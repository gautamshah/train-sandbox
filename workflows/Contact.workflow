<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Created_Email_Notification</fullName>
        <description>Contact Created Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>IN_AN_BR_East_KOL_Email_Notification</fullName>
        <description>IN AN/BR East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_AN_BR_MNSE_KOL_Approved_Notification</fullName>
        <description>IN AN/BR &amp; MNSE KOL Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_AN_BR_North_KOL_Email_Notification</fullName>
        <description>IN AN/BR North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_AN_BR_South_KOL_Email_Notification</fullName>
        <description>IN AN/BR South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_AN_BR_West_KOL_Email_Notification</fullName>
        <description>IN AN/BR West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EBD_East_KOL_Email_Notification</fullName>
        <description>IN EBD East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EBD_KOL_Approved_Notification</fullName>
        <ccEmails>test@test.com</ccEmails>
        <description>IN EBD KOL Approved Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_EBD_North_KOL_Email_Notification</fullName>
        <description>IN EBD North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EBD_South_KOL_Email_Notification</fullName>
        <description>IN EBD South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EBD_West_KOL_Email_Notification</fullName>
        <description>IN EBD West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EMID_East_KOL_Email_Notification</fullName>
        <description>IN EMID East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EMID_KOL_Approved_Notification</fullName>
        <description>IN EMID KOL Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_EMID_North_KOL_Email_Notification</fullName>
        <description>IN EMID North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EMID_South_KOL_Email_Notification</fullName>
        <description>IN EMID South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_EMID_West_KOL_Email_Notification</fullName>
        <description>IN EMID West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shankar.sudhir@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Hernia_East_KOL_Email_Notification</fullName>
        <description>IN Hernia East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Hernia_KOL_Approved_Notification</fullName>
        <description>IN Hernia KOL Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_Hernia_North_KOL_Email_Notification</fullName>
        <description>IN Hernia North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Hernia_South_KOL_Email_Notification</fullName>
        <description>IN Hernia South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Hernia_West_KOL_Email_Notification</fullName>
        <description>IN Hernia West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>zhen-xiong.yap@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_MNSE_East_KOL_Email_Notification</fullName>
        <description>IN MNSE East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>zhen-xiong.yap@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_MNSE_North_KOL_Email_Notification</fullName>
        <description>IN MNSE North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>zhen-xiong.yap@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_MNSE_South_KOL_Email_Notification</fullName>
        <description>IN MNSE South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>zhen-xiong.yap@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_MNSE_West_KOL_Email_Notification</fullName>
        <description>IN MNSE West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>zhen-xiong.yap@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_PCS_NC_East_KOL_Email_Notification</fullName>
        <description>IN PCS&amp;NC East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_PCS_NC_KOL_Approved_Notification</fullName>
        <description>IN PCS&amp;NC KOL Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_PCS_NC_North_KOL_Email_Notification</fullName>
        <description>IN PCS&amp;NC North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_PCS_NC_South_KOL_Email_Notification</fullName>
        <description>IN PCS&amp;NC South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_PCS_NC_West_KOL_Email_Notification</fullName>
        <description>IN PCS&amp;NC West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Suture_East_KOL_Email_Notification</fullName>
        <description>IN Suture East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Suture_KOL_Approved_Notification</fullName>
        <description>IN Suture KOL Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_Suture_North_KOL_Email_Notification</fullName>
        <description>IN Suture North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Suture_South_KOL_Email_Notification</fullName>
        <description>IN Suture South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Suture_West_KOL_Email_Notification</fullName>
        <description>IN Suture West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Vents_East_KOL_Email_Notification</fullName>
        <description>IN Vents East KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Vents_KOL_Approved_Notification</fullName>
        <description>IN Vents KOL Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Approved</template>
    </alerts>
    <alerts>
        <fullName>IN_Vents_North_KOL_Email_Notification</fullName>
        <description>IN Vents North KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Vents_South_KOL_Email_Notification</fullName>
        <description>IN Vents South KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>IN_Vents_West_KOL_Email_Notification</fullName>
        <description>IN Vents West KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ankit.singhal1@covidien.com.testuser</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>MY_RMS_VT_MS_KOL_Email_Notification</fullName>
        <description>MY RMS, VT &amp; MS KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>fenny.saputra@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>MY_Surgical_KOL_Email_Notification</fullName>
        <description>MY Surgical KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>fenny.saputra@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>Notify_ID_EMID_Head_for_KOL_Request_ID</fullName>
        <description>Notify ID EMID Head for KOL Request (ID)</description>
        <protected>false</protected>
        <recipients>
            <recipient>ID_EMID_Head</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/ID_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>Notify_PACE_and_Marketing_for_KOL_Request_Korea</fullName>
        <ccEmails>diane.park@covidien.com</ccEmails>
        <description>Notify PACE and Marketing for KOL Request (Korea)</description>
        <protected>false</protected>
        <recipients>
            <recipient>agatha.kim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jack.hwang@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeena.kim@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeffrey.kim@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.kim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shawn.kim@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>Notify_PACE_for_KOL_request_for_Singapore</fullName>
        <ccEmails>billshan3@gmail.com</ccEmails>
        <ccEmails>pace.asia@covidien.com</ccEmails>
        <description>Notify PACE for KOL request (for Singapore)</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>PDPA_Email_Notification_MY</fullName>
        <ccEmails>pdpa.my@covidien.com</ccEmails>
        <description>PDPA Email Notification - MY</description>
        <protected>false</protected>
        <recipients>
            <field>Personal_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pdpa.my@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Asia_Template/PDPA_Email_Notification_MY</template>
    </alerts>
    <alerts>
        <fullName>TH_KOL_Email_Notification</fullName>
        <description>TH KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Last_Modified_User_s_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>TW_ET_KOL_Email_Notification</fullName>
        <description>TW ET KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shiau-lei.dai@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_EbD_KOL_Email_Notification</fullName>
        <description>TW EbD KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>josephine.liu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_Fail_Catch_KOL_Email_Notification</fullName>
        <ccEmails>carol.chen29@medtronic.com</ccEmails>
        <description>TW Fail-Catch KOL Email Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_RMS_KOL_Email_Notification</fullName>
        <description>TW RMS KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jessie.ho@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_SD_KOL_Email_Notification</fullName>
        <description>TW SD KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kenny.yu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_VT_KOL_Email_Notification</fullName>
        <description>TW VT KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eddy.huang@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KOL_Request_TW</template>
    </alerts>
    <alerts>
        <fullName>VN_EbD_KOL_Email_Notification</fullName>
        <description>VN EbD KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>thu.huong.nguyen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vy.huynh@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>VN_PCNC_KOL_Email_Notification</fullName>
        <description>VN PCNC KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>fenny.saputra@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>VN_PMR_KOL_Email_Notification</fullName>
        <description>VN PMR KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>fenny.saputra@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <alerts>
        <fullName>VN_Surgical_KOL_Email_Notification</fullName>
        <description>VN Surgical KOL Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>anh.h.le@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ngoc.vinh.nguyen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/IN_KOL_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Consent_status_Accepted</fullName>
        <field>Privacy_Status__c</field>
        <literalValue>Accepted</literalValue>
        <name>Consent status - Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_In_Process_Update_Record_Type</fullName>
        <description>Update the record type for in process contacts.</description>
        <field>RecordTypeId</field>
        <lookupValue>In_Process_Connected_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact In Process - Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Private_Covidien_Contact</fullName>
        <field>Private_Contact__c</field>
        <literalValue>1</literalValue>
        <name>Contact - Private Covidien Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Affiliated_Contact_Last</fullName>
        <description>Update the Last Name of an Affiliated Contact if the Master Contact Name changes.</description>
        <field>LastName</field>
        <formula>Master_Contact_Record__r.LastName</formula>
        <name>Contact - Update Affiliated Contact Last</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Affiliated_Contact_Name</fullName>
        <description>Update the first name of an Affiliated Contact when the Master Contact Name changes.</description>
        <field>FirstName</field>
        <formula>Master_Contact_Record__r.FirstName</formula>
        <name>Contact - Update Affiliated Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Email_Last_Updated_Fiel</fullName>
        <description>Field update to stamp today&apos;s date on the hidden field that indicates when an Email address has been changed.</description>
        <field>Email_Last_Updated_Date__c</field>
        <formula>TODAY()</formula>
        <name>Contact-Update Email Last Updated Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_In_Process_Contact</fullName>
        <description>Update In Process for Connected Contact when there is a Master Contact Record</description>
        <field>Contact_Creation_Status__c</field>
        <literalValue>Contact Creation Approved</literalValue>
        <name>Contact: Update In Process Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_phoneAtAccount_Autopopulate_Asi</fullName>
        <field>Phone_Number_at_Account__c</field>
        <formula>Account.Phone</formula>
        <name>Contact_phoneAtAccount_Autopopulate _Asi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDPA_Notification_Date</fullName>
        <field>Date_of_Notification__c</field>
        <formula>today()</formula>
        <name>PDPA Notification Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PH_Update_Distributor_Contact</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Distributor_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PH Update Distributor Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Send_PDPA</fullName>
        <field>Send_PDPA__c</field>
        <literalValue>0</literalValue>
        <name>Reset Send PDPA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update</fullName>
        <description>Record_Type_Original__c</description>
        <field>Record_Type_Original__c</field>
        <formula>If(Text(CurrencyIsoCode) = &quot;USD&quot;,&quot;Affiliated_Contact_US&quot; 
,&quot;Affiliated_Clinician&quot;)</formula>
        <name>Update Record Type Original</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Record_Type_In_Process</fullName>
        <field>RecordTypeId</field>
        <lookupValue>In_Process_Connected_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Contact Record Type - In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Modified_User_s_Manager</fullName>
        <field>Last_Modified_User_s_Manager__c</field>
        <formula>$User.User_Manager_Email__c</formula>
        <name>Update Last Modified User&apos;s Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Request_for_KOL_Eval_to_False</fullName>
        <field>KOL_Status__c</field>
        <literalValue>0</literalValue>
        <name>Update Request for KOL Eval to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Contact_Photograph</fullName>
        <field>Contact_Photograph__c</field>
        <name>Wipe Contact Photograph</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Home_Phone</fullName>
        <field>HomePhone</field>
        <name>Wipe Home Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Mail_State</fullName>
        <field>MailingState</field>
        <name>Wipe Mail State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Mailing_City</fullName>
        <field>MailingCity</field>
        <name>Wipe Mailing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Mailing_Country</fullName>
        <field>MailingCountry</field>
        <name>Wipe Mailing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Mailing_Street</fullName>
        <field>MailingStreet</field>
        <name>Wipe Mailing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Mailing_Zip_Code</fullName>
        <field>MailingPostalCode</field>
        <name>Wipe Mailing Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Mobile_Phone</fullName>
        <field>MobilePhone</field>
        <name>Wipe Mobile Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Other_Phone</fullName>
        <field>OtherPhone</field>
        <name>Wipe Other Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Personal_Website</fullName>
        <field>Website__c</field>
        <name>Wipe Personal Website</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Personal_email</fullName>
        <field>Personal_Email__c</field>
        <name>Wipe Personal email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CN EMID KOL Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Request_EMID_KOL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Master Clinician</value>
        </criteriaItems>
        <description>When seller requests to mark a contact as KOL, send an email to Marketing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN EbD KOL Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Request_EbD_KOL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Master Clinician</value>
        </criteriaItems>
        <description>When seller requests to mark a contact as KOL, send an email to Marketing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN RMS KOL Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Request_RMS_KOL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Master Clinician</value>
        </criteriaItems>
        <description>When seller requests to mark a contact as KOL, send an email to PACE.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN STI KOL Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Request_STI_KOL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Master Clinician</value>
        </criteriaItems>
        <description>When seller requests to mark a contact as KOL, send an email to Marketing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN VT KOL Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Request_VT_KOL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Master Clinician</value>
        </criteriaItems>
        <description>When seller requests to mark a contact as KOL, send an email to Marketing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Email is Changed</fullName>
        <actions>
            <name>Contact_Update_Email_Last_Updated_Fiel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update the Email Last Updated (hidden) field when a Contact&apos;s Email (Business Email) has been added or changed.  This will be used for European contacts for a daily report.
After org merge, will have to update for Country.</description>
        <formula>OR(AND( ISNEW(), (NOT(ISBLANK(Email)))), ISCHANGED(Email))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Master Contact %3C%3ENull</fullName>
        <actions>
            <name>Contact_In_Process_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Update_In_Process_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update the In-Process field on the contact to Contact Creation Approved when there is a Master Contact selected.</description>
        <formula>AND(NOT(ISBLANK( Master_Contact_Record__c )), NOT($RecordType.DeveloperName= &quot;Master_Contact&quot;), NOT($User.ProfileId=&quot;00eU0000000dLrt&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Master Contact Is Null</fullName>
        <actions>
            <name>Update_Contact_Record_Type_In_Process</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow to update the In-Process field on the contact to Contact Creation Approved when there is not a Master Contact selected.</description>
        <formula>AND((ISBLANK( Master_Contact_Record__c )), NOT($RecordType.DeveloperName= &quot;Master_Contact&quot;), NOT($User.ProfileId=&quot;00eU0000000dLrt&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Master Contact Name Change</fullName>
        <actions>
            <name>Contact_Update_Affiliated_Contact_Last</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Update_Affiliated_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Affiliated Contact Name when the Master Contact Record Name is changed.</description>
        <formula>NOT(ISBLANK (  Master_Contact_Record__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Private Contact</fullName>
        <actions>
            <name>Contact_Private_Covidien_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Contact is created, sets the Private Contact checkbox to TRUE if the User&apos;s Profile is &apos;Private Covidien User&apos;. ProfileId is used in case the Profile Name is changed</description>
        <formula>IF($User.ProfileId = &quot;00eU0000000ddrF&quot;, TRUE, FALSE)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact Created Email alert</fullName>
        <actions>
            <name>Contact_Created_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When contact is created and Created date = Today, send email to Contact email (Email field - Email) with text indicating he has been created and with Opt-out Instructions</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact Created%2FOpt-Out Email alert</fullName>
        <actions>
            <name>Contact_Created_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>When contact is created and Created date = Today, send email to Contact email (Email field - Email) with text indicating he has been created and with Opt-out Instructions. Also if Contact Email Opt out = TRUE, send Notification email to Owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact Record Type Original</fullName>
        <actions>
            <name>Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set Specialty 1 and Specialty 2 to read-only</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Specialty1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Specialty_2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>US - Contact - Physician</value>
        </criteriaItems>
        <description>Sets &quot;Specialty 1&quot; and &quot;Specialty 2&quot; fields to read-only by changing the record type which changes the page layout.  (Boulder 11/17)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact_phoneAtAccount_Autopopulate _Asia</fullName>
        <actions>
            <name>Contact_phoneAtAccount_Autopopulate_Asi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Asia - HK / SG</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ID EMID KOL Request</fullName>
        <actions>
            <name>Notify_ID_EMID_Head_for_KOL_Request_ID</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>ID</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EMID</value>
        </criteriaItems>
        <description>When ID EMID team requests to mark a contact as KOL, send an email to EMID Head</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN AN%2FBR %26 MNSE KOL Approved</fullName>
        <actions>
            <name>IN_AN_BR_MNSE_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an AN/BR KOL, the relevant AN/BR BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;ANBR &amp; MNSE&apos;)),  INCLUDES( KOL_Type__c, &apos;ANBR &amp; MNSE&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN AN%2FBR East KOL Request</fullName>
        <actions>
            <name>IN_AN_BR_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When AN/BR sales rep from the East requests to mark a contact as KOL, send an email to the East AN/BR Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;AN/BR&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN AN%2FBR North KOL Request</fullName>
        <actions>
            <name>IN_AN_BR_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When AN/BR sales rep from the North requests to mark a contact as KOL, send an email to the North AN/BR Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;AN/BR&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN AN%2FBR South KOL Request</fullName>
        <actions>
            <name>IN_AN_BR_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When AN/BR sales rep from the South requests to mark a contact as KOL, send an email to the South AN/BR Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;AN/BR&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN AN%2FBR West KOL Request</fullName>
        <actions>
            <name>IN_AN_BR_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When AN/BR sales rep from the West requests to mark a contact as KOL, send an email to the West AN/BR Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;AN/BR&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EBD East KOL Request</fullName>
        <actions>
            <name>IN_EBD_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EBD sales rep from the East requests to mark a contact as KOL, send an email to the East EBD Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EbD&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EbD&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EBD KOL Approved</fullName>
        <actions>
            <name>IN_EBD_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an EBD KOL, the relevant EBD BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;EbD&apos;)),  INCLUDES( KOL_Type__c, &apos;EbD&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN EBD North KOL Request</fullName>
        <actions>
            <name>IN_EBD_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EBD sales rep from the North requests to mark a contact as KOL, send an email to the North EBD Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EbD&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EbD&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EBD South KOL Request</fullName>
        <actions>
            <name>IN_EBD_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EBD sales rep from the South requests to mark a contact as KOL, send an email to the South EBD Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EbD&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EbD&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EBD West KOL Request</fullName>
        <actions>
            <name>IN_EBD_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EBD sales rep from the West requests to mark a contact as KOL, send an email to the West EBD Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EbD&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EbD&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EMID East KOL Request</fullName>
        <actions>
            <name>IN_EMID_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EMID sales rep from the East requests to mark a contact as KOL, send an email to the East EMID Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EMID&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EMID&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EMID KOL Approved</fullName>
        <actions>
            <name>IN_EMID_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an EMID KOL, the relevant EMID BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;EMID&apos;)),  INCLUDES( KOL_Type__c, &apos;EMID&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN EMID North KOL Request</fullName>
        <actions>
            <name>IN_EMID_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EMID sales rep from the North requests to mark a contact as KOL, send an email to the North EMID Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EMID&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EMID&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EMID South KOL Request</fullName>
        <actions>
            <name>IN_EMID_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EMID sales rep from the South requests to mark a contact as KOL, send an email to the South EMID Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EMID&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EMID&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN EMID West KOL Request</fullName>
        <actions>
            <name>IN_EMID_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When EMID sales rep from the West requests to mark a contact as KOL, send an email to the West EMID Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;EMID&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;EMID&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Hernia East KOL Request</fullName>
        <actions>
            <name>IN_Hernia_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Hernia sales rep from the East requests to mark a contact as KOL, send an email to the East Hernia Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Hernia&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Mesh&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Hernia KOL Approved</fullName>
        <actions>
            <name>IN_Hernia_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an Hernia KOL, the relevant Hernia BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;Hernia&apos;)),  INCLUDES( KOL_Type__c, &apos;Hernia&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN Hernia North KOL Request</fullName>
        <actions>
            <name>IN_Hernia_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Hernia sales rep from the North requests to mark a contact as KOL, send an email to the North Hernia Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Hernia&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Mesh&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Hernia South KOL Request</fullName>
        <actions>
            <name>IN_Hernia_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Hernia sales rep from the South requests to mark a contact as KOL, send an email to the South Hernia Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Hernia&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Mesh&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Hernia West KOL Request</fullName>
        <actions>
            <name>IN_Hernia_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Hernia sales rep from the West requests to mark a contact as KOL, send an email to the West Hernia Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Hernia&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Mesh&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN MNSE East KOL Request</fullName>
        <actions>
            <name>IN_MNSE_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When MNSE sales rep from the East requests to mark a contact as KOL, send an email to the East MNSE Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;MNSE&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN MNSE North KOL Request</fullName>
        <actions>
            <name>IN_MNSE_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When MNSE sales rep from the North requests to mark a contact as KOL, send an email to the North MNSE Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;MNSE&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN MNSE South KOL Request</fullName>
        <actions>
            <name>IN_MNSE_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When MNSE sales rep from the South requests to mark a contact as KOL, send an email to the South MNSE Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;MNSE&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN MNSE West KOL Request</fullName>
        <actions>
            <name>IN_MNSE_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When MNSE sales rep from the West requests to mark a contact as KOL, send an email to the West MNSE Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;MNSE&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;ANBR &amp; MNSE&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN PCS%26NC East KOL Request</fullName>
        <actions>
            <name>IN_PCS_NC_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When PCS&amp;NC sales rep from the East requests to mark a contact as KOL, send an email to the East PCS&amp;NC Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;VT &amp; MS&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;PCS &amp; NC&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN PCS%26NC KOL Approved</fullName>
        <actions>
            <name>IN_PCS_NC_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an PCS&amp;NC KOL, the relevant PCS&amp;NC BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;VT &amp; MS&apos;)),  INCLUDES( KOL_Type__c, &apos;VT &amp; MS&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN PCS%26NC North KOL Request</fullName>
        <actions>
            <name>IN_PCS_NC_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When PCS&amp;NC sales rep from the North requests to mark a contact as KOL, send an email to the North PCS&amp;NC Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;VT &amp; MS&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;PCS &amp; NC&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN PCS%26NC South KOL Request</fullName>
        <actions>
            <name>IN_PCS_NC_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When PCS&amp;NC sales rep from the South requests to mark a contact as KOL, send an email to the South PCS&amp;NC Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;VT &amp; MS&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;PCS &amp; NC&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN PCS%26NC West KOL Request</fullName>
        <actions>
            <name>IN_PCS_NC_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When PCS&amp;NC sales rep from the West requests to mark a contact as KOL, send an email to the West PCS&amp;NC Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;VT &amp; MS&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;PCS &amp; NC&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Suture East KOL Request</fullName>
        <actions>
            <name>IN_Suture_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Suture  sales rep from the East requests to mark a contact as KOL, send an email to the East Suture  Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Sutures&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Wound Closure&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Suture KOL Approved</fullName>
        <actions>
            <name>IN_Suture_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an Suture KOL, the relevant Suture BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;Sutures&apos;)),  INCLUDES( KOL_Type__c, &apos;Sutures&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN Suture North KOL Request</fullName>
        <actions>
            <name>IN_Suture_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Suture  sales rep from the North requests to mark a contact as KOL, send an email to the North Suture  Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Sutures&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Wound Closure&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Suture South KOL Request</fullName>
        <actions>
            <name>IN_Suture_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Suture  sales rep from the South requests to mark a contact as KOL, send an email to the South Suture  Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Sutures&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Wound Closure&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Suture West KOL Request</fullName>
        <actions>
            <name>IN_Suture_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Suture  sales rep from the West requests to mark a contact as KOL, send an email to the West Suture Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Sutures&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;STI-Wound Closure&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Vents East KOL Request</fullName>
        <actions>
            <name>IN_Vents_East_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Vents sales rep from the East requests to mark a contact as KOL, send an email to the East Vents Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Ventilator&apos;),  CONTAINS($User.Division, &apos;East&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;Vents&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Vents KOL Approved</fullName>
        <actions>
            <name>IN_Vents_KOL_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When contact is marked as an Vents KOL, the relevant Vents BU Head or RSM is informed.</description>
        <formula>AND(   Account.BillingCountry = &apos;IN&apos;,  NOT(INCLUDES( PRIORVALUE(KOL_Type__c), &apos;Ventilator&apos;)),  INCLUDES( KOL_Type__c, &apos;Ventilator&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IN Vents North KOL Request</fullName>
        <actions>
            <name>IN_Vents_North_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Vents sales rep from the North requests to mark a contact as KOL, send an email to the North Vents Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Ventilator&apos;),  CONTAINS($User.Division, &apos;North&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;Vents&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Vents South KOL Request</fullName>
        <actions>
            <name>IN_Vents_South_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Vents sales rep from the South requests to mark a contact as KOL, send an email to the South Vents Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Ventilator&apos;),  CONTAINS($User.Division, &apos;South&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;Vents&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN Vents West KOL Request</fullName>
        <actions>
            <name>IN_Vents_West_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When Vents sales rep from the West requests to mark a contact as KOL, send an email to the West Vents Manager only.</description>
        <formula>AND(   KOL_Status__c = TRUE,  Account.BillingCountry = &apos;IN&apos;,  CONTAINS(TEXT($User.Sales_Org_PL__c), &apos;Ventilator&apos;),  CONTAINS($User.Division, &apos;West&apos;),  NOT(INCLUDES(KOL_Type__c, &apos;Vents&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>KOL Request</fullName>
        <actions>
            <name>Notify_PACE_for_KOL_request_for_Singapore</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Master Clinician</value>
        </criteriaItems>
        <description>When seller requests to mark a contact as KOL, send an email to PACE.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>KR KOL Request</fullName>
        <actions>
            <name>Notify_PACE_and_Marketing_for_KOL_Request_Korea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <description>When KR seller requests to mark a contact as KOL, send an email to PACE &amp; Marketing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY RMS%2C VT %26 MS KOL Request</fullName>
        <actions>
            <name>MY_RMS_VT_MS_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>MY</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>contains</operation>
            <value>RMS,VT,MS</value>
        </criteriaItems>
        <description>When RMS, VT, &amp; MS sales rep requests to mark a contact as KOL, send an email to RMS, VT &amp; MS BU Head only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY Surgical KOL Request</fullName>
        <actions>
            <name>MY_Surgical_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>MY</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>contains</operation>
            <value>EbD,STI,EMID</value>
        </criteriaItems>
        <description>When STI, EMID or EBD sales rep requests to mark a contact as KOL, send an email to Surgical BU Head only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PDPA Collection - Opt out - MY</fullName>
        <actions>
            <name>Wipe_Contact_Photograph</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Home_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Mail_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Mailing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Mobile_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Other_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Personal_Website</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Personal_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (  ISPICKVAL( Privacy_Status__c , &quot;Declined&quot;) , ISPICKVAL(  Country__c  ,&quot;MY&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PDPA Collection2 - Opt out - MY</fullName>
        <actions>
            <name>Wipe_Mailing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wipe_Mailing_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (  ISPICKVAL( Privacy_Status__c , &quot;Declined&quot;) , ISPICKVAL(  Country__c  ,&quot;MY&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PDPA Notification - Opt out - MY</fullName>
        <actions>
            <name>PDPA_Email_Notification_MY</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Consent_status_Accepted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PDPA_Notification_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND ( NOT(ISBLANK(  Email )), ISPICKVAL( Contact_Creation_Status__c , &quot;Contact Creation Approved&quot;), ISPICKVAL(  Country__c  ,&quot;MY&quot;), ISBLANK( Date_of_Notification__c ), ISBLANK( Notification_URL__c ), ISBLANK( Master_Contact_Record__c ), RecordType.Name = &quot;In Process Connected Contact&quot;, $Profile.Name  &lt;&gt; &quot;CRM SSC Admin and Data Support&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PDPA Notification ReSend - Opt out - MY</fullName>
        <actions>
            <name>PDPA_Email_Notification_MY</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Consent_status_Accepted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PDPA_Notification_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Send_PDPA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND ( NOT(ISBLANK(  Email )), Send_PDPA__c = True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PH Update Distributor Contact</fullName>
        <actions>
            <name>PH_Update_Distributor_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>In Process Distributor Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Creation_Status__c</field>
            <operation>equals</operation>
            <value>Contact Creation Approved</value>
        </criteriaItems>
        <description>Once the status of contact  of record type &quot;In Process Distributor Contact&quot; is changed to &quot;Contact Creation Approved&quot;, the record type should change to &quot;Distributor Contact&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TH KOL Request</fullName>
        <actions>
            <name>TH_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Last_Modified_User_s_Manager__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When Thailand sales rep requests to mark a contact as KOL, send an email to sales rep&apos;s direct manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TW ET KOL Request</fullName>
        <actions>
            <name>TW_ET_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>ET</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Connected Clinician,In Process Connected Contact</value>
        </criteriaItems>
        <description>When EBD sales rep requests to mark a contact as KOL, send an email to EBD BU Head and sales ops.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW EbD KOL Request</fullName>
        <actions>
            <name>TW_EbD_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EbD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Connected Clinician,In Process Connected Contact</value>
        </criteriaItems>
        <description>When EBD sales rep requests to mark a contact as KOL, send an email to EBD BU Head and sales ops.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW Other KOL Request</fullName>
        <actions>
            <name>TW_Fail_Catch_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>notEqual</operation>
            <value>EbD,EMID,RMS,STI,VT,ET</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Connected Clinician,In Process Connected Contact</value>
        </criteriaItems>
        <description>When a sales rep requests to mark a contact as KOL, send an email to  sales ops only. This is the fail-catch for the KOL notification process.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW RMS KOL Request</fullName>
        <actions>
            <name>TW_RMS_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Connected Clinician,In Process Connected Contact</value>
        </criteriaItems>
        <description>When RMS sales rep requests to mark a contact as KOL, send an email to RMS BU Head and sales ops.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW SD KOL Request</fullName>
        <actions>
            <name>TW_SD_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EMID,STI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Connected Clinician,In Process Connected Contact</value>
        </criteriaItems>
        <description>When EMIT and STI sales rep requests to mark a contact as KOL, send an email to SD BU Head and sales ops.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW VT KOL Request</fullName>
        <actions>
            <name>TW_VT_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>VT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Connected Clinician,In Process Connected Contact</value>
        </criteriaItems>
        <description>When VT sales rep requests to mark a contact as KOL, send an email to VT BU Head and sales ops.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Modified User%27s Manager</fullName>
        <actions>
            <name>Update_Last_Modified_User_s_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>TH</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VN EbD KOL Request</fullName>
        <actions>
            <name>VN_EbD_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>VN</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EbD</value>
        </criteriaItems>
        <description>When EbD sales rep requests to mark a contact as KOL, send an email to the EbD Manager only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VN PCNC KOL Request</fullName>
        <actions>
            <name>VN_PCNC_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>VN</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>VT &amp; MS</value>
        </criteriaItems>
        <description>When PCNC sales rep requests to mark a contact as KOL, send an email to the PCNC Manager only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VN PMR KOL Request</fullName>
        <actions>
            <name>VN_PMR_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>VN</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <description>When PMR sales rep requests to mark a contact as KOL, send an email to the PMR Manager only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VN Surgical KOL Request</fullName>
        <actions>
            <name>VN_Surgical_KOL_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Request_for_KOL_Eval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.KOL_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>VN</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EMID,STI</value>
        </criteriaItems>
        <description>When surgical sales rep requests to mark a contact as KOL, send an email to the Surgical Manager only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call Contact</fullName>
        <actions>
            <name>zaapit__Call_Contact</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Call Contact  - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email Contact</fullName>
        <actions>
            <name>zaapit__Email_Contact</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Email Contact - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter Contact</fullName>
        <actions>
            <name>zaapit__Send_Letter_Contact</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Letter Contact - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Quote Contact</fullName>
        <actions>
            <name>zaapit__Send_Quote_Contact</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Quote Contact - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>zaapit__Call_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Letter_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Quote_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Quote</subject>
    </tasks>
</Workflow>
