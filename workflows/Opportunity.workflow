<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CN_Inform_oppty_owner_of_rejected_approval</fullName>
        <description>CN - Inform oppty owner of rejected approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_Rejected_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Contract_Date_Notification</fullName>
        <description>EMEA Contract Date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_five_days_before_Contract_date</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Tender_Expiration_Date_Notification</fullName>
        <description>EMEA Tender Expiration Date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_five_days_before_Tender_exp_date</template>
    </alerts>
    <alerts>
        <fullName>LQS_Opportunity_transfer_email_update</fullName>
        <description>LQS Opportunity transfer email update</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Lead_Converted_Opportunity_Notification_alert</fullName>
        <description>Lead Converted Opportunity Notification alert</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Lead_Converted_Opportunity_Follow_up</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Opp_Manager</fullName>
        <description>Send Email to Opp Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Overdue_Lead_Convert_Opportunity_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Email_to_Opp_Manager</fullName>
        <description>Send Notification Email to Opp Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Overdue_Lead_Convert_Opportunity_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Overdue_Lead_Converted_Opportunity</fullName>
        <description>Send Notification Overdue Lead Converted Opportunity</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Overdue_Lead_Convert_Opportunity_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>TW_EbD_Tender_Expiration_Email_Reminder</fullName>
        <description>TW-EbD Tender Expiration Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>josephine.liu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_Fail_Catch_Tender_Expiration_Email_Reminder</fullName>
        <description>TW-Fail Catch Tender Expiration Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_RMS_Tender_Expiration_Email_Reminder</fullName>
        <description>TW-RMS Tender Expiration Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jessie.ho@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_SD_Tender_Expiration_Email_Reminder</fullName>
        <description>TW-SD Tender Expiration Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>kenny.yu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder_TW</template>
    </alerts>
    <alerts>
        <fullName>TW_VT_MS_Tender_Expiration_Email_Reminder</fullName>
        <description>TW-VT&amp;MS Tender Expiration Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>eddy.huang@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder_TW</template>
    </alerts>
    <alerts>
        <fullName>Tender_Reminder</fullName>
        <description>Tender Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Tender_Reminder_IN</fullName>
        <description>Tender Reminder IN</description>
        <protected>false</protected>
        <recipients>
            <field>Reminder_Email1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Reminder_Email2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Reminder_Email3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Tender_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Test_opp_qualified_lead_email</fullName>
        <description>Test opp qualified lead email</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Overdue_Lead_Convert_Opportunity_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>USMS_NB_CW_and_AR_CL_NBF_Reminder</fullName>
        <description>USMS - NB CW and AR CL NBF Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/USMS_NBF_Reminder_on_Closed_opps_3_Days</template>
    </alerts>
    <alerts>
        <fullName>US_RMS_Notification_to_owner_and_manager</fullName>
        <description>US RMS Notification to owner and manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/US_RMS_Lead_Converted_Opportunity_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Verified_to_False</fullName>
        <field>Verified__c</field>
        <literalValue>0</literalValue>
        <name>Change Verified to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_closed_date</fullName>
        <field>Closed_Date__c</field>
        <name>Clear closed date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Forecast_update_to_0</fullName>
        <description>Forecast field is updated to 0% when a user closes out an opportunity under New Business - Closed Missed or At Risk - Closed Saved</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Omit (0% Probability)</literalValue>
        <name>Forecast update to 0%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Forecast_update_to_100</fullName>
        <description>Forecast field is updated to 100% when a user closes out an opportunity under New Business - Closed Won or At Risk - Closed Lost</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Closed (100% Probability)</literalValue>
        <name>Forecast update to 100%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Include_in_Call_Check</fullName>
        <field>ZVP_Forecast__c</field>
        <literalValue>1</literalValue>
        <name>Include in Call Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Include_in_Call_Uncheck</fullName>
        <field>ZVP_Forecast__c</field>
        <literalValue>0</literalValue>
        <name>Include in Call Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JP_Update_Field_Initial_Close_Date</fullName>
        <field>JP_Initial_Close_Date__c</field>
        <formula>CloseDate</formula>
        <name>JP Update Field Initial Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JP_Update_Field_RecordType_Name</fullName>
        <field>JP_RecordType_Name__c</field>
        <formula>$RecordType.DeveloperName</formula>
        <name>JP Update Field RecordType Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NCPCS_Set_Probability_to_100</fullName>
        <field>Confidence_Level__c</field>
        <formula>100</formula>
        <name>NCPCS Set Probability to 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Op_Closed_Date</fullName>
        <description>Set date of field when the Opportunity is marked Closed (Won, Lost, or Resolved)?</description>
        <field>Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Op Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Add_Account_Name</fullName>
        <description>Add account name at the beginning of any new opportunity.</description>
        <field>Name</field>
        <formula>Account.Name &amp; &quot;: &quot; &amp;   Name</formula>
        <name>Opportunity: Add Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Append_Name_with_Original</fullName>
        <description>Append name with Account Name and Original Opportunity Name when the opportunity is edited.</description>
        <field>Name</field>
        <formula>Account.Name &amp; &quot;: &quot; &amp;  Original_Opportunity_Name__c</formula>
        <name>Opportunity - Append Name with Original</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Notes_Append</fullName>
        <field>Opportunity_Notes__c</field>
        <formula>PRIORVALUE( Opportunity_Notes__c ) + BR() + 
LastModifiedBy.Username + BR() + 
TEXT(LastModifiedDate) + BR() + 
 New_Notes__c</formula>
        <name>Opportunity Notes Append</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Private_Opportunity</fullName>
        <field>Private_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - Private Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Next_Step_Date_to_null</fullName>
        <description>Sets the value of the &quot;Next Step Date&quot; field to null</description>
        <field>Next_Step_Date__c</field>
        <name>Opportunity: Set Next Step Date to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Sales_Rep_Next_Step_nul</fullName>
        <description>Sets &quot;Sales Rep Next Step&quot; to null</description>
        <field>Sales_Rep_Next_Step__c</field>
        <name>Opportunity: Set Sales Rep Next Step nul</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Probability_100</fullName>
        <description>Update probability field when user selects gut probability -  100% - Sales Data Verified in Cognos</description>
        <field>Probability</field>
        <formula>1.00</formula>
        <name>Opportunity: Update Probability 100%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Probability_25</fullName>
        <description>Update probability to 25% when gut probability is updated to 25% introduction.</description>
        <field>Probability</field>
        <formula>0.25</formula>
        <name>Opportunity: Update Probability 25%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Probability_50</fullName>
        <description>Update probability on opportunity when gut probability is changed to 50% - In Process.</description>
        <field>Probability</field>
        <formula>0.50</formula>
        <name>Opportunity: Update Probability 50%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Probability_75</fullName>
        <description>Update probability field when gut probability: 75% -expected to close is selected by user.</description>
        <field>Probability</field>
        <formula>0.75</formula>
        <name>Opportunity: Update Probability 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Probability_90</fullName>
        <description>Update probability to 90% when user selects gut probability - 90% - Verbal Close(PO pending)</description>
        <field>Probability</field>
        <formula>0.90</formula>
        <name>Opportunity: Update Probability 90%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_OCR_Email_of_opportunity</fullName>
        <description>This copies email of opportunity&apos;s contact role in OCR Email of opportunity</description>
        <field>OCR_Email__c</field>
        <formula>Account.Email_Address__c</formula>
        <name>Populate OCR Email of opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reprocessing_Opportunity_is_False</fullName>
        <field>Reprocessing_Opportunity__c</field>
        <literalValue>0</literalValue>
        <name>Reprocessing Opportunity is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reprocessing_Opportunity_is_True</fullName>
        <field>Reprocessing_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Reprocessing Opportunity is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RollBackOppCloseDate</fullName>
        <field>CloseDate</field>
        <formula>PRIORVALUE( CloseDate )</formula>
        <name>RollBackOppCloseDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Next_Fiscal_Year_Impact_field</fullName>
        <description>Sets the &quot;Next Fiscal Year Impact&quot; field.  The evaluation is too large for a formula field.</description>
        <field>Next_Fiscal_Year_Impact__c</field>
        <formula>Amount  -  Current_Fiscal_Year_Impact__c</formula>
        <name>Set &quot;Next Fiscal Year Impact&quot; field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RM_Forecast_to_Best_Case</fullName>
        <description>Update RM Forecast Status to Best Case when Forecast Category is updated</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Best Case</literalValue>
        <name>Set RM Forecast to Best Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RM_Forecast_to_Closed</fullName>
        <description>Update RM Forecast Status to Closed when Forecast Category is updated</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Set RM Forecast to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RM_Forecast_to_Commit</fullName>
        <description>Update RM Forecast Category to Commit when Forecast Category is updated</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Commit</literalValue>
        <name>Set RM Forecast to Commit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RM_Forecast_to_Omitted</fullName>
        <description>Update the RM Forecast Status to Omitted when forecast category is updated</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Omitted</literalValue>
        <name>Set RM Forecast to Omitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RM_Forecast_to_Pipeline</fullName>
        <description>Update RM Forecast to Pipeline when Forecast Category is updated</description>
        <field>RM_Forecast_Status__c</field>
        <literalValue>Pipeline</literalValue>
        <name>Set RM Forecast to Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>The_Reviewed_check_box_should_be_uncheck</fullName>
        <description>The Reviewed check box should be unchecked on any modification on an opportunity stage.</description>
        <field>Reviewed__c</field>
        <literalValue>0</literalValue>
        <name>The Reviewed check box should be uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Include_in_Call</fullName>
        <field>ZVP_Forecast__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Include in Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Include_in_Call_Again</fullName>
        <field>ZVP_Forecast__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Include in Call Again</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateProbPercent</fullName>
        <field>Probability</field>
        <formula>Probability_Score__c</formula>
        <name>UpdateProbPercent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateProgressMonitor</fullName>
        <field>Progress_Monitor__c</field>
        <formula>Probability_of_Success_Sales_Nav__c</formula>
        <name>UpdateProgressMonitor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OCR_Email_of_opportunity</fullName>
        <description>This field update removes the email from OCR Email of opportunity</description>
        <field>OCR_Email__c</field>
        <name>Update OCR Email of opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Manager_Email</fullName>
        <field>Manager_Email__c</field>
        <formula>Manager_Email_Formula__c</formula>
        <name>Update Opp Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Original_Name</fullName>
        <field>Original_Opportunity_Name__c</field>
        <formula>Name</formula>
        <name>Update Opportunity Original Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Type</fullName>
        <field>Type</field>
        <literalValue>New Business</literalValue>
        <name>Update Opportunity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prob_with_SalesNav_Prob</fullName>
        <field>Probability</field>
        <formula>Probability_of_Success_Sales_Nav__c</formula>
        <name>Update Prob with SalesNav Prob</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RMS_Include_in_Call</fullName>
        <field>ZVP_Forecast__c</field>
        <literalValue>1</literalValue>
        <name>Update RMS Include in Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Verified_to_True</fullName>
        <field>Verified__c</field>
        <literalValue>1</literalValue>
        <name>Update Verified to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_manager_email</fullName>
        <field>Manager_Email__c</field>
        <formula>Manager_Email_Formula__c</formula>
        <name>Update manager email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_probability_field_to_0</fullName>
        <field>Confidence_Level__c</field>
        <formula>0</formula>
        <name>Update probability field to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_probability_field_to_100</fullName>
        <field>Confidence_Level__c</field>
        <formula>100</formula>
        <name>Update probability field to 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_probability_field_to_50</fullName>
        <field>Confidence_Level__c</field>
        <formula>50</formula>
        <name>Update probability field to 50%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_probability_field_to_75</fullName>
        <field>Confidence_Level__c</field>
        <formula>75</formula>
        <name>Update probability field to 75</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_probability_to_90</fullName>
        <field>Confidence_Level__c</field>
        <formula>90</formula>
        <name>Update probability to 90</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>smartgoal_opportunity_SmartGoalcheckbox</fullName>
        <field>Smart_Goal__c</field>
        <literalValue>1</literalValue>
        <name>smartgoal_opportunity_SmartGoalcheckbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>smartgoal_opportunity_name_update</fullName>
        <field>Name</field>
        <formula>&apos;SG&apos;+Text(Quarter__c)+&apos;FY&apos;+RIGHT(Text(Fiscal_Year__c ),2)+Name</formula>
        <name>smartgoal_opportunity_name_update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Append Opportunity Notes</fullName>
        <actions>
            <name>Opportunity_Notes_Append</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( AND( ISNEW(), !ISBLANK( New_Notes__c )), ISCHANGED( New_Notes__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CN - Opportunity - Set Manager Email</fullName>
        <actions>
            <name>Update_manager_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(OwnerId ) || ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CN - Unverify Oppty upon change in Amount</fullName>
        <actions>
            <name>Change_Verified_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Amount)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear closed date for reopened opptys</fullName>
        <actions>
            <name>Clear_closed_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears the closed date to a blank value for reopened opportunities.</description>
        <formula>AND((ISPICKVAL(PRIORVALUE(StageName), &quot;Closed Won&quot; ) || ISPICKVAL(PRIORVALUE(StageName), &quot;Closed Lost&quot; ) || ISPICKVAL(PRIORVALUE(StageName), &quot;Closed Missed&quot; ) || ISPICKVAL(PRIORVALUE(StageName), &quot;Closed Saved&quot; ) || ISPICKVAL(PRIORVALUE(StageName), &quot;Closed (Resolved)&quot; ))  &amp;&amp;  (ISPICKVAL(StageName, &quot;Identify&quot; ) ||  ISPICKVAL(StageName, &quot;Develop&quot; )||  ISPICKVAL(StageName, &quot;Evaluate&quot; ) ||  ISPICKVAL(StageName, &quot;Propose&quot; ) || ISPICKVAL(StageName, &quot;Negotiate&quot; )),OR( RecordTypeId  = &apos;012U0000000LgOv&apos;, RecordTypeId  = &apos;012U0000000Ly0Z&apos;, RecordTypeId  = &apos;012U0000000LgOs&apos;, RecordTypeId  = &apos;012U0000000Mm55&apos;, RecordTypeId  = &apos;012U0000000Mm56&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Forecast Category - Best Case</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>equals</operation>
            <value>Best Case</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Forecast Category - Committed</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>equals</operation>
            <value>Commit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Forecast Category - Omitted</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>equals</operation>
            <value>Omitted,Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Forecast Category - Pipeline</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>equals</operation>
            <value>Pipeline</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JP Opportunity%3A Update Initial Close Date</fullName>
        <actions>
            <name>JP_Update_Field_Initial_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(     OR(         $RecordType.DeveloperName = &apos;Japan_EbD_Opportunity&apos;,         $RecordType.DeveloperName = &apos;Japan_RMS_Opportunity&apos;,         $RecordType.DeveloperName = &apos;Japan_SD_Opportunity&apos;,         $RecordType.DeveloperName = &apos;Japan_VT_Opportunity&apos;     ),     ISBLANK(JP_Initial_Close_Date__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JP Opportunity%3A Update RecordType Name</fullName>
        <actions>
            <name>JP_Update_Field_RecordType_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()  ||  ISCHANGED( RecordTypeId  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Converted Opportunity Follow up</fullName>
        <active>true</active>
        <description>Follow up on Lead Converted Opportunity if there is no action within last 7 days</description>
        <formula>AND ( Owner.Profile.Name = &apos;Canada - All&apos;,   ISPICKVAL(Owner.Region__c, &apos;CA&apos;),  Created_by_Lead_Conversion__c = True)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Converted_Opportunity_Notification_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Converted Opportunity US RMS</fullName>
        <actions>
            <name>US_RMS_Notification_to_owner_and_manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Follow up on Lead Converted Opportunity if there is no action in 2 working days</description>
        <formula>AND ( Owner.Profile.Name = &apos;US - RMS&apos;,   ISPICKVAL(Owner.Region__c, &apos;US&apos;),  Created_by_Lead_Conversion__c = True, TODAY() = Two_Working_Days__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NCPCS Opp Type Populate from Lead</fullName>
        <actions>
            <name>Update_Opportunity_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND((OwnerId &lt;&gt; LastModifiedById),  AND(Created_by_Lead_Conversion__c = TRUE),  AND(CreatedBy.Profile.Name = &quot;MITG Lead Qualification&quot;), AND(RecordType.Name  = &quot;US - Med Supplies - Opportunity&quot;), AND(Owner.Profile.Name &lt;&gt; &quot;US - MITG Lead Qualification&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NCPCS Probability Field CW and CR</fullName>
        <actions>
            <name>NCPCS_Set_Probability_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 And 2 AND 3) Or (1 AND 2 AND 5) Or (1 AND 4 AND 5) Or (1 AND 4 AND 7) Or (1 AND 6 AND 7) OR (1 AND 2 AND 5) OR (1 AND 4 AND 7)</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - Med Supplies</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Write Down</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed (Resolved)</value>
        </criteriaItems>
        <description>Workflow will update probability field (confidence_level__c) to 100% once it meets the rule criteria.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Assignee</fullName>
        <actions>
            <name>LQS_Opportunity_transfer_email_update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Custom notification for when the Lead Qualification Specialist transfers an opportunity to field sales</description>
        <formula>AND((OwnerId &lt;&gt; LastModifiedById), AND(Created_by_Lead_Conversion__c = TRUE), AND(CreatedBy.Profile.Name = &quot;MITG Lead Qualification&quot;), AND(DATEVALUE(CreatedDate) = TODAY()), AND(Owner.Profile.Name &lt;&gt; &quot;MITG Lead Qualification&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify five days before Contract date</fullName>
        <actions>
            <name>EMEA_Contract_Date_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>5 days before Contract Date Opportunity owner will receive notification email</description>
        <formula>AND( OR( $Profile.Name = &quot;EMEA EM&quot;,$Profile.Name = &quot;EM - All&quot;),Contract_Date__c = TODAY() + 5)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify five days before Tender exp date</fullName>
        <actions>
            <name>EMEA_Tender_Expiration_Date_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>5 days before Tender Expiration Date Opportunity owner will receive notification email</description>
        <formula>AND( OR( $Profile.Name = &quot;EMEA EM&quot;,$Profile.Name = &quot;EM - All&quot;),Tender_Expiration_Date__c  =  TODAY() + 5)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Op Closed Date</fullName>
        <actions>
            <name>Op_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Closed Date to the date the Opportunity was marked Closed (Won, Lost, or Resolved)?</description>
        <formula>( 	ISPICKVAL(PRIORVALUE(StageName), &quot;Identify&quot; ) ||  	ISPICKVAL(PRIORVALUE(StageName), &quot;Develop&quot; ) ||  	ISPICKVAL(PRIORVALUE(StageName), &quot;Evaluate&quot; ) ||  	ISPICKVAL(PRIORVALUE(StageName), &quot;Propose&quot; ) ||  	ISPICKVAL(PRIORVALUE(StageName), &quot;Negotiate&quot; )||	ISNEW() )   &amp;&amp; ( 	ISPICKVAL(StageName, &quot;Closed (Resolved)&quot; ) ||  	ISPICKVAL(StageName, &quot;Closed Lost&quot; )||  	ISPICKVAL(StageName, &quot;Closed Won&quot; ) )|| ISPICKVAL(StageName, &quot;Closed Saved&quot;) || ISPICKVAL(StageName, &quot;Closed Missed&quot; )   &amp;&amp; ( 	$RecordType.DeveloperName &lt;&gt; &apos;Japan_EbD_Opportunity&apos; &amp;&amp; 	$RecordType.DeveloperName &lt;&gt; &apos;Japan_RMS_Opportunity&apos; &amp;&amp; 	$RecordType.DeveloperName &lt;&gt; &apos;Japan_SD_Opportunity&apos; &amp;&amp; 	$RecordType.DeveloperName &lt;&gt; &apos;Japan_VT_Opportunity&apos; &amp;&amp; 	$RecordType.DeveloperName &lt;&gt; &apos;Japan_Base_Opportunity&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Private Opportunity</fullName>
        <actions>
            <name>Opportunity_Private_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an Opportunity is created, sets the Private Opportunity checkbox to TRUE if the User&apos;s Profile is &apos;Private Covidien User&apos;. ProfileId is used in case the Profile Name is changed.</description>
        <formula>IF($User.ProfileId = &quot;00eU0000000ddrF&quot;, TRUE, FALSE)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Append Account Name</fullName>
        <actions>
            <name>Opportunity_Add_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to add account name: to the beginning of any opportunity name.</description>
        <formula>AND(  NOT(ISPICKVAL(Region__c  , &apos;US&apos;)),  RecordTypeId   &lt;&gt;   &quot;012U0000000LgOk&quot;, NOT(ISBLANK(  Name )))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Append Account Name - Edits</fullName>
        <actions>
            <name>Opportunity_Append_Name_with_Original</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow to add account name: to the beginning of any opportunity name.</description>
        <formula>NOT(ISBLANK(  Original_Opportunity_Name__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Create task when %22Next Step%22 fields are populated</fullName>
        <actions>
            <name>Opportunity_Set_Next_Step_Date_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Set_Sales_Rep_Next_Step_nul</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Next_Step</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Sales_Rep_Next_Step__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Next_Step_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When &quot;Sales Rep Next Step&quot; and &quot;Next Step Date&quot; fields are populated, create a task and set the values to null.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Follow Up Date Changed</fullName>
        <actions>
            <name>Reactivate_Opportunity</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Workflow to create task for opportunity owner when reactivate date is changed.</description>
        <formula>ISCHANGED(  Followup_Date__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Overdue Lead Convert Opportunity Email Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Created_by_Lead_Conversion__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>lessThan</operation>
            <value>USD 11</value>
        </criteriaItems>
        <description>Send notification email to opportunity owner and regional manager for opportunities created via Qualified Leads that have not received follow up in 2 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_to_Opp_Manager</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Two_Working_Days__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Overdue Lead Convert Opportunity Emailtest</fullName>
        <actions>
            <name>Test_opp_qualified_lead_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Overdue Lead Converted Opportunity 2 Business Days Notification</fullName>
        <active>false</active>
        <description>Follow up on Lead Converted Opportunity if there is no action in 2 working days</description>
        <formula>AND(Created_by_Lead_Conversion__c = True, ISPICKVAL(CurrencyIsoCode  , &apos;USD&apos;), Amount  &lt;  11.00,  CloseDate &lt;=  Two_Working_Days__c )  ||  AND(Created_by_Lead_Conversion__c = True, ISPICKVAL(CurrencyIsoCode  , &apos;USD&apos;), Amount   &gt;   11.00,  CloseDate &lt;=  Two_Working_Days__c )  ||  AND(Created_by_Lead_Conversion__c = True, ISPICKVAL(CurrencyIsoCode  , &apos;USD&apos;), Amount    &lt;    11.00,  CloseDate  &gt;=  Two_Working_Days__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Notification_Overdue_Lead_Converted_Opportunity</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Two_Working_Days__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate New Business on Convted Lead</fullName>
        <actions>
            <name>Update_Opportunity_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((OwnerId &lt;&gt; LastModifiedById),  AND(Created_by_Lead_Conversion__c = TRUE),  AND(CreatedBy.Profile.Name = &quot;MITG Lead Qualification&quot;), AND(OR(RecordType.Name  = &quot;US - Med Supplies - Opportunity&quot;,RecordType.Name = &quot;US - RMS - Opportunity&quot;,RecordType.Name = &quot;US – AST – Opportunity&quot;,RecordType.Name = &quot;US - SUS - Opportunity&quot;), AND(Owner.Profile.Name &lt;&gt; &quot;US - MITG Lead Qualification&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate OCR Email of opportunity</fullName>
        <actions>
            <name>Populate_OCR_Email_of_opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.No_of_OCR__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RM_Forecast_Status_Best_Case</fullName>
        <actions>
            <name>Set_RM_Forecast_to_Best_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(ISNEW(),ISCHANGED(ForecastCategoryName)),ISPICKVAL(ForecastCategoryName,&apos;Best Case&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM_Forecast_Status_Closed</fullName>
        <actions>
            <name>Set_RM_Forecast_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(ISNEW(),ISCHANGED(ForecastCategoryName)),ISPICKVAL(ForecastCategoryName,&apos;Closed&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM_Forecast_Status_Commit</fullName>
        <actions>
            <name>Set_RM_Forecast_to_Commit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(ISNEW(),ISCHANGED(ForecastCategoryName)),ISPICKVAL(ForecastCategoryName,&apos;Commit&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM_Forecast_Status_Omitted</fullName>
        <actions>
            <name>Set_RM_Forecast_to_Omitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(ISNEW(),ISCHANGED(ForecastCategoryName)),ISPICKVAL(ForecastCategoryName,&apos;Omitted&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM_Forecast_Status_Pipeline</fullName>
        <actions>
            <name>Set_RM_Forecast_to_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(ISNEW(),ISCHANGED(ForecastCategoryName)),ISPICKVAL(ForecastCategoryName,&apos;Pipeline&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RollBackOppCloseDate</fullName>
        <actions>
            <name>RollBackOppCloseDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ( 2 AND 3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.NeedRollBackCloseDate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.JP_RecordType_Name__c</field>
            <operation>notEqual</operation>
            <value>Japan_EbD_Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.JP_RecordType_Name__c</field>
            <operation>notEqual</operation>
            <value>Japan_RMS_Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.JP_RecordType_Name__c</field>
            <operation>notEqual</operation>
            <value>Japan_SD_Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.JP_RecordType_Name__c</field>
            <operation>notEqual</operation>
            <value>Japan_VT_Opportunity</value>
        </criteriaItems>
        <description>When opp stage is changed to closed won or closed resolved, SFDC set the current date as the closed date automatically. This work flow is roll it back to the user selected date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Navigator Prob Per update</fullName>
        <actions>
            <name>UpdateProbPercent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Customer - Conversion,Existing Customer - New Business,At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>contains</operation>
            <value>US - Sales Nav Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Capital_Disposable__c</field>
            <operation>equals</operation>
            <value>Disposable,Both</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Probability_Score__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Navigator Progress Monitor</fullName>
        <actions>
            <name>UpdateProgressMonitor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Customer - Conversion,Existing Customer - New Business,At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>contains</operation>
            <value>US - Sales Nav Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Capital_Disposable__c</field>
            <operation>equals</operation>
            <value>Disposable,Both</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SalesNav Probability</fullName>
        <actions>
            <name>Update_Prob_with_SalesNav_Prob</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Customer - Conversion,Existing Customer - New Business,At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>contains</operation>
            <value>US - Sales Nav Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Capital_Disposable__c</field>
            <operation>equals</operation>
            <value>Disposable,Both</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Probability_Score__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set %22Next Fiscal Year Impact%22 field</fullName>
        <actions>
            <name>Set_Next_Fiscal_Year_Impact_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW-EbD Tender Expiration Reminder</fullName>
        <active>true</active>
        <description>Tender Expiration for TW-EBD, notify Owner, Manager and EBD Head.</description>
        <formula>AND(ISPICKVAL( Owner.Business_Unit__c, &apos;EbD&apos;) ,  Reminder_Date__c  &gt;  TODAY()  ,  Account.BillingCountry = &apos;TW&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TW_EbD_Tender_Expiration_Email_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TW-Fail Catch Tender Expiration Reminder</fullName>
        <active>true</active>
        <description>Tender Expiration for TW, notify owner and sales ops.</description>
        <formula>AND( NOT(OR(ISPICKVAL( Owner.Business_Unit__c, &apos;STI&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;EMID&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;EBD&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;RMS&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;VT&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;MS&apos;))) ,  Reminder_Date__c  &gt;  TODAY()  ,  Account.BillingCountry = &apos;TW&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TW_Fail_Catch_Tender_Expiration_Email_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TW-RMS Tender Expiration Reminder</fullName>
        <active>true</active>
        <description>Tender Expiration for TW-RMS, notify Owner, Manager and RMS Head.</description>
        <formula>AND( ISPICKVAL( Owner.Business_Unit__c, &apos;RMS&apos;)  , Reminder_Date__c &gt; TODAY()  ,  Account.BillingCountry = &apos;TW&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TW_RMS_Tender_Expiration_Email_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TW-SD Tender Expiration Reminder</fullName>
        <active>true</active>
        <description>Tender Expiration for TW-STI &amp; EMID, notify Owner, Manager and SD Head.</description>
        <formula>AND( OR(ISPICKVAL( Owner.Business_Unit__c, &apos;STI&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;EMID&apos;)) ,  Reminder_Date__c  &gt;  TODAY()  ,  Account.BillingCountry = &apos;TW&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TW_SD_Tender_Expiration_Email_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TW-VT%26MS Tender Expiration Reminder</fullName>
        <active>true</active>
        <description>Tender Expiration for TW-VT &amp; MS, notify Owner, Manager and VT&amp;MS Head.</description>
        <formula>AND( OR(ISPICKVAL( Owner.Business_Unit__c, &apos;VT&apos;) , ISPICKVAL( Owner.Business_Unit__c, &apos;MS&apos;)) ,  Reminder_Date__c  &gt;  TODAY()  ,  Account.BillingCountry = &apos;TW&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TW_VT_MS_Tender_Expiration_Email_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Tender Expiration Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Reminder_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Tender_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Tender Expiration Reminder - Asia India</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Reminder_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Tender_Reminder_IN</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Reminder_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>The Reviewed check box should be unchecked on any modification on an opportunity stage for EMEA EM</fullName>
        <actions>
            <name>The_Reviewed_check_box_should_be_uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The Reviewed check box should be unchecked on any modification on an opportunity stage for EMEA EM</description>
        <formula>ISCHANGED(StageName)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>US MITG Forecast 0%25</fullName>
        <actions>
            <name>Forecast_update_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3 AND 4) OR (1 AND 5 AND 6) OR (2 AND 3 AND 4) OR (2 AND 5 AND 6) OR (7 AND 3 AND 4) OR (7 AND 5 AND 6) OR (8 AND 3 AND 4) OR (8 AND 5 AND 6) OR (9 AND 3 AND 4) OR (9 AND 5 AND 6)</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - Med Supplies</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>APTTUS - RMS US Sales (RS+PM)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Missed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Saved</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>APTTUS - SSG</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - AST</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - RCS</value>
        </criteriaItems>
        <description>US - MITG workflow rule that changes the forecast field to 0% when a user closes out an opportunity under New Business - Closed Missed or At Risk - Closed Saved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US MITG Forecast 100%25</fullName>
        <actions>
            <name>Forecast_update_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3 AND 4) OR (1 AND 5 AND 6) OR (2 AND 3 AND 4) OR (2 AND 5 AND 6) OR (7 AND 3 AND 4) OR (7 AND 5 AND 6) OR (8 AND 3 AND 4) OR (8 AND 5 AND 6) OR (9 AND 3 AND 4) OR (9 AND 5 AND 6)</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - Med Supplies</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>APTTUS - RMS US Sales (RS+PM)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>APTTUS - SSG</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - RCS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - AST</value>
        </criteriaItems>
        <description>US - MITG workflow rule that changes the forecast field to 100% when a user closes out an opportunity under New Business - Closed Won or At Risk - Closed Lost.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG - Reprocessing Opportunity True</fullName>
        <actions>
            <name>Reprocessing_Opportunity_is_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Initiative__c</field>
            <operation>equals</operation>
            <value>Reprocessing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Include in Call Check</fullName>
        <actions>
            <name>Include_in_Call_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3) OR (1 AND 4) OR (2 AND 3) OR (2 AND 4) OR (3 AND 5) OR (4 AND 5) OR (3 AND 6) OR (4 AND 6) OR (3 AND 7) OR (4 AND 7)</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - Med Supplies</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>APTTUS - RMS US Sales (RS+PM)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Commit (90% Probability)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Closed (100% Probability)</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>APTTUS - SSG</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - RCS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - AST</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Include in Call Uncheck</fullName>
        <actions>
            <name>Uncheck_Include_in_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3) OR (2 AND 3) OR (3 AND 4) OR (3 AND 5) OR (3 AND 6)</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - Med Supplies</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>APTTUS - RMS US Sales (RS+PM)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identify,Closed Saved,Closed Missed</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - AST</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>APTTUS - SSG</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - RCS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Include in Call uncheck reuncheck</fullName>
        <actions>
            <name>Uncheck_Include_in_Call_Again</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3 AND 4) OR (2 AND 3 AND 4) OR (3 AND 4 AND 5) OR (3 AND 4 AND 6) OR (3 AND 4 AND 7)</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - Med Supplies</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>APTTUS - RMS US Sales (RS+PM)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identify,Closed Saved,Closed Missed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ZVP_Forecast__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - AST</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>APTTUS - SSG</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>US - RCS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Reprocessing Opportunity False</fullName>
        <actions>
            <name>Reprocessing_Opportunity_is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Initiative__c</field>
            <operation>notEqual</operation>
            <value>Reprocessing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Update probability field to 0</fullName>
        <actions>
            <name>Update_probability_field_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Omit (0% Probability)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Update probability field to 100</fullName>
        <actions>
            <name>Update_probability_field_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Closed (100% Probability)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Update probability field to 50</fullName>
        <actions>
            <name>Update_probability_field_to_50</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Pipeline (50% Probability)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Update probability field to 75</fullName>
        <actions>
            <name>Update_probability_field_to_75</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Best Case (75% Probability)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMITG Update probability to 90</fullName>
        <actions>
            <name>Update_probability_to_90</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Commit (90% Probability)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>USMS NBF reminder after 3 days</fullName>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 5 AND 6) OR (3 AND 4 AND 5 AND 6)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>At Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.of_New_Business_Forms__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>US - Med Supplies - Opportunity</value>
        </criteriaItems>
        <description>US Med Supplies workflow on the Opportunity object. This rule will evaluate if the opportunity was won or lost and will send out an email if a New Business Form has not been created within 3 days of closing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>USMS_NB_CW_and_AR_CL_NBF_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Closed_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update OCR Email of opportunity</fullName>
        <actions>
            <name>Update_OCR_Email_of_opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.No_of_OCR__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Original Opportunity Name</fullName>
        <actions>
            <name>Update_Opportunity_Original_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update RMS Forecast Field</fullName>
        <actions>
            <name>Update_RMS_Include_in_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Commit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RM_Forecast_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.JP_RecordType_Name__c</field>
            <operation>equals</operation>
            <value>US_Opportunity_RMS,US_RMS_PM_Opportunity,US_RMS_RS_Opportunity</value>
        </criteriaItems>
        <description>Updating the Include in Call checkbox when Forecast Status is Commit or Closed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>task for ANZ Smart Goal opportunities</fullName>
        <actions>
            <name>smartgoal_opportunity_SmartGoalcheckbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>smartgoal_opportunity_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Close</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Develop</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Evaluate</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Identify</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Negotiate</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Propose</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>RecordType.Id=&apos;012U0000000MERS&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call Opportunity</fullName>
        <actions>
            <name>zaapit__Call_oppts</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Call Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email Opportunity</fullName>
        <actions>
            <name>zaapit__Email_oppts</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Email Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter Opportunity</fullName>
        <actions>
            <name>zaapit__Send_Letter_oppts</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Letter Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Quote Opportunity</fullName>
        <actions>
            <name>zaapit__Send_Quote_oppts</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Quote Opportunity - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Close</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Close</subject>
    </tasks>
    <tasks>
        <fullName>Develop</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Develop</subject>
    </tasks>
    <tasks>
        <fullName>Evaluate</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Evaluate</subject>
    </tasks>
    <tasks>
        <fullName>Identify</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Identify</subject>
    </tasks>
    <tasks>
        <fullName>Negotiate</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Negotiate</subject>
    </tasks>
    <tasks>
        <fullName>Next_Step</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.Next_Step_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step</subject>
    </tasks>
    <tasks>
        <fullName>Propose</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Propose</subject>
    </tasks>
    <tasks>
        <fullName>Reactivate_Opportunity</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.Followup_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Opportunity Follow up Date</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Call_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Letter_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Quote_oppts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Quote</subject>
    </tasks>
</Workflow>
