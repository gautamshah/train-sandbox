<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CN_Demo_Product_Expiration_Reminder_Email</fullName>
        <description>CN Demo Product Expiration Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Demo_Product_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_Demo_Product_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>CN_Send_email_notification_to_requested_by</fullName>
        <description>CN Send email notification to requested by</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Approver_s_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Demo_Product_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_New_Product_Movement_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>EMEA_EM_Demo_Loan_Approval</fullName>
        <description>EMEA EM Demo/Loan Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMEA_EM_Templates/Demo_Loan_Approval</template>
    </alerts>
    <alerts>
        <fullName>EMEA_EM_Demo_Loan_Final_Approval</fullName>
        <description>EMEA EM Demo/Loan Final Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMEA_EM_Templates/Demo_Loan_Final_Approval</template>
    </alerts>
    <alerts>
        <fullName>EMEA_EM_Demo_Loan_Rejected</fullName>
        <description>EMEA EM Demo/Loan Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMEA_EM_Templates/Demo_Loan_Rejected</template>
    </alerts>
    <alerts>
        <fullName>HK_movement_request_submit_to_CS</fullName>
        <ccEmails>ivy.chan@covidien.com</ccEmails>
        <ccEmails>HK-CS@covidien.com</ccEmails>
        <description>HK movement request submit to CS</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/HK_Movement_Submit_to_CS_Team</template>
    </alerts>
    <alerts>
        <fullName>KR_Approval_Email_Notification_for_Demo_Product_Movement</fullName>
        <ccEmails>tapy79@naver.com</ccEmails>
        <ccEmails>medtronic.demo01@gmail.com</ccEmails>
        <description>KR Approval Email Notification for Demo Product Movement</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_s_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>iris.joo@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_New_Product_Movement_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>KR_Demo_Product_Expiration_Reminder_Email</fullName>
        <description>KR Demo Product Expiration Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_Demo_Product_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>KR_Movement_Request_Approval_Reminder</fullName>
        <description>KR Movement Request Approval Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_s_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_Product_Movement_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>KR_Product_Movement_Delivery_Date_Reminder</fullName>
        <description>KR Product Movement Delivery Date Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>iris.joo@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_Product_Movement_Delivery_Date_Reminder</template>
    </alerts>
    <alerts>
        <fullName>KR_Send_Delivery_Collection_Demo_Equipment_Form</fullName>
        <ccEmails>medtronic.demo01@gmail.com</ccEmails>
        <ccEmails>tapy79@naver.com</ccEmails>
        <description>KR Send Delivery/Collection Demo Equipment Form</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_s_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>iris.joo@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>teng.hoon.tan@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_Demo_Product_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Send_Delivery_Collection_Form_to_EBD_Product_Mgr</fullName>
        <description>MY Send Delivery/Collection Form to EBD Product Mgr</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>nicholas.wong@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>si.hao.tan@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Send_Delivery_Collection_Form_to_EBD_STI_Product_Mgr</fullName>
        <description>MY Send Delivery/Collection Form to EBD/STI Product Mgr</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>brandon.lim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Send_Delivery_Collection_Form_to_EMID_Product_Mgr</fullName>
        <description>MY Send Delivery/Collection Form to EMID Product Mgr</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>siew.kee.tan@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Send_Delivery_Collection_Form_to_STI_Product_Mgr</fullName>
        <description>MY Send Delivery/Collection Form to STI Product Mgr</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>brandon.lim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nicholas.wong@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Send_Delivery_Collection_Form_to_Self</fullName>
        <description>MY Send Delivery/Collection Form to Self</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Send_email_notification_to_requested_by</fullName>
        <description>MY Send email notification to requested by</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_New_Product_Movement_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>SG_Demo_Product_Expiration_Reminder_Email</fullName>
        <description>SG Demo Product Expiration Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_Demo_Product_Expiration_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SG_Send_Delivery_Collection_Form_to_Sales_Admin</fullName>
        <ccEmails>Singapore-salesadmin@covidien.com</ccEmails>
        <description>SG Send Delivery/Collection Form to Sales Admin</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_Delivery_Collection_Form</template>
    </alerts>
    <alerts>
        <fullName>SG_Send_email_notification_to_requested_by</fullName>
        <description>SG Send email notification to requested by</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_s_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_New_Product_Movement_Approval_Status</template>
    </alerts>
    <fieldUpdates>
        <fullName>Default_Contact_Phone_Number</fullName>
        <field>Contact_Phone__c</field>
        <formula>Contact_Name__r.MobilePhone</formula>
        <name>Default Contact Phone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HK_Movement_Submit_Count</fullName>
        <field>Submit_Count__c</field>
        <formula>Submit_Count__c + 1</formula>
        <name>HK Movement Submit Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HK_Movement_Submit_Reset_Checkbox</fullName>
        <field>Submit_to_CS_Team__c</field>
        <literalValue>0</literalValue>
        <name>HK Movement Submit Reset Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Approval Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_s_Manager</fullName>
        <description>Update approver&apos;s manager email address</description>
        <field>Approver_s_Manager__c</field>
        <formula>CreatedBy.Manager.User_Manager_Email__c</formula>
        <name>Update Approver&apos;s Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Demo_Product_CurrentLocation</fullName>
        <field>Current_Location__c</field>
        <literalValue>Hospital</literalValue>
        <name>Update_Demo_Product_CurrentLocation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Demo_Product_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Demo_Product_Owner_Email</fullName>
        <field>Demo_Product_Owner_Email__c</field>
        <formula>Demo_Product_Name__r.Owner:User.Email</formula>
        <name>Update Demo Product Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Movement_Request_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Movement Request Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Code_to_Product_SKU_Name</fullName>
        <field>Product_Code__c</field>
        <formula>Demo_Product_Name__r.Asset_Name__r.Name</formula>
        <name>Update Product Code to Product SKU Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CN Copy Approver%27s Manager Email in Movement Request</fullName>
        <actions>
            <name>Update_Approver_s_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Movement__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <description>Copy Approver&apos;s Manager Email in Movement Request to send email notification after the request has been approved.
CN Country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CN Demo Product Expiration Reminder Email</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Movement__c.Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <description>Send email notification to Demo Product Owner 83 days after start date.
CN country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CN_Demo_Product_Expiration_Reminder_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Movement__c.Start_Date__c</offsetFromField>
            <timeLength>83</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Copy Demo Product Owner Email</fullName>
        <actions>
            <name>Update_Demo_Product_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy Demo Product Owner email address if Movement creator is not same as Demo Product owner</description>
        <formula>Demo_Product_Name__r.OwnerId &lt;&gt;  CreatedById</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Contact Phone Number</fullName>
        <actions>
            <name>Default_Contact_Phone_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND     (         $Profile.Name = &quot;Asia - HK&quot;,         ISBLANK( Contact_Phone__c )     )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HK Movement Submit to CS Team</fullName>
        <actions>
            <name>HK_movement_request_submit_to_CS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>HK_Movement_Submit_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HK_Movement_Submit_Reset_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Asia - HK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Submit_to_CS_Team__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>KR Demo Product - Update Current Location</fullName>
        <actions>
            <name>Update_Demo_Product_CurrentLocation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the current location of the demo product after a delivery date is updated in the movement request.</description>
        <formula>AND(   NOT(ISBLANK( Delivery_Date__c )),   NOT(ISBLANK( Collection_Date__c )),   ISPICKVAL(Demo_Product_Name__r.Country__c,&apos;KR&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>KR Demo Product Expiration Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Hospital_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send email notification to KR Sales Rep 7 days before end date and on end date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>KR_Demo_Product_Expiration_Reminder_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Movement__c.End_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>KR_Demo_Product_Expiration_Reminder_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Movement__c.End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>KR Movement Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Submitted for Approval</value>
        </criteriaItems>
        <description>Send email notification to KR Sales Manager if request is not approved after 3 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>KR_Movement_Request_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>KR Product Movement Delivery Date Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Delivery_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Movement__c.Collection_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send email notification to KR Sales Rep and Sales Admin if delivery date is empty 3 days after collection date is keyed in.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>KR_Product_Movement_Delivery_Date_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SG Copy Approver%27s Manager Email in Movement Request</fullName>
        <actions>
            <name>Update_Approver_s_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Movement__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>SG</value>
        </criteriaItems>
        <description>Copy Approver&apos;s Manager Email in Movement Request to send email notification after the request has been approved.
SG Country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SG Demo Product Expiration Reminder Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Movement__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>SG</value>
        </criteriaItems>
        <description>Send email notification to Demo Product Owner 3 days before end date.
SG country</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SG_Demo_Product_Expiration_Reminder_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Movement__c.End_Date__c</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Product Code Field</fullName>
        <actions>
            <name>Update_Product_Code_to_Product_SKU_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Movement__c.Product_SKU__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To update Product Code field to be searchable by user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
