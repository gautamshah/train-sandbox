<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>DIS-Create Task when Account Affiliation has been modified %28KR%29</fullName>
        <actions>
            <name>Account_Affiliation_has_been_updated_Please_update_Distributor_Territory</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AffiliationMember__r.BillingCountry = &quot;KR&quot; &amp;&amp; (RecordType.Name = &quot;Sub Dealer&quot; || RecordType.Name = &quot;Sub Entity&quot;)&amp;&amp; ISCHANGED( Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Account_Affiliation_has_been_updated_Please_update_Distributor_Territory</fullName>
        <assignedToType>owner</assignedToType>
        <description>Account Affiliation has been updated, please update the Distributor Territory.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Account Affiliation has been updated: Please update Distributor Territory</subject>
    </tasks>
</Workflow>
