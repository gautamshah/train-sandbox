<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Back_up_responsible_party_email</fullName>
        <field>Back_up_responsible_party_email_address__c</field>
        <formula>Back_up_responsible_party__r.Email</formula>
        <name>Back up responsible party email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capnography_Notes_Flag</fullName>
        <description>This sets Capnography Notes Flag to TRUE if Capnography Notes contains some data</description>
        <field>Capnography_Notes_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Capnography Notes Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capnography_Notes_Flag_Reset</fullName>
        <description>This sets Capnography Notes Flag to FALSE if Capnography Notes is blank</description>
        <field>Capnography_Notes_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Capnography Notes Flag Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Concious_Monitoring_Notes_Flag</fullName>
        <description>This sets Concious Monitoring Notes Flag to TRUE if Concious Monitoring Notes contains some data</description>
        <field>Concious_Monitoring_Notes_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Concious Monitoring Notes Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Concious_Monitoring_Notes_Flag_Reset</fullName>
        <description>This sets Concious Monitoring Notes Flag to FALSE if it is blank</description>
        <field>Concious_Monitoring_Notes_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Concious Monitoring Notes Flag Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pulse_Oximetry_Notes_Flag</fullName>
        <description>This sets Pulse Oximetry Notes Flag to TRUE if Pulse Oximetry Notes contains some data</description>
        <field>Pulse_Oximetry_Notes_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Pulse Oximetry Notes Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pulse_Oximetry_Notes_Flag_Reset</fullName>
        <description>This sets Pulse Oximetry Notes Flag to FALSE if it is blank</description>
        <field>Pulse_Oximetry_Notes_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Pulse Oximetry Notes Flag Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Responsible_Party_Email_Addrss</fullName>
        <field>Responsible_party_email_address__c</field>
        <formula>Sonicision_Care_Plan_Reponsible_Party__r.Email</formula>
        <name>Responsible Party Email Addrss</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AEP-Sonicision Care Service-Contacts</fullName>
        <actions>
            <name>Back_up_responsible_party_email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Responsible_Party_Email_Addrss</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Extended_Profile__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>EU - Sonicision Care Service</value>
        </criteriaItems>
        <description>Pull in the additional contact information</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capnography Notes Blank</fullName>
        <actions>
            <name>Capnography_Notes_Flag_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks that field is blank and updates a field</description>
        <formula>ISBLANK(Notes2__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capnography Notes has value</fullName>
        <actions>
            <name>Capnography_Notes_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks that field contains some value and updates a field</description>
        <formula>IF((LEN(Notes2__c ) &gt; 0),TRUE,FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Concious Monitoring Notes Blank</fullName>
        <actions>
            <name>Concious_Monitoring_Notes_Flag_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks that Concious Monitoring Notes field is blank</description>
        <formula>ISBLANK(Notes1__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Concious Monitoring Notes has value</fullName>
        <actions>
            <name>Concious_Monitoring_Notes_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks that field contains some value and updates a field</description>
        <formula>IF( LEN( Notes1__c ) &gt; 0, TRUE, FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pulse Oximetry Notes Blank</fullName>
        <actions>
            <name>Pulse_Oximetry_Notes_Flag_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks that field is blank and updates a field</description>
        <formula>ISBLANK(Notes__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pulse Oximetry Notes has value</fullName>
        <actions>
            <name>Pulse_Oximetry_Notes_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks that field contains some value and updates a field</description>
        <formula>IF((LEN(Notes__c ) &gt; 0),TRUE,FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
