<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Old_of_Procedures_Month_EMID</fullName>
        <field>Previous_of_Procedures_Month_EMID__c</field>
        <formula>PRIORVALUE( Number_of_Procedures_Month_EMID__c )</formula>
        <name>Update Old # of Procedures/Month (EMID)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Old_of_Procedures_Month_EbD</fullName>
        <field>Previous_of_Procedures_Month_EbD__c</field>
        <formula>PRIORVALUE(Procedures_Month_EbD__c )</formula>
        <name>Update Old # of Procedures/Month (EbD)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PPC_Speciality</fullName>
        <description>used by Korea</description>
        <field>Specialty_txt__c</field>
        <formula>TEXT( Physician__r.Specialty__c )</formula>
        <name>Update PPC Speciality</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_of_Procedures_Month_S</fullName>
        <field>Previous_of_Procedures_Month_STI__c</field>
        <formula>PRIORVALUE( Number_of_Procedures_Month_STI__c )</formula>
        <name>Update Previous # of Procedures/Month (S</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Save Previous %23 of Procedures%2FMonth %28EMID%29</fullName>
        <actions>
            <name>Update_Old_of_Procedures_Month_EMID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(IsNew()),( PRIORVALUE(  Number_of_Procedures_Month_EMID__c ) &lt;&gt; Number_of_Procedures_Month_EMID__c ),CONTAINS( $Profile.Name , &apos;EMEA EM&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Save Previous %23 of Procedures%2FMonth %28EbD%29</fullName>
        <actions>
            <name>Update_Old_of_Procedures_Month_EbD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(IsNew()),( PRIORVALUE( Procedures_Month_EbD__c ) &lt;&gt; Procedures_Month_EbD__c ),  CONTAINS( $Profile.Name , &apos;EMEA EM&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Save Previous %23 of Procedures%2FMonth %28STI%29</fullName>
        <actions>
            <name>Update_Previous_of_Procedures_Month_S</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(IsNew()),( PRIORVALUE(  Number_of_Procedures_Month_STI__c ) &lt;&gt;  Number_of_Procedures_Month_STI__c ),CONTAINS( $Profile.Name , &apos;EMEA EM&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PPC</fullName>
        <actions>
            <name>Update_PPC_Speciality</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>update specialty of the PPC after PPC is created. used by Korea &amp; HK</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
