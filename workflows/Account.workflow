<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EMS_Account_Alert_East</fullName>
        <description>EMS Account Alert East</description>
        <protected>false</protected>
        <recipients>
            <recipient>holly.stewart@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michael.malloy@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/EMS_Account_Alert</template>
    </alerts>
    <alerts>
        <fullName>EMS_Account_Alert_South</fullName>
        <description>EMS Account Alert South</description>
        <protected>false</protected>
        <recipients>
            <recipient>holly.stewart@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>terry.m.reed@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/EMS_Account_Alert</template>
    </alerts>
    <alerts>
        <fullName>EMS_Account_Alert_West</fullName>
        <ccEmails>chaucey.edwards@covidien.com</ccEmails>
        <description>EMS Account Alert - West</description>
        <protected>false</protected>
        <recipients>
            <recipient>holly.stewart@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/EMS_Account_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Copy_City_to_Billing_City</fullName>
        <field>BillingCity</field>
        <formula>City__c</formula>
        <name>Copy City to Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Country_to_Billing_Country</fullName>
        <field>BillingCountry</field>
        <formula>TEXT(Country__c)</formula>
        <name>Copy Country to Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_State_to_Billing_State</fullName>
        <field>BillingState</field>
        <formula>TEXT(State_Province__c)</formula>
        <name>Copy State to Billing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Street_to_Billing_Street</fullName>
        <field>BillingStreet</field>
        <formula>Street__c</formula>
        <name>Copy Street to Billing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Zip_to_Billing_Zip</fullName>
        <field>BillingPostalCode</field>
        <formula>Zip_Postal_Code__c</formula>
        <name>Copy Zip to Billing Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Account_Type_Update</fullName>
        <description>Update Account Type to EMS when created by = Jacob Castiel</description>
        <field>AccountType__c</field>
        <literalValue>EMS</literalValue>
        <name>EMS Account Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Status_to_Active</fullName>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <name>Update Account Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EMS_Account_Code_To_AccountEXID</fullName>
        <field>Account_External_ID__c</field>
        <formula>&quot;TEMP-EMS-&quot;  &amp;   CreatedBy.Country   &amp; &quot;-&quot; &amp; Account_Auto_Number__c</formula>
        <name>Update EMS Account Code To AccountEXID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manufacturer_Account_Name</fullName>
        <description>Update Account Name for Manufacturers to read Account Name (Country)</description>
        <field>Name</field>
        <formula>Name&amp; &quot; &quot; &amp;&quot;(&quot;&amp;BillingCountry&amp;&quot;)&quot;</formula>
        <name>Update Manufacturer Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Parent_Account_Number</fullName>
        <description>Update parent account number with account number from parent account record.</description>
        <field>Parent_Account_Number__c</field>
        <formula>Parent.CovidienAccountNumber__c</formula>
        <name>Update Parent Account Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_US_COV_Healthcare</fullName>
        <description>Update record Type to US-COV Healthcare Facility</description>
        <field>RecordTypeId</field>
        <lookupValue>US_COV_Healthcare_Facility</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to US-COV Healthcare</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Temp_Account_Code_To_AccountEXID</fullName>
        <field>Account_External_ID__c</field>
        <formula>&quot;TEMP&quot;  &amp;   CreatedBy.Country   &amp; &quot;-&quot; &amp; Account_Auto_Number__c</formula>
        <name>Update Temp Account Code To AccountEXID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Temp_Account_Code_To_AccountSAPID</fullName>
        <field>Account_SAP_ID__c</field>
        <formula>&quot;TEMP&quot; &amp; CreatedBy.Country &amp; &quot;-&quot; &amp; Account_Auto_Number__c</formula>
        <name>Update Temp Account Code To AccountSAPID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_External_Id</fullName>
        <field>Account_External_ID__c</field>
        <formula>Duplicate_external_Id__c</formula>
        <name>Update the External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_the_source</fullName>
        <field>Source__c</field>
        <formula>&quot;US_PPA&quot;</formula>
        <name>update the source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account - Manufacturer Update Name</fullName>
        <actions>
            <name>Update_Manufacturer_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account - Manufacturer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Source__c</field>
            <operation>notContain</operation>
            <value>EU,EMEA</value>
        </criteriaItems>
        <description>If the account is a manufacturer, update the name to be Account Name (Country)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account - Update Parent Account %23</fullName>
        <actions>
            <name>Update_Parent_Account_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to update parent account # when a parent account is added.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Active User Submitted accounts to Healthcare</fullName>
        <actions>
            <name>Update_Record_Type_to_US_COV_Healthcare</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>User Submitted Healthcare Facility</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>User Submitted Healthcare facilities which have been verified and set to active,change their record type to US-COV Healthcare Facility</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Address Info to Billing Address</fullName>
        <actions>
            <name>Copy_City_to_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Country_to_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_State_to_Billing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Street_to_Billing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Zip_to_Billing_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ems In Process Account</value>
        </criteriaItems>
        <description>Street field needs to populate Billing street,
City field needs to populate Billing City,State/Province field needs to populate Billing State/Province

Values enter in Zip/Postal Code field needs to populate Billing Zip/Postal Code</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Update Temp Account Code To Account External ID</fullName>
        <actions>
            <name>Update_Temp_Account_Code_To_AccountEXID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_External_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sub-Dealer,Sub-Entity,ASIA-Distributor,ASIA-Healthcare Facility</value>
        </criteriaItems>
        <description>Update the Account External ID with the Temp code</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Update Temp Account Code To Account SAP ID</fullName>
        <actions>
            <name>Update_Temp_Account_Code_To_AccountSAPID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_SAP_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sub-Dealer,Sub-Entity,ASIA-Distributor,ASIA-Healthcare Facility</value>
        </criteriaItems>
        <description>Update the Account SAP ID with the Temp code</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS Account Alert East</fullName>
        <actions>
            <name>EMS_Account_Alert_East</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>CT,DE,KY,ME,MD,MA,MI,NH,NJ,NY,NC,OH,PA,RI,SC,TN,VT,VA,WVA,DC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.AccountType__c</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>Jacob Castiel</value>
        </criteriaItems>
        <description>Email to notify that new EMS account created in Territory EMS10, EMS Sales Manager EAST</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS Account Alert South</fullName>
        <actions>
            <name>EMS_Account_Alert_South</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>AL,AR,FL,GA,LA,MS,OK,TX</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.AccountType__c</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>Jacob Castiel</value>
        </criteriaItems>
        <description>Email to notify that new EMS account created in Territory EMS20, EMS Sales Manager South</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS Account Alert West</fullName>
        <actions>
            <name>EMS_Account_Alert_West</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>AK,AZ,CA,CO,HI,ID,IL,IN,IA,KS,MN,MO,MT,NE,NV,NM,ND,OR,SD,UT,WA,WI,WY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.AccountType__c</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>Jacob Castiel</value>
        </criteriaItems>
        <description>Email to notify that new EMS account created in EMS30 for EMS Sales Manager - West</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS Account Temp Account Code To Account External ID</fullName>
        <actions>
            <name>Update_EMS_Account_Code_To_AccountEXID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_External_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ems In Process Account</value>
        </criteriaItems>
        <description>Update the EMS In process Account External ID with the Temp code</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS Account Type Update</fullName>
        <actions>
            <name>EMS_Account_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>Jacob Castiel</value>
        </criteriaItems>
        <description>Rule to Update all accounts created by Jacob Castiel to EMS account Type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>External Id and Source field automation</fullName>
        <actions>
            <name>Update_the_External_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_the_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>US-Hospital Community</value>
        </criteriaItems>
        <description>We are automating it for the purpose of community accounts only</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set TEMP Account as Active</fullName>
        <actions>
            <name>Update_Account_Status_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When distributor user create TEMP account, set the account to active by default.</description>
        <formula>NOT(ISPICKVAL( $User.UserType , &quot;Standard&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call  - Account %28Exmaple by ZaapIT%29</fullName>
        <active>true</active>
        <description>Call Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call %28Exmaple by ZaapIT%29</fullName>
        <active>false</active>
        <description>Call Account Example by ZaapIT (don&apos;t activated - an example)</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email - Account %28Exmaple by ZaapIT%29</fullName>
        <actions>
            <name>zaapit__Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Email Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter  - Account %28Exmaple by ZaapIT%29</fullName>
        <actions>
            <name>zaapit__SendLetterAccount</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Letter Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Quote - Account %28Exmaple by ZaapIT%29</fullName>
        <actions>
            <name>zaapit__SendQuoteAccount</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send Quote Account - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>zaapit__Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__SendLetterAccount</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__SendQuoteAccount</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Quote</subject>
    </tasks>
</Workflow>
