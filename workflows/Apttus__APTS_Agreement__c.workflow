<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Activation_Cash_Quote_with_Vital_Sync</fullName>
        <description>Activation of Cash Quote with Vital Sync products</description>
        <protected>false</protected>
        <recipients>
            <recipient>andrea.peterson@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>carl.u.buice@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>david.teixeira@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Activation_Notification_Email_Outright_Quote_VS</template>
    </alerts>
    <alerts>
        <fullName>Activation_of_COOP_with_Vital_Sync_products</fullName>
        <description>Activation of COOP with Vital Sync products</description>
        <protected>false</protected>
        <recipients>
            <recipient>andrea.peterson@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>carl.u.buice@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>david.teixeira@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Activation_Notification_Email_COOP_VS</template>
    </alerts>
    <alerts>
        <fullName>Activation_of_Vital_Sync_Agreement</fullName>
        <ccEmails>rshiandm-1@covidien.com</ccEmails>
        <description>Activation of Vital Sync Agreement</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Vital_Sync_Agreement_Notification</template>
    </alerts>
    <alerts>
        <fullName>Adding_Facilities_to_PM_Service_Contract</fullName>
        <description>Adding Facilities to PM Service Contract</description>
        <protected>false</protected>
        <recipients>
            <recipient>pmservice@covidien.com.queue</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PM_Service_Queue_Amendment_Email</template>
    </alerts>
    <alerts>
        <fullName>Airway_Products_Incentive_Program_Notification</fullName>
        <description>Airway Products Incentive Program Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>endcustomer.rebates@covidien.com.queue</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Airway_Products_Incentive_Program_APIP</template>
    </alerts>
    <alerts>
        <fullName>Asset_Operations_Notification_Email_Equity_Rental_Agreement</fullName>
        <description>Asset Operations Notification Email Equity Rental Agreement</description>
        <protected>false</protected>
        <recipients>
            <recipient>rms.assetops@covidien.com.queue</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Asset_Operations_Notification_Equity_Rental</template>
    </alerts>
    <alerts>
        <fullName>Asset_Operations_Notification_Email_Rental_Agreement</fullName>
        <description>Asset Operations Notification Email Rental Agreement</description>
        <protected>false</protected>
        <recipients>
            <recipient>rms.assetops@covidien.com.queue</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Asset_Operations_Notification_Rental</template>
    </alerts>
    <alerts>
        <fullName>Customer_Service_Notification_Email</fullName>
        <description>Customer Service Notification Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>nassccs@covidien.com.queue</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Email_Templates/Cust_Service_Notification_Outright_VF</template>
    </alerts>
    <alerts>
        <fullName>End_Customer_Rebates_Notification_Email</fullName>
        <description>End Customer Rebates Notification Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>endcustomer.rebates@covidien.com.queue</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/End_Customer_Rebates_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>MAX_Sensor_Remanufacturing_Program_Agreement_Email</fullName>
        <ccEmails>paul.danek@statera.com</ccEmails>
        <description>MAX Sensor Remanufacturing Program Agreement Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>judith.randall@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PM_Marketing_Queue_Nellcor_Email</template>
    </alerts>
    <alerts>
        <fullName>PM_Service_Queue_Email</fullName>
        <ccEmails>paul.danek@statera.com</ccEmails>
        <description>PM Service Queue Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>judith.randall@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PM_Service_Queue_Email</template>
    </alerts>
    <alerts>
        <fullName>RMS_Agreement_Approval_Email_Alert</fullName>
        <description>RMS Agreement Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>RSM_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/RMS_Agreement_Approval_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_Agreement_Approval_Email_Alert</fullName>
        <description>SSG Agreement Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Agreement_Approval_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_Custom_Kit_Email_Alert</fullName>
        <description>SSG Custom Kit Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bpkits.fax@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Custom_Kits_VF</template>
    </alerts>
    <alerts>
        <fullName>SSG_Custom_Kit_Reorder_Code_Email_Alert</fullName>
        <description>SSG Custom Kit Reorder Code Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bpkits.fax@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Custom_Kit_Reorder_Code_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_New_Agreement_Proposal_Created</fullName>
        <description>SSG New Custom Pricing Proposal Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>mansfield.individualussurgebd@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Agreement_Proposal_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_New_Product_LOC_created</fullName>
        <description>New Product LOC created</description>
        <protected>false</protected>
        <recipients>
            <recipient>mansfield.individualussurgebd@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Rebates_Notification_EmailSSG_New_Product_LOC_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_New_Promotional_LOC_created</fullName>
        <description>SSG New Promotional LOC created</description>
        <protected>false</protected>
        <recipients>
            <recipient>mansfield.individualussurgebd@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_LOC_Promotion_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_OD_Activate_Notification_Email</fullName>
        <description>SSG OD Activate Notification Email</description>
        <protected>false</protected>
        <recipients>
            <field>OD_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_OD_Activate_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_OD_Agreement_Approver_Missing_Email_Alert</fullName>
        <description>SSG OD Agreement Approver Missing Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_OD_Agreement_Approver_Missing_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_RFA_Email_Alert</fullName>
        <description>SSG RFA Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ssgcomplianceq@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ssgswmcsq@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_RFA_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_Rebate_Email_Alert</fullName>
        <description>SSG Rebate Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ssgrebatesq@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Rebates_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_Rental_Email_Alert</fullName>
        <description>SSG Rental Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ssgcomplianceq@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ssgswmcsq@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Rental_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_Scrub_PO_Notification_Email</fullName>
        <description>Scrub PO Notification Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>ssgcustserv@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_ScrubPO_Notification_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Agreement_Approval_HQD</fullName>
        <field>Apttus__Status_Category__c</field>
        <literalValue>Request</literalValue>
        <name>Agreement Approval HQD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_Approvals_HQD_Status</fullName>
        <field>Apttus__Status__c</field>
        <literalValue>Request</literalValue>
        <name>Agreement Approvals HQD Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_End_Date_Order_Date</fullName>
        <field>Apttus__Contract_End_Date__c</field>
        <formula>SSG_Order_Date__c</formula>
        <name>Agreement End Date = Order Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apttus__SearchFieldUpdate</fullName>
        <description>Update the account search field with Account Name</description>
        <field>Apttus__Account_Search_Field__c</field>
        <formula>Apttus__Account__r.Name  &amp;  Apttus__FF_Agreement_Number__c</formula>
        <name>Search Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apttus__SetAgreementNumber</fullName>
        <description>Set agreement number from the auto generated contract number</description>
        <field>Apttus__Agreement_Number__c</field>
        <formula>Apttus__Contract_Number__c</formula>
        <name>Set Agreement Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apttus__SetClonetriggertofalse</fullName>
        <description>Set Clone trigger to false</description>
        <field>Apttus__Workflow_Trigger_Created_From_Clone__c</field>
        <literalValue>0</literalValue>
        <name>Set Clone trigger to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GenAgmtUpdt_StatusCat_ReadyforSignatures</fullName>
        <description>for Generated Agreements only</description>
        <field>Apttus__Status_Category__c</field>
        <literalValue>In Signatures</literalValue>
        <name>GenAgmtUpdt StatusCat ReadyforSignatures</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GenAgmtUpdt_Status_Ready_for_Signatures</fullName>
        <description>For Generated Agreements only</description>
        <field>Apttus__Status__c</field>
        <literalValue>Ready for Signatures</literalValue>
        <name>GenAgmtUpdt Status Ready for Signatures</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presented_Date</fullName>
        <field>Presented_Date__c</field>
        <formula>TODAY()</formula>
        <name>Presented Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Custom_Kit</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Custom_Kit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type = Custom Kit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Smart_Cart</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Smart_Cart</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type = Smart Cart</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSG_Set_Agreement_End_Date</fullName>
        <field>Apttus__Contract_End_Date__c</field>
        <formula>Apttus__Contract_Start_Date__c + 366</formula>
        <name>SSG Set Agreement End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSG_Smart_Cart_Actual_Customer_Cost</fullName>
        <description>Calculates Actual Customer Cost for Smart Cart Stocking Order when Quantity or Stocking Level changes an &quot;Actual Customer Cost&quot; contains a default value.</description>
        <field>SSG_Customer_Cost_Actual__c</field>
        <formula>IF(
BEGINS(TEXT(SSG_Stocking_Level__c),&quot;L1&quot;),
0,
IF(
BEGINS(TEXT(SSG_Stocking_Level__c),&quot;L2&quot;),
1000,
IF(
BEGINS(TEXT(SSG_Stocking_Level__c),&quot;L3&quot;),
2000,
0
)
)
)</formula>
        <name>SSG Smart Cart Actual Customer Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MPM_User</fullName>
        <description>Set the Marketing Product Manager to the SSG Custom Kit queue</description>
        <field>Marketing_Product_Manager__c</field>
        <lookupValue>jennifer.a.iasalvatore@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set MPM User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Scrub_PO_Lock_Status</fullName>
        <description>This is to set the Scrub PO agreement to lock status when the agreement is accepted or send for eSignatures.</description>
        <field>RecordTypeId</field>
        <lookupValue>Scrub_PO_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Scrub PO Lock Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Category_In_Signatures</fullName>
        <field>Apttus__Status_Category__c</field>
        <literalValue>In Signatures</literalValue>
        <name>Status Category = In Signatures</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Fully_Signed</fullName>
        <field>Apttus__Status__c</field>
        <literalValue>Fully Signed</literalValue>
        <name>Status = Fully Signed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Activated Order Form</fullName>
        <actions>
            <name>Activation_of_Vital_Sync_Agreement</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order_Form_Vital_Sync</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Exhibit B - Order Form Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>Sends a Task to User &quot;CommOps - Equip Admin Queue&quot; when an Order Form becomes activated and the record type equals &quot;Exhibit B - Order Form Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Agreement Approval Trigger</fullName>
        <actions>
            <name>Agreement_Approval_HQD</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Agreement_Approvals_HQD_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus_Approval__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Airway Products Incentive Program %28APIP%29</fullName>
        <actions>
            <name>Airway_Products_Incentive_Program_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Airway Prods Incentive Program (APIP)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;NASSC - End Customer Rebates Queue&quot; when an agreement becomes activated and when the record type equals &quot;Airway Prods Incentive Program (APIP)-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - COOP</fullName>
        <actions>
            <name>Add_Facilities_COOP</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Add_Facilities_COOP_CommOps</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>COOP Plus Program-AG Locked</value>
        </criteriaItems>
        <description>Sends a Task to Users &quot;NASSC - Individual Pricing Queue&quot; and &quot;CommOps - Equip Admin&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to Customer Optimization Plus Program (COOP).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - IHN</fullName>
        <actions>
            <name>Add_Facilities_IHN</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>Ventilator IHN Standardization Agreement-AG Locked</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to IHN Standardization Agreement.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - LNA</fullName>
        <actions>
            <name>Add_Facilities_LNA</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>Locally Negotiated Agreement (LNA)-AG Locked</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to Locally Negotiated Agreement (LNA).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - PM Equip Lease</fullName>
        <actions>
            <name>Add_Facilities_PM_Lease</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>PM Equipment Lease Program-AG Locked</value>
        </criteriaItems>
        <description>Sends a Task to User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to Patient Monitoring Equipment Lease Program.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - PM Service Contract</fullName>
        <actions>
            <name>Adding_Facilities_to_PM_Service_Contract</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>PM Service Contract-AG Locked</value>
        </criteriaItems>
        <description>Sends an email to User &quot;PM Service Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to PM Service Contract.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - Price Offer</fullName>
        <actions>
            <name>Add_Facilities_Price_Offer_Letter</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>Price Offer Letter-AG Locked</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to Price Offer Letter.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - Resp COOP</fullName>
        <actions>
            <name>Add_Facilities_Resp_COOP</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Add_Facilities_Resp_COOP_2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>Respiratory Solutions COOP - AG Locked</value>
        </criteriaItems>
        <description>Sends a Task to Users &quot;NASSC - Individual Pricing Queue&quot; and &quot;CommOps - Equip Admin&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to Respiratory Solutions COOP.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - Sales and Pricing</fullName>
        <actions>
            <name>Add_Facilities_Sales_Pricing</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>Sales and Pricing Agreement-AG Locked</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to  Sales &amp; Pricing Agreement.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Amendment to Add Facilities - Vital Sync</fullName>
        <actions>
            <name>Add_Facilities_Vital_Sync</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Amendment to Add Facilities-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Amendment_to_Add_Facilities_Source_RT__c</field>
            <operation>equals</operation>
            <value>Vital Sync S and S Agreement-AG Locked</value>
        </criteriaItems>
        <description>Sends a Task to User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Amendment to Add Facilities-AG Locked&quot;, and Adding Facilities to Vital Sync.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Apttus__Reset Clone Trigger</fullName>
        <actions>
            <name>Apttus__SetClonetriggertofalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Workflow_Trigger_Created_From_Clone__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Reset Clone Trigger</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apttus__Search Field Update</fullName>
        <actions>
            <name>Apttus__SearchFieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate an external Id search field with account name, so that side bar support can work with Account name search</description>
        <formula>or(not (isnull(Apttus__Account__r.Name)) ,not (isnull(Apttus__FF_Agreement_Number__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Apttus__Set Agreement Number</fullName>
        <actions>
            <name>Apttus__SetAgreementNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Agreement_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set agreement number for new agreements. The agreement number is auto generated.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>COOP - Amendment</fullName>
        <actions>
            <name>Request_for_COOP_Implementation_Amend1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_COOP_Implementation_Amend2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>COOP Plus Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Deal_Type__c</field>
            <operation>equals</operation>
            <value>Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing&quot; and &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;COOP Plus Program-AG Locked&quot;, and the Deal Type is equal to &quot;Amendment&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COOP Quote w%2F Vital Sync Activated</fullName>
        <actions>
            <name>Activation_of_COOP_with_Vital_Sync_products</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>COOP Plus Program-AG Locked,Respiratory Solutions COOP - AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.T_HLVL_VA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule sends an email notification to the HI&amp;M post-sales team when an agreement becomes activated, the record type equals &quot;COOP Plus Program-AG Locked&quot; and T_HLVL_VA__c = TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COOP with Rebate</fullName>
        <actions>
            <name>Implementation_Request_for_COOP_with_Rebate</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_COOP_with_Rebate</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 5) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.SSG_Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>COOP Plus Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Non_Standard_Use_of_Rebate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow rule will fire if a Rebate is selected with Sales and Pricing or COOP record types.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CPP Activation</fullName>
        <actions>
            <name>Request_for_CPP_Implementation</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_CPP_Implementation_EquipAdmin</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>A new CPP agreement is ready to be implemented.</description>
        <formula>AND( RecordType.DeveloperName = &quot;Rebate_Agreement_AG_Locked&quot;,  ISPICKVAL(Apttus__Status__c,&quot;Activated&quot;),  OR( CPP_PM_HPG_Accrual_Rebate__c = True, CPP_PM_Standard_Accrual_Rebate__c = True, CPP_RS_Standard_Accrual_Rebate__c = True, CPP_North_Star__c = True))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cash Quote w%2F CPP Funds</fullName>
        <actions>
            <name>Quote_with_CPP_Funds</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Outright Quote (Hardware)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Using_CPP_Funds__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Quote Direct-AG Locked</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Outright Quote (Hardware)-AG Locked&quot; or &quot;Hardware Quote Direct-AG Locked&quot;, and CPP Funds = TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cash Quote w%2F TBD Dollars</fullName>
        <actions>
            <name>Quote_with_TBD_Dollars</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Outright Quote (Hardware)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Using_TBD_Dollars__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Quote Direct-AG Locked</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Outright Quote (Hardware)-AG Locked&quot; or &quot;Hardware Quote Direct-AG Locked&quot;, and TBD Dollars = TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cash Quote w%2F Vital Sync</fullName>
        <actions>
            <name>Activation_Cash_Quote_with_Vital_Sync</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_with_Vital_Sync</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Outright Quote (Hardware)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.T_HLVL_VA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Quote Direct-AG Locked</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Outright Quote (Hardware)-AG Locked&quot; or &quot;Hardware Quote Direct-AG Locked&quot;, and T_HLVL_VA__c = TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cash Quote w%2F Vital Sync Activated</fullName>
        <actions>
            <name>Activation_Cash_Quote_with_Vital_Sync</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_with_Vital_Sync</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Outright Quote (Hardware)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.T_HLVL_VA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Quote Direct-AG Locked</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Outright Quote (Hardware)-AG Locked&quot; or &quot;Hardware Quote Direct-AG Locked&quot;, and T_HLVL_VA__c = TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CommOps - Asset Ops Evaluation Notification</fullName>
        <actions>
            <name>Comm_Opps_Evaluation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Evaluation-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Asset Ops Queue&quot; when an agreement becomes activated and when the record type equals &quot;Evaluation-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Custom Kit - Set MPM</fullName>
        <actions>
            <name>Set_MPM_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Marketing_Product_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Agreement_Developer_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Custom_Kit</value>
        </criteriaItems>
        <description>Set the Marketing Product Manager for Custom Kit deal types.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Customer Optimization Plus Program %28COOP%29</fullName>
        <actions>
            <name>Request_for_COOP_Implementation</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_COOP_Implementation_commops</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>COOP Plus Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Deal_Type__c</field>
            <operation>notEqual</operation>
            <value>Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; and &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated and when the record type equals &quot;COOP Plus Program-AG Locked&quot;, and Deal Type is not equal to &quot;Amendment&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Presented Date</fullName>
        <actions>
            <name>Presented_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Presented_Date__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Equipment Placement Program  %28EPP%27s%29 w%2F Pricing</fullName>
        <actions>
            <name>Request_for_an_EPP_CD</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_an_EPP_nassc</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_an_EPP_needs_Pricing</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Equipment Placement Program (EPPs)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Consumables_Subtotal__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Asset Ops Queue&quot; when an agreement becomes activated and when the record type equals &quot;Equipment Placement Program (EPPs)-AG Locked&quot;, and the Proposal Amount is not equal to null.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Equipment Placement Program  %28EPP%27s%29 w%2Fo Pricing</fullName>
        <actions>
            <name>Request_for_an_EPP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Equipment Placement Program (EPPs)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Consumables_Subtotal__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Consumables_Subtotal__c</field>
            <operation>equals</operation>
            <value>USD 0</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Asset Ops Queue&quot; when an agreement becomes activated and when the record type equals &quot;Equipment Placement Program (EPPs)-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Equity Rental Agreements</fullName>
        <actions>
            <name>Asset_Operations_Notification_Email_Equity_Rental_Agreement</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Request_for_an_Equity_Rental</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Equity Rental Agreements-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Asset Ops Queue&quot; and an email to the User &quot;RS Service Queue&quot; when an agreement becomes activated and when the record type equals &quot;Equity Rental Agreements-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IHN Standardization Agreement</fullName>
        <actions>
            <name>Request_for_an_IHN</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ventilator IHN Standardization Agreement-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated and when the record type equals &quot;Ventilator IHN Standardization Agreement-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Locally Negotiated Agreement %28LNA%29</fullName>
        <actions>
            <name>Request_for_an_LNA</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Locally Negotiated Agreement (LNA)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated and when the record type equals &quot;Locally Negotiated Agreement (LNA)-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MAX Sensor Remanufacturing Program Agreement</fullName>
        <actions>
            <name>MAX_Sensor_Remanufacturing_Program_Agreement_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>MAX Sensor Remanufacturing Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;PM Marketing Queue - Nellcor Queue&quot; when an agreement becomes activated and when the record type equals &quot;MAX Sensor Remanufacturing Program-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Non Standard Agreements</fullName>
        <actions>
            <name>Request_for_a_Non_Standard_Agreement</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This rule sends a Task to the User &quot;CommOps - Contract Dev Queue&quot; when an agreement becomes activated and when any of the &quot;Non Standard&quot; checkboxes = True.</description>
        <formula>AND($Profile.Name = &apos;APTTUS - RMS US Sales (RS+PM)&apos;, OR( Request_to_Adjust_Payment_Terms__c = true,    Request_for_Price_Guarantee_Language__c = true,  Request_to_Adjust_Waive_Min_Order_Fee__c = true,  Non_Standard_Use_of_Marketing_Promotion__c =  true,  Non_Standard_Use_of_Rebate__c = true,  Request_to_Adjust_Waive_Freight__c = true,  Other_Non_Standard_Value_Requests__c = true,  Non_Standard_Governing_Law__c = true,  Non_Standard_Termination_Section__c = true,  Non_Standard_Warranty_Language__c = true,  Non_Standard_Indemnification__c = true,  Other_Non_Standard_Term_Requests__c = true))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Outright Quote %28Hardware%29 w%2F Service</fullName>
        <actions>
            <name>Quote_with_Service_Customer_Acceptance_Form</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Outright Quote (Hardware)-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Service__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;RS Service Queue&quot; when an agreement becomes activated and when the record type equals &quot;Outright Quote (Hardware)-AG Locked&quot;, Service checkbox = True.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PM Service Contract Agreement</fullName>
        <actions>
            <name>PM_Service_Queue_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>PM Service Contract-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;PM Service Queue&quot; when an agreement becomes activated and when the record type equals &quot;PM Service Contract-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Patient Monitoring Equipment Lease Program</fullName>
        <actions>
            <name>Request_for_PM_Equipment_Lease_Implementation_PME1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_PM_Equipment_Lease_Implementation_PME2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>PM Equipment Lease Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Equip Admin Queue&quot; and User &quot;NASSC - End Customer Rebates Queue&quot; when an agreement becomes activated and when the record type equals &quot;PM Equipment Lease Program-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Price Offer Letter Agreement</fullName>
        <actions>
            <name>Request_for_Courtesy_Pricing</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Price Offer Letter-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated and when the record type equals &quot;Price Offer Letter-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMS Agreement Approval Workflow Rule</fullName>
        <actions>
            <name>RMS_Agreement_Approval_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Quote Direct-AG,Hardware Quote Direct-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus_Approval__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>This workflow sends an email to the agreement owner and RSM manager when an approval is approved or rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rebate</fullName>
        <actions>
            <name>End_Customer_Rebates_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Request_for_Rebate_Implementation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - End Customer Rebates Queue&quot; when a Rebate Agreement becomes activated AND one of the rebate checkboxes gets checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rental Agreements</fullName>
        <actions>
            <name>Asset_Operations_Notification_Email_Rental_Agreement</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Request_for_a_Rental_Agreement</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rental Agreements-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Asset Ops Queue&quot; and an email to the User &quot;RS Service Queue&quot; when an agreement becomes activated and when the record type equals &quot;Rental Agreements-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Respiratory Solutions COOP</fullName>
        <actions>
            <name>Request_for_COOP_Implementation_Resp1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_COOP_Implementation_Resp2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Respiratory Solutions COOP - AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Deal_Type__c</field>
            <operation>notEqual</operation>
            <value>Amendment</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing&quot; and &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Respiratory Solutions COOP - AG Locked&quot;, and the Deal Type is not equal to &quot;Amendment&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Respiratory Solutions COOP Amendment</fullName>
        <actions>
            <name>Request_for_Respiritory_Solutions_COOP_Implementation_Amend1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Request_for_Respiritory_Solutions_COOP_Implementation_Amend2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Respiratory Solutions COOP - AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Deal_Type__c</field>
            <operation>equals</operation>
            <value>Amendment</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing&quot; and &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated, the record type equals &quot;Respiratory Solutions COOP - AG Locked&quot;, and the Deal Type is equal to &quot;Amendment&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Agreement Approval Workflow Rule</fullName>
        <actions>
            <name>SSG_Agreement_Approval_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Pricing Proposal-AG Locked,Custom Kit Locked,GPO LOC - Locked,New Product LOC-AG Locked,Promotional LOC-AG Locked,Rentals-AG Locked,Royalty Free Agreement-AG Locked,Scrub PO Locked,Smart Cart Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Kit,GPO LOC,Scrub PO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus_Approval__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>This rule sends an email to the record owner when an agreement approval status = accepted or rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Custom Kit Reorder Code</fullName>
        <actions>
            <name>SSG_Custom_Kit_Reorder_Code_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Reorder_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Kit</value>
        </criteriaItems>
        <description>This workflow notifies the custom kit queue when a custom kit agreement reorder code is entered or changed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Custom Kits Workflow Rule</fullName>
        <actions>
            <name>SSG_Custom_Kit_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Kit Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Custom Kit Queue&quot; when an agreement becomes activated and when the record type equals &quot;Custom Kit Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Custom Pricing Proposal Workflow Rule</fullName>
        <actions>
            <name>SSG_New_Agreement_Proposal_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Pricing Proposal-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Contracts Team Queue Queue&quot; when an agreement becomes activated and when the record type equals Custom Pricing Proposal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG LOC New Product Workflow Rule</fullName>
        <actions>
            <name>SSG_New_Product_LOC_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Product LOC-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Contracts Team Queue Queue&quot; when an agreement becomes activated and when the record type equals New Product LOC-AG Locked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG LOC Promotions Workflow Rule</fullName>
        <actions>
            <name>SSG_New_Promotional_LOC_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Promotional LOC-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Contracts Team Queue Queue&quot; when an agreement becomes activated and when the record type equals Promotional LOC-AG Locked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Non Standard Redline Agreement Workflow Rule</fullName>
        <actions>
            <name>SSG_Non_Standard_Request</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This rule sends a Task to the User &quot;SSG Legal Team Queue&quot; when an agreement becomes activated and when the &quot;SSG Non Standard&quot; checkbox = True.</description>
        <formula>AND ($Profile.Name = &apos;APTTUS - SSG US Account Executive&apos;, OR ( SSG_NonStandard_Language_Requested__c = true, Other_Non_Standard_Value_Requests__c = true, Other_Non_Standard_Term_Requests__c = true))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG OD Activate Workflow Rule</fullName>
        <actions>
            <name>SSG_OD_Activate_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agreement Proposal-AG,Agreement Proposal-AG,New Product LOC-AG,Promotional LOC-AG,Royalty Free Agreement-AG,Rentals-AG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Fully Signed</value>
        </criteriaItems>
        <description>This rule sends an email to the OD Approver when an agreement becomes fully signed and when the record type equals any of the SSG record types</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG OD Approver Missing Agreement</fullName>
        <actions>
            <name>SSG_OD_Agreement_Approver_Missing_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>GPO LOC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.OD_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>notify owner of an agreement record if the OD approver is not set</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG RFA Workflow Rule</fullName>
        <actions>
            <name>SSG_RFA_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>RFA_Processing</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Royalty Free Agreement-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Contracts Team Queue Queue&quot; and &quot;SSG SWM Customer Service Queus&quot; when an agreement becomes activated and when the record type equals Royalty Free Agreement-AG Locked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Rebate Workflow Rule</fullName>
        <actions>
            <name>SSG_Rebate_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends a Task to the User &quot;SSG - End Customer Rebates Queue&quot; when an agreement becomes activated AND one of the rebate checkboxes gets checked.</description>
        <formula>AND (ISPICKVAL(Apttus__Status__c,&quot;Activated&quot;),  SSG_Rebates_Selected__c = True)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Rental Workflow Rule</fullName>
        <actions>
            <name>SSG_Rental_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rentals-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Compliance Team Queue&quot; and &quot;SSG SWM Customer Service Queue&quot; when an agreement becomes activated and when the record type equals Rental Locked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Scrub PO Workflow Rule</fullName>
        <actions>
            <name>SSG_Scrub_PO_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Scrub PO - Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends an email to the User &quot;SSG Customer Service Team Queue &quot; when a scrub PO agreement is activated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Set Agreement End Date</fullName>
        <actions>
            <name>SSG_Set_Agreement_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Contract_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets the end date for the agreement to 1 year from start date</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SSG Smart Cart Calculate Actual Customer Cost</fullName>
        <actions>
            <name>SSG_Smart_Cart_Actual_Customer_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Calculates value for &quot;Actual Customer Cost&quot; if not overridden by user. Will not recalculate if a non-standard value is provided.</description>
        <formula>AND(   OR(     ISNEW(),     ISCHANGED(SSG_Quantity_of_Carts_Ordered__c),     ISCHANGED(SSG_Stocking_Level__c)   ),   OR(     ISBLANK(SSG_Customer_Cost_Actual__c),     AND( BEGINS(TEXT(PRIORVALUE(SSG_Stocking_Level__c)),&quot;&quot;) , SSG_Customer_Cost_Actual__c = 0 ),     AND( BEGINS(TEXT(PRIORVALUE(SSG_Stocking_Level__c)),&quot;L1&quot;) , SSG_Customer_Cost_Actual__c = 0 ),     AND( BEGINS(TEXT(PRIORVALUE(SSG_Stocking_Level__c)),&quot;L2&quot;) , SSG_Customer_Cost_Actual__c = 1000 ),     AND( BEGINS(TEXT(PRIORVALUE(SSG_Stocking_Level__c)),&quot;L3&quot;) , SSG_Customer_Cost_Actual__c = 2000 )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales and Pricing Agreement</fullName>
        <actions>
            <name>Request_for_a_Sales_Pricing_Agreement</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales and Pricing Agreement-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated and when the record type equals &quot;Sales and Pricing Agreement-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales and Pricing with Rebate</fullName>
        <actions>
            <name>Request_for_Pricing_with_Rebate</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 5) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.SSG_Rebates_Selected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales and Pricing Agreement-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Non_Standard_Use_of_Rebate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow rule will fire if a Rebate is selected with Sales and Pricing or COOP record types.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Scrub PO Accepted %3D Fully Signed</fullName>
        <actions>
            <name>Status_Category_In_Signatures</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_Fully_Signed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets Agreement Status to &quot;Fully Signed&quot; when &quot;Customer Accepts&quot; box is checked.</description>
        <formula>AND(  BEGINS(RecordType.DeveloperName, &apos;Scrub_PO&apos;),  ISCHANGED(I_Accept__c),  I_Accept__c = TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Scrub PO Lock After Accept</fullName>
        <actions>
            <name>Set_Scrub_PO_Lock_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to set the Scrub PO agreement to lock status when the agreement is accepted.</description>
        <formula>I_Accept__c = true &amp;&amp; RecordType.DeveloperName = &apos;Scrub_PO&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Scrub PO Lock After eSig</fullName>
        <actions>
            <name>Set_Scrub_PO_Lock_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to set the Scrub PO agreement to lock status when the agreement sent for eSignatures.</description>
        <formula>ISPICKVAL(Apttus__Status__c ,&apos;Other Party Signatures&apos;) &amp;&amp; RecordType.DeveloperName = &apos;Scrub_PO&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Smart Cart Agmt End Date %3D Order Date</fullName>
        <actions>
            <name>Agreement_End_Date_Order_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets Agreement End date from Order Date on Smart Cart deals</description>
        <formula>AND( BEGINS(Agreement_Developer_Record_Type_Name__c, &quot;Smart_Cart&quot;) , OR( ISNEW(), ISCHANGED( SSG_Order_Date__c ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TaperGuard Seed Program</fullName>
        <actions>
            <name>Request_for_a_TaperGuard_Seed_Program</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>TaperGuard Seed Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated and when the record type equals &quot;TaperGuard Seed Program-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TaperGuard Volume Pricing Program</fullName>
        <actions>
            <name>Request_for_a_TaperGuard_Volume_Pricing</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>TaperGuard Volume Pricing Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;NASSC - Individual Pricing Queue&quot; when an agreement becomes activated and when the record type equals &quot;TaperGuard Volume Pricing Program-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unlock Approval Recall%3A Custom Kit</fullName>
        <actions>
            <name>Record_Type_Custom_Kit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Kit Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus_Approval__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approval Required</value>
        </criteriaItems>
        <description>Unlocks a Custom Kit agreement when recalled by a submitting user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unlock Approval Recall%3A Smart Cart</fullName>
        <actions>
            <name>Record_Type_Smart_Cart</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Smart Cart Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus_Approval__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approval Required</value>
        </criteriaItems>
        <description>Unlocks a Smart Cart agreement when recalled by a submitting user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updt Chevron-Generated Agmt Document</fullName>
        <actions>
            <name>GenAgmtUpdt_StatusCat_ReadyforSignatures</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GenAgmtUpdt_Status_Ready_for_Signatures</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Workflow_Trigger_Viewed_Final__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status_Category__c</field>
            <operation>equals</operation>
            <value>Request</value>
        </criteriaItems>
        <description>Utilizes the Workflow Trigger Generated Agreement field to only look at generated agreements and sets the Status to Ready for Signatures and Status Category to In Signatures to set the Chevron to &apos;DOCUMENT GENERATED&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Vital Sync</fullName>
        <actions>
            <name>Request_for_Vital_Sync_Implementation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vital Sync S and S Agreement-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Equip Admin Queue&quot; when an agreement becomes activated and when the record type equals &quot;Vital Sync S and S Agreement-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Warmtouch Placement Exchange Program</fullName>
        <actions>
            <name>Request_for_a_Warmtouch_Placement</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Warmtouch Placement Exch Program-AG Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>This rule sends a Task to the User &quot;CommOps - Asset Ops Queue&quot; when an agreement becomes activated and when the record type equals &quot;Warmtouch Placement Exch Program-AG Locked&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Add_Facilities_COOP</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a COOP is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - COOP</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_COOP_CommOps</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a COOP is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - COOP</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_IHN</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to an IHN is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - IHN</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_LNA</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to an LNA is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - LNA</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_PM_Lease</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a PM Lease is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - PM Lease</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_Price_Offer_Letter</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a Price Offer Letter is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - Price Offer Letter</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_Resp_COOP</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a Respiratory Solutions COOP is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - Resp. COOP</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_Resp_COOP_2</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a Respiratory Solutions COOP is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - Resp. COOP</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_Sales_Pricing</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a Sales &amp; Pricing Agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - Sales &amp; Pricing</subject>
    </tasks>
    <tasks>
        <fullName>Add_Facilities_Vital_Sync</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amendment to Add Facilities to a Vital Sync agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Facilities - Vital Sync</subject>
    </tasks>
    <tasks>
        <fullName>Comm_Opps_Evaluation</fullName>
        <assignedTo>rms.assetops@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Evaluation agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an Evaluation</subject>
    </tasks>
    <tasks>
        <fullName>Implementation_Request_for_COOP_with_Rebate</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new COOP Agreement with a Rebate is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Implementation Request for COOP with Rebate</subject>
    </tasks>
    <tasks>
        <fullName>Order_Form_Vital_Sync</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Vital Sync Order Form agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Order Form - Vital Sync</subject>
    </tasks>
    <tasks>
        <fullName>Quote_with_CPP_Funds</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Quote using CPP Funds is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Quote with CPP Funds</subject>
    </tasks>
    <tasks>
        <fullName>Quote_with_Service_Customer_Acceptance_Form</fullName>
        <assignedTo>rsservice@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Quote with Service Customer Acceptance Form is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Quote with Service Customer Acceptance Form</subject>
    </tasks>
    <tasks>
        <fullName>Quote_with_TBD_Dollars</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Quote using TBD Dollars is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Quote with TBD Dollars</subject>
    </tasks>
    <tasks>
        <fullName>Quote_with_Vital_Sync</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Quote with a Vital Sync product is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Quote with Vital Sync</subject>
    </tasks>
    <tasks>
        <fullName>RFA_Processing</fullName>
        <assignedTo>ssgcomplianceq@medtronic.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please process this signed RFA and close this task once completed. 
Thank you!</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for RFA Processing</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_Implementation</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new COOP agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_Implementation_Amend1</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amended COOP  is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_Implementation_Amend2</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amended COOP  is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_Implementation_Resp1</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Respiratory Solutions COOP agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Respiratory Solutions COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_Implementation_Resp2</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Respiratory Solutions COOP agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Respiratory Solutions COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_Implementation_commops</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new COOP agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_COOP_with_Rebate</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new COOP Agreement with a Rebate is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for COOP with Rebate</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_CPP_Implementation</fullName>
        <assignedTo>endcustomer.rebates@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new CPP agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for CPP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_CPP_Implementation_EquipAdmin</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new CPP agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for CPP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Courtesy_Pricing</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Price Offer Letter is ready for implementation</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Courtesy Pricing</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_PM_Equipment_Lease_Implementation_PME1</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new PM Equipment Lease agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for PM Equipment Lease Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_PM_Equipment_Lease_Implementation_PME2</fullName>
        <assignedTo>endcustomer.rebates@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new PM Equipment Lease agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for PM Equipment Lease Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Pricing_with_Rebate</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Sales &amp; Pricing Agreement with a Rebate is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Sales and Pricing with Rebate</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Quote_Processing</fullName>
        <assignedTo>nassccs@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new quote is ready to be processed.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Quote Processing</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Rebate_Implementation</fullName>
        <assignedTo>endcustomer.rebates@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Rebate agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Rebate Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Respiritory_Solutions_COOP_Implementation_Amend1</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amended Respiratory Solutions COOP  is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Respiritory Solutions COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Respiritory_Solutions_COOP_Implementation_Amend2</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An Amended Respiratory Solutions COOP  is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Respiritory Solutions COOP Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_Vital_Sync_Implementation</fullName>
        <assignedTo>rms.equipadmin@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Vital Sync agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Vital Sync Implementation</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_a_Non_Standard_Agreement</fullName>
        <assignedTo>rms.contractdev@covidien.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A Non Standard Proposal has been approved and is ready to be drafted.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for a Non Standard Agreement</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_a_Rental_Agreement</fullName>
        <assignedTo>rsservice@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Rental agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for a Rental Agreement</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_a_Sales_Pricing_Agreement</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Sales &amp; Pricing Agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for a Sales &amp; Pricing Agreement</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_a_TaperGuard_Seed_Program</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new TG Seed Program agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for a TaperGuard Seed Program</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_a_TaperGuard_Volume_Pricing</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new TaperGuard Volume Pricing  agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for a TaperGuard Volume Pricing</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_a_Warmtouch_Placement</fullName>
        <assignedTo>rms.assetops@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Warmtouch Placement Exchange agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for a Warmtouch Placement</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_EPP</fullName>
        <assignedTo>rms.assetops@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new EPP agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an EPP</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_EPP_CD</fullName>
        <assignedTo>rms.contractdev@covidien.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new EPP agreement is ready for implementation</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an EPP</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_EPP_nassc</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new EPP agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an EPP</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_EPP_needs_Pricing</fullName>
        <assignedTo>rms.assetops@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new EPP agreement is ready for implementation</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an EPP</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_Equity_Rental</fullName>
        <assignedTo>rsservice@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Equity Rental agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an Equity Rental</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_IHN</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new IHN agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an IHN</subject>
    </tasks>
    <tasks>
        <fullName>Request_for_an_LNA</fullName>
        <assignedTo>nasscindv@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new LNA agreement is ready for implementation.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Apttus__APTS_Agreement__c.Apttus__Activated_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for an LNA</subject>
    </tasks>
    <tasks>
        <fullName>SSG_Non_Standard_Request</fullName>
        <assignedTo>ssglegalq@medtronic.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Customer has requested redlining for a contract.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Customer has requested contract redlining</subject>
    </tasks>
</Workflow>
