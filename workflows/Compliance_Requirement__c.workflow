<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Compliance Task Creation</fullName>
        <active>false</active>
        <formula>Commitment_Level__c &gt; 10</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Test</fullName>
        <active>false</active>
        <formula>Commitment_Level__c &gt; 10</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
