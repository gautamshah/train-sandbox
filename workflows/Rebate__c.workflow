<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_BIS_COT_false</fullName>
        <field>BIS_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set BIS COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_BIS_COT_true</fullName>
        <field>BIS_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set BIS COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Breathing_Systems_COT_false</fullName>
        <field>Breathing_Systems_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set Breathing Systems COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Breathing_Systems_COT_true</fullName>
        <field>Breathing_Systems_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set Breathing Systems COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Capnography_COT_false</fullName>
        <field>Capnography_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set Capnography COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Capnography_COT_true</fullName>
        <field>Capnography_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set Capnography COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ETT_COT_false</fullName>
        <field>ETT_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set ETT COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ETT_COT_true</fullName>
        <field>ETT_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set ETT COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_INVOS_COT_false</fullName>
        <field>INVOS_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set INVOS COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_INVOS_COT_true</fullName>
        <field>INVOS_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set INVOS COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_McGrath_COT_false</fullName>
        <field>McGrath_Blade_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set McGrath COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_McGrath_COT_true</fullName>
        <field>McGrath_Blade_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set McGrath COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nellcor_COT_false</fullName>
        <field>Nellcor_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set Nellcor COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nellcor_COT_true</fullName>
        <field>Nellcor_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set Nellcor COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Tracheostomy_COT_false</fullName>
        <field>Tracheostomy_COT__c</field>
        <literalValue>0</literalValue>
        <name>Set Tracheostomy COT - false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Tracheostomy_COT_true</fullName>
        <field>Tracheostomy_COT__c</field>
        <literalValue>1</literalValue>
        <name>Set Tracheostomy COT - true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Related_Proposal__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Set BIS COT - false</fullName>
        <actions>
            <name>Set_BIS_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.BIS_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set BIS COT - true</fullName>
        <actions>
            <name>Set_BIS_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.BIS_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Breathing Systems COT - false</fullName>
        <actions>
            <name>Set_Breathing_Systems_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Breathing_Systems_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Breathing Systems COT - true</fullName>
        <actions>
            <name>Set_Breathing_Systems_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Breathing_Systems_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Capnography COT - false</fullName>
        <actions>
            <name>Set_Capnography_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Capnography_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Capnography COT - true</fullName>
        <actions>
            <name>Set_Capnography_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Capnography_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set ETT COT - false</fullName>
        <actions>
            <name>Set_ETT_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.ETT_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set ETT COT - true</fullName>
        <actions>
            <name>Set_ETT_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.ETT_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set INVOS COT - false</fullName>
        <actions>
            <name>Set_INVOS_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.INVOS_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set INVOS COT - true</fullName>
        <actions>
            <name>Set_INVOS_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.INVOS_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set McGrath COT - false</fullName>
        <actions>
            <name>Set_McGrath_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.McGrath_Blade_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set McGrath COT - true</fullName>
        <actions>
            <name>Set_McGrath_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.McGrath_Blade_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Nellcor COT - false</fullName>
        <actions>
            <name>Set_Nellcor_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Nellcor_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Nellcor COT - true</fullName>
        <actions>
            <name>Set_Nellcor_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Nellcor_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Tracheostomy COT - false</fullName>
        <actions>
            <name>Set_Tracheostomy_COT_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Tracheostomy_COT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Tracheostomy COT - true</fullName>
        <actions>
            <name>Set_Tracheostomy_COT_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Rebate__c.Tracheostomy_COT__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Rebate__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPP North Star</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rebate Agreement</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
