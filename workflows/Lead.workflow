<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Lead_Responsible_Party_after_51_days_of_not_qualifying_the_lead</fullName>
        <description>Email to Lead Responsible Party after 51 days of not qualifying the lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>hanspeter.schielly@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/Email_to_Lead_Responsible_Party_after_51_days_of_not_qualifying_the_lead</template>
    </alerts>
    <alerts>
        <fullName>New_Lead_Assignment1</fullName>
        <description>New Lead Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/New_Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_assigned_to_a_user_for_6_days</fullName>
        <description>lead has not been assigned to a user for 6 days</description>
        <protected>false</protected>
        <recipients>
            <recipient>hanspeter.schielly@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/lead_has_not_been_assigned_to_a_user_for_6_days</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_assigned_to_a_user_for_a_few_days</fullName>
        <description>lead has not been assigned to a user for a few days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/lead_has_not_been_assigned_to_a_user_for_a_few_days</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_qualified_within_30_days</fullName>
        <description>lead has not been qualified within 30 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/lead_has_not_been_qualified_within_30_days</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_qualified_within_37_days</fullName>
        <description>lead has not been qualified within 37 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/lead_has_not_been_qualified_within_37_days</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_qualified_within_44_days</fullName>
        <description>lead has not been qualified within 44 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Template_Folder/lead_has_not_been_qualified_within_44_days</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_Unqualified_Lead_RMS_Queue</fullName>
        <description>Change Owner to Unqualified Lead RMS Queue</description>
        <field>OwnerId</field>
        <lookupValue>Unqualified_Leads_RMS</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unqualified Lead RMS Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Unqualified_Lead_Surgical_Q</fullName>
        <description>Change ownership of an unqualified Surgical lead to the Unqualified Lead Surgical Queue</description>
        <field>OwnerId</field>
        <lookupValue>Unqualified_Leads_Surgical</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unqualified Lead Surgical Q</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_to_Waiting_Assignment_Q</fullName>
        <description>Change ownership of the lead to the Waiting Assignment Queue</description>
        <field>OwnerId</field>
        <lookupValue>Waiting_Assignment</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Move to Waiting Assignment Q</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Lead_Status</fullName>
        <description>Reset Lead Status to &quot;Not Attempted&quot;</description>
        <field>Status</field>
        <literalValue>Not Attempted</literalValue>
        <name>Reset Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_to_Close_field_update</fullName>
        <field>Time_to_Close__c</field>
        <formula>NOW()-(CreatedDate)</formula>
        <name>Time to Close field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_to_Contact_catch</fullName>
        <field>Time_to_Contact__c</field>
        <formula>NOW()- (CreatedDate)</formula>
        <name>Time to Contact catch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_to_Contact_field_update</fullName>
        <field>Time_to_Contact__c</field>
        <formula>NOW()-(CreatedDate)</formula>
        <name>Time to Contact field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Dispose Unqualified RMS Lead</fullName>
        <actions>
            <name>Assign_to_Unqualified_Lead_RMS_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>RMS Lead Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Is_Parent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Change ownership of an unqualified RMS lead to the Unqualified Lead RMS Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dispose Unqualified Surgical Lead</fullName>
        <actions>
            <name>Assign_to_Unqualified_Lead_Surgical_Q</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Surgical Lead Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Is_Parent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Change ownership of an unqualified Surgical lead to the Unqualified Lead Surgical Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Owner - email Change Notification</fullName>
        <actions>
            <name>New_Lead_Assignment1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email the new owner of the lead</description>
        <formula>AND(OR(RecordType.Name =&apos;RMS Lead Type&apos; ,RecordType.Name =&apos;Surgical Lead Type&apos;), OwnerId &lt;&gt; PRIORVALUE(OwnerId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads Assigned to Queue</fullName>
        <active>true</active>
        <formula>AND(NOT(ISBLANK( Owner:Queue.Id )), ISBLANK(Owner:User.Id),NOT (CONTAINS(  Owner:Queue.QueueName , &apos;EU_All&apos;)), RecordType.Name = &apos;EU S2 Lead PILOT&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_assigned_to_a_user_for_6_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_assigned_to_a_user_for_a_few_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Not qualified in 30 days</fullName>
        <active>false</active>
        <formula>And(NOT(ISBLANK(Owner:User.Id)),   ISPICKVAL(Status , &apos;Qualified&apos;),  RecordType.Name = &apos;EU S2 Lead PILOT&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_qualified_within_30_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_qualified_within_44_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>44</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_Lead_Responsible_Party_after_51_days_of_not_qualifying_the_lead</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>51</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_qualified_within_37_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>37</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Reposition unqualified lead</fullName>
        <actions>
            <name>Move_to_Waiting_Assignment_Q</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Unqualified Leads RMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastModifiedById</field>
            <operation>contains</operation>
            <value>Eloqua</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Is_Parent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Unqualified Leads Surgical</value>
        </criteriaItems>
        <description>workflow to take a lead out of the Unqualified Lead queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Time to Close</fullName>
        <actions>
            <name>Time_to_Close_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified,Unqualified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Time to Contact</fullName>
        <actions>
            <name>Time_to_Contact_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Contacted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Time to Contact catch</fullName>
        <actions>
            <name>Time_to_Contact_catch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3) OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Time_to_Contact__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Call Lead</fullName>
        <active>true</active>
        <description>Call Lead - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Email Lead</fullName>
        <active>true</active>
        <description>Email Lead - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>zaapit__Send Letter Lead</fullName>
        <active>true</active>
        <description>Send Letter Lead - by ZaapIT</description>
        <formula>false</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>zaapit__Call_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Email_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Email</subject>
    </tasks>
    <tasks>
        <fullName>zaapit__Send_Letter_Lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Letter</subject>
    </tasks>
</Workflow>
