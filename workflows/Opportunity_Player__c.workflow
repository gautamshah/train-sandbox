<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Oppor_Player</fullName>
        <field>OpportunityPlayers__c</field>
        <formula>Name &amp; TEXT( Player_Type__c)</formula>
        <name>Unique Oppor Player</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mirror_Field</fullName>
        <field>Supports_Solution_2_Mirror__c</field>
        <formula>Supports_Solution_2__c</formula>
        <name>Update Mirror Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Player_Name</fullName>
        <field>Name</field>
        <formula>Contact__r.Full_Name_with_Salutation__c</formula>
        <name>Update Opportunity Player Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity Player Name</fullName>
        <actions>
            <name>Update_Opportunity_Player_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Player__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support Solutions Image</fullName>
        <actions>
            <name>Update_Mirror_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Player__c.CreatedDate</field>
            <operation>notEqual</operation>
            <value>11/21/2000</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>opp_player_dup_prevention</fullName>
        <actions>
            <name>Unique_Oppor_Player</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Player__c.Name</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
