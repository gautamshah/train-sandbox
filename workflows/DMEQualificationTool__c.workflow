<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IRIS_Sales_Rep_and_Manager_Email_Alert</fullName>
        <ccEmails>carson.friend@medtronic.com</ccEmails>
        <description>IRIS Sales Rep and Manager Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>IRISSalesRep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/IRIS_Qualifiers_notification</template>
    </alerts>
    <rules>
        <fullName>IRIS Notification Email Rule</fullName>
        <actions>
            <name>IRIS_Sales_Rep_and_Manager_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DMEQualificationTool__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>IRIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>DMEQualificationTool__c.IRISSalesRep__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
