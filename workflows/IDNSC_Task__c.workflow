<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_IDNSC_Task_Assigned_Date</fullName>
        <description>When Preparation Activities are created, set their Assigned Date manually</description>
        <field>Date_Assigned__c</field>
        <formula>Today()</formula>
        <name>Set IDNSC Task Assigned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Task_Owner_Email</fullName>
        <field>Task_Owner_Email__c</field>
        <formula>Case (Role__c,
&quot;Sales Analyst&quot;, IDNSC_Parent__r.Role_Sales_Analyst__r.Email ,
&quot;Customer Care Manager&quot;, IDNSC_Parent__r.Role_Customer_Care__r.Email ,
&quot;Data Steward&quot;, IDNSC_Parent__r.Role_Data_Steward__r.Email ,
&quot;Pricing Specialist&quot;, IDNSC_Parent__r.Role_Pricing_Specialist__r.Email ,
&quot;RVP&quot;, IDNSC_Parent__r.Role_RVP__r.Email ,
&quot;Customer Service&quot;, IDNSC_Parent__r.Role_Customer_Service__r.Email ,
&quot;EDI Support&quot;, IDNSC_Parent__r.Role_EDI_Support__r.Email ,
&quot;Rebates &amp; Tracings Analyst&quot;, IDNSC_Parent__r.Role_Rebates_Tracings_Analyst__r.Email ,
&quot;Transportation Analyst&quot;, IDNSC_Parent__r.Role_Transportation_Analyst__r.Email ,
&quot;fixemail@covidien.com&quot;)</formula>
        <name>Set Task Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Task_Start_Date</fullName>
        <description>Set the Start_Date_Actual__c field to Today()</description>
        <field>Start_Date_Actual__c</field>
        <formula>Today()</formula>
        <name>Set Task Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Owner Email Address</fullName>
        <actions>
            <name>Set_Task_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Task Assigned Date</fullName>
        <actions>
            <name>Set_IDNSC_Task_Assigned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Task_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDNSC_Task__c.Runs_in_State__c</field>
            <operation>equals</operation>
            <value>Preparation Activity</value>
        </criteriaItems>
        <description>The &apos;SetTaskAssignedDate&apos; trigger does this for all tasks but the first set because the first set have not been created yet when the trigger runs on the parent relationship record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
