<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DIS_UpdateVerifiedBy</fullName>
        <field>Verification_By__c</field>
        <formula>LEFT(LastModifiedBy.FirstName &amp; &quot; &quot; &amp;  LastModifiedBy.LastName, 50)</formula>
        <name>DIS-UpdateVerifiedBy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DIS_UpdateVerifiedDate</fullName>
        <field>Verification_Date__c</field>
        <formula>TODAY()</formula>
        <name>DIS-UpdateVerifiedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Error_in_Salesout</fullName>
        <field>Error_Messages__c</field>
        <formula>Error_Messages__c &amp; &quot; UOM Invalid &quot;</formula>
        <name>Update Error in Salesout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Error_Message</fullName>
        <description>Update Error message in Salesout with product UOM information</description>
        <field>Error_Messages__c</field>
        <formula>Error_Messages__c &amp; &quot;Product UOM is incorrect&quot;</formula>
        <name>update Error Message</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>DIS-Check UOM for Salesout</fullName>
        <active>false</active>
        <formula>and (( trim(UOM__c) &lt;&gt;   trim(Product_Uom__c ) ), (trim(UOM__c) &lt;&gt;    trim(Product_Selling_UOM__c)  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Update Error Message</fullName>
        <actions>
            <name>update_Error_Message</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Error message if the UOM measure is incorrect</description>
        <formula>and(( UOM__c &lt;&gt;  Product_Code1__r.Selling_UOM__c ) , (UOM__c &lt;&gt;  Product_Code1__r.UOM__c ),not(ISBLANK(Product_Code1__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DIS-UpdateVerifiedBy</fullName>
        <actions>
            <name>DIS_UpdateVerifiedBy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DIS_UpdateVerifiedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Out__c.Verification_Status__c</field>
            <operation>equals</operation>
            <value>VERIFIED</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
