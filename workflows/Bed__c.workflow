<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ILS_Update_Bed_Name_field</fullName>
        <field>Bed_Name__c</field>
        <formula>Account__r.SAP_Account_Number__c  &amp; &quot;_&quot; &amp;  Bed_Types__r.Name  &amp; &quot;_&quot;  &amp;  Bed_Name__c</formula>
        <name>ILS Update Bed Name field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BedName</fullName>
        <field>Name</field>
        <formula>Account__r.SAP_Account_Number__c  &amp; &quot;_&quot; &amp;  Bed_Types__r.Name  &amp; &quot;_&quot;  &amp;   Name</formula>
        <name>Update BedName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ILS Update Bed Name</fullName>
        <actions>
            <name>ILS_Update_Bed_Name_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_BedName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>the Bed Name field and unique record key shall be updated by including the SAP Account Number and Bed Type in addition to the information entered by the user.</description>
        <formula>NOT( CONTAINS( Name  &amp; &quot; &quot;,  Account__r.SAP_Account_Number__c  &amp; &quot;_&quot; +  Bed_Types__r.Name  &amp; &quot;_&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
