<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EMS_Copy_CostElement_on_SampleCCI_Expens</fullName>
        <field>SKU_Unit_Price__c</field>
        <formula>Cost_Element__r.Unit_Price__c</formula>
        <name>EMS Copy CostElement on SampleCCI Expens</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Copy_Product_SKU_on_SampleCCI_Expens</fullName>
        <field>SKU_Unit_Price__c</field>
        <formula>Product_SKU__r.EMS_SKU_Price__c</formula>
        <name>EMS Copy Product SKU on SampleCCI Expens</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Cost_Element_Amount_update</fullName>
        <field>Amount__c</field>
        <formula>SKU_Unit_Price__c * Quantity__c</formula>
        <name>EMS Cost Element Amount update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Product_SKU_Amount_update</fullName>
        <field>Amount__c</field>
        <formula>Quantity__c * SKU_Unit_Price__c</formula>
        <name>EMS Product SKU  Amount update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Amount_for_CCI_Samples</fullName>
        <field>Amount__c</field>
        <formula>Quantity__c *  SKU_Unit_Price__c</formula>
        <name>EMS Update Amount for CCI Samples</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EMS CostElement  Amount update</fullName>
        <actions>
            <name>EMS_Cost_Element_Amount_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Cost_Element__c!=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Product SKU  Amount update</fullName>
        <actions>
            <name>EMS_Product_SKU_Amount_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Product_SKU__c!=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Sample CCI Expense Amount update</fullName>
        <actions>
            <name>EMS_Update_Amount_for_CCI_Samples</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Cost_Element__c!=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Copy Amount from CostElement</fullName>
        <actions>
            <name>EMS_Copy_CostElement_on_SampleCCI_Expens</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Cost_Element__c!=null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Copy Amount from Product SKU</fullName>
        <actions>
            <name>EMS_Copy_Product_SKU_on_SampleCCI_Expens</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Product_SKU__c !=null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
