<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Idea_Submission_Confirmation</fullName>
        <ccEmails>MITG.iD8@medtronic.com</ccEmails>
        <description>Idea Submission Confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>corporate.initiatives@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Covidien_Email_Templates/Ideation_Template</template>
    </alerts>
    <rules>
        <fullName>Ideation Send Confirmation Email</fullName>
        <actions>
            <name>Idea_Submission_Confirmation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea_Submission__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>Send email to the Idea submitter both letting them know it was received and thanking them.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
