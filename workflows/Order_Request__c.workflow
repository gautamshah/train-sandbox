<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Order_Request_Bill_To_Address</fullName>
        <field>Bill_To_Address__c</field>
        <formula>Bill_To__r.Address_1__c  &amp; &quot; &quot; &amp;
Bill_To__r.Address_2__c  &amp; &quot; &quot; &amp;
Bill_To__r.Address_3__c  &amp; &quot; &quot; &amp;
Bill_To__r.Address_4__c  &amp; BR() &amp;
Bill_To__r.City__c &amp; &quot; &quot; &amp;
Bill_To__r.State_Region__c  &amp; BR() &amp;
Bill_To__r.Country__c &amp; &quot; &quot; &amp; Bill_To__r.Zip_Postal_Code__c</formula>
        <name>Update Order Request Bill-To Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Request_Ship_To_Address</fullName>
        <field>Ship_To_Address__c</field>
        <formula>Ship_To__r.Address_1__c &amp; &quot; &quot; &amp;
Ship_To__r.Address_2__c &amp; &quot; &quot; &amp;
Ship_To__r.Address_3__c &amp; &quot; &quot; &amp;
Ship_To__r.Address_4__c &amp; BR() &amp;
Ship_To__r.City__c &amp; &quot; &quot; &amp;
Ship_To__r.State_Region__c &amp; BR() &amp;
Ship_To__r.Country__c &amp; &quot; &quot; &amp; Ship_To__r.Zip_Postal_Code__c</formula>
        <name>Update Order Request Ship-To Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DIS-Update Order Request Bill-To Address</fullName>
        <actions>
            <name>Update_Order_Request_Bill_To_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Bill To Address based on the distributor ERP bill to address</description>
        <formula>ISCHANGED( Bill_To__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Update Order Request Ship-To Addresses</fullName>
        <actions>
            <name>Update_Order_Request_Ship_To_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Ship To Address based on the distributor ERP ship to address</description>
        <formula>ISCHANGED( Ship_To__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
