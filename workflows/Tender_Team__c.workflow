<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_tender_link_to_new_tender_team_members</fullName>
        <description>Email tender link to new tender team members</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/sendtenderlink</template>
    </alerts>
    <rules>
        <fullName>Emailtenderlinktonewmember</fullName>
        <actions>
            <name>Email_tender_link_to_new_tender_team_members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR (ISNEW(),   AND( ISCHANGED( Team_Member__c ), NOT(ISNULL(Team_Member__c)) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
