<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EMS_ANZ_AUS_TRAVEL</fullName>
        <ccEmails>aust.travel.pace@covidien.com</ccEmails>
        <description>EMS ANZ AUS TRAVEL</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_ANZ_Travel_Team_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMS_ANZ_Minor_SponsorShip_Agreement_Upload_Notification</fullName>
        <description>EMS ANZ Minor SponsorShip Agreement Upload Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Name_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_ANZ_Minor_Sponsorship_Agreement</template>
    </alerts>
    <alerts>
        <fullName>EMS_ANZ_Samples_Notification_Email</fullName>
        <ccEmails>aust.customer.service@covidien.com</ccEmails>
        <description>EMS ANZ Samples Notification Email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_ANZ_Samples_Team_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMS_Any_HCP_from_outside_ANZ</fullName>
        <ccEmails>christopher.smith53@medtronic.com;</ccEmails>
        <ccEmails>alina.angheluta@medtronic.com</ccEmails>
        <description>EMS Any HCP from outside ANZ</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Any_HCP_from_Outside_ANZ</template>
    </alerts>
    <alerts>
        <fullName>EMS_Any_HCP_from_outside_Asia</fullName>
        <ccEmails>Asia.GCCCoordinator@covidien.com;</ccEmails>
        <ccEmails>yann.shuang.lee@medtronic.com</ccEmails>
        <description>EMS Any HCP from outside Asia</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Any_HCP_from_Outside_Asia</template>
    </alerts>
    <alerts>
        <fullName>EMS_CPA_Coordinator_Reminder</fullName>
        <description>EMS CPA Coordinator Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>CCP_Coordinator_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CCP_Coordinator_2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_CPA_Reminder_Template</template>
    </alerts>
    <alerts>
        <fullName>EMS_NZ_AUS_TRAVEL</fullName>
        <ccEmails>madeline.hart@medtronic.com</ccEmails>
        <description>EMS NZ AUS TRAVEL</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_ANZ_Travel_Team_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMS_Notification_to_Legal_on_Notify_legal_Button</fullName>
        <ccEmails>Asia.GCCCoordinator@covidien.com;</ccEmails>
        <ccEmails>yann.shuang.lee@medtronic.com;</ccEmails>
        <ccEmails>emily.tan@medtronic.com</ccEmails>
        <description>EMS Notification to Legal on Notify legal Button</description>
        <protected>false</protected>
        <recipients>
            <field>CCP_Coordinator_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CCP_Coordinator_2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Notification_to_Legal_on_Notify_Legal_Button</template>
    </alerts>
    <alerts>
        <fullName>EMS_Notification_to_Requestor_on_Legal_Document_Upload</fullName>
        <description>EMS Notification to Requestor on Legal Document Upload</description>
        <protected>false</protected>
        <recipients>
            <field>CCP_Coordinator_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor_Name_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Notification_to_Requestor_when_Legal_uploads_Document</template>
    </alerts>
    <alerts>
        <fullName>EMS_Notify_Decline_eCPA</fullName>
        <description>EMS Notify Decline eCPA</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Owner_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Manager1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor_Name_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Notification_for_Decline</template>
    </alerts>
    <alerts>
        <fullName>EMS_Notify_Event_Owner_Requestor_and_Manager</fullName>
        <description>EMS Notify Event Owner,Requestor and Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Owner_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Manager1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor_Name_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Notification_to_Event_Owner_Requestor</template>
    </alerts>
    <alerts>
        <fullName>EMS_Notify_GCC_Coordinator_on_GCC_Approval_request</fullName>
        <ccEmails>Asia.GCCCoordinator@covidien.com</ccEmails>
        <description>EMS Notify GCC Coordinator on GCC Approval request</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Notification_to_Asia_GCC_on_GCC_Approval</template>
    </alerts>
    <alerts>
        <fullName>EMS_Notify_Requestor_on_eCPA_Submission</fullName>
        <description>EMS Notify Requestor on eCPA Submission</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Name_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Notification_for_requestor_on_submission</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_Email_to_ANZ_GCC_for_Speaker</fullName>
        <ccEmails>christopher.smith53@medtronic.com;</ccEmails>
        <ccEmails>alina.angheluta@medtronic.com</ccEmails>
        <description>EMS Send Email to ANZ GCC for Speaker</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Speaker_from_outside_ANZ</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_Email_to_Asia_GCC_for_CN_KR_KOLs_GCPs</fullName>
        <ccEmails>Asia.GCCCoordinator@covidien.com;</ccEmails>
        <ccEmails>yann.shuang.lee@medtronic.com</ccEmails>
        <description>EMS Send Email to Asia GCC for CN/KR KOLs GCPs</description>
        <protected>false</protected>
        <recipients>
            <field>GCC_Finance_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>GCC_Legal_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>GCC_Medical_Affairs_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>GCC_PACE_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_KOL_HCP_from_China</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_Email_to_Asia_GCC_for_Speaker</fullName>
        <ccEmails>Asia.GCCCoordinator@covidien.com;</ccEmails>
        <ccEmails>yann.shuang.lee@medtronic.com</ccEmails>
        <description>EMS Send Email to Asia GCC for Speaker</description>
        <protected>false</protected>
        <recipients>
            <field>GCC_Finance_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>GCC_Legal_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>GCC_Medical_Affairs_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>GCC_PACE_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EMS_Speaker_from_outside_Asia</template>
    </alerts>
    <fieldUpdates>
        <fullName>EMS_ANZ_Update_CPA_Ref_No</fullName>
        <field>CPA_Ref__c</field>
        <formula>&apos;eCPA&apos;+&apos;-&apos;+ Owner_Country__c+&apos;-&apos;+ Financial_Year__c+&apos;-&apos;+ Auto_Number__c +&apos;-&apos;+&apos;R&apos;+TEXT(No_of_Approved_eCPAs__c+1)</formula>
        <name>EMS ANZ Update CPA Ref No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_ARO_Clinical_Affairs_approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending CA Director Approval</literalValue>
        <name>EMS ARO Clinical Affairs approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_ARO_Comms_PR</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Corporate Communication Approval</literalValue>
        <name>EMS ARO Comms &amp; PR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_ARO_Comms_PR_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Corporate Communication Approval</literalValue>
        <name>EMS_ARO_Comms &amp; PR Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_ARO_Marketing_Head</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending GBU Marketing Head Approval</literalValue>
        <name>EMS ARO Marketing Head</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CCP_Coordinator_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Sales Country Controller Approval</literalValue>
        <name>EMS CCP Coordinator Approval Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CCP_Coordinator_Approval_Marketing</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Marketing Country Controller Approval</literalValue>
        <name>EMS CCP Coordinator Approval Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Approved_Date_Today</fullName>
        <field>CPA_Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>EMS CPA Approved Date=Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Approved_flag_true</fullName>
        <field>CPA_Approved__c</field>
        <literalValue>1</literalValue>
        <name>EMS CPA Approved flag=true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Declined_by_Country_Manager</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Country Manager</literalValue>
        <name>EMS CPA Declined by Country Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Pending_Country_Manager</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Country Manager</literalValue>
        <name>EMS CPA Pending Country Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_RF_approval_Staus</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Completed</literalValue>
        <name>EMS CPA RF approval Staus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Request_Date_Today</fullName>
        <field>CPA_Request_Date__c</field>
        <formula>NOW()</formula>
        <name>EMS CPA Request Date=Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Request_Date_to_Blank</fullName>
        <field>CPA_Request_Date__c</field>
        <name>EMS CPA Request Date to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Status_Cancelled</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Cancelled</literalValue>
        <name>EMS CPA Status Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Status_Recalled</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>EMS CPA Status Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Status_to_Pending_CCP_Coordin</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending CCP Coordinator Approval</literalValue>
        <name>EMS CPA Status to Pending CCP Coordin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Submitted_to_False</fullName>
        <field>CPA_Submitted__c</field>
        <literalValue>0</literalValue>
        <name>EMS CPA Submitted to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_Submitted_true</fullName>
        <field>CPA_Submitted__c</field>
        <literalValue>1</literalValue>
        <name>EMS CPA Submitted=true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_CPA_status_Pending_New_Request</fullName>
        <field>CPA_Status__c</field>
        <literalValue>New Request</literalValue>
        <name>EMS CPA status Pending New Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Clear_CPA_Approved_Date</fullName>
        <field>CPA_Approved_Date__c</field>
        <name>EMS Clear CPA Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Clear_CPA_Approved_Flag</fullName>
        <field>CPA_Approved__c</field>
        <literalValue>0</literalValue>
        <name>EMS Clear CPA Approved Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Clear_Notify_Special_EMails</fullName>
        <field>Notify_Special_EMails__c</field>
        <literalValue>0</literalValue>
        <name>EMS Clear Notify Special EMails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Clear_Speaker_query</fullName>
        <field>Any_speaker_consultant_from_outside_Asia__c</field>
        <name>EMS Clear Speaker query</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_ARO_Govt_Affairs</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by ARO Govt Affairs</literalValue>
        <name>EMS Declined by ARO Govt Affairs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Area_Business_Head</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Area Business Head</literalValue>
        <name>EMS Declined by Area Business Head</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_CA_Director</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by CA Director</literalValue>
        <name>EMS Declined by CA Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_CCP_Coordinator</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by CCP Coordinator</literalValue>
        <name>EMS Declined by CCP Coordinator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Corporate_Communication</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Corporate Communication</literalValue>
        <name>EMS Declined by Corporate Communication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Direct_Supervisor</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Direct Supervisor</literalValue>
        <name>EMS Declined by Direct Supervisor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Event_Owner</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Event Owner</literalValue>
        <name>EMS Declined by Event Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Finance_Director</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by  Finance Director</literalValue>
        <name>EMS Declined by Finance Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_GBU_Marketing_Head</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by GBU Marketing Head</literalValue>
        <name>EMS Declined by GBU Marketing Head</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_GCC</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by GCC</literalValue>
        <name>EMS Declined by GCC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_In_Country_PACE_Manager</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by In-Country PACE Manager</literalValue>
        <name>EMS Declined by In-Country PACE Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Managing_Director</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Managing Director</literalValue>
        <name>EMS Declined by Managing Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Marketing_Country_Contro</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Marketing Country Controller Approval</literalValue>
        <name>EMS Declined by Marketing Country Contro</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Medical_Affairs</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Medical Affairs</literalValue>
        <name>EMS Declined by Medical Affairs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Operational_Director</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Operational Director</literalValue>
        <name>EMS Declined by Operational Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_PACE_Head</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by PACE HEAD</literalValue>
        <name>EMS Declined by PACE Head</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Declined_by_Sales_Country_Controller</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Sales Country Controller</literalValue>
        <name>EMS Declined by Sales Country Controller</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Air_Travel_flag</fullName>
        <field>Track_Air_Travel__c</field>
        <literalValue>0</literalValue>
        <name>EMS Deselect Air Travel flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Consultancy_Fee</fullName>
        <field>Track_Consultancy_Fees__c</field>
        <literalValue>0</literalValue>
        <name>EMS Deselect Consultancy Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Gifts_Flag</fullName>
        <field>Track_Gifts__c</field>
        <literalValue>0</literalValue>
        <name>EMS Deselect Gifts Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Lodging_Flag</fullName>
        <field>Track_Lodging__c</field>
        <literalValue>0</literalValue>
        <name>EMS Deselect Lodging Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Meal_Flag</fullName>
        <field>Track_Meal_Flag__c</field>
        <literalValue>0</literalValue>
        <name>EMS_Deselect Track  Meal Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Others_flag</fullName>
        <field>Track_Others__c</field>
        <literalValue>0</literalValue>
        <name>EMS Deselect Others flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Deselect_Track_LTFlag</fullName>
        <field>Track_LT_Flag__c</field>
        <literalValue>0</literalValue>
        <name>EMS_Deselect Track LTFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Direct_Supervisor_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Direct Supervisor Approval</literalValue>
        <name>EMS Direct Supervisor Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Event_Owner_Declined</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Event Owner</literalValue>
        <name>EMS Event Owner Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Event_Status_Active</fullName>
        <field>Event_Status__c</field>
        <literalValue>Active</literalValue>
        <name>EMS Event Status Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Event_Status_Budgeted</fullName>
        <field>Event_Status__c</field>
        <literalValue>Budget</literalValue>
        <name>EMS Event Status Budgeted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Event_Status_Submitted_for_approval</fullName>
        <field>Event_Status__c</field>
        <literalValue>Submitted for eCPA Approval</literalValue>
        <name>EMS Event Status Submitted for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Event_Update_Ev_Owner_Approval_Date</fullName>
        <field>Event_Owner_Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>EMS Event Update Ev Owner Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_GCC_5k_GCC_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending GCC Approval...</literalValue>
        <name>EMS GCC&gt;5k GCC Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_GCC_Legal_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending GCC Approval..</literalValue>
        <name>EMS GCC Legal Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_GCC_PACE_status</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending GCC Approval....</literalValue>
        <name>EMS GCC PACE status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_GCC_R_D_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending GCC Approval....</literalValue>
        <name>EMS GCC R&amp;D Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Korea_Event_Manager_Certification</fullName>
        <field>Manager_Certified_By__c</field>
        <formula>$User.Username</formula>
        <name>EMS Korea Event Manager Certification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Mediacal_Affairs_approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Medical Affairs Approval</literalValue>
        <name>EMS Mediacal Affairs approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Pending_Finance_Director_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Finance Director Approval</literalValue>
        <name>EMS Pending Finance Director Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Pending_Govt_Affairs_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending ARO Government Affairs Approval</literalValue>
        <name>EMS Pending ARO Govt Affairs Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Pending_Managing_Director_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Managing Director Approval</literalValue>
        <name>EMS Pending Managing Director Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Pending_Operactional_Director_ANZ</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Operational Director Approval</literalValue>
        <name>EMS Pending Operational Director ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Pending_PACE_Head_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending PACE HEAD Approval</literalValue>
        <name>EMS Pending PACE Head Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Regional_PACE_Status</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending In-Country PACE Manager Approval</literalValue>
        <name>EMS Regional PACE Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Rejection_CCP_Coordinator</fullName>
        <field>CPA_Status__c</field>
        <name>EMS Rejection CCP Coordinator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_Calcelled_Rejected_flag_to_false</fullName>
        <field>Rejected_Recalled_flag__c</field>
        <literalValue>0</literalValue>
        <name>EMS Set Calcelled/Rejected flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_Calcelled_Rejected_flag_to_true</fullName>
        <field>Rejected_Recalled_flag__c</field>
        <literalValue>1</literalValue>
        <name>EMS Set Calcelled/Rejected flag to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_Meal_Change_Flag</fullName>
        <field>Track_Meal_Flag__c</field>
        <literalValue>1</literalValue>
        <name>EMS_Set Meal Change Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_track_AT_change_flag</fullName>
        <field>Track_Air_Travel__c</field>
        <literalValue>1</literalValue>
        <name>EMS Set track AT change flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_track_Gifts_change_flag</fullName>
        <field>Track_Gifts__c</field>
        <literalValue>1</literalValue>
        <name>EMS Set track Gifts change flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_track_Others_change_flag</fullName>
        <field>Track_Others__c</field>
        <literalValue>1</literalValue>
        <name>EMS Set track Others change flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Set_track_lodging_change_flag</fullName>
        <field>Track_Lodging__c</field>
        <literalValue>1</literalValue>
        <name>EMS Set track lodging change flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Track_Consultacy_Change_flag</fullName>
        <field>Track_Consultancy_Fees__c</field>
        <literalValue>1</literalValue>
        <name>EMS Track Consultacy Change flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Track_LTcChange</fullName>
        <field>Track_LT_Flag__c</field>
        <literalValue>1</literalValue>
        <name>EMS_Track LTcChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Manager_KR_Certification_Date</fullName>
        <field>Manager_Certification_Date__c</field>
        <formula>NOW()</formula>
        <name>EMS Update Manager KR Certification Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Owner_Country_for_Sharing_Rul</fullName>
        <field>Owner_Country_Sharing_Rule__c</field>
        <formula>Event_Owner_User__r.Country</formula>
        <name>EMS Update Owner Country for Sharing Rul</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_eCPA_Submitted_to_Manager</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Manager Approval</literalValue>
        <name>EMS eCPA Submitted to Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_eCPA_declined_by_Manager</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Declined by Manager</literalValue>
        <name>EMS eCPA declined by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_g10K_Regional_Business_Head_Approval</fullName>
        <field>CPA_Status__c</field>
        <literalValue>Pending Area Business Head Approval</literalValue>
        <name>EMS_g10K_Regional Business Head Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_no_of_Completed_eCPAs</fullName>
        <field>No_of_Completed_eCPAs__c</field>
        <formula>No_of_Completed_eCPAs__c +1</formula>
        <name>EMS no of Completed eCPAs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_no_of_eCPAs_on_Event_increment</fullName>
        <field>No_of_Approved_eCPAs__c</field>
        <formula>No_of_Approved_eCPAs__c + 1</formula>
        <name>EMS_no of eCPAs on Event increment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EMS ANZ Samples Notification</fullName>
        <actions>
            <name>EMS_ANZ_Samples_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(Owner_Country__c ==&apos;AU&apos;,Owner_Country__c ==&apos;NZ&apos;),NOT(ISPICKVAL(PRIORVALUE(CPA_Status__c),&apos;Completed&apos;)), ISPICKVAL(CPA_Status__c,&apos;Completed&apos;),OR( Others_SKU_Sample_NCE_Amount__c &gt;0 , Others_SKU_Sample_Covidien_Event_Amount__c&gt;0))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS ANZ Travel Team Notification</fullName>
        <actions>
            <name>EMS_ANZ_AUS_TRAVEL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(Owner_Country__c=&apos;AU&apos;,Owner_Country__c=&apos;NZ&apos;), NOT(ISPICKVAL(PRIORVALUE(CPA_Status__c),&apos;Completed&apos;)), ISPICKVAL(CPA_Status__c,&apos;Completed&apos;), OR(Total_Air_Travel__c&gt; 0.0,Total_Local_GTA__c&gt;0.0,Total_LodA__c&gt;0.0) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS AU Travel Team Notification</fullName>
        <actions>
            <name>EMS_ANZ_AUS_TRAVEL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Owner_Country__c=&apos;AU&apos;, NOT(ISPICKVAL(PRIORVALUE(CPA_Status__c),&apos;Completed&apos;)), ISPICKVAL(CPA_Status__c,&apos;Completed&apos;), OR(Total_Air_Travel__c&gt; 0.0,Total_Local_GTA__c&gt;0.0,Total_LodA__c&gt;0.0) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Clear Speaker query</fullName>
        <actions>
            <name>EMS_Clear_Speaker_query</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Speaker_or_other_consulting_services__c ==false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Event Status Cancelled</fullName>
        <actions>
            <name>EMS_CPA_Status_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EMS_Event__c.Event_Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS HCP from outside ANZ</fullName>
        <actions>
            <name>EMS_Any_HCP_from_outside_ANZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(CPA_Status__c),&quot;New Request&quot;), ISPICKVAL(Any_HCP_from_outside_of_ANZ__c,&quot;YES&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS HCP from outside ASIA</fullName>
        <actions>
            <name>EMS_Any_HCP_from_outside_Asia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(CPA_Status__c),&quot;New Request&quot;), ISPICKVAL(Any_HCP_Invitees_from_Outside_Asia__c,&quot;YES&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS KR Event Manager Certification</fullName>
        <actions>
            <name>EMS_Korea_Event_Manager_Certification</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Update_Manager_KR_Certification_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Owner_Country__c ==&apos;KR&apos;,ISCHANGED( Manager_Certified_only_for_Korea_eCPAs__c),Manager_Certified_only_for_Korea_eCPAs__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS NZ Travel Team Notification</fullName>
        <actions>
            <name>EMS_NZ_AUS_TRAVEL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Owner_Country__c=&apos;NZ&apos;, NOT(ISPICKVAL(PRIORVALUE(CPA_Status__c),&apos;Completed&apos;)), ISPICKVAL(CPA_Status__c,&apos;Completed&apos;), OR(Total_Air_Travel__c&gt; 0.0,Total_Local_GTA__c&gt;0.0,Total_LodA__c&gt;0.0) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Notify CN%2FKR KOL%2FHCPs</fullName>
        <actions>
            <name>EMS_Send_Email_to_Asia_GCC_for_CN_KR_KOLs_GCPs</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(CPA_Status__c),&quot;New Request&quot;),ISPICKVAL(Any_recipient_s_who_are_KOL_s_HCP_s__c,&quot;YES&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Notify Speaker from outside ANZ</fullName>
        <actions>
            <name>EMS_Send_Email_to_ANZ_GCC_for_Speaker</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(CPA_Status__c),&quot;New Request&quot;), Speaker_or_other_consulting_services__c , ISPICKVAL( Any_speaker_consultant_from_outside_ANZ__c,&quot;YES&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Notify Speaker from outside Asia</fullName>
        <actions>
            <name>EMS_Send_Email_to_Asia_GCC_for_Speaker</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(PRIORVALUE(CPA_Status__c),&quot;New Request&quot;), Speaker_or_other_consulting_services__c , ISPICKVAL(Any_speaker_consultant_from_outside_Asia__c,&quot;YES&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Send mail to Legal on Notify Legal Button</fullName>
        <actions>
            <name>EMS_Notification_to_Legal_on_Notify_legal_Button</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Owner_Country__c==&apos;MY&apos;, Owner_Country__c==&apos;SG&apos;), ISPICKVAL(PRIORVALUE(CPA_Status__c),&quot;Pending CCP Coordinator Approval&quot;), NOT(     OR(ISPICKVAL(CPA_Status__c,&quot;Declined by CCP Coordinator&quot;),ISPICKVAL(CPA_Status__c,&quot;Cancelled&quot;),ISPICKVAL(CPA_Status__c,&quot;Recalled&quot;))   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Send mail to requestor on Legal Upload</fullName>
        <actions>
            <name>EMS_Notification_to_Requestor_on_Legal_Document_Upload</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()),ISCHANGED(Track_count_of_legal_documents__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Update Owner Country for Sharing Rule</fullName>
        <actions>
            <name>EMS_Update_Owner_Country_for_Sharing_Rul</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Deselect all tracking Flag</fullName>
        <actions>
            <name>EMS_Deselect_Air_Travel_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Deselect_Consultancy_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Deselect_Gifts_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Deselect_Lodging_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Deselect_Meal_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Deselect_Others_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMS_Deselect_Track_LTFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE(No_of_Completed_eCPAs__c)!=No_of_Completed_eCPAs__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Reminder Mail on CPA Coordinator</fullName>
        <actions>
            <name>EMS_CPA_Coordinator_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(TODAY()-DATEVALUE(CPA_Request_Date__c)==2,ISPICKVAL(CPA_Status__c,&apos;Pending CCP Coordinator Approval&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track AT Change</fullName>
        <actions>
            <name>EMS_Set_track_AT_change_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_Air_Travel_Count__c)!=Total_Air_Travel_Count__c),((PRIORVALUE(Total_Air_Travel__c)!=Total_Air_Travel__c))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)), No_of_Approved_eCPAs__c&gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track Consultancy  Change</fullName>
        <actions>
            <name>EMS_Track_Consultacy_Change_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_Consultancy_Count__c)!=Total_Consultancy_Count__c),((PRIORVALUE(Total_Consultancy_Fee__c )!=Total_Consultancy_Fee__c ))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)),  No_of_Completed_eCPAs__c &gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track Gifts Change</fullName>
        <actions>
            <name>EMS_Set_track_Gifts_change_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_Gifts_Count__c)!=Total_Gifts_Count__c),((PRIORVALUE(Total_Gifts_amount__c )!=Total_Gifts_amount__c ))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)),  No_of_Completed_eCPAs__c &gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track LT Change</fullName>
        <actions>
            <name>EMS_Track_LTcChange</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_LGT_Count__c)!=Total_LGT_Count__c),((PRIORVALUE( Total_Local_GTA__c)!=Total_Local_GTA__c))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)), No_of_Completed_eCPAs__c &gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track Lodging Change</fullName>
        <actions>
            <name>EMS_Set_track_lodging_change_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_Lodging_Count__c)!=Total_Lodging_Count__c),((PRIORVALUE(Total_LodA__c)!=Total_LodA__c))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)), No_of_Completed_eCPAs__c &gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track Meal Change</fullName>
        <actions>
            <name>EMS_Set_Meal_Change_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_Meal_Count__c)!=Total_Meal_Count__c),((PRIORVALUE(  Total_Meal_Amount__c )!=Total_Meal_Amount__c ))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)), No_of_Completed_eCPAs__c &gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_Track Others Change</fullName>
        <actions>
            <name>EMS_Set_track_Others_change_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(OR((PRIORVALUE(Total_Others_Count__c)!=Total_Others_Count__c),((PRIORVALUE(Total_Others_Amount__c )!=Total_Others_Amount__c ))),(ISPICKVAL(CPA_Status__c,&apos;Completed&apos;)), No_of_Completed_eCPAs__c &gt;=1),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS_no of eCPAs on Event on Cancelled</fullName>
        <actions>
            <name>EMS_no_of_eCPAs_on_Event_increment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EMS_Event__c.Event_Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
