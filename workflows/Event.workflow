<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Event_Ended</fullName>
        <description>Mark Event Ended field as true/checked.</description>
        <field>Event_Ended__c</field>
        <literalValue>1</literalValue>
        <name>Update Event Ended</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Event End Passed</fullName>
        <active>false</active>
        <description>An event&apos;s End has passed.</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Event_Ended</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Event.EndDateTime</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
