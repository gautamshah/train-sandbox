<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CN_Send_email_notification_to_owner</fullName>
        <description>CN Send email notification to owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_New_Demo_Product_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>KR_Demo_Product_Current_Location_Updated</fullName>
        <description>KR Demo Product - Current Location Updated</description>
        <protected>false</protected>
        <recipients>
            <recipient>iris.joo@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>teng.hoon.tan@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_Demo_Product_Current_Location_Updated</template>
    </alerts>
    <alerts>
        <fullName>New_Demo_Loan_Request_to_CS</fullName>
        <description>New Demo/Loan Request to CS</description>
        <protected>false</protected>
        <recipients>
            <recipient>kemal.tiner@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMEA_EM_Templates/Demo_Loan_Delivery_Form</template>
    </alerts>
    <alerts>
        <fullName>US_Demo_Product_Owner_Change</fullName>
        <ccEmails>aileen.a.mcgarry@medtronic.com</ccEmails>
        <description>US Demo Product Owner Change</description>
        <protected>false</protected>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All/Demo_Product_Owner_Change</template>
    </alerts>
    <alerts>
        <fullName>US_Demo_Product_Transfer_Form_Required</fullName>
        <description>US Demo Product Transfer Form Required</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All/US_Demo_Product_Transfer_Form_Required</template>
    </alerts>
    <fieldUpdates>
        <fullName>Customer_Service_Request_Form_Sent</fullName>
        <field>Customer_Service_Request_Form_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Customer Service Request Form Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Physical_Inspection</fullName>
        <field>Physically_Inspected__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Physical Inspection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <description>Update Approval Status to Submitted for Approval</description>
        <field>Approval_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Code_in_Demo_Product</fullName>
        <field>Product_Code__c</field>
        <formula>Asset_Name__r.Name</formula>
        <name>Update Product Code in Demo Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Device Physical Inspection</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Demo_Product__c.Physically_Inspected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Uncheck_Physical_Inspection</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>KR Demo Product - Current Location Updated</fullName>
        <actions>
            <name>KR_Demo_Product_Current_Location_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to Sales Admin when the current location is changed</description>
        <formula>AND( ISCHANGED(Current_Location__c), ISPICKVAL(Country__c,&apos;KR&apos;), Not(ISPICKVAL($User.User_Role__c ,&apos;Other&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TR New Demo Product Notification to CS</fullName>
        <actions>
            <name>New_Demo_Loan_Request_to_CS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer_Service_Request_Form_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>TR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.Serial_No__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.of_Movement_Request__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Product__c.Customer_Service_Request_Form_Sent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US Owner Changed</fullName>
        <actions>
            <name>US_Demo_Product_Owner_Change</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>US_Demo_Product_Transfer_Form_Required</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>If owner changes inform person making the change to fill in a transfer form and email to Aileen McGarry
Send notification of change to Aileen McGarry</description>
        <formula>AND(   ISPICKVAL(Country__c,&apos;US&apos;),     ISCHANGED(OwnerId)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Product Code Field</fullName>
        <actions>
            <name>Update_Product_Code_in_Demo_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Demo_Product__c.Country__c</field>
            <operation>equals</operation>
            <value>SG</value>
        </criteriaItems>
        <description>To update Product Code field to be searchable by user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
