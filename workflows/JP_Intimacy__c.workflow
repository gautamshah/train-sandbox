<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JP_Update_Field_Contact_SR</fullName>
        <field>JP_Contact_SR__c</field>
        <formula>CASESAFEID( JP_Contacts__c )  &amp; &quot;|&quot; &amp;  CASESAFEID( JP_SR__c )</formula>
        <name>JP Update Field Contact_SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JP Intimacy%3A Update Contact_SR</fullName>
        <actions>
            <name>JP_Update_Field_Contact_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update JP_Contact_SR when Intimacy record is inserted or SR is changed.</description>
        <formula>OR( ISNEW() ,  ISCHANGED(  JP_SR__c  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
