<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Adjusted_Price_Service_Charge_Amt</fullName>
        <field>Apttus_Config2__AdjustedPrice__c</field>
        <formula>ServiceChargeAmount__c</formula>
        <name>Adjusted Price = Service Charge Amt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Apttus_Config2__LineItemId__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Base_Extended_Price_Service_Charge_Amt</fullName>
        <field>Apttus_Config2__BaseExtendedPrice__c</field>
        <formula>ServiceChargeAmount__c</formula>
        <name>Base Extended Price = Service Charge Amt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Apttus_Config2__LineItemId__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Base_Price_Service_Charge_Amt</fullName>
        <field>Apttus_Config2__BasePrice__c</field>
        <formula>ServiceChargeAmount__c</formula>
        <name>Base Price = Service Charge Amt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Apttus_Config2__LineItemId__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Extended_Price_Service_Charge_Amt</fullName>
        <field>Apttus_Config2__ExtendedPrice__c</field>
        <formula>ServiceChargeAmount__c</formula>
        <name>Extended Price = Service Charge Amt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Apttus_Config2__LineItemId__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Net_Price_Service_Charge_Amt</fullName>
        <field>Apttus_Config2__NetPrice__c</field>
        <formula>ServiceChargeAmount__c</formula>
        <name>Net Price = Service Charge Amt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Apttus_Config2__LineItemId__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update SPQ Product Price</fullName>
        <actions>
            <name>Adjusted_Price_Service_Charge_Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Base_Extended_Price_Service_Charge_Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Base_Price_Service_Charge_Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Extended_Price_Service_Charge_Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Net_Price_Service_Charge_Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ServiceChargeAmount__c != PRIORVALUE(ServiceChargeAmount__c), Apttus_Config2__LineItemId__r.Apttus_Config2__ProductId__r.ProductCode == &apos;SPQ&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
