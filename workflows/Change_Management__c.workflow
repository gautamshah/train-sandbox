<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Project_Notification</fullName>
        <description>New Project Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Quality_Assurance_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_New_Project_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Project_Owner</fullName>
        <description>Set the Project Owner to one of members of the Quality AssuranceTeam</description>
        <field>OwnerId</field>
        <lookupValue>judith.randall@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Project Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_owner</fullName>
        <description>set owner back to BA team</description>
        <field>OwnerId</field>
        <lookupValue>elena.frigeri@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>set owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Project Notification when Status equals Quality Assurance</fullName>
        <actions>
            <name>New_Project_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Project_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Change_Management__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Quality Assurance</value>
        </criteriaItems>
        <description>Notify the Quality Assurance team of a new project ready for testing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project Close Notification when Status equals Transition %26 Close</fullName>
        <actions>
            <name>set_owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Change_Management__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Transition &amp; Close</value>
        </criteriaItems>
        <description>Notify the BA team of a new project</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
