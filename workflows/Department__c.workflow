<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Department_Update_Department_Name</fullName>
        <description>Update Department Name to Account Name: Department Name.</description>
        <field>Name</field>
        <formula>LEFT(TRIM(Name &amp; &quot;: &quot; &amp; Account__r.Name), 80)</formula>
        <name>Department: Update Department Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Department - Append Account Name to Department Name</fullName>
        <actions>
            <name>Department_Update_Department_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Department__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to append Account Name to Department Name</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
