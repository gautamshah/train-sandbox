<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copying_Quantity_to_Demand_Quantity</fullName>
        <field>Demand__c</field>
        <formula>Quantity</formula>
        <name>Copying Quantity to Demand Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Product_Type_Disposable</fullName>
        <field>Product_Type__c</field>
        <literalValue>Disposable</literalValue>
        <name>Opportunity Product - Type Disposable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Product_Update_Type_Capita</fullName>
        <field>Product_Type__c</field>
        <literalValue>Capital</literalValue>
        <name>Opportunity Product Update Type - Capita</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uniqueproductfieldupdate</fullName>
        <field>OppIdPricebookentryId__c</field>
        <formula>OpportunityId &amp;&apos;_&apos;&amp; PricebookEntryId</formula>
        <name>Uniqueproductfieldupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_All_Lost_check_box_EMEA_EM</fullName>
        <description>If Non-Compliant check box is checked All Lost check box should be checked as well</description>
        <field>All_Lost__c</field>
        <literalValue>1</literalValue>
        <name>Update All Lost check box EMEA EM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Copying Quantity to Demand Quantity for EMEA EM</fullName>
        <actions>
            <name>Copying_Quantity_to_Demand_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Profile.Name =&apos;EMEA EM&apos;,ISNULL( Demand__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Product - Update Product Types</fullName>
        <actions>
            <name>Opportunity_Product_Update_Type_Capita</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((ISPICKVAL( Product2.Product_Type__c, &quot;Capital&quot;)), (ISPICKVAL( Product_Type__c, &quot;None&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Product - Update Product Types - Disp</fullName>
        <actions>
            <name>Opportunity_Product_Type_Disposable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((ISPICKVAL( Product2.Product_Type__c, &quot;Disposable&quot;)), (ISPICKVAL( Product_Type__c, &quot;None&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UniqueProduct</fullName>
        <actions>
            <name>Uniqueproductfieldupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Profile.Name ==&apos;US - Med Supplies&apos;||$Profile.Name ==&apos;EU - One Covidien&apos;||$Profile.Name ==&apos;EM - All&apos;||$Profile.Name ==&apos;EMEA EM&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update All Lost check box EMEA EM</fullName>
        <actions>
            <name>Update_All_Lost_check_box_EMEA_EM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>OpportunityLineItem.Non_Compliant__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>EMEA EM</value>
        </criteriaItems>
        <description>If Non-Compliant check box is checked All Lost check box should be checked as well</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
