<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Approved_Notification</fullName>
        <description>Send Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_STI_Sample_Approved_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Rejected_Notification</fullName>
        <description>Send Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_STI_Sample_Rejected_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clinical_Evaluation_Autoname</fullName>
        <description>Update Clinical Evaluation Name with Hospital Name: Evaluation Name</description>
        <field>Name</field>
        <formula>Opportunity_Name__r.Account.Name  &amp;&quot;: &quot;&amp; Name</formula>
        <name>Clinical Evaluation Autoname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Draft</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Clinical Eval - Follow Up</fullName>
        <actions>
            <name>Clinical_Eval_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Follow up to a Clinical Eval</description>
        <formula>TODAY() - EvaluationStartDate__c  &gt; 60</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clinical Evaluation - Autoname</fullName>
        <actions>
            <name>Clinical_Evaluation_Autoname</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Clinical_Evaluation__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update name for Clinical Evaluation to be Hospital Name: Evaluation Name (given by rep)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clinical Evaluation - Unacceptable Outcome</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Clinical_Evaluation__c.Evaluation_Outcome__c</field>
            <operation>equals</operation>
            <value>Not Acceptable</value>
        </criteriaItems>
        <description>Workflow to capture if a clinical evaluation has an unacceptable outcome for use in a rollup summary field on the opportunity.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Clinical_Eval_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder follow up for the Clinical Eval linked to this Opportunity</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Clinical Eval 60 Day Follow Up</subject>
    </tasks>
</Workflow>
