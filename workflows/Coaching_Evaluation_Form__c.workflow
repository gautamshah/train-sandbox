<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Asia_Coaching_Evaluation_CDP_Manager_Approval</fullName>
        <description>Asia Coaching Evaluation CDP Manager Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>ASTD__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Coaching_Evaluation_Form_Mgr</template>
    </alerts>
    <alerts>
        <fullName>Asia_Coaching_Evaluation_Supervisor_Approval</fullName>
        <description>Asia  Coaching Evaluation Supervisor Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CreatedbyManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CoachingManager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Coaching_Evaluation_Form_Rep</template>
    </alerts>
    <alerts>
        <fullName>Asia_TH_Next_CDP_Reminder</fullName>
        <description>Asia TH Next CDP Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CreatedbyManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_TH_Next_CDP_Reminder</template>
    </alerts>
    <alerts>
        <fullName>TH_Coaching_Evaluation_CDP_Manager_Approval</fullName>
        <description>TH Coaching Evaluation CDP Manager Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>natchapat.sungdejapadadol@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>ASTD__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/TH_Coaching_Evaluation_Form_Mgr</template>
    </alerts>
    <alerts>
        <fullName>TH_Coaching_Evaluation_Supervisor_Approval</fullName>
        <description>TH Coaching Evaluation Supervisor Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CreatedbyManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>natchapat.sungdejapadadol@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>CoachingManager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/TH_Coaching_Evaluation_Form_Rep</template>
    </alerts>
    <fieldUpdates>
        <fullName>TH_Update_SM_Evaluation_Score</fullName>
        <description>Thailand Sales Managers: Calculate the score in the SM Coaching form by dividing total by 42.</description>
        <field>SMEvaluationScore__c</field>
        <formula>(VALUE(TEXT( State_Purpose_of_Coaching2__c ))+ 
VALUE(TEXT( Reviewedprevresults_sup__c ))+ 
VALUE(TEXT( Analyze_Customer_s_Situation2__c ))+ 
VALUE(TEXT( Align_with_SR2__c ))+ 
VALUE(TEXT( Observation_Listening_Note_taking2__c ))+ 
VALUE(TEXT( Supporting2__c ))+ 
VALUE(TEXT( On_Successes_Areas_that_SR_did_well2__c ))+ 
VALUE(TEXT( Opportunities_for_improvement2__c ))+ 
VALUE(TEXT( Concluding_Sup__c ))+ 
VALUE(TEXT( Focused_on_Coaching_objectives2__c ))+ 
VALUE(TEXT( Demonstrate_listening_skill2__c ))+ 
VALUE(TEXT( Provide_specific_feedback_with_examples2__c ))+ 
VALUE(TEXT( Coached_1_to_2_specific_key_areas2__c ))+ 
VALUE(TEXT( Lead_Sales_Rep_to_identify_action_plan2__c )) 
)/42</formula>
        <name>TH Update SM Evaluation Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TH_Update_SR_Evaluation_Score</fullName>
        <description>Thailand Sales Reps: Calculate the score in the SR Coaching form by dividing total by 69.</description>
        <field>SREvaluationScore__c</field>
        <formula>(VALUE(TEXT(Confirmed_schedule2__c))+ 
VALUE(TEXT(Reviewed_previous_visit_results2__c))+ 
VALUE(TEXT(Clarified_visit_purpose2__c))+ 
VALUE(TEXT(Prepared_selling_tools2__c))+ 
VALUE(TEXT(Product_knowledge2__c))+ 
VALUE(TEXT(Understands_market_and_business2__c))+ 
VALUE(TEXT(Understands_competitors2__c))+ 
VALUE(TEXT(Understandscustomeranditsorganizatio__c))+ 
VALUE(TEXT(Opening2__c))+ 
VALUE(TEXT(Capability_Statement2__c))+ 
VALUE(TEXT(Exploring_NCPO2__c))+ 
VALUE(TEXT(Presenting_Solutions2__c))+ 
VALUE(TEXT(Closing2__c))+ 
VALUE(TEXT(Concluding2__c))+ 
VALUE(TEXT(Asking_High_Gain_questions2__c))+ 
VALUE(TEXT(Objections_Handling2__c))+ 
VALUE(TEXT(Sales_Tools_utilization2__c))+ 
VALUE(TEXT(Time_management2__c))+ 
VALUE(TEXT(Marketing_material_used_during_call2__c))+ 
VALUE(TEXT(Ability_to_gather_information2__c))+ 
VALUE(TEXT(Presentable2__c))+ 
VALUE(TEXT(Professional_service_manners2__c))+ 
VALUE(TEXT(Passion_and_energy_during_sales_call2__c)))/69</formula>
        <name>TH Update SR Evaluation Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sign_Off</fullName>
        <field>SignOff__c</field>
        <literalValue>1</literalValue>
        <name>Update Sign Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sign_Off_Date</fullName>
        <field>SignOffDate__c</field>
        <formula>Today()</formula>
        <name>Update Sign Off Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Asia TH Next CDP Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Coaching_Evaluation_Form__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asia-TH Coaching Evaluation Form SR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Coaching_Evaluation_Form__c.NextCoachingDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asia_TH_Next_CDP_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Coaching_Evaluation_Form__c.NextCoachingDate__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Calculate SM Evaluation Score</fullName>
        <actions>
            <name>TH_Update_SM_Evaluation_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Asia - TH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Coaching_Evaluation_Form__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asia-TH Coaching Evaluation Form SM</value>
        </criteriaItems>
        <description>Calculate SM Evaluation Score for Asia</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Calculate SR Evaluation Score</fullName>
        <actions>
            <name>TH_Update_SR_Evaluation_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Asia - TH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Coaching_Evaluation_Form__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asia-TH Coaching Evaluation Form SR</value>
        </criteriaItems>
        <description>Calculate ASIA SR Evaluation Score:For Sales Rep, add up all the scores in the Sales Rep form and divide by 69</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
