<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Contact_Affiliation_Name_c</fullName>
        <field>Name__c</field>
        <formula>Contact__r.LastName &amp;&quot;: &quot;&amp; AffiliatedTo__r.Name</formula>
        <name>Update Contact Affiliation Name__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
