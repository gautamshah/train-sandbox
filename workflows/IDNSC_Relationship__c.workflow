<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Go_Live_Actual_Date</fullName>
        <description>When Relationship is set to Live, set the Actual Go-Live Date field</description>
        <field>Go_Live_Date_Actual__c</field>
        <formula>Today()</formula>
        <name>Update Go-Live Actual Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Go-Live Date Actual</fullName>
        <actions>
            <name>Update_Go_Live_Actual_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDNSC_Relationship__c.Current_Event_State__c</field>
            <operation>equals</operation>
            <value>Live</value>
        </criteriaItems>
        <description>When process is live, set the golive date to today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
