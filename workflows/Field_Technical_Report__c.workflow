<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_FTR_Acknowledgement_Letter_Alert</fullName>
        <ccEmails>mdr@covidien.com</ccEmails>
        <description>ANZ FTR Acknowledgement Letter Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ANZ_FTR_Visualforce</template>
    </alerts>
    <alerts>
        <fullName>ANZ_FTR_Alert_to</fullName>
        <ccEmails>mdr@covidien.com</ccEmails>
        <description>ANZ FTR Alert to rep, manager and MDR mailbox</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ANZ_FTR_Submission_Alert</template>
    </alerts>
    <alerts>
        <fullName>A_Field_Technical_Report_You_Created_Has_Not_Been_Submitted</fullName>
        <description>A Field Technical Report You Created Has Not Been Submitted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_Not_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Notify_EMID_Manager</fullName>
        <description>Field Technical Report - Notify EMID Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>ID_EMID_Head</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/ID_Complaint_Form_Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_Reminder_to_Submit</fullName>
        <description>Field Technical Report - Send Reminder to Submit</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_CQA</fullName>
        <ccEmails>cqa@covidien.com</ccEmails>
        <description>Field Technical Report - Send to CQA</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_S2_Submit_New</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_China_Complaint</fullName>
        <ccEmails>China.pir@covdidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for China Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/CN_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_EbD</fullName>
        <ccEmails>CQA@Covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for EbD</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_S2_Submit_New</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_HK_Complaint</fullName>
        <ccEmails>ruby.chau@Covidien.com</ccEmails>
        <ccEmails>taiwan.QA@covidien.com</ccEmails>
        <ccEmails>Bonnie.Lam@Covidien.com</ccEmails>
        <ccEmails>samantha.chow@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for HK Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_ANBR_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, debabrata.bhowmick@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-ANBR Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_EBD_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-EBD Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>pranshu.abhishek@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rajat.tandon@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_EMID_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, vishal.bhatia@medtronic.com, dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-EMID Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>rajat.tandon@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_ET_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, vishal.verma@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-ET Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_Hernia_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, kunal.kishore@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-Hernia Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_MNSE_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, debabrata.bhowmick@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-MNSE Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_NV_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, mona.galhotra@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-NV Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_PCS_NC_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, vishwas.mathur@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-PCS&amp;NC Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_Suture_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com, chanderdev.ahuja@medtronic.com, atul.gupta@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-Suture Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_IN_Vents_Complaint</fullName>
        <ccEmails>isc.pmv@medtronic.com,kingshook.mallik@medtronic.com,</ccEmails>
        <ccEmails>dl.indiaqualitycomplaintsall@medtronic.com,</ccEmails>
        <ccEmails>Shalini.sharma@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for IN-Vents Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Indonesia_EMID_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com</ccEmails>
        <ccEmails>Frieda.Rachmawati@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for Indonesia EMID Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ID_EMID_Head</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Korea_Complaint</fullName>
        <ccEmails>yeongjin.cho@medtronic.com</ccEmails>
        <ccEmails>kyung.ju.cho@medtronic.com</ccEmails>
        <ccEmails>junsu.kim@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for Korea Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>hyewon.kim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form_local_language</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Malaysia_Complaint</fullName>
        <ccEmails>Agnes.Tan@Covidien.com,</ccEmails>
        <ccEmails>Chloe.Tan@Covidien.com,</ccEmails>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>yeok.mooi.lim@medtronic.com</ccEmails>
        <ccEmails>mariani.binti.masjidan@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for Malaysia Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Philippines_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>Regina.Patena@medtronic.com,</ccEmails>
        <ccEmails>Sharon.f.de.mesa@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for Philippines Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_SD</fullName>
        <ccEmails>Quality.Assurance@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for SD</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_S2_Submit_New</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Singapore_Complaint</fullName>
        <ccEmails>Singapore-salesadmin@covidien.com</ccEmails>
        <ccEmails>SEA.QA@covidien.com</ccEmails>
        <ccEmails>sg.customerorder@covidien.com</ccEmails>
        <ccEmails>sgwhpcr@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for Singapore Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_CPDV_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>thunchanok.sirikantaramas@medtronic.com,</ccEmails>
        <ccEmails>nathakrit.panchit@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-CPDV Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>supaporn.kamphae@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_EMID_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>jutamart.Ongartyuttanakorn@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-EMID Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>voravut.taechakasembundit@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_EbD_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>saranpart.buasua@medtronic.com,</ccEmails>
        <ccEmails>kanokwan.thassaneryavech@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-EbD Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>thanan.nualnukul@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_MSAirway_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>pornsunee.chaikaew@medtronic.com,</ccEmails>
        <ccEmails>nathakrit.panchit@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-MSAirway Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>supaporn.kamphae@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_RMS_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>phunnjima.jeerasottikule@medtronic.com, pornsunee.chaikaew@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-RMS Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>supaporn.kamphae@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_STI_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>warangkana.punjapratheep@medtronic.com,</ccEmails>
        <ccEmails>Prompatsorn.weerisrupong@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-STI Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>dararat.luyaphan@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_VENTPM_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>phunnjima.jeerasottikule@medtronic.com,</ccEmails>
        <ccEmails>nathakrit.panchit@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-VENTPM Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>supaporn.kamphae@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_TH_VTMS_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com,</ccEmails>
        <ccEmails>duangkamol.jarupatrakorn@medtronic.com, parichart.bunjobchokchai@medtronic.com,</ccEmails>
        <ccEmails>anuwat.thammarungsi@medtronic.com,</ccEmails>
        <ccEmails>thunchanok.sirikantaramas@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to QA for TH-VTMS Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Taiwan_Complaint</fullName>
        <ccEmails>taiwan.qa@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for Taiwan Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form_local_language</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Taiwan_EbD_Complaint</fullName>
        <ccEmails>taiwan.qa@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for Taiwan-EbD Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>carol.chen29@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form_local_language</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Taiwan_Surgical_Complaint</fullName>
        <ccEmails>taiwan.qa@covidien.com</ccEmails>
        <ccEmails>maggie.cheng@Covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for Taiwan-Surgical Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form_local_language</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_QA_for_Vietnam_Complaint</fullName>
        <ccEmails>SEA.QA@covidien.com</ccEmails>
        <description>Field Technical Report - Send to QA for Vietnam Complaint</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Send_to_SD_Marketing_for_Korea_Complaint</fullName>
        <ccEmails>kevin.yeo@covidien.com</ccEmails>
        <ccEmails>hy.nam@covidien.com</ccEmails>
        <ccEmails>seho.kim@medtronic.com</ccEmails>
        <description>Field Technical Report - Send to SD Marketing for Korea Complaint</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Complaint_Form_local_language</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Submit_Clinical_Hotline</fullName>
        <ccEmails>CQA@covidien.com</ccEmails>
        <description>Field Technical Report - Submit Clinical Hotline</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_Clinical_Hotline</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Submit_RMS_Airways</fullName>
        <ccEmails>hqtsweb@covidien.com</ccEmails>
        <description>Field Technical Report - Submit RMS Airways</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_Submit</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Submit_RMS_Ventilation</fullName>
        <ccEmails>carlsb1-vent-tech-support@covidien.com</ccEmails>
        <description>Field Technical Report - Submit RMS Ventilation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_Submit</template>
    </alerts>
    <alerts>
        <fullName>Field_Technical_Report_Submit_to_RMS_Patient_Monitoring</fullName>
        <ccEmails>hqtsweb@covidien.com</ccEmails>
        <description>Field Technical Report - Submit to RMS Patient Monitoring</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Technical_Report_Submit</template>
    </alerts>
    <alerts>
        <fullName>SGFTRCreditNoteEmailAlert</fullName>
        <ccEmails>singapore-salesadmin@covidien.com</ccEmails>
        <description>SGFTRCreditNoteEmailAlert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_Complaint_Form_for_Credit_Note</template>
    </alerts>
    <alerts>
        <fullName>SGOffsetConsignmentEmailAlert</fullName>
        <ccEmails>jessie.zhou@covidien.com</ccEmails>
        <ccEmails>singapore-salesadmin@covidien.com</ccEmails>
        <description>SGOffsetConsignmentEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/SG_Complaint_Form</template>
    </alerts>
    <fieldUpdates>
        <fullName>ANZ_FTR_Update_Manager_Email_Field_Updat</fullName>
        <field>Sales_Rep_Manager_Email__c</field>
        <formula>CreatedBy.Manager.Email</formula>
        <name>ANZ FTR Update Manager Email Field Updat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Phone_Num</fullName>
        <field>Contact_Phone_Num__c</field>
        <formula>IF (
 ISNEW(),   Contact_Person_at_Facility__r.Phone_Number_at_Account__c , 

   IF(ISCHANGED( Contact_Phone_Num__c ), 

        IF(NOT(REGEX(Contact_Person_at_Facility__r.Phone_Number_at_Account__c, &quot;\\D*?(\\d\\D*?){10}&quot;)), Contact_Phone_Num__c ,  Contact_Person_at_Facility__r.Phone_Number_at_Account__c ) ,  Contact_Person_at_Facility__r.Phone_Number_at_Account__c )   
)</formula>
        <name>Contact Phone Num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FTR_Update_Submit_Checkbox</fullName>
        <field>Submit__c</field>
        <literalValue>1</literalValue>
        <name>FTR Update Submit Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Technical_Report_Update_BU_EbD</fullName>
        <description>Update the Business Unit to EbD when the user who creates the Field Technical Report has Business Unit=EbD.</description>
        <field>Business_Unit__c</field>
        <literalValue>EbD</literalValue>
        <name>Field Technical Report - Update BU EbD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Technical_Report_Update_BU_RMS</fullName>
        <field>Business_Unit__c</field>
        <literalValue>RMS</literalValue>
        <name>Field Technical Report: Update BU RMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Technical_Report_Update_BU_SD</fullName>
        <field>Business_Unit__c</field>
        <literalValue>SD</literalValue>
        <name>Field Technical Report: Update BU SD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Technical_Report_Update_BU_VT</fullName>
        <field>Business_Unit__c</field>
        <literalValue>VT</literalValue>
        <name>Field Technical Report: Update BU VT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Technical_Report_Update_Catalog</fullName>
        <field>Capital_Catalog_Number__c</field>
        <formula>Product__r.SKU__c</formula>
        <name>Field Technical Report - Update Catalog#</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Technical_Report_Update_Rep_Addr</fullName>
        <description>Update rep address when Send Exakt Pak to?=rep.</description>
        <field>Rep_Mailing_Address__c</field>
        <formula>$User.Street  &amp; &quot;, &quot; &amp;   $User.City  &amp; &quot;, &quot; &amp;   $User.State &amp; &quot;, &quot; &amp;   $User.PostalCode</formula>
        <name>Field Technical Report: Update Rep Addr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submit</fullName>
        <field>Submit__c</field>
        <literalValue>1</literalValue>
        <name>Submit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_City_on_FTR</fullName>
        <description>Added rule to update City which was missed when originally put into production.  Could have been done with a formula on FTR I believe, but doing it this way to keep consistent with other FTR field updates.</description>
        <field>City__c</field>
        <formula>Facility_Hospital_Name__r.BillingCity</formula>
        <name>Update City on FTR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Exakt_Pak_Mailing_Address_Cust</fullName>
        <description>Update Exakt Pak Mailing Address to reflect Contact Person at Facility Name and Address for Field Technical Report.</description>
        <field>Rep_Mailing_Address__c</field>
        <formula>Contact_Person_at_Facility__r.FirstName &amp; &quot; &quot; &amp; Contact_Person_at_Facility__r.LastName &amp; &quot;, &quot;&amp;   Facility_Hospital_Name__r.Name   &amp; &quot;, &quot;&amp;  Contact_Person_at_Facility__r.MailingStreet &amp; &quot;, &quot;&amp;  Contact_Person_at_Facility__r.MailingCity &amp; &quot;, &quot;&amp;  Contact_Person_at_Facility__r.MailingPostalCode</formula>
        <name>Update Exakt Pak Mailing Address - Cust</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field_Technical_BU_EbD</fullName>
        <description>If user&apos;s BU=EbD, update BU for field technical report to route correctly.</description>
        <field>Business_Unit__c</field>
        <literalValue>EbD</literalValue>
        <name>Update Field Technical BU - EbD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_State</fullName>
        <description>Update State for Field Technical Report/Account.</description>
        <field>State__c</field>
        <formula>Facility_Hospital_Name__r.BillingState</formula>
        <name>Update State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Street</fullName>
        <description>Update street for Field Technical Report/Account.</description>
        <field>Street__c</field>
        <formula>Facility_Hospital_Name__r.BillingStreet</formula>
        <name>Update Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zip_Code</fullName>
        <description>Update Zip Code for Field Technical Report/Account.</description>
        <field>ZipPostalCode__c</field>
        <formula>Facility_Hospital_Name__r.BillingPostalCode</formula>
        <name>Update Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ANZ FTR Acknowledgement Letter</fullName>
        <actions>
            <name>ANZ_FTR_Acknowledgement_Letter_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Field Technical Report,ANZ PB980 Field Technical Report</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.AcknowledgementLetterRequired__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Sends Acknowledgement Letter to sales rep and MDR mailbox when select in the FTR record.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ANZ FTR Submission Alert</fullName>
        <actions>
            <name>ANZ_FTR_Alert_to</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Field Technical Report,ANZ PB980 Field Technical Report</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ANZ FTR Update Manager Email</fullName>
        <actions>
            <name>ANZ_FTR_Update_Manager_Email_Field_Updat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Field Technical Report,ANZ PB980 Field Technical Report</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact Phone Num</fullName>
        <actions>
            <name>Contact_Phone_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate contact phone if not populated on the contact record.</description>
        <formula>NOT(ISBLANK(Name)) &amp;&amp; NOT(ISBLANK(Contact_Person_at_Facility__r.Phone_Number_at_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Clinical Hotline</fullName>
        <actions>
            <name>Field_Technical_Report_Submit_Clinical_Hotline</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Private</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>USD</value>
        </criteriaItems>
        <description>Workflow to generate and send e-mail to EbD quality control when a Clinical Hotline Ticket is submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - EbD Complaint</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_EbD</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>S2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Workflow to generate and send e-mail to EbD quality control when Field Technical Report is filled out by EbD user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Notify Manager - ID EMID</fullName>
        <actions>
            <name>Field_Technical_Report_Notify_EMID_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Business_Unit_F__c</field>
            <operation>equals</operation>
            <value>EMID</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>ID</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>Notify EMID manager when FTR is created by sales rep.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Populate Part Catalog %23</fullName>
        <actions>
            <name>Field_Technical_Report_Update_Catalog</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNULL( Product__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Exakt Pak to Customer</fullName>
        <actions>
            <name>Update_Exakt_Pak_Mailing_Address_Cust</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.SendExaktPakto__c</field>
            <operation>equals</operation>
            <value>Customer</value>
        </criteriaItems>
        <description>Workflow to add contact address to Field Technical Report when Customer is selected for Send Exakt Pack To.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - China</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_China_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <description>Workflow to submit China Field Technical Report to the Quality Control Group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - HK</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_HK_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>HK,MO</value>
        </criteriaItems>
        <description>Workflow to submit HK Field Technical Report to the Quality Control Group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - ID EMID</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Indonesia_EMID_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>ID</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Business_Unit_F__c</field>
            <operation>equals</operation>
            <value>EMID</value>
        </criteriaItems>
        <description>Workflow to submit Indonesia EMID Field Technical Report to the Quality Control Group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN ANBR</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_ANBR_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-ANBR Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;AN:BR:TM:RE&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;AN:BR:TM:RE&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;AN:BR:TM:RE&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;AN:BR:TM:RE&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;AN:BR:TM:RE&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN EBD</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_EBD_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-EBD Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;ES:HW:RD:VS&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;ES:HW:RD:VS&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;ES:HW:RD:VS&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;ES:HW:RD:VS&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;ES:HW:RD:VS&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN EMID</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_EMID_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-EMID Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;LA:SP:BG:AC:OS&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;LA:SP:BG:AC:OS&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;LA:SP:BG:AC:OS&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;LA:SP:BG:AC:OS&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;LA:SP:BG:AC:OS&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN ET</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_ET_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-ET Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;AB:GD:GS:IL&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;AB:GD:GS:IL&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;AB:GD:GS:IL&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;AB:GD:GS:IL&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;AB:GD:GS:IL&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN Hernia</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_Hernia_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-Hernia Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;MS:SO:SY&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;MS:SO:SY&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;MS:SO:SY&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;MS:SO:SY&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;MS:SO:SY&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN MNSE</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_MNSE_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-MNSE Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;MN:SE:AS:AM:CC&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;MN:SE:AS:AM:CC&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;MN:SE:AS:AM:CC&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;MN:SE:AS:AM:CC&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;MN:SE:AS:AM:CC&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN NV</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_NV_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-NV Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;CO:ND:ON:FD:NV&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;CO:ND:ON:FD:NV&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;CO:ND:ON:FD:NV&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;CO:ND:ON:FD:NV&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;CO:ND:ON:FD:NV&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN PCS%26NC</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_PCS_NC_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-PCS&amp;NC Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;AH:AW:CP:CS:CT:DV:EF:EL:NS:OR:OT:SH:SK:SN:SU:TH:UR:WC:OK:PN:MO:CH:OK:PN&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;AH:AW:CP:CS:CT:DV:EF:EL:NS:OR:OT:SH:SK:SN:SU:TH:UR:WC:OK:PN:MO:CH:OK:PN&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;AH:AW:CP:CS:CT:DV:EF:EL:NS:OR:OT:SH:SK:SN:SU:TH:UR:WC:OK:PN:MO:CH:OK:PN&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;AH:AW:CP:CS:CT:DV:EF:EL:NS:OR:OT:SH:SK:SN:SU:TH:UR:WC:OK:PN:MO:CH:OK:PN&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;AH:AW:CP:CS:CT:DV:EF:EL:NS:OR:OT:SH:SK:SN:SU:TH:UR:WC:OK:PN:MO:CH:OK:PN&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN Suture</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_Suture_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-Suture Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;ST&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;ST&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;ST&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;ST&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;ST&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - IN Vents</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_IN_Vents_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit India-Vents Field Technical Report to the local Quality Control Group, submitter, rep, manager of rep and product manager.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;IN&apos; , OR(   CONTAINS(&quot;AD:HV:VE:XT:MT&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;AD:HV:VE:XT:MT&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;AD:HV:VE:XT:MT&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;AD:HV:VE:XT:MT&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;AD:HV:VE:XT:MT&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - Korea</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Korea_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <description>Workflow to submit Korea Field Technical Report to the Quality Control Group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - Korea SD</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_SD_Marketing_for_Korea_Complaint</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to copy Korea SD Marketing with the Field Technical Report.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c  , &apos;Yes&apos;) , Country__c = &apos;KR&apos; , OR(     ISPICKVAL(Product_SKU_1__r.GBU__c , &apos;SD&apos;)    ,ISPICKVAL(Product_SKU_2__r.GBU__c , &apos;SD&apos;)    ,ISPICKVAL(Product_SKU_3__r.GBU__c , &apos;SD&apos;)    ,ISPICKVAL(Product_SKU_4__r.GBU__c , &apos;SD&apos;)    ,ISPICKVAL(Product_SKU_5__r.GBU__c , &apos;SD&apos;)      ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - MY</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Malaysia_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>MY</value>
        </criteriaItems>
        <description>Workflow to submit Malaysia Field Technical Report to the Quality Control Group, the record creator and his/her manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - PH</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Philippines_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>PH</value>
        </criteriaItems>
        <description>Workflow to submit Philippines Field Technical Report to the Quality Control Group, the record creator and his/her manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - RMS Airways</fullName>
        <actions>
            <name>Field_Technical_Report_Submit_RMS_Airways</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.RMS_Business_Unit__c</field>
            <operation>equals</operation>
            <value>Airways</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Workflow to submit Field Technical Report to the Quality Control Group that works with RMS Airways.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - RMS PM</fullName>
        <actions>
            <name>Field_Technical_Report_Submit_to_RMS_Patient_Monitoring</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.RMS_Business_Unit__c</field>
            <operation>equals</operation>
            <value>Patient Monitoring</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Workflow to submit Field Technical Report to the Quality Control Group that works with RMS Patient Monitoring.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - RMS Ventilation</fullName>
        <actions>
            <name>Field_Technical_Report_Submit_RMS_Ventilation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.RMS_Business_Unit__c</field>
            <operation>equals</operation>
            <value>Ventilation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Workflow to submit Field Technical Report to the Quality Control Group that works with RMS Ventilation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - Singapore</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Singapore_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>SG</value>
        </criteriaItems>
        <description>Workflow to submit Singapore Field Technical Report to the Quality Control Group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-Airway</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_MSAirway_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand-Airway (AS:SE:AM:MN:TM:CA:CC:AN:BE:RE) Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR( CONTAINS(&quot;AN:BR:RE&quot;, Product_SKU_1__r.Sales_Class__c),  AND( CONTAINS(&quot;AS:SE:AM:MN:TM:CA:CC:AN:BE:RE&quot;, Product_SKU_2__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_2__c ) ) ),  AND( CONTAINS(&quot;AS:SE:AM:MN:TM:CA:CC:AN:BE:RE&quot;, Product_SKU_3__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_3__c ) ) ),  AND( CONTAINS(&quot;AS:SE:AM:MN:TM:CA:CC:AN:BE:RE&quot;, Product_SKU_4__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_4__c ) ) ),  AND( CONTAINS(&quot;AS:SE:AM:MN:TM:CA:CC:AN:BE:RE&quot;, Product_SKU_5__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_5__c ) ) ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-CPDV</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_CPDV_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand-CP&amp;DV Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR( CONTAINS(&quot;CP:DV&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;CP:DV&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;CP:DV&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;CP:DV&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;CP:DV&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-EMID</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_EMID_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand-EMID Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR( CONTAINS(&quot;LA:AC:SP:OS&quot;, Product_SKU_1__r.Sales_Class__c),  AND( CONTAINS(&quot;LA:AC:SP:OS&quot;, Product_SKU_2__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_2__c ) ) ),  AND( CONTAINS(&quot;LA:AC:SP:OS&quot;, Product_SKU_3__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_3__c ) ) ),  AND( CONTAINS(&quot;LA:AC:SP:OS&quot;, Product_SKU_4__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_4__c ) ) ),  AND( CONTAINS(&quot;LA:AC:SP:OS&quot;, Product_SKU_5__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_5__c ) ) ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-EbD</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_EbD_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand-EbD Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR( ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;EbD&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-MS</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_MSAirway_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand-MS Field Technical Report to the Quality Control Group.</description>
        <formula>AND(   ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) ,   Country__c = &apos;TH&apos; ,   OR(     ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;MS&apos;) ,    ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;MS&apos;) ,    ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;MS&apos;) ,     ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;MS&apos;) ,     ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;MS&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-RMS</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_RMS_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow to submit Thailand-RMS Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR( ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;RMS&apos;) , ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;RMS&apos;) , ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;RMS&apos;) , ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;RMS&apos;) , ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;RMS&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-STI</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_STI_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand-STI Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR(  CONTAINS(&quot;BG:MS:SY:SO:ST&quot;, Product_SKU_1__r.Sales_Class__c),  AND( CONTAINS(&quot;BG:MS:SY:SO:ST&quot;, Product_SKU_2__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_2__c ) ) ),  AND( CONTAINS(&quot;BG:MS:SY:SO:ST&quot;, Product_SKU_3__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_3__c ) ) ),  AND( CONTAINS(&quot;BG:MS:SY:SO:ST&quot;, Product_SKU_4__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_4__c ) ) ),  AND( CONTAINS(&quot;BG:MS:SY:SO:ST&quot;, Product_SKU_5__r.Sales_Class__c), NOT(ISBLANK( Product_SKU_5__c ) ) ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-VENTPM</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_VENTPM_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Thailand COT (VE , HV , MT   and AM and AS ) Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; ,  OR( CONTAINS(&quot;VE:HV:MT:AM:AS&quot;, Product_SKU_1__r.Sales_Class__c),   AND( CONTAINS(&quot;VE:HV:MT:AM:AS&quot;, Product_SKU_2__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_2__c ) ) ),   AND( CONTAINS(&quot;VE:HV:MT:AM:AS&quot;, Product_SKU_3__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_3__c ) ) ),   AND( CONTAINS(&quot;VE:HV:MT:AM:AS&quot;, Product_SKU_4__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_4__c ) ) ),   AND( CONTAINS(&quot;VE:HV:MT:AM:AS&quot;, Product_SKU_5__r.Sales_Class__c),  NOT(ISBLANK( Product_SKU_5__c ) ) )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - TH-VTMS</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_TH_VTMS_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow to submit Thailand-VTMS Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TH&apos; , OR( ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;VT&apos;) ,  ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;MS&apos;) , ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;VT&apos;) , ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;MS&apos;) , ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;VT&apos;) ,  ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;MS&apos;) , ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;VT&apos;) ,  ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;MS&apos;) , ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;VT&apos;) , ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;MS&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - Taiwan</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Taiwan_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <description>Workflow to submit Taiwan Field Technical Report to the Quality Control Group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - Taiwan-EbD</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Taiwan_EbD_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Taiwan-EbD Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TW&apos; , OR( ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;EbD&apos;) , ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;EbD&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - Taiwan-Surgical</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Taiwan_Surgical_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to submit Taiwan-Surgical Field Technical Report to the Quality Control Group.</description>
        <formula>AND( ISPICKVAL(Ready_To_Submit__c , &apos;YES&apos;) , Country__c = &apos;TW&apos; , OR( ISPICKVAL(Product_SKU_1__r.GBU__c, &apos;SD&apos;) , ISPICKVAL(Product_SKU_2__r.GBU__c, &apos;SD&apos;) , ISPICKVAL(Product_SKU_3__r.GBU__c, &apos;SD&apos;) , ISPICKVAL(Product_SKU_4__r.GBU__c, &apos;SD&apos;) , ISPICKVAL(Product_SKU_5__r.GBU__c, &apos;SD&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report - VN</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_Vietnam_Complaint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FTR_Update_Submit_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Ready_To_Submit__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>VN</value>
        </criteriaItems>
        <description>Workflow to submit Vietnam Field Technical Report to the Quality Control Group, the record creator and his/her manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report SD</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_QA_for_SD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to generate and send e-mail to S2 quality control when Field Technical Report is filled out by S2 user.</description>
        <formula>AND(  Submit__c  = TRUE,  Product__r.Sales_Class_Description__c    &lt;&gt;  &quot;OR SAFETY&quot;,  Region__c =&quot;US&quot;, ISPICKVAL(  CurrencyIsoCode  , &quot;USD&quot;),OR( ISPICKVAL(   $User.Business_Unit__c , &quot;Surgical Innovations (SI)&quot;),ISPICKVAL(  $User.Business_Unit__c , &quot;Strategic Accounts&quot;), ISPICKVAL(  $User.Business_Unit__c , &quot;S2&quot;) ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Send Report Situate CQA</fullName>
        <actions>
            <name>Field_Technical_Report_Send_to_CQA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to generate and send e-mail to cqa@covidien.com. when Field Technical Report is filled out/created by S2 user and product sales class description=&quot;OR SAFETY&quot; (for Situate products)..</description>
        <formula>AND( Submit__c  = TRUE, Product__r.Sales_Class_Description__c  = &quot;OR SAFETY&quot;, Region__c=&quot;US&quot;, ISPICKVAL( CurrencyIsoCode , &quot;USD&quot;),OR( ISPICKVAL(  $User.Business_Unit__c , &quot;Surgical Innovations (SI)&quot;),ISPICKVAL( $User.Business_Unit__c  , &quot;Strategic Accounts&quot;), ISPICKVAL( $User.Business_Unit__c  , &quot;S2&quot;) ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report - Submit Not Checked</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Submit__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Clinical Hotline</value>
        </criteriaItems>
        <description>Workflow to send alert and create task for user when a Field Technical Report has been created and the user hasn&apos;t selected the submit button in the first 24 hours.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>A_Field_Technical_Report_You_Created_Has_Not_Been_Submitted</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Rep Address</fullName>
        <actions>
            <name>Field_Technical_Report_Update_Rep_Addr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.SendExaktPakto__c</field>
            <operation>equals</operation>
            <value>Sales Rep</value>
        </criteriaItems>
        <description>Workflow to add rep mailing address to Field Technical Report if Send Exact Pak to = Rep.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Update Account Address</fullName>
        <actions>
            <name>Update_City_on_FTR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update the address for the Facility Hospital Name.</description>
        <formula>NOT(ISNULL(  Facility_Hospital_Name__r.Name  ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Update BU EbD</fullName>
        <actions>
            <name>Field_Technical_Report_Update_BU_EbD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow to update Business Unit of Field Technical Report Owner.</description>
        <formula>ISPICKVAL( $User.Business_Unit__c , &quot;EbD&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Update BU RMS</fullName>
        <actions>
            <name>Field_Technical_Report_Update_BU_RMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow to update Business Unit of Field Technical Report Owner.</description>
        <formula>ISPICKVAL( $User.Business_Unit__c , &quot;RMS&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Update BU SD</fullName>
        <actions>
            <name>Field_Technical_Report_Update_BU_SD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL( $User.Business_Unit__c , &quot;SD&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Update BU VT</fullName>
        <actions>
            <name>Field_Technical_Report_Update_BU_VT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL( $User.Business_Unit__c , &quot;VT&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Technical Report%3A Update Business Unit</fullName>
        <actions>
            <name>Update_Field_Technical_BU_EbD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL( $User.Business_Unit__c , &quot;EbD&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGFTRCreditNoteRule</fullName>
        <actions>
            <name>SGFTRCreditNoteEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Field_Technical_Report__c.Country__c</field>
            <operation>equals</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Technical_Report__c.Credit_Note_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Credit Note Required is ticked when the record is created/updated, trigger an email alert.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGOffsetConsignmentRule</fullName>
        <actions>
            <name>SGOffsetConsignmentEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When one or more of the &apos;Offset Consignment&quot; fields are selected when the record is created, trigger an email alert.</description>
        <formula>AND( Country__c = &quot;SG&quot;, OR(ISPICKVAL( Offset_Consignment_1__c , &quot;Yes&quot;) ,ISPICKVAL( Offset_Consignment_2__c , &quot;Yes&quot;) ,ISPICKVAL( Offset_Consignment_3__c , &quot;Yes&quot;) ,ISPICKVAL( Offset_Consignment_4__c , &quot;Yes&quot;) ,ISPICKVAL( Offset_Consignment_5__c , &quot;Yes&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Field_Technical_Report_has_Been_Completed_and_Not_Submitted</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Field Technical Report has Been Completed and Not Submitted</subject>
    </tasks>
</Workflow>
