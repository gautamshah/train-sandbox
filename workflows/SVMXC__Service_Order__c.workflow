<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Remove_Closed_By_when_reopened</fullName>
        <description>Set Closed by to blank if work order is re-opened after closing</description>
        <field>Closed_By_Custom__c</field>
        <name>Remove Closed By when reopened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Closed_Date_when_reopened</fullName>
        <description>Set Closed Date to blank if work order is re-opened after closing</description>
        <field>Closed_Date__c</field>
        <name>Remove Closed Date when reopened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Work_Order_Closed_By</fullName>
        <description>Populate closed by with current user</description>
        <field>Closed_By_Custom__c</field>
        <formula>$User.Full_Name__c</formula>
        <name>Work Order Closed By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Work_Order_Closed_Date</fullName>
        <description>Date and time the work order was closed</description>
        <field>Closed_Date__c</field>
        <formula>now()</formula>
        <name>Work Order Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Remove Closed By and Date when reopened</fullName>
        <actions>
            <name>Remove_Closed_By_when_reopened</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_Closed_Date_when_reopened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>New,In progress,On hold</value>
        </criteriaItems>
        <description>Reset Closed By and Closed Date fields back to blank if Work Order is re-opened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Closed Date and User</fullName>
        <actions>
            <name>Work_Order_Closed_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Work_Order_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>Closed,Canceled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
