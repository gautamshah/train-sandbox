<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_Field_Visit_Email_Alert</fullName>
        <description>ANZ Field Visit Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/ANZ_Field_Visit_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_FST_MITG_US_Is_Created</fullName>
        <description>Email FST MITG US Is Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>zahin.n.maneckshaw@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>FST_RM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>FST__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Seller_RM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Seller__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/FST_MITG_US_Created</template>
    </alerts>
    <alerts>
        <fullName>Email_FST_team_and_Training_Managers_GSP</fullName>
        <ccEmails>leena.khatri@covidien.com</ccEmails>
        <description>Email FST team and Training Managers - GSP</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/FST_Template_GSPhernia</template>
    </alerts>
    <alerts>
        <fullName>Email_FST_team_and_Training_Managers_SED</fullName>
        <ccEmails>leena.khatri@covidien.com</ccEmails>
        <description>Email FST team and Training Managers - SED</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/FST_Template_SED</template>
    </alerts>
    <alerts>
        <fullName>Field_Visit_Has_Been_Updated</fullName>
        <description>Field Visit Has Been Updated</description>
        <protected>false</protected>
        <recipients>
            <field>Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Field_Visit_Has_Been_Updated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_FST_FSTRM_Seller_SellerRM</fullName>
        <description>Send Email to FST FSTRM Seller SellerRM</description>
        <protected>false</protected>
        <recipients>
            <recipient>zahin.n.maneckshaw@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>FST_RM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>FST__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Seller_RM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Seller__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/FST_MITG_US_Template_Updated</template>
    </alerts>
    <alerts>
        <fullName>Send_confirmation_email</fullName>
        <description>Send confirmation email</description>
        <protected>false</protected>
        <recipients>
            <recipient>frank.renteria@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FST_Confirmation_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_FST_team_and_Training_Managers</fullName>
        <ccEmails>leena.khatri@covidien.com</ccEmails>
        <description>Email FST team and Training Managers - Hernia</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/FST_Template_GSPhernia</template>
    </alerts>
    <fieldUpdates>
        <fullName>EU_S2_Field_Visit_RecordType_Update</fullName>
        <description>This field updates the recordtype of the field visits for EU-S2 users when the Field Visit Status changes to closed.</description>
        <field>RecordTypeId</field>
        <lookupValue>EU_S2_Field_Visit_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>EU_S2_Field_Visit_RecordType_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ANZ Field Visit Email Alert</fullName>
        <actions>
            <name>ANZ_Field_Visit_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.VisitLetterComplete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ MITG Field Visit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EU_S2_Field_Visit_Record_Type_Switch</fullName>
        <actions>
            <name>EU_S2_Field_Visit_RecordType_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>S2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.Field_Visit_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>EU S2 Field Visit Planned</value>
        </criteriaItems>
        <description>This workflow rule will switch the record type used by a Field Visit record if the Field Visit Status is Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FST MITG US 10K Opp Closed Checked</fullName>
        <actions>
            <name>Send_Email_to_FST_FSTRM_Seller_SellerRM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.X10K_Opportunity_Closed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>when 10K Opportunities Closed is checked, email notification go to FST, FST RM, Seller, Seller RM and Zahin  Maneckshaw.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FST MITG US Created</fullName>
        <actions>
            <name>Email_FST_MITG_US_Is_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FST MITG US</value>
        </criteriaItems>
        <description>when FST MITG US is created, email notification go to FST, FST RM, Seller, Seller RM.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Field Visit - Field Visit Has Been Updated</fullName>
        <actions>
            <name>Field_Visit_Has_Been_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.Send_Email_Field_Visit_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to send e-mail to sales rep and manager listed on a field visit when a field visit has been updated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Visit FST Email Alert GSP</fullName>
        <actions>
            <name>Email_FST_team_and_Training_Managers_GSP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.Visit_1_Summary_FST__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.Visit_1_Summary_Seller__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FST Field Visit #1,FST Field Visit #2,FST Field Visit #3</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Visit FST Email Alert Hernia</fullName>
        <actions>
            <name>Send_email_to_FST_team_and_Training_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.Visit_1_Summary_FST__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.Visit_1_Summary_Seller__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FST-Hernia Visit #1,FST-Hernia Visit #2,FST-Hernia Visit #3</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Visit FST Email Alert SED</fullName>
        <actions>
            <name>Email_FST_team_and_Training_Managers_SED</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.Send_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FST-AST Field Visit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Confirmation Email</fullName>
        <actions>
            <name>Send_confirmation_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Field_Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FST-AST Field Visit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Field_Visit__c.Send_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
