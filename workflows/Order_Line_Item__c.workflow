<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Current_FY_Full_Cost</fullName>
        <field>Current_FY_Full_Cost__c</field>
        <formula>Product_Name__r.Current_FY_Full_Cost__c</formula>
        <name>Update Current FY Full Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Current_FY_Standard_Cost</fullName>
        <field>Current_FY_Standard_Cost__c</field>
        <formula>Product_Name__r.Current_FY_Standard_Cost__c</formula>
        <name>Update Current FY Standard Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DIS-Update Product Full Cost and STD Cost</fullName>
        <actions>
            <name>Update_Current_FY_Full_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Current_FY_Standard_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Current FY Full Cost and Standard Cost from Product SKU</description>
        <formula>ISCHANGED( Product_Name__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
