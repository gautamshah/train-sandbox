<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Seller_team_member</fullName>
        <description>New Seller team member</description>
        <protected>false</protected>
        <recipients>
            <field>UserId</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/New_Seller_Team_Member</template>
    </alerts>
    <rules>
        <fullName>Notify new team member</fullName>
        <actions>
            <name>New_Seller_team_member</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify new team member that they have been added to a seller team.</description>
        <formula>CreatedDate =  LastModifiedDate</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
