<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SVMX_Task_Notification_PS</fullName>
        <description>SVMX Task Notification PS</description>
        <protected>false</protected>
        <recipients>
            <recipient>Professional_Services</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SVMXC__ServiceMaxEmailTemplates/SVMX_Work_Order_Task_Notification</template>
    </alerts>
    <alerts>
        <fullName>SVMX_Task_Notification_Wipro</fullName>
        <ccEmails>amey.adivarekar@medtronic.com;nupur.choudhury@medtronic.com;vikas.kapoor@medtronic.com;ishan.lamture@medtronic.com;ganesh.maru@medtronic.com</ccEmails>
        <description>SVMX Task Notification Wipro</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>SVMXC__ServiceMaxEmailTemplates/SVMX_Work_Order_Task_Notification</template>
    </alerts>
    <rules>
        <fullName>Rebate with Pricing</fullName>
        <actions>
            <name>Request_for_Rebate_Implementation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Request for COOP with Rebate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Request for Sales and Pricing with Rebate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>This workflow will fire once Mansfield Individual Pricing has completed their task for S&amp;P or COOP processing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Request_for_Rebate_Implementation</fullName>
        <assignedTo>endcustomer.rebates@covidien.com.queue</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Rebate agreement is ready to be implemented.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request for Rebate Implementation</subject>
    </tasks>
</Workflow>
