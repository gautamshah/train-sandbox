<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>USMS_HCS_Opportunity_Score</fullName>
        <field>Opportunity_Score__c</field>
        <formula>IF(MS_total_potential_at_account_total__c*4+Account_Sales_Force_Total__c*3+Competitive_Situation_Total__c*3+Covidien_share_at_account_total__c*2+Covidien_share_trend_at_account_total__c*3+Account_s_position_in_marketplace_total__c*3-10 &gt;=60, 60,

IF(MS_total_potential_at_account_total__c*4+Account_Sales_Force_Total__c*3+Competitive_Situation_Total__c*3+Covidien_share_at_account_total__c*2+Covidien_share_trend_at_account_total__c*3+Account_s_position_in_marketplace_total__c*3-10 &lt;= 0, 0,

MS_total_potential_at_account_total__c*4+Account_Sales_Force_Total__c*3+Competitive_Situation_Total__c*3+Covidien_share_at_account_total__c*2+Covidien_share_trend_at_account_total__c*3+Account_s_position_in_marketplace_total__c*3-10))</formula>
        <name>USMS HCS Opportunity Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>USMS_HCS_Update_Fit_Score</fullName>
        <field>Fit_Score__c</field>
        <formula>PS_Total__c*1+CPO_Total__c*1+Business_Relationship_Total__c*3+Hospital_Relationship_Total__c*2+Decision_Making_Process_Total__c*2+Distribution_Channel_Total__c*2+Payor_Mix_Total__c*1+Private_Label_Total__c*3-10</formula>
        <name>USMS HCS Update Fit Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>USMS HCS Fit Score Rule</fullName>
        <actions>
            <name>USMS_HCS_Update_Fit_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.PS_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.CPO_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Business_Relationship_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Hospital_Relationship_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Decision_Making_Process_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Distribution_Channel_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Payor_Mix_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Private_Label_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>USMS HCS Opportunity Score Rule</fullName>
        <actions>
            <name>USMS_HCS_Opportunity_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.MS_total_potential_at_account_total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Account_Sales_Force_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Competitive_Situation_Total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Covidien_share_trend_at_account_total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Covidien_share_at_account_total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Segmentation_Scorecard__c.Account_s_position_in_marketplace_total__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
