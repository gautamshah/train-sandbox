<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Asset_SWM_Email</fullName>
        <description>Asset SWM Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Mansfield_SWM_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Asset_SWM_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Asset_Update_Model_Name</fullName>
        <field>Name</field>
        <formula>TEXT(Model__c)</formula>
        <name>Asset Update Model Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_Update_Model_to_COV</fullName>
        <field>Manufacturer_SI__c</field>
        <literalValue>Covidien</literalValue>
        <name>Asset Update Model to COV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cap_Equip_Inst_Base_Autoname_Covidien</fullName>
        <description>Rule to update the autoname the record name to &quot;Covidien - Product Name&quot;</description>
        <field>Name</field>
        <formula>IF( ISBLANK(Covidien_Product__c),  Manufacturer_Product__r.ManufacturerOEM_Product__c ,Covidien_Product__c )</formula>
        <name>Cap Equip Inst Base - Autoname Covidien</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cap_Equip_Inst_Base_Autoname_Not_Cov</fullName>
        <description>Update to autoname the record name to Manufacturer - Competitor Product</description>
        <field>Name</field>
        <formula>Manufacturer_Product__r.ManufacturerOEM_Product__c</formula>
        <name>Cap Equip Inst Base - Autoname Not Cov</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capital_Installed_Base_EMEA_MS</fullName>
        <field>Name</field>
        <formula>Manufacturer_Product__r.ManufacturerOEM_Product__c</formula>
        <name>Capital Installed Base - EMEA MS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ILS_Update_Warranty_EndDate</fullName>
        <description>Install Date + 365  (1 year)</description>
        <field>Warranty_End_Date__c</field>
        <formula>InstallDate  + 364</formula>
        <name>ILS Update Warranty EndDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ILS_Update_Warranty_Start_Date</fullName>
        <description>Warranty Start date=Install Date</description>
        <field>Warranty_Start_Date__c</field>
        <formula>InstallDate</formula>
        <name>ILS Update Warranty Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Asset_Ownership_Type_to_Demo</fullName>
        <field>Ownership_Type__c</field>
        <literalValue>Demo</literalValue>
        <name>Set Asset Ownership Type to Demo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Serial_No_Unique</fullName>
        <field>Serial_No_Unique__c</field>
        <formula>SerialNumber</formula>
        <name>Update Serial No Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Asset Model Name</fullName>
        <actions>
            <name>Asset_Update_Model_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Asset.Business_Unit_text__c</field>
            <operation>equals</operation>
            <value>Surgical Innovations (SI)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Name</field>
            <operation>contains</operation>
            <value>S2FORCE,SURGII20,LIGASURE,RAPIDVAC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Model__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Business_Unit_text__c</field>
            <operation>equals</operation>
            <value>S2</value>
        </criteriaItems>
        <description>Takes the Model and updates the Name field for new records</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset SWM Email</fullName>
        <actions>
            <name>Asset_SWM_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Manufacturer_SI__c</field>
            <operation>equals</operation>
            <value>Covidien</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Business_Unit_text__c</field>
            <operation>equals</operation>
            <value>S2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Asset_External_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Asset Set Manufacturer</fullName>
        <actions>
            <name>Asset_Update_Model_to_COV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Asset.Source_del__c</field>
            <operation>equals</operation>
            <value>US_MANS,Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Name</field>
            <operation>contains</operation>
            <value>FORCE,SURGII20,LIGASURE,RAPIDVAC</value>
        </criteriaItems>
        <description>Takes the Asset Name and updates the Manufacturer picklist</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capital Equipment Installed Base - Autoname EU MS</fullName>
        <actions>
            <name>Capital_Installed_Base_EMEA_MS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For use in EMEA MS only to they can create new demo/loan records for Covidien equipment</description>
        <formula>AND( OR( ISPICKVAL( Ownership_Type__c , &quot;Demo&quot;), ISPICKVAL( Ownership_Type__c , &quot;Loaner&quot;), ISPICKVAL( Ownership_Type__c , &quot;Rental&quot;), ISPICKVAL( Ownership_Type__c , &quot;Free of Charge&quot;)),  $Profile.Name  = &quot;EU - One Covidien&quot;, CONTAINS( Manufacturer__r.Name , &quot;Covidien&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capital Equipment Installed Base - Autoname Not Covidien</fullName>
        <actions>
            <name>Cap_Equip_Inst_Base_Autoname_Not_Cov</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Autoname Capital Equipment Installed Base when the Manufacturer is not Covidien.</description>
        <formula>NOT(CONTAINS( Manufacturer__r.Name , &quot;Covidien&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capital Equipment Installed Base - Autoname Record</fullName>
        <actions>
            <name>Cap_Equip_Inst_Base_Autoname_Covidien</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(CONTAINS( Manufacturer__r.Name , &quot;Covidien&quot;) , Manufacturer__r.BillingCountry &lt;&gt; &quot;KR&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capital Equipment Installed Base - Update Account</fullName>
        <active>false</active>
        <formula>NOT(ISNULL(ERP_Account__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ILS Update Asset Warranty Start%2FEnd Date</fullName>
        <actions>
            <name>ILS_Update_Warranty_EndDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ILS_Update_Warranty_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Asset created by current user:profile =ILS Technical Service
update Warranty Start Date and Warranty End Date</description>
        <formula>OR(ISCHANGED( InstallDate )  &amp;&amp; (CreatedBy.ProfileId= &quot;00e0B000000eOO9&quot;  ||  $User.ProfileId   = &quot;00e0B000000eOO9&quot;), (CreatedBy.ProfileId =&quot;00e0B000000eOO9&quot;||  $User.ProfileId   = &quot;00e0B000000eOO9&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Installed Base%3A Service Contract Expiration 30 days</fullName>
        <active>false</active>
        <description>Workflow to create task and send notification when installed base service contract date is expiring in 30 days.</description>
        <formula>IF(  Service_Contract_End_Date__c  =TODAY( )-30,  true,  false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Installed Base%3A Service Contract Expiration 60 days</fullName>
        <active>false</active>
        <description>Workflow to create task and send notification when installed base service contract date is expiring in 60 days.</description>
        <formula>IF(  Service_Contract_End_Date__c  =TODAY( )-60,  true,  false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Installed Base%3A Service Contract Expiration 90 days</fullName>
        <active>false</active>
        <description>Workflow to create task and send notification when installed base service contract date is expiring in 90 days.</description>
        <formula>IF(  Service_Contract_End_Date__c  =TODAY( )-90,  true,  false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prevent Dup Serial Number</fullName>
        <actions>
            <name>Update_Serial_No_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Profile.Name =&apos;EU - One Covidien&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Asset Type Demo</fullName>
        <actions>
            <name>Set_Asset_Ownership_Type_to_Demo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>HK</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
