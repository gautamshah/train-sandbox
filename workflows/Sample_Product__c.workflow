<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HK_Update_Stand_Cost</fullName>
        <field>Current_FY_Stand_Cost__c</field>
        <formula>Product_SKU__r.Current_FY_Standard_Cost__c</formula>
        <name>HK Update Stand Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HK Capture Stand Cost</fullName>
        <actions>
            <name>HK_Update_Stand_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Sample_Product__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>HK Sample Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sample_Product__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>KR Sample Product</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
