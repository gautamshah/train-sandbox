<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Independent_Development_Plan_IDP_has_been_Submitted</fullName>
        <description>Independent Development Plan - IDP has been Submitted</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Manager_Mentor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/IDP_Has_Been_Submitted</template>
    </alerts>
    <rules>
        <fullName>IDP has been Submitted</fullName>
        <actions>
            <name>Independent_Development_Plan_IDP_has_been_Submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Individual_Development_Plan__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to send e-mail to manager or mentor listed on an IDP when an IDP has been submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
