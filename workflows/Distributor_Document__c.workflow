<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Account_Country</fullName>
        <field>Account_s_Country__c</field>
        <formula>Account_Name__r.BillingCountry</formula>
        <name>Update Account Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Country on Documents</fullName>
        <actions>
            <name>Update_Account_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Distributor_Document__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Account Country</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
