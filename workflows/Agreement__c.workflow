<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Notification_Stage</fullName>
        <field>Notification_Stage__c</field>
        <name>Notification Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>NextValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Notification_Stage2</fullName>
        <field>Notification_Stage__c</field>
        <literalValue>Overdue_Alert_Cycle</literalValue>
        <name>Notification Stage2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trigger_Reminder</fullName>
        <field>Trigger_Email_Alert__c</field>
        <literalValue>1</literalValue>
        <name>Trigger Reminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>01%2E Evaluation Initial Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Agreement__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement__c.Status__c</field>
            <operation>equals</operation>
            <value>Open (Assets Still At Customer Site)</value>
        </criteriaItems>
        <description>Sends Email to the owners of any assets that have not been returned 2 weeks prior to Contract Completion Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Trigger_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Agreement__c.Contract_Completion_Date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>02%2E Evaluation Final Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Agreement__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement__c.Status__c</field>
            <operation>equals</operation>
            <value>Open (Assets Still At Customer Site)</value>
        </criteriaItems>
        <description>Final Reminder - 1 Day Before End Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Trigger_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Agreement__c.Contract_Completion_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>03%2E Evaluation Overdue Alert</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Agreement__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement__c.Status__c</field>
            <operation>equals</operation>
            <value>Open (Assets Still At Customer Site)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement__c.Overdue_Alert_Count__c</field>
            <operation>lessThan</operation>
            <value>5</value>
        </criteriaItems>
        <description>Runs 1hr Before Next Overdue Alert Agreement until the Alert Count Reaches 5. Apex updates the Next Overdue Alert date, each time this workflow runs.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_Stage2</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Trigger_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Agreement__c.Next_Overdue_Alert_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>04%2E Evaluation Final Escalation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Agreement__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement__c.Status__c</field>
            <operation>equals</operation>
            <value>Open (Assets Still At Customer Site)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Agreement__c.Overdue_Alert_Count__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <description>Once Alert Count Reaches 5, Region Managers, Asset Operations, and Sales Reps are notified.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Trigger_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Agreement__c.Next_Overdue_Alert_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
