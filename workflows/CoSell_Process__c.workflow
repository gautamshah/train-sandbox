<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CoSell_Confirmation_Email</fullName>
        <description>CoSell Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/CoSell_Process_Alert</template>
    </alerts>
    <rules>
        <fullName>CoSell Process Alert</fullName>
        <actions>
            <name>CoSell_Confirmation_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CoSell_Process__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
