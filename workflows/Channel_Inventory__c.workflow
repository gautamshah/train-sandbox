<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_UOM_Status_in_CI</fullName>
        <field>Error_Message__c</field>
        <formula>Error_Message__c &amp; &quot; UOM Invalid&quot;</formula>
        <name>Update UOM Status in CI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DIS-check for UOM Values</fullName>
        <actions>
            <name>Update_UOM_Status_in_CI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>and ((UOM__c  &lt;&gt;   Product_UOM__c), (UOM__c  &lt;&gt;     Product_Selling_UOM__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
