<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Calc_Business_Unit</fullName>
        <description>On Preferred Product object - update Business Unit based on the business unit from the Targeted Covidien Product (which is a lookup to the SFDC Product object)</description>
        <field>Business_Unit__c</field>
        <formula>Target_Covidien_Product__r.GBU__c</formula>
        <name>Update Calc Business Unit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Calc Business Unit</fullName>
        <actions>
            <name>Update_Calc_Business_Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Prod_Preference_Card_Product__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Preferred Product - Linked to Product Preference Card</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
