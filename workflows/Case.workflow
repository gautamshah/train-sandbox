<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Status_has_been_changed</fullName>
        <description>Case Status has been changed</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Case_Status_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Case_has_been_closed</fullName>
        <description>Case has been closed</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Case_has_been_closed</template>
    </alerts>
    <alerts>
        <fullName>Comp_Case_Rejected</fullName>
        <description>Comp Case Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_NASSC_Compensation_Case_Rejected_by_Manager</template>
    </alerts>
    <alerts>
        <fullName>Compliance_Case_Created_Alert</fullName>
        <description>Compliance Case Created Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/SUPPORTNewassignmentnotification</template>
    </alerts>
    <alerts>
        <fullName>Compliance_Case_Reminder_Alert</fullName>
        <ccEmails>shawn.p.clark@medtronic.com</ccEmails>
        <ccEmails>john.m.romanowski@medtronic.com</ccEmails>
        <description>Compliance Case Reminder Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Compliance_Case_Reminder_Alert_HTML</template>
    </alerts>
    <alerts>
        <fullName>DQ_Case_Notify_Case_Closed</fullName>
        <description>DQ Case: Notify Case Closed</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SupportDataQualityCaseClosed</template>
    </alerts>
    <alerts>
        <fullName>DQ_Case_Notify_Requested_By_User</fullName>
        <description>DQ Case: Notify Requested By User</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SupportDataQualityCaseCreated</template>
    </alerts>
    <alerts>
        <fullName>Email_to_rep_informing_them_that_case_has_been_approved_by_manager</fullName>
        <description>Email to rep informing them that case has been approved by manager</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_SS_Case_Received_Customer_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>ILS_Notify_Account_Manager_Case_Updated</fullName>
        <description>ILS Notify Account Manager Case Updated</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/ILS_Case_is_Updated_Email</template>
    </alerts>
    <alerts>
        <fullName>ILS_Service_Requested_Alert_to_Owner_and_Rep</fullName>
        <description>ILS Service Requested Alert to Owner and Rep</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/ILS_Case_is_Updated_Email</template>
    </alerts>
    <alerts>
        <fullName>ITSM_Closed_Case_Notification</fullName>
        <description>ITSM Closed Case Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_ITSM_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>MITG_Intake_Request_Development_Lead_Notification</fullName>
        <description>MITG Intake Request - Development Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Development_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MITG_Intake_Request_Development_Lead_Email</template>
    </alerts>
    <alerts>
        <fullName>MITG_Intake_Request_Training_Lead_Notification</fullName>
        <description>MITG Intake Request - Training Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Training_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MITG_Intake_Request_Training_Lead_Email</template>
    </alerts>
    <alerts>
        <fullName>Milestone_Success</fullName>
        <description>Milestone Success</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SFDC_Milestone_Success</template>
    </alerts>
    <alerts>
        <fullName>Milestone_Violation</fullName>
        <description>Milestone Violation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>judith.randall@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SFDC_Milestone_Violation</template>
    </alerts>
    <alerts>
        <fullName>Milestone_Warning</fullName>
        <description>Milestone Warning</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>judith.randall@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SFDC_Milestone_Warning</template>
    </alerts>
    <alerts>
        <fullName>NASSC_Support_Send_Email_to_Requestor_and_Case_team_when_case_is_closed</fullName>
        <description>NASSC: Support Send Email to Requestor and Case team when case is closed</description>
        <protected>false</protected>
        <recipients>
            <recipient>Analyst (Primary)</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Manager</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Rep</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales VP</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Team Lead</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_NASSC_Compensation_Case_Resolved</template>
    </alerts>
    <alerts>
        <fullName>New_User_Request_Approval_Received</fullName>
        <description>New User Request Approval Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/NewUserApprovalReceived</template>
    </alerts>
    <alerts>
        <fullName>Notify_DQ_Queue_member</fullName>
        <description>Notify DQ Queue member</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SupportDataQualityCaseCreated</template>
    </alerts>
    <alerts>
        <fullName>Notify_Managers_when_the_Escalated_Box_is_checked</fullName>
        <description>Notify Managers when the Escalated Box is checked</description>
        <protected>false</protected>
        <recipients>
            <recipient>jason.rayner@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>judith.randall@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>matthew.drap@covidien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_Escalation_Notify</template>
    </alerts>
    <alerts>
        <fullName>Notify_Requested_By_user_on_case_creation_or_when_requested_by_added_to_case</fullName>
        <description>Notify Requested By user on case creation or when requested by added to case</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/CRM_Support_Notify_Requestedby</template>
    </alerts>
    <alerts>
        <fullName>OEM_Case_Closed_Send_Mail</fullName>
        <description>OEM Case Closed Send Mail</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>OEM_Notifications/OEM_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Out_of_Compliance_Alert</fullName>
        <ccEmails>SSG.compliance@covidien.com</ccEmails>
        <ccEmails>shadia.coram@medtronic.com</ccEmails>
        <ccEmails>shawn.p.clark@medtronic.com</ccEmails>
        <description>Out of Compliance Alert</description>
        <protected>false</protected>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Out_of_Compliance_Case</template>
    </alerts>
    <alerts>
        <fullName>SUPPORT_CRM_Case_Closed</fullName>
        <description>SUPPORT: CRM Case Closed Notify Requested By</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_Case_Close_Notification</template>
    </alerts>
    <alerts>
        <fullName>SUPPORT_CRM_Case_Closed_CreatedBy</fullName>
        <description>SUPPORT: CRM Case Closed Notify Created By</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_Case_Close_Notification</template>
    </alerts>
    <alerts>
        <fullName>SUPPORT_New_Public_Case_Comment_Alert</fullName>
        <description>SUPPORT: New Public Case Comment Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_Public_Comment_Notify</template>
    </alerts>
    <alerts>
        <fullName>SUPPORT_New_Public_Case_Comment_Alert_To_Created</fullName>
        <description>SUPPORT: New Public Case Comment Alert To Created User</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_Public_Comment_Notify</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_UpdateMessage_KR</fullName>
        <description>SellTo Code Update Message (KR)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>mun.ki.lee@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>kr.dissubmissions@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_KR</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_CN</fullName>
        <description>SellTo Code Update Message (CN)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>cn.dissubmissions@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_CN</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_EMS</fullName>
        <description>SellTo Code Update Message (EMS)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_EMS</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_HK</fullName>
        <description>SellTo Code Update Message (HK)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>hk.dissubmissions@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_HK</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_KR</fullName>
        <description>SellTo Code Update Message (KR)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>mun.ki.lee@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_KR</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_MO</fullName>
        <description>SellTo Code Update Message (MO)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>mo.dissubmissions@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_MO</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_SA</fullName>
        <description>SellTo Code Update Message (SA)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_SA</template>
    </alerts>
    <alerts>
        <fullName>SellTo_Code_Update_Message_TW</fullName>
        <description>SellTo Code Update Message (TW)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>tw.dissubmissions@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Distributor_Email_Template/DIS_SellTo_Code_Update_TW</template>
    </alerts>
    <alerts>
        <fullName>Send_Alert_to_Case_Owner_and_ILS_Sales_Rep</fullName>
        <description>Send Alert to Case Owner and ILS Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/ILS_Case_Creation_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Email_to_Manger_PR_Sales_Inquiry</fullName>
        <description>Send Notification Email to Manger - PR Sales Inquiry</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_of_Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Inquiry_Email_PR_SI</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Email_to_Manger_SI_Sales_Inquiry</fullName>
        <description>Send Notification Email to Manger - SI Sales Inquiry</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_of_Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Inquiry_Email_PR_SI</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Email_to_Requested_By_PR_Sales_Inquiry</fullName>
        <description>Send Notification Email to Requested By - PR Sales Inquiry</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORT_SS_Case_Received_Customer_Notification_Template_PR_SI</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Email_to_Requested_By_SI_Sales_Inquiry</fullName>
        <description>Send Notification Email to Requested By - SI Sales Inquiry</description>
        <protected>false</protected>
        <recipients>
            <field>Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORT_SS_Case_Received_Customer_Notification_Template_PR_SI</template>
    </alerts>
    <alerts>
        <fullName>Support_RMS_Mgr_Email_Notification_Case_received</fullName>
        <description>Support: RMS Email Notification for Manager Comp Case Received</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_NASSC_Compensation_Case_Received_Manager_Nofication</template>
    </alerts>
    <alerts>
        <fullName>Support_S2_Mgr_Email_Notification_Case_received</fullName>
        <description>Support: S2 Email Notification for Manager Comp Case Received</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_NASSC_Compensation_Case_Received_Manager_Nofication</template>
    </alerts>
    <alerts>
        <fullName>Support_VT_Mgr_Email_Notification_Case_received</fullName>
        <description>Support: VT Email Notification for Manager Comp Case Received</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/SUPPORT_NASSC_Compensation_Case_Received_Manager_Nofication</template>
    </alerts>
    <alerts>
        <fullName>US_PR_Sales_Comp_Team_All_Approvals_Received_Email_Alert</fullName>
        <description>US PR Sales Comp Team All Approvals Received Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/US_PR_Sales_Comp_All_Approvals_Received_Template</template>
    </alerts>
    <alerts>
        <fullName>US_PR_Sales_Comp_Team_Rejection_Received_Email_Alert</fullName>
        <description>US PR Sales Comp Team Rejection Received Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/US_PR_Sales_Comp_Approval_Rejected_Template</template>
    </alerts>
    <alerts>
        <fullName>Warranty_Claim_Request_Approved_Email_Notification_CN</fullName>
        <description>Warranty Claim Request Approved Email Notification (CN)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/Warranty_Claim_Case_has_been_Approved_CN</template>
    </alerts>
    <alerts>
        <fullName>Warranty_Claim_Request_Approved_Email_Notification_KR</fullName>
        <description>Warranty Claim Request Approved Email Notification (KR)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/Warranty_Claim_Case_has_been_Approved_KR</template>
    </alerts>
    <alerts>
        <fullName>Warranty_Claim_Request_Rejected_Email_Notification_CN</fullName>
        <description>Warranty Claim Request Rejected Email Notification (CN)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/Warranty_Claim_Case_has_been_Rejected_CN</template>
    </alerts>
    <alerts>
        <fullName>Warranty_Claim_Request_Rejected_Email_Notification_KR</fullName>
        <description>Warranty Claim Request Rejected Email Notification (KR)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Distributor_Email_Template/Warranty_Claim_Case_has_been_Rejected_KR</template>
    </alerts>
    <alerts>
        <fullName>X30_Day_Inactivity_Reminder</fullName>
        <description>30 Day Inactivity Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Compliance_Case_30_Day_Inactivity</template>
    </alerts>
    <alerts>
        <fullName>X60_Day_Inactivity_Escalation</fullName>
        <description>60 Day Inactivity Escalation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>notification@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Compliance_Case_60_Day_Inactivity</template>
    </alerts>
    <fieldUpdates>
        <fullName>Acceptance_Required</fullName>
        <field>Acceptance_Required__c</field>
        <literalValue>1</literalValue>
        <name>Acceptance Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acceptance_SLA_Alert</fullName>
        <field>Acceptance_SLA_Alert__c</field>
        <literalValue>1</literalValue>
        <name>Acceptance SLA Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acceptance_SLA_Missed</fullName>
        <field>Acceptance_SLA_Missed__c</field>
        <literalValue>1</literalValue>
        <name>Acceptance SLA Missed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Accepted</fullName>
        <field>Acceptance_Required__c</field>
        <literalValue>0</literalValue>
        <name>Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Aggregate_Transfer_Count</fullName>
        <field>Transfer_Count__c</field>
        <formula>Transfer_Count__c + 1</formula>
        <name>Support - Aggregate Transfer Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approvals_Received_Check</fullName>
        <field>ApprovalsReceived__c</field>
        <literalValue>1</literalValue>
        <name>Approvals Received Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_to_Comp_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_US_Surgical_EbD</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NASSC Comp: Assign Case to S2 Comp Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Close_Cases</fullName>
        <description>Updates status to Closed-No Action Necessary.</description>
        <field>Status</field>
        <literalValue>Closed-No Action Necessary</literalValue>
        <name>Auto-Close Cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Adjust_Commitment1</fullName>
        <field>Status</field>
        <literalValue>In Progress:Adjust Commitment</literalValue>
        <name>CC Set Status to Adjust Commitment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Adjust_Pricing</fullName>
        <description>Contract Compliance Set Status to Adjust Pricing</description>
        <field>Status</field>
        <literalValue>In progress: Adjust Pricing</literalValue>
        <name>CC Set Status to Adjust Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Extend_Get_Well_Plan</fullName>
        <description>Contract Compliance Set Status to Extend Get Well Plan</description>
        <field>Status</field>
        <literalValue>In progress: Get Well Plan extended for IDN</literalValue>
        <name>CC Set Status to Extend Get Well Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Get_Well_Plan</fullName>
        <description>Contract Compliance Set Status to Get Well Plan</description>
        <field>Status</field>
        <literalValue>In progress: Get Well Plan (recovery)</literalValue>
        <name>CC Set Status to Get Well Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Review_Data_Integrity</fullName>
        <field>Status</field>
        <literalValue>In Progress: Review Data Integrity</literalValue>
        <name>CC Set Status to Review Data Integrity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Terminate_Contract</fullName>
        <description>Contract Compliance Set Status to Terminate Contract</description>
        <field>Status</field>
        <literalValue>In progress: Terminate Contract</literalValue>
        <name>CC Set Status to Terminate Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Status_to_Watch_and_Wait</fullName>
        <description>Contract Compliance Set Status to Watch and Wait</description>
        <field>Status</field>
        <literalValue>In progress: Watch and Wait</literalValue>
        <name>CC Set Status to Watch and Wait</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COE_Milestone_3_Owner</fullName>
        <field>Case_Owner_EQUALS_COE_Support__c</field>
        <literalValue>1</literalValue>
        <name>COE Milestone 3 Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COE_Milestone_3_Technical</fullName>
        <field>Functional_StageEQUALSTechnical_Analysis__c</field>
        <literalValue>1</literalValue>
        <name>COE Milestone 3 Technical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_MITG_Intake_Form_Update</fullName>
        <field>OwnerId</field>
        <lookupValue>US_MITG_LDF</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case MITG Intake Form Update</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_NASSC_update_case_assigned_analyst</fullName>
        <description>Workflow to update the case field Case_Assigned_Time_to_Analyst with the time that the case is enters in the NASSC queue</description>
        <field>Case_Assigned_Time_to_Analyst__c</field>
        <formula>IF(AND(contains(Owner:Queue.QueueName,&quot;Support - COMP&quot;),(IsClosed = FALSE)), NOW(), NULL)</formula>
        <name>Case- NASSC update case assigned analyst</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Assignment_RMS_CPQ</fullName>
        <field>OwnerId</field>
        <lookupValue>US_RMS_CPQ_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Assignment - RMS CPQ</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_Assignment_Sales_Analytics</fullName>
        <description>Assigns support cases for Me Tabs/Sales Analytics primary affected object to the Sales Analytics queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_RMS_Sales_Analytics</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner Assignment - Sales Analytics</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Case_Recordtype_to_DQ_Case</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Data_Quality_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Case Recordtype to DQ Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <description>Set the case status to closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case_via_Status</fullName>
        <description>Auto close production changed Cases to resolved</description>
        <field>Status</field>
        <literalValue>Closed-Resolved</literalValue>
        <name>Close Case via Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Resolve_Case</fullName>
        <field>Status</field>
        <literalValue>Closed-Resolved</literalValue>
        <name>Close Resolve Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Assign_Case_to_RMS_Comp_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_Respiratory</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NASSC Comp:Assign Case to RMS Comp Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Assign_Case_to_VT_Comp_Queue</fullName>
        <description>After manager approval, assign cases submitted by a VT user to the VT Queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_Vascular</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NASSC Comp: Assign Case to VT Comp Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DIS_Assign_to_COV_Claim_Engineer_KR</fullName>
        <field>OwnerId</field>
        <lookupValue>DIS_Warranty_Claim_Engineer_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>DIS-Assign to COV Claim Engineer - KR</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DIS_Assign_to_DIS_ADMIN_KR</fullName>
        <field>OwnerId</field>
        <lookupValue>DIS_ADMIN_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>DIS-Assign to DIS ADMIN - KR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DIS_Assign_to_DIS_COV_AROTS_KR</fullName>
        <field>OwnerId</field>
        <lookupValue>DIS_Warranty_Claim_AROTS_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>DIS-Assign to DIS COV AROTS - KR</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DIS_Assign_to_DIS_COV_Contact_KR</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Out_COV_Contact_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>DIS-Assign to DIS COV Contact - KR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DIS_Assign_to_DIS_TS_Coordinator_KR</fullName>
        <field>OwnerId</field>
        <lookupValue>DIS_Installation_TS_Coordinator_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>DIS-Assign to DIS TS Coordinator - KR</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Extend_Get_Well_Plan</fullName>
        <description>Update date for reminder notification</description>
        <field>Next_Reminder__c</field>
        <formula>Now() + 60</formula>
        <name>Extend Get Well Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>External_Requestor_Suture_Express_field</fullName>
        <description>Updates field with External</description>
        <field>Internal_External_Requestor__c</field>
        <literalValue>External</literalValue>
        <name>External Requestor Suture Express field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>External_Requestor_field_update</fullName>
        <description>Updates Internal/External field to External when it hits External queue.</description>
        <field>Internal_External_Requestor__c</field>
        <literalValue>External</literalValue>
        <name>External Requestor field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Functional_StatusEQUALSQuality_Review</fullName>
        <field>Functional_StatusEQUALSQuality_Review__c</field>
        <literalValue>1</literalValue>
        <name>Functional StatusEQUALSQuality Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Functional_StatusEQUALSTest_Validation</fullName>
        <field>Functional_StatusEQUALSTest_Validation__c</field>
        <literalValue>1</literalValue>
        <name>Functional StatusEQUALSTest / Validation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ILS_Update_Service_Due_Date_Field</fullName>
        <field>Service_Due_Date__c</field>
        <formula>Date__c</formula>
        <name>ILS Update Service Due Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ILS_Update_Service_Requested_Date_Field</fullName>
        <field>Service_Requested_Date__c</field>
        <formula>Date__c</formula>
        <name>ILS Update Service Requested Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Requestor</fullName>
        <description>Updates Internal/External Requester field to Internal when Web Email contains @medtronic.com or covidien.com</description>
        <field>Internal_External_Requestor__c</field>
        <literalValue>Internal</literalValue>
        <name>Internal Requestor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Milestone_4_Field_Update_Development</fullName>
        <field>Functional_StageEQUALSDevelopment__c</field>
        <literalValue>1</literalValue>
        <name>Milestone 4 Field Update Development</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Resolution_SLA_Alert</fullName>
        <field>Resolution_SLA_Alert__c</field>
        <literalValue>1</literalValue>
        <name>Resolution SLA Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Resolution_SLA_Missed</fullName>
        <field>Resolution_SLA_Missed__c</field>
        <literalValue>1</literalValue>
        <name>Resolution SLA Missed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Responded</fullName>
        <field>Response_Required__c</field>
        <literalValue>No</literalValue>
        <name>Responded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Response_SLA_Alert</fullName>
        <field>Response_SLA_Alert__c</field>
        <literalValue>1</literalValue>
        <name>Response SLA Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Response_SLA_Missed</fullName>
        <field>Response_SLA_Missed__c</field>
        <literalValue>1</literalValue>
        <name>Response SLA Missed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Root_Cause_on_Production_Change</fullName>
        <description>Set the Root Cause when auto closing production change cases</description>
        <field>Root_Cause_Analysis__c</field>
        <literalValue>Changes Logged in Production</literalValue>
        <name>Root Cause on Production Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUPPORT_NASSC_No_Mgr_Approval_Req_RMS</fullName>
        <description>If the requestor role= manager level, then the request can go straight to the comp queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_Respiratory</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SUPPORT: NASSC No Mgr Approval Req RMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUPPORT_NASSC_No_Mgr_Approval_Req_S2</fullName>
        <description>If the requestor role= manager level, then the request can go straight to the comp queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_US_Surgical_EbD</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SUPPORT: NASSC No Mgr Approval Req S2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUPPORT_NASSC_No_Mgr_Approval_Req_VT</fullName>
        <description>If the requestor role= manager level, then the request can go straight to the comp queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_Vascular</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SUPPORT: NASSC No Mgr Approval Req VT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Commitment_Adjustment_reminder_date1</fullName>
        <field>Next_Reminder__c</field>
        <formula>NOW()+45</formula>
        <name>Set Commitment Adjustment reminder date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_GBU</fullName>
        <description>Set the GBU field to &apos;Not Applicable&apos;</description>
        <field>GBU__c</field>
        <literalValue>Not Applicable</literalValue>
        <name>Set GBU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Get_Well_Plan_start_date</fullName>
        <description>set Get_Well_Plan_start_date to todays date</description>
        <field>Get_Well_Plan_start_date__c</field>
        <formula>Today()</formula>
        <name>Set Get_Well_Plan_start_date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Get_Well_reminder_date</fullName>
        <description>Set Get Well reminder date</description>
        <field>Next_Reminder__c</field>
        <formula>Now() + 90</formula>
        <name>Set Get Well reminder date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ITSM_Assignment_ID</fullName>
        <description>Sets the ITSM Assignment ID from the email that generated the Case</description>
        <field>ITSM_Assignment_ID__c</field>
        <formula>MID(Subject,13,7)</formula>
        <name>Set ITSM Assignment ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ITSM_Incident_ID</fullName>
        <description>Sets the ITSM Incident ID based on the email that generated the Case</description>
        <field>ITSM_Incident_ID__c</field>
        <formula>RIGHT(MID(Subject,(FIND(&quot;Incident#&quot;,Subject)),19),9)</formula>
        <name>Set ITSM Incident ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Integrity_Reminder_Date</fullName>
        <description>Set Integrity Reminder Date</description>
        <field>Next_Reminder__c</field>
        <formula>Now() + 6</formula>
        <name>Set Integrity Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Price_Adjustment_reminder_date</fullName>
        <description>Set Price Adjustment reminder date</description>
        <field>Next_Reminder__c</field>
        <formula>Now() + 30</formula>
        <name>Set Price Adjustment reminder date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Price_Adjustment_start_date</fullName>
        <field>Price_Adjustment_start_date__c</field>
        <formula>Today()</formula>
        <name>Set Price Adjustment start date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Price_Adjustment_start_date1</fullName>
        <field>Commitment_Adjustment_start_date__c</field>
        <formula>TODAY()</formula>
        <name>Set Price Adjustment start_date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Initiation_Date</fullName>
        <field>Project_Initiation_Date__c</field>
        <formula>Today()</formula>
        <name>Set Project Initiation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Stage_to_ACB_Pending_Review</fullName>
        <field>Project_Stage__c</field>
        <literalValue>ACB Pending Review</literalValue>
        <name>Set Project Stage to ACB Pending Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Stage_to_ACB_Project_Initiat</fullName>
        <field>Project_Stage__c</field>
        <literalValue>ACB Project Initiated</literalValue>
        <name>Set Project Stage to ACB Project Initiat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Stage_to_GGB_Pending_Review</fullName>
        <field>Project_Stage__c</field>
        <literalValue>GGB Pending Review</literalValue>
        <name>Set Project Stage to GGB Pending Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Stage_to_RMCCB_Pending_Migra</fullName>
        <field>Project_Stage__c</field>
        <literalValue>RMCCB Pending Migration</literalValue>
        <name>Set Project Stage to RMCCB Pending Migra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Team_to_ACB</fullName>
        <field>Project_Team__c</field>
        <literalValue>ACB</literalValue>
        <name>Set Project Team to ACB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Team_to_GGB</fullName>
        <field>Project_Team__c</field>
        <literalValue>GGB</literalValue>
        <name>Set Project Team to GGB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Project_Team_to_RMCCB</fullName>
        <field>Project_Team__c</field>
        <literalValue>RMCCB</literalValue>
        <name>Set Project Team to RMCCB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Public_Case_Comment_Checkbox_to_Fals</fullName>
        <description>Rule to set Public Case Comment tickbox to false, after email alert has been dispatched.</description>
        <field>New_Public_Case_Comment__c</field>
        <literalValue>0</literalValue>
        <name>Set Public Case Comment Checkbox to Fals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Requested_by_ITSM_email</fullName>
        <description>Grabs the value of the users email from the description field.</description>
        <field>Requested_By_Email__c</field>
        <formula>MID(Description,FIND(&quot;Email: &quot;,Description,1)+7,(FIND(&quot;@covidien.com&quot;,LOWER(Description),FIND(&quot;Email: &quot;,Description,1))-(FIND(&quot;Email: &quot;,Description,1)))+7)</formula>
        <name>Set Requested by ITSM email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Review_data_integrity_Date</fullName>
        <description>set Review_data_integrity_Date to todays date</description>
        <field>Review_data_integrity_Date__c</field>
        <formula>Today()</formula>
        <name>Set Review_data_integrity_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Assigned</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Set Status to Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_TEMP_address_for_Korea</fullName>
        <field>TEMP_Account_Address__c</field>
        <formula>Account.BillingStreet+&apos;, &apos;+Account.BillingCity+&apos;, &apos;+Account.BillingState+&apos;, &apos;+Account.BillingPostalCode+&apos;, &apos;+Account.BillingCountry</formula>
        <name>Set TEMP address for Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Terminate_Contract_reminder_date</fullName>
        <description>Set Terminate Contract reminder date</description>
        <field>Next_Reminder__c</field>
        <formula>Now() + 30</formula>
        <name>Set Terminate Contract reminder date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Terminate_Contract_start_date</fullName>
        <field>Terminate_Contract_start_date__c</field>
        <formula>Today()</formula>
        <name>Set Terminate_Contract_start_date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Watch_and_Wait_reminder_date</fullName>
        <description>Set Watch and Wait reminder date</description>
        <field>Next_Reminder__c</field>
        <formula>Now() + 60</formula>
        <name>Set Watch and Wait reminder date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Watch_and_Wait_start_date</fullName>
        <field>Watch_and_Wait_start_date__c</field>
        <formula>Today()</formula>
        <name>Set Watch and Wait start date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Name_text</fullName>
        <field>Account_Name_text__c</field>
        <formula>Account.Name</formula>
        <name>Support - Update Account Name (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Number_text</fullName>
        <field>Account_Number_text__c</field>
        <formula>Account.Account_External_ID__c</formula>
        <name>Support - Update Account Number (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver</fullName>
        <field>Approver__c</field>
        <lookupValue>graham.fyffe@covidien.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Graham Fyffe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Anna_Testa</fullName>
        <field>Approver__c</field>
        <lookupValue>anna.testa@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Anna Testa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Derek_Carless</fullName>
        <field>Approver__c</field>
        <lookupValue>chenda.phen@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver - Derek Carless</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_EM_Region</fullName>
        <field>Approver__c</field>
        <lookupValue>judith.randall@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-EM Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_EU_MS</fullName>
        <field>Approver__c</field>
        <lookupValue>judith.randall@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-EU MS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Gethin_Davies_Jones</fullName>
        <field>Approver__c</field>
        <lookupValue>carlo.gomes@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Corinna Zachariou</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Hans_Kresinski</fullName>
        <field>Approver__c</field>
        <lookupValue>hans.r.kresinski@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Hans Kresinski</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Jeff_Fryer</fullName>
        <field>Approver__c</field>
        <lookupValue>jeff.fryer@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Jeff Fryer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Marco_Van_Kleef</fullName>
        <field>Approver__c</field>
        <lookupValue>carlo.gomes@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-EMEA RMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Michael_Vannier</fullName>
        <field>Approver__c</field>
        <lookupValue>michael.t.vannier@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Michael Vannier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Paul_Asprey</fullName>
        <field>Approver__c</field>
        <lookupValue>carlo.gomes@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Paul Asprey</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_Terry_Callahan</fullName>
        <field>Approver__c</field>
        <lookupValue>david.monteiro@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver-Terry Callahan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_US_Health_Systems</fullName>
        <field>Approver__c</field>
        <lookupValue>hans.r.kresinski@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver- US Health Systems</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_for_EU</fullName>
        <field>Approver__c</field>
        <lookupValue>carlo.gomes@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver for EU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_when_Region_Asia</fullName>
        <field>Approver__c</field>
        <lookupValue>christina.teng@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver when Region=Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver_when_Region_Japan</fullName>
        <field>Approver__c</field>
        <lookupValue>naoya.kotani@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Approver when Region=Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Country_of_Account</fullName>
        <field>Account_Country_Code__c</field>
        <formula>Account.BillingCountry</formula>
        <name>Update Billing Country of Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_CN_AROTS_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>DIS_Warranty_Claim_AROTS_CN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to CN AROTS Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_KR_AROTS_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>DIS_Warranty_Claim_AROTS_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to KR AROTS Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_ApprovalInProgress</fullName>
        <field>Status</field>
        <literalValue>Approval In Progress</literalValue>
        <name>Update Case Status to ApprovalInProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Processing</fullName>
        <field>Status</field>
        <literalValue>Processing</literalValue>
        <name>Update Case Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Case Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Submitted</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Update Case Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Email_text_with_Linked</fullName>
        <description>Updates the Contact E-mail (text) with the linked Contact Email field</description>
        <field>Contact_Email_text__c</field>
        <formula>Contact.Email</formula>
        <name>Support - Update Contact Email (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Email_text_with_Web_Ema</fullName>
        <field>Contact_Email_text__c</field>
        <formula>SuppliedEmail</formula>
        <name>Support - Update Contact Email (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Email_with_Web_Email</fullName>
        <field>Contact_Email_text__c</field>
        <formula>SuppliedEmail</formula>
        <name>Update Contact Email (text) with Web Ema</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Full_Name_text</fullName>
        <field>Contact_Name_text__c</field>
        <formula>Contact.Full_Name_with_Salutation__c</formula>
        <name>Support - Update Contact Name (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Name_text_with_Web_Name</fullName>
        <field>Contact_Name_text__c</field>
        <formula>SuppliedName</formula>
        <name>Support - Update *Contact w Web Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Phone_text</fullName>
        <field>Contact_Phone_text__c</field>
        <formula>Contact.Phone</formula>
        <name>Support - Update Contact Phone (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EMEA_Email_Case_Origin</fullName>
        <description>Update EMEA email-to-case case origin to Email</description>
        <field>Origin</field>
        <literalValue>Email</literalValue>
        <name>Update EMEA Email Case Origin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>US_RMS_Contract_Inquiry</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_ACB</fullName>
        <description>Update the Project Owner to ACB</description>
        <field>OwnerId</field>
        <lookupValue>COE_Architects</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Project Owner ACB</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_COE_Quality_Analyst</fullName>
        <field>OwnerId</field>
        <lookupValue>CoE_Quality_Analysts</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Project Owner COE Quality Analyst</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_COE_Support</fullName>
        <field>OwnerId</field>
        <lookupValue>COESupport189961</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Project Owner COE Support</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_GGB</fullName>
        <field>OwnerId</field>
        <lookupValue>Global_Governance_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Project Owner Global Governance</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_Release_MGMT</fullName>
        <field>OwnerId</field>
        <lookupValue>COE_Release_Management</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Project Owner Release MGMT</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_Steering_Committee</fullName>
        <field>OwnerId</field>
        <lookupValue>Steering_Committee</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Project Owner Steering Committee</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Score</fullName>
        <field>Total_Project_Score__c</field>
        <formula>Competitive_Advantage_Score__c + Strategic_Alignment_Score__c + Compliance_Score__c + Financial_Score__c + Process_Improvement_Score__c + Process_System_Application_Score__c + Span_of_Use_Score__c + Project_Risk_Score__c + Enhancement_Score__c + Level_of_IS_Effort_Score__c</formula>
        <name>Update Project Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Status_to_In_Review</fullName>
        <description>On Case Creation and Assignment to COE Support, the Stage should also be set to In Review</description>
        <field>Project_Stage__c</field>
        <literalValue>RMCCB In Review</literalValue>
        <name>Update Project Status to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Inquiry_Owner_PR_Queue</fullName>
        <description>assigned case owner to Support - Comp - Patient Recovery queue.</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_Patient_Recovery</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Sales Inquiry Owner PR Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Inquiry_Owner_RMS_Queue</fullName>
        <description>Assigned case owner to Support_COMP_Respiratory</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_Respiratory</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Sales Inquiry Owner RMS Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Inquiry_Owner_SI_Queue</fullName>
        <description>Assigned case owner to Support_COMP_US_Surgical_EbD queue.</description>
        <field>OwnerId</field>
        <lookupValue>Support_COMP_US_Surgical_EbD</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Sales Inquiry Owner SI Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_based_on_Record_Type</fullName>
        <field>Subject</field>
        <formula>CASE( 
RecordType.Name , &quot;Warranty Claim Request&quot;, RecordType.Name  &amp; &quot;: &quot; &amp;  TEXT(Model_Number__c) &amp; &quot; | &quot; &amp;  Serial_Number__c  , &quot;Equipment Installation Request&quot;,
RecordType.Name, &quot;Upload Invoice Request&quot;, Subject , &quot;New Account Request&quot;, RecordType.Name  &amp; &quot;: &quot; &amp;  Account.Name,RecordType.Name)</formula>
        <name>Update Subject based on Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Warehouse_Responded</fullName>
        <field>Status</field>
        <literalValue>In Process</literalValue>
        <name>Warehouse Responded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Warehouse_Responded_From</fullName>
        <field>Awaiting_Response_From__c</field>
        <name>Warehouse Responded - From</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACB Approval No GGB Required</fullName>
        <actions>
            <name>Set_Project_Initiation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Project_Stage_to_ACB_Project_Initiat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If all GGB Questions are answered &quot;NO&quot; then the project does not have to go to the GGB. The Project should stay with the ACB and be placed into Project Initiated Status</description>
        <formula>AND(    RecordType.Name = &apos;Project&apos;,     TEXT(Project_Team__c) = &apos;ACB&apos;,    TEXT(Project_Stage__c) = &apos;ACB Initial Approval&apos;,    TEXT(Is_COE_Resource_Required__c) = &apos;No&apos;,    TEXT(Is_Funding_Required__c) = &apos;No&apos;,    TEXT(Does_Project_Have_Global_Impact__c)= &apos;No&apos;    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACB Approval to GGB Pending Review</fullName>
        <actions>
            <name>Set_Project_Stage_to_GGB_Pending_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Project_Team_to_GGB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Owner_GGB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If all GGB Questions are answered &quot;NO&quot; then the project does not have to go to the GGB</description>
        <formula>AND(    RecordType.Name = &apos;Project&apos;,     TEXT(Project_Team__c) = &apos;ACB&apos;,    TEXT(Project_Stage__c) = &apos;ACB Initial Approval&apos;, OR(TEXT(Is_COE_Resource_Required__c) = &apos;Yes&apos;,    TEXT(Is_Funding_Required__c) = &apos;Yes&apos;,    TEXT(Does_Project_Have_Global_Impact__c)= &apos;Yes&apos;    ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACB Final Approval to GGB RMCCB</fullName>
        <actions>
            <name>Set_Project_Stage_to_RMCCB_Pending_Migra</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Project_Team_to_RMCCB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Owner_Release_MGMT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>ACB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Stage__c</field>
            <operation>equals</operation>
            <value>ABC Final Approval</value>
        </criteriaItems>
        <description>Once ACB Select Final Approval, the Project will automatically be routed back to RMCCB and placed in &quot;RMCCB Pending Migration&quot; Status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ANZ Alert to Steve Capel</fullName>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>jeff.fryer@covidien.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Stevan Petrofsky</value>
        </criteriaItems>
        <description>Australia requests that Steve Capel be informed when any case from Jeff Fryer of ANZ is created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto close Production Change type Cases</fullName>
        <actions>
            <name>Close_Case_via_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Root_Cause_on_Production_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Production Change</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Support</value>
        </criteriaItems>
        <description>When a Case is logged as a Production Change type case the case is automatically closed via field updates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto-Close Cases</fullName>
        <actions>
            <name>Auto_Close_Cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ind Contracts Case Feed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Email Acknowledgement</value>
        </criteriaItems>
        <description>Closes unnecessary cases triggered by generic email acknowledgements. For Ind Contracts Case Feed record type.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CC Adjust Commitment is checked</fullName>
        <actions>
            <name>CC_Set_Status_to_Adjust_Commitment1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Commitment_Adjustment_reminder_date1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Price_Adjustment_start_date1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Adjust_Commitment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract Compliance Adjust Commitment is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Adjust Pricing checked</fullName>
        <actions>
            <name>CC_Set_Status_to_Adjust_Pricing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Price_Adjustment_reminder_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Price_Adjustment_start_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Adjust_Pricing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract Compliance Adjust Pricing is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Extend Get Well Plan checked</fullName>
        <actions>
            <name>CC_Set_Status_to_Extend_Get_Well_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Extend_Get_Well_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Extend_Get_Well_Plan__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract Compliance Extend Get Well Plan checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Get Well Plan checked</fullName>
        <actions>
            <name>CC_Set_Status_to_Get_Well_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Get_Well_Plan_start_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Get_Well_reminder_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Get_Well_Plan_recovery__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract Compliance Get Well Plan is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Set Review data integrity Date</fullName>
        <actions>
            <name>Out_of_Compliance_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CC_Set_Status_to_Review_Data_Integrity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Integrity_Reminder_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Review_data_integrity_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Review_data_integrity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract compliance set Review_data_integrity_Date when Review_data_integrity is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Terminate Contract checked</fullName>
        <actions>
            <name>CC_Set_Status_to_Terminate_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Terminate_Contract_reminder_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Terminate_Contract_start_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Terminate_Contract__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract Compliance Terminate Contract checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Watch and Wait checked</fullName>
        <actions>
            <name>CC_Set_Status_to_Watch_and_Wait</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Watch_and_Wait_reminder_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Watch_and_Wait_start_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Watch_and_Wait__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Contract Compliance Watch and Wait checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Milestone 3</fullName>
        <actions>
            <name>COE_Milestone_3_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>COE_Milestone_3_Technical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CoE - Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Technical Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Milestone 4</fullName>
        <actions>
            <name>Milestone_4_Field_Update_Development</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Development</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Milestone 5</fullName>
        <actions>
            <name>Functional_StatusEQUALSQuality_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Functional_Role__c</field>
            <operation>equals</operation>
            <value>Quality Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Milestone 6</fullName>
        <actions>
            <name>Functional_StatusEQUALSTest_Validation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Functional_Role__c</field>
            <operation>equals</operation>
            <value>Test / Validation Ready</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Assigned to Queue</fullName>
        <actions>
            <name>Acceptance_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Owner_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Assignment - RMS CPQ</fullName>
        <actions>
            <name>Case_Owner_Assignment_RMS_CPQ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Object__c</field>
            <operation>equals</operation>
            <value>CPQ (Configure-Price-Quote)</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_By_Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Assignment - RMS Sales Analytics</fullName>
        <actions>
            <name>Case_Owner_Assignment_Sales_Analytics</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Object__c</field>
            <operation>equals</operation>
            <value>Me Tab/Sales Analytic</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Patient Monitoring &amp; Recovery (PMR)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Request_By_Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Close Notify Created By User</fullName>
        <actions>
            <name>SUPPORT_CRM_Case_Closed_CreatedBy</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If Requested By &lt;&gt; Created By username then send email to Created By when a case is closed</description>
        <formula>And ( IF( Requested_By__r.Username   &lt;&gt; CreatedBy.Username, True , False ), IF ($RecordType.Name  = &quot;CRM Support&quot;, True, False),  IsClosed )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed notify to Requested By</fullName>
        <actions>
            <name>SUPPORT_CRM_Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Support</value>
        </criteriaItems>
        <description>Workflow to dispatch an email confirming a CRM support case has been closed to the Requested By user</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case MITG Intake Form</fullName>
        <actions>
            <name>Case_MITG_Intake_Form_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>MITG Intake Request</value>
        </criteriaItems>
        <description>Update case own to MITG LDF based on record type and currency</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Project Total Score Update</fullName>
        <actions>
            <name>Update_Project_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <description>Sum the Total Scorecard for the Case Project</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Closed</fullName>
        <actions>
            <name>Case_has_been_closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(
ISPICKVAL(Status, &quot;Closed-Resolved&quot;),
ISPICKVAL(Status, &quot;Closed - Resolved&quot;),
ISPICKVAL(Status, &quot;Closed-Future&quot;),
ISPICKVAL(Status, &quot;Closed - Future&quot;),
ISPICKVAL(Status, &quot;Closed-Duplicate&quot;),
ISPICKVAL(Status, &quot;Closed - Duplicate&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Case When Type Is Notification</fullName>
        <actions>
            <name>Close_Resolve_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Notification</value>
        </criteriaItems>
        <description>Case status is set to closed - Resolved when case type is Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close if from invalid origin</fullName>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_GBU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - GPO Approvals Address</value>
        </criteriaItems>
        <description>If the case origin is &quot;Email - GPO Approvals Address&quot; automatically close the case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Compliance Case 30 Day Inactivity</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agreement Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>&quot;In progress: Case accepted, in review&quot;,New</value>
        </criteriaItems>
        <description>Compliance Case Owner Notified 30 days After Case Creation if no activity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X30_Day_Inactivity_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Compliance Case 60 Day Inactivity</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agreement Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>&quot;In progress: Case accepted, in review&quot;,New</value>
        </criteriaItems>
        <description>Compliance Case Owner Notified 60 days After Case Creation if no activity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X60_Day_Inactivity_Escalation</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Payment_Received_Date_Time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Compliance Case Reminder Alert</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agreement Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Compliance_Case_Reminder_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Next_Reminder__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Created or Edited by GBU user</fullName>
        <actions>
            <name>Set_Status_to_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$User.ProfileId = &quot;00eU0000000dWzl&quot;  /* Profile: GBU Administrator */ &amp;&amp; NOT( ISPICKVAL( Status , &quot;Assigned&quot; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Equipment Installation to DIS TS Coordinator - KR</fullName>
        <actions>
            <name>DIS_Assign_to_DIS_TS_Coordinator_KR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Equipment Installation Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <description>Assign case to COV TS Coodinator - KR Queue when Status is updated to &apos;Submitted&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Invoice Uploaded - Assign to DIS COV CONTACT - KR</fullName>
        <actions>
            <name>DIS_Assign_to_DIS_COV_Contact_KR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Upload Invoice Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <description>Assign case to Sales Out-COV Contact Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Update Subject based on Record Type</fullName>
        <actions>
            <name>Update_Subject_based_on_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Default the Subject as Record Type and other info</description>
        <formula>ISNEW()|| ( (RecordType.Name = &quot;Warranty Claim Request&quot; &amp;&amp;  (ISCHANGED( Model_Number__c ) ||  ISCHANGED(Serial_Number__c)))|| (RecordType.Name = &quot;Equipment Installation Request&quot;) || (RecordType.Name = &quot;New Account Request&quot; &amp;&amp;  ISCHANGED(  AccountId))|| (RecordType.Name = &quot;Upload Invoice Request&quot; &amp;&amp;  ISCHANGED(   Submission__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Warranty Claim Request- Assign to DIS ARTOS - KR</fullName>
        <actions>
            <name>DIS_Assign_to_DIS_COV_AROTS_KR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Warranty Claim Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Processing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <description>Assign case to DIS Warranty Claim AROTS - KR Queue when Status is updated to &apos;Processing&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIS-Warranty Claim Request- Assign to DIS Warranty Claim Engineer - KR</fullName>
        <actions>
            <name>DIS_Assign_to_COV_Claim_Engineer_KR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Warranty Claim Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <description>Assign case to DIS Warranty Claim Engineer - KR Queue when Status is updated to &apos;Submitted&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIST-EmailUpdatedAccountId_CN</fullName>
        <actions>
            <name>SellTo_Code_Update_Message_CN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Created_By_Country__c</field>
            <operation>equals</operation>
            <value>CN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notContain</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Send an email to the distributor to notify them that the temp AccountId has been replaced with a permanent AccountId</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIST-EmailUpdatedAccountId_HK</fullName>
        <actions>
            <name>SellTo_Code_Update_Message_HK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Created_By_Country__c</field>
            <operation>equals</operation>
            <value>HK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notContain</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Send an email to the distributor to notify them that the temp AccountId has been replaced with a permanent AccountId</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIST-EmailUpdatedAccountId_KR</fullName>
        <actions>
            <name>SellTo_Code_UpdateMessage_KR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SellTo_Code_Update_Message_KR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Created_By_Country__c</field>
            <operation>equals</operation>
            <value>KR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notContain</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Send an email to the distributor to notify them that the temp AccountId has been replaced with a permanent AccountId</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIST-EmailUpdatedAccountId_MO</fullName>
        <actions>
            <name>SellTo_Code_Update_Message_MO</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Created_By_Country__c</field>
            <operation>equals</operation>
            <value>MO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notContain</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Send an email to the distributor to notify them that the temp AccountId has been replaced with a permanent AccountId</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIST-EmailUpdatedAccountId_SA</fullName>
        <actions>
            <name>SellTo_Code_Update_Message_SA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Created_By_Country__c</field>
            <operation>equals</operation>
            <value>SA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notContain</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Send an email to the distributor to notify them that the temp AccountId has been replaced with a permanent AccountId</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DIST-EmailUpdatedAccountId_TW</fullName>
        <actions>
            <name>SellTo_Code_Update_Message_TW</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Created_By_Country__c</field>
            <operation>equals</operation>
            <value>TW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Temporary_Account_Number__c</field>
            <operation>notContain</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Send an email to the distributor to notify them that the temp AccountId has been replaced with a permanent AccountId</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DQ Case Closed</fullName>
        <actions>
            <name>DQ_Case_Notify_Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Data Quality,Data Integrity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed-Future,Closed-Resolved,Closed - Duplicate,Closed,Closed - Future,Closed - Resolved,Closed-Duplicate</value>
        </criteriaItems>
        <description>DQ Case: Notify Case Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email To Case%3A Extract ITSM IDs from Description Field</fullName>
        <actions>
            <name>Set_ITSM_Assignment_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_ITSM_Incident_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Requested_by_ITSM_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>covidienisservicedesk@covidien.com</value>
        </criteriaItems>
        <description>Extracts the ITSM Incident and Assignment ID from the body of the email that generated the case.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Emailtocase ITSM%3A Notify Case Closed</fullName>
        <actions>
            <name>ITSM_Closed_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed-Resolved,Closed-Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ITSM_Assignment_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ITSM_Incident_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>ITSM: Notify Case Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalation alert</fullName>
        <actions>
            <name>Notify_Managers_when_the_Escalated_Box_is_checked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Escalation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Support</value>
        </criteriaItems>
        <description>If the Escalation box is checked on a case send email to management</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>External Requestor Suture Express field update</fullName>
        <actions>
            <name>External_Requestor_Suture_Express_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ind Contracts Case Feed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>contains</operation>
            <value>@sutureexpress.com</value>
        </criteriaItems>
        <description>This rule updates the Internal/External Requestor field based on the queue that it hits.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>External Requestor field update</fullName>
        <actions>
            <name>External_Requestor_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ind Contracts Case Feed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>@medtronic.com,@sutureexpress.com</value>
        </criteriaItems>
        <description>This rule updates the Internal/External Requestor field based on the queue that it hits.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GGB Approval to ACB Project Initiated</fullName>
        <actions>
            <name>Set_Project_Initiation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Project_Stage_to_ACB_Project_Initiat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Project_Team_to_ACB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Owner_ACB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>GGB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Stage__c</field>
            <operation>equals</operation>
            <value>GGB Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ILS Case is Created Notification</fullName>
        <actions>
            <name>Send_Alert_to_Case_Owner_and_ILS_Sales_Rep</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Technical Service Case - ILS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Type__c</field>
            <operation>notEqual</operation>
            <value>Due</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>To Be Scheduled</value>
        </criteriaItems>
        <description>Notify Case Owner and Account Manager when a new Technical Service case has been created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ILS Technical Service Support Case Update SR</fullName>
        <actions>
            <name>ILS_Service_Requested_Alert_to_Owner_and_Rep</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Account Manager when an existing case Owner is changed or Date Type changed to “Service Requested” and Record Type=Technical Service Case –ILS.</description>
        <formula>ISCHANGED( Date_Type__c )  &amp;&amp;  ( ISPICKVAL( Date_Type__c , &quot;Service Requested&quot;))  &amp;&amp;  RecordTypeId =&quot;012n00000004hWA&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ILS Technical Service Support Case is Updated</fullName>
        <actions>
            <name>ILS_Notify_Account_Manager_Case_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Account Manager when an existing case Owner is changed or Date Type changed to “Scheduled” or “Completed” and Record Type=Technical Service Case –ILS.</description>
        <formula>ISCHANGED( Date_Type__c )  &amp;&amp;  ( ISPICKVAL( Date_Type__c , &quot;Scheduled&quot;)  ||  ISPICKVAL( Date_Type__c , &quot;Complete&quot;) )  &amp;&amp;  RecordTypeId =&quot;0120B000000N7yL&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ILS Update Service Due Date</fullName>
        <actions>
            <name>ILS_Update_Service_Due_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Date_Type__c</field>
            <operation>equals</operation>
            <value>Due</value>
        </criteriaItems>
        <description>When Date type=Due, update Service Due Date field with Date field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ILS Update Service Requested Date</fullName>
        <actions>
            <name>ILS_Update_Service_Requested_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Date_Type__c</field>
            <operation>equals</operation>
            <value>Service Requested</value>
        </criteriaItems>
        <description>When Date type=Service Requested, update Service Requested Date field with Date field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Internal Requestor field update</fullName>
        <actions>
            <name>Internal_Requestor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ind Contracts Case Feed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>contains</operation>
            <value>@medtronic.com,@covidien.com</value>
        </criteriaItems>
        <description>This rule updates the Internal/External Requestor field based on the queue that it hits.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Korea TEMP fields population</fullName>
        <actions>
            <name>Set_TEMP_address_for_Korea</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Account Creation Case of Korea</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Creation Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TEMP_Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>ASIA-Healthcare Facility</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MITG Intake Request Development Lead</fullName>
        <actions>
            <name>MITG_Intake_Request_Development_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(  Development_Lead__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MITG Intake Request Training Lead</fullName>
        <actions>
            <name>MITG_Intake_Request_Training_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Training_Lead__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notifications</fullName>
        <actions>
            <name>DQ_Case_Notify_Requested_By_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_Case_Recordtype_to_DQ_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the requestor; Also updates the Case RecordType based on owner.</description>
        <formula>OR (AND (CONTAINS(Owner:User.Username,&apos;Data Quality&apos;), RecordType.Name &lt;&gt; &apos;Account Creation Case&apos;, RecordType.Name &lt;&gt; &apos;User Submitted Healthcare Facility&apos;, CreatedBy.Country &lt;&gt; &apos;AU&apos;, CreatedBy.Country &lt;&gt; &apos;NZ&apos; ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Email Notifications With Case Queue</fullName>
        <actions>
            <name>Notify_DQ_Queue_member</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Asia - China - Data Quality,Asia - Hong Kong - Data Quality,Asia - Korea - Data Quality,ASIA - Singapore - Data Quality</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Compliance Case Notification</fullName>
        <actions>
            <name>Compliance_Case_Created_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agreement Compliance</value>
        </criteriaItems>
        <description>Process Builder Bug prevents new case notifications from going out when case is created automatically. This will be a temp fix until SFDC addresses isseu</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Requested By on Case Open or Change</fullName>
        <actions>
            <name>Notify_Requested_By_user_on_case_creation_or_when_requested_by_added_to_case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify the Requested By user when the name is not the same as the Created By user or when requested by has been added later</description>
        <formula>And (Requested_By__c&lt;&gt;CreatedById, $RecordType.Name  = &quot;CRM Support&quot;, not(isblank(Requested_By__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OEM Case Closed Notification Rule</fullName>
        <actions>
            <name>OEM_Case_Closed_Send_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed,Closed - Resolved,Closed-Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>OEM Integration,OEM Test Tools,OEM Customer Issues,OEM NRE,OEM Connectivity,OEM Non Specific</value>
        </criteriaItems>
        <description>Sends a mail when an OEM Case is closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PR Sales Inquiry Email Notification</fullName>
        <actions>
            <name>Send_Notification_Email_to_Manger_PR_Sales_Inquiry</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Notification_Email_to_Requested_By_PR_Sales_Inquiry</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Sales_Inquiry_Owner_PR_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Inquiry PR</value>
        </criteriaItems>
        <description>Send notification email to the manager and assigned case to a queue Support – Comp – Patient_Recovery.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Public Case Comment Checkbox</fullName>
        <actions>
            <name>SUPPORT_New_Public_Case_Comment_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Public_Case_Comment_Checkbox_to_Fals</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>crm support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.New_Public_Case_Comment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Rule to send email to Requested by User name and to untick public case comments tick box after email with comments has been dispatched except if the Case Creator is the same one making the public comment (then don&apos;t fire)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Public Comment Send Second Email</fullName>
        <actions>
            <name>SUPPORT_New_Public_Case_Comment_Alert_To_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If Requested By &lt;&gt; Created By username then send email to Created By for new public case comments and crm support record type</description>
        <formula>And ( IF( Requested_By__r.Username   &lt;&gt; CreatedBy.Username, True , False ),  IF ($RecordType.Name  = &quot;CRM Support&quot;, True, False), IF ( New_Public_Case_Comment__c , True, False) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RMCCB Approval to ACB Pending Review</fullName>
        <actions>
            <name>Set_Project_Stage_to_ACB_Pending_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Project_Team_to_ACB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Owner_ACB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>RMCCB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Stage__c</field>
            <operation>equals</operation>
            <value>RMCCB Approved</value>
        </criteriaItems>
        <description>When the Release Management team sets the Stage to RMCCB Approved, this workflow will automatically switch the case over to the ACB Team and set to ACB Pending Review</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMS Sales Inquiry Email Notification</fullName>
        <actions>
            <name>Send_Notification_Email_to_Manger_SI_Sales_Inquiry</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Notification_Email_to_Requested_By_SI_Sales_Inquiry</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Sales_Inquiry_Owner_RMS_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <description>Send notification email to the manager and assign case to a queue Support - COMP - Respiratory</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SI Sales Inquiry Email Notification</fullName>
        <actions>
            <name>Send_Notification_Email_to_Manger_SI_Sales_Inquiry</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Notification_Email_to_Requested_By_SI_Sales_Inquiry</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Sales_Inquiry_Owner_SI_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Inquiry SI</value>
        </criteriaItems>
        <description>Send notification email to the manager and assigned case to a queue Support_COMP_US_Surgical_EbD</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SUPPORT%3A NASSC Compensation Case Closed notify requestor and case team</fullName>
        <actions>
            <name>NASSC_Support_Send_Email_to_Requestor_and_Case_team_when_case_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Inquiry,Sales Inquiry PR,Sales Inquiry SI</value>
        </criteriaItems>
        <description>Workflow to dispatch an email confirming a sales inquiry case has been closed.  Email goes to case team as well as requestor</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SUPPORT%3A NASSC No Mgr Approval Req RMS</fullName>
        <actions>
            <name>Support_RMS_Mgr_Email_Notification_Case_received</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SUPPORT_NASSC_No_Mgr_Approval_Req_RMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Requestor_Role__c</field>
            <operation>equals</operation>
            <value>Manager Level</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <description>If the requestor role=manager, submit directly to the compensation team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SUPPORT%3A NASSC No Mgr Approval Req S2</fullName>
        <actions>
            <name>Support_S2_Mgr_Email_Notification_Case_received</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SUPPORT_NASSC_No_Mgr_Approval_Req_S2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Requestor_Role__c</field>
            <operation>equals</operation>
            <value>Manager Level</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>S2,SD,EbD</value>
        </criteriaItems>
        <description>:  If the requestor role=manager, submit directly to the compensation team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SUPPORT%3A NASSC No Mgr Approval Req VT</fullName>
        <actions>
            <name>Support_VT_Mgr_Email_Notification_Case_received</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SUPPORT_NASSC_No_Mgr_Approval_Req_VT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Requestor_Role__c</field>
            <operation>equals</operation>
            <value>Manager Level</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>VT</value>
        </criteriaItems>
        <description>If the requestor role=manager, submit directly to the compensation team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SUPPORT- NASSC Support Queue Handling time</fullName>
        <actions>
            <name>Case_NASSC_update_case_assigned_analyst</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>support</value>
        </criteriaItems>
        <description>Updates field Case_Assigned_Time_to_Analyst with the time that it takes for a case to close once assigned to the sales inquery queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status is changed</fullName>
        <actions>
            <name>Case_Status_has_been_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp; ( ISPICKVAL(Status, &quot;Assigned&quot;) || ISPICKVAL(Status, &quot;Closed-Future&quot;) || ISPICKVAL(Status, &quot;Closed - Future&quot;) || ISPICKVAL(Status, &quot;Global Lead Review&quot;) || ISPICKVAL(Status, &quot;Escalated&quot;) || ISPICKVAL(Status, &quot;Closed - Promoted to Change Request&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status is changed and case is not closed</fullName>
        <actions>
            <name>Case_Status_has_been_changed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( ISCHANGED(Status), NOT( OR( ISPICKVAL(Status, &quot;Closed-Resolved&quot;), ISPICKVAL(Status, &quot;Closed - Resolved&quot;), ISPICKVAL(Status, &quot;Closed-Future&quot;), ISPICKVAL(Status, &quot;Closed - Future&quot;), ISPICKVAL(Status, &quot;Closed-Duplicate&quot;), ISPICKVAL(Status, &quot;Closed - Duplicate&quot;) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Account Name</fullName>
        <actions>
            <name>Update_Account_Name_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Account Name from the linked Account Name to the Account Name (text) for reporting purposes.</description>
        <formula>LEN ( Account.Name ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Account Number</fullName>
        <actions>
            <name>Update_Account_Number_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Covidien Account Number from the linked Account to the Account Number (text) for reporting purposes.</description>
        <formula>LEN (  Account.Account_External_ID__c  ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Contact E-mail</fullName>
        <actions>
            <name>Update_Contact_Email_text_with_Linked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Contact Email from the linked Contact to the Contact Email (text) for reporting purposes.</description>
        <formula>LEN(Contact.Email) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Contact Name</fullName>
        <actions>
            <name>Update_Contact_Full_Name_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Contact Name from the linked Contact Name to the Contact Name (text) for reporting purposes.</description>
        <formula>LEN (   TRIM(Contact.Full_Name_with_Salutation__c  )) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Contact Phone</fullName>
        <actions>
            <name>Update_Contact_Phone_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Contact Phone from the linked Contact to the Contact Phone (text) for reporting purposes.</description>
        <formula>LEN( Contact.Phone ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Web Email</fullName>
        <actions>
            <name>Update_Contact_Email_with_Web_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the contents of the Web Email to editable field.</description>
        <formula>AND ( LEN ( Contact.FirstName &amp; Contact.FirstName ) = 0 ,  LEN ( SuppliedEmail ) &gt; 0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Web Name</fullName>
        <actions>
            <name>Update_Contact_Name_text_with_Web_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the contents of the Web Name to editable field.</description>
        <formula>AND ( LEN ( Contact.FirstName &amp; Contact.FirstName ) = 0 ,  LEN (SuppliedName) &gt; 0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support - Transfer Count</fullName>
        <actions>
            <name>Aggregate_Transfer_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Aggregate the Transfer Count by 1 every time that a record changes owner or queue.</description>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Country on Case</fullName>
        <actions>
            <name>Update_Billing_Country_of_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DData Integration in US</fullName>
        <actions>
            <name>Update_Approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>Data Integration</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DEBD  in EU</fullName>
        <actions>
            <name>Update_Approver_Paul_Asprey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>EU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>SD,S2,EbD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DEBD%2C SD in US</fullName>
        <actions>
            <name>Update_Approver_Derek_Carless</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>EbD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>SD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DHealth Systems in US</fullName>
        <actions>
            <name>Update_Approver_US_Health_Systems</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>Health Systems</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DMS in EU</fullName>
        <actions>
            <name>Update_Approver_EU_MS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>EU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>MS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DMS in US</fullName>
        <actions>
            <name>Update_Approver_Hans_Kresinski</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>MS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DRMS in EU</fullName>
        <actions>
            <name>Update_Approver_Marco_Van_Kleef</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>EU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DRMS in US</fullName>
        <actions>
            <name>Update_Approver_Michael_Vannier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>RMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DVT in EU</fullName>
        <actions>
            <name>Update_Approver_Gethin_Davies_Jones</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>EU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>VT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when GBU%3DVT in US</fullName>
        <actions>
            <name>Update_Approver_Terry_Callahan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GBU__c</field>
            <operation>equals</operation>
            <value>VT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when Region %3D ANZ</fullName>
        <actions>
            <name>Update_Approver_Jeff_Fryer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>ANZ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when Region %3D CA</fullName>
        <actions>
            <name>Update_Approver_Anna_Testa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>Canada,CA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when Region%3DAsia</fullName>
        <actions>
            <name>Update_Approver_when_Region_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>Asia</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when Region%3DEM</fullName>
        <actions>
            <name>Update_Approver_EM_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>EM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when Region%3DJapan</fullName>
        <actions>
            <name>Update_Approver_when_Region_Japan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>Japan</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Approver when region %3D EU</fullName>
        <actions>
            <name>Update_Approver_for_EU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>EU</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Contract Inquiry to Queue</fullName>
        <actions>
            <name>Update_Owner_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Contract Inquiry</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update EMEA Email Case Origin</fullName>
        <actions>
            <name>Update_EMEA_Email_Case_Origin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>startsWith</operation>
            <value>Email - ZA</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner ACB</fullName>
        <actions>
            <name>Update_Project_Owner_ACB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>ACB</value>
        </criteriaItems>
        <description>Used with Project Case Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner COE Quality Analyst</fullName>
        <actions>
            <name>Update_Project_Owner_COE_Quality_Analyst</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Status_to_In_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Quality Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Functional_Role__c</field>
            <operation>equals</operation>
            <value>Quality Review</value>
        </criteriaItems>
        <description>Used with Project Case Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner COE Support</fullName>
        <actions>
            <name>Update_Project_Owner_COE_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Status_to_In_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>RMCCB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Admin/Support</value>
        </criteriaItems>
        <description>Used with Project Case Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner Global Governance</fullName>
        <actions>
            <name>Update_Project_Owner_GGB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>GGB</value>
        </criteriaItems>
        <description>Used with Project Case Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner Release MGMT</fullName>
        <actions>
            <name>Update_Project_Owner_Release_MGMT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Project_Status_to_In_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>RMCCB</value>
        </criteriaItems>
        <description>Used with Project Case Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner Steering Committee</fullName>
        <actions>
            <name>Update_Project_Owner_Steering_Committee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Project_Team__c</field>
            <operation>equals</operation>
            <value>Steering Committee</value>
        </criteriaItems>
        <description>Used with Project Case Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
