<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>KitProductUnitQuanityFU</fullName>
        <description>This is a field updates that updates the field Unit Quantity Alt with the value in Unit Quantity.</description>
        <field>Unit_Quantity_ALT__c</field>
        <formula>Product_SKU__r.Unit_Quantity__c</formula>
        <name>KitProductUnitQuanityFU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>KitProductUnitQuanityWR</fullName>
        <actions>
            <name>KitProductUnitQuanityFU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is a workflow rule on the Kit Product Object that fires whenever the Kit Product field &quot;Unit Quantity&quot; is not blank.</description>
        <formula>Product_SKU__r.Unit_Quantity__c &gt;0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
