<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Comment_Added_To_Your_Idea</fullName>
        <description>New Comment Added To Your Idea</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/Yor_Idea_is_Commented_On</template>
    </alerts>
    <rules>
        <fullName>Idea Notify Users</fullName>
        <actions>
            <name>New_Comment_Added_To_Your_Idea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The comment made on the idea, get notified to that user who is the owner of the comment.</description>
        <formula>LastCommentDate &gt;= LastModifiedDate</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
