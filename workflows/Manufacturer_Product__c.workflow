<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Manufacturer_Product_Update_ID</fullName>
        <field>Name</field>
        <formula>ManufacturerOEM_Name__r.Name  &amp;&quot; - &quot; &amp; ManufacturerOEM_Product__c</formula>
        <name>Manufacturer Product - Update ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manufacturer_Product_BU</fullName>
        <field>Business_Unit__c</field>
        <formula>TEXT( $User.Business_Unit__c)</formula>
        <name>Update Manufacturer Product BU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manufacturer_Product_Country</fullName>
        <field>Country__c</field>
        <formula>$User.Country</formula>
        <name>Update Manufacturer Product Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Manufacturer Product - Update BU</fullName>
        <actions>
            <name>Update_Manufacturer_Product_BU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Manufacturer_Product__c.Business_Unit__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manufacturer Product - Update Country</fullName>
        <actions>
            <name>Update_Manufacturer_Product_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Manufacturer_Product__c.Country__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manufacturer Product - Update Name</fullName>
        <actions>
            <name>Manufacturer_Product_Update_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT (ISNULL( Name ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
