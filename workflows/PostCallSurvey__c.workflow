<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IDNSC_Post_Call_Survey</fullName>
        <description>IDNSC Post Call Survey</description>
        <protected>false</protected>
        <recipients>
            <recipient>IDNSC_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>dottie.m.dambra@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.r.nogueira@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.r.boulger@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>customeroperations@covidien.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IDNSC_Case_Number/IDNSC_Post_Call_Survey</template>
    </alerts>
    <rules>
        <fullName>IDNSC Manager Notification</fullName>
        <actions>
            <name>IDNSC_Post_Call_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PostCallSurvey__c.CLID__c</field>
            <operation>contains</operation>
        </criteriaItems>
        <description>Notifies IDNSC Manager that a survey has been completed.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
