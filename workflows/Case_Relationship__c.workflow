<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Support_Update_Account_Name_text</fullName>
        <field>Account_Name_text__c</field>
        <formula>Account__r.Name</formula>
        <name>Support - Update Account Name (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Support_Update_Account_Number_text</fullName>
        <field>Account_Number_text__c</field>
        <formula>Account__r.Account_External_ID__c</formula>
        <name>Support - Update Account Number (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Support_Update_Contact_Email_text</fullName>
        <field>Contact_Email_text__c</field>
        <formula>Contact__r.Email</formula>
        <name>Support - Update Contact Email (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Support_Update_Contact_Name_text</fullName>
        <field>Contact_Name_text__c</field>
        <formula>Contact__r.Full_Name_with_Salutation__c</formula>
        <name>Support - Update Contact Name (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Support_Update_Contact_Phone_text</fullName>
        <field>Contact_Phone_text__c</field>
        <formula>Contact__r.Phone</formula>
        <name>Support - Update Contact Phone (text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Support - Copy Account Name</fullName>
        <actions>
            <name>Support_Update_Account_Name_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Account Name from the linked Account Name to the Account Name (text) for reporting purposes.</description>
        <formula>LEN (Account__r.Name) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Account Number</fullName>
        <actions>
            <name>Support_Update_Account_Number_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Account Number from the linked Account to the Account Number (text) for reporting purposes.</description>
        <formula>LEN (  Account__r.Account_External_ID__c  ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Contact Email</fullName>
        <actions>
            <name>Support_Update_Contact_Email_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Contact Email from the linked Contact to the Contact Email (text) for reporting purposes.</description>
        <formula>LEN (Contact__r.Email) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Contact Name</fullName>
        <actions>
            <name>Support_Update_Contact_Name_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Contact Name from the linked Contact to the Contact Name (text) for reporting purposes.</description>
        <formula>LEN ( TRIM( Contact__r.Full_Name_with_Salutation__c )) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support - Copy Contact Phone</fullName>
        <actions>
            <name>Support_Update_Contact_Phone_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the Contact Phone from the linked Contact to the Contact Phone (text) for reporting purposes.</description>
        <formula>LEN (Contact__r.Phone ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
