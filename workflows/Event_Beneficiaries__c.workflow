<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EMS_Benefeciaries_Employee_Unique_field</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c +&apos;;&apos;+Employee__r.Id+&apos;;&apos;+&apos;Employee&apos;</formula>
        <name>EMS Benefeciaries Employee Unique field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Beneficiary_Unique</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c +&apos;;&apos;+Contact__r.Id+&apos;;&apos;+&apos;Contact&apos;+&apos;;&apos;+&apos;Budgeted&apos;</formula>
        <name>EMS Beneficiary Unique Budgeted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Beneficiary_Unique_Actual</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c +&apos;;&apos;+Contact__r.Id+&apos;;&apos;+&apos;Contact&apos;+&apos;;&apos;+&apos;Actual&apos;</formula>
        <name>EMS Beneficiary Unique Actual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_No_of_Pax_on_Contact_to_1</fullName>
        <field>No_of_pax__c</field>
        <formula>1</formula>
        <name>EMS No of Pax on Contact to 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_No_of_Pax_on_Org_to_0</fullName>
        <field>No_of_pax__c</field>
        <formula>0</formula>
        <name>EMS No of Pax on Org to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Organization_Unique_Actual</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c +&apos;;&apos;+Organization__r.Id+&apos;;&apos;+&apos;Organization&apos;+&apos;;&apos;+&apos;Actual&apos;</formula>
        <name>EMS Organization Unique Actual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Organization_Unique_Budgeted</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c +&apos;;&apos;+Organization__r.Id +&apos;;&apos;+&apos;Organization&apos;+&apos;;&apos;+&apos;Budgeted&apos;</formula>
        <name>EMS Organization Unique Budgeted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_PSU_BUdgeted_Unique</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c+&apos;;&apos;+ Contact__r.Id +&apos;;&apos;+&apos;Contact&apos;+&apos;;&apos;+&apos;PSU Budgeted Beneficiary&apos;</formula>
        <name>EMS PSU BUdgeted Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_PSU_Beneficiary_Unique</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c+&apos;;&apos;+ Organization__r.Id +&apos;;&apos;+&apos;Organization&apos;+&apos;;&apos;+&apos;PSU Budgeted Beneficiary&apos;</formula>
        <name>EMS PSU Bud Org Beneficiary Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_PreEvent_Budgeted_Beneficiary</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c+&apos;;&apos;+ Contact__r.Id +&apos;;&apos;+&apos;Contact&apos;+&apos;;&apos;+&apos;PreEvent Budgeted Beneficiary&apos;</formula>
        <name>EMS PreEvent Budgeted Beneficiary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Pre_Event_Bud_Org_Beneficiary_Unique</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c+&apos;;&apos;+ Organization__r.Id +&apos;;&apos;+&apos;Organization&apos;+&apos;;&apos;+&apos;PreEvent Budgeted Beneficiary&apos;</formula>
        <name>EMS Pre-Event Bud Org Beneficiary Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Speaker_flag</fullName>
        <field>Speaker__c</field>
        <literalValue>1</literalValue>
        <name>EMS Update Speaker flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_first_Budgeted_Beneficiary</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c+&apos;;&apos;+ Contact__r.Id +&apos;;&apos;+&apos;Contact&apos;+&apos;;&apos;+&apos;First Budgeted&apos;</formula>
        <name>EMS first Budgeted Beneficiary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_first_Org_Beneficiary_Unique</fullName>
        <field>Unique__c</field>
        <formula>EMS_Event__r.Event_Id__c+&apos;;&apos;+ Organization__r.Id +&apos;;&apos;+&apos;Organization&apos;+&apos;;&apos;+&apos;First Budgeted&apos;</formula>
        <name>EMS first Org Beneficiary Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EMS Actual Benefeciaries Unique</fullName>
        <actions>
            <name>EMS_Beneficiary_Unique_Actual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Contact__c!=null &amp;&amp; ISPICKVAL(Invitee_Type__c,&apos;Actual Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Actual Organization Benefeciaries Unique</fullName>
        <actions>
            <name>EMS_Organization_Unique_Actual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Organization__c!=null &amp;&amp; ISPICKVAL(Invitee_Type__c,&apos;Actual Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Benefeciaries Employee Unique field</fullName>
        <actions>
            <name>EMS_Benefeciaries_Employee_Unique_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Employee__c!=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Benefeciaries Unique</fullName>
        <actions>
            <name>EMS_Beneficiary_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Contact__c!=null &amp;&amp;ISPICKVAL(Invitee_Type__c,&apos;Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Budgeted Organization Benefeciaries Unique</fullName>
        <actions>
            <name>EMS_Organization_Unique_Budgeted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Organization__c!=null &amp;&amp; ISPICKVAL(Invitee_Type__c,&apos;Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS First Beneficiaries Unique</fullName>
        <actions>
            <name>EMS_first_Budgeted_Beneficiary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Contact__c!=null &amp;&amp;ISPICKVAL(Invitee_Type__c,&apos;Initial Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS First Beneficiaries Unique Organization</fullName>
        <actions>
            <name>EMS_first_Org_Beneficiary_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Organization__c!=null &amp;&amp; ISPICKVAL(Invitee_Type__c,&apos;Initial Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS No of Pax on Contact</fullName>
        <actions>
            <name>EMS_No_of_Pax_on_Contact_to_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(Contact__c!=null,Employee__c!=null)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS No of Pax on Org</fullName>
        <actions>
            <name>EMS_No_of_Pax_on_Org_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Organization__c!=null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMS PSU Beneficiaries Unique</fullName>
        <actions>
            <name>EMS_PSU_BUdgeted_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Contact__c!=null &amp;&amp;ISPICKVAL(Invitee_Type__c,&apos;PSU Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS PSU Beneficiaries Unique Organization</fullName>
        <actions>
            <name>EMS_PSU_Beneficiary_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Organization__c!=null &amp;&amp; ISPICKVAL(Invitee_Type__c,&apos;PSU Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS PreEvent Budgeted Beneficiaries Unique</fullName>
        <actions>
            <name>EMS_PreEvent_Budgeted_Beneficiary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Contact__c!=null &amp;&amp;ISPICKVAL(Invitee_Type__c,&apos;PreEvent Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS PreEvent Budgeted Organization Unique</fullName>
        <actions>
            <name>EMS_Pre_Event_Bud_Org_Beneficiary_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Organization__c!=null &amp;&amp;ISPICKVAL(Invitee_Type__c,&apos;PreEvent Budgeted Beneficiary&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMS Update Speaker on Consultancy Fees</fullName>
        <actions>
            <name>EMS_Update_Speaker_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK( Consultancy_fees__c )),Consultancy_fees__c &gt;0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
