<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>HK_Approval_Notification</fullName>
        <description>HK Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Sample_Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>HK_Rejection_Notification</fullName>
        <description>HK Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Sample_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>HK_Resend_Sample_Request_to_CS</fullName>
        <ccEmails>HK-CS@covidien.com</ccEmails>
        <description>HK Resend Sample Request to CS</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CS_Owner_Email_For_HK_Only__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/HK_Sample_Request_to_CS_from_admin</template>
    </alerts>
    <alerts>
        <fullName>HK_Sample_Request_Notify_CS</fullName>
        <ccEmails>HK-CS@covidien.com</ccEmails>
        <description>HK Sample Request Notify CS</description>
        <protected>false</protected>
        <recipients>
            <field>CS_Owner_Email_For_HK_Only__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>HK_BU_Head_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Sample_Request_Template_to_CS</template>
    </alerts>
    <alerts>
        <fullName>HK_Submit_Notification</fullName>
        <description>HK Submit Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Sample_Request_Submit</template>
    </alerts>
    <alerts>
        <fullName>ID_Approval_Delivery_Note</fullName>
        <description>ID Approval - Delivery Note</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/ID_Sample_Delivery_Note</template>
    </alerts>
    <alerts>
        <fullName>ID_Rejection_Notification</fullName>
        <description>ID Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Sample_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>KR_Sample_Approval</fullName>
        <description>KR Sample Approval Notification Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SalesAdmin__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_Sample_Goods_Withdrawal_Form</template>
    </alerts>
    <alerts>
        <fullName>KR_Sample_Evaluation_Reminder</fullName>
        <description>KR Sample Evaluation Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/KR_Sample_Evaluation_Reminder</template>
    </alerts>
    <alerts>
        <fullName>KR_Sample_Rejection_Notification_Email</fullName>
        <description>KR Sample Rejection Notification Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Sample_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>MY_Approval_Notification_GWF_Form_Email</fullName>
        <ccEmails>agnes.tan@medtronic.com,</ccEmails>
        <ccEmails>jo.wei.iau@medtronic.com,</ccEmails>
        <ccEmails>tze.xin.ooi@medtronic.com,</ccEmails>
        <ccEmails>yen.bee.saw@medtronic.com</ccEmails>
        <description>MY Approval Notification - GWF Form Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>MYBUHeadEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>MYMarketingEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>yeok.mooi.lim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Sample_Good_Withdrawal_Form</template>
    </alerts>
    <alerts>
        <fullName>MY_Approval_Notification_GWF_Form_Email_Revision</fullName>
        <ccEmails>agnes.tan@medtronic.com.test,</ccEmails>
        <ccEmails>jo.wei.iau@medtronic.com.test,</ccEmails>
        <ccEmails>tze.xin.ooi@medtronic.com.test,</ccEmails>
        <ccEmails>yen.bee.saw@medtronic.com.test</ccEmails>
        <description>MY Approval Notification - GWF Form Email Revision</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>MYBUHeadEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>MYMarketingEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>yeok.mooi.lim@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/MY_Sample_Good_Withdrawal_Form_Revision</template>
    </alerts>
    <alerts>
        <fullName>MY_Rejection_Notification</fullName>
        <description>MY Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Requestor_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Sample_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>Notify_Requestor_of_each_step</fullName>
        <description>Notify Requestor of each step</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Asia_Template/Asia_Sample_Request_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_User_of_Manager_Approval</fullName>
        <description>Notify User of Manager Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/ANZ_Sample_Tracking_Manager_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notify_User_of_Managers_Rejection</fullName>
        <description>Notify User of Managers Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/ANZ_Sample_Tracking_Manager_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_User_of_PM_Rejection</fullName>
        <description>Notify User of PM Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/ANZ_Sample_Tracking_PM_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_User_of_Review</fullName>
        <ccEmails>Aust.Customer.Service@Covidien​​.com</ccEmails>
        <description>Notify User of  Review</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/ANZ_Sample_Tracking_Response</template>
    </alerts>
    <alerts>
        <fullName>Notify_User_of_Review_Case_Link</fullName>
        <ccEmails>Aust.Customer.Service@Covidien​​.com</ccEmails>
        <description>Notify User of Review &amp; Case Link</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/ANZ_Sample_Tracking_PM_Approval</template>
    </alerts>
    <alerts>
        <fullName>Reminder</fullName>
        <description>Reminder : ANZ Sample Tracking Submission approval</description>
        <protected>false</protected>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/ANZ_Sample_Tracking_Submission</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_by_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Approved by Manager</literalValue>
        <name>Approved by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_by_Product_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Approved by Product Manager</literalValue>
        <name>Approved by Product Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_the_CS_Email</fullName>
        <field>CS_Owner_Email_For_HK_Only__c</field>
        <formula>CASE(
CreatedBy.Business_Unit__c , 
&quot;EMID&quot;, &quot;Aries.Choi@Covidien.com&quot;, 
&quot;EBD&quot;, &quot;Aries.Choi@Covidien.com&quot;,
&quot;STI&quot;, &quot;ruby.chau@Covidien.com&quot;,
&quot;VT &amp; MS&quot;, &quot;ruby.chau@Covidien.com&quot;,
&quot;VT-NV&quot;, &quot;ruby.chau@Covidien.com&quot;,
&quot;Bonnie.Lam@Covidien.com&quot;
)</formula>
        <name>Capture the CS Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HK_capture_BU_head_email</fullName>
        <field>HK_BU_Head_Email__c</field>
        <formula>CASE 
( 
CreatedBy.Business_Unit__c , 
&quot;EMID&quot;, &quot;winnie.yeung@medtronic.com&quot;, 
&quot;EBD&quot;, &quot;ivan.cheung@medtronic.com&quot;, 
&quot;STI&quot;, &quot;joyce.chan@medtronic.com&quot;, 
&quot;VT &amp; MS&quot;, &quot;jass.leung@medtronic.com&quot;,
&quot;ET&quot;,&quot;cathy.ho@medtronic.com&quot;,
&quot;RMS&quot;, &quot;gilbert.yim@medtronic.com&quot;,
&quot;cathy.ho@medtronic.com&quot; 
)</formula>
        <name>HK capture BU head email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Manager</literalValue>
        <name>Rejected by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_Product_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by Product Manager</literalValue>
        <name>Rejected by Product Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Create_Case_Flag</fullName>
        <description>Setting CreateCase = True will initiate the Create Case Trigger</description>
        <field>CreateCase__c</field>
        <literalValue>1</literalValue>
        <name>Set Create Case Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Pending</fullName>
        <description>Set Status to Pending Approval by ANZ Product Manager</description>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Set Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status update to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_to_approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status update to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_to_draft</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Status update to draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_to_submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Status update to submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EBD_Product_Manager</fullName>
        <field>Product_Manager__c</field>
        <lookupValue>fenny.saputra@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update EBD Product Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EMID_Product_Manager</fullName>
        <field>Product_Manager__c</field>
        <lookupValue>fenny.saputra@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update EMID Product Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ET_Product_Manager</fullName>
        <field>Product_Manager__c</field>
        <lookupValue>fenny.saputra@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update ET Product Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Approved_Date</fullName>
        <field>LastApprovedDate__c</field>
        <formula>Today()</formula>
        <name>Update Last Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Approver</fullName>
        <field>Last_Approver__c</field>
        <formula>$User.Full_Name__c</formula>
        <name>Update Last Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_STI_Product_Manager</fullName>
        <field>Product_Manager__c</field>
        <lookupValue>fenny.saputra@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update STI Product Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture the CS Email</fullName>
        <actions>
            <name>Capture_the_CS_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CreatedBy.Profile.Name = &quot;Asia - HK&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HK admin resend sample to CS</fullName>
        <actions>
            <name>HK_Resend_Sample_Request_to_CS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>HK Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.Re_Send_to_CS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HK capture BU head email</fullName>
        <actions>
            <name>HK_capture_BU_head_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>HK Sample Request</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>KR Sample Evaluation Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>KR Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.DateofDelivery__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Reminder to sales rep to obtain sample evaluation from doctor a month after sample is given.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>KR_Sample_Evaluation_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Samples__c.DateofDelivery__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MY Sample - Update EBD Product Manager</fullName>
        <actions>
            <name>Update_EBD_Product_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>MY Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.GBU__c</field>
            <operation>equals</operation>
            <value>EBD</value>
        </criteriaItems>
        <description>Update PM user for EBD</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY Sample - Update EMID Product Manager</fullName>
        <actions>
            <name>Update_EMID_Product_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>MY Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.GBU__c</field>
            <operation>equals</operation>
            <value>EMID</value>
        </criteriaItems>
        <description>Update PM user for EMID</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY Sample - Update ET Product Manager</fullName>
        <actions>
            <name>Update_ET_Product_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>MY Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.GBU__c</field>
            <operation>equals</operation>
            <value>ET</value>
        </criteriaItems>
        <description>Update PM user for ET</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY Sample - Update STI Product Manager</fullName>
        <actions>
            <name>Update_STI_Product_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Samples__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>MY Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.GBU__c</field>
            <operation>equals</operation>
            <value>STI</value>
        </criteriaItems>
        <description>Update PM user for STI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY Send GWF Form Email</fullName>
        <actions>
            <name>MY_Approval_Notification_GWF_Form_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Samples__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.Send_MY_GWF_Form__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MY Send GWF Form Email After Revision</fullName>
        <actions>
            <name>MY_Approval_Notification_GWF_Form_Email_Revision</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Samples__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.Send_MY_GWF_Form__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Samples__c.Revision__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reminder to Product manager ANZ</fullName>
        <active>true</active>
        <formula>AND (  ISPICKVAL(Status__c,&apos;Pending Approval&apos;),NOT(ISPICKVAL(Status__c,&apos;Approved by Product Manager&apos;)), RecordType.Name = &apos;ANZ Sample&apos;    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
