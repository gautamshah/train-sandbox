<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Default_to_Box_Case_Pricing</fullName>
        <field>Pricing_Type__c</field>
        <literalValue>BOX/CASE</literalValue>
        <name>Default to Box Case Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_to_Each_Pricing</fullName>
        <field>Pricing_Type__c</field>
        <literalValue>FOR EACH</literalValue>
        <name>Default to Each Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinanceFloorApproval_Check</fullName>
        <field>Finance_Floor_Approval__c</field>
        <literalValue>1</literalValue>
        <name>FinanceFloorApproval Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinanceFloorApproval_Uncheck</fullName>
        <field>Finance_Floor_Approval__c</field>
        <literalValue>0</literalValue>
        <name>FinanceFloorApproval Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>List_Extended_Price_List_Price_Qty</fullName>
        <field>List_Extended_Price__c</field>
        <formula>BLANKVALUE(List_Price__c,0) * BLANKVALUE(Apttus_Config2__Quantity__c,0)</formula>
        <name>List Extended Price = List Price * Qty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MarketingFloorApproval_Check</fullName>
        <field>Marketing_Floor_Approval__c</field>
        <literalValue>1</literalValue>
        <name>MarketingFloorApproval Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MarketingFloorApproval_Uncheck</fullName>
        <field>Marketing_Floor_Approval__c</field>
        <literalValue>0</literalValue>
        <name>MarketingFloorApproval Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMSFloorApproval_Check</fullName>
        <field>RMS_Floor_Approval__c</field>
        <literalValue>1</literalValue>
        <name>RMSFloorApproval Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMSFloorApproval_Uncheck</fullName>
        <field>RMS_Floor_Approval__c</field>
        <literalValue>0</literalValue>
        <name>RMSFloorApproval Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pricing_to_Each</fullName>
        <field>Use_Each_Pricing__c</field>
        <literalValue>1</literalValue>
        <name>Set Pricing to Each</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZVPFloorApproval_Check</fullName>
        <field>ZVP_Floor_Approval__c</field>
        <literalValue>1</literalValue>
        <name>ZVPFloorApproval Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZVPFloorApproval_Uncheck</fullName>
        <field>ZVP_Floor_Approval__c</field>
        <literalValue>0</literalValue>
        <name>ZVPFloorApproval Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FinanceFloorApproval Check</fullName>
        <actions>
            <name>FinanceFloorApproval_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(AND(ISBLANK(MKTFloorPrice__c), ISBLANK(ZVPFloorPrice__c), ISBLANK(RSMFloorPrice__c), ISBLANK(REPFloorPrice__c))),  Unit_Net_Price__c &lt; MIN(BLANKVALUE(MKTFloorPrice__c, 999999999), BLANKVALUE(ZVPFloorPrice__c, 999999999), BLANKVALUE(RSMFloorPrice__c, 999999999), BLANKVALUE(REPFloorPrice__c, 999999999))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FinanceFloorApproval Uncheck</fullName>
        <actions>
            <name>FinanceFloorApproval_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(AND(ISBLANK(MKTFloorPrice__c), ISBLANK(ZVPFloorPrice__c), ISBLANK(RSMFloorPrice__c), ISBLANK(REPFloorPrice__c))),  Unit_Net_Price__c &lt; MIN(BLANKVALUE(MKTFloorPrice__c, 999999999), BLANKVALUE(ZVPFloorPrice__c, 999999999), BLANKVALUE(RSMFloorPrice__c, 999999999), BLANKVALUE(REPFloorPrice__c, 999999999))  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MarketingFloorApproval Check</fullName>
        <actions>
            <name>MarketingFloorApproval_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(ISBLANK(MKTFloorPrice__c)),  NOT(AND(ISBLANK(ZVPFloorPrice__c), ISBLANK(RSMFloorPrice__c), ISBLANK(REPFloorPrice__c))),  Unit_Net_Price__c &lt; MIN(BLANKVALUE(ZVPFloorPrice__c, 999999999), BLANKVALUE(RSMFloorPrice__c, 999999999), BLANKVALUE(REPFloorPrice__c, 999999999))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MarketingFloorApproval Uncheck</fullName>
        <actions>
            <name>MarketingFloorApproval_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(ISBLANK(MKTFloorPrice__c)),  NOT(AND(ISBLANK(ZVPFloorPrice__c), ISBLANK(RSMFloorPrice__c), ISBLANK(REPFloorPrice__c))),  Unit_Net_Price__c &lt; MIN(BLANKVALUE(ZVPFloorPrice__c, 999999999), BLANKVALUE(RSMFloorPrice__c, 999999999), BLANKVALUE(REPFloorPrice__c, 999999999))  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quick Quote Creation</fullName>
        <actions>
            <name>Default_to_Box_Case_Pricing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Trying to avoid triggers on Line Item per Apttus. We can only set a static picklist value in a workflow rule. If it becomes necessary to look at the product uom and set the default based on that we will need to move to a trigger.</description>
        <formula>Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName = &apos;Hardware_or_Product_Quote&apos; &amp;&amp; Apttus_Config2__DerivedFromId__c = &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RMSFloorApproval Check</fullName>
        <actions>
            <name>RMSFloorApproval_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(ISBLANK(RSMFloorPrice__c)),  NOT(ISBLANK(REPFloorPrice__c)),  Unit_Net_Price__c &lt; REPFloorPrice__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RMSFloorApproval Uncheck</fullName>
        <actions>
            <name>RMSFloorApproval_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(ISBLANK(RSMFloorPrice__c)),  NOT(ISBLANK(REPFloorPrice__c)),  Unit_Net_Price__c &lt; REPFloorPrice__c  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Scrub PO or Custom Kit Creation</fullName>
        <actions>
            <name>Default_to_Each_Pricing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Pricing_to_Each</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(Apttus_Config2__ConfigurationId__r.Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName = &apos;Scrub_PO&apos; || Apttus_Config2__ConfigurationId__r.Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName = &apos;Custom_Kit&apos;) &amp;&amp; Apttus_Config2__DerivedFromId__c = &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set List Extended Price</fullName>
        <actions>
            <name>List_Extended_Price_List_Price_Qty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets List Extended Price with Partner List Price * Quantity.</description>
        <formula>OR( ISNEW(), ISCHANGED(Apttus_Config2__Quantity__c), ISCHANGED(List_Price__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ZVPFloorApproval Check</fullName>
        <actions>
            <name>ZVPFloorApproval_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(ISBLANK(ZVPFloorPrice__c)),  NOT(AND(ISBLANK(RSMFloorPrice__c), ISBLANK(REPFloorPrice__c))),  Unit_Net_Price__c &lt; MIN(BLANKVALUE(RSMFloorPrice__c, 999999999), BLANKVALUE(REPFloorPrice__c, 999999999))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ZVPFloorApproval Uncheck</fullName>
        <actions>
            <name>ZVPFloorApproval_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(AND(  Apttus_Config2__BasePrice__c &lt;&gt; Unit_Net_Price__c,  NOT(ISBLANK(ZVPFloorPrice__c)),  NOT(AND(ISBLANK(RSMFloorPrice__c), ISBLANK(REPFloorPrice__c))),  Unit_Net_Price__c &lt; MIN(BLANKVALUE(RSMFloorPrice__c, 999999999), BLANKVALUE(REPFloorPrice__c, 999999999))  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
