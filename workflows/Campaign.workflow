<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Truclear_Rep_Campaign_email_notification</fullName>
        <description>Truclear Rep Campaign email notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jacob.kelly@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Covidien_Email_Templates/Truclear_Rep_Campaign_notification</template>
    </alerts>
    <rules>
        <fullName>Truclear Rep Campaign notifcation</fullName>
        <actions>
            <name>Truclear_Rep_Campaign_email_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.NotifyMarketing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Truclear rep campaign workflow rule which fires off an email to Jacob Kelly</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
