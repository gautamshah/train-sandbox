<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ILS_Update_Room_Key_Field</fullName>
        <description>update with Room Name</description>
        <field>Key__c</field>
        <formula>Room_Name__c</formula>
        <name>ILS Update Room Key Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ILS_Update_Room_Name_Field</fullName>
        <field>Room_Name__c</field>
        <formula>Account_Name__r.SAP_Account_Number__c  &amp; &quot;_&quot; &amp;  Room_Name__c</formula>
        <name>ILS Update Room Name Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ILS_Room_Name</fullName>
        <field>Name</field>
        <formula>Account_Name__r.SAP_Account_Number__c  &amp; &quot;_&quot; &amp;   Name</formula>
        <name>Update ILS Room Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Room_Key</fullName>
        <field>Key__c</field>
        <formula>Name</formula>
        <name>Update Room Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ILS Update Room Key Field</fullName>
        <actions>
            <name>Update_Room_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAP Account +&apos;_&quot;+Room Name</description>
        <formula>OR(ISNEW(),ISCHANGED(Account_Name__c ),ISCHANGED( Name ))&amp;&amp;  NOT( CONTAINS( Name+ &quot; &quot;,  Account_Name__r.SAP_Account_Number__c  + &quot;_&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ILS Update Room Name</fullName>
        <actions>
            <name>ILS_Update_Room_Key_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ILS_Update_Room_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_ILS_Room_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SAP Account +&apos;_&quot;+Room Name</description>
        <formula>OR(ISNEW(),ISCHANGED(Account_Name__c ),ISCHANGED( Name ))&amp;&amp;  NOT( CONTAINS( Name+ &quot; &quot;,  Account_Name__r.SAP_Account_Number__c  + &quot;_&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
