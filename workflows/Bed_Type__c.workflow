<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ILS_New_Bed_Type_Created_Updated_Notification</fullName>
        <description>ILS New Bed Type Created/Updated Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>US_ILS_Tech_Support_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/ILS_New_Bed_Type_Created_Updated</template>
    </alerts>
    <fieldUpdates>
        <fullName>ILS_Update_Bed_Type_Key_field</fullName>
        <description>with bed type name</description>
        <field>Key__c</field>
        <formula>Name</formula>
        <name>ILS Update Bed Type Key field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ILS_Update_Bed_Type_Name_Field</fullName>
        <field>Name</field>
        <formula>Brand__c  &amp; &quot;_&quot; &amp;  Model__c</formula>
        <name>ILS Update Bed Type Name Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bed_Type_Key_field</fullName>
        <description>Brand+ &quot;_&quot; + Model+&quot;_&quot;+ Bed Type Name</description>
        <field>Key__c</field>
        <formula>Brand__c  &amp; &quot;_&quot; &amp;  Model__c  &amp; &quot;_&quot; &amp;  Name</formula>
        <name>Update Bed Type Key field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ILS Notify Tech Support Team when a Bed Type is Updated</fullName>
        <actions>
            <name>ILS_New_Bed_Type_Created_Updated_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()  ||  ISCHANGED( Brand__c )  ||  ISCHANGED( Model__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ILS Update Bed Type Key</fullName>
        <actions>
            <name>Update_Bed_Type_Key_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Brand+ &quot;_&quot; + Model+&quot;_&quot;+ Bed Type Name</description>
        <formula>ISCHANGED( Brand__c )  ||   ISCHANGED( Model__c )  ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ILS Update Bed Type Name</fullName>
        <actions>
            <name>ILS_Update_Bed_Type_Key_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ILS_Update_Bed_Type_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Brand+ &quot;_&quot; + Model</description>
        <formula>ISCHANGED( Brand__c )  ||   ISCHANGED( Model__c )  ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
