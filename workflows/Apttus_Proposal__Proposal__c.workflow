<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RMS_Proposal_Approval_Email_Alert</fullName>
        <description>RMS Proposal Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Owners_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/RMS_Quote_Approval_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_OD_Approver_Missing_Email_Notification</fullName>
        <description>SSG OD Approver Missing Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_OD_Quote_Approver_Missing_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SSG_Quote_Approval_Email_Alert</fullName>
        <description>SSG Quote Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSG_Quote_Approval_Notification_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Presented_Date</fullName>
        <field>Apttus_Proposal__Presented_Date__c</field>
        <formula>TODAY()</formula>
        <name>Presented Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proposal_Record_Type_Quick_Quote</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Hardware_or_Product_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Proposal Record Type = Quick Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Quick_Quote</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Hardware_or_Product_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type = Quick Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSG_Default_Non_Standard_Contract_Reques</fullName>
        <description>updates the non-standard language request comments</description>
        <field>SSG_Non_Standard_Language_Comments__c</field>
        <formula>&quot;Contract Modifications are Required&quot;</formula>
        <name>SSG Default Non Standard Contract Commen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSG_Default_Non_Standard_Contract_Requir</fullName>
        <description>updates the non standard checkbox to true for default value</description>
        <field>SSG_NonStandard_Language_Requested__c</field>
        <literalValue>1</literalValue>
        <name>SSG Default Non Standard Contract Requir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trade_In_Amount_Competitor_Other_NULL</fullName>
        <field>Trade_In_Amount_Competitor_Unit_Other__c</field>
        <formula>IF( 
ISPICKVAL(Trade_In_Amount_Competitor_Unit__c,&quot;Other&quot;),  Trade_In_Amount_Competitor_Unit_Other__c, 
VALUE(&quot;&quot;) 
)</formula>
        <name>Trade-In Amount Competitor (Other) =NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trade_In_Amount_ForceFXCS_Other_NULL</fullName>
        <field>Trade_In_Amount_ForceFXCS_Other__c</field>
        <formula>IF(
ISPICKVAL(Trade_In_Amount_ForceFXCS__c,&quot;Other&quot;),
Trade_In_Amount_ForceFXCS_Other__c,
VALUE(&quot;&quot;)
)</formula>
        <name>Trade-In Amt ForceFXCS (Other) = NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trade_In_Amount_ForceTriad_Other_NULL</fullName>
        <field>Trade_In_Amount_ForceTriad_Other__c</field>
        <formula>IF(
ISPICKVAL(Trade_In_Amount_ForceTriad__c,&quot;Other&quot;),
Trade_In_Amount_ForceTriad_Other__c,
VALUE(&quot;&quot;)
)</formula>
        <name>Trade-In Amt ForceTriad (Other) = NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Manager_Email</fullName>
        <description>Updates the Q/P Owner Manager Email from the Users Owner Manager email</description>
        <field>Owners_Manager_Email__c</field>
        <formula>Owner:User.User_Manager_Email__c</formula>
        <name>Update Owner Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Default Presented Date</fullName>
        <actions>
            <name>Presented_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(Apttus_Proposal__Presented_Date__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMS Proposal Approval Workflow Rule</fullName>
        <actions>
            <name>RMS_Proposal_Approval_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agreement Proposal Locked,Hardware or Product Quote Locked,New Product LOC Locked,Promotional LOC Locked,Royalty Free Agreement Locked,Rentals Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_QPApprov__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>This workflow sends an email to the quote owner and his/her manager when an approval is approved or rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Custom Pricing Proposal Legal Notification Workflow</fullName>
        <actions>
            <name>SSG_Agreement_Proposal_Approved</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Custom Pricing Proposal,Custom Pricing Proposal Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_QPApprov__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>This workflow creates a task for the legal queue to alert them a Custom Pricing Proposal quote has been approved and is ready for contract creation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Default Non Standard Contract Request</fullName>
        <actions>
            <name>SSG_Default_Non_Standard_Contract_Reques</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SSG_Default_Non_Standard_Contract_Requir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agreement Proposal</value>
        </criteriaItems>
        <description>this workflow defaults the non-standard contract request checkbox and comments for certain record types</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG OD Approver Missing Proposal</fullName>
        <actions>
            <name>SSG_OD_Approver_Missing_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware or Product Quote,Agreement Proposal,New Product LOC,Promotional LOC,Royalty Free Agreement,Rentals</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.OD_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>this workflow sends an email to the creator of a proposal if the OD analyst is not populated instructing them that they will not be able to submit their quote for approvals and to notify their OD analyst for assistance</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSG Quick Quote Set Trade-In Amount %28Other%29</fullName>
        <actions>
            <name>Trade_In_Amount_Competitor_Other_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trade_In_Amount_ForceFXCS_Other_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Trade_In_Amount_ForceTriad_Other_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears Trade-In Amounts (Other) fields when a picklist value is selected</description>
        <formula>OR( AND( NOT(ISPICKVAL(Trade_In_Amount_ForceFXCS__c,&quot;Other&quot;)), NOT(ISBLANK(Trade_In_Amount_ForceFXCS_Other__c)) ), AND( NOT(ISPICKVAL(Trade_In_Amount_ForceTriad__c,&quot;Other&quot;)), NOT(ISBLANK(Trade_In_Amount_ForceTriad_Other__c)) ), AND( NOT(ISPICKVAL( Trade_In_Amount_Competitor_Unit__c,&quot;Other&quot;)), NOT(ISBLANK(Trade_In_Amount_Competitor_Unit_Other__c)) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SSG Quick Quote Unlock on Deny</fullName>
        <actions>
            <name>Proposal_Record_Type_Quick_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Unlocks proposal on Deny so sales rep can modify the deal.</description>
        <formula>AND(  Proposal_Developer_Record_Type_Name__c = &quot;Hardware_or_Product_Quote_Locked&quot;,  ISCHANGED(Apttus_Proposal__Approval_Stage__c ),  ISPICKVAL(Apttus_Proposal__Approval_Stage__c,&quot;Denied&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SSG Quote Approval Workflow Rule</fullName>
        <actions>
            <name>SSG_Quote_Approval_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Agreement_Proposal_Locked,Hardware_or_Product_Quote_Locked,New_Product_LOC_Locked,Promotional_LOC_Locked,Rentals_Locked,Royalty_Free_Agreement_Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Agreement_Proposal,Hardware_or_Product_Quote,New_Product_LOC,Promotional_LOC,Rentals,Royalty_Free_Agreement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_QPApprov__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>this workflow sends an email to the quote owner when an approval is approved or rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unlock Approval Recall%3A Quick Quote</fullName>
        <actions>
            <name>Record_Type_Quick_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quick Quote Locked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_QPApprov__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approval Required</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Owner Manager Email</fullName>
        <actions>
            <name>Update_Owner_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.RecordTypeId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the Owner Managers Email from the Owners User record</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>SSG_Agreement_Proposal_Approved</fullName>
        <assignedTo>ssglegalq@medtronic.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please begin contract creation for this custom pricing proposal quote</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>SSG Custom Pricing Proposal has been approved</subject>
    </tasks>
</Workflow>
