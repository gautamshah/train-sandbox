<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Thousands</displayUnits>
            <gaugeMax>1500000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Total ECIII Opportunities ($)</header>
            <indicatorBreakpoint1>500000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>1000000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_SI_Archive/ECIII_Pipeline_Report</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Total $</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Thousands</displayUnits>
            <gaugeMax>450000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Total Open Lapro-Clip Opportunities ($)</header>
            <indicatorBreakpoint1>150000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>300000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_SI_Archive/Lapro_Clip_Pipeline_Report</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Total $</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Territory.Opportunities$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Who&apos;s closing ECIII Opps?</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_SI_Archive/ECIII_Closed_Won_Lost</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>EMID &amp; Supplies</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Territory$ParentTerritory</groupingColumn>
            <header>Who&apos;s leading ECIII by Region?</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_SI_Archive/ECIII_Pipeline_Report</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>EMID &amp; Supplies Regions</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Who&apos;s leading in Lapro-Clip Opps?</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_SI_Archive/Lapro_Clip_Pipeline_Report</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>EMID &amp; Supplies Regions</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Territory.Opportunities$StageName</groupingColumn>
            <header>What&apos;s our Pipeline for ECIII?</header>
            <legendPosition>Right</legendPosition>
            <report>US_SI_Archive/ECIII_Pipeline_Funnel</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps by Stage</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Territory.Opportunities$StageName</groupingColumn>
            <header>What&apos;s our Pipeline for Lapro-Clip?</header>
            <legendPosition>Right</legendPosition>
            <report>US_SI_Archive/Lapro_Clip_Pipeline_Funnel</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps by Stage</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Territory.Opportunities$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Territory.Opportunities.OpportunityLineItems$TotalPrice</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Who&apos;s closing Lapro-Clip</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_SI_Archive/Lapro_Clip_Closed_Won_Lost</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>EMID &amp; Supplies</title>
        </components>
    </rightSection>
    <runningUser>derek.a.carless@medtronic.com</runningUser>
    <textColor>#000000</textColor>
    <title>Surgical Supplies Clip Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
