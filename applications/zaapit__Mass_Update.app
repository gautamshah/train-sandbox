<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>zaapit__Contact_Working_List</defaultLandingTab>
    <description>Smart  Mass Update™ App by ZaapIT.com</description>
    <formFactors>Large</formFactors>
    <label>Smart Mass Update™</label>
    <tab>zaapit__Contact_Working_List</tab>
    <tab>zaapit__Accounts</tab>
    <tab>zaapit__Opportunities</tab>
    <tab>zaapit__Leads</tab>
    <tab>zaapit__Campaigns</tab>
    <tab>zaapit__EventsWL</tab>
    <tab>zaapit__TasksWL</tab>
    <tab>zaapit__Accounts_Activities</tab>
    <tab>Brief_Utilization_Tool__c</tab>
    <tab>Employee__c</tab>
    <tab>Contacts</tab>
    <tab>Account_Creation_Case</tab>
    <tab>Event_Management__c</tab>
    <tab>Similar_Cases_Filter_Field__c</tab>
    <tab>Me_Tab</tab>
    <tab>ILS_ME</tab>
</CustomApplication>
