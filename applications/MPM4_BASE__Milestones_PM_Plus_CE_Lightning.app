<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#FA6600</headerColor>
        <logo>MPM4_BASE__MPM_Plus_LN_Logo</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <formFactors>Large</formFactors>
    <label>Milestones PM Plus</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>MPM4_BASE__Milestone1_Getting_Started</tab>
    <tab>MPM4_BASE__Milestone1_Summary</tab>
    <tab>MPM4_BASE__Milestone1_Program__c</tab>
    <tab>MPM4_BASE__Milestone1_Project__c</tab>
    <tab>MPM4_BASE__Milestone1_Milestone__c</tab>
    <tab>MPM4_BASE__Milestone1_Task__c</tab>
    <tab>MPM4_BASE__Milestone1_Log__c</tab>
    <tab>MPM4_BASE__Milestone_Calendar</tab>
    <tab>MPM4_BASE__Milestone1_Import_Project_Template</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>MPM4_BASE__TH_Time_Entry</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
