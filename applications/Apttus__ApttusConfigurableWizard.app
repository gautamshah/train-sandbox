<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Contains tabs for Wizard design, Wizard Component Library and Wizards (runtime).</description>
    <formFactors>Large</formFactors>
    <label>Apttus Contract Wizard</label>
    <logo>Apttus__ApttusDocuments/Apttus__ApttusLogo.gif</logo>
    <tab>Apttus__WizardDesigns</tab>
    <tab>Apttus__WizardComponentLibrary</tab>
    <tab>Apttus__Wizards</tab>
    <tab>Me_Tab</tab>
    <tab>ILS_ME</tab>
</CustomApplication>
