<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>ConcurConnect__My_Concur</defaultLandingTab>
    <description>An application that integrates Concur Travel &amp; Expense solutions with Salesforce.com.</description>
    <formFactors>Large</formFactors>
    <label>Concur Connector</label>
    <logo>ConcurConnect__Concur_Connect_Assets/ConcurConnect__Concur_Logo.gif</logo>
    <tab>standard-Chatter</tab>
    <tab>ConcurConnect__My_Concur</tab>
    <tab>ConcurConnect__Setup</tab>
    <tab>ConcurConnect__AdminTools</tab>
    <tab>ConcurConnect__CostTrackingConfig</tab>
    <tab>Event_Management__c</tab>
    <tab>Similar_Cases_Filter_Field__c</tab>
    <tab>Me_Tab</tab>
    <tab>ILS_ME</tab>
</CustomApplication>
