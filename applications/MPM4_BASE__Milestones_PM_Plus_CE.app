<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>MPM4_BASE__Milestone1_Getting_Started</defaultLandingTab>
    <description>Milestones PM+ Community Edition</description>
    <label>Milestones PM+ CE</label>
    <logo>MPM4_BASE__Project_Management_Folder/MPM4_BASE__Milestones_PM_Logo.gif</logo>
    <tab>standard-Chatter</tab>
    <tab>MPM4_BASE__Milestone1_Getting_Started</tab>
    <tab>MPM4_BASE__Milestone1_Summary</tab>
    <tab>MPM4_BASE__Milestone1_Project__c</tab>
    <tab>MPM4_BASE__Milestone1_Milestone__c</tab>
    <tab>MPM4_BASE__Milestone1_Task__c</tab>
    <tab>MPM4_BASE__Milestone1_Time__c</tab>
    <tab>MPM4_BASE__Milestone1_Expense__c</tab>
    <tab>MPM4_BASE__Milestone1_Log__c</tab>
    <tab>MPM4_BASE__Milestone_Calendar</tab>
    <tab>MPM4_BASE__Milestone1_Import_Project_Template</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Event_Management__c</tab>
    <tab>Similar_Cases_Filter_Field__c</tab>
    <tab>Me_Tab</tab>
    <tab>ILS_ME</tab>
</CustomApplication>
