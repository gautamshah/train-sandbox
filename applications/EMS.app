<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Event_Management_System</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>EMS</label>
    <tab>EMS_Event__c</tab>
    <tab>Event_Management_System</tab>
    <tab>Cost__c</tab>
    <tab>Cost_Center__c</tab>
    <tab>Event_Procedures__c</tab>
    <tab>Approver_Matrix__c</tab>
    <tab>Event_Management__c</tab>
    <tab>Similar_Cases_Filter_Field__c</tab>
    <tab>Me_Tab</tab>
    <tab>ILS_ME</tab>
</CustomApplication>
