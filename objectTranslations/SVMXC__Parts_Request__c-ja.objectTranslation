<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>部品要求</value>
    </caseValues>
    <fields>
        <help><!-- Any additional information relevant to this parts request. --></help>
        <label><!-- Additional Information --></label>
        <name>SVMXC__Additional_Information__c</name>
    </fields>
    <fields>
        <help><!-- Age of the parts request in number of days. --></help>
        <label><!-- Age --></label>
        <name>SVMXC__Age__c</name>
    </fields>
    <fields>
        <help><!-- Name of the salesforce user that canceled this parts request. --></help>
        <label><!-- Canceled By --></label>
        <name>SVMXC__Canceled_By__c</name>
        <relationshipLabel><!-- Parts Requests (Canceled By) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Date/time on which this parts request was canceled. --></help>
        <label><!-- Canceled on --></label>
        <name>SVMXC__Canceled_on__c</name>
    </fields>
    <fields>
        <help><!-- Name of the salesforce user that closed this parts request. --></help>
        <label><!-- Closed By --></label>
        <name>SVMXC__Closed_By__c</name>
        <relationshipLabel><!-- Parts Requests (Closed By) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Date/time when the request was completed. --></help>
        <label><!-- Closed On --></label>
        <name>SVMXC__Closed_On__c</name>
    </fields>
    <fields>
        <help><!-- Name of the supplier company. Applicable for replenishing stock at warehouse locations. Lookup to an existing salesforce account record. --></help>
        <label><!-- Supplier --></label>
        <name>SVMXC__Company__c</name>
        <relationshipLabel><!-- Parts Requests (Supplier) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Flag indicates if Parts Request is for/by a Partner or not. --></help>
        <label><!-- IsPartnerRecord --></label>
        <name>SVMXC__IsPartnerRecord__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if Parts Request is for/by a Partner or not. --></help>
        <label><!-- Is Partner --></label>
        <name>SVMXC__IsPartner__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to Partner Account. --></help>
        <label><!-- Partner Account --></label>
        <name>SVMXC__Partner_Account__c</name>
        <relationshipLabel><!-- Parts Requests (Partner Account) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to Partner Contact. --></help>
        <label><!-- Partner Contact --></label>
        <name>SVMXC__Partner_Contact__c</name>
        <relationshipLabel><!-- Parts Requests --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Location from where stock is requested. This is a lookup to an existing site record. --></help>
        <label><!-- Requested From --></label>
        <name>SVMXC__Requested_From__c</name>
        <relationshipLabel><!-- Parts Requests (Requested From) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Location where stock is required. This is a lookup to an existing site record. --></help>
        <label><!-- Required At Location --></label>
        <name>SVMXC__Required_At_Location__c</name>
        <relationshipLabel><!-- Parts Requests (Required At Location) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Current status of parts request. --></help>
        <label><!-- Status --></label>
        <name>SVMXC__Status__c</name>
        <picklistValues>
            <masterLabel>Canceled</masterLabel>
            <translation>キャンセル済み</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Completed</masterLabel>
            <translation>完了</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Open</masterLabel>
            <translation>未完了</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Sum of Total Line Price --></help>
        <label><!-- Total Amount --></label>
        <name>SVMXC__Total_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Shipping Courier --></label>
        <name>SVMX_Shipping_Courier__c</name>
        <picklistValues>
            <masterLabel>Fedex</masterLabel>
            <translation><!-- Fedex --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UPS</masterLabel>
            <translation><!-- UPS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>USPS</masterLabel>
            <translation><!-- USPS --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Shipping Method --></label>
        <name>SVMX_Shipping_Method__c</name>
        <picklistValues>
            <masterLabel>2nd Day</masterLabel>
            <translation><!-- 2nd Day --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ground</masterLabel>
            <translation><!-- Ground --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Next Day</masterLabel>
            <translation><!-- Next Day --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Work Order --></label>
        <name>Work_Order__c</name>
        <relationshipLabel><!-- Parts Requests --></relationshipLabel>
    </fields>
    <nameFieldLabel>名前</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- Requested From location cannot be the same as Required At Location. --></errorMessage>
        <name>SVMXC__SVMXC_ValidateLocationsAreSame</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Requested From should be a stockable location if Supplier name is not entered. --></errorMessage>
        <name>SVMXC__SVMXC_ValidateRequestedFromStockable1</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Requested From should be a non-stockable location when supplier name is entered. --></errorMessage>
        <name>SVMXC__SVMXC_ValidateRequestedFromStockable2</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Required At is not a stockable location. Please select a stockable location. --></errorMessage>
        <name>SVMXC__SVMXC_ValidateRequiredAtStockable</name>
    </validationRules>
    <webLinks>
        <label><!-- Add_Lines --></label>
        <name>SVMXC__Add_Lines</name>
    </webLinks>
    <webLinks>
        <label><!-- Cancel_Parts_Request --></label>
        <name>SVMXC__Cancel_Parts_Request</name>
    </webLinks>
    <webLinks>
        <label><!-- Create_Shipment_Order --></label>
        <name>SVMXC__Create_Shipment_Order</name>
    </webLinks>
    <webLinks>
        <label><!-- Generate_Document_s --></label>
        <name>SVMXC__Generate_Document_s</name>
    </webLinks>
    <webLinks>
        <label><!-- Process_Receipts_Internal --></label>
        <name>SVMXC__Process_Receipts_Internal</name>
    </webLinks>
    <webLinks>
        <label><!-- Process_Receipts_Supplier --></label>
        <name>SVMXC__Process_Receipts_Supplier</name>
    </webLinks>
    <webLinks>
        <label><!-- StockLookupPREQ --></label>
        <name>SVMXC__StockLookupPREQ</name>
    </webLinks>
</CustomObjectTranslation>
