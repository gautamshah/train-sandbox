<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Segmentation Scorecard</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Segmentation Scorecard</value>
    </caseValues>
    <fields>
        <label><!-- Account Sales Force Total --></label>
        <name>Account_Sales_Force_Total__c</name>
    </fields>
    <fields>
        <help><!-- Does the account have a field (or inside) selling organization calling on referral sources?

(Please select a value in the pick list) --></help>
        <label><!-- Account Sales Force --></label>
        <name>Account_Sales_Force__c</name>
        <picklistValues>
            <masterLabel>No sales force or activity</masterLabel>
            <translation><!-- No sales force or activity --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Sales representatives at multiple referral sources</masterLabel>
            <translation><!-- Sales representatives at multiple referral sources --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Some sales representatives at single referral source</masterLabel>
            <translation><!-- Some sales representatives at single referral source --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>Account__c</name>
        <relationshipLabel><!-- Segmentation Scorecard --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- What is happening to the account&apos;s market share over time?  This question should be answered relative to their competitors.						

(Please select a value on the pick list) --></help>
        <label><!-- Account&apos;s position in marketplace --></label>
        <name>Account_s_position_in_marketplace__c</name>
        <picklistValues>
            <masterLabel>Business stable</masterLabel>
            <translation><!-- Business stable --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Declining in marketplace</masterLabel>
            <translation><!-- Declining in marketplace --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Growing share in marketplace</masterLabel>
            <translation><!-- Growing share in marketplace --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Account&apos;s position in marketplace total --></label>
        <name>Account_s_position_in_marketplace_total__c</name>
    </fields>
    <fields>
        <label><!-- Block Description --></label>
        <name>Block_Description__c</name>
    </fields>
    <fields>
        <label><!-- Block Text --></label>
        <name>Block_Text__c</name>
    </fields>
    <fields>
        <label><!-- Business Relationship Total --></label>
        <name>Business_Relationship_Total__c</name>
    </fields>
    <fields>
        <help><!-- This criteria defines your relationship with the various levels of decision makers at the account.			

(Please select the value that applies on the pick list) --></help>
        <label><!-- Business Relationship --></label>
        <name>Business_Relationship__c</name>
        <picklistValues>
            <masterLabel>Established relationship with decision maker</masterLabel>
            <translation><!-- Established relationship with decision maker --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Established relationship with executive / owner</masterLabel>
            <translation><!-- Established relationship with executive / owner --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Established relationship with multiple decision makers</masterLabel>
            <translation><!-- Established relationship with multiple decision makers --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No meaningful relationship</masterLabel>
            <translation><!-- No meaningful relationship --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- CPO Total --></label>
        <name>CPO_Total__c</name>
    </fields>
    <fields>
        <label><!-- Competitive Situation Total --></label>
        <name>Competitive_Situation_Total__c</name>
    </fields>
    <fields>
        <help><!-- What is the account&apos;s relationship with our competition like?						

(Please select a value from the pick list) --></help>
        <label><!-- Competitive Situation --></label>
        <name>Competitive_Situation__c</name>
        <picklistValues>
            <masterLabel>Competition disliked by account</masterLabel>
            <translation><!-- Competition disliked by account --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competition vulnerable</masterLabel>
            <translation><!-- Competition vulnerable --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Entrenched competition</masterLabel>
            <translation><!-- Entrenched competition --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- What percentage of the business do we have today in the product categories we offer?  A lower percentage gets a higher score because there is significant opportunity for growth.

(Please select a value from the pick list) --></help>
        <label><!-- Covidien share % at account --></label>
        <name>Covidien_share_at_account__c</name>
        <picklistValues>
            <masterLabel>25% - 50%</masterLabel>
            <translation><!-- 25% - 50% --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>50% - 75%</masterLabel>
            <translation><!-- 50% - 75% --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>&lt; 25%</masterLabel>
            <translation><!-- &lt; 25% --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>&gt; 75%</masterLabel>
            <translation><!-- &gt; 75% --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Covidien share % at account total --></label>
        <name>Covidien_share_at_account_total__c</name>
    </fields>
    <fields>
        <label><!-- Covidien share trend at account total --></label>
        <name>Covidien_share_trend_at_account_total__c</name>
    </fields>
    <fields>
        <help><!-- What is happening to our share at the account over time?						

(Please select a value from the pick list) --></help>
        <label><!-- Covidien share trend at the account --></label>
        <name>Covidien_share_trend_at_the_account__c</name>
        <picklistValues>
            <masterLabel>Declining share, lost to competition</masterLabel>
            <translation><!-- Declining share, lost to competition --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Increasing share, Covidien gaining business</masterLabel>
            <translation><!-- Increasing share, Covidien gaining business --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Stable share, no change</masterLabel>
            <translation><!-- Stable share, no change --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- An applied rating is given for every product category the account offers.  The more categories they offer, the more they align with our strategy and therefore get a higher score						
						
(Please select all of the values that apply on the pick list) --></help>
        <label><!-- Customer Product Offering --></label>
        <name>Customer_Product_Offering__c</name>
        <picklistValues>
            <masterLabel>Enteral Access</masterLabel>
            <translation><!-- Enteral Access --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Enteral Feeding</masterLabel>
            <translation><!-- Enteral Feeding --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Incontinence</masterLabel>
            <translation><!-- Incontinence --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SharpSafety</masterLabel>
            <translation><!-- SharpSafety --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Urology</masterLabel>
            <translation><!-- Urology --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wound Care</masterLabel>
            <translation><!-- Wound Care --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- What is the customer Type?

If &quot;Other&quot; is selected, please enter the customer type in the Home Care Scoring Notes text box.

(Please select the correct value from the pick list) --></help>
        <label><!-- Customer Type --></label>
        <name>Customer_Type__c</name>
        <picklistValues>
            <masterLabel>DME / Provider</masterLabel>
            <translation><!-- DME / Provider --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Home Health</masterLabel>
            <translation><!-- Home Health --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other</masterLabel>
            <translation><!-- Other --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pharmacy</masterLabel>
            <translation><!-- Pharmacy --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Decision Making Process Total --></label>
        <name>Decision_Making_Process_Total__c</name>
    </fields>
    <fields>
        <help><!-- Customers driven by clinical outcomes align with our strategy, those who are solely price driven, do not.						

(Please select a value from the pick list) --></help>
        <label><!-- Decision Making Process --></label>
        <name>Decision_Making_Process__c</name>
        <picklistValues>
            <masterLabel>Customer feedback driven</masterLabel>
            <translation><!-- Customer feedback driven --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Features, benefits and clinical outcome driven</masterLabel>
            <translation><!-- Features, benefits and clinical outcome driven --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Solely price driven</masterLabel>
            <translation><!-- Solely price driven --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Distribution Channel Total --></label>
        <name>Distribution_Channel_Total__c</name>
    </fields>
    <fields>
        <help><!-- We consider Medline distributed accounts a direct threat to our business.  Therefore, those using a regional distributor are a better fit.						

(Please select a value from the pick list) --></help>
        <label><!-- Distribution Channel --></label>
        <name>Distribution_Channel__c</name>
        <picklistValues>
            <masterLabel>Direct</masterLabel>
            <translation><!-- Direct --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Medline</masterLabel>
            <translation><!-- Medline --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>National Distributor</masterLabel>
            <translation><!-- National Distributor --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Regional/Local Distributor</masterLabel>
            <translation><!-- Regional/Local Distributor --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Fit Block Assign --></label>
        <name>Fit_Block_Assign__c</name>
    </fields>
    <fields>
        <label><!-- Fit Score --></label>
        <name>Fit_Score__c</name>
    </fields>
    <fields>
        <help><!-- If &quot;Other&quot; was selected in the Customer Type field, then please type the customer type in this field.

Also, please put any relative notes in the scoring of this customer in this field. --></help>
        <label><!-- Home Care Scoring Notes --></label>
        <name>Home_Care_Scoring_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Hospital Relationship Total --></label>
        <name>Hospital_Relationship_Total__c</name>
    </fields>
    <fields>
        <help><!-- Any relationship with a Covidien Acute facility or signed IDN makes the account a better strategic fit.						

(Please select a value from the pick list) --></help>
        <label><!-- Hospital Relationship --></label>
        <name>Hospital_Relationship__c</name>
        <picklistValues>
            <masterLabel>Affiliation with Covidien hospital</masterLabel>
            <translation><!-- Affiliation with Covidien hospital --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Affiliation with Covidien signed IDN</masterLabel>
            <translation><!-- Affiliation with Covidien signed IDN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Affiliation with competitive hospital</masterLabel>
            <translation><!-- Affiliation with competitive hospital --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No hospital affiliation</masterLabel>
            <translation><!-- No hospital affiliation --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- This is the customers total spending in the product categories we offer, not our base of business today.  In other words, what is our total potential at this account?

(Please select a value from the pick list) --></help>
        <label><!-- MS total potential at account --></label>
        <name>MS_total_potential_at_account__c</name>
        <picklistValues>
            <masterLabel>$100,001 - $500,000</masterLabel>
            <translation><!-- $100,001 - $500,000 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>$500,001 - $1MM</masterLabel>
            <translation><!-- $500,001 - $1MM --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>&lt; $100,000</masterLabel>
            <translation><!-- &lt; $100,000 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>&gt; $1,000,000</masterLabel>
            <translation><!-- &gt; $1,000,000 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- MS total potential at account total --></label>
        <name>MS_total_potential_at_account_total__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Block --></label>
        <name>Opportunity_Block__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Score --></label>
        <name>Opportunity_Score__c</name>
    </fields>
    <fields>
        <label><!-- PS Total --></label>
        <name>PS_Total__c</name>
    </fields>
    <fields>
        <label><!-- Payor Mix Total --></label>
        <name>Payor_Mix_Total__c</name>
    </fields>
    <fields>
        <help><!-- A diverse payor mix is a more stable business model and receives a higher fit score.						

(Please select a value from the pick list) --></help>
        <label><!-- Payor Mix --></label>
        <name>Payor_Mix__c</name>
        <picklistValues>
            <masterLabel>Medicare &amp; multiple Medicaid payors</masterLabel>
            <translation><!-- Medicare &amp; multiple Medicaid payors --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Medicare, Medicaid and private pay</masterLabel>
            <translation><!-- Medicare, Medicaid and private pay --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>One Medicaid payor (MCO)</masterLabel>
            <translation><!-- One Medicaid payor (MCO) --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Private Label Total --></label>
        <name>Private_Label_Total__c</name>
    </fields>
    <fields>
        <help><!-- Customers using private label incontinence products are probably price focused.  Our strategy is to sell branded products where ever possible.						

(Please select a value from the pick list) --></help>
        <label><!-- Private Label --></label>
        <name>Private_Label__c</name>
        <picklistValues>
            <masterLabel>Use a mix of branded and private label</masterLabel>
            <translation><!-- Use a mix of branded and private label --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Use primarily branded products</masterLabel>
            <translation><!-- Use primarily branded products --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Use primarily private label products</masterLabel>
            <translation><!-- Use primarily private label products --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Any account purchasing $1000 or less annually is a &apos;non customer&apos;.						

(Please select a value from the pick list) --></help>
        <label><!-- Purchasing Status --></label>
        <name>Purchasing_Status__c</name>
        <picklistValues>
            <masterLabel>Existing Customer</masterLabel>
            <translation><!-- Existing Customer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Non Customer (&lt; $1,000)</masterLabel>
            <translation><!-- Non Customer (&lt; $1,000) --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Total Account Score --></label>
        <name>Total_Account_Score__c</name>
    </fields>
    <recordTypes>
        <label><!-- Home Care Scorecard --></label>
        <name>Home_Care_Scorecard</name>
    </recordTypes>
    <startsWith>Consonant</startsWith>
    <validationRules>
        <errorMessage><!-- Customer Type &quot;Other&quot; has been selected. A customer type needs to be put into the Home Care Scoring Notes field. --></errorMessage>
        <name>Customer_Type_Other</name>
    </validationRules>
</CustomObjectTranslation>
