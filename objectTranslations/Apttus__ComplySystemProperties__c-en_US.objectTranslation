<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <label><!-- Auto Sync With Opportunity --></label>
        <name>Apttus_CMConfig__AutoSyncWithOpportunity__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether agreement line items should be used to synchronize bundle products. The default uses agreement summary objects. --></help>
        <label><!-- Sync Bundle Using Line Items --></label>
        <name>Apttus_CMConfig__SyncBundleUsingLineItems__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the option products should be synchronized along with the bundle. The default is synchronize bundle only. --></help>
        <label><!-- Sync Option Products --></label>
        <name>Apttus_CMConfig__SyncOptionProducts__c</name>
    </fields>
    <fields>
        <help><!-- The admin user is the default owner of activities created by a user who is not allowed to be the owner (for e.g. customer portal user). --></help>
        <label><!-- Admin User --></label>
        <name>Apttus__AdminUser__c</name>
    </fields>
    <fields>
        <help><!-- The API name of the agreement number field for third party documents --></help>
        <label><!-- Agreement Number Field For Imported Docs --></label>
        <name>Apttus__AgreementNumberFieldForImportedDocs__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the pdf selection is allowed to be overridden by the user. Only applicable when pdf is auto enabled for final documents. --></help>
        <label><!-- Allow PDF Selection Override --></label>
        <name>Apttus__AllowPDFSelectionOverride__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the private document selection is allowed to be overridden by the user. Only applicable when private indicator is auto enabled for documents --></help>
        <label><!-- Allow Private Selection Override --></label>
        <name>Apttus__AllowPrivateSelectionOverride__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the reconcile selection is allowed to be overridden by the user. Only applicable when reconciliation is auto enabled for documents. --></help>
        <label><!-- Allow Reconcile Selection Override --></label>
        <name>Apttus__AllowReconcileSelectionOverride__c</name>
    </fields>
    <fields>
        <help><!-- NO LONGER USED --></help>
        <label><!-- Api User Key (DEPRECATED) --></label>
        <name>Apttus__ApiUserKey__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether PDF format is auto enabled for final documents --></help>
        <label><!-- Auto Enable PDF For Final Docs --></label>
        <name>Apttus__AutoEnablePDFForFinalDocs__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether private indicator is auto enabled for documents --></help>
        <label><!-- Auto Enable Private Indicator --></label>
        <name>Apttus__AutoEnablePrivateIndicator__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether reconciliation is auto enabled for documents --></help>
        <label><!-- Auto Enable Reconciliation --></label>
        <name>Apttus__AutoEnableReconciliation__c</name>
    </fields>
    <fields>
        <help><!-- Indicator to allow automatic placement of Agreement number in the header and last modified timestamp in the footer of the document for generate, offline and import actions. --></help>
        <label><!-- Auto Insert Header Footer Data --></label>
        <name>Apttus__AutoInsertHeaderFooterData__c</name>
    </fields>
    <fields>
        <label><!-- Bypass Sharing --></label>
        <name>Apttus__BypassSharing__c</name>
    </fields>
    <fields>
        <help><!-- The name of the contract summary template. --></help>
        <label><!-- Contract Summary Template --></label>
        <name>Apttus__ContractSummaryTemplate__c</name>
    </fields>
    <fields>
        <help><!-- Default set of document tags to be used during check-in from X-Author. Tags are comma separated values. --></help>
        <label><!-- Default Document Tags --></label>
        <name>Apttus__DefaultDocumentTags__c</name>
    </fields>
    <fields>
        <help><!-- The default owner for the agreement created from an opportunity. Valid values are Opportunity Owner and Current User. If not set, the Opportunity Owner will become the owner of the new agreement --></help>
        <label><!-- Default Opportunity Agreement Owner --></label>
        <name>Apttus__DefaultOpportunityAgreementOwner__c</name>
    </fields>
    <fields>
        <help><!-- You may apply your own naming convention to documents at generation, check-in and signature events, by defining a string of delimited variables, in the format %VarName1%_%VarName 2% … The default value is &apos;%:Name%_%action%_%templatename%_%timestamp%&apos;. --></help>
        <label><!-- Document Naming Convention --></label>
        <name>Apttus__DocumentNamingConvention__c</name>
    </fields>
    <fields>
        <help><!-- Applies FX2 framework to CM actions Create, Import Offline Documents, else Pre-FX2. Not for X-Author actions --></help>
        <label><!-- Document Structure FX2 For Imported Docs --></label>
        <name>Apttus__DocumentStructureFX2ForImportedDocs__c</name>
    </fields>
    <fields>
        <help><!-- Optional naming convention to be used for naming versioned documents --></help>
        <label><!-- Document Versioning Naming Convention (D --></label>
        <name>Apttus__DocumentVersioningNamingConvention__c</name>
    </fields>
    <fields>
        <help><!-- The email template for sending checkin notifications. --></help>
        <label><!-- Email Template For Checkin Notification --></label>
        <name>Apttus__EmailTemplateForCheckinNotification__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether clause approvals are enabled --></help>
        <label><!-- Enable Clause Approvals --></label>
        <name>Apttus__EnableClauseApprovals__c</name>
    </fields>
    <fields>
        <help><!-- Enables document versioning. Make sure EnableVersionControl is set to true. --></help>
        <label><!-- Enable Document Versioning --></label>
        <name>Apttus__EnableDocumentVersioning__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether the merge service debug is enabled --></help>
        <label><!-- Enable Merge Call Debug --></label>
        <name>Apttus__EnableMergeCallDebug__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether multiple checkout is allowed. Only applicable when version control is in effect. --></help>
        <label><!-- Enable Multiple Checkout --></label>
        <name>Apttus__EnableMultipleCheckout__c</name>
    </fields>
    <fields>
        <help><!-- Enabling pdf security lets users apply security settings to pdf documents and protect them with a password. --></help>
        <label><!-- Enable PDF Security --></label>
        <name>Apttus__EnablePDFSecurity__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether merge calls may be submitted for processing. If enabled, a Submit button is displayed during document generation. --></help>
        <label><!-- Enable Submit Merge Call --></label>
        <name>Apttus__EnableSubmitMergeCall__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether version control is in effect. If enabled, checkout/check-in policy is enforced for agreement documents. --></help>
        <label><!-- Enable Version Control --></label>
        <name>Apttus__EnableVersionControl__c</name>
    </fields>
    <fields>
        <help><!-- The format of the date time to place in the footer of the imported document --></help>
        <label><!-- Footer Datetime Format For Imported Docs --></label>
        <name>Apttus__FooterDatetimeFormatForImportedDocs__c</name>
    </fields>
    <fields>
        <label><!-- Instance Url --></label>
        <name>Apttus__InstanceUrl__c</name>
    </fields>
    <fields>
        <help><!-- The maximum level to generate the merge data for --></help>
        <label><!-- Max Child Level --></label>
        <name>Apttus__MaxChildLevel__c</name>
    </fields>
    <fields>
        <label><!-- Merge Call Timeout Millis --></label>
        <name>Apttus__MergeCallTimeoutMillis__c</name>
    </fields>
    <fields>
        <label><!-- Merge Webservice Endpoint --></label>
        <name>Apttus__MergeWebserviceEndpoint__c</name>
    </fields>
    <fields>
        <help><!-- The password required to change permissions of the PDF document like printing or editing. --></help>
        <label><!-- PDF Owner Password --></label>
        <name>Apttus__PDFOwnerPwd__c</name>
    </fields>
    <fields>
        <help><!-- Indicates whether author events should be published. If enabled, a record is inserted in the Merge Event table. --></help>
        <label><!-- Publish Author Events --></label>
        <name>Apttus__PublishAuthorEvents__c</name>
    </fields>
    <fields>
        <label><!-- Publish Merge Events --></label>
        <name>Apttus__PublishMergeEvents__c</name>
    </fields>
    <fields>
        <help><!-- Indicates the hours of inactivity after which a temp email template is considered abandoned. The default value is 4 hours --></help>
        <label><!-- Temp Email Template Inactive Hours --></label>
        <name>Apttus__TempEmailTemplateInactiveHours__c</name>
    </fields>
    <fields>
        <help><!-- Indicates agreement lock is turned on for a version-aware record on first document check-out, thereby locking all the record’s documents to that user. Lock released on all documents on first check-in. --></help>
        <label><!-- Use Agreement Locks for Versioning --></label>
        <name>Apttus__UseAgreementLocksForVersioning__c</name>
    </fields>
</CustomObjectTranslation>
