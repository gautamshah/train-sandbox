<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Linea fattura proforma</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Linee fattura proforma</value>
    </caseValues>
    <fields>
        <help><!-- Labor Rate type applied to this line --></help>
        <label><!-- Applied Rate Type --></label>
        <name>SVMXC__Applied_Rate_Type__c</name>
        <picklistValues>
            <masterLabel>Flat Rate</masterLabel>
            <translation>Tariffa base</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Markup %</masterLabel>
            <translation>Contrassegno %</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Per Hour</masterLabel>
            <translation>All&apos;ora</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Billable price computed for this line item. --></help>
        <label><!-- Billable Line Price --></label>
        <name>SVMXC__Billable_Line_Price__c</name>
    </fields>
    <fields>
        <help><!-- Billable Qty computed for this line item. UOM is hours for Labor and Travel if applicable. --></help>
        <label><!-- Billable Qty --></label>
        <name>SVMXC__Billable_Quantity__c</name>
    </fields>
    <fields>
        <help><!-- Case for which this invoice is generated. This is a lookup to an existing salesforce case record. --></help>
        <label><!-- Case --></label>
        <name>SVMXC__Case__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Location covered under the contract being billed. This is applicable to invoices generated for Service Contracts only &amp; is a lookup to a Covered Location record. --></help>
        <label><!-- Covered Locations --></label>
        <name>SVMXC__Covered_Locations__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Product covered under the contract being billed. This is applicable to invoices generated for Service Contracts only &amp; is a lookup to a Covered Product record. --></help>
        <label><!-- Covered Products --></label>
        <name>SVMXC__Covered_Products__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Percentage of discount applied to this line item. --></help>
        <label><!-- Discount % --></label>
        <name>SVMXC__Discount__c</name>
    </fields>
    <fields>
        <help><!-- List of Expenses commonly incurred. --></help>
        <label><!-- Expense Type --></label>
        <name>SVMXC__Expense_Type__c</name>
        <picklistValues>
            <masterLabel>Airfare</masterLabel>
            <translation>Trasporto aereo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Entertainment</masterLabel>
            <translation>Spettacolo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Food - Breakfast</masterLabel>
            <translation>Cibo - Colazione</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Food - Dinner</masterLabel>
            <translation>Cibo - Cena</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Food - Lunch</masterLabel>
            <translation>Cibo - Pranzo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Gas</masterLabel>
            <translation><!-- Gas --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Lodging</masterLabel>
            <translation>Alloggio</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mileage</masterLabel>
            <translation>Chilometraggio</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Miscellaneous</masterLabel>
            <translation>Varie</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Parking</masterLabel>
            <translation>Parcheggio</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Parts</masterLabel>
            <translation>Parti</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation>Telefono</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rental Car</masterLabel>
            <translation>Auto a nolo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tolls</masterLabel>
            <translation>Pedaggi</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Services covered under the contract being billed. This is applicable to invoices generated for Service Contracts only &amp; is a lookup to a Included Service record. --></help>
        <label><!-- Included Services --></label>
        <name>SVMXC__Included_Services__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Installed Product --></label>
        <name>SVMXC__Installed_Product__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Billable indicator. This will be false if this line item is not being billed to customer. --></help>
        <label><!-- Is Billable --></label>
        <name>SVMXC__Is_Billable__c</name>
    </fields>
    <fields>
        <help><!-- Information about the pricing rules applied to this line. This is updated by the Billing Engine. --></help>
        <label><!-- Line Notes --></label>
        <name>SVMXC__Line_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>SVMXC__Line_Type__c</name>
        <picklistValues>
            <masterLabel>Covered Location</masterLabel>
            <translation>Sede coperta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Covered Product</masterLabel>
            <translation>Prodotto coperto</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Expenses</masterLabel>
            <translation>Spese</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Included Service</masterLabel>
            <translation>Servizio incluso</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Labor</masterLabel>
            <translation>Manodopera</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Parts</masterLabel>
            <translation>Parti</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Travel</masterLabel>
            <translation>Viaggi</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Location --></label>
        <name>SVMXC__Location__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- PM&apos;s offered under the contract being billed. This is applicable to invoices generated for Service Contracts only &amp; is a lookup to a PM Offering record. --></help>
        <label><!-- PM Offering --></label>
        <name>SVMXC__PM_Offering__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Price per unit of Part, Labor, Travel or Expense. --></help>
        <label><!-- Price --></label>
        <name>SVMXC__Price__c</name>
    </fields>
    <fields>
        <help><!-- Part used in this Work order. This is a lookup to an existing product record. --></help>
        <label><!-- Product --></label>
        <name>SVMXC__Product__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Invoice under which this line is billed. This is a lookup to an existing Proforma Invoice Detail record. --></help>
        <label><!-- Invoice Detail --></label>
        <name>SVMXC__Proforma_Invoice_Detail__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Document under which this invoice is consolidated. This is a lookup to an existing Proforma Invoice record. --></help>
        <label><!-- Proforma Invoice --></label>
        <name>SVMXC__Proforma_Invoice__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Actual quantity of Parts, Labor, Travel or Expense incurred for this line. UOM for Labor and Travel (if applicable) will be in hours. --></help>
        <label><!-- Quantity --></label>
        <name>SVMXC__Quantity__c</name>
    </fields>
    <fields>
        <label><!-- Service Contract --></label>
        <name>SVMXC__Service_Contract__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Link to labor line recorded on Case. --></help>
        <label><!-- Time Tracker --></label>
        <name>SVMXC__Time_Tracker__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Total price for this line before any contract adjustments/discounts are applied. --></help>
        <label><!-- Total Line Price --></label>
        <name>SVMXC__Total_Line_Price__c</name>
    </fields>
    <fields>
        <help><!-- Work Detail corresponding to this Line entry. This is a lookup to an existing Work detail record. --></help>
        <label><!-- Work Details --></label>
        <name>SVMXC__Work_Details__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Work order for which this invoice is generated. This is a lookup to an existing Work order record. --></help>
        <label><!-- Work Order --></label>
        <name>SVMXC__Work_Order__c</name>
        <relationshipLabel><!-- Proforma Invoice Lines --></relationshipLabel>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Numero record</nameFieldLabel>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
