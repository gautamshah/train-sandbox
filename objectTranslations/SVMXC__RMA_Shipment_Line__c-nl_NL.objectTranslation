<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Regel Onderdelenorder</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Regels Onderdelenorder</value>
    </caseValues>
    <fields>
        <help><!-- Actual number of units shipped/received vs. expected quantity --></help>
        <label><!-- Actual Qty --></label>
        <name>SVMXC__Actual_Quantity2__c</name>
    </fields>
    <fields>
        <help><!-- Date on which the product was actually received --></help>
        <label><!-- Actual Receipt Date --></label>
        <name>SVMXC__Actual_Receipt_Date__c</name>
    </fields>
    <fields>
        <help><!-- Date on which the product was actually shipped --></help>
        <label><!-- Actual Ship Date --></label>
        <name>SVMXC__Actual_Ship_Date__c</name>
    </fields>
    <fields>
        <help><!-- User that canceled this shipment line. This is set automatically when a user cancels the shipment line. --></help>
        <label><!-- Canceled By --></label>
        <name>SVMXC__Canceled_By__c</name>
        <relationshipLabel><!-- Parts Order Lines (Canceled By) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Date/time when this shipment line was canceled. This is set automatically when a user cancels the shipment line. --></help>
        <label><!-- Canceled On --></label>
        <name>SVMXC__Canceled_On__c</name>
    </fields>
    <fields>
        <label><!-- Case Line --></label>
        <name>SVMXC__Case_Line__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- User that closed this RMA/shipment order. --></help>
        <label><!-- Closed By --></label>
        <name>SVMXC__Closed_By__c</name>
        <relationshipLabel><!-- Parts Order Lines (Closed By) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Date/time when this RMA/Shipment line was closed. --></help>
        <label><!-- Closed On --></label>
        <name>SVMXC__Closed_On__c</name>
    </fields>
    <fields>
        <label><!-- Delivered By --></label>
        <name>SVMXC__Delivered_By__c</name>
        <relationshipLabel><!-- Parts Order Lines (Delivered By) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Delivered On --></label>
        <name>SVMXC__Delivered_On__c</name>
    </fields>
    <fields>
        <label><!-- Delivered? --></label>
        <name>SVMXC__Delivered__c</name>
    </fields>
    <fields>
        <label><!-- Delivery Location --></label>
        <name>SVMXC__Delivery_Location__c</name>
        <relationshipLabel><!-- Parts Order Lines (Delivery Location) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Percentage discount to be applied on the Parts Order Line Price --></help>
        <label><!-- Discount % --></label>
        <name>SVMXC__Discount_Percentage__c</name>
    </fields>
    <fields>
        <help><!-- Any special handling notes for product disposition --></help>
        <label><!-- Disposition Instructions --></label>
        <name>SVMXC__Disposition_Instructions__c</name>
    </fields>
    <fields>
        <help><!-- Type of action to be taken by receiving department when the product arrives --></help>
        <label><!-- Disposition --></label>
        <name>SVMXC__Disposition__c</name>
        <picklistValues>
            <masterLabel>Repair</masterLabel>
            <translation>Reparatie</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Restock</masterLabel>
            <translation>Voorraad Aanvullen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Salvage</masterLabel>
            <translation>Bergen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Scrap</masterLabel>
            <translation>Afgedankt</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Entitled Exchange Type --></label>
        <name>SVMXC__Entitled_Exchange_Type__c</name>
        <picklistValues>
            <masterLabel>Advance Exchange</masterLabel>
            <translation>Eerdere Omruil</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Return Exchange</masterLabel>
            <translation>Retour Omruil</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Entitlement Status --></label>
        <name>SVMXC__Entitlement_Status__c</name>
        <picklistValues>
            <masterLabel>Entitled To Warranty</masterLabel>
            <translation>Recht Op Garantie</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Entitled</masterLabel>
            <translation>Niet Gerechtigd</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Condition of the product returned --></help>
        <label><!-- Expected Condition --></label>
        <name>SVMXC__Expected_Condition__c</name>
        <picklistValues>
            <masterLabel>Defective</masterLabel>
            <translation>Defect</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Good/Working</masterLabel>
            <translation>Goed/Werkend</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Shipping Damage</masterLabel>
            <translation>Verzendschade</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Number of products returned. Usually 1 for serialized products --></help>
        <label><!-- Expected Qty --></label>
        <name>SVMXC__Expected_Quantity2__c</name>
    </fields>
    <fields>
        <help><!-- Date on which the product is expected to be received at the depot/factory/warehouse --></help>
        <label><!-- Expected Receipt Date --></label>
        <name>SVMXC__Expected_Receipt_Date__c</name>
    </fields>
    <fields>
        <help><!-- Date on which the product is expected to be shipped to the customer/field location --></help>
        <label><!-- Expected Ship Date --></label>
        <name>SVMXC__Expected_Ship_Date__c</name>
    </fields>
    <fields>
        <label><!-- Fulfillment Qty --></label>
        <name>SVMXC__Fulfillment_Qty__c</name>
    </fields>
    <fields>
        <help><!-- Price for Line Item. --></help>
        <label><!-- Line Price --></label>
        <name>SVMXC__Line_Price2__c</name>
    </fields>
    <fields>
        <help><!-- Current status of the RMA/Shipment line. --></help>
        <label><!-- Line Status --></label>
        <name>SVMXC__Line_Status__c</name>
        <picklistValues>
            <masterLabel>Canceled</masterLabel>
            <translation>Geannuleerd</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Completed</masterLabel>
            <translation>Voltooid</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Open</masterLabel>
            <translation><!-- Open --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Picked</masterLabel>
            <translation>Gekozen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Processing</masterLabel>
            <translation>Verwerking</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Internal line type of the RMA/Shipment line. Not displayed. --></help>
        <label><!-- Line Type --></label>
        <name>SVMXC__Line_Type__c</name>
        <picklistValues>
            <masterLabel>Inbound (RMA)</masterLabel>
            <translation>Inkomend (RMA)</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outbound (Shipment)</masterLabel>
            <translation>Uitgaand (Verzending)</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Master Order Line --></label>
        <name>SVMXC__Master_Order_Line__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Any special handling notes for shipping department --></help>
        <label><!-- Packing Instructions --></label>
        <name>SVMXC__Packing_Instructions__c</name>
    </fields>
    <fields>
        <help><!-- The ServiceMax parts request line for which this shipment line was created. --></help>
        <label><!-- Parts Request Line --></label>
        <name>SVMXC__Parts_Request_Line__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Picked By --></label>
        <name>SVMXC__Picked_By__c</name>
        <relationshipLabel><!-- Parts Order Lines (Picked By) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Picked On --></label>
        <name>SVMXC__Picked_On__c</name>
    </fields>
    <fields>
        <label><!-- Picked Qty --></label>
        <name>SVMXC__Picked_Qty__c</name>
    </fields>
    <fields>
        <label><!-- Picked? --></label>
        <name>SVMXC__Picked__c</name>
    </fields>
    <fields>
        <help><!-- Flag Indicates if this RMA/Shipment line has been posted to inventory or not. --></help>
        <label><!-- Posted To Inventory --></label>
        <name>SVMXC__Posted_To_Inventory__c</name>
    </fields>
    <fields>
        <help><!-- Name of the product to be shipped/received. Is a lookup to an existing salesforce product record --></help>
        <label><!-- Product --></label>
        <name>SVMXC__Product__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- RMA/Shipment number. Is a lookup to an existing RMA/Shipment record in ServiceMax --></help>
        <label><!-- Parts Order --></label>
        <name>SVMXC__RMA_Shipment_Order__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Received By --></label>
        <name>SVMXC__Received_By__c</name>
        <relationshipLabel><!-- Parts Order Lines (Received By) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Received On --></label>
        <name>SVMXC__Received_On__c</name>
    </fields>
    <fields>
        <label><!-- Received? --></label>
        <name>SVMXC__Received__c</name>
    </fields>
    <fields>
        <label><!-- Reconciliation Action --></label>
        <name>SVMXC__Reconciliation_Action__c</name>
        <picklistValues>
            <masterLabel>Add</masterLabel>
            <translation>Toevoegen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hold</masterLabel>
            <translation>Vasthouden</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Remove</masterLabel>
            <translation>Verwijderen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Replace</masterLabel>
            <translation>VERVANGEN</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Condition of the product when received (As expected or otherwise) --></help>
        <label><!-- Returned Condition --></label>
        <name>SVMXC__Returned_Condition__c</name>
        <picklistValues>
            <masterLabel>As Expected</masterLabel>
            <translation>Als Verwacht</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Shipping Damage</masterLabel>
            <translation>Verzendschade</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Route Card --></label>
        <name>SVMXC__Route_Card__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Route Stop --></label>
        <name>SVMXC__Route_Stop__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Sales order number for this shipment if available. --></help>
        <label><!-- Sales Order Number --></label>
        <name>SVMXC__Sales_Order_Number__c</name>
    </fields>
    <fields>
        <help><!-- Field to bind checkbox in List of Parts Order Line records on Visualforce page with controller. --></help>
        <label><!-- Select --></label>
        <name>SVMXC__Select__c</name>
    </fields>
    <fields>
        <label><!-- Serial Number --></label>
        <name>SVMXC__Serial_Number_List__c</name>
    </fields>
    <fields>
        <help><!-- ID of the installed product returned/shipped. Is a lookup to an existing installed product record in ServiceMax --></help>
        <label><!-- IB Serial Number --></label>
        <name>SVMXC__Serial_Number__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Service Contract End Date --></label>
        <name>SVMXC__Service_Contract_End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Service Contract Start Date --></label>
        <name>SVMXC__Service_Contract_Start_Date__c</name>
    </fields>
    <fields>
        <help><!-- Name of the service engineer associated with this RMA/shipment order. Is a lookup to an existing salesforce user record --></help>
        <label><!-- Service Engineer --></label>
        <name>SVMXC__Service_Engineer__c</name>
        <relationshipLabel><!-- Parts Order Lines (Service Engineer) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The ServiceMax service order parts request line for which this shipment line was created. --></help>
        <label><!-- Work Details --></label>
        <name>SVMXC__Service_Order_Line__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Work Order --></label>
        <name>SVMXC__Service_Order__c</name>
        <relationshipLabel><!-- Parts Order Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The actual site/location where the product was shipped/received. Is a lookup to an existing site record --></help>
        <label><!-- Return/Ship Location --></label>
        <name>SVMXC__Ship_Location__c</name>
        <relationshipLabel><!-- Parts Order Lines (Return/Ship Location) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Shipped By --></label>
        <name>SVMXC__Shipped_By__c</name>
        <relationshipLabel><!-- Parts Order Lines (Shipped By) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Shipped On --></label>
        <name>SVMXC__Shipped_On__c</name>
    </fields>
    <fields>
        <label><!-- Shipped? --></label>
        <name>SVMXC__Shipped__c</name>
    </fields>
    <fields>
        <help><!-- Total Line Price --></help>
        <label><!-- Total Line Price --></label>
        <name>SVMXC__Total_Line_Price2__c</name>
    </fields>
    <fields>
        <help>Bevestigings- of foutbericht als resultaat van een transactie. Dit bericht is afgeleid van het uitvoeren van een gedefinieerd proces in SFM API-configuratie.</help>
        <label><!-- Transaction Results --></label>
        <name>SVMXC__Transaction_Results__c</name>
    </fields>
    <fields>
        <help><!-- Leave this box checked if you want the price to be filled automatically from the default price book. If you like to enter a price manually for this part, simply uncheck this box. --></help>
        <label><!-- Use Price From Pricebook --></label>
        <name>SVMXC__Use_Price_From_Pricebook__c</name>
    </fields>
    <fields>
        <help><!-- Name/number of the warehouse from which the products will be shipped --></help>
        <label><!-- Warehouse --></label>
        <name>SVMXC__Warehouse__c</name>
    </fields>
    <fields>
        <label><!-- Warranty End Date --></label>
        <name>SVMXC__Warranty_End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Warranty Start Date --></label>
        <name>SVMXC__Warranty_Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Work Order Created? --></label>
        <name>SVMXC__Work_Order_Created__c</name>
    </fields>
    <gender>Neuter</gender>
    <nameFieldLabel>Regelnummer</nameFieldLabel>
    <recordTypes>
        <label><!-- RMA --></label>
        <name>RMA</name>
    </recordTypes>
    <recordTypes>
        <label><!-- Shipment --></label>
        <name>Shipment</name>
    </recordTypes>
</CustomObjectTranslation>
