<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>IDNSC Task</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>IDNSC Tasks</value>
    </caseValues>
    <fields>
        <label><!-- Finish Date --></label>
        <name>Completion_Date_Actual__c</name>
    </fields>
    <fields>
        <label><!-- Finish Date - Expected --></label>
        <name>Completion_Date_Calc__c</name>
    </fields>
    <fields>
        <help><!-- Checked to indicate the task duration will add to the overall project duration --></help>
        <label><!-- Critical Path --></label>
        <name>Critical_Path__c</name>
    </fields>
    <fields>
        <help><!-- Date the task assigned to role and owner --></help>
        <label><!-- Date Assigned --></label>
        <name>Date_Assigned__c</name>
    </fields>
    <fields>
        <help><!-- This is a description of your task in detail --></help>
        <label><!-- Description --></label>
        <name>Description__c</name>
    </fields>
    <fields>
        <help><!-- How many business days will this task take, accounting for holidays and vacation? --></help>
        <label><!-- Expected Duration in BUSINESS Days --></label>
        <name>Duration_Days_Expected__c</name>
    </fields>
    <fields>
        <label><!-- Duration Days Actual --></label>
        <name>Duration_Days_calc__c</name>
    </fields>
    <fields>
        <label><!-- Duration Days for Calc --></label>
        <name>Duration_Days_for_Calc__c</name>
    </fields>
    <fields>
        <label><!-- Finish Date - Estimated --></label>
        <name>Finish_Date_Estimated__c</name>
    </fields>
    <fields>
        <label><!-- IDNSC Parent --></label>
        <name>IDNSC_Parent__c</name>
        <relationshipLabel><!-- IDNSC Tasks --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Notes --></label>
        <name>Notes__c</name>
    </fields>
    <fields>
        <help><!-- Check this box to NOT send both an email notification and a chatter post.  Only Chatter posts will occur. --></help>
        <label><!-- Only notify via Chatter --></label>
        <name>Only_notify_via_Chatter__c</name>
    </fields>
    <fields>
        <help><!-- An explanation is required when the Actual Completion Date is greater than the Expected Completion Date --></help>
        <label><!-- Reason if Completed Late --></label>
        <name>Reason_Completed_Late__c</name>
    </fields>
    <fields>
        <label><!-- Role --></label>
        <name>Role__c</name>
        <picklistValues>
            <masterLabel>Customer Care Manager</masterLabel>
            <translation><!-- Customer Care Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Service</masterLabel>
            <translation><!-- Customer Service --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Data Steward</masterLabel>
            <translation><!-- Data Steward --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>EDI Support</masterLabel>
            <translation><!-- EDI Support --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pricing Specialist</masterLabel>
            <translation><!-- Pricing Specialist --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RVP</masterLabel>
            <translation><!-- RVP --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rebates and Tracings Analyst</masterLabel>
            <translation><!-- Rebates and Tracings Analyst --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Sales Analyst</masterLabel>
            <translation><!-- Sales Analyst --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Transportation Analyst</masterLabel>
            <translation><!-- Transportation Analyst --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Runs in State --></label>
        <name>Runs_in_State__c</name>
        <picklistValues>
            <masterLabel>Accounts Confirmation</masterLabel>
            <translation><!-- Accounts Confirmation --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CS Ready</masterLabel>
            <translation><!-- CS Ready --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Contracts Loaded</masterLabel>
            <translation><!-- Contracts Loaded --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Contracts Validated</masterLabel>
            <translation><!-- Contracts Validated --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Confirmed Pricing</masterLabel>
            <translation><!-- Customer Confirmed Pricing --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Kickoff</masterLabel>
            <translation><!-- Customer Kickoff --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Data Synchronization Phase 1</masterLabel>
            <translation><!-- Data Synchronization Phase 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Go-Live Cutover</masterLabel>
            <translation><!-- Go-Live Cutover --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Go-Live Ready</masterLabel>
            <translation><!-- Go-Live Ready --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Initial Stocking Order</masterLabel>
            <translation><!-- Initial Stocking Order --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Initiation</masterLabel>
            <translation><!-- Initiation --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Live</masterLabel>
            <translation><!-- Live --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Preparation Activity</masterLabel>
            <translation><!-- Preparation Activity --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pricing Approval</masterLabel>
            <translation><!-- Pricing Approval --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pricing Distributed</masterLabel>
            <translation><!-- Pricing Distributed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pricing Loaded</masterLabel>
            <translation><!-- Pricing Loaded --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pricing Report Complete</masterLabel>
            <translation><!-- Pricing Report Complete --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pricing Synchronization</masterLabel>
            <translation><!-- Pricing Synchronization --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Required when task status is set to Skipped or On Hold --></help>
        <label><!-- Reason if Skipped/On Hold --></label>
        <name>Skipped_Reason__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>Start_Date_Actual__c</name>
    </fields>
    <fields>
        <label><!-- Start Date - Estimated --></label>
        <name>Start_Date_Expected__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Completed</masterLabel>
            <translation><!-- Completed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Progress</masterLabel>
            <translation><!-- In Progress --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Started</masterLabel>
            <translation><!-- Not Started --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Hold</masterLabel>
            <translation><!-- On Hold --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Skipped</masterLabel>
            <translation><!-- Skipped --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Task Number --></label>
        <name>Task_Number__c</name>
    </fields>
    <fields>
        <help><!-- Set via workflow, but can be overwritten manually --></help>
        <label><!-- Task Owner Email --></label>
        <name>Task_Owner_Email__c</name>
    </fields>
    <fields>
        <label><!-- Task Owner --></label>
        <name>Task_Owner__c</name>
    </fields>
    <fields>
        <help><!-- Used for capacity and workforce planning. Represents the % of time of the duration days the task owner actually spends working on the task. --></help>
        <label><!-- Work Effort as Percent of Duration --></label>
        <name>Work_Effort_as_Percent_of_Duration__c</name>
    </fields>
    <fields>
        <help><!-- Enter email address to notify a person that the task is to be worked.  Typically sent to a non-salesforce user. --></help>
        <label><!-- cc email --></label>
        <name>cc_email__c</name>
    </fields>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- Completion date must be after the actual start date --></errorMessage>
        <name>Completion_Date_before_Start_Date</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Task Names Can Not be changed --></errorMessage>
        <name>Do_not_allow_editing_Task_Name</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please explain why the task was completed after the expected completion date --></errorMessage>
        <name>Reason_Required_if_Late</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please include a reason as to why the task is being placed on hold --></errorMessage>
        <name>Reason_Required_if_On_Hold</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please include a reason as to why the task is being skipped --></errorMessage>
        <name>Reason_Required_if_Skipped</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You must enter the Actual Start Date when the task is in progress or completed --></errorMessage>
        <name>Require_Start_Date</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please set the date when the task was completed --></errorMessage>
        <name>Required_when_Status_is_Completed</name>
    </validationRules>
    <webLinks>
        <label><!-- Mass_Edit --></label>
        <name>Mass_Edit</name>
    </webLinks>
</CustomObjectTranslation>
