({  
    completeMilestone : function(component, controllerMethodName) {
        
    	var action = component.get(controllerMethodName);
        action.setParams({
            "msId": component.get("v.recordId")
        });
		
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                
				var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/" + response.getReturnValue()
                });
                urlEvent.fire();
                
            }
            else if (component.isValid() && state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	alert("Error message: " + 
                                 errors[0].message); 
                    }
                } 
            }
        });
        
        $A.enqueueAction(action);
	}
    
})