({
    doInit: function(component) {
		var action = component.get("c.isPremium");
        action.setCallback(this, function(response) {
        	component.set("v.premium", response.getReturnValue());
        });
        
        $A.enqueueAction(action);
    },
    stayOnCurrentRecord: function(component, event, helper) {
        helper.completeMilestone(component, 'c.backMilestone');
    },
    goToNextRecord: function(component, event, helper) {
    	helper.completeMilestone(component, 'c.retrieveNextSuccessorMilestone');
    }
})