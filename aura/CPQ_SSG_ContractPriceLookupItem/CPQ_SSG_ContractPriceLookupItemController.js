({
	removeProduct : function(cmp) {
		var product = cmp.get("v.product");
		var removeEvent = cmp.getEvent("removeProduct");
		removeEvent.setParams({"product":product}).fire();
	}
})