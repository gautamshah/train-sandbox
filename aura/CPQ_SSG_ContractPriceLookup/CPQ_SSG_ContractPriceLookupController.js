({
	doInit: function(cmp, evt, hlp) {
		hlp.hideProductLookup(cmp);
	},
	acctChanged: function(cmp, evt, hlp) {
		// Value is changed to valid value
		if(evt.getParam('value') && evt.getParam('value') !== evt.getParam('oldValue') && typeof evt.getParam('value') === 'string') {
			hlp.getAccount(cmp);
			hlp.showProductLookup(cmp);
		} else {
			// Clear account and product information
			cmp.set('v.account.Id', null);
			hlp.removeProducts(cmp);
			hlp.hideProductLookup(cmp);
		}
	},
	openUrl: function(cmp) {
		var recordId = cmp.get("v.account.Id");

		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
			"recordId": recordId,
			"slideDevName": "detail"
		});

		// var navEvt = $A.get("e.force:navigateToURL");
		// navEvt.setParams({
		// 	"url": "/" + recordId
		// });

		navEvt.fire();
	},
	prodChanged: function(cmp, evt, hlp) {
		// Value is changed to valid value
		if(evt.getParam('value') && evt.getParam('value') !== evt.getParam('oldValue') && typeof evt.getParam('value') === 'string') {
			hlp.getProductPrice(cmp, hlp);
		}
	},
	removeProduct: function(cmp, evt, hlp) {
		hlp.removeProduct(cmp, evt);
	},
	getPricing: function(cmp, evt, hlp) {
		hlp.getPricing(cmp, hlp);
	}
})