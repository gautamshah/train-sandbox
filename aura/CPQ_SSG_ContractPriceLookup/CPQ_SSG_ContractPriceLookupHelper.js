({
	getAccount : function(cmp) {
		var action = cmp.get('c.getAccount');
		var accountId = cmp.get('v.record.Account__c');
		action.setParams({accountId : accountId});
		action.setCallback(this, function(res){
			cmp.set('v.account', res.getReturnValue());
		});
		$A.enqueueAction(action);
	},
	addToList : function(cmp, hlp, object) {
			var products = cmp.get('v.products');
			var hasItem = hlp.hasItem('productId', object.productId, products);
			if(!hasItem) {
				products.push(object);
				cmp.set('v.products', products);
			}
	},
	hasItem : function(key, value, array) {
		for(var i=0; i < array.length; i += 1){
			if(array[i][key] === value) {
				return true;
			}
		}
		return false;
	},
	showProductLookup: function(cmp) {
			var cmpTarget = cmp.find('productLookupElement');
			$A.util.removeClass(cmpTarget, 'display-none');
	},
	hideProductLookup: function(cmp) {
		var cmpTarget = cmp.find('productLookupElement');
		$A.util.addClass(cmpTarget, 'display-none');
	},
	removeProducts: function(cmp){
		cmp.set('v.products', []);
	},
	getProductPrice : function(cmp, hlp) {
		// Create new Product Price record
		var action = cmp.get('c.getProductPrice');
		var productId = cmp.get('v.record.Product__c');
		action.setParams({productId : productId});
		action.setCallback(this, function(res){
			var state = res.getState();
			if (state === "SUCCESS") {
				hlp.addToList(cmp, hlp, res.getReturnValue());
			} else if (state === "ERROR") {
				var error = res.getError()[0].message;
				hlp.openPrompt({
					"header" : "Product Error",
					"message" : error
				});
			}
			hlp.clearProduct(cmp);
		});
		$A.enqueueAction(action);
	},
	clearProduct : function(cmp) {
		var field = cmp.find('productLookup');
		// field.set('v.selectedLabel', '');
		field.set('v.value', '');
	},
	removeProduct : function(cmp, evt) {
		var product = evt.getParam('product');
		var products = cmp.get('v.products');
		for(var i=0; i<products.length; i += 1){
			if(products[i].Id === product.Id){
				products.splice(i, 1);
				break;
			}
		}
		cmp.set('v.products', products);
	},
	openPrompt : function(params) {
		var modalEvt = $A.get('e.c:CPQ_SSG_ContractPriceLookupPromptToggleEvt');
		modalEvt.setParams(params);
		modalEvt.fire();
	},
	getPricing : function(cmp, hlp) {
		var spinner = cmp.find('theSpinner');
		var action = cmp.get('c.getCalloutPricing');
		var products = cmp.get('v.products');
		var productsJSON = JSON.stringify(products);
		// var productsJSON = $A.util.json.encode(products);
		var account = cmp.get('v.account');

		// Show the spinner
		$A.util.removeClass(spinner, 'slds-hide');
		// Prepare the callout
		action.setParams({
			"productListJSON" : productsJSON,
			"acct" : account
		});
		action.setCallback(this, function(res){

			// Hide the spinner
			$A.util.addClass(spinner, 'slds-hide');

			var state = res.getState();
			if (state === "SUCCESS") {
				products = res.getReturnValue();
				cmp.set('v.products', products);
			} else if (state === "ERROR") {
				var errors = res.getError();
				var error = '';
				if (errors) {
					if (errors[0] && errors[0].message) {
						error = errors[0].message;
					}
				} else {
					error = 'Unknown error';
				}
				hlp.openPrompt({
					"header" : "Pricing Error",
					"message" : error
				});
			}
		});
		$A.enqueueAction(action);
	}
})