<aura:documentation>
	<aura:description>
		<h1>Overview</h1>
		<table>
	    <tbody>
	      <tr>
	        <td>@Author</td>
	        <td>Isaac Lewis</td>
	      </tr>
	      <tr>
	        <td>@CreatedDate</td>
	        <td>2016-03-31</td>
	      </tr>
	      <tr>
	        <td>@Description</td>
	        <td>The <code>c:CPQ_SSG_ContractPriceLookup</code> component implements a mobile version of the SSG pricing lookup tool.</td>
	      </tr>
				<tr>
					<td>@See</td>
					<td>Dekstop Visualforce Page: <code>CPQ_SSG_ContractPriceLookup.page</code></td>
				</tr>
	    </tbody>
	  </table>
		<h1>Change Log</h1>
		<table>
			<thead>
				<tr>
					<th>@ModifiedBy</th>
					<th>@ModifiedDate</th>
					<th>@ChangeDescription</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Isaac Lewis</td>
					<td>2016-04-04</td>
					<td>Added "Unit Price" column header to display <code>qtyUnitPrice</code></td>
				</tr>
				<tr>
					<td>Isaac Lewis</td>
					<td>2016-04-06</td>
					<td>Added "Effective Date" column header to display <code>effectiveDate</code></td>
				</tr>
				<tr>
					<td>Isaac Lewis</td>
					<td>2016-04-18</td>
					<td>Updated JS to comply with the <a href="https://www.npmjs.com/package/salesforce-lightning-cli" target="_blank">saleforce-lightning-cli</a> linter</td>
				</tr>
				<tr>
					<td>Isaac Lewis</td>
					<td>2016-07-15</td>
					<td>Added hidden <code>force:outputField</code> references as a workaround for Salesforce bug #W-3211778</td>
				</tr>
				<tr>
					<td>Isaac Lewis</td>
					<td>2016-08-03</td>
					<td>Modified <code>ui:outputURL</code> to fix navigation issues when previewing selected Account record</td>
				</tr>
				<tr>
					<td>Isaac Lewis</td>
					<td>2016-09-20</td>
					<td>Removed reference to <code>v.selectedLabel</code> to prevent access error from throwing after selecting a product.</td>
				</tr>
			</tbody>
		</table>
	</aura:description>
</aura:documentation>