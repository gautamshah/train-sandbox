({
    doInit: function(component) {
		var action = component.get("c.isPremium");
        action.setCallback(this, function(response) {
        	component.set("v.premium", response.getReturnValue());
        });
         
        $A.enqueueAction(action);
    },
    stayOnCurrentRecord: function(component, event, helper) {
        helper.completeTask(component, 'c.backTask');
    },
    goToNextRecord: function(component, event, helper) {
    	helper.completeTask(component, 'c.retrieveNextSuccessorTask');
    }
})