({
	doInit : function(cmp, evt, hlp) {
		hlp.doInit(cmp, hlp);
	},
	onObjectTypeChange : function(cmp, evt, hlp) {
		hlp.setRecordTypes(cmp, hlp);
	},
	onRecordTypeChange : function(cmp, evt, hlp) {
		hlp.toggleCreateDeal(cmp);
	},
	createDeal : function(cmp, evt, hlp) {
		hlp.createDeal(cmp);
	},
	deleteDeal : function(cmp, evt, hlp) {
		hlp.deleteDeal(cmp);
	},
	saveDeal : function(cmp) {
		cmp.find("edit").get("e.recordSave").fire();
	},
	onSaveSuccess : function(cmp, evt, hlp) {

		// Get recordId before reset
		var recordId = cmp.get('v.recordId');

		// Reset forms and data
		hlp.doInit(cmp, hlp);

		// Open the Record
		$A.get('e.force:navigateToSObject')
			.setParams({"recordId" : recordId})
			.fire();

	},
	onOpportunityChange : function(cmp, evt, hlp) {

		// If value is changed to valid value
		if(evt.getParam('value') && evt.getParam('value') !== evt.getParam('oldValue') && typeof evt.getParam('value') === 'string') {
			hlp.getOpportunity(cmp, hlp);
			// Hide acount lookup
			$A.util.addClass(cmp.find('accountLookupElement'), 'slds-hide');
			$A.util.addClass(cmp.find('accountLookupLabel'), 'slds-hide');
		} else {
			hlp.clearLookup(cmp, 'opportunityLookup');
			cmp.set('v.opportunity', '');
			// Show account lookup
			$A.util.removeClass(cmp.find('accountLookupElement'), 'slds-hide');
			$A.util.removeClass(cmp.find('accountLookupLabel'), 'slds-hide');
		}

	},
	onAccountChange : function(cmp, evt, hlp) {

		// If value is changed to valid value
		if(evt.getParam('value') && evt.getParam('value') !== evt.getParam('oldValue') && typeof evt.getParam('value') === 'string') {
			hlp.getAccount(cmp, hlp);
			// Hide opportunity lookup
			$A.util.addClass(cmp.find('opportunityLookupElement'), 'slds-hide');
			$A.util.addClass(cmp.find('opportunityLookupLabel'), 'slds-hide');
		} else {
			hlp.clearLookup(cmp, 'accountLookup');
			cmp.set('v.account', '');
			// Show opportunity lookup
			$A.util.removeClass(cmp.find('opportunityLookupElement'), 'slds-hide');
			$A.util.removeClass(cmp.find('opportunityLookupLabel'), 'slds-hide');
		}

	}
})