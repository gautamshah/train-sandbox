({
	doInit : function(cmp, hlp) {

		var configForm = cmp.find('theConfigForm');
		var editForm = cmp.find('theEditForm');
		var recordId = cmp.get('v.recordId');

		// Clear data
		cmp.set('v.recordId', '');
		cmp.set('v.account', '');
		cmp.set('v.opportunity', '');

		// Reset Lookups
		hlp.clearLookup(cmp, 'opportunityLookup');
		hlp.clearLookup(cmp, 'accountLookup');

		// Reset Picklists
		hlp.setObjectTypes(cmp);
		hlp.setRecordTypes(cmp, hlp);

		// Hide Edit Form, Display Config Form
		$A.util.removeClass(configForm, 'slds-hide');
		$A.util.addClass(editForm, 'slds-hide');

	},
	getOpportunity : function(cmp, hlp) {

		var action = cmp.get('c.getOpportunity');
		var opportunityId = cmp.find('opportunityLookup').get("v.value");
		action.setParams({oppId : opportunityId});
		action.setCallback(this, function(res){
			var opportunity = res.getReturnValue();
			cmp.set('v.opportunity', opportunity);
			hlp.clearLookup(cmp, 'accountLookup');
			cmp.set('v.account', '');
		});
		$A.enqueueAction(action);

	},
	getAccount : function(cmp, hlp) {

		var action = cmp.get('c.getAccount');
		var accountId = cmp.find('accountLookup').get("v.value");
		action.setParams({accId : accountId});
		action.setCallback(this, function(res){
			var account = res.getReturnValue();
			cmp.set('v.account', account);
			hlp.clearLookup(cmp, 'opportunityLookup');
			cmp.set('v.opportunity', '');
		});
		$A.enqueueAction(action);

	},
	clearLookup : function(cmp, fieldId) {

		var field = cmp.find(fieldId);
		// field.set('v.selectedLabel', '');
		field.set('v.value', '');

	},
	setObjectTypes : function(cmp) {

		var opts = [
				{ label: "Proposal", value: "Apttus_Proposal__Proposal__c", selected: "true" },
				{ label: "Agreement", value: "Apttus__APTS_Agreement__c" }
		];
		cmp.find("objectTypeInputSelect").set("v.options", opts);

	},
	toggleCreateDeal : function(cmp) {

		var recordTypeId = cmp.find("recordTypeInputSelect").get("v.value");
		var create = cmp.find('createOptions');
		if(recordTypeId && recordTypeId !== '--None--'){
			$A.util.removeClass(create, 'slds-hide');
		} else {
			$A.util.addClass(create, 'slds-hide');
		}

	},
	setRecordTypes : function(cmp, hlp) {

		var action = cmp.get('c.getRecordTypes');
		var sObjectName = cmp.find("objectTypeInputSelect").get("v.value");
		var spinner = cmp.find('theSpinner');
		$A.util.removeClass(spinner, 'slds-hide');

		action.setParams({sObjectName : sObjectName});
		action.setCallback(this, function(res){
			var recordTypes = res.getReturnValue();
			// Set record type list
			cmp.set('v.recordTypes', recordTypes);
			// Set default selection
			var selected = cmp.find("recordTypeInputSelect");
			if(recordTypes.length === 1){
				selected.set('v.value', recordTypes[0].Id);
			} else {
				selected.set("v.value", "--None--");
			}
			hlp.toggleCreateDeal(cmp);
			$A.util.addClass(spinner, 'slds-hide');
		});
		$A.enqueueAction(action);

	},
	createDeal : function(cmp) {

		var configForm = cmp.find('theConfigForm');
		var editForm = cmp.find('theEditForm');
		var spinner = cmp.find('theSpinner');
		$A.util.toggleClass(spinner, 'slds-hide');

		var objectType = cmp.find("objectTypeInputSelect").get("v.value");
		var recordTypeId = cmp.find("recordTypeInputSelect").get("v.value");
		var opportunity = cmp.get("v.opportunity");
		var account = cmp.get("v.account");
		var parent;
		if(opportunity){
			parent = opportunity;
		}
		if(account){
			parent = account;
		}

		var action = cmp.get('c.createRecord');
		action.setParams({
			sObjectName : objectType,
			recordTypeId : recordTypeId,
			parentId : parent.Id
		});
		action.setCallback(this, function(res){
			var recordId = res.getReturnValue().Id;
			cmp.set('v.recordId', recordId);
			$A.util.toggleClass(spinner, 'slds-hide');
			// Hide Config Form, Display Edit Form
			$A.util.toggleClass(configForm, 'slds-hide');
			$A.util.toggleClass(editForm, 'slds-hide');
		});

		$A.enqueueAction(action);

	},
	toggleConfig : function(cmp) {

		var edit = cmp.find('theConfigForm');
		$A.util.toggleClass(edit, 'slds-hide');

	},
	deleteDeal : function(cmp) {

		var configForm = cmp.find('theConfigForm');
		var editForm = cmp.find('theEditForm');
		var recordId = cmp.get('v.recordId');
		var spinner = cmp.find('theSpinner');
		$A.util.toggleClass(spinner, 'slds-hide');

		var action = cmp.get('c.deleteRecord');
		action.setParams({
			recordId : recordId
		});
		action.setCallback(this, function(res){
			var isSuccess = res.getReturnValue();
			if(isSuccess) {
				// Hide Popup, Display Edit
				$A.util.toggleClass(configForm, 'slds-hide');
				$A.util.toggleClass(editForm, 'slds-hide');
				// Clear Record Data
				cmp.set('v.recordId', '');

			} else {
				console.log('Error: ', isSuccess);
			}
			$A.util.toggleClass(spinner, 'slds-hide');
		});
		$A.enqueueAction(action);

	}
})