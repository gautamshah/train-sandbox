({
	close : function(cmp) {

		// Hide the component
		var modal = cmp.find('theModal');
		var backdrop = cmp.find('theModalBackdrop');
		$A.util.removeClass(modal, 'slds-fade-in-open');
		$A.util.removeClass(backdrop, 'slds-backdrop--open');

		// Clear component data
		cmp.set('v.header', '');
		cmp.set('v.message', '');

	},
	open : function(cmp, evt) {

		// Set component data
		var header = evt.getParam('header');
		var message = evt.getParam('message');
		cmp.set('v.header', header);
		cmp.set('v.message', message);

		// Display the component
		var modal = cmp.find('theModal');
		var backdrop = cmp.find('theModalBackdrop');
		$A.util.addClass(modal, 'slds-fade-in-open');
		$A.util.addClass(backdrop, 'slds-backdrop--open');

	}
})