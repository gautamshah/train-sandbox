({
	close : function(cmp, evt, hlp) {
		hlp.close(cmp);
	},
	open : function(cmp, evt, hlp) {
		hlp.open(cmp, evt);
	}
})