<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Represents a single dimension in a dimension set</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Apttus_DealMgr__DimensionId__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the associated dimension</description>
        <externalId>false</externalId>
        <label>Dimension</label>
        <referenceTo>Apttus_DealMgr__DealGuidanceDimension__c</referenceTo>
        <relationshipLabel>Dimension Sets</relationshipLabel>
        <relationshipName>DimensionSets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Apttus_DealMgr__DimensionSetId__c</fullName>
        <deprecated>false</deprecated>
        <description>ID of the associated dimension set</description>
        <externalId>false</externalId>
        <label>Dimension Set</label>
        <referenceTo>Apttus_DealMgr__DealGuidanceDimensionSet__c</referenceTo>
        <relationshipLabel>Dimensions</relationshipLabel>
        <relationshipName>Dimensions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Apttus_DealMgr__InputType__c</fullName>
        <deprecated>false</deprecated>
        <description>The input type associated with the dimension</description>
        <externalId>false</externalId>
        <label>Input Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Discrete</fullName>
                    <default>false</default>
                    <label>Discrete</label>
                </value>
                <value>
                    <fullName>Range</fullName>
                    <default>false</default>
                    <label>Range</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Apttus_DealMgr__Sequence__c</fullName>
        <deprecated>false</deprecated>
        <description>The dimension sequence</description>
        <externalId>false</externalId>
        <inlineHelpText>The dimension sequence</inlineHelpText>
        <label>Sequence</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Deal Guidance Dimension Set Member</label>
    <nameField>
        <label>Dimension Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Deal Guidance Dimension Set Members</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
