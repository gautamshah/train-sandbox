<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Allows user to associate multiple products to a Clinical Evaluation</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Capital_Equipment_Installed_Base_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Capital Equipment Installed Base Unit</label>
        <referenceTo>Asset</referenceTo>
        <relationshipLabel>Clinical Evaluation Products</relationshipLabel>
        <relationshipName>Clinical_Evaluation_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Clinical_Evaluation__c</fullName>
        <externalId>false</externalId>
        <label>Clinical Evaluation</label>
        <referenceTo>Clinical_Evaluation__c</referenceTo>
        <relationshipLabel>Clinical Evaluation Products</relationshipLabel>
        <relationshipName>Clinical_Evaluation_Products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Price__c</fullName>
        <description>Price of each individual product line item in the Clinical Product evaluation</description>
        <externalId>false</externalId>
        <inlineHelpText>Price of each individual product line item in the Clinical Product evaluation</inlineHelpText>
        <label>Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Product_SKU__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Product SKU</label>
        <referenceTo>Product_SKU__c</referenceTo>
        <relationshipLabel>Clinical Evaluation Products</relationshipLabel>
        <relationshipName>Clinical_Evaluation_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Clinical Evaluation Products</relationshipLabel>
        <relationshipName>Clinical_Evaluation_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Serial__c</fullName>
        <externalId>false</externalId>
        <label>Serial #</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Price__c</fullName>
        <externalId>false</externalId>
        <formula>Price__c *  Units_eaches__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Units_eaches__c</fullName>
        <externalId>false</externalId>
        <label>Units (eaches)</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Clinical Evaluation Product</label>
    <nameField>
        <displayFormat>CEPID-{000000}</displayFormat>
        <label>CEPID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Clinical Evaluation Products</pluralLabel>
    <recordTypes>
        <fullName>Master_Clinical_Evaluation_Product_Record_Type</fullName>
        <active>true</active>
        <description>Master Record Type for Clinical Evaluation</description>
        <label>Master Clinical Evaluation Product Record Type</label>
    </recordTypes>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Clinical_Evaluation__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>Clinical_Evaluation__c</lookupFilterFields>
        <lookupFilterFields>Product__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Clinical_Evaluation__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Clinical_Evaluation__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
