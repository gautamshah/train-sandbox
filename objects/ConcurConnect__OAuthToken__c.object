<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Describes a user&apos;s 3-Legged OAuth token</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ConcurConnect__Consumer_Key__c</fullName>
        <deprecated>false</deprecated>
        <description>Consumer Key for the Concur Partner Application</description>
        <externalId>false</externalId>
        <label>Consumer Key</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConcurConnect__Consumer_Secret__c</fullName>
        <deprecated>false</deprecated>
        <description>Secret for a Concur Partner Application</description>
        <externalId>false</externalId>
        <label>Consumer Secret</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConcurConnect__Is_Access_Token__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Whether or not the token is an access token or a request token.</description>
        <externalId>false</externalId>
        <label>Is Access Token</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ConcurConnect__Token_Secret__c</fullName>
        <deprecated>false</deprecated>
        <description>Contains the Token Secret for the User&apos;s Request or Access Token</description>
        <externalId>false</externalId>
        <label>Token Secret</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConcurConnect__Token__c</fullName>
        <deprecated>false</deprecated>
        <description>Contains the Request and Eventually the Access Token</description>
        <externalId>false</externalId>
        <label>Token</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>OAuth Token</label>
    <listViews>
        <fullName>ConcurConnect__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Token Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>OAuth Tokens</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>ConcurConnect__Is_Access_Token__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ConcurConnect__Consumer_Key__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ConcurConnect__Consumer_Secret__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ConcurConnect__Token__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ConcurConnect__Token_Secret__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OWNER.ALIAS</customTabListAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
