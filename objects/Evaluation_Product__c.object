<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Additional_Comments__c</fullName>
        <externalId>false</externalId>
        <label>Additional Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Clinical_Performance__c</fullName>
        <externalId>false</externalId>
        <label>Clinical Performance</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Acceptable</fullName>
                    <default>false</default>
                    <label>Acceptable</label>
                </value>
                <value>
                    <fullName>Further Evaluation Needed</fullName>
                    <default>false</default>
                    <label>Further Evaluation Needed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Community_Evaluation_ID__c</fullName>
        <description>This field adds the community evaluation id from the auto number name of the community evaluation</description>
        <externalId>false</externalId>
        <formula>Community_Evaluation__r.Name</formula>
        <label>Community Evaluation ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Community_Evaluation__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Community Evaluation</label>
        <referenceTo>Community_Evaluation__c</referenceTo>
        <relationshipLabel>Evaluation Products</relationshipLabel>
        <relationshipName>Evaluation_Products</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Ease_of_Use__c</fullName>
        <externalId>false</externalId>
        <label>Ease of Use</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Acceptable</fullName>
                    <default>false</default>
                    <label>Acceptable</label>
                </value>
                <value>
                    <fullName>Further Evaluation Needed</fullName>
                    <default>false</default>
                    <label>Further Evaluation Needed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>If_Further_Evaluation_Needed__c</fullName>
        <externalId>false</externalId>
        <label>If Further Evaluation Needed</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Clinical_Performance__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending additional cases</fullName>
                    <default>true</default>
                    <label>Pending additional cases</label>
                </value>
                <value>
                    <fullName>Pending patient outcomes</fullName>
                    <default>false</default>
                    <label>Pending patient outcomes</label>
                </value>
                <value>
                    <fullName>Pending cases and outcomes</fullName>
                    <default>false</default>
                    <label>Pending cases and outcomes</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Further Evaluation Needed</controllingFieldValue>
                <valueName>Pending additional cases</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Further Evaluation Needed</controllingFieldValue>
                <valueName>Pending patient outcomes</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Further Evaluation Needed</controllingFieldValue>
                <valueName>Pending cases and outcomes</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Product_SKU__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Product SKU</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>(1 AND (2 OR 3)) OR (4 AND 5 AND 6)</booleanFilter>
            <filterItems>
                <field>Product_SKU__c.Country__c</field>
                <operation>equals</operation>
                <value>US</value>
            </filterItems>
            <filterItems>
                <field>$Profile.Id</field>
                <operation>equals</operation>
                <value>00eU0000000dLtk</value>
            </filterItems>
            <filterItems>
                <field>$Profile.Id</field>
                <operation>equals</operation>
                <value>00eU0000000dLsn</value>
            </filterItems>
            <filterItems>
                <field>Product_SKU__c.Country__c</field>
                <operation>equals</operation>
                <value>AE, AT, AU, AZ, BE, BH, CA, CH, CN, DE, DK, ES, FI, FR, GB, HK, IE, IL, IR, IT, JO, JP, KR, KW, LY, NL, NO, NZ, OM, PL, PT, QA, RU, SA, SE, SG, TR, TW, US, YE, ZA</value>
            </filterItems>
            <filterItems>
                <field>$Profile.Id</field>
                <operation>notEqual</operation>
                <value>00eU0000000dLtk</value>
            </filterItems>
            <filterItems>
                <field>$Profile.Id</field>
                <operation>notEqual</operation>
                <value>00eU0000000dLsn</value>
            </filterItems>
            <infoMessage>Select All Fields to search by SKU</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product_SKU__c</referenceTo>
        <relationshipLabel>Evaluation Products</relationshipLabel>
        <relationshipName>Evaluation_Products</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_Sku_ID__c</fullName>
        <description>This field pulss in the item code from the product sku object for the selected product sku</description>
        <externalId>false</externalId>
        <formula>Product_SKU__r.SKU__c</formula>
        <label>Product Sku ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Sku_Name__c</fullName>
        <description>This field pulls in the product name for the selected product sku</description>
        <externalId>false</externalId>
        <formula>Product_SKU__r.Product_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Sku Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Evaluation Product</label>
    <nameField>
        <displayFormat>CEP-{0000}</displayFormat>
        <label>Evaluation Product Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Evaluation Products</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>FIll_Additional_comments</fullName>
        <active>true</active>
        <description>Fill the additional comments if you select Ease of use as Further Evaluation Needed</description>
        <errorConditionFormula>ISPICKVAL( Ease_of_Use__c ,&apos;Further Evaluation Needed&apos;) &amp;&amp; (ISNULL( Additional_Comments__c ) ||  ISBLANK( Additional_Comments__c ))</errorConditionFormula>
        <errorDisplayField>Additional_Comments__c</errorDisplayField>
        <errorMessage>Add comments when selecting Further Evaluation Needed</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Evaluation_Product</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Evaluation Product</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/a4B/e?CF00NU000000539fU={!Community_Evaluation__c.Name }&amp;CF00NU000000539fU_lkid={!Community_Evaluation__c.Id }&amp;retURL=%2F{!Community_Evaluation__c.Id}&amp;saveURL=%2F{!  Community_Evaluation__c.Id}</url>
    </webLinks>
</CustomObject>
