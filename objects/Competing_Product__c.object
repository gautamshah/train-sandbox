<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This Object allows the ability to track information about competitor products at the Opportunity level</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Awarded_Opportunity_Amount__c</fullName>
        <description>Please indicate Opportunity value awarded to this Competitor</description>
        <externalId>false</externalId>
        <inlineHelpText>Please indicate Opportunity value awarded to this Competitor</inlineHelpText>
        <label>Awarded Opportunity Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Awarded_Opportunity_Share__c</fullName>
        <description>Please indicate % share of Opportunity value awarded to this Competitor</description>
        <externalId>false</externalId>
        <inlineHelpText>Please indicate % share of Opportunity value awarded to this Competitor</inlineHelpText>
        <label>Awarded Opportunity Share (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Competing_Product_Check__c</fullName>
        <externalId>false</externalId>
        <formula>1</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Competing Product Check</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Competitor_Price__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please enter the Competitor&apos;s Price if indicated on the Tender.</inlineHelpText>
        <label>Competitor Price</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Competitor_Product__c</fullName>
        <externalId>false</externalId>
        <label>Competitor Product</label>
        <length>150</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Competitor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Competitor</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Account - Manufacturer, Japan - Manufacturer</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account Competitor</relationshipLabel>
        <relationshipName>Account_Competitor</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>JP_Total__c</fullName>
        <externalId>false</externalId>
        <formula>Competitor_Price__c  *  Quantity__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Objections__c</fullName>
        <externalId>false</externalId>
        <label>Objections</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Competing Products</relationshipLabel>
        <relationshipName>Competing_Products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Other_Competitor__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the competitor name in this field if you don&apos;t find the needed competitor in the predefined list.</inlineHelpText>
        <label>Other Competitor</label>
        <length>150</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Manufacturer_Product__c.ManufacturerOEM_Name__c</field>
                <operation>equals</operation>
                <valueField>$Source.Competitor__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Manufacturer_Product__c</referenceTo>
        <relationshipLabel>Competing Products</relationshipLabel>
        <relationshipName>Competing_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <description>Specifies quantity of Competitor Product for an Opportunity</description>
        <externalId>false</externalId>
        <inlineHelpText>Please enter Competitor Product Quantity (If known)</inlineHelpText>
        <label>Annual Units</label>
        <precision>8</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Strengths__c</fullName>
        <description>Please specify any strengths this competitor has relative to this Opportunity</description>
        <externalId>false</externalId>
        <inlineHelpText>Please specify any strengths this competitor has relative to this Opportunity</inlineHelpText>
        <label>Strengths</label>
        <length>10000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Weaknesses__c</fullName>
        <description>Please specify any weaknesses this competitor has relative to this Opportunity</description>
        <externalId>false</externalId>
        <inlineHelpText>Please specify any weaknesses this competitor has relative to this Opportunity</inlineHelpText>
        <label>Weaknesses</label>
        <length>10000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <label>Competing Product</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Opportunity__c</columns>
        <columns>Product__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>CP-{000000}</displayFormat>
        <label>CPID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Competing Products</pluralLabel>
    <recordTypes>
        <fullName>Master_Competing_Product_Record_Type</fullName>
        <active>true</active>
        <description>Master Record Type for Competing Product</description>
        <label>Master Competing Product Record Type</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Opportunity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Competitor__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Strengths__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Weaknesses__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Opportunity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Competitor__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Strengths__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Weaknesses__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>Opportunity__c</lookupFilterFields>
        <lookupFilterFields>Competitor__c</lookupFilterFields>
        <lookupFilterFields>Product__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Opportunity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Competitor__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Strengths__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Weaknesses__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Opportunity__c</searchFilterFields>
        <searchFilterFields>Competitor__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
        <searchResultsAdditionalFields>Opportunity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Competitor__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Product__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Strengths__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Weaknesses__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Competitor_Required</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
 ISBLANK( Competitor__c ) ,
 $User.Country == &apos;HK&apos;
)</errorConditionFormula>
        <errorDisplayField>Competitor__c</errorDisplayField>
        <errorMessage>Please select the competitor name</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
