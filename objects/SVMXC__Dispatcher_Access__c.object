<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelp>SVMXC__SVMXC_LaunchHelp</customHelp>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This object is used to determine the scope of territories and service teams a dispatcher sees in dispatch console. This is especially useful for large organizations with multiple dispatchers where each dispatcher operates within a set of territories and service teams</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>SVMXC__Dispatcher__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Name of the dispatcher that can dispatch to this team or territory. Reference to an existing Salesforce user</description>
        <externalId>false</externalId>
        <label>Dispatcher</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Dispatcher_Access</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Select__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Checkbox used in ServiceMax screens to manage record selection</description>
        <externalId>false</externalId>
        <label>Select</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SVMXC__Service_Team__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Name of the Service Team to grant access to. Reference to an existing Service Team record in ServiceMax</description>
        <externalId>false</externalId>
        <label>Service Team</label>
        <referenceTo>SVMXC__Service_Group__c</referenceTo>
        <relationshipLabel>Dispatcher Access</relationshipLabel>
        <relationshipName>Dispatcher_Access</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Territory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Name of the Territory to grant access to. Reference to an existing Territory record in ServiceMax</description>
        <externalId>false</externalId>
        <label>Territory</label>
        <referenceTo>SVMXC__Territory__c</referenceTo>
        <relationshipLabel>Dispatcher Access</relationshipLabel>
        <relationshipName>Dispatcher_Access</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Dispatcher Access</label>
    <nameField>
        <displayFormat>{00000000}</displayFormat>
        <label>Record Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Dispatcher Access</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>SVMXC__Dispatcher__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Service_Team__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Territory__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Dispatcher__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Service_Team__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Territory__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>SVMXC__Dispatcher__c</searchFilterFields>
        <searchFilterFields>SVMXC__Service_Team__c</searchFilterFields>
        <searchFilterFields>SVMXC__Territory__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
