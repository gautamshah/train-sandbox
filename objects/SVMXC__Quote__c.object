<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelp>SVMXC__SVMXC_LaunchHelp</customHelp>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>In ServiceMax, Quote contains the estimated cost of a service event to be presented to a customer to get approval. A quote can be either created manually or generated automatically from the estimate lines of a service order.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>SVMXC__Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Customer account for whom the quote is prepared. Is a lookup to an existing salesforce account record</description>
        <externalId>false</externalId>
        <inlineHelpText>Customer account for whom the quote is prepared. Is a lookup to an existing salesforce account record</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Service Quotes</relationshipLabel>
        <relationshipName>Quote</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Customer contact for whom the quote is prepared. Is a lookup to an existing salesforce contact record</description>
        <externalId>false</externalId>
        <inlineHelpText>Customer contact for whom the quote is prepared. Is a lookup to an existing salesforce contact record</inlineHelpText>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Quote</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Discount_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Indicates if the discount is entered as an amount or percentage of total price</description>
        <externalId>false</externalId>
        <inlineHelpText>Select Discount Type</inlineHelpText>
        <label>Discount Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Percent</fullName>
                    <default>false</default>
                    <label>Percent</label>
                </value>
                <value>
                    <fullName>Amount</fullName>
                    <default>false</default>
                    <label>Amount</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SVMXC__Discount__c</fullName>
        <deprecated>false</deprecated>
        <description>To capture discount percentage or amount based on selected Discount Type</description>
        <externalId>false</externalId>
        <inlineHelpText>To capture discount percentage or amount based on selected Discount Type</inlineHelpText>
        <label>Discount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__EndpointURL__c</fullName>
        <deprecated>false</deprecated>
        <description>Not used</description>
        <externalId>false</externalId>
        <formula>$Api.Enterprise_Server_URL_70</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>EndpointURL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__Quote_Amount2__c</fullName>
        <deprecated>false</deprecated>
        <description>Total amount of the quote after discount</description>
        <externalId>false</externalId>
        <inlineHelpText>Total amount of the quote after discount</inlineHelpText>
        <label>Quote Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>3</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SVMXC__Revised_From__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The original quote number which was revised to create this quote. Is a lookup to an existing quote in ServiceMax</description>
        <externalId>false</externalId>
        <inlineHelpText>The original quote number which was revised to create this quote. Is a lookup to an existing quote in ServiceMax</inlineHelpText>
        <label>Revised From</label>
        <referenceTo>SVMXC__Quote__c</referenceTo>
        <relationshipName>Quote</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__SESSION_ID__c</fullName>
        <deprecated>false</deprecated>
        <description>Not used</description>
        <externalId>false</externalId>
        <formula>$Api.Session_ID</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SESSION_ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__Service_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Service order number for which this quote is created. Is a lookup to an existing service order record in ServiceMax</description>
        <externalId>false</externalId>
        <inlineHelpText>Service order number for which this quote is created. Is a lookup to an existing service order record in ServiceMax</inlineHelpText>
        <label>Work Order</label>
        <referenceTo>SVMXC__Service_Order__c</referenceTo>
        <relationshipLabel>Service Quotes</relationshipLabel>
        <relationshipName>Quote</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Status__c</fullName>
        <deprecated>false</deprecated>
        <description>Current status of the quote</description>
        <externalId>false</externalId>
        <inlineHelpText>Current status of the quote</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draft</fullName>
                    <default>false</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>Presented</fullName>
                    <default>false</default>
                    <label>Presented</label>
                </value>
                <value>
                    <fullName>Accepted</fullName>
                    <default>false</default>
                    <label>Accepted</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Revised</fullName>
                    <default>false</default>
                    <label>Revised</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SVMXC__Total_Line_Price2__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of Line Price for all the lines</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of Line Price for all the lines</inlineHelpText>
        <label>Total Line Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>3</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SVMXC__Valid_Until__c</fullName>
        <deprecated>false</deprecated>
        <description>Date until which the quote is valid</description>
        <externalId>false</externalId>
        <inlineHelpText>Date until which the quote is valid</inlineHelpText>
        <label>Valid Until</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Service Quote</label>
    <listViews>
        <fullName>SVMXC__All_Service_Quotes</fullName>
        <columns>NAME</columns>
        <columns>SVMXC__Company__c</columns>
        <columns>SVMXC__Contact__c</columns>
        <columns>SVMXC__Service_Order__c</columns>
        <columns>SVMXC__Quote_Amount2__c</columns>
        <columns>SVMXC__Status__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All Service Quotes</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>Q-{00000000}</displayFormat>
        <label>Quote Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Service Quotes</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>SVMXC__Company__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SVMXC__Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SVMXC__Service_Order__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SVMXC__Quote_Amount2__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SVMXC__Revised_From__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SVMXC__Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Company__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Service_Order__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Quote_Amount2__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SVMXC__Valid_Until__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Company__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Contact__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Service_Order__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Quote_Amount2__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SVMXC__Valid_Until__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>SVMXC__Company__c</searchFilterFields>
        <searchFilterFields>SVMXC__Contact__c</searchFilterFields>
        <searchFilterFields>SVMXC__Quote_Amount2__c</searchFilterFields>
        <searchFilterFields>SVMXC__Revised_From__c</searchFilterFields>
        <searchFilterFields>SVMXC__Service_Order__c</searchFilterFields>
        <searchFilterFields>SVMXC__Status__c</searchFilterFields>
        <searchFilterFields>SVMXC__Valid_Until__c</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
        <searchResultsAdditionalFields>SVMXC__Company__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SVMXC__Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SVMXC__Service_Order__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SVMXC__Quote_Amount2__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SVMXC__Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SVMXC__Valid_Until__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>SVMXC__Create_Edit_Quote_Lines</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Create/Edit Quote Lines</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>{!URLFOR($Site.Prefix+&apos;/apex/SVMXC__ServiceMaxConsole?SVMX_recordId=&apos;+CASESAFEID(SVMXC__Quote__c.Id)+&apos;&amp;SVMX_processId=TDM019&amp;SVMX_retURL=&apos;+URLFOR($Site.Prefix+&apos;/&apos;+CASESAFEID(SVMXC__Quote__c.Id)))}</url>
    </webLinks>
</CustomObject>
