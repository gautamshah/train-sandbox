<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object showed the upload summary of each file uploads in Sales out and inventory submission.
This object was created to support the Asia Distributor Community project. Please contact the Asia Distributor Community project group asia.partnerconnect@covidien.com before making changes to this object</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Distributor ID</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>SalesOut uploads</relationshipLabel>
        <relationshipName>SalesOut_uploads</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Cycle_Period_MMYYYY__c</fullName>
        <externalId>false</externalId>
        <formula>Cycle_Period__r.Cycle_Period_Reference__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cycle_Period_MMYYYY</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cycle_Period__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Cycle Period</label>
        <referenceTo>Cycle_Period__c</referenceTo>
        <relationshipName>SalesOut_uploads</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Date_Uploaded__c</fullName>
        <externalId>false</externalId>
        <label>Date Uploaded</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Total_Number_of_Records__c</fullName>
        <externalId>false</externalId>
        <label>Total Number of Records</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Upload_Object__c</fullName>
        <externalId>false</externalId>
        <label>Upload Object</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Sales Out</fullName>
                    <default>false</default>
                    <label>Sales Out</label>
                </value>
                <value>
                    <fullName>Channel Inventory</fullName>
                    <default>false</default>
                    <label>Channel Inventory</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Upload_Status__c</fullName>
        <externalId>false</externalId>
        <label>Upload Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Uploaded</fullName>
                    <default>false</default>
                    <label>Uploaded</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Upload_file_name__c</fullName>
        <externalId>false</externalId>
        <label>Upload file name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Upload Statistics</label>
    <nameField>
        <displayFormat>SOU-{0000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Upload Statistics</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>New</excludedStandardButtons>
        <searchResultsAdditionalFields>Cycle_Period_MMYYYY__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Upload_Object__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Upload_file_name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Upload_Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Upload</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Upload</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>https://c.cs9.visual.force.com/apex/uploadsalesout</url>
    </webLinks>
</CustomObject>
