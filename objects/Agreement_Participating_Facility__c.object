<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is for CPQ Project. The Participating Facilities that are related to an Apttus Agreement.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Participated Agreements</relationshipLabel>
        <relationshipName>Agreement_Participating_Facilities</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Agreement__c</fullName>
        <externalId>false</externalId>
        <label>Agreement</label>
        <referenceTo>Apttus__APTS_Agreement__c</referenceTo>
        <relationshipLabel>Agreement Participating Facilities</relationshipLabel>
        <relationshipName>Agreement_Participating_Facilities</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Billing_Street__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.BillingStreet</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Billing Street</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <description>Account__r.ShippingCity</description>
        <externalId>false</externalId>
        <formula>Account__r.ShippingCity</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Shipping City</inlineHelpText>
        <label>City</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Implementation_Location__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Used to indicate whether participating facility is the implementation location for a Vital Sync product. Only displays in the &quot;Configure Participating Facilities&quot; UI (CPQ_ProposalCustomer_Config) when Vital Sync products are present (T_HLVL_VA__c = TRUE)</description>
        <externalId>false</externalId>
        <label>Implementation Location</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Licensed_Location__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Used to indicate whether participating facility is a licensed location for the Vital Sync product. Only displays in the &quot;Configure Participating Facilities&quot; UI (CPQ_ProposalCustomer_Config) when Vital Sync products are present (T_HLVL_VA__c = TRUE)</description>
        <externalId>false</externalId>
        <label>Licensed Location</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <description>Shipping Name</description>
        <externalId>false</externalId>
        <formula>Account__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Shipping Name</inlineHelpText>
        <label>Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PostalCode__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.ShippingPostalCode</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Postal Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SSF_Template_Account_External_Id__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Account_External_ID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Template Account External ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT( Account__r.State_Province__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>State</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.ShippingStreet</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Street</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Agreement Participating Facility</label>
    <nameField>
        <displayFormat>FACILITY{0}</displayFormat>
        <label>Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Agreement Participating Facilities</pluralLabel>
    <recordTypes>
        <fullName>OrderFormManualEntry</fullName>
        <active>true</active>
        <label>Order Form (vs) Manual Entry</label>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
