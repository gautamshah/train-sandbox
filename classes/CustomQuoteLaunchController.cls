/**
* @Name: CustomQuoteLaunchController
* @Description: Controller for launching Apttus pages via mobile actions
* @UsedBy: All visualforce pages named CustomQuoteLaunch<ACTION_NAME>
*
* CHANGE HISTORY
* ===============================================================================
* DATE            NAME                DESC
* 2016-03-09      Isaac Lewis         Created.
* ===============================================================================
*/

public with sharing class CustomQuoteLaunchController {

	private final Apttus_Proposal__Proposal__c proposal;

	public CustomQuoteLaunchController(ApexPages.StandardController stdController) {
		this.proposal = (Apttus_Proposal__Proposal__c)stdController.getRecord();
	}

	// public PageReference doLaunchConfigureProducts(){
	// 	PageReference pageRef = Page.Apttus_QPConfig__ProposalConfiguration;
	// 	pageRef.getParameters().put('id', this.proposal.Id);
	// 	pageRef.getParameters().put('flow', 'SSG Cart');
	// 	pageRef.getParameters().put('useAdvancedApproval','true');
	// 	pageRef.getParameters().put('useDealOptimizer','false');
	// 	// pageRef.getParameters().put('','');
	// 	pageRef.setRedirect(true);
	// 	return pageRef;
	// }

	public PageReference doLaunchPreview() {
		PageReference pageRef = Page.Apttus_Proposal__ProposalGenerate;
		pageRef.getParameters().put('id', this.proposal.Id);
		pageRef.getParameters().put('action', 'Preview');
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchGenerate() {
		PageReference pageRef = Page.Apttus_Proposal__ProposalGenerate;
		pageRef.getParameters().put('id', this.proposal.Id);
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchPresent(){
		PageReference pageRef = Page.Apttus_Proposal__ProposalPresent;
		pageRef.getParameters().put('id', this.proposal.Id);
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchAccept(){
		PageReference pageRef = Page.Apttus_QPConfig__ProposalAccept;
		pageRef.getParameters().put('id', this.proposal.Id);
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchDeny(){
		PageReference pageRef = Page.CPQ_ProposalFormulaButtons;
		pageRef.getParameters().put('id', this.proposal.Id);
		pageRef.getParameters().put('purpose','Deny');
		pageRef.setRedirect(true);
		return pageRef;
	}

}