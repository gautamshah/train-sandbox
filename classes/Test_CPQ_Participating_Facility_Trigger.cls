/**
Test class

CPQ_Participating_Facility_After
CPQ_ProposalProcesses.LoadERPAddresses

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_Participating_Facility_Trigger {
	static List<Participating_Facility__c> testFacilities;
	static List<Account> testAccounts;
	static Apttus_Proposal__Proposal__c testProposal;
	
	static void createTestData() {
		testAccounts = new List<Account>{
			new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111'),
			new Account(Status__c = 'Active', Name = 'Test Facilitiy', Account_External_ID__c = 'US-222222'),
			new Account(Status__c = 'Active', Name = 'Test Facilitiy 1', Account_External_ID__c = 'US-444444'),
			new Account(Status__c = 'Active', Name = 'Test Account 2', Account_External_ID__c = 'US-333333')
		};
		insert testAccounts;
		
		List<ERP_Account__c> testERPs = new List<ERP_Account__c> {
			new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[0].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
			new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[1].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
			new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[2].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
			new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[3].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active')
		};
		insert testERPs;
		
		testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', Apttus_Proposal__Account__c = testAccounts[0].ID);
		insert testProposal;
		
		testFacilities = new List<Participating_Facility__c> {
			new Participating_Facility__c(Proposal__c = testProposal.ID, Account__c = testAccounts[1].ID)
		};
	}
	
    static testMethod void myUnitTest() {
    	createTestData();
    	
        Test.startTest();
        	insert testFacilities;
        	
        	Participating_Facility__c newFacility = new Participating_Facility__c(Proposal__c = testProposal.ID, Account__c = testAccounts[2].ID);
        	insert newFacility;
        	
        	delete testFacilities[0];
        	
        	testProposal.Apttus_Proposal__Account__c = testAccounts[3].ID;
        	update testProposal;
        Test.stopTest();
    }
	
    static testMethod void TestDisabledTrigger() {
    	try {
		CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Participating_Facility_After__c = true, CPQ_Product2_After__c = true, CPQ_Product2_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_ProductConfiguration_Before__c = true, CPQ_Proposal_After__c = true, CPQ_Proposal_Before__c = true, CPQ_Proposal_Distributor_After__c = true, CPQ_Rebate_After__c = true);
 		insert testTrigger;
		} catch (Exception e) {}
 		
    	createTestData();
    	
        Test.startTest();
        	insert testFacilities;
        Test.stopTest();
    }
}