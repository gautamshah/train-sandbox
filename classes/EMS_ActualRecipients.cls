public with sharing class EMS_ActualRecipients{

    private Id EventId;
    public String firstname{get;set;}
    public String Organization{get;set;} 
    public String lastname{get;set;}
    public integer selectedsearchcriteria{get;set;}
    public EMS_Event__c CurrentEMSRecord{get;set;}
    public List<EmpConWrapper> ContactResults{get;set;}
    public List<Event_Beneficiaries__c> lstBudgetReciepients;
    public List<Event_Beneficiaries__c> LstActualRecipients;
    public boolean section1{get;set;}
    public boolean section2{get;set;}
    public boolean section3{get;set;}
    public boolean IsOthersExpandable{get;set;}
    public boolean IsATExpandable{get;set;}
    public integer gifts{get;set;}
    public integer Sponsor{get;set;}
    public integer InternalOrderFee{get;set;}
    public integer MeetingPackage{get;set;}
    public integer MeetingRoomRental{get;set;}
    public integer BoothSpace{get;set;}
    public integer Advertisement{get;set;}
    public String sourceflowUrl{get;set;}
    public integer Miscellaneous{get;set;}
    public boolean displaypopupgifts{get;set;}
    public boolean displayOthersPanel{get;set;}
    public String ReciepientId{
        get;
        set {
            ReciepientId= value;
        }
    }

    public List<Event_Beneficiaries__c> getLstActualRecipients(){
    LstActualRecipients=null;
    Set<String> type = new Set<String>();
    type.add('Individual Beneficiary');
    type.add('Organization');
    LstActualRecipients=new List<Event_Beneficiaries__c>();
    LstActualRecipients=[Select Id,Type__c,Employee__r.First_Name__c,Employee__r.Last_Name__c,Contact__r.Name,Speaker__c,Invitee_Type__c,Meal__c,Total_Ground_Transportation__c,Taxi__c,
                                  Train__c,Coach__c,Pickup__c,Others__c,Sponsor__c,Internal_Order_Fee__c,Meeting_Package__c,Meeting_Room_Rental__c,Booth_Space__c,Beneficiary__c,Lodging_Amount__c,
                                  Consultancy_Fees__c,Gifts__c,Check_In_Date__c,Air_Travel__c,Others_Others__c,Miscellaneous__c,Ticket_Type__c,Advertisement__c,Check_out_Date__c,Rate_per_night__c,Bed_Type__c 
                                  from Event_Beneficiaries__c where EMS_Event__c=:currentEMSRecord.Id and type__c in :type and Invitee_type__c='Actual Beneficiary' order by Type__c];
                                  
    return LstActualRecipients;
    }
    public boolean IsLTExpandable{get;set;}
    public boolean IsLodgingExpandable{get;set;}
    
    public Pagereference expandLTcolumns(){
    
    IsLTExpandable=true;
    return null;
    
    }
    
    public Pagereference collapseLTColumns(){
    
    IsLTExpandable=false;
    return null;
    
    }
    public Pagereference expandLodgingColumns(){
    
    IsLodgingExpandable=true;
    return null;
    
    }
    public Pagereference collapseLodgingColumns(){
    
    IsLodgingExpandable=false;
    return null;
    
    }
    
    public Pagereference expandOthersColumns(){
    
    IsOthersExpandable=true;
    return null;
    
    }
    
    
    public Pagereference collapseOthersColumns(){
    
    IsOthersExpandable=false;
    return null;
    
    
    }
    
    
    public Pagereference expandATcolumns(){
    
    IsATExpandable=true;
    return null;
    
    }
    
    public Pagereference collapseATColumns(){
    
    IsATExpandable=false;
    return null;
    
    }
    
    //Constructor
    public EMS_ActualRecipients(ApexPages.StandardController controller) {
         section1=true;
         selectedContactIds=new Set<Id>();
         selectedsearchcriteria=1;
         isLTExpandable=false;
         IsLodgingExpandable=false;
         IsATExpandable=false;
         EventId= ApexPages.currentPage().getParameters().get('EventId');
         String Chosenstep= ApexPages.currentPage().getParameters().get('step');

         sourceflowUrl='/apex/EMS_VFProcessflow?EventId='+EventId+'&step='+Chosenstep;

         CurrentEMSRecord=[Select Id,Name,Start_Date__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Design_Fee_Remarks__c,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,Event_Production_Fee__c,ISR_CSR__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,Total_Local_GTA__c,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Venue__c,Publication_grant__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,Division_Multiple__c,
                                  Total_No_of_Recipients__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,GBU_Multiple__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Total_Meal_Count__c,Total_LGT_Count__c 
                                  from EMS_Event__c where Id=:EventId];
         lstBudgetReciepients= new List<Event_Beneficiaries__c>();
        
          ContactResults=new List<EmpConWrapper>();
    }
    
    //methods for ecpa preview
    public pagereference setsection1(){
    
    section1=true;
    section2=false;
    section3=false;
    
    return null;   
    }
    
    public pagereference setsection2(){
    
    section1=false;
    section2=true;
    section3=false;
    
    return null;   
    }
    
    public pagereference setsection3(){
    
    section1=false;
    section2=false;
    section3=true;
    
    return null;   
    }
    
    
    //Method to redirect back to Event Detail Page Record
    Public Pagereference Back(){
    
    Pagereference P = new Pagereference('/'+EventId);
    P.setredirect(true);
    return P;    
    }
    
    
    
    
   
    public boolean iscriteriachanged{get;set;}
    public Pagereference rendersearchfields(){
    system.debug('}}}}}}}called');
    iscriteriachanged=true;
    selectedContactIds=new Set<Id>();

    firstname='';
    lastname='';
    getContacts();
    Organization='';
    return null;
    
    }
    //Method to save EMS Transactions 
    
    public PageReference SaveTransactions(){   
    System.debug('---->'+'inside loop'); 
    try{
        IsOthersExpandable=false;
        IsLTExpandable=false;
        IsLodgingExpandable=false;
        database.update(LstActualRecipients,false);
        
        System.debug('------>'+LstActualRecipients);
         List<Attachment> LBenfAttachments= new List<Attachment>([Select Id,Name,ParentId from Attachment where Name='Event_Invitees_Budgeted.xls' and ParentId=:EventId]);
        
        if(!LBenfAttachments.isEmpty()){
            database.delete(LBenfAttachments,false);
        } 
        
        PageReference P= Page.EMS_VFInviteesCsvFile;
        P.getparameters().put('EventId',EventId);
        P.setredirect(true);
        Blob b;
        //Blob b= new PageReference('/apex/EMS_VFInviteesCsvFile?Eventid='+EventId).getcontent();
        if(!test.isRunningTest())
         b=P.getcontent();
        else
        b= Blob.valueOf('12345');    
        Attachment a = new Attachment();
        a.body=b;
        a.name='Event_Invitees_Budgeted.xls';
        a.ParentId=EventId;
        insert a;  
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Expense Data for Beneficiaries inserted successfully');
        ApexPages.addMessage(myMsg);
        return null; 
       }
    catch(Exception E){
                        System.debug('---->An Error occured while inserting Transactions records'+E.getmessage());
                        return null;
                      }   
    
    }
    
   
    //Method to insert Beneficiaries
    public PageReference AddContacts(){  
    
    //System.debug('==============>'+ContactResults.isEmpty()+'>>>>>>>>>>>>'+EmployeeResults.isEmpty());
    
    lstBudgetReciepients.clear();
    for(Id eachConId : selectedContactIds){
        if(selectedsearchcriteria==1){
        Event_Beneficiaries__c Eventbenf = new Event_Beneficiaries__c(Contact__c=eachConId,EMS_Event__c=EventId,Type__c='Individual Beneficiary',Invitee_Type__c='Actual Beneficiary');
        lstBudgetReciepients.add(Eventbenf);
        }
        else if(selectedsearchcriteria==2){
        System.debug('////////inside Org insert');
        Event_Beneficiaries__c Eventbenf = new Event_Beneficiaries__c(Organization__c=eachConId,EMS_Event__c=EventId,Type__c='Organization',Invitee_Type__c='Actual Beneficiary');
        lstBudgetReciepients.add(Eventbenf);
        
        }
    }
    //database.insert(lstBudgetReciepients,false);
    
     try{    
         System.debug('>>>>>'+lstBudgetReciepients);
         Database.saveresult[] sr=database.insert(lstBudgetReciepients,false);
         System.debug('<<<<<<<'+sr);
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Beneficiaries added to Event successfully');
         ApexPages.addMessage(myMsg);
         return null; 
        }
     catch(Exception E){
     
      System.debug('---->An Error occured while inserting Transactions records'+E.getmessage());
      return null;
     }   
        
    
    
    }
    
    //Method to add Covidien Employees
    
    
        public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null && selectedsearchcriteria==1) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Name,FirstName,Lastname FROM Contact WHere Id=null]));
            }
            else if(setCon == null && selectedsearchcriteria==2){
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Name FROM Account WHere Id=null]));
            }
            
            return setCon;
        }
        set;
    }
 
        private Set<Id> selectedContactIds=  new Set<Id>();
      
       //Function to display Contact Search Results
       
       public String query;
       public PageReference SearchContacts(){
       iscriteriachanged=false;
       Contactresults.clear();
       //ContactResults=new List<EmpConWrapper>();
       
       
        this.selectedContactIds= new Set<Id>();
        
        setcon=null;
        
        
        if(selectedsearchCriteria==1 &&( firstname!=''|| lastname!='')){
        query='Select Id,Name,Lastname,FirstName from Contact where Firstname LIKE \'%' + firstname + '%\'' + 'and Lastname LIKE \'%' + lastname+ '%\' ORDER BY Name DESC ';

        this.setCon= new ApexPages.StandardSetController( database.query(query) );
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(5);
       
       if(setcon==null ){
           setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id,Name from Contact where Id=null]));
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search results found for this search criteria');
           ApexPages.addMessage(myMsg);
       }
        }
        else if(selectedsearchCriteria==2 && Organization!=''){
         query='Select Id,Name from Account where Name LIKE \'%' + Organization + '%\'';

        this.setCon= new ApexPages.StandardSetController( database.query(query) );
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(5);
       
       if(setcon==null ){
           setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id,Name from Account where Id=null]));
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search results found for this search criteria');
           ApexPages.addMessage(myMsg);
       }
       
                
        }
 
        //gather data set
        
       return Null;
       }
       
      
       public List<EmpCOnWrapper> getContacts(){
 
        List<EmpCOnWrapper> rows = new List<EmpCOnWrapper>();
        System.debug('>>>>>'+iscriteriachanged);
        
        if(iscriteriachanged==true){
        
        return rows;
        }
        
        if(selectedsearchCriteria==1 &&( firstname!=''|| lastname!='')){
        
        for(sObject r : this.setCon.getRecords()){
            EmpCOnWrapper row;
            Contact c = (Contact)r;
            row = new EmpCOnWrapper(c,null,null,false); 
            
            if(this.selectedContactIds.contains(r.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
        }
        }
        else if(selectedsearchCriteria==2 && Organization!=''){
         
       System.debug('+++++++'+setcon);
        for(sObject r : this.setCon.getRecords()){
            EmpCOnWrapper row;
            Account c = (Account)r;
            row = new EmpCOnWrapper(null,null,c,false); 
            
            if(this.selectedContactIds.contains(r.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
        }
        
        }
      
        return rows;
 
    }
       
       
      
      //Delete Contact
      public Pagereference deleteContact(){
      try{
      database.delete(ReciepientId);
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'HCP Beneficiary successfully deleted');
      ApexPages.addMessage(myMsg);
      return null; 
      }
      catch(Exception e){
      
      System.debug('---->An Error occured while deleting HCP Beneficiary'+E.getmessage());
      return null;
      
      
      
      }
      } 
      
      
     
       //Wrapper Class for Contacts Search Results
       public Class EmpConWrapper{
           public Contact c{get;set;}
           public Employee__c e{get;set;}
           public Account a{get;set;}
           public Boolean isSelected{get;set;} 
           
        public EmpConWrapper(Contact eachContact,Employee__c eachEmployee,Account eachAccount,boolean b){
            c=eachContact;        
            e=eachEmployee;
            a=eachAccount;
            isselected=b;
        }
       
       }
       
       
       
       
   
public Boolean displayPopup {get;set;}
public Boolean displaypopuplodging{get;set;}
public void showPopup()
    {
        
    displayPopup = true;
    displaypopuplodging=false;
    displaygiftspanel=false;
    displayotherspanel=false;
    }
  public void showOthers(){
    displayPopup = false;
    displaypopuplodging=false;
    displaygiftspanel=false;
    displayotherspanel=true;
  
  
  } 
  public void showPopuplodging()
    {
        
    displayPopup = false;
    displaypopuplodging=true;
    displaygiftspanel=false;
    displayotherspanel=false;
    }  
    public boolean displaygiftspanel{get;set;}
    public void closePopup() {
        displayPopup = false;
         displaypopuplodging=false;
         displaygiftspanel=false;
         displayotherspanel=false;
        
    }
    
    public void showGifts(){
    displayPopup = false;
    displaypopuplodging=false;
    displaygiftspanel=true;
    
    }
    public Integer MealtotalLumpsum{get;set;}
    public Integer RateperNight{get;set;}
    public Date CheckinDate{get;set;}
    public Date CheckOutDate{get;set;}
    public Integer Taxi {get;set;}
    public Integer Train {get;set;}
    public Integer Coach {get;set;}
    public Integer Pickup {get;set;}
    public Integer Others {get;set;}
    
    public PageReference redirectPopup()
    {
        displayPopup = false;
        
        for(Event_Beneficiaries__c eachtrans : LstActualRecipients){
        
            eachtrans.Meal__c = MealtotalLumpsum/LstActualRecipients.size();        
        }   
        
        database.update(LstActualRecipients,false); 
        return null;    
    }
    
    
    Public Pagereference redirectpopupOthers(){

    displayotherspanel=false;
    for(Event_Beneficiaries__c eachtrans : LstActualRecipients){
    eachtrans.Sponsor__c=Sponsor/LstActualRecipients.size(); 
    eachtrans.Internal_Order_fee__c=InternalOrderFee/LstActualRecipients.size(); 
    eachtrans.Meeting_Package__c=MeetingPackage/LstActualRecipients.size(); 
    eachtrans.Meeting_Room_rental__c=MeetingRoomRental/LstActualRecipients.size(); 
    eachtrans.Booth_Space__c=BoothSpace/LstActualRecipients.size(); 
    eachtrans.Advertisement__c=Advertisement/LstActualRecipients.size(); 
    eachtrans.Miscellaneous__C=Miscellaneous/LstActualRecipients.size(); 
                 
        }   
        
        database.update(LstActualRecipients,false); 
        return null;
    
    }
    
    
    
    public PageReference redirectpopuptransport(){
    displaypopuplodging=false;
    for(Event_Beneficiaries__c eachtrans : LstActualRecipients){
    
        eachtrans.Taxi__c = Taxi/LstActualRecipients.size();
        eachtrans.Train__c = Train/LstActualRecipients.size();
        eachtrans.Coach__c = Coach/LstActualRecipients.size();
        eachtrans.Pickup__c =  Pickup/LstActualRecipients.size();   
        eachtrans.Others__c=  Others/LstActualRecipients.size(); 
        }
        
    database.update(LstActualRecipients,false); 
    
    return null;  
    }
    
     public PageReference redirectpopupgifts(){
    displaygiftspanel=false;
    for(Event_Beneficiaries__c eachtrans : LstActualRecipients){
    
        eachtrans.Gifts__c = Gifts/LstActualRecipients.size();
         
        }
        
    database.update(LstActualRecipients,false); 
    
    return null;  
    }
    
    public String contextItem{get;set;}
    public void doSelectItem(){
        System.debug('--->'+contextitem);
        selectedContactIds.add(contextItem);
 
    }
 
    /*
    *   handle item deselected
    */
    public void doDeselectItem(){
 
        selectedContactIds.remove(contextItem);
 
    }
 
    /*
    *   return count of selected items
    */
    public Integer getSelectedCount(){
 
        return this.selectedContactIds.size();
 
    }
 
    /*
    *   advance to next page
    */
    public void doNext(){
 
        if(this.setCon.getHasNext())
            this.setCon.next();
 
    }
 
    /*
    *   advance to previous page
    */
    public void doPrevious(){
 
        if(this.setCon.getHasPrevious())
            this.setCon.previous();
 
    }
    
    
    public Boolean getHasPrevious(){
 
        return this.setCon.getHasPrevious();
 
    }
 
    /*
    *   return whether next page exists
    */
    public Boolean getHasNext(){
 
        return this.setCon.getHasNext();
 
    }
 
    /*
    *   return page number
    */
    public Integer getPageNumber(){
 
        return this.setCon.getPageNumber();
 
    }
 
    /*
    *    return total pages
    */
    Public Integer getTotalPages(){
 
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
 
        Decimal pages = totalSize/pageSize;
 
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
}