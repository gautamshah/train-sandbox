/**
Controller for PartnerContract Deail Page

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11/21      Yuli Fintescu       Created
2014-12/15      Tony Do             Created
2015-08/13      Paul Berglund       Changed the customerList to customerNameList
                                    and added call to the GetCustomerEligibilityByRoot
                                    web service
2015-09/24      Paul Berglund       Updated GetCustomerEligibilityByRoot call
===============================================================================
*/
public with sharing class CPQ_Controller_PartnerContract_Deail {

    public cpqPartnerContractInfoClasses.ContractInfo theContract {get; set;}
    public String CommitmentType {get; set;}
    public String RootContractSFID {get; set;}
    
    public Partner_Root_Contract__c theRecord{get;set;}
    public List<string> customerNames {get; set;}
    public List<string> customerNameList {get; set;}
    public Boolean displayAlphaList {get; set;}
    public Integer hiddenTotalCustomerCount {get; set;}
    private Map<String, Commitment_Type__c> commTypeMap;
    public List<string> AlphaList {get; set;}
    public string AlphaFilter {get; set;}
    public string RootContractNumber {get; set;}
    public List<CPQ_ProxyContractType.Contract> contracts {get; set;}
    public List<Account> Customers {get; set;}
    
    public CPQ_Controller_PartnerContract_Deail() {
        AlphaList = new list<String> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        customerNames = new List<string>();
        
        commTypeMap = new Map<String, Commitment_Type__c>();
        for (Commitment_Type__c c : [Select ID, Name, Description__c From Commitment_Type__c]) {
            commTypeMap.put(c.Name, c);
        }
        
        RootContractNumber = ApexPages.currentPage().getParameters().get('rootNo');
        
        List<Partner_Root_Contract__c> rootContract = [ SELECT Id, Name, Annual_Net_Sales__c, 
                                                            Account__c,
                                                            Account__r.Primary_GPO_Buying_Group_Affiliation__c, 
                                                            Account__r.AccountType__c, 
                                                            Account__r.BillingCountry, 
                                                            Account__r.BillingPostalCode, 
                                                            Account__r.BillingState, 
                                                            Account__r.BillingCity, 
                                                            Account__r.BillingStreet, 
                                                            Account__r.RecordTypeId, 
                                                            Account__r.Name, 
                                                            Account__r.Id, 
                                                            Account__r.Account_External_ID__c , 
                                                            Commitment_Type__c, 
                                                            Direction__c, 
                                                            Effective_Date__c, 
                                                            Expiration_Date__c, 
                                                            Root_Contract_Name__c, 
                                                            Root_Contract_Number__c, 
                                                            Sales_Org__c, 
                                                        (SELECT Id, Name FROM Related_Partner_Contracts__r Order by Name desc), 
                                                        (Select Id, Name, Apttus__Account__c, 
                                                            Apttus__Account__r.Primary_GPO_Buying_Group_Affiliation__c, 
                                                            Apttus__Account__r.AccountType__c, 
                                                            Apttus__Account__r.BillingCountry, 
                                                            Apttus__Account__r.BillingPostalCode, 
                                                            Apttus__Account__r.BillingState, 
                                                            Apttus__Account__r.BillingCity, 
                                                            Apttus__Account__r.BillingStreet, 
                                                            Apttus__Account__r.RecordTypeId, 
                                                            Apttus__Account__r.Name, 
                                                            Apttus__Account__r.Id, 
                                                            Apttus__Account__r.Account_External_ID__c , 
                                                            Apttus__Status_Category__c, 
                                                            Apttus__Status__c, 
                                                            AgreementID__c 
                                                        From Agreements__r 
                                                        Where Apttus__Status__c = 'Activated' Order By AgreementID__c desc)
                                                    FROM    Partner_Root_Contract__c
                                                    WHERE   Root_Contract_Number__c =: RootContractNumber ];
        
        if(rootContract.size() > 0) {
            theRecord = rootContract[0];
        }
        
        List<String> ContractNums = new List<String>();
        //we have the Partner_Root_Contract__c record in SF
        if (theRecord != null) {
            RootContractSFID = theRecord.ID;
            
            //if there is no related contract in sf, we will try R and D contract.
            if (theRecord.Related_Partner_Contracts__r.size() == 0) {
                ContractNums.add('R' + RootContractNumber );
                ContractNums.add('D' + RootContractNumber );
            } else {
                for (Related_Partner_Contract__c r : theRecord.Related_Partner_Contracts__r) {
                    ContractNums.add(r.Name);
                }
            }
            
            //we get primary account from related agreement record
            if (theRecord.Agreements__r.size() == 0) {
                if (theRecord.Account__r != null)
                    customerNames.add(theRecord.Account__r.Name);
            } else {
                //customer list comes from associated agreement of the root contract
                Apttus__APTS_Agreement__c theAgreement = theRecord.Agreements__r[0];
                
                if (theAgreement.Apttus__Account__c != null) {
                    customerNames.add(theAgreement.Apttus__Account__r.Name);
                }
            }
        //we don't have Partner_Root_Contract__c record in SF
        } else {
            //we will try R and D contract.
            ContractNums.add('R' + RootContractNumber);
            ContractNums.add('D' + RootContractNumber);
        }
        
        BuildQuery();
		
        cpqPartnerContractInfoService service = new cpqPartnerContractInfoService();
        theContract = service.getTheContract(ContractNums);

        if (theRecord != null) {
            if (String.isEmpty(theContract.ContractName))
                theContract.ContractName = theRecord.Root_Contract_Name__c;
            
            if (String.isEmpty(theContract.RootContractNum)) {
                theContract.RootContractNum = theRecord.Root_Contract_Number__c;
            }
            
            if (String.isEmpty(theContract.effectiveDateField))
            {
                theContract.effectiveDateField = (theRecord.Effective_Date__c == null ? '' : 
                        theRecord.Effective_Date__c.format()) + ' - ' + (theRecord.Expiration_Date__c == null ? '' : 
                                theRecord.Expiration_Date__c.format());
            }
 
        }
        
        Commitment_Type__c c = commTypeMap.get(theContract.CommitmentType);
        if (c != null) {
            CommitmentType = c.Name + ' - ' + c.Description__c;
        } else {
            CommitmentType = theContract.CommitmentType;
        }
    }

    public void BuildQuery() {
        // This first time BuildQuery is called, displayAlphaList will be null
        // For subsequent calls, the value will always set to true or false
        // depending on the logic below, once it is set true it will remain
        // true
        if (displayAlphaList == null) displayAlphaList = false;
        // Get a list of Customer Names that are eligible for this root contract.
        // The ws can return duplicates, so we use a Set to eliminate them
        cpqPartnerContractInfoService service = new cpqPartnerContractInfoService();
        cpqPartnerContractInfoClasses.Customer_Wrapper customerWrapper = service.GetContractEligibilityByRoot(RootContractNumber, AlphaFilter);
        // If the GetCustomerEligibilityByRoot returns null, then that means there we no customers
        // If it returns an empty array, then the count > 100 and the AlphaFilter
        // was null so we set the displayAlphaList = true.
        if (customerWrapper != null) {

            if (customerWrapper.TotalCount > 100) {
                displayAlphaList = true;
            }

            if (customerWrapper.Customers != null && 
                customerWrapper.Customers.Information != null && 
                customerWrapper.Customers.Information.size() > 0)
            {
                Set<string> externalIds = new Set<string>();
                for (cpqPartnerContractInfoClasses.Information customer : customerWrapper.Customers.Information)
                {
                    if (customer.ShipTo <> null && customer.shipTo > 0)
                    {
                        externalIds.add('US-' + customer.ShipTo);
                    }

                }
                
                Customers = [SELECT Account_External_ID__c, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, AccountType__c 
                             FROM Account
                             WHERE Account_External_ID__c IN :externalIds
                             ORDER BY Name];
            }
        }
    }
}

/* TO create G3 Partner Root and Related Partner Contract record:
    //GPO
    List<String> s = new List<String> {
        'D007716-01', 'DR9718-01', 'D007290-01', 'D004762', 'DR9720-01', 
        'D05500-1', 'D007292-01', 'R007817-05', 'D05321-1', 'D05322-1', 
        'D05363-1', 'D007700', 'D004662', 'D05161-1', 'D007809-01', 
        'D9748-01', 'D9742-06', 'D9747-01', 'DR9719-01', 'D05341-1', 
        'D007851-01', 'D05385-1'};
    //Freight Contracts
    List<String> s = new List<String> {
        'D007108', 'D007109', 'D007110', 'D072320', 'D072990', 
        'D007708', 'D007848', 'D9739', 'D05271', 'D9740', 
        'D05272', 'D007855', 'V797P2071D', 'V797P2071R', 'D05150-1', 
        'D05150-2', 'D05150-3', 'D05345', 'D05360-1', 'D05360-2', 
        'D05360-3', 'D05360-4', 'D9742-06', 'R007817-05', 'D9743', 
        'DR9718-01', 'DR9719-01', 'D05161-1', 'D9747-01', 'D9747-03', 
        'D05363-1', 'D05385-1', 'D9748-01', 'D05341-1', 'D9748-02', 
        'D9748-03', 'D9748-04', 'D9748-05', 'D9748-06', 'D05341-1', 
        'DR9720-01', 'D9768-01', 'D9768-02', 'DR9720-04', 'D007716-01', 
        'D05322-1', 'D007289-04', 'D05322-3', 'D007700', 'D05396', 
        
        'D004510-02', 'D004510-06', 'D004510-04', 'D005970', 'D007753', 
        'D007222', 'D007753', 'D007305', 'D007798-04', 'D007396', 
        'D004799-04', 'D005684', 'D007140', 'D004610',
        
    //D9742-06, R007817-05, DR9718-01, DR9719-01, D05161-1
    //D9747-01, D05363-1, D05385-1, D9748-01, D9748-02, 
    //D9748-03, D9748-04, D9748-05, D9748-06, D05341-1
    //DR9720-01, D007716-01, D05322-1, D05322-3, D007700

    //RS GPO Contracts
    List<String> s = new List<String> { 
        '05293-1', 'R007858-01', 'R9750-01', '5262', '05273-1', 
        'R9733-01', 'R007699', 'R007857-01', 'R9751-01', '5262',
        '05274-1', 'R9732-01', 'R9741', 'R007393-01', 'R004661', 
        'R007365-01'};

    String ss = s[63];
    CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
    CPQ_ProxyContractInfoDataType.ContractInfoRequestType request = new CPQ_ProxyContractInfoDataType.ContractInfoRequestType();
        request.contractNumberField = ss;
        request.getContractItemsField = true;
        request.getDealerServiceField = true;
        request.identityTokenField = '1000';
        request.maximumResponseRecordsField = '1000';
    
    CPQ_ProxyContractInfoDataType.ContractInfoResponseType response = service.ContractInfo(request);
    if (response.errorCodeField == '0') {
        Partner_Root_Contract__c root = new Partner_Root_Contract__c();
        root.Name = response.contractInfoField.rootContractField;
        root.Root_Contract_Number__c = response.contractInfoField.rootContractField;
        System.Debug('*** root.Root_Contract_Number__c ' + root.Root_Contract_Number__c);
        
        if (!String.isEmpty(response.contractInfoField.commitmentTypeField)) {
            Commitment_Type__c c = new Commitment_Type__c(Name = response.contractInfoField.commitmentTypeField);
            root.Commitment_Type__r = c;
        }
        
        root.Effective_Date__c = response.contractInfoField.effectiveDateField.Date();
        root.Expiration_Date__c = response.contractInfoField.expirationDateField.Date();
        root.Group_Code__c = response.contractInfoField.groupCodeField;
        root.Root_Contract_Name__c = response.contractInfoField.nameField;
        root.Prior_Contract__c = response.contractInfoField.priorContractField;
        
        upsert root Name;
        
        Related_Partner_Contract__c related = new Related_Partner_Contract__c(Root_Contract__c = root.ID);
        related.Name = response.contractInfoField.pricingIdField;
        System.Debug('*** related.Name ' + related.Name);
        upsert related Name;
    }
*/