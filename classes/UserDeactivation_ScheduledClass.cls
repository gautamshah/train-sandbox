global class UserDeactivation_ScheduledClass implements Schedulable
{

    global void execute(SchedulableContext sc)
    {
    	UserDeactivation Autobot = new UserDeactivation();
    	
    		Autobot.runDeactivation();
    	
    }
}