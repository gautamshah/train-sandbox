@isTest
private class TestBriefUtilizationExtension { 
 
   
   static testMethod void test_BriefUtilizationExtension() 
   {
      testUtility tu = new testUtility();
      Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
      List<Brief_Utilization_Tool__c> lstbut = new List<Brief_Utilization_Tool__c>();
      Brief_Utilization_Tool__c but1 = new Brief_Utilization_Tool__c();
      but1.Account__c = a.id;
      but1.Item__c = '65033 - Simplicity Quilted M';
      but1.Number_of_Residents__c = '1';
      lstbut.add(but1);
      Brief_Utilization_Tool__c but2 = new Brief_Utilization_Tool__c();
      but2.Account__c = a.id;
      but2.Item__c = '63073 - Wings Hook and Loop Ultra M';
      but2.Number_of_Residents__c = '2';      
      lstbut.add(but2);      
      ApexPages.StandardController lController = new ApexPages.StandardController(but2);
      BriefUtilizationExtension Ext = new BriefUtilizationExtension(lController);
      Ext.briefUsageRecs = lstbut;
      Ext.addRow();
      Ext.removeLastEmptyRecord();
      Ext.saveRecords();
      Ext.cancel();
               
      //PageReference pageRef = new PageReference('/apex/Brief_Utilization_Tool');
      //ApexPages.currentPage().getParameters().put('id', l1.id);

      //PageReference resultPage = Ext.convertLead();
   }
}