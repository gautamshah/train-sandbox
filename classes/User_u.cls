public virtual class User_u extends sObject_u
{
	private static User_g cache = new User_g();
	
	public static Map<Id, User> fetch(set<Id> ids)
	{
		Map<Id, User> existing = cache.fetch(ids);
		if (existing.size() < ids.size())
		{
			List<User> fromDB = fetchFromDB(existing.keySet());
			cache.put(fromDB);
		}
		
		return cache.fetch(ids);
	}
	
    ////
    //// fetchFromDb
    ////
	@testVisible
    private static List<User> fetchFromDB(set<id> existingIds)
    {
    	string query =
			SOQL_select.build(
				User.sObjectType,
				null,
				' WHERE isActive = true AND Id NOT IN :existingIds ',
				null,
				null);
    	
		return (List<User>)Database.query(query);
    }
}