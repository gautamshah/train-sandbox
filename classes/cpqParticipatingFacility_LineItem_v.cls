public class cpqParticipatingFacility_LineItem_v {
	public Apttus_Proposal__Proposal__c proposal {get;set;}
	public List<LineItemFacilityWrapper> lineItemFacilityWrappers {get;set;}
	public Boolean hasTradeIns {get;set;}
	public Integer rowClicked {get;set;}

	private Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemsMap;
	private List<Apttus_Proposal__Proposal_Line_Item__c> hardwareLineItems;
	private List<Apttus_Proposal__Proposal_Line_Item__c> tradeInLineItems;
	private List<Participating_Facility_Address__c> facilities;
	private Map<Id,Participating_Facility_Address__c> facilitiesMap;
	private List<cpqParticipatingFacility_Lineitem__c> facilityLineItems;

	public cpqParticipatingFacility_LineItem_v(ApexPages.StandardController stdController) {
        proposal = (Apttus_Proposal__Proposal__c)stdController.getRecord();
        init();
	}

	private void init() {
		// Load option proposal line items and split them into hardware and trade-ins
		proposalLineItemsMap = cpqProposalLineItem_c.fetchOptionsMap(proposal.Id);
		hardwareLineItems = cpqProposalLineitem_u.filterByCategory(proposalLineItemsMap.values(), cpqProposalLineitem_u.HARDWARE);
		tradeInLineItems = cpqProposalLineitem_u.filterByCategory(proposalLineItemsMap.values(), cpqProposalLineitem_u.TRADE_IN);

		// Load Facility Line Items associated with the proposal
		facilityLineItems = cpqParticipatingFacility_LineItem_c.fetch(proposal.Id);

		// Use the proposal line items and facility line items to create wrappers for display editing
		lineItemFacilityWrappers = createWrappers(hardwareLineItems, tradeInLineItems, facilityLineitems);

		// Get participating facilities with names needed for error messaging
		facilitiesMap = cpqParticipatingFacilityAddress_c.fetchMap(proposal.Id);
	}

	private List<LineItemFacilityWrapper> createWrappers(List<Apttus_Proposal__Proposal_Line_Item__c> hardwareItems,
													    List<Apttus_Proposal__Proposal_Line_Item__c> tradeInItems,
													    List<cpqParticipatingFacility_Lineitem__c> facilityLineItems) {
		List<LineItemFacilityWrapper> wrappers = new List<LineItemFacilityWrapper>();

		// Create a Map from Proposal Line Item Id to a List of existing association rows
		Map<Id,List<cpqParticipatingFacility_Lineitem__c>> facilityLineItemMap = new Map<Id,List<cpqParticipatingFacility_Lineitem__c>>();
		for (cpqParticipatingFacility_Lineitem__c facilityLineItem: facilityLineItems) {
			if (!facilityLineItemMap.containsKey(facilityLineItem.ProposalLineItem__c)) {
				facilityLineItemMap.put(facilityLineItem.ProposalLineItem__c, new List<cpqParticipatingFacility_Lineitem__c>());
			}
			List<cpqParticipatingFacility_Lineitem__c> currentList = facilityLineItemMap.get(facilityLineItem.ProposalLineItem__c);
			currentList.add(facilityLineItem);
		}

		// Determine if trade-ins are present and what quantity is present
		hasTradeIns = false;
		Integer tradeInQuantity = 0;
		if (tradeInItems != null && !tradeInItems.isEmpty()) {
			hasTradeIns = true;
			tradeInQuantity = Integer.valueOf(tradeInItems[0].Apttus_QPConfig__Quantity2__c);
		}

		// Loop through the hardware items, link them to existing rows, and create wrappers for display
		for (Apttus_Proposal__Proposal_Line_Item__c pli: hardwareItems) {
			List<cpqParticipatingFacility_Lineitem__c> lineItems = facilityLineItemMap.get(pli.Id);

			if (lineItems == null || lineItems.isEmpty()) {
				LineItemFacilityWrapper wrapper = new LineItemFacilityWrapper();
				wrapper.proposalLineItem = pli;
				wrapper.sObj = new cpqParticipatingFacility_Lineitem__c();
				wrapper.sObj.Proposal__c = proposal.Id;
				wrapper.sobj.ProposalLineItem__c = pli.Id;
				wrapper.sObj.QuantityToBePlacedAtShipTo__c = 0;
				wrapper.isAnchor = true;
				if (hasTradeIns) {
					wrapper.hasTradeIns = cpqProposalLineItem_c.hasTradeIns(pli);
					if (wrapper.hasTradeIns) {
						wrapper.sObj.TradeinSerialNumbersCount__c = tradeInQuantity;
					} else {
						wrapper.sObj.TradeinSerialNumbersCount__c = 0;
					}
				} else {
					wrapper.hasTradeIns = false;
					wrapper.sObj.TradeinSerialNumbersCount__c = 0;
				}
				wrappers.add(wrapper);
			} else {
				Boolean isAnchor = true;
				for (cpqParticipatingFacility_Lineitem__c lineItem: lineItems) {
					LineItemFacilityWrapper wrapper = new LineItemFacilityWrapper();
					wrapper.proposalLineItem = pli;
					wrapper.sObj = lineItem;
					if (isAnchor) {
						wrapper.isAnchor = true;
						isAnchor = false;
					}
					if (hasTradeIns) {
						wrapper.hasTradeIns = cpqProposalLineItem_c.hasTradeIns(pli);
						if (wrapper.hasTradeIns) {
							wrapper.sObj.TradeinSerialNumbersCount__c = tradeInQuantity;
						} else {
							wrapper.sObj.TradeinSerialNumbersCount__c = 0;
						}
					} else {
						wrapper.hasTradeIns = false;
						wrapper.sObj.TradeinSerialNumbersCount__c = 0;
					}
					wrappers.add(wrapper);
				}
			}
		}
		return wrappers;
	}

	// public static boolean locked()
	// public static boolean restricted()
	// public static boolean open()


	// Add row to the screen copying some values from its anchor row (based on Proposal Line Item)
	public void addRow() {
		LineItemFacilityWrapper source = lineItemFacilityWrappers.get(rowClicked);
		LineItemFacilityWrapper destination = new LineItemFacilityWrapper();
		destination.proposalLineItem = source.proposalLineItem;
		destination.sObj = source.sObj.clone();
		destination.isAnchor = false;
		destination.hasTradeIns = source.hasTradeIns;
		if (rowClicked + 1 == lineItemFacilityWrappers.size()) {
			lineItemFacilityWrappers.add(destination);
		} else {
			lineItemFacilityWrappers.add(rowClicked + 1, destination);
		}
	}

	// Remove row from the screen
	public void removeRow() {
		lineItemFacilityWrappers.remove(rowClicked);
	}

	// Validate the user input and save
	public PageReference save() {
		validateFacilitiesAndQuantities();
		if (ApexPages.hasMessages()) {
			return null;
		} else {
			if (facilityLineItems != null && !facilityLineItems.isEmpty()) {
				delete facilityLineItems;
			}

			List<cpqParticipatingFacility_Lineitem__c> newFacilityLineItems = new List<cpqParticipatingFacility_Lineitem__c>();
			for (LineItemFacilityWrapper wrapper: lineItemFacilityWrappers) {
				Participating_Facility_Address__c facility = facilitiesMap.get(wrapper.sObj.ParticipatingFacilityAddress__c);
				wrapper.sObj.ERPRecord__c = facility.ERP_Record__c;
				wrapper.sObj.ERP_Record__c = facility.ERP_Record__c;
				wrapper.sObj.Id = null;
				newFacilityLineItems.add(wrapper.sObj);
			}
			insert newFacilityLineItems;

			return new PageReference('/' + proposal.Id);
		}
	}

	private void validateFacilitiesAndQuantities() {
		List<cpqParticipatingFacility_Lineitem__c> sObjList = getFacilityLineItems(lineItemFacilityWrappers);

		// Validate the participating facilities quantities entered
		Set<String> errorMessages = cpqParticipatingFacility_Lineitem_u.validate(sObjList, proposalLineItemsMap, facilitiesMap);
		for (String message: errorMessages) {
			ApexPages.Message pageMessage = new ApexPages.Message(ApexPages.Severity.ERROR, message);
			ApexPages.addMessage(pageMessage);
		}
	}

	public List<cpqParticipatingFacility_Lineitem__c> getFacilityLineItems(List<LineItemFacilityWrapper> wrappers) {
		List<cpqParticipatingFacility_Lineitem__c> sObjList = new List<cpqParticipatingFacility_Lineitem__c>();
		for (LineItemFacilityWrapper wrapper: wrappers) {
			sObjList.add(wrapper.sObj);
		}

		return sObjList;
	}

	public class LineItemFacilityWrapper {
		public Apttus_Proposal__Proposal_Line_Item__c proposalLineItem {get;set;}
		public cpqParticipatingFacility_Lineitem__c sObj {get;set;}
		public Boolean isAnchor {get;set;}
		public Boolean hasTradeIns {get;set;}
	}
}