public with sharing class RelatedCasesController 
{
	private Id currentCaseId {get;set;}
	private Case currentCase {get;set;}
	public Map<String,Schema.SObjectField> mFields {get;set;}
	public String filterField1 {get;set;}
	public String filterField2 {get;set;}
	public String filterField3 {get;set;}
	public List<SelectOption> fieldOptions {get;set;}
	public List<Case> cases {get;set;}
	public Boolean showCases {get;set;}
	
	public RelatedCasesController(ApexPages.StandardController controller)
	{
		currentCaseId = controller.getId();
		//currentCase = (Case) controller.getRecord();
		mFields = Schema.SobjectType.Case.fields.getMap();
		List<String> lFields = new List<String>(mFields.keySet());
		lFields.sort();
		fieldOptions = new List<SelectOption>();
		fieldOptions.add(new SelectOption('', '-- None --'));
		String query = 'Select ';
		
		//for(Schema.SObjectField sField : mFields.values())
		for(String s : lFields)
		{
			Schema.SObjectField sField = mFields.get(s);
			Schema.DescribeFieldResult dfr = sField.getDescribe();
			if(dfr.isAccessible())
			{
				fieldOptions.add(new SelectOption(dfr.getLocalName(), dfr.getLabel()));
				query += dfr.getLocalName() + ', ';
			}
		}
		query = query.removeEnd(', ');
		query += ' From Case Where Id = \'' + currentCaseId + '\'';
		System.debug('query: ' + query);
		currentCase = Database.query(query);
		cases = new List<Case>();
		showCases = false;
		fetchProfileFilters();
	}
	
	private void fetchProfileFilters()
	{
		//get filter fields from custom setting
		List<Similar_Cases_Filter_Field__c> allFilterFields = new List<Similar_Cases_Filter_Field__c>([Select Name, Field_API_Name__c, User_Profile__c From Similar_Cases_Filter_Field__c]);
		Map<String, List<Similar_Cases_Filter_Field__c>> filtersByProfile = new Map<String, List<Similar_Cases_Filter_Field__c>>();
		for(Similar_Cases_Filter_Field__c ff : allFilterFields)
		{
			List<Similar_Cases_Filter_Field__c> profileFilters;
			if(!filtersByProfile.containsKey(ff.User_Profile__c))
			{
				profileFilters = new List<Similar_Cases_Filter_Field__c>();
			}
			else
			{
				profileFilters = filtersByProfile.get(ff.User_Profile__c);
			}
			profileFilters.add(ff);
			filtersByProfile.put(ff.User_Profile__c, profileFilters);
		}
		List<String> orderedFields = new List<String>();
		Map<String, Similar_Cases_Filter_Field__c> filterFieldMap = new Map<String, Similar_Cases_Filter_Field__c>();
		if(filtersByProfile.containsKey(Utilities.CurrentUserProfileName))
		{
			List<Similar_Cases_Filter_Field__c> currentProfileFilters = filtersByProfile.get(Utilities.CurrentUserProfileName);
			for(Similar_Cases_Filter_Field__c ff : currentProfileFilters)
			{
				orderedFields.add(ff.Name);
				filterFieldMap.put(ff.Name, ff);
			}
			orderedFields.sort();
			fieldOptions.clear();
			fieldOptions.add(new SelectOption('', '-- None --'));
			for(String s : orderedFields)
			{
				fieldOptions.add(new SelectOption(filterFieldMap.get(s).Field_API_Name__c, filterFieldMap.get(s).Name));
			}
		}
	}
	
	public void fetchCases()
	{
		cases.clear();
		List<String> lFilterFields = new List<String>();
		
		if(!String.isBlank(filterField1))
		{
			lFilterFields.add(filterField1);
		}
		if(!String.isBlank(filterField2))
		{
			lFilterFields.add(filterField2);
		}
		if(!String.isBlank(filterField3))
		{
			lFilterFields.add(filterField3);
		}
		System.debug('lFilterFields.size(): ' + lFilterFields.size());
		if(!lFilterFields.isEmpty())
		{
			String query = 'Select CaseNumber, Subject, Status, CreatedDate From Case Where ';
			for(Integer i = 0; i < lFilterFields.size(); i++)
			{
				Schema.DisplayType dataType = mFields.get(lFilterFields[i]).getDescribe().getType();
				System.debug('dataType: ' + dataType.name());
				String quoteContainer = '';
				if(dataType != Schema.DisplayType.DOUBLE && dataType != Schema.DisplayType.INTEGER && dataType != Schema.DisplayType.BOOLEAN)
				{
					quoteContainer = '\'';
				}
				System.debug('quoteContainer: ' + quoteContainer);
				query += lFilterFields[i] + ' = ' + quoteContainer + currentCase.get(lFilterFields[i]) + quoteContainer;
				if(lFilterFields.size() > (i+1))
				{
					query += ' And ';
				}
			}
			query += ' And Id != \'' + currentCaseId + '\' Order By CreatedDate DESC';
			System.debug('query: ' + query);
			cases = Database.query(query);
			if(!cases.isEmpty())
			{
				showCases = true;
			}
			else
			{
				showCases = false;
			}
		}
	}
	
	@isTest(SeeAllData=true)
	static void test()
	{
		Case c = [Select Id From Case Limit 1];
		Test.startTest();
		PageReference pageRef = Page.RelatedCases;
		Test.setCurrentPage(pageRef);
		ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(c);
		RelatedCasesController rcc = new RelatedCasesController(controller);
		rcc.filterField1 = 'RecordTypeId';
		rcc.filterField2 = 'Requested_By__c';
		rcc.filterField3 = 'Status';
		rcc.fetchCases();
		Test.stopTest();
	}
}