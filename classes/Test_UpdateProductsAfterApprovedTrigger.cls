/****************************************************************************************
 * Name    : Test_UpdateProductsAfterApprovedTrigger 
 * Author  : Fenny Saputra
 * Date    : 31/05/2016
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers. 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/

@isTest
private class Test_UpdateProductsAfterApprovedTrigger{

    static testMethod void myUnitTest() {
    List<User> u = [select Id from User where Country = 'MY'];
        
    Account a = new Account(
        Name = 'MY test',
        BillingCountry = 'MY'); 
    insert a;
    
    Contact c = new Contact(
        LastName = 'test con',
        AccountId = a.Id,
        Country__c = 'MY');
    insert c;
    
    Opportunity o = new Opportunity(
        Name = 'test opp',
        Capital_Disposable__c = 'Capital',
        StageName = 'Develop',
        CloseDate = system.today(),
        AccountId = a.Id);
    insert o;
        
    Samples__c sam = new Samples__c(
        Name = 'MY Sample',
        Approval_Status__c = 'Draft',
        Send_MY_GWF_Form__c = false,
        GBU__c = 'EBD',
        Sample_Date__c = system.today(),
        Sample_Type__c = 'SA - Normal Sample',
        Purpose_of_Request__c = 'Demo',
        Procedure__c = 'Cardiovascular Condition',
        Ship_To__c = 'Hospital',
        Deliver_To_Contact__c = 'John',
        Contact_Department__c = 'Surgery',
        Contact_Phone_No__c = '98777',
        Surgeon__c = c.Id,
        Opportunity__c = o.Id);
    insert sam;
    
    Product_SKU__c sku = new Product_SKU__c(
        Name = 'SKU',
        Country__c = 'MY',
        Current_FY_Standard_Cost__c = 500,
        Product_SKU_Status__c = 'Active');
    insert sku;
    
    Sample_Product__c sp1 = new Sample_Product__c(
        Sample_Request__c = sam.Id,
        Product_SKU__c = sku.id,
        Qty_eaches__c = 1);
    insert sp1;
    
    Sample_Product__c sp2 = new Sample_Product__c(
        Sample_Request__c = sam.Id,
        Product_SKU__c = sku.id,
        Qty_eaches__c = 3);
    insert sp2;
    
//    sam.Approval_Status__c = 'Approved';
//    update sam;
    }
}