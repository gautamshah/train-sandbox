global with sharing class NewOppMobileFlow2 {
 
 /****************************************************************************************
   * Name    : NewOppMobileFlow 
   * Author  : Shawn Clark
   * Date    : 04/29/2015 
   * Purpose : Extends the Save feature on the Opportunity to redirect to Add Products 
 *****************************************************************************************/
    Public String Opportunity_Name {get;set;}
    Public String Account_GUID {get;set;}
    Public String Account_Name {get;set;}
    Public String Type {get;set;}
    Public String Stage {get;set;}
    Public Decimal Amount {get;set;}
    Public Date Close_Date {get;set;}
    Public String Dealers {get;set;}
    Public String ReccType {get;set;}
    Public String NewOpp {get;set;}

 

    // SOQL query loads the case, with related Sample & Sample Product Data
    public NewOppMobileFlow2 (ApexPages.StandardController controller) {
         Account_GUID = ApexPages.currentPage().getParameters().get('id');
         Account_Name = [SELECT Name From Account Where Id = :Account_GUID LIMIT 1].Name;
     }
      

public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Opportunity' ORDER BY name]) {
            for (RecordType rt : rts) {
                options.add(new SelectOption(rt.ID, rt.Name));
            } 
        }
        return options;
    }
      
  
 //Get list of available Case Record Types to be used in the CaseType list

    public List<SelectOption> getOpportunityRecTypes() {
        List <RecordType> rl;
        List<SelectOption> sol = new List<SelectOption>();
       
        rl = [select Id, Name from RecordType where sObjectType='Opportunity' and isActive=true];
       
        Schema.DescribeSObjectResult d = Schema.SObjectType.Opportunity;
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
       
        sol.add(new SelectOption('','-- Select a Record Type --'));
       
        for (RecordType r : rl) {
                if(rtMapById.get(r.id).isAvailable()){
                    sol.add(new SelectOption(r.id,r.Name));
                }
         }

        return sol;
    }  
  
  
  
      
   //Retrieve Picklist values for StageName on Opportunity Object   

    public List<SelectOption> getTypeList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.Type.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }     
    
    //Retrieve Picklist values for StageName on Opportunity Object     
      public List<SelectOption> getStageList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }
    
     //Retrieve Picklist values for Dealers on Opportunity Object     
      public List<SelectOption> getDealersList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.Dealers__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }             
  
  
 public PageReference Save() {
       Opportunity opp = new Opportunity();
       opp.RecordTypeId = Recctype;                 
       opp.Name = Opportunity_Name;
       opp.AccountId = Account_GUID;
       opp.Type = Type;
       opp.Amount = Amount;
       opp.StageName = Stage;
       opp.CloseDate = Close_Date ;
       opp.Dealers__c = Dealers;

            try{ 
            insert opp; 
            NewOpp = opp.Id;
            } 
            catch (Exception e) 
            {
            ApexPages.addMessages(e);
            }
            return null;
      }       
  
 public PageReference Redir() {

       return null;  

   }
  }