public with sharing abstract class dm
{
	public class applicationException extends Exception {}

    public static void onInsert(List<sObject> newlist)
    {
        for (sObject o : newList)
            if (o.get('GUID__c') == null)
                o.put('GUID__c', dm.GUID);
    }

    //
    // CONSTRUCTORS
    //
    
    public dm() { }
    
    protected dm(ApexPages.StandardController controller)
    {
        this.controller = controller;
        this.base = controller.getRecord();
    }
    
    protected dm(ApexPages.StandardSetController setcontroller)
    {
        this.setcontroller = setcontroller;
    }

	//
	// PROPERTIES
	//
    protected sObject base;
    protected ApexPages.StandardController controller;
    protected ApexPages.StandardSetController setcontroller;

	public List<Id> Ids
	{
		get
		{
			if (Ids == null) return new List<Id>();
			return Ids;
		}
		
		set;
	}
	
	//
	// UTILITIES
	//
    public static string GUID
    {
        get
        {
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = 'guid' + h.SubString(0,8) + h.SubString(8,12) + h.SubString(12,16) + h.SubString(16,20) + h.substring(20);
            return guid;
        }
    }
    
    private static sObjectType getSObjectType(Type objType)
    {
    	return ((sObject)objType.newInstance()).getSObjectType();
    }
    
    public static string keyPrefix(Type objType)
    {
        Schema.DescribeSObjectResult describe = getSObjectType(objType).getDescribe();
        String keyPrefix = describe.getKeyPrefix();
        return keyPrefix;
    }    
    
	public static Set<string> extractGUIDs(List<sObject> objs)
	{
		system.debug('objs: ' + objs);
		
		if (objs == null) return null;
		
		Set<string> guids = new Set<string>();
		for (sObject o : objs)
		{
			if (o.get('GUID__c') != null)
				guids.add((string)o.get('GUID__c'));
		}

		return guids;
	}
	
	public static string buildSOQL(sObjectType objType, Set<string> fieldsToInclude, string whereClause)
	{
        Schema.DescribeSObjectResult describe = objType.getDescribe();

		Set<string> ignore = new Set<string>
		{
			'createdbyid',
			'createddate',
			'currencyisocode',
			'isdeleted',
			'lastmodifiedbyid',
			'lastmodifieddate',
			'lastreferenceddate',
			'lastvieweddate',
			'ownerid',
			'systemmodstamp',
			'body'
		};
		
		if (fieldsToInclude == null) fieldsToInclude = new Set<string>();
		for(string name : describe.fields.getMap().keySet())
			if (!ignore.contains(name.toLowercase()))
				fieldsToInclude.add(name);

		String query =
			'SELECT ' +
    		String.join( new List<string>(fieldsToInclude), ',' ) +
  			' FROM ' +
    		describe.getName();
    		
    	query += (whereClause == null) ? '' : whereClause;
    	
    	return query;
	}

    public static sObject fetchRecord(Id id)
    {
	   	if (id == null) return null;
    	
  		string whereClause = ' WHERE Id = \'' + id + '\' LIMIT 1 ';
  		string query = buildSOQL(id.getSObjectType(), null, whereClause);
  						
  		List<SObject> records = Database.query(query);
  		if (records.size() == 0)
  			return null;
  		else		
  			return records[0];
    }

	//
	// VISUAL FORCE PROPERTIES & METHODS
	//
    // Properties & Methods that support the VF component or page
    //
    public class Parameters
    {
		public Id objectid { get; set; }
		public Id applicationid { get; set; }
    	
    }
    public class Selected
    {
		public Id objectid { get; set; }
		public Id sobjectid { get; set; }
		public boolean assigned { get; set; }
    }

	public Selected selected { get { if (selected == null) selected = new Selected(); return selected; } private set; }
	public Parameters parameters { get { if (parameters == null) parameters = new Parameters(); return parameters; } private set; }
	
	public static StaticResource sr
	{
		get
		{
			if (sr == null)
				sr = [SELECT Id,
							 Name,
							 SystemModStamp
					  FROM StaticResource 
                      WHERE Name = 'dm'
                      LIMIT 1];
                      
				return sr;
		}
		
		private set;
	}
	
	public Id applicationId { get; set; }

    public static Map<string, string> querystring { get { return ApexPages.currentPage().getParameters(); } }
    
    public static Map<string, string> headers { get { return ApexPages.currentPage().getHeaders(); } }

    public string mode
    {
        get
        {
            string id = querystring.get('id');
            string retUrl = querystring.get('retUrl');
            if (id == null)
                return 'List';
            else if (retUrl != null)
                return 'Edit';
            else
                return 'View';              
        }
    }

    public static string baseURL
    {
        get
        {
        	URL current = URL.getCurrentRequestUrl();
            return current.getProtocol() + '://' + current.getAuthority();
        }
    }
}