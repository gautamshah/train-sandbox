@isTest
public class SETCase_Test {

    static testMethod void testrun() {
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        
        Opportunity o = new Opportunity(CurrencyIsoCode = 'USD', AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won',WonLossReasons__c='Other');        
        insert o; 
        
        Cost_Analysis_Case__c c = new Cost_Analysis_Case__c(Start_Date__c=System.Today());
        insert c;
        
        c.Opportunity__c = o.Id;
        update c;
    }
}