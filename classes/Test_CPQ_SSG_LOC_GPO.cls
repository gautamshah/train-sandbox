/*

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-08-07      Bryan Fry           Created
2016-11-02      Isaac Lewis         Added cpq_test.buildScenario_CPQ_ConfigSettings to createTestData
===============================================================================
*/
@isTest
private class Test_CPQ_SSG_LOC_GPO
{
    @isTest static void Test_CPQ_SSG_LOC_GPO()
    {
        PageReference pageRef = Page.CPQ_SSG_GPO_LOC_Custom_Button_Page;
        Test.setCurrentPage(pageRef);

        Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
        insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
        insert opp;

        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.SSG;

        Apttus_Approval__ApprovalsCustomConfig__c approvalsConfig = new Apttus_Approval__ApprovalsCustomConfig__c(Name = 'Apttus__APTS_Agreement__c', Apttus_Approval__ApprovalStatusField__c = 'Apttus_Approval__Approval_Status__c', Apttus_Approval__ApprovalContextType__c = 'Single');
        insert approvalsConfig;

        RecordType gpoLOC = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'GPO_LOC');
        
        Test.startTest();
            CPQ_SSG_LOC_GPO_CustomButton p = new CPQ_SSG_LOC_GPO_CustomButton(new ApexPages.StandardController(opp));
            System.currentPageReference().getParameters().put('Id', acct.Id);
            p.createAgreement();
            p = new CPQ_SSG_LOC_GPO_CustomButton(new ApexPages.StandardController(opp));
            p.createAgreement();
            p.save();
            Apttus__APTS_Agreement__c agmt = p.AptsAgreement;
        Test.stopTest();
    }
}