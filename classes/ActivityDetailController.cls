/****************************************************************************************
* Name    : ActivityDetailController
* Author  : Gautam Shah
* Date    : 4/20/2016
* Purpose : Extension for the VF page that is embedded into Event/Task page layouts.
*           Ultimately this was to migrate custom Activity fields into another object to free up those fields.
* 
* Dependancies: 
* Referenced By: ActivityDetail.vfp
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
*   DATE            AUTHOR                  CHANGE
*   ----            ------                  ------
*   
*****************************************************************************************/ 
public class ActivityDetailController 
{
    public Event currentEvent {get;set;}
    public Activity_Detail__c objDetail {get;set;}
    public List<Schema.FieldSetMember> eventFieldSet {get;private set;} //= new List<Schema.FieldSetMember>(); 

    public ActivityDetailController(ApexPages.StandardController controller){
        this.eventFieldSet = new List<Schema.FieldSetMember>();
        
        List<String> addlFields = new List<String>{'Activity_Detail__c'};
        if(!Test.isRunningTest())
        {
        	controller.addFields(addlFields);
        }
        this.currentEvent = (Event) controller.getRecord();
        system.debug('currentEvent' + currentEvent);
        
        Id eventRtId = Schema.SObjectType.Event.getRecordTypeInfosByName().get(Label.OEM_Event_RecordType).getRecordTypeId();
        
        system.debug('eventRtId' + eventRtId);
        
        if(eventRtId == this.currentEvent.RecordTypeId){
        	eventFieldSet = SObjectType.Activity_Detail__c.FieldSets.getMap().get(Label.OEM_Event_Field).getFields();
        	system.debug('eventFieldSet' + eventFieldSet);
        }	
        else{
        	eventFieldSet = SObjectType.Activity_Detail__c.FieldSets.getMap().get('EU_ANZ_Event_Master').getFields();
        	system.debug('eventFieldSet' + eventFieldSet);
        }
        
        if(String.isNotBlank(currentEvent.Activity_Detail__c))
        {
            List<Activity_Detail__c> objDetailList = new List<Activity_Detail__c>();
            String activityQuery = 'Select Id';
            for(Schema.FieldSetMember fsm : eventFieldSet)
            {
                System.debug('FieldPath: ' + fsm.FieldPath);
                activityQuery += ',' + fsm.FieldPath;
            }
            activityQuery += ' From Activity_Detail__c Where Id = \'' + currentEvent.Activity_Detail__c + '\' Limit 1';
            System.debug('activityQuery: ' + activityQuery);
            objDetailList = Database.query(activityQuery);
            if(!objDetailList.isEmpty())
            {
                this.objDetail = objDetailList[0];
            }
        }
        else
        {
            this.objDetail = new Activity_Detail__c();
        }
    }
    
    public void doSave()
    {
        objDetail.ActivityId__c = currentEvent.Id;
        System.debug('objDetail.ActivityId__c: ' + objDetail.ActivityId__c);
        upsert objDetail;
        if(String.isBlank(currentEvent.Activity_Detail__c))
        {
            currentEvent.Activity_Detail__c = objDetail.Id;
            System.debug('currentEvent: ' + currentEvent);
            update currentEvent;
        }
    }
}