global class EMS_AddPdfonApproval{
    private static boolean run = true;
   
    webservice static void addPDF(list<id> EMSEventIds,String Filetype){
        //Instantiate a list of attachment object
        
        System.debug('---->'+filetype);
        Map<Id,EMS_Event__c> MEmsEvents = new Map<Id,EMS_Event__c>([Select Id,Financial_Year__c,Auto_Number__c,Owner_Country__c,Event_Id__c,CPA_Ref__c,No_of_Approved_eCPAs__c from EMS_Event__c where Id in :(EMSEventIds)]);
        System.debug('---->'+MEmsEvents );
        
        list<attachment> insertAttachment = new list<attachment>();
        list<attachment> insertpdfattachments = new list<attachment>();
        if(Filetype=='excel'){
        for(Id eacheventId : EMSEventIds){
           
            PageReference P1= Page.EMS_VFInviteesCsvFile;
            P1.getparameters().put('EventId',eacheventId);
            P1.getparameters().put('filetype','External');
            //P.setredirect(true);
            Blob b1;
            
            //Blob b= new PageReference('/apex/EMS_VFInviteesCsvFile?Eventid='+EventId).getcontent();
            //getContent is not supported in Test Class. Added by Vinoth Kumar
            if(!Test.isRunningTest())
            b1=P1.getcontent();
            else
            b1 = blob.valueOf('123');
            
            Attachment a1 = new Attachment();
            a1.body=b1;
            a1.name='Event_Invitees_Budgeted'+'-'+'eCPA'+'-'+MEmsEvents.get(eachEventId).Owner_Country__c+'-'+MEmsEvents.get(eachEventId).Financial_Year__c+'-'+MEmsEvents.get(eachEventId).Auto_Number__c+'-'+'R'+(String.valueOf(MEmsEvents.get(eachEventId).No_of_Approved_eCPAs__c+1))+'.xls';
            a1.ParentId=eachEventId;
            insertAttachment.add(a1);            
            //MEmsEvents.get(eachEventId).Budget_External_Recipient_URL__c='/'+a.Id;
            
            
            PageReference P2= Page.EMS_VFInviteesCsvFile;
            P2.getparameters().put('EventId',eacheventId);
            P2.getparameters().put('filetype','Internal');
            //P.setredirect(true);
        
            //Blob b= new PageReference('/apex/EMS_VFInviteesCsvFile?Eventid='+EventId).getcontent();
            
            Blob b2;
            //getContent is not supported in Test Class. Added by Vinoth Kumar
            if(!test.isRunningTest())
            b2=P2.getcontent();
            else
            b2 = blob.valueOf('123');
            Attachment a2 = new Attachment();
            a2.body=b2;
            a2.name='Employees_Budgeted'+'-'+'eCPA'+'-'+MEmsEvents.get(eachEventId).Owner_Country__c+'-'+MEmsEvents.get(eachEventId).Financial_Year__c+'-'+MEmsEvents.get(eachEventId).Auto_Number__c+'-'+'R'+(String.valueOf(MEmsEvents.get(eachEventId).No_of_Approved_eCPAs__c+1))+'.xls';
            a2.ParentId=eacheventId;
            insertAttachment.add(a2);            
            MEmsEvents.get(eachEventId).CPA_ref__c='eCPA'+'-'+MEmsEvents.get(eachEventId).Owner_Country__c+'-'+MEmsEvents.get(eachEventId).Financial_Year__c+'-'+MEmsEvents.get(eachEventId).Auto_Number__c+'-'+'R'+(String.valueOf(MEmsEvents.get(eachEventId).No_of_Approved_eCPAs__c+1));
         }
         //insert the list
         Database.saveresult[] srlist = database.insert(insertAttachment);
         //System.debug('========***'+srlist);
         update MEmsEvents.values();
         }
         else if(filetype=='pdf'){
         
                 for(Id eacheventId : EMSEventIds){

         //create a pageReference instance of the VF page.
            pageReference pdf = Page.EMS_CPAFormpdf;
            //pass the Event Id parameter to the class.
            pdf.getParameters().put('EventId',eachEventId);
            Attachment attach = new Attachment();
            Blob body;
            //getContent is not supported in Test Class. Added by Vinoth Kumar
            if(!Test.isRunningTest())
                body = pdf.getContent();
            else
                body = blob.valueof('123');
                
            attach.Body = body;
            attach.Name = 'eCPA'+'-'+MEmsEvents.get(eachEventId).Owner_Country__c+'-'+MEmsEvents.get(eachEventId).Financial_Year__c+'-'+MEmsEvents.get(eachEventId).Auto_Number__c+'-'+'R'+String.valueOf(MEmsEvents.get(eachEventId).No_of_Approved_eCPAs__c)+'.pdf';
            
            attach.ParentId = eachEventId;//This is the record to which the pdf will be attached
            insertpdfAttachments.add(attach);            
            MEmsEvents.get(eachEventId).CPA_Ref__c= 'eCPA'+'-'+MEmsEvents.get(eachEventId).Owner_Country__c+'-'+MEmsEvents.get(eachEventId).Financial_Year__c+'-'+MEmsEvents.get(eachEventId).Auto_Number__c+'-'+'R'+String.valueOf(MEmsEvents.get(eachEventId).No_of_Approved_eCPAs__c);
       }
         
        Database.saveresult[] srlist1 = database.insert(insertpdfAttachments);
        System.debug('========***'+srlist1);
        update MEmsEvents.values();
         
         }
         }
          
    }