public class SOQL_Result implements SOQL_Result_i
{
    private object result;
    
    public SOQL_Result(Database.DeleteResult result)
    {
        this.result = result;
    }
    
    public SOQL_Result(Database.SaveResult result)
    {
        this.result = result;
    }
    
    public SOQL_Result(Database.UpsertResult result)
    {
        this.result = result;
    }
    
    public SOQL_Result(Database.UndeleteResult result)
    {
        this.result = result;
    }
    
    public SOQL_Result(Database.MergeResult result)
    {
        this.result = result;
    }
    
    public Database.Error[] getErrors()
    {
        Database.Error[] errors;
        
        if (this.result instanceof Database.DeleteResult)
            errors = ((Database.DeleteResult)this.result).getErrors();
        else if (this.result instanceof Database.SaveResult)
            errors = ((Database.SaveResult)this.result).getErrors();
        else if (this.result instanceof Database.UpsertResult)
            errors = ((Database.UpsertResult)this.result).getErrors();
        else if (this.result instanceof Database.UndeleteResult)
            errors = ((Database.UndeleteResult)this.result).getErrors();
        else if (this.result instanceof Database.MergeResult)
            errors = ((Database.MergeResult)this.result).getErrors();
        
        return errors;
    }
    
    public Id getId()
    {
        Id id;
        
        if (this.result instanceof Database.DeleteResult)
            id = ((Database.DeleteResult)this.result).getId();
        else if (this.result instanceof Database.SaveResult)
            id = ((Database.SaveResult)this.result).getId();
        else if (this.result instanceof Database.UpsertResult)
            id = ((Database.UpsertResult)this.result).getId();
        else if (this.result instanceof Database.UndeleteResult)
            id = ((Database.UndeleteResult)this.result).getId();
        else if (this.result instanceof Database.MergeResult)
            id = ((Database.MergeResult)this.result).getId();
        
        return id;
    }
    
    public boolean isSuccess()
    {
        boolean isSuccess;
        
        if (this.result instanceof Database.DeleteResult)
            isSuccess = ((Database.DeleteResult)this.result).isSuccess();
        else if (this.result instanceof Database.SaveResult)
            isSuccess = ((Database.SaveResult)this.result).isSuccess();
        else if (this.result instanceof Database.UpsertResult)
            isSuccess = ((Database.UpsertResult)this.result).isSuccess();
        else if (this.result instanceof Database.UndeleteResult)
            isSuccess = ((Database.UndeleteResult)this.result).isSuccess();
        else if (this.result instanceof Database.MergeResult)
            isSuccess = ((Database.MergeResult)this.result).isSuccess();
        
        return isSuccess;
    }
    
    public static SOQL_Result[] convert(Database.DeleteResult[] results)
    {
        List<SOQL_Result> targets = new List<SOQL_Result>();
        for(Database.DeleteResult result : results)
            targets.add(new SOQL_Result(result));
            
        return targets;
    }

    public static SOQL_Result[] convert(Database.SaveResult[] results)
    {
        List<SOQL_Result> targets = new List<SOQL_Result>();
        for(Database.SaveResult result : results)
            targets.add(new SOQL_Result(result));
            
        return targets;
    }

    public static SOQL_Result[] convert(Database.UpsertResult[] results)
    {
        List<SOQL_Result> targets = new List<SOQL_Result>();
        for(Database.UpsertResult result : results)
            targets.add(new SOQL_Result(result));
            
        return targets;
    }

    public static SOQL_Result[] convert(Database.UndeleteResult[] results)
    {
        List<SOQL_Result> targets = new List<SOQL_Result>();
        for(Database.UndeleteResult result : results)
            targets.add(new SOQL_Result(result));
            
        return targets;
    }

    public static SOQL_Result[] convert(Database.MergeResult[] results)
    {
        List<SOQL_Result> targets = new List<SOQL_Result>();
        for(Database.MergeResult result : results)
            targets.add(new SOQL_Result(result));
            
        return targets;
    }
}