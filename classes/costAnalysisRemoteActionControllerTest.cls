@isTest
private class costAnalysisRemoteActionControllerTest{

    static testmethod void testCovProductsEmpty(){
        Test.startTest();
        String country = 'GB';
        String productId = '';
        String searchText = '';
        List<SObject> products = costAnalysisRemoteActionController.covProducts(country, productId, searchText);
        Test.stopTest();
        System.assertEquals(0, products.size());
    }    
    
    static testmethod void testCovProductsFilled(){
        Test.startTest();
        Product_SKU__c product = new Product_SKU__c(Country__c='GB') ;
        insert product;
        String country = 'GB';
        String productId = '';
        String searchText = '';
        List<SObject> products = costAnalysisRemoteActionController.covProducts(country, productId, searchText);
        Test.stopTest();
        System.assertNotEquals(1, products.size());
    }    
    
    static testmethod void gettersTests(){
        Test.startTest();
        costAnalysisRemoteActionController instance = new costAnalysisRemoteActionController(new ngForceController()); 
        System.assertEquals(instance.userLang, UserInfo.getLanguage());
        System.assertEquals(instance.userLocale , UserInfo.getLocale());
        String enDict = instance.enDict;
        String frDict = instance.frDict;
        String deDict = instance.deDict;
        String itDict = instance.itDict;
        String esDict = instance.esDict;

    }  
}