public class TerritoryHelper {

    private List<Territory> territories = null;
    private List<UserTerritory> userTerritories = null;
    private List<User> users = null;

    private Map<Id, User> userMap = new Map<Id, User>();
    private Map<Id, Territory> territoryMap = new Map<Id, Territory>();
    private Map<Id, UserTerritory> userTerritoryMap = new Map<Id, UserTerritory>();
    private Map<Id, UserTerritory> territoryUserMap = new Map<Id, UserTerritory>();
    private Map<Id, UserTerritory> PMUserMap = new Map<Id, UserTerritory>();
    private Map<Id, UserTerritory> RSUserMap = new Map<Id, UserTerritory>();
    private Map<Id, List<Territory>> parentChildMap = new Map<Id, List<Territory>>();
    private Map<string, Territory> territoryNameMap = new Map<string, Territory>();

    public TerritoryHelper() {
        system.debug('TerritoryHelper constructor');
        territories = [SELECT Id, ParentTerritoryId, Custom_External_TerritoryID__c FROM Territory WHERE Source__c = 'US_MANS' ORDER BY ParentTerritoryId];
        userTerritories = [SELECT UserId, TerritoryId FROM UserTerritory WHERE IsActive = True and TerritoryId in: territories];
        users = [SELECT Id, Title, Franchise__c FROM User];
    }
    
    private void Initialize() {
    
    
        if (territoryMap.size() == 0) {
            for(Territory territory : territories) {
                territoryMap.put(territory.Id, territory);
                territoryNameMap.put(territory.Custom_External_TerritoryID__c, territory);
            }
        }
        
        if (userMap.size() == 0) {
            for(User user : users) {
                userMap.put(user.Id, user);
            }
        }

        if (userTerritoryMap.size() == 0) {
            for(UserTerritory userTerritory : userTerritories) {
                territoryUserMap.put(userTerritory.TerritoryId, userTerritory);
                userTerritoryMap.put(userTerritory.UserId, userTerritory);
            }
        }
        
        for(User u : users) {
            if (u.Title == 'Vice President') {
                UserTerritory userTerritory = userTerritoryMap.get(u.Id);
                if (userTerritory != null) {
                    system.debug('USER: ' + u);
                    if(u.Franchise__c == 'Patient Monitoring')
                        PMUserMap.put(userTerritory.TerritoryId, userTerritory);
                    else
                        RSUserMap.put(userTerritory.TerritoryId, userTerritory);
                }
            }
        }
        
        system.debug('PMUserMap: ' + PMUserMap);
        system.debug('RSUserMap: ' + RSUserMap);
        
        if (territoryMap.size() == 0) {
            List<Territory> childTerritories = null;
            Id parentTerritoryId = null;

            for(Territory territory : territories) {
                territoryMap.put(territory.Id, territory);
            
                if (territory.ParentTerritoryId != parentTerritoryId) {
                    if (parentTerritoryId != null) {
                        parentChildMap.put(parentTerritoryId, childTerritories);
                        childTerritories = new List<Territory>();
                    }
                    parentTerritoryId = territory.ParentTerritoryId;
                }
                childTerritories.add(territory);
            }
        }
    }

    public User getParentUser(Id userId) {
    
        Initialize();
        
        UserTerritory a = null;
        Territory b = null;
        UserTerritory c = null;
        
        a = userTerritoryMap.get(userId);
        if (a != null) {
            b = territoryMap.get(a.TerritoryId);
            if (b != null) {
                UserTerritory ut = territoryUserMap.get(b.ParentTerritoryId);
                if (ut != null) {
                    return userMap.get(ut.UserId);
                }
            }
        }
        return null;
    }
    
    public User getParentUser(Id userId, boolean isSVP, boolean isPM) {
    
        Initialize();
        
        UserTerritory a = null;
        Territory b = null;
        UserTerritory c = null;
        
        a = userTerritoryMap.get(userId);
        if (a != null) {
            b = territoryMap.get(a.TerritoryId);
            if (b != null) {
                UserTerritory ut = null;
                if (isSVP) {
                    if (isPM)
                        ut = PMUserMap.get(b.ParentTerritoryId);
                    else
                        ut = RSUserMap.get(b.ParentTerritoryId);
                        
                    if (ut != null) {
                        system.debug('User Territory: ' + ut);
                        return userMap.get(ut.UserId);
                    }
                else {
                    ut = territoryUserMap.get(b.ParentTerritoryId);
                    if (ut != null) {
                        return userMap.get(ut.UserId);
                    }
                    }
                }
            }
        }
        return null;
    }

}