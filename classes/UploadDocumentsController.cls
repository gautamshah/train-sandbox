public with sharing class UploadDocumentsController {
    public List<DocumentWrap> lstWrap{get; set;}
    public Id RecordId{get; set;}
    public integer selIndex{get; set;}
    public Account acc{get; set;}
    public string hiddenCount{get; set;}
    public UploadDocumentsController(ApexPages.StandardController sc){
        
        RecordId = Apexpages.currentPage().getParameters().get('id');
        acc = [Select id, name from Account where id =:recordId];
        lstWrap = new List<DocumentWrap>();
        
        //Adding values count list - you can change this according to your need
        for(Integer i = 1 ; i < 11 ; i++)
            lstWrap.add(new DocumentWrap(lstWrap.size(),new Distributor_Document__c(), new Attachment()));
    
    }
    
    public pagereference clear(){
        pagereference ref = new pagereference('/apex/UploadDocuments?id='+recordId);
        ref.setredirect(true);
        return ref;
    }
    public pagereference saveData(){
        System.debug('>>>>>>>>>>lstwrap>>>>>>>>>>'+lstwrap);
        Map<Integer,Attachment> mpAttachments = new Map<Integer,Attachment>();
        Map<Integer,Distributor_Document__c> mpDocuments = new Map<Integer,Distributor_Document__c>();
        
        for(DocumentWrap dw : lstwrap){
        	if(dw.att.body != null && dw.doc.name != null && dw.doc.Document_Type__c != null){
        		dw.doc.Account_Name__c = recordId;
                mpDocuments.put(dw.index,dw.doc);
                mpAttachments.put(dw.Index,dw.att);
        	
        	}
        }
       
        lstWrap = null; 
        if(mpDocuments != null && mpDocuments.size()>0 && mpAttachments != null && mpAttachments.size()>0){
            insert mpDocuments.values();
            
            for(integer i : mpAttachments.Keyset()){
                Attachment att = mpAttachments.get(i);
                att.parentId = mpDocuments.get(i).Id;
            }
            
            insert mpAttachments.values();
        }
        
        pagereference ref = new pagereference('/'+recordId);
        return ref;
    }
    
    public class DocumentWrap{
        
        public integer index{get; set;}
        public Distributor_Document__c doc{get; set;}
        public Attachment att{get; set;}
        
        public DocumentWrap(integer i, Distributor_Document__c d, Attachment a){
            index = i;
            doc = d;
            att = a;
        }
        
    }
    
    
}