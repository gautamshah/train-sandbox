public class cpqProduct2_c extends Product2_c
{
    private cpqProduct2_c()
    {
    	super();
    }
    
    public enum ConfigurationType
    {
    	Standalone,
    	Bundle,
    	Option
    }

	public cpqProduct2_c(Map<sObjectField, object> fieldValues)
	{
		super(fieldValues);
		
		this.put(Product2.field.Apttus_Product__c, true);
		this.putIfNull(Product2.field.Apttus_Config2__ConfigurationType__c, ConfigurationType.Standalone.name());
	}
	
	public cpqProduct2_c(string name, OrganizationNames_g.Abbreviations abbr)
	{
		this(
			new Map<sObjectField, object>
				{
					Product2.field.OrganizationName__c => abbr.name(),
			 		Product2.field.Name => name
			 	});
	}
	
	public static Product2 create(OrganizationNames_g.Abbreviations abbr)
	{
		return create(abbr, 'TestProduct', 1);
	}

	public static Product2 create(OrganizationNames_g.Abbreviations abbr, string name, integer num)
	{
		Product2 p = new Product2();
		p.ProductCode = 'ITEM' + name.toUpperCase() + num;
		p.Name = 'ITEM ' + name.toUpperCase() + ' ' + num;
		p.Product_External_ID__c = 'US:' +  abbr.name() + ':' + p.ProductCode;
		p.OrganizationName__c = abbr.name();
		p.Apttus_Config2__ConfigurationType__c = ConfigurationType.Standalone.name();
		p.Apttus_Product__c = true;
		p.isActive = true;
		
		return p; 
	}

	public static Product2 create(OrganizationNames_g.Abbreviations abbr, string name, integer num, string config)
	{
		Product2 p = new Product2();
		p.ProductCode = 'ITEM' + name.toUpperCase() + num;
		p.Name = 'ITEM ' + name.toUpperCase() + ' ' + num;
		p.Product_External_ID__c = 'US:' +  abbr.name() + ':' + p.ProductCode;
		p.OrganizationName__c = abbr.name();
		p.Apttus_Config2__ConfigurationType__c = config;
		p.Apttus_Product__c = true;
		p.isActive = true;
		
		return p; 
	}
}