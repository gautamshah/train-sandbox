/****************************************************************************************
 * Name    : Test_AccountTrigger 
 * Author  : Gautam Shah
 * Date    : 07/08/2013 
 * Purpose : test class for AccountTrigger.trigger
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 7/25/13     Bill Shan            Add testing for account update logic.
 *****************************************************************************************/
@isTest (SeeAllData=true)
private class Test_AccountTrigger {

    static testMethod void myUnitTest() {
        
        ID s2ProfileID = [SELECT Id FROM Profile WHERE Name = 'Asia Distributor - CN'].ID;
        
        User u = [Select Name, UserType, Country From User Where profileId = : s2ProfileID and IsActive = true Limit 1];
        
        /*User u = new User(username='20121217@test.com',
                            alias = 'apitest',
                            email='20121217@test.com', 
                            emailencodingkey='UTF-8',
                            lastname='Covidien',
                            languagelocalekey='en_US',
                            localesidkey='en_US',                                             
                            profileid = s2ProfileID,
                            timezonesidkey='Europe/Berlin',
                            Region__c = 'EU',
                            Business_Unit__c = 'All'
                            );
        insert u;*/
        //User u = [SELECT Id, Name FROM User Where IsActive = true and Profile.Name = 'EU - S2' Limit 1];
        System.runAs(u)
        {
            Account a = new Account(Name='TestAcct');
            try
            {
                insert a;
            }
            catch(DMLException e)
            {
                
            }
        }
        
        u = [SELECT Id, Name FROM User Where IsActive = true and Profile.Name = 'System Administrator' Limit 1];
        System.runAs(u)
        {           
            Account b = [select Name, NoOfBeds__c from Account limit 1];
            b.NoOfBeds__c = 43;
            update b;
            
            
            Account c = new Account(Name='TestAcctc');
            insert c;
        }
    }
}