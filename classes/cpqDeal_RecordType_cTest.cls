@isTest
private class cpqDeal_RecordType_cTest
{

    private Static cpq_TestSetup setup = new cpq_TestSetup();
    private Static RecordType aRT;
    private Static RecordType pRT;

    private Static void setup()
    {
        aRT = cpqRecordType_TestSetup.getRecordType(cpq_u.AGREEMENT_DEV_NAME);
        pRT = cpqRecordType_TestSetup.getRecordType(cpq_u.PROPOSAL_DEV_NAME);
    }

    @isTest static void getRecordTypesById()
    {
        Integer queries;

        queries = Limits.getQueries();
        System.assert(cpqDeal_RecordType_c.getRecordTypesById().keySet().size() > 0, 'No record types returned');
        // TODO: Reduce to queries + 1 after fixing cpq_u.SystemProperties reference.
        System.assertEquals(queries + 1, Limits.getQueries(), 'Used more than one query');
        RecordType rt = cpqRecordType_TestSetup.getRecordType(cpq_u.AGREEMENT_DEV_NAME);

        queries = Limits.getQueries();
        System.assert(cpqDeal_RecordType_c.getRecordTypesById().keySet().contains(rt.Id), 'Record type not found');
        System.assertEquals(queries,Limits.getQueries(), 'Used more than one query');

    }

    @isTest static void getRecordTypesByName()
    {
        System.assert(cpqDeal_RecordType_c.getRecordTypesByName().keySet().size() > 0,
            'No record types returned');
        System.assert(cpqDeal_RecordType_c.getRecordTypesByName().keySet()
            .contains(cpqRecordType_TestSetup.getRecordType(cpq_u.AGREEMENT_DEV_NAME).DeveloperName),
            'Named Record Type Not Found');
    }

    @isTest static void getRecordTypesBySObjectAndId()
    {
        setup();

        System.assertEquals(pRT.Name,cpqDeal_RecordType_c.getRecordTypesBySObjectAndId()
        	.get(cpq_u.PROPOSAL_SOBJECT_TYPE).get(pRT.Id).Name,
            'Proposal Record Type Not Found');
        System.assertEquals(aRT.Name,cpqDeal_RecordType_c.getRecordTypesBySObjectAndId()
        	.get(cpq_u.AGREEMENT_SOBJECT_TYPE).get(aRT.Id).Name,
            'Agreement Record Type Not Found');

    }

    @isTest static void getRecordTypesBySObjectAndName ()
    {
        setup();

        System.assertEquals(pRT.Id,cpqDeal_RecordType_c.getRecordTypesBySObjectAndName()
            .get(cpq_u.PROPOSAL_SOBJECT_TYPE).get(pRT.DeveloperName).Id,
            'Proposal Record Type Not Found');

        System.assertEquals(aRT.Id,cpqDeal_RecordType_c.getRecordTypesBySObjectAndName()
            .get(cpq_u.AGREEMENT_SOBJECT_TYPE).get(aRT.DeveloperName).Id,
            'Agreement Record Type Not Found');

    }

    @isTest static void getRecordTypesByOrgAndName()
    {
        for(OrganizationNames_g.Abbreviations abbr : OrganizationNames_g.Abbreviations.values())
        {
            System.assert(cpqDeal_RecordType_c.getRecordTypesByOrgAndName().containsKey(abbr),
                abbr.name() + ' not found in map');
            System.assert(cpqDeal_RecordType_c.getRecordTypesByOrgAndName().get(abbr).size() > 0,
                'No record types returned for ' + cpq_TestSetup.org.name());
        }
    }

    @isTest static void getAgreementRecordTypesById()
    {
        Map<Id,RecordType> result = cpqDeal_RecordType_c.getAgreementRecordTypesById();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.PROPOSAL_DEV_NAME,rt.SObjectType, 'Pulled Proposal Record Type');
        }
    }

    @isTest static void getAgreementRecordTypesByName()
    {
        Map<String,RecordType> result = cpqDeal_RecordType_c.getAgreementRecordTypesByName();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.PROPOSAL_DEV_NAME,rt.SObjectType, 'Pulled Proposal Record Type');
        }
    }

    @isTest static void getProposalRecordTypesById()
    {
        Map<Id,RecordType> result = cpqDeal_RecordType_c.getProposalRecordTypesById();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.AGREEMENT_DEV_NAME,rt.SObjectType, 'Pulled Agreement Record Type');
        }
    }

    @isTest static void getProposalRecordTypesByName()
    {
        Map<String,RecordType> result = cpqDeal_RecordType_c.getProposalRecordTypesByName();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.AGREEMENT_DEV_NAME,rt.SObjectType, 'Pulled Agreement Record Type');
        }
    }

    // TODO: Build scenario for proposals and agreements
    @isTest static void getUnlockedRecordTypeName()
    {
        String name;

        Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c();
        name = cpqDeal_RecordType_c.getUnlockedRecordTypeName(prop);
        System.assert(!name.contains('Locked'));

        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
        name = cpqDeal_RecordType_c.getUnlockedRecordTypeName(agmt);
        System.assert(!name.contains('Locked'));
    }

}