@isTest
private class SalesNavigatorStage_TriggerHandler_Test 
{
    private static List<String> TemplateNames = new List<String>{'Identify', 'Develop', 'Evaluate', 'Propose', 'Negotiate'};
    private static List<Sales_Navigation_Stage_Template__c> GetTemplates()
    {
        List<Sales_Navigation_Stage_Template__c> temps = new List<Sales_Navigation_Stage_Template__c>();
        for(Integer i = 0; i < 5; i++)
        {
            Sales_Navigation_Stage_Template__c s = new Sales_Navigation_Stage_Template__c(
                Name = TemplateNames[i] + ' Stage',
                Record_Type__c = TemplateNames[i],
                Critical_Steps__c = 4,
                Opportunity_Record_Type__c = 'US_AST_Opportunity'
            );
            temps.Add(s);
        }
        insert temps;
        return temps;
    }
    
    private static Opportunity GetOpportunityTestRecord(string recordTypeName)
    {
        Opportunity testOp = new Opportunity(
            Name = 'Test Opp',
            RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Opportunity').Get(recordTypeName).Id,
            Type = 'New Customer - Conversion',
            Capital_Disposable__c = 'Disposable',
            StageName = 'Identify',
            Opportunity_Type__c = 'Multi Physician Sale',
            CloseDate = Date.today()
        );
        insert testOp;
        return testOp;
    }
    static testMethod void TestTrigger() 
    {
        Test.startTest();
        GetTemplates();
        GetOpportunityTestRecord('US_AST_Opportunity');
        Test.stopTest();
        Opportunity o = [SELECT Id, Total_Completed_Steps__c FROM Opportunity LIMIT 1];
        System.assert(o.Total_Completed_Steps__c == 0, 'Total_Completed_Steps__c should be 0: ' + o.Total_Completed_Steps__c);
        Sales_Navigator_Stage__c s = [SELECT Id, Name, VAC_or_equivalent_identified__c FROM Sales_Navigator_Stage__c WHERE Name = 'Identify Stage' LIMIT 1];
        System.assert(s != null, 'Should have found a sales nav stage: ' + s);
        s.VAC_or_equivalent_identified__c = true;
        update s;
        o = [SELECT Id, Total_Completed_Steps__c FROM Opportunity LIMIT 1];
        System.assert(o.Total_Completed_Steps__c == 1, 'Total_Completed_Steps__c should be 1: ' + o.Total_Completed_Steps__c);
        delete s;
        o = [SELECT Id, Total_Completed_Steps__c FROM Opportunity LIMIT 1];
        System.assert(o.Total_Completed_Steps__c == 0, 'Total_Completed_Steps__c should be 0: ' + o.Total_Completed_Steps__c);
    }
}