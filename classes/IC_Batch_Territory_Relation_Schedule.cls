/**
Schedular for the IC_Batch_Territory_Relation class 
Territory Reparenting runs faster if one at time - batch size should set to 1.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-06-09      Yuli Fintescu		Created
2015-08-12		Bill Shan			Line #20 & 23, changed batch size to 200
===============================================================================

To Schedule:

IC_Batch_Territory_Relation_Schedule m = new IC_Batch_Territory_Relation_Schedule();
String schedule = '0 30 * * * ?';
String jobID = system.schedule('Establish Territory Relationship US', schedule, m);
*/
global class IC_Batch_Territory_Relation_Schedule implements Schedulable{
    global void execute(SchedulableContext ctx) {
	    Integer batchSize = 200;
		IC_Config_Setting__c cs = IC_Config_Setting__c.getValues('TerritoryRelation Batch Size');
		if (cs == null && Test.isRunningTest()) {
			cs = new IC_Config_Setting__c(Name = 'TerritoryRelation Batch Size', Value__c = '200');
		}
		
	    if (cs != null && cs.Value__c != null) {
	    	try {
	    		batchSize = Integer.valueOf(cs.Value__c);
	    	} catch (Exception e) {}
	    }
	    System.Debug('*** batchSize: ' + batchSize);
    	
        IC_Batch_Territory_Relation batchApex = new IC_Batch_Territory_Relation();
        List<AsyncApexJob> openJobs = [select Id from AsyncApexJob where Status = 'Processing' OR Status = 'Queued']; 
        
        //if(openJobs.size() < 5)
        if(!Test.isRunningTest() && openJobs.size() < 50) {
            ID batchprocessid = Database.executeBatch(batchApex, batchSize);
        }
    }
}