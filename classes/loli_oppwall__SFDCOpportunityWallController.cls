/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SFDCOpportunityWallController extends loli_oppwall.VirtualWallController {
    global SFDCOpportunityWallController() {

    }
    global override List<String> getFields() {
        return null;
    }
    global override String getObjectName() {
        return null;
    }
    global override loli_oppwall.VirtualCard newCard() {
        return null;
    }
    global override loli_oppwall.VirtualCard newCard(SObject item) {
        return null;
    }
    global override loli_oppwall.VirtualCard newCard(String parentField, String parentId) {
        return null;
    }
}
