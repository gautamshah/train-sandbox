/**
Handler class for CPQ Product-related triggers like CPQ_Product_SKU_Before trigger.
Functions that involve products

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
04/11/2016      Bryan Fry           Created
04/25/2016		Bryan Fry			Added checks to not query or update rows if no SKU is set
===============================================================================
*/
public class CPQ_ProductProcesses {
	/**
	============================================================================================
	Look for surgical products in Product2 that match the US Product SKU rows being updated and
	set a lookup to link them together to improve the speed and reliability of connections
	between the 2 objects.
	============================================================================================
	*/
	public static void linkSurgicalProducts(List<Product_SKU__c> inputProducts) {
		// Create a Map of product codes to Ids from Product_SKU__c
		Map<String,Id> skuToIdMap = new Map<String,Id>();
		for (Product_SKU__c sku: inputProducts) {
			if (sku.Id != null && sku.Country__c == 'US' && sku.SKU__c != null) {
				skuToIdMap.put(sku.SKU__c, sku.Id);
			}
		}

		// Query the surgical product records that are not linked to a Product_SKU__c
		// record for the product codes passed in.
		if (!skuToIdMap.isEmpty()) {
			List<Product2> relatedProducts = [Select Id, ProductCode
			                                  From Product2 
			                                  Where ProductCode in :skuToIdMap.keySet()
			                                  And Apttus_Surgical_Product__c = true
			                                  And Product_SKU__c = null];

			// Set the Product_SKU__c lookup on the products returned
			for (Product2 product: relatedProducts) {
				product.Product_SKU__c = skuToIdMap.get(product.ProductCode);
			}

			// Update the product changes to the database.
			update relatedProducts;
		}
	}
}