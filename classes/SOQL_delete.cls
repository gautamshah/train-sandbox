public class SOQL_delete extends SOQL implements Queueable 
{
    private SOQL_delete()
    {
        
    }
    
    public SOQL_delete(List<sObject> sobjs)
    {
        super(sobjs); 
    }

    protected override List<SOQL_Result> executeDML(List<sObject> sObjects, boolean allOrNone)
    {
    	return SOQL_Result.convert(Database.delete(sObjects, allOrNone));
    }
}