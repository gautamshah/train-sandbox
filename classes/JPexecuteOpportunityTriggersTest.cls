@isTest

private class JPexecuteOpportunityTriggersTest {
  static TestMethod void test1() {
      String x;
      RecordType rt = [SELECT id FROM RecordType WHERE name LIKE 'Japan%' and isactive=true AND sobjecttype='Opportunity' LIMIT 1][0];
      Opportunity opp = new Opportunity (name='test opp', closedate=Date.newInstance(2015,5,1), recordtypeid=rt.id, stagename='B:75%');
      insert opp;
      
      test.startTest();
      
      //PageReference pageRef = Page.JPexecuteOpportunityTriggersVF;
      //Test.setCurrentPage(pageRef);
      
      JPexecuteOpportunityTriggersController jpEOTC = new JPexecuteOpportunityTriggersController();
      
      jpEOTC.skey_CloseDateBeginStr = '2015/12/1';
      jpEOTC.skey_CloseDateEndStr = '2015/12/31';
      jpEOTC.skey_SalesStartDateBeginStr = '2015/1/1';
      jpEOTC.skey_SalesStartDateEndStr = '2015/12/31';
      x = jpEOTC.skey_CloseDateBeginStr;
      x = jpEOTC.skey_CloseDateEndStr;
      x = jpEOTC.skey_SalesStartDateBeginStr;
      x = jpEOTC.skey_SalesStartDateEndStr;
      x = jpEOTC.oppListSizeStr;

	  // Error Condition 1: no search results
      jpEOTC.search();
      
      // Success Condition
      jpEOTC.skey_CloseDateBeginStr = '2015/1/1';
      jpEOTC.search();
      x = jpEOTC.oppListSizeStr;
      jpEOTC.execute();
      
      // Error Condition 2: empty opportunity record type list
      jpEOTC.oppRecordTypeList = NULL;
      jpEOTC.search();
      
      // Error Condition 3: empty oppList
      jpEOTC.oppList = NULL;
      jpEOTC.execute();
      
      // Error Condition 4: empty ID set
      try {
        JPexecuteOpportunityTriggersController.executeFuture(new Set<ID>(), NULL);
      } catch(Exception e) {
      }
      // Error Condition5: Invalid inputs
      jpEOTC.skey_SalesStartDateEndStr = 'yyyy/12/31';
      jpEOTC.search();
      jpEOTC.skey_SalesStartDateBeginStr = 'yyyy/1/1';
      jpEOTC.search();
      jpEOTC.skey_CloseDateEndStr = 'yyyy/12/31';
      jpEOTC.search();
      jpEOTC.skey_CloseDateBeginStr = 'yyyy/12/1';
      jpEOTC.search();
      jpEOTC.skey_CloseDateBeginStr = '2015-12-1';
      
      test.stopTest();
  }
}