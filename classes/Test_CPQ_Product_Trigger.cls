/**
Test class

CPQ_PriceListProcesses.SyncPriceListItem()

CPQ_Product2_Before
CPQ_Product2_After

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
2016-08-31      Isaac Lewis         Added test for surgical products
2017-03-16      Bryan Fry           Fixed testVars behavior in myTestNoVar 
===============================================================================
*/
@isTest
public class Test_CPQ_Product_Trigger
{
    static List<Product2> testProducts;
    static List<CPQ_Variable__c> testVars;
    static List<string> externalIds = new List<string>
    {
        'US:RMS:ITEM1',
        'US:RMS:ITEM2',
        'US:SSG:ITEM3',
        'US:SSG:ITEM4',
        'US:MS:ITEM5',
        'US:MS:ITEM6'
    };

    @testVisible
    static void createTestData()
    {
		testVars = cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);
        insert testVars;

        testProducts = new List<Product2> {
            new Product2(Name = 'ITEM1', ProductCode = 'ITEM1', Product_External_ID__c = externalIds[0], Apttus_Product__c = false, OrganizationName__c = 'RMS', IsActive = true, UOM_Desc__c = 'CT'),
            new Product2(Name = 'ITEM2', ProductCode = 'ITEM2', Product_External_ID__c = externalIds[1], Apttus_Product__c = true, OrganizationName__c = 'RMS', IsActive = true, UOM_Desc__c = 'BG'),
            new Product2(Name = 'ITEM3', ProductCode = 'ITEM3', Product_External_ID__c = externalIds[2], Apttus_Product__c = false, OrganizationName__c = 'SSG', IsActive = true, UOM_Desc__c = 'CT'),
            new Product2(Name = 'ITEM4', ProductCode = 'ITEM4', Product_External_ID__c = externalIds[3], Apttus_Product__c = true, OrganizationName__c = 'SSG', IsActive = true, UOM_Desc__c = 'BG'),
            new Product2(Name = 'ITEM5', ProductCode = 'ITEM5', Product_External_ID__c = externalIds[4], Apttus_Product__c = false, OrganizationName__c = 'MS', IsActive = true, UOM_Desc__c = 'CT'),
            new Product2(Name = 'ITEM6', ProductCode = 'ITEM6', Product_External_ID__c = externalIds[5], Apttus_Product__c = true, OrganizationName__c = 'MS', IsActive = true, UOM_Desc__c = 'BG')
        };
    }

    static testMethod void myUnitTest()
    {
        createTestData();

        Test.startTest();
            insert testProducts;
            
            List<Product2> ps =
                [SELECT Id,
                        Name,
                        ProductCode,
                        Product_External_ID__c,
                        Apttus_Product__c,
                        UOM_Desc__c,
                        OrganizationName__c
                 FROM Product2
                 WHERE Name LIKE 'ITEM%'];
                 
            system.debug('Test_CPQ_Product_Trigger.' + ps);

            testProducts[0].Apttus_Product__c = true;
            testProducts[2].Apttus_Product__c = true;

            testProducts[1].UOM_Desc__c = 'EA';
            testProducts[3].UOM_Desc__c = 'EA';

            for(Product2 p : testProducts)
                system.debug('Test_CPQ_Product_Trigger.' + p);

            update testProducts;

            testProducts[1].ProductCode = 'ITEM7';
            testProducts[3].ProductCode = 'ITEM8';
            update testProducts[1];
            update testProducts[3];

            CPQ_Utilities.translateUOM('CT');
            CPQ_Utilities.translateUOM('EA');
            CPQ_Utilities.translateUOM('BX');
            CPQ_Utilities.translateUOM('CA');
            CPQ_Utilities.translateUOM('PK');
            CPQ_Utilities.translateUOM('GA');
            CPQ_Utilities.translateUOM('BG');
            CPQ_Utilities.translateUOM('');
        Test.stopTest();
    }

    static testMethod void myTestNoVar()
    {
        createTestData();

        Integer index = 0;
        for (CPQ_Variable__c cpqVar: testVars) {
            cpqVar.Name = 'Blah ' + index;
            index++;
        }
        update testVars;
        
        Test.startTest();
            insert testProducts;
        Test.stopTest();
    }

/*
    static testMethod void TestDisabledTrigger()
    {
        try
        {
            CPQ_Trigger_Profile__c testTrigger =
                new CPQ_Trigger_Profile__c(
                    SetupOwnerId = UserInfo.getUserID(),
                    CPQ_Agreement_After__c = true,
                    CPQ_Agreement_Before__c = true,
                    CPQ_Participating_Facility_After__c = true,
                    CPQ_Product2_After__c = true,
                    CPQ_Product2_Before__c = true,
                    CPQ_ProductConfiguration_After__c = true,
                    CPQ_ProductConfiguration_Before__c = true,
                    CPQ_Proposal_After__c = true,
                    CPQ_Proposal_Before__c = true,
                    CPQ_Proposal_Distributor_After__c = true,
                    CPQ_Rebate_After__c = true,
                    CPQ_Rebate_Agreement_After__c = true);
                    
            insert testTrigger;
        }
        catch (Exception e) {}

        testProducts = new List<Product2> {
            new Product2(Name = 'ITEM1', ProductCode = 'ITEM1', Product_External_ID__c = externalIds[0], Apttus_Product__c = false, OrganizationName__c = 'RMS', IsActive = true, UOM_Desc__c = 'CT'),
            new Product2(Name = 'ITEM2', ProductCode = 'ITEM2', Product_External_ID__c = externalIds[1], Apttus_Product__c = true, OrganizationName__c = 'RMS', IsActive = true, UOM_Desc__c = 'BG')
        };

        Test.startTest();
            insert testProducts;

            testProducts[0].Apttus_Product__c = true;
            testProducts[1].UOM_Desc__c = 'EA';
            update testProducts;
        Test.stopTest();
    }
*/
}
/*
        insert testProducts;
            [SELECT Id,
                    Name,
                    ProductCode,
                    Product_External_ID__c,
                    Apttus_Product__c,
                    OrganizationName__c,
                    IsActive,
                    UOM_Desc__c
             FROM Product2
             WHERE Product_External_ID__c IN :externalIds];
             
*/