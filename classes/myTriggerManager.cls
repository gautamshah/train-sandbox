/****************************************************************************************
* Name    : myTriggerManager
* Author  : Paul Berglund
* Date    : 11/12/2015
* Purpose : Classes used to "manage" Triggers and xTriggerDispatcher classes
* 
* Dependancies:
*   Triggers:  Agreement
*              Proposal
*              Task
*   Class: AgreementTriggerDispatcher
*          ProposalTriggerDispatcher
*          TaskTriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 11/12/2015    Paul Berglund           Initial version of the class
* 05/12/2016    Paul Berglund           Bypassing the SOQL query to see if that fixes the Too
                                        many SOQL queries error in TaskConfigSetting test class
* 08/31/2016    Bryan Fry               Adding static map of myTriggerManager__c data to reduce queries.
*****************************************************************************************/
global class myTriggerManager {

    // Declare the instance of myTriggerManager in the xTriggerDispatcher class
    // to be static
    private myTriggerManager__c Instance;

    @TestVisible
    private static Map<String,myTriggerManager__c> myTriggerManagerMap;
    
    public myTriggerManager(string name)
    {
        try
        {
	       	if (myTriggerManagerMap == null) {
                myTriggerManagerMap = new Map<String,myTriggerManager__c>();
                List<myTriggerManager__c> myTriggerManagers = [SELECT Id, Name, isActive__c, Method_Is_Disabled__c, Method_Debug_Is_Disabled__c FROM myTriggerManager__c];
                for (myTriggerManager__c myTriggerManager: myTriggerManagers) {
                    myTriggerManagerMap.put(myTriggerManager.Name, myTriggerManager);
                }
            }

        	Instance = myTriggerManagerMap.get(name);
        }
	    catch (Exception ex)
	    {
    	    system.debug('myTriggerManager error: ' + ex);
            Instance = null;
        }
    }
    
    public boolean isActive {
        get {
            if(Instance == null) return true;
            return Instance.isActive__c;
        }
    }
    
    public boolean MethodIsEnabled(string name) {
        if(name == null) return true;
        if(Instance == null) return true;
        if(Instance.Method_Is_Disabled__c == null) return true;
        return !Instance.Method_Is_Disabled__c.contains(name);
    }

    public boolean MethodDebugIsEnabled(string name) {
        if(name == null) return true;
        if(Instance == null) return true;
        if(Instance.Method_Debug_Is_Disabled__c == null) return true;
        return !Instance.Method_Debug_Is_Disabled__c.contains(name);    
    }
    
    public void Debug(string methodName, string message) {
        if(MethodDebugIsEnabled(methodName)) {
            system.debug(message);
        }
    }
    
    public void Debug(string methodName, system.LoggingLevel level, string message) {
        if(MethodDebugIsEnabled(methodName)) {
            system.debug(level, message);
        }
    }
    
    public static void TriggerDebug(string name, boolean isBefore, boolean isInsert, boolean isDelete, boolean isUpdate, List<sobject> newList, List<sobject> oldList) {
        myTriggerManager tmgr = new myTriggerManager(name);
        
        if(tmgr.MethodDebugIsEnabled('TriggerDebug')) {
            string msg = 'trigger ' + name + ' - ';
            if(isBefore)
                msg += 'Before, ';
            else
                msg += 'After, ';
        
            if(isInsert)
                msg += 'Insert, new.size(): ' + newList.size();
            else if (isDelete)
                msg += 'Delete, old.size(): ' + oldList.size();
            else if (isUpdate)
                msg += 'Update, new.size(): ' + newList.size() + ' old.size(): ' + oldList.size();
        
            system.debug(msg);
        }
    }
}