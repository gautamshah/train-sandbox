/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20170309  A    BF         AV-280     Add test coverage to allow AV-280 deployment

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
@isTest
public class cpqParticipatingFacility_LineItem_vTest
{
	@testSetup
    static void Setup()
    {
		Schema.RecordTypeInfo gpoRT =
			Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
		
		Schema.RecordTypeInfo acctRT =
			Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

		List<Account> testGPOs =
			new List<Account>
			{
				new Account(
					Name = 'Test GPO 1', 
					Status__c = 'Active', 
					Account_External_ID__c = 'USGG-SG0150', 
					RecordTypeID = gpoRT.getRecordTypeId()),
			
				new Account(
					Name = 'Test GPO 2', 
					Status__c = 'Active', 
					Account_External_ID__c = 'USGG-SG0150', 
					RecordTypeID = gpoRT.getRecordTypeId())
			};

		insert testGPOs;

		List<Account> testAccounts =
			new List<Account>
			{
				new Account(
					Status__c = 'Active', 
					Name = 'Test Account w GPO1', 
					Primary_GPO_Buying_Group_Affiliation__c = testGPOs[0].ID,
					RecordTypeID = acctRT.getRecordTypeId(),
					Account_External_ID__c = 'US-111111'),
				
				new Account(
					Status__c = 'Active', 
					Name = 'Test Account w GPO2', 
					Primary_GPO_Buying_Group_Affiliation__c = testGPOs[1].ID,
					RecordTypeID = acctRT.getRecordTypeId(), 
					Account_External_ID__c = 'US-222222')
			};

		insert testAccounts;

		List<ERP_Account__c> testERPs =
			new List<ERP_Account__c>
			{
				new ERP_Account__c(
					Name = 'Test ERP', 
					Parent_Sell_To_Account__c = testAccounts[0].ID, 
					ERP_Account_Type__c = 'S', 
					ERP_Source__c = 'E1', 
					ERP_Account_Status__c = 'Active'),

				new ERP_Account__c(
					Name = 'Test ERP', 
					Parent_Sell_To_Account__c = testAccounts[1].ID, 
					ERP_Account_Type__c = 'S', 
					ERP_Source__c = 'E1', 
					RMS_Total_Prior_12_Mos_Sales1__c = 100, 
					ERP_Account_Status__c = 'Active'),

				new ERP_Account__c(
					Name = 'Test ERP', 
					Parent_Sell_To_Account__c = testAccounts[1].ID, 
					ERP_Account_Type__c = 'X', 
					ERP_Source__c = 'E1', 
					RMS_Total_Prior_12_Mos_Sales1__c = 1000, 
					ERP_Account_Status__c = 'Active')
			};

		insert testERPs;

		List<Product2> products = cpqProduct2_TestSetup.generateProducts(1);
		products[0].Charge_Type__c = cpqProposalLineitem_u.HARDWARE;
		products[0].Name = cpqProposalLineItem_c.FT10;
		insert products;

		Map<String,Id> mapProposalRType = New Map<String,Id>();
		Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class);
		for(RecordType R: rts.values())
			mapProposalRType.put(R.DeveloperName, R.Id);

		List<Apttus_Proposal__Proposal__c> testProposals =
			new List<Apttus_Proposal__Proposal__c>
			{
				new Apttus_Proposal__Proposal__c(
					Apttus_Proposal__Proposal_Name__c = 'Test No Account',
					Apttus_Proposal__Account__c = testAccounts[0].ID, 
					RecordTypeID = mapProposalRType.get('Current_Price_Quote'))
			};

		insert testProposals;

		List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems =
			new List<Apttus_Proposal__Proposal_Line_Item__c>
			{
	        	new Apttus_Proposal__Proposal_Line_Item__c(
	        		Apttus_Proposal__Proposal__c = testProposals[0].Id,
	            	Apttus_QPConfig__ChargeType__c = cpqProposalLineitem_u.HARDWARE,
	                Apttus_QPConfig__LineType__c = 'Option',
	                Apttus_QPConfig__OptionId__c = products[0].Id),
	        	
	        	new Apttus_Proposal__Proposal_Line_Item__c(
	        		Apttus_Proposal__Proposal__c = testProposals[0].Id,
	                Apttus_QPConfig__ChargeType__c = cpqProposalLineitem_u.TRADE_IN,
	                Apttus_QPConfig__LineType__c = 'Option',
	                Apttus_Proposal__Product__c = products[0].Id)
        	};

        insert proposalLineItems;

		Participating_Facility_Address__c mockFacility = 
		    new Participating_Facility_Address__c(
		    	ERP_Record__c = testERPs[0].Id,
		    	Proposal__c = testProposals[0].Id);
		
		insert mockFacility;

		List<cpqParticipatingFacility_Lineitem__c> pflis =
			new List<cpqParticipatingFacility_Lineitem__c>
			{
				new cpqParticipatingFacility_Lineitem__c(
					Proposal__c = testProposals[0].Id,
					ProposalLineItem__c = proposalLineItems[0].Id,
					ERP_Record__c = testERPs[0].Id,
                    ERPRecord__c = testERPs[0].Id,
                    QuantityToBePlacedAtShipTo__c = 1,
                    ParticipatingFacilityAddress__c = mockFacility.Id),
				
				new cpqParticipatingFacility_Lineitem__c(
					Proposal__c = testProposals[0].Id,
					ProposalLineItem__c = proposalLineItems[1].Id,
					ERP_Record__c = testERPs[0].Id,
                    ERPRecord__c = testERPs[0].Id,
                    QuantityToBePlacedAtShipTo__c = 1,
                    ParticipatingFacilityAddress__c = mockFacility.Id)
			};

		insert pflis;

    }

    @isTest
    public static void Test_ParticipatingFacility_LineItem_TradeIn()
    {
    	Apttus_Proposal__Proposal__c proposal = [Select Id From Apttus_Proposal__Proposal__c Limit 1];
    	Test.startTest();
			cpqParticipatingFacility_LineItem_v con = new cpqParticipatingFacility_LineItem_v(new ApexPages.StandardController(proposal));
			con.rowClicked = 0;
			con.addRow();
			con.rowClicked = 1;
			con.removeRow();
			for (cpqParticipatingFacility_LineItem_v.LineItemFacilityWrapper wrapper: con.lineItemFacilityWrappers) {
				wrapper.sobj.TradeinSerialNumbers__c = '123';
			}
			con.save();
		Test.stopTest();
	}

    @isTest
    public static void Test_ParticipatingFacility_LineItem_TradeIn_NoFacilityLineItems()
    {
    	Apttus_Proposal__Proposal__c proposal = [Select Id From Apttus_Proposal__Proposal__c Limit 1];
    	List<cpqParticipatingFacility_Lineitem__c> facilityLineItems = [Select Id From cpqParticipatingFacility_Lineitem__c];
    	delete facilityLineItems;

    	Test.startTest();
			cpqParticipatingFacility_LineItem_v con = new cpqParticipatingFacility_LineItem_v(new ApexPages.StandardController(proposal));
		Test.stopTest();
	}

    @isTest
    public static void Test_ParticipatingFacility_LineItem_NoTradeIn()
    {
    	Apttus_Proposal__Proposal__c proposal = [Select Id From Apttus_Proposal__Proposal__c Limit 1];
    	Apttus_Proposal__Proposal_Line_Item__c pli = [Select Apttus_QPConfig__ChargeType__c 
    												  From Apttus_Proposal__Proposal_Line_Item__c 
    												  Where Apttus_QPConfig__ChargeType__c = :cpqProposalLineitem_u.TRADE_IN];
    	pli.Apttus_QPConfig__ChargeType__c = 'Consumable';
    	update pli;

    	Test.startTest();
			cpqParticipatingFacility_LineItem_v con = new cpqParticipatingFacility_LineItem_v(new ApexPages.StandardController(proposal));
		Test.stopTest();
	}
}