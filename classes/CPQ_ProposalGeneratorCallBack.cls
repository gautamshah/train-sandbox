public interface CPQ_ProposalGeneratorCallBack {
    void BeforeGenerate(Apttus_Proposal__Proposal__c sourceProposal,
		Apttus_Proposal__Proposal__c outputProposal,
        Apttus_Config2__ProductConfiguration__c outputConfig,
        List<Apttus_Config2__LineItem__c> outputLineItems);
    
    void BeforeGenerate(Apttus__APTS_Agreement__c sourceAgreement,
		Apttus_Proposal__Proposal__c outputProposal,
        Apttus_Config2__ProductConfiguration__c outputConfig,
        List<Apttus_Config2__LineItem__c> outputLineItems);
}