@isTest
private class Test_CPQ_SSG_DM_Batch_Price
{
    static testmethod void price_Method1()
    {
/** Commented out because the code cannot complete in production without a properly setup user in an Apttus surgical profile to use
        // Create User information
        Profile p = new Profile();
        p = [SELECT Id FROM Profile WHERE Name='APTTUS - SSG US Account Executive'];
        User u = new User();
        u.Alias = 'standt';
        u.Email = 'standarduser@testorg.com';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'kimberly';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.ProfileId = p.Id;
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.UserName = 'deal.statera@statera.com';
        u.Region__c = 'US';
        u.Sales_Org_PL__c = 'General Surgical Products';
        insert u;

        // Create CPQ DM Variable
        CPQ_DM_Variable__c cpqDMVar = new CPQ_DM_Variable__c();
        cpqDMVar.Value__c = u.Id;
        cpqDMVar.Type__c = 'User';
        cpqDMVar.Name = 'StateraDeal';
        insert cpqDMVar;
*/

        // Create Product2 records
        Product2 prd = new product2();
        prd.Name = 'Product_Price';
        prd.ProductCode = '1234qwer';
        insert prd;

        // Create account
        Account acc = new Account();
        acc.Name = 'Deal_Account';
        acc.Account_External_ID__c = 'US-342881';
        insert acc;

        // Create Opportunity
        Opportunity opp=new Opportunity();
        opp.Name = 'COT Opportunity';
        opp.AccountId = acc.Id;
        opp.Capital_Disposable__c = 'Capital';
        opp.Type = 'New Custome';
        opp.Financial_Program__c = 'Advanced Tech Bridge';
        opp.Promotion_Program__c = 'Capnography Customer Care- PM';
        opp.CloseDate = Date.Today()+50;
        opp.StageName = 'Draft';
        insert opp;

        RecordType rtype = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values().get(0);

        // Create Quote/Proposal record
        Apttus_Proposal__Proposal__c qp = new Apttus_Proposal__Proposal__c();
        qp.Apttus_Proposal__Account__c = acc.Id;
        qp.Committed_Stapling_Vessel_Sealing__c = 'True';
        //qp.OwnerId = u.Id;
        qp.Term_Months__c = '24';
        qp.Legacy_External_Id__c = '1649';
        qp.Apttus_Proposal__ExpectedStartDate__c = Date.Today()+1;
        qp.Apttus_Proposal__Opportunity__c = opp.Id;
        qp.Apttus_Proposal__Approval_Stage__c = 'In Review';
        qp.RecordTypeId = rtype.Id;
        qp.Apttus_QPApprov__Approval_Status__c = 'Approved';
        insert qp;

        // Create CPQ_SSG STG Deal record
        CPQ_SSG_STG_Deal__c cpqStgDeal = new CPQ_SSG_STG_Deal__c();
        cpqStgDeal.STG_DealId__c = '1649';
        cpqStgDeal.STG_Process__c = true;
        insert cpqStgDeal;

        CPQ_SSG_Class_of_Trade__c cot = new CPQ_SSG_Class_of_Trade__c();
        cot.Code__c = 'E02';
        insert cot;

        CPQ_SSG_QP_Class_of_Trade__c qpCot = new CPQ_SSG_QP_Class_of_Trade__c(Quote_Proposal__c = qp.Id, Legacy_External_Id__c = '1649', CPQ_SSG_Class_of_Trade__c = cot.Id);
        insert qpCot;

        //Creating CPQ SSG STG Deal Price record
        CPQ_SSG_STG_Deal_Price__c cpqSTGPrice = new CPQ_SSG_STG_Deal_Price__c();
        cpqSTGPrice.STG_DealId__c = '1649';
        cpqSTGPrice.STG_ClassOfTradeCode__c = 'E02';
        cpqSTGPrice.STG_CreatedByName__c = 'StateraDeal';
        cpqSTGPrice.STG_ProductCode__c = '1234qwer';
        insert cpqSTGPrice;

        Test.startTest();
            CPQ_SSG_DM_Batch_Price cpqPrice = new CPQ_SSG_DM_Batch_Price();
            Database.executebatch(cpqPrice);
        Test.stopTest();
    }
}