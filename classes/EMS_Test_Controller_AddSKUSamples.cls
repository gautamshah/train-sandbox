@isTest
public class EMS_Test_Controller_AddSKUSamples{
     public static testmethod void testMethod1(){
         Product_SKU__c objPSKU = new Product_SKU__c();
         objPSKU.Name = 'Test PSKU';
         insert objPSKU;
         
         Apexpages.CurrentPage().getParameters().put('Id',objPSKU.Id);
         ApexPages.StandardController SC = new ApexPages.standardController(objPSKU);
         EMS_Controller_AddSKUSamples objCls = new EMS_Controller_AddSKUSamples(SC);
         EMS_Controller_AddSKUSamples.CCIExpenseWrapper objWrap = new EMS_Controller_AddSKUSamples.CCIExpenseWrapper(null,null,null,null);
         objCls.getCountries();
         objCls.ReciepientId = '';
         objCls.Back();
         objCls.AddCCISamples();
         objCls.SearchContacts();
         objCls.getContacts();
         objCls.deleteContact();
         objCls.doSelectItem();
         objCls.doDeselectItem();
         objCls.getSelectedCount();
         objCls.doNext();
         objCls.doPrevious();
         objCls.getHasPrevious();
         objCls.getHasNext();
         objCls.getPageNumber();
         objCls.getTotalPages();
         
         
         
     }
}