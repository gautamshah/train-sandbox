/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
private class Test_CPQ_SSG_CreateDeal
{
	static Account acct;
	static Opportunity opp;

	static void createTestData()
	{
		User currentUser = cpqUser_c.CurrentUser;
		currentUser.Sales_Org_PL__c = 'Surgical';
		update currentUser;

		acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
		insert acct;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
		insert c;

		opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
		insert opp;

		Apttus_Approval__ApprovalsCustomConfig__c approvalsConfig = new Apttus_Approval__ApprovalsCustomConfig__c(Name = 'Apttus__APTS_Agreement__c', Apttus_Approval__ApprovalStatusField__c = 'Apttus_Approval__Approval_Status__c', Apttus_Approval__ApprovalContextType__c = 'Single');
		insert approvalsConfig;
	}

	@isTest
	static void itShouldGetRecordTypes()
	{
		List<RecordType> rts = CPQ_SSG_Controller_CreateDeal.getRecordTypes('Account');
		System.assert(rts.size() > 0);
	}

	@isTest
	static void itShouldGetOpportunity()
	{
		createTestData();
		Opportunity o = CPQ_SSG_Controller_CreateDeal.getOpportunity(opp.Id);
		System.assertEquals(opp.Id,o.Id);
	}

	@isTest
	static void itShouldGetAccount()
	{
		createTestData();
		Account a = CPQ_SSG_Controller_CreateDeal.getAccount(acct.Id);
		System.assertEquals(acct.Id,a.Id);
	}

	@isTest
	static void itShouldDeleteRecord()
	{
		createTestData();
		Boolean success = CPQ_SSG_Controller_CreateDeal.deleteRecord(opp.Id);
		System.assert(true,success);
		Boolean isDeleted = false;
		try{
			Opportunity o = CPQ_SSG_Controller_CreateDeal.getOpportunity(opp.Id);
		} catch (Exception e) {
			System.debug('Exception: ' + e);
			isDeleted = true;
		}
		System.assert(isDeleted);
	}

	@isTest
	static void itShouldCreateRecord_Proposal()
	{
		// GIVEN existing parent Account
		createTestData();
		String parentId = acct.Id;
		String sObjectDevName = 'Apttus_Proposal__Proposal__c';
		String recordTypeDevName = CPQ_ProposalProcesses.QUICK_QUOTE_RT;
		RecordType rt = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,recordTypeDevName);

		// WHEN Method is executed
		Test.startTest();
		sObject result = CPQ_SSG_Controller_CreateDeal.createRecord(sObjectDevName, rt.Id, parentId);

		// THEN Proposal is Created
		System.assertEquals(Schema.Apttus_Proposal__Proposal__c.sObjectType,result.getSObjectType());
		Apttus_Proposal__Proposal__c prop = [SELECT Id FROM Apttus_Proposal__Proposal__c WHERE RecordType.DeveloperName = :recordTypeDevName LIMIT 1];
		System.assertNotEquals(NULL,prop);

		Test.stopTest();
	}

	@isTest
	static void itShouldCreateRecord_Agreement()
	{
		// GIVEN existing parent Account
		createTestData();
		String parentId = opp.Id;
		String sObjectDevName = 'Apttus__APTS_Agreement__c';
		String recordTypeDevName = CPQ_AgreementProcesses.SMART_CART_RT;
		RecordType rt = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,recordTypeDevName);

		// WHEN Method is executed
		Test.startTest();
		sObject result = CPQ_SSG_Controller_CreateDeal.createRecord(sObjectDevName, rt.Id, parentId);

		// THEN Proposal is Created
		System.assertEquals(Schema.Apttus__APTS_Agreement__c.sObjectType,result.getSObjectType());
		Apttus__APTS_Agreement__c agmt = [SELECT Id FROM Apttus__APTS_Agreement__c WHERE RecordType.DeveloperName = :recordTypeDevName LIMIT 1];
		System.assertNotEquals(NULL,agmt);

		Test.stopTest();
	}

	@isTest
	static void itShouldCreateDeal_CustomKit_Acc()
	{
		// GIVEN existing parent Account
		createTestData();
		String parentId = acct.Id;
		String sObjectDevName = 'Apttus__APTS_Agreement__c';
		String recordTypeDevName = 'Custom_Kit';

		// WHEN Page is launched from button
		Test.startTest();

		// AND page is initialized
		PageReference pr = Page.CPQ_SSG_CreateDeal;
		Test.setCurrentPage(pr);
		ApexPages.currentPage().getParameters().put('parentId', parentId);
		ApexPages.currentPage().getParameters().put('sObjectDevName',sObjectDevName);
		ApexPages.currentPage().getParameters().put('recordTypeDevName',recordTypeDevName);

		// AND controller action is fired
		CPQ_SSG_Controller_CreateDeal_VF ctrl = new CPQ_SSG_Controller_CreateDeal_VF();
		PageReference pageResult = ctrl.generateDeal();

		// THEN Agreement is Created
		Apttus__APTS_Agreement__c agmt = [SELECT Id FROM Apttus__APTS_Agreement__c WHERE RecordType.DeveloperName = :recordTypeDevName LIMIT 1];
		System.assertNotEquals(NULL,agmt);

		// AND Redirect URL is created
		String pageURL = pageResult.getUrl();
		System.assertNotEquals(null,pageURL,'No redirect');
		System.debug('Redirect URL: ' + pageURL);

		// AND Redirect URL contains Agreement Id
		// {cancelURL=//..., retURL=/a2hK0000001NTvPIAW}
		Map<String,String> params = pageResult.getParameters();
		System.debug('Redirect Params: ' + params);
		System.assert(params.get('retURL').contains(agmt.Id),'No Id for Agreement');

		// AND Cancel URL contains Parent Id
		// {accountId=001K000001J58lUIAR, actionName=create_oppty_proposal, agreementId=a2hK0000001NTvFIAW, objectId=001K000001J58lUIAR, rollbackId=a2hK0000001NTvFIAW}
		PageReference cancelPageRef = new PageReference(params.get('cancelURL'));
		Map<String,String> cancelParams = cancelPageRef.getParameters();
		System.debug('Cancel Params: ' + cancelParams);
		System.assert(cancelParams.get('accountId').contains(acct.Id),'No Parent Id');

		Test.stopTest();
	}

	@isTest
	static void itShouldCreateDeal_ScrubPO_Opp()
	{
		// GIVEN existing parent Opportunity
		createTestData();
		String parentId = opp.Id;
		String sObjectDevName = 'Apttus__APTS_Agreement__c';
		String recordTypeDevName = 'Scrub_PO';

		// WHEN Page is launched from button
		Test.startTest();

		// AND page is initialized
		PageReference pr = Page.CPQ_SSG_CreateDeal;
		Test.setCurrentPage(pr);
		ApexPages.currentPage().getParameters().put('parentId', parentId);
		ApexPages.currentPage().getParameters().put('sObjectDevName',sObjectDevName);
		ApexPages.currentPage().getParameters().put('recordTypeDevName',recordTypeDevName);

		// AND controller action is fired
		CPQ_SSG_Controller_CreateDeal_VF ctrl = new CPQ_SSG_Controller_CreateDeal_VF();
		PageReference pageResult = ctrl.generateDeal();

		// THEN Agreement is Created
		Apttus__APTS_Agreement__c agmt = [SELECT Id FROM Apttus__APTS_Agreement__c WHERE RecordType.DeveloperName = :recordTypeDevName LIMIT 1];
		System.assertNotEquals(NULL,agmt);

		// AND Redirect URL is created
		String pageURL = pageResult.getUrl();
		System.assertNotEquals(null,pageURL,'No redirect');
		System.debug('Redirect URL: ' + pageURL);

		// AND Redirect URL contains Agreement Id
		// {cancelURL=//..., retURL=/a2hK0000001NTvPIAW}
		Map<String,String> params = pageResult.getParameters();
		System.debug('Redirect Params: ' + params);
		System.assert(params.get('retURL').contains(agmt.Id),'No Id for Agreement');

		// AND Cancel URL contains Parent Id
		// {accountId=001K000001J58lUIAR, actionName=create_oppty_proposal, agreementId=a2hK0000001NTvFIAW, objectId=001K000001J58lUIAR, rollbackId=a2hK0000001NTvFIAW}
		PageReference cancelPageRef = new PageReference(params.get('cancelURL'));
		Map<String,String> cancelParams = cancelPageRef.getParameters();
		System.debug('Cancel Params: ' + cancelParams);
		System.assert(cancelParams.get('opportunityId').contains(opp.Id),'No Parent Id');

		Test.stopTest();
	}
}