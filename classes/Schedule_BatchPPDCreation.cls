global class Schedule_BatchPPDCreation implements Schedulable
{
  global void execute(SchedulableContext sc)
  {
    batchPPDCreation batchPPDCreate = new batchPPDCreation();
    Database.executeBatch(batchPPDCreate, 10);
  }
}