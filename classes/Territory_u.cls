public class Territory_u extends sObject_u 
{
	private static Territory_g cache = new Territory_g(); 

	////
	//// fetch(sets)
	////
	public static Map<Id, Territory> fetch(set<Id> ids)
	{
		Map<Id, Territory> existing = cache.fetch(ids);
		if (existing.size() < ids.size())
		{
			set<Id> toFetch = new set<Id>();
			for(Id id : ids)
				if (!existing.containsKey(id))
					toFetch.add(id);

			
			List<Territory> fromDB = fetchFromDB(toFetch);
			cache.put(fromDB);
		}
		
		return cache.fetch(ids);
	}
	
	public static Map<object, Map<Id, Territory>> fetch(sObjectField indexField)
	{
		return cache.fetch(indexField);
	}

	public static Map<object, Map<Id, Territory>> fetch(sObjectField indexField, set<object> keys)
	{
		Map<object, Map<Id, Territory>> existingIndex = cache.fetch(indexField, keys);
		
		if (existingIndex.size() != keys.size())
		{
			set<object> toFetch = new set<object>();
			for(object key : keys)
				if (!existingIndex.containsKey(key))
					toFetch.add(key);

			List<Territory> fromDB = fetchFromDB(indexField, toFetch);
			
			cache.put(fromDB); //// UserId is already included to be indexed					

			return cache.fetch(indexField, keys);
		}
		else
			return existingIndex;
	}

	////
	//// fetchFromDB
	////
	@testVisible
	private static List<Territory> fetchFromDB(sObjectField field, set<object> ovalues)
	{
		// We only support string for the moment
		set<string> values = DATATYPES.castToString(ovalues);
		
		// All the fields are Ids, so convert the values to Ids
		string whereStr = ' WHERE ' + field.getDescribe().getName() + ' IN :values ';

		string query =
			SOQL_select.buildQuery(
				Territory.sObjectType,
				Territory_c.fieldsToInclude,
				whereStr,
				null,
				null);
					
		return (List<Territory>)Database.query(query);
	}

    ////
    //// fetchFromDb
    ////
	@testVisible
    private static List<Territory> fetchFromDB(set<id> ids)
    {
    	string query =
			SOQL_select.buildQuery(
				Territory.sObjectType,
				Territory_c.fieldsToInclude,
				' WHERE Id IN :ids ',
				null,
				null);
    	
		return (List<Territory>)Database.query(query);
    }
}