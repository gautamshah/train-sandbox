/**
Controller for CPQ_ProposalDistrbutor_Config page

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11/21      Yuli Fintescu       Created
2015-08/22      Paul Berglund       Changed Secondary COT to Class of Trade
                                    Added isActive = true when selecting Accounts
===============================================================================
*/
public with sharing class CPQ_Controller_ProposalDistrbutor_Config extends cpqControllerProposalBase {
    public Apttus_Proposal__Proposal__c theRecord{get;set;}
    private Set<String> incontracts;
    
    public CPQ_Controller_ProposalDistrbutor_Config(ApexPages.StandardController controller) {
        SelectedResultMap = new Map<String, CPQ_FacilityItem>();
        
        //get source proposal record
        theRecord = (Apttus_Proposal__Proposal__c)controller.getRecord();
        
        if (theRecord.ID != null) {
            List<Apttus_Proposal__Proposal__c> records = [Select Name, Id, Apttus_Proposal__Account__c, 
                                                            Apttus_Proposal__Account__r.Name,
                                                            Apttus_Proposal__Account__r.Status__c,
                                                            Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c, 
                                                            Apttus_Proposal__Account__r.Account_External_ID__c, 
                                                            Apttus_Proposal__Account__r.BillingStreet, 
                                                            Apttus_Proposal__Account__r.BillingState, 
                                                            Apttus_Proposal__Account__r.BillingPostalCode, 
                                                            Apttus_Proposal__Account__r.BillingCity, 
                                                            Apttus_Proposal__Account__r.BillingCountry,
                                                            Apttus_Proposal__Opportunity__c, 
                                                            Destined_to_Partner__c,
                                                            RecordTypeId,
                                                            RecordType.Name,
                                                            RecordType.DeveloperName,
                                                            Deal_Type__c,
                                                            Apttus_QPComply__MasterAgreementId__c,
                                                            Apttus_QPComply__MasterAgreementId__r.RecordTypeId,
                                                            Apttus_QPComply__MasterAgreementId__r.RecordType.Name,
                                                            Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                                                            (Select Id, Name, Account__c,
                                                                Account__r.Name,
                                                                Account__r.Status__c,
                                                                Account__r.Primary_GPO_Buying_Group_Affiliation__c, 
                                                                Account__r.Account_External_ID__c, 
                                                                Account__r.BillingStreet, 
                                                                Account__r.BillingState, 
                                                                Account__r.BillingPostalCode, 
                                                                Account__r.BillingCity, 
                                                                Account__r.BillingCountry, 
                                                                Proposal__c 
                                                            From Proposal_Distributors__r
                                                            Where Account__r.isActive__c = true
                                                            Order by Account__r.Name, Account__r.Account_External_ID__c)
                                                        From Apttus_Proposal__Proposal__c s Where ID =: theRecord.ID];
            if (records.size() > 0) {
                theRecord = records[0];
                
                incontracts = new Set<String>();
                if (theRecord.Deal_Type__c == 'Amendment' && CPQ_Utilities.isProposalDestinedToPartner(theRecord) && theRecord.Apttus_QPComply__MasterAgreementId__c != null) {
                    for (Agreement_Distributor__c f : [Select Id, Name, Agreement__c, Account__c, Account__r.Account_External_ID__c From Agreement_Distributor__c Where Account__r.Account_External_ID__c like 'US-%' and Agreement__c =: theRecord.Apttus_QPComply__MasterAgreementId__c]) {
                        incontracts.add(f.Account__r.Account_External_ID__c);
                    }
                }
                
                //update the "selected results" table from existing records
                for (Proposal_Distributor__c r : theRecord.Proposal_Distributors__r) {
                    CPQ_FacilityItem f = new CPQ_FacilityItem();
                    f.account = r.Account__r;
                    f.disabled = incontracts.contains(r.Account__r.Account_External_ID__c);
                    
                    if (!SelectedResultMap.containsKey(r.Account__r.Account_External_ID__c)) {
                        SelectedResultMap.put(r.Account__r.Account_External_ID__c, f);
                        
                        SelectedResults.add(f);
                    }
                }
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The record does not exist.'));
            }
        }
    }
    
/*=======================================================
    copy related records from other proposal or agreement
=========================================================*/
    public String SearchProposalSourceID {get;set;}
    public String SearchAgreementSourceID {get;set;}
    public String SearchSource {get;set;}
    public PageReference doSearchByCopy() {
        if (SearchSource == 'Proposal') {
            if (String.isEmpty(SearchProposalSourceID)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the source Proposal ID.'));
                return null;
            }
            
            //retrieve from source proposal
            String sql = 'Select Id, Name, Account__c, ' +
                                    'Account__r.Name, ' +
                                    'Account__r.Status__c, ' + 
                                    'Account__r.Primary_GPO_Buying_Group_Affiliation__c, ' + 
                                    'Account__r.Account_External_ID__c,  ' +
                                    'Account__r.BillingStreet,  ' +
                                    'Account__r.BillingState,  ' +
                                    'Account__r.BillingPostalCode, ' + 
                                    'Account__r.BillingCity,  ' +
                                    'Account__r.BillingCountry, ' +
                                    'Proposal__c, ' +
                                    'Proposal__r.Name ' +
                                'From Proposal_Distributor__c ' + 
                                'Where Proposal__r.Name =: SearchProposalSourceID and ' + 
                                    injectDistributorQueryPredicate('Account__r')  +
                                    ' and Account__r.isActive__c = true and ' +
                                    'Account__r.Account_External_ID__c like \'US-%\' ' +
                                'Order by Account__r.Name, Account__r.Account_External_ID__c';
                    System.debug('Search By Coyp SQL : ' + sql);
        	List<Proposal_Distributor__c> records = Database.query(sql);
            if (records.size() > 0) {
                for (Proposal_Distributor__c r : records) {
                    CPQ_FacilityItem f = new CPQ_FacilityItem();
                    f.account = r.Account__r;
                    f.disabled = incontracts.contains(r.Account__r.Account_External_ID__c);
                    
                    if (!SelectedResultMap.containsKey(r.Account__r.Account_External_ID__c)) {
                        SelectedResultMap.put(r.Account__r.Account_External_ID__c, f);
                        
                        SelectedResults.add(f);
                    }
                }
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Distributor is found for the given Proposal ' + SearchProposalSourceID + '.'));
            }
        } else if (SearchSource == 'Agreement') {
                if (String.isEmpty(SearchAgreementSourceID)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the source Agreement ID.'));
                return null;
            }
            
            //retrieve from source agreement
            String sql = 'Select Id, Name, Account__c, ' + 
                                    'Account__r.Name, ' +
                                    'Account__r.Status__c,  ' +
                                    'Account__r.Primary_GPO_Buying_Group_Affiliation__c,  ' +
                                    'Account__r.Account_External_ID__c,  ' +
                                    'Account__r.BillingStreet,  ' +
                                    'Account__r.BillingState,  ' +
                                    'Account__r.BillingPostalCode, ' + 
                                    'Account__r.BillingCity,  ' +
                                    'Account__r.BillingCountry, ' +
                                    'Agreement__c, ' +
                                    'Agreement__r.AgreementId__c ' +
                                'From Agreement_Distributor__c  ' +
                                'Where Agreement__r.AgreementId__c =: SearchAgreementSourceID and ' + 
                                 injectDistributorQueryPredicate('Account__r')  +
                                    ' and Account__r.isActive__c = true and ' +
                                    'Account__r.Account_External_ID__c like \'US-%\' ' +
                                'Order by Account__r.Name, Account__r.Account_External_ID__c';
        
        List<Agreement_Distributor__c> records = Database.query(sql);
            if (records.size() > 0) {
                for (Agreement_Distributor__c r : records) {
                    CPQ_FacilityItem f = new CPQ_FacilityItem();
                    f.account = r.Account__r;
                    f.disabled = incontracts.contains(r.Account__r.Account_External_ID__c);
                    
                    if (!SelectedResultMap.containsKey(r.Account__r.Account_External_ID__c)) {
                        SelectedResultMap.put(r.Account__r.Account_External_ID__c, f);
                        
                        SelectedResults.add(f);
                    }
                }
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Distributor is found for the given Agreement ' + SearchAgreementSourceID + '.'));
            }
        }
        
        SelectedResults.sort();
        return null;
    }
    
/*=======================================================
    search by CRN
=========================================================*/
    public String SearchCRNs{get;set;}
    public PageReference doSearchByCRNs() {
        SearchResults.clear();
        SearchAllChecked = false;
        
        if (String.isEmpty(SearchCRNs)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Customer Report Number to search.'));
            return null;
        }
        
        String[] a = SearchCRNs.split(',');
        Set<String> inList = new Set<String>();
        for (String s : a) {
            if (!String.isEmpty(s)) {
                String trimmed = s.trim();
                if (!String.isEmpty(trimmed)) {
                    if (trimmed.startsWith('US-'))
                        inList.add(trimmed);
                    else
                        inList.add('US-' + trimmed);
                }
            }
        }
        
        String sql = 'Select Id, Name, ' +
                                    'Status__c,  ' +
                                    'Primary_GPO_Buying_Group_Affiliation__c,  ' +
                                    'Account_External_ID__c,  ' +
                                    'BillingStreet,  ' +
                                    'BillingState,  ' +
                                    'BillingPostalCode,  ' +
                                    'BillingCity,  ' +
                                    'BillingCountry ' +
                                'From Account ' +
                                'Where Account_External_ID__c in: inList and  ' +
                                   injectDistributorQueryPredicate()  +
                                      ' and isActive__c = true ' +
                                'Order by Name, Account_External_ID__c';
        List<Account> records = Database.query(sql);
      
        if (records.size() > 0) {
            for (Account r : records) {
                if (!String.isEmpty(CPQ_Utilities.ValidCRN(r.Account_External_ID__c))) {
                    CPQ_FacilityItem f = new CPQ_FacilityItem();
                    f.account = r;
                    SearchResults.add(f);
                }
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Distributor is found for the given CRNs.'));
        }
                
        return null;
    }
    
/*=======================================================
    general search
=========================================================*/
    public String SearchString{get;set;}
    public PageReference doSearch() {
        SearchResults.clear();
        SearchAllChecked = false;
        
        if (String.isEmpty(SearchString)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter search criteria.'));
            return null;
        }
        
        String searchQuery='FIND \'' + SearchString + '\' ' +
                            'RETURNING Account(Id, Name, ' + 
                                'Status__c, ' + 
                                'Primary_GPO_Buying_Group_Affiliation__c, ' + 
                                'Account_External_ID__c, ' + 
                                'BillingStreet, ' + 
                                'BillingState, ' + 
                                'BillingPostalCode, ' + 
                                'BillingCity, ' + 
                                'BillingCountry, ' +
                                'Secondary_Class_of_Trade__c ' +
                            'Where isActive__c = true and Account_External_ID__c like \'US-%\' and' +
                            injectDistributorQueryPredicate() +
                            'Order by Name, Account_External_ID__c)'; 
        List<List<SObject>> searchList;
        
        if (Test.isRunningTest()) {
            List<Account> testAccounts = new List<Account>{
                new Account(Status__c = 'Active', Name = 'Test Distributor', Account_External_ID__c = 'US-111111', AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
                new Account(Status__c = 'Active', Name = 'Test Distributor', Account_External_ID__c = 'US-222222', AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
                new Account(Status__c = 'Active', Name = 'Test Distributor 3', Account_External_ID__c = 'US-333333', AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
            };
            insert testAccounts;
        
            searchList = new List<List<Account>>();
            searchList.add(testAccounts);
        } else
            searchList = Search.query(searchQuery);
                
        List<Account> records = searchList[0];
        if (records.size() > 0) {
            for (Account r : records) {
                if (!String.isEmpty(CPQ_Utilities.ValidCRN(r.Account_External_ID__c))) {
                    CPQ_FacilityItem f = new CPQ_FacilityItem();
                    f.account = r;
                    SearchResults.add(f);
                }
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Distributor is found for the given criteria.'));
        }
                
        return null;
    }
    
/*=======================================================
    get Top distributors
    Top distributors are stored in custom setting: CPQ_Top_Distributor__c
=========================================================*/
    public PageReference doSearchByTopDistributors() {
        SearchResults.clear();
        SearchAllChecked = false;

        for (CPQ_Top_Distributor__c t : [Select Id, Name, 
                                    Account__c,
                                    Account__r.Name, 
                                    Account__r.Status__c, 
                                    Account__r.Primary_GPO_Buying_Group_Affiliation__c, 
                                    Account__r.Account_External_ID__c, 
                                    Account__r.BillingStreet, 
                                    Account__r.BillingState, 
                                    Account__r.BillingPostalCode, 
                                    Account__r.BillingCity, 
                                    Account__r.BillingCountry
                                From CPQ_Top_Distributor__c
                                Where Account__c <> NULL and
                                      Account__r.isActive__c = true
                                Order by Account__r.Account_External_ID__c]) {
            
            Account r = t.Account__r;
            if (!String.isEmpty(CPQ_Utilities.ValidCRN(r.Account_External_ID__c))) {
                CPQ_FacilityItem f = new CPQ_FacilityItem();
                f.account = r;
                SearchResults.add(f);
            }
        }
                
        return null;
    }
    
    //check all checkboxes
    public Boolean SearchAllChecked {get; set;}
    public PageReference doSearchCheckAll() {
        for (CPQ_FacilityItem f : SearchResults) {
            f.checked = SearchAllChecked;
        }
        
        return null;
    }
        
    public Boolean getShowSearchResult() {
        return SearchResults.size() > 0;
    }

    public List<CPQ_FacilityItem> SearchResults {
        get {
            if (SearchResults == null) {
                SearchResults = new List<CPQ_FacilityItem>();
            }
            return SearchResults; 
        }
        set;
    }
    
    //add all checked
    public Boolean getAddCheckedEnabled() {
        for (CPQ_FacilityItem f : SearchResults) {
            if (f.checked == true)
                return true;
        }
        
        return false;
    }
    
    public PageReference doAddChecked() {
        SelectAllChecked = false;
        for (CPQ_FacilityItem f : SearchResults) {
            if (f.checked) {        
                if (!SelectedResultMap.containsKey(f.account.Account_External_ID__c)) {
                    CPQ_FacilityItem copy = f.copy();
                    copy.bgColor = CPQ_FacilityItem.INPUT_COLOR;
                    copy.disabled = incontracts.contains(f.account.Account_External_ID__c);
                    
                    SelectedResultMap.put(f.account.Account_External_ID__c, copy);
                    SelectedResults.add(copy);
                }
            }
        }
        
        SearchResults.clear();
        
        SelectedResults.sort();
        
        return null;
    }
    
    //check all checkboxes
    public Boolean SelectAllChecked {get; set;}
    public PageReference doSelectCheckAll() {
        for (CPQ_FacilityItem f : SelectedResults) {
            if (f.disabled == false)
                f.checked = SelectAllChecked;
        }
        
        return null;
    }
    
    //remove all checked
    public Boolean getRemoveCheckedEnabled() {
        for (CPQ_FacilityItem f : SelectedResults) {
                if (f.checked == true)
                        return true;
        }
        
        return false;
    }
    
    public PageReference doRemoveChecked() {
        List<CPQ_FacilityItem> l = new List<CPQ_FacilityItem>();
        Map<String, CPQ_FacilityItem> m = new Map<String, CPQ_FacilityItem>();
        
        for (CPQ_FacilityItem f : SelectedResults) {
            if (!f.checked) {
                l.add(f);
                m.put(f.account.Account_External_ID__c, f);
            }
        }
        
        SelectedResults = l;
        SelectedResultMap = m;
        return null;
    }
    
    public Boolean getShowSelectedResult() {
        return SelectedResults.size() > 0;
    }
    
    public Map<String, CPQ_FacilityItem> SelectedResultMap;
    public List<CPQ_FacilityItem> SelectedResults {
        get {
            if (SelectedResults == null) {
                SelectedResults = new List<CPQ_FacilityItem>();
            }
            return SelectedResults; 
        }
        set;
    }
    
    //Save, add if CRNs do not existing for exisitng proposal. deleted if CRNs are no longer selected
    public PageReference doSave() {
        Map<String, Proposal_Distributor__c> existings = new Map<String, Proposal_Distributor__c>();
        for (Proposal_Distributor__c r : theRecord.Proposal_Distributors__r) {
            existings.put(r.Account__r.Account_External_ID__c, r);
        }
        
        List<Proposal_Distributor__c> toInsert = new List<Proposal_Distributor__c>();
        for (String key : SelectedResultMap.keySet()) {
            if (!existings.containsKey(key)) {
                CPQ_FacilityItem f = SelectedResultMap.get(key);
                
                Proposal_Distributor__c r = new Proposal_Distributor__c();
                r.Proposal__c = theRecord.ID;
                r.Account__c = f.account.ID;
                
                toInsert.add(r);
            }
        }
        
        List<Proposal_Distributor__c> toDelete = new List<Proposal_Distributor__c>();
        for (String key : existings.keySet()) {
            if (!SelectedResultMap.containsKey(key)) {
                toDelete.add(existings.get(key));
            }
        }
        
        if (toInsert.size() > 0)
            insert toInsert;
        
        if (toDelete.size() > 0)
            delete toDelete;
        
        return new ApexPages.StandardController(theRecord).view();
    }
}