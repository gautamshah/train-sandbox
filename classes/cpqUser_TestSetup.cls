@isTest
public class cpqUser_TestSetup
{
    /**
     * Gets the system admin user
     * @return the system admin user
     */
    public static User getSysAdminUser() {
        // get the sys admin profile
        Id profileId = Profile_c.fetch('System Administrator').Id;
        // get the system admin user
        List<User> users = [SELECT Id,
                                   Name,
                                   ProfileId,
                                   UserName,
                                   FirstName,
                                   LastName,
                                   Email,
                                   TimeZoneSidKey,
                                   EmailEncodingKey,
                                   LocaleSidKey,
                                   LanguageLocaleKey
                            FROM User
                            WHERE IsActive = TRUE AND
                                  ProfileId = :profileId
                            LIMIT 1];

        return (!users.isEmpty() ? users[0] : null);

    }

}