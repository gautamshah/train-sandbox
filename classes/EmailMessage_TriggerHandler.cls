/****************************************************************************************
 * Name    : EmailMessage_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 14/01/2015 
 * Purpose : Contains all the logic coming from EmailMessage trigger
 * Dependencies: EmailMessageTrigger Trigger
 *             , Case & EmailMessage Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE             AUTHOR               CHANGE
 * * 4/23/2015    Gaurav Gulanjkar    Ristricted multiple appending the SystemNote__c.Line no -128 to 130
 * * 4/27/2015    Gaurav Gulanjkar    Assign case to contact from Email message body 
 * * 5/07/2015    Gaurav Gulanjkar	  Auto Follow logic
 *****************************************************************************************/
public with sharing class EmailMessage_TriggerHandler {

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    Boolean matchFound = false;
    List<String> validEmailIds = new List<String>();
    List<EntitySubscription> entitySubToBeInsert = new List<EntitySubscription>();
	
    public EmailMessage_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void OnBeforeInsert(List<EmailMessage> newObjects){
        //only handle one fax each time.
        if(BatchSize==1)
        {
            EmailMessage em  = newObjects[0];
            if(em.Headers!=null && em.Incoming && (em.Subject == null || 
                (em.Subject != null && em.Subject.contains('has been received. ref:') == false && em.Subject.endsWith(':ref') == false)))
            {
                List<Case> cases = [Select Id,ContactId from Case where Id = :em.ParentId limit 1];
                if(cases.size()>0)
                {
                    Case newCase = cases[0];
                        
                    List<String> MessageIDs = new List<String>();
                    for(String s : em.Headers.split('\n'))
                    {
                        if(s.startsWith('References: '))
                        {
                            for(String MID : s.replace('References: ','').trim().replace(',',' ').split(' '))
                            {
                                if(MID.trim()!='')
                                	MessageIDs.add(MID);
                            }
                            break;
                        }
                    }
                    
                    List<Case> oldcases = [Select Id from Case where Id != :newCase.Id and Email_Message_ID__c = :MessageIDs and isClosed = false
                                            order by CreatedDate desc limit 1];
                    if(oldcases.size()>0)
                    {
                        em.ParentId = oldCases[0].Id;
                        delete newCase;
                    }
                    else
                    {
                        oldcases = [Select Id,ClosedDate,OwnerId from Case where Id != :newCase.Id and Email_Message_ID__c = :MessageIDs and isClosed = true
                                            order by ClosedDate desc limit 1];
                        if(oldcases.size()>0 && oldcases[0].ClosedDate.addHours(48)>datetime.now())
                        {
                            em.ParentId = oldCases[0].Id;
                            delete newCase;
                        }
                    }
                }
            }
        }
    }
    
    public void OnAfterInsert(List<EmailMessage> newObjects){
        //only handle one fax each time.
        if(BatchSize==1)
        {
            //Fax-to-Case
            EmailMessage em  = newObjects[0];
            
            List<Case> cases = [Select Id,OwnerId,ContactId,Status,IsClosed,CreatedDate,SuppliedEmail,CS_Center__c,Email_Message_ID__c,SystemNote__c from Case where Id = :em.ParentId limit 1];
            if(cases.size()>0)
            {
                Case c = cases[0];
                if(em.Incoming) 
                {
                    Boolean case2BeUpdated = false;
                    if(em.Subject != null && em.Subject.startsWith('A fax has arrived from remote ID \''))
                    {
                        c.Origin = 'Fax';
                        case2BeUpdated = true;
                        Integer index = em.Subject.indexOf('\'',35);
                        if(index!=-1)
                        {
                            String faxNum = em.Subject.substring(34,index);
                            List<Contact> Contacts = [select Id from Contact where Fax = :faxNum];
                            if(Contacts.size()>0)
                            {
                                c.ContactId = Contacts[0].Id;
                            }
                            else
                            {
                                List<Account> accs = [Select Id from Account where Fax=:faxNum];
                                if(Accs.size()>0)
                                {
                                    c.AccountId = accs[0].Id;
                                }
                            }
                        }
                    }
                    
                    if(em.Headers!=null && c.Email_Message_ID__c == null && (em.Subject == null ||
                         (em.Subject != null && em.Subject.contains('has been received. ref:') == false && em.Subject.endsWith(':ref') == false)))
                    {
                        for(String s : em.Headers.split('\n'))
                        {
                            if(s.startsWith('Message-ID: '))
                            {
                                c.Email_Message_ID__c = s.replace('Message-ID: ','').trim();
                                case2BeUpdated = true;
                                break;
                            }
                        }
                    }
                    
                    //update new message flag
                    if(c.CreatedDate.addSeconds(30)<datetime.now())
                    { 
                        if(c.SystemNote__c==null)
                            c.SystemNote__c = '#';
                        
                        boolean isContain = c.SystemNote__c.contains('NewMessage#');
                            if(!isContain)
                                c.SystemNote__c +='NewMessage#';
                        
                        if(c.IsClosed && c.CS_Center__c!=null && c.CS_Center__c =='ZA')
                            c.Status = 'Re-Open';
                        case2BeUpdated = true;
                    }
                    else if(c.ContactId==NULL)
                    {
                        Set<String> emailIdsToBeExcluded = new Set<String>();
                                            
                        Matcher validMatcher = null;
                        Matcher invalidMatcher = null;
                        
                        if(em.Headers!=null && (em.TextBody!=null || em.HtmlBody != null))
                        {
                            String validEmailRegex =  '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';
                            String inValidEmailRegex = '([a-zA-Z0-9_\\-\\.]+)@((covidien.com)|(medtronic.com)|(Covidien.com)|(Medtronic.com))';
                            
                            Pattern validPattern = Pattern.compile(validEmailRegex );
                            Pattern inValidPattern = Pattern.compile(inValidEmailRegex);
                            
                            if(em.HTMLBody != null)
                            {
                                validMatcher = validPattern.matcher(em.HtmlBody);
                                invalidMatcher = inValidPattern.matcher(em.HtmlBody);
                            }
                            else if(em.TextBody != null)
                            {
                                validMatcher = validPattern.matcher(em.TextBody);
                                invalidMatcher = inValidPattern.matcher(em.TextBody);
                            }
                            while(invalidMatcher.find())
                            {   
                                 emailIdsToBeExcluded.add(invalidMatcher.group());
                            }
                            
                            while(validMatcher.find())
                            {   
                                 if(emailIdsToBeExcluded.contains(validMatcher.group())==false)
                                    validEmailIds.add(validMatcher.group());
                            }
                        }
                        
                        List<Contact> emailContacts = [Select Id, Name,Email From Contact where email in: validEmailIds
                                                      and recordTypeId !=: Utilities.recordTypeMap_Name_Id.get('Covidien Employee') and recordTypeId !=: Utilities.recordTypeMap_Name_Id.get('Master Clinician')];
                        
                        if(emailContacts.size() > 0)
                        {
                            c.ContactId = emailContacts.get(0).Id;
                            case2BeUpdated = true;
                        }
                    }
                    if(case2BeUpdated)
                    {
                        try{
                            update c;
                        }
                        catch(DmlException e)
                        {
                            if(e.getDmlType(0) == StatusCode.DUPLICATE_VALUE && 
                               e.getDmlMessage(0).StartsWith('duplicate value found: Email_Message_ID__c duplicates value on record with id:'))
                            {
                                c.Email_Message_ID__c = null;
                                c.Status = 'Closed-Duplicate';
                                update c;
                            }
                            else
                                throw e;
                        }
                    }
                    if(em.ToAddress!=null || em.CcAddress!=null)
                    {
                    	set <String> finalEmail = new set<String>();
						set <String> toEmail = new set<String>();
						set <String> ccEmail = new set<String>();
                        
                        if(toEmail!=null && toEmail.size()>0)
                        	toEmail.addAll(em.ToAddress.split(';'));
                        if(ccEmail!=null && ccEmail.size()>0)
                        	ccEmail.addAll(em.CcAddress.split(';'));
						
						set <String> emailSet = new set<String>(toEmail);
						emailSet.addAll(ccEmail);
						
						if(emailSet!=null && emailSet.size()>0){
							for(String email:emailSet){
								if(email!='' && email!=null){
									finalEmail.add(email.trim());
								}
							}
						}
                        List<Id> currentFollowers = new List<Id>();
                        for(EntitySubscription es: [Select SubscriberId from EntitySubscription Where ParentId = :c.Id])
                        {
                            currentFollowers.add(es.SubscriberId);
                        }
                        
						for(user u : [Select id from user where IsActive=true and 
                                      email in : finalEmail and UserType = 'Standard' and Id !=:currentFollowers])
						{
							EntitySubscription ent = new EntitySubscription();
							ent.SubscriberId = u.Id;
							ent.ParentId = c.Id;
							ent.NetworkId = Network.getNetworkId();
							entitySubToBeInsert.add(ent);	
						}
						try{
							insert entitySubToBeInsert;
						}
						catch(DmlException e)
                        {
                        	system.debug('--Exception--'+e);
                        }
                    }	
                }
                //Close response milestone
                if(em.Incoming==false && em.ParentId != null && em.ParentId.getSObjectType().getDescribe().getName()=='Case')
                {
                    if(c.SuppliedEmail!=null && em.ToAddress != null && em.ToAddress.contains(c.SuppliedEmail))
                    {
                        List<CaseMilestone> responseMilestones = [Select CompletionDate from CaseMilestone 
                        where CaseId = :c.Id and IsCompleted = false and 
                            MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Response Time')];
                        
                        for(CaseMilestone cm : responseMilestones)
                        {
                            cm.CompletionDate = DateTime.now();
                        }
                        
                        update responseMilestones;
                    }
                }
                /*if(em.Incoming==false && string.valueOf(c.OwnerId).startsWith('00G'))
           		{
				 c.addError('case is still in queue.');
				 system.debug('--inside queue--');
           		}*/
            }
        }
    }
    
}