global with sharing class MissionInMotion {
    
    /****************************************************************************************
* Name    : MissionInMotion 
* Author  : Shawn Clark
* Date    : 04/27/2017 
* Purpose : Engine for MIM Microsite
*****************************************************************************************/
    
    //Main Class Variables
    public String SalesOrg {get;set;}
    public String Area {get;set;}
    public String ConType {get;set;}
    public String ContributionAmount {get;set;}
    public Boolean MedtronicMatch {get;set;}
    public String CharityName {get; set;}  
    public String VFPageName {get; set;} 
    public String CurrentUser {get; set;}  
    public String UserFirstname {get; set;}
    public String UserLastname {get; set;}
    public String UserEmail {get; set;}
    public String MyComment {get; set;}
    public String MyCharity {get; set;}
    public Double gt2 {get; set;} 
    public List<AggregateResult> Act_Total_Value = new List<AggregateResult>{};  
        public List<AggregateResult> Act_Total_Hours = new List<AggregateResult>{};  
            public List<MissionInMotionConfig__c>  MiM_Config = new  List<MissionInMotionConfig__c>{};
                public Integer TotalValue {get; set;}
    public Integer TotalHours {get; set;}
    public Integer LeaderNum {get;set;}
    public String DisplayTypes {get;set;}
    public Boolean ShowDetail {get;set;}
    public String LinkTitle {get;set;}
    public String SortBy {get;set;}
    public String DonationPath {get;set;}
    public String VolunteerPath {get;set;}
    public String LogoFilename {get;set;}
    public Date StartDate {get;set;}
    public Date EndDate {get;set;}
    public Boolean CampaignOpen {get;set;}
    public String CampaignMsg {get;set;}
    
    public Boolean ShowMainSection {get;set;}
    
    
    //Wrapper Class to Hold Totals by Org
    public class SalesOrgAgr 
    {
        public String SalesOrg {get;set;}
        public Double CoreDonation {get;set;}
        public Double VolunteeringHours {get;set;}
        
        //Constructor
        public SalesOrgAgr (string iorg, Double er, Double vh) 
        {
            SalesOrg = iorg;
            CoreDonation = er;
            VolunteeringHours = vh;
        }
    } 
    
    
    //Wrapper Class to Hold Totals by Area
    public class AreaAgr 
    {
        public String SalesOrg {get;set;}
        public String Area {get;set;}
        public String Type {get;set;}
        public Double CoreDonation {get;set;}
        public Double VolunteeringHours {get;set;}
        public Integer OrdNum {get;set;}
        
        //Constructor
        public AreaAgr (string iorg, string iarea, string itype, Double er, Double vh, Integer ord) 
        {
            SalesOrg = iorg;
            Area = iarea;
            Type = itype;
            CoreDonation = er;
            VolunteeringHours = vh;
            OrdNum = ord;
        }
    }
    
    
    // Main Class Constructor
    public MissionInMotion() {
        
        //Will parse the VF Page name of URL String for Charity Name Assignment
        GetCause();
        //Set Variables
        CurrentUser = userinfo.getuserid();
        UserFirstName = [select Firstname from user where id=:userinfo.getuserid()].Firstname;
        UserLastName = [select Lastname from user where id=:userinfo.getuserid()].Lastname ;
        UserEmail = [select Email from user where id=:userinfo.getuserid()].Email;
        MiM_Config = [select DefaultSortOrder__c, DonationPath__c, VolunteerPath__c, Logo_Filename__c, Start_Date__c, End_Date__c from MissionInMotionConfig__c where Name = :VFPageName];
        SortBy = MiM_Config[0].DefaultSortOrder__c;
        DonationPath = MiM_Config[0].DonationPath__c;
        VolunteerPath = MiM_Config[0].VolunteerPath__c;
        LogoFilename = MiM_Config[0].Logo_Filename__c;
        StartDate = MiM_Config[0].Start_Date__c;
        EndDate = MiM_Config[0].End_Date__c;

        IF (Date.Today() > = StartDate && Date.Today() <= EndDate) 
                       {
                       CampaignOpen = TRUE;
                       }
                       Else 
                       {
                       CampaignOpen = FALSE;
                       }
        IF (Date.Today() < StartDate) {
        CampaignMsg = 'This campaign is currently closed. Submissions will begin on: ' + StartDate;
        }
        Else IF (Date.Today() > EndDate) { 
        CampaignMsg = 'This campaign is currently closed. Submissions ended on: ' + EndDate;
        }              
        MedtronicMatch = False;
        ConType = 'Donation';
        DisplayTypes = 'Donation';
        ShowMainSection = true;
        LinkTitle = 'More Details';
        ShowDetail = false;
        
        
        system.debug('Mission in Motion Config: ' + MiM_Config[0]);
        
    }
    
    
    
    //Extracts VF Page Name from URL and assigns as Charity Name to be submitted with Record
    public pageReference GetCause(){
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1]; 
        String strurl_abb = strurl.substringBefore('?');
        VFPageName = strurl_abb;
        System.Debug(VFPageName);
        CharityName = strurl_abb.replace('_', ' ');
        System.Debug(CharityName);
        return null;
    }
    
    
    public String[] getContribTypes()
    {
        list <Mission_In_Motion__c> ContribTypeList = new list <Mission_In_Motion__c>();
        set<String> setContribs = new Set<String>();
        list <String> Contribs = new list <String>();
        
        ContribTypeList = [SELECT Contribution_Type__c FROM Mission_In_Motion__c Order by Contribution_Type__c ]; 
        
        for (Integer i = 0; i< ContribTypeList.size(); i++)
        {
            setContribs.add(ContribTypeList[i].Contribution_Type__c); // Set to dedup Therapy List
        }
        
        // convert the set into a string array  
        Contribs = new String[setContribs.size()];
        Integer i = 0;
        for (String t : setContribs) { 
            Contribs[i] = t;
            i++;
        } 
        return Contribs;
    } 
    
    
    
    public integer getGrandTotal() {
        Act_Total_Value = [SELECT SUM(Core_Donation__c), SUM(Volunteering_Hours__c) FROM Mission_In_Motion__c WHERE Charity_Name__c = :CharityName];
        
        TotalValue = Integer.valueOf(Act_Total_Value[0].get('expr0'));
        
        IF (TotalValue == NULL)
        {
            TotalValue = 0;
        }
        
        return TotalValue; 
    }
    
    
    public integer getGrandTotalHours() {
        Act_Total_Hours = [SELECT SUM(Volunteering_Hours__c) FROM Mission_In_Motion__c WHERE Charity_Name__c = :CharityName];
        
        TotalHours  = Integer.valueOf(Act_Total_Hours[0].get('expr0'));
        
        
        IF (TotalHours == NULL)
        {
            TotalHours = 0;
        }
        
        return TotalHours;
    }
    
    //Get Aggregated Data by SalesOrg & Area and Add to Wrapper Class AreaAgr
    public list<AreaAgr> getMetriclist() {
        //Create new list to hold results based on new class
        List<AreaAgr> pm = new List<AreaAgr>();
        
        
        IF(SortBy == 'Hours') {
            
            
            AggregateResult[] Level3 =
                [SELECT Sales_Organization__c iorg, Area__c iarea,
                 SUM(Core_Donation__c) er,
                 SUM(Volunteering_Hours__c) vh
                 FROM Mission_In_Motion__c 
                 WHERE Charity_Name__c = :CharityName
                 GROUP BY Sales_Organization__c, Area__c
                 ORDER BY SUM(Volunteering_Hours__c) DESC NULLS LAST];
            
            //Loop through results and add each result to new class so we can display    
            for (AggregateResult ar : Level3 )  {
                pm.add(new AreaAgr ((String)ar.get('iorg'),(String)ar.get('iarea'), '', (Double)ar.get('er')==null?0:(Double)ar.get('er'), (Double)ar.get('vh')==null?0:(Double)ar.get('vh'),  3));
            }
            
            for (Integer i = 0; i< pm.size(); i++)
            {
                System.Debug('Aggregate by Org/Area ' + i + ': ' + pm[i]);   
            }        
        }
        
        ELSE IF(SortBy == 'Dollars') { 
            AggregateResult[] Level3 =
                [SELECT Sales_Organization__c iorg, Area__c iarea,
                 SUM(Core_Donation__c) er,
                 SUM(Volunteering_Hours__c) vh
                 FROM Mission_In_Motion__c 
                 WHERE Charity_Name__c = :CharityName
                 GROUP BY Sales_Organization__c, Area__c
                 ORDER BY SUM(Core_Donation__c) DESC NULLS LAST];
            
            //Loop through results and add each result to new class so we can display    
            for (AggregateResult ar : Level3 )  {
                pm.add(new AreaAgr ((String)ar.get('iorg'),(String)ar.get('iarea'), '', (Double)ar.get('er')==null?0:(Double)ar.get('er'), (Double)ar.get('vh')==null?0:(Double)ar.get('vh'),  3));
            }
        }
        
        
        return pm;                         
    }
    
    
    //Get Aggregated Data by SalesOrg and Add to Wrapper Class SalesOrgAgr 
    public list<SalesOrgAgr> getOgTotals() {
        
        //Create new list to hold results based on new class
        List<SalesOrgAgr> pm = new List<SalesOrgAgr>();
        
        IF(SortBy == 'Hours') {
            System.Debug('Ok, we are sorting by Hours');
            AggregateResult[] OrgTotals =
                [SELECT Sales_Organization__c iorg, 
                 SUM(Core_Donation__c) er,
                 SUM(Volunteering_Hours__c) vh
                 FROM Mission_In_Motion__c 
                 WHERE Charity_Name__c = :CharityName
                 GROUP BY Sales_Organization__c
                 ORDER BY SUM(Volunteering_Hours__c) DESC];
            
            //Loop through results and add each result to new class so we can display    
            for (AggregateResult ar : OrgTotals )  {
                pm.add(new SalesOrgAgr ((String)ar.get('iorg'), (Double)ar.get('er')==null?0:(Double)ar.get('er'), (Double)ar.get('vh')==null?0:(Double)ar.get('vh')));
            }
        }
        
        ELSE IF(SortBy == 'Dollars') {  
            System.Debug('Ok, we are sorting by Dollars');
            AggregateResult[] OrgTotals =
                [SELECT Sales_Organization__c iorg, 
                 SUM(Core_Donation__c) er,
                 SUM(Volunteering_Hours__c) vh
                 FROM Mission_In_Motion__c 
                 WHERE Charity_Name__c = :CharityName
                 GROUP BY Sales_Organization__c
                 ORDER BY SUM(Core_Donation__c) DESC];
            
            //Loop through results and add each result to new class so we can display    
            for (AggregateResult ar : OrgTotals )  {
                pm.add(new SalesOrgAgr ((String)ar.get('iorg'), (Double)ar.get('er')==null?0:(Double)ar.get('er'), (Double)ar.get('vh')==null?0:(Double)ar.get('vh')));
            }
        }
        
        return pm;  
    } 
    
    
    //Get Sales Organizations
    public List<SelectOption> getSalesOrganization() {    
        List<SelectOption> options =  new List<SelectOption>();
        options.add(new SelectOption('',''));     
        Schema.DescribeFieldResult fieldResult = Mission_In_Motion__c.Sales_Organization__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                  
        }    
        return Options;    
    }  
    
    public void PullArea() {
        getAreaList();
    }
    
    //Get AccountsType by Country
    public list<SelectOption> getAreaList() {
        list<SelectOption> options = new list<SelectOption>();
        list<ApexPickListValLookup__c> rts = [SELECT PickListLabel__c, PickListVal__c FROM ApexPickListValLookup__c WHERE ObjectName__c ='Mission_In_Motion__c' AND Controlling_Field_Name__c = 'Sales_Org__c' and Controlling_Picklist_Value__c = :SalesOrg ORDER BY PickListVal__c];
        
        system.debug(rts.size());
        IF (rts.size() > 1) {
            options.add(new SelectOption('',''));
        }
        
        //If the User belongs to a Country that has Picklist Values Defined, then use them
        if(String.IsNotEmpty(SalesOrg))
        {   
            for (ApexPickListValLookup__c rt : rts) 
            {
                options.add(new SelectOption(rt.PickListVal__c, rt.PickListLabel__c));
            } 
            return options;
        }         
        
        return options;
    }  
    
    
    
    
    //Return Validation Error
    public PageReference ValidationMsgReturn() 
    {
        If (String.IsNotEmpty(ContributionAmount) && ContributionAmount.isNumeric() == false) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Amount field can only contain numbers!'));
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please fill out all fields!'));
        }
        return null;
    }
    
    
    
    //Return Validation Error2
    public PageReference ValidationMsgReturn2() 
    {
        if (ConType == 'Donation') {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Thank you for your $' + ContributionAmount + ' Donation!'));
        }
        else if (ConType == 'Volunteer') {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Thank you for volunteering ' + ContributionAmount + ' Hours!'));
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Redirecting to Mission In Motion... (Please wait)'));
        return null;
    }
    
    //Create MiM Record
    public PageReference CreateMIM() {
        
        Mission_In_Motion__c mim = new Mission_In_Motion__c();
        mim.Charity_Name__c = CharityName;
        mim.First_Name__c = UserFirstname;
        mim.Last_Name__c = UserLastname;
        mim.Email__c = UserEmail;
        mim.Sales_Organization__c = SalesOrg;
        mim.Area__c = Area;
        mim.Comments__c = MyComment;
        mim.MiM_Charity_Name__c = MyCharity;
        mim.Contribution_Type__c = ConType;
        if (ConType == 'Donation') {
            mim.Core_Donation__c = Double.valueOf(ContributionAmount);
            mim.Medtronic_Match__c = true;
        }
        else if (ConType == 'Volunteer') {
            mim.Volunteering_Hours__c = Double.valueOf(ContributionAmount);
            mim.Medtronic_Match__c = false;
        }
        
        
        try{ 
            insert mim; 
            ContributionAmount = '';
        } 
        catch (Exception ee) 
        {}
        return null;      
    }   
}