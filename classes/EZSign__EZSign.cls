/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@isTest
global class EZSign {
    global String base64 {
        get;
        set;
    }
    global Boolean blockSFRedirect {
        get;
        set;
    }
    global Boolean createExtImage {
        get;
        set;
    }
    global Boolean hasName {
        get;
        set;
    }
    global Boolean hasSig {
        get;
        set;
    }
    global Boolean hasSigImage {
        get;
        set;
    }
    global Boolean multSigs;
    global String objectType;
    global Boolean redirect {
        get;
        set;
    }
    global Boolean refresh {
        get;
        set;
    }
    global String refreshURL {
        get;
        set;
    }
    global Boolean retToRecord {
        get;
        set;
    }
    global Boolean saveCoord {
        get;
        set;
    }
    global Boolean sforceone {
        get;
        set;
    }
    global String sigOutput {
        get;
        set;
    }
    global SObject theObject {
        get;
        set;
    }
    global List<SObject> theObjectList {
        get;
        set;
    }
    global Id theObjId {
        get;
        set;
    }
    global Boolean validKey {
        get;
        set;
    }
    global EZSign() {

    }
    global Boolean checkNullOrBlank(String input) {
        return null;
    }
    global System.PageReference clearSignature() {
        return null;
    }
    global System.PageReference createSignature() {
        return null;
    }
    global Boolean getMultSigs() {
        return null;
    }
    global Boolean isSF1() {
        return null;
    }
    global static void mapToParent(List<EZSign__Signature__c> newSignatures) {

    }
    global System.PageReference saveSignature() {
        return null;
    }
    global void setMultSigs(Boolean mSigs) {

    }
    global System.PageReference submitSignature() {
        return null;
    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testEZSign() {

    }
}
