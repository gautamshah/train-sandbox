public with sharing class EMS_ApplicationPageController {
public List <ProcessInstanceWorkitem> Lpes{get;set;}
public List<EMS_Event__c> MyCreatedEvents{get;set;}
public List<EMS_Event__c> OngoingEvents{get;set;}
public List<EMS_Event__c> EventsIOwn{get;set;}
public Map<String,Id> MNameFolder{get;set;}
Public String CreatedByMe{get;set;}
Public String OnGoingEventsURL{get;set;}
Public String EventsIOwnURL{get;set;}

Map<String,String> PrefixList;
public EMS_ApplicationPageController(){
    MNameFolder = new Map<String,Id>();
    PrefixList = PrefixList();
    List<EMS_GCC_Approvers_Delegated__c> AllDelegatedApprovals = EMS_GCC_Approvers_Delegated__c.getall().values();
    Set<String> FinalGCCApprovers = new Set<string>();
    for(EMS_GCC_Approvers_Delegated__c eachGCCApp : AllDelegatedApprovals){
        FinalGCCApprovers.add(eachGCCApp.Name);
    
    }
    
    Set<String> SFoldernames = new Set<String>{'Compliance_Reports','Generic_DashBoards','Finance_Tracking'};
    if(!FinalGCCApprovers.contains(UserInfo.getUserId())){
        Lpes = new List<ProcessInstanceWorkitem>([SELECT Id,ActorId,ProcessInstance.TargetObjectId,ProcessInstance.Id,ProcessInstance.LastActorId,ProcessInstance.Status FROM ProcessInstanceWorkitem where ActorId=:Userinfo.getUserId() and ProcessInstance.TargetObjectId!=null and ProcessInstance.Status ='Pending']);
    }
    else{
        EMS_GCC_Approvers_Delegated__c myCS1 = EMS_GCC_Approvers_Delegated__c.getValues(UserInfo.getUserId());
        Lpes = new List<ProcessInstanceWorkitem>([SELECT Id,ActorId,ProcessInstance.TargetObjectId,ProcessInstance.Id,ProcessInstance.LastActorId,ProcessInstance.Status FROM ProcessInstanceWorkitem where ActorId=:myCs1.Delegated_Approval__c and ProcessInstance.TargetObjectId!=null and ProcessInstance.Status ='Pending']);
    }
    MyCreatedEvents=new List<EMS_Event__c>([Select Id,Name,OwnerId from EMS_Event__c where OwnerId=:Userinfo.getUserId()]);
    OngoingEvents=new List<EMS_Event__c>([Select Id,Name,Event_Status__c,OwnerId from EMS_Event__c where OwnerId=:Userinfo.getUserId() and Event_Status__c!='Concluded']);
    EventsIOwn = new List<EMS_Event__c>([Select Id,Name,OwnerId,Event_Owner_User__c from EMS_Event__c where Event_Owner_User__c =:Userinfo.getUserId() ]);
    List<Folder> LFolders  = [select id,developername  from folder where developername in :SFoldernames];
    for(Folder eachFolder : LFolders){
    
    MNameFolder.put(eachFolder.developerName,eachFolder.Id);

    }
    EventsIOwnURL='/apex/EMS_EventsIOwn';
    CreatedByMe = ListViewURL('Events Created by Me');
    OnGoingEventsURL=ListViewURL('Ongoing Events Created by Me');
}

Public Map<String,String> PrefixList(){

        Map<String,String> PrefixList = new Map<String,String>{};
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(String sObj : gd.keySet()){

            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();

            PrefixList.put(r.getName(), r.getKeyPrefix());

        }

        return PrefixList;

    }

Public String ListViewURL(String ListViewLabel){

        String ListViewURL;

        String ListViewId;

        String q = 'SELECT Name FROM EMS_Event__c LIMIT 1';

        ApexPages.StandardSetController ACC = new ApexPages.StandardSetController(Database.getQueryLocator(q));

        List<SelectOption> ListViews = ACC.getListViewOptions();

        for(SelectOption w : ListViews ){

            if(w.getLabel()==ListViewLabel){

                ListViewId = w.getValue().left(15);

                ListViewURL='/'+PrefixList.get('EMS_Event__c')+'?fcf='+ListViewId;

            }

        }

        return ListViewURL;

    }
}