@istest
private class CustomQuoteApprovalsControllerTest
{
    static testmethod void method_One()
    {

        User usr = cpqUser_TestSetup.getSysAdminUser();
        if (usr == null) {
            usr = cpqUser_c.CurrentUser;
        }

        cpqOpportunity_TestSetup.buildScenario_CPQ_Opportunity();
        Account acc = cpq_TestSetup.testAccounts.get(0);
        Contact ct = cpq_TestSetup.testContacts.get(0);
        Opportunity opp = cpq_TestSetup.testOpportunities.get(0);

        Apttus_Proposal__Proposal__c prop = cpqProposal_TestSetup.generateProposal(acc,ct,opp,
            cpqRecordType_TestSetup.getRecordType('Apttus_Proposal__Proposal__c').DeveloperName);
        prop.ownerId = usr.Id;
        prop.Approval_Preview_Status__c='Pending';
        insert prop;

        Product2 prd = cpqProduct2_TestSetup.generateProducts(1).get(0);
        insert prd;

        Apttus_Proposal__Proposal_Line_Item__c pli = cpqProposalLineItem_TestSetup.generateProposalLineItem(prop,prd);
        pli.Apttus_QPApprov__Approval_Status__c='Not Submitted';
        insert pli;

        // Make records generic to improve test portability
        sObject record = prop;
        sObject child = pli;
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_SHOW_HEADER,'TRUE');
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_SHOW_SIDEBAR,'TRUE');
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_AR_FIELDSET_NAME,'Field Set');
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURN_BUTTON_LABEL,'test');
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_QUOTE_ID,record.Id);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNID,record.Id);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNPAGE,'/'+record.Id);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_LINEITEM_IDS,child.Id);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_IS_DIALOG,'TRUE');
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_HIDE_SUBMIT_WITH_ATTACHMENTS,'TRUE');
        test.startTest();
        CustomQuoteApprovalsController ctrl = new CustomQuoteApprovalsController();
        ctrl.setInSf1Mode(True);
        ctrl.getInSf1Mode();
        ctrl.getPageLoaded();
        ctrl.getPageURL();
        ctrl.doLaunchSObjectApprovals();
        ctrl.getReturnLabel();
        ctrl.getFieldSetName();
        ctrl.getShowHeader();
        ctrl.getShowSidebar();
        Apttus_Approval.SObjectApprovalContextParam2 aas=ctrl.getContextInfo();
        ctrl.setContextInfo(aas);
        ctrl.getCanCancel();
        ctrl.getCanSubmit();
        ctrl.doReturn();
        ctrl.doSubmit();
        ctrl.doSubmitWithAttachments();
        ctrl.getHideSubmitWithAttachments();
        ctrl.doCancel();
        ctrl.doReturnToCaller();
        ctrl.getHideSubmitWithAttachments();

        opp.Apttus_Approval__Approval_Status__c = 'Approved';
        update opp;

        test.stopTest();

    }

}