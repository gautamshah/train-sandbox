/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RemoteCPQAdminController {
    global RemoteCPQAdminController() {

    }
    global RemoteCPQAdminController(ApexPages.StandardController stdController) {

    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.ProductAttributeResponseDO associateAttributeGroup(Apttus_Config2.CPQAdminStruct.ProductAttributeRequestDO prodAttrRequestDO) {
        return null;
    }
    @RemoteAction
    global static Boolean associateProductToCategory(Apttus_Config2.CPQAdminStruct2.ProductRequestDO2 productRequestDO) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.HierarchyResponseDO buildHierarchy(Apttus_Config2.CPQAdminStruct.HierarchyRequestDO hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.ProductAttributeResponseDO createAttributeGroup(Apttus_Config2.CPQAdminStruct.ProductAttributeRequestDO prodAttrRequestDO) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.CategoryResponseDO createCategories(Apttus_Config2.CPQAdminStruct.CategoryRequestDO categoryRequestDO) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2__PriceListItem__c> createPriceListItems(List<Apttus_Config2__PriceListItem__c> priceListItemSOs) {
        return null;
    }
    @RemoteAction
    global static List<Product2> createProducts(List<Product2> productsToCreate) {
        return null;
    }
    @RemoteAction
    global static Boolean deletePriceListItems(List<Id> priceListItemIds) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.AttributeGroupRequestDO getAllAttributes(Integer numberOfAttributeGroupsPerChunk) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.GetOptionGroupResponseDO getAllOptionGroups(Apttus_Config2.CPQAdminStruct2.GetOptionGroupRequestDO request) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2__ProductOptionGroup__c> getAllOptionGroups() {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.PriceListDO> getAllPriceLists() {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.ProductGroupDO> getAllProductGroups() {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.GetProductResponseDO getAllProducts(Apttus_Config2.CPQAdminStruct2.GetProductRequestDO request) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.AttributeGroupDO> getAssociatedAttributes(Id productId) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.CategoryNodeDO> getAssociatedCategories(Id productId) {
        return null;
    }
    @RemoteAction
    global static List<Id> getAssociatedCategoryIds(Id productId) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.FeatureSetDO> getAssociatedFeatures(Id productId) {
        return null;
    }
    @RemoteAction
    global static List<Attachment> getAttachmentsByParentId(List<Id> parentIds) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.HierarchyResponseDO getChildOptionGroups(Apttus_Config2.CPQAdminStruct.HierarchyRequestDO hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.HierarchyResponseDO getChildOptions(Apttus_Config2.CPQAdminStruct.HierarchyRequestDO hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.GetCategoryResponseDO getFullCatalogHierarchy(Apttus_Config2.CPQAdminStruct2.GetCategoryRequestDO request, Boolean isPaginated) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.CategoryNodeDO> getFullCatalogHierarchy(Id priceListId) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.ProductOptionGroupDO getOptionGroupDetails(Id productOptionGroupId) {
        return null;
    }
    @RemoteAction
    global static List<Product2> getParentBundles(Id hierarhcyId) {
        return null;
    }
    @RemoteAction
    global static List<Id> getParentProducts(Id productId) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.PricingMetadataDO getPriceListItemPicklistMetadata() {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.PriceListDO> getPriceListItems(Id productId) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct2.ProductRecordDetailsDO getProductDetails(Id productId, String fieldSetName) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2.CPQAdminStruct2.FieldDO> getProductDisplayColumns(String fieldSetName) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2__Footnote__c> getProductFootnotes(Id productId) {
        return null;
    }
    @RemoteAction
    global static List<Id> getProductIds(String productName) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2__ProductInformation__c> getProductInformation(Id productId) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.HierarchyResponseDO getProductStructure(Apttus_Config2.CPQAdminStruct.HierarchyRequestDO hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static List<Product2> getProductsAssociatedToCategory(Id categoryId) {
        return null;
    }
    @RemoteAction
    global static List<Apttus_Config2__RelatedProduct__c> getRelatedProducts(Id productId) {
        return null;
    }
    global static String getSampleAttachmentId() {
        return null;
    }
    global static Map<String,Object> getSystemSettings() {
        return null;
    }
    @RemoteAction
    global static Boolean isProductInHierarchy(Apttus_Config2.CPQAdminStruct2.HierarchyRequestDO2 hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static Boolean removeAttachments(List<Id> attachmentIds) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.ProductAttributeResponseDO removeAttributeGroup(Apttus_Config2.CPQAdminStruct.ProductAttributeRequestDO prodAttrRequestDO) {
        return null;
    }
    @RemoteAction
    global static Boolean removeOptionGroupsFromProduct(List<Id> productOptionGroupIds) {
        return null;
    }
    @RemoteAction
    global static Boolean removeProductFromCategory(Apttus_Config2.CPQAdminStruct2.ProductRequestDO2 productRequestDO) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.HierarchyResponseDO removeProductFromHierarchy(Apttus_Config2.CPQAdminStruct.HierarchyRequestDO hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static Boolean removeProductsFromOptionGroups(Apttus_Config2.CPQAdminStruct2.HierarchyRequestDO2 hierarchyRequestDO) {
        return null;
    }
    @RemoteAction
    global static Boolean setOptionGroupDetails(List<Apttus_Config2.CPQAdminStruct2.ProductOptionGroupDO> productOptionGroupDOs) {
        return null;
    }
    @RemoteAction
    global static Boolean updateAttachments(List<Apttus_Config2.CPQAdminStruct2.AttachmentRequestDO> attachmentRequestDOs) {
        return null;
    }
    @RemoteAction
    global static Apttus_Config2.CPQAdminStruct.ProductAttributeResponseDO updateAttributeGroup(Apttus_Config2.CPQAdminStruct.ProductAttributeRequestDO prodAttrRequestDO) {
        return null;
    }
    @RemoteAction
    global static Boolean updatePriceListItems(List<Apttus_Config2__PriceListItem__c> priceListItemSOs) {
        return null;
    }
    @RemoteAction
    global static Boolean updateProducts(List<Product2> productsToUpdate) {
        return null;
    }
    @RemoteAction
    global static List<Id> uploadAttachments(List<Apttus_Config2.CPQAdminStruct2.AttachmentRequestDO> attachmentRequestDOs) {
        return null;
    }
}
