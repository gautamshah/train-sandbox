@isTest
public class cpqParticipatingFacility_LineItem_uTest {
	
        static Map<Id, Id> proposalId2MasterAgreementId;
        static List<ERP_Account__c> erpAccountList;
        static List<Apttus_Proposal__Proposal__c> proposalList;
        static List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemList;
        static List<Apttus__APTS_Agreement__c> agreementList;
        static List<cpqParticipatingFacility_Lineitem__c> facilityLineItems;
    
    private static Participating_Facility_Address__c setup()
    {
    	Participating_Facility_Address__c mockFacility = cpqParticipatingFacilityAddress_cTest.setup();
    	upsert mockFacility;
    	
    	proposalId2MasterAgreementId = new Map<Id,Id>();
    	erpAccountList = new List<ERP_Account__c>();
    	proposalList = new List<Apttus_Proposal__Proposal__c>();
    	proposalLineItemList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
    	agreementList = new List<Apttus__APTS_Agreement__c>();
    	facilityLineItems = new List<cpqParticipatingFacility_Lineitem__c>();
    	
        for (integer i = 0; i < 4; i++) 
        {
          ERP_Account__c erpRec = new ERP_Account__c();
          erpAccountList.add(erpRec);

        }
        insert erpAccountList;
      
        for (integer i = 0; i < 4; i++) 
        {
          Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c();   
          proposalList.add(proposal);
        }
        insert proposalList;
      
        for (integer i = 0; i < 4; i++) 
        {
	        Apttus_Proposal__Proposal_Line_Item__c proposalLineItem 
	            = new Apttus_Proposal__Proposal_Line_Item__c(Apttus_Proposal__Proposal__c = proposalList.get(i).ID,
	                                                    Apttus_QPConfig__ChargeType__c = 'Hardware');
           proposalLineItemList.add(proposalLineItem);
        }
        insert proposalLineItemList;
      
        for (integer i = 0; i < 4; i++) 
        {
          Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
          agreementList.add(agreement);
        }
        insert agreementList;
      
        for (integer i = 0; i < 4; i++) 
        {
          integer QuantityToBePlacedAtShipTo = 10 + i;
          if (i == 2) 
          {
        	  QuantityToBePlacedAtShipTo *= -1;
          }
          //mockFacility.Proposal__c = proposalList.get(i).ID;
          //ONLY give this a proposal (not an agreement; that is what we will test).
          cpqParticipatingFacility_Lineitem__c partFacLineItem 
            = new cpqParticipatingFacility_Lineitem__c(Proposal__c = proposalList.get(i).ID, 
                                                       ProposalLineItem__c = proposalLineItemList.get(i).ID,
                                                       ERP_Record__c = erpAccountList.get(i).ID,
                                                       ERPRecord__c = erpAccountList.get(i).ID,
                                                       QuantityToBePlacedAtShipTo__c = QuantityToBePlacedAtShipTo,
                                                       ParticipatingFacilityAddress__c = mockFacility.ID);
          facilityLineItems.add(partFacLineItem);
          proposalId2MasterAgreementId.put(proposalList.get(i).ID, agreementList.get(i).ID);
            
        }
        ////insert facilityLineItems;
        return mockFacility;
    }
    
   //@isTest 
   static void testSetAgreement()
   {

      cpqParticipatingFacility_LineItem_uTest.setup();
      cpqParticipatingFacility_Lineitem_u.setAgreement(proposalId2MasterAgreementId);
      
      List<cpqParticipatingFacility_Lineitem__c> entries =
            [SELECT Id
                    ,Proposal__c
                    ,Agreement__c
             FROM cpqParticipatingFacility_Lineitem__c
             WHERE Proposal__c IN :proposalId2MasterAgreementId.keySet()];
     
      for(cpqParticipatingFacility_Lineitem__c entry : entries)
      {
      	 ID agreementId = proposalId2MasterAgreementId.get(entry.Proposal__c);
         System.Assert(entry.Agreement__c == agreementId);
      }
   }
   
   @isTest static void testValidate()
   {
   	    Participating_Facility_Address__c mockFacility = cpqParticipatingFacility_LineItem_uTest.setup();
        Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemMap = new Map<Id,Apttus_Proposal__Proposal_Line_Item__c>();
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : proposalLineItemList)
        {
      	  proposalLineItemMap.put(proposalLineItem.ID, proposalLineItem);
        }
      
        Map<Id,Participating_Facility_Address__c> facilitiesMap = new Map<Id,Participating_Facility_Address__c>();
        facilitiesMap.put(mockFacility.ID, mockFacility);
    	
    	Set<String> validations = cpqParticipatingFacility_Lineitem_u.validate(facilityLineItems, proposalLineItemMap, facilitiesMap);
    	System.debug('HCN:testValidate:validations size:: ' + validations.size());
    	for (String validation : validations)
    	{
    		System.debug('HCN:validation: ' + validation);
    	}
    	
   }
}