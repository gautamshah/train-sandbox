public with sharing class dmAttachments
{
	public static Map<Id, Attachment> All { get; set; }
	public static Map<string, Attachment> GUID2Attachment { get; set; }

	static
	{
		List<string> guids = dmDocuments.GUIDs;
		string whereClause = ' WHERE Description IN :guids';
    	string query = dm.buildSOQL(Attachment.getSObjectType(), new Set<string>{ 'lastmodifiedbyid', 'lastmodifieddate' }, whereClause);
    	All = new Map<Id, Attachment>((List<Attachment>)Database.query(query));
	
		GUID2Attachment = new Map<string, Attachment>();
		for (Attachment a : All.values())
		{
			GUID2Attachment.put(a.Description, a);
		}
	}
}