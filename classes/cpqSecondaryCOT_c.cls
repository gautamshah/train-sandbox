/**
Build where predicates for selecting Participating Facilities or Dealers

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-12/06      Henry Noerdlinger   Created
===============================================================================
*/
public class cpqSecondaryCOT_c extends cpqClassOfTrade_c 
{
	  static final String secondaryClassOfTrdeFieldname = 'Secondary_Class_of_Trade__c';
	  cpqPrimaryCOT_c primaryCOTLeftExpression {get; set;}
	
	  public cpqSecondaryCOT_c(String value)
	  {
	  	this(null, value);
	  }
	
	  public cpqSecondaryCOT_c(cpqPrimaryCOT_c left, String value)
	  {
	 		super(value);
	 		primaryCOTLeftExpression = left;
	  }
	  	
	  public override  String buildParticipatingFacilityExpression(String obj)
	  {
            return buildParticipatingFacilityExpression(obj, false);
	  }
	  
	 public String buildParticipatingFacilityExpression(String obj, boolean forDistributor)
	 {
		      String expression = ' ( ';
       
        if (primaryCOTLeftExpression != null)
        {
          //primary needs to be flipped
          expression += primaryCOTLeftExpression.inverse(obj, forDistributor) + ' AND '; 
        }
        
        if (obj != null)
        {
           expression += obj + '.';
        }
      
        expression += secondaryClassOfTrdeFieldname;
        
          if (exclude)
          {
             expression += ' !';
          }
          expression += ' = \'' + value + '\'';

        return expression + ' ) ';
	 }

	
	  /**
	   *  In the example where Participating Facility should either primay_cot *not* be 003 OR primary_cot is 003 and seconcary_cot is 07h
	   *  
	   *  According to requirements, we need to take the inverse of this, which means to negate groups of expressions and flip AND and OR
	   *  So, then if participating facilities is configured:
	   *  -003
	   *  +07h
	   *  
	   *  Let P = 003 and S = 07h
	   *  
	   *  we should have ( (!P) OR (P AND S))
	   *  
	   *  And to negate this for distributor, according to Demorgan's theorum, we should have
	   *  ( (P) AND !(P AND S))
	   *  
	   *  This will not cover all possible permutations, but it should cover our immediate need and potentially a few similar cases.
	   *  
	  */
	  public override String buildDistributorExpression(String obj)
	  {
	  	  //if we have a standalone primary class filter set, we need to negate the combined expression formed from
	  	  //that and the secondary.
		  if (primaryCOTLeftExpression != null)
	      {
            String expr = buildParticipatingFacilityExpression(obj, true).replaceFirst(' \\(', ' \\( NOT \\(');
            return expr += ')';
	      }
	      else
	  	  {
			  if (exclude) 
			  {
			  	 return buildParticipatingFacilityExpression(obj, true).replaceAll('!', '');
			  }
			  else
			  {
				return buildParticipatingFacilityExpression(obj, true).replaceAll(' = ', ' != ');
	
			  }
	  	  }
	  
	  }
}