@isTest
public class cpqVariable_cTest
{
    static map<string, string> testValues = new map<string, string>{
        'A' => 'a',
        'B' => 'b',
        'List' => 'a|b|c|d',
        'Duplicates' => 'a|a|a'
    };
    
    @testSetup()
    static void setup()
    {
        List<CPQ_Variable__c> variables = new List<CPQ_Variable__c>();
        
        for(string key : testValues.keySet())
            variables.add(new CPQ_Variable__c(Name = key, Value__c = testValues.get(key)));
            
        insert variables;
        
        system.debug(variables);
    }
    
    @isTest
    static void fetch()
    {
        system.assertEquals('a', cpqVariable_c.getValue('A'), 'The letter A should return the letter a');
    }
    
    @isTest
    static void fetchNull()
    {
        system.assertEquals(null, cpqVariable_c.getValue(null), 'Passing in null returns null');
    }
    
    @isTest
    static void fetchWithSeparator()
    {
        system.assertEquals(new Set<string>{ 'a' }, cpqVariable_c.getValue('Duplicates', '\\|'), 'The name Duplicates should return 1 letter a');
        system.assertEquals(new Set<string>{ 'a', 'b', 'c', 'd' }, cpqVariable_c.getValue('List', '\\|'), 'The name List should return a set of values');
        system.assertEquals(new Set<string>{ testValues.get('List') }, cpqVariable_c.getValue('List', ''), 'A blank separator should return a a Set<string> containging the original string');
    }
    
    @isTest
    static void fetchNullSeparator()
    {
        system.assertEquals(null, cpqVariable_c.getValue(null, '\\|'), 'Passing in null returns null');
        system.assertEquals(new Set<string>{ testValues.get('List') }, cpqVariable_c.getValue('List', null), 'A null value for the separator will return a Set<string> containging the original string');
    }
    
    @isTest
    static void fetchWrongSeparator()
    {
        system.assertEquals(new Set<string>{ 'a|b|c|d' }, cpqVariable_c.getValue('List', ':'), 'Using the wrong separator should return the value without it being split');
    }
}