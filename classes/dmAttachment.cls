public with sharing class dmAttachment extends dmDocument
{
    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<Attachment> newList,
            Map<Id, Attachment> newMap,
            List<Attachment> oldList,
            Map<Id, Attachment> oldMap,
            integer size)
    { 
    	system.debug('dmAttachment: ' + newList);
    	
        // This section of code should only run if the Parent Object
        // the Attachment was created for is a myDocument__c object
        if (isBefore)
        {
            if (isInsert || isUpdate)
            {
	            List<Attachment> filteredList = SelectByKeyPrefix(newList, dmDocument.keyPrefix);
                    
    	        if (filteredList.size() == 0) return; // There's nothing to process
        
				if (containsMoreThan1AttachmentForParent(filteredList)) return;
				
                attach(filteredList);
            }
        }
    }
    
    public dmAttachment(ApexPages.StandardSetController setcontroller)
    {
        super(setcontroller);
    }
    
    public dmAttachment(ApexPages.StandardController controller)
    {
        super(controller);
    }
    
    private static boolean containsMoreThan1AttachmentForParent(List<Attachment> newList)
    {
    	Map<Id, integer> numberOfAttachmentsForParent = new Map<Id, integer>();
    	for(Attachment a : newList)
    	{
    		if (!numberOfAttachmentsForParent.containsKey(a.ParentId))
    			numberOfAttachmentsForParent.put(a.ParentId, 1);
    		else
    		{
    			a.addError('You can only attach 1 document for this object');
    			return true;
    		}
    	}
    			
    	return false;
    }
    
    private static List<Attachment> SelectByKeyPrefix(List<Attachment> aList, string keyPrefix)
    {
        List<Attachment> target = new List<Attachment>();
        
    	if (aList != null)
		{
	        for(Attachment a : aList)
	        	if(a.ParentId != null)
    	        	if (string.valueOf(a.ParentId).startsWith(keyPrefix))
        	        	target.add(a);
		}  	

        return target;
    }
    
    private static Map<Id, Attachment> SelectByKeyPrefix(Map<Id, Attachment> aMap, string keyPrefix)
    {
        Map<Id, Attachment> target = new Map<Id, Attachment>();

		if (aMap != null)
		{
        	for(Attachment a : aMap.values())
	        	if(a.ParentId != null)
	            	if (string.valueOf(a.ParentId).startsWith(keyPrefix))
    	            	target.put(a.Id, a);
		}
		       
        return target;
    }

    public PageReference onLoadAttachment()
    {
        if (dm.querystring.get('sObject') != null)
            return dmAttachment.view(dm.querystring.get('sObject'));
        else
        {
            if (record != null && record.Id != null)
                record = dmDocuments.All.get(record.Id);
                
            return null;
        } 
    }

    // Related Attachment methods
    // NOTE: Use the private Attachments method to retrieve values when possible
    //       It contains the != NULL, which will avoid issues if the description being
    //       searched for is blank
    public Attachment Attachment
    {
    	get
    	{
    		return fetch(record.GUID__c);
    	}
    }
    
    public Attachment fetchAttachment()
    {
    	return fetch(record.GUID__c);
    }
   
	public static Attachment fetch(string guid)
	{
		return dmAttachments.GUID2Attachment.get(guid);
	}
	
    public static List<Attachment> fetch(List<string> guids)
    {
    	List<Attachment> targets = new List<Attachment>();
    	
    	for (string guid : guids)
    		targets.add(dmAttachments.GUID2Attachment.get(guid));
    		
        return targets;
    }
    
    private static void attach(List<Attachment> newList)
    {
		List<dmDocument__c> parents = new List<dmDocument__c>();
		
    	for(Attachment a : newList)
    	{
    		if (dmDocuments.All.containsKey(a.ParentId))
    		{
    			dmDocument__c parent = dmDocuments.All.get(a.ParentId);
    			// parent.Name = a.Name; // Victoria requested that the Document (dm) Name not change to the uploaded document name
    			a.Description = parent.GUID__c;
    		}
    	}
    }
    
    public void removeAttachment()
    {
    	remove(record.GUID__c);
    }
    
    public static void remove(string guid)
    {
    	remove(new List<string>{ guid });
    }
    
    public static void remove(List<string> guids)
    {
    	List<Attachment> found = fetch(guids);
    	
    	// Victoria requested that the Document (dm) Name not be updated
    	//List<Id> parentIds = new List<Id>();
    	//for (Attachment a : found)
    	//	parentIds.add(a.ParentId);
    		
    	//List<dmDocument__c> parents =
    	//		[SELECT Name
    	//	 	 FROM dmDocument__c
    	//	 	 WHERE Id IN :parentIds];
    		 	 
    	//for (dmDocument__c parent : parents)
    	//	parent.Name = '<--- Please upload the file for this document --->';
    		
    	//update parents;
    		 	 
        delete found;
    }

    public static PageReference view()
    {
        return view(dm.querystring.get('sObject'));
    }
    
    public static PageReference view(string guid)
    {
    	Attachment a = fetch(guid);
    	
    	if (a != null)
    		return new PageReference('/servlet/servlet.FileDownload?file=' + a.Id);
    	else
    		return null;
    }
}