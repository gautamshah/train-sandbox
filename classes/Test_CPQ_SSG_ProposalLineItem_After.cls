@isTest
private class Test_CPQ_SSG_ProposalLineItem_After
{
    @isTest static void Test_CPQ_SSG_ProposalLineItem_After()
    {
		RecordType proposalRT = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,'Agreement_Proposal');

        Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1');
		insert product;
			
            Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
            insert acct;
            
            Opportunity opp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acct.Id);
            insert opp;
            
			Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', Amendment_Sequence__c = 1, Apttus_Proposal__Opportunity__c = opp.Id, Apttus_Proposal__Approval_Stage__c = 'Draft', Apttus_Proposal__Account__c = acct.Id, RecordTypeId = proposalRT.Id, Apttus_QPApprov__Approval_Status__c = 'Pending Approval', Type_SSG__c = 'New Agreement', Product_Line__c = 'Access Only', Accessing_Primary_GPO_Agreement__c = 'Yes', Term_Months__c = '12', Compliance__c = 'Revenue Commitment', Compliance_Measurement_For_IHN__c = 'Member Level', Sub_Agreement_Needed__c = 'No', Document_Type__c = 'Letter of Intent', Price_Increase__c = 'Fixed', Price_Increase_Detail__c = '1 % per Year', Revenue_Commitment__c = 1000.0);
            insert proposal;
            
                Apttus_Config2__ProductConfiguration__c config = new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = proposal.Id, Apttus_Config2__Status__c = 'Ready for Finalization', Apttus_Config2__BusinessObjectId__c = proposal.Id, Apttus_Config2__BusinessObjectType__c = 'Proposal', Apttus_Config2__VersionNumber__c = 1);
                insert config;
                Apttus_Config2__LineItem__c lineItem = new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = config.id, Apttus_Config2__ProductId__c = product.Id, Apttus_Config2__Quantity__c = 1, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1, Apttus_Config2__LineType__c = 'Product', Apttus_Config2__NetPrice__c = 12345.0);
                insert lineItem;
                CPQ_SSG_QP_Class_of_Trade__c qpCot = new CPQ_SSG_QP_Class_of_Trade__c(Quote_Proposal__c = proposal.id);
                insert qpCot;
                CPQ_SSG_QP_Product_Pricing__c productPricing = new CPQ_SSG_QP_Product_Pricing__c(CPQ_SSG_QP_Class_of_Trade__c = qpCot.Id, Price__c = 1.0, Product__c = product.Id, Proposal_Product_Combined_Key_Text__c = proposal.Id + '_' + product.Id);
                insert productPricing;
		Apttus_Proposal__Proposal_Line_Item__c pli = new Apttus_Proposal__Proposal_Line_Item__c(Apttus_Proposal__Proposal__c = proposal.Id, Apttus_QPConfig__ConfigurationId__c = config.Id, Apttus_Proposal__Product__c = product.Id, Apttus_QPConfig__NetPrice__c = 12345.0);

		Test.startTest();
			insert pli;
			productPricing = [Select Id, Price__c From CPQ_SSG_QP_Product_Pricing__c Where Id = :productPricing.Id];
			System.assertEquals(12345.0, productPricing.Price__c);
		Test.stopTest();
	}
}