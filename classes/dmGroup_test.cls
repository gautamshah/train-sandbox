@isTest
public class dmGroup_test
{
	@isTest
	public static void constructors()
	{
		dmGroup g = new dmGroup();
		
		dmGroup__c sg = new dmGroup__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(sg);
		g = new dmGroup(sc);
		
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<dmGroup__c>{ sg });
		g = new dmGroup(ssc);
	}
	
	@isTest static void record()
	{
		Id a = dm_test.createApplication();
		dmGroup__c g = dm_test.createdmGroup(a);
		dmGroup dm = new dmGroup();
		dm.record = g;
		dmGroup__c r = dm.record;
	}
	
	@isTest static void sobj()
	{
		Id a = dm_test.createApplication();
		dmGroup__c g = dm_test.createdmGroup(a);
		dmGroup dm = new dmGroup();
		dm.record = g;
		Group sobj = dm.sobj;
	}

	@isTest
	public static void triggercode()
	{
		Id a = dm_test.createApplication();
		dmGroup__c g = dm_test.createdmGroup(a);
		dmGroup dmg = new dmGroup();
		dmg.selected.objectId = g.Id;
		dmg.selected.sobjectid = g.Id;
		dmg.selected.assigned = true;
		dmg.record = g;
		dmg.AssignGroup();

		dm_test.updatedmGroup(g);
		dm_test.deletedmGroup(g);
	}
	
	@isTest
	public static void sharingmethods()
	{
		Id a = dm_test.createApplication();
		dmGroup.deleteSharing();
		dmGroup.fixSharing();
	}

	@isTest
	public static void createObjectShareRecord()
	{
		Id a = dm_test.createApplication();
		Id f = dm_test.createSubFolder(a);
		dmGroup dm = new dmGroup();

		dmGroup__c g = dm_test.createdmGroup(a);

		sObject share = dm.createObjectShareRecord(f, g.Id);
	}
	
	@isTest
	public static void someProperties()
	{
		Id a = dm_test.createApplication();
		Id f = dm_test.createSubFolder(a);
		dmGroup__c g = dm_test.createdmGroup(a);
		dmGroup__c g2 = dm_test.createdmGroup(a);
		dmGroup dm = new dmGroup();
		dmGroup dm2 = new dmGroup();
		dm.record = g;
		dm2.record = g2;
				
		Id aid = dm.ApplicationId;
		
		List<dmGroup> groups = new List<dmGroup>();
		groups.add(dm);
		groups.add(dm2);
		groups.sort();
		dm.record.Name = 'a';
		dm2.record.Name = 'b';
		groups.sort();
		dm.record.Name = 'b';
		dm2.record.Name = 'a';
		groups.sort();
		dm.record.isAdministrator__c = true;
		groups.sort();
		
		boolean result = dm.assigned;
		result = dm.locked;
		
		//sObject share = dm.createObjectShareRecord(f, dm.sobj.Id);
		//insert share;
	}
	
	@isTest
	public static void RelatedGroups()
	{
		Id a = dm_test.createApplication();
		Id f = dm_test.createSubFolder(a);
		Id cae = dm_test.createGroup(a, 'CAE');
		Id rbm = dm_test.createGroup(a, 'RBM');
		Set<Id> groupIds = new Set<Id>();
		groupIds.add(cae);
		groupIds.add(rbm);
		
		List<dmGroup__c> groups = dmGroup.fetch(groupIds); 

		dmGroup dm = new dmGroup();
		dm.parameters.objectId = cae;
		dm.parameters.applicationId = a;

		List<dmGroup> related = dm.RelatedGroups;
		
		Id d = dm_test.createDocument(f);
		
		dm.selected.assigned = false;
		dm.selected.objectId = d;
		dm.selected.sobjectid = related[0].sobj.id;
		
		dm.AssignGroup();
		
		dmGroup__c ag = dmGroup.fetch(related[0].sobj.id);
	}
	
	@isTest
	public static void fetchApplicationId2Objects()
	{
		Id a = dm_test.createApplication();
		Id f = dm_test.createSubFolder(a);
		Id cae = dm_test.createGroup(a, 'CAE');
		Id rbm = dm_test.createGroup(a, 'RBM');

		List<sObject> sobjs = new List<sObject>();
		dmFolder__c app = dmFolder.ApplicationFolder(a);
		sobjs.add(app);
		
		Map<Id, List<dmGroup>> results = dmGroup.fetchApplicationId2Objects(sobjs);
	}
	
	@isTest
	public static void vf1()
	{
		Id a = dm_test.createApplication();
		dmGroup__c dmg = dm_test.createdmGroup(a);
		
		ApexPages.StandardController sc = new ApexPages.StandardController(dmg);
		dmGroup g = new dmGroup(sc);
		
		PageReference pageRef = Page.dmGroup; 
		pageRef.getParameters().put('id', string.valueOf(dmg));
		Test.setCurrentPage(pageRef);
		g.onload();
	}
}