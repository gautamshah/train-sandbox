public class UserTerritory_c extends sObject_c
{
	public static set<string> fieldsToInclude =
		new set<string>
		{
			'Id',
			'UserId',
			'TerritoryId'
		};
}