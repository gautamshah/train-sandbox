public class KitDetailsVFTemplateController {
    
    public Kit__c kitRecord{get;set;}
    
    public KitDetailsVFTemplateController(ApexPages.StandardController controller) {
        
        kitRecord=(Kit__c)controller.getRecord();
    }

}