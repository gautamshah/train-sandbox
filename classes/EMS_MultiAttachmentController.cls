/*
 * Controller for multi attachment component
 */
 
public with sharing class EMS_MultiAttachmentController 
{
    // the parent object it
    public Id sobjId {get; set;}
    public String sourceflowUrl{get;set;}
    public EMS_Event__c currentEMSRecord{get;set;}
    public Id AttId{get;set;}
    
    // list of existing attachments - populated on demand
    public List<Attachment> attachments;
     public List<Attachment> Legalattachments;
    
    // list of new attachments to add
    public List<Attachment> newAttachments {get; set;}
    public List<Attachment> LegalDocuments {get;set;}
    public string chosenstep{get;set;}

    
    // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_ADD=5;

    // constructor
    public EMS_MultiAttachmentController ()
    {
        // instantiate the list with a single attachment
        newAttachments=new List<Attachment>{new Attachment()};
        LegalDocuments = new List<Attachment>{new Attachment()};
       
        LChecklistDocs = new Set<String>();
        // Input record Id from the Page
        sObjId= ApexPages.currentPage().getParameters().get('EventId');
        chosenstep=ApexPages.currentPage().getParameters().get('step');
        sourceflowUrl='/apex/EMS_VFProcessflow?EventId='+sObjId+'&step='+chosenstep;
        currentEMSRecord = [Select Id,Name,Event_Status__c,Event_Id__c ,Booth_Space__c,Owner_Country__c,No_of_Completed_eCPAs__c ,Workshop_conducted_by_Covidien_sales__c,Advertisement__c,Track_count_of_legal_documents__c,
                                   Stand_alone_gift__c,Training_conducted_by_HCPs__c,CPA_Status__c,Track_number_for_Notifying_Legal__c,Education_Grant__c,CEP__c,ISR_CSR__c,Speaker_or_other_consulting_services__c,
                                   Any_benefits_involving_govt_officials__c  
                                   from EMS_Event__c where id=:sObjId limit 1];

    }  
    
    public Set<String> LChecklistDocs{
    get{
    if (currentEMSRecord.Booth_Space__c){
    
        LChecklistDocs.add('Official brochure/meeting agenda');
        LChecklistDocs.add('Exhibition layout');
        
    }
    if(currentEMSRecord.Workshop_conducted_by_Covidien_sales__c){
        LChecklistDocs.add('Workshop agenda/brochure');
        LChecklistDocs.add('HCP activities list/itinerary');
    }
    if(currentEMSRecord.Advertisement__c){
        LChecklistDocs.add('Final product of advertising material attached');        
    }
    if(currentEMSRecord.Stand_alone_gift__c){
        LChecklistDocs.add('Quotation and description');
    }
    if(currentEMSRecord.Training_conducted_by_HCPs__c){
        LChecklistDocs.add('Training agenda');
        LChecklistDocs.add('HP activity list/itinerary');
    }
    if(currentEMSRecord.Education_Grant__c){
        LChecklistDocs.add('Official proposal from requestor');
        LChecklistDocs.add('CV of HCP');
        LChecklistDocs.add('DRAFT sponsorship agreement/letter');
        LChecklistDocs.add('FINAL sponsorship agreement/letter *Legal use only:');
    }
    if(currentEMSRecord.CEP__c){
        LChecklistDocs.add('Official proposal from requestor');
        LChecklistDocs.add('CV of HCP');
        LChecklistDocs.add('DRAFT sponsorship agreement/letter');
        LChecklistDocs.add('FINAL sponsorship agreement/letter *Legal use only:');
    }
    if(currentEMSRecord.ISR_CSR__c){
        LChecklistDocs.add('Official proposal from requestor');
        LChecklistDocs.add('CV of HCP');
        LChecklistDocs.add('DRAFT sponsorship agreement/letter');
        LChecklistDocs.add('FINAL sponsorship agreement/letter *Legal use only:');
    }
    if(currentEMSRecord.Speaker_or_other_consulting_services__c){
        LChecklistDocs.add('CV of speaker');
        LChecklistDocs.add('DRAFT consulting agreement');
        LChecklistDocs.add('FINAL consulting agreement *Legal use only:');
       
    }
    if(currentEMSRecord.Any_benefits_involving_govt_officials__c  ){
        LChecklistDocs.add('Official request');
        LChecklistDocs.add('Official document detailing purpose of event');
        
    }
    
    
    return LChecklistDocs;
    }
    set;    
    }
    


    
    // retrieve the existing attachments
    public List<Attachment> getAttachments()
    {
        // only execute the SOQL if the list hasn't been initialised
        if (null==attachments)
        {
            attachments=[select Id, ParentId, Name, Description,createdDate from Attachment where parentId=:sobjId];
        }
        
        return attachments;
    }
    public List<Attachment> getLegalAttachments()
    {
         Map<Id,Event_Document__c> MEventDOcs = new Map<Id,Event_Document__c>([Select Id,EMS_Event__c from Event_Document__c where EMS_Event__c=:currentEMSRecord.Id]);
        // only execute the SOQL if the list hasn't been initialised
        if (null==LegalAttachments && !MEventDOcs.isEmpty())
        {
            
            LegalAttachments=[select Id,ParentId,Name,Description from Attachment where parentId in :(MEventDOcs.keyset())];
        }
        
        return LegalAttachments;
    }
    
    public Pagereference Back(){
    
    Pagereference P = new Pagereference('/'+sObjId);
    P.setredirect(true);
    return P;
    
    
    }
    // Add more attachments action method
    public void addMore()
    {
        save();        
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            newAttachments.add(new Attachment());
        }
    } 
    
    public void addMoreLegal()
    {
        savelegal();
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            LegalDocuments.add(new Attachment());
        }
    }     
    
    // Save action method
    public void save()
    {
        List<Attachment> toInsert=new List<Attachment>();
        for (Attachment newAtt : newAttachments)
        {
            if (newAtt.Body!=null)
            {
                newAtt.parentId=sobjId;
                toInsert.add(newAtt);
            }
        }
        insert toInsert;
        newAttachments.clear();
        newAttachments.add(new Attachment());
        
        // null the list of existing attachments - this will be rebuilt when the page is refreshed
        attachments=null;
    }
    
    
    //Action to save legal documents
    
    public void saveLegal()
    {
        save();
        List<Attachment> toInsert=new List<Attachment>();
        //Integer i=1;
        database.saveresult[] sr;
        List<Event_Document__c> LEvntDocs = new List<Event_Document__c>();
        for(Attachment c : LegalDocuments){
        if(c.body!=null){
        Event_Document__c eachEventDoc = new Event_Document__c(EMS_Event__c =sobjId);
        LEvntDocs.add(eachEventDoc );
        }
        System.debug('<<<<<<'+LEvntDocs);
        
        }
        sr=database.insert(LEvntDocs,false);
        System.debug('+++++++++'+sr);
        Map<Integer,Id> MSnoIds = new Map<Integer,Id>();
        Integer i=1;
        for (Database.SaveResult eachsr : sr){
            MSnoIds.put(i,eachsr.getId());
            i=i+1;
        }
        i=1;
        system.debug('------->'+MSnoIds);
        for (Attachment newAtt : LegalDocuments)
        {
            if (newAtt.Body!=null)
            {
                newAtt.parentId=MSnoIds.get(i);
                newAtt.isprivate=false;
                toInsert.add(newAtt);
                i=i+1;
            }
        }
        insert toInsert;
        currentEMSRecord.Track_count_of_legal_documents__c=(currentEMSRecord.Track_count_of_legal_documents__c!=null?(currentEMSRecord.Track_count_of_legal_documents__c+1):1);
        update currentEMSRecord;
        Legaldocuments.clear();
        legaldocuments.add(new Attachment());
        
        // null the list of existing attachments - this will be rebuilt when the page is refreshed
        attachments=null;
    }
    
    public Pagereference NotifyLegal(){
    
    currentEMSRecord.Track_number_for_Notifying_Legal__c=(currentEMSRecord.Track_number_for_Notifying_Legal__c!=null?(currentEMSRecord.Track_number_for_Notifying_Legal__c+1):1);
    update currentEMSRecord;
    return null;
    
    }
    public PageReference eCPAPreview(){
    
    Pagereference P = new Pagereference('/apex/EMS_CPAForm?EventId='+CurrentEMSRecord.Id+'&IsPreview=1');
    P.setredirect(true);
    return P;
    
    
    }
    
    
    //Delete Attachment
    
    Public Pagereference DeleteAttachment(){
    
    database.Delete(AttId);
    Pagereference P= new Pagereference('/apex/EMS_OverrideAddAttachmentButton?EventId='+currentEMSRecord.Id+'&step=6');
    P.setredirect(true);
    return P;
    }
    // Action method when the user is done
    public PageReference done()
    {
        // send the user to the detail page for the sobject
        return new PageReference('/' + sobjId);
    }
}