/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161210  A    BF         AV-240     Create new Proposal for Cancelled Agreement - Created page to reopen proposal

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/    
public with sharing class cpqProposalReopen_v {

	public Apttus_Proposal__Proposal__c proposal {get;set;}
	public Apttus__APTS_Agreement__c agreement {get;set;}

	public cpqProposalReopen_v(ApexPages.StandardController controller) {
        // Get source proposal record
        proposal = (Apttus_Proposal__Proposal__c)controller.getRecord();
        proposal = [Select Id,
        				   RecordType.DeveloperName
        			From Apttus_Proposal__Proposal__c
        			Where Id = :proposal.Id];
        List<Apttus__APTS_Agreement__c> agreements = 
        			[Select Id,
                			Apttus__Status__c,
        					Apttus_QPComply__RelatedProposalId__c,
        					Parent_Proposal_Cancelled__c,
        					AgreementID__c
        			 From Apttus__APTS_Agreement__c
        			 Where Apttus_QPComply__RelatedProposalId__c = :proposal.Id
        			 Order By CreatedDate Desc
        			 Limit 1];
        if (agreements != null && !agreements.isEmpty()) {
        	agreement = agreements[0];
        }
	}

	public PageReference reopenProposal() {
		if (agreement == null) {
			return new PageReference('/' + proposal.Id);
		}
		if (agreement.Apttus__Status__c != cpqAgreement_c.convert(cpqAgreement_c.StatusIndicators.Activated)) {
			cpqAgreement_c.cancel(agreement);
			cpqProposal_c.unlock(proposal);
			return new PageReference('/' + proposal.Id);
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
				'The proposal cannot be reopened because the associated Agreement has already been activated.');
			ApexPages.addMessage(msg);
			return null;
		}
	}

	public PageReference returnToProposal() {
		return new PageReference('/' + proposal.Id);
	}
}