/****************************************************************************************
 * Name    : Event_TriggerHandler 
 * Author  : Tejas Kardile
 * Date    : 13-Jan-2014 
 * Purpose : Contains the logic coming from Event trigger
 * Dependencies: UpdateEventLocationField Trigger
 *             , Event Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
public with sharing class Event_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public Event_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
 
    public void OnbeforeInsertUpdate(List<Event> newObjects)
    {
        /*Update location field with contact & Account*/
        Set<ID> setwhoid = new Set<ID>();
        Set<ID> setwhatid = new Set<ID>();
        for (Event t : newObjects) 
        {
            setwhoid.add(t.whoid);
            setwhatid.add(t.whatid);
        }
        Map<id,Contact> mapcontact = new Map<id,Contact>([Select id,Name from contact where Id in : setwhoid]); 
        Map<id,Account> mapaccount = new Map<id,Account>([Select id,Name from Account where Id in : setwhatid]);
        for (Event t : newObjects) 
        {
            String strlocation = '';
            if(mapaccount.containsKey(t.Whatid))
            {
                strlocation = mapaccount.get(t.Whatid).Name;
            }
            if(mapcontact.containsKey(t.whoid))
            {
                if(strlocation != '')
                {
                    strlocation = strlocation + ' - ' + mapcontact.get(t.whoid).Name;
                }
                else
                {
                    strlocation =  mapcontact.get(t.whoid).Name;
                }
            }
            t.location = strlocation;
        }
    }
}