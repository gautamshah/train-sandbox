/****************************************************************************************
 * Name    : CreateOppWithProductsFromPPCExt 
 * Author  : Bill Shan
 * Date    : 16/05/2014 
 * Purpose : Create opportuntiy with products from PPC
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
public with sharing class CreateOppWithProductsFromPPCExt {

	public List<ProductPriceBook> prodPb {get; set;}
	public boolean hasInvalidRecords {get; set;}
	public List<SelectOption> pricebooks {get; set;}
	public Id pricebookId {get; set;}

	private List<Prod_Preference_Card_Product__c> lstPro {get; set;}
	private Map<Id,set<Id>> pro_pbs = new Map<Id,Set<Id>>();
	private Set<Id> prdIds = new Set<Id>(); 
	private Set<Id> pricebookIds = new Set<Id>();
	private Opportunity newOpp = new Opportunity();
	private Set<Id> ppSet = new Set<Id>();
	
	class ProductPriceBook{
		public string product{get;set;}
		public string pricebook{get;set;}
		public ProductPriceBook(string str1, String str2)
		{
			product = str1;
			pricebook = str2;
		}
	}
	
	public CreateOppWithProductsFromPPCExt(ApexPages.StandardSetController controller)
	{
		hasInvalidRecords = false;
		prodPb = new List<ProductPriceBook>();
		pricebooks = new List<SelectOption>();
		for(Prod_Preference_Card_Product__c t : (List<Prod_Preference_Card_Product__c>)controller.getSelected())
		{
			ppSet.add(t.Id);
		}
	}
	
	public PageReference CreateOppFromPPC()
	{		
		if(ppSet.size()==0)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.No_item_selected));
		}
		else
		{
		
			lstPro = [Select Id,Product_Preference_Card__c,Target_Covidien_Product__c,Target_Covidien_Product__r.Name, 
							Product_Preference_Card__r.Procedure__c,Product_Preference_Card__r.Physician__r.Full_Name_with_Salutation__c,
							Product_Preference_Card__r.Physician__c, Product_Preference_Card__r.Physician__r.AccountId 
					  From Prod_Preference_Card_Product__c where Id = :ppSet];
			
			for(Prod_Preference_Card_Product__c pp : lstPro)
			{
				if(pp.Target_Covidien_Product__c != null)
					prdIds.add(pp.Target_Covidien_Product__c);
			}
						
			ID standardPricebookId = [SELECT Id FROM Pricebook2 WHERE IsStandard = true AND IsActive = true limit 1].Id;
			
			for(PricebookEntry pbe : [Select Id, Pricebook2Id, Product2Id, Product2.Name, Pricebook2.Name from PricebookEntry 
									  Where Product2Id = :prdIds and IsActive = true and CurrencyISOCode = :UserInfo.getDefaultCurrency()
									  and Pricebook2Id != :standardPricebookId and Pricebook2.IsActive = true])
			{
				pricebookIds.add(pbe.Pricebook2Id);
				if(pro_pbs.containskey(pbe.Product2Id))
					pro_pbs.get(pbe.Product2Id).add(pbe.Pricebook2Id);
				else
					pro_pbs.put(pbe.Product2Id, new set<Id>{pbe.Pricebook2Id});
					
				prodPb.add(new ProductPriceBook(pbe.Product2.Name,pbe.Pricebook2.name));
			}
			
			
			boolean hasprdwithoutpricebook = false;
			for(Prod_Preference_Card_Product__c pid : lstPro)
			{
				if(pid.Target_Covidien_Product__c!=null && !pro_pbs.containskey(pid.Target_Covidien_Product__c))
				{
					prodPb.add(new ProductPriceBook(pid.Target_Covidien_Product__r.Name,''));
					hasprdwithoutpricebook = true;
				}
			}
			
			if(hasprdwithoutpricebook)
			{
				hasInvalidRecords = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Product_not_in_any_opportunity_product_group));
			}
			
			
			for(Id pbi : pricebookIds)
			{
				for(Id proi : pro_pbs.keyset())
				{
					if(!pro_pbs.get(proi).contains(pbi))
					{
						pricebookIds.remove(pbi);
						break;
					}
						
				}
			}
			
			if(prdIds.size()==0)
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.No_item_selected));
			else if(pricebookIds.size()==0)
			{
				hasInvalidRecords = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Please_choose_products_from_the_same_opportunity_product_group));
			}
			else if(pricebookIds.size()>1 && !hasInvalidRecords)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Please_choose_opportunity_product_group_first));
				
				for(Pricebook2 pb2 : [Select Id, Name From Pricebook2 Where Id = :pricebookIds])
				{
					pricebooks.add(new SelectOption(pb2.Id,pb2.Name));
				}
			}
			else if(pricebookIds.size()==1 && !hasInvalidRecords)
			{
				//create opportunity
				List<Id> temp = new List<Id>();
				temp.addAll(pricebookIds);
				this.pricebookId = temp[0];
				return createOpp();
			}
		}
		return null;
	}
	
	public PageReference createOpp()
	{
		Id contactId = lstPro[0].Product_Preference_Card__r.Physician__c;
		newOpp.AccountId = lstPro[0].Product_Preference_Card__r.Physician__r.AccountId;
		newOpp.CloseDate = date.Today().addMonths(3);
		newOpp.Pricebook2Id = this.pricebookId;
        newOpp.StageName = 'Identify'; 
        newOpp.Related_Product_Preference_Card__c = lstPro[0].Product_Preference_Card__c;
        newOpp.Name = lstPro[0].Product_Preference_Card__r.Physician__r.Full_Name_with_Salutation__c;
        if(lstPro[0].Product_Preference_Card__r.Procedure__c != null)
        newOpp.Name += ' : ' + lstPro[0].Product_Preference_Card__r.Procedure__c;
        newOpp.Related_Product_Preference_Card__c = lstPro[0].Product_Preference_Card__c;
		
		insert newOpp;
		
		List<OpportunityLineItem> NewOppLines = new List<OpportunityLineItem>();
		
		for(PricebookEntry pbe : [Select Id, UnitPrice From PricebookEntry Where Product2Id = :prdIds 
			and CurrencyISOCode = :UserInfo.getDefaultCurrency() and Pricebook2Id = :this.pricebookId])
		{
			NewOppLines.add(new OpportunityLineItem(OpportunityId=newOpp.Id, PriceBookEntryId=pbe.Id, Quantity=1, UnitPrice=pbe.UnitPrice));
		}
		
		if(NewOppLines.size()>0)
			insert NewOppLines;
			
		Opportunity_Contact_Role__c ocr = new Opportunity_Contact_Role__c();
        ocr.Opportunity_Contact__c = lstPro[0].Product_Preference_Card__r.Physician__c;
        ocr.Opportunity_del__c = newOpp.Id;
        ocr.Contact_Role__c = 'Decision Maker';
        insert ocr;
		
        PageReference nextPage = new PageReference('/' + newOpp.Id);
        return nextPage;
	}
	
	public boolean gethasMultiplePB()
	{
		if(pricebooks.size()>0) return true;
		
		return false;
	}
}