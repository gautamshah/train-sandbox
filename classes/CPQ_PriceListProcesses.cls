/**
Handler class for triggers Product2, Apttus_Proposal__Proposal__c

Functions that involve pricelist, pricebooks

CHANGE HISTORY
===============================================================================
DATE        NAME			DESC
2014-01-07  Yuli Fintescu	Created
2016-08-31	Isaac Lewis		Added Surgical Product Flag to auto-create price list item
10/7/2016   Paul Berglund   Moved SyncPriceListItem to
                            cpqPriceListItem_Product2_c, refactored to use
                            Organization Names global value list
10/31/2016  Paul Berglund   Remediation with CPQ                            
===============================================================================
*/
public class CPQ_PriceListProcesses
{
	//=========================================================
	// Auto add pricebook to opportunity when apttus proposal is created
	//=========================================================
	public static void SelectOpportunityPriceBook(Set<ID> inputProposals) {
		if (inputProposals.size() == 0)
			return;

	    String stdPBID = Pricebook2_c.getStandardPriceBookID();
		if (String.isEmpty(stdPBID))
			return;

		List<Opportunity> opps = new List<Opportunity>();
		Set<ID> uniq = new Set<ID>();
		for (Apttus_Proposal__Proposal__c p : [Select ID, Apttus_Proposal__Opportunity__c, Apttus_Proposal__Opportunity__r.ID, Apttus_Proposal__Opportunity__r.PriceBook2Id From Apttus_Proposal__Proposal__c Where ID in: inputProposals]) {
			if (!uniq.contains(p.Apttus_Proposal__Opportunity__c) && p.Apttus_Proposal__Opportunity__r.PriceBook2Id == null) {
				p.Apttus_Proposal__Opportunity__r.PriceBook2Id = stdPBID;
				opps.add(p.Apttus_Proposal__Opportunity__r);
				uniq.add(p.Apttus_Proposal__Opportunity__c);
			}
		}

		if (opps.size() > 0)
			update opps;
	}

	//=========================================================
	// Trigger on CPQ_Customer_Specific_Pricing__c:
	// Link Commitment_Type__c and Partner_Root_Contract__c object
	// when CommTypeDesc_INT__c and Root_Contract__c field is populated
	//=========================================================
	public static void Lookup(List<CPQ_Customer_Specific_Pricing__c> inputCSPs, Map<ID, CPQ_Customer_Specific_Pricing__c> oldMap) {
		Map<String, List<CPQ_Customer_Specific_Pricing__c>> candidates1 = new Map<String, List<CPQ_Customer_Specific_Pricing__c>>();
		Map<String, List<CPQ_Customer_Specific_Pricing__c>> candidates2 = new Map<String, List<CPQ_Customer_Specific_Pricing__c>>();
		for (CPQ_Customer_Specific_Pricing__c p : inputCSPs) {
			if (oldMap == null || p.CommTypeDesc_INT__c != oldMap.get(p.ID).CommTypeDesc_INT__c) {
				p.Commitment_Type__c = null;

				if (p.CommTypeDesc_INT__c != null) {
					String commL = p.CommTypeDesc_INT__c.toLowerCase();

					List<CPQ_Customer_Specific_Pricing__c> csps;
					if (candidates1.containsKey(commL)) {
						csps = candidates1.get(commL);
					} else {
						csps = new List<CPQ_Customer_Specific_Pricing__c>();
						candidates1.put(commL, csps);
					}
					csps.add(p);
				}
			}

			if (oldMap == null || p.Root_Contract__c != oldMap.get(p.ID).Root_Contract__c) {
				p.Partner_Root_Contract__c = null;

				if (p.Root_Contract__c != null) {
					String rootL = p.Root_Contract__c.toLowerCase();

					List<CPQ_Customer_Specific_Pricing__c> csps;
					if (candidates2.containsKey(rootL)) {
						csps = candidates2.get(rootL);
					} else {
						csps = new List<CPQ_Customer_Specific_Pricing__c>();
						candidates2.put(rootL, csps);
					}
					csps.add(p);
				}
			}
		}

		if (candidates1.size() > 0) {
			for (Commitment_Type__c c : [Select Name, ID From Commitment_Type__c Where Name in: candidates1.keySet()]) {
				String commL = c.Name.toLowerCase();

				if (candidates1.containsKey(commL)) {
					for (CPQ_Customer_Specific_Pricing__c p : candidates1.get(commL)) {
						p.Commitment_Type__c = c.ID;
					}
				}
			}
		}

		if (candidates2.size() > 0) {
			for (Partner_Root_Contract__c c : [Select Name, ID From Partner_Root_Contract__c Where Name in: candidates2.keySet()]) {
				String rootL = c.Name.toLowerCase();

				if (candidates2.containsKey(rootL)) {
					for (CPQ_Customer_Specific_Pricing__c p : candidates2.get(rootL)) {
						p.Partner_Root_Contract__c = c.ID;
					}
				}
			}
		}
	}
}