public with sharing class SalesNavigatorStage_Utilities
{
    Opportunities_Utilities utils = new Opportunities_Utilities();
    public static Set<Id> deletedStages = new Set<Id>();
    
    //used to get all the templates associated with this opportunity by record type
    private static Map<String, List<Sales_Navigation_Stage_Template__c>> templates = null;
    public static List<Sales_Navigation_Stage_Template__c> GetTemplates(String recordTypeName)
    {
    System.debug('this is the recordTypeName: ' + recordTypeName);

        if(templates == null)
        {
            List<Sales_Navigation_Stage_Template__c> t = [  SELECT Id, Name, Critical_Steps__c, Record_Type__c, Opportunity_Record_Type__c, Average_Stage_Duration_days__c,
                                                            URL_001__c, URL_002__c, URL_003__c, URL_004__c, URL_005__c, URL_006__c, URL_007__c, URL_008__c, URL_009__c, URL_010__c, URL_011__c, 
                                                            URL_012__c, URL_013__c, URL_014__c, URL_015__c, URL_016__c, URL_017__c, URL_018__c, URL_019__c, URL_020__c, URL_021__c, URL_022__c, 
                                                            URL_023__c, URL_024__c, URL_025__c, URL_026__c, URL_027__c, URL_028__c, URL_029__c, URL_030__c, URL_031__c, URL_032__c, URL_033__c, 
                                                            URL_034__c, URL_035__c, URL_036__c, URL_037__c, URL_038__c, URL_039__c, URL_040__c, URL_041__c, URL_042__c, URL_043__c, URL_044__c, 
                                                            URL_045__c, URL_046__c, URL_047__c, URL_048__c, URL_049__c, URL_050__c
                                                            FROM Sales_Navigation_Stage_Template__c 
                                                            //WHERE Opportunity_Record_Type__c IN :availableRecordTypes
                                                            ORDER BY Order_Number__c
                        ];
            templates = new Map<String, List<Sales_Navigation_Stage_Template__c>>();
            System.debug('this is  Sales_Navigation_Stage_Template__c: ' + t);

            for(Sales_Navigation_Stage_Template__c s: t)
            {
                if(templates.get(s.Opportunity_Record_Type__c) == null)
                    templates.put(s.Opportunity_Record_Type__c, new List<Sales_Navigation_Stage_Template__c>());
                templates.get(s.Opportunity_Record_Type__c).add(s);
            }
        }
        return templates.get(recordTypeName) == null ? new List<Sales_Navigation_Stage_Template__c>() : templates.get(recordTypeName);
    }
    
    //called from the opportunity utilities class to insert sales nav stages when an opportunity is inserted or updated
    public static void InsertSalesNavStages(List<Opportunity> opportunitiesToAddNavTo)
    {
        System.debug('In InsertSalesNavStages');
        List<Sales_Navigator_Stage__c> navsToInsert = new List<Sales_Navigator_Stage__c>();
        Map<Id, RecordType> rtMap = Opportunities_Utilities.GetRecordTypeMap();
        for( Opportunity o : opportunitiesToAddNavTo)
        {
            if(rtMap.containsKey(o.RecordTypeId) &&( o.Type == 'New Customer - Conversion'|| o.Type =='Existing Customer - New Business'|| o.Type =='At Risk') &&( o.Capital_Disposable__c == 'Disposable'||o.Capital_Disposable__c =='Both') && (o.Opportunity_Type__c == 'Multi Physician Sale'))
            {
                System.debug('GetTemplates(O.RecordType.DeveloperName): ' + GetTemplates(O.RecordType.DeveloperName));
                for(Sales_Navigation_Stage_Template__c t: GetTemplates(O.RecordType.DeveloperName))
                {
                System.debug('Inside for loop');
 System.debug('my Record_Type__c: ' + t.Record_Type__c);
                    Sales_Navigator_Stage__c sns = new Sales_Navigator_Stage__c(
                        Name = t.Name,
                        Opportunity__c = o.Id,
                        RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Sales_Navigator_Stage__c').Get(t.Record_Type__c).Id,
                        Critical_Steps__c = t.Critical_Steps__c,
                        Avg_Duration_days__c = t.Average_Stage_Duration_days__c,
                        URL_001__c = t.URL_001__c,URL_002__c = t.URL_002__c,URL_003__c = t.URL_003__c,URL_004__c = t.URL_004__c,URL_005__c = t.URL_005__c,URL_006__c = t.URL_006__c,URL_007__c = t.URL_007__c,URL_008__c = t.URL_008__c,
                        URL_009__c = t.URL_009__c,URL_010__c = t.URL_010__c,URL_011__c = t.URL_011__c,URL_012__c = t.URL_012__c,URL_013__c = t.URL_013__c,URL_014__c = t.URL_014__c,URL_015__c = t.URL_015__c,URL_016__c = t.URL_016__c,
                        URL_017__c = t.URL_017__c,URL_018__c = t.URL_018__c,URL_019__c = t.URL_019__c,URL_020__c = t.URL_020__c,URL_021__c = t.URL_021__c,URL_022__c = t.URL_022__c,URL_023__c = t.URL_023__c,URL_024__c = t.URL_024__c,
                        URL_025__c = t.URL_025__c,URL_026__c = t.URL_026__c,URL_027__c = t.URL_027__c,URL_028__c = t.URL_028__c,URL_029__c = t.URL_029__c,URL_030__c = t.URL_030__c,URL_031__c = t.URL_031__c,URL_032__c = t.URL_032__c,
                        URL_033__c = t.URL_033__c,URL_034__c = t.URL_034__c,URL_035__c = t.URL_035__c,URL_036__c = t.URL_036__c,URL_037__c = t.URL_037__c,URL_038__c = t.URL_038__c,URL_039__c = t.URL_039__c,URL_040__c = t.URL_040__c,
                        URL_041__c = t.URL_041__c,URL_042__c = t.URL_042__c,URL_043__c = t.URL_043__c,URL_044__c = t.URL_044__c,URL_045__c = t.URL_045__c,URL_046__c = t.URL_046__c,URL_047__c = t.URL_047__c,URL_048__c = t.URL_048__c,
                        URL_049__c = t.URL_049__c,URL_050__c = t.URL_050__c

                    );
                    navsToInsert.add(sns);
                }
            }
        }
        System.debug('navsToInsert: ' + navsToInsert);
        try
        {
            insert navsToInsert;
        }
        catch( DmlException exc )
        {
            System.debug('DML Exception Caught: ' + exc.getMessage());
        }
    }
    
    public void OnBeforeInsert(List<Sales_Navigator_Stage__c> stages)
    {
        Set<Id> oppsTocheckSet = new Set<Id>();
        for(Sales_Navigator_Stage__c s: stages)
            oppsTocheckSet.add(s.Opportunity__c);
        utils.RollupStages(oppsTocheckSet);
    }
    
    public void OnBeforeUpdate(List<Sales_Navigator_Stage__c> stages)
    {
        Set<Id> oppsTocheckSet = new Set<Id>();
        for(Sales_Navigator_Stage__c s: stages)
            oppsTocheckSet.add(s.Opportunity__c);
        utils.RollupStages(oppsTocheckSet);
    }   
    
    public void OnBeforeDelete(List<Sales_Navigator_Stage__c> stages, Map<ID, Sales_Navigator_Stage__c> stagesMap)
    {
        deletedStages = stagesMap.keySet();
        Set<Id> oppsTocheckSet = new Set<Id>();
        for(Sales_Navigator_Stage__c s: stages)
            oppsTocheckSet.add(s.Opportunity__c);
        utils.RollupStages(oppsTocheckSet);
    }
}