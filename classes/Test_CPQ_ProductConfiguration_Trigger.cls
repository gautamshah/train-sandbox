/*
Test class

CPQ_ProposalProcesses.FinalizingConfig()
CPQ_ProposalProcesses.UpdateConfig()

CPQ_ProductConfiguration_After
CPQ_ProductConfiguration_Before

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
private class Test_CPQ_ProductConfiguration_Trigger
{
    static List<Apttus_Config2__ProductConfiguration__c> testConfigs;
    static List<Apttus_Config2__LineItem__c> testLines;

    @testVisible
    static void createTestData()
    {
        List<Apttus_Config2__PriceList__c> testPriceLists = cpqPriceList_c.PriceLists.values();
            
        upsert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class);
        for(RecordType R: rts.values())
        {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        Map<String,Id> mapAgreementRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus__APTS_Agreement__c.class).values())
        {
            mapAgreementRType.Put(R.DeveloperName, R.Id);
        }

        Apttus_Proposal__Proposal__c testProposal =
            new Apttus_Proposal__Proposal__c(
                Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                RecordTypeID = mapProposalRType.get('Rebate_Agreement'),
                Duration__c = '1 Year',
                Buyout_as_of_Date__c = System.Today(),
                Buyout_Amount__c = 100,
                Tax_Gross_Net_of_TradeIn__c = 'Net',
                Apttus_Proposal__Sales_Tax_Percent__c = 0.04,
                TBD_Amount__c = 100,
                Total_Annual_Sensor_Commitment_Amount__c = 100,
                Apttus_Proposal__Proposal_Expiration_Date__c = System.Today()
            );
        insert testProposal;

        Apttus__APTS_Agreement__c testAgreement =
            new Apttus__APTS_Agreement__c(
                Name = 'Test Agreement',
                RecordTypeID = mapAgreementRType.get('Rebate_Agreement_AG'),
                Apttus__Status__c = 'Requested',
                Apttus_Approval__Approval_Status__c = 'Approved',
                Duration__c = '1 Years'
            );
        insert testAgreement;

        testConfigs = new List<Apttus_Config2__ProductConfiguration__c>
        {
            //config for proposal
            new Apttus_Config2__ProductConfiguration__c(
                Apttus_QPConfig__Proposald__c = testProposal.ID,
                Apttus_Config2__PriceListId__c = testPriceLists[0].ID,
                Apttus_Config2__Status__c = 'Saved',
                Apttus_Config2__VersionNumber__c = 1
            ),

            new Apttus_Config2__ProductConfiguration__c(
                Apttus_QPConfig__Proposald__c = testProposal.ID,
                Apttus_Config2__PriceListId__c = testPriceLists[0].ID,
                Apttus_Config2__Status__c = 'Saved',
                Apttus_Config2__VersionNumber__c = 2
            ),

            //config for agreement
            new Apttus_Config2__ProductConfiguration__c(
                Apttus_CMConfig__AgreementId__c = testAgreement.ID,
                Apttus_Config2__PriceListId__c = testPriceLists[0].ID,
                Apttus_Config2__Status__c = 'Saved',
                Apttus_Config2__VersionNumber__c = 1
            ),

            new Apttus_Config2__ProductConfiguration__c(
                Apttus_CMConfig__AgreementId__c = testAgreement.ID,
                Apttus_Config2__PriceListId__c = testPriceLists[0].ID,
                Apttus_Config2__Status__c = 'Saved',
                Apttus_Config2__VersionNumber__c = 2
            )
        };

        // 20161115-A Had to replace the Price List Item setup, done automatically now
        Product2 testProduct =
            new Product2(
                Name = 'ITEM1',
                ProductCode = 'ITEM1',
                Product_External_ID__c = 'US:RMS:ITEM1',
                Apttus_Product__c = true,
                IsActive = true,
                OrganizationName__c = 'RMS',
                Apttus_Config2__EffectiveDate__c = System.Today().addDays(120),
                Charge_Type__c = 'Consumable');

        insert testProduct;

        Apttus_Config2__PriceListItem__c testPriceListItem =
            [SELECT Id,
                    Apttus_Config2__PriceListId__c,
                    Apttus_Config2__ProductId__c,
                    Apttus_Config2__Active__c
             FROM Apttus_Config2__PriceListItem__c
             WHERE Apttus_Config2__ProductId__c = :testProduct.Id
             LIMIT 1];

        Partner_Root_Contract__c testRoot =
            new Partner_Root_Contract__c(
                Name = '011111',
                Root_Contract_Name__c = '011111',
                Root_Contract_Number__c = '011111',
                Direction__c = 'I',
                Effective_Date__c = System.Today(),
                Compliance_Language__c = 'The Term');

        insert testRoot;

        testLines = new List<Apttus_Config2__LineItem__c> {
            new Apttus_Config2__LineItem__c(
                Apttus_Config2__IsPrimaryLine__c = true,
                Apttus_Config2__ProductId__c = testProduct.ID,
                Apttus_Config2__PriceListItemId__c = testPriceListItem.ID,
                Apttus_Config2__ItemSequence__c = 1,
                Apttus_Config2__LineNumber__c = 1,
                Apttus_Config2__PrimaryLineNumber__c = 1,
                Apttus_Config2__NetAdjustmentPercent__c = 0.5,
                Apttus_Config2__ChargeType__c = 'Consumable',
                Current_Root_Contract__c = '011111'),

            new Apttus_Config2__LineItem__c(
                Apttus_Config2__IsPrimaryLine__c = true,
                Apttus_Config2__ProductId__c = testProduct.ID,
                Apttus_Config2__PriceListItemId__c = testPriceListItem.ID,
                Apttus_Config2__ItemSequence__c = 1,
                Apttus_Config2__LineNumber__c = 1,
                Apttus_Config2__PrimaryLineNumber__c = 1,
                Apttus_Config2__NetAdjustmentPercent__c = 0.5,
                Apttus_Config2__ChargeType__c = 'Consumable',
                Current_Root_Contract__c = '011111')
        };
    }

    static testMethod void myUnitTest() {
        createTestData();
        Test.startTest();
            insert testConfigs;

            for (Apttus_Config2__ProductConfiguration__c c : testConfigs) {
                c.Apttus_Config2__Status__c = 'Finalized';
            }
            update testConfigs;

            for (Apttus_Config2__ProductConfiguration__c c : testConfigs) {
                c.Apttus_Config2__Status__c = 'Saved';
            }
            update testConfigs;

            testLines[0].Apttus_Config2__ConfigurationId__c = testConfigs[0].ID;
            testLines[1].Apttus_Config2__ConfigurationId__c = testConfigs[2].ID;
            insert testLines;

            testConfigs[0].Apttus_Config2__Status__c = 'Finalized';
            update testConfigs[0];

            testConfigs[2].Apttus_Config2__Status__c = 'Finalized';
            update testConfigs[2];

        Test.stopTest();
    }

    static testMethod void TestDisabledTrigger() {
        try {
        CPQ_Trigger_Profile__c testTrigger =
            new CPQ_Trigger_Profile__c(
                SetupOwnerId = cpqUser_c.CurrentUser.Id,
                CPQ_Agreement_After__c = true,
                CPQ_Agreement_Before__c = true,
                CPQ_Participating_Facility_After__c = true,
                CPQ_Product2_After__c = true,
                CPQ_Product2_Before__c = true,
                CPQ_ProductConfiguration_After__c = true,
                CPQ_ProductConfiguration_Before__c = true,
                CPQ_Proposal_After__c = true,
                CPQ_Proposal_Before__c = true,
                CPQ_Proposal_Distributor_After__c = true,
                CPQ_Rebate_After__c = true,
                CPQ_Rebate_Agreement_After__c = true
            );
        insert testTrigger;
        } catch (Exception e) {}

        createTestData();

        Test.startTest();
            insert testConfigs;

            for (Apttus_Config2__ProductConfiguration__c c : testConfigs)
                c.Apttus_Config2__Status__c = 'Finalized';

            update testConfigs;

        Test.stopTest();
    }
}