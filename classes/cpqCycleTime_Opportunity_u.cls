/*
11/02/2016   Paul Berglund   While looking for duplicate Id issues, noticed that oldMap is not used
                             and removed it
                             Also noticed that opportunityIds wasn't being used and the code setting
                             it up was removed
                             Changed set to list, no reason for a set because you will only encouter
                             a record 1 time in a trigger
                             Removed check for empty list to insert, no DML is permformed if the
                             list is empty, no error
*/
public class cpqCycleTime_Opportunity_u extends cpqCycleTime_u
{
    public override void capture(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<sObject> newList,
            Map<Id, sObject> newMap,
            List<sObject> oldList,
            Map<Id, sObject> oldMap,
            integer size
        )
    {
        system.debug(string.valueOf(this) + '.capture.queries: ' + Limits.getQueries());
        
        if (isAfter && isUpdate)
            capture(newList, oldMap, isInsert, isUpdate);
    }
    
    // Capture the cycle times associated with changes to an Opportunity
    protected override void capture(
        List<sObject> objNewList,
        Map<Id,sObject> objOldMap,
        Boolean isInsert,
        Boolean isUpdate)
    {
    	List<Opportunity> newList = (List<Opportunity>)objNewList;
    	Map<Id, Opportunity> oldMap = (Map<Id, Opportunity>)objOldMap;

        List<CPQ_Cycle_Time__c> cycleTimesToInsert = new List<CPQ_Cycle_Time__c>();
        ////List<Id> opportunityIds = new List<Id>();
        DateTime NOW = system.now();
        
        ////List<Opportunity> insertOpportunities = new List<Opportunity>();
        ////for (Opportunity o:newList)
        ////{
        ////        insertOpportunities.add(o);
        ////        opportunityIds.add(o.Id);
        ////}

        // Create new cycle time rows with the opportunity and opportunity created date set
        for (Opportunity o : newList)
        {
            CPQ_Cycle_Time__c cycleTime = new CPQ_Cycle_Time__c();
            cycleTime.Opportunity__c = o.Id;
            cycleTime.Opportunity_Created_Date__c = NOW;
            cycleTimesToInsert.add(cycleTime);
        }
        
        insert cycleTimesToInsert;

        ////if (!cycleTimesToInsert.isEmpty())
        ////{
        ////    insert new List<CPQ_Cycle_Time__c>(cycleTimesToInsert);
        ////}
    }
}