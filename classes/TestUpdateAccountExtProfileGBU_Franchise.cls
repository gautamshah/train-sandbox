@isTest
private class TestUpdateAccountExtProfileGBU_Franchise { 

/*********************************************************************
*
* Updates
*
*  Date      Author               Description
* ----------------------------------------------------------------------
*
*  x8-3-2012   MJM         Re-configured to used test account creation from TestUtility class
*
*********************************************************************/
    static testMethod void myUnitTest() {
       // Create the Test Data
        User u = [Select Id from User where Id = :UserInfo.getUserId()];
        
        u.Business_Unit__c = 'The Unit';
       // u.Franchise__c = 'Burger King';
        
        update u;
        testUtility tu = new testUtility();
       
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        
        Account_Extended_Profile__c o = new Account_Extended_Profile__c(Account__c = a.Id);
        insert o;
        
        //System.assertEquals('Burger King', [Select Franchise__c from Account_Extended_Profile__c where Id = :o.Id].Franchise__c);
        //System.assertEquals('The Unit', [Select Business_Unit__c from Account_Extended_Profile__c where Id = :o.Id].Business_Unit__c);
        
        
        u.Business_Unit__c = '';
        u.Franchise__c = ''; 
        
        update u;
        update o;
        
       // System.assertEquals(null, [Select Franchise__c from Account_Extended_Profile__c where Id = :o.Id].Franchise__c);
       // System.assertEquals(null, [Select Business_Unit__c from Account_Extended_Profile__c where Id = :o.Id].Business_Unit__c);
        
        
            Id pId = [select Id 
                        from Profile 
                       where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id;
                       
             User u2 = new User();
             u2.LastName = 'Shmoe Test';
             u2.Business_Unit__c = 'The Unit';
             u2.Franchise__c = 'Burger King';
             u2.email = 'testShmoe@covidian.com';
             u2.alias = 'testShmo';
             u2.username = 'testShmoe@covidian.com';
             u2.communityNickName = 'testShmoe@covidian.com';
             u2.ProfileId = pId;
             u2.CurrencyIsoCode='USD'; 
             u2.EmailEncodingKey='ISO-8859-1';
             u2.TimeZoneSidKey='America/New_York';
             u2.LanguageLocaleKey='en_US';
             u2.LocaleSidKey ='en_US';
             insert u2;
             System.runAs(u2){
                Account_Extended_Profile__c o2 = new Account_Extended_Profile__c(Account__c = a.Id);
                insert o2;
                //System.assertEquals(null, [Select Franchise__c from Account_Extended_Profile__c where Id = :o2.Id].Franchise__c);
                //System.assertEquals(null, [Select Business_Unit__c from Account_Extended_Profile__c where Id = :o2.Id].Business_Unit__c);
             }

    }
}