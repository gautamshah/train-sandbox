/**
Test class

CPQ_Controller_ProposalFormulaButtons

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_Controller_PropFormulaBtns {

    static Apttus_Proposal__Proposal__c testProposal;

    static void createTestData() {

        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        CPQ_Variable__c testVar = new CPQ_Variable__c(Name = 'RMS PriceList ID', Value__c = testPriceList.ID);
        insert testVar;

        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
        Account testAccount = new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main');
        insert testAccount;

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values()) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', Apttus_Proposal__Account__c = testAccount.ID, RecordTypeID = mapProposalRType.get('COOP_Plus_Program'));
        insert testProposal;

    }

    static testMethod void myUnitTest() {

        System.Debug('*** myUnitTest ***');
        createTestData();

        Test.startTest();
        //purpose = Deny
            PageReference pageRef = Page.CPQ_ProposalFormulaButtons;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('purpose', 'Deny');

            CPQ_Controller_ProposalFormulaButtons p = new CPQ_Controller_ProposalFormulaButtons(new ApexPages.StandardController(testProposal));
            p.doAction();
            System.Debug(p.getHasErrors());

        //purpose = AmendedAreas
            ApexPages.currentPage().getParameters().put('purpose', 'AmendedAreas');
            p = new CPQ_Controller_ProposalFormulaButtons(new ApexPages.StandardController(testProposal));
            System.Debug(p.isRSCOOP);
        Test.stopTest();

    }

}