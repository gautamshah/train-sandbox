@isTest(SeeAllData=true)
private class TestOpportunity_Main {
     
    static testMethod void testrun() {
     
       Profile p = [SELECT Id FROM Profile WHERE Name='GBU Administrator']; 
          User u = new User(
          Email='standarduser@testorg.com', 
          EmailEncodingKey='UTF-8', 
          LastName='1Testing', 
          LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', 
          ProfileId = p.Id, 
          Business_Unit__c = 'X', 
          Region__c = 'US',
          User_Role__c = 'Rep',
          Function__c = 'Channel',
          TimeZoneSidKey='America/Los_Angeles', 
          Username = 'tester20131025@test.com',
          Alias = 'Tester',
          CommunityNickname = 'Tester');
    
          System.runAs(u) {
        
        Pricebook2 s = [select id from Pricebook2 where IsStandard = true limit 1];     
     // create the product  
     Product2 p1 = new Product2(  
         name='M&S Product 1',  
         IsActive=true,  
         Description='My Product',  
         ProductCode='12345' 
         );  
     insert p1;  
     // create 2nd product  
     Product2 p2 = new Product2(  
         name='Base Product 2',  
         IsActive=true,  
         Description='My Base Product',  
         ProductCode='11111' 
         );  
     insert p2;  

     // create the pricebookentry  
         PricebookEntry pbe1 = new PricebookEntry(  
         Pricebook2Id=s.id,  
         Product2Id=p1.id,  
         UnitPrice=0.00,  
         IsActive=true,  
         UseStandardPrice=false 
     );  
     insert pbe1;   
      // create 2nd pricebookentry  
         PricebookEntry pbe2 = new PricebookEntry(  
         Pricebook2Id=s.id,  
         Product2Id=p2.id,  
         UnitPrice=0.00,  
         IsActive=true,  
         UseStandardPrice=false 
     );  
     insert pbe2;   
     
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        
        //Opportunity o = new Opportunity(Pricebook2Id=s.id,   Video__c = 'test', Sales_Aid__c = 'Test', PPT__c='Test',Best_Practice__c = 'Test class',CurrencyIsoCode = 'USD', OwnerId = u.id,AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won');
        Opportunity o = new Opportunity(Pricebook2Id=s.id,   CurrencyIsoCode = 'USD', OwnerId = u.id,AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won',WonLossReasons__c='Other');        
        insert o; 
        //Opportunity o1 = new Opportunity(Pricebook2Id=s.id,  Video__c = 'test', Sales_Aid__c = 'Test', PPT__c='Test',Best_Practice__c = 'Test class', CurrencyIsoCode = 'USD', OwnerId = u.id,AccountId = a.Id, name = 'Test oppty 111', CloseDate = System.Today(), StageName = 'Identify');
        Opportunity o1 = new Opportunity(Pricebook2Id=s.id,  CurrencyIsoCode = 'USD', OwnerId = u.id,AccountId = a.Id, name = 'Test oppty 111', CloseDate = System.Today(), StageName = 'Identify');        
        insert o1; 
        List<Opportunity> lstopp = new List<Opportunity>();
        Set<ID> setids = new Set<ID>();
        lstopp.add(o);
        lstopp.add(o1);        
        setids.add(o.id);
        setids.add(o1.id);     

        OpportunityLineItem oli = new OpportunityLineItem(Quantity = 1,PricebookEntryId = pbe1.id, OpportunityId = o1.id, UnitPrice=1, All_Lost__c = true);
        insert oli;  
        
        OpportunityLineItem oli2 = new OpportunityLineItem();  
         oli2.Quantity = 2;  
         oli2.PricebookEntryId = pbe2.id;  
         oli2.OpportunityId = o.id;  
         oli2.UnitPrice = 2;    
         insert oli2;  
         
         oli2.All_Lost__c = true;
         update oli2;
         
        //Opportunity_Main opp_main = new Opportunity_Main();
        Opportunity_Main.UpdateOpportunityGBU_Franchise(lstopp);
        ///Opportunity_Main.ProductReqForUSMedSupplies(lstopp);
        Opportunity_Main.OwnerRegionAssignment(lstopp);
        Opportunity_Main.reEstablishSchedule(lstopp);
       
    
        }
      
    
        
        
        
     
    }
}