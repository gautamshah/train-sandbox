public without sharing class distributorHomecls {
    public List<Id> lstEmp {get; set;}

    public distributorHomecls(){
        lstwrap = new List<UserClass>();
        //Logged in User Id
        User u = [Select id, contactid,User_Phone__c,User_email__c,LanguageLocaleKey from User where Id=:UserInfo.getUserId()];
        
        //Querying all User Records future Use
        Set<String> alpha = new Set<String>{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
      
        if(u.contactId != null){
            Contact con = [Select id, Accountid from Contact where id=:u.contactid];
            Map<String,list<EmpClass>> mpUsersFinal = new Map<String,List<EmpClass>>();
            List<Convidien_Contact__c> lst = [Select id,name,Distributor_Name__c,Type__c,Employee__c from Convidien_Contact__c where Distributor_Name__c =: con.AccountId and Employee__c != null];
            lstEmp = new List<Id>();
            
            for(Convidien_Contact__c cc : lst){
                if(cc.Employee__c != null){
                    lstEmp.add(cc.Employee__c);  
                }
                
            }
            lstEmpClass = new List<EmpClass>();
             Map<String,Convidien_Contact__c > mpCOV = new Map<String,Convidien_Contact__c >();
            if(lstEmp.size()>0){
                Map<Id,Employee__c> mpEmployees = new Map<Id,Employee__c>([Select id,name,User_details__c,Phonetic_EmpName__c,User_details__r.fullphotourl,Employee_Photo__c,First_Name__c,Last_name__c,Phonetic_FirstName__c,Phonetic_LastName__c,Telephone_Number__c,Email__c from Employee__c where Id in: lstEmp]);
               
                for(Convidien_Contact__c cc : lst){
                        mpCOV.put(cc.Type__c,cc);
                        if(mpUsersFinal.get(cc.Type__c) != null){
                            if(mpEmployees.get(cc.Employee__c).Phonetic_EmpName__c != null)
                            	mpUsersFinal.get(cc.Type__c).add(new EmpClass(mpEmployees.get(cc.Employee__c),true));
                            else
                            	mpUsersFinal.get(cc.Type__c).add(new EmpClass(mpEmployees.get(cc.Employee__c),false));
                        }
                        else{
                            if(mpEmployees.get(cc.Employee__c).Phonetic_EmpName__c != null)
                            	mpUsersFinal.put(cc.Type__c, new List<EmpClass>{new EmpClass(mpEmployees.get(cc.Employee__c),true)});
                            else
                            	mpUsersFinal.put(cc.Type__c, new List<EmpClass>{new EmpClass(mpEmployees.get(cc.Employee__c),false)});
                        }
                }
            }
            
            
            
            for(String s :mpUsersFinal.keyset()){
                lstwrap.add(new UserClass(s,mpUsersFinal.get(s),mpCOV.get(s)));   
            }
            
        } 
        
        
    
    }
    
    public List<UserClass> lstwrap{get; set;}
    public class UserClass{
        public string UserType{get; set;}
        public List<EmpClass> lstUsers{get; set;}
        public Convidien_Contact__c cov{get; set;}
        public UserClass(string a, list<EmpClass> lst,Convidien_Contact__c cc){
            UserType = a;
            lstUsers = lst;
            cov = cc;
        }
    
    }
    public List<EmpClass> lstEmpClass{get; set;}
    public class EmpClass{
        public Employee__c emp{get; set;}
        public boolean isEnglish{get; set;}
        public EmpClass(Employee__c e, boolean b){
            emp = e;
            isEnglish = b;
        }
    }
    
    
}