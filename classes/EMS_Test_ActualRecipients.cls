@isTest
public class EMS_Test_ActualRecipients {
    
    static testmethod void m1(){
    
        Approver_matrix__c app = new Approver_matrix__c();
        app.Name='SG';
        app.Marketing_Country_Controller_1__c = UserInfo.getUserId(); 
        app.Marketing_Country_Controller_2__c = UserInfo.getUserId();
        app.CCP_Coordinator_1__c = UserInfo.getUserId();
        app.CCP_Coordinator_2__c = UserInfo.getUserId();             
        app.GCC_Finance_1__c = UserInfo.getUserId();
        app.GCC_Finance_2__c = UserInfo.getUserId(); 
        app.GCC_Legal_1__c = UserInfo.getUserId();
        app.GCC_Legal_2__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_1__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_2__c = UserInfo.getUserId();
        app.GCC_PACE_1__c = UserInfo.getUserId(); 
        app.GCC_PACE_2__c = UserInfo.getUserId();
        app.GCC_R_D_1__c = UserInfo.getUserId();
        app.GCC_R_D_2__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_1__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_2__c = UserInfo.getUserId();
        app.Regional_Business_Head_1__c = UserInfo.getUserId();
        app.Regional_Business_Head_2__c = UserInfo.getUserId();
        app.Regional_PACE_Manager_1__c =UserInfo.getUserId(); 
        app.Regional_PACE_Manager_2__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_1__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_2__c = UserInfo.getUserId(); 
        app.ARO_CA_Director_1__c=UserInfo.getUserId();
        app.ARO_CA_Director_2__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_1__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_2__c= UserInfo.getUserId();
        insert app;
        
        Account acc =new Account();
        acc.name='test acc';
        insert acc;
        
        Contact con = new Contact();
        con.LastName='Test Contact';
        con.accountid= acc.id;
        insert con;
        
        Contact con1 = new Contact();
        con1.LastName='Test Contact';
        con1.accountid= acc.id;
        insert con1;
        
        Employee__c emp = new Employee__c();
        emp.Name = 'Test';
        emp.User_Details__c= UserInfo.getUserId();
        insert emp;
        Profile p = [select id from profile where name='System Administrator'];   
        User u = new User(profileId = p.id, username = 'c@mixmaster.com', email = 'c@k.com', 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', country='SG',IsEMSEventOwner__c=true,
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname');
        
        System.runAs(u){
        EMSMarketingGBU__c mycs = new EMSMarketingGBU__c(Name= 'SG;EBD');
        mycs.ARO_Marketing_Head_1__c= u.id;
        mycs.ARO_Marketing_Head_2__c=u.id;
        insert mycs;
        EMS_EVent__c EMSRec = new EMS_Event__c();
        EMSRec.Event_Owner_User__c = UserInfo.getUserId();
        EMSrec.Upload_External_Unique_Id__c= '1248';         
        EMSRec.GBU_Primary__c='EBD';
        EMSRec.Division_Primary__c='test';
        EMSRec.Start_Date__c = System.Today().addDays(22);
        EMSRec.End_Date__c = System.Today().addDays(45);
        EMSRec.Plan_Budget_Amt__c=50000;
        EMSRec.CPA_Required__c='Yes';
        EMSRec.Name= 'test event';
        EMSrec.Event_Native_Name__c='test event name';
        EMSRec.Location_Country__c='China';  
        EMSRec.Location_State__c='Shanghai';
        EMSRec.Expense_Type__c='Sales';
        EMSRec.Event_Type__c='CME';
        EMSRec.Training_conducted_in_Hosp_Training_Cent__c='Yes';
        EMSRec.HTC_Name__c='Test';
        EMSRec.Primary_COT__c='AB';
        EMSRec.CPA_Status__c='New Request';
        EMSRec.Reoccurring_or_New__c='New';
        EMSRec.Event_Host__c='Covidien';
        EMSRec.Location_City__c='Test City';
        EMSRec.Venue__c= 'Test Venue'; 
        EMSRec.First_Payment_Date__c=System.Today();
        EMSRec.Max_Attendees__c=400;
        EMSRec.Any_recipient_s_who_are_KOL_s_HCP_s__c='YES';
        EMSRec.Any_HCP_Invitees_from_Outside_Asia__c='YES';
        EMSRec.Benefit_Summary__c='Test Summary';
        EMSRec.CCI__c='YES';
        EMSrec.Event_Status__c='Planned';
        insert EMSrec;
        
       Event_Beneficiaries__c eb = new Event_Beneficiaries__c();
        eb.EMS_Event__c = emsrec.Id;
        eb.Invitee_Type__c='Actual Beneficiary';
        eb.type__c='Individual Beneficiary';
        eb.no_of_pax__c = 25;
        insert eb;
        Event_Beneficiaries__c eb1 = new Event_Beneficiaries__c();
        eb1.EMS_Event__c = emsrec.Id;
        eb1.Invitee_Type__c='Actual Beneficiary';
        eb1.type__c='Individual Beneficiary';
        eb1.no_of_pax__c = 25;
        insert eb1;
        Event_Beneficiaries__c eb2 = new Event_Beneficiaries__c();
        eb2.EMS_Event__c = emsrec.Id;
        eb2.Invitee_Type__c='Actual Beneficiary';
        eb2.type__c='Individual Beneficiary';
        eb2.no_of_pax__c = 25;
        insert eb2;
        Event_Beneficiaries__c eb3 = new Event_Beneficiaries__c();
        eb3.EMS_Event__c = emsrec.Id;
        eb3.Invitee_Type__c='Actual Beneficiary';
        eb3.type__c='Individual Beneficiary';
        eb3.no_of_pax__c = 25;
        insert eb3;
        
        Attachment att = new Attachment();
        att.parentId = EMSrec.Id;
        att.name='Event_Invitees_Budgeted.xls';
        att.body = blob.valueOf('1234');
        insert att;   
        
        ApexPages.currentPage().getParameters().put('EventId',EMSrec.Id);
        ApexPages.currentPage().getParameters().put('step','2');
        ApexPAges.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
        
        EMS_ActualRecipients obj = new EMS_ActualRecipients(new ApexPages.StandardController(new Account()));
        List<EMS_ActualRecipients.EmpConWrapper> lstwrap = new List<EMS_ActualRecipients.EmpConWrapper>();
        lstwrap.add(new EMS_ActualRecipients.EmpConWrapper(con,new Employee__c(),acc,true));
        obj.MealtotalLumpsum=20000;
        obj.sponsor=200;
        obj.InternalOrderFee=200;
        obj.MeetingPackage=200;
        obj.MeetingRoomRental=200;
        obj.BoothSpace=200;
        obj.Advertisement=200;
        obj.Miscellaneous=200;
        obj.Gifts=200;
        obj.Taxi=200;
        obj.train=200;
        obj.coach=200;
        obj.pickup=200;
        obj.others=200;
        obj.getContacts();
        obj.getHasNext();
        obj.getHasPrevious();
        obj.getLstActualRecipients();
        obj.getPageNumber();
        obj.getSelectedCount();
        obj.getTotalPages();
        obj.setsection1();
        obj.setsection2();
        obj.setsection3();
        obj.firstName='test';
        obj.searchContacts();
        obj.expandLTcolumns();
        obj.collapseLTcolumns();
        obj.expandLodgingColumns();
        obj.collapseLodgingColumns();
        obj.expandOthersColumns();
        obj.collapseOthersColumns();
        obj.expandATcolumns();
        obj.collapseATColumns();
        obj.back();
        obj.renderSearchFields();
        obj.saveTransactions();
        obj.AddContacts();
        obj.deleteContact();
        
        obj.showPopUp();
        obj.showOthers();
        obj.showPopUpLodging();
        obj.closePopup();
        obj.showGifts();
        obj.redirectPopUp();
        obj.redirectPopupOthers();
        obj.redirectpopupTransport();
        obj.redirectpopupgifts();
        obj.doSelectItem();
        obj.doDeSelectItem();
        obj.doNext();
        obj.doPrevious();
        
        obj.contextitem=con.Id;
        obj.doSelectItem();
        obj.selectedsearchcriteria=1;
        obj.AddContacts();
        obj.selectedsearchcriteria=2;
        obj.AddContacts();
        
        obj.ReciepientId=eb1.Id;
        obj.deleteContact();
       }
    }

}