/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {

	public String pacSitePrefix {get;set;}
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        return Network.communitiesLanding();
    }

    public String getPacPrefix(){
      Performance_Analyzer_Community__c pac = Performance_Analyzer_Community__c.getValues('Performance Analyzer Community Prefix');
      pacSitePrefix = pac.Value__c;
      return pacSitePrefix; 
    }

    
    public PageReference forwardToCustomAuthPage() {
       
       getPacPrefix();

       if(Site.getPathPrefix() == pacSitePrefix){
        	return Network.communitiesLanding();
       }else{
        	return new PageReference(Site.getPrefix() + '/home/home.jsp'); 
        }

    }
    
    public CommunitiesLandingController() {}
}