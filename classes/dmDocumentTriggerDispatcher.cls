public with sharing class dmDocumentTriggerDispatcher extends dmTriggerDispatcher
{
    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<dmDocument__c> newList,
            Map<Id, dmDocument__c> newMap,
            List<dmDocument__c> oldList,
            Map<Id, dmDocument__c> oldMap,
            integer size
        )
    {
        dmDocument.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size);
    }
}