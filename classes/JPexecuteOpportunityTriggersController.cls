public class JPexecuteOpportunityTriggersController {
    public List<Opportunity> oppList {get;set;}
    public Date skey_CloseDateBegin {get; set;}
    public Date skey_CloseDateEnd {get; set;}
    public Date skey_SalesStartDateBegin {get; set;}
    public Date skey_SalesStartDateEnd {get; set;}
    public Integer numOfRows {get; set;}
    public Integer totalNumOfRows {get; set;}
    
    private List<RecordType> oppRecordTypes;
    
    private Date stringToDate(String s) {
        // String s needs to be in format YYYY/MM/DD
        List<String> l = s.split('/');
        if (l.size() != 3){
            return null;
        }
        if (l[0].isNumeric() && l[1].isNumeric() && l[2].isNumeric()) {
            return Date.newInstance(Integer.valueOf(l[0]), Integer.valueOf(l[1]), Integer.valueOf(l[2]));
        }
        return null;
    }
    public String skey_CloseDateBeginStr {
        get { return skey_CloseDateBegin.year()+'/'+skey_CloseDateBegin.month()+'/'+skey_CloseDateBegin.day();}
        set { skey_CloseDateBegin = stringToDate(value); }
    }
    public String skey_CloseDateEndStr {
        get { return skey_CloseDateEnd.year()+'/'+skey_CloseDateEnd.month()+'/'+skey_CloseDateEnd.day();}
        set { skey_CloseDateEnd = stringToDate(value); }
    }
    public String skey_SalesStartDateBeginStr {
        get { return skey_SalesStartDateBegin.year()+'/'+skey_SalesStartDateBegin.month()+'/'+skey_SalesStartDateBegin.day();}
        set { skey_SalesStartDateBegin = stringToDate(value); }
    }
    public String skey_SalesStartDateEndStr {
        get { return skey_SalesStartDateEnd.year()+'/'+skey_SalesStartDateEnd.month()+'/'+skey_SalesStartDateEnd.day();}
        set { skey_SalesStartDateEnd = stringToDate(value); }
    }
    public String oppListSizeStr {
      get { return totalNumOfRows.format();}
      set;
    }
    public List<OppRecordTypeClass> oppRecordTypeList {get; set;}
    public class OppRecordTypeClass{
      public RecordType rt {get; set;}
      public Boolean b {get; set;}
      
      OppRecordTypeClass(RecordType r, Boolean bl) {
        rt = r;
        b = bl;
      }
    }
    
    public JPexecuteOpportunityTriggersController () {
      init();  
    }
    
    public void init() {
        oppList = new List<Opportunity>();
        JPExecuteOpportunityTriggerCS__c CS = JPExecuteOpportunityTriggerCS__c.getInstance();
        
        String OppRtLIKE = 'Japan%';
        skey_CloseDateBegin = System.today();
        skey_CloseDateEnd = System.today();
        skey_SalesStartDateBegin = System.today();
        skey_SalesStartDateEnd = System.today();
        numOfRows = 100;
        totalNumOfRows = 0;
        
        if (CS != NULL) {
            skey_CloseDateBegin = CS.CloseDateBegin__c == NULL ? skey_CloseDateBegin : CS.CloseDateBegin__c;
            skey_CloseDateEnd = CS.CloseDateEnd__c == NULL ? skey_CloseDateEnd :  CS.CloseDateEnd__c;
            skey_SalesStartDateBegin = CS.SalesStartDateBegin__c == NULL ? skey_SalesStartDateBegin : CS.SalesStartDateBegin__c;
            skey_SalesStartDateEnd = CS.SalesStartDateEnd__c == NULL ? skey_SalesStartDateEnd : CS.SalesStartDateEnd__c;
            OppRtLIKE = CS.OpportunityRecordTypeLIKE__c == NULL ? OppRtLIKE : CS.OpportunityRecordTypeLIKE__c;
            numOfRows = CS.NumberOfRows__c == NULL ? numOfRows : CS.NumberOfRows__c.intValue();
        }
        // Limit numOfRows to 500.
        numOfRows = numOfRows > 500 ? 500 : numOfRows;
        
        oppRecordTypes = [SELECT id, Name FROM RecordType WHERE name LIKE :OppRtLIKE and isactive=true AND sobjecttype='Opportunity'];
        oppRecordTypeList = new List<OppRecordTypeClass>();
        for (RecordType r : oppRecordTypes){
          oppRecordTypeList.add(new OppRecordTypeClass(r,true));
        }
    }

   public void search() {
     if (skey_CloseDateBegin == null){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR: ' + 'Invalid input for Close Date Begin'));
         return;
     }
     if (skey_CloseDateEnd == null){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR: ' + 'Invalid input for Close Date End'));
         return;
     }
     if (skey_SalesStartDateBegin == null){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR: ' + 'Invalid input for Sales Start Date Begin'));
         return;
     }
     if (skey_SalesStartDateEnd == null){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR: ' + 'Invalid input for Sales Start Date End'));
         return;
     }
     try {
        Set<ID> rtSet = new Set<ID>();
        for (OppRecordTypeClass ort : oppRecordTypeList) {
          if (ort.b) rtSet.add(ort.rt.id);
        }
        // Get total number of Rows
         totalNumOfRows= [ SELECT Count()  
                    FROM Opportunity 
                    WHERE closedate >= :skey_CloseDateBegin 
                      AND closedate <= :skey_CloseDateEnd
                      AND JP_Sales_start_date__c >= :skey_SalesStartDateBegin 
                      AND JP_Sales_start_date__c <= :skey_SalesStartDateEnd 
                      AND recordtypeid IN :rtSet];
        // Get oppList
        oppList = [ SELECT id, name, account.name, amount, stagename, closedate, recordtypeid, JP_Sales_start_Month__c, JP_Sales_start_Date__c, JP_RemainMonth__c, JP_AnnualAmount__c, JP_Budget_Amount__c, LastModifiedDate 
                    FROM Opportunity 
                    WHERE closedate >= :skey_CloseDateBegin 
                      AND closedate <= :skey_CloseDateEnd
                      AND JP_Sales_start_date__c >= :skey_SalesStartDateBegin 
                      AND JP_Sales_start_date__c <= :skey_SalesStartDateEnd 
                      AND recordtypeid IN :rtSet 
                    ORDER BY LastModifiedDate, Name
                    LIMIT :numOfRows];
      } catch (Exception e) {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR: ' + e.getMessage()));
      }
      if (oppList.size() == 0) {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'WARNING: No Search Results.'));
      }
    }
    
    public void execute() {
      Set<Id> reDivideIds = new Set<Id>();
      JPExecuteOpportunityTriggerCS__c CS = JPExecuteOpportunityTriggerCS__c.getInstance();
      List<String> CCemails = new List<String>();
      
      if(CS.LogEmailCC1__c != NULL) CCemails.add(CS.LogEmailCC1__c);
      if(CS.LogEmailCC2__c != NULL) CCemails.add(CS.LogEmailCC2__c);
      if(CS.LogEmailCC3__c != NULL) CCemails.add(CS.LogEmailCC3__c);
      
      try {
        for (Opportunity opp : oppList) {
          reDivideIds.add(opp.id);
        }
      
        executeFuture(reDivideIds, CCemails);
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'処理が開始されました。'));
      } catch (Exception e) {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR: ' + e.getMessage()));
      }
    }
    
    @future
    public static void executeFuture(Set<ID> idSet, List<String> CCemails) {
      DateTime starttime = DateTime.Now();
      String msg = '';
      Boolean excFlag = false;
      
      System.Debug('JP_EOTC: Entering executeFuture().');
          
      try {
        JP_OpportunityBusiness business = new JP_OpportunityBusiness();
        System.Debug('JP_EOTC: idSet.size() = ' + idSet.size() + '.');
        
        if(idSet.size() > 0){
          System.Debug('JP_EOTC: Calling JP_OpportunityBusiness.reDevideRevenueSchaduleBySalesStartDate().');
          business.reDevideRevenueSchaduleBySalesStartDate(idSet);
          System.Debug('JP_EOTC: JP_OpportunityBusiness.reDevideRevenueSchaduleBySalesStartDate() completed.');
        }
      } catch (Exception e) {
        System.Debug('JP_EOTC: EXCEPTION: ' + e.getMessage());
        excFlag = true;
        msg = e.getMessage();      
      }

      Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

      String email_body = '';
      
      if (excFlag) {
        email.setSubject('[ERROR] JP Execute Opportunity Trigger Log');
        email_body += 'Error Message:\n';
        email_body += msg + '\n';
        email_body += 'Tried to execute JP_OpportunityBusiness trigger on following Opportunity records:\n\n';
      } else {
        email.setSubject('JP Execute Opportunity Trigger Log');
        email_body += 'Executed JP_OpportunityBusiness trigger on following Opportunity records:\n\n';
      }

      email_body += 'Start: ' + starttime + '\n';
      email_body += 'End: ' + DateTime.Now() + '\n\n';
      email_body += 'List of Opportunity IDs\n';
      email_body += 'Total: ' + idSet.size() + ' record(s)\n';
      for (Id i : idSet) {
          email_body += i + '\n';
      }
      email_body += '\nEnd of Log';
        
      email.setToAddresses(new String[]{UserInfo.getUserEmail()});
      email.setCcAddresses(CCemails);
      email.setPlainTextBody(email_body);
        
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }
}