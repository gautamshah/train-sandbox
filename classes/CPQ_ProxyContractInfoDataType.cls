//Generated by wsdl2apex

public class CPQ_ProxyContractInfoDataType {
    public class ArrayOfContractDealerType {
        public CPQ_ProxyContractInfoDataType.ContractDealerType[] ContractDealerType;
        private String[] ContractDealerType_type_info = new String[]{'ContractDealerType','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'ContractDealerType'};
    }
    public class ContractItemType {
        public Integer alternateUOMCaseQuantityField;
        public String alternateUOMField;
        public Decimal dealerNetField;
        public Decimal directPriceField;
        public DateTime effectiveDateField;
        public DateTime expirationDateField;
        public String itemDescriptionField;
        public String itemNumberField;
        public Decimal marginField;
        public Integer quantityBreakField;
        public String salesClassField;
        public Decimal suggestedResaleField;
        public String unitOfMeasureField;
        private String[] alternateUOMCaseQuantityField_type_info = new String[]{'alternateUOMCaseQuantityField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] alternateUOMField_type_info = new String[]{'alternateUOMField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] dealerNetField_type_info = new String[]{'dealerNetField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] directPriceField_type_info = new String[]{'directPriceField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] effectiveDateField_type_info = new String[]{'effectiveDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] expirationDateField_type_info = new String[]{'expirationDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] itemDescriptionField_type_info = new String[]{'itemDescriptionField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] itemNumberField_type_info = new String[]{'itemNumberField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] marginField_type_info = new String[]{'marginField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] quantityBreakField_type_info = new String[]{'quantityBreakField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] salesClassField_type_info = new String[]{'salesClassField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] suggestedResaleField_type_info = new String[]{'suggestedResaleField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] unitOfMeasureField_type_info = new String[]{'unitOfMeasureField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'alternateUOMCaseQuantityField','alternateUOMField','dealerNetField','directPriceField','effectiveDateField','expirationDateField','itemDescriptionField','itemNumberField','marginField','quantityBreakField','salesClassField','suggestedResaleField','unitOfMeasureField'};
    }
    public class ContractDealerType {
        public Integer billToField;
        public String cityField;
        public DateTime effectiveDateField;
        public DateTime expirationDateField;
        public String nameField;
        public Integer shipToField;
        public String stateField;
        public String zipCodeField;
        private String[] billToField_type_info = new String[]{'billToField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] cityField_type_info = new String[]{'cityField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] effectiveDateField_type_info = new String[]{'effectiveDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] expirationDateField_type_info = new String[]{'expirationDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] nameField_type_info = new String[]{'nameField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] shipToField_type_info = new String[]{'shipToField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] stateField_type_info = new String[]{'stateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] zipCodeField_type_info = new String[]{'zipCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'billToField','cityField','effectiveDateField','expirationDateField','nameField','shipToField','stateField','zipCodeField'};
    }
    public class ContractInfoResponseType {
        public CPQ_ProxyContractInfoDataType.ContractInfoType contractInfoField;
        public CPQ_ProxyContractInfoDataType.ArrayOfContractDealerType dealersField;
        public String errorCodeField;
        public String errorDescriptionField;
        public CPQ_ProxyContractInfoDataType.ArrayOfContractItemType itemsField;
        private String[] contractInfoField_type_info = new String[]{'contractInfoField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] dealersField_type_info = new String[]{'dealersField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] errorCodeField_type_info = new String[]{'errorCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] errorDescriptionField_type_info = new String[]{'errorDescriptionField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] itemsField_type_info = new String[]{'itemsField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'contractInfoField','dealersField','errorCodeField','errorDescriptionField','itemsField'};
    }
    public class ContractInfoRequestType {
        public String contractNumberField;
        public Boolean getContractItemsField;
        public Boolean getDealerServiceField;
        public String identityTokenField;
        public String maximumResponseRecordsField;
        private String[] contractNumberField_type_info = new String[]{'contractNumberField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] getContractItemsField_type_info = new String[]{'getContractItemsField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] getDealerServiceField_type_info = new String[]{'getDealerServiceField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] identityTokenField_type_info = new String[]{'identityTokenField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] maximumResponseRecordsField_type_info = new String[]{'maximumResponseRecordsField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'contractNumberField','getContractItemsField','getDealerServiceField','identityTokenField','maximumResponseRecordsField'};
    }
    public class ArrayOfContractItemType {
        public CPQ_ProxyContractInfoDataType.ContractItemType[] ContractItemType;
        private String[] ContractItemType_type_info = new String[]{'ContractItemType','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'ContractItemType'};
    }
    public class ContractInfoType {
        public String acquisitionCodeField;
        public String agreementNumberField;
        public String alternateSiteField;
        public String commitmentFormRequiredField;
        public String commitmentTypeField;
        public String companyField;
        public String contactField;
        public String corporateContractField;
        public String currencyCodeField;
        public DateTime dateCreatedField;
        public String divisionField;
        public DateTime downloadDateField;
        public DateTime effectiveDateField;
        public DateTime expirationDateField;
        public String groupCodeField;
        public DateTime lastMaintenanceDateField;
        public String lastMaintenanceUserField;
        public Integer levelField;
        public String lineOfBusinessField;
        public String nameField;
        public String nextContractField;
        public DateTime originalEndDateField;
        public String pricingGroupField;
        public String pricingIdField;
        public String priorContractField;
        public String renewableFlagField;
        public String restrictDealerServiceField;
        public String rootContractField;
        public String statusField;
        public String territoryField;
        public String tierGroupField;
        public String typeField;
        public DateTime uploadDateField;
        private String[] acquisitionCodeField_type_info = new String[]{'acquisitionCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] agreementNumberField_type_info = new String[]{'agreementNumberField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] alternateSiteField_type_info = new String[]{'alternateSiteField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] commitmentFormRequiredField_type_info = new String[]{'commitmentFormRequiredField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] commitmentTypeField_type_info = new String[]{'commitmentTypeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] companyField_type_info = new String[]{'companyField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] contactField_type_info = new String[]{'contactField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] corporateContractField_type_info = new String[]{'corporateContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] currencyCodeField_type_info = new String[]{'currencyCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] dateCreatedField_type_info = new String[]{'dateCreatedField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] divisionField_type_info = new String[]{'divisionField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] downloadDateField_type_info = new String[]{'downloadDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] effectiveDateField_type_info = new String[]{'effectiveDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] expirationDateField_type_info = new String[]{'expirationDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] groupCodeField_type_info = new String[]{'groupCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] lastMaintenanceDateField_type_info = new String[]{'lastMaintenanceDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] lastMaintenanceUserField_type_info = new String[]{'lastMaintenanceUserField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] levelField_type_info = new String[]{'levelField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','false'};
        private String[] lineOfBusinessField_type_info = new String[]{'lineOfBusinessField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] nameField_type_info = new String[]{'nameField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] nextContractField_type_info = new String[]{'nextContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] originalEndDateField_type_info = new String[]{'originalEndDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] pricingGroupField_type_info = new String[]{'pricingGroupField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] pricingIdField_type_info = new String[]{'pricingIdField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] priorContractField_type_info = new String[]{'priorContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] renewableFlagField_type_info = new String[]{'renewableFlagField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] restrictDealerServiceField_type_info = new String[]{'restrictDealerServiceField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] rootContractField_type_info = new String[]{'rootContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] statusField_type_info = new String[]{'statusField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] territoryField_type_info = new String[]{'territoryField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] tierGroupField_type_info = new String[]{'tierGroupField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] typeField_type_info = new String[]{'typeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] uploadDateField_type_info = new String[]{'uploadDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'acquisitionCodeField','agreementNumberField','alternateSiteField','commitmentFormRequiredField','commitmentTypeField','companyField','contactField','corporateContractField','currencyCodeField','dateCreatedField','divisionField','downloadDateField','effectiveDateField','expirationDateField','groupCodeField','lastMaintenanceDateField','lastMaintenanceUserField','levelField','lineOfBusinessField','nameField','nextContractField','originalEndDateField','pricingGroupField','pricingIdField','priorContractField','renewableFlagField','restrictDealerServiceField','rootContractField','statusField','territoryField','tierGroupField','typeField','uploadDateField'};
    }
}