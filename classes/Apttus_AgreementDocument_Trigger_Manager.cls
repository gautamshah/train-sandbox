/*
Manager class to handle execution limits and method calls as a result of triggers
on the Apttus__Agreement_Document__c object.

CHANGE HISTORY
===============================================================================
DATE       NAME					DESC
05/25/2016 Bryan Fry			Created
===============================================================================
*/
public class Apttus_AgreementDocument_Trigger_Manager {
    @TestVisible Static Integer SendScrubPOActivationEmail_Exec_Num = 1;
    
    Boolean beforeInsert, beforeUpdate, afterInsert, afterUpdate;
    
    List<Apttus__Agreement_Document__c> newList, oldList; 
    Map<Id,Apttus__Agreement_Document__c> oldMap, newMap;
    
    public Apttus_AgreementDocument_Trigger_Manager(Boolean isInsert, 
		                                            Boolean isUpdate, 
		                                            Boolean isBefore, 
		                                            Boolean isAfter, 
		                                            List<Apttus__Agreement_Document__c> triggerNew, 
		                                            List<Apttus__Agreement_Document__c> triggerOld, 
		                                            Map<Id,Apttus__Agreement_Document__c> triggerOldMap, 
		                                            Map<Id,Apttus__Agreement_Document__c> triggerNewMap){
        beforeInsert = ((isInsert) && (isBefore));
        beforeUpdate = ((isUpdate) && (isBefore));
        afterInsert = ((isInsert) && (isAfter));
        afterUpdate = ((isUpdate) && (isAfter));
        
        newList = triggerNew;
        oldList = triggerOld;
        oldMap = triggerOldMap;
        newMap = triggerNewMap;
    }
    
    public void execute(){
        if (afterInsert) {
		    if (Apttus_AgreementDocument_Trigger_Manager.SendScrubPOActivationEmail_Exec_Num > 0) {
		        CPQ_AgreementProcesses.BatchScrubPOActivationEmail(newList);
		        Apttus_AgreementDocument_Trigger_Manager.SendScrubPOActivationEmail_Exec_Num --;
		  	}
        }
    }
}