/****************************************************************************************
* Name    : ActivityFieldMigrator
* Author  : Gautam Shah
* Date    : 4/20/2016
* Purpose : Used to migrate custom Activity fields into the Activity_Detail__c object to free up those fields.
* 
* Dependancies: 
* Referenced By: ActivityMigratorBatch.apxc
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
*   DATE            AUTHOR                  CHANGE
*   ----            ------                  ------
*   
* ========================
* = TO-DOS =
* ========================
*   
*****************************************************************************************/
public class ActivityFieldMigrator 
{
    public static boolean migrateEventValues(List<Event> events, Map<String,String> fieldMapping)
    {
        List<Activity_Detail__c> actDetails = new List<Activity_Detail__c>();
        for(Event e : events)
        {
            Activity_Detail__c ad = new Activity_Detail__c();
            for(String destField : fieldMapping.keySet())
            {
                ad.put(destField, e.get(fieldMapping.get(destField)));
            }
            actDetails.add(ad);
        }
        try
        {
            insert actDetails;
        }
        catch(Exception e)
        {
            System.debug(e.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean migrateTaskValues(List<Task> tasks, Map<String,String> fieldMapping)
    {
        List<Activity_Detail__c> actDetails = new List<Activity_Detail__c>();
        for(Task t : tasks)
        {
            Activity_Detail__c ad = new Activity_Detail__c();
            for(String destField : fieldMapping.keySet())
            {
                ad.put(destField, t.get(fieldMapping.get(destField)));
            }
            actDetails.add(ad);
        }
        try
        {
            insert actDetails;
        }
        catch(Exception e)
        {
            System.debug(e.getMessage());
            return false;
        }
        return true;
    }
}