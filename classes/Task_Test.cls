/*
Description: This test class creates the necessary code coverage for the xTriggerDispatcher
             class and the trigger definition associated with the trigger:  Task
        Note: Do not put test methods for the classes you call, just cause the trigger to fire!
              If you want to

     Created: 02/02/2016
      Author: Paul Berglund

History
02/02/2016 Paul Berglund    Initial version
02/15/2016 Paul Berglund    Changed Setup() from @testSetup to regular method and added
                            call to each @isTest to fix a problem with SF calling it multiple
                            times for each test method
03/10/2016 Paul Berglund    Added test for other languages
05/02/2016 Paul Berglund    Updated for changes made to fix COE reported issue
*/
@isTest(seeAllData=false)
private class Task_Test {

	static Set<string> languages = new Set<string>{ 'en_US', 'en_GB', 'zh_CN', 'fr' };

    static User currentUser()
    {
		return cpqUser_c.CurrentUser;
    }

	static Task currentTask(Id id)
	{
		if(id == null)
			return
				[SELECT Id,
				        Subject
				 FROM Task];
		else
			return
				[SELECT Id,
					    Subject
		     	 FROM Task
		     	 WHERE Id = :id
		     	 LIMIT 1];
	}

    // Called by every test method
    static void Setup()
    {
        // Required fields
        Map<Id, RecordType> rts = RecordType_u.fetch(Task.class);
        RecordType rt = rts.values().get(0);
        Task target = new Task();
        target.RecordTypeId = rt.Id;
        target.Subject = 'Insert Test';
        insert target;
    }

    @isTest
    static void testInsert() {
		User u = currentUser();

		for (string language : languages) {
			u.LanguageLocaleKey = language;
			update u;

			system.runas(u) {
		        Setup();

        		// Insert
        		Task target = currentTask(null);
        		system.assertNotEquals(
        			target.Id,
        			null,
        			language + ': A record should have been created');
        		delete target;
			}
		}
    }

    @isTest
    static void testUpsert() {
		User u = currentUser();

		for (string language : languages) {
			u.LanguageLocaleKey = language;
			update u;

			system.runas(u) {
		        Setup();

		        // Upsert
        		Task target = currentTask(null);
		        target.Subject = 'Upsert Test';
		        upsert target;
		        target = currentTask(target.Id);
		        system.assertEquals(
		        	target.Subject,
		        	'Upsert Test',
		        	language + ': Existing record should be updated');
		        delete target;
			}
		}
    }

    @isTest
    static void testUpdate() {
		User u = currentUser();

		for (string language : languages) {
			u.LanguageLocaleKey = language;
			update u;

			system.runas(u) {
		        Setup();

		        // Update
        		Task target = currentTask(null);
		        target.Subject = 'Update Test';
		        update target;
		        target = currentTask(target.Id);
        		system.assertEquals(
        			target.Subject,
        			'Update Test',
        			language + ': Existing record should be updated');
        		delete target;
			}
		}
    }

    @isTest
    static void testDelete() {
		User u = currentUser();

		for (string language : languages) {
			u.LanguageLocaleKey = language;
			update u;

			system.runas(u) {
		        Setup();

		        // Delete
        		Task target = currentTask(null);
		        delete target;
		        try {
		            target = currentTask(target.Id);
		            system.assert(
		            	false,
		            	language + ': Record should have been deleted');
		        }
		        catch (Exception ex) {
		        }
			}
		}
    }

    @isTest
    static void CPQ_Trigger_Profile() {
		User u = currentUser();

		for (string language : languages) {
			u.LanguageLocaleKey = language;
			update u;

			system.runas(u) {
		        Setup();

		        // CPQ Trigger Profile
		        CPQ_Trigger_Profile__c p
		        	= new CPQ_Trigger_Profile__c(
		        		SetupOwnerId  = cpqUser_c.CurrentUser.Id,
		        		Task__c = true);
		        upsert p;

		        // Upsert
        		Task target = currentTask(null);
		        target.Subject = 'Upsert Test';
		        upsert target;
		        target = currentTask(target.Id);
		        system.assertEquals(
		        	target.Subject,
		        	'Upsert Test',
		        	language + ': Existing record should have changed');
		        delete target;
		        delete p;
			}
		}
    }
}