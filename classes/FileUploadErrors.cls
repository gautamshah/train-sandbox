public class FileUploadErrors {
    
    public List<String> RecordErrors{get;set;}
    
    public static String currencies = Label.PartnerConnect_Currencies;
    
    public static void CheckErrors(Sales_Out__c so)
    {
        String RecordErrorMessage='';
        
        if(so.Currency_Text__c!=null && so.Currency_Text__c !='' && currencies.contains('#' + so.Currency_Text__c + '#')==true)
        {
            so.CurrencyIsoCode=so.Currency_Text__c;
        }
       
        boolean checkPrice = pattern.matches('[-]{0,1}[0-9]{1,14}[.]{0,1}[0-9]{0,4}',so.Selling_Price_Text__c);
        If (checkPrice ==true)
        {
            so.Selling_Price__c=decimal.valueOf(so.Selling_Price_Text__c);
        }
        else
        	so.Selling_Price__c=null;
        
        so.Document_Date__c = null;
        if(so.Document_Date_Text__c!=null && so.Document_Date_Text__c!='')    
        {
        	String strSeperator;
	        Integer iYear, iMonth, iDate;
	        Date dt;
	        
	        if(so.Date_Format__c == 'YYYY-MM-DD' || so.Date_Format__c == 'MM-DD-YYYY' || so.Date_Format__c == 'DD-MM-YYYY')
	        	strSeperator = '-';
	        else if(so.Date_Format__c == 'YYYY/MM/DD' || so.Date_Format__c == 'MM/DD/YYYY' || so.Date_Format__c == 'DD/MM/YYYY')
	        	strSeperator = '/';
	        else if(so.Date_Format__c == 'YYYY.MM.DD' || so.Date_Format__c == 'MM.DD.YYYY' || so.Date_Format__c == 'DD.MM.YYYY')
	        	strSeperator = '.';
	        	
	        try{        	
	        	String strDate = so.Document_Date_Text__c.trim();
	        	
	        	Integer iPart1 = strDate.indexOf(strSeperator);
	        	Integer iPart2 = strDate.indexOf(strSeperator, iPart1 + 1);
	        	
	        	if(so.Date_Format__c == 'YYYY'+ strSeperator + 'MM' + strSeperator + 'DD')
	        	{
	        		iYear = Integer.ValueOf(strDate.substring(0, iPart1));
	        		iMonth = Integer.ValueOf(strDate.substring(iPart1+1, iPart2));
	        		iDate = Integer.ValueOf(strDate.substring(iPart2+1));
	        	}
	        	
	        	if(so.Date_Format__c == 'MM'+ strSeperator + 'DD' + strSeperator + 'YYYY')
	        	{
	        		iYear = Integer.ValueOf(strDate.substring(iPart2+1));
	        		iMonth = Integer.ValueOf(strDate.substring(0, iPart1));
	        		iDate = Integer.ValueOf(strDate.substring(iPart1+1, iPart2));
	        	}
	        	
	        	if(so.Date_Format__c == 'DD'+ strSeperator + 'MM' + strSeperator + 'YYYY')
	        	{
	        		iYear = Integer.ValueOf(strDate.substring(iPart2+1));
	        		iMonth = Integer.ValueOf(strDate.substring(iPart1+1, iPart2));
	        		iDate = Integer.ValueOf(strDate.substring(0, iPart1));
	        	}
	        	
	        	if(iYear < 100)
	        		iYear += 2000;
	        	
	        	dt = Date.newInstance(iYear,iMonth,iDate);
	        }
	        catch(Exception e)
	        {
	        	RecordErrorMessage = RecordErrorMessage+ label.Date_Format_Invalid + ',\n' ;
	        }
            if(dt!=null) 
            {
            	if(dt.month() != iMonth)
            		RecordErrorMessage = RecordErrorMessage+ label.Date_Format_Invalid + ',\n' ;
            	else if(dt>date.today())
                	RecordErrorMessage = RecordErrorMessage+ label.Document_date_cannot_be_greater_than_Today + ',\n';
	            else
	                so.Document_Date__c=dt;
            }
        }
        
        so.Error_Messages__c=RecordErrorMessage;
    }
    //Get a list of currencies
    public static set<String> getActiveCurrencies()
    {
    	/*
        set<String> setCur=new set<String>();
        for(CurrencyType cur:[Select IsoCode from CurrencyType where IsActive=true])
            setCur.add(cur.IsoCode);
        return setCur;*/
        return new set<String>{'KRW','CNY','USD','HKD'};
    }
    // Validate Sales Out for submission
    //Validate Formats for channel Inventory
    
    public static Map<string, string> statusValueMap()
    {
        Map<string, string> statusValues =  new Map<String, String>();
        statusValues.put('C', 'C - Consignment');
        statusValues.put('S', 'S - Saleable');
        statusValues.put('N', 'N - Non-Saleable');
        statusValues.put('C - Consignment', 'C - Consignment');
        statusValues.put('S - Saleable', 'S - Saleable');
        statusValues.put('N - Non-Saleable', 'N - Non-Saleable');
        return statusValues;
    }
    public static void CheckInventoryErrors(Channel_Inventory__c so)
    {
        String RecordErrorMessage='';
        
        Map<string, string> statusValues =statusValueMap();

        //if (so.comments__c == '')
       // {
       //     RecordErrorMessage = RecordErrorMessage+'comment cananot be Blank\n' ;
            //   ErrorHappened = 'T';
     //  }
                
        if (statusValues.keyset().contains(so.Inventory_Status__c))
            so.Inventory_Status__c=statusValues.get(so.Inventory_Status__c);
        else
        {
      //      RecordErrorMessage = RecordErrorMessage+'Invalid Status Value\n' ;
            RecordErrorMessage = RecordErrorMessage+ label.Invalid_Status_Value + '\n' ;
            
            //   ErrorHappened = 'T';
        }
        if(so.Quantity_Text__c==null||so.Quantity_Text__c=='')  
        {
            RecordErrorMessage = RecordErrorMessage+ label.quantity_is_required + ',\n';
        }
        else
        {
            boolean checkQuantity = pattern.matches('[0-9]{0,14}[.]{0,1}[0-9]{0,4}',so.Quantity_Text__c);
            If (checkQuantity ==false)
            {
          //      RecordErrorMessage = RecordErrorMessage+ 'quantity has to be Numeric,\n' ;
                RecordErrorMessage = RecordErrorMessage+ label.Quantity_has_to_be_positive_numeric + ',\n';
                // ErrorHappened = 'T';
            }
            else
                so.quantity__c = decimal.valueOf(so.Quantity_Text__c);
        }
        so.Error_Message__c=RecordErrorMessage;
    }
    
    public static void deleteChannelInventory(Id DistributorName, Id CyclePeriod,String userChoice)
    {
        //List<Channel_Inventory_Staging__c> lstStagingToDelete=[Select Id from Channel_Inventory_Staging__c where Cycle_Period__c=:CyclePeriod and Distributor_Name__c=:DistributorName];
        List<Channel_Inventory__c> lstCIToDelete=[Select Id from Channel_Inventory__c where Verification_Status__c not in ('Verified', 'Rejected') and Cycle_Period__c=:CyclePeriod and Distributor_Name__c=:DistributorName];
        //if(lstStagingToDelete!=null)
        //  delete lstStagingToDelete;
        if(lstCIToDelete!=null&&userChoice!='Append')
            delete lstCIToDelete;   
    }
    
    private static boolean alreadyRanValidate = false;


    public static boolean hasValidationRun() {
        return alreadyRanValidate;
    }
    
    public static void setValidationHasRan() {
        alreadyRanValidate = true;
    }

}