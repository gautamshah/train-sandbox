@isTest
private class MissionInMotion_Test {
    
    //Wrapper Class to Hold Totals by Org
    private class SalesOrgAgr 
    {
        String SalesOrg;
        Double CoreDonation;
        
        //Constructor
        private SalesOrgAgr (string iorg, Double er) 
        {
            SalesOrg = iorg;
            CoreDonation = er;
        }
    } 
    
    
    //Wrapper Class to Hold Totals by Area
    private class AreaAgr 
    {
        String SalesOrg;
        String Area;
        String Type;
        Double CoreDonation;
        Double VolunteeringHours;
        Integer OrdNum;
        
        //Constructor
        private AreaAgr (string iorg, string iarea, string itype, Double er, Double vh, Integer ord) 
        {
            SalesOrg = iorg;
            Area = iarea;
            Type = itype;
            CoreDonation = er;
            VolunteeringHours = vh;
            OrdNum = ord;
        }
    }
    
    
    static testMethod void TestPageFunctionality() {
        
        // Create Mission In Motion Record
        Mission_In_Motion__c mim_rec = new Mission_In_Motion__c(Sales_Organization__c = 'Strategic Partnerships and Channels',
                                                                Area__c = 'Central',
                                                                Contribution_Type__c = 'Donation',
                                                                Core_Donation__c = 10.0,
                                                                Medtronic_Match__c = true,
                                                                Charity_Name__c = 'Test Charity',
                                                                First_Name__c = 'Bob',
                                                                Last_Name__c = 'Smith',
                                                                Email__c = 'bsmithtest@medtronic.com');  
        insert mim_rec;  
        
        // Create Mission In Motion Config
        MissionInMotionConfig__c mim_config = new MissionInMotionConfig__c
        (
        Name = 'Colon_Cancer_Alliance',
        DefaultSortOrder__c = 'Dollars',
        DonationPath__c = 'http://www.google.com',
        VolunteerPath__c = 'http://www.yahoo.com'    
        );  
        
        insert mim_config; 
        
        // Create Mission In Motion Config
        MissionInMotionConfig__c mim_config2 = new MissionInMotionConfig__c
        (
        Name = 'SIFPI_Pay_It_Forward',
        DefaultSortOrder__c = 'Hours',
        DonationPath__c = 'http://www.google.com',
        VolunteerPath__c = 'http://www.yahoo.com'   
        );  
        
        insert mim_config2; 
        
        PageReference pageRef2 = Page.Colon_Cancer_Alliance; 
        Test.setCurrentPageReference(pageRef2);
        
        MissionInMotion mim = new MissionInMotion(); 
        
        // Set Variable Value
        mim.SalesOrg = 'GSP';
        mim.Area = 'Central';
        mim.ConType = 'Donation';
        mim.ContributionAmount = '10';
        mim.MedtronicMatch = true;
        mim.CharityName = 'Test Charity';
        mim.UserFirstname = 'Bob';
        mim.UserLastname = 'Smith';
        mim.UserEmail = 'bsmithtest@medtronic.com';
        mim.TotalValue = 1000;
        mim.ShowDetail = true;
        mim.LinkTitle = 'More Details';
        
        // Execute Methods
        mim.CreateMIM();
        mim.getGrandTotal();
        mim.GetMetricList();
        mim.getOgTotals();
        mim.getSalesOrganization();
        mim.PullArea();
        mim.getContribTypes();
        mim.ValidationMsgReturn();
        mim.ValidationMsgReturn2();
        mim.getGrandTotalHours();
        
        
        
        PageReference pageRef3 = Page.SIFPI_Pay_It_Forward; 
        Test.setCurrentPageReference(pageRef3);
        
        MissionInMotion mim2 = new MissionInMotion(); 
        
        // Set Variable Value
        mim2.SalesOrg = 'GSP';
        mim2.Area = 'Central';
        mim2.ConType = 'Volunteer';
        mim2.ContributionAmount = '10';
        mim2.MedtronicMatch = true;
        mim2.CharityName = 'Test Charity';
        mim2.UserFirstname = 'Bob';
        mim2.UserLastname = 'Smith';
        mim2.UserEmail = 'bsmithtest@medtronic.com';
        mim2.TotalValue = 1000;
        mim2.ShowDetail = true;
        mim2.LinkTitle = 'More Details';
        mim2.MyComment = 'Test Comment';
        
        // Execute Methods
        mim2.CreateMIM();
        mim2.getGrandTotal();
        mim2.GetMetricList();
        mim2.getOgTotals();
        mim2.getSalesOrganization();
        mim2.PullArea();
        mim2.getContribTypes();
        mim2.ValidationMsgReturn();
        mim2.ValidationMsgReturn2();       
        mim2.getGrandTotalHours();
        
    }
}