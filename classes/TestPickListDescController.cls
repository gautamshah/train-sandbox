@isTest
private class TestPickListDescController 
{
    static testMethod void runTest() {
    Test.startTest(); 
    RecordType  RtID = [Select Name, id from RecordType where Name =: 'ANZ-Buying Group' limit 1];
    ApexPages.currentPage().getParameters().put('id', '001');
    ApexPages.currentPage().getParameters().put('recordTypeId', RtID.ID);
    ApexPages.currentPage().getParameters().put('recordTypeName', RtID.Name);
    ApexPages.currentPage().getParameters().put('sobjectType', 'Account'); 
    ApexPages.currentPage().getParameters().put('picklistFieldName', 'Type');     
    PickListDescController con = new PickListDescController();
    //boolean b = PickListDescController.isBlank();
    
    Test.stopTest();
    }
static testMethod void runTest1() {
    Test.startTest(); 
      testUtility tu = new testUtility();
    Account sellto = tu.testAccount('US-Healthcare Facility','Test Sell-To');
    RecordType  RtID = [Select Name, id from RecordType where Name =: 'ANZ-Buying Group' limit 1];
    ApexPages.currentPage().getParameters().put('id', sellto.id);
    ApexPages.currentPage().getParameters().put('recordTypeId', RtID.ID);
    ApexPages.currentPage().getParameters().put('recordTypeName', RtID.Name);
    ApexPages.currentPage().getParameters().put('sobjectType', null); 
    ApexPages.currentPage().getParameters().put('picklistFieldName', 'Type');     
    PickListDescController con = new PickListDescController();
    //boolean b = PickListDescController.isBlank();
    
    Test.stopTest();
    }
}