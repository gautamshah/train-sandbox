/****************************************************************************************
 Name    : Attachment_td (Trigger Dispatcher) 
 Author  : Paul Berglund
 Date    : 08/17/2016
 
 How To Use:  Copy the main() method and place it in your class - then reference it from
              this class and pass all the values to it.
              IMMEDIATELY PROMOTE TO PRODUCTION
              - comment out any code in your class so that the main() in your class doesn't
                reference anything
              - create a change set with this class and your class and promote it to
                production.  Since there is no logic in your class being called, it
                doesn't require any testing at this point.
                
              That will allow you to manage your logic separately from all the other
              projects using Attachment and you won't have to coordinate deployments
              as stringently.
              
 ========================
 = MODIFICATION HISTORY =
 ========================
 DATE        AUTHOR           CHANGE
 ----        ------           ------
 08/17/2016  Paul Berglund    Refactored code in other trigger and classes being called
 						      from those triggers into their own classes.
*****************************************************************************************/
public with sharing class Attachment_td
{
    public static void main(
        boolean isExecuting,
        boolean isInsert,
        boolean isUpdate,
        boolean isDelete,
        boolean isBefore,
        boolean isAfter,
        boolean isUndelete,
        List<Attachment> newList,
        Map<Id, Attachment> newMap,
        List<Attachment> oldList,
        Map<Id, Attachment> oldMap,
        integer size)
    {
        dmAttachment.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size);
/*
        Samples_c.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size);
*/
    }
}