global class AddMembersToPublicgroupsSchedule implements Schedulable{
    
    /****************************************************************************************
     * Name    : AddMembersToPublicgroupsSchedule
     * Author  : Lakhan Dubey
     * Date    : 30/09/2014  
     * Purpose : Schedular for the AddMembersToPublicgroups class            
     * Dependencies: AddMembersToPublicgroups Apex Class
     *****************************************************************************************/
    global void execute(SchedulableContext sc) 
    {
     
       AddMembersToPublicgroups b= new AddMembersToPublicgroups();
            
       Database.executeBatch(b);
        

}
}