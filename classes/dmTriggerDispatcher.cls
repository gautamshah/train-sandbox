public with sharing abstract class dmTriggerDispatcher
{
    public class NotBulkifiedException extends Exception { }
    public static void ThrowNotBulkifiedException()
    {
        throw new NotBulkifiedException(
            'This trigger dispatcher is not bulkified yet - ' +
            'it can only handle 1 object at a time');
    }
}