/**
Batch class to create partner contracts from staging table

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11/21      Yuli Fintescu       Created
11/02/2016   Paul Berglund   Commented out to save space, but keep in case we
                             do something similar again
===============================================================================

To Run
-------
        ****TURN OFF EMAILS!!!****
        CPQ_DM_Batch_Contract p = new CPQ_DM_Batch_Contract();
        Database.executeBatch(p);

Data clean up
-------------

CPQ: 005K0000002GNNs
QA: 005K0000002GHCZ
IC: 005U0000000eoT2
Prod:

run once. test in force.com explorer first
===============================================
delete [SELECT ID, Name, createddate, LastModifiedDate 
        FROM Partner_Root_Contract__c 
        where commitment_type__c = null and 
            createdbyid in ('005K0000002GNNs', '005U0000000eoT2') and 
            LastModifiedDate >= 2015-02-06T00:00:00.000Z and LastModifiedDate < 2015-02-12T00:00:00.000Z];

run once. test in force.com explorer first
===============================================
List<Partner_Root_Contract__c> rooots = new List<Partner_Root_Contract__c>();
for (Partner_Root_Contract__c r : [SELECT ID, Account__c, Name, lastmodifiedbyid, lastmodifiedDate, createdDate
                                FROM Partner_Root_Contract__c 
                                where commitment_type__r.Name in ('IP', 'IR') and 
                                lastmodifiedbyid in ('005K0000002GNNs', '005U0000000eoT2') and Account__c <> null and
                                createdDate >= 2015-02-19T00:00:00.000Z and createdDate < 2015-02-20T00:00:00.000Z]) {
    if (r.Account__c != null) {
        r.Account__c = null;
        rooots.add(r);
    }
}

if (rooots.size() > 0)
    update rooots;
    
run once.
===============================================
for (List<CPQ_STG_Partner_Contract__c> ls : [Select ID, STGPC_DM_Error__c From CPQ_STG_Partner_Contract__c ]) {
    List<CPQ_STG_Partner_Contract__c> lines = new List<CPQ_STG_Partner_Contract__c>();
    
    for (CPQ_STG_Partner_Contract__c l : ls) {
        if (l.STGPC_DM_Error__c != null) {
            l.STGPC_DM_Error__c = null;
            lines.add(l);
        }
    }
    
    if (lines.size() > 0)
        update lines;
}

Clean up Stages:
----------------
delete [SELECT ID FROM CPQ_STG_Partner_Contract__c LIMIT 10000];
*/
global class CPQ_DM_Batch_Contract // implements Database.Batchable<sObject>
{
/*
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select STGPC_RootContractNumber__c,  ' + 
                            'STGPC_MigrationDate__c,  ' + 
                            'STGPC_IsRoot__c,  ' + 
                            'STGPC_FacilityPristine__c,  ' + 
                            'STGPC_ExpirationDate__c,  ' + 
                            'STGPC_EffectiveDate__c,  ' + 
                            'STGPC_Direction__c,  ' + 
                            'STGPC_Deal_External_ID__c,  ' + 
                            'STGPC_DealType__c,  ' + 
                            'STGPC_DealID__c,  ' + 
                            'STGPC_DM_Error__c,  ' + 
                            'STGPC_ContractStatus__c,  ' + 
                            'STGPC_ContractNumber__c,  ' + 
                            'STGPC_ContractName__c,  ' + 
                            'STGPC_Account_External_ID__c,  ' + 
                            'Name, Id, STGPC_Agreement__c  ' + 
                        'From CPQ_STG_Partner_Contract__c ' +
                        'Order by STGPC_Deal_External_ID__c asc, STGPC_IsRoot__c desc, STGPC_RootContractNumber__c desc';// Where STGPC_Deal_External_ID__c in (\'D-143187\', \'D-143189\', \'D-143199\')
        return Database.getQueryLocator(query);
    }
    
    private class ContractRootItem {
        //corresponding stage record
        CPQ_STG_Partner_Contract__c stage;
        
        //related partner contract
        Map<String, CPQ_STG_Partner_Contract__c> items;
        
        //a root can have more than one deals
        Map<String, ContractRootAgreementItem> agreements;
        
        //if root record already exists in SF
        Partner_Root_Contract__c existing;
        
        //linked to account
        Account account;
        
        //direction is multi-picklist
        Set<String> directions;
        
        String name;
        Date effectiveDate;
        Date expirationDate;
        String direction;
    }
    
    private class ContractRootAgreementItem {
        //corresponding stage record
        CPQ_STG_Partner_Contract__c stage;
        
        //linked to agreement
        Apttus__APTS_Agreement__c agreement;
    }
    
    global void execute(Database.BatchableContext BC, List<CPQ_STG_Partner_Contract__c > scope) {
//=========================================================
//    Extract data from stages to get existing records
//=========================================================
        Set<String> acctExternalIDs = new Set<String>();
        Set<String> rootNos = new Set<String>();
        Set<String> dealExternalIDs = new Set<String>();
        for (CPQ_STG_Partner_Contract__c p : scope) {
            if (p.STGPC_Account_External_ID__c != null)
                acctExternalIDs.add(p.STGPC_Account_External_ID__c);
            
            if (p.STGPC_Deal_External_ID__c != null)
                dealExternalIDs.add(p.STGPC_Deal_External_ID__c);
            
            if (p.STGPC_IsRoot__c == true && p.STGPC_ContractNumber__c != null)
                rootNos.add(p.STGPC_ContractNumber__c);
        }
        
        Map<String, Apttus__APTS_Agreement__c> existingAgreements = new Map<String, Apttus__APTS_Agreement__c> ();
        for (Apttus__APTS_Agreement__c a : [Select ID, Apttus__Account__c, Legacy_External_ID__c, Partner_Root_Contract__c From Apttus__APTS_Agreement__c Where Legacy_External_ID__c in: dealExternalIDs]) {
            existingAgreements.put(a.Legacy_External_ID__c, a);
        }
        
        Map<String, Partner_Root_Contract__c> existingRoots = new Map<String, Partner_Root_Contract__c>();
        for (Partner_Root_Contract__c r : [Select ID, Root_Contract_Number__c, Direction__c From Partner_Root_Contract__c Where Root_Contract_Number__c in: rootNos]) {
            existingRoots.put(r.Root_Contract_Number__c, r);
        }
        
        Map<String, Account> accounts = new Map<String, Account> ();
        for (Account a : [Select Account_External_ID__c, ID From Account Where Account_External_ID__c in: acctExternalIDs]) {
            accounts.put(a.Account_External_ID__c, a);
        }
        
//========================================================
//    Extract data from stages to build linkage structures
//========================================================
        Map<String, ContractRootItem> roots = new Map<String, ContractRootItem>();
        for (CPQ_STG_Partner_Contract__c p : scope) {
            if (p.STGPC_IsRoot__c == true) {
                ContractRootItem root;
                
                if (roots.containsKey(p.STGPC_ContractNumber__c)) {
                    root = roots.get(p.STGPC_ContractNumber__c);
                } else {
                    root = new ContractRootItem();
                    root.stage = p;
                    
                    root.name = p.STGPC_ContractNumber__c;
                    
                    //related partner contract
                    root.items = new Map<String, CPQ_STG_Partner_Contract__c>();
                    
                    root.directions = new Set<String>();
                    
                    //linked agreements
                    root.agreements = new Map<String, ContractRootAgreementItem>();
                    
                    //linked account
                    if (!accounts.containsKey(p.STGPC_Account_External_ID__c)) {
                        p.STGPC_DM_Error__c = (p.STGPC_DM_Error__c == null ? '' : p.STGPC_DM_Error__c) + 'Account record is not found.\r\n';
                    } else {
                        root.account = accounts.get(p.STGPC_Account_External_ID__c);
                    }
                    
                    //already exists in SF?
                    if (existingRoots.containsKey(p.STGPC_ContractNumber__c))
                        root.existing = existingRoots.get(p.STGPC_ContractNumber__c);
                    
                    roots.put(p.STGPC_ContractNumber__c, root);
                }
                
                //multi-picklist 
                if (p.STGPC_Direction__c != null) {
                    root.directions.add(p.STGPC_Direction__c);
                    
                    String dir = '';
                    for (String d : root.directions)
                        dir = dir + d + '; ';
                    root.direction = dir.endsWith('; ') ? dir.substring(0, dir.length() - 2) : dir;
                }
                
                if (!existingAgreements.containsKey(p.STGPC_Deal_External_ID__c)) {
                    p.STGPC_DM_Error__c = (p.STGPC_DM_Error__c == null ? '' : p.STGPC_DM_Error__c) + 'Agreement record is not found.\r\n';
                } else if (!root.agreements.containsKey(p.STGPC_Deal_External_ID__c)) {
                    ContractRootAgreementItem item = new ContractRootAgreementItem();
                    item.agreement = existingAgreements.get(p.STGPC_Deal_External_ID__c);
                    item.stage = p;
                    item.stage.STGPC_Agreement__c = existingAgreements.get(p.STGPC_Deal_External_ID__c).ID;
                    root.agreements.put(p.STGPC_Deal_External_ID__c, item);
                } else {
                    p.STGPC_DM_Error__c = (p.STGPC_DM_Error__c == null ? '' : p.STGPC_DM_Error__c) + 'Contract superseeded.\r\n';
                }
            }
        }
        
        //related partner contract stages
        for (CPQ_STG_Partner_Contract__c p : scope) {
            if (p.STGPC_IsRoot__c == false) {
                if (p.STGPC_RootContractNumber__c == null ||
                        !roots.containsKey(p.STGPC_RootContractNumber__c)) {
                    p.STGPC_DM_Error__c = (p.STGPC_DM_Error__c == null ? '' : p.STGPC_DM_Error__c) + 'Root does not exist. Skip.\r\n';
                } else {
                    ContractRootItem root = roots.get(p.STGPC_RootContractNumber__c);
                    if (p.STGPC_ContractName__c != null)
                        root.name = p.STGPC_ContractName__c;
                    
                    root.effectiveDate = p.STGPC_EffectiveDate__c;
                    root.expirationDate = p.STGPC_ExpirationDate__c;
                    
                    root.items.put(p.STGPC_ContractNumber__c, p);
                }
            }
        }
        System.Debug('*** roots ' + roots);

//=========================================================
//    Creating root contracts
//=========================================================
        List<Partner_Root_Contract__c> toRootUpsert = new List<Partner_Root_Contract__c>();
        List<CPQ_STG_Partner_Contract__c> rootOneToOne = new List<CPQ_STG_Partner_Contract__c>();
        for (ContractRootItem p : roots.values()) {
            Partner_Root_Contract__c root = new Partner_Root_Contract__c();
            
            root.Name = p.stage.STGPC_ContractNumber__c;
            root.Root_Contract_Number__c = p.stage.STGPC_ContractNumber__c;
            if (p.existing == null) {
                root.Root_Contract_Name__c = p.name;
                root.OwnerID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
                //root.CreatedByID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
                //root.LastModifiedByID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
            }
            
            if (p.effectiveDate != null)
                root.Effective_Date__c = p.effectiveDate;
            
            if (p.expirationDate != null)
                root.Expiration_Date__c = p.expirationDate;
            
            root.Account__c = (p.account == null ? null : p.account.ID);
            root.Direction__c = p.direction;
            //root.Legacy_Deal_ID__c = p.stage.STGPC_Deal_External_ID__c;
            //root.Sales_Org__c;
            
            toRootUpsert.add(root);
            rootOneToOne.add(p.stage);
        }
        System.Debug('*** toRootUpsert ' + toRootUpsert);
        
        if (toRootUpsert.size() > 0) {
            Database.UpsertResult[] results = Database.upsert(toRootUpsert, Schema.Partner_Root_Contract__c.Root_Contract_Number__c, false);
            
            for (Integer i = 0; i < results.size(); i ++) {
                Database.UpsertResult result = results[i];
                CPQ_STG_Partner_Contract__c stage = rootOneToOne[i];
                
                if (!result.isSuccess()) {
                    String error = '';
                    for(Database.Error err : result.getErrors())
                        error = error + (err.getStatusCode() + ' - ' + err.getMessage()) + '\r\n';
                    stage.STGPC_DM_Error__c = (stage.STGPC_DM_Error__c == null ? '' : stage.STGPC_DM_Error__c) + 'Error in creating Root Contract: ' + error + '\r\n';
                }
            }
        }
        
//=========================================================
//    Update agreement Partner_Root_Contract__c field
//=========================================================
        Map<String, ID> idMap = new Map<String, ID>();
        for (Partner_Root_Contract__c r : toRootUpsert) {
            idMap.put(r.Root_Contract_Number__c, r.Id);
        }
        
        Set<ID> uniqAgmts = new Set<ID>();
        List<Apttus__APTS_Agreement__c> toAgmtUpdate = new List<Apttus__APTS_Agreement__c>();
        List<CPQ_STG_Partner_Contract__c> agmtOneToOne = new List<CPQ_STG_Partner_Contract__c>();
        for (ContractRootItem p : roots.values()) {
            for (ContractRootAgreementItem item : p.agreements.values()) {
                //the same agreement might have already been set
                if (uniqAgmts.contains(item.agreement.Id))
                    continue;
                
                if (idMap.containsKey(p.stage.STGPC_ContractNumber__c))
                    item.agreement.Partner_Root_Contract__c = idMap.get(p.stage.STGPC_ContractNumber__c);
                
                if (p.effectiveDate != null)
                    item.agreement.Apttus__Contract_Start_Date__c = p.effectiveDate;
                
                if (p.expirationDate != null) {
                    item.agreement.Apttus__Contract_End_Date__c = p.expirationDate;
                    item.agreement.Apttus__Perpetual__c = false;
                }
                
                toAgmtUpdate.add(item.agreement);
                agmtOneToOne.add(item.stage);
                uniqAgmts.add(item.agreement.Id);
            }
        }
        System.Debug('*** toAgmtUpdate ' + toAgmtUpdate);
        
        if (toAgmtUpdate.size() > 0) {
            Database.SaveResult[] results = Database.update(toAgmtUpdate, false);
            
            for (Integer i = 0; i < results.size(); i ++) {
                Database.SaveResult result = results[i];
                CPQ_STG_Partner_Contract__c stage = agmtOneToOne[i];
                
                if (!result.isSuccess()) {
                    String error = '';
                    for(Database.Error err : result.getErrors())
                        error = error + (err.getStatusCode() + ' - ' + err.getMessage()) + '\r\n';
                    stage.STGPC_DM_Error__c = (stage.STGPC_DM_Error__c == null ? '' : stage.STGPC_DM_Error__c) + 'Error in update agreement: ' + error + '\r\n';
                }
            }
        }
        
//=========================================================
//    Upsert related contracts
//=========================================================
        List<Related_Partner_Contract__c> toContUpsert = new List<Related_Partner_Contract__c>();
        List<CPQ_STG_Partner_Contract__c> contOneToOne = new List<CPQ_STG_Partner_Contract__c>();
        for (ContractRootItem p : roots.values()) {
            for (CPQ_STG_Partner_Contract__c item : p.items.values()) {
                Related_Partner_Contract__c cont = new Related_Partner_Contract__c();
                
                cont.Name = item.STGPC_ContractNumber__c;
                cont.Root_Contract__r = new Partner_Root_Contract__c(Root_Contract_Number__c = p.stage.STGPC_ContractNumber__c);
                
                toContUpsert.add(cont);
                contOneToOne.add(item);
            }
        }
        System.Debug('*** toContUpsert ' + toContUpsert);
        
        //upsert related contracts
        if (toContUpsert.size() > 0) {
            Database.UpsertResult[] results = Database.upsert(toContUpsert, Schema.Related_Partner_Contract__c.Name, false);
            
            for (Integer i = 0; i < results.size(); i ++) {
                Database.UpsertResult result = results[i];
                CPQ_STG_Partner_Contract__c stage = contOneToOne[i];
                
                if (!result.isSuccess()) {
                    String error = '';
                    for(Database.Error err : result.getErrors())
                        error = error + (err.getStatusCode() + ' - ' + err.getMessage()) + '\r\n';
                    stage.STGPC_DM_Error__c = (stage.STGPC_DM_Error__c == null ? '' : stage.STGPC_DM_Error__c) + 'Error in creating Related Contract: ' + error + '\r\n';
                }
            }
        }
        
        if (scope.size() > 0)
            database.update(scope, false);
    }
    
    global void finish(Database.BatchableContext BC) {}
*/
}