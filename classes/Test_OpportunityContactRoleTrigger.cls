@isTest

private class Test_OpportunityContactRoleTrigger  
{
    
    static testMethod void insertNewOppContactRole() 
    {
    Test.StartTest();
    RecordType rtOpp=[Select Id from RecordType where DeveloperName='ANZ_Opportunity' Limit 1];
    RecordType rtContact=[Select Id from RecordType where DeveloperName='In_Process_Connected_Contact' Limit 1];
    RecordType rtAcc=[Select Id from RecordType where DeveloperName='ASIA_Healthcare_Facility' Limit 1];
    Account newaccount=new Account();
    newaccount.Name='TestAccount';
    newaccount.RecordTypeId=rtAcc.Id;
    insert newaccount;  
    
    Contact newcontact=new Contact();
    newcontact.LastName='TestContact';
    newcontact.FirstName='Testing';
    newcontact.AccountId=newaccount.Id;  
    newcontact.RecordTypeId=rtContact.Id;
    insert newcontact;
    
    Contact newcont=new Contact();
    newcont.LastName='TestContact2';
    newcont.FirstName='Testing2';
    newcont.AccountId=newaccount.Id;
    newcont.RecordTypeId=rtContact.Id;
    insert newcont;
    
    Opportunity newOpp=new Opportunity();
    newOpp.RecordTypeId=rtOpp.Id;
    newOpp.Name='TestOpp';
    newOpp.Account=newaccount;
    newOpp.Capital_Disposable__c='Capital';
    newOpp.StageName='Identify';
    newOpp.CloseDate=Date.today().addDays(+60);
    insert newOpp;
    
    Opportunity_Contact_Role__c OppContactRole=new Opportunity_Contact_Role__c();
    OppContactRole.Contact_Role__c='Business User';
    OppContactRole.Level_of_Support__c='Champion';
    OppContactRole.Opportunity_Contact__c=newcontact.Id;
    OppContactRole.Opportunity_del__c=newOpp.Id;
    insert OppContactRole;
    
    OppContactRole.Opportunity_Contact__c=newcont.Id;
    update OppContactRole;
      
    delete OppContactRole;
    Test.stopTest();
    }
    
    
    
}