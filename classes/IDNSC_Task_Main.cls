/****************************************************************************************
* Name    : Class: IDNSC_Task_Main
* Author  : Gautam Shah
* Date    : 8/12/2014
* Purpose : A wrapper for static functionality for IDNSC_Relationship__c records
* 
* Dependancies: 
* 	Called By: IDNSC_Task_TriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
public with sharing class IDNSC_Task_Main 
{
	public static void postCompletionToGroup(List<IDNSC_Task__c> completedTasks)
	{
		Set<Id> relIDs = new Set<Id>();
		for(IDNSC_Task__c task : completedTasks)
		{
			relIDs.add(task.IDNSC_Parent__c);
		}
		Map<Id, IDNSC_Relationship__c> relMap = new Map<Id, IDNSC_Relationship__c>([Select Id, Name From IDNSC_Relationship__c Where Id In :relIDs]);
		List<String> groupNames = new List<String>();
		for(IDNSC_Relationship__c rel : relMap.values())
		{
			groupNames.add(rel.Name);
		}
		List<CollaborationGroup> groups = new List<CollaborationGroup>([Select Id, Name From CollaborationGroup Where Name In :groupNames]);
		Map<String, Id> cGroupMap = new Map<String, Id>();
		for(CollaborationGroup cGroup : groups)
		{
			cGroupMap.put(cGroup.Name, cGroup.Id);
		}
		for(IDNSC_Task__c task : completedTasks)
		{
			ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
			ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
			ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
			messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
			textSegment.text = task.Name + ' has been completed.';// by the ' + task.Role__c + '.';
			messageInput.messageSegments.add(textSegment);
			input.body = messageInput;
			input.feedElementType = ConnectApi.FeedElementType.FeedItem;
			input.subjectId = cGroupMap.get(relMap.get(task.IDNSC_Parent__c).Name);//subjectId == groupId
			try
			{
				ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, input, null);
			}
			catch(Exception e)
			{
				System.debug(e.getMessage());
			}
		}
	}
	
	public static void postAssignmentToChatter(List<IDNSC_Task__c> updatedTasks)
	{
		Set<Id> relIDs = new Set<Id>();
		Map<Id, String> relNames = new Map<Id, String>();
		for(IDNSC_Task__c task : updatedTasks)
		{
			relIDs.add(task.IDNSC_Parent__c);
		}
		List<IDNSC_Relationship__c> rels = new List<IDNSC_Relationship__c>([Select Id, Name, Current_Event_State__c, Role_Sales_Analyst__c, Role_Customer_Care__c, Role_Data_Steward__c, Role_Pricing_Specialist__c, Role_RVP__c, Role_Customer_Service__c, Role_EDI_Support__c, Role_Rebates_Tracings_Analyst__c, Role_Transportation_Analyst__c From IDNSC_Relationship__c Where Id In :relIDs]);
		Map <Id, Map <String, Id>> userRoleMap = new Map <Id, Map <String, Id>>();
		for(IDNSC_Relationship__c rel : rels)
		{
			Map <String, Id> roleMap = new Map <String, Id>();
	        roleMap.put('Sales Analyst', rel.Role_Sales_Analyst__c);
	        roleMap.put('Customer Care Manager', rel.Role_Customer_Care__c);
	        roleMap.put('Data Steward', rel.Role_Data_Steward__c);
	        roleMap.put('Pricing Specialist', rel.Role_Pricing_Specialist__c); 
	        roleMap.put('RVP', rel.Role_RVP__c);
	        roleMap.put('Customer Service', rel.Role_Customer_Service__c);
	        roleMap.put('EDI Support', rel.Role_EDI_Support__c);
	        roleMap.put('Rebates & Tracings Analyst', rel.Role_Rebates_Tracings_Analyst__c);
	        roleMap.put('Transportation Analyst', rel.Role_Transportation_Analyst__c);
	        userRoleMap.put(rel.Id,roleMap);
	        relNames.put(rel.Id, rel.Name);
		}
        for(IDNSC_Task__c idnscTask : updatedTasks)
		{
			Map <String, Id> currentRoleMap = new Map <String, Id>(userRoleMap.get(idnscTask.IDNSC_Parent__c));
	        if(currentRoleMap.keySet().contains(idnscTask.Role__c))
        	{
				ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
				ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
				ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
				ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
				messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
				//mention
				mentionSegment.id = currentRoleMap.get(idnscTask.Role__c);
				messageInput.messageSegments.add(mentionSegment);
				//text
				textSegment.text = ', the task: ' + idnscTask.Name + ' - has just been assigned to you for the ' + relNames.get(idnscTask.IDNSC_Parent__c) + ' IDNSC Relationship. You may click the title of this post to view the task details.';
				messageInput.messageSegments.add(textSegment);
				input.feedElementType = ConnectApi.FeedElementType.FeedItem;
				input.body = messageInput;
				input.subjectId = idnscTask.Id;
				try
				{
					ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, input, null);
				}
				catch(Exception e)
				{
					System.debug(e.getMessage());
				}
	        }
		} 
		IDNSC_Task_TriggerDispatcher.executedMethods.add('postAssignmentToChatter');      
	}
	
	public static void updateDaysRemaining(List<IDNSC_Task__c> changedStatusTasks)
	{
		String IDNSC_Id = changedStatusTasks[0].IDNSC_Parent__c;
		Map<String,Decimal> criticalTasks = new Map<String,Decimal>();
		for(IDNSC_Task__c task : [SELECT Duration_Days_Expected__c, Runs_in_State__c FROM IDNSC_Task__c Where Critical_Path__c = true and Status__c != 'Completed' and Status__c != 'Skipped' and IDNSC_Parent__c = :IDNSC_Id Order By Duration_Days_Expected__c ASC])
		{
			criticalTasks.put(task.Runs_in_State__c, task.Duration_Days_Expected__c);
		}
		Decimal totalDays = 0;
		for(Decimal i : criticalTasks.values())
		{
			totalDays = totalDays + i;
		}
		IDNSC_Relationship__c rel = [Select Business_Days_Remaining__c From IDNSC_Relationship__c Where Id = :IDNSC_Id Limit 1];
		rel.Business_Days_Remaining__c = totalDays;
		update rel;
		IDNSC_Task_TriggerDispatcher.executedMethods.add('updateDaysRemaining');
	}
	
	public static void lastTaskEstimatedCompletionDate(List<IDNSC_Task__c> changedTasks)
	{
		String IDNSC_Id = changedTasks[0].IDNSC_Parent__c;
		List<IDNSC_Task__c> listTask = new List<IDNSC_Task__c>([Select Finish_Date_Estimated__c From IDNSC_Task__c Where Finish_Date_Estimated__c != null And IDNSC_Parent__c = :IDNSC_Id Order By Finish_Date_Estimated__c DESC Limit 1]);
		List<IDNSC_Relationship__c> listRel = new List<IDNSC_Relationship__c>([Select Last_Task_Completion_Date__c From IDNSC_Relationship__c Where Id = :IDNSC_Id Limit 1]);
		if(!listTask.isEmpty() && !listRel.isEmpty())
		{
			listRel[0].Last_Task_Completion_Date__c = listTask[0].Finish_Date_Estimated__c;
			update listRel;
		}
		IDNSC_Task_TriggerDispatcher.executedMethods.add('lastTaskEstimatedCompletionDate');
	}
	
	public static void calculateStartDates_Update(Map<Id, List<IDNSC_Task__c>> relTaskMap)
	{
		List<IDNSC_Task__c> updatedTasks = new List<IDNSC_Task__c>();
		
		for(List<IDNSC_Task__c> lTasks : relTaskMap.values())
		{
			Map<String, IDNSC_Task__c> taskMap = new Map<String, IDNSC_Task__c>();
			List<String> lSortedCriticalPath = new List<String>();			
			for(IDNSC_Task__c t : lTasks)
			{
				taskMap.put(t.Task_Number__c, t);
				if(t.Critical_Path__c && t.Status__c != 'Skipped') // && t.Status__c != 'Completed'
				{
					lSortedCriticalPath.add(t.Task_Number__c);
				}
				if(t.Status__c == 'Skipped')
				{
					t.Start_Date_Expected__c = null;
					t.Finish_Date_Estimated__c = null;
				}
			}
			lSortedCriticalPath.sort();
			Map<String, Date> lastCriticalFinishForStage = new Map<String, Date>();
			Map<String, Date> criticalStartForStage = new Map<String, Date>();
			Map<String, Decimal> stageMaxDurationMap = new Map<String, Decimal>();
			
			Map<Integer, String> numbersToLetters = new Map<Integer, String>();
			numbersToLetters.put(0,'A');
			numbersToLetters.put(1,'B');
			numbersToLetters.put(2,'C');
			numbersToLetters.put(3,'D');
			numbersToLetters.put(4,'E');
			numbersToLetters.put(5,'F');
			numbersToLetters.put(6,'G');
			numbersToLetters.put(7,'H');
			numbersToLetters.put(8,'I');
			numbersToLetters.put(9,'J');
			Map<String, Integer> lettersToNumbers = new Map<String, Integer>();
			lettersToNumbers.put('A',0);
			lettersToNumbers.put('B',1);
			lettersToNumbers.put('C',2);
			lettersToNumbers.put('D',3);
			lettersToNumbers.put('E',4);
			lettersToNumbers.put('F',5);
			lettersToNumbers.put('G',6);
			lettersToNumbers.put('H',7);
			lettersToNumbers.put('I',8);
			lettersToNumbers.put('J',9);
			
			for(String taskNum : lSortedCriticalPath)
			{
				Decimal longestDuration = 0;
				String stageLetter = taskNum.substring(0, 1);
				
				Integer taskDuration = Integer.valueOf(taskMap.get(taskNum).Duration_Days_Expected__c);
				
				if(stageMaxDurationMap.containsKey(stageLetter))
				{
					longestDuration = stageMaxDurationMap.get(stageLetter);
				}
				if(taskDuration > longestDuration)
				{
					stageMaxDurationMap.put(stageLetter, taskDuration);
				}
				
				Date FinishDate;// = taskMap.get(taskNum).Finish_Date_Estimated__c;
				Date StartDate;// = taskMap.get(taskNum).Start_Date_Expected__c;
				if(taskMap.get(taskNum).Start_Date_Expected__c != null)
				{
					StartDate = taskMap.get(taskNum).Start_Date_Expected__c;
				}
				else if(taskMap.get(taskNum).Start_Date_Actual__c != null)
				{
					StartDate = taskMap.get(taskNum).Start_Date_Actual__c;
				}
				else
				{
					StartDate = Date.today();
				}

				if(lettersToNumbers.get(stageLetter) > 0)
				{
					String previousStage = numbersToLetters.get((lettersToNumbers.get(stageLetter)-1));
					if(lastCriticalFinishForStage.containsKey(previousStage))
					{
						StartDate = lastCriticalFinishForStage.get(previousStage).addDays(1);
					}
					StartDate = adjustDateForWeekend(StartDate);
				}
				
				if(criticalStartForStage.containsKey(stageLetter))
				{
					if(StartDate < criticalStartForStage.get(stageLetter))
					{
						criticalStartForStage.put(stageLetter, StartDate);
					}
				}
				else
				{
					criticalStartForStage.put(stageLetter, StartDate);
				}
				
				if(taskMap.get(taskNum).Status__c == 'Completed')
				{
					FinishDate = taskMap.get(taskNum).Completion_Date_Actual__c;
				}
				else if(taskMap.get(taskNum).Status__c == 'In Progress' || taskMap.get(taskNum).Status__c == 'Not Started')
				{
					if(taskMap.get(taskNum).Start_Date_Actual__c != null)
					{
						StartDate = taskMap.get(taskNum).Start_Date_Actual__c;
					}
					System.debug('Critical Path Task, Stage: ' + stageLetter + ', Start Date: ' + StartDate);
					Integer calendarDays = Integer.valueOf((Math.floor(taskDuration/5) * 7) + (Math.mod(taskDuration, 5)));
					Date endDate = StartDate.addDays(calendarDays);
					FinishDate = adjustDateForWeekend(endDate);
				}
				
				Date LastFinish;// = taskMap.get(lSortedCriticalPath[0]).Start_Date_Expected__c;
				if(taskMap.get(lSortedCriticalPath[0]).Start_Date_Expected__c != null)
				{
					LastFinish = taskMap.get(lSortedCriticalPath[0]).Start_Date_Expected__c;
				}
				else if(taskMap.get(lSortedCriticalPath[0]).Start_Date_Actual__c != null)
				{
					LastFinish = taskMap.get(lSortedCriticalPath[0]).Start_Date_Actual__c;
				}
				else
				{
					LastFinish = Date.today();
				}
				if(lastCriticalFinishForStage.containsKey(stageLetter))
				{
					LastFinish = lastCriticalFinishForStage.get(stageLetter);
				}
				else
				{
					lastCriticalFinishForStage.put(stageLetter, LastFinish);
				}
				System.debug(stageLetter + ' LastFinish: ' + lastCriticalFinishForStage.get(stageLetter));
				System.debug(stageLetter + ' FinishDate: ' + FinishDate);
				if(FinishDate > LastFinish)
				{
					System.debug('Critical Path Task, Stage: ' + stageLetter + ', Finish Date: ' + FinishDate);
					lastCriticalFinishForStage.put(stageLetter, FinishDate);
				}
			}
			List<String> sortedStageLetters = new List<String>();
			sortedStageLetters.addAll(stageMaxDurationMap.keySet());
			sortedStageLetters.sort();
			
			Map<String, Map<String, Date>> stageStartEndDatesMap = new Map<String, Map<String, Date>>();
			for(Integer i = 0; i < sortedStageLetters.size(); i++)
			{
				System.debug('sortedStageLetters: ' + sortedStageLetters[i]);
				Map<String, Date> currentStageStartEndDatesMap;
				Map<String, Date> previousStageStartEndDatesMap;
				if(stageStartEndDatesMap.containsKey(sortedStageLetters[i]))
				{
					currentStageStartEndDatesMap = stageStartEndDatesMap.get(sortedStageLetters[i]);
				}
				else
				{
					currentStageStartEndDatesMap = new Map<String, Date>();
				}
				Integer stageDuration = Integer.valueOf(stageMaxDurationMap.get(sortedStageLetters[i]));
				Integer calendarDays = Integer.valueOf((Math.floor(stageDuration/5) * 7) + (Math.mod(stageDuration, 5)));
				Date startDate = criticalStartForStage.get(sortedStageLetters[i]);
				Date endDate;
				
				if(i > 0)
				{
					previousStageStartEndDatesMap = stageStartEndDatesMap.get(sortedStageLetters[i-1]);
					System.debug('Previous Stage: ' + sortedStageLetters[i-1] + ', End Date: ' + previousStageStartEndDatesMap.get('EndDate'));
					startDate = previousStageStartEndDatesMap.get('EndDate').addDays(1);
					startDate = adjustDateForWeekend(startDate);
				}
						
				if(lastCriticalFinishForStage.containsKey(sortedStageLetters[i]))
				{
					System.debug('lastCriticalFinishForStage: ' + lastCriticalFinishForStage.get(sortedStageLetters[i]));
					endDate = lastCriticalFinishForStage.get(sortedStageLetters[i]);
				}
				else
				{
					if(i > 0)
					{
						previousStageStartEndDatesMap = stageStartEndDatesMap.get(sortedStageLetters[i-1]);
						startDate = previousStageStartEndDatesMap.get('EndDate').addDays(1);
						startDate = adjustDateForWeekend(startDate);
						endDate = startDate.addDays(calendarDays);
						endDate = adjustDateForWeekend(endDate);
					}
				}
				if(startDate != null)
				{
					System.debug('Current Stage: ' + sortedStageLetters[i] + ', Start Date: ' + startDate);
					currentStageStartEndDatesMap.put('StartDate', startDate);
				}
				currentStageStartEndDatesMap.put('EndDate', endDate);
				stageStartEndDatesMap.put(sortedStageLetters[i], currentStageStartEndDatesMap);
			}
			
			for(IDNSC_Task__c t : lTasks)
			{
				String taskNum = t.Task_Number__c;
				String stageLetter = taskNum.substring(0, 1);
				
				if(t.Status__c == 'Not Started')
				{
					if(stageStartEndDatesMap.get(stageLetter).containsKey('StartDate'))
					{
						t.Start_Date_Expected__c = stageStartEndDatesMap.get(stageLetter).get('StartDate');
					}
					System.debug('Task Number: ' + t.Task_Number__c + ', Start Date - Expected: ' + t.Start_Date_Expected__c);
					Integer taskDuration = Integer.valueOf(t.Duration_Days_Expected__c);
					Integer calendarDays = Integer.valueOf((Math.floor(taskDuration/5) * 7) + (Math.mod(taskDuration, 5)));
					Date endDate = t.Start_Date_Expected__c.addDays(calendarDays);
					t.Finish_Date_Estimated__c = adjustDateForWeekend(endDate);
					updatedTasks.add(t);
				}
				else
				{
					t.Finish_Date_Estimated__c = t.Completion_Date_Calc__c;
					updatedTasks.add(t);
				}
				/*
				else if(t.Status__c == 'In Progress')
				{
					if(stageStartEndDatesMap.get(stageLetter).containsKey('EndDate'))
					{
						t.Finish_Date_Estimated__c = stageStartEndDatesMap.get(stageLetter).get('EndDate');
					}
					updatedTasks.add(t);
				}
				*/
			}
		}
		
		IDNSC_Task_TriggerDispatcher.executedMethods.add('calculateEstimatedStartDates');
		
		if(!updatedTasks.isEmpty())
			update updatedTasks;
	}
	
	public static void calculateStartDates_Insert(Map<Id, List<IDNSC_Task__c>> relTaskMap)
	{
		List<IDNSC_Task__c> updatedTasks = new List<IDNSC_Task__c>();
		
		for(List<IDNSC_Task__c> lTasks : relTaskMap.values())
		{
			List<String> lSortedCriticalPath = new List<String>();
			Map<String, IDNSC_Task__c> taskMap = new Map<String, IDNSC_Task__c>();
			for(IDNSC_Task__c t : lTasks)
			{
				taskMap.put(t.Task_Number__c, t);
				if(t.Critical_Path__c) // && t.Status__c != 'Completed' && t.Status__c != 'Skipped'
				{
					lSortedCriticalPath.add(t.Task_Number__c);
				}
			}
			lSortedCriticalPath.sort();
			
			Map<String, Decimal> stageMaxDurationMap = new Map<String, Decimal>();
			for(String taskNum : lSortedCriticalPath)
			{
				Decimal longestDuration = 0;
				String stageLetter = taskNum.substring(0, 1);
				Decimal taskDuration = taskMap.get(taskNum).Duration_Days_Expected__c;
				
				if(stageMaxDurationMap.containsKey(stageLetter))
				{
					longestDuration = stageMaxDurationMap.get(stageLetter);
				}
				if(taskDuration > longestDuration)
				{
					stageMaxDurationMap.put(stageLetter, taskDuration);
				}
			}
			List<String> sortedStageLetters = new List<String>();
			sortedStageLetters.addAll(stageMaxDurationMap.keySet());
			sortedStageLetters.sort();
			//don't actually want totalPriorDurations because we need to factor in weekends. To determine a start date add stageMaxDuration to the prior stage's end date and adjust for weekends for both.
			/*
			Map<String, Decimal> totalPriorDurationsMap = new Map<String, Decimal>();
			for(Integer i = 0; i < sortedStageLetters.size(); i++)
			{
				Decimal totalPriorDurations = 0;
				for(Integer j = i; j > 0; j--)
				{
					totalPriorDurations += stageMaxDurationMap.get(sortedStageLetters[j-1]);
				}
				totalPriorDurationsMap.put(sortedStageLetters[i], totalPriorDurations);
			}
			*/
			Map<String, Map<String, Date>> stageStartEndDatesMap = new Map<String, Map<String, Date>>();
			for(Integer i = 0; i < sortedStageLetters.size(); i++)
			{
				Map<String, Date> currentStageStartEndDatesMap;
				Map<String, Date> previousStageStartEndDatesMap;
				if(stageStartEndDatesMap.containsKey(sortedStageLetters[i]))
				{
					currentStageStartEndDatesMap = stageStartEndDatesMap.get(sortedStageLetters[i]);
				}
				else
				{
					currentStageStartEndDatesMap = new Map<String, Date>();
				}
				Integer stageDuration = Integer.valueOf(stageMaxDurationMap.get(sortedStageLetters[i]));
				Integer calendarDays = Integer.valueOf((Math.floor(stageDuration/5) * 7) + (Math.mod(stageDuration, 5)));
				Date startDate;
				if(i == 0)
				{
					startDate = Date.today();
				}
				else
				{
					previousStageStartEndDatesMap = stageStartEndDatesMap.get(sortedStageLetters[i-1]);
					startDate = previousStageStartEndDatesMap.get('EndDate').addDays(1);
				}
				startDate = adjustDateForWeekend(startDate);
				Date endDate = startDate.addDays(calendarDays);
				endDate = adjustDateForWeekend(endDate);
				
				currentStageStartEndDatesMap.put('StartDate', startDate);
				currentStageStartEndDatesMap.put('EndDate', endDate);
				stageStartEndDatesMap.put(sortedStageLetters[i], currentStageStartEndDatesMap);
			}
			
			for(IDNSC_Task__c t : lTasks)
			{
				if(t.Status__c == 'Not Started')
				{
					String taskNum = t.Task_Number__c;
					String stageLetter = taskNum.substring(0, 1);
					//t.Start_Date_Actual__c = stageStartEndDatesMap.get(stageLetter).get('StartDate');
					t.Start_Date_Expected__c = stageStartEndDatesMap.get(stageLetter).get('StartDate');
					Integer taskDuration = Integer.valueOf(t.Duration_Days_Expected__c);
					Integer calendarDays = Integer.valueOf((Math.floor(taskDuration/5) * 7) + (Math.mod(taskDuration, 5)));
					Date endDate = t.Start_Date_Expected__c.addDays(calendarDays);
					t.Finish_Date_Estimated__c = adjustDateForWeekend(endDate);
					updatedTasks.add(t);
				}
			}
		}
		
		IDNSC_Task_TriggerDispatcher.executedMethods.add('calculateEstimatedStartDates');
		
		if(!updatedTasks.isEmpty())
			update updatedTasks;
			
	}
	
	public static Date adjustDateForWeekend(Date evalDate)
	{
		Date monday = Date.newInstance(2001, 1, 1);//Monday's dayIndex == 0
		Integer dayIndex = Math.mod(monday.daysBetween(evalDate), 7);
		Integer weekendFix = 7 - dayIndex;
		if(weekendFix == 1 || weekendFix == 2)
		{
			evalDate = evalDate.addDays(weekendFix);
		}
		return evalDate;
	}
}