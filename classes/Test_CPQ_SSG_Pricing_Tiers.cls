@isTest
private class Test_CPQ_SSG_Pricing_Tiers {
	@isTest static void Test_CPQ_SSG_Pricing_Tier_Before() {
		CPQ_SSG_Class_of_Trade__c cot = new CPQ_SSG_Class_of_Trade__c(Name = 'Test COT');
		insert cot;

		CPQ_SSG_Price_List__c priceList = new CPQ_SSG_Price_List__c(Price_List_Code__c = 'TESTLIST1');
		insert priceList;

		Test.startTest();
			// Insert tier and verify that class of trade lookup is filled.
			CPQ_SSG_Pricing_Tier__c tier = new CPQ_SSG_Pricing_Tier__c(Class_Of_Trade_Name__c = cot.Name, Name = 'Test Tier');
			insert tier;
			tier = [Select Id, CPQ_SSG_Class_of_Trade__c, Name From CPQ_SSG_Pricing_Tier__c Where Id = :tier.Id];  
			System.assertEquals(cot.Id, tier.CPQ_SSG_Class_of_Trade__c);

			// Insert pricing tier/price list and verify that tier and price list lookups are filled.
			CPQ_SSG_Pricing_Tier_Price_List__c tierList = new CPQ_SSG_Pricing_Tier_Price_List__c(Price_List_Code__c = priceList.Price_List_Code__c, Pricing_Tier_Name__c = tier.Name);
			insert tierList;
			tierList = [Select Id, CPQ_SSG_Pricing_Tier__c, CPQ_SSG_Price_List__c From CPQ_SSG_Pricing_Tier_Price_List__c Where Id = :tierList.Id];
			System.assertEquals(tier.Id, tierList.CPQ_SSG_Pricing_Tier__c);
			System.assertEquals(priceList.Id, tierList.CPQ_SSG_Price_List__c);
		Test.stopTest();
	}
}