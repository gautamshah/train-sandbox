/***********************
Aurthor: Yap Zhen-Xiong
Email: Zhenxiong.Yap@covidien.com

Apex Involved: 
UpdateEventsDepartmentSpecialty.trigger
Task_Send_Email_Upon_Mgr_Feedback.trigger
Activity_Send_Email_Upon_Mgr_Feedback.trigger
Event_Notify_Manager.trigger

Modification Log:
   21/05/2014 ZX: Updated Test class to be a test class for all Asia activities. 
   When created: ZX: Test Class for Task_Send_Email_Upon_Mgr_Feedback trigger and Activity_Send_Email_Upon_Mgr_Feedback trigger.
   28/05/2014 ZX: Updated Test class to test UpdateEventLocationField.trigger
   26/06/2014 ZX: Updated Test class to test Event_Notify_Manager.trigger
   15/09/2015 FS: Updated Test class line 688 to prevent error from new validation rule: KR_Fields_Must_Be_Filled_In
   04/11/2015 FS: Updated Test class to add Philippines to test Event_Notify_Manager.trigger and Event_Send_Email_Upon_Mgr_Feedback.trigger
***********************/

@isTest

public class AsiaActivityTestClass{
    /*   
    static testMethod void AsiaActivityTestClass1()
    {
         System.debug('Trigger_Profile null? '+ Trigger_Profile__c.getInstance());
         if (Trigger_Profile__c.getInstance('GBU_Franchise_Update') == null)
                upsert new Trigger_Profile__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                                Profile_Name__c='API Data Loader',
                                                Name = 'GBU_Franchise_Update');
         Id pId = [select Id 
                      from Profile 
                     where name = 'CRM Admin Support'].Id;
         List<User> userList1 = new List<User>();
         List<User> userList2 = new List<User>();
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('AsiaActivityTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         }
         
         User u2mgr = new User();
         u2mgr.LastName = 'Shmoe Test';
         u2mgr.Business_Unit__c = 'The Unit';
         u2mgr.Franchise__c = 'Burger King';
         u2mgr.email = 'testZXmgr1@covidien.com';
         u2mgr.alias = 'testmgr1';
         u2mgr.username = 'testZXmgr1@covidien.com';
         u2mgr.communityNickName = 'testZXmgr1@covidien.com';
         u2mgr.ProfileId = pId;
         u2mgr.CurrencyIsoCode='SGD'; 
         u2mgr.EmailEncodingKey='ISO-8859-1';
         u2mgr.TimeZoneSidKey='America/New_York';
         u2mgr.LanguageLocaleKey='en_US';
         u2mgr.LocaleSidKey ='en_US';
         u2mgr.Country = 'SG';
         userList1.add(u2mgr);
         
         User u3mgr = new User();
         u3mgr.LastName = 'Shmoe Test';
         u3mgr.Business_Unit__c = 'The Unit';
         u3mgr.Franchise__c = 'Burger King';
         u3mgr.email = 'testZXmgr2@covidien.com';
         u3mgr.alias = 'testmgr2';
         u3mgr.username = 'testZXmgr2@covidien.com';
         u3mgr.communityNickName = 'testZXmgr2@covidien.com';
         u3mgr.ProfileId = pId;
         u3mgr.CurrencyIsoCode='TWD'; 
         u3mgr.EmailEncodingKey='ISO-8859-1';
         u3mgr.TimeZoneSidKey='America/New_York';
         u3mgr.LanguageLocaleKey='zh_TW';
         u3mgr.LocaleSidKey ='en_US';
         u3mgr.Country = 'TW';
         userList1.add(u3mgr);
         
         User u4mgr = new User();
         u4mgr.LastName = 'Shmoe Test';
         u4mgr.Business_Unit__c = 'The Unit';
         u4mgr.Franchise__c = 'Burger King';
         u4mgr.email = 'testZXmgr3@covidien.com';
         u4mgr.alias = 'testmgr3';
         u4mgr.username = 'testZXmgr3@covidien.com';
         u4mgr.communityNickName = 'testZXmgr3@covidien.com';
         u4mgr.ProfileId = pId;
         u4mgr.CurrencyIsoCode='CNY'; 
         u4mgr.EmailEncodingKey='ISO-8859-1';
         u4mgr.TimeZoneSidKey='America/New_York';
         u4mgr.LanguageLocaleKey='zh_CN';
         u4mgr.LocaleSidKey ='en_US';
         u4mgr.Country = 'CN';
         userList1.add(u4mgr);
         
         insert userList1;
         
         User u2 = new User();
         u2.LastName = 'Shmoe Test';
         u2.Business_Unit__c = 'The Unit';
         u2.Franchise__c = 'Burger King';
         u2.email = 'testZX1@covidien.com';
         u2.alias = 'testShmo';
         u2.username = 'testZX1@covidien.com';
         u2.communityNickName = 'testZX1@covidien.com';
         u2.ProfileId = pId;
         u2.CurrencyIsoCode='SGD'; 
         u2.EmailEncodingKey='ISO-8859-1';
         u2.TimeZoneSidKey='America/New_York';
         u2.LanguageLocaleKey='en_US';
         u2.LocaleSidKey ='en_US';
         u2.Country = 'SG';
         u2.ManagerId = u2mgr.id;
         userList2.add(u2);
         
         User u3 = new User();
         u3.LastName = 'Shmoe Test';
         u3.Business_Unit__c = 'The Unit';
         u3.Franchise__c = 'Burger King';
         u3.email = 'testZX2@covidien.com';
         u3.alias = 'testShm2';
         u3.username = 'testZX2@covidien.com';
         u3.communityNickName = 'testZX2@covidien.com';
         u3.ProfileId = pId;
         u3.CurrencyIsoCode='TWD'; 
         u3.EmailEncodingKey='ISO-8859-1';
         u3.TimeZoneSidKey='America/New_York';
         u3.LanguageLocaleKey='zh_TW';
         u3.LocaleSidKey ='en_US';
         u3.Country = 'TW';
         u3.ManagerId = u3mgr.id;
         userList2.add(u3);
         
         User u4 = new User();
         u4.LastName = 'Shmoe Test';
         u4.Business_Unit__c = 'The Unit';
         u4.Franchise__c = 'Burger King';
         u4.email = 'testZX3@covidien.com';
         u4.alias = 'testShm3';
         u4.username = 'testZX3@covidien.com';
         u4.communityNickName = 'testZX3@covidien.com';
         u4.ProfileId = pId;
         u4.CurrencyIsoCode='CNY'; 
         u4.EmailEncodingKey='ISO-8859-1';
         u4.TimeZoneSidKey='America/New_York';
         u4.LanguageLocaleKey='zh_CN';
         u4.LocaleSidKey ='en_US';
         u4.Country = 'CN';
         u4.ManagerId = u4mgr.id;
         userList2.add(u4);
         
         /*User not in Allowed Country list
         User u5 = new User();
         u5.LastName = 'Shmoe Test';
         u5.Business_Unit__c = 'The Unit';
         u5.Franchise__c = 'Burger King';
         u5.email = 'testZX4@covidien.com';
         u5.alias = 'testShm4';
         u5.username = 'testZX4@covidien.com';
         u5.communityNickName = 'testZX4@covidien.com';
         u5.ProfileId = pId;
         u5.CurrencyIsoCode='CNY'; 
         u5.EmailEncodingKey='ISO-8859-1';
         u5.TimeZoneSidKey='America/New_York';
         u5.LanguageLocaleKey='zh_CN';
         u5.LocaleSidKey ='en_US';
         u5.Country = 'ZW';
         u5.ManagerId = u4mgr.id;
         userList2.add(u5);*/
         /*
         insert userList2;
         
        List<AccountShare> shareList = new List<AccountShare>();
        AccountShare share1 = new AccountShare();
        share1.AccountId = a.id;
        share1.UserOrGroupId = u2.id;
        share1.AccountAccessLevel = 'Edit';
        share1.OpportunityAccessLevel = 'Read';
        share1.CaseAccessLevel = 'Edit';
        shareList.add(share1);
        
        AccountShare share2 = new AccountShare();
        share2.AccountId = a.id;
        share2.UserOrGroupId = u3.id;
        share2.AccountAccessLevel = 'Edit';
        share2.OpportunityAccessLevel = 'Read';
        share2.CaseAccessLevel = 'Edit';
        shareList.add(share2);

        AccountShare share3 = new AccountShare();
        share3.AccountId = a.id;
        share3.UserOrGroupId = u4.id;
        share3.AccountAccessLevel = 'Edit';
        share3.OpportunityAccessLevel = 'Read';
        share3.CaseAccessLevel = 'Edit';
        shareList.add(share3);

        AccountShare share4 = new AccountShare();
        share4.AccountId = a.id;
        share4.UserOrGroupId = u2mgr.id;
        share4.AccountAccessLevel = 'Edit';
        share4.OpportunityAccessLevel = 'Read';
        share4.CaseAccessLevel = 'Edit';
        shareList.add(share4);
        
        AccountShare share5 = new AccountShare();
        share5.AccountId = a.id;
        share5.UserOrGroupId = u3mgr.id;
        share5.AccountAccessLevel = 'Edit';
        share5.OpportunityAccessLevel = 'Read';
        share5.CaseAccessLevel = 'Edit';
        shareList.add(share5);
        
        AccountShare share6 = new AccountShare();
        share6.AccountId = a.id;
        share6.UserOrGroupId = u4mgr.id;
        share6.AccountAccessLevel = 'Edit';
        share6.OpportunityAccessLevel = 'Read';
        share6.CaseAccessLevel = 'Edit';
        shareList.add(share6);
        
        /*AccountShare share7 = new AccountShare();
        share7.AccountId = a.id;
        share7.UserOrGroupId = u5.id;
        share7.AccountAccessLevel = 'Edit';
        share7.OpportunityAccessLevel = 'Read';
        share7.CaseAccessLevel = 'Edit';
        shareList.add(share7);*/
        /*        
        insert shareList;
        
        Contact c;
        try{
            c = [SELECT id FROM Contact LIMIT 1];
        }catch (Exception e){
            System.debug('AsiaActivityTestClass Test Class: Getting Any Contact Exception: ' + e);
            if(c == null){
                c = new Contact(LastName = 'Test Contact', AccountId=a.id, Department_pl__c='Test Dept', Specialty__c='Test Specialty');
                insert c;
            }
        }
        
        AsiaActivityTestClass testClass = new AsiaActivityTestClass();
        
        testClass.testTaskTrigger(u2, a, u2mgr); //test profile sending Default Email in English
        testClass.testTaskTrigger(u3, a, u3mgr); //test profile sending Traditional Chinese Email
        testClass.testTaskTrigger(u4, a, u4mgr); //test profile sending Simplified Chinese Email
        //testClass.testTaskTrigger(u5, a, u4mgr); //test un-allowed profile
        testClass.testEventTrigger(u2, a, u2mgr, c); //test profile sending Default Email in English
        testClass.testEventTrigger(u3, a, u3mgr, c); //test profile sending Traditional Chinese Email
        testClass.testEventTrigger(u4, a, u4mgr, c); //test profile sending Simplified Chinese Email
        //testClass.testEventTrigger(u5, a, u4mgr); //test un-allowed profile
    }*/
    
    static testMethod void AsiaActivityTestMethod2()
    {
         System.debug('Trigger_Profile null? '+ Trigger_Profile__c.getInstance());
         if (Trigger_Profile__c.getInstance('GBU_Franchise_Update') == null)
                upsert new Trigger_Profile__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                                Profile_Name__c='API Data Loader',
                                                Name = 'GBU_Franchise_Update');
         Id pId = [select Id 
                      from Profile 
                     where name = 'CRM Admin Support'].Id;
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('AsiaActivityTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         }
         
         User u2mgr = new User();
         u2mgr.LastName = 'Shmoe Test';
         u2mgr.Business_Unit__c = 'The Unit';
         u2mgr.Franchise__c = 'Burger King';
         u2mgr.email = 'testZXmgr1@covidien.com';
         u2mgr.alias = 'testmgr1';
         u2mgr.username = 'testZXmgr1@covidien.com';
         u2mgr.communityNickName = 'testZXmgr1@covidien.com';
         u2mgr.ProfileId = pId;
         u2mgr.CurrencyIsoCode='SGD'; 
         u2mgr.EmailEncodingKey='ISO-8859-1';
         u2mgr.TimeZoneSidKey='America/New_York';
         u2mgr.LanguageLocaleKey='en_US';
         u2mgr.LocaleSidKey ='en_US';
         u2mgr.Country = 'SG';
         
         insert u2mgr;
         
         User u2 = new User();
         u2.LastName = 'Shmoe Test';
         u2.Business_Unit__c = 'The Unit';
         u2.Franchise__c = 'Burger King';
         u2.email = 'testZX1@covidien.com';
         u2.alias = 'testShmo';
         u2.username = 'testZX1@covidien.com';
         u2.communityNickName = 'testZX1@covidien.com';
         u2.ProfileId = pId;
         u2.CurrencyIsoCode='SGD'; 
         u2.EmailEncodingKey='ISO-8859-1';
         u2.TimeZoneSidKey='America/New_York';
         u2.LanguageLocaleKey='en_US';
         u2.LocaleSidKey ='en_US';
         u2.Country = 'SG';
         u2.ManagerId = u2mgr.id;
         
         insert u2;
         
        List<AccountShare> shareList = new List<AccountShare>();
        AccountShare share1 = new AccountShare();
        share1.AccountId = a.id;
        share1.UserOrGroupId = u2.id;
        share1.AccountAccessLevel = 'Edit';
        share1.OpportunityAccessLevel = 'Read';
        share1.CaseAccessLevel = 'Edit';
        shareList.add(share1);

        AccountShare share4 = new AccountShare();
        share4.AccountId = a.id;
        share4.UserOrGroupId = u2mgr.id;
        share4.AccountAccessLevel = 'Edit';
        share4.OpportunityAccessLevel = 'Read';
        share4.CaseAccessLevel = 'Edit';
        shareList.add(share4);
                
        insert shareList;
        
        Contact c;
        try{
            c = [SELECT id FROM Contact LIMIT 1];
        }catch (Exception e){
            System.debug('AsiaActivityTestClass Test Class: Getting Any Contact Exception: ' + e);
            if(c == null){
                c = new Contact(LastName = 'Test Contact', AccountId=a.id, Department_pl__c='Test Dept', Specialty__c='Test Specialty');
                insert c;
            }
        }
        
        AsiaActivityTestClass testClass = new AsiaActivityTestClass();
        
        Test.startTest();
        testClass.testTaskTrigger(u2, a, u2mgr); //test profile sending Default Email in English
        testClass.testEventTrigger(u2, a, u2mgr, c); //test profile sending Default Email in English
        Test.stopTest();
    }
    
    static testMethod void AsiaActivityTestMethod3()
    {
         System.debug('Trigger_Profile null? '+ Trigger_Profile__c.getInstance());
         if (Trigger_Profile__c.getInstance('GBU_Franchise_Update') == null)
                upsert new Trigger_Profile__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                                Profile_Name__c='API Data Loader',
                                                Name = 'GBU_Franchise_Update');
         Id pId = [select Id 
                      from Profile 
                     where name = 'CRM Admin Support'].Id;
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('AsiaActivityTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         }
         
         User u3mgr = new User();
         u3mgr.LastName = 'Shmoe Test';
         u3mgr.Business_Unit__c = 'The Unit';
         u3mgr.Franchise__c = 'Burger King';
         u3mgr.email = 'testZXmgr2@covidien.com';
         u3mgr.alias = 'testmgr2';
         u3mgr.username = 'testZXmgr2@covidien.com';
         u3mgr.communityNickName = 'testZXmgr2@covidien.com';
         u3mgr.ProfileId = pId;
         u3mgr.CurrencyIsoCode='TWD'; 
         u3mgr.EmailEncodingKey='ISO-8859-1';
         u3mgr.TimeZoneSidKey='America/New_York';
         u3mgr.LanguageLocaleKey='zh_TW';
         u3mgr.LocaleSidKey ='en_US';
         u3mgr.Country = 'TW';
         
         insert u3mgr;
                  
         User u3 = new User();
         u3.LastName = 'Shmoe Test';
         u3.Business_Unit__c = 'The Unit';
         u3.Franchise__c = 'Burger King';
         u3.email = 'testZX2@covidien.com';
         u3.alias = 'testShm2';
         u3.username = 'testZX2@covidien.com';
         u3.communityNickName = 'testZX2@covidien.com';
         u3.ProfileId = pId;
         u3.CurrencyIsoCode='TWD'; 
         u3.EmailEncodingKey='ISO-8859-1';
         u3.TimeZoneSidKey='America/New_York';
         u3.LanguageLocaleKey='zh_TW';
         u3.LocaleSidKey ='en_US';
         u3.Country = 'TW';
         u3.ManagerId = u3mgr.id;
         insert u3;
        
        List<AccountShare> shareList = new List<AccountShare>();
        AccountShare share2 = new AccountShare();
        share2.AccountId = a.id;
        share2.UserOrGroupId = u3.id;
        share2.AccountAccessLevel = 'Edit';
        share2.OpportunityAccessLevel = 'Read';
        share2.CaseAccessLevel = 'Edit';
        shareList.add(share2);
        
        AccountShare share5 = new AccountShare();
        share5.AccountId = a.id;
        share5.UserOrGroupId = u3mgr.id;
        share5.AccountAccessLevel = 'Edit';
        share5.OpportunityAccessLevel = 'Read';
        share5.CaseAccessLevel = 'Edit';
        shareList.add(share5);
        
        insert shareList;
        
        Contact c;
        try{
            c = [SELECT id FROM Contact LIMIT 1];
        }catch (Exception e){
            System.debug('AsiaActivityTestClass Test Class: Getting Any Contact Exception: ' + e);
            if(c == null){
                c = new Contact(LastName = 'Test Contact', AccountId=a.id, Department_pl__c='Test Dept', Specialty__c='Test Specialty');
                insert c;
            }
        }
        
        AsiaActivityTestClass testClass = new AsiaActivityTestClass();
        
        Test.startTest();
        testClass.testTaskTrigger(u3, a, u3mgr); //test profile sending Traditional Chinese Email
        testClass.testEventTrigger(u3, a, u3mgr, c); //test profile sending Traditional Chinese Email
        Test.stopTest();
    }
    
    static testMethod void AsiaActivityTestMethod4()
    {
         System.debug('Trigger_Profile null? '+ Trigger_Profile__c.getInstance());
         if (Trigger_Profile__c.getInstance('GBU_Franchise_Update') == null)
                upsert new Trigger_Profile__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                                Profile_Name__c='API Data Loader',
                                                Name = 'GBU_Franchise_Update');
         Id pId = [select Id 
                      from Profile 
                     where name = 'CRM Admin Support'].Id;
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('AsiaActivityTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         }
         
         User u4mgr = new User();
         u4mgr.LastName = 'Shmoe Test';
         u4mgr.Business_Unit__c = 'The Unit';
         u4mgr.Franchise__c = 'Burger King';
         u4mgr.email = 'testZXmgr3@covidien.com';
         u4mgr.alias = 'testmgr3';
         u4mgr.username = 'testZXmgr3@covidien.com';
         u4mgr.communityNickName = 'testZXmgr3@covidien.com';
         u4mgr.ProfileId = pId;
         u4mgr.CurrencyIsoCode='CNY'; 
         u4mgr.EmailEncodingKey='ISO-8859-1';
         u4mgr.TimeZoneSidKey='America/New_York';
         u4mgr.LanguageLocaleKey='zh_CN';
         u4mgr.LocaleSidKey ='en_US';
         u4mgr.Country = 'CN';
         
         insert u4mgr;
         
         User u4 = new User();
         u4.LastName = 'Shmoe Test';
         u4.Business_Unit__c = 'The Unit';
         u4.Franchise__c = 'Burger King';
         u4.email = 'testZX3@covidien.com';
         u4.alias = 'testShm3';
         u4.username = 'testZX3@covidien.com';
         u4.communityNickName = 'testZX3@covidien.com';
         u4.ProfileId = pId;
         u4.CurrencyIsoCode='CNY'; 
         u4.EmailEncodingKey='ISO-8859-1';
         u4.TimeZoneSidKey='America/New_York';
         u4.LanguageLocaleKey='zh_CN';
         u4.LocaleSidKey ='en_US';
         u4.Country = 'CN';
         u4.ManagerId = u4mgr.id;
         
         insert u4;
         
        List<AccountShare> shareList = new List<AccountShare>();
        
        AccountShare share3 = new AccountShare();
        share3.AccountId = a.id;
        share3.UserOrGroupId = u4.id;
        share3.AccountAccessLevel = 'Edit';
        share3.OpportunityAccessLevel = 'Read';
        share3.CaseAccessLevel = 'Edit';
        shareList.add(share3);
        
        AccountShare share6 = new AccountShare();
        share6.AccountId = a.id;
        share6.UserOrGroupId = u4mgr.id;
        share6.AccountAccessLevel = 'Edit';
        share6.OpportunityAccessLevel = 'Read';
        share6.CaseAccessLevel = 'Edit';
        shareList.add(share6);  
        insert shareList;
        
        Contact c;
        try{
            c = [SELECT id FROM Contact LIMIT 1];
        }catch (Exception e){
            System.debug('AsiaActivityTestClass Test Class: Getting Any Contact Exception: ' + e);
            if(c == null){
                c = new Contact(LastName = 'Test Contact', AccountId=a.id, Department_pl__c='Test Dept', Specialty__c='Test Specialty');
                insert c;
            }
        }
        
        AsiaActivityTestClass testClass = new AsiaActivityTestClass();
        
        Test.startTest();
        testClass.testTaskTrigger(u4, a, u4mgr); //test profile sending Simplified Chinese Email
        testClass.testEventTrigger(u4, a, u4mgr, c); //test profile sending Simplified Chinese Email
        Test.stopTest();
    }
    
    static testMethod void AsiaActivityTestMethod5()
    {
         System.debug('Trigger_Profile null? '+ Trigger_Profile__c.getInstance());
         if (Trigger_Profile__c.getInstance('GBU_Franchise_Update') == null)
                upsert new Trigger_Profile__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                                Profile_Name__c='API Data Loader',
                                                Name = 'GBU_Franchise_Update');
         Id pId = [select Id 
                      from Profile 
                     where name = 'CRM Admin Support'].Id;
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('AsiaActivityTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         }
         
         User u5mgr = new User();
         u5mgr.LastName = 'Shmoe Test';
         u5mgr.Business_Unit__c = 'The Unit';
         u5mgr.Franchise__c = 'Burger King';
         u5mgr.email = 'testmgr5@covidien.com';
         u5mgr.alias = 'testmgr5';
         u5mgr.username = 'testmgr5@covidien.com';
         u5mgr.communityNickName = 'testmgr5@covidien.com';
         u5mgr.ProfileId = pId;
         u5mgr.CurrencyIsoCode='USD'; 
         u5mgr.EmailEncodingKey='ISO-8859-1';
         u5mgr.TimeZoneSidKey='Asia/Manila';
         u5mgr.LanguageLocaleKey='en_PH';
         u5mgr.LocaleSidKey ='en_US';
         u5mgr.Country = 'PH';
         
         insert u5mgr;
         
         User u5 = new User();
         u5.LastName = 'Shmoe Test';
         u5.Business_Unit__c = 'The Unit';
         u5.Franchise__c = 'Burger King';
         u5.email = 'testuser5@covidien.com';
         u5.alias = 'tt5';
         u5.username = 'testuser5@covidien.com';
         u5.communityNickName = 'testuser5@covidien.com';
         u5.ProfileId = pId;
         u5.CurrencyIsoCode='USD'; 
         u5.EmailEncodingKey='ISO-8859-1';
         u5.TimeZoneSidKey='Asia/Manila';
         u5.LanguageLocaleKey='en_PH';
         u5.LocaleSidKey ='en_US';
         u5.Country = 'PH';
         u5.ManagerId = u5mgr.id;
         
         insert u5;
         
        List<AccountShare> shareList = new List<AccountShare>();
        
        AccountShare share7 = new AccountShare();
        share7.AccountId = a.id;
        share7.UserOrGroupId = u5.id;
        share7.AccountAccessLevel = 'Edit';
        share7.OpportunityAccessLevel = 'Read';
        share7.CaseAccessLevel = 'Edit';
        shareList.add(share7);
        
        AccountShare share8 = new AccountShare();
        share8.AccountId = a.id;
        share8.UserOrGroupId = u5mgr.id;
        share8.AccountAccessLevel = 'Edit';
        share8.OpportunityAccessLevel = 'Read';
        share8.CaseAccessLevel = 'Edit';
        shareList.add(share8);  
        insert shareList;
        
        Contact c;
        try{
            c = [SELECT id FROM Contact LIMIT 1];
        }catch (Exception e){
            System.debug('AsiaActivityTestClass Test Class: Getting Any Contact Exception: ' + e);
            if(c == null){
                c = new Contact(LastName = 'Test Contact', AccountId=a.id, Department_pl__c='Test Dept', Specialty__c='Test Specialty');
                insert c;
            }
        }
        
        AsiaActivityTestClass testClass = new AsiaActivityTestClass();
        
        Test.startTest();
        testClass.testTaskTrigger(u5, a, u5mgr); //test profile sending Default Email in English
        testClass.testEventTrigger(u5, a, u5mgr, c); //test profile sending Default Email in English
        Test.stopTest();
    }
      
    
    static testMethod void AsiaActivityTestMethodForUpdateEventLocationField() //UpdateEventLocationField.trigger
    {
         System.debug('Trigger_Profile null? '+ Trigger_Profile__c.getInstance());
         if (Trigger_Profile__c.getInstance('GBU_Franchise_Update') == null)
                upsert new Trigger_Profile__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                                Profile_Name__c='API Data Loader',
                                                Name = 'GBU_Franchise_Update');
         Id pId = [select Id 
                      from Profile 
                     where name = 'CRM Admin Support'].Id;
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('AsiaActivityTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         }
         
         User krUserMgr = new User();
         krUserMgr.LastName = 'Shmoe Test';
         krUserMgr.Business_Unit__c = 'The Unit';
         krUserMgr.Franchise__c = 'Burger King';
         krUserMgr.email = 'testZXmgr3@covidien.com';
         krUserMgr.alias = 'testmgr3';
         krUserMgr.username = 'testZXmgr3@covidien.com';
         krUserMgr.communityNickName = 'testZXmgr3@covidien.com';
         krUserMgr.ProfileId = pId;
         krUserMgr.CurrencyIsoCode='KRW'; 
         krUserMgr.EmailEncodingKey='ISO-8859-1';
         krUserMgr.TimeZoneSidKey='America/New_York';
         krUserMgr.LanguageLocaleKey='ko';
         krUserMgr.LocaleSidKey ='ko_KR';
         krUserMgr.Country = 'KR';
         
         insert krUserMgr;
         
         User krUser = new User();
         krUser.LastName = 'Shmoe Test';
         krUser.Business_Unit__c = 'The Unit';
         krUser.Franchise__c = 'Burger King';
         krUser.email = 'testZX3@covidien.com';
         krUser.alias = 'testShm3';
         krUser.username = 'testZX3@covidien.com';
         krUser.communityNickName = 'testZX3@covidien.com';
         krUser.ProfileId = pId;
         krUser.CurrencyIsoCode='KRW'; 
         krUser.EmailEncodingKey='ISO-8859-1';
         krUser.TimeZoneSidKey='America/New_York';
         krUser.LanguageLocaleKey='ko';
         krUser.LocaleSidKey ='ko_KR';
         krUser.Country = 'KR';
         krUser.ManagerId = krUserMgr.id;
         
         insert krUser;
         
        List<AccountShare> shareList = new List<AccountShare>();
        
        AccountShare share3 = new AccountShare();
        share3.AccountId = a.id;
        share3.UserOrGroupId = krUser.id;
        share3.AccountAccessLevel = 'Edit';
        share3.OpportunityAccessLevel = 'Read';
        share3.CaseAccessLevel = 'Edit';
        shareList.add(share3);
        
        AccountShare share6 = new AccountShare();
        share6.AccountId = a.id;
        share6.UserOrGroupId = krUserMgr.id;
        share6.AccountAccessLevel = 'Edit';
        share6.OpportunityAccessLevel = 'Read';
        share6.CaseAccessLevel = 'Edit';
        shareList.add(share6);  
        insert shareList;
        
        Contact c;
        try{
            c = [SELECT id FROM Contact LIMIT 1];
        }catch (Exception e){
            System.debug('AsiaActivityTestClass Test Class: Getting Any Contact Exception: ' + e);
            if(c == null){
                c = new Contact(LastName = 'Test Contact', AccountId=a.id, Department_pl__c='Test Dept', Specialty__c='Test Specialty');
                insert c;
            }
        }
        
        AsiaActivityTestClass testClass = new AsiaActivityTestClass();
        
        Test.startTest();
        testClass.testTaskTrigger(krUser, a, krUserMgr); 
        testClass.testEventTrigger(krUser, a, krUserMgr, c); 
        Test.stopTest();
    }  
    
      
    public void testTaskTrigger(User u, Account a, User mgr){
        
        Task t;
        System.runAs(u) {
            System.debug('User Profile: '+ userInfo.getProfileID());
            System.debug('Trigger profile: '+ Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c);
            t = new Task(Subject='Test Subject', Activity_Type__c='Test Type', 
                        Status='Test Status', Priority='Test Priority',
                        WhatId = a.id,
                        OwnerId=u.id, Description='Test Description');
                        
            
            insert t;
        
        }
        
        //User mgr = [SELECT id FROM User WHERE id =:u.ManagerId];
        System.runAs(mgr){
            t = [SELECT JP_Feedback__c FROM Task WHERE id=:t.id];
            t.JP_Feedback__c = 'Test feedback';
            update t;
        }
        
    }
    
    public void testEventTrigger(User u, Account a, User mgr, Contact c){
        Event e;
        System.runAs(u) {
            System.debug('User Profile: '+ userInfo.getProfileID());
            System.debug('Trigger profile: '+ Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c);
            e = new Event(Subject='Test Subject', Activity_Type__c='Test Type', WhoId = c.Id,
                        WhatId = a.id, DurationInMinutes=60, ActivityDateTime=System.now(),
                        OwnerId=u.id, Description='Test Description', Notify_Manager__c=true,
                        Asia_Product_BU__c='RMS', Asia_Product__c='760 vent');
            insert e;
        
        }
        
        //User mgr = [SELECT id FROM User WHERE id =:u.ManagerId];
        System.runAs(mgr){ 
            e = [SELECT JP_Feedback__c FROM Event WHERE id=:e.id];
            e.JP_Feedback__c = 'Test feedback';
            update e;
        }
    }
    
}