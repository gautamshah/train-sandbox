// A few HTML methods that return HTML for the tag
public with sharing class HTML {
	
	public static string a(string href, string name) {
    	return '<a href="' + href + '" target="_top">' + name + '</a>';
    }
    
    public static string br = '<br/>';

}