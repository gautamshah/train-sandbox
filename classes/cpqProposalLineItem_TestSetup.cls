@isTest
public class cpqProposalLineItem_TestSetup
{

    public static Apttus_Proposal__Proposal_Line_Item__c generateProposalLineItem (
        Apttus_Proposal__Proposal__c proposal,
        Product2 product
    ) {
        Apttus_Proposal__Proposal_Line_Item__c pli = new Apttus_Proposal__Proposal_Line_Item__c();
        pli.Apttus_Proposal__Proposal__c = proposal.ID;
        pli.Apttus_Proposal__Product__c = product.ID;
        return pli;
    }

    public static Apttus_Proposal__Proposal_Line_Item__c generateProposalLineItem (
        Apttus_Proposal__Proposal__c proposal,
        Apttus_Config2__ProductConfiguration__c productConfiguration,
        Product2 product,
        Apttus_Config2__PriceListItem__c priceListItem
    ) {
        Apttus_Proposal__Proposal_Line_Item__c pli = generateProposalLineItem(proposal,product);
        pli.Apttus_QPConfig__ConfigurationId__c = productConfiguration.ID;
        pli.Apttus_QPConfig__PriceListItemId__c = priceListItem.ID;
        // pli.Apttus_QPConfig__DerivedFromId__c = testLine.ID;
        // pli.Apttus_QPConfig__ItemSequence__c = 1;
        // pli.Apttus_QPConfig__LineNumber__c = 1;
        pli.Apttus_QPConfig__IsPrimaryLine__c = true;
        return pli;
    }


}