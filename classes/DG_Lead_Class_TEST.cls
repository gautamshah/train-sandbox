@isTest
private class DG_Lead_Class_TEST {
	static testMethod void ChildCreationUpdate_TEST() {
		
		Group RMSParentQueue = [Select g.id, g.name From Group g where g.type = 'Queue' 
    	and g.name = 'RMS Parent Lead Queue' LIMIT 1];
    	
    	RecordType RMSRType = [Select r.Name, r.Id From RecordType r
    	where r.SobjectType = 'Lead' and r.Name = 'RMS Lead Type' LIMIT 1];
		
		List<Campaign> listCampaign = new List<Campaign>();
		
		Campaign c1 = new Campaign(
        Name = 'Campaign A');
        
        listCampaign.add(c1);
        
       	Campaign c2 = new Campaign(
        Name = 'Campaign B');
        
        listCampaign.add(c2);
        
        insert listCampaign;
        
        system.debug('***Camp A: ' + c1.id);
        system.debug('***Camp B: ' + c2.id);
        
        List<Lead> listLead = new List<Lead>();        
        
        Lead l1 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = '94080',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = 'OPG1,OPG2', 
        Most_Recent_Product_Solution_of_Interest__c = 'Prod1,Prod2', 
        LastName = 'test', 
        Is_Parent__c = true, 
        Has_Interest__c = true,
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test',
        Campaign_Most_Recent__c = c1.id   
        );
        
        listLead.add(l1);
        
        Lead l2 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = '94080',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        Most_Recent_Product_Solution_of_Interest__c = 'Prod1', 
        LastName = 'test', 
        Is_Parent__c = true, 
        Has_Interest__c = true,
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test',
        Campaign_Most_Recent__c = c1.id   
        );
        
        listLead.add(l2);
               
        Lead l3 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = '94080',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        Most_Recent_Product_Solution_of_Interest__c = Null, 
        LastName = 'test', 
        Is_Parent__c = true, 
        Has_Interest__c = true,
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test',
        Campaign_Most_Recent__c = c1.id   
        );
        
        listLead.add(l3);
        
        Lead l4 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = 'AUS',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = 'Blank', 
        Most_Recent_Product_Solution_of_Interest__c = Null, 
        LastName = 'test', 
        Is_Parent__c = true, 
        Has_Interest__c = true,
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'AUS', 
        Company = 'test',
        Campaign_Most_Recent__c = c1.id   
        );
        
        listLead.add(l4);
        
        Lead l5 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = 'AUS',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = Null, 
        Most_Recent_Product_Solution_of_Interest__c = Null, 
        LastName = 'test', 
        Is_Parent__c = true, 
        Has_Interest__c = false,
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'AUS', 
        Company = 'test',
        Campaign_Most_Recent__c = c1.id   
        );
        
        listLead.add(l5);
        
        insert listLead;
        
        List<Lead> ls = [Select id from Lead where Parent_Lead__c = :l1.id];
        system.debug('***Child Count: ' + ls.size());
        
        l1.Opportunity_Product_Group__c = 'OPG2,OPG3';
        l1.Most_Recent_Product_Solution_of_Interest__c = 'Prod2,Prod2B,Prod3';
        l1.Has_Interest__c = true;
        l1.Campaign_Most_Recent__c = c2.id;
        
        l2.Opportunity_Product_Group__c = 'OPG1';
        l2.Most_Recent_Product_Solution_of_Interest__c = Null;
        l2.Has_Interest__c = true;
        l2.Campaign_Most_Recent__c = c2.id;
        
        l3.Opportunity_Product_Group__c = 'OPG1';
        l3.Most_Recent_Product_Solution_of_Interest__c = 'Prod1B';
        l3.Has_Interest__c = true;
        l3.Campaign_Most_Recent__c = c2.id;
        
        l5.Opportunity_Product_Group__c = 'Blank';
        l5.Most_Recent_Product_Solution_of_Interest__c = 'Prod1B';
        l5.Has_Interest__c = true;
        
        update listLead;
        
	}

    static testMethod void AssignLeads_TEST() {
    	
    	List<User> listUser = new List<User>();
    	
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
		
		User u1 = new User(Alias = 'cov1', Email='covstandarduser1@testorg.com', 		
		EmailEncodingKey='UTF-8', LastName='cov1', LanguageLocaleKey='en_US', 
		LocaleSidKey='en_US', ProfileId = p.Id, 
		TimeZoneSidKey='America/Los_Angeles', UserName='covstandarduser1@testorg.com');
		
		listUser.add(u1);
		
		User u2 = new User(Alias = 'cov2', Email='covstandarduser2@testorg.com', 		
		EmailEncodingKey='UTF-8', LastName='cov2', LanguageLocaleKey='en_US', 
		LocaleSidKey='en_US', ProfileId = p.Id, 
		TimeZoneSidKey='America/Los_Angeles', UserName='covstandarduser2@testorg.com');
		
		listUser.add(u2);

		User u3 = new User(Alias = 'cov3', Email='covstandarduser3@testorg.com', 		
		EmailEncodingKey='UTF-8', LastName='cov3', LanguageLocaleKey='en_US', 
		LocaleSidKey='en_US', ProfileId = p.Id, 
		TimeZoneSidKey='America/Los_Angeles', UserName='covstandarduser3@testorg.com');
		
		listUser.add(u3);
		
		User u4 = new User(Alias = 'cov4', Email='covstandarduser4@testorg.com', 		
		EmailEncodingKey='UTF-8', LastName='cov4', LanguageLocaleKey='en_US', 
		LocaleSidKey='en_US', ProfileId = p.Id, 
		TimeZoneSidKey='America/Los_Angeles', UserName='covstandarduser4@testorg.com');
		
		listUser.add(u4);
		
		insert listUser;
		
		List<Lead_Assignment__c> listLA = new List<Lead_Assignment__c>();
		
		Lead_Assignment__c la1 = new Lead_Assignment__c(
		User__c = u1.id, 
		Postal_Code__c = '94080', 
		Opportunity_Product_Group__c = 'OPG1', 
		Country__c = 'US'     
        );
        
        listLA.add(la1);
		
		Lead_Assignment__c la2 = new Lead_Assignment__c(
		User__c = u1.id, 
		Postal_Code__c = '94080', 
		Opportunity_Product_Group__c = 'OPG1', 
		Country__c = 'US'     
        );
        
        listLA.add(la2);
        
        Lead_Assignment__c la3 = new Lead_Assignment__c(
		User__c = u1.id, 
		Postal_Code__c = '94080', 
		Opportunity_Product_Group__c = 'OPG1', 
		Country__c = 'US'     
        );
        
        listLA.add(la3);
        
        Lead_Assignment__c la4 = new Lead_Assignment__c(
		User__c = u1.id, 
		Postal_Code__c = '94080', 
		Opportunity_Product_Group__c = 'OPG1', 
		Country__c = 'US'     
        );
        
        listLA.add(la4);
        
        insert listLA;	
        
        Group WaitQueue = [Select g.id, g.name From Group g where g.type = 'Queue' 
    	and g.name = 'Waiting Assignment Queue' LIMIT 1];
    	
    	map<String, Id> mapRType = new map<String, Id>();
    	for(RecordType r : [Select r.SobjectType, r.Name, r.Id From RecordType r
		where r.SobjectType = 'Lead' and r.Name in ('RMS Lead Type','Surgical Lead Type','General Lead Type')]){
    		mapRType.put(r.name,r.id);
    	}
    	    	
        List<Lead> listLead = new List<Lead>();      
                
        Lead l1 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = '94080',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l1);
        
        Lead l2 = new Lead(
        RecordTypeId = mapRType.get('Surgical Lead Type'), 
        PostalCode = '94080',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG2', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l2);
        
        Lead l3 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = '94111-1234',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l3);
        
        Lead l4 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = '94111-1234',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG2', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l4);
        
        Lead l5 = new Lead(
        RecordTypeId = mapRType.get('Surgical Lead Type'), 
        PostalCode = '94080',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG3', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l5);
        
        Lead l6 = new Lead(
        RecordTypeId = mapRType.get('Surgical Lead Type'), 
        PostalCode = '94050',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG2', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l6);
        
        Lead l7 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = Null,
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l7);
        
        Lead l8 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = '9408A',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l8);
        
        Lead l9 = new Lead(
        RecordTypeId = mapRType.get('General Lead Type'), 
        PostalCode = Null,
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = Null, 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l9);
        
        Lead l10 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = Null,
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = Null, 
        LastName = 'test', 
        Is_Parent__c = true, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test'        
        );
        
        listLead.add(l10);
        
        Lead l11 = new Lead(
        RecordTypeId = mapRType.get('RMS Lead Type') , 
        PostalCode = '94080',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'China', 
        Company = 'test'        
        );
        
        listLead.add(l11);
        
        Lead l12 = new Lead(
        RecordTypeId = mapRType.get('Surgical Lead Type'), 
        PostalCode = '94080',
        OwnerId = WaitQueue.id,
        Opportunity_Product_Group__c = 'OPG2', 
        LastName = 'test', 
        Is_Parent__c = false, 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'China', 
        Company = 'test'        
        );
        
        listLead.add(l12);
        
        insert listLead;
        
        new DG_Lead_Class().AssignLeads();
    }
}