public with sharing class ProfileTabUserController {
private ApexPages.StandardController c;
public User subjectUser { get; set;}
public String subjectID {get;set;}

//UIDstring for the subject user (being viewed)
//Constructor method for the controller
public ProfileTabUserController(ApexPages.StandardController stdController)
{
    c = stdController;
    subjectID=getTargetSFDCUID();
    //If we're operating inside a tab running inside of a profile...
    if (subjectID != null)
    {
        //Inject the sfdc.userId URL parameter value into the id param 
        //so the std User controller loads the right User record
        ApexPages.currentPage().getParameters().put('id',subjectID);
    }

// Load the User record for the user whose profile we’re viewing
    this.subjectUser = (User)stdController.getRecord();
    Id subject = Id.valueOf(subjectID);
}
//Fetches URL parameter passed into a profile tab indicating which user is being viewed 

private String getTargetSFDCUID(){
        return
        ApexPages.currentPage().getParameters().get('sfdc.userId');
    }
}