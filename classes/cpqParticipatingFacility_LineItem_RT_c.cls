/**
CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
            Paul Berglund   Created.
===============================================================================

2017.01.25 - IL - This implementation of record types could be replaced with a "locked/unlocked" picklist
                  and validation rule, keeping things in config instead of code.
*/
public class cpqParticipatingFacility_LineItem_RT_c
{
    private static Map<Id, RecordType> rtMap = RecordType_u.fetch(cpqParticipatingFacility_Lineitem__c.class);

    private static Map<string, RecordType> DeveloperName2RecordType = new Map<string, RecordType>();

    static
    {
        for(RecordType rt : rtMap.values())
            DeveloperName2RecordType.put(rt.DeveloperName, rt);
    }

    public static Id RecordTypeId(RecordTypes type)
    {
        return DeveloperName2RecordType.get(convert(type)).Id;
    }

    public enum RecordTypes
    {
        Locked,
        Unlocked
    }

    public static string convert(RecordTypes type)
    {
        return type.name();
    }
}