/****************************************************************************************
 * Name    : PubActionController 
 * Author  : Gautam Shah
 * Date    : October 3, 2014 
 * Purpose : Controller for custom publisher actions for SF1
 * Dependencies: None
 * Reference By: PubActionRedirect[Object] VF pages
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        			AUTHOR              CHANGE
 * ----        			------              ------
 * October 3, 2014		Gautam Shah			Created
 *****************************************************************************************/
global with sharing class PubActionController 
{
	public PubActionController(){}

	@RemoteAction
	global static List<RecordType> getRecordTypes(String sObj)
	{
		String[] objs = new String[]{sObj};
		List<RecordType> recordTypes = new List<RecordType>();
		List<RecordType> results = new List<RecordType>();
		Map<Id,RecordType> rtMap = new Map<Id,RecordType>();
		String q = 'Select Id, Name From RecordType Where SobjectType = \'' + sObj + '\' and isActive = true';
		results = Database.query(q);
		for(RecordType rt : results)
			rtMap.put(rt.Id,rt);
		
		List<Schema.DescribeSObjectResult> R = Schema.describeSObjects(objs);
		List<Schema.RecordTypeInfo> RT = R[0].getRecordTypeInfos();
		for(Schema.RecordTypeInfo rtInfo : RT)
		{
			if(rtInfo.isAvailable())
				recordTypes.add(rtMap.get(rtInfo.getRecordTypeId()));
		}
		return recordTypes;
	}
}