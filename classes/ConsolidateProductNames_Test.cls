@IsTest (SeeAllData = true)
public class ConsolidateProductNames_Test  {

    static testMethod void TestConsolidatingProductNames() {

        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity Where HasOpportunityLineItem = true and Business_Unit__c = 'RMS' Limit 10];
        ConsolidateProductNames.Main(opps);

        for( Opportunity opp : opps)
        {
            System.Debug(opp.Name);
            System.Debug(opp.Product_Name_List__c);
            System.Debug(opp.Product_Family_List__c);
        }
    }
}