@isTest

private class TestFieldVisitApexShare2  {
    
    static testMethod void insertNewFv() {
        Profile p = [SELECT Id FROM Profile WHERE Name='US - SUS'];
        User rep = new User(Alias = 'test', Email='testuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='testtest@testorg.com');
        System.runAs(rep) {
      	Field_Visit__c fv = new Field_Visit__c();
        fv.RecordTypeId = '012U0000000MGkVIAW';
        //fv.Manager__c = '005U0000000fUaq';
        fv.Sales_Rep__c = '005U0000000eoJV';
        fv.FST__c = '005U0000000fv0G';
        fv.Seller__c = '005U0000000gZwz';	
        fv.RM__c = '005U0000000epjy';
        insert fv;
        }
    }
    
    static testMethod void updateFv() {
        Field_Visit__c fv = new Field_Visit__c();
        fv.RecordTypeId = '012U0000000LqoaIAC';
        fv.Manager__c = '005U0000000fUaq';
        fv.Sales_Rep__c = '005U0000000eoJV';
        fv.FST__c = '005U0000000fv0G';
        fv.Seller__c = '005U0000000gZwz';
        fv.RM__c = '005U0000000epjy';
        insert fv;
        
        fv.Manager__c = '005U0000000epPF';
        fv.Sales_Rep__c = '005U0000000epjy';
        fv.FST__c = '005U0000000gZwz';
        fv.Seller__c = '005U0000000fv0G';
        fv.RM__c = '005U0000000eoJV';
        update fv;
    }


}