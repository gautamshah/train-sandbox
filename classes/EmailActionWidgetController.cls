public with sharing class EmailActionWidgetController 
{
	public String recipients {get;set;}
	private String currCaseId {get;set;}
	public EmailActionWidgetController(ApexPages.StandardController controller)
	{
		this.currCaseId = controller.getId();
		recipients = '';
		Set<String> lUsers = new Set<String>();
		Set<String> lContacts = new Set<String>();
		for(CaseTeamMember ctm : [Select MemberId, Member.Email From CaseTeamMember Where ParentId = :currCaseId])
		{
			if(ctm.MemberId.getSObjectType() == User.SObjectType)
			{
				lUsers.add(ctm.MemberId);
			}
			else if(ctm.MemberId.getSObjectType() == Contact.SObjectType)
			{
				lContacts.add(ctm.MemberId);
			}
		}
		for(User u : [Select Email From User Where Id In :lUsers])
		{
			recipients += u.Email + ', ';
		}
		for(Contact c : [Select Email From Contact Where Id In :lContacts])
		{
			recipients += c.Email + ', ';
		}
		recipients = recipients.removeEnd(', ');
	}
}