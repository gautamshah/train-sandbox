global class cpqPriceListItem_g extends sObject_g 
{
    global cpqPriceListItem_g()
    {
        super(cpqPriceListItem_cache.get());
    }

    ////
    //// cast
    ////
    global static Map<Id, Apttus_Config2__PriceListItem__c> cast(Map<Id, sObject> sobjs)
    {
        return (sobjs != null) ?
            new Map<Id, Apttus_Config2__PriceListItem__c>((List<Apttus_Config2__PriceListItem__c>)sobjs.values()) :
            new Map<Id, Apttus_Config2__PriceListItem__c>();
    }

    global static Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> cast(Map<object, Map<Id, sObject>> source)
    {
        Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> target = new Map<object, Map<Id, Apttus_Config2__PriceListItem__c>>();
        for(object key : source.keySet())
            target.put(key, cast(source.get(key)));
            
        return target;
    }
    
    ////
    //// fetch sets
    ////
    global Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> fetch(sObjectField field, set<object> values)
    {
        return cast(this.cache.fetch(field, values));
    }

    global Map<Id, Apttus_Config2__PriceListItem__c> fetch(sObjectField field, object value)
    {
        return cast(this.cache.fetch(field, value));
    }
    
    ////
    //// This should only be used during tests to avoid govenor limits
    ////
    public Map<Id, Apttus_Config2__PriceListItem__c> fetch()
    {
        return cast(this.cache.fetch());
    }

    ////
    //// Additional methods
    ////

    // Variables related to the Price List Item in CPQ
    public static Map<Id, Apttus_Config2__PriceListItem__c> PriceListItems = new Map<Id, Apttus_Config2__PriceListItem__c>();
}