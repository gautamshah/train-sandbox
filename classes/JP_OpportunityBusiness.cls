public with sharing class JP_OpportunityBusiness{
     /****************************************************************************************
     * Name    : JP_OpportunityBusiness 
     * Author  : Hiroko Kambayashi
     * Date    : 10/11/2012 
     * Purpose : Business logic to input Intimacy Information to Opportunity record  
     *           by searching KeyDoctor in JP_Intimacy__c record.
     *           To input Account_Extended_Profile__c.Id to Opportunity record 
     *           by searching Business Unit and AccountId in Account_Extended_Profile__c record.
     *           To delete OpportunityLineItem records, and insert copied OpportunityLineItem records 
     *           when Opportunity.JP_Sales_start_Date__c is updated.
     *           
     * Dependencies: Opportunity Object
     *             , JP_Intimacy__c Object
     *             , Account_Extended_Profile__c Object
     *             , OpportunityLineItem Object
     *
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 11/08/2012  Hiroko Kambayashi    adding the method "inputBUInfoByBusinessUnit"
     * 11/09/2012  Hiroko kambayashi    adding the method "reDevideRevenueSchaduleBySalesStartDate"
     * 11/12/2015  Syu    Iken          dorrecting the mtehod "reDevideRevenueSchaduleBySalesStartDate"
     * 12/10/2015  Syu    Iken          delete the mtehod "inputIntimacyByKeyDoctor" and "inputBUInfoByBusinessUnit"
     *****************************************************************************************/
	
    /*
     * Set JP_Intimacy__c value selected from JP_Intimacy__c object by Field JP_Key_Dr__c and OwnerId on Opportunity
     * @param  List<Opportunity> oppList        Opportunity List which is handed by JP_OpportunityTriggerHandler from Trigger.new
     * @param  Set<String> uniqueIntimacyKey    The Set which is added Field JP_Key_Dr_Owner__c value on Opporutnity
     * @return none
     */
    /* 12/10/2015 Comment out
	public void inputIntimacyByKeyDoctor(List<Opportunity> oppList, Set<String> uniqueIntimacyKey){
		//The map which has JP_Contact_SR__c as key, and JP_Intimacy__c as value
		Map<String, String> intiMap = new Map<String, String>();
		for(JP_Intimacy__c intm : [SELECT Id, 
											JP_Contact_SR__c, 
											JP_Intimacy__c 
										FROM JP_Intimacy__c 
										WHERE JP_Contact_SR__c IN : uniqueIntimacyKey]){
			intiMap.put(intm.JP_Contact_SR__c, intm.JP_Intimacy__c);
		}
		//Set the value of intiMap to Opportunity record
		for(Opportunity opItem : oppList){
			String docotorKey = opItem.JP_Key_Dr__c;
			String intimacyValue = intiMap.get(opItem.JP_Key_Dr_Owner__c);
			if(docotorKey == null || docotorKey == ''){
				opItem.JP_Intimacy__c = '';
			} else {
				if(intimacyValue == null || intimacyValue == ''){
					opItem.JP_Intimacy__c = System.Label.JP_Opportunity_Intimacy_Nothing;
				} else {
					opItem.JP_Intimacy__c = intimacyValue;
				}
			}
		}
	}*/
	
    /*
     * Set Account_Extended_Profile__c id from Account_Extended_Profile__cobject by Field AccountId and JP_Business_Unit__c on Opportunity
     * @param  List<Opportunity> oppList      Opportunity List which is handed by JP_OpportunityTriggerHandler from Trigger.new
     * @param  Set<String> accountBunits      The Set which is added Field AccountId + '|' + Field JP_Business_Unit__c on Opportunity
     * @return none
     */
    /*12/10/2015 Comment out
	public void inputBUInfoByBusinessUnit(List<Opportunity> oppList, Set<String> accountBunits){
		//The map to get Account_Extended_Profile__c selected by Opportunity info
		Map<String, Id> inputBUMap = new Map<String, Id>();
		//Get Account_Extended_Profile__c Id and set it to the value of inputBUMap
		for(Account_Extended_Profile__c extProf : [SELECT Id, 
														JP_Account_Business_Unit__c 
													FROM Account_Extended_Profile__c 
														WHERE JP_Account_Business_Unit__c IN : accountBunits 
																ORDER BY LastModifiedDate]){
			
			inputBUMap.put(extProf.JP_Account_Business_Unit__c, extProf.Id);
		}
		//Set the value of inputBUMap to Opportunity record
		for(Opportunity opp : oppList){
			String oppKey = opp.AccountId + '|' + opp.JP_Business_Unit__c;
			String oppValue = inputBUMap.get(oppKey);
			if(oppValue != null){
				opp.JP_Account_Extended_Profile__c = oppValue;
			}
		}
	}*/
	
    /*
     * Delete OpportunitylineItem records and insert copied OpportunitylineItem records when Field JP_Sales_start_Date__c is updated on Opportunity
     * @param  List<Opportunity> oppList      Opportunity List which is handed by JP_OpportunityTriggerHandler from Trigger.new
     * @return none
     */
	public void reDevideRevenueSchaduleBySalesStartDate(Set<Id> oppIds){
		//OpportunityLineItem List for delete
		//2015.11.06 ecs i-shu add start
		/*CVJ_SELLOUT_DEV-44 of fix*/
		List<OpportunityLineItem> dltTargetList = new List<OpportunityLineItem>();
        for(OpportunityLineItem lineItem :[SELECT Id, 
                                                    Quantity, 
                                                    TotalPrice, 
                                                    Description, 
                                                    OpportunityId, 
                                                    Product_Type__c, 
                                                    PricebookEntry.Product2Id, 
                                                    PricebookEntry.Pricebook2Id, 
                                                    ServiceDate, 
                                                    JP_Target_Product_Category__c,
                                                    JP_OriginalTotalPrice__c 
                                                FROM OpportunityLineItem 
                                                WHERE OpportunityId IN : oppIds 
                                                    AND (
                                                           (PricebookEntry.Product2.CanUseRevenueSchedule = true AND PricebookEntry.Product2.NumberOfRevenueInstallments = NULL)
                                                        OR (PricebookEntry.Product2.CanUseQuantitySchedule = true AND PricebookEntry.Product2.NumberOfQuantityInstallments = NULL)
                                                        )
                                           ]
              ){
            dltTargetList.add(lineItem);
        }
		
		
		//ecs i-shu mod start
		/*List<OpportunityLineItem> dltTargetList = new List<OpportunityLineItem>();
		for(OpportunityLineItem lineItem :[SELECT Id, 
													Quantity, 
													TotalPrice, 
													Description, 
													OpportunityId, 
													Product_Type__c, 
													PricebookEntry.Product2Id, 
													PricebookEntry.Pricebook2Id, 
													ServiceDate, 
													JP_Target_Product_Category__c, 
													JP_OriginalTotalPrice__c 
												FROM OpportunityLineItem 
												WHERE OpportunityId IN : oppIds 
													AND PricebookEntry.Product2.CanUseRevenueSchedule = true 
														AND PricebookEntry.Product2.NumberOfRevenueInstallments = NULL]){
			dltTargetList.add(lineItem);
		}*/
		//ecs i-shu mod end
		
		//2015.11.06 ecs i-shu add end
		if(!dltTargetList.isEmpty()){
			//Copy of OpportunityLineItem List which was acquired then Create new OpportunityLineItem List
			List<OpportunityLineItem> insTargetlist = dltTargetList.deepClone();
			delete dltTargetList;
			insert insTargetlist;
		}
	}
}