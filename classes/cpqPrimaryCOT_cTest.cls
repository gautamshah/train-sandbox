/*
ClassOfTradeTest

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-12/06      Henry Noerdlinger   Created
===============================================================================
*/
@isTest
public class cpqPrimaryCOT_cTest {
	
	@isTest static void itCreatesPrimaryCotExpressionForParticipatingFacility() 
	{
		cpqClassOfTrade_c primary = new cpqPrimaryCOT_c('-003');
		String expectedValue = ' (  NOT Class_of_Trade__c IN (\'003\'))';
		System.assertEquals(expectedValue, primary.buildParticipatingFacilityExpression(null));
		
	}

	@isTest static void itCreatesPrimaryCotExpressionForDistributor() 
	{
		cpqClassOfTrade_c primary = new cpqPrimaryCOT_c('-003');
		String expectedValue = ' ( Class_of_Trade__c IN (\'003\'))';
		System.assertEquals(expectedValue, primary.buildDistributorExpression(null));
	}
}