global class UpdatePricingService{

    global class PriceResponseAck {
        webservice String PricingRequestID;
        webservice String StatusCode;
    }
    
    global class PricingResponse {
        webservice String RequestError;
        webservice String RequestErrorDescription;
        webservice List<SKUType> SKUs;
        webservice String PricingRequestID;
    }
   /*
    global class SKUType{
        webservice String SKUCode;
        webservice String SalesClass;
        webservice String UOM;
        webservice String SKUErrorDescription;
        webservice String SKUError;
        webservice List<PriceType> Price;
    }

    global class PriceType{
        webservice String QuantityRange;
        webservice Date EffectiveDate;
        webservice Date ExpirationDate;
        webservice Decimal UnitPrice;
        webservice Decimal Markup;
        webservice Decimal EndCustomerPrice;
        webservice String ContractNumber;
        webservice String ContractDesignator;
        webservice String ContractDescription;
        webservice Integer SequenceNumber;
        webservice String PriceCurrency;
        webservice String PriceError;
        webservice String PriceErrorDescription;
    }
   */
    webService static PriceResponseAck updatePricingInformation (PricingResponse pResponse) {
        PriceResponseAck prAck = new PriceResponseAck();
        prAck.PricingRequestId= pResponse.PricingRequestID;
        prAck.StatusCode='SUCCESS';
        Integer v_err_loc = 0;
        try
        {
	        list<PRICING_INTEGRATION__c> pricing = new list<PRICING_INTEGRATION__c>();
	        Integer key = 0;
	        map<Integer, SKUType>requestMap = new map<Integer, SKUType>();
	        map<Integer, PRICING_INTEGRATION_SKU_TYPE__c>skuMap = new map<Integer, PRICING_INTEGRATION_SKU_TYPE__c>();
	        for(SKUType st : pResponse.SKUs)
	        {
	        	v_err_loc = 10;
	        	requestMap.put(key,st);
	        	skuMap.put(key, new PRICING_INTEGRATION_SKU_TYPE__c(SKUCode__c = st.SKUCode
	        	                                                   ,SalesClass__c = st.SalesClass
	        	                                                   ,UOM__c = st.UOM
	        	                                                   ,SKUErrorDescription__c = st.SKUErrorDescription
	        	                                                   ,SKUError__c = st.SKUError
	        	                                                   ,PricingRequestID__c = pResponse.PricingRequestID));
	        	key++;
	        }
	        
	        if(skuMap.values().size() > 0)
	        {
	          v_err_loc = 15;
	          insert skuMap.values();
	          v_err_loc = 20;
	          for(Integer i : skuMap.keySet())
	          {
	          	  v_err_loc = 30;
	          	  PRICING_INTEGRATION_SKU_TYPE__c ps = skuMap.get(i);
	          	  if(requestMap.get(i).Price != null)
	          	  {
		          	  v_err_loc = 40;
		          	  for(PriceType p : requestMap.get(i).Price)
		          	  {
		          	  	  pricing.add( new PRICING_INTEGRATION__c(PRICING_INTEGRATION_SKU_TYPE__c = ps.Id
		          	  	                                         ,QuantityRange__c = p.QuantityRange
		          	  	                                         ,EffectiveDate__c = p.EffectiveDate
		          	  	                                         ,ExpirationDate__c = p.ExpirationDate
		          	  	                                         ,UnitPrice__c = p.UnitPrice
		          	  	                                         ,Markup__c = p.Markup
		          	  	                                         ,EndCustomerPrice__c = p.EndCustomerPrice
		          	  	                                         ,ContractNumber__c = p.ContractNumber
		          	  	                                         ,ContractDesignator__c = p.ContractDesignator
		          	  	                                         ,ContractDescription__c = p.ContractDescription) );
		          	  }
	          	  }
	          } 
	          v_err_loc = 50;
	          if(pricing.size() > 0)
	             insert pricing;
	          
	        }
        }
	    catch(Exception e)
	    {
	    	prAck.StatusCode = e.getMessage()+ ' Location '+ v_err_loc;
	    }
        return prAck; 
    }
}