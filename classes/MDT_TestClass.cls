@isTest
private class MDT_TestClass {


   static testMethod void TestPageFunctionality() {
      
        MDT m = new MDT();

        
        // Create MDT_Rep_Detail Record
        MDT_RepDetail__c mdt = new MDT_RepDetail__c( BU__c = 'Spine',
        City__c = 'Norwood',
        State__c = 'MA',
        Org__c = 'MITG',
        Cust_Name__c = 'Norwood Hospital',
        Cust_Num__c = '999999',
        Postal_Code_5_Digit__c = '02062');  
        insert mdt;   
        
       
          // Create Sales Definition Record
        Sales_Definitions__c sales_def = new Sales_Definitions__c( 
        Definition__c = 'Definiton Goes Here',
        Examples__c = 'My Examples',
        Resources__c = 'URL For additional Info',
        Term__c = 'Sales Definition #1');  
        insert sales_def;   
       
       // Create Zip To Account Record
        ZipToAccount__c Z_to_A = new  ZipToAccount__c( 
        City__c = 'Norwood',
        Cust_Name__c = 'Norwood Hospital',
        Cust_Num__c = '999999',
        Org__c = 'MITG',
        Postal_Code_5_Digit__c = '02062',
        State__c = 'MA');  
        insert Z_to_A;   
       
        // Set Variable Value
        m.searchTerm = 'Norwood Hospital';
        m.searchstring = '02062';
        m.selectedAccountId = '999999';
        m.selectedAccountName = 'Norwood Hospital';
        m.selectedZip = '02062';
        m.selectedAccountCity = 'Norwood';
        m.selectedAccountState = 'MA';
        m.SelectedOrg = 'MITG';
        m.selectedAccountState = 'MA';
       
    

        
        PageReference pageRef = Page.MDT; 
        PageReference pageRef2 = Page.MDT2;
        Test.setCurrentPageReference(pageRef);
        Test.setCurrentPageReference(pageRef2);
  
        //Execute Methods
       
        m.ResetPage(); 
        m.MainMenu();
        m.getStates();
        m.getUniqueCity();
        m.getAccounts();
        m.getAccountsByState();
        m.searchMain();
        m.searchByState();
        m.RetrieveRepByAccount();
        m.ShowPostalCodeSearch();
        m.ShowOrgDefinitions();
        m.ShowSearchByCityandState();
        m.TrackAccountSearch();
        m.trackVisits();
        m.UpdateForm();
        m.ShowCities();
        m.ShowAccounts();
        m.ShowAccounts();
        m.IsMobile();
        m.IsDesktop();
        m.getOrgs();
        //Asserts 
       
  }   
}