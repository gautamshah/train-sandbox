/**
Test class

CPQ_Attachment_Before

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-04-08      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_Attachment_Trigger {
	static List<Attachment> testAttachments;
	
    static void createTestData()
    {
        Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement');
		insert testAgreement;

		CPQ_Error_Log__c testAny = new CPQ_Error_Log__c();
		insert testAny;
		
 		testAttachments = new List<Attachment> {
 			new Attachment(Name = 'ATT1', ParentID = testAgreement.ID, Body = Blob.valueOf('Test')),
 			new Attachment(Name = 'ATTAny', ParentID = testAny.ID, Body = Blob.valueOf('Test'))
 		};
 		insert testAttachments;
    }

    static testMethod void myUnitTest() {
    	createTestData();
    	
    	Test.startTest();
	    	try {
	        	testAttachments[0].Name = 'ATT2';
	    		update testAttachments[0];
	    	} catch (Exception e) {}
	    	
	    	try {
	    		delete testAttachments[0];
	    	} catch (Exception e) {}
	    	
	    	try {
	        	testAttachments[1].Name = 'ATTAny2';
	    		update testAttachments[1];
	    	} catch (Exception e) {}
    	Test.stopTest();
    }
	
    static testMethod void TestDisabledTrigger() {
 		createTestData();
 		
    	try {
			CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, CPQ_Attachment_Before__c = true);
 			insert testTrigger;
		} catch (Exception e) {}
 		
        Test.startTest();
	        try {
	    		delete testAttachments[0];
	    	} catch (Exception e) {}
        Test.stopTest();
    }
}