/****************************************************************************************
 * Name    : CCACaseTriggerTest
 * Author  : John Romanowski
 * Date    : 24/04/2017 
 * Purpose : Test Contract Compliance Automation (CCA) triggers
 * Dependencies: 
 *
 * MODIFICATION HISTORY 
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
@isTest
public class CCACaseTriggerTest {
    //Test on close case fields in parent are updated
    static testmethod void testNotifyParentCaseClosed() {
        // Insert Agreement
        Agreement__c agree = new Agreement__c(
           Name = 'Test Agreement'
        );
        insert agree;
        
        // Insert Compliance Requirement
        Compliance_Requirement__c requirement = new Compliance_Requirement__c(
           Name = 'Test Requirement'
           ,Compliance_ID__c = agree.Id
           ,CurrencyIsoCode = 'USD'
        );
        insert requirement;
        
        //Insert Case
        Case c = new Case(
            RecordType = [SELECT Id FROM RecordType WHERE Name = 'Agreement Compliance' LIMIT 1]
            ,Compliance_Requirement__c = requirement.Id
            ,Status = 'New'
        );
        insert c;
        
        //Update Case to fire the trigger
        c.Status = 'Closed-Resolved';
        c.Compliance__c = 'Unresolved';
        c.Resolution_Desc__c = 'done';
        update c;
        
        //Make sure updates
        Compliance_Requirement__c requireTest = [SELECT Id,CaseClosedDate__c,CaseCreated__c,Out_of_Compliance__c,CanOpenCase__c FROM Compliance_Requirement__c WHERE Id = :requirement.id];  
        System.assert(requireTest.CaseClosedDate__c == date.today());
        System.assert(requireTest.CaseCreated__c == FALSE);
        System.assert(requireTest.Out_of_Compliance__c == FALSE);
        System.assert(requireTest.CanOpenCase__c == FALSE);
    }
}