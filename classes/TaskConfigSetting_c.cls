public class TaskConfigSetting_c extends sObject_c
{
/*
	private static TaskConfigSetting_g cache = new TaskConfigSetting_g();
	
    public static FINAL string SYSTEM_PROPERTIES = 'System Properties';
 		
	public static final string DEFAULT_USER_PROFILE_NAMES =
		'-All' + CONSTANTS.CRLF + // This clears the array initially
		'+APTTUS - RMS US Sales (RS+PM)' + CONSTANTS.CRLF +
		'+APTTUS - SSG US Account Executive';
	
	public static final Set<string> ADMIN_PROFILE_NAMES = new Set<string>
	{
		'System Administrator',
		'CRM Admin Support'
	};

	private static final Map<Id, Profile> profileMap
		= new Map<Id, Profile>(
			[SELECT Id,
			        Name
			 FROM Profile]);

    private static Map<Id, RecordType> recTypeMap =
    	RecordType_u.fetch(Apttus__APTS_Agreement__c.class);
        

    public static TaskConfigSetting__c SystemProperties
    {
    	get
    	{
    		if (SystemProperties == null)
    		{
    			load();
				SystemProperties = fetch();
    		}
    		
    		return SystemProperties;
    	}
    	
    	private set;
    }

	//// We only need to return a record with the Name of 'System Properties'
	private static TaskConfigSetting__c fetch()
	{
		Map<Id, TaskConfigSetting__c> found = cache.fetch(TaskConfigSetting__c.field.Name, SYSTEM_PROPERTIES);
		//// There needs to be 1 record for this to work
		if (found == null || found.isEmpty()) return null;
		
		return found.values()[0];
	}

	////
	////
	////
    private static void load()
    {
       	TaskConfigSetting__c cached = fetch();
       	if (cached == null)
       	{
       		TaskConfigSetting__c cs;
			try
			{
       			cs = getFromDB();
        	}
        	catch (QueryException e)
        	{
        		//// This code would only run if there was no
        		//// existing record - if one already exists,
        		//// it gets cached
	            if (Test.isRunningTest())
	            {
	                insert create();
	        		cs = getFromDB();
	            }
            }
            cache.put(cs);
        }
    }

	public static void reload()
	{
    	cache.clear(); //// Start with an empty cache and indexes
		load();
	}
	
	////
	////
	////
    private static TaskConfigSetting__c getFromDB()
    {
	    string query =
	    	SOQL_select.buildQuery(
	    		TaskConfigSetting__c.sObjectType,
	    		' WHERE Name = :SYSTEM_PROPERTIES ',
	    		null,
	    		1);

		return (TaskConfigSetting__c)Database.query(query);
    }
    
	private static TaskConfigSetting__c create()
	{
		TaskConfigSetting__c setting = new TaskConfigSetting__c();
		setting.Name = SYSTEM_PROPERTIES; // May need to expand on this?
		setting.CPQ_Profile_s__c = DEFAULT_USER_PROFILE_NAMES;
		
		return setting;
	}
*/
}