@isTest
private class Test_Account_Terr_Contact_Affiliation {
/*********************************************************************
*
* Updates
*
*  Date      Author               Description
* ----------------------------------------------------------------------
*
*  8-3-2012   MJM         Re-configured to used test account creation from TestUtility class
*
*********************************************************************/
    static testMethod void runTest() {
        // Create data
        /*Territory t = new Territory();
        t.Name = 'TestTerr';
        t.Custom_External_TerritoryID__c = '1111';
        insert t;
        
        Territory t2 = new Territory();
        t2.Name = 'TestTerr2'; 
        t2.Custom_External_TerritoryID__c = '1111';
        insert t2; 
        */
        Territory t = [select Id, name, Custom_External_TerritoryID__c from Territory limit 1];
        
        Territory t2 = [select Id, name, Custom_External_TerritoryID__c from Territory where Id != :t.Id limit 1];
        
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','TestAcct');
        Account a2 = tu.testAccount('US-Healthcare Facility','TestAcct');
        
        Contact c = new Contact();
        c.LastName = 'x';
        c.AccountId = a.Id;
        List<RecordType> recordTypes = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND Name = 'Connected Clinician' LIMIT 1]; 
        c.RecordTypeId = recordTypes[0].Id;
        insert c;
                
        Contact_Affiliation__c ca = new Contact_Affiliation__c();
        ca.AffiliatedTo__c = a.Id;
        ca.Contact__c = c.Id;
        insert ca;
        
        Contact_Affiliation__c ca2 = new Contact_Affiliation__c();
        ca2.AffiliatedTo__c = a2.Id;
        ca2.Contact__c = c.Id;
        insert ca2;
        
        Account_Territory__c  at = new Account_Territory__c();
        at.Territory_ID__c = t.Id;
        at.AccountID__c = a.Id;
        insert at;
        
        Account_Territory__c  at2 = new Account_Territory__c();
        at2.Territory_ID__c = t.Id;
        at2.AccountID__c = a2.Id;
        insert at2;

        List<Contact_Affiliation__c> cas = new List<Contact_Affiliation__c>();
        
        cas.add(ca);
        cas.add(ca2);
        
        update cas;
        
        
    }
}