public class Profile_u extends sObject_u
{
	private static Profile_g cache = new Profile_g();
	
	   public static set<string> fieldsToInclude =
        new set<string>
        {
            'Id',
            'Description',
            'Name',
            'UserLicenseId',
            'UserType'
        };
	
	static
    {
        load();
    }
    
    ////
    //// load
    ////
    public static void load()
    {
        if (cache.isEmpty())
        {
            List<Profile> fromDB = fetchFromDB();
            cache.put(fromDB);
        }
    }
	
	////
	//// fetch
	////
	public static Map<Id, Profile> fetch()
	{
		return cache.fetch();
	}
	
	////
	//// fetch(sets)
	////

    public static Map<Object, Map<Id, Profile>> fetch(sObjectField field, set<string> names)
    {
  	  return cache.fetch(field, DATATYPES.castToObject(names));
    }
  
  	 ////
    //// fetchFromDB
    ////
    @testVisible
    private static List<Profile> fetchFromDB()
    {
        string query =
            SOQL_select.buildQuery(
                Profile.sObjectType,
                fieldsToInclude,
                null,
                null,
                null);
        
        return (List<Profile>)Database.query(query);
    }

}