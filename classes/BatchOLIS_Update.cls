/****************************************************************************************
* Name    : Class: BatchOLIS_Update -> Batchable
* Author  : Gautam Shah
* Date    : 4/5/2014
* Purpose : Copies the Opportunity Close Date to each Opportunity Product Schedule record
* 
* Dependancies: 
* 	Subsequent: BatchOppRevenueScheduleUpdate	->	Needs to be manually invoked after this batch process has completed.
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
public class BatchOLIS_Update implements Database.Batchable<sObject>
{
	private String query;
	private List<OpportunityLineItemSchedule> olisList = new List<OpportunityLineItemSchedule>();
	public BatchOLIS_Update()
	{
		query = 'Select Id, Description, OpportunityLineItem.Opportunity.CloseDate from OpportunityLineItemSchedule Where OpportunityLineItem.Opportunity.isDeleted = false And OpportunityLineItem.Opportunity.RecordType.DeveloperName In (\'US_AST_Opportunity\',\'US_SUS_Opportunity\') And (Not Description Like \'CloseDate%\')';
	}
	public Database.Querylocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		for(sObject obj : scope)
		{
			OpportunityLineItemSchedule olis = (OpportunityLineItemSchedule)obj;
			olis.Description = 'CloseDate:' + olis.OpportunityLineItem.Opportunity.CloseDate;
			olisList.add(olis);
		}
		List<Database.SaveResult> srList = Database.update(olisList, false);
		for(Database.SaveResult sr : srList)
		{
			if(!sr.isSuccess())
			{
				System.debug('Error in writing OLIS.Description field');
				for(Database.Error e : sr.getErrors())
				{
					System.debug('OLIS ID: ' + sr.getId() + '  -->  ' + e.getMessage());
				}
			}
		}
	}
	public void finish(Database.BatchableContext BC){}
}