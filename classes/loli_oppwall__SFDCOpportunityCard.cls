/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SFDCOpportunityCard extends loli_oppwall.VirtualCard {
    global SFDCOpportunityCard() {

    }
    global SFDCOpportunityCard(SObject item) {

    }
    global override String getCssClasses() {
        return null;
    }
    global override SObject newItem() {
        return null;
    }
}
