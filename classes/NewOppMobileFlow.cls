global with sharing class NewOppMobileFlow {
 
 /****************************************************************************************
   * Name    : NewOppMobileFlow 
   * Author  : Shawn Clark
   * Date    : 04/29/2015 
   * Purpose : Extends the Save feature on the Opportunity to redirect to Add Products 
   
   **Modifications: 
   *11/4/2016   S. Clark   Removed Write down from Type Picklist
   
 *****************************************************************************************/
    public Account o {get; private set;}
    Public String Opportunity_Name {get;set;}
    Public String Account_GUID {get;set;}
    Public String Account_Name {get;set;}
    Public String Type {get;set;}
    Public String Stage {get;set;}
    Public Decimal Amount {get;set;}
    //Public Decimal Probability {get; set;}
    Public String Forecast {get;set;}
    Public Date Close_Date {get;set;}
    Public String Dealers {get;set;}
    Public String ReccType {get;set;}
    Public String NewOpp {get;set;}
    Public String Description {get;set;}
    Public Boolean RecordTypeReadOnly {get;set;}
    Public List<SelectOption> sol {get;set;}
    public List<SelectOption> stages {get;set;}
    Public Boolean IsRecTypeDisabled {get;set;}
    Public Boolean IsStageDisabled {get;set;}
    Public Boolean IsForecastDisabled {get;set;}
    Public Boolean isSaveDisabled {get; private set;}
    public Integer PageLoadCount {get;set;}
 

    // SOQL query loads the case, with related Sample & Sample Product Data
    public NewOppMobileFlow (ApexPages.StandardController stdController) {
         this.o = (Account)stdController.getRecord();
         this.Account_GUID = this.o.id;
         this.Account_Name = [SELECT Name FROM Account where id = :o.id].Name;
         
    
   //If there is only One Record Type, Set it, and Disable the field    
        getOpportunityRecTypes();
           IF (sol.size() < 2) {
              IsRecTypeDisabled = true;
               ReccType = sol[0].getValue();
           }
           Else {
                IsRecTypeDisabled = false;
                ReccType = sol[0].getValue(); //Set It to the first value, so that it won't be null if they dont change it
           }
           
   // If There is only one Stage, set it and disable the picklist 
 
    IF(PageLoadCount==null) 
       {  
              Stage = 'Identify';
              Forecast = 'Omit (0% Probability)';
              IsForecastDisabled = true; 
       }
       
       ELSE 
    {
       PageLoadCount = PageLoadCount+1;
    }  

    }


    //Get Record Types for the Current User

    public List<SelectOption> getOpportunityRecTypes() {
        List <RecordType> rl;
        sol = new List<SelectOption>();
        rl = [select Id, Name from RecordType where sObjectType='Opportunity' and isActive=true];
       
        Schema.DescribeSObjectResult d = Schema.SObjectType.Opportunity;
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
       
        for (RecordType r : rl) {
                if(rtMapById.get(r.id).isAvailable()){
                    sol.add(new SelectOption(r.id,r.Name));
                }
         }
           return sol;
        }  
 
 
    //Get Opportunity Type Picklist Values

    public List<SelectOption> getForecastList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.RM_Forecast_Status__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            IF (f.getLabel().contains('Omit')
            || f.getLabel().contains('Pipeline')
            || f.getLabel().contains('Best Case')
            || f.getLabel().contains('Omit')     
            || f.getLabel().contains('Commit')
            || f.getLabel().contains('Closed')
            ) {
            options.add(new selectOption(f.getLabel(),f.getValue()));    
            }                
        }       
        return options;    
    }  
 
 
  
    //Get Opportunity Type Picklist Values

    public List<SelectOption> getTypeList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.Type.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            IF (f.getLabel() == 'At Risk' || f.getLabel() == 'New Business') {
            options.add(new selectOption(f.getLabel(),f.getValue()));    
            }                
        }    
        return Options;    
    }     
    
    //Get Opportunity Stages Picklist Values   
       
    public List<SelectOption> getStageList()    
    {    
        stages =  new List<SelectOption>();   
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
            {    
            IF (f.getLabel() == 'Identify' 
            || f.getLabel() == 'Develop'
            || f.getLabel() == 'Evaluate'
            //|| f.getLabel() == 'Propose'     //Med Supplies only wants Identify for Now
            || f.getLabel() == 'Negotiate'
            //|| f.getLabel() == 'Closed Won'
            //|| f.getLabel() == 'Closed Lost'
            //|| f.getLabel() == 'Closed (Resolved)' 
            ) {
            stages.add(new selectOption(f.getLabel(),f.getValue()));    
            }                
        }       
        return stages;    
    }
    
     //Retrieve Picklist values for Dealers on Opportunity Object     
      public List<SelectOption> getDealersList()    
      {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.Dealers__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
       }             
  
 
      public Void LockForecastCheck() 
      {
      IF(Stage == 'Identify') {
      Forecast = 'Omit (0% Probability)';
      IsForecastDisabled = true; 
         }
      ELSE IF (Stage != 'Identify') {
      IsForecastDisabled = false; 
         }
      }
 
  
       public PageReference Save() {
 
       isSaveDisabled = true;
       Opportunity opp = new Opportunity();
       opp.RecordTypeId = Recctype;                 
       opp.Name = Opportunity_Name;
       opp.AccountId = Account_GUID;
       opp.Type = Type;
       opp.Amount = Amount;
       opp.StageName = Stage;
       opp.CloseDate = Close_Date ;
       opp.Dealers__c = Dealers;
       opp.Created_via_Salesforce1__c = true;
       //opp.Confidence_Level__c = Probability;
       opp.RM_Forecast_Status__c = Forecast;
       opp.Description = Description;
            try{ 
            insert opp; 
            NewOpp = opp.Id;
            } 
            catch (Exception e) 
            {
            ApexPages.addMessages(e);
            }
            return null;
      }       
  
   }