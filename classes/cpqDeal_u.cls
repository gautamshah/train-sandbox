/**
Functions that operate on both CPQ Proposal and Agreement records.

    - Record Type = Actual record types (e.g. RFA, RFA_Locked, RFA_AG, RFA_AG_Locked)
    - Deal Type = Core deal type of record type names (RFA). Based on Record Type Developer Name.
    - Name = Developer Name used in APIs (RFA, RFA_Locked)
    - Label = Presentation Name used in UIs (RFA, RFA Locked)
    - getXbyY = Returns Map<Y,X>
    - getXbyYandZ = Returns Map<Y,Map<Z,X>>

CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
10/10/2016  Paul Berglund   Created
10/18/2016  Isaac Lewis     Refactored methods for working with record types and deal types
10/25/2016  Paul Berglund	Refactored deal type logic to cpqDeal_Type_c
                           	Refactored record type logic to cpqDeal_RecordType_c
===============================================================================
*/
public abstract class cpqDeal_u extends sObject_u
{
	protected cpqDeal_u()
	{
		super();
	}

	protected cpqDeal_u(sObjectType sObjType, string whereClause)
	{
		super(sObjType, whereClause);
	}

    public override virtual void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<sObject> newList,
            Map<Id, sObject> newMap,
            List<sObject> oldList,
            Map<Id, sObject> oldMap,
            integer size
        )
    {

		system.debug(string.valueOf(this) + '.main.queries START: ' + Limits.getQueries());

        cpqDeal_SalesRepLevel_u.assign(newList, isInsert, isUpdate, isBefore);

        if (isBefore && (isInsert || isUpdate))
            new cpqDeal_Approver_u.Assignment().main(newList);

		system.debug(string.valueOf(this) + '.main.queries FINISH: ' + Limits.getQueries());
    }

	// cpqDeal_User_u (refactor)
	public abstract Set<Id> extractOwners(List<sObject> newList);
	public abstract Set<Id> extractApprovers(List<sObject> newList);

    // Utility methods for basic identification

    public static boolean isDeal (sObject deal) {
        return isAgreement(deal) || isProposal(deal);
    }

    public static boolean isAgreement (sObject deal) {
        return deal.getSObjectType() == cpq_u.AGREEMENT_SOBJECT_TYPE ? true : false;
    }

    public static boolean isProposal (sObject deal) {
        return deal.getSObjectType() == cpq_u.PROPOSAL_SOBJECT_TYPE ? true : false;
    }

	// 2017.01.23 - IL - Method isn't called anywhere, and logic doesn't branch correctly.
	// Commented out to increase code coverage
	public static Boolean isRecordDestinedToPartner (sObject record) {
        // checks if proposal or agreement will be sent to partner
        if (record.get('Destined_to_Partner__c') == false) {
            return false;
        }
        String DealTypeName = cpqDeal_Type_c.getDealType(record);
        System.Debug('*** DealTypeName ' + DealTypeName);
        if (DealTypeName != 'COOP_Plus_Program' && DealTypeName != 'Respiratory_Solutions_COOP')
            return true;

        CPQ_Variable__c cs = CPQ_Variable__c.getValues('Send COOP Deals to Partner');
        if (cs != null && cs.Value__c != 'On')
            return false;

        if (record.get('Deal_Type__c') == 'Amendment') {
            cs = CPQ_Variable__c.getValues('Send COOP ADJ Deals to Partner');
            if (cs != null && cs.Value__c != 'On')
                return false;
        }

        return true;
    }

}