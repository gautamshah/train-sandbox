/**
 * TerritoryUserListCtl Test Class
 */
@isTest
private class TerritoryUserListCtlTest{
    static testMethod void myTest(){
    List<String> terList = new List<String>();
    List<String> usrList = new List<String>();
    //List<String> userList = new List<String>();
    //*******************
    //create testUser1
    User testU1 = new User(FirstName = 't_fn1'
        , LastName = 't_ln1'
        , email='t_email1@covidien.com'
        , profileid = '00eU0000000dcmHIAQ'
        , UserName='t_usrnm1@covidien.com'
        , alias='t_alias1'
        , Franchise__c='t_franchise1'
        , Phone ='t_phone1'
        , CommunityNickName='t_cnnm1'
        , TimeZoneSidKey='America/New_York'
        , LocaleSidKey='en_US'
        , EmailEncodingKey='ISO-8859-1'
        , LanguageLocaleKey='en_US'
        , IsActive=True);
    insert testU1;
    System.assertNotEquals(null,testU1);

    System.runAs (testU1) {
        testUtility tu = new testUtility();
        Account a = tu.testAccount('Japan - Healthcare Facility','New Account Test Name 1');
        Account b = tu.testAccount('Japan - Healthcare Facility','New Account Test Name 2');

        //Test.setCurrentPageReference(Page.Ter_Usr_Info_In_Hp_V);
        System.assertNotEquals(null,a);
        //ApexPages.StandardController ctlr = new ApexPages.StandardController(a);
        //Ter_Usr_Info_In_Hp tul = new Ter_Usr_Info_In_Hp(ctlr);

        List<UserTerritory> utL = [Select Id, territoryID from UserTerritory LIMIT 10];
        List<ID> utIDL = new List<ID>();
        for (UserTerritory u: utL) {
            utIDL.add(u.territoryID);
        }
        System.assertNotEquals(0,utIDL.size());

        List<Territory> terL = [Select Id, name from Territory WHERE Id IN :utIDL limit 2];  
        //List<Territory> terL = [Select Id, name from Territory limit 1];
        for (Territory aTer: terL) {
            terList.add((String)aTer.get('Id'));
        }
        System.assertNotEquals(0,terList.size());
        //Account_Territory__c insert
        for (String aTer: terList) {
            Account_Territory__c at = new Account_Territory__c(AccountID__c = a.Id, Territory_ID__c = aTer);
            insert at;
        }
        ApexPages.StandardController ctlr = new ApexPages.StandardController(a);
        TerritoryUserListCtl tul = new TerritoryUserListCtl(ctlr);

        ctlr = new ApexPages.StandardController(b);
        tul = new TerritoryUserListCtl(ctlr);
        }
    }

}