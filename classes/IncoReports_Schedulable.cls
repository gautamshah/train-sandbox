global class IncoReports_Schedulable implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        IncoPPDReports ipr = new IncoPPDReports();
        ipr.sendCustomerReports();
        ipr.sendDistributorReports();
    }
}