/****************************************************************************************
 * Name    : SalesOut_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 15/10/2013 
 * Purpose : Contains all the logic coming from Sales Out trigger
 * Dependencies: SalesOutTrigger Trigger
 *             , SalesOut Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *22/5/2015  Amogh Ghodke        Removed check for status ACCEPTED,line no.47
 *****************************************************************************************/
 public without sharing class SalesOut_TriggerHandler {
  private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public SalesOut_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
   public void OnBeforeInsert(List<Sales_Out__c> newObjects){
     runValidations.lookupValidations(newObjects);
   }
   
   public void OnBeforeUpdate(List<Sales_Out__c> newObjects, Map<Id, Sales_Out__c> oldMap){
     
     List<Sales_Out__c> lsSalesOutToBeValidated = new List<Sales_Out__c>();
     for(Sales_Out__c so : newObjects)
     {
       Sales_Out__c old = oldMap.get(so.Id);
         
       //Cannot modify locked submission
       if(so.Submission_Locked__c && !Utilities.isSysAdminORAPIUser)
         so.addError(Label.Cannot_Modify_Locked_Submission);
       //Cannot Modify validated Record
       else if(Utilities.isPortalUser && 
          (old.Verification_Status__c=='On Hold' || old.Verification_Status__c=='Rejected' || old.Verification_Status__c=='Verified'))
         so.addError(Label.Cannot_Modify_Sales_Out_Record);
       //Cannot verify temp accounts
       else if((so.Verification_Status__c != old.Verification_Status__c) && 
           (so.Verification_Status__c == 'VERIFIED')&&
           ((so.Sell_to__c != null && so.Sell_to__c.left(4).toUpperCase()=='TEMP') || (so.Sell_from__c!=null && so.Sell_from__c.left(4).toUpperCase()=='TEMP')))
         so.addError(Label.Cannot_Verify_Sales_Out_with_TEMP_Account);
       //Enter reason code on verification
       else if(!Utilities.isSysAdminORAPIUser && !Utilities.isPortalUser && so.Submitted__c && (so.Reason_Code__c==null || so.Reason_Code__c=='') &&
                 (so.Selling_Price_Text__c != old.Selling_Price_Text__c ||
                   so.Quantity_Text__c  != old.Quantity_Text__c ||
                   so.UOM__c != old.UOM__c ||
                   so.Product_Code__c != old.Product_Code__c ||
                   so.Sell_To__c != old.Sell_To__c ||
                   so.Sell_From__c != old.Sell_From__c ||
                   so.Document_Date_Text__c != old.Document_Date_Text__c ||
                   so.Document_No__c != old.Document_No__c))
         so.addError(Label.Reason_Code_Required);
       else if(so.Submitted__c == old.Submitted__c)
         lsSalesOutToBeValidated.add(so);
     }
         
     runValidations.lookupValidations(lsSalesOutToBeValidated);
        
     for(Sales_Out__c so : lsSalesOutToBeValidated)
     {
        if(so.Submitted__c && ((so.Validation_Message__c!=null && so.Validation_Message__c.trim()!='')
                        || (so.Error_Messages__c!=null && so.Error_Messages__c.trim()!='')))
            so.Submitted__c = false;
     }
     
   }

   public void OnBeforeDelete(List<Sales_Out__c> oldObjects){
     
     //ID adminProfileID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].ID;
     //List<Profile> lsProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
     if(!Utilities.isSysAdminORAPIUser)
     {
       for(Sales_Out__c item : oldObjects)
       {
         if(!item.Can_be_Deleted__c)
         {
           item.addError(Label.Cannot_delete_Sales_Out);
         }
       }
     }
   }
}