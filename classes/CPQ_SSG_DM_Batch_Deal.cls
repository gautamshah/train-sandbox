global class CPQ_SSG_DM_Batch_Deal implements Database.Batchable<sObject>
{
    /****************************************************************************************
    * Name    : CPQ SSG DM Batch Deal
    * Author  : Subba Reddy Muchumari
    * Date    : 6/17/2015
    * Purpose : Batch Job used to SSG Staging Table Mapping to Apttus tables.
    * Dependencies: Related CPQ_SSG_STG_Deal__c,Opportunity,Apttus_Proposal__Proposal__c,
                    Apttus_Config2__ProductConfiguration__c, Account and CPQ_DM_Variable__c objects
    *
    *****************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //String query = 'SELECT Id,STG_EffectiveDate__c,STG_Status__c,STG_AccountNumber__c,STG_ASTCommitmentFlag__c,STG_CreatedByName_c__c,STG_DealDuration__c,STG_DealId__c,STG_OpportunityId__c FROM CPQ_SSG_STG_Deal__c where STG_AccountNumber__c=\'342881\'';
        String query = 'SELECT Id,STG_EffectiveDate__c,STG_Status__c,STG_AccountNumber__c,STG_ASTCommitmentFlag__c,STG_CreatedByName_c__c,STG_DealDuration__c,STG_DealId__c,STG_OpportunityId__c FROM CPQ_SSG_STG_Deal__c Where STG_Process__c = true';
        system.debug('query ****************'+query );
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<CPQ_SSG_STG_Deal__c> scope)
    {
        system.debug('scope*********'+scope);
        set<string> acctExtIds=new set<string>();
        set<string> createdNameSet=new set<string>();
        List<CPQ_Error_Log__c> errorLogs = new List<CPQ_Error_Log__c>();
        for(CPQ_SSG_STG_Deal__c c:scope) {
            acctExtIds.add('US-'+c.STG_AccountNumber__c );
            createdNameSet.add(c.STG_CreatedByName_c__c);
        }

        List<Account> accList=[select Id,Name,Account_External_ID__c from Account where Account_External_ID__c IN:acctExtIds];
        Map<string,Account> accListMap=new Map<string,Account>();
        for(Account a:accList) {
            accListMap.put(a.Account_External_ID__c,a);
        }
        List<CPQ_DM_Variable__c> CPQDMVarList=[Select Value__c,Name From CPQ_DM_Variable__c Where Type__c= 'User' And Name IN:createdNameSet];
        Map<string,string> CPQDMVarMap=new Map<string,string>();
        for(CPQ_DM_Variable__c cdv:CPQDMVarList) {
            CPQDMVarMap.put(cdv.Name,cdv.Value__c);
        }
        RecordType recordTypeId = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,'Agreement_Proposal');
        system.debug('recordTypeId*********'+recordTypeId);
        RecordType recordTypeIdOpp = RecordType_u.fetch(Opportunity.class,'US_SUS_Opportunity');
        //Inserting Opportunity records
        List<Opportunity> OppNewRecList=new List<Opportunity>();
        for(CPQ_SSG_STG_Deal__c c:scope) {
            if(c.STG_OpportunityId__c==Null)
            {
                Opportunity oppRec=new Opportunity();
                if(accListMap.containsKey('US-'+c.STG_AccountNumber__c))
                {
                    oppRec.AccountId=accListMap.get('US-'+c.STG_AccountNumber__c).Id;

                    system.debug('CPQDMVarMap***********'+CPQDMVarMap);
                    system.debug('c.STG_CreatedByName_c__c*********'+c.STG_CreatedByName_c__c);
                    system.debug('CPQDMVarMap.get(c.STG_CreatedByName_c__c*********'+CPQDMVarMap.get(c.STG_CreatedByName_c__c));
                    if(CPQDMVarMap.containskey(c.STG_CreatedByName_c__c))
                    {
                        oppRec.OwnerId=CPQDMVarMap.get(c.STG_CreatedByName_c__c);

                        oppRec.Legacy_Deal_ID__c=Decimal.valueof(c.STG_DealId__c);
                        oppRec.CloseDate=c.STG_EffectiveDate__c;
                        if(accListMap.containsKey('US-'+c.STG_AccountNumber__c))
                        {
                            oppRec.Name=accListMap.get('US-'+c.STG_AccountNumber__c).Name+' '+c.STG_DealId__c.substringBefore('.');
                            if(c.STG_Status__c=='Closed - Lost')
                            {
                                oppRec.StageName='Closed Lost';
                            }
                            else if(c.STG_Status__c=='Closed - Won')
                            {
                                oppRec.StageName='Closed Won';
                            }
                            else
                            {
                                oppRec.StageName='Draft';
                            }
                            //OppRec.RecordTypeId=recordTypeIdOpp.Id;
                            OppNewRecList.add(oppRec);
                        }
                    } else {
                        errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Deal', 'Error', 'User ' +  c.STG_CreatedByName_c__c + ' not found in CPQ_DM_Variable__c', 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
                    }
                } else {
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Deal', 'Error', 'Account not found with Account Number ' +  c.STG_AccountNumber__c, 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
                }
            }
        }
        //Insert OppNewRecList;

        List<Database.SaveResult> srsOppNewList = Database.insert(OppNewRecList, FALSE);
        for (Database.SaveResult sr : srsOppNewList )
        {
            if (sr.isSuccess())
            {

                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Opportunity Records: ' + sr.getId());
            }
            else
            {
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occurred.');
                    System.debug('Fields that affected this error: ' + err.getFields());
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Deal', 'Error', 'Insert Opportunity failed ' + err.getMessage(), null, null));
                }
            }
        }


        system.debug('OppNewRecList*********'+OppNewRecList);
        Map<decimal,Opportunity> oppNewRecMap=new Map<decimal,Opportunity>();

        for(opportunity o:OppNewRecList)
        {
            oppNewRecMap.put(o.Legacy_Deal_ID__c,o);
        }

        //Inserting Quote/Proposal records
        List<Apttus_Proposal__Proposal__c> apttusProRecList=new List<Apttus_Proposal__Proposal__c>();
        for(CPQ_SSG_STG_Deal__c c:scope)
        {
            Apttus_Proposal__Proposal__c apttusProRec=new Apttus_Proposal__Proposal__c();
            if(accListMap.ContainsKey('US-'+c.STG_AccountNumber__c))
            {
                apttusProRec.Apttus_Proposal__Account__c=accListMap.get('US-'+c.STG_AccountNumber__c).Id;
                if(c.STG_ASTCommitmentFlag__c==True)
                {
                    apttusProRec.Committed_Stapling_Vessel_Sealing__c='Yes';
                }
                if(c.STG_ASTCommitmentFlag__c==False)
                {
                    apttusProRec.Committed_Stapling_Vessel_Sealing__c='No';
                }
                if(CPQDMVarMap.ContainsKey(c.STG_CreatedByName_c__c))
                {
                    apttusProRec.OwnerId=CPQDMVarMap.get(c.STG_CreatedByName_c__c);
                    apttusProRec.Term_Months__c=string.valueof(c.STG_DealDuration__c*12);
                    apttusProRec.Legacy_External_Id__c=c.STG_DealId__c;
                    apttusProRec.Apttus_Proposal__ExpectedStartDate__c=c.STG_EffectiveDate__c;
                    if(c.STG_OpportunityId__c==Null)
                    {
                        system.debug('New Opportunity***********');

                        apttusProRec.Apttus_Proposal__Opportunity__c=oppNewRecMap.get(decimal.valueof(c.STG_DealId__c)).Id;
                    }
                    if(c.STG_OpportunityId__c!=Null)
                    {
                        system.debug('Existing Opportunity***********');
                        apttusProRec.Apttus_Proposal__Opportunity__c=c.STG_OpportunityId__c;
                    }
                    if(c.STG_Status__c.startswith('Assigned to'))
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='In Review';
                    }
                    if(c.STG_Status__c.startswith('Rejected'))
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='Denied';
                    }
                    if(c.STG_Status__c=='Approved')
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='Approved';
                    }
                    if(c.STG_Status__c=='Closed - Lost')
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='Denied';
                    }
                    if(c.STG_Status__c=='Closed - Won')
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='Accepted';
                    }
                    if(c.STG_Status__c=='DELETED')
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='Denied';
                    }
                    if(c.STG_Status__c=='Draft')
                    {
                        apttusProRec.Apttus_Proposal__Approval_Stage__c='Draft';
                    }
                    apttusProRec.RecordTypeId=recordTypeId.Id;
                    apttusProRecList.add(apttusProRec);

                }
            }
        }
        //Insert apttusProRecList;
        List<Database.SaveResult> srsapttusProProNewList= Database.insert(apttusProRecList, FALSE);
        for (Database.SaveResult sr : srsapttusProProNewList)
        {
            if (sr.isSuccess())
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Apttus Proposal Records: ' + sr.getId());
            }
            else
            {
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occurred.');
                    System.debug('Fields that affected this error: ' + err.getFields());
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Deal', 'Error', 'Insert Quote/Proposal failed ' + err.getMessage(), null, null));
                }
            }
        }


        system.debug('apttusProRecList*********'+apttusProRecList);
        Map<string,Apttus_Proposal__Proposal__c> apttusPropNewRecMap=new Map<string,Apttus_Proposal__Proposal__c>();
        for(Apttus_Proposal__Proposal__c acp: apttusProRecList)
        {
            apttusPropNewRecMap.put(acp.Legacy_External_Id__c,acp);
        }

        //Inserting Product Configuration records
        Apttus_Config2__PriceList__c apptusConfPriceList = cpqPriceList_c.SSG;
     
        List<Apttus_Config2__ProductConfiguration__c> apttusConfPrdNewRecList=new List<Apttus_Config2__ProductConfiguration__c>();
        for(CPQ_SSG_STG_Deal__c c:scope)
        {
            Apttus_Config2__ProductConfiguration__c apttusConfPrdRec=new Apttus_Config2__ProductConfiguration__c();
            if(CPQDMVarMap.containskey(c.STG_CreatedByName_c__c))
            {
                apttusConfPrdRec.OwnerId=CPQDMVarMap.get(c.STG_CreatedByName_c__c);
                apttusConfPrdRec.Legacy_External_Id__c=c.STG_DealId__c;
                apttusConfPrdRec.Apttus_Config2__Status__c='Saved';
                if(apttusPropNewRecMap.containskey(c.STG_DealId__c))
                {
                    apttusConfPrdRec.Apttus_QPConfig__Proposald__c=apttusPropNewRecMap.get(c.STG_DealId__c).Id;
                    apttusConfPrdRec.Apttus_Config2__VersionNumber__c=1;
                    apttusConfPrdRec.Apttus_Config2__BusinessObjectId__c=apttusPropNewRecMap.get(c.STG_DealId__c).Id;
                    apttusConfPrdRec.Apttus_Config2__BusinessObjectType__c='Proposal';
                    if(apptusConfPriceList != null)
                    {
                        apttusConfPrdRec.Apttus_Config2__PriceListId__c = apptusConfPriceList.Id;
                        apttusConfPrdRec.Apttus_Config2__EffectivePriceListId__c = apptusConfPriceList.Id;
                    }
                    apttusConfPrdNewRecList.add(apttusConfPrdRec);
                }
            }
        }
        //Insert apttusConfPrdNewRecList;

        List<Database.SaveResult> srsapttusConfProdNewList= Database.insert(apttusConfPrdNewRecList, FALSE);
        for (Database.SaveResult sr : srsapttusConfProdNewList)
        {
            if (sr.isSuccess())
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Apttus Configuration Prod Records: ' + sr.getId());
            }
            else
            {
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors())
                {
                    System.debug('The following error has occurred.');
                    System.debug('Fields that affected this error: ' + err.getFields());
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Deal', 'Error', 'Insert Product Configuration failed ' + err.getMessage(), null, null));
                }
            }
        }
        system.debug('apttusConfPrdNewRecList*********'+apttusConfPrdNewRecList);

        if (!errorLogs.isEmpty()) {
            insert errorLogs;
        }

    }
    global void finish(Database.BatchableContext BC)
    {
    }
}