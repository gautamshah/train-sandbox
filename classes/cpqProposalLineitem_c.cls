/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD  A    PAB        CPR-000    Updated comments section
                          AV-000
               PAB                   Created.
20170124       IL                    Added comment header.

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

public class cpqProposalLineitem_c {
	public static String FT10 = 'GENERATOR FT10';

	public static List<Apttus_Proposal__Proposal_Line_Item__c> fetchOptions(Id proposalId) {
		return [Select Id,
						Apttus_QPConfig__Quantity2__c,
						Apttus_QPConfig__ChargeType__c,
						Apttus_QPConfig__OptionId__r.Name,
						Apttus_QPConfig__LineType__c
		        From Apttus_Proposal__Proposal_Line_Item__c
		        Where Apttus_Proposal__Proposal__c = :proposalId
		        And Apttus_QPConfig__LineType__c = 'Option'];
	}

    public static Map<Id,Apttus_Proposal__Proposal_Line_Item__c> fetchOptionsMap(Id proposalId) {
        return new Map<Id,Apttus_Proposal__Proposal_Line_Item__c>(cpqProposalLineitem_c.fetchOptions(proposalId));
    }

	public static Boolean hasTradeIns(Apttus_Proposal__Proposal_Line_Item__c pli) {
		if (pli.Apttus_QPConfig__OptionId__r.Name == FT10) {
			return true;
		} else {
			return false;
		}
	}
}