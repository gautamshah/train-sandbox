@isTest
public class UserTerritory_gTest
{
	@isTest
	static void constructor()
	{
		UserTerritory_g cache = new UserTerritory_g();
		system.assertNotEquals(null, cache);
		
	}

	@isTest
	static void castMap()
	{
		Map<Id, sObject> input;
		
		Map<Id, UserTerritory> result = UserTerritory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		input = new Map<Id, sObject>();
		result = UserTerritory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		UserTerritory ut = [SELECT Id FROM UserTerritory LIMIT 1];
		
		input.put(ut.Id, null);
		result = UserTerritory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		input.put(ut.Id, ut);
		result = UserTerritory_g.cast(input);
		system.assert(result != null &&
		              !result.isEmpty() &&
		              result.containsKey(ut.Id) &&
		              result.get(ut.Id) == ut);
	}
	
	@isTest
	static void castFieldMap()
	{
		Map<object, Map<Id, sObject>> input;
		Map<object, Map<Id, UserTerritory>> result = UserTerritory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		input = new Map<object, Map<Id, sObject>>();
		result = UserTerritory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		UserTerritory ut = [SELECT Id, UserId FROM UserTerritory LIMIT 1];
		
		object key = ut.UserId;
		input.put(key, new Map<Id, sObject>());
		result = UserTerritory_g.cast(input);
		system.assert(result != null && !result.isEmpty());
		
		input.get(key).put(ut.Id, null);
		result = UserTerritory_g.cast(input);
		system.assert(result != null && !result.isEmpty());

		input.get(key).put(ut.Id, ut);
		result = UserTerritory_g.cast(input);
		system.assert(result != null &&
		              !result.isEmpty() &&
		              !result.get(key).isEmpty() &&
		              result.get(key).containsKey(ut.Id) &&
		              result.get(key).get(ut.Id) == ut);
	}
	
    @isTest
    static void fetch()
    {
		List<UserTerritory> uts = [SELECT Id, UserId FROM UserTerritory LIMIT 10];
		set<object> keys = new set<object>();
		for(UserTerritory ut : uts)
			keys.add(ut.UserId);
  		
  		UserTerritory_g cache = new UserTerritory_g();
  		
  		Test.startTest();
  		
  		Map<object, Map<Id, UserTerritory>> found = cache.fetch(UserTerritory.field.UserId, keys);
  		
  		Test.stopTest();

  		system.assert(found != null && found.isEmpty());
    }
}