/**
 * This is a test class for class 'uploadSaleout_new1'
 * to test the upload functionality for object 'Sales Out' and 'Channel Inventory'
 * using Tab Delimted , xlsx and xls files
 */
@isTest(seeAllData=true)
private class TestSalesOutUpload_New 
{
  static testMethod void Channel_Inventory_test() 
    {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest2@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@mdtmitg.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
            Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            //Product_SKU__c;
            insert ps;
            Partner_Product_SKU__c pp = new Partner_Product_SKU__c(Partner_Product_Code__c='1111',COV_Product__c=ps.id,Partner__c=acc.Id);
            insert pp;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp1.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='-23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='-23');
            insert so1;
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            
            //Create portalusercontact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = acc.Id,
                RecordtypeID=rtc.id,
                Email = System.now().millisecond() + 'mdttest2@mdttest.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where Name='Asia Distributor - KR' Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'test123456@test.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                Asia_Team_Asia_use_only__c = 'PRM Team',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            ContactShare cs=new ContactShare(ContactId=contact1.Id,UserOrGroupId=user1.Id,ContactAccessLevel='Read');
            Database.insert(cs);
            AccountShare ash=new AccountShare(AccountId=acc.Id,UserOrGroupId=user1.Id,AccountAccessLevel='Read',OpportunityAccessLevel='Read',ContactAccessLevel='Read',CaseAccessLevel='Read');
            
            Database.insert(ash);
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='Inventory_xls_file'];
           

                 
           
            //Sale_Out_xlsx_file , Sale_Out_xls_file
            Blob b=resourceList[0].Body;
            System.runAs ( user1 ) 
            {
                User utest=[select contactID,Contact.AccountId,Contact.Lastname from User where Id=:Userinfo.getUserId() Limit 1];
                Contact con=[select Id,Lastname,AccountId from Contact where Id=:utest.contactID];
                                
                System.assert(user1.Username==Userinfo.getUserName());
                Test.startTest();
                PageReference pageRef= Page.uploadSaleout_new1;
                Test.setCurrentPage(pageRef);
                uploadSaleout_new1 controller = new uploadSaleout_new1(new ApexPages.StandardController(cp));
                controller.attachment.body = resourceList[0].Body;
                controller.attachment.Name = 'Inventory_xls_file.xls';
                controller.fileType ='xls';
                controller.selectObjType='Channel Inventory';
                controller.nameFile = 'Sale_Out_xls.xls';
                Pagereference pg = controller.Upload_Attachment();
                cp.Partner_ID_Used__c = 'Account,Product SKU';
                update cp;
                pg=controller.Upload_Attachment();
                //Pagereference pg1 = controller.ReadFile();
               // controller.selectObjType='Channel Inventory';
               // pg=controller.Upload_Attachment();
                
                /*
                controller.attachment.body = resStatXls.Body;
                controller.attachment.Name = 'Sale_Out_xls.xls'; 
                //controller.selectObjType = '';
                controller.selectObjType='Sales Out';
                pg = controller.Upload_Attachment(); */                           
                
                //pg=controller.Upload_Attachment();
                //System.debug('file text = '+controller.fileText);                
                
                /*StaticResource resStatXlsx = [Select Name,Body from StaticResource where Name='Sale_Out_xlsx_file'];
                controller.attachment.body = resStatXlsx.Body;
                controller.attachment.Name = 'Sale_Out_xlsx.xlsx'; 
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                            
                controller.selectObjType='Sales Out';
                pg=controller.Upload_Attachment(); 
                
                StaticResource resStatInventXlsx = [Select Name,Body from StaticResource where Name='Inventory_xls_file'];
                controller.attachment.body = resStatInventXlsx.Body;
                controller.attachment.Name = 'Inventory_xls_file.xls';
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                               
                controller.selectObjType='Channel Inventory';
                pg=controller.Upload_Attachment(); */
                
                List<SelectOption> soptions=controller.getObjList();
                so.UOM__c = 'EA';
                update so;
                delete so;
                
                Test.stopTest();
            }
        }
    }
   static testMethod void Sales_Out_Test() 
    {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest3@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
            Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            insert ps;
            Partner_Product_SKU__c pp = new Partner_Product_SKU__c(Partner_Product_Code__c='1111',COV_Product__c=ps.id,Partner__c=acc.Id);
            insert pp;
            //Product_SKU__c;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp1.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='-23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='-23');
            insert so1;
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            
            //Create portalusercontact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = acc.Id,
                RecordtypeID=rtc.id,
                Email = System.now().millisecond() + 'mdttest4@mdttest.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where Name='Asia Distributor - KR' Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'mdttest123456@mdttest.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                Asia_Team_Asia_use_only__c = 'PRM Team',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            ContactShare cs=new ContactShare(ContactId=contact1.Id,UserOrGroupId=user1.Id,ContactAccessLevel='Read');
            Database.insert(cs);
            AccountShare ash=new AccountShare(AccountId=acc.Id,UserOrGroupId=user1.Id,AccountAccessLevel='Read',OpportunityAccessLevel='Read',ContactAccessLevel='Read',CaseAccessLevel='Read');
            
            Database.insert(ash);
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='Sale_Out_xls_file'];
            //Sale_Out_xlsx_file , Sale_Out_xls_file
            Blob b=resourceList[0].Body;
            System.runAs ( user1 ) 
            {
                User utest=[select contactID,Contact.AccountId,Contact.Lastname from User where Id=:Userinfo.getUserId() Limit 1];
                Contact con=[select Id,Lastname,AccountId from Contact where Id=:utest.contactID];
                                
                System.assert(user1.Username==Userinfo.getUserName());
                Test.startTest();
                PageReference pageRef= Page.uploadSaleout_new1;
                Test.setCurrentPage(pageRef);
                uploadSaleout_new1 controller = new uploadSaleout_new1(new ApexPages.StandardController(cp));
                controller.selectObjType=null;
                Pagereference pg = controller.Upload_Attachment();
                controller.attachment.body = resourceList[0].Body;
                controller.attachment.Name = 'Sale_Out_xls.xls';
                controller.fileType ='xls';
                controller.selectObjType='Sales Out';
                controller.nameFile = 'Sale_Out_xls.xls';
                pg = controller.Upload_Attachment();
                cp.Partner_ID_Used__c = 'Account,Product SKU';
                update cp;
                pg=controller.Upload_Attachment();
                //pg = controller.ReadFile();  
                //pg = controller.InsertSalesout();              
               // controller.selectObjType='Channel Inventory';
               // pg=controller.Upload_Attachment();
                
                /*
                controller.attachment.body = resStatXls.Body;
                controller.attachment.Name = 'Sale_Out_xls.xls'; 
                //controller.selectObjType = '';
                controller.selectObjType='Sales Out';
                pg = controller.Upload_Attachment(); */                           
                
                //pg=controller.Upload_Attachment();
                //System.debug('file text = '+controller.fileText);                
                
                /*StaticResource resStatXlsx = [Select Name,Body from StaticResource where Name='Sale_Out_xlsx_file'];
                controller.attachment.body = resStatXlsx.Body;
                controller.attachment.Name = 'Sale_Out_xlsx.xlsx'; 
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                            
                controller.selectObjType='Sales Out';
                pg=controller.Upload_Attachment(); 
                
                StaticResource resStatInventXlsx = [Select Name,Body from StaticResource where Name='Inventory_xls_file'];
                controller.attachment.body = resStatInventXlsx.Body;
                controller.attachment.Name = 'Inventory_xls_file.xls';
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                               
                controller.selectObjType='Channel Inventory';
                pg=controller.Upload_Attachment(); */
                
                List<SelectOption> soptions=controller.getObjList();
                so.UOM__c = 'EA';
                update so;
                delete so;
                
                Test.stopTest();
            }
        }
    }
     static testMethod void Tab_Delimted_ChannelInventory_Test() 
    {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest5@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
            Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            //Product_SKU__c;
            insert ps;
            Partner_Product_SKU__c pp = new Partner_Product_SKU__c(Partner_Product_Code__c='1111',COV_Product__c=ps.id,Partner__c=acc.Id);
            insert pp;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp1.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='-23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='-23');
            insert so1;
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            
            //Create portalusercontact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = acc.Id,
                RecordtypeID=rtc.id,
                Email = System.now().millisecond() + 'mdttest6@mdttest.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where Name='Asia Distributor - KR' Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'test123456@mdttest.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                Asia_Team_Asia_use_only__c = 'PRM Team',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            ContactShare cs=new ContactShare(ContactId=contact1.Id,UserOrGroupId=user1.Id,ContactAccessLevel='Read');
            Database.insert(cs);
            AccountShare ash=new AccountShare(AccountId=acc.Id,UserOrGroupId=user1.Id,AccountAccessLevel='Read',OpportunityAccessLevel='Read',ContactAccessLevel='Read',CaseAccessLevel='Read');
            
            Database.insert(ash);
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='Tab_Delimted_Sales_Out_File'];
            //Sale_Out_xlsx_file , Sale_Out_xls_file
            Blob b=resourceList[0].Body;
            System.runAs ( user1 ) 
            {
                User utest=[select contactID,Contact.AccountId,Contact.Lastname from User where Id=:Userinfo.getUserId() Limit 1];
                Contact con=[select Id,Lastname,AccountId from Contact where Id=:utest.contactID];
                                
                System.assert(user1.Username==Userinfo.getUserName());
                Test.startTest();
                PageReference pageRef= Page.uploadSaleout_new1;
                Test.setCurrentPage(pageRef);
                uploadSaleout_new1 controller = new uploadSaleout_new1(new ApexPages.StandardController(cp));
                controller.attachment.body = resourceList[0].Body;
                controller.attachment.Name = 'Tab_Delimted_Sales_Out_File.txt';
                controller.fileType ='txt';
                controller.selectObjType='Channel Inventory';
                controller.nameFile ='Tab_Delimted_Sales_Out_File.txt';
                cp.Partner_ID_Used__c = 'Account,Product SKU';
                update cp;
                Pagereference pg = controller.Upload_Attachment();
                pg=controller.InsertSalesout();
                //Pagereference pg1 = controller.ReadFile();                
               // controller.selectObjType='Channel Inventory';
               // pg=controller.Upload_Attachment();
                
                /*StaticResource resStatXls = [Select Name,Body from StaticResource where Name='Sale_Out_xls_file'];
                controller.attachment.body = resStatXls.Body;
                controller.attachment.Name = 'Sale_Out_xls.xls'; 
                //controller.selectObjType = '';
                controller.selectObjType='Sales Out';
                pg = controller.Upload_Attachment(); */                           
                
                //pg=controller.Upload_Attachment();
                //System.debug('file text = '+controller.fileText);                
                
                /*StaticResource resStatXlsx = [Select Name,Body from StaticResource where Name='Sale_Out_xlsx_file'];
                controller.attachment.body = resStatXlsx.Body;
                controller.attachment.Name = 'Sale_Out_xlsx.xlsx'; 
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                            
                controller.selectObjType='Sales Out';
                pg=controller.Upload_Attachment(); 
                
                StaticResource resStatInventXlsx = [Select Name,Body from StaticResource where Name='Inventory_xls_file'];
                controller.attachment.body = resStatInventXlsx.Body;
                controller.attachment.Name = 'Inventory_xls_file.xls';
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                               
                controller.selectObjType='Channel Inventory';
                pg=controller.Upload_Attachment(); */
                
                List<SelectOption> soptions=controller.getObjList();
                so.UOM__c = 'EA';
                update so;
                delete so;
                
                Test.stopTest();
            }
        }
    }
    static testMethod void Tab_Delimted_SaleOut_Test() 
    {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest7@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            Asia_Team_Asia_use_only__c = 'PRM Team',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
            Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            //Product_SKU__c;
            insert ps;
            Partner_Product_SKU__c pp = new Partner_Product_SKU__c(Partner_Product_Code__c='1111',COV_Product__c=ps.id,Partner__c=acc.Id);
            insert pp;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp1.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='-23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='-23');
            insert so1;
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            
            //Create portalusercontact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = acc.Id,
                RecordtypeID=rtc.id,
                Email = System.now().millisecond() + 'mdttest8@mdttest.com'
            );
            Database.insert(contact1);
            
            Profile portalProfile = [SELECT Id FROM Profile where Name='Asia Distributor - KR' Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'test123458@mdttest.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                Asia_Team_Asia_use_only__c = 'PRM Team',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            ContactShare cs=new ContactShare(ContactId=contact1.Id,UserOrGroupId=user1.Id,ContactAccessLevel='Read');
            Database.insert(cs);
            AccountShare ash=new AccountShare(AccountId=acc.Id,UserOrGroupId=user1.Id,AccountAccessLevel='Read',OpportunityAccessLevel='Read',ContactAccessLevel='Read',CaseAccessLevel='Read');
            
            Database.insert(ash);
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='Sales_Out_File_Tab_Delimted'];
            //Sale_Out_xlsx_file , Sale_Out_xls_file
            Blob b=resourceList[0].Body;            
            System.runAs ( user1 ) 
            {
                User utest=[select contactID,Contact.AccountId,Contact.Lastname from User where Id=:Userinfo.getUserId() Limit 1];
                Contact con=[select Id,Lastname,AccountId from Contact where Id=:utest.contactID];
                                
                System.assert(user1.Username==Userinfo.getUserName());
                Test.startTest();
                PageReference pageRef= Page.uploadSaleout_new1;
                Test.setCurrentPage(pageRef);
                uploadSaleout_new1 controller = new uploadSaleout_new1(new ApexPages.StandardController(cp));
                System.debug('here i am = '+resourceList[0].Body);
                controller.attachment.body = resourceList[0].Body;
                controller.fileText = resourceList[0].Body.tostring();
                controller.attachment.Name = 'Sales_Out_File_Tab_Delimted.csv';
                controller.fileType ='txt';
                controller.selectObjType='Sales Out';
                controller.nameFile ='Tab_Delimted_Sales_Out_File.txt';
                cp.Partner_ID_Used__c = 'Account,Product SKU';
                update cp;
                Pagereference pg = controller.Upload_Attachment();
                pg=controller.InsertSalesout();
                //Pagereference pg1 = controller.ReadFile();                
               // controller.selectObjType='Channel Inventory';
               // pg=controller.Upload_Attachment();
                
                /*StaticResource resStatXls = [Select Name,Body from StaticResource where Name='Sale_Out_xls_file'];
                controller.attachment.body = resStatXls.Body;
                controller.attachment.Name = 'Sale_Out_xls.xls'; 
                //controller.selectObjType = '';
                controller.selectObjType='Sales Out';
                pg = controller.Upload_Attachment(); */                           
                
                //pg=controller.Upload_Attachment();
                //System.debug('file text = '+controller.fileText);                
                
                /*StaticResource resStatXlsx = [Select Name,Body from StaticResource where Name='Sale_Out_xlsx_file'];
                controller.attachment.body = resStatXlsx.Body;
                controller.attachment.Name = 'Sale_Out_xlsx.xlsx'; 
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                            
                controller.selectObjType='Sales Out';
                pg=controller.Upload_Attachment(); 
                
                StaticResource resStatInventXlsx = [Select Name,Body from StaticResource where Name='Inventory_xls_file'];
                controller.attachment.body = resStatInventXlsx.Body;
                controller.attachment.Name = 'Inventory_xls_file.xls';
                controller.selectObjType = '';
                pg = controller.Upload_Attachment();                               
                controller.selectObjType='Channel Inventory';
                pg=controller.Upload_Attachment(); */
                
                List<SelectOption> soptions=controller.getObjList();
                so.UOM__c = 'EA';
                update so;
                delete so;
                
                Test.stopTest();
            }
        }
    }
}