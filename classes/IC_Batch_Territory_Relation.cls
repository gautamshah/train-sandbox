/**
Batch Job facilitates IC Territory ETL
	Build Hierarchical Territory relationships based on external Ids.
	Territory Reparenting runs faster if one at time - batch size should set to 1.
	
	Territory Reparenting can be very slow. The batch is scheduled every hour. Scheculer class: IC_Batch_Territory_Relation_Schedule
CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-05-13      Yuli Fintescu		Created
2015-08-12		Bill Shan			Not execute batch jobs while testing in the finish method
===============================================================================

IC_Batch_Territory_Relation p = new IC_Batch_Territory_Relation();
Database.executeBatch(p, 1);
*/
global class IC_Batch_Territory_Relation implements Database.Batchable<Sobject>{
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'Select flagToDelete__c, ' + 
							'flagForProcessing__c, ' + 
							'ParentTerritoryId, ' + 
							'Id, Name, ' + 
							'ExternalParentTerritoryID__c, ' + 
							'Custom_External_TerritoryID__c, ' + 
							'Source__c ' + 
						'From Territory ' + 
						'Where flagForProcessing__c = TRUE and ' + 
							'flagToDelete__c = FALSE and ' + 
							'Source__c in (' + IC_Execute_Territory_Deletion.includedSourcesInList() + ') ' + 
						'Order By ParentTerritoryId NULLS FIRST, LastModifiedDate desc';
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<Territory> scope) {
		List<IC_ETL_Process_Log__c> errors = new List<IC_ETL_Process_Log__c>();
		
		// Add the parent territory name to this set
        Set<String> ParentTerritoryNames = new Set<String> ();
        Map<String, String> DeleteTerritoryIDs = new Map<String, String> ();
		for(Territory terr : scope) {
			if (terr.ExternalParentTerritoryID__c != null && terr.ExternalParentTerritoryID__c != 'NOPARENT') {
				ParentTerritoryNames.add(terr.ExternalParentTerritoryID__c);
			}
		}
		
		Map<String, Id> TerritoryNameIds = new Map<String,Id>();
		//parent territory external ids to territory sf id
		for(Territory t : [select Id, Custom_External_TerritoryID__c from Territory where Custom_External_TerritoryID__c IN :ParentTerritoryNames]){
			TerritoryNameIds.put(t.Custom_External_TerritoryID__c, t.Id);
		}
		
		List<Territory> updateTerritory = new List<Territory>();
		for(Territory terr : scope) {
			terr.flagForProcessing__c = false;
			
			//associate this territory to its parent territory
			if (terr.ExternalParentTerritoryID__c != null && terr.ExternalParentTerritoryID__c != 'NOPARENT') {
				terr.ParentTerritoryId = TerritoryNameIds.get(terr.ExternalParentTerritoryID__c);
			}
			
			updateTerritory.add(terr);
		}
		System.Debug('*** updateTerritory ' + updateTerritory);
		
		if (updateTerritory.size() > 0) {
			Database.SaveResult[] results = database.update(updateTerritory, false);
			
        	for (Integer i = 0; i < results.size(); i ++) {
        		Database.SaveResult result = results[i];
        		Territory errored = updateTerritory[i];
        		
            	if (!result.isSuccess()) {
                	for(Database.Error err : result.getErrors())
                		System.Debug('*** result: IC_Batch_Territory_Relation.Update, ' + err.getStatusCode() + ' - ' + err.getMessage() + ', Territory: ' + errored.Custom_External_TerritoryID__c + ', ID: ' + errored.Id);
            	}
        	}
		}
		
/*=========================================================
	Error logs
===========================================================*/
		if (errors.size() > 0)
			database.insert (errors, false);
	}
	
	global void finish(Database.BatchableContext BC) {
		//deletion need to happen after reparenting
		if(!Test.isRunningTest())
			IC_Execute_Territory_Deletion.execute();
	}
}