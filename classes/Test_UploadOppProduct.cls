/****************************************************************************************
 * Name    : Test_UploadOppProduct 
 * Author  : Bill Shan
 * Date    : 15/05/2014 
 * Purpose : Test class for UploadOppProduct
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
@isTest (seeAllData=true)
private class Test_UploadOppProduct {

    static testMethod void myUnitTest() {
        Profile profile1 = [Select Id from Profile where name = 'EM - All'];
        User emUser = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest2@mdttest.com',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bill',
            Lastname='Shan',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF');
        insert emUser;
        
        Account acc=new Account(Name='Test EM Accuont',Status__c='Active',OwnerId = emUser.Id);
        insert acc;
        Product2 prd = new Product2(Name = 'testproduct', CurrencyIsoCode = 'CHF',ProductCode = 'TestProduct',isActive=true);
        insert prd;     
        Pricebook2 testbook = new Pricebook2(Name = 'testbook', IsActive = true);
        insert testbook;
        Pricebook2 teststd = [SELECT Id, IsActive FROM Pricebook2 WHERE IsStandard = true AND IsActive = true limit 1];
        PricebookEntry stdentry = new PricebookEntry(Pricebook2Id = teststd.Id, Product2Id = prd.Id,  
                                                        UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF');
        insert stdentry;
        
        PricebookEntry testentry = new PricebookEntry(Pricebook2Id = testbook.Id, Product2Id = prd.Id, UseStandardPrice = true, 
                                                        UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF');
        insert testentry;
        Opportunity testopp = new Opportunity(AccountId = acc.Id,Name = 'Test Opp', CurrencyISOcode = 'CHF', Pricebook2Id = testbook.Id,
                                            OwnerId=emUser.Id,Stagename = 'Develop', CloseDate = Date.newInstance(2014, 05, 15));
        insert testopp;
        List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='OppProducts'];
        
        Blob b=resourceList[0].Body;
        System.runAs(emUser) 
        {
            Test.startTest();
            PageReference pageRef= Page.UploadOppProducts;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('oppId', testopp.Id);
            UploadOppProduct controller = new UploadOppProduct(new ApexPages.StandardController(testopp));
            Pagereference pg=controller.ReadFile();
            controller.contentFile=resourceList[0].Body;
            pg=controller.ReadFile();
            boolean b1 = controller.gethasInvalidRecords();
            PageReference p = controller.goBacktoOpp();
            Test.stopTest();
        }
    }
}