public class FileUploader 
{
	public string nameFile{get;set;}
	public Blob contentFile{get;set;}
	public Id salesUploadId{get;set;}
	public String userChoice {get;set;}
	public String selectObjType{get;set;}
	public String cyclePeriodId{get;set;}
	public String ErrorHappened='F';
	public String ErrorAlert{get;set;}
	
	Public string nameFile1;
	String[] filelines = new String[]{};
	List<Sales_Out__c> salesoutupload;
	
	private final Cycle_Period__c sfcp;
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public FileUploader(ApexPages.StandardController stdController) {
    	
    	this.sfcp=[Select Id,Distributor_Name__c,Channel_Inventory_Validated__c,Sales_Out_Validated__c,
    					Status__c,No_Sales_This_Month__c,Cycle_Locked__c, Cycle_Period_Reference__c  
    				From Cycle_Period__c where Id=:stdController.getId()];
    				
        selectObjType=ApexPages.currentPage().getParameters().get('objName');
        
        cyclePeriodId = sfcp.Id;
        
        userChoice='Append';
    }
    
	public Pagereference ReadFile()
    {
		//declare a array of Upload records
		// get The Distributor Id
			
		if(selectObjType==''||selectObjType==null)
		{
			ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_select_Salesout_or_Channel_Inventory_for_Upload);
       		ApexPages.addMessage(errormsg);
   		}
   		else if(contentFile==null)
   		{
			ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_choose_a_valid_file);
       		ApexPages.addMessage(errormsg);
   		}
   		else if(((selectObjType=='Sales Out')&&sfcp.no_sales_this_month__c!=false))
   		{
       		sfcp.No_Sales_This_Month__c = False;
       		update sfcp;
   		}
   		else if(sfcp.cycle_locked__C != False)
   		{
       		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, label.Cycle_Period_is_Locked);
       		ApexPages.addMessage(errormsg);
   		}
   		else
		{
			try{
				nameFile1=contentFile.toString();
			}
			catch (Exception e){
				ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later);
				ApexPages.addMessage(errormsg);
				return null;
            }
            // Break the content by CRLF
            Integer pivot = nameFile1.indexOf( '\n', Integer.valueOf(Math.floor(nameFile1.length() / 2)) );
            String left = nameFile1.substring(0,pivot);
    		String right = nameFile1.substring(pivot);
            
            filelines = left.split('\n');
            filelines.addAll(right.split('\n'));
            
            //filelines = nameFile1.split('\n');
            
            if(filelines.size()<2||(selectObjType=='Sales Out'&&filelines[0].split('\t').size()<11)||(selectObjType=='Channel Inventory'&&filelines[0].split('\t').size()<6))
            {
            	ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_check_data_in_file_for_correct_format);
                ApexPages.addMessage(errormsg);
            }
            else
            {
            	SalesOut_uploads__c upl = new SalesOut_uploads__c();
            	upl.date_uploaded__c =Date.today(); //myDate;
            	upl.Upload_file_name__c = nameFile;
            	upl.upload_status__C = 'Uploaded';
            	upl.account__c = sfcp.Distributor_Name__c;
            	upl.cycle_period__c = sfcp.Id;
            	upl.Upload_Object__c=selectObjType;
            	upl.Total_Number_of_Records__c = filelines.size();
            	
            	insert upl;
            	
            	salesUploadId=upl.id;
            	
            	if(selectObjType=='Sales Out')
            	{
            		processSaleOutUpload(upl);
            		
            		if(UserInfo.getUserType()=='Standard')
            			return new PageReference('/apex/ValidateSalesOut?Id='+cyclePeriodId+'&Validate=Y');
            		else
            			return new PageReference(Label.distributorapex+'ValidateSalesOut?Id='+cyclePeriodId+'&Validate=Y');
            	}
        		if(selectObjType=='Channel Inventory')
        			processChannelInventoryUpload(upl);
			}
        }
        return null;
    }
    
    public void processSaleOutUpload(SalesOut_uploads__c upl)
    {
    	salesoutupload = new List<Sales_Out__c>();
    	for (Integer i=0;i<filelines.size();i++)
        {
        	if(i==0)
                continue;
            String[] inputvalues = new String[]{};
           
            //split the Record by fields based on the delimiter
            inputvalues = filelines[i].split('\t');
            if(inputvalues.size()<11)
            {
                ErrorHappened='T';
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.File_format_Error_Number_of_columns_are_less_than_expected);
                ApexPages.addMessage(errormsg);
                continue;
            }
            else
            {
                Sales_Out__c so = new Sales_Out__c();
                
                so.cycle_period__c = sfcp.Id;
                so.Document_No__c=inputvalues[0].trim();
                so.Document_Date_Text__c=inputvalues[1].trim();
                so.Sell_From__c=inputvalues[2].trim();
                so.Sell_To__c=inputvalues[3].trim();
                so.Product_Code__c=inputvalues[4].trim();
                so.UOM__c=inputvalues[5].trim();
                so.Quantity_Text__c = inputvalues[6].trim();
                so.Currency_Text__c=inputvalues[7].trim();
                so.Selling_Price_Text__c=inputvalues[8].trim();
                so.Lots_Serial_Number__c=inputvalues[9].trim();
                so.comments__c = inputvalues[10];
                so.salesout_uploads__c = upl.Id;
                
                salesoutupload.add(so);
            }
        }
        try 
        {
			ErrorAlert='F';
            if (salesoutupload.size()>0)
            {
                insert salesoutupload;
            }
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later);
            ApexPages.addMessage(errormsg);
            
        }
    }
    
    //Process Channel Inventory Upload
    public void processChannelInventoryUpload(SalesOut_uploads__c upl)
    {
        FileUploadErrors.setValidationHasRan();
         //declare a array of Objects to Load
       //List<Channel_Inventory_Staging__c> cistagingupload = new List<Channel_Inventory_Staging__c>();
       List<Channel_Inventory__c> ciupload = new List<Channel_Inventory__c>();
       
       //for everyline in the File 
       for (Integer i=0;i<filelines.size();i++)
        {
            if(i==0)
                continue;
            String[] inputvalues = new String[]{};
           
            
            //split the Record by fields based on the delimiter
            inputvalues = filelines[i].split('\t');
            system.debug (inputvalues[0]);
            if(inputvalues.size()<6)
            {
                ErrorHappened='T';
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.File_format_Error_Number_of_columns_are_less_than_expected);
                ApexPages.addMessage(errormsg);
                continue;
            }
            else
            {
                ErrorAlert='CF';
                Channel_Inventory__c so = new Channel_Inventory__c();
                so.cycle_period__c = sfcp.Id;
                
                so.Product_Code_Text__c=inputvalues[0].trim();
                so.Quantity_Text__c = inputvalues[1].trim();
                so.UOM__c=inputvalues[2].trim();
                so.Lots_Serial_Number__c=inputvalues[3].trim();
                so.Inventory_Status__c=inputvalues[4].trim();
                so.comments__c = inputvalues[5];
                
                so.Distributor_Name__c=upl.account__c;
                so.Upload_Statistics__c = upl.Id;
                ciupload.add(so);
            }
            
                    }
        try {
        // delete all rows before inserting new rows
           
        //delete [Select Id from account where Name like 'Covidien%' Limit 9000];
        //need to add something here to prevent duplication
        
        // Insert into the Account Object
        //insert into salesout object
        if (ciupload.size()>0)
            {    
                // need to get the Id of the insert row and pass it to the salesout record
                 //If there are no Errors then Load the sales out object
                insert ciupload;
            }
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later);
            
            ApexPages.addMessage(errormsg);
        } 
    }
    
    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Replace','Replace'));
            options.add(new SelectOption('Append','Append'));
            return options;
    }

	public List<SelectOption> getObjList() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('Sales Out','Sales Out'));
		options.add(new SelectOption('Channel Inventory','Channel Inventory'));
		return options;
	}
	
    public static void deleteSalesOut(Id DistributorName, Id CyclePeriod,String userChoice)
    {
        //List<Sales_Out_Staging__c> lstStagingToDelete=[Select Id from Sales_Out_Staging__c where Cycle_Period__c=:CyclePeriod and Distributor_Name__c=:DistributorName];
        List<Sales_Out__c> lstSalesOutToDelete=[Select Id from Sales_Out__c where Verification_Status__c not in ('Verified', 'Rejected') and Cycle_Period__c=:CyclePeriod];
        // and Distributor_Name_F__c=:DistributorName
      //  if(lstStagingToDelete!=null)
      //      delete lstStagingToDelete;
        if(lstSalesOutToDelete!=null&&userChoice!='Append')
            delete lstSalesOutToDelete; 
    }
 }