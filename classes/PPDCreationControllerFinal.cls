/****************************************************************************************
* Name    : PPDCreationControllerFinal
* Author  : Leena Khatri
* Date    : May 15, 2016
* Purpose : Controller for PPDCreation.vfp
* 
* Dependancies: 
*  	Custom Label: Inco_PPD_Portal_Help_Text
* ========================
* = MODIFICATION HISTORY =
* ========================
* 	DATE          		AUTHOR      		CHANGE
* 	----          		------          	------
* 	June 30, 2016		Gautam Shah			Added Custom Label: Inco_PPD_Portal_Help_Text to VF page
*****************************************************************************************/
public without sharing class PPDCreationControllerFinal 
{
    Public List<Incontinence_Submissions__c>  lstIncoPPD{get;set;}  
    Public List<Incontinence_Submissions__c>  lstAllIncoPPD{get;set;}  
    Public Date DateSelected {Get;Set;}
    Public Integer i {get;set;} 
    public String selectedWeek {get;set;}
    public Set<String> setAllWeek {get;set;}
    Public List<String>  lstIncoPPDTemp{get;set;}     
    Public Integer Count {get;set;} 
    public Map<Integer, String> mapWeeks{get;set;} 
    Public Integer intADC {get;set;}
    Public String MsgSuccess {get;set;}
    Public List<Inco_Product_PAR__c>  lstIncoProductPAR{get;set;}
    
    public PPDCreationControllerFinal ()
    {
        try
        {
            MsgSuccess='';         
            mapWeeks =new Map<Integer, String>();
            lstIncoPPD = new List<Incontinence_Submissions__c>(); 
            lstAllIncoPPD= new List<Incontinence_Submissions__c>(); 
            Count =0;
            i=0;
            
            setAllWeek=new Set<String>();
            
            User u = [Select Id, Contact.AccountId From User Where Id = :UserInfo.getUserId()];
            
            lstAllIncoPPD= [Select i.Submitted__c,i.Total_Residents_SKU__c, i.Num_Residents_SKU_Night__c, i.Num_Residents_SKU_Day__c, i.Inco_Product_PAR__r.Nighttime_PAR__c, i.Inco_Product_PAR__r.Daytime_PAR__c, i.Inco_Product_PAR__r.Product_Series__c, i.Inco_Product_PAR__r.Product_Family__c, i.Inco_Product_PAR__r.Name, i.Inco_Product_PAR__c, i.Inco_Item__r.Size__c, i.Inco_Item__r.Product_SKU__c, i.Inco_Item__c, i.Id, i.For_the_week_of__c, i.Current_Cases_on_Hand__c, i.Account__r.Id, i.Account__c, i.ADC__c, i.Submitted_Date__c 
                            From Incontinence_Submissions__c i
                            Where i.Account__c = :u.Contact.AccountId and Submitted__c=false and For_the_week_of__c!=null ORDER BY For_the_week_of__c ASC];  //ORDER BY For_the_week_of__c ASC
            
            
            for(Incontinence_Submissions__c incoPPDItem:lstAllIncoPPD)
            {
                if(incoPPDItem.Submitted__c==false && incoPPDItem.For_the_week_of__c.format()!=null)
                {
                    setAllWeek.add(string.valueof(incoPPDItem.For_the_week_of__c.format()));                
                }                                               
            }
            
            for(String weekIn:setAllWeek)
            {
                mapWeeks.put(i,weekIn);
                i++;
            }
            
            lstIncoPPDTemp = new List<String>(setAllWeek);           
            getItems();
            DisplayPPD();         
        }
        catch(Exception e)
        {
            System.debug(e.getMessage());
        }
    }
    
    public List<SelectOption> getItems() 
    {
        Integer WeekCount=0;
        String week='';
        String weeklst='';
        List<SelectOption> options = new List<SelectOption>();        
        
        if(setAllWeek!=null && setAllWeek.size()>0 && mapWeeks!=null && mapWeeks.size()>0)
        {                                       
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'You have pending submissions for the following weeks:'));
            for(Integer j=0;WeekCount<mapWeeks.size();j++)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,mapWeeks.get(WeekCount)));
                WeekCount++;
            }
            if(mapWeeks.get(Count)!=null)
            {
                week=mapWeeks.get(Count);
                options.add(new SelectOption(week,week)); 
            } 
            
        }  
        else
        {
            options.add(new SelectOption('No week Left','No week Left')); 
        }          
        
        return options;
    }

    public void DisplayPPD()
    {
    	String selectWeek='';
        intADC = null;
        Date DateSelected;
        lstIncoPPD = new List<Incontinence_Submissions__c>();            
        selectWeek = mapWeeks.get(Count);  
        
        if(selectWeek != '' && selectWeek != null)
        {
        	DateSelected = Date.parse(selectWeek);
        }
		
        if(lstAllIncoPPD != null && lstAllIncoPPD.size() > 0 && DateSelected != null)
        {
        	for(Incontinence_Submissions__c PPDItem : lstAllIncoPPD)
            {    
            
                if(DateSelected == PPDItem.For_the_week_of__c)           
                {
                    if(PPDItem.Submitted__c == false)
                    {
                    	lstIncoPPD.add(PPDItem);              
                    }
                    else
                    {
                        apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Data already submitted' + PPDItem.Id); 
                        ApexPages.addMessage(msg);                
                    }
                }
         	}
    	}                             
    }
    
  
    public Pagereference Save()
    {
        PageReference newPage = New PageReference('/apex/PPDCreation');
        List<Incontinence_Submissions__c> UpdatelstPPDCreation = new List<Incontinence_Submissions__c>();
        MsgSuccess = '';
        
        if(lstIncoPPD != null && lstIncoPPD.size() > 0)
        {
            try 
            {
                for(Incontinence_Submissions__c PPDItem : lstIncoPPD)
                {   
                    if(PPDItem.Total_Residents_SKU__c != null && PPDItem.Num_Residents_SKU_Day__c != null && PPDItem.Num_Residents_SKU_Night__c != null && PPDItem.Current_Cases_on_Hand__c != null )
                    {
                        PPDItem.Submitted__c = true;
                        PPDItem.Submitted_Date__c = Date.today();
                        PPDItem.ADC__c = intADC;
                        UpdatelstPPDCreation.add(PPDItem);                              
                    }
                    else
                    {
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'All Fields are required in all records'));  
                         return null;          
                    }
                }
            
                try
                {
                    if(UpdatelstPPDCreation!=null && UpdatelstPPDCreation.size()>0)
                    {
                        upsert UpdatelstPPDCreation; 
                        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info,'Record Created Successfully.Thank you!'));  
                        MsgSuccess = 'Record Created Successfully.Thank you!';       
                    }
                }
                catch(DMLException ex)
                {
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'Errors are ' + ex.getmessage())); 
                }
            
                if(setAllWeek.contains(selectedWeek))
                {
                     setAllWeek.remove(selectedWeek);          
                }
                
                Count++;
                lstIncoPPDTemp = new List<String>(setAllWeek);            
                getItems();
                DisplayPPD();          
                 
                if(selectedWeek == 'No weeks left')
                {
                    lstIncoPPD = null;
                }
                 
                newPage.setRedirect(true);        
                return newPage;
          
            } 
            catch (DMLException e) 
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new PPD.'));
                return null; 
            }                
        }
        return newPage;
    }
    
    public Pagereference Cancel()
    {
        PageReference newPage = New PageReference('/apex/PPDCreation');
        return newPage;
    }

}