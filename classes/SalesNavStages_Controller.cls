public with sharing class SalesNavStages_Controller
{
	public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Name FROM Sales_Navigator_Stage__c]));
            }
            return setCon; 
        }
        set;
    }
	
	// Initialize setCon and return a list of records
    public List<Sales_Navigator_Stage__c> getStages() {
        return (List<Sales_Navigator_Stage__c>) setCon.getRecords();
    }
}