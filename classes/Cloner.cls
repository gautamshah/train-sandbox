public class Cloner 
{
	public static void cloneNotes(String oldParentId, String newParentId)
    {
        if(String.isNotBlank(oldParentId) && String.isNotBlank(newParentId))
        {
            List<Note> lNotes;// = new List<Note>([Select Id, ParentId, Title, IsPrivate, Body, OwnerId From Note Where ParentId = :l.Id]);
            List<Note> clonedNotes = new List<Note>();
            Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Note.fields.getMap();
            String fields = '';
            for(String f : mapFields.keySet())
            {
                Schema.DescribeFieldResult dfr = mapFields.get(f).getDescribe();
                if(dfr.accessible && dfr.isCreateable())
                {
                    fields += f + ',';
                }
            }
            fields = fields.left(fields.length() - 1);
            System.debug('fields: ' + fields);
            String query = 'Select ' + fields + ' From Note Where ParentId = \'' + oldParentId + '\'';
            System.debug('query: ' + query);
            lNotes = Database.query(query);
            for(Note n : lNotes)
            {
                Note oNote = n.clone(false,true,false,false);
                oNote.ParentId = newParentId;
                clonedNotes.add(oNote);
            }
            if(!clonedNotes.isEmpty())
            {
                insert clonedNotes;
            }
        }
    }
    
    public static void cloneAttachments(String oldParentId, String newParentId)
    {
        List<Attachment> lAttachments;// = new List<Attachment>([Select Id, ParentId, Name, IsPrivate, ContentType, BodyLength, Body, OwnerId, Description From Attachment Where ParentId = :l.Id]);
        List<Attachment> clonedAttachments = new List<Attachment>();
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Attachment.fields.getMap();
        String fields = '';
        for(String f : mapFields.keySet())
        {
            Schema.DescribeFieldResult dfr = mapFields.get(f).getDescribe();
            if(dfr.accessible && dfr.isCreateable())
            {
                fields += f + ',';
            }
        }
        fields = fields.left(fields.length() - 1);
        System.debug('fields: ' + fields);
        String query = 'Select ' + fields + ' From Attachment Where ParentId = \'' + oldParentId + '\'';
        System.debug('query: ' + query);
        lAttachments = Database.query(query);
        for(Attachment at : lAttachments)
        {
            Attachment oAttach = at.clone(false,true,false,false);
            oAttach.ParentId = newParentId;
            clonedAttachments.add(oAttach);
        }
        if(!clonedAttachments.isEmpty())
        {
            insert clonedAttachments;
        }
    }
    
    public static void cloneTasks(String oldWhoId, String newWhoId, String oldWhatId, String newWhatId)
    {
        List<Task> lTasks;
        List<Task> clonedTasks = new List<Task>();
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Task.fields.getMap();
        String fields = '';
        for(String f : mapFields.keySet())
        {
            Schema.DescribeFieldResult dfr = mapFields.get(f).getDescribe();
            if(dfr.accessible && dfr.isCreateable())
            {
            	fields += f + ',';
            }
        }
        fields = fields.left(fields.length() - 1);
        System.debug('fields: ' + fields);
        if(String.isNotBlank(newWhoId) && oldWhoId != newWhoId)
        {
            String query = 'Select ' + fields + ' From Task Where WhoId = \'' + oldWhoId + '\'';
            System.debug('query: ' + query);
            lTasks = Database.query(query);
            for(Task t : lTasks)
            {
                Task newT = t.clone(false,true,false,false);
                newT.WhoId = newWhoId;
                clonedTasks.add(newT);
            }
        }
        else if(String.isNotBlank(newWhatId) && oldWhatId != newWhatId)
        {
            String query = 'Select ' + fields + ' From Task Where WhatId = \'' + oldWhatId + '\'';
            System.debug('query: ' + query);
            lTasks = Database.query(query);
            for(Task t : lTasks)
            {
                Task newT = t.clone(false,true,false,false);
                newT.WhatId = newWhatId;
                clonedTasks.add(newT);
            }
        }
        if(!clonedTasks.isEmpty())
        {
            insert clonedTasks;
        }
    }
    
    public static void cloneEvents(String oldWhoId, String newWhoId, String oldWhatId, String newWhatId)
    {
        List<Event> lEvents;
        List<Event> clonedEvents = new List<Event>();
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Event.fields.getMap();
        String fields = '';
        for(String f : mapFields.keySet())
        {
            Schema.DescribeFieldResult dfr = mapFields.get(f).getDescribe();
            if(dfr.accessible && dfr.isCreateable())
            {
            	fields += f + ',';
            }
        }
        fields = fields.left(fields.length() - 1);
        System.debug('fields: ' + fields);
        if(String.isNotBlank(newWhoId) && oldWhoId != newWhoId)
        {
            String query = 'Select ' + fields + ' From Event Where WhoId = \'' + oldWhoId + '\'';
            System.debug('query: ' + query);
            lEvents = Database.query(query);
            for(Event t : lEvents)
            {
                Event newT = t.clone(false,true,false,false);
                newT.WhoId = newWhoId;
                clonedEvents.add(newT);
            }
        }
        else if(String.isNotBlank(newWhatId) && oldWhatId != newWhatId)
        {
            String query = 'Select ' + fields + ' From Event Where WhatId = \'' + oldWhatId + '\'';
            System.debug('query: ' + query);
            lEvents = Database.query(query);
            for(Event t : lEvents)
            {
                Event newT = t.clone(false,true,false,false);
                newT.WhatId = newWhatId;
                clonedEvents.add(newT);
            }
        }
        if(!clonedEvents.isEmpty())
        {
            insert clonedEvents;
        }
    }
}