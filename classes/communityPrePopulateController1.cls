public with sharing class communityPrePopulateController1 {
    

List<Account> rqs; 
public Id accId;
public List<Community_User__c> theUserLst {get; set;}
public String userId = UserInfo.getUserId();
public Set<Id> ids = new Set<Id>();
List<Opportunity> oppty;
public Set<Id> opptyIds = new Set<Id>();
List<Community_Evaluation__c> cliEva;
Map<ID,Account> maps= new Map<ID,Account>();
Set<ID> oppAccIds = new Set<ID>();
public list<WrapperClinical> wrapperC{get;set;}

List<Account_Affiliation__c> acc1 = new List<Account_Affiliation__c>();

     
 public communityPrePopulateController1(ApexPages.StandardController controller) 
 {
 Id profileId=userinfo.getProfileId();
String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
system.debug('ProfileName===========>'+profileName);
     //User currentUser = [SELECT id, name FROM User WHERE Id = :userId ];
    if( (profileName =='AEM Community Administrator'  ||profileName =='US - AST' ||profileName == 'US - SUS' ||profileName =='System Administrator'))
  {
   theUserLst = [SELECT Hospital_Community_User__c,Hospital_Community__c,Id,Name,website__c FROM Community_User__c where SFDC_User__c=:userId and Hospital_Community_User__c=true];
 if(theUserLst.size()>0)
 {
 for(Community_User__c cu:theUserLst )
     {
       List<Account> record = new List<Account>();
       List<RecordType> recordname = new List<RecordType>();
       record= [select Id,RecordTypeId from Account where Id=:cu.Hospital_Community__c];
       String sobjnme = 'Account';
       String rectypename = 'US-Hospital Community';
       String recrdId;
       for(Account rcd:record){
          recrdId = rcd.Id;
       }
       recordname = [select Id,Name,SobjectType FROM RecordType where Id=:recrdId and SobjectType=:sobjnme and Name=:rectypename];
     if(recordname!=null){
         ids.add(cu.Hospital_Community__c);
      }
     }
 system.debug('ids Hospital comunity =====>'+ids);
 }
 }else
 {
 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'At this time Partnerships is reserved for US Surgical Sales Professionals only'));
 }
 
 }
 public string SelectedAccountId{get;set;}
 public pagereference evalButtonClick()
 { String cName1= ApexPages.currentPage().getParameters().get('cName');
 System.debug('communityName=========>: '+cName1);
 Schema.DescribeSObjectResult r = Community_Evaluation__c.sObjectType.getDescribe();
String keyPrefix = r.getKeyPrefix();
System.debug('PrintingKeyPrefix==========> --'+keyPrefix );
  
   //return new PageReference('/a2i/e?cancelURL=%2Fapex/communityNamePrePopulate&retURL=%2Fa2i%2Fo&&CF00NK0000001WyvF='+SelectedAccountId);
 return new PageReference('/'+keyPrefix+'/e?cancelURL=%2Fapex/communityNamePrePopulate&retURL=%2F'+keyPrefix+'%2Fo&&CF00NU000000539f9='+SelectedAccountId);
 }
 
 public List<Account> getReq()
 { 
 System.debug('set is------------------'+ids); 
 /// Get the data for the list
 rqs = [select id, Name,Website,Web1__c from Account  Where id IN :ids  order by name];
  return  rqs;
 }
 
 public List<WrapperClinical> getOpp() {
  wrapperC = new List<WrapperClinical>();
  cliEva= [select id,Name,Community_Name__c,Account_Name__r.Name,Account_Name__r.id,Evaluating_Clinician__c,Procedure_1__c,Procedure_2__c,Procedure_3__c,Other_Procedure__c,
   LastModifiedById,LastModifiedBy.Name,LastModifiedDate,createdById,Evaluating_Clinician__r.Name,Community_Name__r.name,CreatedDate from Community_Evaluation__c where Community_Name__c IN :ids and signed__c =false order by LastModifiedDate desc];
      
  for(Community_Evaluation__c ce :cliEva) {
   //System.debug('CliEna Community Name: is------------------>'+ce.Community_Name__c); 
   //system.debug('ids Hospital comunity in get opp =====>'+ids);
   
   acc1=[select id, name,Affiliated_with__r.name ,AffiliationMember__c from Account_Affiliation__c where AffiliationMember__c = :ce.Account_Name__r.id order by Affiliated_with__r.name];

    if(acc1.size()>0) {
      wrapperC.add(new WrapperClinical(ce,acc1[0].Affiliated_with__r.name));
    } else {
      wrapperC.add(new WrapperClinical(ce,''));
    }
  }
  
  return  wrapperC; 
 }
 
 public PageReference Cinical(){
       
        
        //PageReference reRend = new PageReference('/apex/customlookupOpp');
        PageReference reRend = new PageReference('/a0D/e?retURL=%2Fa0D%2Fo');
        return reRend;
    }
    
    public class WrapperClinical
    {
    public Community_Evaluation__c ce {get;set;}
    public String name {get;set;}
    
    
    public WrapperClinical (Community_Evaluation__c ce1, String name1)
    {
    this.ce=ce1;
    this.name=name1;
    }
    }
    
}