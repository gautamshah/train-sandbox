/****************************************************************************************
   * Name    : CreateBaseOpportunityBatch
   * Author  : Tatsuya Takesue
   * Date    : 1/12/2015
 *****************************************************************************************/

global class CreateBaseOpportunityBatch implements Database.Batchable<sObject>,Database.Stateful{
    public String query='select Id, CurrencyIsoCode ,Location_ID__c, Seller_Name_del__c, ' +
                        'JP_Last_FY_Actual_Amount_Capital__c,JP_Last_FY_Actual_Amount_Dispo__c,Sellout_Date__c,' +
                        'RecordTypeId, LastModifiedById, CreatedById, OwnerId,' +
                        'Actual_Amount_Capital__c, Actual_Amount_Dispo__c, Actual_Amount_Dispo_UP__c, Eval_Class__c,' +
                        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod start
                        // 'MONTH_Quota__c, Sales_Class__c ' +
                        'MONTH_Quota__c, Sales_Class__c, Name ' +
                        'from Seller_Quota__c where LastModifiedDate >= :lastDate' +
                        ' And Location_ID__c != Null' +
                        ' And CurrencyIsoCode = :currencyJPY';
                        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end

    // 2016.03.22 ECS)kawada CVJ_SELLOUT_DEV-100 mod start
    // private static Integer MAX_BACTH_SIZE = 50;
    private static Integer MAX_BACTH_SIZE = 40;
    // 2016.03.22 ECS)kawada CVJ_SELLOUT_DEV-100 mod end

    // バッチサイズ(スケジューラーで取得)
    public Integer batchSize {get; set;}
    // エラー通知メッセージ内容
    public String errMsg {get; set;}
    // カスタム例外フラグ
    public Boolean isCustomException {get; set;}
    // 例外情報
    public Integer lineNumber {get; set;}
    public String stackTrace {get; set;}
    public String typeName {get; set;}
    // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
    // 通貨文字列(日本)
    private static final String currencyJPY = 'JPY';
    // スキップ対象用変数
    public Boolean skipSellerFlg {get; set;}
    public set<String> skipSellerNameSet {get; set;}
    // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end
    // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add start
    // ベース案件用案件製品価格表定数・変数
    private static final String BASE_PRICEBOOKNAME = 'JP:CMN';
    public Id basePbeCapitalId {get; set;}
    public Id basePbeDispoId {get; set;}
    //2016.08.15 Kotani add
    public Id basePbeBaseId {get; set;}
    // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add end
    // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 add start
    public set<String> upsOppNameSet {get; set;}
    // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 add end

    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('Start CreateBaseOpportunityBatch：' + datetime.now());
        errMsg = '';
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
        skipSellerNameSet = new Set<String>();
        skipSellerFlg = false;
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end
        // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 add start
        upsOppNameSet = new Set<String>();
        // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 add end

        //　バッチサイズの判定
        if(batchSize > MAX_BACTH_SIZE){
            system.debug('Error!!! バッチサイズは ' + MAX_BACTH_SIZE +'より大きい値は設定できません。 バッチサイズ：' + batchSize);
            errMsg = 'Error!!! バッチサイズは ' + MAX_BACTH_SIZE +'より大きい値は設定できません。 バッチサイズ：' + batchSize;
            isCustomException = true;
            //throw new CreateBaseOpportunityBatchException('Error!!! バッチサイズは ' + MAX_BACTH_SIZE +'より大きい値は設定できません。 バッチサイズ：' + batchSize);
        }

        // バッチ最終実行日の取得
        Create_Base_Opportunity_Batch_LastDate__c lastDateSetting = Create_Base_Opportunity_Batch_LastDate__c.getOrgDefaults();
        Date lastDate = null;
        if(lastDateSetting != null){
            lastDate = lastDateSetting.Date__c;
        }

        if(lastDate == null){
            system.debug('Error!!! カスタム設定「Create_Base_Opportunity_Batch_LastDate__c」が未登録です。');
            errMsg = 'Error!!! カスタム設定「Create_Base_Opportunity_Batch_LastDate__c」が未登録です。';
            isCustomException = true;
            //throw new CreateBaseOpportunityBatchException('Error!!! カスタム設定「Create_Base_Opportunity_Batch_LastDate__c」が未登録です。');
        }

        // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add start
        // ベース案件用案件製品に紐づける価格表エントリの取得
        // (価格表エントリは既に作成されている前提)
        Create_Base_Opportunity_Batch_Pd2Capital__c basePd2CapitalNameSetting = Create_Base_Opportunity_Batch_Pd2Capital__c.getOrgDefaults();
        Create_Base_Opportunity_Batch_Pd2Dispo__c basePd2DispoNameSetting = Create_Base_Opportunity_Batch_Pd2Dispo__c.getOrgDefaults();
        //2016.08.15 Kotani add
        Create_Base_Opportunity_Batch_Pd2Base__c basePd2BaseNameSetting = Create_Base_Opportunity_Batch_Pd2Base__c.getOrgDefaults();
        List<Product2> tmpBasePd2List = new List<Product2>();
        Id basePd2Id = null;
        String basePd2Name = null;
        List<PricebookEntry> tmpBasePbeList = new List<PricebookEntry>();
        // カスタム設定不正値・製品非存在フラグ
        Boolean illegalValCapitalFlg = false;
        Boolean illegalValDispoFlg = false;
        //2016.08.15 Kotani add
        Boolean illegalValBaseFlg = false;
        // 価格表エントリ非存在フラグ
        Boolean nonePriceBookEntryCapitalFlg = false;
        Boolean nonePriceBookEntryDispoFlg = false;
        //2016.08.15 Kotani add
        Boolean nonePriceBookEntryBaseFlg = false;

        // 当年実績(H) 製品価格表エントリ取得
        basePd2Name = basePd2CapitalNameSetting.Product2Name__c;
        tmpBasePd2List = [select Id from Product2 where Name = :basePd2Name];
        if(tmpBasePd2List != null && tmpBasePd2List.size() == 1){
            basePd2Id = tmpBasePd2List[0].Id;
            tmpBasePbeList = [select Id,Pricebook2.Name
                                from PricebookEntry
                               where Product2Id = :basePd2Id
                                 and Pricebook2.Name = :BASE_PRICEBOOKNAME];
            if(tmpBasePbeList != null && tmpBasePbeList.size() > 0){
                basePbeCapitalId = tmpBasePbeList[0].Id;
            }
            else {
                nonePriceBookEntryCapitalFlg = true;
            }
        }
        else {
            illegalValCapitalFlg = true;
        }
        // 変数初期化
        tmpBasePd2List = new List<Product2>();
        basePd2Id = null;
        basePd2Name = null;
        tmpBasePbeList = new List<PricebookEntry>();
        // 当年実績(D) 製品価格表エントリ取得
        basePd2Name = basePd2DispoNameSetting.Product2Name__c;
        tmpBasePd2List = [select Id from Product2 where Name = :basePd2Name];
        if(tmpBasePd2List != null && tmpBasePd2List.size() == 1){
            basePd2Id = tmpBasePd2List[0].Id;
            tmpBasePbeList = [select Id,Pricebook2.Name
                                from PricebookEntry
                               where Product2Id = :basePd2Id
                                 and Pricebook2.Name = :BASE_PRICEBOOKNAME];
            if(tmpBasePbeList != null && tmpBasePbeList.size() > 0){
                basePbeDispoId = tmpBasePbeList[0].Id;
            }
            else {
                nonePriceBookEntryDispoFlg = true;
            }
        }
        else {
            illegalValDispoFlg = true;
        }
        //2016.08.15 Kotani add
        // 変数初期化
        tmpBasePd2List = new List<Product2>();
        basePd2Id = null;
        basePd2Name = null;
        tmpBasePbeList = new List<PricebookEntry>();
        // ベース金額 製品価格表エントリ取得
        basePd2Name = basePd2BaseNameSetting.Product2Name__c;
        tmpBasePd2List = [select Id from Product2 where Name = :basePd2Name];
        if(tmpBasePd2List != null && tmpBasePd2List.size() == 1){
            basePd2Id = tmpBasePd2List[0].Id;
            tmpBasePbeList = [select Id,Pricebook2.Name
                                from PricebookEntry
                               where Product2Id = :basePd2Id
                                 and Pricebook2.Name = :BASE_PRICEBOOKNAME];
            if(tmpBasePbeList != null && tmpBasePbeList.size() > 0){
                basePbeBaseId = tmpBasePbeList[0].Id;
            }
            else {
                nonePriceBookEntryBaseFlg = true;
            }
        }
        else {
            illegalValBaseFlg = true;
        }
        // 設定値が不正(設定値なし　or 存在しない設定値 or 2件以上HIT)
        if(illegalValCapitalFlg){
            system.debug('Error!!! カスタム設定「Create_Base_Opportunity_Batch_Pd2Capital__c」の値が不正です。');
            errMsg = 'Error!!! カスタム設定「Create_Base_Opportunity_Batch_Pd2Capital__c」の値が不正です。';
            isCustomException = true;
        }
        else if(illegalValDispoFlg){
            system.debug('Error!!! カスタム設定「Create_Base_Opportunity_Batch_Pd2Dispo__c」の値が不正です。');
            errMsg = 'Error!!! カスタム設定「Create_Base_Opportunity_Batch_Pd2Dispo__c」の値が不正です。';
            isCustomException = true;
        }
        //2016.08.15 Kotani add
        else if(illegalValBaseFlg){
            system.debug('Error!!! カスタム設定「Create_Base_Opportunity_Batch_Pd2Base__c」の値が不正です。');
            errMsg = 'Error!!! カスタム設定「Create_Base_Opportunity_Batch_Pd2Base__c」の値が不正です。';
            isCustomException = true;
        }
        // 価格表エントリが存在しない
        else if(nonePriceBookEntryCapitalFlg){
            system.debug('Error!!! 「JP:CMN」価格表のハード価格表エントリが存在しません。');
            errMsg = 'Error!!! 「JP:CMN」価格表のハード価格表エントリが存在しません。';
            isCustomException = true;
        }
        else if(nonePriceBookEntryDispoFlg){
            system.debug('Error!!! 「JP:CMN」価格表の消耗品価格表エントリが存在しません。');
            errMsg = 'Error!!! 「JP:CMN」価格表の消耗品価格表エントリが存在しません。';
            isCustomException = true;
        }
        //2016.08.15 Kotani add
        else if(nonePriceBookEntryBaseFlg){
            system.debug('Error!!! 「JP:CMN」価格表の消耗品価格表エントリが存在しません。');
            errMsg = 'Error!!! 「JP:CMN」価格表の消耗品価格表エントリが存在しません。';
            isCustomException = true;
        }
        // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add end

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        if(scope == null || !String.isBlank(errMsg)){
            throw new CreateBaseOpportunityBatchException(errMsg);
        }
        List<Seller_Quota__c> sellList = (List<Seller_Quota__c>) scope;
        Map<String, Seller_Quota__c> sellMap = new Map<String, Seller_Quota__c>();
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add start
        Map<String, Seller_Quota__c> sellTotalMap = new Map<String, Seller_Quota__c>();
        List<Seller_Quota__c> sellTotalList = new List<Seller_Quota__c>();
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add end
        Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
        List<Opportunity> updOppList = new List<Opportunity>();
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 del start
        //Set<Id> territoryIdList = new Set<Id>();
        //Map<String, Territory> territoryMap = new Map<String, Territory>();
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 del end
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
        List<Territory> territoryList = new List<Territory>();
        Map<String, Id> territoryExternalIdMap = new Map<String, Id>();
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end
        // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add start
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        Set<Id> updOppIdSet = new Set<Id>();
        List<OpportunityLineItem> delOppLineItemList = new List<OpportunityLineItem>();
        // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add end

        List<Account> accList  = [SELECT Name FROM Account where Account_External_ID__c = 'JP-Base'];
        if(accList == null || accList.size() == 0){
            system.debug('Error!!! ベース病院(Account_External_ID__c "JP-Base")が取得できませんでした。');
            errMsg = 'Error!!! ベース病院(Account_External_ID__c "JP-Base")が取得できませんでした。';
            isCustomException = true;
            throw new CreateBaseOpportunityBatchException('Error!!! ベース病院(Account_External_ID__c "JP-Base")が取得できませんでした。');
        }

        Account baseAcc = accList[0];

        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
        territoryList = [select Id,Custom_External_TerritoryID__c,CurrencyIsoCode
                              from Territory];
        if(territoryList.size() > 0){
            for(Territory t : territoryList){
                territoryExternalIdMap.put(t.Custom_External_TerritoryID__c,t.Id);
            }
        }
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end

        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add start
        for(Seller_Quota__c sell : sellList){
            String totalKey = sell.Location_Id__c + String.valueOf(sell.Sellout_Date__c) + sell.Eval_Class__c;
            if(sell.JP_Last_FY_Actual_Amount_Capital__c == null) sell.JP_Last_FY_Actual_Amount_Capital__c = 0;
            if(sell.JP_Last_FY_Actual_Amount_Dispo__c == null) sell.JP_Last_FY_Actual_Amount_Dispo__c = 0;
            if(sell.Actual_Amount_Capital__c == null) sell.Actual_Amount_Capital__c = 0;
            if(sell.Actual_Amount_Dispo__c == null) sell.Actual_Amount_Dispo__c = 0;
            if(sell.MONTH_Quota__c == null) sell.MONTH_Quota__c = 0;
            if(sellTotalMap.containsKey(totalKey)){
                Seller_Quota__c sellTotal = sellTotalMap.get(totalKey);
                sellTotal.JP_Last_FY_Actual_Amount_Capital__c += sell.JP_Last_FY_Actual_Amount_Capital__c;
                sellTotal.JP_Last_FY_Actual_Amount_Dispo__c += sell.JP_Last_FY_Actual_Amount_Dispo__c;
                sellTotal.Actual_Amount_Capital__c += sell.Actual_Amount_Capital__c;
                sellTotal.Actual_Amount_Dispo__c += sell.Actual_Amount_Dispo__c;
                //sellTotal.Actual_Amount_Dispo__c += sell.Actual_Amount_Dispo_UP__c;
                sellTotal.MONTH_Quota__c += sell.MONTH_Quota__c;
                sellTotalMap.put(totalKey, sellTotal);
            }else{
                Seller_Quota__c sellTotal = sell;
                //sellTotal.Actual_Amount_Dispo__c = sell.Actual_Amount_Dispo_UP__c;
                sellTotalMap.put(totalKey, sellTotal);
            }
        }
        sellTotalList = sellTotalMap.values();
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add end

        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 mod start
        //for(Seller_Quota__c sell : sellList){
        for(Seller_Quota__c sell : sellTotalList){
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 mod end
            String selloutMonth = String.valueOf(sell.Sellout_Date__c.month());
            // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 mod start
            //String key = baseAcc.Name + ': ' + sell.Sales_Class__c + '-' + sell.Seller_Name_del__c + '-' + selloutMonth + '-' + sell.Eval_Class__c;
            String key = baseAcc.Name + ': ' + sell.Location_Id__c + '-' + sell.Seller_Name_del__c + '-' + selloutMonth + '-' + sell.Eval_Class__c;
            // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 mod end
            sellMap.put(key, sell);
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
            if(!territoryExternalIdMap.containsKey(sell.Location_ID__c)){
                skipSellerFlg = true;
                skipSellerNameSet.add(sell.Name);
            }
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 del start
            /*
            try{
               Id checkId = sell.Location_ID__c;
            }catch(Exception e){
                system.debug('Error!!! Location_ID__cがテリトリIDではありません。');
                errMsg = 'Error!!! Location_ID__cがテリトリIDではありません。';
                isCustomException = true;
                throw new CreateBaseOpportunityBatchException('Error!!! Location_ID__cがテリトリIDではありません。');
            }
            territoryIdList.add(sell.Location_ID__c);
            */
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 del end
        }
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
        if(skipSellerFlg){
            system.debug('Warning!!! テリトリーと一致しないLocation_ID__cが存在します。 SellerQuotaName = '+ skipSellerNameSet);
        }
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end

        // get Opportunity
        // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 mod start
        //List<Opportunity> oppList  = [SELECT Name FROM Opportunity where Name in :sellMap.keySet()];
        List<Opportunity> oppList  = [SELECT Name, Last_FY_Actual_Amount_Capital__c, Last_FY_Actual_Amount_Dispo__c, Month_Quota__c, Actual_Amount_Capital__c, Actual_Amount_Dispo__c FROM Opportunity where Name in :sellMap.keySet()];
        // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 mod end
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 del start
        /*
        // get Territory
        List<Territory> territoryList = [Select Id From Territory where Id in :territoryIdList];
        system.debug('territoryList.size:' + territoryList.size());

        for(Territory t : territoryList){
            String idStr = t.Id;
            String id15digit = idStr.substring(0, 15);
            territoryMap.put(id15digit, t);
        }
        system.debug('territory:' + territoryList[0].Id);
        */
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 del end
        for(Opportunity opp : oppList){
           oppMap.put(opp.Name, opp);
        }

        for(String key : sellMap.keyset()){
            Opportunity opp = oppMap.get(key);
            Seller_Quota__c sell = sellMap.get(key);
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod start
            // テリトリーが一致しないsellerは連携対象としない
            if(!skipSellerNameSet.contains(sell.Name)){
                if(opp != null){
                    //updOppList.add(setOpportunityData(sell, opp, baseAcc, territoryMap));
                    // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add start
                    updOppIdSet.add(opp.Id);
                    // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add end
                    updOppList.add(setOpportunityData(sell, opp, baseAcc, territoryExternalIdMap));
                }else{
                    //updOppList.add(setOpportunityData(sell, null, baseAcc, territoryMap));
                    updOppList.add(setOpportunityData(sell, null, baseAcc, territoryExternalIdMap));
                }
            }
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod end
        }

        try{
            if(updOppList != null && updOppList.size() > 0) upsert updOppList;
        }catch(Exception e){
            errMsg = e.getMessage();
            lineNumber = e.getLineNumber();
            stackTrace = e.getStackTraceString();
            typeName = e.getTypeName();
            isCustomException = false;
            throw e;
        }

        // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add start
        if(updOppList != null && updOppList.size() > 0){
            for(Opportunity opp :updOppList){
                oppLineItemList.add(setOppLineItemData(opp,basePbeCapitalId,true,false));
                oppLineItemList.add(setOppLineItemData(opp,basePbeDispoId,false,false));
                // 2016.08.15 kotani add
                oppLineItemList.add(setOppLineItemData(opp,basePbeBaseId,false,true));
            }
            // 再作成対象案件製品が存在する場合
            if(updOppIdSet != null && updOppIdSet.size() > 0){
                delOppLineItemList = [select Id from OpportunityLineItem where OpportunityId in :updOppIdSet];
            }
        }
        try{
            if(oppLineItemList != null && oppLineItemList.size() > 0){
                if(delOppLineItemList != null && delOppLineItemList.size() > 0){
                    delete delOppLineItemList;
                }
                insert oppLineItemList;
            }
        }catch(Exception e){
            errMsg = e.getMessage();
            lineNumber = e.getLineNumber();
            stackTrace = e.getStackTraceString();
            typeName = e.getTypeName();
            isCustomException = false;
            throw e;
        }
        // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add end
    }
    // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod start
    //private Opportunity setOpportunityData(Seller_Quota__c sell, Opportunity opp, Account baseAcc, Map<String, Territory> territoryMap){
    private Opportunity setOpportunityData(Seller_Quota__c sell, Opportunity opp, Account baseAcc, Map<String, Id> territoryExternalIdMap){
    // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod end
        Opportunity upsOpp = new Opportunity();
        if(opp != null){
            upsOpp.Id = opp.Id;
            // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 add start
            if(upsOppNameSet.contains(opp.Name)){
                if (opp.Last_FY_Actual_Amount_Capital__c != null) {
                    if (sell.JP_Last_FY_Actual_Amount_Capital__c != null) {
                        sell.JP_Last_FY_Actual_Amount_Capital__c += opp.Last_FY_Actual_Amount_Capital__c;
                    } else {
                        sell.JP_Last_FY_Actual_Amount_Capital__c = opp.Last_FY_Actual_Amount_Capital__c;
                    }
                }
                if (opp.Last_FY_Actual_Amount_Dispo__c != null) {
                    if (sell.JP_Last_FY_Actual_Amount_Dispo__c != null) {
                        sell.JP_Last_FY_Actual_Amount_Dispo__c += opp.Last_FY_Actual_Amount_Dispo__c;
                    } else {
                        sell.JP_Last_FY_Actual_Amount_Dispo__c = opp.Last_FY_Actual_Amount_Dispo__c;
                    }
                }
                if (opp.Month_Quota__c != null) {
                    if (sell.MONTH_Quota__c != null) {
                        sell.MONTH_Quota__c += opp.Month_Quota__c;
                    } else {
                        sell.MONTH_Quota__c = opp.Month_Quota__c;
                    }
                }
                if (opp.Actual_Amount_Capital__c != null) {
                    if (sell.Actual_Amount_Capital__c != null) {
                        sell.Actual_Amount_Capital__c += opp.Actual_Amount_Capital__c;
                    }else{
                        sell.Actual_Amount_Capital__c = opp.Actual_Amount_Capital__c;
                    }
                }
                if (opp.Actual_Amount_Dispo__c != null) {
                    if (sell.Actual_Amount_Dispo__c != null) {
                        sell.Actual_Amount_Dispo__c += opp.Actual_Amount_Dispo__c;
                    } else {
                        sell.Actual_Amount_Dispo__c = opp.Actual_Amount_Dispo__c;
                    }
                }
            }
            // 2016.04.14 FM)harada CVJ_SELLOUT_DEV-100 add end
        }else{
            String selloutMonth = String.valueOf(sell.Sellout_Date__c.month());
            // 2015.04.05 FM)harada CVJ_SELLOUT_DEV-100 mod start
            //upsOpp.Name = sell.Sales_Class__c + '-' + sell.Seller_Name_del__c + '-' + selloutMonth + '-' + sell.Eval_Class__c;
            upsOpp.Name = sell.Location_Id__c + '-' + sell.Seller_Name_del__c + '-' + selloutMonth + '-' + sell.Eval_Class__c;
            // 2015.04.05 FM)harada CVJ_SELLOUT_DEV-100 mod end
            // 2015.04.05 FM)harada CVJ_SELLOUT_DEV-100 del start
            //upsOpp.LastModifiedById = sell.LastModifiedById;
            //upsOpp.CreatedById = sell.CreatedById;
            // 2015.04.05 FM)harada CVJ_SELLOUT_DEV-100 del end
            upsOpp.StageName = 'Closed Won';
        }

        upsOpp.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Japan_Base_Opportunity');
        upsOpp.AccountId = baseAcc.Id;
        // 2015.04.05 FM)harada CVJ_SELLOUT_DEV-100 del start
        //upsOpp.JP_Seller_Quota__c = sell.Id;
        // 2015.04.05 FM)harada CVJ_SELLOUT_DEV-100 del end
        upsOpp.OwnerId = sell.OwnerId;
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add start
        upsOpp.JP_OpportunityClassification__c = 'Base';
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add end

        if(sell.CurrencyIsoCode != null) upsOpp.CurrencyIsoCode = sell.CurrencyIsoCode;
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod start
        //if(sell.Location_ID__c != null) upsOpp.TerritoryId = sell.Location_ID__c;
        if(sell.Location_ID__c != null) upsOpp.TerritoryId = territoryExternalIdMap.get(sell.Location_ID__c);
        // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 mod end
        // 2016.04.06 FM)harada CVJ_SELLOUT_DEV-100 del start
        //if(sell.JP_Last_FY_Actual_Amount_Dispo__c != null) upsOpp.Amount = sell.JP_Last_FY_Actual_Amount_Dispo__c;
        // 2016.04.06 FM)harada CVJ_SELLOUT_DEV-100 del end
        if(sell.Sellout_Date__c != null) upsOpp.CloseDate = sell.Sellout_Date__c;
        if(sell.Actual_Amount_Capital__c != null) upsOpp.Actual_Amount_Capital__c = sell.Actual_Amount_Capital__c;
        if(sell.Actual_Amount_Dispo__c != null) upsOpp.Actual_Amount_Dispo__c = sell.Actual_Amount_Dispo__c;
        if(sell.Eval_Class__c != null) upsOpp.Eval_Class__c = sell.Eval_Class__c;
        if(sell.MONTH_Quota__c != null) upsOpp.Month_Quota__c = sell.MONTH_Quota__c;
        if(sell.Sales_Class__c != null) upsOpp.Sales_Class__c = sell.Sales_Class__c;
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add start
        if(sell.JP_Last_FY_Actual_Amount_Capital__c != null) upsOpp.Last_FY_Actual_Amount_Capital__c = sell.JP_Last_FY_Actual_Amount_Capital__c;
        if(sell.JP_Last_FY_Actual_Amount_Dispo__c != null) upsOpp.Last_FY_Actual_Amount_Dispo__c = sell.JP_Last_FY_Actual_Amount_Dispo__c;
        // 2016.04.05 FM)harada CVJ_SELLOUT_DEV-100 add end
        // 2016.04.15 FM)harada CVJ_SELLOUT_DEV-100 add start
        if(upsOpp.Name != null && !upsOppNameSet.contains(upsOpp.Name)){
            upsOppNameSet.add(baseAcc.Name + ': ' + upsOpp.Name);
        }
        if(opp != null && opp.Name != null && !upsOppNameSet.contains(opp.Name)){
            upsOppNameSet.add(opp.Name);
        }
        // 2016.04.15 FM)harada CVJ_SELLOUT_DEV-100 add end
        return upsOpp;
    }

    // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add start
    private OpportunityLineItem setOppLineItemData(Opportunity opp, Id basePbeId, Boolean isCapitalFlg, Boolean isBaseFlg){
        OpportunityLineItem InsOppLineItem = new OpportunityLineItem();
        Decimal tmpUnitPrice = null;
        // マッピング値を設定
        if(opp != null) InsOppLineItem.OpportunityId = opp.Id;
        if(basePbeId != null) InsOppLineItem.PricebookEntryId = basePbeId;
        InsOppLineItem.Quantity = 1;
        if(isBaseFlg){
                if(opp.Last_FY_Actual_Amount_Dispo__c == null){
                    tmpUnitPrice = 0;
                }
                else {
                    tmpUnitPrice = opp.Last_FY_Actual_Amount_Dispo__c;
                }
        }
        else{
            if(isCapitalFlg){
                if(opp.Actual_Amount_Capital__c == null){
                    tmpUnitPrice = 0;
                }
                else {
                    tmpUnitPrice = opp.Actual_Amount_Capital__c;
                }
            }
            else{
                if(opp.Actual_Amount_Dispo__c == null){
                    tmpUnitPrice = 0;
                }
                else {
                    tmpUnitPrice = opp.Actual_Amount_Dispo__c;
                }
            }
        }
        InsOppLineItem.UnitPrice = tmpUnitPrice;
        if(isBaseFlg){
            InsOppLineItem.Product_Type__c = 'Disposable';
        }
        else{
            if(isCapitalFlg){
                InsOppLineItem.Product_Type__c = 'Capital';
            }
            else {
                InsOppLineItem.Product_Type__c = 'Disposable';
            }
        }
        return InsOppLineItem;
    }
    // 2016.03.10 ECS)kawada CVJ_SELLOUT_DEV-100 add end

    global void finish(Database.BatchableContext BC){
        if(String.isBlank(errMsg)){
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
            if(skipSellerFlg){
                skipWarnSendMail(skipSellerNameSet);
            }
            // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end
            Create_Base_Opportunity_Batch_LastDate__c lastDateSetting = Create_Base_Opportunity_Batch_LastDate__c.getOrgDefaults();
            lastDateSetting.Date__c = System.today();
            update lastDateSetting;
            system.debug('End CreateBaseOpportunityBatch：' + datetime.now());
        }
        else {
            errSendMail(errMsg,isCustomException);
        }
    }

    public class CreateBaseOpportunityBatchException extends Exception{}

    global void errSendMail(String errmsg, Boolean isCustomException){
        // 例外内容をメール送信
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Object executeDatetimeObj = datetime.now();
        String executeDatetime = String.valueOf((Datetime)executeDatetimeObj);
        String emailAddress = UserInfo.getUserEmail();
        String[] toAddresses = new String[] {emailAddress};
        String subject = '■BatchErrorNotification ： CreateBaseOpportunityBatch : ' + executeDatetime;
        String body = '以下のバッチ実行時にエラーが発生しました。\n';
        body += '■BatchErrorNotification ： CreateBaseOpportunityBatch\n';
        body += '・ ExecuteDatetime : ' + executeDatetime + '\n';
        if(!isCustomException){
            body += '・ ErrorDetail : ' + errmsg+ '\n';
            body += '・ lineNumber : ' + lineNumber + '\n';
            body += '・ typeName : ' + typeName + '\n';
            body += '・ stackTrace : ' + stackTrace;
        }
        else {
            body += '・ ErrorDetail : ' + errmsg;
        }
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        try{
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            if (results[0].isSuccess()) {
                System.debug(LoggingLevel.INFO, 'Succeeded');
            }
        }
        catch(Exception e){
            System.debug(e.getMessage());
            throw e;
        }
    }

    // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add start
    global void skipWarnSendMail(Set<String> skipSellerNameSet){
        // 処理をスキップしたSellerQuotaの名前をメール送信
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Object executeDatetimeObj = datetime.now();
        String executeDatetime = String.valueOf((Datetime)executeDatetimeObj);
        String emailAddress = UserInfo.getUserEmail();
        String[] toAddresses = new String[] {emailAddress};
        String subject = '■BatchWarningNotification ： CreateBaseOpportunityBatch : ' + executeDatetime;
        String body = '以下のsellerQuotaは、Location_ID__cが一致しない為連携されませんでした。\n';
        body += '■BatchWarningNotification ： CreateBaseOpportunityBatch\n';
        body += '・ ExecuteDatetime : ' + executeDatetime + '\n';
        body += '  ・ SellerQuota Name : ';
        for(String s :skipSellerNameSet){
            body += '\n' + '  　';
            body += s;
        }
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        try{
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            if (results[0].isSuccess()) {
                System.debug(LoggingLevel.INFO, 'Succeeded');
            }
        }
        catch(Exception e){
            System.debug(e.getMessage());
            throw e;
        }
    }
    // 2016.02.29 ECS)kawada CVJ_SELLOUT_DEV-99 add end

}