public class repeatCon {


  public String Country{get;set;}
  public String GBU{get;set;}
  public String Division{get;set;}

  public void next() { 
       System.debug('--->callingnext');

    addMonth(1);
    
  }
   public void apply() { 
       System.debug('--->callingnext');

    addMonth(0);
    
  }
  
 
  public void prev() { 
    addMonth(-1); 
  }
  
  
  public List<SelectOption> getGBUValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--None--'));
        for(String individualGBUName : GBUCountry__c.getValues('All').GBUs__c.split(','))
            options.add(new SelectOption(individualGBUName ,individualGBUName ));                
        return options;
        }
        
        
        
        
        public List<SelectOption> getDivisionValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--None--'));
        for(String eachDivisionName : DivisionCountry__c.getValues('All').Divisions__c.split(','))
            options.add(new SelectOption(eachDivisionName,eachDivisionName));            
        return options;
        }
  
  
 public Pagereference ChangeCalender(){
  getmonth();
  getweeks();
  return null;
 } 
   
  public repeatCon() {
  System.debug('--->callingrepeatcon');
   GBU='';
   Division='';
   Country='China';
   Date d = system.today();  // default to today 
   Integer mo = d.month(); 
   String m_param = System.currentPageReference().getParameters().get('mo');
   String y_param = System.currentPageReference().getParameters().get('yr');
   
   
   if (m_param != null) { 
        Integer mi = Integer.valueOf(m_param); 
        if (mi > 0 && mi <= 12) {
          d = Date.newInstance(d.year(),mi,d.day());
        }
   }
   
   if (y_param != null) { 
        Integer yr = Integer.valueOf(y_param); 
        d = Date.newInstance(yr, d.month(), d.day());
   }

   setMonth(d);
  }
 
  public List<Month.Week> getWeeks() { 
       System.debug('--->callinggetweeks');

    system.assert(month!=null,'month is null');
    return month.getWeeks();
  }
  
  public Month getMonth() {
       System.debug('--->callinggetmonth');
 return month; } 
  
  public List<Selectoption> getallEMSCountries(){
  
      list<SelectOption> options = new list<SelectOption>();
      Schema.sObjectType objType = EMS_Event__c.getSObjectType(); 
      
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      list<Schema.PicklistEntry> values = fieldMap.get('Location_Country__c').getDescribe().getPickListValues();
      for (Schema.PicklistEntry a : values)
      { 
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }
      return options; 
  }

  private void setMonth(Date d) { 
   System.debug('--->callingsetmonth');
    month = new Month(d);  
    system.assert(month != null); 

    Date[] da = month.getValidDateRange();  // gather events that fall in this month
    Date firstdate=da[0];
    Date seconddate=da[1];
    If(GBU=='None')
    GBU='';
    If(Division=='None')
    Division='';
    String query='select id,Name,End_Date__c,Start_Date__c,Division_Primary__c,GBU_Primary__c,Location_Country__c,DurationInMinutes__c from EMS_Event__c where Start_Date__c >= :firstdate AND End_Date__c<=:seconddate AND Location_Country__c=:Country AND GBU_Primary__c LIKE \'%' + GBU + '%\''+' AND Division_Primary__c LIKE \'%' + Division + '%\''+' order by Start_Date__c';
    system.debug('+++++'+query);
    /*
     events = [ select id,Name,End_Date__c,Start_Date__c,Location_Country__c,DurationInMinutes__c
               from EMS_Event__c where Start_Date__c >= :da[0] AND End_Date__c<= :da[1] AND Location_Country__c=:Country order by Start_Date__c ];
    */
    events=database.query(query);
    
    
    /*System.debug('------>again called');         
    String query = 'select id,Name,End_Date__c,Start_Date__c,Location_Country__c,DurationInMinutes__c from EMS_Event__c where Start_Date__c >= :firstdate AND End_Date__c<= :seconddate and Location_Country__c =:Country and GBU_Primary__c LIKE \'%' + GBU+'%\''+' and Division_Primary__c LIKE \'%'+Division+'%\'';
    System.debug('----->'+Query+'>>>>>>>>>'+country);
    events=database.query(query);
    */
    month.setEvents(events);  // merge those events into the month class
  }
  
  private void addMonth(Integer val) { 
     System.debug('--->callingaddmonth');

    Date d = month.getFirstDate();
    d = d.addMonths(val);
    setMonth(d);
  }

  private List<EMS_Event__c> events;
  private Month month;
}