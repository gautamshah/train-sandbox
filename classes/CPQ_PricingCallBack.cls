/**
Pricing CallBack function invoked when clicking in Go To Pricing and Reprice button

DEVELOPER INFORMATION
=====================
Name                Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund       PAB             Medtronic.com
Isaac Lewis         IL              Statera.com
Bryan Fry           BF              Statera.com

MODIFICATION HISTORY
====================
Date-Id         Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
2014-12-13      Yuli Fintescu       Created
10/29/2015      Paul Berglund       Merged in Apttus's latest updates
05/26/2016      Bryan Fry           Removed surgical changes to Price Uom field.
20161201-A      BF          AV-260  Null reference - added validQuantityAndPrice method
*/ 
global class CPQ_PricingCallBack implements Apttus_Config2.CustomClass.IPricingCallback2 {
    //cart
    private Apttus_Config2.ProductConfiguration cart = null;
    
    //mode
    private Apttus_Config2.CustomClass.PricingMode mode = null;
        
    private List<Apttus_Config2__LineItem__c> lineSOs;
    private Apttus_Config2__ProductConfiguration__c configSO;
    private Apttus_Config2__ProductConfiguration__c configRecordTypes;
    //private List<Apttus_Config2__LineItem__c> allPromotionalLineItems;
    private List<Apttus_Config2__LineItem__c> AllOptionLineItems;
    private String ssgPriceListID;
    
    public static final String EACH_PRICING = 'FOR EACH';

    global void start(Apttus_Config2.ProductConfiguration cart) {
        System.debug('*** start');
        this.cart = cart;
        
        lineSOs = new List<Apttus_Config2__LineItem__c>();
        if (Test.isRunningTest()) {
            List<Apttus_Config2__ProductConfiguration__c> configSOs = new List<Apttus_Config2__ProductConfiguration__c>();
            Test_CPQ_PricingCallBack.createTestData(lineSOs, configSOs);
            configSO = configSOs[0];
        } else {
            configSO = cart.getConfigSO();
        }

        ssgPriceListID = CPQ_Utilities.getSSGPriceListID();
        configRecordTypes = [Select Apttus_QPConfig__Proposald__r.RecordType.DeveloperName,
                                    Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName
                             From Apttus_Config2__ProductConfiguration__c
                             Where Id = :configSO.Id];
                             
  /*      allPromotionalLineItems=[select id,name,is_Promotional__c,Apttus_Config2__Quantity__c from Apttus_Config2__LineItem__c where                    is_Promotional__c=true and Apttus_Config2__LineType__c='Product/Service' and Apttus_Config2__ConfigurationId__c=:configSO];
        
        Set<String> allPromotionalLineItemsId = new Set<String>();
        
        if(allPromotionalLineItems.size() > 0){
            for(Apttus_Config2__LineItem__c promotional :allPromotionalLineItems ){
                allPromotionalLineItemsId.add(promotional.id);                
            }
        }
            
        AllOptionLineItems =[select id,name,Apttus_Config2__Quantity__c from Apttus_Config2__LineItem__c where Apttus_Config2__LineType__c='Option' and and Apttus_Config2__ConfigurationId__c=:configSO] */
        
    }
    
    Map<String, Apttus_Config2__PriceListItem__c> priceListItems;
    Map<String, CPQ_Customer_Specific_Pricing__c> csps;
    List<CPQ_Error_Log__c> errors;
    Decimal uplift = 1.03;
    /**
     * Callback to indicate the pricing mode
     * @param mode the pricing mode
     */
    global void setMode(Apttus_Config2.CustomClass.PricingMode mode) {
        System.debug('The mode in setMode is ' + mode);
        this.mode = mode;
        
        if (mode != Apttus_Config2.CustomClass.PricingMode.ROLLDOWN) {
//prepare data for call back
//==========================
            priceListItems = new Map<String, Apttus_Config2__PriceListItem__c>();
            csps = new Map<String, CPQ_Customer_Specific_Pricing__c>();
            errors = new List<CPQ_Error_Log__c>();
            CPQ_PricingProcesses.prepareCallBack(configSO.ID, priceListItems, csps, errors);
            uplift = (100.00 + CPQ_Utilities.getRenewalUpliftPercentage()) / 100.00;
        }
    }
    
    /**
     * Callback before pricing the line item collection.
     * Use this method to do all required pre-processing to prepare the line items for pricing.
     * @param itemColl the line item collection to pre-process
     */  
    global void beforePricing(Apttus_Config2.ProductConfiguration.LineItemColl itemColl) {
        System.debug('*** beforePricing*****');
        /* Yuli: This piece of code sucks. rewrote below.
        if (!Test.isRunningTest()) {
        
            // Code Added by Mehul Parmar 
            // Date : 19th Oct, 2015
            // Updated: 27th Oct, 2015
            // Purpose : Update the quantity of options with the quantity of Bundle
            
            // Code Started
            Set<Decimal> ProductServiceLineNumber= new Set<Decimal>();
            Map<Decimal,Decimal> ProductWiseQuantity= new Map<Decimal,Decimal>();
            Set<Id> setProductOptionIds = new Set<Id>();
            Set<Id> setProductPromotionIds = new Set<Id>();
            
            System.debug('*** in test Method');
            for(Apttus_Config2.LineItem lineItemMO : itemColl.getAllLineItems()) {
                System.debug('*** in LineitemMO');
                Apttus_Config2__LineItem__c line = lineItemMO.getLineItemSO();
               
                if(line.Apttus_Config2__LineType__c =='Product/Service' && line.Apttus_Config2__ChargeType__c=='Promotion' && line.is_Promotional__c ==true) {
                    //ProductServiceLineNumber.add(line.Apttus_Config2__LineNumber__c);
                    ProductWiseQuantity.put(line.Apttus_Config2__LineNumber__c,line.Apttus_Config2__Quantity__c );
                    setProductPromotionIds.add(line.Apttus_Config2__ProductId__c);
                    
                } else If (line.Apttus_Config2__LineType__c =='Option') {
                
                    //PriceListItems
                    setProductOptionIds.add(line.Apttus_Config2__OptionId__c);
                }
            }
           
            //if (mode == Apttus_Config2.CustomClass.PricingMode.BASEPRICE){
                for(Decimal keyVal : ProductWiseQuantity.keySet()) {
                    for(Apttus_Config2.LineItem lineItemMO : itemColl.getAllLineItems()) {
                        System.debug('### in LineitemMO..' + lineItemMO.getLineItemSO());
                        Apttus_Config2__LineItem__c line = lineItemMO.getLineItemSO();
                         
                        //Decimal defaultQuantity = poc.Apttus_Config2__DefaultQuantity__c;// > 0 ? Apttus_Config2__DefaultQuantity__c: 0;
                        Decimal defaultQuantity = line.Default_Quantity__c;
                        
                        If ( Integer.ValueOf(line.Default_Quantity__c) == 0){
                            line.Default_Quantity__c = line.Apttus_Config2__Quantity__c;
                        }
                        
                        System.debug('### parentBundleNumber .. ' + line.Apttus_Config2__ParentBundleNumber__c);
                        if(line.Apttus_Config2__LineType__c =='Option' && line.Apttus_Config2__ParentBundleNumber__c==keyVal) {
                            System.debug('###If parentBundleNumber .. ' + line.Apttus_Config2__ParentBundleNumber__c);
                            
                            If (line.Apttus_Config2__ChargeType__c == 'Promotion - Accessory' && defaultQuantity > 0) {
                                line.Apttus_Config2__Quantity__c = defaultQuantity * ProductWiseQuantity.get(keyVal);

                            } else If (line.Apttus_Config2__ChargeType__c == 'Promotion - Accessory' && defaultQuantity <= 0) {
                                line.Apttus_Config2__Quantity__c = line.Apttus_Config2__Quantity__c * ProductWiseQuantity.get(keyVal);

                            } else {    
                            
                                line.Apttus_Config2__Quantity__c =ProductWiseQuantity.get(keyVal);
                            }
                        }
                       
                        lineItemMO.updatePrice();
                    }
                }
            //}
          
        }
        // Code Ended
        */
        
        if (!Test.isRunningTest()) {
            for(Apttus_Config2.LineItem lineItemMO : itemColl.getAllLineItems()) {
                Apttus_Config2__LineItem__c line = lineItemMO.getLineItemSO();
                lineSOs.add(line);
            }
        }
        
/*
    Bug: quantity is not correct for options in a bundle, if users select overall quantity on bundle.
*/
        //bundle line => list of options lines
        Map<String, Set<String>> lineBundleToOptions = CPQ_PricingProcesses.buildLineBundleToOptions(lineSOs);
        //line items as map
        Map<ID, Apttus_Config2__LineItem__c> lineSOsMap = new Map<ID, Apttus_Config2__LineItem__c>(lineSOs);
        for(Apttus_Config2__LineItem__c lineItemSO : lineSOs) {
            if (lineItemSO.Apttus_Config2__Quantity__c == null || lineItemSO.is_Promotional__c == false)
                continue;
            
            //loop through each bundle
            if (lineBundleToOptions.containsKey(lineItemSO.ID)) {
                Set<String> optionIds = lineBundleToOptions.get(lineItemSO.ID);
                
                for (String optionLineId : optionIds) {
                    Apttus_Config2__LineItem__c optionLine = lineSOsMap.get(optionLineId);
                    
                    if (optionLine.Option_DefaultQuantity__c == null)
                        continue;
                    
                    optionLine.Default_Quantity__c = optionLine.Option_DefaultQuantity__c;
                    optionLine.Apttus_Config2__Quantity__c = optionLine.Option_DefaultQuantity__c * lineItemSO.Apttus_Config2__Quantity__c;
                    
                    System.debug('*** user input overall bundle quantity ' + lineItemSO.Apttus_Config2__Quantity__c + 
                                    '\noption group label ' + optionLine.Apttus_Config2__OptionGroupLabel__c + 
                                    '\noption default quantity ' + optionLine.Option_DefaultQuantity__c + 
                                    '\noption line quantity ' + optionLine.Apttus_Config2__Quantity__c);
                }
            }
        }

/*
at BASEPRICE mode 
    1. call ws to get current contract price. 
    2. update Apttus_Config2__LineItem__c.Apttus_Config2__BasePrice__c with the contract price
    3. copy all the pricing related fields from Apttus_Config2__PriceListItem__c to Apttus_Config2__LineItem__c
    4. set Apttus_Config2__LineItem__c.Apttus_Config2__IsCustomPricing__c to true, so BasePrice field CAN be set. 
        if IsCustomPricing__c to false, BasePrice always gets overriden with the PriceList list Price.
        ***UPDATE*** IsCustomPricing__c no longer stops the BasePrice from being kept.  It also triggers Apttus code
                     to create additional line items between beforePricing and afterPricing, so needs to be removed.
                     However, QA and production are on different versions of Apttus packages that still have the pricing
                     issue so it must remain set to true to have cart pricing not be overwritten until the packages
                     are updated.

*/

        if (mode == Apttus_Config2.CustomClass.PricingMode.BASEPRICE) {
            
            for(Apttus_Config2__LineItem__c lineItemSO : lineSOs) {
                
                String product_option = lineItemSO.Product_ID__c;
                
                if (lineItemSO.Apttus_Config2__IsPrimaryLine__c == true && 
                        csps.containsKey(product_option) && 
                        priceListItems.containsKey(product_option)) {
                    
                    Apttus_Config2__PriceListItem__c pli = priceListItems.get(product_option);
                    CPQ_Customer_Specific_Pricing__c csp = csps.get(product_option);

                    lineItemSO.Apttus_Config2__IsCustomPricing__c = true;
                    lineItemSO.Apttus_Config2__ChargeType__c = pli.Apttus_Config2__ChargeType__c;
                    lineItemSO.Apttus_Config2__PriceType__c = pli.Apttus_Config2__PriceType__c;
                    lineItemSO.Apttus_Config2__PriceMethod__c = pli.Apttus_Config2__PriceMethod__c;
                    lineItemSO.Apttus_Config2__BasePriceMethod__c = pli.Apttus_Config2__PriceMethod__c;
                    lineItemSO.Apttus_Config2__PriceUom__c = pli.Apttus_Config2__PriceUom__c;
                    lineItemSO.Apttus_Config2__BasePrice__c = csp.Contract_Price__c == null ? pli.Apttus_Config2__ListPrice__c : csp.Contract_Price__c;
                    lineItemSO.Original_Uom_Price__c = lineItemSO.Apttus_Config2__BasePrice__c;
                    lineItemSO.Apttus_Config2__ListPrice__c = pli.Apttus_Config2__ListPrice__c;
                    lineItemSO.Apttus_Config2__PriceListItemId__c = pli.Id;
                    lineItemSO.Apttus_Config2__BaseCost__c = pli.Apttus_Config2__Cost__c;
                    lineItemSO.Apttus_Config2__Cost__c = pli.Apttus_Config2__Cost__c;
                    lineItemSO.Commitment_Type__c = csp.Commitment_Type__c;
                    lineItemSO.Current_Root_Contract__c = csp.Root_Contract__c;
                    lineItemSO.GPO__c = csp.GPO_Price__c;
                    lineItemSO.Non_Committed_Price__c = csp.Non_Committed_Price__c;
                    lineItemSO.Pricing_Status__c = 'Success';
                    System.debug('*** lineItemSO csp.Root_Contract__c ' + csp.Root_Contract__c + 
                                                '\ncsp.Contract_Price__c ' + csp.Contract_Price__c + 
                                                '\ncsp.GPO_Price__c ' + csp.GPO_Price__c + 
                                                '\ncsp.Non_Committed_Price__c ' + csp.Non_Committed_Price__c);
                    
                    if (lineItemSO.Apttus_Config2__PriceListId__c == ssgPriceListID) {
                        if (lineItemSO.Pricing_Type__c == EACH_PRICING) {
                            // 20161201-A
                            if (validQuantityAndPrice(lineItemSO.Unit_Quantity__c, lineItemSO.Original_Uom_Price__c))
                            {
                                lineItemSO.Apttus_Config2__BasePrice__c = lineItemSO.Original_Uom_Price__c/lineItemSO.Unit_Quantity__c;
                                Decimal basePrice;
                                if (lineItemSO.Apttus_Config2__AdjustmentType__c == 'Base Price Override' && lineItemSO.Apttus_Config2__BasePriceOverride__c != null) {
                                    basePrice = lineItemSO.Apttus_Config2__BasePriceOverride__c;
                                } else {
                                    basePrice = lineItemSO.Apttus_Config2__BasePrice__c;
                                }
                                lineItemSO.Apttus_Config2__ExtendedPrice__c = basePrice * lineItemSO.Apttus_Config2__Quantity__c;
                                lineItemSO.Apttus_Config2__BaseExtendedPrice__c = lineItemSO.Apttus_Config2__ExtendedPrice__c;
                                if (lineItemSO.Apttus_Config2__Uom__c == null) {
                                    lineItemSO.Apttus_Config2__Uom__c = lineItemSO.Apttus_Config2__PriceUom__c;
                                }
                            }
                        } else if (lineItemSO.Original_Uom_Price__c != null && lineItemSO.Original_Uom_Price__c != lineItemSO.Apttus_Config2__BasePrice__c) {
                            lineItemSO.Apttus_Config2__BasePrice__c = lineItemSO.Original_Uom_Price__c;
                            Decimal basePrice;
                            if (lineItemSO.Apttus_Config2__AdjustmentType__c == 'Base Price Override' && lineItemSO.Apttus_Config2__BasePriceOverride__c != null) {
                                basePrice = lineItemSO.Apttus_Config2__BasePriceOverride__c;
                            } else {
                                basePrice = lineItemSO.Apttus_Config2__BasePrice__c;
                            }
                            lineItemSO.Apttus_Config2__ExtendedPrice__c = lineItemSO.Apttus_Config2__BasePrice__c * lineItemSO.Apttus_Config2__Quantity__c;
                            lineItemSO.Apttus_Config2__BaseExtendedPrice__c = lineItemSO.Apttus_Config2__ExtendedPrice__c;
                        }
                    }

                    if (configSO.Deal_Type__c == 'Renewal Deal' && lineItemSO.Apttus_Config2__BasePrice__c != null) {
                        Decimal uplifted = lineItemSO.Apttus_Config2__BasePrice__c * uplift;
                        if (uplifted < (pli.Apttus_Config2__ListPrice__c == null ? 0 : pli.Apttus_Config2__ListPrice__c))
                            lineItemSO.Apttus_Config2__BasePrice__c = uplifted;
                    }
                } else if (!csps.containsKey(product_option) &&
                           configRecordTypes.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName != 'New_Product_LOC' &&
                           configRecordTypes.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName != 'Promotional_LOC') {
                    lineItemSO.Pricing_Status__c = 'Pricing Not Found';
                } else {
                    lineItemSO.Pricing_Status__c = 'Success';
                }
            }
        } else if (mode == Apttus_Config2.CustomClass.PricingMode.ADJUSTMENT) {
            
            for(Apttus_Config2__LineItem__c lineItemSO : lineSOs) {
                                
                String product_option = lineItemSO.Product_ID__c;
                
                if (lineItemSO.Is_Bundle_Header__c == false && 
                        lineItemSO.Apttus_Config2__IsPrimaryLine__c == true && 
                        lineItemSO.Apttus_Config2__LineType__c == 'Product/Service' &&                  //skipping option
                        priceListItems.containsKey(product_option)) {
                    
                    Apttus_Config2__PriceListItem__c pli = priceListItems.get(product_option);
                    
                    if (lineItemSO.Price_Mode__c == 'Rep Floor' && pli.Apttus_Config2__ProductId__r.Rep_Floor_Price__c != null) {
                        CPQ_PricingProcesses.SetFloorPrice(lineItemSO, pli.Apttus_Config2__ProductId__r.Rep_Floor_Price__c);
                        
                    } else if (lineItemSO.Price_Mode__c == 'RM Floor' && pli.Apttus_Config2__ProductId__r.RM_Floor_Price__c != null) {
                        CPQ_PricingProcesses.SetFloorPrice(lineItemSO, pli.Apttus_Config2__ProductId__r.RM_Floor_Price__c);
                        
                    } else if (lineItemSO.Price_Mode__c == 'AVP Floor' && pli.Apttus_Config2__ProductId__r.Zone_VP_Floor_Price__c != null) {
                        CPQ_PricingProcesses.SetFloorPrice(lineItemSO, pli.Apttus_Config2__ProductId__r.Zone_VP_Floor_Price__c);
                        
                    } else if (lineItemSO.Price_Mode__c == 'Reset') {
                        CPQ_PricingProcesses.ResetBasePrice(lineItemSO);
                    }
                }

                if (lineItemSO.Apttus_Config2__PriceListId__c == ssgPriceListID) {
                    if (lineItemSO.Pricing_Type__c == EACH_PRICING) {
                        // 20161201-A
                        if (validQuantityAndPrice(lineItemSO.Unit_Quantity__c, lineItemSO.Original_Uom_Price__c))
                        {
                            lineItemSO.Apttus_Config2__BasePrice__c = lineItemSO.Original_Uom_Price__c/lineItemSO.Unit_Quantity__c;
                            Decimal basePrice;
                            if (lineItemSO.Apttus_Config2__AdjustmentType__c == 'Base Price Override' && lineItemSO.Apttus_Config2__BasePriceOverride__c != null) {
                                basePrice = lineItemSO.Apttus_Config2__BasePriceOverride__c;
                            } else {
                                basePrice = lineItemSO.Apttus_Config2__BasePrice__c;
                            }
                            lineItemSO.Apttus_Config2__ExtendedPrice__c = basePrice * lineItemSO.Apttus_Config2__Quantity__c;
                            lineItemSO.Apttus_Config2__BaseExtendedPrice__c = lineItemSO.Apttus_Config2__ExtendedPrice__c;
                            if (lineItemSO.Apttus_Config2__Uom__c == null) {
                                lineItemSO.Apttus_Config2__Uom__c = lineItemSO.Apttus_Config2__PriceUom__c;
                            }
                        }
                    } else if (lineItemSO.Original_Uom_Price__c != null && lineItemSO.Original_Uom_Price__c != lineItemSO.Apttus_Config2__BasePrice__c) {
                        lineItemSO.Apttus_Config2__BasePrice__c = lineItemSO.Original_Uom_Price__c;
                        Decimal basePrice;
                        if (lineItemSO.Apttus_Config2__AdjustmentType__c == 'Base Price Override' && lineItemSO.Apttus_Config2__BasePriceOverride__c != null) {
                            basePrice = lineItemSO.Apttus_Config2__BasePriceOverride__c;
                        } else {
                            basePrice = lineItemSO.Apttus_Config2__BasePrice__c;
                        }
                        lineItemSO.Apttus_Config2__ExtendedPrice__c = basePrice * lineItemSO.Apttus_Config2__Quantity__c;
                        lineItemSO.Apttus_Config2__BaseExtendedPrice__c = lineItemSO.Apttus_Config2__ExtendedPrice__c;
                    }
                }

                //clear out price mode, so it won't come here again
                lineItemSO.Price_Mode__c = null;
                
                lineItemSO.Apttus_Config2__IsCustomPricing__c = false;
            }
        } else {
            for(Apttus_Config2__LineItem__c lineItemSO : lineSOs) {
                                
                //exclude misc charges - they didn't go through the process above
                String product_option = lineItemSO.Product_ID__c;
                if (product_option != null) {
                    lineItemSO.Apttus_Config2__IsCustomPricing__c = false;
                }
            }
        }
    }
    
    // 20161201-A
    // There must be a quantity & price for the web service to work
    private boolean validQuantityAndPrice(decimal quantity, decimal price)
    {
        return NotNullNotZero(quantity) && NotNullNotZero(price);
    }
    
    private boolean NotNullNotZero(integer value)
    {
        return (value != null && value != 0);
    }

    private boolean NotNullNotZero(decimal value)
    {
        return (value != null && value != 0.00);
    }
    
    global void beforePricingLineItem(Apttus_Config2.ProductConfiguration.LineItemColl itemColl, Apttus_Config2.LineItem lineItemMO) {
        System.debug('*** mode in beforePricingLineItem ' + mode);
    }
    
    /**
     * Callback after pricing the line item collection
     * Use this method to do all required post-processing after line items are priced.
     * @param itemColl the line item collection to post-process
     */
    global void afterPricing(Apttus_Config2.ProductConfiguration.LineItemColl itemColl) {
        System.debug('*** mode in afterPricing ' + mode);
    }     
    
    global void afterPricingLineItem(Apttus_Config2.ProductConfiguration.LineItemColl itemColl, Apttus_Config2.LineItem lineItemMO) {
        System.debug('*** mode in afterPricingLineItem ' + mode);
    }
    
    /**
     * Callback after all batches of line items are processed
    */
    global void finish() {      
        System.debug('*** finish');
        /* this code is to update the product configuration and configuration line items */
        if (mode == Apttus_Config2.CustomClass.PricingMode.BASEPRICE || 
                mode == Apttus_Config2.CustomClass.PricingMode.ADJUSTMENT) {
            /*
            List<Apttus_Config2__LineItem__c> linesToUpdate = new List<Apttus_Config2__LineItem__c>();
            List<Apttus_Config2.LineItem> allLines = cart.getlineItems();
            for(Apttus_Config2.LineItem lineItemMO : allLines) {
                linesToUpdate.add(lineItemMO.getLineItemSO());
            }
            
            System.debug('*** linesToUpdate.size ' + linesToUpdate.size());
            if (linesToUpdate.size() > 0) {
                Database.update(linesToUpdate, false);
            }
            */
            
            
            List<CPQ_Customer_Specific_Pricing__c> cspstoUpsert = new List<CPQ_Customer_Specific_Pricing__c>();
            for (CPQ_Customer_Specific_Pricing__c csp : csps.values()) {
                if (csp.ID == null) {
                    cspstoUpsert.add(csp);
                }
            }
            
            System.debug('*** cspstoUpsert.size ' + cspstoUpsert.size());
            if (cspstoUpsert.size() > 0) {
                Database.upsert(cspstoUpsert, CPQ_Customer_Specific_Pricing__c.Fields.External_ID__c, false);
            }

            System.debug('*** errors.size ' + errors.size());
            if (errors.size() > 0) {
                insert errors;
            }
        }
    }
}