@isTest
public class Pricebook2_cTest
{
    @isTest
    static void itShouldGetStandardPriceBookId ()
    {
    	upsert cpqVariable_TestSetup.create(null);
    	
        //cpq_test.buildScenario_CPQ_ConfigSettings();
        String comparisonId = Test.getStandardPriceBookId();
        System.debug('comparisonId:' + comparisonId);
        String standardId = Pricebook2_c.getStandardPriceBookID();
        System.debug('standardId: ' + standardId);
        System.assertEquals(comparisonId,standardId);
    }
}