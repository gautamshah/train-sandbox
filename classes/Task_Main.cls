/****************************************************************************************
* Name    : Class: Task_Main
* Author  : Paul Berglund
* Date    : 07/23/2015
* Purpose : A wrapper for static functionality for Task records
*
* Dependancies:
*   Called by: TaskTriggerDispatcher
*
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 10/21/2015    Paul Berglund           Added support for the "Notify Owner when CD done"
*                                       method
* 10/28/2015    Paul Berglund           Added support for the "Notify CS when Agreement
*                                       Activated"
* 11/16/2015    Paul Berglund           Changed the Account/Contact creation from a static
*                                       code block to a method that is only called once
*                                       a valid task is detected. Also fixed the error
*                                       being thrown if the Account does not exist.
* 12/02/2015    Paul Berglund           Changed attachment code to occur during the SendEmails
*                                       to allow the files to be created and attached.
* 12/23/2015    Paul Berglund           Added support for the "Notify Owner Agreement Implemented" task
* 01/11/2015    Paul Berglund           Added support to disable "Notify CS when Agreement Activated"
*                                       if the TBD_or_CPP_Funds__c on Apttus__APTS_Agreement__c
*                                       is checked
* 03/08/2016    Paul Berglund           Moved CPQ logic into TaskConfigSetting
* 05/11/2016    Paul Berglund           Removed calles to myTriggerManager
*                                    Added some debug statements
* 08/02/2016    Isaac Lewis             Added support to disable "Notify CS when Agreement Activated"
*                                       if T_HLVL_VA__c on Apttus__APTS_Agreement__c
*                                       is checked. Replaced TBD_or_CPP_Funds__c with Using_TBD_Dollars__c and Using_CPP_Funds__c.
* 08/17/2016    Paul Berglund           Added some @testVisible attributes
*****************************************************************************************/
public with sharing class Task_Main
{
    private class LocalException extends Exception { }

    // Set this to True before you call any of the
    // public methods if you want the Exceptions
    // to make it to your code.  It defaults to
    // False if you are running a test.
    //
    //This property will return True under two
    // conditions:
    //    Previously set to True (either before
    //    a public method is called or set by the getter)
    //    Not Previously set and Not Running a Test
    // Otherwise it returns False
    @testVisible
    private static boolean throwException
    {
        get
        {
            if (throwException == null)
                throwException = !Test.isRunningTest();
            return throwException;
        }
        set;
    }

  @testVisible
  private static void ThrowException(string msg)
  {
    if (throwException)
      throw new LocalException(msg);
  }

  @testVisible
  private static void ThrowException(Exception ex)
  {
    if (throwException)
      throw ex;
  }

    @testVisible
    private static Map<Id, Apttus__APTS_Agreement__c> Agreements;
    @testVisible
    private static Map<Id, Apttus_Proposal__Proposal__c> Proposals;
    @testVisible
    private static Map<Id, Set<Id>> TaskAttachments = new Map<Id, Set<Id>>();
  @testVisible
    private static Map<Id, User> Users;
    @testVisible
    private static Map<Id, myEMailMessage> mailToSend = new Map<Id, myEMailMessage>();
    @testVisible
    private static List<Task> tasksToCreate = new List<Task>();
    @testVisible
    private static Contact Contact;

    private static final string CRLF = '\r\n';

    // Since the JSON.Serialize adds an extra "debug" token to the JSON
    // string it creates, we can't
    // use the SingleEmailMessage class to pass to our SendEmail method,
    // so we create our own
    @testVisible
    private class myEMailMessage
    {
        string[] To;
        Id OrgWideEmailAddressId;
        Id WhatId;
        Id TargetObjectId;
        Id TemplateId;
        Set<Id> DocumentIds;
        Set<Id> AttachmentIds;
        @testVisible
        string Description;
    }
 
    @testVisible
    private static void CreateAccountAndContactRecords()
    {
      system.debug('CreateAccountAndContactRecords');
        if (Contact != null) return;

        // This try {} block either gets the Contact Id of a dummy contact,
        // or creates it if it doesn't exist
        // The Contact.Id is used for the TargetObjectId when we send an email
        try
        {
            Contact = [SELECT Id,
                              Name
                       FROM Contact
                       WHERE Name = 'Task Activity Contact - do not delete'
                       LIMIT 1];
        }
        catch (QueryException cex)
        {
            Account a;
            RecordType rt;
            try
            {
                a = [SELECT Id
                     FROM Account
                     WHERE Name = 'Task Activity Account - do not delete'
                     LIMIT 1];
            }
            catch (QueryException aex)
            {
                try
                {
                    rt = RecordType_u.fetch(Account.class,'US_Account_Hospital');
                }
                catch (QueryException ex)
                {
                    ThrowException('In the Account object, create a Record Type called ' +
                                   '\'US-Healthcare Facility\' - that will fix this problem');
                }
                a = new Account();
                a.RecordTypeId = rt.Id;
                a.Name = 'Task Activity Account - do not delete';
                insert a;
            }

            Contact = new Contact();
            Contact.AccountId = a.Id;
            Contact.LastName = 'Task Activity Contact - do not delete';
            Contact.Email = 'paul.berglund@medtronic.com';
            insert Contact;
        }
    }

    //
    //
    // Notify Owner when CD done methods
    //
    //

    // This method is used to Notify an Agreement Owner when Contract Dev completes
    // a task (controlled by the "Notify Owner when CD done - xxx" fields on the
    // "System Properties" record in the TaskConfigSetting object
    public static void NotifyOwnerwhenCDdone(Map<Id, Task> newMap, Map<Id, Task> oldMap)
    {
        TaskTriggerDispatcher.executedMethods.add('NotifyOwnerwhenCDdone');
        try
        {
            if (oldMap == null) return; // This is not an Update, changing Status to Completed
                                        // should only happen during an Update

            // Get a Map of Agreements based on the WhatId of all the tasks
            Set<Id> relatedTo = new Set<Id>();
            for(Task t : newMap.values())
                relatedTo.add(t.WhatId);

            Agreements = new Map<Id, Apttus__APTS_Agreement__c>(
              [SELECT Id,
                        Owner.Email
                 FROM Apttus__APTS_Agreement__c
                 WHERE Id IN :relatedTo AND
                      RecordTypeId IN :TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.RecordTypeIds AND
                     Owner.Profile.Id IN :TaskConfigSetting.CPQ.Settings.ProfileIds]);

      // None of these tasks are associated with Agreements
            // that we need to worry about, so we can exit
            if(Agreements.size() == 0) return;

            // Get a Map of the Users for all of the task LastModifiedByIds
            // so we can populate the email with the name of the person
            // who completed the task
            Set<Id> lastModifiedByIds = new Set<Id>();
            for(Id key : newMap.keySet())
                lastModifiedByIds.add(newMap.get(key).LastModifiedById);

            Users = new Map<Id, User>([SELECT Id,
                                              Name
                                       FROM User
                                       WHERE Id IN :lastModifiedByIds]);

            // Since mailToSend is static, it will retain the values after each
            // call to the trigger, so we need to clear it for each new batch
            // of tasks
            mailToSend.clear();

            for(Id key : newMap.keySet())
                NotifyOwnerwhenCDdone(newMap.get(key), oldMap.get(key));

            // Update the Contract Dev fields before we send the email
            upsert Agreements.values();

            if (mailToSend.size() > 0)
                SendEmails(0, JSON.serialize(mailToSend));
        }
        catch (Exception ex) {
            ThrowException(ex);
        }
    }

    // This is the method that processes each individual taak for
    // the Notify Owner when CD done process.  It sends an email
    // and creates an activity on the Agreement if the criteria
    // found in the Notify Owner when CD done - xxxx fields
    // of the TaskConfigSetting is met.
    @testVisible
    private static void NotifyOwnerwhenCDdone(Task newTask, Task oldTask)
    {
        if(statusIsCompleted(newTask) &&
           statusHasChanged(newTask, oldTask) &&
           isAgreementTask(newTask) &&
           isValidSubject(newTask))
        {
            CreateAccountAndContactRecords();

            Apttus__APTS_Agreement__c agreement = Agreements.get(newTask.WhatId);

            // Assign the Task information we will need in the Email Template
            agreement.Task_Contract_Dev_CompletedBy__c = newTask.LastModifiedById;
            agreement.Task_Contract_Dev_CompletedOn__c = newTask.LastModifiedDate;
            agreement.Task_Contract_Dev_Comments__c = newTask.Description;

            myEMailMessage mail = new myEMailMessage();
            mail.To = new string[] { agreement.Owner.Email }; // Send it to the Agreement Owner
            mail.WhatId = agreement.Id; // So the activity created is assigned to the Agreement
            mail.TargetObjectId = contact.Id; // Required - activity must be associated with
                                              // a Contact
            mail.TemplateId = TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.TemplateId;

            mailToSend.put(agreement.Id, mail);
        }
        else
            return; // Do nothing if the status isn't Completed or status
                    // hasn't changed or Subject isn't right
                    // or Record Type isn't right
    }

    //
    //
    // Notify CS Agreement Activated
    //
    //

    public static void NotifyCSAgreementActivated(Map<Id, Task> newMap)
    {
        TaskTriggerDispatcher.executedMethods.add('NotifyCSAgreementActivated');
        try
        {
            // Get a Map of Agreements based on the WhatId of all the tasks
            Set<Id> relatedTo = new Set<Id>();
            for(Task t : newMap.values())
                relatedTo.add(t.WhatId);

            Agreements = new Map<Id, Apttus__APTS_Agreement__c>(
              [SELECT Id
                 FROM Apttus__APTS_Agreement__c
                 WHERE Id IN :relatedTo AND
                      RecordTypeId IN :TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.RecordTypeIds AND
                       (Using_CPP_Funds__c = false OR Using_TBD_Dollars__c = false OR T_HLVL_VA__c = false) AND
                       Owner.Profile.Id IN :TaskConfigSetting.CPQ.Settings.ProfileIds]);

            if(Agreements.size() == 0) return; // None of these tasks are associated with
                                               // Agreements
                                               // that we need to worry about, so we can exit

            // Since mailToSend and tasksToCreate are static, they will retain
            // the values after each
            // call to the trigger, so we need to clear it for each new batch
            // of tasks
            mailToSend.clear();
            tasksToCreate.clear();

            for(Id key : newMap.keySet())
                NotifyCSAgreementActivated(newMap.get(key));

            upsert Agreements.values(); // Update the Contract Dev fields before we
                                        // send the email

            if(tasksToCreate.size() > 0)
                insert tasksToCreate;

            system.debug('mailToSend: ' + mailToSend);

            if(mailToSend.size() > 0)
                SendEmails(60*1000, JSON.serialize(mailToSend));
        }
        catch (Exception ex) {
            ThrowException(ex);
        }
    }

    // This is the method that processes each individual task for
    // the Notify CS when Agreement is Activated.  It sends an email
    // and creates an activity on the Agreement
    @testVisible
    private static void NotifyCSAgreementActivated(Task newTask)
    {
        if(newTask.Subject == 'Activated Agreement' &&
           newTask.Status == 'Completed')
        {
            CreateAccountAndContactRecords();

            myEMailMessage mail = new myEMailMessage();
            mail.To = new List<string>(TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.ToAddresses);
            mail.WhatId = newTask.WhatId; // So the activity created is assigned to the Agreement
            mail.TargetObjectId = contact.Id; // Required - activity must be associated with a Contact
            mail.TemplateId = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.TemplateId;
            mail.OrgWideEmailAddressId = TaskConfigSetting.CPQ.Settings.OrgWideEmailAddressId;
            mail.Description = newTask.Description;

            mailToSend.put(newTask.WhatId, mail);

            CreateNewAgreementImplementationTask(newTask.WhatId);
        }
        else
            return; // Do nothing, this isn't an Activated Agreement Task
    }

    @testVisible // Test created
    private static void CreateNewAgreementImplementationTask(Id agreementId)
    {
        Task target = new Task();
        target.Subject = 'Agreement Implementation';
        target.Status = 'New';
        target.OwnerId = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.AssignedToId;
        target.WhatId = agreementId;
        target.Type = 'To-Do';
        target.Description = 'Agreement Implementation Task assigned to Customer Service';
        tasksToCreate.add(target);
    }

    //
    //
    // Notify Agreement Owner when CS completes implementation
    //
    //

    // This method is used to Notify an Agreement Owner when Customer Service completes
    // the implemenation of an Agreement.
    public static void NotifyOwnerAgreementImplemented(
      Map<Id, Task> newMap,
      Map<Id, Task> oldMap)
    {
        TaskTriggerDispatcher.executedMethods.add('NotifyOwnerAgreementImplemented');
        try
        {
            if (oldMap == null) return; // This is not an Update, changing Status to Completed
                                        // should only happen during an Update

            // Get a Map of Agreements and/or Proposals based on the WhatId of all the tasks
            Set<Id> relatedToAgreements = new Set<Id>();
            Set<Id> relatedToProposals = new Set<Id>();
            for(Task t : newMap.values()) {
                if(t.WhatId !=null){
                   if (t.WhatId.getSObjectType() == cpq_u.AGREEMENT_SOBJECT_TYPE) {
                    relatedToAgreements.add(t.WhatId);
                   } else if (t.WhatId.getSObjectType() == cpq_u.PROPOSAL_SOBJECT_TYPE) {
                    relatedToProposals.add(t.WhatId);
                   }
               }
            }
            Agreements = new Map<Id, Apttus__APTS_Agreement__c>(
              [SELECT Id,
                        Owner.Email
                 FROM Apttus__APTS_Agreement__c
                 WHERE Id IN :relatedToAgreements AND
                       RecordTypeId IN :TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.RecordTypeIds AND
                       Owner.Profile.Id IN :TaskConfigSetting.CPQ.Settings.ProfileIds]);

            Proposals = new Map<Id, Apttus_Proposal__Proposal__c>(
                [SELECT Id,
                        Owner.Email
                 FROM Apttus_Proposal__Proposal__c
                 WHERE Id IN :relatedToProposals AND
                       RecordTypeId IN :TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.RecordTypeIds AND
                       Owner.Profile.Id IN :TaskConfigSetting.CPQ.Settings.ProfileIds]);


            if(Agreements.size() == 0 && Proposals.size() == 0) return; // None of these tasks are associated with
                                                                        // Agreements or Proposals
                                                                        // that we need to worry about, so we can exit

            // Get a Map of the Users for all of the task LastModifiedByIds
            // so we can populate the email with the name of the person
            // who completed the task
            Set<Id> lastModifiedByIds = new Set<Id>();
            for(Id key : newMap.keySet())
                lastModifiedByIds.add(newMap.get(key).LastModifiedById);

            Users = new Map<Id, User>([SELECT Id,
                                              Name
                                       FROM User
                                       WHERE Id IN :lastModifiedByIds]);

            // Since mailToSend is static, it will retain the values after each
            // call to the trigger, so we need to clear it for each new batch
            // of tasks
            mailToSend.clear();

            for(Id key : newMap.keySet())
                NotifyOwnerAgreementImplemented(newMap.get(key), oldMap.get(key));

            upsert Agreements.values(); // Update the Contract Dev fields before we
                                        // send the email

            upsert Proposals.values(); // Update the Contract Dev fields before we
                                        // send the email

            if (mailToSend.size() > 0)
                SendEmails(0, JSON.serialize(mailToSend));
        }
        catch (Exception ex)
        {
            ThrowException(ex);
        }
    }

    // This is the method that processes each individual task for
    // the Notify Owner Agreement Implemented process.  It sends an email
    // and creates an activity on the Agreement if the criteria
    // found in the Notify Owner Agreement Implemented - xxxx fields
    // of the TaskConfigSetting is met.
    @testVisible
    private static void NotifyOwnerAgreementImplemented(Task newTask, Task oldTask)
    {
        if(statusIsCompleted(newTask) &&
           statusHasChanged(newTask, oldTask) &&
           (isAgreementTask(newTask) ||
            isProposalTask(newTask)) &&
           (newTask.Subject == 'Agreement Implementation' ||
            newTask.Subject == 'Sent Activation Email to Customer Service' ||
            newTask.Subject == 'Sent to Customer Service'))
        {
            CreateAccountAndContactRecords();

            SObject deal;
            String ownerEmail;
            if (Agreements.containsKey(newTask.WhatId)) {
                deal = (SObject)Agreements.get(newTask.WhatId);
                ownerEmail = Agreements.get(newTask.WhatId).Owner.Email;
            } else if (Proposals.containsKey(newTask.WhatId)) {
                deal = (SObject)Proposals.get(newTask.WhatId);
                ownerEmail = Proposals.get(newTask.WhatId).Owner.Email;
            }

            // Assign the Task information we will need in the Email Template
            deal.put('Task_Contract_Dev_CompletedBy__c', newTask.LastModifiedById);
            deal.put('Task_Contract_Dev_CompletedOn__c', newTask.LastModifiedDate);
            deal.put('Task_Contract_Dev_Comments__c', newTask.Description);

            myEMailMessage mail = new myEMailMessage();
            mail.To = new string[] { ownerEmail }; // Send it to the Deal Owner
            mail.WhatId = deal.Id; // So the activity created is assigned to the Agreement
            mail.TargetObjectId = contact.Id; // Required - activity must be associated with a Contact
            mail.TemplateId = TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.TemplateId;
            mail.AttachmentIds = TaskAttachments.get(newTask.Id);

            mailToSend.put(deal.Id, mail);
        }
        else
            return; // Do nothing if the status isn't Completed or status
                    // hasn't changed or Subject isn't right
                    // or Record Type isn't right
    }

    // In order to effect a delay before emails are sent (which allows other
    // class @future calls to complete before we try to access data on those
    // objects) this SendEmails was created.
    // The millisecondsDelay is the amount of delay you want to wait
    // before sending the emails.
    // Because you can't pass Objects or sObjects to a @future method,
    // I found a trick where you turn them to a JSON string first, pass
    // the data as a string, and then turn them back into the original
    // Object.
    @testVisible
    @future (callout=true)
    private static void SendEmails(integer millisecondDelay, string emails)
    {
        system.debug('SendEmails: ' + emails);

        // First, wait n milliseconds
        if (millisecondDelay > 0 && !test.isRunningTest())
        {
            CPQ_ProxyServices.BasicHttpBinding_IProxy service
                = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
            service.delay(millisecondDelay);
        }

        // Convert JSON string of emails back into a List of myEmailMessage objects
        mailToSend
          = (Map<Id, myEMailMessage>)JSON.deserialize(emails, Map<Id, myEMailMessage>.class);
        system.debug('mailToSend: ' + mailToSend);

        // Getting the Agreements and their Documents
        Agreements = new Map<Id, Apttus__APTS_Agreement__c>(
                   [SELECT Id,
                                 (SELECT Name,
                                         Apttus__URL__c
                                  FROM Apttus__Agreement_Document__r)
                          FROM Apttus__APTS_Agreement__c
                          WHERE Id IN :mailToSend.keySet()]);

        system.debug('Agreements: ' + Agreements.values());

        for (Apttus__APTS_Agreement__c agreement : Agreements.values())
        {
            // Check to see if there are any Agreement Documents
            if (agreement.Apttus__Agreement_Document__r != null &&
                agreement.Apttus__Agreement_Document__r.size() > 0)
            {
                system.debug('Documents: ' + agreement.Apttus__Agreement_Document__r);
                // If there are, then we are going to build an HTML block for the email
                // that contains links to the documents (this is in addition to the files
                // being attached to the email
                string selectedDocsHTML
                  = CRLF + CRLF + '<p>Selected Agreement Document Link(s):<ul>' + CRLF;

                List<Id> agreementDocumentIds = new List<Id>();

                Set<Id> documentIds = new Set<Id>();
                for(Apttus__Agreement_Document__c agreementDocument
                        : agreement.Apttus__Agreement_Document__r)
                {
                  string docName = agreementDocument.Name.left(70);
                    if(agreementDocument.Name != null &&
                     agreementDocument.Apttus__URL__c != null &&
                     mailToSend.get(agreement.Id) != null &&
                       mailToSend.get(agreement.Id).Description != null &&
                       mailToSend.get(agreement.Id).Description.contains(docName))
                    {
                        if(agreementDocument.Apttus__URL__c.toLowerCase().startsWith('http'))
                            // We assume this is an external link to a document
                            selectedDocsHTML += '<li><a href="' +
                                      agreementDocument.Apttus__URL__c +
                                      '" target="_blank">' +
                                      agreementDocument.Name +
                                      '</a></li>' +
                                      CRLF;
                        else
                        {
                            // We assume this is a document found in Salesforce
                            selectedDocsHTML += '<li><a href="' +
                                      URL.getSalesforceBaseUrl().toExternalForm() +
                                      agreementDocument.Apttus__URL__c +
                                      '" target="_blank">' +
                                      agreementDocument.Name + '</a></li>' +
                                      CRLF;

                            // If the Agreement Document resides in Salesforce as a Document
                            // the Document Id will be at the end of the URL field in the
                            // Agreement Document object.  Build a list of Document Ids
                            // from the Ids extracted from the URLs
                            string id = agreementDocument.Apttus__URL__c.trim().right(18);
                            if(Pattern.matches('^[a-zA-Z0-9]*$', id))
                                documentIds.add(id);
                        }
                    }
                }
                selectedDocsHTML += '</ul></p>' + CRLF;

                if(documentIds.size() > 0)
                {
                    mailToSend.get(agreement.Id).DocumentIds = documentIds;
                    agreement.Task_Customer_Service_Selected_Docs__c = selectedDocsHTML;
                }
            }
        }
        update Agreements.values();

        system.debug('Agreements: ' + Agreements.values());

        Set<Id> documentIds = new Set<Id>();
        for(myEMailMessage msg: mailToSend.values())
        {
            if (msg.DocumentIds != null &&
                msg.DocumentIds.size() > 0)
            {
                documentIds.addAll(msg.DocumentIds);
            }
        }

        Map<Id, Document> documents;
        if(documentIds.size() > 0)
        {
            // Fetch the actual Document(s) from Salesforce
            documents = new Map<Id, Document>(
                            [SELECT Id,
                                    ContentType,
                                    Body,
                                    Name,
                                    Type
                             FROM Document
                             WHERE Id IN :documentIds]);
        }

        // So we can retrieve the appropriate Agreement Document(s) for attachments,
        // build a list
        // of Document Ids found in the emails
        Set<Id> attachmentIds = new Set<Id>();
        for(myEMailMessage msg: mailToSend.values())
        {
            if (msg.AttachmentIds != null &&
                msg.AttachmentIds.size() > 0)
            {
                attachmentIds.addAll(msg.AttachmentIds);
            }
        }

        Map<Id, Attachment> attachments;
        if(attachmentIds.size() > 0) {
            // Fetch the actual attachments from Salesforce
            attachments = new Map<Id, Attachment>(
                              [SELECT Id,
                                      Body,
                                      ContentType,
                                      Description,
                                      Name
                               FROM Attachment
                               WHERE Id IN :attachmentIds]);
        }

        system.debug('Documents: ' + documents);
        system.debug('Attachments: ' + attachments);

        // Now we need to convert myEMailMessage objects into SingleEmailMessage objects
        // for the SendEmail method
        List<Messaging.SingleEmailMessage> sems = new List<Messaging.SingleEmailMessage>();

        for(myEMailMessage msg : mailToSend.values())
        {
            Messaging.SingleEmailMessage target
              = new Messaging.SingleEmailMessage();
            target.toAddresses = msg.To;
            target.WhatId = msg.WhatId;
            target.TargetObjectId = msg.TargetObjectId;
            target.TemplateId = msg.TemplateId;
            target.OrgWideEmailAddressId = msg.OrgWideEmailAddressId;
            target.SaveAsActivity = true;

            // If the email has attachments, build EmailFileAttachments from
            // each Document related to the Agreement
            List<Messaging.EmailFileAttachment> efattachments
              = new List<Messaging.EmailFileAttachment>();

            if (documents != null &&
                documents.size() > 0 &&
                msg.DocumentIds != null &&
                msg.DocumentIds.size() > 0)
            {
                for (Id id : msg.DocumentIds)
                {
                    Document document = documents.get(id);
                    if (document != null)
                    {
                        Messaging.EmailFileAttachment attachment
                          = new Messaging.EmailFileAttachment();
                        attachment.ContentType = document.ContentType;
                        attachment.FileName = document.Name + '.' + document.Type;
                        attachment.Inline = false;
                        attachment.Body = document.Body;

                        efattachments.add(attachment);
                    }
                }
            }

            if (attachments != null &&
                attachments.size() > 0 &&
                msg.AttachmentIds != null &&
                msg.AttachmentIds.size() > 0)
            {
                for (Id id : msg.AttachmentIds)
                {
                    Attachment attachment = attachments.get(id);
                    if (attachment != null) {
                        Messaging.EmailFileAttachment efattachment
                          = new Messaging.EmailFileAttachment();
                        efattachment.ContentType = attachment.ContentType;
                        efattachment.FileName = attachment.Name;
                        efattachment.Inline = false;
                        efattachment.Body = attachment.Body;

                        efattachments.add(efattachment);
                    }
                }
            }

            if(efattachments.size() > 0)
                target.FileAttachments = efattachments;

            sems.add(target);
        }

        if(sems.size() > 0 && !Test.isRunningTest()) Messaging.sendEmail(sems);
    }

    //
    //
    // Supporting methods
    //
    //

    // Builds an <a> tag for Id and Name passed in
    @testVisible // Test created
    private static string Link(string id, string name)
    {
        return '<a href="' +
               URL.getSalesforceBaseUrl().toExternalForm() +
               '/' + id + '" target="_blank">' +
               name + '</a>';
    }

    // Builds an <a> tag for the Agreement object
    @testVisible // Test created
    private static string Link(Apttus__APTS_Agreement__c obj)
    {
        return Link(obj.Id, obj.Name);
    }

    // Builds an <a> tag for the Task object
    @testVisible // Test created
    private static string Link(Task obj)
    {
        return Link(obj.Id, obj.Subject);
    }

    // Converts a null string to the string passed in
    @testVisible // Test created
    private static string ISNULL(string str, string val)
    {
        return (str == null) ? val : str;
    }

    // Converst a null datetime to the string pass in
    @testVisible // Test created
    private static string ISNULL(datetime dt, string val)
    {
        return (dt == null) ? val : dt.format();
    }

    // Method to check if status is set to Completed
    @testVisible // Test created
    private static boolean statusIsCompleted(Task t)
    {
        return (t.Status == 'Completed');
    }

    // Method to check if status was changed
    @testVisible // Test created
    private static boolean statusHasChanged(Task n, Task o)
    {
        return (n.Status != o.Status);
    }

    // Method to check if task is associated with an Agreement
    @testVisible // Test created
    private static boolean isAgreementTask(Task t)
    {
        return (Agreements.containsKey(t.WhatId));
    }

    // Method to check if task is associated with a Proposal
    @testVisible // Test created
    private static boolean isProposalTask(Task t)
    {
        return (Proposals.containsKey(t.WhatId));
    }

    // Method to check if the task Subject is one we are allowing
    @testVisible // Test created
    private static boolean isValidSubject(Task t)
    {
        return TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.Subjects != null &&
               (TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.Subjects.contains(t.Subject) ||
                TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.Subjects.contains('All'));
    }

    //
    //
    // This should be called Before an Insert or Update
    // Updates the Task owner with the Agreement owner
    @testVisible
    public static void ChangeChildsOwnerIdOnChangeofTaskOwner(List<Task> newList)
    {
/*
        TaskTriggerDispatcher.executedMethods.add('ChangeChildsOwnerIdOnChangeofTaskOwner');

        //--------------------------List & Map declaration---------------------------------------
        set<Id> setMasterObjectIds = new set<Id>();

        for (Task task : newList) {
            setMasterObjectIds.add(task.WhatId);
        }

        List<Profile> profileList = new List<Profile>([SELECT Id FROM Profile WHERE Name = 'APTTUS - RMS US Account Executive - PM' OR Name = 'APTTUS - RMS US Account Executive - RS' OR Name = 'APTTUS - RMS US Contract Dev']);
        Set<Id> profileIdSet = new Set<Id>();
        for(Profile profile : profileList) {
            profileIdSet.add(profile.Id);
        }

        Map<Id, Apttus__APTS_Agreement__c> agreementMap = new Map<Id, Apttus__APTS_Agreement__c>([SELECT Id, OwnerId, Owner.ProfileId FROM Apttus__APTS_Agreement__c WHERE Id IN :setMasterObjectIds AND Owner.ProfileId IN :profileIdSet]);
        Set<Id> agreementOwnerIdSet = new Set<Id>();
        for(Apttus__APTS_Agreement__c agreement : agreementMap.values()) {
            agreementOwnerIdSet.add(agreement.OwnerId);
        }

        for (Task task : newList) {
            // Check to see if the Task Subect contains one of the search strings
            if (CPQ_Utilities.getSetTaskOwnerToAgreementOwner(task.Subject)) {
                if (agreementMap.containsKey(task.WhatId)) {
                    Apttus__APTS_Agreement__c agreement = agreementMap.get(task.WhatId);
                    if (agreement.OwnerId != null) {
                        task.OwnerId = agreement.OwnerId;
                    }
                }
            }
        }
*/
    }
}