global with sharing class Add_Opp_Prod_Ctlr {
 
 /****************************************************************************************
   * Name    : Add_Opp_Prod_Ctlr
   * Author  : Shawn Clark
   * Date    : 04/29/2015 
   * Purpose : Quick action friendly
 *****************************************************************************************/

    public Opportunity o {get; private set;}
    public String OPG {get; set;}
    public String OPGName {get; private set;}
    public List<Product> products {get; private set;}
    public Boolean isOPGSet {get; private set;}
    public String Type {get; set;}
    public String Competitor {get; set;}
    global String ReturnOpp {get;set;}
    global String flow {get;set;}
    public Boolean isSaveDisabled {get; private set;}
    public String Redirect {get; private set;}

    public Add_Opp_Prod_Ctlr(ApexPages.StandardController stdController)
    {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'Pricebook2Id','Pricebook2.Name','CurrencyIsoCode'});
        }
        this.redirect = 'No';
        IF (ApexPages.currentPage().getParameters().get('flow') == 'Yes')
           {
           this.flow = 'Yes';
           }
           else {
           this.flow = 'No';
           }
        this.o = (Opportunity)stdController.getRecord();
        this.OPG = this.o.Pricebook2Id; 
        this.isOPGSet = false;
        this.ReturnOpp = o.id;
        this.Type = [SELECT Type FROM Opportunity where id = :o.id].Type;
        if(this.OPG != null)
        {
            fetchProducts();
            this.isOPGSet = true; this.isSaveDisabled = false;
            this.OPGName = this.o.Pricebook2.Name;
        }
      }
      public List<SelectOption> getOPGList()
    {
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('--Please Select Opportunity Product Group--', '--Please Select Opportunity Product Group--'));
      for(Pricebook2 opg : [Select Id, Name From Pricebook2 Where Name like '%US:MS%' and IsActive = true and CurrencyIsoCode = 'USD' Order By Name])
      {
        options.add(new SelectOption(opg.Id, opg.Name));
      }
      return options;
    } 
    
     //Retrieve Picklist values for Dealers on Opportunity Object     
      public List<SelectOption> getCompetitorList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = OpportunityLineItem.Competitor__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    } 

      public void fetchProducts()
    {
        products = new List<Product>();
        if(!String.isBlank(OPG))
        {
            List<PricebookEntry> prodList = new List<PricebookEntry>([Select Id, Name, UnitPrice From PricebookEntry Where isActive = true And Pricebook2Id = :OPG And CurrencyIsoCode = 'USD' Order By Name]);
            for(PricebookEntry prod : prodList)
            {
                Product p = new Product(prod.Id, prod.Name, 0, prod.UnitPrice, '', '');
                products.add(p);
            }
        }
      }
      

      public PageReference CreateOLI() 
      {
          isSaveDisabled = true;
          Redirect = 'Yes';
          ReturnOpp = this.o.id;
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();
        for(Integer i=0;i<products.size();i++) {
        
        IF (products[i].Quantity <> 0 && products[i].UnitPrice <> 0 && products[i].Competitor == null){
        Redirect = 'No';

        }
    
            IF (products[i].Quantity <> 0 && products[i].UnitPrice <> 0 && products[i].Competitor != null) {
    
        OpportunityLineItem oppi = new OpportunityLineItem();
        oppi.OpportunityId = o.id; 
        oppi.Competitor__c = products[i].Competitor;
        oppi.Description = products[i].Description;
        oppi.PricebookEntryId = products[i].Id;
        
        //Validation Quantity
        
        IF ((Type == 'At Risk' || Type == 'Write Down') && products[i].Quantity > 0) 
        {
        oppi.Quantity = (products[i].Quantity)*-1;
        }
        ELSE IF (Type == 'New Business' && products[i].Quantity < 0) {
        oppi.Quantity = (products[i].Quantity)*-1;
        }
        Else {
        oppi.Quantity = products[i].Quantity;
        }
        
        
        //Validation Unit Price
        
        IF (products[i].UnitPrice < 0) 
        {
        oppi.UnitPrice = (products[i].UnitPrice)*-1;
        }
        ELSE {
        oppi.UnitPrice = products[i].UnitPrice;
        }
     
        oli.add(oppi);
        }
     }
     
     IF (Redirect == 'No') {
     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'* Competitor is a required field'));
      return null;
      }
      ELSE {
         try {
            insert oli;        
             } catch(DmlException e) {
             ApexPages.addMessages(e);
            System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
     return null;
       }
     }

     
        public class Product
    {
      public String Id {get;set;}
      public String Name {get;set;}
      public Integer Quantity {get;set;}
      public Decimal UnitPrice {get;set;}
      public String Competitor {get;set;}
      public String Description {get;set;}
      public String oppID {get;set;}
      
      public Product(String i, String n, Integer q, Decimal u, String c, String d)
      {
        this.Id = i;
        this.Name = n;
        this.Quantity = q;
        this.UnitPrice = u;
        this.Competitor = c;
        this.Description = d;
      } 
    }       
  }