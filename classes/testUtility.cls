public class testUtility { 
  
/****************************************************************************************
    * Name    : Test Utility
    * Author  : Mike Melcher
    * Date    : 2-7-2012
    * Purpose : Create records for test methods
    *          
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 8/2/2012    MJM          Added Billing Address information to Account to pass new validation rule
    *****************************************************************************************/ 
    
    // Build map of recordtypes
    
    Map<String,Id> rtMap = new Map<String,Id>();
    
    public Account testAccount(string recordtype, string name) {
    
    // Create an Account with the given recordtype and name
    
       for (RecordType rt : [select Id, Name from Recordtype]) {
          rtMap.put(rt.Name, rt.Id);
       }
       
       Account a = new Account();
       
       if (recordtype != null && rtMap.get(recordtype) != null) {
          a.RecordTypeId = rtMap.get(recordtype);
       }
       
       if (name != null) {
          a.Name = name;
       } else {
          a.Name = 'Test';
       }
       a.BillingStreet = 'Test Billing Street';
       a.BillingCity = 'Test Billing City';
       a.BillingState = 'Test';
       a.BillingPostalCode = '12345';
       System.debug('Creating account in testUtility, recordtype: ' + a.recordtypeId + ' ' + recordtype);
       
       Database.insert(a,false);
       
       return a;
       
   
    }
    
    public Contact testContact(string recordtype, Id accountId, string lastname, id mastercontact) {
    
    // Create a contact with the given recordtype, accountId, and lastname
       
       if (rtMap == null) {
          for (RecordType rt : [select Id, Name from Recordtype]) {
             rtMap.put(rt.Name, rt.Id);
          }
       }
       
       Contact c = new Contact();
       
       if (lastname != '') {
          c.LastName = lastName;
       } else {
          c.LastName = 'TestContact';
       }
       
       if (recordtype != '' && rtMap.get(recordtype) != null) {
          c.RecordTypeId = rtMap.get(recordtype);
       }
       if (mastercontact != null) {
          c.Master_Contact_Record__c = mastercontact;
       }
       
       c.AccountId = accountId;
       
       Database.insert(c,false);
       
       return c;
       

    
    }
 }