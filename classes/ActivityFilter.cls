public with sharing class ActivityFilter {
    public string activityContext{get;set;}
    public string parentId{get;set;}
    public string activityStatus{get;set;}
    public string activityGBU{get;set;}
    public string activityFranchise{get;set;}
    public string activityType{get;set;}
    public boolean myActivities{get;set;}
    public boolean showEvents{get;set;}
    public boolean showTasks{get;set;}
    public string franchiseLike{get;set;}
    public string gbuLike{get;set;}
    public string statusFilter{get;set;}
    public string assignedToId{get;set;}
    public string recName{get;set;}
    public string country{get;set;}
    
    
    public Task taskParam{get;set;}
    public ActivityFilter()
    {
        activityContext = ApexPages.currentPage().getParameters().get('activitycontext');
        activityType = ApexPages.currentPage().getParameters().get('activitytype');
        parentId = ApexPages.currentPage().getParameters().get('parentid');
        recName = ApexPages.currentPage().getParameters().get('name');
        taskParam = new Task();
        showEvents = false;
        showEvents = false;
        assignedToId = '%';
        myActivities = true;
        statusFilter = '%';
        activityType = 'Both';
        
        for(User u : [select Id, Business_Unit__c, Region__c, Franchise__c
                        from User 
                       where Id = :UserInfo.getUserId()])
        {
           taskParam.Business_Unit__c = u.Business_Unit__c;
           taskParam.Franchise__c = u.Franchise__c;
           country = u.Region__c;
           
        }
        
        if(parentId !=null)
          search();
    }
    
    public List<SelectOption> getActivityTypeOptions() {
            //Returns the activity Type options
            List<SelectOption> options = new List<SelectOption>(); 
                       
            options.add(new SelectOption('Both', 'Both'));
            options.add(new SelectOption('Task', 'Task'));
            options.add(new SelectOption('Event', 'Event'));
            
            return options;
    }
    
    public List<SelectOption> getStatus() {
            //Returns the status options for both objects
            List<SelectOption> options = new List<SelectOption>(); 
            options.add(new SelectOption('%', '--All--'));           
            Schema.DescribeFieldResult F = Task.Status.getDescribe();
            for(Schema.Picklistentry p : F.getPicklistValues())
                options.add(new SelectOption(p.getValue(), p.getLabel()));
            
            return options;
    }
    
    public list<Event> getEvents()
    {
        set<id> whatIds = new set<Id>();
        set<id> whoIds = new set<Id>();
        
        if(parentId.startsWith('001'))//Account
        {
            for(Opportunity o : [select Id from Opportunity where accountId = :parentId])
              whatIds.add(o.Id);
              
            for(Contact c : [select Id from Contact where accountId = :parentId])
              whoIds.add(c.Id);
              
            whatIds.add(parentId);
        }
        
        if(parentId.startsWith('006'))//Opportunity
        {
            for(Opportunity o : [select Id, accountId from Opportunity where Id = :parentId])
              whatIds.add(o.Id);
              
             whatIds.add(parentId);
        }
        
        if(parentId.startsWith('003'))//Contact
        {
            for(Contact c : [select accountId from Contact where accountId = :parentId])
              whatIds.add(c.Id);
              
            whoIds.add(parentId);
        }
        
        List<Event> eventList = new list<Event>();
        map<Id, Id> userMap = new map<Id,Id>();
        set<id> userSet = new set<Id>();
        
        if(myActivities)
            for(Event e : [Select e.Subject
                         , e.StartDateTime
                         , e.Owner.Name
                         , e.Id
                         , e.Franchise__c
                         , e.Business_Unit__c
                         , e.ActivityDateTime 
                         , who.Name
                         , what.Name
                         , OwnerId
                      From Event e
                    where ((WhatId in :whatIds //and WhoId = null)
                            or WhoId in :whoIds ))//and WhatId = null) )
                      and (Franchise__c like :franchiseLike or Franchise__c like :taskParam.Franchise__c)
                      and (Business_Unit__c like :gbuLike or Business_Unit__c like :taskParam.Business_Unit__c)
                      and OwnerId = :UserInfo.getUserId()])
             {
                userSet.add(e.ownerId);
                eventList.add(e);
             }         
         else
            for(Event e : [Select e.Subject
                         , e.StartDateTime
                         , e.Owner.Name
                         , e.Id
                         , e.Franchise__c
                         , e.Business_Unit__c
                         , e.ActivityDateTime 
                         , who.Name
                         , what.Name
                         , OwnerId
                      From Event e
                    where ((WhatId = :whatIds //and WhoId = null)
                            or WhoId in :whoIds ))//and WhatId = null) )
                      and (Franchise__c like :franchiseLike or Franchise__c like :taskParam.Franchise__c)
                      and (Business_Unit__c like :gbuLike or Business_Unit__c like :taskParam.Business_Unit__c)])
             {
                userSet.add(e.ownerId);
                eventList.add(e);
             }
                      
                      
          
        
        List<Event> returnList = new list<Event>();
        if(country != null && country != '')
        {
            for(User u : [Select Id from User where Id in :userSet and Region__c like :country])
            {
                userMap.put(u.Id, u.Id);
            }
            
            for(Event e : eventList)
            {
                if(usermap.get(e.ownerId) != null)
                 returnList.add(e);
            }
             
            return returnList;
        }
        else
           return eventList;
    }
    
    public list<Task> getTasks()
    {
        set<id> whatIds = new set<Id>();
        set<id> whoIds = new set<Id>();
        
        if(parentId.startsWith('001'))//Account
        {
            for(Opportunity o : [select Id from Opportunity where accountId = :parentId])
              whatIds.add(o.Id);
              
            for(Contact c : [select Id from Contact where accountId = :parentId])
              whoIds.add(c.Id);
              
            whatIds.add(parentId);
        }
        
        if(parentId.startsWith('006'))//Opportunity
        {
            for(Opportunity o : [select Id, accountId from Opportunity where Id = :parentId])
              whatIds.add(o.Id);
              
             whatIds.add(parentId);
        }
        
        if(parentId.startsWith('003'))//Contact
        {
            for(Contact c : [select accountId from Contact where Id = :parentId])
              whoIds.add(c.Id);
              
            whoIds.add(parentId);
        }
        
        List<Task> taskList = new list<Task>();
        map<Id, Id> userMap = new map<Id,Id>();
        set<id> userSet = new set<Id>();
        
        if(myActivities)
            for(Task T : [Select t.Subject
                         , t.Status
                         , t.Owner.Name
                         , t.Franchise__c
                         , t.Business_Unit__c
                         , t.ActivityDate 
                         , who.Name
                         , what.Name
                         , OwnerId
                      From Task t
                     where ((WhatId in :whatIds //and WhoId = null)
                            or WhoId in :whoIds ))//and WhatId = null) )
                      and (Franchise__c like :franchiseLike or Franchise__c like :taskParam.Franchise__c)
                      and (Business_Unit__c like :gbuLike or Business_Unit__c like :taskParam.Business_Unit__c)
                      and status like :statusFilter
                      //and Owner.Region__c like :country
                      and OwnerId = :UserInfo.getUserId()] )
             {
                userSet.add(T.ownerId);
                taskList.add(T);
             }
         else
            for(Task T : [Select t.Subject
                         , t.Status
                         , t.Owner.Name
                         , t.Franchise__c
                         , t.Business_Unit__c
                         , t.ActivityDate 
                         , t.ownerId
                         , who.Name
                         , what.Name
                      From Task t
                     where ((WhatId in :whatIds //and WhoId = null)
                            or WhoId in :whoIds ))//and WhatId = null) )
                      and (Franchise__c like :franchiseLike or Franchise__c like :taskParam.Franchise__c)
                      and (Business_Unit__c like :gbuLike or Business_Unit__c like :taskParam.Business_Unit__c)
                      //and Owner.Region__c like :country
                      and status like :statusFilter])
             {
                userSet.add(T.ownerId);
                taskList.add(T);
             }
            
        //return taskList;
        
        List<Task> returnList = new list<Task>();
        if(country != null && country != '')
        {
            for(User u : [Select Id from User where Id in :userSet and Region__c like :country])
            {
                userMap.put(u.Id, u.Id);
            }
            
            for(Task t : taskList)
            {
                if(usermap.get(t.ownerId) != null)
                 returnList.add(t);
            }
             
            return returnList;
        }
        else
           return taskList;
        
    }
    
    public pageReference search()
    {
        if(activityType == 'Event' || activityType == 'Both')
          showEvents = true;
        else
          showEvents = false;
          
        if(activityType == 'Task' || activityType == 'Both')
          showTasks = true;
        else
          showTasks = false;
          
        
        franchiseLike = '%'+(taskParam.Franchise__c== null ?'':taskParam.Franchise__c)+'%';
        gbuLike = '%'+(taskParam.Business_Unit__c == null ?'':taskParam.Business_Unit__c)+'%';
        if(myActivities)
          assignedToId = UserInfo.getUserId();
        else
           assignedToId = '%';
        return null;
    }
    
    public pageReference cancel()
    {
        return new PageReference('/'+parentId);
    }
    
    public List<SelectOption>  getGBU()
    {
        List<SelectOption> options = new List<SelectOption>();
        map<string, string> gbuMap = new map<string,string>();
        options.add(new SelectOption('','--None--'));
        if([Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name == 'EU - One Covidien'){
           options.add(new SelectOption('Corporate','Corporate'));
           options.add(new SelectOption('Data Integration','Data Integration'));
           options.add(new SelectOption('One Covidien','One Covidien'));
           options.add(new SelectOption('VT','VT'));
           options.add(new SelectOption('MCS','MCS'));
           options.add(new SelectOption('AST','AST'));
           options.add(new SelectOption('Hernia','Hernia'));
           options.add(new SelectOption('GSP','GSP'));
           options.add(new SelectOption('S2','S2'));
        }
        else{
        for (GBU_Franchises__c gbu : GBU_Franchises__c.getAll().values())
        {
             if(gbuMap.get(gbu.GBU__c) == null)   
                options.add(new SelectOption(gbu.GBU__c,gbu.GBU__c));
                
             gbuMap.put(gbu.GBU__c,gbu.GBU__c);
             
        }
        }
            
        return options;
        
    }
    
    /** Filters Franchises based on the selected GBU filter
        using a custom setting to determine the relationship
     **/
    public List<SelectOption>  getFranchise()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--None--'));
        for (GBU_Franchises__c gbu : GBU_Franchises__c.getAll().values())
        {
            if(taskParam.Business_Unit__c == gbu.GBU__c )   
            {
                if(gbu.Franchise__c!=null && gbu.Franchise__c!='')
                options.add(new SelectOption(gbu.Franchise__c,gbu.Franchise__c));
            }                 
        }
        return options;
        
        
    }
    
    public List<SelectOption> getCountries()
    {
      List<SelectOption> options = new List<SelectOption>();
       
       Schema.DescribeFieldResult fieldResult = User.Region__c.getDescribe();
       
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       options.add(new SelectOption('','--None--'));
            
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    

}