@isTest
public class cpqRecordType_TestSetup
{
    public static RecordType getRecordType (String sObjectType)
    {
    	Map<Id, RecordType> rts = RecordType_u.fetch(sObject_u.getSObjectType(sObjectType));
        return rts.values().get(0);
    }

    public static RecordType getRecordTypeLocked (String sObjectType) {
        // Used by CPQ Proposals and Agreements
        return [SELECT Id,
                       Name,
                       DeveloperName,
                       SObjectType
                FROM RecordType
                WHERE SObjectType = :sObjectType AND
                      DeveloperName LIKE '%Locked' LIMIT 1];
    }
}