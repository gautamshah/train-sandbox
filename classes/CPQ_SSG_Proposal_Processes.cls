public with sharing class CPQ_SSG_Proposal_Processes {
	public static final String LIGASURE_CATEGORY = 'Vessel Sealing';
	public static final String ELECTROSURGERY_CATEGORY = 'ElectroSurgery';

	public static void RFACalculateUsage(List<Apttus_Config2__ProductConfiguration__c> inputConfigs, List<Apttus_Config2__ProductConfiguration__c> oldConfigs) {
		Map<Id,Apttus_Config2__ProductConfiguration__c> finalizedConfigMap = new Map<Id,Apttus_Config2__ProductConfiguration__c>();
		for (Integer index = 0; index < inputConfigs.size(); index++) {
			Apttus_Config2__ProductConfiguration__c oldConfig = oldConfigs[index];
			Apttus_Config2__ProductConfiguration__c newConfig = inputConfigs[index];
			if (oldConfig.Apttus_Config2__Status__c != 'Finalized' && newconfig.Apttus_Config2__Status__c == 'Finalized' && newConfig.Apttus_Config2__BusinessObjectType__c == 'Proposal') {
				finalizedConfigMap.put(newConfig.Apttus_Config2__BusinessObjectId__c, newConfig);
			}
		}

		if (!finalizedConfigMap.isEmpty()) {
			// Get Proposals that are not for the RFA record type and remove them from  the list to consider.
			List<Apttus_Proposal__Proposal__c> nonRFAProposals = [Select Id From Apttus_Proposal__Proposal__c Where Id in :finalizedConfigMap.keySet() And RecordType.DeveloperName != 'Royalty_Free_Agreement' And RecordType.DeveloperName != 'Royalty_Free_Agreement_Locked'];
			for (Apttus_Proposal__Proposal__c proposal: nonRFAProposals) {
				finalizedConfigMap.remove(proposal.Id);
			}

			if (!finalizedConfigMap.isEmpty()) {
				// Get Product SKUs for line items 
				Map<Id,Apttus_Config2__ProductConfiguration__c> configIdMap = new Map<Id,Apttus_Config2__ProductConfiguration__c>();
				for (Apttus_Config2__ProductConfiguration__c config: finalizedConfigMap.values()) {
					configIdMap.put(config.Id, config);
				}
				List<Apttus_Config2__ProductConfiguration__c> configsWithConsumableLineItems = [Select Id, Apttus_Config2__BusinessObjectId__c, (Select Id, Product_Code__c, Apttus_Config2__Quantity__c From Apttus_Config2__LineItems__r  Where Apttus_Config2__ChargeType__c = 'Consumable') From Apttus_Config2__ProductConfiguration__c Where Id in :configIdMap.keySet()];
				List<Apttus_Config2__ProductConfiguration__c> configsWithHardwareLineItems = [Select Id, Apttus_Config2__BusinessObjectId__c, (Select Id, Apttus_Config2__ProductId__r.Category__c, Apttus_Config2__Quantity__c From Apttus_Config2__LineItems__r Where Apttus_Config2__ChargeType__c = 'Hardware') From Apttus_Config2__ProductConfiguration__c Where Id in :configIdMap.keySet()];

				Set<String> productCodes = new Set<String>();
				Map<Id,Set<String>> configProductCodesMap = new Map<Id,Set<String>>();
				Map<Id,Id> configToProposalIdMap = new Map<Id,Id>();
				Map<Id,Id> proposalToConfigIdMap = new Map<Id,Id>();
				for (Apttus_Config2__ProductConfiguration__c config: configsWithConsumableLineItems) {
					Set<String> configProductCodes = new Set<String>();
					configProductCodesMap.put(config.Id,configProductCodes);
					for (Apttus_Config2__LineItem__c lineItem: config.Apttus_Config2__LineItems__r) {
						configProductCodes.add(lineItem.Product_Code__c);
					}
					productCodes.addAll(configProductCodes);
					configToProposalIdMap.put(config.Id, config.Apttus_Config2__BusinessObjectId__c);
					proposalToConfigIdMap.put(config.Apttus_Config2__BusinessObjectId__c, config.Id);
				}

				Map<Id,Integer> configElectroHardwareQuantityMap = new Map<Id,Integer>();
				Map<Id,Integer> configLigasureHardwareQuantityMap = new Map<Id,Integer>();
				for (Apttus_Config2__ProductConfiguration__c config: configsWithHardwareLineItems) {
					Integer electroHardwareQuantity = 0;
					Integer ligasureHardwareQuantity = 0;
					for (Apttus_Config2__LineItem__c lineItem: config.Apttus_Config2__LineItems__r) {
						if (lineItem.Apttus_Config2__ProductId__r.Category__c == ELECTROSURGERY_CATEGORY) {
							electroHardwareQuantity += Integer.valueOf(lineItem.Apttus_Config2__Quantity__c);
						} else if (lineItem.Apttus_Config2__ProductId__r.Category__c == LIGASURE_CATEGORY) {
							ligasureHardwareQuantity += Integer.valueOf(lineItem.Apttus_Config2__Quantity__c);
						}
						
					}
					configElectroHardwareQuantityMap.put(config.Id, electroHardwareQuantity);
					configLigasureHardwareQuantityMap.put(config.Id, ligasureHardwareQuantity);
				}

				// Get all account Ids for Account and participating facilities
				List<Apttus_Proposal__Proposal__c> proposalParticipatingFacilities = [Select Id, Apttus_Proposal__Account__c, (Select Id, Account__c From Participating_Facilities__r) From Apttus_Proposal__Proposal__c Where Id in :finalizedConfigMap.keySet()];
				Set<Id> accountIds = new Set<Id>();
				Map<Id,Set<Id>> configAccountIdMap = new Map<Id,Set<Id>>();
				Map<Id,Apttus_Proposal__Proposal__c> configProposalMap = new Map<Id,Apttus_Proposal__Proposal__c>();
				for (Apttus_Proposal__Proposal__c proposal: proposalParticipatingFacilities) {
					Set<Id> proposalAccountIds = new Set<Id>();
					configAccountIdMap.put(proposalToConfigIdMap.get(proposal.Id), proposalAccountIds);
					configProposalMap.put(proposalToConfigIdMap.get(proposal.Id), proposal);
					for (Participating_Facility__c pf: proposal.Participating_Facilities__r) {
						proposalAccountIds.add(pf.Account__c);
					}
					proposalAccountIds.add(proposal.Apttus_Proposal__Account__c);
					accountIds.addAll(proposalAccountIds);
				}

				// Get product categories
				List<Product2> products = [Select Id, ProductCode, Category__c From Product2 Where ProductCode in :productCodes];
				Map<String,String> productCodeCategoryMap = new Map<String,String>();
				for (Product2 product: products) {
					productCodeCategoryMap.put(product.ProductCode, product.Category__c);
				}

				// Get sales history by account ids and product codes
				List<AggregateResult> salesHistories = [SELECT Sum(Total_Current_Year_Eaches__c) Total_Eaches, Sum(Total_Current_Year_Sales__c) Total_Sales, Customer__c, SKU__c FROM Sales_History__c where Customer__c in :accountIds And SKU__c in :productCodes Group By SKU__c, Customer__c];
				Map<String,AggregateResult> salesHistoryMap = new Map<String,AggregateResult>();
				for (AggregateResult ar: salesHistories) {
					String key = ar.get('Customer__c') + '_' + ar.get('SKU__c');
					salesHistoryMap.put(key, ar);
				}
				// Loop through each config.
				for (Id configId: configAccountIdMap.keySet()) {
					Integer electroEaches = 0;
					Integer ligasureEaches = 0;
					Decimal electroSales = 0.0;
					Decimal ligasureSales = 0.0;
					Apttus_Proposal__Proposal__c proposal = configProposalMap.get(configId);
					// For each config, loop through the accounts
					for (Id accountId: configAccountIdMap.get(configId)) {
						// For each account, loop through the products
						for (String productCode: configProductCodesMap.get(configId)) {
							// Build a key, find sales history results, and sum up totals
							String key = accountId + '_' + productCode;
							AggregateResult ar = salesHistoryMap.get(key);
							if (ar != null) {
								String category = productCodeCategoryMap.get((String)ar.get('SKU__c'));
								if (category == ELECTROSURGERY_CATEGORY) {
									Integer eaches = Integer.ValueOf((Decimal)ar.get('Total_Eaches'));
									Decimal sales = (Decimal)ar.get('Total_Sales');
									if (eaches != null) {
										electroEaches += eaches;
									}
									if (sales != null) {
										electroSales += sales;
									}
								} else if (category == LIGASURE_CATEGORY) {
									Integer eaches = Integer.ValueOf((Decimal)ar.get('Total_Eaches'));
									Decimal sales = (Decimal)ar.get('Total_Sales');
									if (eaches != null) {
										ligasureEaches += eaches;
									}
									if (sales != null) {
										ligasureSales += sales;
									}
								}
							}
						}
					}

					Integer electroHardwareQuantity = configElectroHardwareQuantityMap.get(configId);
					Integer ligasureHardwareQuantity = configLigasureHardwareQuantityMap.get(configId);
					Boolean hasElectroHardware = electroHardwareQuantity != null && electroHardwareQuantity > 0;
					Boolean hasLigasureHardware = ligasureHardwareQuantity != null && ligasureHardwareQuantity > 0;

					proposal.Multiple_COTs__c = hasElectroHardware && hasLigasureHardware;
					proposal.Electrosurgery_Eaches__c = electroEaches;
					proposal.Ligasure_Eaches__c = ligasureEaches;
					proposal.Electrosurgery_Sales__c = electroSales;
					proposal.Ligasure_Sales__c = ligasureSales;
					proposal.Electrosurgery_Incremental_Quantity__c = configElectroHardwareQuantityMap.get(configId);
					proposal.Ligasure_Incremental_Quantity__c = configLigasureHardwareQuantityMap.get(configId);
				}

				update configProposalMap.values();
			}
		}
	}

	public static void SetProposalApprovalTemplateFields(List<Apttus_Proposal__Proposal__c> inputProposals, Map<ID, Apttus_Proposal__Proposal__c> oldMap) {
		// Get all SSG Record Types
		Set<Id> recordTypeIds = new Set<Id>();
		Map<Id,RecordType> recordTypes = CPQ_ProposalProcesses.getProposalRecordTypesByIdMap();
		for (RecordType rt: recordTypes.values()) {
			if (rt.DeveloperName == 'Agreement_Proposal' || rt.DeveloperName == 'Energy_Hardware' || rt.DeveloperName == 'Hardware_or_Product_Quote' ||
			    rt.DeveloperName == 'New_Product_LOC' || rt.DeveloperName == 'Promotional_LOC' || rt.DeveloperName == 'Royalty_Free_Agreement') {
				recordTypeIds.add(rt.Id);
			}
		}

		// Set primary contact and valid until date fields on all SSG  proposals.
		for (Apttus_Proposal__Proposal__c prop: inputProposals) {
            if (recordTypeIds.contains(prop.RecordTypeId)) {
            	if (prop.Primary_Contact2__c != null) {
					prop.Apttus_Proposal__Primary_Contact__c = prop.Primary_Contact2__c;
				}
				if (prop.Apttus_Proposal__Proposal_Expiration_Date__c != null) {
					prop.Apttus_Proposal__Valid_Until_Date__c = prop.Apttus_Proposal__Proposal_Expiration_Date__c;
				}
			}
		}
	}

	public static void SetProposalLineItemApprovalTemplateFields(List<Apttus_Proposal__Proposal_Line_Item__c> inputProposalLineItems, Map<ID, Apttus_Proposal__Proposal_Line_Item__c> oldMap) {
		// Get all proposalIds
		Set<Id> proposalIds = new Set<Id>();
		for (Apttus_Proposal__Proposal_Line_Item__c pli: inputProposalLineItems) {
			proposalIds.add(pli.Apttus_Proposal__Proposal__c);
		}

		// Get all related proposals with SSG record types
		Map<Id,Apttus_Proposal__Proposal__c> proposalMap = new Map<Id,Apttus_Proposal__Proposal__c>([Select Id From Apttus_Proposal__Proposal__c Where Id in :proposalIds And RecordType.DeveloperName in ('Agreement_Proposal', 'Energy_Hardware', 'Hardware_or_Product_Quote', 'New_Product_LOC', 'Promotional_LOC', 'Royalty_Free_Agreement')]);

		// Set quantity and unit net price for all SSG record types
		for (Apttus_Proposal__Proposal_Line_Item__c pli: inputProposalLineItems) {
            if (proposalMap.containsKey(pli.Apttus_Proposal__Proposal__c)) {
            	if (pli.Unit_Net_Price__c != null) {
					pli.Apttus_Proposal__Unit_Price__c = pli.Unit_Net_Price__c;
				}
				if (pli.Apttus_QPConfig__Quantity2__c != null) {
					pli.Apttus_Proposal__Quantity__c = pli.Apttus_QPConfig__Quantity2__c;
				}
			}
		}
	}
}