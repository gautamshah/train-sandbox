public class cpqErrorLog_c extends sObject_c
{
	//
	// cpqErrorLog_c
	//
    public static CPQ_Error_Log__c createErrorLogEntry (
    	String operation,
    	String logType,
    	String errorMessage,
    	String additional,
    	String request)
    {
        CPQ_Error_Log__c error =
        	new CPQ_Error_Log__c(
        			Operation__c = operation,
        			Log_Type__c = logType,
        			Error_Message__c = errorMessage,
        			Additional_Info__c = additional,
        			Request_Body__c = request);
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.allowFieldTruncation = true;
        error.setOptions(dml);
        return error;
    }
}