// TODO: Should use a map to pass field values instead of method variables. Keeps methods cleaner.
@isTest
public class cpqAgreement_cTest
{
    @testVisible
    private static Apttus__APTS_Agreement__c create(integer ndx)
    {
        Apttus__APTS_Agreement__c sobj = new Apttus__APTS_Agreement__c();
        sobj.Name = 'Test Agreement ' + string.valueOf(ndx).leftPad(10);

        return sobj;
    }

    @testVisible
    private static Apttus__APTS_Agreement__c create(
        string name,
        Id ownerId,
        Id opportunityId,
        Id accountId,
        Id recordType,
        string scrubPOType,
        Id primaryContactId,
        string ssgProductUsage,
        string approvalStatus,
        string status)
    {
        return create(
            0,
            name,
            ownerId,
            opportunityId,
            accountId,
            recordType,
            scrubPOType,
            primaryContactId,
            ssgProductUsage,
            approvalStatus,
            null,
            status,
            null,
            null);
    }

    @testVisible
    private static Apttus__APTS_Agreement__c create(
        integer ndx,
        string name,
        Id ownerId,
        Id opportunityId,
        Id accountId,
        Id recordType,
        string scrubPOType,
        Id primaryContactId,
        string ssgProductUsage,
        string approvalStatus,
        string statusCategory,
        string status,
        string po,
        string notes)
    {
        Apttus__APTS_Agreement__c sobj = create(ndx);
        sobj.Name = string.isBlank(name) ? sobj.Name : name; // Leave it set or override
        sobj.OwnerId = ownerId;
        sobj.Apttus__Related_Opportunity__c = opportunityId;
        sobj.Apttus__Account__c = accountId;
        sobj.RecordTypeId = recordType;
        sobj.Scrub_PO_Type__c = string.isBlank(scrubPOType) ? null : scrubPOType;
        sobj.Primary_Contact2__c = primaryContactId;
        sobj.SSG_Product_Usage__c = string.isBlank(ssgProductUsage) ? null : ssgProductUsage;
        sobj.Apttus_Approval__Approval_Status__c = string.isBlank(approvalStatus) ? null : approvalStatus;
        sobj.Apttus__Status_Category__c = string.isBlank(statusCategory) ? 'In Signatures' : statusCategory;
        sobj.Apttus__Status__c = string.isBlank(status) ? 'Fully Signed' : status;
        sobj.PO__c = string.isBlank(po) ? '12345' : po;
        sobj.Notes__c = string.isBlank(notes) ? 'Hello World' : notes;
        return sobj;
    }

    public static Apttus__APTS_Agreement__c addSSGvalues(Apttus__APTS_Agreement__c a)
    {
        a.SSG_Tri_Staple_Core_Reloads__c = 1;
        a.SSG_Tri_Staple_Specialty_Reloads__c = 1;
        a.SSG_Legacy_Reloads__c = 98;
        a.SSG_Stocking_Order_PO__c = '12345';
        a.SSG_Stocking_Order_Confirmation__c = '12345';
        a.SSG_Stocking_Order_Type__c = 'Test';

        return a;
    }
}