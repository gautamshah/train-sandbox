@isTest
public class Test_CPQ_Extension_ProductConfiguration {

    @isTest
    public static void displayPromotions() {

        // Try without document
        CPQ_Extension_ProductConfiguration e = new CPQ_Extension_ProductConfiguration();
        ApexPages.PageReference page = e.displayPromotions();
        system.assertEquals(page, null);

        // Insert Document
        Document d = new Document();
        d.FolderId = cpqUser_c.CurrentUser.Id;
        d.Name = 'US RMS Promotions';
        d.Body = (Blob)EncodingUtil.base64Decode('JVBERi0xLjUNCiW1tbW1DQoxIDAgb2JqDQo8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMiAwIFIvTGFuZyhlbi1VUykgL1N0cnVjdFRyZWVSb290IDEwIDAgUi9NYXJrSW5mbzw8L01hcmtlZCB0cnVlPj4+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sgMyAwIFJdID4+DQplbmRvYmoNCjMgMCBvYmoNCjw');
        d.DeveloperName = 'US_RMS_Promotions';
        d.ContentType = 'application/pdf';
        insert d;

        // Try with document
        e = new CPQ_Extension_ProductConfiguration();
        page = e.displayPromotions();
        system.assertNotEquals(page, null);

    }

    @isTest
    public static void displayPromotionsSSG() {

        // Try without document
        CPQ_Extension_ProductConfiguration e = new CPQ_Extension_ProductConfiguration();
        ApexPages.PageReference page = e.displayPromotionsSSG();
        system.assertEquals(page, null);

        // Insert Document
        Document d = new Document();
        d.FolderId = cpqUser_c.CurrentUser.Id;
        d.Name = 'US SSG Promotions';
        d.Body = (Blob)EncodingUtil.base64Decode('JVBERi0xLjUNCiW1tbW1DQoxIDAgb2JqDQo8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMiAwIFIvTGFuZyhlbi1VUykgL1N0cnVjdFRyZWVSb290IDEwIDAgUi9NYXJrSW5mbzw8L01hcmtlZCB0cnVlPj4+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sgMyAwIFJdID4+DQplbmRvYmoNCjMgMCBvYmoNCjw');
        d.DeveloperName = 'US_SSG_Promotions';
        d.ContentType = 'application/pdf';
        insert d;

        // Try with document
        e = new CPQ_Extension_ProductConfiguration();
        page = e.displayPromotionsSSG();
        system.assertNotEquals(page, null);

    }

}