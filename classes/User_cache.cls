public class User_cache extends sObject_cache
{
	public static User_cache get() { return cache; }
	
	private static User_cache cache
	{
		get
		{
			if (cache == null)
				cache = new User_cache();
				
			return cache;
		}
		
		private set;
	}
	
	private User_cache()
	{
		super(User.sObjectType, User.field.Id);
		this.addIndex(User.field.OrganizationName__c);
		this.addIndex(User.field.SalesOrganizationLevel__c);
	}
}