/****************************************************************************************
 * Name    : Test_AssetTriggerHandler 
 * Author  : Bill Shan
 * Date    : 17/12/2012 
 * Purpose : Test Asset trigger and trigger handler
 * Dependencies: AssetTrigger Trigger
 *             , Asset_TriggerHandler
 *             , Asset Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
@isTest
private class Test_AssetTriggerHandler {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        createTestUser();
        createTestData();
        testingMethods();
    }
    
     //Test methods in AccountStatement controller
    private static void testingMethods()
    {
        //Run teset as API User
        Test.startTest();
        System.runAs(apiUser){
            Asset device = [SELECT id FROM Asset where Asset_External_ID__c <> null LIMIT 1];
            delete device;
        }
        
        //Run test as EMEA Emerging Market User
        System.runAs(emUser){
            
            try{
                Asset device = [SELECT id FROM Asset where Asset_External_ID__c <> null LIMIT 1];
                delete device;
            }
            catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains(Label.DeleteAssetError) ? true : false;
              //  System.AssertEquals(expectedExceptionThrown, true);
            }
            
        }
        
        Test.stopTest();
    }

    //Create Testing User
    private static User apiUser;
    private static User emUser; 
    private static void createTestUser(){
        
        ID apiProfileID = [SELECT Id FROM Profile WHERE Name = 'API Data Loader'].ID;
        ID emProfileID = [SELECT Id FROM Profile WHERE Name = 'EM - All'].ID;
        
        apiUser = new User(username='api20121217@test.com',
                            alias = 'apitest',
                            email='api20121217@test.com', 
                            emailencodingkey='UTF-8',
                            lastname='Covidien',
                            languagelocalekey='en_US',
                            localesidkey='en_US',                                             
                            profileid = apiProfileID,
                            timezonesidkey='Europe/Berlin',
                            Region__c = 'EM',
                            Business_Unit__c = 'All'
                            );
        
        emUser = new User(username='EM20121217@test.com',
                            alias = 'apitest',
                            email='EM20121217@test.com', 
                            emailencodingkey='UTF-8',
                            lastname='Covidien',
                            languagelocalekey='en_US',
                            localesidkey='en_US',                                             
                            profileid = emProfileID,
                            timezonesidkey='Europe/Berlin',
                            Region__c = 'EM',
                            Business_Unit__c = 'All'
                            );
        insert apiUser;
        insert emUser;
    }
    
    // Create Test Data
    private static void createTestData()
    {
        testUtility tu = new testUtility();
        Account a = tu.testAccount('EM-Healthcare Facility','TestAcct');
        
        Asset item1 = new Asset(AccountId=a.Id,Name='Asset 1 - in test class for asset trigger testing', 
                               Asset_External_ID__c = 'Asset - in test class for asset trigger testing');
        Asset item2 = new Asset(AccountId=a.Id,Name='Asset 2 - in test class for asset trigger testing', 
                               Asset_External_ID__c = 'Asset - in test class for asset trigger testing');  
        insert item1;
        insert item2;       
    }
}