@isTest() 
public class Schedule_EmptyRecycleBinOfObjectsTest {
/****************************************************************************************
    * Name    : Schedule_EmptyRecycleBinOfObjectsTest
    * Author  : Brajmohan Sharma
    * Date    : 17-Feb-2017
    * Purpose : Performs a deletion job to empty the recycle bin of the given object.
    * 
    * Dependancies: Need to send object name while calling the class
    *       
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/
   
   @testSetup static void testSetData(){ 
          List<Account> lstAccount = new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        List<Territory_Quota__c> lstTerritoryQuota = new List<Territory_Quota__c>();
        List<Seller_Quota__c> lstSellerQuota = new List<Seller_Quota__c>();
        List<Sales_History_CA__c > lstSalesHistoryCA  = new List<Sales_History_CA__c >();
        List<ERP_Account__c > lstERPAccount  = new List<ERP_Account__c >();
        List<Sales_Transaction__c > lstSalesTransaction  = new List<Sales_Transaction__c >();
        List<Sales_History__c  > lstSalesHistory   = new List<Sales_History__c  >();
        List<Demo_Product__c> lstAsset   = new List<Demo_Product__c  >();
        //##record creation for all object##
        Account acc = new Account();                           // account object creation
            acc.Name ='accName';
            insert acc;
        //lstAccount.add(acc);
        Contact cc = new Contact();                            // contact object creation
            cc.LastName = 'contactamogh';
            cc.accountId = acc.Id;
            lstContact.add(cc);
        Territory_Quota__c tq = new Territory_Quota__c();      // Territory Quota object creation
            tq.Name = 'territoryname'; 
            lstTerritoryQuota.add(tq);
        Seller_Quota__c sq = new Seller_Quota__c();            // Seller Quota object creation
            sq.Name = 'sellername';
            lstSellerQuota.add(sq);
        Sales_History_CA__c shc = new Sales_History_CA__c();   // Sales History CA object creation
            shc.Name = 'shistoryname';
            lstSalesHistoryCA.add(shc);
        ERP_Account__c ea = new ERP_Account__c();              // Sales History CA object creation
            ea.Name = 'erpname';
            lstERPAccount.add(ea);
        Sales_Transaction__c st = new Sales_Transaction__c();   // Sales Transaction object creation
            st.Name = 'salestxname';
            lstSalesTransaction.add(st);
        Sales_History__c sh = new Sales_History__c();           // Sales History object creation
            sh.Name = 'saleshtname';
            lstSalesHistory.add(sh);
        Demo_Product__c ast = new Demo_Product__c();            // Asset object creation
            ast.Name = 'assetname';
            lstAsset.add(ast);
        try{
             //insert list of all given object
             //insert lstAccount;
             insert lstContact;
             insert lstTerritoryQuota;
             insert lstSellerQuota;
             insert lstSalesHistoryCA;
             insert lstERPAccount;
             insert lstSalesTransaction;
             insert lstSalesHistory;
             insert lstAsset;
        } catch (DmlException e) {
                    system.debug('getting issue with insert operation for given object record'+e);
                  }

         //delete record for each given object
         Account[] Accts = [SELECT Id, Name FROM Account WHERE Name = 'accName'];
         Contact[] Cts = [SELECT Id, Name FROM Contact WHERE LastName = 'contactamogh'];
         Territory_Quota__c[] Tqs = [SELECT Id, Name FROM Territory_Quota__c WHERE Name = 'territoryname'];
         Seller_Quota__c[] Sqs = [SELECT Id, Name FROM Seller_Quota__c WHERE Name = 'sellername'];
         Sales_History_CA__c[] Shcas = [SELECT Id, Name FROM Sales_History_CA__c WHERE Name = 'shistoryname'];
         ERP_Account__c[] Eas = [SELECT Id, Name FROM ERP_Account__c WHERE Name = 'erpname'];
         Sales_Transaction__c[] Sts = [SELECT Id, Name FROM Sales_Transaction__c WHERE Name = 'salestxname'];
         Sales_History__c[] Shs = [SELECT Id, Name FROM Sales_History__c WHERE Name = 'saleshtname'];
         Demo_Product__c [] asts = [SELECT Id, Name FROM Demo_Product__c WHERE Name = 'assetname'];
             try {
                   database.delete(Accts,false);
                   database.delete(Cts,false);
                   database.delete(Tqs,false);
                   database.delete(Sqs,false);
                   database.delete(Shcas,false);
                   database.delete(Eas,false);
                   database.delete(Sts,false);
                   database.delete(Shs,false);
                   database.delete(asts,false); 
                  } catch (DmlException e) {
                    system.debug('getting issue with delete operation for given object record');
                  }
  }   
   
    
    
 static testMethod void testSchedulerOfRecycleBin2(){ 
         Test.startTest();
         Schedule_EmptyRecycleBinOfObjects var = new Schedule_EmptyRecycleBinOfObjects();
         String sch = '0 0 23 * * ?';
          //if(!Test.isRunningTest())
         System.schedule('strJobName2', sch, var);
         Test.stopTest();
    }
}