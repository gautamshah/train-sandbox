/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBatch_CyclePeriod_InTransit {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',BillingCountry='SG',Submission_Offset_Day__c=null);
        insert acc;
        
        RecordType crt=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        Contact c = new Contact(RecordTypeID = crt.id, AccountId = acc.Id,Type__c = 'DIS Contact', Email='test@covidien.com', LastName='test');
        insert c;
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
        	CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=	String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
        	PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear=	String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
        	AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=	String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
        									Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
        									Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
    	insert CurrentCpRef;
    	
    	Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
        									Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
    	insert AdvCpRef;
    	Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
    	//Product_SKU__c;
    	insert ps;
   //  this should be commented out as this is will be created by the batch job
    	Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id);
    	insert cp;
    	Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
    	//si.
    	insert si;
    	
    	Date myDate = date.valueOf('2013-07-19');
    	system.debug(PrevCpRef.Id);
    	Country_Calendar__c ccal = new Country_Calendar__c(Cycle_Period_Reference__c=PrevCpRef.Id,Country__C='SG',submission_date__C=myDate);
    	insert ccal;
    	Country_Calendar__c ccal1 = new Country_Calendar__c(Cycle_Period_Reference__c=AdvCpRef.Id,Country__C='SG',submission_date__C=myDate);
    	insert ccal1;
    	Country_Calendar__c ccal2 = new Country_Calendar__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Country__C='SG',submission_date__C=myDate);
    	insert ccal2;
    	Test.startTest();
        System.debug('Start Batch Test');
        Database.BatchableContext BC;
        Schedulablecontext sc;
        
        ScheduleCreateCyclePeriod srcalc=new ScheduleCreateCyclePeriod();
        srcalc.testMode=true;
        srcalc.execute(sc);
        
        ScheduleBatchCopyInTransit srcalc1=new ScheduleBatchCopyInTransit();
        srcalc1.testMode=true;
        srcalc1.execute(sc);
        
        Test.stopTest();
    }

    static testMethod void TestoutWithDISContact() {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',BillingCountry='SG',Submission_Offset_Day__c=null);
        insert acc;
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
        	CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=	String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
        	PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear=	String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
        	AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=	String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
        									Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
        									Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
    	insert CurrentCpRef;
    	
    	Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
        									Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
    	insert AdvCpRef;
    	Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
    	//Product_SKU__c;
    	insert ps;
   //  this should be commented out as this is will be created by the batch job
    	Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id);
    	insert cp;
    	Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
    	//si.
    	insert si;
    	
    	Date myDate = date.valueOf('2013-07-19');
    	system.debug(PrevCpRef.Id);
    	Country_Calendar__c ccal = new Country_Calendar__c(Cycle_Period_Reference__c=PrevCpRef.Id,Country__C='SG',submission_date__C=myDate);
    	insert ccal;
    	Country_Calendar__c ccal1 = new Country_Calendar__c(Cycle_Period_Reference__c=AdvCpRef.Id,Country__C='SG',submission_date__C=myDate);
    	insert ccal1;
    	Country_Calendar__c ccal2 = new Country_Calendar__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Country__C='SG',submission_date__C=myDate);
    	insert ccal2;
    	Test.startTest();
        System.debug('Start Batch Test');
        Database.BatchableContext BC;
        Schedulablecontext sc;
        
        ScheduleCreateCyclePeriod srcalc=new ScheduleCreateCyclePeriod();
        srcalc.testMode=true;
        srcalc.execute(sc);
        
        ScheduleBatchCopyInTransit srcalc1=new ScheduleBatchCopyInTransit();
        srcalc1.testMode=true;
        srcalc1.execute(sc);
        
        Test.stopTest();
    }
}