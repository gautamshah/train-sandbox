/*
cpqAll_cTest

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-12/06      Henry Noerdlinger   Created
===============================================================================
*/
@isTest
public class cpqAll_cTest {
	
	@isTest static void itBuildParticipatingFacilityExpressionInclude() 
    {
        cpqAll_c all = new cpqAll_c('+All');
        String expectedValue = ' (Id != null) ';
        System.assertEquals(expectedValue.trim(), all.buildParticipatingFacilityExpression(null).trim());
    }
    
    @isTest static void itBuildParticipatingFacilityExpressionExclude() 
    {
        cpqAll_c all = new cpqAll_c('-All');
        String expectedValue = ' (Id = null) ';
        System.assertEquals(expectedValue.trim(), all.buildParticipatingFacilityExpression(null).trim());
    }
  
  	@isTest static void itBuildDistributorExpression() 
    {
        cpqClassOfTrade_c all = new cpqAll_c('+All');
        String expectedValue = ' (Id = null) ';
        System.assertEquals(expectedValue.trim(), all.buildDistributorExpression(null).trim());
    }
    

}