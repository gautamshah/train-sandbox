/*
Test class

CPQ_Controller_ProposalCustomer_Config
CPQ_FacilityItem

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
private class Test_CPQ_ControllerPropCustConfig
{
    static List<Apttus_Proposal__Proposal__c> testProposals;
    static List<Apttus__APTS_Agreement__c> testSourceAgreements;
    private static CPQ_Config_Setting__c cs;

    static void createTestData() {

        cs = cpqConfigSetting_c.SystemProperties;
        cs.Participating_Facility_COTs__c = '-003\r\n::07H';

        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

		insert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.ENABLED);

        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

        Account testGPO = new Account(Name = 'Test GPO', Status__c = 'Active', Account_External_ID__c = 'US-111111_GPO', RecordTypeID = gpoRT.getRecordTypeId());
        insert testGPO;

        List<Account> testAccounts = new List<Account>{
            new Account(Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID, Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Allied_Group__c = '11111', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '00A' ),
            new Account(Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID, Status__c = 'Active', Name = 'Test Facilitiy', Account_External_ID__c = 'US-222222', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Allied_Group__c = '22222', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '07H' ),
            new Account(Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID, Status__c = 'Active', Name = 'Test Facilitiy 1', Account_External_ID__c = 'US-444444', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Allied_Group__c = '44444', Class_of_Trade__c = '003', Secondary_Class_of_Trade__c = '07EE'),
            new Account(Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID, Status__c = 'Active', Name = 'Test Facilitiy 2', Account_External_ID__c = 'US-333333', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Allied_Group__c = '33333', Class_of_Trade__c = '003', Secondary_Class_of_Trade__c = '07H'),
            new Account(Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID, Status__c = 'Active', Name = 'Test Facilitiy 3', Account_External_ID__c = 'US-555555', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Allied_Group__c = '55555', Class_of_Trade__c = '005', Secondary_Class_of_Trade__c = '123' )
        };
        insert testAccounts;

        List<ERP_Account__c> testERPs = new List<ERP_Account__c> {
            new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[1].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
            new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[2].ID, ERP_Account_Type__c = 'X', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
            new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[3].ID, ERP_Account_Type__c = 'TS', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
            new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[4].ID, ERP_Account_Type__c = 'TX', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active')
        };
        insert testERPs;

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values()) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        testSourceAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Name = 'Test Source Agreement', Apttus__Account__c = testAccounts[0].ID, Duration__c = '5 Years'),
            new Apttus__APTS_Agreement__c(Name = 'Test Source Agreement No Facilities', Apttus__Account__c = testAccounts[0].ID, Duration__c = '5 1/2 Years'),
            new Apttus__APTS_Agreement__c(Name = 'Test Master Agreement', Apttus__Account__c = testAccounts[0].ID, Duration__c = '8 Years')
        };
        insert testSourceAgreements;

        List<Agreement_Participating_Facility__c> testSourceAgreementFacilities = new List<Agreement_Participating_Facility__c> {
            new Agreement_Participating_Facility__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[1].ID),
            new Agreement_Participating_Facility__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[2].ID),
            new Agreement_Participating_Facility__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[3].ID),
            new Agreement_Participating_Facility__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[4].ID),

            new Agreement_Participating_Facility__c(Agreement__c = testSourceAgreements[2].ID, Account__c = testAccounts[2].ID)
        };
        insert testSourceAgreementFacilities;

        testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Source Proposal', Apttus_Proposal__Account__c = testAccounts[0].ID, Duration__c = '3 1/2 Years'),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Source Proposal No Facilities', Apttus_Proposal__Account__c = testAccounts[0].ID, Duration__c = '4 Years'),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', 
                                             Apttus_Proposal__Account__c = testAccounts[0].ID, 
                                             Duration__c = '4 1/2 Years', 
                                             Apttus_QPComply__MasterAgreementId__c = testSourceAgreements[2].ID, 
                                             Deal_Type__c = 'Amendment', 
                                             Legacy_External_ID__c = 'LEG-EXT-ID',
                                             RecordTypeID = mapProposalRType.get('Locally_Negotiated_Agreement_LNA')),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Source Proposal With Vital Sync', Apttus_Proposal__Account__c = testAccounts[0].ID, Duration__c = '4 Years', T_HLVL_VA__c = true, RecordTypeId = mapProposalRType.get('COOP_Plus_Program'))
        };
        insert testProposals;

        List<Participating_Facility__c> testSourceProposalFacilities = new List<Participating_Facility__c> {
            new Participating_Facility__c(Proposal__c = testProposals[0].ID, Account__c = testAccounts[1].ID),
            new Participating_Facility__c(Proposal__c = testProposals[1].ID, Account__c = testAccounts[2].ID),
            new Participating_Facility__c(Proposal__c = testProposals[2].ID, Account__c = testAccounts[3].ID),
            new Participating_Facility__c(Proposal__c = testProposals[3].ID, Account__c = testAccounts[4].ID)
        };
        insert testSourceProposalFacilities;

        List<Participating_Facility__c> testProposalFacilities = new List<Participating_Facility__c> {
            new Participating_Facility__c(Proposal__c = testProposals[2].ID, Account__c = testAccounts[1].ID)
        };
        insert testProposalFacilities;
    }

    static testMethod void myUnitTest() {
        System.Debug('*** myUnitTest ***');
        createTestData();
        List<Apttus_Proposal__Proposal__c> props = [Select Name From Apttus_Proposal__Proposal__c Where ID in: testProposals];
        List<Apttus__APTS_Agreement__c> agms = [Select AgreementId__c From Apttus__APTS_Agreement__c Where ID in: testSourceAgreements];

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalCustomer_Config;
            Test.setCurrentPage(pageRef);
            CPQ_Controller_ProposalCustomer_Config p = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(testProposals[2]));

            //copy from proposal. no proposal specified
            p.SearchSource = 'Proposal';
            p.doSearchByCopy();

            //no facility
            p.SearchProposalSourceID = props[1].Name;
            p.doSearchByCopy();

            //copy from proposal
            p.SearchProposalSourceID = props[0].Name;
            p.doSearchByCopy();

            p.SelectedResultMap.clear();

            //copy from agreement. no agreement specified
            p.SearchSource = 'Agreement';
            p.doSearchByCopy();

            //no facility
            p.SearchAgreementSourceID = agms[1].AgreementId__c;
            p.doSearchByCopy();

            //copy from agreement
            p.SearchAgreementSourceID = agms[0].AgreementId__c;
            p.doSearchByCopy();

            //search by CRN, no CRN specified
            p.doSearchByCRNs();

            //search by CRN
            p.SearchCRNs = '888888';
            p.doSearchByCRNs();

            //search by CRN
            p.SearchCRNs = '222222, 333333, 444444, 555555';
            p.doSearchByCRNs();

            p.SearchCRNs = 'US-222222, US-333333, US-444444, US-555555';
            p.doSearchByCRNs();

            //search by Allied Group
            p.SearchAllied = '88888';
            p.doSearchByAllied();

            p.SearchAllied = '22222, 33333, 44444, 55555';
            p.doSearchByAllied();

            //search by string, no string specified
            p.doSearch();

            //search by string
            p.SearchString = 'CO';
            p.doSearch();

        //test return results
            CPQ_FacilityItem f = p.SearchResults[0];
            f.doGoToAccount();

            System.Debug(p.getShowSearchResult());
            //no search result is checked
            System.Debug(p.getAddCheckedEnabled());

            //check all search results
            p.SearchAllChecked = true;
            p.doSearchCheckAll();

            //all search results are checked
            System.Debug(p.getAddCheckedEnabled());

            p.doAddChecked();

            //no selected result is checked
            System.Debug(p.getRemoveCheckedEnabled());

            //check all selected results
            p.SelectAllChecked = true;
            p.doSelectCheckAll();

            System.Debug(p.getShowSelectedResult());
            //all selected results are checked
            System.Debug(p.getRemoveCheckedEnabled());

            p.doRemoveChecked();
        Test.stopTest();
   }


    static testMethod void Save() {
        System.Debug('*** TestSave ***');
        createTestData();

        List<Apttus_Proposal__Proposal__c> props = [Select Name From Apttus_Proposal__Proposal__c Where ID in: testProposals];

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalCustomer_Config;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_ProposalCustomer_Config p = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(testProposals[2]));

            //copy from proposal
            p.SearchSource = 'Proposal';
            p.SearchProposalSourceID = props[0].Name;
            p.doSearchByCopy();

            //check all search results
            p.SearchAllChecked = true;
            p.doSearchCheckAll();
            p.doAddChecked();

            //remove the one originally in proposal
            p.SelectedResults[0].checked = true;
            p.doRemoveChecked();
            System.Debug('*** p.SelectedResults ' + p.SelectedResults);

            p.doSave();
        Test.stopTest();
    }

    static testMethod void testLocationAssignment() {

        System.debug('*** testLocationAssignment ***');
        createTestData();

        List<Apttus_Proposal__Proposal__c> props = [Select Name From Apttus_Proposal__Proposal__c Where ID in: testProposals];

        Test.startTest();

            PageReference pageRef = Page.CPQ_ProposalCustomer_Config;
            Test.setCurrentPage(pageRef);

            // GIVEN a Proposal with Vital Sync Product (T_HLVL_VA__c = TRUE)

            // WHEN user configures participating facilities (Init Controller)
            // AND proposal has no participating facilities
            CPQ_Controller_ProposalCustomer_Config p = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(testProposals[3]));

            // THEN hasImplementationLocation = FALSE
            System.debug('hasImplementationLocation: ' + p.hasImplementationLocation);

            //copy from proposal
            p.SearchSource = 'Proposal';
            p.SearchProposalSourceID = props[0].Name;
            p.doSearchByCopy();

            //check all search results
            p.SearchAllChecked = true;
            p.doSearchCheckAll();
            p.doAddChecked();

            //remove the one originally in proposal
            p.SelectedResults[0].checked = true;
            p.doRemoveChecked();
            System.Debug('*** p.SelectedResults ' + p.SelectedResults);

            p.doSave();

            // GIVEN a Proposal with Vital Sync Product (T_HLVL_VA__c = TRUE)
            // AND Participating Facilities with location designations
            List<Participating_Facility__c> facilities = [SELECT Id, Implementation_Location__c, Licensed_Location__c FROM Participating_Facility__c WHERE Proposal__c = :testProposals[3].Id];
            System.assertEquals(1,facilities.size(),'Incorrect number of facilities');
            facilities[0].Implementation_Location__c = true;
            update facilities;

            // WHEN user configures participating facilities (Init Controller)
            p = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(testProposals[3]));

            // THEN hasImplementationLocation = TRUE
            System.assert(p.hasImplementationLocation,'No implementation location designated');

            // AND user updates an existing participating Facility
            p.selectedResultMap.values()[0].implementationLocation = false;
            p.selectedResultMap.values()[0].licensedLocation = true;

            p.doSave();

            // THEN records are inserted and updated correctly
            facilities = [SELECT Id, Implementation_Location__c, Licensed_Location__c FROM Participating_Facility__c WHERE Proposal__c = :testProposals[3].Id];
            System.assertEquals(false,facilities[0].Implementation_Location__c,'Should not be implementation location');
            System.assertEquals(true,facilities[0].Licensed_Location__c,'Should be licensed location');

        Test.stopTest();

    }

}