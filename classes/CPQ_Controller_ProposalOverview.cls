/**
Controller for CPQ_ProposalOverview page

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-09-08      Yuli Fintescu		Created
===============================================================================
*/
public with sharing class CPQ_Controller_ProposalOverview {
    public Apttus_Proposal__Proposal__c theRecord {get; set;}
    public List<Apttus_Proposal__Proposal_Line_Item__c> QuoteLineItems {get; private set;}
    
    public CPQ_Controller_ProposalOverview(ApexPages.StandardController controller) {
        theRecord = (Apttus_Proposal__Proposal__c)controller.getRecord();
        String proposalID = theRecord.ID;
        
        //COOP consumables come from category. Other record type come from charge type
        Boolean byChargeType = true;
        String dealTypeName = CPQ_Utilities.getCoreDealTypeName(theRecord);
		if (dealTypeName == 'Respiratory_Solutions_COOP' || dealTypeName == 'COOP_Plus_Program')
			byChargeType = false;
		
        Set<String> lineFields = CPQ_Utilities.getCustomFieldNames(Apttus_Proposal__Proposal_Line_Item__c.SObjectType, false); 
        String lineQueryStr = CPQ_Utilities.buildObjectQuery(lineFields, 
        													'Apttus_Proposal__Proposal__r.Award_Credit__c, Apttus_Proposal__Proposal__r.Maximum_Rebate_Percentage_Sum__c ',
                                                            'Apttus_Proposal__Proposal__c =: proposalID Order by Product_Code__c', 
                                                            'Apttus_Proposal__Proposal_Line_Item__c'); 
		List<Apttus_Proposal__Proposal_Line_Item__c> lines = (List<Apttus_Proposal__Proposal_Line_Item__c>)Database.query(lineQueryStr);
        
        QuoteLineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        
        CPQ_RollupFieldEngine rollupEngine = new CPQ_RollupFieldEngine();
        rollupEngine.ClassifyLineItems(lines);
        
        for (Apttus_Proposal__Proposal_Line_Item__c line : lines) {
        	if (line.Bundle_or_Primary__c == false && line.Apttus_QPConfig__LineType__c != 'Misc') {
        		if (byChargeType == false) {
		            if (!rollupEngine.EquipmentSkus.contains(line.Product_Code__c)) {
		                QuoteLineItems.add(line);
		            }
        		} else {
        			if (line.Apttus_QPConfig__ChargeType__c == 'Consumable') {
        				QuoteLineItems.add(line);
        			}
        		}
        	}
        }
    }
}