/****************************************************************************************
* Name    : IncoReportWrapperController
* Author  : Gautam Shah
* Date    : June 1, 2016
* Purpose : Makes the call to compile a report and emails it to customer and distributor contacts for the Inco PPD program
* 
* Dependancies: 
*  "acctID" query-string parameter (comma-separated list of AccountId values)            
*  "daysAgo" query-string parameter (number of days back to include in the query for Incontinence Submission records)    
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
*
*****************************************************************************************/
public class IncoReportWrapperController 
{
    public List<String> accountIDs {get;set;}
    public String daysAgo {get;set;}
    public String recipient {get;set;}
    
    public IncoReportWrapperController()
    {
        this.accountIDs = ApexPages.currentPage().getParameters().get('acctID').split(',');
        this.daysAgo = ApexPages.currentPage().getParameters().get('daysAgo');
        this.recipient = ApexPages.currentPage().getParameters().get('recipient');
    }
    
    public List<Incontinence_Submissions__c> getRptData()
    {
        String query = 'Select Account__r.Name,Inco_Product_PAR__r.Product_Series__c,Item_Name__c,Item_SKU__c,Item_Size__c,Item_Description__c,Item_Case_Qty__c,Total_Residents_SKU__c,Num_Residents_SKU_Day__c,Inco_Product_PAR__r.Daytime_PAR__c,Num_Residents_SKU_Night__c,Inco_Product_PAR__r.Nighttime_PAR__c,Total_Daily_Use__c,Total_Weekly_Use__c,Cases_Needed_per_Week__c,Current_Cases_on_Hand__c,Cases_to_be_Ordered__c From Incontinence_Submissions__c Where Submitted__c = true And Submitted_Date__c = LAST_N_DAYS:' + daysAgo;
        if(recipient == 'customer')
        {
             query += ' And Account__c In :accountIDs Order By Account__r.Name,Inco_Product_PAR__r.Product_Series__c';
        }
        else if(recipient == 'distributor')
        {
             query += ' And Account__r.Inco_Distributor__c In :accountIDs Order By Account__r.Name,Inco_Product_PAR__r.Product_Series__c';
        }
        List<Incontinence_Submissions__c> records = Database.query(query);
        return records;
    }
    
    public Boolean getIsCustomer()
    {
        if(recipient == 'customer')
        {
            return true;
        }
        return false;
    }
    
    public Boolean getIsDistributor()
    {
        if(recipient == 'distributor')
        {
            return true;
        }
        return false;
    }
}