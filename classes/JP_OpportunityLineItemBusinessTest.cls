@isTest(SeeAllData=true)
private class JP_OpportunityLineItemBusinessTest{
     /****************************************************************************************
     * Name    : JP_OpportunityLineItemBusinessTest
     * Author  : Hiroko Kambayashi
     * Date    : 09/21/2012
     * Purpose : Test class for OpportunityLineItembusiness
     *
     * Dependencies: RecordType Object
     *             , Account Object
     *             , Opportunity Object
     *             , Pricebook2 Object
     *             , Product2 Object
     *             , PricebookEntry Object
     *             , OpportunityLineItem Object
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 10/05       Hiroko Kambayashi    Adding testMethod for inputTargetProductCategoryToOpportunity
     * 12/10/2015  Syu    Iken          Adding testMethod for Test_DivideProfitScheduleByOpportunity_04 to test OpportunityLineItemSchedule.Type = Quantity
     * 12/10/2015  Syu    Iken          Adding testMethod for Test_DivideProfitScheduleByOpportunity_05 to test OpportunityLineItemSchedule.Type = Both
     * 02/23/2016  Hidetoshi kawada     Adding testMethod for Test_DivideProfitScheduleByOpportunity_06 to 08
     *****************************************************************************************/

    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test01
     *   (Case: RecordType.DeveloperName of Opportunity begins 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_01(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky0918');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky0918:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 8, 20, null);
        Opportunity insertedOpp = [SELECT Id, JP_RemainMonth__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseRevenueSchedule=True)
        Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id, CurrencyIsoCode, Revenue, ScheduleDate, Type
                                                        FROM OpportunityLineItemSchedule
                                                        WHERE OpportunityLineItemId = :lineitem.Id];
        //System.assertEquals(testlist.size(), 1);
        Decimal asr = 10000 * 12 / 12 * insertedOpp.JP_RemainMonth__c;
        Date asd = Date.newInstance(2013, 9, 20);
        System.assertEquals(testlist[0].CurrencyIsoCode, 'JPY');
        //System.assertEquals(testlist[0].Revenue.round(), asr);
        //System.assertEquals(testlist[0].ScheduleDate, asd);
        System.assertEquals(testlist[0].Type, 'Revenue');
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test02
     *    (Case: RecordType.DeveloperName of Opportunity doesn't contain 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_02(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky0924');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky0924:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 7, 20, null);
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseRevenueSchedule=True)
        Product2 prd = JP_TestUtil.createTestProduct('test_product002', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 1, 10000);

        //OpportunityLineItemSchedule info when not running Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId = :lineitem.Id];
        Integer listsize = testlist.size();
        System.assertEquals(listsize, 0);
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test03
     *    (Case: OpportunityLineItem.PricebookEntry.Product2.CanUseRevenueSchedule == False)
     */
    static testMethod void Test_DevideProfitScheduleByOpportunity_03(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1005');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky0924:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 7, 20, null);
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_CustomPricebook');
        //Create product (CanUseRevenueSchedule=False)
        Product2 prd = JP_TestUtil.createTestProduct('test_product003', false, null, System.Label.JP_Product2_BusinessUnit_EM);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);

        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 100, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId = :lineitem.Id];
        System.assertEquals(testlist.size(), 0);

        //Opportunity info after run Trigger.isInsert
        List<Opportunity> testOpp = [SELECT Id, JP_Target_Product_Category__c FROM Opportunity WHERE Id = : opp.Id];
        System.assertEquals(testOpp[0].JP_Target_Product_Category__c, null);
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test04
     *   (Case: RecordType.DeveloperName of Opportunity begins 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_04(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'ECStest');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'ECStest:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 8, 20, null);
        Opportunity insertedOpp = [SELECT Id, JP_RemainMonth__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseQuantitySchedule=True)
        Product2 prd = JP_TestUtil.createTestProductQuantity('test_product129', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id, CurrencyIsoCode, Quantity, ScheduleDate, Type
                                                        FROM OpportunityLineItemSchedule
                                                        WHERE OpportunityLineItemId = :lineitem.Id];
        //System.assertEquals(testlist.size(), 1);
        Decimal asr = 10000 * 12 / 12 * insertedOpp.JP_RemainMonth__c;
        Date asd = Date.newInstance(2013, 9, 20);
        System.assertEquals(testlist[0].CurrencyIsoCode, 'JPY');
        //System.assertEquals(testlist[0].Revenue.round(), asr);
        //System.assertEquals(testlist[0].ScheduleDate, asd);
        System.assertEquals(testlist[0].Type, 'Quantity');
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test05
     *   (Case: RecordType.DeveloperName of Opportunity begins 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_05(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'ECStest');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'ECStest:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 8, 20, null);
        Opportunity insertedOpp = [SELECT Id, JP_RemainMonth__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseQuantitySchedule=True,CanUseRevenueSchedule = True)
        Product2 prd = JP_TestUtil.createTestProductQuantityRevenue('test_product129', true,true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id, CurrencyIsoCode, Quantity,Revenue, ScheduleDate, Type
                                                        FROM OpportunityLineItemSchedule
                                                        WHERE OpportunityLineItemId = :lineitem.Id];
        //System.assertEquals(testlist.size(), 1);
        Decimal asr = 10000 * 12 / 12 * insertedOpp.JP_RemainMonth__c;
        Date asd = Date.newInstance(2013, 9, 20);
        System.assertEquals(testlist[0].CurrencyIsoCode, 'JPY');
        //System.assertEquals(testlist[0].Revenue.round(), asr);
        //System.assertEquals(testlist[0].ScheduleDate, asd);
        System.assertEquals(testlist[0].Type, 'Both');
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test06
     *   (Case: RecordType.DeveloperName of Opportunity begins 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_06(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'ECStest');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'ECStest:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 8, 20, null);
        opp.Capital_Disposable__c = 'Capital';
        opp.JP_RentalFlg__c  = true;
        opp.JP_MonthFlg__c  = true;
        update opp;
        Opportunity insertedOpp = [SELECT Id, JP_RemainMonth__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseQuantitySchedule=True,CanUseRevenueSchedule = True)
        Product2 prd = JP_TestUtil.createTestProductQuantityRevenue('test_product129', true,true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id, CurrencyIsoCode, Quantity,Revenue, ScheduleDate, Type
                                                        FROM OpportunityLineItemSchedule
                                                        WHERE OpportunityLineItemId = :lineitem.Id];
        //System.assertEquals(testlist.size(), 1);
        Decimal asr = 10000 * 12 / 12 * insertedOpp.JP_RemainMonth__c;
        Date asd = Date.newInstance(2013, 9, 20);
        System.assertEquals(testlist[0].CurrencyIsoCode, 'JPY');
        //System.assertEquals(testlist[0].Revenue.round(), asr);
        //System.assertEquals(testlist[0].ScheduleDate, asd);
        System.assertEquals(testlist[0].Type, 'Both');
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test07
     *   (Case: RecordType.DeveloperName of Opportunity begins 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_07(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky0918');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky0918:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 8, 20, null);
        opp.Capital_Disposable__c = 'Capital';
        opp.JP_RentalFlg__c  = true;
        opp.JP_MonthFlg__c  = true;
        update opp;
        Opportunity insertedOpp = [SELECT Id, JP_RemainMonth__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseRevenueSchedule=True)
        Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id, CurrencyIsoCode, Revenue, ScheduleDate, Type
                                                        FROM OpportunityLineItemSchedule
                                                        WHERE OpportunityLineItemId = :lineitem.Id];
        //System.assertEquals(testlist.size(), 1);
        Decimal asr = 10000 * 12 / 12 * insertedOpp.JP_RemainMonth__c;
        Date asd = Date.newInstance(2013, 9, 20);
        System.assertEquals(testlist[0].CurrencyIsoCode, 'JPY');
        //System.assertEquals(testlist[0].Revenue.round(), asr);
        //System.assertEquals(testlist[0].ScheduleDate, asd);
        System.assertEquals(testlist[0].Type, 'Revenue');
        Test.stopTest();
    }
    /*
     * Revenue OpportunityLineItemSchedule insert Trigger test08
     *   (Case: RecordType.DeveloperName of Opportunity begins 'Japan')
     */
    static testMethod void Test_DivideProfitScheduleByOpportunity_08(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'ECStest');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'ECStest:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2013, 8, 20, null);
        opp.Capital_Disposable__c = 'Capital';
        opp.JP_RentalFlg__c  = true;
        opp.JP_MonthFlg__c  = true;
        update opp;
        Opportunity insertedOpp = [SELECT Id, JP_RemainMonth__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseQuantitySchedule=True)
        Product2 prd = JP_TestUtil.createTestProductQuantity('test_product129', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

        //OpportunityLineItemSchedule info after run Trigger.isInsert
        List<OpportunityLineItemSchedule> testlist = [SELECT Id, CurrencyIsoCode, Quantity, ScheduleDate, Type
                                                        FROM OpportunityLineItemSchedule
                                                        WHERE OpportunityLineItemId = :lineitem.Id];
        //System.assertEquals(testlist.size(), 1);
        Decimal asr = 10000 * 12 / 12 * insertedOpp.JP_RemainMonth__c;
        Date asd = Date.newInstance(2013, 9, 20);
        System.assertEquals(testlist[0].CurrencyIsoCode, 'JPY');
        //System.assertEquals(testlist[0].Revenue.round(), asr);
        //System.assertEquals(testlist[0].ScheduleDate, asd);
        System.assertEquals(testlist[0].Type, 'Quantity');
        Test.stopTest();
    }

    /*
     * Update JP_Target_Product_Category__c Trigger test01
     *     (Case: OpportunityLineItem.PricebookEntry.Product2.JP_PRProductCTG__c != null && Opportunity.JP_Target_Product_Category__c == null)
     */

    static testMethod void Test_InputTargetProductCategoryToOpportunity_01(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1005');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky1005:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2013, 8, 20, null);
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_CustomPricebook');
        //Create Test Target Product Category
        JP_Target_Product_Category__c tpc = JP_TestUtil.createTestTargetProductCategory('test_targetProduct001', System.Label.JP_Product2_BusinessUnit_EM);
        //Create product (CanUseRevenueSchedule=True, JP_PRProductCTG__c != null)
        Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, tpc.Id, System.Label.JP_Product2_BusinessUnit_EM);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 50, 10000);
        //Opportunity info after run Trigger.isInsert
        List<Opportunity> testOpp = [SELECT Id, JP_Target_Product_Category__c FROM Opportunity WHERE Id = : opp.Id];
        //System.assertEquals(testOpp[0].JP_Target_Product_Category__c, tpc.Id);
        Test.stopTest();
    }

    /*
     * Update JP_Target_Product_Category__c Trigger test02
     *     (Case: OpportunityLineItem.PricebookEntry.Product2.JP_PRProductCTG__c != null && Opportunity.JP_Target_Product_Category__c != null)
     */

    static testMethod void Test_InputTargetProductCategoryToOpportunity_02(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1005');
        //Create Test Target Product Category
        JP_Target_Product_Category__c tpc = JP_TestUtil.createTestTargetProductCategory('test_targetProduct002', System.Label.JP_Product2_BusinessUnit_EM);
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky1005:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2013, 8, 20, tpc.Id);
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_CustomPricebook');
        //Create Test Target Product Category
        JP_Target_Product_Category__c tpc2 = JP_TestUtil.createTestTargetProductCategory('test_targetProduct002_other', System.Label.JP_Product2_BusinessUnit_EbD);
        //Create product (CanUseRevenueSchedule=True, JP_PRProductCTG__c != null)
        Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, tpc2.Id, System.Label.JP_Product2_BusinessUnit_EbD);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 50, 10000);
        //Opportunity info after run Trigger.isInsert
        List<Opportunity> testOpp = [SELECT Id, JP_Target_Product_Category__c FROM Opportunity WHERE Id = : opp.Id];
        System.assertNotEquals(testOpp[0].JP_Target_Product_Category__c, tpc2.Id);
        Test.stopTest();
    }

    /*
     * Set the value of UnitPrice * Quantity to Field JP_OriginalTotalPrice__c Trigger test01
     */
    static testMethod void Test_InputOriginalTotalPrice_01(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1113');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky1113:Opportunity',
                                                            System.Label.JP_Opportunity_StageName_Develop,
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                            2012, 12, 20, null);
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_CustomPricebook');
        //Create product (CanUseRevenueSchedule=True)
        Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        //Create Test OpportunityLineItem
        OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 30, 60000);
        OpportunityLineItem insertedItem = [SELECT Id, Quantity, UnitPrice,
                                                    TotalPrice, JP_OriginalTotalPrice__c
                                                FROM OpportunityLineItem
                                                WHERE OpportunityId = : opp.Id];
        //System.assertNotEquals(insertedItem.TotalPrice, insertedItem.JP_OriginalTotalPrice__c);
        List<OpportunityLineItemSchedule> schs = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId = : insertedItem.Id];
        //Run Trigger after update JP_Sales_start_Date__c
        Opportunity insertedOpp = [SELECT Id, CloseDate FROM Opportunity WHERE Id = : opp.Id];
        insertedOpp.CloseDate = Date.newInstance(2013, 4, 18);
        //update insertedOpp;
        OpportunityLineItem reDivideItem = [SELECT Id, Quantity, UnitPrice FROM OpportunityLineItem WHERE OpportunityId = : opp.Id];
        List<OpportunityLineItemSchedule> reSchs = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId = : reDivideItem.Id];
        //System.assertNotEquals(insertedItem.Id, reDivideItem.Id);
        //System.assertNotEquals(schs.size(), reSchs.size());
        Test.stopTest();
    }
}