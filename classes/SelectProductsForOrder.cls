public with sharing class SelectProductsForOrder {
    List<productWrapper> productList = new List<productWrapper>();
    List<Product_SKU__c> selectedProducts = new List<Product_SKU__c>();
    //Set<Product_SKU__c> tempSet= new set<Product_SKU__c>();
    ///////////
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String searchString{get;set;}
    String Value;
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = 20;// Integer.valueOf(System.Label.StagingPageSize);//10;
                value = ApexPages.currentPage().getParameters().get('id');
                system.debug (value);
                String strCountry = [select Country from User where Id = :UserInfo.getUserId() limit 1].Country;
                //Product_SKU__c.
                string queryString = 'select Id,GBU__c, Name, SKU__c, conversion_Factor__C,Selling_UOM__c, Sales_Class__c,Sales_Class_Description__c, Sales_Class_2__c,Sales_Class_2_Description__c,Sales_Class_3__c,Sales_Class_3_Description__c from Product_SKU__c';
                if(searchString!=null&&searchString!='')
                 	queryString=queryString+' where (Name like \'%'+searchString+'%\' or SKU__c like \'%'+searchString+'%\') and Country__c = \''+strCountry + '\'';
                system.debug (querystring);
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
     public pageReference doSearch() {
        setCon = null;
        getProducts();
        setCon.setPageNumber(1);
        return null;
    }
    public pageReference refresh() {
     //   setCon = null;
     //   getProducts();
    //   setCon.setPageNumber(1);
        return null;
    }
    ///////////////////
    public List<productWrapper> getProducts()
    {
        productList=new List<productWrapper>();
        for(Product_SKU__c a: (List<Product_SKU__c>)setCon.getRecords())
            productList.add(new productWrapper(a));
        return productList; 
        
    }
    public PageReference getSelected()
    {
        selectedProducts.clear();
        for(productWrapper prdwrapper: productList)
            if(prdwrapper.selected == true)
                selectedProducts.add(prdwrapper.prd);
        return null;
        
    }
    
    public List<Product_SKU__c> GetSelectedProducts()
    {
        if(selectedProducts.size()>0)
        return selectedProducts;
        else
        return null;
    }
    public Pagereference AddProductsToOrder()
    {
        if(selectedProducts!=null&&selectedProducts.size()>0)
        {
            List<Order_Line_Item__c> lstOli=new List<Order_Line_Item__c>();
            for(Product_SKU__c prod:selectedProducts)
            {
                Order_Line_Item__c oli=new Order_Line_Item__c(Order_Request__c=value,Product_Name__c=prod.Id);
                lstOli.add(oli);
            }
            insert lstOli;
            selectedProducts.clear();
        }
        
        return null;
    }
    public Pagereference AddAndDone()
    {
        Pagereference pgref=AddProductsToOrder();
        
    //    pgref=new Pagereference('/apex/EditOrderLineItems?Id='+value);
        pgref=new Pagereference('/'+value);

        return pgref;
    }
    public class productWrapper
    {
        public Product_SKU__c prd{get; set;}
        public Boolean selected {get; set;}
        public productWrapper(Product_SKU__c a)
        {
            prd = a;
            selected = false;
        }
    }
}