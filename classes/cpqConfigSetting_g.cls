global class cpqConfigSetting_g extends sObject_g
{
	global cpqConfigSetting_g()
	{
		super(cpqConfigSetting_cache.get());
	}

	////
	//// cast
	////
	public static Map<Id, CPQ_Config_Setting__c> cast(Map<Id, sObject> sobjs)
	{
		try
		{
			return new Map<Id, CPQ_Config_Setting__c>((List<CPQ_Config_Setting__c>)sobjs.values());
		}
		catch(Exception e)
		{
			return new Map<Id, CPQ_Config_Setting__c>();
		}
	}

	public static Map<object, Map<Id, CPQ_Config_Setting__c>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, CPQ_Config_Setting__c>> target = new Map<object, Map<Id, CPQ_Config_Setting__c>>();
		if (source != null)
			for(object key : source.keySet())
				target.put(key, cast(source.get(key)));
			
		return target;
	}

	global Map<Id, CPQ_Config_Setting__c> fetch(sObjectField field, object value)
	{
		return cast(this.cache.fetch(field, value));
	}
}