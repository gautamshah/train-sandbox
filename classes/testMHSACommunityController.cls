@isTest (SeeAllData=false)
private class testMHSACommunityController {
    
   private static User internalUser;
   private static User externalUser;
   private static Account testAccount;
   private static Contact testContact;
   private static List<MHSA_Community_Control__c> ccList;
   private static List<MHSA_Community_Sub_Control__c> cscList;
   private static List<MHSA_Community_Account_Access__c> mcaaList;
   private static MHSA_Community_Control__c cc1;
   private static MHSA_Community_Control__c cc2;
   private static MHSA_Community_Control__c cc3;
   private static MHSA_Community_Control__c cc4;
   private static MHSA_Community_Sub_Control__c csc1;
   private static MHSA_Community_Sub_Control__c csc2;
   private static MHSA_Community_Sub_Control__c csc3;
   private static MHSA_Community_Sub_Control__c csc4;
   private static MHSA_Community_Sub_Control__c csc5;
   private static MHSA_Community_Account_Access__c mcaa1;
   private static MHSA_Community_Account_Access__c mcaa2;
   private static MHSA_Community_Account_Access__c mcaa3;

   static testMethod void init() { /*Mock Data Setup*/

    /*Get System Admin Profile Id*/
    Profile pi = [SELECT Id FROM Profile WHERE Name='System Administrator']; 

    /*Get Customer Community User Profile Id*/
    Profile pe = [SELECT Id FROM Profile WHERE Name='Customer Community User']; 
    
    /*Get 'US-Healthcare Facility' Account Record Type Id */
    RecordType acctRt = [SELECT Id FROM Recordtype WHERE Name='US-Healthcare Facility' LIMIT 1];
    
    /*Create Account*/
    testAccount = new Account(Name='TestAccount',
                              RecordTypeId = acctRt.Id,
                              BillingStreet = 'Test Billing Street',
                              BillingCity = 'Test Billing City', 
                              BillingState = 'Test',
                              BillingPostalCode = '12345'
                              );
    insert testAccount;

    /*Create Contact*/
    testContact = new Contact(FirstName = 'PATestExtUser',
                              LastName='Stevens',
                              AccountId = testAccount.Id
                              );
    insert testContact;

    /*Create Internal User*/
    internalUser = new User(Alias = 'patesti', 
                            Email='paIntUser@medtronic.com', 
                            FirstName='PATestIntUser',
                            LastName='Sweet',
                            UserName='paIntUser@medtronic.com',
                            EmailEncodingKey='UTF-8', 
                            LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', 
                            Community_Role__c = 'Internal',
                            ProfileId = pi.Id, 
                            TimeZoneSidKey='America/Los_Angeles'
                            );
    insert internalUser;
    
    /*Create External User*/
    externalUser = new User(Alias = 'pateste', 
                            Email='paExtUser@medtronic.com', 
                            FirstName='PATestExtUser',
                            LastName='Stevens',
                            ContactId = testContact.Id,
                            UserName='paExtUser@medtronic.com',
                            EmailEncodingKey='UTF-8', 
                            LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', 
                            Community_Role__c = 'Basic',
                            ProfileId = pe.Id, 
                            TimeZoneSidKey='America/Los_Angeles'
                            );
    insert externalUser;
      
    /* MHSA Community Controls and MHSA Community Sub-Control Lists */
     ccList = new List<MHSA_Community_Control__c>();
     cscList = new List<MHSA_Community_Sub_Control__c>();
    /* MHSA Community Account Accesses Lists */
     mcaaList = new List<MHSA_Community_Account_Access__c>();
      
      /* Create MHSA Community Controls */
     cc1 = new MHSA_Community_Control__c(Name = 'Summary',
                                         Control_Type__c = 'Toggle',
                                         Description__c = 'Test Summary Description',
                                         Menu_Nav_Order__c = 1,
                                         Role_Access__c = 'Basic;Doctor;External Executive;Internal',
                                         State__c = 'summary',
                                         Status__c = 'Active',
                                         Tableau_URL__c = 'https://public.tableau.com/views/USMassShooting2013-2015TrendsPerState/Story1?:embed=y'
                                         ); 

     cc2 = new MHSA_Community_Control__c(Name = 'Facility Outcomes',
                                         Control_Type__c = 'Toggle',
                                         Description__c = 'Test Facility Outcomes Description',
                                         Menu_Nav_Order__c = 2,
                                         Role_Access__c = 'Basic;Doctor;External Executive;Internal',
                                         State__c = 'facilityOutcomes',
                                         Status__c = 'Active',
                                         Tableau_URL__c = 'https://public.tableau.com/views/USMassShooting2013-2015TrendsPerState/Story1?:embed=y'
                                         ); 

      cc3 = new MHSA_Community_Control__c(Name = 'Surgeon Outcomes',
                                          Control_Type__c = 'Link',
                                          Description__c = 'Test Surgeon Scorecard Description',
                                          Menu_Nav_Order__c = 3,
                                          Role_Access__c = 'External Executive;Internal',
                                          State__c = 'surgeonOutcomes',
                                          Status__c = 'Inactive',
                                          Tableau_URL__c = 'https://public.tableau.com/views/USMassShooting2013-2015TrendsPerState/Story1?:embed=y'
                                          );  

       cc4 = new MHSA_Community_Control__c(Name = 'Population Description',
                                           Control_Type__c = 'Link',
                                           Description__c = 'Test Surgeon Scorecard Description',
                                           Menu_Nav_Order__c = 4,
                                           Role_Access__c = 'Doctor;External Executive',
                                           State__c = 'popDescription',
                                           Status__c = 'Active',
                                           Tableau_URL__c = 'https://public.tableau.com/views/USMassShooting2013-2015TrendsPerState/Story1?:embed=y'
                                           );   

       /* Add all created MHSA Community Controls to MHSA Community Control List */
       ccList.addAll(new List<MHSA_Community_Control__c>{cc1,cc2,cc3,cc4});
       /* Insert MHSA Community Control List */
       insert ccList;

       /* Create MHSA Community Sub-Controls */ 
       csc1 = new MHSA_Community_Sub_Control__c(Name = 'Overview',
                                                Content_Header__c = 'Test Sub Header: Overview', 
                                                Control_Type__c = 'Link',
                                                Menu_Nav_Order__c = 1,
                                                MHSA_Community_Parent_Nav__c = cc1.Id,
                                                Role_Access__c = 'Internal',
                                                State__c = 'overview',
                                                Status__c = 'Active',
                                                Tableau_URL__c = 'https://public.tableau.com/views/RegionalSampleWorkbook/Storms'
                                               ); 

        csc2 = new MHSA_Community_Sub_Control__c(Name = 'Average Total Cost',
                                                 Content_Header__c = 'Test Sub Header: Avg. Total Cost', 
                                                 Control_Type__c = 'Link',
                                                 Menu_Nav_Order__c = 2,
                                                 MHSA_Community_Parent_Nav__c = cc1.Id,
                                                 Role_Access__c = 'Internal',
                                                 State__c = 'avgTotalCost',
                                                 Status__c = 'Inactive',
                                                 Tableau_URL__c = 'https://public.tableau.com/views/RegionalSampleWorkbook/Storms'
                                                ); 

        csc3 = new MHSA_Community_Sub_Control__c(Name = 'Clinical',
                                                Content_Header__c = 'Test Sub Header: Clinical', 
                                                Control_Type__c = 'Link',
                                                Menu_Nav_Order__c = 1,
                                                MHSA_Community_Parent_Nav__c = cc2.Id,
                                                Role_Access__c = 'Internal Publisher',
                                                State__c = 'clinical',
                                                Status__c = 'Active',
                                                Tableau_URL__c = 'https://public.tableau.com/views/RegionalSampleWorkbook/Storms'
                                               );  

       csc4 = new MHSA_Community_Sub_Control__c(Name = 'Overview',
                                                Content_Header__c = 'Test Sub Header: Performance Review', 
                                                Control_Type__c = 'Link',
                                                Menu_Nav_Order__c = 1,
                                                MHSA_Community_Parent_Nav__c = cc3.Id,
                                                Role_Access__c = 'Internal',
                                                State__c = 'overview',
                                                Status__c = 'Inactive',
                                                Tableau_URL__c = 'https://public.tableau.com/views/RegionalSampleWorkbook/Storms'
                                               ); 

       csc5 = new MHSA_Community_Sub_Control__c(Name = 'Clinical',
                                                Content_Header__c = 'Test Sub Header: Performance Review', 
                                                Control_Type__c = 'Link',
                                                Menu_Nav_Order__c = 1,
                                                MHSA_Community_Parent_Nav__c = cc3.Id,
                                                Role_Access__c = 'Internal',
                                                State__c = 'clinical',
                                                Status__c = 'Inactive',
                                                Tableau_URL__c = 'https://public.tableau.com/views/RegionalSampleWorkbook/Storms'
                                               ); 
       
       /* Add all created MHSA Community Sub-Controls to MHSA Community Sub-Control List */
       cscList.addAll(new List<MHSA_Community_Sub_Control__c>{csc1,csc2,csc3,csc4});
       /* Insert MHSA Community Sub-Control List */
       insert cscList;

       /* Create MHSA Community Account Accesses */ 
       mcaa1 = new MHSA_Community_Account_Access__c(Account__c = testAccount.Id,
                                                    MHSA_Community_Sub_Control__c = csc1.Id); 
       mcaa2 = new MHSA_Community_Account_Access__c(Account__c = testAccount.Id,
                                                    MHSA_Community_Sub_Control__c = csc2.Id);
       mcaa3 = new MHSA_Community_Account_Access__c(Account__c = testAccount.Id,
                                                    MHSA_Community_Sub_Control__c = csc3.Id);
       
       /* Add all created MHSA Community Sub-Controls to MHSA Community Sub-Control List */
       mcaaList.addAll(new List<MHSA_Community_Account_Access__c>{mcaa1,mcaa2,mcaa3});
       /* Insert MHSA Community Sub-Control List */
       insert mcaaList;

   }
   
   static testMethod void testGetAllMHSAControlsInternal()
   {
      init();

      Test.startTest();
       System.runAs(internalUser){
         System.AssertNotEquals(null, MHSACommunityController.returnAllMHSAControls()); 
         System.AssertEquals(2, MHSACommunityController.returnAllMHSAControls().size());
       }       
      Test.stopTest();  
   }

   static testMethod void testGetAllMHSAControlsExternal()
   {
      init();
           
      Test.startTest();
       System.runAs(externalUser){
            System.AssertNotEquals(null, MHSACommunityController.returnAllMHSAControls()); 
            System.AssertEquals(2, MHSACommunityController.returnAllMHSAControls().size());   
       }
      Test.stopTest();    
   }
    
}