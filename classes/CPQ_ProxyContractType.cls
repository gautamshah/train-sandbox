/****************************************************************************************
* Name    : Class: CPQ_ProxyContractType
* Author  : Paul Berglund
* Date    : 08/13/2015
* Purpose : A file to hold the Contract and ArrayOfContract classes
* 
* Dependancies: 
*   Called by: None
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 9/24/2015     Paul Berglund           Added CustomerWrapper class
*****************************************************************************************/
public class CPQ_ProxyContractType {
    public class ArrayOfContract {
        public CPQ_ProxyContractType.Contract[] Contract;
        private String[] Contract_type_info = new String[]{'Contract','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract','true','false'};
        private String[] field_order_type_info = new String[]{'Contract'};
    }
    public class Contract {
        public String Address;
        public Decimal BillTo;
        public String City;
        public String ClassofTrade;
        public String CommitmentType;
        public String ContractName;
        public String ContractType;
        public DateTime CustomerEffDt;
        public DateTime CustomerExpDt;
        public String CustomerName {get; set;}
        public String CustomerType;
        public String CustomerTypeDsc;
        public String Group_x;
        public String Number_x;
        public Decimal Seq;
        public Decimal ShipTo {get; set;}
        public String State;
        public String Zip;
        private String[] Address_type_info = new String[]{'Address','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] BillTo_type_info = new String[]{'BillTo','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','false'};
        private String[] City_type_info = new String[]{'City','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] ClassofTrade_type_info = new String[]{'ClassofTrade','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] CommitmentType_type_info = new String[]{'CommitmentType','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] ContractName_type_info = new String[]{'ContractName','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] ContractType_type_info = new String[]{'ContractType','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] CustomerEffDt_type_info = new String[]{'CustomerEffDt','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','false'};
        private String[] CustomerExpDt_type_info = new String[]{'CustomerExpDt','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','false'};
        private String[] CustomerName_type_info = new String[]{'CustomerName','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] CustomerType_type_info = new String[]{'CustomerType','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] CustomerTypeDsc_type_info = new String[]{'CustomerTypeDsc','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] Group_x_type_info = new String[]{'Group','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] Number_x_type_info = new String[]{'Number','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] Seq_type_info = new String[]{'Seq','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','false'};
        private String[] ShipTo_type_info = new String[]{'ShipTo','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','false'};
        private String[] State_type_info = new String[]{'State','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] Zip_type_info = new String[]{'Zip','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract','true','false'};
        private String[] field_order_type_info = new String[]{'Address','BillTo','City','ClassofTrade','CommitmentType','ContractName','ContractType','CustomerEffDt','CustomerExpDt','CustomerName','CustomerType','CustomerTypeDsc','Group_x','Number_x','Seq','ShipTo','State','Zip'};
    }
    public class Proxy_CustomerWrapper {
        public CPQ_ProxyContractType.ArrayOfContract Customers;
        public Integer TotalCount;
        private String[] Customers_type_info = new String[]{'Customers','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy',null,'0','1','true'};
        private String[] TotalCount_type_info = new String[]{'TotalCount','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy','true','false'};
        private String[] field_order_type_info = new String[]{'Customers','TotalCount'};
    }
 }