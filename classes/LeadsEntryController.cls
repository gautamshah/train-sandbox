/***********************
Aurthor: Yap Zhen-Xiong
Email: Zhenxiong.Yap@covidien.com
Description: Controller for Leads Entry Module (Public Facing)
***********************/

public with sharing class LeadsEntryController {

    public String ownerName {get;set;}
    public Lead newLead{get;set;}
    public CampaignMember cm {get;set;}
    
    public LeadsEntryController (){
        String cid = ApexPages.currentPage().getParameters().get('cid');
        String oid = ApexPages.currentPage().getParameters().get('oid');
        
        cm = new CampaignMember();
        newLead = new Lead();
        
        try{
            cm.CampaignId = cid;
           
            newLead.OwnerId = oid;
            
            List<User> uList = [SELECT FirstName, LastName FROM User WHERE id=:newLead.OwnerId LIMIT 1];
            ownerName = uList[0].FirstName + ' ' + uList[0].LastName;
        }catch(Exception e) {

        } 
    }
    
    public PageReference submitLead() {
        try{
            if(newLead.MobilePhone==null && newLead.Email==null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please provide Mobile Number or Email Address.');
                ApexPages.addMessage(myMsg);
                
                return null;
            }
           
            insert newLead;
            cm.LeadId= newLead.id;
                        
            insert cm;
        }catch(DMLException e) {
            return null;
        } 
        
        PageReference pageRef = new PageReference('/apex/LeadsThankYou?oid='+newLead.OwnerId+'&cid='+cm.CampaignId);
        pageRef .setRedirect(true);
        return pageRef;
    }
}