public with sharing class CreateLeadExtension {
    /****************************************************************************************
    * Name    : CreateLeadExtension
    * Author  : Mike Melcher
    * Date    : 3-02-2011
    * Purpose : Send user to Lead edit screen with the Company and Last Name pre-populated with "*"
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    *
    *****************************************************************************************/ 
    private final Lead l;
    
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String accountID {get; set;}
    public String masterID {get; set;}
    public String firstname {get; set;}
    public String lastname {get; set;}


   
    public CreateLeadExtension(ApexPages.StandardController stdController) {
        this.l = (Lead)stdController.getRecord();
    }
   
    
   public pagereference createLead() {
                            
                             
      // retURL = ApexPages.currentPage().getParameters().get('retURL');
       rType = ApexPages.currentPage().getParameters().get('RecordType');
      // cancelURL = ApexPages.currentPage().getParameters().get('\001\' + currentA.Id');
       ent = ApexPages.currentPage().getParameters().get('ent');
       confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
       saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
       Pagereference returnURL = new PageReference('/00Q/e');
       returnURL.getParameters().put('retURL', retURL);
       //returnURL.getParameters().put('RecordType', newRT);
       returnURL.getParameters().put('cancelURL', cancelURL);
       returnURL.getParameters().put('ent', ent);
       returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
       returnURL.getParameters().put('save_new_url', saveNewURL);
       returnURL.getParameters().put('nooverride', '1');

                 
       returnURL.getParameters().put('lea3', '*');      
       returnURL.getParameters().put('name_lastlea2', '*');            
            
       returnURL.setRedirect(true);
       return returnURL;
  }
 
}