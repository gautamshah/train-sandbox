public class Territory_c extends sObject_c
{
	private static Territory_g cache = new Territory_g();
	
	public static set<string> fieldsToInclude =
		new set<string>
		{
			'Id',
			'Name',
			'Custom_External_TerritoryID__c',
			'ParentTerritoryId',
			'DeveloperName',
			'Description'
		};

	////
	//// create
	////
	public static Map<sObjectField, object> defaults =
		new Map<sObjectField, object>
		{
			Territory.field.AccountAccessLevel => 'Edit',
	        Territory.field.OpportunityAccessLevel => 'Edit',
    	    Territory.field.CaseAccessLevel => 'None',
        	Territory.field.ContactAccessLevel => 'Edit',
        	Territory.field.Source__c => 'EU_Varicent'
		};
		
	public static Territory create(integer i) 
	{
		return create(Territory.fields.Name, null, i);
	}
	
	public static Territory create(sObjectField field, object value, integer i)
	{
		Territory t = new Territory();
		for(sObjectField def : defaults.keySet())
			t.put(def, defaults.get(def));

		t.put(field, value);
		
		string name = (t.Name == null) ? 'NAME ' + i : t.Name;
		sObject_c.putIfNull(t, Territory.field.Name, name);
		sObject_c.putIfNull(t, Territory.field.Description, name);
		sObject_c.putIfNull(t, Territory.DeveloperName, name.replace(' ', ''));

		sObject_c.putIfNull(t, Territory.field.Custom_External_TerritoryID__c, 'EXTID' + i);
		
		return t;
	}
	
}