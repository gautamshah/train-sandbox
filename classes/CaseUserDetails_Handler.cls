public without sharing class CaseUserDetails_Handler {
/****************************************************************************************
    * Name    : CaseUserDetails_Handler
    * Author  : Brajmohan Sharma
    * Date    : 21-Apr-2017
    * Purpose :it will aware about case work load of user
    * case    : 00981782
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/
   
   public Map<String,Integer> MapofOwners = new Map<String,Integer>();
   public void CaseUserDetailsonBefore(List<case> caselist,Map<ID,case> newmap , Map<ID,case> oldmap){
        system.debug('values of caselist'+caselist+'values of new map'+newmap+'values of old map'+oldmap);
        Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
        system.debug('value of rt_ map'+rt_Map);  
   
             for(case newcase:caselist) {
                system.debug('---Owner---'+newcase.Ownerid);
                if(rt_map.get(newcase.recordTypeID).getName().containsIgnoreCase('IDNSC Case Feed')){
                    if(((newmap.get(newcase.id).Ownerid != oldmap.get(newcase.id).Ownerid) && (newmap.get(newcase.id).Status =='In Process')) || (newmap.get(newcase.id).Status == 'In Process' && oldmap.get(newcase.id).Status == 'New') || (newmap.get(newcase.id).Status == 'In Process' && oldmap.get(newcase.id).Status == 'On Hold') || (newmap.get(newcase.id).Status == 'In Process' && oldmap.get(newcase.id).Status == 'Awaiting Response/Dependancy') || (newmap.get(newcase.id).Status == 'In Process' && oldmap.get(newcase.id).Status == 'Closed-Resolved')){
                        system.debug('newmap.get(newcase.id).Ownerid'+newmap.get(newcase.id).Ownerid+'oldmap.get(newcase.id).Ownerid'+oldmap.get(newcase.id).Ownerid);
                        MapofOwners.put(newcase.ownerId,0);
                        system.debug('value of map'+MapofOwners);
                    }
             }
           }
       system.debug('value of map---'+MapofOwners);
       AggregateResult[] groupedResults = [SELECT ownerId,Status ,count(Id)c FROM Case where ownerId In : MapofOwners.keySet() AND Status ='In Process' AND RecordType.name = 'IDNSC Case Feed' GROUP BY Ownerid,Status];
     
         if(groupedResults.size()>0){
              system.debug(groupedResults.size()+'value of groupedResults'+groupedResults);
          for(AggregateResult ar : groupedResults)  {
              system.debug(String.valueOf(ar.get('ownerId'))+'value of aggregate='+Integer.valueOf(ar.get('c')));
              MapOfOwners.put(String.valueOf(ar.get('ownerId')),Integer.valueOf(ar.get('c')));
              system.debug(MapOfOwners.keySet()+' key values of map'+MapOfOwners.values()+' agregate case count values'+Integer.valueOf(ar.get('c'))+' agregate owner id value  '+String.valueOf(ar.get('ownerId')));
                 }
           }else{
                system.debug('No need to operation on groupedResults');   
            }
          
            system.debug(MapOfOwners.keySet()+'+++++++'+MapOfOwners.values());
           
       for(Case can : caselist){
             if(rt_map.get(can.recordTypeID).getName().containsIgnoreCase('IDNSC Case Feed')){
                 system.debug('trigger calling3'+can.ownerid);   
                 system.debug('newmap.get(can.id).Ownerid   '+newmap.get(can.id).OwnerId+'oldmap.get(can.id).Ownerid   '+oldmap.get(can.id).Ownerid);
                 system.debug('count value of current owner'+MapofOwners.get(can.Ownerid));
                 integer counts = MapofOwners.get(can.Ownerid);
                 system.debug('count value'+counts);
               if(((newmap.get(can.id).Ownerid != oldmap.get(can.id).Ownerid) && (newmap.get(can.id).Status =='In Process')) || (newmap.get(can.id).Status == 'In Process' && oldmap.get(can.id).Status == 'New') || (newmap.get(can.id).Status == 'In Process' && oldmap.get(can.id).Status == 'On Hold') || (newmap.get(can.id).Status == 'In Process' && oldmap.get(can.id).Status == 'Awaiting Response/Dependancy') || (newmap.get(can.id).Status == 'In Process' && oldmap.get(can.id).Status == 'Closed-Resolved')){ 
                   if(counts >= 1 ){
                      can.Ownerid.addError('User already working on another case');
                       system.debug('inside map value condition');                     
                    }
                    else{
                        system.debug('inside else map value condition');             
                    }
                }//if if changing owner
              }//if record type check
           }//for trigger.new
    }// CaseUserDetailsonBefore
 }//class