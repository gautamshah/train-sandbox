global class ScheduleCreateLoginhistoryLog implements Schedulable{
    
    String batchQuery;  
    global void execute(SchedulableContext sc) {
        System.debug('Before insert'+System.now()); 
        batchQuery = 'select id,userid,status,logintime,application,logintype from loginhistory where logintime=YESTERDAY';        
        System.debug('===batchQuery==='+batchQuery);
        BatchInsertLoginHistory b = new BatchInsertLoginHistory(batchQuery); 
        database.executebatch(b);
    }
    
}