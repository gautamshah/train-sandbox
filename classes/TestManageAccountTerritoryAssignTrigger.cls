@isTest(SeeAllData=True)
private class TestManageAccountTerritoryAssignTrigger {

	private static final integer recordCount = 200;
	
	static private List<Account_Territory_Assignment__c> createTestData(){
		IC_Config_Setting__c cs = IC_Config_Setting__c.getValues('ATA Trigger Exclude Sources');
	    if (cs == null) {
			insert new IC_Config_Setting__c(Name = 'ATA Trigger Exclude Sources', Value__c = 'POIU|YTRE');
	    }

		List <Account> listAccountInsert = new List <Account>(); 
        Set <Id> setAccountIdInsert = new Set <Id>();
        for(Integer a=0;a<recordCount;a++){
        	Account testAccount = new Account();
        	testAccount.Name = 'TestAccount'+a;
        	listAccountInsert.add(testAccount);
        }
        
        Database.SaveResult[] srA = Database.insert(listAccountInsert);
        for(Integer i=0; i<srA.size(); i++){
            Database.SaveResult saveResultAccount = srA[i];
            setAccountIdInsert.add(saveResultAccount.getId());
        }
        
       	List <Account> listAccount = [Select Id from Account where Id IN :setAccountIdInsert]; 
        List<Territory> listTerritory = [Select Id from Territory LIMIT :recordCount];
        List<Account_Territory_Assignment__c> listATA = new List<Account_Territory_Assignment__c>();
        
        for(Account acct : listAccount){
        	Integer tCount = 0;
        	Account_Territory_Assignment__c ataInsert = new Account_Territory_Assignment__c();
        	ataInsert.AccountId__c = acct.Id;
        	ataInsert.TerritoryId__c = listTerritory[tCount].Id;
        	ataInsert.Action__c = 'I';
        	ataInsert.Sequence_Number__c = '1';
        	ataInsert.Account_External_Id__c = '1';
        	ataInsert.Territory_External_Id__c = '1';
        	listATA.add(ataInsert);
        	tCount++;
        }
        system.debug('***listATA.size(): ' + listATA.size());
        system.debug('***listATA:' + listATA);
        return listATA;
	}
	
    static testMethod void testInsert() {
        
        List<Account_Territory_Assignment__c> listATAInsert = createTestData();
        system.debug('***listATAInsert:' + listATAInsert);
        
        Test.startTest();
        insert listATAInsert;
        Test.stopTest();
    }
    
    static testMethod void testDelete() {
        
        List<Account_Territory_Assignment__c> listATADeletions = new List<Account_Territory_Assignment__c>();
        List<Account_Territory_Assignment__c> listATAInsertions = createTestData();
        system.debug('***listATAInsertions: ' + listATAInsertions);
        
        insert listATAInsertions;
        
        for (Account_Territory_Assignment__c ata : listATAInsertions){
        	Account_Territory_Assignment__c dAta  = new Account_Territory_Assignment__c();   
        	dAta.AccountId__c = ata.AccountId__c;
        	dAta.TerritoryId__c = ata.TerritoryId__c; 
        	dAta.Action__c = 'D';
        	dAta.Sequence_Number__c = '1';
        	dAta.Account_External_Id__c = '1';
        	dAta.Territory_External_Id__c = '1';
        	listATADeletions.add(dAta);
        }
        
        Test.startTest();
        insert listATADeletions;
        Test.stopTest();
        
    } // end testDelete()   
}