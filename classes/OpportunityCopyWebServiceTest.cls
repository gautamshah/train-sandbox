/**
Test class for
OpportunityCopyWebService

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-01-27      Hidetoshi kawada     Created
===============================================================================
*/

@isTest
private class OpportunityCopyWebServiceTest{

    static testMethod void oppCopyWithLineItem() {

        /*
        Id pId = [select Id from Profile where name = 'CRM Admin Support'].Id;
        Id tu1Id = [SELECT Id FROM Territory where Name='REGION TERRITORY ASIA'].Id;
        Id tu1userId = [SELECT Id, UserId FROM UserTerritory where TerritoryId=:tu1Id LIMIT 1].UserId;
        User testuser = [select Id from User where Id = :tu1userId Limit 1];
        */
        // Create User Data
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User testUser = new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest',
                        Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr',
                        ProfileId = profile1.Id, TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                        LanguageLocaleKey = 'en_US', DefaultCurrencyIsoCode = 'JPY', LocaleSidKey = 'en_US');
        insert testUser;
        
        System.runAs (testUser) {
            // 案件作成
            RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
            RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
            RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
            Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_OPWS');
            Contact con = JP_TestUtil.createTestContact(conRec.Id, acc.Id, 'test_OPWS');
            Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_OPWS:Opportunity',
                                                                System.Label.JP_Opportunity_StageName_Develop,
                                                                System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                                2013, 7, 20, null);

            // 案件製品作成
            Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, null, null);
            prd.CanUseQuantitySchedule = true;
            prd.CanUseRevenueSchedule = true;
            update prd;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry stdentry = new PricebookEntry(Pricebook2Id = pricebookId,
                                                         Product2Id = prd.Id,
                                                         CurrencyIsoCode = 'JPY',
                                                         UnitPrice = 10000,
                                                         IsActive = true);
            insert stdentry;
            Pricebook2 ctbook = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert ctbook;
            PricebookEntry testentry = new PricebookEntry(Pricebook2Id = ctbook.Id,
                                                          Product2Id = prd.Id,
                                                          CurrencyIsoCode = 'JPY',
                                                          UnitPrice = 12000,
                                                          IsActive = true);
            insert testentry;
            OpportunityLineItem lineitem = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 12, 10000);

            String IdStr = (string) opp.Id;
            OpportunityCopyWebService.oppCopyWithLineItem(IdStr);
        }

    }

    static testMethod void oppCopyWithLineItem_noLineItem() {

        /*
        Id pId = [select Id from Profile where name = 'CRM Admin Support'].Id;
        Id tu1Id = [SELECT Id FROM Territory where Name='REGION TERRITORY ASIA'].Id;
        Id tu1userId = [SELECT Id, UserId FROM UserTerritory where TerritoryId=:tu1Id LIMIT 1].UserId;
        User testuser = [select Id from User where Id = :tu1userId Limit 1];
        */
        // Create User Data
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User testUser = new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest',
                        Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr',
                        ProfileId = profile1.Id, TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                        LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert testUser;
        
        System.runAs (testUser) {
            // 案件作成
            RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
            RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
            RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
            Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_OPWS');
            Contact con = JP_TestUtil.createTestContact(conRec.Id, acc.Id, 'test_OPWS');
            Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_OPWS:Opportunity',
                                                                System.Label.JP_Opportunity_StageName_Develop,
                                                                System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                                2013, 7, 20, null);
            String IdStr = (string) opp.Id;
            OpportunityCopyWebService.oppCopyWithLineItem(IdStr);
        }

    }

    static testMethod void oppCopyWithoutLineItem() {

        /*
        Id pId = [select Id from Profile where name = 'CRM Admin Support'].Id;
        Id tu1Id = [SELECT Id FROM Territory where Name='REGION TERRITORY ASIA'].Id;
        Id tu1userId = [SELECT Id, UserId FROM UserTerritory where TerritoryId=:tu1Id LIMIT 1].UserId;
        User testuser = [select Id from User where Id = :tu1userId Limit 1];
        */
        // Create User Data
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User testUser = new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest',
                        Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr',
                        ProfileId = profile1.Id, TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                        LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert testUser;
        System.runAs (testUser) {
            // 案件作成
            RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
            RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
            RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
            Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_OPWS');
            Contact con = JP_TestUtil.createTestContact(conRec.Id, acc.Id, 'test_OPWS');
            Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_OPWS:Opportunity',
                                                                System.Label.JP_Opportunity_StageName_Develop,
                                                                System.Label.JP_Opportunity_CapitalDisposable_Disposable,
                                                                2013, 7, 20, null);
            String IdStr = (string) opp.Id;
            OpportunityCopyWebService.oppCopyWithoutLineItem(IdStr);
        }

    }

}