/****************************************************************************************
 Name    : dm_test - test all Document Management (dm) related (prefixed with dm)
 Author  : Paul Berglund
 Date    : 08/17/2016
 
 ========================
 = MODIFICATION HISTORY =
 ========================
 DATE        AUTHOR           CHANGE
 ----        ------           ------
 08/17/2016  Paul Berglund    Initial deployment to production in support of Document
                              Management application
 *****************************************************************************************/
@isTest
public class dm_test
{
	@isTest
	public static void ThrowNotBulkifiedException()
	{
		try
		{
			dmTriggerDispatcher.ThrowNotBulkifiedException();
		}
		catch (Exception ex)
		{
			system.assertEquals(ex.getMessage(), 'This trigger dispatcher is not bulkified yet - it can only handle 1 object at a time', 'Should match');
		}
	}
	
	@isTest
	public static void test1()
	{
		createApplication();
	}
	
	@isTest
	public static void test2()
	{
		Id a = createApplication();
		id f = createSubFolder(a);
		dmGroup.deleteSharing();
		dmGroup.fixSharing();
		dmFolder.deleteSharing();
		dmFolder.fixSharing();
	}
	
	@isTest
	public static void test3()
	{
		Id a = createApplication();
		Id f = createSubFolder(a);
		Id d = createDocument(f);
		dmDocument.deleteSharing();
		dmDocument.fixSharing();
		deleteDocument(d);
	}
	
	@isTest
	public static void test4()
	{
		Id a = createApplication();
		Id f = createSubFolder(a);
		Id d = createDocument(f);
		Id att = addAttachment(d);
	}
	
	//@isTest
	//public static void test5()
	//{	Id o = createOpportunity();
	//	Id s = createSamples(o);
	//	Id att = addAttachment(s);
	//}
	
	@isTest
	public static void vf1()
	{
		Id a = createApplication();
		dmGroup__c dmg = createdmGroup(a);
		
		ApexPages.StandardController sc = new ApexPages.StandardController(dmg);
		dmGroup g = new dmGroup(sc);
		
		PageReference pageRef = Page.dmGroup;
		pageRef.getParameters().put('id', string.valueOf(dmg));
		Test.setCurrentPage(pageRef);
	}

	public static Id createGroup(Id id, string name)
	{
		dmGroup__c g = new dmGroup__c();
		g.Name = name;
		g.Application__c = id;
		insert g;
		return g.Id;
	}
	
	public static dmGroup__c createdmGroup(Id id)
	{
		dmGroup__c g = new dmGroup__c();
		g.Application__c = id;
		g.Name = 'Test Group';
		insert g;
		return g;
	}
	
	public static void updatedmGroup(dmGroup__c g)
	{
		g.Name = 'Updated Test Group';
		update g;	
	}
	
	public static void deletedmGroup(dmGroup__c g)
	{
		delete g;
	}
	
	public static Id createOpportunity()
	{
		Opportunity o = new Opportunity();
		o.Name = 'Test Opportunity';
		o.StageName = 'Identify';
		o.CloseDate = system.now().date();
		insert o;
		return o.Id;
	}
	
	public static Id createApplication()
	{
    	RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'dmFolder__c' AND DeveloperName = 'Application'];
    	dmFolder__c a = new dmFolder__c();
    	a.RecordTypeId = rt.Id;
    	a.Name = 'Test Application';
    	insert a;
    	return a.Id;
	}
	
	public static Id createSubFolder(Id id)
	{
    	RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'dmFolder__c' AND DeveloperName = 'Folder'];
    	dmFolder__c f = new dmFolder__c();
    	f.RecordTypeId = rt.Id;
    	f.Parent__c = id;
    	f.Name = 'Test Folder';
    	insert f;
    	return f.Id;
	}
	
	public static Id createDocument(Id id)
    {
    	dmDocument__c d = new dmDocument__c();
    	d.Folder__c = id;
    	d.Name = 'Test Document';
    	insert d;
    	return d.Id;
    }
    
    public static Id addAttachment(Id id)
    {
    	Attachment a = new Attachment();
    	a.ParentId = id;
    	a.Name = 'test';
    	a.Body = Blob.valueOf('test document');
    	insert a;
		return a.Id;    	
    }
    
    public static void deleteDocument(Id id)
    {
    	dmDocument__c d = [SELECT Id FROM dmDocument__c WHERE Id = :id];
    	delete d;
    }
}