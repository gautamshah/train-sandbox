/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class LMS_API {
    global LMS_API() {

    }
    global static void importJSON(String jsonData, Boolean addTimeStamps) {

    }
    global static void upsertAssignments(Set<Id> selectedUserIds, Id trainingPlanId) {

    }
}
