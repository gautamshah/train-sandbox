/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
public class cpqCycleTime_uTest // This will work for the moment, rethink it when refactoring
{
    static Apttus__APTS_Agreement__c agreement;
    static Opportunity opp;
    
    @testVisible
    static void setupCycleTimeData()
    {
        //List<myTriggerManager__c> tmgrs = new List<myTriggerManager__c>();
        //tmgrs.add(new myTriggerManager__c(Name = 'Task', isActive__c = true));
        //tmgrs.add(new myTriggerManager__c(Name = 'Agreement', isActive__c = true));
        //insert tmgrs;

        Account account = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
        insert account;
        
        opp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = account.Id);
        insert opp;

		// Replace with call to cpqDeal_RecordType_c.????
        Id agreementRTId = CPQ_AgreementProcesses.getAgreementRecordTypesByNameMap().get('Hardware_Quote_Direct_AG').Id;
		// Replace with cpqAgreement_uTest.create()
        agreement = new Apttus__APTS_Agreement__c(Apttus__Related_Opportunity__c = opp.Id, RecordTypeId = agreementRTId);
        insert agreement;
    }
    
    @isTest
    public static void testAV574()
    {
        
        Test.startTest();
        cpqCycleTime_uTest.setupCycleTimeData();
        agreement.Apttus__Status__c = new cpqCycleTime_Agreement_u().FULLY_SIGNED;

        cpqCycleTime_uTest.deleteCycleTimeRecs();
       
        update agreement;
        Test.stopTest();
        
    }
    
    static void deleteCycleTimeRecs()
    {
        Map<Id, CPQ_Cycle_Time__c> Id2CycleTime = new Map<Id, CPQ_Cycle_Time__c>();
        Id2CycleTime.putAll(new Map<id, CPQ_Cycle_Time__c>(
            [SELECT Id,
                    Opportunity__c,
                    Proposal__c,
                    Agreement__c,
                    Opportunity_Created_Date__c,
                    Agreement_Enter_Signatures_Date__c,
                    Agreement_Route_Signatures_Date__c
             FROM CPQ_Cycle_Time__c
             WHERE Opportunity__c = :opp.ID OR
                   Agreement__c = :agreement.ID]));

        delete Id2CycleTime.values();
    }


}