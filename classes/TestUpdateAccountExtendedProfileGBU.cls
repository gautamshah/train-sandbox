@isTest
private class TestUpdateAccountExtendedProfileGBU {

    static testMethod void runTest() {

          Profile p = [SELECT Id FROM Profile WHERE Name='GBU Administrator']; 
          User u = new User(
          Email='standarduser@testorg.com', 
          EmailEncodingKey='UTF-8', 
          LastName='1Testing', 
          LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', 
          ProfileId = p.Id, 
          //Business_Unit__c = 'X', 
          //Franchise__c = 'X',
          TimeZoneSidKey='America/Los_Angeles', 
          Username = 'tester20131025@test.com',
          Alias = 'Tester',
          CommunityNickname = 'Tester');
    
          System.runAs(u) {
            Trigger_Profile__c setting = new Trigger_Profile__c();
            setting.Name = 'GBU_Franchise_Update';
            setting.Profile_Name__c = 'API Data Loader';
            insert setting;
             testUtility tu = new testUtility();
             Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
             
             Account_Extended_Profile__c objAEP = new Account_Extended_Profile__c();
             objAEP.Account__c = a.id;
             objAEP.S2_Quota_Bucket__c = 'AST-Endo/Energy: Stapling';
             insert objAEP;
        }
    }
    
}