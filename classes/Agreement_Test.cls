//
// Description: This test class creates the necessary code coverage for the xTriggerDispatcher
//              class and the trigger definition associated with the trigger:  Agreement
//        Note: Do not put test methods for the classes you call, just cause the trigger to fire!
//              If you want to 
//
//     Created: 02/02/2016
//      Author: Paul Berglund
//
//     History: 02/02/2016 Paul Berglund      Initial version
//              02/15/2016 Paul Berglund      Bryan converted the Setup() method from @testSetup
//                                            to a regular class and then called it from each
//                                            test method - SF was running it multiple times
//                                            within one transaction and hitting govenor limits
//
/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest(seeAllData=false)
private class Agreement_Test
{
}