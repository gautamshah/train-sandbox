@isTest
public class cpqContact_TestSetup
{
    public static List<Contact> generateContacts(List<Account> accounts, Integer quantity)
    {
        List<Contact> contacts = new List<Contact>();
        Integer aIdx = 0;
        for (Account acc : accounts) {
            for (Integer i = 0; i < quantity; i ++) {
                contacts.add(new Contact(
                    FirstName = 'Test',
                    LastName = 'Contact ' + aIdx + '-' + i,
                    AccountId = acc.Id
                ));
            }
            aIdx++;
        }
        return contacts;
    }
}