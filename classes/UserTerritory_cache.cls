public class UserTerritory_cache extends sObject_cache
{
	public static UserTerritory_cache get() { return cache; }
	
	private static UserTerritory_cache cache
	{
		get
		{
			if (cache == null)
				cache = new UserTerritory_cache();
				
			return cache;
		}
		
		private set;
	}
	
	private UserTerritory_cache()
	{
		super(UserTerritory.sObjectType, UserTerritory.field.Id);
		this.addIndex(UserTerritory.field.UserId);
		this.addIndex(UserTerritory.field.TerritoryId);
	}
}