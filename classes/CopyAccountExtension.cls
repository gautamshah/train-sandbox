public with sharing class CopyAccountExtension {
    /****************************************************************************************
    * Name    : CopyAccountExtension
    * Author  : Mike Melcher
    * Date    : 11-23-2011
    * Purpose : Creates a Sell-To (Healthcare Facility) Account from an ERP Account
    *          
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 2/29/2012    MJM             Modify functionality so an Account is no longer created.  Instead the 
    *                              ERP Record recordtype is changed to signal the integration processes that
    *                              a new Sell To account has been requested.
    *
    * 3/7/2012     MJB             ERP Record record type name changed to "ERP Record Requesting Conversion To Sell-To" (Line 43)
    *
    * 3/15/2012    MJB             ERP record type name changed to "ERP Change In Process" and added new field "In_Process_Status__c" (Line 48)
    *
    * 4/6/2012     MJM             Modified to use current user as originator of DQ Case instead of the person who last modified the ERP Record.
    *                              This will prevent the integration user from appearing as the Case Originator
    * 5/16/2012    MJM             Removed logic 
    *
    *****************************************************************************************/ 
    private final ERP_Account__c acct;
    

   
    public CopyAccountExtension(ApexPages.StandardController stdController) {
        this.acct = (ERP_Account__c)stdController.getRecord();
    }
   
    
    public pagereference copyAccount() {
       SObjectType acctObj;
       string acctQueryFields;
       List<string> acctFields = new List<string>();
       
       ERP_Account__c erpRecord = [select id, name, lastmodifiedbyId, recordtypeid
                                    from ERP_Account__c
                                    where id = :acct.Id];
                                    
       
      List<RecordType> recordtypes = [select Id from RecordType where Name = 'ERP Change In Process'];
      if (recordtypes.size() > 0) {
         erpRecord.RecordtypeId = recordtypes[0].id;
         erpRecord.In_Process_status__c = 'Sell-To Conversion Pending';         
      }
       
      update erpRecord;
        
     // Create Case to notify Data Quality
        
     DataQuality dq = new DataQuality();
     Id currentUser = UserInfo.getUserId();
     dq.createCase('New Parent Sell-To Account Requested for ERP Record ' + erpRecord.Name,erpRecord.Id,'Data Quality','Medium', currentUser,'Validation','Assigned');
   
        
     PageReference acctPage = new ApexPages.StandardController(erpRecord).view();
     acctPage.setRedirect(true);
     return acctPage;
    }
}