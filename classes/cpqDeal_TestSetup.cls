@isTest
public class cpqDeal_TestSetup extends cpq_TestSetup
{
    // Creates Proposal or Agreement CPQ Scenario
    public static void buildScenario_CPQ_Deal (Schema.SObjectType sObjectType, String recordTypeName)
    {
        // 2017.01.24 - IL - Refactored to provide override of organization name
        // Default to SSG
        buildScenario_CPQ_Deal(
        	SObjectType,
        	recordTypeName,
        	OrganizationNames_g.Abbreviations.SSG);
    }
 
    // Creates Proposal or Agreement CPQ Scenario
    public static void buildScenario_CPQ_Deal (
        Schema.SObjectType sObjectType,
        String recordTypeName,
        OrganizationNames_g.Abbreviations testOrg
        )
    {
        cpq_TestSetup.org = testOrg;
        cpqOpportunity_TestSetup.buildScenario_CPQ_Opportunity();
        
        if (sObjectType == cpq_u.AGREEMENT_SOBJECT_TYPE)
        {
            Apttus__APTS_Agreement__c agmt = 
                cpqAgreement_TestSetup.generateAgreement(
                    cpq_TestSetup.testAccounts.get(0),
                    cpq_TestSetup.testContacts.get(0),
                    cpq_TestSetup.testOpportunities.get(0),
                    recordTypeName);
            cpq_TestSetup.testAgreements.add(agmt);
            
            insert cpq_TestSetup.testAgreements;

            Apttus_Config2__ProductConfiguration__c pc =
            	cpqProductConfiguration_TestSetup.generateProductConfiguration(cpqPriceList_c.RMS, agmt);
            	
            cpq_TestSetup.testProductConfigurations.add(pc);
            
            insert cpq_TestSetup.testProductConfigurations;
        }

        if (sObjectType == cpq_u.PROPOSAL_SOBJECT_TYPE)
        {
            Apttus_Proposal__Proposal__c prop =
                cpqProposal_TestSetup.generateProposal(
                    cpq_TestSetup.testAccounts.get(0),
                    cpq_TestSetup.testContacts.get(0),
                    cpq_TestSetup.testOpportunities.get(0),
                    recordTypeName);
            cpq_TestSetup.testProposals.add(prop);
            
            insert cpq_TestSetup.testProposals;

            Apttus_Config2__ProductConfiguration__c pc =
            	cpqProductConfiguration_TestSetup.generateProductConfiguration(
            		cpqPriceList_c.RMS,
            		prop);
            		
            cpq_TestSetup.testProductConfigurations.add(pc);
            
            insert cpq_TestSetup.testProductConfigurations;
        }
    }
}