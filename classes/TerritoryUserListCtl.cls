public class TerritoryUserListCtl{
/*****************************************************************************************
* Name    : xRef - Territory User List Controll
* Author  : Naoya Kotani
* Date    : 08/20/2014
* Outline : This apex class is TerritoryUser List View in Account Page Layout
*****************************************************************************************/
    public Account acct{get;set;}
    public String acctID{get;set;}
    public String buName{get;set;}
    public List<usrDTO> usrList{get;set;}

    // User DaTa Object
    public class usrDTO{
        public String usrID{get;set;}
        public String usrEpN{get;set;}
        public String usrNameJPN{get;set;}
        public String usrNameENG{get;set;}
        public String usrBU{get;set;}
        public String usrDiv{get;set;}
        public String usrDep{get;set;}
        public String usrSPh{get;set;}
        public String usrMPh{get;set;}
        public String usrEMl{get;set;}

        public usrDTO(){
            usrID      = '';
            usrEpN     = '';
            usrNameJPN = '';
            usrNameENG = '';
            usrBU      = '';
            usrDiv     = '';
            usrDep     = '';
            usrSPh     = '';
            usrMPh     = '';
            usrEMl     = '';
        }
    }

    public TerritoryUserListCtl(ApexPages.StandardController ctlr){
        acct = (Account)ctlr.getRecord();
        acctID = acct.Id;
        List<String> ugrpIdList = new List<String>();
        List<AccountShare> acctShareL = [SELECT id, userOrGroupId, RowCause FROM AccountShare WHERE AccountId = :acctID AND RowCause = 'TerritoryManual'];
        for (AccountShare aAcctShare: acctShareL) {
            ugrpIdList.add((String)aAcctShare.get('userOrGroupId'));
        }

        List<String> rltdIdList = new List<String>();

        List<Group> gList = [select Id, RelatedId from Group where Type = 'Territory' and Id IN :ugrpIdList];
        for (Group aRltdId: gList) {
            rltdIdList.add((String)aRltdId.get('RelatedId'));
        }
        List<UserTerritory> utList = new List<UserTerritory>();

        utList = [select UserID from UserTerritory where IsActive = TRUE AND TerritoryId IN : rltdIdList];
        List<String> xxl = new List<String>();
        for (UserTerritory utL: utList) {
            xxl.add((String)utL.get('UserID'));
        }

        If (utList.size() <= 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'この病院の担当者はいません'));
        }

        usrList = new List<usrDTO>();
        List<User> uMapList = new List<User>([SELECT ID, Alias, Department, Division, SmallPhotoUrl , EmployeeNumber, MobilePhone, Franchise__c, Name, Phone FROM User WHERE IsActive = TRUE AND ID IN :xxl order by  Franchise__c, Division, Department,Name]);

        for (User utL: uMapList) {
            usrDTO uDTO = new usrDTO();
            uDTO.usrID      = (String)utL.get('ID');
            uDTO.usrEpN     = (String)utL.get('EmployeeNumber');
            uDTO.usrNameJPN = (String)utL.get('Alias');
            uDTO.usrNameENG = (String)utL.get('Name');
            uDTO.usrBU      = (String)utL.get('Franchise__c');
            uDTO.usrDiv     = (String)utL.get('Division');
            uDTO.usrDep     = (String)utL.get('Department');
            uDTO.usrSPh     = (String)utL.get('Phone');
            uDTO.usrMPh     = (String) utL.get('MobilePhone');
            uDTO.usrEMl     = (String)utL.get('SmallPhotoUrl');
            usrList.add(uDTO);
        }
    }
}