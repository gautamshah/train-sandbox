@isTest
private class PersonalInitiative_TriggerHandler_Test 
{
	private static Opportunity GetOpportunityTestRecord(string recordTypeName)
	{
		Opportunity testOp = new Opportunity(
			Name = 'Test Opp',
			RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Opportunity').Get(recordTypeName).Id,
			Type = 'New Customer - Conversion',
			Capital_Disposable__c = 'Disposable',
			StageName = 'Identify',
			CloseDate = Date.today(),
			Amount = 500
		);
		return testOp;
	}
	
	static testMethod void TestRollup() 
	{
		Test.startTest();
		Personal_Initiative__c pi = new Personal_Initiative__c(
			Name = 'Test Persona Initiative',
			Amount_Roll_up__c = 0
		);
		insert pi;
		system.assert(pi.Amount_Roll_up__c == 0, 'pi.Amount_Roll_up__c should be 0: ' + pi.Amount_Roll_up__c);
		/*update pi;
		system.assert(pi.Amount_Roll_up__c == 0, 'pi.Amount_Roll_up__c should be 0: ' + pi.Amount_Roll_up__c);
		*/Opportunity o = GetOpportunityTestRecord('US_AST_Opportunity');
		o.Personal_Initiative__c = [select Id FROM Personal_Initiative__c LIMIT 1].Id;
		o.Amount = 500;
		insert o;
		system.assert(o.Amount == 500, 'o.Amount should be 500: ' + pi.Amount_Roll_up__c);
		Opportunity o2 = GetOpportunityTestRecord('US_AST_Opportunity');
		o2.Personal_Initiative__c = [select Id FROM Personal_Initiative__c LIMIT 1].Id;
		o2.Amount = 500;
		insert o2;
		o2.Amount = 700;
		update o2;
		delete o2;
		//system.assert(pi.Amount_Roll_up__c == 500, 'pi.Amount_Roll_up__c should be 500: ' + pi.Amount_Roll_up__c);
		Test.stopTest();
	}
}