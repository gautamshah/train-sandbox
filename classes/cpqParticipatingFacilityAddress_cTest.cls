@isTest
public class cpqParticipatingFacilityAddress_cTest {
	
   static Apttus_Proposal__Proposal__c proposal;
   
   public static Participating_Facility_Address__c setup()
   {
   	   proposal = new Apttus_Proposal__Proposal__c();
       insert proposal;
    
      ERP_Account__c erpRec = new ERP_Account__c(name = 'test Account Rec', ERP_ID__c = 'shiptToId');
      insert erpRec;

      Participating_Facility_Address__c mockFacility = 
            new Participating_Facility_Address__c(ERP_Record__c = erpRec.ID, Proposal__c = proposal.ID);
      insert mockFacility;
      
       mockFacility = [select ID, Name_Template__c, Shipto_Number_Template__c, ERP_Record__c from Participating_Facility_Address__c where id = :mockFacility.id ];
       return mockFacility;
   }
   
   @isTest static void testFetchMap()
   {
   	  setup();
   	  Map<Id,Participating_Facility_Address__c> testMap = cpqParticipatingFacilityAddress_c.fetchMap(proposal.ID);
   	  System.Assert(testMap.size() == 1);
   }
   
   @isTest static void testGetDisplayName()
   {
       Participating_Facility_Address__c mockFacility = setup();
       String val = cpqParticipatingFacilityAddress_c.getDisplayName(mockFacility);
       System.assertEquals(val, 'test Account Rec (shiptToId)');
   }
   
    
   @isTest static void testFetch()
   {
   	  setup();
   	  List<Participating_Facility_Address__c> participatingFacilityList = cpqParticipatingFacilityAddress_c.fetch(proposal.ID);
   	  System.assertEquals(participatingFacilityList.size(), 1);
   	  System.assertEquals(participatingFacilityList.get(0).Name_Template__c, 'test Account Rec');

   }
   
}