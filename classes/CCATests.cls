/****************************************************************************************
 * Name    : CCATests
 * Author  : John Romanowski
 * Date    : 24/04/2017 
 * Purpose : Test Contract Compliance Automation (CCA) features
 * Dependencies: 
 *****************************************************************************************/
@isTest
public class CCATests {

    //Process Builder test
    @isTest static void TestProcessBuilder() {
        System.debug('Process Builder check');
        
        // Create test Agreement
        Agreement__c agree = CreateAgreementObject();
        
        // Create test Compliance Requirement
        Compliance_Requirement__c requirement = CreateComplianceRequirementObject(agree);
        Compliance_Requirement__c requireTest = [SELECT Id,CaseClosedDate__c,CaseCreated__c,Out_of_Compliance__c FROM Compliance_Requirement__c WHERE Id = :requirement.id];  
        System.assert(requireTest.CaseClosedDate__c == null);
        System.assert(requireTest.CaseCreated__c == FALSE);
        System.assert(requireTest.Out_of_Compliance__c == FALSE);
        
        //Set out of compliance
        requirement.Out_of_Compliance__c = TRUE;
        requirement.RegionManager__c = [SELECT Id FROM user WHERE Name = 'Informatica Cloud' LIMIT 1].Id;
        UPDATE requirement;
        requireTest = [SELECT Id,CaseClosedDate__c,CaseCreated__c,Out_of_Compliance__c FROM Compliance_Requirement__c WHERE Id = :requirement.id];  
        System.assert(requireTest.CaseClosedDate__c == null);
        System.assert(requireTest.CaseCreated__c == TRUE);
        System.assert(requireTest.Out_of_Compliance__c == TRUE);

        //count Cases associated with this requirment 
        Integer cnt = [SELECT count() FROM Case WHERE Compliance_Requirement__c = :requirement.id];
        System.assert(cnt == 1);
    }
    
     @isTest static void TestCaseValidations() {
         System.debug('Case validation checks');
         
        // Create test Agreement
        Agreement__c agree = CreateAgreementObject();
        
        // Create test Compliance Requirement
        Compliance_Requirement__c requirement = CreateComplianceRequirementObject(agree);

        //Update compliance requirement to create a case  
        requirement.Out_of_Compliance__c = TRUE;
		requirement.RegionManager__c = [SELECT Id FROM user WHERE Name = 'Informatica Cloud' LIMIT 1].Id;
        UPDATE requirement;

        //VALIDATION CHECK: Need to accept case first before Get_Well_Plan_recovery__c
        case theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Get_Well_Plan_recovery__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Get_Well_Plan_recovery'); 
         
        //VALIDATION CHECK: Need to accept case first before Extend_Get_Well_Plan__c
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Extend_Get_Well_Plan__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Extend_Get_Well_Plan'); 

        //VALIDATION CHECK: Need to accept case first before Adjust_Pricing__c
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Adjust_Pricing__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Adjust_Pricing'); 

        //VALIDATION CHECK: Need to accept case first before Adjust_Commitment__c
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Adjust_Commitment__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Adjust_Commitment'); 
                
		//VALIDATION CHECK: Need to accept case first before Watch_and_Wait__c
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Watch_and_Wait__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Watch_and_Wait'); 

		//VALIDATION CHECK: Need to accept case first before Terminate_Contract__c
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Terminate_Contract__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Terminate_Contract'); 

		//VALIDATION CHECK: Need to accept case first before Review_data_integrity__c
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Review_data_integrity__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, case was not accepted Review_data_integrity'); 
                      
        //Accept case (manually)
        theCase.Status = 'In progress: Case accepted, in review';
        update theCase;
         
        //VALIDATION CHECK: Cannot_Check_Extend_Get_Well_plan
        //Cannot Check Extend Get Well plan unless a Get Well plan has already been selected	
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Extend_Get_Well_Plan__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot Check Extend Get Well plan unless a Get Well plan has already been selected'); 

        //VALIDATION CHECK: Cannot_Check_Extend_Get_Well_plan
        //Cannot Check Extend Get Well plan unless a Get Well plan has already been selected	
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Extend_Get_Well_Plan__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot Check Extend Get Well plan unless a Get Well plan has already been selected'); 
 		
        //Update to Get well plan
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Get_Well_Plan_recovery__c = TRUE;
		update theCase;      
        
        //VALIDATION CHECK:Cannot_Check_Watch_And_Wait	
		//Cannot select Watch And Wait if a Get Well Plan has been checked
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Watch_and_Wait__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot select Watch And Wait if a Get Well Plan has been checked'); 
 		
        //Terminate contract
        theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Terminate_Contract__c = TRUE;
        update theCase;
         
        //VALIDATION CHECK:Cannot_Check_Adjust_Commit_Terminate_Co
		//Cannot Check Adjust Commitment if Terminate Contract has been checked 
		theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Adjust_Commitment__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot Check Adjust Commitment if Terminate Contract has been checked'); 
 		
        //VALIDATION CHECK:Cannot_Check_Watch_And_Wait_Terminate_Co
		//Cannot Check Watch And Wait if Terminate Contract has been checked
		theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Watch_and_Wait__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot Check Watch And Wait if Terminate Contract has been checked'); 

        //VALIDATION CHECK:Cannot_Check_Extend_Get_Well_plan_Termco
		//Cannot Check Extend Get Well plan if Terminate Contract is already checked
		theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        theCase.Extend_Get_Well_Plan__c = TRUE;
        system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot Check Extend Get Well plan if Terminate Contract is already checked');
         
		//VALIDATION CHECK:Cannot_Check_Get_Well_Plan_Terminate_Co
		//Cannot Check Get Well Plan if Terminate Contract has been checked
		//theCase = [SELECT Id FROM Case WHERE Compliance_Requirement__c = :requirement.id LIMIT 1];
        //theCase.Get_Well_Plan_recovery__c = TRUE;
        //system.assert(!CanUpdateCase(theCase), 'Update should fail, Cannot Check Get Well Plan if Terminate Contract has been checked');
                  
     }
    
    //test the formula field for Compliance_Requirement__c.CanOpenCase
    @isTest static void TestFormulaCanOpenCase() {
        System.debug('Compliance_Requirement__c.CanOpenCase formula check');
        
        // Create test Agreement
        Agreement__c agree = CreateAgreementObject();
        
        // Create test Compliance Requirement
        Compliance_Requirement__c requirement = CreateComplianceRequirementObject(agree);
        
        //CanOpenCase__c true when CaseClosedDate__c is blank
        Compliance_Requirement__c requireTest = [SELECT Id, CaseClosedDate__c,CanOpenCase__c FROM Compliance_Requirement__c WHERE Id = :requirement.id];  
        System.assert(requireTest.CaseClosedDate__c == null); 
        System.assert(requireTest.CanOpenCase__c == TRUE);
        
        //Set the CaseClosedDate__c to a date within 30 days of today
        //CanOpenCase__c should be FALSE
        requirement.CaseClosedDate__c = date.today();
        update requirement;
        requireTest = [SELECT Id, CaseClosedDate__c,CanOpenCase__c FROM Compliance_Requirement__c WHERE Id = :requirement.id];  
        System.assert(requireTest.CaseClosedDate__c == date.today()); 
        System.assert(requireTest.CanOpenCase__c == FALSE);
        
        //Set the CaseClosedDate__c to a date less than 30 days from today
        //CanOpenCase__c should be TRUE
        requirement.CaseClosedDate__c = date.today() - 35;
        update requirement;
        requireTest = [SELECT Id, CaseClosedDate__c,CanOpenCase__c FROM Compliance_Requirement__c WHERE Id = :requirement.id];  
        System.assert(requireTest.CaseClosedDate__c == date.today() - 35); 
        System.assert(requireTest.CanOpenCase__c == TRUE); 
    }
    
    //Helper functions
    //Create Agreement
    private static Agreement__c CreateAgreementObject(){  
        Agreement__c agree = new Agreement__c(Name = 'Test Agreement');
        insert agree;
        return agree;
    } 
    //Create Compliance Requirement
    private static Compliance_Requirement__c CreateComplianceRequirementObject(Agreement__c agree){ 
        Compliance_Requirement__c requirement = new Compliance_Requirement__c(
           Name = 'Test Requirement'
           ,Compliance_ID__c = agree.Id
           ,CurrencyIsoCode = 'USD'
        );
        insert requirement; 
        return requirement;
    }
    //Try to update case
    private static Boolean CanUpdateCase(case theCase)
    {
        Boolean ret = TRUE;
        try{update theCase;
         }catch (Exception ex) {
             ret = FALSE;
         }
        return ret;
    }
}