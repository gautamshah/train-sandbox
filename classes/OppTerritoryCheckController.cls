public without sharing class OppTerritoryCheckController {
 /****************************************************************************************
     * Name    : OppTerritoryCheckController  
     * Author  : Mihir Shah
     * Date    : 10/1/2012  
     * Purpose : To Perform a check for the opportunity Territoty Selection Process.
     *
     * Dependencies: 
     *
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 11/15/2012   Mihir        Added the login to capture manually assigned territories for the accounts from the AccountShare Object
     * 10/12/2015   Namita Pai   Modified getNoOverrideURL() in order to get Display Access button working. (Added code from Line#118 to Line #130)
     * 10/19/2015   Bill Shan	 only assign opp to territories that matched both account and user criteria.
     *
     *****************************************************************************************/

    Opportunity o {get;set;}
    public List<UserTerritory> lst_matchedTerr{get;set;}
    public List<UserTerritory> lst_notMatchedTerr{get;set;}
    public String selectedTerrId {get;set;}
    public Map<Id,Territory> map_Territory {get;set;}
    public List<Id> lsTerrIds {get;set;}
    
    public OppTerritoryCheckController(ApexPages.StandardController con) {
        
        if(!Test.isRunningTest())
        	con.addFields(new String[]{'CreatedDate','LastModifiedDate','AccountId','OwnerId','TerritoryId','DoNotByPassTerritorySelection__c','Owner_Region__c'});     
        o = (Opportunity) con.getRecord();
    }

    public PageReference init()
    {
        PageReference p;
        
        if(!o.DoNotByPassTerritorySelection__c || o.OwnerId!= UserInfo.getUserId())
        {
            return getNoOverrideURL();
        }
        else
        {
            // get all the territoroes associated with the account of the opportunity
			Set<Id> setAccTerrIds = new Set<Id>();
            Set<String> set_GroupId = new Set<String>();   
           	for(AccountShare at:[Select UserOrGroupId from AccountShare where AccountId =:o.AccountId and RowCause = 'TerritoryManual'])
            	set_GroupId.add(at.UserOrGroupId);
            
            Set<String> setRegions_IgnoreAccountTerritory = new Set<String>();
            Set<String> setRegions_IgnoreUserTerritory = new Set<String>();
            for(Regional_Variables__c rv : Regional_Variables__c.getAll().values())
            {
                if(rv.Ignore_Account_Territory__c)
                    setRegions_IgnoreAccountTerritory.add(rv.Name);
                if(rv.Ignore_User_Territory__c)
                    setRegions_IgnoreUserTerritory.add(rv.Name);
            }
            
            if(set_GroupId.size()>0)
            {
            	Set<String> Open_TerrExternalIds = new Set<String>();
                for(Open_Territory__c ot : Open_Territory__c.getAll().values())
                {
                    Open_TerrExternalIds.add(ot.Territory_External_ID__c);
                }
                
                for(Group at:[Select RelatedId from Group where Id in: set_GroupId And
                          					RelatedId not in (select Id from Territory where Custom_External_TerritoryID__c = :Open_TerrExternalIds)])
                    setAccTerrIds.add(at.RelatedId);    
                
                if(setAccTerrIds.size()>0)
                {
                    Set<Id> set_TerrIds = new Set<Id>();
                    for(UserTerritory ut:[Select TerritoryId from UserTerritory where UserId=:o.OwnerId And isActive = True And TerritoryId in :setAccTerrIds])
                        set_TerrIds.add(ut.TerritoryId);
                    if(set_TerrIds.size()==1)
                    {
                        lsTerrIds = new List<Id>(set_TerrIds);
                        o.TerritoryId = lsTerrIds[0];
                        o.DoNotByPassTerritorySelection__c = false;
                        update o;
            			return getNoOverrideURL();
                    }
                    else if(set_TerrIds.size()>1)
                    {                       
                        lsTerrIds = new List<Id>(set_TerrIds);
                		map_Territory = new Map<Id,Territory>([Select Id,Name, Custom_External_TerritoryID__c from Territory where Id In:set_TerrIds]);
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 
                                                       'You are currently associated to more than one territories for this account.  Please select the correct territory to associate with this opportunity.'));
						return null;
                    }
                    else if(setRegions_IgnoreUserTerritory.contains(o.Owner_Region__c))
                    {
                        lsTerrIds = new List<Id>(setAccTerrIds);
                		map_Territory = new Map<Id,Territory>([Select Id,Name, Custom_External_TerritoryID__c from Territory where Id In:setAccTerrIds]);
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 
                                                       'The opportunity account is not part of your territory. Please select the correct territory to associate with this opportunity.'));
                        
						return null;
                    }
                }
            }
            
            if(setRegions_IgnoreAccountTerritory.contains(o.Owner_Region__c))
            {
                Set<Id> set_TerrIds = new Set<Id>();
                for(UserTerritory ut:[Select TerritoryId from UserTerritory where UserId=:o.OwnerId And isActive = True])
                        set_TerrIds.add(ut.TerritoryId);
                if(set_TerrIds.size()==1)
                {
                    lsTerrIds = new List<Id>(set_TerrIds);
                    o.TerritoryId = lsTerrIds[0];
                    o.DoNotByPassTerritorySelection__c = false;
                    update o;
                    return getNoOverrideURL();
                }
                else if(set_TerrIds.size()>1)
                {
                    lsTerrIds = new List<Id>(set_TerrIds);
                    map_Territory = new Map<Id,Territory>([Select Id,Name, Custom_External_TerritoryID__c from Territory where Id In:set_TerrIds]);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 
                                                           'You are currently associated to more than one territories.  Please select the correct territory to associate with this opportunity.'));
                            
                    return null;
                }
            }
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 
                                                       'No territory found for the opportunity. Please click save to continue.'));
            return null;         
        }
    }
    
    public PageReference getNoOverrideURL()
    {
        //PageReference p = new PageReference('/'+o.Id+'?nooverride=1'); 
        //p.setRedirect(true);
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        String VfURL = ApexPages.currentPage().getUrl();
        String pageURL = baseURL+VfURL;
        if(!pageURL.contains('displayTrueTeamAccess')){
            Pagereference p = new Pagereference('/'+o.id+'?nooverride=1');
            p.setRedirect(true);
            return p;
        }
        else{
            Pagereference p = new Pagereference('/'+o.id+'?nooverride=1&displayTrueTeamAccess=1#'+o.id+'_RelatedOpportunitySalesTeam');
            p.setRedirect(false);
            return p;
        }              
    }
    
    public PageReference saveTerritory()
    {
    	if(selectedTerrId==null || selectedTerrId=='' && map_Territory!=null && map_Territory.size()>0)
        {
        	return getNoOverrideURL();
        }
        else if(map_Territory!= null && map_Territory.size()>0)
        {
            o.TerritoryId = selectedTerrId;
            o.DoNotByPassTerritorySelection__c = false;
            update o;
        }
        return getNoOverrideURL();
    }
}