@istest
        Public class PreventingAdvancementOfRMSOpp_Test {
       /****************************************************************************************
        * Name    : PreventingAdvancementOfRMSOpp_Test
        * Author  : Lakhan Dubey
        * Date    : 16/04/2015
        * Purpose : Test class for trigger PreventingAdvancementOfRMSOpp
        *****************************************************************************************/

        static  testmethod void createopp(){
        
        String opp1StageName;
        Boolean expectedExceptionThrown;
        Boolean expectedExceptionThrown1;
        Profile p = [SELECT Id FROM Profile WHERE Name='US - RMS']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='s11tandarduser@testorg.com');

        System.runAs(u) {
            Account acc =new Account(name='testa',phone='88888888',BillingPostalCode='2333',billingstate='us',billingstreet='tststreet',billingcity='portland',Website='www',BillingCountry='Mexico');
            insert acc;

            Contact con =new Contact(lastname='tectc',AccountId=acc.id , Department_picklist__c='3rd Party Biller',Connected_As__c='Adjunct Professor');
            insert con;
            
            Opportunity opp1 =new Opportunity(Name='test1',Capital_Disposable__c='Capital',Type='New Customer',StageName='Identify',ForecastCategoryName='Pipeline',RM_Forecast_Status__c='Working',CloseDate=system.Today(),AccountId=acc.id,Financial_Program__c='_No Program',Promotion_Program__c='Shamrock - PM');
            insert opp1;
            
            OpportunityContactRole ocr=new OpportunityContactRole(ContactId=con.id,OpportunityId=opp1.id,role='Economic Buyer');
            insert ocr;
            
            opp1.StageName='Negotiate';
            
            
            Opportunity opp2 =new Opportunity(Name='test2',Capital_Disposable__c='Capital',Type='New Customer',StageName='Identify',ForecastCategoryName='Pipeline',RM_Forecast_Status__c='Working',CloseDate=system.Today(),AccountId=acc.id,Financial_Program__c='_No Program',Promotion_Program__c='Shamrock - PM');
            insert opp2;
            opp2.StageName='Negotiate'; 
            
            Opportunity opp3 =new Opportunity(Name='test3',Capital_Disposable__c='Capital',Type='New Customer',StageName='Negotiate',ForecastCategoryName='Pipeline',RM_Forecast_Status__c='Working',CloseDate=system.Today(),AccountId=acc.id,Financial_Program__c='_No Program',Promotion_Program__c='Shamrock - PM');
            
            Test.starttest();
               
            try{
            update opp1;
            update opp2;
            
        }
        Catch(exception e){
        opp1StageName=opp1.StageName;


      expectedExceptionThrown =  e.getMessage().contains('No Opportunity in the system can be set to Evaluate, Propose, Negotiate, Closed Won, or Closed Lost without a Contact in a Contact Role on the Opportunity. The Opportunity  can be created or saved in the Identify, Develop or Closed Cancelled stages.');
      try    { 
      insert  opp3;
      }
      Catch(exception ex){
      expectedExceptionThrown1 =  ex.getMessage().contains('No Opportunity in the system can be set to Evaluate, Propose, Negotiate, Closed Won, or Closed Lost without a Contact in a Contact Role on the Opportunity. The Opportunity  can be created or saved in the Identify, Develop or Closed Cancelled stages.');
      
      }
        }
        Test.stoptest();
        }

        System.AssertEquals(opp1StageName , 'Negotiate');
        System.AssertEquals(expectedExceptionThrown,true);
        System.AssertEquals(expectedExceptionThrown1,true);
        
    }
}