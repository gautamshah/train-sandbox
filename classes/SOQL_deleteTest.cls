/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20170123        IL         AV-280     Created. To increase coverage on dependencies blocking AV-280.

Notes:
1.  If there are multiple related Jiras, add them on the next line
2.  Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
@isTest 
private class SOQL_deleteTest {

    @isTest static void constructor () {

        List<Account> accs = cpqAccount_TestSetup.generateAccounts(2);
        insert accs;

        SOQL_delete del;

        // Null Constructor
        del = new SOQL_delete(null);

        // Regular Constructor
        del = new SOQL_delete(accs);

    }

    @isTest static void remove () {

        List<Account> accs = cpqAccount_TestSetup.generateAccounts(200);
        insert accs;

        SOQL_delete del;
        List<String> errors;
        AsyncApexJob job;

        Test.startTest();

            // Return empty
            del = new SOQL_delete(NULL);
            errors = del.execute();
            System.assertEquals(null, errors);

            // Return errors
            del = new SOQL_delete(new List<Account>());
            errors = del.execute();
            System.assertEquals(1, errors.size());
            ////System.assertEquals(SOQL_delete.ERR_NONE_FOUND, errors.get(0));

            // Remove Non-Queueable
            del = new SOQL_delete(accs);
            errors = del.execute();

        Test.stopTest();

        System.assertEquals(0, [SELECT COUNT() FROM Account]);

    }

    @isTest static void status () {

        // Job id returns null test context
        // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_queueing_jobs.htm

        List<Account> accs = cpqAccount_TestSetup.generateAccounts(2);
        insert accs;
        SOQL_delete del = new SOQL_delete(accs);
        System.assertEquals(null, del.status());

    }

    // @isTest
    // static void sObject_uTest.remove()
    // {
    //  sObject_uTest concrete = new sObject_uTest();
    //  concrete.addAll(testData());
    //
    //  List<string> errors = concrete.save();
    //
    //  sObject_uTest results = new sObject_uTest(Account.sObjectType, null);
    //  List<string> delErrors = results.remove();
    //
    //  system.assertEquals(delErrors, new List<string>{'total 1', 'number to be deleted 1', 'not deleted 0'}, delErrors);
    //
    //  string status = results.checkStatus();
    //  system.assertEquals(null, status, 'Should be done');
    // }
    //
    // public virtual List<string> sObject_u.remove()
    // {
    //  del = new SOQL_delete(this.sobjs);
    //  return del.remove();
    // }

}