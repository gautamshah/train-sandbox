global class UserTerritory_g extends sObject_g
{
	global UserTerritory_g()
	{
		super(UserTerritory_cache.get());
	}

	////
	//// cast
	////
	global static Map<Id, UserTerritory> cast(Map<Id, sObject> sobjs)
	{
		try
		{
			return new Map<Id, UserTerritory>((List<UserTerritory>)sobjs.values());
		}
		catch(Exception ex)
		{
			return new Map<Id, UserTerritory>();
		}
	}

	global static Map<object, Map<Id, UserTerritory>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, UserTerritory>> target = new Map<object, Map<Id, UserTerritory>>();
		if (source != null)
			for(object key : source.keySet())
				target.put(key, cast(source.get(key)));
			
		return target;
	}

	////
	//// fetch sets
	////
	global Map<object, Map<Id, UserTerritory>> fetch(sObjectField field, set<object> keys)
	{
		Map<object, Map<Id, sObject>> result = this.cache.fetch(field, keys);
		return cast(result);
	}
}