/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestInvoiceUploadRequest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',BillingCountry='SG',Submission_Offset_Day__c=null);
        insert acc;
        
        RecordType crt=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        Contact c = new Contact(RecordTypeID = crt.id, AccountId = acc.Id,Type__c = 'DIS Contact', Email='test@covidien.com', LastName='test');
        insert c;
        
        Profile asiaDist = [Select Id From Profile Where Name = 'Asia Distributor - KR' Limit 1];
        //UserRole asiaRole = [Select Id From UserRole Where Name = 'China' Limit 1];
        User runningUser = new User();
        runningUser.FirstName = 'Tester';
        runningUser.LastName = 'Tester';
        runningUser.Username = 'tester20131025@test.com';
        runningUser.Email = 'tester@test.com';
        runningUser.Alias = 'Tester';
        runningUser.CommunityNickname = 'Tester';
        runningUser.TimeZoneSidKey = 'America/New_York';
        runningUser.EmailEncodingKey = 'UTF-8';
        runningUser.LocaleSidKey = 'en_US';
        runningUser.LanguageLocaleKey = 'en_US';
        runningUser.ProfileId = asiaDist.Id;
        runningUser.ContactId = c.Id;
        runningUser.Asia_Team_Asia_use_only__c = 'PRM Team';
        //runningUser.UserRoleId = asiaRole.Id;
        insert runningUser;
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
   //  this should be commented out as this is will be created by the batch job
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id);
        insert cp;
        
        Invoice_Upload_Request__c iur = new Invoice_Upload_Request__c(Distributor_Name__c = acc.Id, Submission__c = cp.Id, Details__c = '4545');
        insert iur; 
    }
}