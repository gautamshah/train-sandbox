@isTest
public class ManageReportOfInActiveUserCTest {
/****************************************************************************************
    * Name    : ManageReportOfInActiveUserCTest
    * Author  : Brajmohan Sharma
    * Date    : 23-Mar-2017
    * Purpose :
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/ 
  @testSetup
  static void Case1InsertUser()
    {    
    Profile P = [select id from profile where name = 'system administrator'];
    String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
    User tuser = new User(  firstname = 'testcase',
                            lastName = 'data',
                            email = uniqueName + '@test' + orgId + '.org',
                            Username = uniqueName + '@test' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = P.Id);
                            //UserRoleId = roleId);
                     insert tuser; 
       List<User> idf = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        String id = idf.get(0).id;
        List<CronTrigger> jobs = ManageReportOfInActiveUserC.getJobs(id);
        List<SheduleRunReport__c> srrlist = new List<SheduleRunReport__c>();
        for(Crontrigger ct:jobs){
        List<Report> repot = [select DeveloperName,CreatedById, CreatedBy.Name FROM Report where ID =:ct.CronJobDetail.Name];
        SheduleRunReport__c srr = new SheduleRunReport__c();   //persist data to schedule report
           List<User> userlist = [select Name FROM User where ID =:id];
           srr.RUserId__c = userlist.get(0).name;
           srr.Name ='CA MED Closed Lost Opportunities'; //repot.get(0).DeveloperName;
           srr.CreatedUserName__c = 'Brajmohan Sharma';//repot.get(0).CreatedBy.Name;   
           srr.EndDate__c = ct.EndTime;
           srr.StartDate__c = ct.StartTime;
           srr.CronExpression__c = ct.CronExpression;
           srr.NextFireDate__c = ct.NextFireTime;  
           srrlist.add(srr);
        }
           insert srrlist;
       
        
       }
     static testMethod void Case2checkReportAvail(){
        List<User> idd = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        Boolean value = ManageReportOfInActiveUserC.checkReportAvail(idd.get(0).id);
        List<CronTrigger> listcount = [SELECT Id FROM CronTrigger WHERE CronJobDetail.JobType = '8' AND OwnerId =: idd.get(0).id];
}//Case2getUser
    static testMethod void Case3getUser(){
        List<User> ctlist = ManageReportOfInActiveUserC.getUser();
        List<User> id = [select id,isActive from user where firstname = 'Sinem' and lastName = 'Mavi'];
        List<User> ctlist1 = [Select id from user where ID IN (SELECT OwnerId FROM CronTrigger WHERE CronJobDetail.JobType = '8') AND (isActive=False)];
        system.assertEquals(ctlist1, ctlist);
     }//Case2getUser
    static testMethod void Case4getJobs(){
        List<User> idq = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        User u = idq.get(0);
        u.isActive = true;
        update u;
        List<User> idd = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        List<CronTrigger> ctlist = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name,CronExpression, CronJobDetail.JobType,EndTime,NextFireTime,PreviousFireTime,StartTime,State FROM CronTrigger where OwnerId =: idd.get(0).id AND CronJobDetail.JobType = '8'];
        List<CronTrigger> ctjlist = ManageReportOfInActiveUserC.getJobs(idd.get(0).id);
        system.assertEquals(ctlist,ctjlist);
     }//Case3getJobs
   
    static testMethod void Case5sdlJobInsert(){
        List<User> ide = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        String idd = ide.get(0).id;
        ManageReportOfInActiveUserC.sdlJobInsert(idd);
     }//Case6sdlJobInsert
    static testMethod void Case6UnSheduleReport(){
        List<User> ide = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        User u = ide.get(0);
        u.isActive = false;
        update u;
        List<User> idf = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        String uid = idf.get(0).id;
        List<CronTrigger> jobs = ManageReportOfInActiveUserC.getJobs(u.id);
        User u1 = ide.get(0);
        u1.isActive = true;
        update u1;
        ManageReportOfInActiveUserC.unSheduleReport(uid);
     }//Case6UnSheduleReport
    static testMethod void Case7InsertSdlRptJobs(){
        List<User> idf = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        String id = idf.get(0).id;
        List<CronTrigger> jobs = ManageReportOfInActiveUserC.getJobs(id);
        List<SheduleRunReport__c> srrlist = new List<SheduleRunReport__c>();
        for(Crontrigger ct:jobs){
        List<Report> repot = [select DeveloperName,CreatedById, CreatedBy.Name FROM Report where ID =:ct.CronJobDetail.Name];
        SheduleRunReport__c srr = new SheduleRunReport__c();   //persist data to schedule report
           List<User> userlist = [select Name FROM User where ID =:id];
           srr.RUserId__c = userlist.get(0).name;
           srr.Name ='CA MED Closed Lost Opportunities'; //repot.get(0).DeveloperName;
           srr.CreatedUserName__c = 'Brajmohan Sharma';//repot.get(0).CreatedBy.Name;   
           srr.EndDate__c = ct.EndTime;
           srr.StartDate__c = ct.StartTime;
           srr.CronExpression__c = ct.CronExpression;
           srr.NextFireDate__c = ct.NextFireTime;  
           srrlist.add(srr);
        }
           insert srrlist;
    }
   static testMethod void Case8SendMailToSheduleReport(){
        List<User> ide = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        User u = ide.get(0);
        u.isActive = true;
        update u;
        List<User> idee = [select id from user where firstname = 'Sinem' and lastName = 'Mavi'];
        String iddd = idee.get(0).id;
        ManageReportOfInActiveUserC.SendMailToSheduleReport(iddd);
     }//Case7SendMailToSheduleReport
   }//ManageReportOfInActiveUserCTest