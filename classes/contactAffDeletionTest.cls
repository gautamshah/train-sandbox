@istest
public class contactAffDeletionTest{
    static testmethod void test1(){
    
    RecordType rt1=[select Id, Name from Recordtype where name='US-Healthcare Facility' limit 1];

Account a1= new Account();
a1.name='sampleTest1';
 a1.RecordTypeId =rt1.id;
a1.BillingStreet = 'Test Billing Street';
a1.BillingCity = 'Test Billing City';  
a1.BillingState = 'Test';  
a1.BillingPostalCode = '12345';
Test.starttest();
insert a1;


Contact ct= new Contact();
ct.Lastname='ramesh';
ct.accountId=a1.id;
insert ct;

Contact_Affiliation__c cac= new Contact_Affiliation__c();
cac.AffiliatedTo__c =a1.id;
cac.Contact__c=ct.id;
cac.User_Login__c=true;
cac.Evaluating_Clinician__c=true;
insert cac;

Account_Affiliation__c aaf= new Account_Affiliation__c();
aaf.Affiliated_with__c=a1.id;
aaf.AffiliationMember__c=a1.id;
insert aaf;

delete aaf;

Test.stopTest();    
    
        
        }
    }