/****************************************************************************************
 * Name    : Task_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 5/2/2015 
 * Purpose : Contains all the logic coming from task trigger
 * Dependencies: TaskTrigger Trigger
 *             , Task Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * 4/20/2015   Bill Shan            A task associated to case is closed, add a "TaskCompleted" message to the SystemNote__c
 * 4/29/2015   Gaurav Gulanjkar     Update standard field with custom ReminderDateTime  field   
 *****************************************************************************************/
public without sharing class Task_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public Task_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void onBeforeInsert(List<Task> newObjects)
    {
        if(BatchSize == 1)
        {
            Task t = newObjects[0];
            //if(t.Reminder_Date_Time__c!=null)
            //{
            //    t.IsReminderSet = true;
            //    t.ReminderDateTime = t.Reminder_Date_Time__c;
            //}
            
            if(t.Franchise__c == 'Inside Sales' && Utilities.CurrentUser.Region__c=='EU')
            {
                t.RecordTypeId = Utilities.recordTypeMap_Name_Id.get('EU - Inside Sales tasks');
            }
        }   
    }
    public void OnAfterInsert(List<Task> newObjects){
        
        if(BatchSize == 1)
        {
            Task t = newObjects[0];
            //case call log
            if(t.Subject=='Call log' && t.Status=='Completed' && t.WhatId != null && t.WhatId.getSObjectType().getDescribe().getName()=='Case' &&
                t.WhoId!=null && t.WhoId.getSObjectType().getDescribe().getName()=='Contact')
            {
                Case c = [Select Id, ContactId From Case where Id = :t.WhatId limit 1];
                if(c.ContactId == t.WhoId)
                {
                    List<CaseMilestone> responseMilestones = [Select CompletionDate from CaseMilestone 
                    where CaseId = :c.Id and IsCompleted = false and 
                        MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Response Time')];
                    
                    for(CaseMilestone cm : responseMilestones)
                    {
                        cm.CompletionDate = DateTime.now();
                    }
                    
                    update responseMilestones;
                }
            }
        }
    }
    
    public void OnAfterUpdate(List<Task> newObjects, Map<Id, Task> oldMap)
    {
        Set<Id> caseIDsToProcess = new Set<Id>();
        for(Task t: newObjects)
        {
            if(t.CreatedById != t.OwnerId && t.IsClosed && oldMap.get(t.Id).IsClosed == false && t.WhatId != null && t.WhatId.getSObjectType().getDescribe().getName()=='Case')
            {
                caseIDsToProcess.add(t.WhatId);
            }
        }
        
        if(caseIDsToProcess.size()>0)
        {
            List<Case> casesToUpdate = [Select Id,SystemNote__c from Case where Id = :caseIDsToProcess];
            if(casesToUpdate.size()>0)
            {
                for(case c: casesToUpdate)
                {
                    c.SystemNote__c += 'TaskCompleted#';
                }
                
                update casesToUpdate;
            }
        }
    }
}