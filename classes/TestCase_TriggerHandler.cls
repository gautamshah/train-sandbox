@isTest
private class TestCase_TriggerHandler {

    static testMethod void myUnitTest() {

        List<Case> caseListInsert = new List<Case>();
        List<User> userList = new List<User>([SELECT Id from User where UserType = 'Standard' and IsActive = True LIMIT 2]);
         Account a = new Account(Name='TestAcct',Account_External_ID__c = 'US-1388321',Account_SAP_ID__c='US-1388321',BillingCountry = 'US');
            try
            {
                insert a;
            }
            catch(DMLException e)
            {
                
            }
             Account a1 = new Account(Name='TestAcct1',Account_External_ID__c = 'US-138832123',Account_SAP_ID__c='US-138832123',BillingCountry = 'US');
            try
            {
                insert a1;
            }
            catch(DMLException e)
            {
                
            }
            Convidien_Contact__c cc = new Convidien_Contact__c(Distributor_Name__c = a.id, Type__c = 'Sales Out Administrator');
            insert cc;
        //Create Test Compensation/Sales Inquiry Cases to Insert
        for(Integer i=0; i<20; i++){
            Case testCompCase = new Case(Subject='Test Upload_Invoice_Request Case' + i);
            testCompCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Upload_Invoice_Request' LIMIT 1].Id;
            testCompCase.Requested_By__c = userList[0].Id;
            testCompCase.Manager_of_Requested_By__c = userList[1].Id;
            testCompCase.AccountId = a.id;
            //testCompCase.Billing_Country__c = 'US';
            testCompCase.Temporary_Account_Number__c= 'US-1388321';
            caseListInsert.add(testCompCase);
        }  
        
        for(Integer i=0; i<20; i++){
            //Create Test R&T Cases (R_T_Case) to Insert 
            Case testRandTCase = new Case(Subject='Test R&T Case');
            testRandTCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Account_Creation_Case' LIMIT 1].Id;
            testRandTCase.Requested_By__c = userList[0].Id;
            testRandTCase.Manager_of_Requested_By__c = userList[1].Id;
            testRandTCase.Master_Account_Number__c = 'US-1388321';
            //testRandTCase.Account_Number__c = 'US-1388321';
            //testRandTCase.Temporary_Account_Number__c= 'US-138832123';
            testRandTCase.AccountId = a.id;
            //testRandTCase.Status = 'Closed-Resolved';
            caseListInsert.add(testRandTCase);
        
        }
        
        Test.startTest();
        insert caseListInsert;
        Case_TriggerHandler controller = new Case_TriggerHandler(false,0);
        controller.OnAfterInsert(caseListInsert);
        //controller.OnBeforeUpdate(caseListInsert);
       
        
        update caseListInsert;
        //controller.mergeTempAccount(a,a1);
        /*
        
        caseListInsert[1].Status = 'Closed-Resolved';
        caseListInsert[1].Temporary_Account_Number__c= 'US-138832123';
        
        update caseListInsert;*/
        
        Test.stopTest();
        
    }
    static testMethod void myUnitTest2() {

        List<Case> caseListInsert = new List<Case>();
        List<User> userList = new List<User>([SELECT Id from User where UserType = 'Standard' and IsActive = True LIMIT 2]);
         Account a = new Account(Name='TestAcct',Account_External_ID__c = 'US-1388321',Account_SAP_ID__c = 'US-1388321', BillingCountry = 'US');
            try
            {
                insert a;
            }
            catch(DMLException e)
            {
                
            }
             Account a1 = new Account(Name='TestAcct1',Account_External_ID__c = 'US-1388321',Account_SAP_ID__c = 'US-1388321',BillingCountry = 'US');
            try
            {
                insert a1;
            }
            catch(DMLException e)
            {
                
            }
            Convidien_Contact__c cc = new Convidien_Contact__c(Distributor_Name__c = a.id, Type__c = 'Sales Out Administrator');
            insert cc;
        //Create Test Compensation/Sales Inquiry Cases to Insert
        for(Integer i=0; i<20; i++){
            Case testCompCase = new Case(Subject='Test Upload_Invoice_Request Case' + i);
            //testCompCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Upload_Invoice_Request' LIMIT 1].Id;
            testCompCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Compensation_Case' LIMIT 1].Id;
            testCompCase.Requested_By__c = userList[0].Id;
            testCompCase.Manager_of_Requested_By__c = userList[1].Id;
            //testCompCase.AccountId = a.id;
            //testCompCase.Billing_Country__c = 'US';
            testCompCase.Temporary_Account_Number__c= 'US-1388321';
            caseListInsert.add(testCompCase);
        }  
        
        for(Integer i=0; i<20; i++){
            //Create Test R&T Cases (R_T_Case) to Insert 
            Case testRandTCase = new Case(Subject='Test R&T Case');
            //testRandTCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Account_Creation_Case' LIMIT 1].Id;
            testRandTCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'R_T_Case' LIMIT 1].Id;
            testRandTCase.Requested_By__c = userList[0].Id;
            testRandTCase.Manager_of_Requested_By__c = userList[1].Id;
            testRandTCase.Master_Account_Number__c = 'US-1388321';
            //testRandTCase.Account_Number__c = 'US-1388321';
            testRandTCase.Temporary_Account_Number__c= 'US-1388321';
           // testRandTCase.AccountId = a.id;
           // testRandTCase.Status = 'Closed';
            caseListInsert.add(testRandTCase);
        
        }
        
        Test.startTest();
        insert caseListInsert;
        Case_TriggerHandler controller = new Case_TriggerHandler(false,0);
        //case_Main ctrl1=new case_Main();
        case_Main.setEMSMasterAccount(caseListInsert);
        controller.OnAfterInsert(caseListInsert);
        //controller.OnBeforeUpdate(caseListInsert);
        update caseListInsert;
        //controller.mergeTempAccount(a,a1);
        Test.stopTest();
    }
    static testMethod void ZACSLogic() {
        
        Profile profile1 = [Select Id from Profile where name = 'EMEA EM Customer Service' limit 1];
        UserRole role = [Select Id from UserRole where name = 'ZA CS Agent' limit 1];
        User zaAgent = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mitgtest1@mitgmdt.com',
            Country='ZA',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='ZA',
            Lastname='Agent',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF',
            UserRoleId=role.Id);
        
        System.runAs(zaAgent) 
        {
            Account a = new Account(Name='Entitlement Account',Fax='123456789', billingcountry= 'US',BillingStreet='Rose Street1',BillingPostalCode='28753',BillingCity='US',BillingState='Lakeview State');
            insert a;
            Entitlement en1 = new Entitlement(Name='Short Shipment', CS_Center__c='ZA',AccountId=a.Id);
            insert en1;
            Entitlement en2 = new Entitlement(Name='Over Supply', CS_Center__c='ZA',AccountId=a.Id);
            insert en2;
            Group q = [Select Id from Group  where Type = 'Queue' and Name = 'ZA-Short Shipment' limit 1];
            GroupMember gm = new GroupMember(UserOrGroupId=zaAgent.Id, GroupId=q.Id);
            insert gm;
            Test.startTest();
            Case c = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Short Shipment'),Status='New', 
                    OwnerId=q.Id, SuppliedEmail='test@test.com',Origin='Email');
            insert c;
            c.OwnerId=zaAgent.Id;
            c.RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Over Supply');
            update c;
            c.Status='Awaiting Response/Dependancy';
            c.Awaiting_Response_From__c = 'Warehouse';
            update c;
            c.Status='Closed-Resolved';
            c.Order_Numbers__c='5645645';
            c.Awaiting_Response_From__c='';
            update c;
            
            Test.stopTest();
        }
    }
    static testMethod void ZAEmailLogic() {
        
        Profile profile1 = [Select Id from Profile where name = 'EMEA EM Customer Service' limit 1];
        UserRole role = [Select Id from UserRole where name = 'ZA CS Agent' limit 1];
        User zaAgent = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest2@mdtmitg.com',
            Country='ZA',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='ZA',
            Lastname='Agent',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF',
            UserRoleId=role.Id);
        
        System.runAs(zaAgent) 
        {
            Account a = new Account(Name='Entitlement Account',Fax='123456789', billingcountry= 'US',BillingStreet='Rosy Street ',BillingPostalCode='28754',BillingCity='US',BillingState='LakeView Site');
            insert a;
            Contact cont = new Contact(FirstName='test',LastName='Covidien',Email='gabby0207@gmail.com',AccountId=a.Id);
            insert cont;
            Entitlement en1 = new Entitlement(Name='Enquiry', CS_Center__c='ZA',AccountId=a.Id);
            insert en1;
            Test.startTest();
            list<Case> testCases = new List<Case>();
            Case c = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Enquiry'),Status='New', 
                    SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry',SystemNote__c='#',ContactId=cont.Id);
            insert c;
                        
            Case cDuplicate = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Enquiry'),Status='New', 
                    SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry');
            insert cDuplicate;
            
            EmailMessage em = new EmailMessage(Subject='Uplift A fax has arrived from remote ID \'123456789\'faxsubject', ParentId=c.Id, 
                                               Incoming=true,Headers='Message-ID: 000001');
            insert em;
            
            Attachment attachment = new Attachment(Name='An attachment',body=blob.valueof('b'),parentId=em.Id,ContentType='application/');
            insert attachment;
            
            EmailMessage emDuplicate = new EmailMessage(Subject='RE: Uplift A fax has arrived from remote ID \'123456789\'faxsubject', ParentId=cDuplicate.Id, 
                                               Incoming=true,Headers='Message-ID: 000001');
            insert emDuplicate;
            
            EmailMessage emreply = new EmailMessage(Subject='A fax has arrived from remote ID \'123456789\'faxsubject', ParentId=c.Id, 
                                               Incoming=true,Headers='Message-ID: 000002\nReferences: 000001');
            insert emreply;
            
            EmailMessage email = new EmailMessage(Subject='A fax consignment', ParentId=c.Id,TextBody='gaurav.gulanjkar@cognizant.com',HtmlBody='gaurav@covidien.com',Incoming=true);
            insert email;
            
            EmailMessage email1 = new EmailMessage(Subject='Test',ParentId=c.Id,TextBody='My personal email id is gabby0207@gmail.com',HtmlBody='My personal email id is gabby0207@gmail.com and gabby@medtronic.com',Incoming=true);
            insert email1;
                    
            Test.stopTest();
        }
    }
    static testMethod void ZANewEmailLogic(){
        Profile profile1 = [Select Id from Profile where name = 'EMEA EM Customer Service' limit 1];
        UserRole role = [Select Id from UserRole where name = 'ZA CS Agent' limit 1];
        User zaAgent = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest3@mdtmitg.com',
            Country='ZA',
            Alias = 'bshan',
            Email='mdtbill.mdtshan@mdtcovidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='ZA',
            Lastname='Agent',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF',
            isActive=true,
            UserRoleId=role.Id);
            
        insert zaAgent;
        
        System.runAs(zaAgent) 
        {    
            Account a = new Account(Name='Entitlement Account',Fax='123456789', billingcountry= 'US',BillingStreet='Rosie Street ',BillingPostalCode='28755',BillingCity='US',BillingState='LakeView Roseland');
            insert a;
            Contact cont = new Contact(FirstName='test',LastName='Covidien',Email='gabby0207@gmail.com',AccountId=a.Id);
            insert cont;
            Entitlement en1 = new Entitlement(Name='Enquiry', CS_Center__c='ZA',AccountId=a.Id);
            insert en1;
            list<Contact> contList = new List<Contact>();
            contList.add(cont);
            Test.startTest();
            list<Case> testCases = new List<Case>();
            Case c = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Enquiry'),Status='New', 
                    SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry',SystemNote__c='');
            insert c;
            EmailMessage email1 = new EmailMessage(Subject='',ParentId=c.Id,TextBody='',HtmlBody='',Incoming=true);
            insert email1;
            EntitySubscription ent = new EntitySubscription(SubscriberId=zaAgent.Id,ParentId=c.Id);
            insert ent;
            Test.stopTest();
        }
    }
    static testMethod void ZAFaxLogic() {
        
        Profile profile1 = [Select Id from Profile where name = 'EMEA EM Customer Service' limit 1];
        UserRole role = [Select Id from UserRole where name = 'ZA CS Agent' limit 1];
        User zaAgent = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest4@mdtmitg.com',
            Country='ZA',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='ZA',
            Lastname='Agent',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF',
            UserRoleId=role.Id);
        
        System.runAs(zaAgent) 
        {
            Account a = new Account(Name='Entitlement Account',Fax='123456789', billingcountry= 'US',BillingStreet='Rosina Street ',BillingPostalCode='28756',BillingCity='US',BillingState='LakeView Rosina');
            insert a;
            Entitlement en1 = new Entitlement(Name='Short Shipment', CS_Center__c='ZA',AccountId=a.Id);
            insert en1;
            Test.startTest();
            
            //Account disAcc = [Select Id from Account where RecordTypeId=:Utilities.recordTypeMap_Name_Id.get('EU-Distributor') limit 1];
            Case c = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Short Shipment'),Status='New', 
                              SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry', AccountId = a.Id);
            insert c;
            EmailMessage em = new EmailMessage(Subject='A fax has arrived from remote ID \'123456789\'faxsubject', ParentId=c.Id, 
                                               Incoming=true,Headers='Message-ID: 000001');
            insert em;
            Contact con = new Contact(Lastname='test', FirstName='TEST', Fax='987654321', AccountId=a.Id);
            insert con;
            EmailMessage em2 = new EmailMessage(Subject='A fax has arrived from remote ID \'987654321\'faxsubject', ParentId=c.Id, Incoming=true);
            insert em2;
            EmailMessage em3 = new EmailMessage(Subject='Response', ParentId=c.Id, Incoming=false,ToAddress='test@test.com');
            insert em3;
            EmailMessage em4 = new EmailMessage(Subject='Consign A fax has arrived from remote ID \'1234576545\'Consignment', ParentId=c.Id, Incoming=true);
            insert em4;
            Test.stopTest();
        }
    }
    static testMethod void RemoveCaseMessageFlag() {
        
        Profile profile1 = [Select Id from Profile where name = 'EMEA EM Customer Service' limit 1];
        UserRole role = [Select Id from UserRole where name = 'ZA CS Agent' limit 1];
        User zaAgent = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest5@mdtmitg.com',
            Country='ZA',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='ZA',
            Lastname='Agent',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF',
            UserRoleId=role.Id);
        
        System.runAs(zaAgent) 
        {
            Account a = new Account(Name='Entitlement Account',Fax='123456789', billingcountry= 'US',BillingStreet='Rose Street',BillingPostalCode='28752',BillingCity='US',BillingState='Lakeview');
            insert a;
            Entitlement en1 = new Entitlement(Name='Short Shipment', CS_Center__c='ZA',AccountId=a.Id);
            insert en1;
            Test.startTest();
            Case c = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Short Shipment'),Status='New', 
                    SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry', SystemNote__c='#NewMessage#');
            insert c;
            
            Case defaultc = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('CRM Support'),Status='New', 
                    SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry',Type='Problem');
            insert defaultc;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(c);
            Case_RemoveMessageFlag controller = new Case_RemoveMessageFlag(sc);
        /*
            PageReference pageRef = Page.Case_RemoveMessageFlag;
            pageRef.getParameters().put('id', String.valueOf(c.Id));
            Test.setCurrentPage(pageRef);*/
            controller.RemoveMessageFlag();
            
            Test.stopTest();
        }
    }
    static testMethod void ZATaskLogic() {
        
        Profile profile1 = [Select Id from Profile where name = 'EMEA EM Customer Service' limit 1];
        UserRole role = [Select Id from UserRole where name = 'ZA CS Agent' limit 1];
        User zaAgent = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest6@mdtmitg.com',
            Country='ZA',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='ZA',
            Lastname='Agent',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF',
            UserRoleId=role.Id);
        
        System.runAs(zaAgent) 
        {
            Account a = new Account(Name='Entitlement Account',Fax='123456789', billingcountry= 'US',BillingStreet='Rosina Street',BillingPostalCode='28757',BillingCity='US',BillingState='Lakeview Rosyland');
            insert a;
            Entitlement en1 = new Entitlement(Name='Short Shipment', CS_Center__c='ZA',AccountId=a.Id);
            insert en1;
            Test.startTest();
            Contact con = new Contact(Lastname='test', FirstName='TEST', Fax='987654321', AccountId=a.Id);
            insert con;
            Case c = new Case(Subject='Test', RecordTypeId=Utilities.recordTypeMap_Name_Id.get('Short Shipment'),Status='New', 
                    SuppliedEmail='test@test.com',Origin='Email - ZA - Enquiry',ContactId=con.Id);
            insert c;
            
            Task t = new Task(Subject='Call log',Status='Completed', WhatId=c.Id,WhoId=con.Id);
            insert t;
            
            Task t2 = new Task(Subject='Call log',Status='New', WhatId=c.Id,WhoId=con.Id);
            insert t2;
            t2.Status='Completed';
            update t2;
            
            Test.stopTest();
        }
    }
    
    
}