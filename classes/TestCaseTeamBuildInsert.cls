@isTest
private class TestCaseTeamBuildInsert {
	
    static testMethod void myUnitTest() {
        List<Case> caseListInsert = new List<Case>();
        List<User> userList = new List<User>([SELECT Id from User where UserType = 'Standard' and IsActive = True LIMIT 2]);
        
        //Create Test Compensation/Sales Inquiry Cases to Insert
        for(Integer i=0; i<20; i++){
        	Case testCompCase = new Case(Subject='Test Compensation Case' + i);
        	testCompCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Compensation_Case' LIMIT 1].Id;
        	testCompCase.Requested_By__c = userList[0].Id;
        	testCompCase.Manager_of_Requested_By__c = userList[1].Id;
        
        	caseListInsert.add(testCompCase);
        }  
        
        for(Integer i=0; i<20; i++){
        	//Create Test R&T Cases (R_T_Case) to Insert 
        	Case testRandTCase = new Case(Subject='Test R&T Case');
        	testRandTCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'R_T_Case' LIMIT 1].Id;
        	testRandTCase.Requested_By__c = userList[0].Id;
        	testRandTCase.Manager_of_Requested_By__c = userList[1].Id;
        
        	caseListInsert.add(testRandTCase);
        
        }
        
        Test.startTest();
        insert caseListInsert;
        Test.stopTest();
    }
}