/*******************************************************************************
@Name: EmailRelatedListController
@Author: Isaac Lewis
@CreateDate: 2016-07-23
@Description: Provides access to related child records in email templates
@UsedBy: EmailRelatedList.component

// NOTES

Any component or page that references EmailRelatedList.component (including
itself) will fail to save if edited via the Tooling API. This includes tools like
the Developer Console and MavensMate. The only workaround is to use a tool that
uses the Metadata API, or edit in the Salesforce Setup UI.

This is due to a known issue reported and discussed here:
- [Known Issue: Tooling API - Bytecode Exception from Tooling API with VF Component](https://success.salesforce.com/issues_view?id=a1p300000008bIkAAI)
- [Forum Post on Known Issue](https://developer.salesforce.com/forums/?id=906F0000000BQHgIAO)

*******************************************************************************/
public with sharing class EmailRelatedListController {

	public Id pId {get;set;}
	public String cName {get;set;}
	public String cFields {get;set;}
	public String rName {get;set;}
	private List<sObject> children;
	private Boolean isInitialized = false;

	public Boolean getHasChildren () {
		// Must init here due to order of execution for component construction
		// Allows to check children list for conditional render of component body
		this.initComponent();
		if( this.children.size() > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	private void initComponent () {

		// Return if already initialized
		if (this.isInitialized) {
			return;
		}

		// Default to empty list so we fail nicely
		this.children = new List<Sobject>();

		// Compile query dynamically (could support multiple inner queries this way)
		List<String> query = new List<String>();

		System.debug('Parent Id: ' + this.pId);

		// Return if no parent
		if(this.pId == NULL) {
			return;
		}

		// Parent Variables
		Schema.DescribeSObjectResult pDescribe = this.pId.getSObjectType().getDescribe();
		String pName = pDescribe.getName();
		System.debug('Parent sObject Name: ' + pName);

		// Child Variables
		Set<String> cFieldSet = new Set<String>(this.cFields.split(','));
		List<Schema.ChildRelationship> rs = pDescribe.getChildRelationships();

		for (Schema.ChildRelationship r : rs) {

			if( r.getChildSObject().getDescribe().getName() == this.cName ) {

				// Increase relationship specificity if multiple relationships to same child object
				if ( this.rName != NULL && r.getRelationshipName() != this.rName ) {
					continue;
				}

				// Child Relationship Variables
				this.rName = r.getRelationshipName();
				String rfName = r.getField().getDescribe().getName();
				cFieldSet.add(rfName);

				System.debug('Child sObject Name: ' + this.cName);
				System.debug('Child Relationship Name: ' + this.rName);
				System.debug('Child Relationship Field: ' + rfName);

				// Validate Child Fields
				Map<String, Schema.SObjectField> rfs = r.getChildSObject().getDescribe().fields.getMap();
				for(String f : cFieldSet){
					if (rfs.get(f) == NULL) {
						// Prevent illegal fields & soql injection
						break;
					}
				}

				// Build Child Query
				query.add(', (SELECT ' + String.join(new List<String>(cFieldSet),', ') + ' FROM ' + this.rName + ')');

				// No need to continue after relationship found
				break;

			}

		}

		// Wrap Child with Parent Query
		query.add(0,'SELECT Id');
		query.add(' FROM ' + pName);
		query.add(' WHERE Id = \'' + this.pId + '\'');
		query.add(' LIMIT 1');
		String queryString = String.join(query,'');
		System.debug('Query: ' + queryString);

		this.children = Database.query(queryString)[0].getSObjects(this.rName);
        if (this.children == null) {
            this.children = new List<sObject>();
        }
		System.debug('Children: ' + this.children);
		this.isInitialized = true;

	}

	public List<sObject> getChildren () {
		if (!this.isInitialized) {
			this.setChildren();
		}
		return this.children;
	}

	private void setChildren () {
		if (!this.isInitialized) {
			this.initComponent();
		}
	}

}