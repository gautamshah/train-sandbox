/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A               AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies
20170123        IL         AV-280     Created to increase code coverage on dependencies

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
@isTest
private class cpqProposalLineItem_cTest
{
	private static void setup ()
	{
        String recordTypeName = (new List<String>(cpqDeal_RecordType_c.getProposalRecordTypesByName().keySet()).get(0));

        cpqDeal_TestSetup.buildScenario_CPQ_Deal( 
			cpq_u.PROPOSAL_SOBJECT_TYPE,
			recordTypeName,
			OrganizationNames_g.Abbreviations.RMS);

		cpq_TestSetup.testProducts = cpqProduct2_TestSetup.create();
		insert cpq_TestSetup.testProducts;
		
		cpq_TestSetup.testPriceListItems =
     		cpqPriceListItem_u.fetch(
     			Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
     			cpqPriceList_c.RMS.Id);

		// Create Standard and Option PLIs
		cpq_TestSetup.testPLIs = new List<Apttus_Proposal__Proposal_Line_Item__c>();
		
		cpq_TestSetup.testPLIs.add(cpqProposalLineItem_TestSetup.generateProposalLineItem(
			cpq_TestSetup.testProposals[0],
			cpq_TestSetup.testProductConfigurations[0],
			cpq_TestSetup.testProducts[0],
			cpq_TestSetup.testPriceListItems.values()[0]
		));
		
		cpq_TestSetup.testPLIs.add(cpqProposalLineItem_TestSetup.generateProposalLineItem(
			cpq_TestSetup.testProposals[0],
			cpq_TestSetup.testProductConfigurations[0],
			cpq_TestSetup.testProducts[1],
			cpq_TestSetup.testPriceListItems.values()[1]
		));

		System.debug('Test RecordType: ' + recordTypeName);
		System.debug('Test Proposal: ' + cpq_TestSetup.testProposals);
		System.debug('Test Products: ' + cpq_TestSetup.testProducts);
		System.debug('Test PriceListItems: ' + cpq_TestSetup.testPriceListItems);
	}

	@isTest
	static void fetchOptions ()
	{
		// GIVEN a proposal with an option proposal line item
		setup();
		
		cpq_TestSetup.testPLIs.get(0).Apttus_QPConfig__OptionId__c = cpq_TestSetup.testProducts[2].Id;
		cpq_TestSetup.testPLIs.get(0).Apttus_QPConfig__LineType__c = 'Option';
		insert cpq_TestSetup.testPLIs;


		// WHEN fetchOptions is called
		List<Apttus_Proposal__Proposal_Line_Item__c> result;
		
		Test.startTest();
			result = cpqProposalLineitem_c.fetchOptions(cpq_TestSetup.testProposals[0].Id);
		Test.stopTest();

		// THEN only option line items are returned
		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
	}

	@isTest
	static void fetchOptionsMap ()
	{
		// GIVEN a proposal with an option proposal line item
		setup();
		
		cpq_TestSetup.testPLIs[0].Apttus_QPConfig__OptionId__c = cpq_TestSetup.testProducts[2].Id;
		cpq_TestSetup.testPLIs[0].Apttus_QPConfig__LineType__c = 'Option';
		insert cpq_TestSetup.testPLIs;

		// WHEN fetchOptionsMap is called
		Map<Id,Apttus_Proposal__Proposal_Line_Item__c> result;
		
		Test.startTest();
			result = cpqProposalLineitem_c.fetchOptionsMap(cpq_TestSetup.testProposals[0].Id);
		Test.stopTest();

		// THEN a map of PLIs is returned
		System.assertNotEquals(null, result);
		System.assertEquals(1, result.keySet().size());

	}

	@isTest
	static void hasTradeIns ()
	{
		setup();

		Product2 FT10 = cpq_TestSetup.testProducts[2];
		FT10.Name = cpqProposalLineitem_c.FT10;
		update FT10;

		cpq_TestSetup.testPLIs[0].Apttus_QPConfig__OptionId__c = cpq_TestSetup.testProducts[2].Id;
		cpq_TestSetup.testPLIs[0].Apttus_QPConfig__OptionId__r = cpq_TestSetup.testProducts[2];
		cpq_TestSetup.testPLIs[0].Apttus_QPConfig__LineType__c = 'Option';
		insert cpq_TestSetup.testPLIs;

		// Call hasTradeIns on a proposal line item with an FT10 option product
		System.assertEquals(true, cpqProposalLineitem_c.hasTradeIns(cpq_TestSetup.testPLIs[0]));

		// Call hasTradeIns on a proposal line item without an FT10 option product
		System.assertEquals(false, cpqProposalLineitem_c.hasTradeIns(cpq_TestSetup.testPLIs[1]));
	}
}