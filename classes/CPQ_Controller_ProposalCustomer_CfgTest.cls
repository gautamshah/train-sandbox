@isTest
public class CPQ_Controller_ProposalCustomer_CfgTest {

    private static CPQ_Config_Setting__c cs;
    private static Map<String,Id> mapProposalRType = New Map<String,Id>();

    private static void setup()
    {
        cs = cpqConfigSetting_c.SystemProperties;
        Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class); 
        for(RecordType R: rts.values())
        {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }
    }

    @isTest static void itCreatesCotExpressionForParticipatingFacility()
    {
        setup();
        cs.Participating_Facility_COTs__c = '-003\r\n::07H';
        Apttus_Proposal__Proposal__c theRecord
            = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                                               RecordTypeID = mapProposalRType.get('Respiratory_Solutions_COOP'));
        CPQ_Controller_ProposalCustomer_Config participatingFacilityConfig
                = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(theRecord));

        String expected = '  (  (  NOT Class_of_Trade__c IN (\'003\')) OR  (  ( Class_of_Trade__c IN (\'003\')) AND Secondary_Class_of_Trade__c = \'07H\' )  )';
        String wherePredicate = participatingFacilityConfig.injectParticipatingFacilityQueryPredicate();
        System.debug('Expression: ' + wherePredicate);
        System.assertEquals(expected.trim(), wherePredicate.trim());

    }

    @isTest static void itDoSearchByCopyNotSurgical()
    {
        setup();
        cs.Participating_Facility_COTs__c = '-003\r\n::07H\r\n-003\r\n::071';
        Apttus_Proposal__Proposal__c theRecord
            = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                                               RecordTypeID = mapProposalRType.get('Respiratory_Solutions_COOP'));
        insert theRecord;

       List<Account> accs = new List<Account>();
            accs.add(new Account( Name = '001 OOA', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '00A' ));
            accs.add(new Account( Name = '001 07H', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '07H' ));
            accs.add(new Account( Name = '003 002', Class_of_Trade__c = '002' ));
            accs.add(new Account( Name = '002 00A', Class_of_Trade__c = '002', Secondary_Class_of_Trade__c = '00A' ));
            accs.add(new Account( Name = '003 07H', Class_of_Trade__c = '003', Secondary_Class_of_Trade__c = '07H' ));
        insert accs;

       accs = [SELECT ID, Class_of_Trade__c, Secondary_Class_of_Trade__c FROM Account];


      List<Participating_Facility__c> participatingFacilities = new List<Participating_Facility__c>
      {
        new Participating_Facility__c(Account__c = accs.get(0).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(1).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(2).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(3).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(4).ID, Proposal__c = theRecord.ID)
      };
      insert participatingFacilities;

      //verify correct set of records?
      CPQ_Controller_ProposalCustomer_Config participatingFacilityConfig
                = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(theRecord));
      participatingFacilityConfig.SearchSource = 'Proposal';
      participatingFacilityConfig.SearchProposalSourceID = theRecord.ID;


      PageReference ref = participatingFacilityConfig.doSearchByCopy();

    }
  
    @isTest 
    static void itDoSearchByCRNs()
    {
    	setup();
        cs.Participating_Facility_COTs__c = '-003\r\n::07H\r\n-003\r\n::071';
        Apttus_Proposal__Proposal__c theRecord
            = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                                               RecordTypeID = mapProposalRType.get('Respiratory_Solutions_COOP'));
        insert theRecord;

       List<Account> accs = new List<Account>();
            accs.add(new Account( Name = '001 OOA', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '00A' ));
            accs.add(new Account( Name = '001 07H', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '07H' ));
            accs.add(new Account( Name = '003 002', Class_of_Trade__c = '002' ));
            accs.add(new Account( Name = '002 00A', Class_of_Trade__c = '002', Secondary_Class_of_Trade__c = '00A' ));
            accs.add(new Account( Name = '003 07H', Class_of_Trade__c = '003', Secondary_Class_of_Trade__c = '07H' ));
        insert accs;

       accs = [SELECT ID, Class_of_Trade__c, Secondary_Class_of_Trade__c FROM Account];


      List<Participating_Facility__c> participatingFacilities = new List<Participating_Facility__c>
      {
        new Participating_Facility__c(Account__c = accs.get(0).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(1).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(2).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(3).ID, Proposal__c = theRecord.ID),
        new Participating_Facility__c(Account__c = accs.get(4).ID, Proposal__c = theRecord.ID)
      };
      insert participatingFacilities;
      
      CPQ_Controller_ProposalCustomer_Config participatingFacilityConfig
                = new CPQ_Controller_ProposalCustomer_Config(new ApexPages.StandardController(theRecord));
      participatingFacilityConfig.SearchCRNs = '549841';
      PageReference ref = participatingFacilityConfig.doSearchByCRNs();


    }
}