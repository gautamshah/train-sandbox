/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@isTest
global class documentViewer {
    global String docName;
    global String pdfBody {
        get;
        set;
    }
    global documentViewer() {

    }
    global String getDocName() {
        return null;
    }
    global void setDocName(String theDocName) {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void testDocumentViewer() {

    }
}
