/* 
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
                PAB                   Created.
20170123        IL         AV-280     Made case-insensitive so fieldsToInclude do not conflict with describe results
20170124        IL         AV-280     Moved `cpq_u.buildObjectQuery` here

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

// NOTE: Must keep apiVersion in sync with apiVersion of SOQL_select.cls

public class SOQL_select extends SOQL implements Queueable 
{
	public SOQL_select(List<sObject> sObjs)
	{
		super(sObjs);
	}
	
	////
	//// buildQuery
	////
	//// This one you pass in the fields you want to include
	public static string buildQuery(
    	sObjectType sObjType,
    	Set<string> fieldsToInclude,
    	string whereClause,
    	string orderBy,
    	integer limitRows)
    {
    	DEBUG.dump(
    		new Map<object, object>
    		{
    			'method' => 'SOQL_select.buildQuery',
    			'sObjType' => sObjType,
    			'fieldsToInclude' => fieldsToInclude,
    			'whereClause' => whereClause,
    			'orderBy' => orderBy,
    			'limitRows' => limitRows
    		});
        
		//// Always include the 'Id'
		//// This is a set<string> so the field is only included 1 time
		Set<string> selectedFields = new Set<string>{ 'id' };

       	if (fieldsToInclude != null)
       		for(string field : fieldsToInclude)
       			selectedFields.add(field.toLowercase());

        String query =
            'SELECT ' +
            string.join(new List<string>(selectedFields), ',' ) +
            ' FROM ' +
            sObject_u.getSObjectDescribe(sObjType).getName();

        // TODO: Should add padding in case user does not provide
        // spacing in clause strings
        query += (whereClause == null) ? '' : ' ' + whereClause + ' ';
        query += (orderBy     == null) ? '' : ' ' + orderBy + ' ';
        query += (limitRows   == null) ? '' : ' LIMIT ' + string.valueOf(limitRows);

        return query;
    }	

	private static set<string> allFieldsExceptTheOnesToIgnore(sObjectType sObjType)
	{
        Schema.DescribeSObjectResult describe = sObject_u.getSObjectDescribe(sObjType);

        Map<string, sObjectField> fieldMap = describe.fields.getMap();
		set<string> availableFields = fieldMap.keySet();
		availableFields = DATATYPES.toLowercase(availableFields);

		for(string ignore : SOQL.fieldsToIgnore)
			if (availableFields.contains(ignore))
				availableFields.remove(ignore);
				
		return availableFields;
	}
	
	//// This one automatically includes all fields (it just calls
	//// the method above and passes in all the fields)
	public static string buildQuery(
    	sObjectType sObjType,
    	string whereClause,
    	string orderBy,
    	integer limitRows)
    {
		return buildQuery(sObjType, allFieldsExceptTheOnesToIgnore(sObjType), whereClause, orderBy, limitRows);
    }	

	//// depricated (use buildQuery methods)
    public static string build(
    	sObjectType sObjType,
    	Set<string> fieldsToInclude,
    	string whereClause,
    	string orderBy,
    	integer limitRows)
    {
    	DEBUG.dump(
    		new Map<object, object>
    		{
    			'method' => 'SOQL_select.build',
    			'sObjType' => sObjType,
    			'fieldsToInclude' => fieldsToInclude,
    			'whereClause' => whereClause,
    			'orderBy' => orderBy,
    			'limitRows' => limitRows
    		});
        
        Schema.DescribeSObjectResult describe = sObject_u.getSObjectDescribe(sObjType);

        Map<string, sObjectField> fieldMap = sObject_u.getSObjectFields(sObjType);

		Set<string> availableFields = fieldMap.keySet();
		availableFields = DATATYPES.toLowercase(availableFields);
		Set<string> selectedFields = new Set<string>(availableFields);
        
		Map<sObjectField, string> sObjectField2Name = new Map<sObjectField, string>();
		for(string key : fieldMap.keySet())
			sObjectField2Name.put(fieldMap.get(key), key.toLowercase());

        for(string ignore : SOQL.fieldsToIgnore)
        	selectedFields.remove(ignore);

       	if (fieldsToInclude != null)
       		for(string field : fieldsToInclude)
       			selectedFields.add(field.toLowercase());

        String query =
            'SELECT ' +
            string.join(new List<string>(selectedFields), ',' ) +
            ' FROM ' +
            describe.getName();

        // TODO: Should add padding in case user does not provide
        // spacing in clause strings
        query += (whereClause == null) ? '' : ' ' + whereClause + ' ';
        query += (orderBy     == null) ? '' : ' ' + orderBy + ' ';
        query += (limitRows   == null) ? '' : ' LIMIT ' + string.valueOf(limitRows);

        return query;
    }
    
    public static String buildObjectQuery(
    	Set<String> fieldNames,
    	String specialFields,
    	String whereClause,
    	String sObjectName)
    {
        String queryStr = 'SELECT ';

        // standard fields
        queryStr += 'Id';
        queryStr += ', Name';

        if(specialFields != null) {
            queryStr += ',' + specialFields;
        }

        // custom fields
        if (fieldNames != null) {
            for (String fieldName : fieldNames) {
                if (fieldName == 'Id' || fieldName == 'Name') {
                    continue;
                }

                queryStr += ',' + fieldName;
            }
        }

        queryStr += ' FROM ' + sObjectName;

        if(whereClause != null) {
            queryStr += ' WHERE ';
            queryStr += whereClause;
        }

        return queryStr;
    }

     public static String getSoqlInListFromSObjectList (List<sObject> input) {
        // Produces string for IN clause of SOQL query - ex. WHERE Id IN {A,B,C,D}
        String s = '';
        for (SObject r : input) {
            if (String.isEmpty(s)) {
                s = '\'' + r.ID + '\'';
            } else {
                s = s + ', \'' + r.ID + '\'';
            }
        }

        if (String.isEmpty(s))
            s = '\'^^THERE IS NOTHING IN THE INLIST^^\'';
        return s;
    }
}