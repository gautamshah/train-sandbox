/****************************************************************************************
* Name    : Class: Test_IDNSC
* Author  : Gautam Shah
* Date    : 8/25/2014
* Purpose : Unit Test for IDNSC Relationship and Task triggers and classes
* 
* Dependancies: 
* 	Class: 
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
@isTest(SeeAllData=true)
private class Test_IDNSC {

    static testMethod void RelationshipAndTask() 
    {
    	List<User> u = new List<User>([Select Id From User Where Profile.Name = 'System Administrator' And isActive = true Limit 9]);
    	System.runAs(u[0])
    	{
	        IDNSC_Relationship__c r = new IDNSC_Relationship__c();
	        r.Name = 'Test Relationship';
	        r.On_Board_Start_Date__c = Date.today();
	        r.Go_Live_Date_Desired__c = Date.today() + 30;
	        r.OwnerId = u[0].Id;
	        insert r;
	        r.Role_Customer_Care__c = u[0].Id;
	        r.Role_Data_Steward__c = u[1].Id;
	        r.Role_Customer_Service__c = u[2].Id;
	        r.Role_EDI_Support__c = u[3].Id;
	        r.Role_Pricing_Specialist__c = u[4].Id;
	        r.Role_RVP__c = u[5].Id;
	        r.Role_Rebates_Tracings_Analyst__c = u[6].Id;
	        r.Role_Sales_Analyst__c = u[7].Id;
	        r.Role_Transportation_Analyst__c = u[8].Id;
	        r.Current_Event_State__c = 'Preparation Activity';
	        update r;
	        IDNSC_Task__c t = new IDNSC_Task__c();
	        t.Name = 'Test Task';
	        t.IDNSC_Parent__c = r.Id;
	        t.Task_Number__c = 'A1';
	        t.Duration_Days_Expected__c = 2;
	        t.Role__c = 'RVP';
	        t.Runs_in_State__c = 'Customer Kickoff';
	        t.Critical_Path__c = true;
	        insert t;
	        r.Current_Event_State__c = 'Customer Kickoff';
	        update r;
	        t.Date_Assigned__c = Date.today();
	        t.Start_Date_Actual__c = Date.today();
	        t.Completion_Date_Actual__c = Date.today() + 1;
	        t.Status__c = 'Completed';
	        update t;
    	}
    }
}