/**
  * Selector class for the standard User and custom MHSA Community Control (and Sub-Control) objects - returns query results for use in the JavaScript Remoting Class (MHSACommunityController)
  * 
  * @author Allie Lafontant
  * @version 1.0
  * @see MHSACommunitySelector
  */
public with sharing class MHSACommunitySelector
{
    public static User currentUser {get; set;}

  /** 
     * Method that gets specific current user's information  
     *
     * @param Current userId (pulled from the Visualforce Page)
     * @return Current User information (First Name, Last Name, Community Role, User's related Contact's Account's Id)
  */ 

  public static User getCurrentUser()
  {
     if(currentUser == null) 
     {
       currentUser = [
                      SELECT FirstName,LastName,Community_Role__c,Contact.Account.Id
                       FROM User
                        WHERE Id =:UserInfo.getUserId() LIMIT 1
                     ];
     }
       return currentUser;
  }

   /** 
     * Method that gets list of MHSA
     *
     * @param No parameters 
     * @return Current User information (First Name, Last Name, Community Role, User's related Contact's Account's Id) + 
   */ 
  public static List<MHSA_Community_Control__c> getAllMHSAControls()
  {    
     getCurrentUser();

    if(currentUser.Contact == null)
     {
        return [
                    SELECT Name,Control_Type__c,Description__c,Height__c,State__c,Tableau_JavaScript_Tag__c,Tableau_URL__c,Page_Link__c,Role_Access__c,Width__c,
                     (
                      SELECT Name,Control_Type__c,State__c,Content_Header__c,Height__c,Tableau_JavaScript_Tag__c,Tableau_URL__c,Page_Link__c,Role_Access__c,Width__c
                       FROM MHSA_Community_Sub_Control__r
                        WHERE Status__c = 'Active' AND Role_Access__c INCLUDES(: currentUser.Community_Role__c)  
                         ORDER BY Menu_Nav_Order__c ASC NULLS LAST 
                     ) 
                      FROM MHSA_Community_Control__c
                       WHERE Status__c = 'Active' AND Role_Access__c INCLUDES(: currentUser.Community_Role__c) 
                        ORDER BY Menu_Nav_Order__c ASC NULLS LAST 
               ];
      }else{

        List<MHSA_Community_Account_Access__c> mcaaList = [SELECT MHSA_Community_Sub_Control__c FROM MHSA_Community_Account_Access__c WHERE Account__c =: currentUser.Contact.Account.Id];
         Set<Id> mcaaIds = new Set<Id>();
          for(MHSA_Community_Account_Access__c mcaa: mcaaList)
          {
              mcaaIds.add(mcaa.MHSA_Community_Sub_Control__c);
          }

        return [
                 SELECT Name,Control_Type__c,Description__c,Height__c,State__c,Tableau_JavaScript_Tag__c,Tableau_URL__c,Page_Link__c,Role_Access__c,Width__c,
                  (
                   SELECT Name,Control_Type__c,State__c,Content_Header__c,Height__c,Tableau_JavaScript_Tag__c,Tableau_URL__c,Page_Link__c,Role_Access__c,Width__c
                    FROM MHSA_Community_Sub_Control__r
                     WHERE Status__c = 'Active' AND Role_Access__c INCLUDES(: currentUser.Community_Role__c) AND Id IN :mcaaIds
                      ORDER BY Menu_Nav_Order__c ASC NULLS LAST 
                  ) 
                   FROM MHSA_Community_Control__c
                    WHERE Status__c = 'Active' AND Role_Access__c INCLUDES(: currentUser.Community_Role__c)
                     ORDER BY Menu_Nav_Order__c ASC NULLS LAST 
                ];  
      }
    }  
}