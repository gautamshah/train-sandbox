/**
 * Remote Action Class - JavaScript remoting that allows the front-end (AngularJs in Static Resources) uses to make an AJAX request from Visualforce pages 
 * (MHSA_Main, MHSA_Summary, MHSA_FacilityOutcomes, etc...) directly to MHSACommunitySelector Apex class. 
 * 
 * @author Allie Lafontant
 * @version 1.0
 * @see MHSACommunityController
 */
global with sharing class  MHSACommunityController
{    
    /** 
     * Remote Action Method that facilitates data access functions used in the front-end.
     *
     * @param sObject List of MHSA Community Controls and Sub-Controls (MHSA_Community_Control__c and MHSA_Community_Sub_Control__c)
     * @return JSON serialized list of MHSA Community Control records 
     * @throws MHSA Community Exception error if there are any errors. 
   */ 
    @RemoteAction
    global static List<MHSA_Community_Control__c> returnAllMHSAControls() 
    {
        try{
             return MHSACommunitySelector.getAllMHSAControls();

        }catch(exception ex)
         { 
           System.Debug(LoggingLevel.ERROR, 'Error encountered during MHSACommmunityController remote action (Unable to return list of MHSA Community Controls). The following exception has occurred:  ' + ex.getMessage());
           throw new MHSACommunityException(System.Label.PA_SF_COMMUNITY_ERROR, ex);
         }
    }

}