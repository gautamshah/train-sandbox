global with sharing class MasterContactSearch_Controller {
 
 /****************************************************************************************
   * Name    : ContactSearch_Controller
   * Author  : Shawn Clark
   * Date    : 10/15/2015 
   * Purpose : Allows Reps to Search for Connected Contacts from the Account object on both
               Mobile & Website. If they find a Connected Contact, they can click the contact
               name to navigate to the record. No Results, they can create a new Connected 
               Contact. A Master contact is also created in the background, but the rep
               does not need to be concerned about this. 
               
    *11/05/2015: We observed that informatica process CON_GLBT_SFDC is already creating
    the associated Master contact, so I have shut off the creation of that here    -S.Clark       
 *****************************************************************************************/
 
   private final SObject parent;
    //Pre set
    RecordType rt = [SELECT Id FROM RecordType where SobjectType = 'Contact' and DeveloperName = 'Affiliated_Contact_US' LIMIT 1];  
    RecordType Master = [SELECT Id FROM RecordType where SobjectType = 'Contact' and RecordType.DeveloperName = 'Master_Non_Clinician' LIMIT 1];
    global string CountryAccount = [select ID from Account where Name = 'US-Account' LIMIT 1].ID;
    
    public list <contact> acc {get;set;}
    
    public string searchstring {get;set;}
    public string searchstring2 {get;set;} 
    public string searchstring3 {get;set;}
      
    global string AccountName {get;set;}
    global string HeaderText {get;set;}
    global String ReturnAccount {get;set;}
    global Integer IndexNum {get;set;}
    global String SelectedGUID {get;set;}
    global Integer Size {get; set;}
    
    global String M_Salutation {get;set;}
    global String M_FirstName {get;set;}
    global String M_LastName {get;set;}
    global String M_Email {get;set;}
    global String M_Phone {get;set;}  
    global String M_Specialty {get;set;}
    global String M_Department {get;set;}
    global String M_OtherDepartment {get;set;} 
    global String M_ConnectedAs {get;set;} 
    global String M_Title {get;set;} 
    global String NewMasterContact {get;set;}  
    global String NewContact {get;set;}
    
    global String RecName {get;set;}
    global String RecName2 {get;set;}
    global String searchquery {get;set;}
    
    
    //Toggle Section Variables
    global Boolean ShowMasterCreate {get; set;}
    global Boolean ShowOtherDepartment {get; set;} 
    global Boolean ShowTitle {get; set;} 
    global Boolean ShowSearchSection {get; set;} 
    global Boolean ShowNoneOfTheseResultsButton {get; set;} 
    global Boolean ShowCreateNewContact  {get; set;} 
    global Boolean RequireTitle  {get; set;}   
    global Boolean RequireOtherDepartment  {get; set;}   


  //Set initial variable states

   public MasterContactSearch_Controller(ApexPages.StandardController controller) {
       parent = controller.getRecord();
       RecName = 'Affiliated_Contact_US';
       RecName2 = 'In_Process_Connected_Contact';
       ReturnAccount = parent.id;
       AccountName = [SELECT Name from Account where Id = :ReturnAccount].Name;
       ShowMasterCreate = false;
       ShowSearchSection = true;
       HeaderText = 'Contact Search';
       ShowOtherDepartment = false;
       ShowNoneOfTheseResultsButton = true; 
       ShowTitle = false;
       RequireOtherDepartment = false;
       RequireTitle = false;

   }

   //Retrieve Picklist values for Department_Picklist__c on Contact Object

    public List<SelectOption> getDepartmentList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));   
        Schema.DescribeFieldResult fieldResult = Contact.Department_picklist__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }  


//Get only Distinct Departments that have been used

    public list<SelectOption> getUniqueDepartment() {
       list<SelectOption> options = new list<SelectOption>();
       list<Contact> DepartmentList = new List<Contact>();
       set<String> setDepartments = new Set<String>();
       list<String> UniqueDepartments = new List<String>();
      
       options.add(new SelectOption('','-- None --'));
 
       DepartmentList = [SELECT Department_picklist__c FROM Contact where AccountId = :ReturnAccount and Department_picklist__c != null and (RecordType.DeveloperName = :RecName or RecordType.DeveloperName = :RecName2)  ORDER BY Department_picklist__c] ;
 
       for (Integer i = 0; i< DepartmentList.size(); i++)
       {
       setDepartments.add(DepartmentList[i].Department_picklist__c); // Set dedups city values in list
       }
   
       UniqueDepartments.addAll(setDepartments);   

       for (Integer i = 0; i< UniqueDepartments.size(); i++)
       {
       options.add(new SelectOption(UniqueDepartments[i],UniqueDepartments[i] )); // contains distict accounts
       }
       return options; 
    } 


   //Retrieve Picklist values for Connected_As on Contact Object

    public List<SelectOption> getConnectedAsList()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));      
        Schema.DescribeFieldResult fieldResult = Contact.Affiliated_Role__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }  
    
//Get only Distinct Departments that have been used

    public list<SelectOption> getUniqueConnectedList() {
       list<SelectOption> options = new list<SelectOption>();
       list<Contact> ConnectedList = new List<Contact>();
       set<String> setConnected = new Set<String>();
       list<String> UniqueConnected = new List<String>();
      
       options.add(new SelectOption('','-- None --'));
 
       ConnectedList = [SELECT Affiliated_Role__c FROM Contact where AccountId = :ReturnAccount and Affiliated_Role__c != null and (RecordType.DeveloperName = :RecName or RecordType.DeveloperName = :RecName2)  ORDER BY Affiliated_Role__c] ;
 
       for (Integer i = 0; i< ConnectedList.size(); i++)
       {
       setConnected.add(ConnectedList[i].Affiliated_Role__c); // Set dedups city values in list
       }
   
       UniqueConnected.addAll(setConnected);   

       for (Integer i = 0; i< UniqueConnected.size(); i++)
       {
       options.add(new SelectOption(UniqueConnected[i],UniqueConnected[i] )); // contains distict accounts
       }
       return options; 
    }     
    
    
    
    
   //Retrieve Picklist values for Specialty1 on Contact Object

    public List<SelectOption> getSpecialty()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));      
        Schema.DescribeFieldResult fieldResult = Contact.Specialty1__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }   
    
    
    public List<SelectOption> getSalutation()    
    {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- None ---'));      
        Schema.DescribeFieldResult fieldResult = Contact.Salutation.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }       
    
    


   //Main Page Search
    
   public PageReference searchMain(){  
     
     searchquery = 'select Salutation, firstname, lastname, name, id, AccountID, Contact.Account.Name, Personal_Email__c, ' +
                   'MobilePhone, Phone_Number_at_Account__c, Department_picklist__c, Other_Department__c, ' +
                   'Affiliated_Role__c, Title, Specialty1__c ' +
                   'from contact where AccountId = :ReturnAccount and (RecordType.DeveloperName = :RecName or RecordType.DeveloperName = :RecName2)';
                                               
     //Dynamically Append additional conditions to the base searchquery if fields are populated
     
     if (!String.isEmpty(searchstring))
        searchquery += ' and name LIKE \'%'+String.escapeSingleQuotes(searchstring)+'%\'';
     if (!String.isEmpty(searchstring2))
        searchquery += ' and Department_picklist__c = \''+String.escapeSingleQuotes(searchstring2)+'\'';
     if (!String.isEmpty(searchstring3))
        searchquery += ' and Affiliated_Role__c = \''+String.escapeSingleQuotes(searchstring3)+'\'';
            
     acc= Database.query(searchquery); 
     Size = acc.size();
     
            
     
   if (String.isEmpty(searchstring) && String.isEmpty(searchstring2) && String.isEmpty(searchstring3) )
    {
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter (1) or more Search Parameters'));
       return null;
    } 
    else if (Size == 0)
    {
       ShowNoneOfTheseResultsButton = false;
       ShowCreateNewContact = true;
       ReturnAccount = parent.id;
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Results'));
       PageReference pageRef= new PageReference('/apex/MasterContactSearchLookup');
       pageRef.setredirect(false);  
       return pageRef;  
    }
   else 
       {
        ShowNoneOfTheseResultsButton = true;
        ShowCreateNewContact = false;
        PageReference pageRef= new PageReference('/apex/MasterContactSearchLookup');
        pageRef.setredirect(false);      
        return pageRef;  
        }   
   }  
   

   
   public PageReference NoResultsMatch(){  
     ShowSearchSection = false;
     ShowMasterCreate = true;
     HeaderText = 'Create Contact';
     searchstring = NULL;
     PageReference pageRef= new PageReference('/apex/MasterContactSearch');
     pageRef.setredirect(false);       
     return pageRef; 
     }   

   //Revise Search 

   public PageReference ReviseSearch(){  
     ShowMasterCreate = false;
     HeaderText = 'Contact Search';
     PageReference pageRef= new PageReference('/apex/MasterContactSearch');
     pageRef.setredirect(false);       
     return pageRef; 
     }   
       
   //Check Department Value and Determine if Other Department needs to be displayed

   public PageReference OtherDeptCheck(){  
     M_Department = M_Department;
       
     if (M_Department == 'Other')
       {
       ShowOtherDepartment = true;
       RequireOtherDepartment = true;
       return null;
       }
       ShowOtherDepartment = false;
       RequireOtherDepartment = false;
       return null;
       }  

   //Check Connected As Value and Determine if Title needs to be displayed

   public PageReference OtherConnectedAsCheck(){  
     M_ConnectedAs = M_ConnectedAs;
       
     if (M_ConnectedAs == 'Other')
     {
     ShowTitle = true;
     RequireTitle = true;
     return null;
     }
     ShowTitle = false;
     RequireTitle = false;
     return null;
     }       
       
   public PageReference RefreshForm(){  
     return null;
     } 
     
   //Captures the Selected Contact GUID for Navigation to that record

   public void processButtonClick() {
     SelectedGUID = acc[IndexNum].Id; //This is actually Contact Guid 
     }
    

   //No Results, User Can Create Master & Connected

 public PageReference CreateMasteAndConnected() {

//If at least 1 of the Email or Phone Fields is not blank then 
//create the contacts, otherwise throw an error message

 if (String.isNotBlank(M_Email) || String.isNotBlank(M_Phone)) 
   {
       /* Create Master Contact
      M_FirstName = M_FirstName;
       M_LastName = M_LastName;
       M_Title = M_Title;
       Contact mcon = new Contact();
       mcon.RecordTypeId = Master.id;
       mcon.Salutation = M_Salutation;
       mcon.FirstName = M_FirstName;
       mcon.LastName = M_LastName;
       mcon.Email = M_Email;
       mcon.Created_via_Salesforce1__c = true;
       mcon.Phone_Number_At_Account__c = M_Phone;
       mcon.AccountId = CountryAccount;
          try{ 
             insert mcon; 
             NewMasterContact = mcon.Id;
             } 
             catch (Exception e) 
             {}
 */
      //Create Connected Contact
      Contact con = new Contact();
      con.RecordTypeId = rt.id;  
      con.Salutation = M_Salutation;               
      con.FirstName = M_FirstName;
      con.LastName = M_LastName;
      con.Email = M_Email;
      con.Phone_Number_At_Account__c = M_Phone;
      con.Specialty1__c = M_Specialty;
      con.Department_picklist__c = M_Department;
      con.Other_Department__c = M_OtherDepartment;
      con.Affiliated_Role__c = M_ConnectedAs;
      con.Title = M_Title;
      con.Created_via_Salesforce1__c = true;
      con.AccountId = ReturnAccount;
      //con.Master_Contact_Record__c = NewMasterContact;   
          try{ 
             insert con; 
             NewContact = con.Id;
             } 
             catch (Exception e) 
             {}
             return null;
       }  
       else {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Provide either Email or Phone # at Account:'));
      return null;
      }
    }  
}