/*
MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
2017.02.10      IL         AV-280    Created. To optimize user query usage.
*/
public class User_g extends sObject_g
{
	public User_g()
	{
		super(User_cache.get());
	}

	////
	//// cast
	////
	public static Map<Id, User> cast(Map<Id, sObject> sobjs)
	{
		//DEBUG.dump(
		//	new Map<object, object>
		//	{
		//		'method' => 'User_g.cast(Map<Id, sObject> sobjs)',
		//		'sobjs' => sobjs
		//	});

		try
		{
			return new Map<Id, User>((List<User>)sobjs.values());
		}
		catch(Exception e)
		{
			return new Map<Id, User>();
		}
	}

	public static Map<object, Map<Id, User>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, User>> target = new Map<object, Map<Id, User>>();
		for(object key : source.keySet())
			target.put(key, cast(source.get(key)));

		return target;
	}

	////
	//// fetch
	////
	public Map<Id, User> fetch()
	{
		return cast(this.cache.fetch());
	}

	public User fetch(Id id)
	{
		return (User)this.cache.fetch(id);
	}

	public Map<Id, User> fetch(Set<Id> ids)
	{
		return cast(this.cache.fetch(ids));
	}

	public Map<Id, User> fetch(sObjectField index, string value) {
		return cast(this.cache.fetch(index, value));
	}

	public Map<Id, User> fetch(sObjectField index, object value)
	{
		return cast(this.cache.fetch(index, value));
	}

	public Map<object, Map<Id, User>> fetch(sObjectField index, set<object> values)
	{
		return cast(this.cache.fetch(index, values));
	}

	////
	//// Additional methods
	////
}