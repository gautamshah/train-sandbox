public class UserTerritory_TestSetup extends sObject_TestSetup
{
	// Generates a cross product list of UserTerritory records
	// User x Territory
    public static List<UserTerritory> generate(List<User> users, List<Territory> territories)
    {
        List<UserTerritory> uts = new List<UserTerritory>();
        for (User u : users)
        {
            for (Territory t : territories)
            {
                uts.add(new UserTerritory(
                    UserId = u.Id,
                    TerritoryId = t.Id
                ));
            }
        }
        return uts;
    }
}