@isTest
public class CPQ_Controller_ProposlDistrbtr_CfgTest {

    private static CPQ_Config_Setting__c cs;
    private static Map<String,Id> mapProposalRType = New Map<String,Id>();

    private static void setup()
    {
        cs = cpqConfigSetting_c.SystemProperties;
        Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class);
        
        for(RecordType R: rts.values())
            mapProposalRType.Put(R.DeveloperName, R.Id);
    }

    @isTest static void itCreatesCotExpressionForDistributor()
    {
        setup();
        cs.Participating_Facility_COTs__c = '-003\r\n::07H';
        Apttus_Proposal__Proposal__c theRecord
            = new Apttus_Proposal__Proposal__c(
            	Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                RecordTypeID = mapProposalRType.get('Respiratory_Solutions_COOP'));
                
        CPQ_Controller_ProposalDistrbutor_Config distributorConfig
        	= new CPQ_Controller_ProposalDistrbutor_Config(new ApexPages.StandardController(theRecord));

        String expected = '(  ( Class_of_Trade__c IN (\'003\')) AND  ( NOT ( Class_of_Trade__c IN (\'003\') AND Secondary_Class_of_Trade__c = \'07H\' ) ) )';
        String wherePredicate = distributorConfig.injectDistributorQueryPredicate();
        System.debug('Expression: ' + wherePredicate);
        System.assertEquals(expected.trim(), wherePredicate.trim());
    }
}