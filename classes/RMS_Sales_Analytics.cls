public with sharing class RMS_Sales_Analytics {

    public Map<string, string> Headers {
        get {
            return ApexPages.CurrentPage().getHeaders();
        }
    }

}