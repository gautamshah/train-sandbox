/****************************************************************************************
 * Name    : StatusbacktoNoShow
 * Author  : Lakhan Dubey
 * Date    : 23/04/2015 
 * Purpose : test class for StatusbacktoNoShow
 * Dependencies: Training__c 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               -------
 *****************************************************************************************/
@istest
Public Class StatusbacktoNoShow_Test{
static testmethod void changestatus (){
Account a=new  Account(name='Roberts');
insert a;
Contact cc =new contact(lastname='Jolly',AccountID=a.id);
insert cc;
Course__c c=new Course__c(Status__c='Open for Nomination',Start_Date__c=system.today());
insert c;
Training__c t=new Training__c(Course__c=c.id,Business_Unit__c='VT',Role__c='Trainee',Contact_Name__c=cc.id,IsOuttafinalApproval__c=false,Status__c='Planned');
insert t;
t.IsOuttafinalApproval__c=true;
update t;
System.AssertNotEquals('No Show',[Select Status__c from Training__c where id =:t.id].Status__c  );
t.Status__c='No Show';
update t;
System.AssertEquals('No Show',[Select Status__c from Training__c where id =:t.id].Status__c  );

}
}