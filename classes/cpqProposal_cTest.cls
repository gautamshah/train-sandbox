/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYY.MM.DD   A    PAB        CPR-000    Updated comments section
2017.01.19   A    IL         AV-280     Created. To increase coverage on dependencies related to AV-280.

Notes:
1.  If there are multiple related Jiras, add them on the next line
2.  Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

@isTest
private class cpqProposal_cTest 
{
	static cpq_TestSetup setup = new cpq_TestSetup();
	
    @isTest
    static void sObjectFields()
    {
        System.assertEquals(Apttus_Proposal__Proposal__c.fields.Apttus_QPComply__MasterAgreementId__c, cpqProposal_c.FIELD_MasterAgreementId);
    }

    @isTest
    static void ApprovalIndicators()
    {
        System.assertEquals('Approved', cpqProposal_c.convert(cpqProposal_c.ApprovalIndicators.Approved));
    }

    @isTest
    static void getRecordTypes()
    {
        // Should be asserted more thoroughly by cpqDeal_RecordType_cTest
        List<RecordType> rts = cpqProposal_c.getRecordTypes();
        System.assertNotEquals(null, rts, 'Record types returned null');
        System.assert(rts.size() > 0, 'Record types returned empty');
    }

    @isTest
    static void create()
    {
        RecordType rt = cpqProposal_c.getRecordTypes().get(0);
        Apttus_Proposal__Proposal__c prop = cpqProposal_c.create(rt.Id,1);
        System.assertNotEquals(null, prop);
        System.assertEquals(rt.Id, prop.RecordTypeId);
    }

    @isTest
    static void fetchApprovers()
    {

        // Fetch Record
        String recordTypeName = (new List<String>(cpqDeal_RecordType_c.getProposalRecordTypesByName().keySet()).get(0));
        cpqDeal_TestSetup.buildScenario_CPQ_Deal(cpq_u.PROPOSAL_SOBJECT_TYPE,recordTypeName);
        Apttus_Proposal__Proposal__c p1 = cpq_TestSetup.testProposals.get(0);
        Apttus_Proposal__Proposal__c p2 = cpqProposal_c.fetchApprovers(p1.Id);
        System.assertEquals(p1.Id, p2.Id);

        // Fetch Null
        Apttus_Proposal__Proposal__c p3;
        try {
            p3 = cpqProposal_c.fetchApprovers(sObject_c.dummyId(cpq_u.PROPOSAL_TYPE,1));
            System.assert(false,'Should throw query exception');
        }
        catch (QueryException e) {
            System.assert(true,'Should throw query exception');
        }

    }

    @isTest
    static void oldVersion()
    {

    }

    @isTest
    static void isLocked()
    {

        Id aIdOld = sObject_c.dummyId(cpq_u.AGREEMENT_TYPE,1);
        Id aIdNew = sObject_c.dummyId(cpq_u.AGREEMENT_TYPE,2);

        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c(
            apttus_qpcomply__masteragreementid__c = aIdOld
        );

        System.assertEquals(false, cpqProposal_c.isLocked(p,aIdOld));
        System.assertEquals(true, cpqProposal_c.isLocked(p,aIdNew));

    }

    @isTest
    static void isRestricted()
    {

        Apttus_Proposal__Proposal__c pOld = new Apttus_Proposal__Proposal__c();
        pOld.Id = sObject_c.dummyId(cpq_u.PROPOSAL_TYPE,1);
        Map<Id,Apttus_Proposal__Proposal__c> pOlds = new Map<Id,Apttus_Proposal__Proposal__c>();
        pOlds.put(pOld.Id,pOld);

        // Approval Status Not Changed
        Apttus_Proposal__Proposal__c pNew = new Apttus_Proposal__Proposal__c();
        pNew.Id = pOld.Id;
        System.assertEquals(false, cpqProposal_c.isRestricted(pNew,pOlds));

        // Approval Status Changed
        pNew.Approval_Indicator__c = cpqProposal_c.convert(cpqProposal_c.ApprovalIndicators.Approved);
        System.assertEquals(true, cpqProposal_c.isRestricted(pNew,pOlds));

    }

    // @isTest
    // static void isOpen()
    // {
    //
    // }

    @isTest
    static void unlock()
    {
        // GIVEN a proposal with a locked record type
        String rtLocked = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = :cpq_u.PROPOSAL_DEV_NAME AND DeveloperName LIKE '%Locked' LIMIT 1].DeveloperName;
        String rtUnlocked = cpqDeal_RecordType_c.getUnlockedRecordTypeName(rtLocked);

        cpqDeal_TestSetup.buildScenario_CPQ_Deal(cpq_u.PROPOSAL_SOBJECT_TYPE,rtLocked);

        Apttus_Proposal__Proposal__c pLocked =
        	[SELECT Id,
        	        RecordType.DeveloperName,
        	        Apttus_Proposal__Approval_Stage__c
        	 FROM Apttus_Proposal__Proposal__c
        	 WHERE Id = :cpq_TestSetup.testProposals.get(0).Id];

        // Workaround to prevent fire of CustomQuoteApprovalsTrigger
        pLocked.Apttus_QPApprov__Approval_Status__c = 'Approved';
        update pLocked;

        // WHEN Record is unlocked by cpqProposal_c
        cpqProposal_c.unlock(pLocked);
        Apttus_Proposal__Proposal__c pUnlocked =
        	[SELECT Id,
        	        RecordType.DeveloperName,
        	        Apttus_Proposal__Approval_Stage__c
        	 FROM Apttus_Proposal__Proposal__c
        	 WHERE Id = :pLocked.Id];

        // THEN Record is unlocked
        System.assertNotEquals(rtLocked, pUnlocked.RecordType.DeveloperName);
        System.assertEquals(rtUnlocked, pUnlocked.RecordType.DeveloperName);
        // AND Record is accepted
        System.assertEquals('Accepted', pUnlocked.Apttus_Proposal__Approval_Stage__c);
    }

    @isTest
    static void ProductConfigurationFinalizedDate()
    {
        Apttus_Proposal__Proposal__c parent = new Apttus_Proposal__Proposal__c();
        parent.Apttus_QPConfig__ConfigurationFinalizedDate__c = System.today();

        System.assertNotEquals(null, parent.Apttus_QPConfig__ConfigurationFinalizedDate__c);

        cpqProposal_c.ProductConfigurationFinalizedDate pcfd = new cpqProposal_c.ProductConfigurationFinalizedDate(parent);
        pcfd.clear();

        System.assertEquals(null, parent.Apttus_QPConfig__ConfigurationFinalizedDate__c);


    }

}