@isTest
private class TestUpdateEventsGBU_Franchise {
    static testMethod void runTest() {
        // Add Test Data
        //User u = [Select Id from User where Id = :UserInfo.getUserId()];
        
        	Id pId = [select Id 
	                    from Profile 
	                   where name = 'CRM Admin Support'].Id;
            User u2 = new User();
             u2.LastName = 'Shmoe Test';
        	 u2.Business_Unit__c = 'The Unit';
		     u2.Franchise__c = 'Burger King';
		     u2.email = 'testShmoe@covidian.com';
		     u2.alias = 'testShmo';
		     u2.username = 'testShmoe@covidian.com';
		     u2.communityNickName = 'testShmoe@covidian.com';
		     u2.ProfileId = pId;
		     u2.CurrencyIsoCode='USD'; 
		     u2.EmailEncodingKey='ISO-8859-1';
		     u2.TimeZoneSidKey='America/New_York';
		     u2.LanguageLocaleKey='en_US';
		     u2.LocaleSidKey ='en_US';
		     insert u2;
        
        
		     System.runAs(u2){
        Event t = new Event();
        t.Activity_Type__c = 'Test';
        t.Description = 'Test';
        t.OwnerId = u2.Id;
        t.DurationInMinutes = 60;
        t.activityDateTime = System.now() + 100;
        
        insert t;
        
        //System.assertEquals('Burger King', [Select Franchise__c from Event where Id = :t.Id].Franchise__c);
        System.assertEquals('The Unit', [Select Business_Unit__c from Event where Id = :t.Id].Business_Unit__c);
        
        
        u2.Business_Unit__c = '';
        u2.Franchise__c = '';
        
        update u2;
        update t;
        
        //System.assertEquals(null, [Select Franchise__c from Event where Id = :t.Id].Franchise__c);
        System.assertEquals(null, [Select Business_Unit__c from Event where Id = :t.Id].Business_Unit__c);
		     }
        /*
        
        	Id pId = [select Id 
	                    from Profile 
	                   where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id;
	                   
             User u2 = new User();
             u2.LastName = 'Shmoe Test';
        	 u2.Business_Unit__c = 'The Unit';
		     u2.Franchise__c = 'Burger King';
		     u2.email = 'testShmoe@covidian.com';
		     u2.alias = 'testShmo';
		     u2.username = 'testShmoe@covidian.com';
		     u2.communityNickName = 'testShmoe@covidian.com';
		     u2.ProfileId = pId;
		     u2.CurrencyIsoCode='USD'; 
		     u2.EmailEncodingKey='ISO-8859-1';
		     u2.TimeZoneSidKey='America/New_York';
		     u2.LanguageLocaleKey='en_US';
		     u2.LocaleSidKey ='en_US';
		     insert u2;
		     System.runAs(u2){
		     	
		     	Event t2 = new Event();
		        t2.Activity_Type__c = 'Test';
		        t2.Description = 'Test';
		        t2.OwnerId = u.Id;
		        
		        insert t2;  
		        
		        System.assertEquals(null, [Select Franchise__c from Event where Id = :t2.Id].Franchise__c);
		        System.assertEquals(null, [Select Business_Unit__c from Event where Id = :t2.Id].Business_Unit__c);
		     }
        
        */
    }
}