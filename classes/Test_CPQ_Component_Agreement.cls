/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
public class Test_CPQ_Component_Agreement {

    @testSetup
    public static void setup() {
    
        //////insert cpqConfigSetting_cTest.create();
        //////cpqConfigSetting_c.reload();
        
        ////CPQ_Config_Setting__c settings = new CPQ_Config_Setting__c();
        ////settings.Name = 'System Properties';
        ////settings.Profiles_allowed_to_see_COOP_Price_File__c = 'System AdministratorCRM Admin Support';
        
        ////insert settings;
    
        Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
        a.Name = 'test agreement';

        insert a;
        
    	Product2 product = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG, 'TEST', 1);
    	insert product;
        
        Apttus__AgreementLineItem__c l = new Apttus__AgreementLineItem__c();
        l.Apttus__AgreementId__c = a.Id;
        l.Apttus__ProductId__c = product.Id;
        l.Apttus__NetPrice__c = 3.00003;
        l.Apttus_CMConfig__ChargeType__c = 'Consumable';
        
        insert l;
                
        ERP_Account__c erp = new ERP_Account__c();
        erp.Name = 'test erp';
        
        insert erp;
        
        Agreement_Participating_Facility_Address__c p = new Agreement_Participating_Facility_Address__c();
        p.Agreement__c = a.Id;
        p.ERP_Record__c = erp.Id;
        p.Ship_to_Address_1__c = 'address 1';
        p.City_Template__c = 'city';
        p.State_Region_Template__c = 'st';
        p.Zip_Postal_Code_Template__c = '00000';
        p.Shipto_Number_Template__c = '00001';
        p.Reporting_Number_Template__c = '00002';
        
        insert p;
    }  

    @isTest
    public static void unitTest() {
    
        CPQ_Component_Agreement c = new CPQ_Component_Agreement();
        
        c.AgreementId = [SELECT Id FROM Apttus__APTS_Agreement__c WHERE Name = 'test agreement'].Id;
        c.showButtons = true;
        c.quoteText = false;

        CPQ_Component_Agreement.COOPPriceFile file = c.getCOOPPriceFile();
        
        system.debug(file);
    }
}