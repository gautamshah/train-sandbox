/**
Author: Adnan Samma

All services (extensions) should register with the register manager. It will create an entry (if
it does not exist) in the SVMX_RegisteredServices custom settings and mark it as active. Admins can turn
on/off the service from there.
*/
public class RAPID_SVMXC_ServiceRegister
{
    /**
    All extensions will check if the service is active before running it
    
    serviceName: is the name of the service. Each extension should have a unique name.
    */
    public static boolean isActive(String serviceName)
    {
        //retrieve the extension from the custom settings.
        SVMX_Services_Register__c registeredService = SVMX_Services_Register__c.getInstance(serviceName);
        
        if (registeredService == null)
        {
            registeredService = registerService(serviceName); 
            
            //error has already been logged in register service if null is being returned.
            if (registeredService == null)
                return false;           
        }
        
        return registeredService.Is_Active__c;
        
    }
    
    public static SVMX_Services_Register__c registerService(String serviceName)
    {
        try
        {
            SVMX_Services_Register__c newRegisteredExtension = new  SVMX_Services_Register__c(Name=serviceName); 
        
            insert newRegisteredExtension;
        
            return newRegisteredExtension;
        }
        catch (Exception insertException)
        {
            System.debug('Service, ' + serviceName + ', could not registered because of the following exception: ' + insertException.getMessage());
        }
        return null;
    }
}