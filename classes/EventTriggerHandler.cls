/****************************************************************************************
* Name    : EventTriggerHandler
* Author  : Kan Hayashi
* Date    : Oct 13, 2015
* Purpose : 
* 
* Dependancies: 
*              
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE              AUTHOR        VER            CHANGE
* ----              ------        -----          ------
* Jan 27, 2016      Kan Hayashi   0.1          
* Jan 27, 2016      Gautam Shah   0.11           Changes to use the Utilities class to minimize SOQL queries(Lines 27-32);
* Apr 12, 2016      Kan Hayashi   0.2            Implement update functionality
*****************************************************************************************/ 
public with sharing class EventTriggerHandler
{
    private String profileName {get; set;}
    private Map<ID, Account> accountMap {get; set;}
    private User currentUser {get; set;}
    private String contact_prefix {get; set;}
    
    public EventTriggerHandler()
    {
        //List<Profile> plist = [SELECT Id, Name FROM Profile WHERE Id=:UserInfo.getProfileId() LIMIT 1];
        //this.profileName = plist[0].Name;
        this.profileName = Utilities.CurrentUserProfileName;
        //List<User> uL = [SELECT Id, DailyReportChatterGroupID__c, DailyReportManagerUserID__c, Sales_Org_PL__c, Region__c FROM User where Id = :UserInfo.getUserId() LIMIT 1];
        //this.currentUser = uL[0];
        this.currentUser = Utilities.CurrentUser;
        this.contact_prefix = Schema.SObjectType.Contact.getKeyPrefix();
    }
    
    public void onBeforeInsert(List<Event> triggerNewList){
        String User_Sales_Org = this.currentUser.Sales_Org_PL__c;
        String User_Region = this.currentUser.Region__c;
        //String User_Sales_Org = [select Id,Sales_Org_PL__c from User where Id = :userinfo.getUserId() limit 1].Sales_Org_PL__c;
        //String User_Region = [select Id,Region__c from User where Id = :userinfo.getUserId() limit 1].Region__c;
        
        for (Event e : triggerNewList) {
            
            // Clear Chatter_Feed_ID__c in case of Event clone
            if (e.Chatter_Feed_ID__c != NULL) {
                e.Chatter_Feed_ID__c = NULL;
            }
            
             if( User_Sales_Org == 'RMS' && User_Region =='EU'|| User_Sales_Org == 'MCS' && User_Region =='EU'|| User_Sales_Org == 'MS' && User_Region =='EU')
            {
                e.RecordTypeId = Utilities.recordTypeMap_Name_Id.get('RMS & MS Events - EU/ANZ');
            }
            
         }
    }
    
    public void onAfterInsert(List<Event> triggerNewList){
        if (checkCondition()) {
            editChatterPost(null, triggerNewList);
        }
    }
    
    public void onBeforeUpdate(Map<Id, Event> triggerOldMap, List<Event> triggerNewList){
    }

    public void onAfterUpdate(Map<Id, Event> triggerOldMap, List<Event> triggerNewList){
        
        if (checkCondition()) {
            editChatterPost(triggerOldMap, triggerNewList);
        }
    }
    
    private void editChatterPost(Map<Id, Event> oldMap, List<Event> eventList) {
        setAccountMap(eventList);
        //List<Event> eList = [SELECT id, subject, AccountId, StartDateTime, EndDateTime, Description, Who.Name, WhoId, JP_Target_Product_Category__c, JP_Visiting_Destination__c, JP_Visiting_Destination2__c, Activity_Type__c from Event WHERE ID in :eventList];
        List<Event> eList = [SELECT id, subject, AccountId, StartDateTime, EndDateTime, Description, Who.Name, WhoId, JP_Target_Product_Category__c, JP_Visiting_Destination__c, JP_Visiting_Destination2__c, Activity_Type__c, Chatter_Feed_ID__c from Event WHERE ID in :eventList];
        List<Event> eventsToUpdate = new List<Event>();
        
       for (Event e : eList) {
            //if (e.AccountId != NULL && (oldMap == NULL || (oldmap != NULL && oldMap.get(e.id).AccountId == NULL))) {
            if (e.AccountId != NULL) {
                if(e.Chatter_Feed_ID__c == NULL) {
                    ConnectApi.FeedItemInput feedItemInput = createFeedItemInput(createMsg(e));
                    feedItemInput.subjectId = this.currentUser.DailyReportChatterGroupID__c;
                    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, feedItemInput, null);
                    e.Chatter_Feed_ID__c = feedElement.id;
                    eventsToUpdate.add(e);
                } else {
                    if(oldmap != NULL && oldMap.get(e.id).Chatter_Feed_ID__c != NULL) {
                        ConnectApi.FeedItemInput feedItemInput = createFeedItemInput(createMsg(e));
                        ConnectApi.FeedElement editedFeedElement = ConnectApi.ChatterFeeds.updateFeedElement(null, e.Chatter_Feed_ID__c, feedItemInput);
                    }
                }
            }
        }
        if (eventsToUpdate.size() > 0) {
            update eventsToUpdate;
        }
    }
    
    private ConnectApi.FeedItemInput createFeedItemInput(List<String> msgSegments) {
        if (msgSegments == NULL) return NULL;
        
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
        for (String m : msgSegments) {
           if (m.startsWith('{') && m.endsWith('}')) {
                /*
                // Use this code if you want to use @mention within the chatter post
                m = m.substringAfter('{');
                m = m.substringBefore('}');
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput.id = m;
                messageBodyInput.messageSegments.add(mentionSegmentInput);
                */
            } else {
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                textSegmentInput.text = m;
                messageBodyInput.messageSegments.add(textSegmentInput);
            }
        }
        feedItemInput.body = messageBodyInput;
        return feedItemInput;
    }
    
    // Create Chatter Post message body
    // Format:
    // 日時：#2015/10/14 14:00 - 15:00
    // 施設：#テスト病院 (JP)
    // 件名：XX病院訪問
    // ---
    // セミナー参加のお礼。
    // 翌週火曜に師長とCC説明のアポイントもらう。
    // ---
    // 顧客：#面談 太郎, 訪問先：#外科系-一般外科 ※RMSとMedは訪問先(分類)、訪問先（詳細）を「-」でつなげる
    // 戦略製品/症例： #CVC
    // 今回のステップ：#PR
    
    private List<String> createMsg(Event e) {
        String l;
        Schema.DescribeFieldResult F;
        List<Schema.PicklistEntry> PL;
        
        if (this.accountMap == NULL) return null;
        
        List<String> msgs = new List<String>(); 
        
        msgs.add('日時：#[' + e.StartDateTime.format('yyyy/MM/dd') + '] ');
        msgs.add(e.StartDateTime.format('HH:mm') + ' - ');
        msgs.add(e.EndDateTime.format('HH:mm') + '\n');
        
        msgs.add('施設：#[' + this.accountMap.get(e.AccountId).name + ']\n');
        msgs.add('件名：' + e.Subject + '\n');
        
        msgs.add('---\n');
        msgs.add(e.Description + '\n');
        msgs.add('---\n');
        
        if (e.WhoId!= NULL) {
          if(((String)e.WhoId).startsWith(contact_prefix)){
            msgs.add('顧客：#[' + e.Who.Name + ']');
            if (e.JP_Visiting_Destination__c != NULL) {
              msgs.add(', ');
            } else {
              msgs.add('\n');
            }
          }
        }
        if (e.JP_Visiting_Destination__c != NULL) {
          l = 'N/A';
          F = Event.JP_Visiting_Destination__c .getDescribe();
          PL = F.getPicklistValues();
          for (Schema.PicklistEntry p : PL) {
            if (e.JP_Visiting_Destination__c == p.getValue()) {
              l = p.getLabel();
            }
          }
          msgs.add('訪問先：#[' + l);
          if (e.JP_Visiting_Destination2__c!= NULL) {
            l = 'N/A';
            F = Event.JP_Visiting_Destination2__c.getDescribe();
            PL = F.getPicklistValues();
            for (Schema.PicklistEntry p : PL) {
              if (e.JP_Visiting_Destination2__c== p.getValue()) {
                l = p.getLabel();
              }
            }
            msgs.add('-' + l);
          }
          msgs.add(']\n');
        }
        if (e.JP_Target_Product_Category__c != NULL) {
        l = 'N/A';
          F = Event.JP_Target_Product_Category__c .getDescribe();
          PL = F.getPicklistValues();
          for (Schema.PicklistEntry p : PL) {
            if (e.JP_Target_Product_Category__c == p.getValue()) {
              l = p.getLabel();
            }
          }
          msgs.add('戦略製品/症例： #[' + l + ']\n');
        }
        if (e.Activity_Type__c  != NULL) {
          l = 'N/A';
          F = Event.Activity_Type__c.getDescribe();
          PL = F.getPicklistValues();
          for (Schema.PicklistEntry p : PL) {
            if (e.Activity_Type__c == p.getValue()) {
              l = p.getLabel();
            }
          }
          msgs.add('今回のステップ：#[' + l  + ']');
        }
        return msgs;
    }

    private Boolean checkCondition(){
        // only execute for Admins and Japan profiles
        String p = this.profileName;
        if (!(p.startsWith('CRM Admin') || p.startsWith('JP') || p=='System Administrator' || p=='システム管理者')) {
            return FALSE;
        }
        EventTrigger_PostToChatterProfiles__c cs = EventTrigger_PostToChatterProfiles__c.getValues(p);
        if (cs != NULL && cs.active__c && this.currentUser.DailyReportChatterGroupID__c != NULL) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    private void setAccountMap(List<Event> eventList) {
        Set<Id> accountIds = new Set<Id>();
        for (Event ev : eventList) accountIds.add(ev.AccountID);
        this.accountMap = new Map<ID, Account>([SELECT Id, Name FROM Account where Id IN :accountIds]);
    }
}