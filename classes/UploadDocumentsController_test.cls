@isTest
public with sharing class UploadDocumentsController_test {
    
    
    static testmethod void m1(){
        
        
        ID ProfileID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].ID;
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User(username='20121217@test.com',
                            alias = 'apitest',
                            userRoleId = portalRole.Id,
                            email='20121217@test.com', 
                            emailencodingkey='UTF-8',
                            lastname='Covidien',
                            languagelocalekey='en_US',
                            localesidkey='en_US',                                             
                            profileid = ProfileID,
                            timezonesidkey='Europe/Berlin',
                            Region__c = 'EU',
                            Business_Unit__c = 'All'
                            );
        insert u;       
        //User u = [SELECT Id, Name FROM User Where IsActive = true and Profile.Name = 'EU - S2' Limit 1];
        System.runAs(u)
        {   
            RecordType rt1=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
            Account a = new Account(Name='TestAcct',recordtypeId = rt1.Id);
            insert a;
            
            Apexpages.currentPage().getParameters().put('id',a.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            UploadDocumentsController obj = new UploadDocumentsController(sc);
            
            
            obj.lstwrap = new List<UploadDocumentsController.DocumentWrap>();
            obj.lstwrap.add(new UploadDocumentsController.DocumentWrap(0,new Distributor_Document__c(Name='test',Document_type__c='test'),new Attachment(body=blob.valueof('123'),name='test')));
            obj.saveData();
            
        }    
    }

}