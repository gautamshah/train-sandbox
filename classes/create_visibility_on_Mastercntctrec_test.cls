/****************************************************************************************
   * Name    : create_visibility_on_Mastercntctrec_test
   * Author  : Lakhan Dubey
   * Date    : 19/12/2014
   * Purpose :test class for trigger  create_visibility_on_Mastercntctrec
   
 *****************************************************************************************/
 
 @istest
       public class create_visibility_on_Mastercntctrec_test {
   
    
        static testmethod  void   editorcreateocr (){
    
        Account a =new Account(name='TestAcc');
        insert a;
        Id recordtypeID=[Select Id from recordtype where name='Master Clinician' and SobjectType='Contact'].Id;
        Contact Co=new contact(lastName='testcon1',AccountId=a.id,recordtypeId=recordtypeID);
        insert Co;
        Contact C=new contact(lastName='testcon2',AccountId=a.id,Master_Contact_Record__c=Co.Id);
        insert C;
        Opportunity op=new Opportunity(Name='tesopp',StageName='Identify',CloseDate=system.today());
        insert op;
        
      
        Opportunity_Contact_Role__c ocr = new Opportunity_Contact_Role__c();
        ocr.Opportunity_Contact__c=c.id;
        ocr.Contact_Role__c='Business User';
        ocr.Level_of_Support__c='Champion';
        ocr.Opportunity_del__c=op.id;
        Test.starttest();
        insert ocr; 
        Test.stoptest();
        
        System.assertEquals(c.Master_Contact_Record__c,[Select MasterContactrecord__c from Opportunity_Contact_Role__c where Id=:ocr.id].MasterContactrecord__c);
     
    
    }
    

}