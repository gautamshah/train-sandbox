/****************************************************************************************
* Name    : ActivityReferencerBatch
* Author  : Gautam Shah
* Date    : 4/20/2016
* Purpose : Used to establish the references from Activity to Activity_Detail__c object
*			Used after running ActivityMigratorBatch
* 
* Dependancies: ActivityFieldMigrator
* Referenced By: 
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* 	DATE         	AUTHOR                  CHANGE
* 	----          	------                  ------
*	
*****************************************************************************************/
public class ActivityReferencerBatch implements Database.Batchable<sObject> 
{
	private String query;
    private String objName;
    private String indicatorField;
    private String indicatorValue;
	
	public ActivityReferencerBatch(String objName, String query, String indicatorField, String indicatorValue)
	{
        this.objName = objName;
        this.query = query;
        this.indicatorField = indicatorField;
        this.indicatorValue = indicatorValue;
	}
    public Database.Querylocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		System.Savepoint sp = Database.setSavepoint();
        try
        {          
            if(objName == 'Event')
            {
                List<Event> events = new List<Event>();
               	events.addAll((List<Event>) scope);
                ActivityReferencer.setEventReference(events,indicatorField,indicatorValue);
            }
            else if(objName == 'Task')
            {
                List<Task> tasks = new List<Task>();
               	tasks.addAll((List<Task>) scope);
                ActivityReferencer.setTaskReference(tasks,indicatorField,indicatorValue);
            }
        }
        catch(Exception e)
        {
            Database.rollback(sp);
            System.debug(e.getMessage());
        }
	}
	public void finish(Database.BatchableContext BC){}
}