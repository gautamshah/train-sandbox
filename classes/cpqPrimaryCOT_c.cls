/**
Build where predicates for selecting Participating Facilities or Dealers

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-12/06      Henry Noerdlinger   Created
===============================================================================
*/
public class cpqPrimaryCOT_c extends cpqClassOfTrade_c 
{
  	protected final String classOfTradeFieldName = 'Class_of_Trade__c';

  	public cpqPrimaryCOT_c(String row)
  	{
		super(row);
  	}
  	
   // (not Account__r.Class_of_Trade__c in ('003'))
   public override String buildParticipatingFacilityExpression(String obj)
   {
      String expr = ' ( ';
      if (exclude) 
      {
      	expr += ' NOT ';
      }
      
      if (obj != null)
      {
      	expr += obj + '.';
      }

      expr += classOfTradeFieldName + ' IN (\''+ value + '\')';
      return expr + ')';
   }
   
   /* this is negation of participating facility */
      public String buildDistributorExpression()
	  {
	  	return buildDistributorExpression(null);
	  }
  	
   public override String buildDistributorExpression(String obj)
   {
	  if (exclude) 
	  {
	  	 return buildParticipatingFacilityExpression(obj).replaceAll(' NOT ', '');
	  }
	  else
	  {
		return buildParticipatingFacilityExpression(obj).replaceAll(classOfTradeFieldName, ' NOT ' + classOfTradeFieldName);
	  }
	  	
   }
   
   public String inverse(String obj, boolean forDistributor)
   {
   	
   	 String expr = this.buildDistributorExpression(obj);
     if (forDistributor)
 	 {
 	  //yes, we actually need to peel away enclosing parens
 	  expr = expr.substring(3, expr.length());
      expr = expr.substring(0, expr.length() - 1);
 	 }
     System.debug('INVERSE ' + expr);
     return expr;

   }
	
}