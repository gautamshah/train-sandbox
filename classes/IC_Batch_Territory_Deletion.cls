/**
Batch Job facilitates IC Territory Deletion
	IC_Execute_Territory_Deletion passes list of territories need to be deleted in the right order to this batch
	Territory Deletion runs fast if delete one at time - batch size should set to 1.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-05-13      Yuli Fintescu		Created
2015-08-12		Bill Shan			Changed batch size to 200 in line #19
									Added line #66 to avoid execute another batch in the finish method.
===============================================================================

IC_Batch_Territory_Deletion p = new IC_Batch_Territory_Deletion();
Database.executeBatch(p, 1);
*/
global class IC_Batch_Territory_Deletion implements Database.Batchable<Sobject>{
	public Map<Integer, List<String>> toDelete;
	public Integer deleteLevel = 0;
	public Integer batchSize = 200;
	global Database.QueryLocator start(Database.BatchableContext BC) {
		List<String> deletes = new List<String>();
		if (deleteLevel >= 0 && toDelete != null && toDelete.containskey(deleteLevel)) {
			deletes = toDelete.get(deleteLevel);
		}
		String query = 'Select ID, Custom_External_TerritoryID__c From Territory Where flagToDelete__c = TRUE and ID in (' + inListFromList(deletes) + ')';
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<Territory> scope) {
		System.Debug('*** scope ' + scope);
		
		Database.DeleteResult[] results = database.delete(scope, false);
    	for (Integer i = 0; i < results.size(); i ++) {
			Database.DeleteResult result = results[i];
			Territory errored = scope[i];
			
        	if (!result.isSuccess()) {
            	for(Database.Error err : result.getErrors())
            		System.Debug('*** result: IC_Batch_Territory_Deletion, ' + err.getStatusCode() + ' - ' + err.getMessage() + ', Territory: ' + errored.Custom_External_TerritoryID__c + ', ID: ' + errored.Id);
        	}
		}
	}
	
 	private static String inListFromList(List<String> input) {
 		String s = '';
		for (String a : input) {
			if (String.isEmpty(s))
				s = '\'' + a + '\'';
			else
				s = s + ', \'' + a + '\'';
		}
		
		if (String.isEmpty(s))
			s = '\'^^THERE IS NOTHING IN THE INLIST^^\'';
		return s;
 	}
 	
	global void finish(Database.BatchableContext BC) {
		Integer nextLevel = deleteLevel - 1;
		if (nextLevel >= 0 && toDelete != null && toDelete.containskey(nextLevel)) {
			IC_Batch_Territory_Deletion p = new IC_Batch_Territory_Deletion();
            p.toDelete = toDelete;
            p.deleteLevel = nextLevel;
            p.batchSize = batchSize;
            System.Debug('*** p.deleteLevel ' + p.deleteLevel);
            if(!Test.isRunningTest())
            	Database.executeBatch(p, batchSize);
		}
	}
}