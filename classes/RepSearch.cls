public with sharing class RepSearch {
    
    /****************************************************************************************
* Name    : RepSearch
* Author  : Shawn Clark
* Date    : 07/20/2015 
* Purpose : Allows Users to Search for which Sales Rep Covers a specific Therapy or Product Line
            For MITG - Real-time retrieval of data from SFDC org using Apex to handle joins
            For MDT - Data is loaded to custom object via DI process and queried using SOQL
*****************************************************************************************/
    
    //Wrapper Class
    
    public class AccountSalesReps 
    {
        public string userguid {get;set;}
        public string name {get;set;}
        public string therapy {get;set;}
        public string bu {get;set;}
        public string title {get;set;}
        public string email {get;set;}
        public string workphone {get;set;}
        public string cellphone {get;set;}
        public string photo {get;set;}
        public string mgrguid {get;set;}
        public string mgrname {get;set;}
    }
    
    //Search Variables
    public transient String searchTerm {get; set;}  //Account Autocomplete Term
    public String SelectedOrg {get; set;}  
    public string PostalCodeInput {get;set;}
    
    //Selected Account Variables
    public String SelectedAct2 {get;set;}  //This one will be either Account GUID (MITG) or Account External ID (MDT). Toggle Controlled by JavaScript based on Selected Org
    public String selectedAccountId {get; set;} 
    public String selectedAccountName {get; set;} 
    public String selectedZip {get; set;}
    public String selectedAccountCity {get; set;}
    public String selectedAccountState {get; set;}

    //Toggle Variables
    Public boolean IsAccountPicklistVisible {get;set;}
    Public boolean IsCityPicklistVisible {get;set;}
    Public boolean ShowResults {get;set;}
    Public boolean ShowAccountBox {get;set;}
    public String CurrentPlatform {get; set;}
    public String DesktopOrMobile {get; set;}

    //Constructor
    Public RepSearch() {
        IsAccountPicklistVisible = False;
        IsCityPicklistVisible = False;
        ShowAccountBox = False;
        ShowResults = False;
        DesktopOrMobile = UserInfo.getUiThemeDisplayed();  //Spring 16 Method of Identifying User Platform
        
        IF (DesktopOrMobile == 'Theme4t') {
            CurrentPlatform = 'Mobile';
        }
        Else {
            CurrentPlatform = 'Desktop';
        }    
    }
    
    //Autocomplete JSRemoting Account Name
    @RemoteAction
    public static List<Sobject> searchAccount(String searchTerm, String selectedOrg) {
        String Orgs = selectedOrg;
        if (selectedOrg == 'MDT') {
            List<ZipToAccount__c> accounts = Database.query('SELECT ID, Cust_Num__c, Cust_Name__c, City__c, State__c, Postal_Code_5_Digit__c FROM ZipToAccount__c WHERE Org__c = :Orgs and  Cust_Name__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'  ORDER BY Cust_Name__c LIMIT 100');
            return accounts;
        }
        else  {
            List<AggregateResult> accounts2 = Database.query('SELECT ID ID, Account_External_ID__C  Cust_Num__c, Name Cust_Name__c, BillingCity City__c, BillingState State__c, BillingPostalCode Postal_Code_5_Digit__c FROM Account WHERE Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' GROUP BY ID, Account_External_ID__C, Name, BillingCity, BillingState, BillingPostalCode  ORDER BY Name LIMIT 100');    
            return accounts2;
        }    
    }   
    
    //When User Changes Org, Values must be reset
    Public Void ResetPage() {
        IsAccountPicklistVisible = False;
        selectedAccountName = '';
        selectedAccountId = '';
        selectedAccountCity = '';
        selectedAccountState = '';
        selectedZip = '';
        PostalCodeInput = '';
        IsCityPicklistVisible = False;  
        ShowResults = False;   
    }
    
    
    public List<SelectOption> getOrgs() {
        List<SelectOption> options = new List<SelectOption>();
       // options.add(new SelectOption('','-- Select an Org --'));  Default it to MITG instead
        options.add(new SelectOption('MITG', 'MITG')); 
        options.add(new SelectOption('MDT', 'MDT'));   
        return options;
    }     
    
    public List<SelectOption> getStates() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select a State --'));  
        options.add(new SelectOption('AK', 'AK'));
        options.add(new SelectOption('AL', 'AL'));
        options.add(new SelectOption('AR', 'AR'));
        options.add(new SelectOption('AZ', 'AZ'));
        options.add(new SelectOption('CA', 'CA'));
        options.add(new SelectOption('CO', 'CO'));
        options.add(new SelectOption('CT', 'CT'));
        options.add(new SelectOption('DC', 'DC'));
        options.add(new SelectOption('DE', 'DE'));
        options.add(new SelectOption('FL', 'FL'));
        options.add(new SelectOption('GA', 'GA'));
        options.add(new SelectOption('HI', 'HI'));
        options.add(new SelectOption('IA', 'IA'));
        options.add(new SelectOption('ID', 'ID'));
        options.add(new SelectOption('IL', 'IL'));
        options.add(new SelectOption('IN', 'IN'));
        options.add(new SelectOption('KS', 'KS'));
        options.add(new SelectOption('KY', 'KY'));
        options.add(new SelectOption('LA', 'LA'));
        options.add(new SelectOption('MA', 'MA'));
        options.add(new SelectOption('MD', 'MD'));
        options.add(new SelectOption('ME', 'ME'));
        options.add(new SelectOption('MI', 'MI'));
        options.add(new SelectOption('MN', 'MN'));
        options.add(new SelectOption('MO', 'MO'));
        options.add(new SelectOption('MS', 'MS'));
        options.add(new SelectOption('MT', 'MT'));
        options.add(new SelectOption('NC', 'NC'));
        options.add(new SelectOption('ND', 'ND'));
        options.add(new SelectOption('NE', 'NE'));
        options.add(new SelectOption('NH', 'NH'));
        options.add(new SelectOption('NJ', 'NJ'));
        options.add(new SelectOption('NM', 'NM'));
        options.add(new SelectOption('NV', 'NV'));
        options.add(new SelectOption('NY', 'NY'));
        options.add(new SelectOption('OH', 'OH'));
        options.add(new SelectOption('OK', 'OK'));
        options.add(new SelectOption('OR', 'OR'));
        options.add(new SelectOption('PA', 'PA'));
        options.add(new SelectOption('RI', 'RI'));
        options.add(new SelectOption('SC', 'SC'));
        options.add(new SelectOption('SD', 'SD'));
        options.add(new SelectOption('TN', 'TN'));
        options.add(new SelectOption('TX', 'TX'));
        options.add(new SelectOption('UT', 'UT'));
        options.add(new SelectOption('VA', 'VA'));
        options.add(new SelectOption('VT', 'VT'));
        options.add(new SelectOption('WA', 'WA'));
        options.add(new SelectOption('WI', 'WI'));
        options.add(new SelectOption('WV', 'WV'));
        options.add(new SelectOption('WY', 'WY'));
        return options;
    }
     
    public list<SelectOption> getUniqueCity() {
        System.debug('MY Selected Org for MITG is: ' + SelectedOrg);
        System.debug('MY Selected Account State is: ' + selectedAccountState);
        //********************************************
        IF(SelectedOrg ==  'MITG') {
        //********************************************
            list<SelectOption> options = new list<SelectOption>();
            set<String> setCities = new Set<String>();
            list<String> UniqueCities = new List<String>();
            
            options.add(new SelectOption('','-- Select a City--'));
            
  
            for (list<ZipToAccount__c> CityList : [SELECT City__c FROM ZipToAccount__c WHERE Org__c = :selectedOrg and State__c like :selectedAccountState  ORDER BY City__c LIMIT 999]) {
                for (ZipToAccount__c za : CityList) {
                    options.add(new SelectOption(za.City__c, za.City__c));
                } 
            } 
            return options; 
            
        } 
        //********************************************
        ELSE {
             System.debug('MY Selected Org for MDT is: ' + SelectedOrg);
        //********************************************
            list<SelectOption> options = new list<SelectOption>();
            list<ZipToAccount__c> CityList = new List<ZipToAccount__c>();
            set<String> setCities = new Set<String>();
            list<String> UniqueCities = new List<String>();
            
            options.add(new SelectOption('','-- Select a City--'));
            
            CityList = [SELECT City__c FROM ZipToAccount__c WHERE Org__c = :selectedOrg and State__c like :selectedAccountState  ORDER BY City__c LIMIT 999] ;
            
            for (Integer i = 0; i< CityList.size(); i++)
            {
                setCities.add(CityList[i].City__c.toUpperCase()); // Set dedups city values in list
            }
            
            UniqueCities.addAll(setCities);   
            
            for (Integer i = 0; i< UniqueCities.size(); i++)
            {
                options.add(new SelectOption(UniqueCities[i],UniqueCities[i] )); // contains distict accounts
            }
            return options; 
        } 
    }
    
    public list<SelectOption> getAccountsByPostalCode() {
        //********************************************
        IF (SelectedOrg == 'MITG') { 
        //********************************************
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('','-- Select an Account --'));        
            
            for (list<Account> rts : [SELECT ID, Name, Account_External_ID__C, BillingCity, BillingState, BillingPostalCode FROM Account WHERE BillingPostalCode = :PostalCodeInput ORDER BY Name]) {
                for (Account rt : rts) {
                    options.add(new SelectOption(rt.ID, rt.Name + ' (' + rt.Account_External_ID__c + ')'));
                } 
            } 
            return options; 
        } 
        
        //********************************************
        ELSE { 
        //********************************************
            
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('','-- Select an Account --'));        
            
            for (list<ZipToAccount__c> rts : [SELECT Cust_Num__c, Cust_Name__c, City__c, State__c, Postal_Code_5_Digit__c  FROM ZipToAccount__c WHERE Org__c = :selectedOrg and Postal_Code_5_Digit__c = :PostalCodeInput  ORDER BY Cust_Name__c]) {
                for (ZipToAccount__c rt : rts) {
                    options.add(new SelectOption(rt.Cust_Num__c, rt.Cust_Name__c + ' (' + rt.Cust_Num__c + ')'));
                } 
            } 
            return options; 
        }
    }      
    
    public list<SelectOption> getAccountsByState() {
        //********************************************
        IF (SelectedOrg == 'MITG') { 
        //********************************************
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('','-- Select an Account --'));        
            
            for (list<Account> rts : [SELECT ID, Name, Account_External_ID__C, BillingCity, BillingState, BillingPostalCode FROM Account WHERE BillingState = :selectedAccountState  and BillingCity = :selectedAccountCity  ORDER BY Name]) {
                for (Account rt : rts) {
                    options.add(new SelectOption(rt.ID, rt.Name + ' (' + rt.Account_External_ID__c + ')'));
                  
                } 
            } 
            return options; 
        } 
        
        //********************************************
        ELSE { 
        //********************************************
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('','-- Select an Account --'));        
            
            for (list<ZipToAccount__c> rts : [SELECT Cust_Num__c, Cust_Name__c, City__c, State__c, Postal_Code_5_Digit__c FROM ZipToAccount__c WHERE Org__c = :selectedOrg and State__c = :selectedAccountState  and City__c = :selectedAccountCity  ORDER BY Cust_Name__c]) {
                for (ZipToAccount__c rt : rts) {
                    options.add(new SelectOption(rt.Cust_Num__c, rt.Cust_Name__c + ' (' + rt.Cust_Num__c + ')'));
                } 
            } 
            return options; 
        }
    }  
    
    
    public list<Sales_Definitions__c> getDefinitions() {
       List<Sales_Definitions__c> definitions  = Database.query('SELECT Term__c, Definition__c, Examples__c, Resources__c FROM Sales_Definitions__c');
       return definitions;
    }    
    
    public Void UpdateForm() {
        IsAccountPicklistVisible = True;
    }
    
    public Void ShowAccounts() {
        IsAccountPicklistVisible = True;
    }
    
    
    public Void ShowCities() {
        IsCityPicklistVisible = True;
    }      
    


    public void RenderResults(){
        ShowResults = True; 
        ShowAccountBox = True;
    }

 
        //Tracking 
    
    public pageReference trackVisits(){
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1]; 
        String strurl_abb = strurl.substringBefore('?');
        
        VF_Tracking__c newTracking = new VF_Tracking__c(
            User__c = UserInfo.getUserId(),
            VF_Page_Visited__c = strurl_abb,
            Date_Time_Visited__c = datetime.now());
        insert newTracking;
        return null;
    }  
    
    

    //*****************************************************************
    //Logic called by all Pages to retrieve Rep Details & Therapies 
    //*****************************************************************
    
    public list<AccountSalesReps> getAccountTeams()
    {
        //********************************************
        IF(SelectedOrg == 'MITG') {
            //********************************************
            System.debug('MY Selected Account for MITG is: ' + SelectedAct2);
            //Get Account Details to return back to page for display
            list <Account> Acts = [SELECT Name, Account_External_ID__C, BillingCity, BillingState, BillingPostalCode FROM Account where ID = :SelectedAct2 LIMIT 1];
            selectedAccountName = Acts[0].Name;
            selectedAccountId = Acts[0].Account_External_ID__C;
            selectedAccountCity = Acts[0].BillingCity;
            selectedAccountState = Acts[0].BillingState;
            selectedZip = Acts[0].BillingPostalCode;
            
            list<AccountSalesReps> aList = new list<AccountSalesReps>();
            Set<String> AK_TerritoryIds = new set<String>();
            
            // Get all RelatedIds from Group object for Groups aligned to Account
            
            for(Group at : [Select RelatedId From Group 
                            where Type = 'Territory' and Id in 
                            (Select UserOrGroupId From AccountShare 
                             Where RowCause in ('Territory', 'TerritoryManual') 
                             and AccountId = :SelectedAct2)])
            {
                AK_TerritoryIds.add(at.RelatedId);
                
            }
            
            
            //Get a distinct set of users
            set<Id> userSet = new set<Id>();
            
            for(UserTerritory ut :[Select u.UserId,  u.TerritoryId, u.Id 
                                   From UserTerritory u
                                   where TerritoryId in :AK_TerritoryIds
                                   And isActive = True
                                   order by UserId])
            {
                userSet.add(ut.UserId);
            }
            
            
            //User Map
            Map<id,User> UserDetails = new Map<id,User>([select Id, Name, Business_Unit__c, MobilePhone, Email, Title, Phone, SmallPhotoURL, ManagerID, Manager.Name
                                                         from User
                                                         where Id in :userSet]);
            
            for (Rep_to_Therapy__c  atm : [select User_ID__c, Therapy__c from Rep_to_Therapy__c where User_ID__c in :userSet])
            {
                AccountSalesReps atr = new AccountSalesReps();
                atr.userguid = atm.User_ID__c;
                atr.therapy = atm.Therapy__c; 
                atr.name = UserDetails.get(atr.userguid).Name;            
                atr.bu = UserDetails.get(atr.userguid).Business_Unit__c;
                atr.title = UserDetails.get(atr.userguid).title;
                atr.email = UserDetails.get(atr.userguid).email;
                atr.workphone = UserDetails.get(atr.userguid).phone;
                atr.cellphone = UserDetails.get(atr.userguid).mobilephone;
                atr.photo = UserDetails.get(atr.userguid).SmallPhotoURL;
                atr.mgrguid = UserDetails.get(atr.userguid).ManagerID;
                atr.mgrname = UserDetails.get(atr.userguid).Manager.Name;
                
                aList.add(atr);
                
            }
            
            return aList;
        }
        //********************************************
        ELSE {
            //********************************************
            list<AccountSalesReps> aList = new list<AccountSalesReps>();
            System.debug('MY Selected Account for MDT is: ' + SelectedAct2);
            //Get Account Details to return back to page for display
            list <ZipToAccount__c> Acts = [SELECT Cust_Num__c, Cust_Name__c, City__c, State__c, Postal_Code_5_Digit__c FROM ZipToAccount__c WHERE Cust_Num__c = :SelectedAct2 LIMIT 1];
            selectedAccountName = Acts[0].Cust_Name__c;
            selectedAccountId = Acts[0].Cust_Num__c;
            selectedAccountCity = Acts[0].City__c;
            selectedAccountState = Acts[0].State__c;
            selectedZip = Acts[0].Postal_Code_5_Digit__c;
            
            for (MDT_RepDetail__c atm : [SELECT Job_Title__c, Name__c, BU__c, Therapy__c, Work_Phone__c, Cell_Phone__c, Email__c, Photo__c, User_GUID__c, MGR_Guid__c, MGR_Name__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Cust_Num__c = :SelectedAct2 Order by Therapy__c, Name__c])
            {
                AccountSalesReps atr = new AccountSalesReps();
                atr.userguid = atm.User_GUID__c;
                atr.therapy = atm.Therapy__c; 
                atr.name = atm.Name__c;            
                atr.bu = atm.BU__c;
                atr.title = atm.Job_Title__c;
                atr.email = atm.Email__c;
                atr.workphone = atm.Work_Phone__c;
                atr.cellphone = atm.Cell_Phone__c;
                atr.photo = atm.Photo__c;
                atr.mgrguid = atm.MGR_Guid__c;
                atr.mgrname = atm.MGR_Name__c;
                
                aList.add(atr);
                
            }
            
            return aList;
        }
        
    }
    
    
    
    public String[] getMyTherapy()
    {
        //********************************************
        IF(SelectedOrg == 'MITG') 
        {
            //********************************************
            list <Rep_To_Therapy__c> TherapyList = new list <Rep_To_Therapy__c>();
            set<String> setTherapies = new Set<String>();
            list <String> Therapy = new list <String>();
            Set<String> AK_TerritoryIds = new set<String>();
            
            // Get all RelatedIds from Group object for Groups aligned to Account
            
            for(Group at : [Select RelatedId From Group 
                            where Type = 'Territory' and Id in 
                            (Select UserOrGroupId From AccountShare 
                             Where RowCause in ('Territory', 'TerritoryManual') 
                             and AccountId = :SelectedAct2)])
            {
                AK_TerritoryIds.add(at.RelatedId);
                
            }
            
            //Get a distinct set of users
            set<Id> userSet = new set<Id>();
            
            for(UserTerritory ut :[Select u.UserId,  u.TerritoryId, u.Id 
                                   From UserTerritory u
                                   where TerritoryId in :AK_TerritoryIds
                                   And isActive = True
                                   order by UserId])
            {
                userSet.add(ut.UserId);
            }
            
            
            TherapyList = [SELECT Therapy__c FROM Rep_To_Therapy__c Where User_ID__c IN :userSet Order by Therapy__c]; 
            
            for (Integer i = 0; i< TherapyList.size(); i++)
            {
                setTherapies.add(TherapyList[i].Therapy__c); // Set to dedup Therapy List
            }
            
            // convert the set into a string array  
            Therapy = new String[setTherapies.size()];
            Integer i = 0;
            for (String t : setTherapies) { 
                Therapy[i] = t;
                i++;
            } 
            return Therapy;
        }     
        
        //********************************************
        ELSE {
            //********************************************
            list <MDT_RepDetail__c> TherapyList = new list <MDT_RepDetail__c>();
            set<String> setTherapies = new Set<String>();
            list <String> Therapy = new list <String>();
            TherapyList = [SELECT Therapy__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Cust_Num__c = :SelectedAct2 Order by Therapy__c] ;
            
            for (Integer i = 0; i< TherapyList.size(); i++)
            {
                setTherapies.add(TherapyList[i].Therapy__c); // contains distict accounts
            }
            
            Therapy = new String[setTherapies.size()];
            Integer i = 0;
            for (String t : setTherapies) { 
                Therapy[i] = t;
                i++;
            }return Therapy;
            
        }
    } 
    
  
        
    
}