public class RecordType_cache extends sObject_cache
{
	public static RecordType_cache get() { return cache; }

	private static RecordType_cache cache
	{
		get
		{
			if (cache == null)
				cache = new RecordType_cache();

			return cache;
		}

		private set;
	}

	private RecordType_cache()
	{
		super(RecordType.getSObjectType(), RecordType.field.Id);
		this.addIndex(RecordType.field.DeveloperName);
		this.addIndex(RecordType.field.sObjectType);
		this.addIndex(RecordType.field.IsActive);
		this.addIndex(RecordType.field.Name);
	}
}