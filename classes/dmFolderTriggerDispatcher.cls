public with sharing class dmFolderTriggerDispatcher extends dmTriggerDispatcher
{
	public static void main(
			boolean isExecuting,
		 	boolean isInsert,
		 	boolean isUpdate,
		 	boolean isDelete,
		 	boolean isBefore,
		 	boolean isAfter,
		 	boolean isUndelete,
		 	List<dmFolder__c> newList,
		 	Map<Id, dmFolder__c> newMap,
		 	List<dmFolder__c> oldList,
		 	Map<Id, dmFolder__c> oldMap,
		 	integer size
		)
	{
        dmFolder.main(isExecuting, isInsert, isUpdate, isDelete, isBefore, isAfter, isUndelete, newList, newMap, oldList, oldMap, size);
	}
}