@isTest
private class TestFieldVisitApexShare_Trigger {

    static testMethod void runTest() {
        Test.startTest(); 
        ID profileID = [ Select id from Profile where Name =: 'System Administrator' Limit 1].id;
        ID USSUSprofileID = [ Select id from Profile where Name =: 'US - SUS' Limit 1].id;
        User u_FST = new User( FirstName = 'Test-01'
                         , LastName = 'Test-02'
                         , Title = 'Rep'
                         , email='testinguser@fakeemail.com'
                         , profileid = USSUSprofileID
                         , UserName='testinguser1212@fakeemail.com'
                         , alias='tusary1'
                         , CommunityNickName='tusary1'
                         , TimeZoneSidKey='America/New_York'
                         , LocaleSidKey='en_US'
                         , EmailEncodingKey='ISO-8859-1'
                         , LanguageLocaleKey='en_US');
        
        insert u_FST;
        User u_Seller = new User( FirstName = 'Test-011'
                         , LastName = 'Test-022'
                         , Title = 'Rep'
                         , email='testinguser@fakeemail.com'
                         , profileid = USSUSprofileID
                         , UserName='testinguser121212@fakeemail.com'
                         , alias='tusary'
                         , CommunityNickName='tusary'
                         , TimeZoneSidKey='America/New_York'
                         , LocaleSidKey='en_US'
                         , EmailEncodingKey='ISO-8859-1'
                         , LanguageLocaleKey='en_US');
        
        insert u_Seller;
        
        User u_RM = new User( FirstName = 'Test-0222'
                         , LastName = 'Test-0222'
                         , Title = 'Rep'
                         , email='testinguser2@fakeemail.com'
                         , profileid = USSUSprofileID
                         , UserName='testinguser12121233@fakeemail.com'
                         , alias='tusary2'
                         , CommunityNickName='tusary2'
                         , TimeZoneSidKey='America/New_York'
                         , LocaleSidKey='en_US'
                         , EmailEncodingKey='ISO-8859-1'
                         , LanguageLocaleKey='en_US');
        
        insert u_RM;
        
        
        
        RecordType rtc=[Select Id from RecordType where DeveloperName='FST_Field_Visit_1' and SobjectType='Field_Visit__c' Limit 1];
          //Create portalusercontac
        Field_Visit__c FieldVisit = new Field_Visit__c(         
        RecordtypeID=rtc.id,
        Seller__c=u_Seller.Id,
        FST__c=u_FST.Id, 
        RM__c= u_RM.Id         
        );            
        
        Database.insert(FieldVisit);   
          
        List<Field_Visit__Share> FldVisitShrs = [SELECT Id, UserOrGroupId, AccessLevel, 
        RowCause FROM Field_Visit__Share WHERE ParentId = :FieldVisit.Id AND UserOrGroupId= :FieldVisit.Seller__c limit 1];
      
        // Test for only one manual share on Field Visit checking
        if(FldVisitShrs.size()>0)
        System.assertEquals(FldVisitShrs.size(), 1, 'Set the object\'s sharing model');
      
        // Test attributes of manual share 
        if(FldVisitShrs.size()>0)    
        System.assertEquals(FldVisitShrs[0].UserOrGroupId, u_Seller.Id);
      
        Test.stopTest();
    }
}