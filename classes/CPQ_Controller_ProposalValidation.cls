/**
Controller for CPQ_ProposalValidation page
Populate the banner on proposal detail page with message returned from partner validation WS or custom validation

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
2016-07-05      Isaac Lewis         Added doValidateRebates() logic
2016-09-08      Isaac Lewis         Added doValidateCOOPAmendment() logic
===============================================================================
*/
public with sharing class CPQ_Controller_ProposalValidation {
    public ID theRecordID {get; set;}
    private SObject theRecord;
    private SObjectType theRecordSOType;
    private Apttus_Proposal__Proposal__c theProposal;
    public List<String> validationMessagesCustom {get;set;}

    //purpose parm indicates what action to take after validation
    private String PagePurpose;

    //This page is used in Activate Agreement button.
    //After validating, the page will be direct to Apttus__AgreementActivate page
    public Boolean getIsActivateAgreement() {
        return PagePurpose == 'Activate';
    }

    public CPQ_Controller_ProposalValidation(ApexPages.StandardController controller) {
        theRecordID = controller.getId();
        theRecord = controller.getRecord();
        theRecordSOType = theRecordID.getSObjectType();
        if(theRecordSOType == Apttus_Proposal__Proposal__c.sObjectType) {
            // Grab all child queries from single parent query for efficiency
            theProposal = [SELECT Id, RecordType.DeveloperName, Deal_Type__c, T_AMD_Equipment__c, T_AMD_ContractPeriod__c, T_AMD_AnnualCommit__c, T_AMD_CommittedPricing__c, T_AMD_Consumable__c, T_AMD_AwardCredit__c, T_AMD_AwardCreditEligibility__c, T_AMD_Facilities__c, T_AMD_Accrual_Credit__c, T_AMD_Rebate__c, T_AMD_TBD__c, (SELECT Id, RecordType.DeveloperName FROM Rebates__r) FROM Apttus_Proposal__Proposal__c WHERE Id = :theRecordId];
        }

        validationMessagesCustom = new List<String>();
        PagePurpose = ApexPages.currentPage().getParameters().get('purpose');
    }

    public pageReference doValidation() {
        try {
            validationMessages = CPQ_PartnerContractWSInvoke.InvokeValidationCheckFromProposal(theRecordID);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        doValidateRebates();
        doValidateCOOPAmendment();
        return null;
    }

    public Boolean showCloseButton {get; set;}
    public pageReference doValidationLite() {
        try {
        	if (theRecord instanceof Apttus_Proposal__Proposal__c)
            	validationMessages = CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromProposal(theRecordID);
            else
            	validationMessages = CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(theRecordID);
            showCloseButton = (validationMessages.complaints.size() > 0);
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        doValidateRebates();
        doValidateCOOPAmendment();
        return null;
    }

    public CPQ_PartnerWSStruct validationMessages {
        get {
            if (validationMessages == null) {
                validationMessages = new CPQ_PartnerWSStruct();
            }
            return validationMessages;
        }
        set;
    }

    private void doValidateRebates () {

        if ( (theRecordSOType != Apttus_Proposal__Proposal__c.sObjectType) || !(theProposal.recordType.developerName == 'Rebate_Agreement') ) {
            return;
        }

        Boolean hasQualifyingRebate = false;
        Boolean hasPLIs = false;

        // Check for qualifying rebate
        if (theProposal.Rebates__r.size() > 0) {

            Set<String> rts = new Set<String>();
            rts.add('Early_Signing_Incentive');
            rts.add('PM_Growth_Rebate_Option_1_Growth_Rebat');
            rts.add('PM_Growth_Rebate_Option_2_Incremental');
            rts.add('PM_Standard_Rebate_Option_1_One_Rebate');
            rts.add('PM_Standard_Rebate_Option_2_Tiered_Reb');

            for (Rebate__c r : theProposal.Rebates__r) {
                if (rts.contains(r.recordType.developerName)) {
                    hasQualifyingRebate = true;
                    break;
                }
            }

        }

        // Check for existing line items
        if (hasQualifyingRebate) {
            Integer pliCount = [SELECT COUNT() FROM Apttus_Proposal__Proposal_Line_Item__c WHERE Apttus_Proposal__Proposal__c = :theRecordID];
            if(pliCount > 0) {
                hasPLIs = true;
            }
        }

        if (hasQualifyingRebate && !hasPLIs) {
            validationMessagesCustom.add('Remember to Configure Products by adding a COT (if all products desired) or Individual SKUs (if only a subset of a product line) that apply to your rebate.');
        }

    }

    private void doValidateCOOPAmendment () {

        // Must be COOP Amendment Proposal
        if (
            !(theRecordSOType == Apttus_Proposal__Proposal__c.SObjectType) ||
            !(theProposal.recordType.developerName.startsWith('COOP_Plus_Program')) ||
            !(theProposal.Deal_Type__c == 'Amendment')
        ) {
            return;
        }

        // Must have one amendment option checked
        if (
            !theProposal.T_AMD_Equipment__c &&
            !theProposal.T_AMD_ContractPeriod__c &&
            !theProposal.T_AMD_AnnualCommit__c &&
            !theProposal.T_AMD_CommittedPricing__c &&
            !theProposal.T_AMD_Consumable__c &&
            !theProposal.T_AMD_AwardCredit__c &&
            !theProposal.T_AMD_AwardCreditEligibility__c &&
            !theProposal.T_AMD_Facilities__c &&
            !theProposal.T_AMD_Accrual_Credit__c &&
            !theProposal.T_AMD_Rebate__c &&
            !theProposal.T_AMD_TBD__c
        ) {
            validationMessagesCustom.add('At least one item in "What is Being Amended" is required.');
        }

    }

}