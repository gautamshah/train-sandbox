global class EMSScheduleWeeklyReminderEmail Implements Schedulable
   {
     global void execute(SchedulableContext sc)
    {  
    Group EMSGCC = [Select Id, Name From Group Where Name = 'EMS GCC Approvers']; 
    List<GroupMember> groupMembers = [Select GroupId, UserOrGroupId From GroupMember Where GroupId = :EMSGCC.Id];
    for(GroupMember EachGroupmember : groupMembers){
    
      sendemail(EachgroupMember.UserorGroupId);
        
    }
    }
    
   public static void sendEmail(ID ActualUser) {
   
   EmailTemplate templateId = [Select id from EmailTemplate where name = 'EMS CPA Reminder Template'];
   
   //New instance of a single email message
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
   // Who you are sending the email to
   mail.setTargetObjectId(ActualUser);
   // The email template ID used for the email
   mail.setTemplateId(templateId.Id);          
   //mail.setWhatId(candidate);    
   mail.setBccSender(false);
   mail.setUseSignature(false);
   //mail.setReplyTo('recruiting@acme.com');
   mail.setSenderDisplayName('eCPA.DoNotReply');
   mail.setSaveAsActivity(false);   
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }  
  
 }