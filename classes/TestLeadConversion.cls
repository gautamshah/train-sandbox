/****************************************************************************************
* Name    : Lead Conversion Tests
* Author  : Mike Melcher
* Date    : 11-20-2011
* Purpose : Test methods for Lead Conversion.
* 
* Dependancies: 
*     - Requires class testUtility          
*      
* 	========================
* 	= MODIFICATION HISTORY =
* 	========================
* 	DATE        			AUTHOR              	CHANGE
* 	----        		------              	------
* 	4-23-2012    		MJM          			Placed Lead Conversion tests in separate class
* 	8-3-2012     		MJM          			Re-configured to used test account creation from TestUtility class
*	Dec. 22, 2014		Gautam Shah				Created new test method "testCustomLeadConversion" and commented-out other methods
*	Feb. 13, 2016		Gautam Shah				Updated testCustomLeadConversion method to be unit test for LeadConversion
*****************************************************************************************/
@isTest(SeeAllData=true)
public class TestLeadConversion 
{ 
	static testmethod void testCustomLeadConversion()
   	{
        Test.startTest();
   		ID RtID = Utilities.recordTypeMap_Name_Id.get('US - MITG Lead Type');
		Lead l1 = new Lead();
		l1.lastname = 'testlead1';
		l1.Company = 'testCompany1';
		l1.Email = 'triggertest1@testemail.com';
		l1.Country = 'US';
		l1.Street = 'Test Street';
		l1.City = 'Test City';
		l1.State = 'Test';
		l1.PostalCode = '12345';
		l1.RecordtypeId  = RtID;
		insert l1;
		
        Note n = new Note();
        n.ParentId = l1.Id;
        n.Title = 'Test Note';
        n.Body = 'Test Note';
        insert n;
        
		PageReference cloner = Page.LeadClone;
		ApexPages.currentPage().getParameters().put('id', l1.id);
        Test.setCurrentPageReference(cloner);
        ApexPages.StandardController clonerController = new ApexPages.StandardController(l1);
		LeadCloneController clonerClass = new LeadCloneController(clonerController);
        clonerClass.doClone();
        
        PageReference pageRef = Page.CustomLeadConversionV2;
		ApexPages.currentPage().getParameters().put('id', l1.id);
        Test.setCurrentPageReference(pageRef);
		ApexPages.StandardController lController = new ApexPages.StandardController(l1);
		LeadConversionExtensionV2 ext = new LeadConversionExtensionV2(lController);
		
        List<SelectOption> options = ext.getOPGList();
        for(SelectOption so : options)
        {
            if(so.getValue() == 'US:MS:Extended Care')
            {
                ext.OPG = so.getValue();
                break;
            }
        }
        //ext.OPG = options[1].getValue();
        System.debug('ext.OPG: ' + ext.OPG);
        System.assertNotEquals(null, ext.OPG);
        ext.fetchProducts();
        //System.assert(!ext.products.isEmpty());
        
        Pricebook2 pb = [Select Id From Pricebook2 Where Name = 'US:MS:Extended Care' Limit 1];
        /*
        leadObj: {"Id":"00QK0000009HuYPMA0","OwnerId":"005U0000001ujU6IAI","Country":"US"}
		acctObj: {"Name":"Test Co.","Phone":"(444) 333-2222","BillingStreet":"test","BillingCity":"Totonto","BillingState":"OH","BillingPostalCode":"44444","BillingCountry":"US"}
		contactObj: {"Salutation":"Mr.","FirstName":"Tester cl1","LastName":"Tester","Phone_Number_at_Account__c":"444-3333","Email":"test@test.com","Title":"Tstr.","Department_picklist__c":"Administration","Affiliated_Role__c":"Administrator"}
		oppObj: {"Name":"test","StageName":"Identify","CloseDate":1456030800000,"Pricebook2Id":"US:MS:Extended Care"}
		prodArray: [{"Id":"01uU0000001atuTIAQ","Quantity":"1","UnitPrice":"10.00","Description":""}]
		addOpp: true
		*/
        String jsonLead = '{"Id":"' + l1.Id + '","OwnerId":"' + l1.OwnerId + '","Country":"US"}';
        String jsonAccount = '{"Name":"Test Co.","Phone":"(444) 333-2222","BillingStreet":"test","BillingCity":"Totonto","BillingState":"OH","BillingPostalCode":"44444","BillingCountry":"US"}';
        String jsonContact = '{"Salutation":"Mr.","FirstName":"Tester cl1","LastName":"Tester","Phone_Number_at_Account__c":"444-3333","Email":"test@test.com","Title":"Tstr.","Department_picklist__c":"Administration","Affiliated_Role__c":"Administrator"}';
        String jsonOpp = '{"Name":"test","StageName":"Identify","CloseDate":' + Datetime.now().getTime() + ',"Pricebook2Id":"US:MS:Extended Care"}';
        String jsonProd = '[{"Id":"' + pb.Id + '","Quantity":"1","UnitPrice":"10.00","Description":""}]';
        Boolean addOpp = true;
        //public static Response tryConvert(String leadJSON, String accountJSON, String contactJSON, String oppJSON, String prodJSON, Boolean addOpp)
        System.debug('jsonLead: ' + jsonLead);
        System.debug('jsonAccount: ' + jsonAccount);
        System.debug('jsonContact: ' + jsonContact);
        System.debug('jsonOpp: ' + jsonOpp);
        System.debug('jsonProd: ' + jsonProd);
        LeadConversionExtensionV2.Response r = LeadConversionExtensionV2.tryConvert(jsonLead, jsonAccount, jsonContact, jsonOpp, jsonProd, addOpp);
        System.assertNotEquals(null, r);
        Test.stopTest();
   	}
    
    static testmethod void testCustomLeadConversion2()
   	{
        Test.startTest();
        OpportunityContactRole ocr = [Select Id, OpportunityId, ContactId, Opportunity.AccountId From OpportunityContactRole Limit 1];
   		ID RtID = Utilities.recordTypeMap_Name_Id.get('US - MITG Lead Type');
		Lead l1 = new Lead();
        l1.Opportunity__c = ocr.OpportunityId;
        l1.Contact__c = ocr.ContactId;
        l1.Account__c = ocr.Opportunity.AccountId;
		l1.lastname = 'testlead1';
		l1.Company = 'testCompany1';
		l1.Email = 'triggertest1@testemail.com';
		l1.Country = 'US';
		l1.Street = 'Test Street';
		l1.City = 'Test City';
		l1.State = 'Test';
		l1.PostalCode = '12345';
		l1.RecordtypeId  = RtID;
		insert l1;
		
        Note n = new Note();
        n.ParentId = l1.Id;
        n.Title = 'Test Note';
        n.Body = 'Test Note';
        insert n;
        
		PageReference cloner = Page.LeadClone;
		ApexPages.currentPage().getParameters().put('id', l1.id);
        Test.setCurrentPageReference(cloner);
        ApexPages.StandardController clonerController = new ApexPages.StandardController(l1);
		LeadCloneController clonerClass = new LeadCloneController(clonerController);
        clonerClass.doClone();
        
        PageReference pageRef = Page.CustomLeadConversionV2;
		ApexPages.currentPage().getParameters().put('id', l1.id);
        Test.setCurrentPageReference(pageRef);
		ApexPages.StandardController lController = new ApexPages.StandardController(l1);
		LeadConversionExtensionV2 ext = new LeadConversionExtensionV2(lController);
		
        List<SelectOption> options = ext.getOPGList();
        for(SelectOption so : options)
        {
            if(so.getValue() == 'US:MS:Extended Care')
            {
                ext.OPG = so.getValue();
                break;
            }
        }
        //ext.OPG = options[1].getValue();
        System.debug('ext.OPG: ' + ext.OPG);
        System.assertNotEquals(null, ext.OPG);
        ext.fetchProducts();
        //System.assert(!ext.products.isEmpty());
        
        Pricebook2 pb = [Select Id From Pricebook2 Where Name = 'US:MS:Extended Care' Limit 1];
        /*
        leadObj: {"Id":"00QK0000009HuYPMA0","OwnerId":"005U0000001ujU6IAI","Country":"US"}
		acctObj: {"Name":"Test Co.","Phone":"(444) 333-2222","BillingStreet":"test","BillingCity":"Totonto","BillingState":"OH","BillingPostalCode":"44444","BillingCountry":"US"}
		contactObj: {"Salutation":"Mr.","FirstName":"Tester cl1","LastName":"Tester","Phone_Number_at_Account__c":"444-3333","Email":"test@test.com","Title":"Tstr.","Department_picklist__c":"Administration","Affiliated_Role__c":"Administrator"}
		oppObj: {"Name":"test","StageName":"Identify","CloseDate":1456030800000,"Pricebook2Id":"US:MS:Extended Care"}
		prodArray: [{"Id":"01uU0000001atuTIAQ","Quantity":"1","UnitPrice":"10.00","Description":""}]
		addOpp: true
		*/
        String jsonLead = '{"Id":"' + l1.Id + '","OwnerId":"' + l1.OwnerId + '","Country":"US","Opportunity__c":"' + l1.Opportunity__c + '"}';
        String jsonAccount = '{"Name":"Test Co.","Phone":"(444) 333-2222","BillingStreet":"test","BillingCity":"Totonto","BillingState":"OH","BillingPostalCode":"44444","BillingCountry":"US"}';
        String jsonContact = '{"Salutation":"Mr.","FirstName":"Tester cl1","LastName":"Tester","Phone_Number_at_Account__c":"444-3333","Email":"test@test.com","Title":"Tstr.","Department_picklist__c":"Administration","Affiliated_Role__c":"Administrator"}';
        String jsonOpp = '{"Name":"test","StageName":"Identify","CloseDate":' + Datetime.now().getTime() + ',"Pricebook2Id":"US:MS:Extended Care"}';
        String jsonProd = '[{"Id":"' + pb.Id + '","Quantity":"1","UnitPrice":"10.00","Description":""}]';
        Boolean addOpp = true;
        //public static Response tryConvert(String leadJSON, String accountJSON, String contactJSON, String oppJSON, String prodJSON, Boolean addOpp)
        System.debug('jsonLead: ' + jsonLead);
        System.debug('jsonAccount: ' + jsonAccount);
        System.debug('jsonContact: ' + jsonContact);
        System.debug('jsonOpp: ' + jsonOpp);
        System.debug('jsonProd: ' + jsonProd);
        LeadConversionExtensionV2.Response r = LeadConversionExtensionV2.tryConvert(jsonLead, jsonAccount, jsonContact, jsonOpp, jsonProd, addOpp);
        System.assertNotEquals(null, r);
        Test.stopTest();
   	}
}