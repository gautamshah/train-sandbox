@isTest
private class Territory_gTest
{
	@isTest
	static void constructor()
	{
		Territory_g cache = new Territory_g();
		system.assertNotEquals(null, cache);
		
	}

	@isTest
	static void castMap()
	{
		Map<Id, sObject> input;
		
		Map<Id, Territory> result = Territory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		input = new Map<Id, sObject>();
		result = Territory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		Territory ut = [SELECT Id FROM Territory LIMIT 1];
		
		input.put(ut.Id, null);
		result = Territory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		input.put(ut.Id, ut);
		result = Territory_g.cast(input);
		system.assert(result != null &&
		              !result.isEmpty() &&
		              result.containsKey(ut.Id) &&
		              result.get(ut.Id) == ut);
	}
	
	@isTest
	static void castFieldMap()
	{
		Map<object, Map<Id, sObject>> input;
		Map<object, Map<Id, Territory>> result = Territory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		input = new Map<object, Map<Id, sObject>>();
		result = Territory_g.cast(input);
		system.assert(result != null && result.isEmpty());
		
		Territory ut = [SELECT Id, Custom_External_TerritoryID__c FROM Territory LIMIT 1];
		
		object key = ut.Custom_External_TerritoryID__c;
		input.put(key, new Map<Id, sObject>());
		result = Territory_g.cast(input);
		system.assert(result != null && !result.isEmpty());
		
		input.get(key).put(ut.Id, null);
		result = Territory_g.cast(input);
		system.assert(result != null && !result.isEmpty());

		input.get(key).put(ut.Id, ut);
		result = Territory_g.cast(input);
		system.assert(result != null &&
		              !result.isEmpty() &&
		              !result.get(key).isEmpty() &&
		              result.get(key).containsKey(ut.Id) &&
		              result.get(key).get(ut.Id) == ut);
	}
	
    @isTest
    static void fetch()
    {
		List<Territory> uts = [SELECT Id, Custom_External_TerritoryID__c FROM Territory LIMIT 10];
		set<object> keys = new set<object>();
		for(Territory ut : uts)
			keys.add(ut.Custom_External_TerritoryID__c);
  		
  		Territory_g cache = new Territory_g();
  		
  		Test.startTest();
  		
  		Map<object, Map<Id, Territory>> found = cache.fetch(Territory.field.Custom_External_TerritoryID__c, keys);
  		
  		Test.stopTest();

  		system.assert(found != null && found.isEmpty());
    }
}