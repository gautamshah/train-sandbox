public with sharing class CreateAffiliatedContactExtension {
    /****************************************************************************************
    * Name    : CreateAffiliatedContactExtension
    * Author  : Mike Melcher
    * Date    : 12-8-2011
    * Purpose : Creates a Connected Contact from a Master Contact.  The recordtype for the new
    *           Contact is based on the recordtype of the Master Contact.
    *          
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    *   DATE        AUTHOR               CHANGE
    *   ----        ------               ------
    *   1/25/2012    MJM                Updated recordtype names to reflect new Contact recordtypes
    *   10/1/2013   Gautam Shah         Added method "getRTByProfile"; Modified "createAffiliatedContact" method to utilize the newly created method
    *   10/6/2013   Gautam Shah         Commented out the pre-populate URL Hack for the Master Contact ID lookup field for DI Sandbox because it was preventing the Prod hack from working.
    *   12/16/2013  Gautam Shah         Added code to support ANZ Contact Regionalization.  Lines:  108, 119-122
    *   10/01/2014  Judith Randall      Added code to support LATAM Contact Regionalisation. Lines: 110, 125-128
    *   6/29/2015    Amogh Ghodke         Removed US - VT from line 180 to remove US - VT profile.
    *****************************************************************************************/ 
    private final Contact c;
    
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String accountID {get; set;}
    public String masterID {get; set;}
    public String firstname {get; set;}
    public String lastname {get; set;}


   
    public CreateAffiliatedContactExtension(ApexPages.StandardController stdController) {
        this.c = (Contact)stdController.getRecord();
    }
   
    
    public pagereference createAffiliatedContact() 
    {
        Contact currentC = [select id, AccountId, RecordTypeId, LastName, FirstName from Contact where id = :c.Id Limit 1];
                             
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        //rType = ApexPages.currentPage().getParameters().get('RecordType');
        cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
        ent = ApexPages.currentPage().getParameters().get('ent');
        confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
        saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
        //accountID = ApexPages.currentPage().getParameters().get('def_account_id');
        masterId = c.Id;
        firstname = currentC.FirstName;
        lastname = currentC.LastName;
        /*
        DataQuality dq = new DataQuality();       
        Map<String,Id> rtmap = dq.getRecordTypes(); 
        Id newRT;
        if (currentC.RecordTypeId == rtmap.get('Master Clinician')) 
        {              
            newRT = rtmap.get('Connected Clinician');
        } 
        else if (currentC.RecordTypeId == rtmap.get('Master Non Clinician')) 
        {
            newRT = rtmap.get('Connected Non Clinician Contact');
        } 
        */
        Id newRT = getRTByProfile('Contact');
        System.debug('newRT: ' + newRT);
        
        Pagereference returnURL = new PageReference('/003/e');
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', newRT);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');

        returnURL.getParameters().put('name_lastcon2', lastname);
        returnURL.getParameters().put('name_firstcon2', firstname);
      
        //explicity pre-populate fields based on which org
        
        //String currentOrg = UserInfo.getOrganizationName();
        /*Fields for Sandbox DI
        returnURL.getParameters().put('CF00NZ0000000T8qJ', firstname + ' ' + lastname);
        returnURL.getParameters().put('CF00NZ0000000T8qJ_lkid', masterId);
      */
        // Fields for Unified (prod)
        returnURL.getParameters().put('CF00NU0000001qYK2', firstname + ' ' + lastname);
        returnURL.getParameters().put('CF00NU0000001qYK2_lkid', masterId);
        //returnURL.getParameters().put('00NJ0000000r8c9', newRT);
        
        returnURL.setRedirect(true);
        return returnURL;
    }
  //Below method added by Gautam Shah on 10/1/2013 and is called from createAffiliatedContact()
    private Id getRTByProfile(String objectType)
    {
        List<RecordType> rtList = new List<RecordType>([Select Id, Name From RecordType Where Sobjecttype = :objectType]);
        Profile userProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
        System.debug('Current Users Profile: ' + userProfile.Name);
        //create the list of profiles on which to evaluate
        Set<String> US_Profiles_GroupA = new Set<String>{'US - All','US - AST','US - Health Systems (Read Only)','US - Med Supplies','US - RMS','US - SUS'};
        Set<String> ANZ_Profiles = new Set<String>{'ANZ - All'};
        Set<String> LATAM_Profiles = new Set<String>{'LATAM - All'};
        Map<String,Id> rtMap = new Map<String,Id>();
        for(RecordType rt : rtList)
        {
            rtMap.put(rt.Name,rt.Id);
        }
        Id rt;
        if(US_Profiles_GroupA.contains(userProfile.Name))
        {
            rt = rtMap.get('Connected Contact US');
        }
        else if(ANZ_Profiles.contains(userProfile.Name))
        {
            rt = rtMap.get('Connected Contact ANZ');
        }
        else if(LATAM_Profiles.contains(userProfile.Name))
        {
            rt = rtMap.get('Connected Contact LATAM');
        }
        else
        {
            rt = rtMap.get('Connected Clinician');
        }
        return rt;
    }
}