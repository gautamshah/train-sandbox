/****************************************************************************************
 * Name    : Submission_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 28/8/2014 
 * Purpose : Contains all the logic coming from Submission trigger
 * Dependencies: SubmissionTrigger Trigger
 *             , Submission Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 *****************************************************************************************/
public with sharing class Submission_TriggerHandler {
	private boolean m_isExecuting = false;
    private integer BatchSize = 0;

	public Submission_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
 	public void OnBeforeInsert(List<Cycle_Period__c> newObjects){
 		
 		Set<Id> accIds = new Set<Id>();
 		for(Cycle_Period__c cp: newObjects)
 		{
			accIds.add(cp.Distributor_Name__c);
 		}
 		
 		Map<Id,Account> accDetails = new Map<Id, Account>();
 		for(Account a : [select Id, Date_Format__c, Partner_ID_Used__c from Account where ID in :accIds])
 		{
 			accDetails.put(a.Id,a);
 		}
 		
 		for(Cycle_Period__c cp: newObjects)
 		{
 			if(cp.Date_Format__c == null)
 				cp.Date_Format__c = accDetails.get(cp.Distributor_Name__c).Date_Format__c;
 			if(cp.Partner_ID_Used__c == null)
 				cp.Partner_ID_Used__c = accDetails.get(cp.Distributor_Name__c).Partner_ID_Used__c;
 		}
 	}

}