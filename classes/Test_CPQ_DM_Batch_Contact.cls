/**
Test class

CPQ_DM_Batch_Contact

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-08      Yuli Fintescu       Created
11/02/2016   Paul Berglund   Commented out to save space, but keep in case we
                             do something similar again
===============================================================================
*/
@isTest
private class Test_CPQ_DM_Batch_Contact
{
/*
    static List<User> testUsers;
    static void createTestData() {
        testUsers = new List<User> {
            new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest', 
                         Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr', 
                         ProfileId = '00eU0000000dLrtIAE', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1', 
                         LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US')
        };
        insert testUsers;
        
        try {
        CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = '00eU0000000dLrtIAE', CPQ_Proposal_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Proposal_After__c = true);
        insert testTrigger;
        } catch (Exception e) {}
        
        List<CPQ_Variable__c> testVars = new List<CPQ_Variable__c> {
            new CPQ_Variable__c(Name = 'Proxy EndPoint', Value__c = 'Test Proxy EndPoint'),
            new CPQ_Variable__c(Name = 'RMS PriceList ID', Value__c = testPriceList.ID),
            new CPQ_Variable__c(Name = 'HPG Group Code', Value__c = 'SG0150')
        };
        insert testVars;
        
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
        List<Account> testAccounts = new List<Account>{
            new Account(Status__c = 'Active', Name = 'Test Pristine', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
        };
        insert testAccounts;
        
        Schema.RecordTypeInfo contRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Connected Contact US');
        List<Contact> testContacts = new List<Contact>{
            new Contact(FirstName = 'Test', LastName = 'byemail', AccountId = testAccounts[0].ID, RecordTypeID = contRT.getRecordTypeId(), Email = 'test111@testaccount.com'),
            new Contact(FirstName = 'Test', LastName = 'byname', AccountId = testAccounts[0].ID, RecordTypeID = contRT.getRecordTypeId())
        };
        insert testContacts;
        
        List<Apttus_Proposal__Proposal__c> testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 2', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 3', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 4', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 5', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 6', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 7', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 8', Apttus_Proposal__Account__c = testAccounts[0].ID),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal 9', Apttus_Proposal__Account__c = testAccounts[0].ID)
        };
        insert testProposals;
        
        List<CPQ_STG_Proposal__c> testStageProposals = new List<CPQ_STG_Proposal__c> {
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal', STGPS_CustomerContactId__c = 'PB-111', STGPS_Account_External_ID__c = 'US-111111', STGPS_Proposal__c = testProposals[0].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 2',STGPS_CustomerContactId__c = 'PB-222',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[1].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 3',STGPS_CustomerContactId__c = 'PB-3331',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[2].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 4',STGPS_CustomerContactId__c = 'PB-3332',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[3].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 5',STGPS_CustomerContactId__c = 'PB-4441',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[4].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 6',STGPS_CustomerContactId__c = 'PB-4442',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[5].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 7',STGPS_CustomerContactId__c = 'PB-555',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[6].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 8',STGPS_CustomerContactId__c = 'PB-666',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[7].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Proposal 9',STGPS_CustomerContactId__c = 'PB-777',STGPS_Account_External_ID__c = 'US-111111',STGPS_Proposal__c = testProposals[8].ID, STGPS_To_Test__c = true)
        };
        insert testStageProposals;
        
        List<CPQ_STG_Quote_Contact__c> testStageContacts = new List<CPQ_STG_Quote_Contact__c> {
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-111', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_EmailAddress__c = 'test111@testaccount.com', STGQC_GivenName__c = 'Test', STGQC_SurName__c = 'byemail'),
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-222', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_GivenName__c = 'Test', STGQC_SurName__c = 'byname'),
            
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-3331', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_EmailAddress__c = 'test333@testaccount.com', STGQC_GivenName__c = '3331', STGQC_SurName__c = 'dupbyemail'),
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-3332', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_EmailAddress__c = 'test333@testaccount.com', STGQC_GivenName__c = '3332', STGQC_SurName__c = 'dupbyemail'),

            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-4441', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_GivenName__c = 'Test', STGQC_SurName__c = 'dupbyname'),
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-4442', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_GivenName__c = 'Test', STGQC_SurName__c = 'dupbyname'),
            
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-555', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_EmailAddress__c = 'test555 @testaccount com', STGQC_SurName__c = 'invalid email'),
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-666', STGQC_Pristine__c = '111111', STGQC_Account_External_ID__c = 'US-111111', STGQC_GivenName__c = 'nolastname'),
            new CPQ_STG_Quote_Contact__c(STGQC_CustomerContactId__c = 'PB-777', STGQC_Pristine__c = '222222', STGQC_Account_External_ID__c = 'US-222222', STGQC_GivenName__c = 'Test', STGQC_SurName__c = 'noaccount')
        };
        insert testStageContacts;
    }
    
    static testMethod void myUnitTest() {
        createTestData();
        
        System.runAs(testUsers[0]) {
            Test.startTest();
                CPQ_DM_Batch_Contact p = new CPQ_DM_Batch_Contact();
                Database.executeBatch(p);
            Test.stopTest();
        }
    }
*/
}