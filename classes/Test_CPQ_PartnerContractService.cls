/*
Test class

CPQ_PartnerContractService

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/


@isTest
private class Test_CPQ_PartnerContractService {
    static List<CPQ_PartnerContractService.PartnerContractItem> testItems;
    
    static void createTestData() {
        //////insert cpqConfigSetting_cTest.create();
        //////cpqConfigSetting_c.reload();
        
        Account testAccount = new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111');
        insert testAccount;
        
        List<Apttus__APTS_Agreement__c> testAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement 1', Apttus__Account__c = testAccount.ID),
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement 2', Apttus__Account__c = testAccount.ID)
        };
        insert testAgreements;
        
        List<Apttus__APTS_Agreement__c> testAgs = [Select AgreementID__c From Apttus__APTS_Agreement__c Where ID in: testAgreements];
        
        testItems = new List<CPQ_PartnerContractService.PartnerContractItem>();
        CPQ_PartnerContractService.PartnerContractItem item = new CPQ_PartnerContractService.PartnerContractItem();
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '11111';
        item.Direction = 'I';
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '11111';
        item.Direction = 'O';
        item.AgreementID = testAgs[0].AgreementID__c;
        item.ActualEffectiveDate = System.Today().AddDays(1);
        item.ActualExpirationDate = System.Today().addYears(2).AddDays(1);
        item.PricingID = 'D11111';
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '11111';
        item.Direction = 'O';
        item.AgreementID = testAgs[0].AgreementID__c;
        item.ActualEffectiveDate = System.Today().AddDays(1);
        item.ActualExpirationDate = System.Today().addYears(2).AddDays(1);
        item.PricingID = 'R11111';
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '22222';
        item.Direction = 'O';
        item.AgreementID = testAgs[1].AgreementID__c;
        item.PricingID = 'D22222';
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '22222';
        item.Direction = 'I';
        item.AgreementID = testAgs[1].AgreementID__c;
        item.PricingID = 'R22222';
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '22222';
        item.Direction = 'I';
        item.AgreementID = testAgs[1].AgreementID__c;
        item.PricingID = 'R22222';
        testItems.add(item);
        
        item = new CPQ_PartnerContractService.PartnerContractItem();
        item.RootContractID = '33333';
        item.Direction = 'I';
        item.AgreementID = '12345';
        item.PricingID = 'R33333';
        testItems.add(item);
        
        Partner_Root_Contract__c testRoot = new Partner_Root_Contract__c(Name = '11111', Root_Contract_Name__c = '11111', Root_Contract_Number__c = '11111', Direction__c = 'I', Effective_Date__c = System.Today(), Expiration_Date__c = System.Today().addYears(1));
        insert testRoot;
    }
    
    static testMethod void myUnitTest() {
        createTestData();
        
        system.debug('cpqConfigSetting_c.SystemProperties: ' + cpqConfigSetting_c.SystemProperties);
        
        Test.startTest();
            System.Debug(CPQ_PartnerContractService.LoadPartnerContractItems(testItems));
        Test.stopTest();
    }
}