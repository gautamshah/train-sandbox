/**


CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
07/25/2016      Bryan Fry         	Created
09/14/2016		Isaac Lewis			Refactored test to Test_CPQ_AgreementProcesses.Test_SendScrubPOActivationEmail()
===============================================================================
*/


@isTest
global class Test_CPQ_SSG_AgreementDocumentTrigger {
	// @isTest static void Test_CPQ_SSG_AgreementDocumentTrigger() {
	// 	Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
	// 	insert acct;
	//
    //     Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
    //     insert c;
	//
    //     Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1', Apttus_Surgical_Product__c = true, Category__c = 'Test');
	// 	insert product;
	//
	// 	Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
	// 	insert opp;
	//
	// 	Apttus_Config2__PriceList__c priceList = new Apttus_Config2__PriceList__c (Name = 'Covidien SSG US Master', Apttus_Config2__Type__c = 'Standard', Apttus_Config2__Active__c = true);
	// 	insert priceList;
	//
	// 	RecordType scrubPOLocked = [Select Id From RecordType Where SObjectType = 'Apttus__APTS_Agreement__c' And DeveloperName = 'Scrub_PO_Locked'];
	//
    //     Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c(Apttus__Account__c=acct.Id, Apttus__Related_Opportunity__c = opp.Id, RecordTypeId = scrubPOLocked.Id, Apttus__Status_Category__c = 'In Signatures', Apttus__Status__c = 'Fully Signed', PO__c = '12345', Primary_Contact2__c = c.Id);
    //     insert agmt;
	//
    //     Apttus_Config2__PriceListItem__c priceListItem = new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = priceList.Id, Apttus_Config2__ProductId__c = product.Id);
    //     insert priceListItem;
	//
    //     Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c(Apttus_Config2__Status__c='Saved', Apttus_Config2__VersionNumber__c=1, Apttus_Config2__BusinessObjectType__c='Agreement', Apttus_Config2__BusinessObjectId__c = agmt.Id, Apttus_Config2__BusinessObjectRefId__c = agmt.Id, Apttus_CMConfig__AgreementId__c = agmt.Id, Apttus_Config2__PriceListId__c = priceList.Id, Apttus_Config2__EffectivePriceListId__c = priceList.Id);
    //     insert pc;
	//
    //     CPQ_Variable__c surgicalDebugActivationEmail = new CPQ_Variable__c(Name = CPQ_AgreementProcesses.SURGICAL_DEBUG_ACTIVATION_EMAIL, Value__c = 'testemail@test.com');
    //     insert surgicalDebugActivationEmail;
	//
    //     List<Document> documents = new List<Document>();
    //     documents.add(new Document(Name = 'Test_File_1', FolderId = UserInfo.getUserId(), Body = Blob.valueOf('123')));
    //     documents.add(new Document(Name = 'Test_File_2', FolderId = UserInfo.getUserId(), Body = Blob.valueOf('321')));
    //     insert documents;
	//
    //     Test.startTest();
	// 		Apttus__Agreement_Document__c agmtDoc1 = new Apttus__Agreement_Document__c(Apttus__Agreement__c = agmt.Id, Name = 'Test_File_1', Apttus__URL__c = '/servlet/servlet.FileDownload/Send_to_Customer_Service.png?file=' + documents[0].Id, Apttus__Type__c = 'Final Electronic Copy', Apttus__Path__c = 'http://Test_File_1');
	// 		insert agmtDoc1;
	//
	// 		Apttus__Agreement_Document__c agmtDoc2 = new Apttus__Agreement_Document__c(Apttus__Agreement__c = agmt.Id, Name = 'Test_File_2', Apttus__URL__c = '/servlet/servlet.FileDownload/Send_to_Customer_Service.png?file=' + documents[1].Id, Apttus__Type__c = 'Final Electronic Copy', Apttus__Path__c = 'http://Test_File_2');
	// 		insert agmtDoc2;
    //     Test.stopTest();
	// }
}