/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class RAPID_SVMXC_AttachmentHelper_UT {
	/**
	 * This method tests that if services for email is not active, then do not send emails
	 */
	static testMethod void testEmailHandler0() {
		List<SVMX_Services_Register__c> serviceRegisterSettings = SVMX_Services_Register__c.getall().values();
        if(ServiceRegisterSettings != null){
			for(SVMX_Services_Register__c serviceSettings : serviceRegisterSettings){
				if(serviceSettings.Name.equals('Email Service Report')){
					serviceSettings.is_Active__c = false;
				}
			}
        }
		
		update serviceRegisterSettings;
		
		Test.StartTest();
		RAPID_SVMXC_AttachmentHelper.isTestFlag = true;
	    RAPID_SVMXC_AttachmentHelper_UT.createData(true);
	    Test.StopTest();
	}

	/**
	 * This method tests that if custom setting for Default Org Id is not set,emails should not go.
	 */
	static testMethod void testEmailHandler1() {
		List<SVMX_Services_Register__c> serviceRegisterSettings = SVMX_Services_Register__c.getall().values();
        if(ServiceRegisterSettings != null){
			for(SVMX_Services_Register__c serviceSettings : serviceRegisterSettings){
				if(serviceSettings.Name.equals('Email Service Report')){
					serviceSettings.is_Active__c = true;
				}
			}
        }
		
		update serviceRegisterSettings;
		
		List<Email_Service_Report_Settings__c> emailServiceReportSettings = Email_Service_Report_Settings__c.getall().values();
        if(emailServiceReportSettings != null){
			for(Email_Service_Report_Settings__c emailSettings : emailServiceReportSettings){
				if(emailSettings.Name.equals('Org_Wide_Email_Address_Record')){
					emailSettings.Value__c = '';
				}
			}
        }
		
		update emailServiceReportSettings;
		
		Test.StartTest();
		RAPID_SVMXC_AttachmentHelper.isTestFlag = false;
	    RAPID_SVMXC_AttachmentHelper_UT.createData(true);
	    Test.StopTest();
	}
	
	/**
	 * This method tests that if custom setting for Default Contact Id is not set,emails should not go.
	 */
	static testMethod void testEmailHandler2() {
		List<SVMX_Services_Register__c> serviceRegisterSettings = SVMX_Services_Register__c.getall().values();
        if(ServiceRegisterSettings != null){
			for(SVMX_Services_Register__c serviceSettings : serviceRegisterSettings){
				if(serviceSettings.Name.equals('Email Service Report')){
					serviceSettings.is_Active__c = true;
				}
			}
        }
		
		update serviceRegisterSettings;
		
		List<Email_Service_Report_Settings__c> emailServiceReportSettings = Email_Service_Report_Settings__c.getall().values();
        if(emailServiceReportSettings != null){
			for(Email_Service_Report_Settings__c emailSettings : emailServiceReportSettings){
				if(emailSettings.Name.equals('Org_Wide_Email_Address_Record')){
					emailSettings.Value__c = '00012asd123fg';
				}else if(emailSettings.Name.equals('Default_Contact_ID')){
					emailSettings.Value__c = null;
				}
			}
        }
		
		update emailServiceReportSettings;
		
		Test.StartTest();
		RAPID_SVMXC_AttachmentHelper.isTestFlag = false;
	    RAPID_SVMXC_AttachmentHelper_UT.createData(true);
	    Test.StopTest();
	}
	
	/**
	 * This method tests that if custom setting for Send To Default Email Address is set to false,emails should not go to default email address
	 */
	static testMethod void testEmailHandler3() {
		Account acct = new Account();
        acct.Name='Apex Test Sold To Account 2';
        insert acct;
        
		Contact contact = new Contact();
        contact.FirstName = 'Testing1';
        contact.LastName = 'Testing2';
        contact.AccountId = acct.Id;
        contact.Email = 'obama@whitehouse.com';
        insert contact;
        
		List<SVMX_Services_Register__c> serviceRegisterSettings = SVMX_Services_Register__c.getall().values();
        if(ServiceRegisterSettings != null){
			for(SVMX_Services_Register__c serviceSettings : serviceRegisterSettings){
				if(serviceSettings.Name.equals('Email Service Report')){
					serviceSettings.is_Active__c = true;
				}
			}
        }
		
		update serviceRegisterSettings;
		
		List<Email_Service_Report_Settings__c> emailServiceReportSettings = Email_Service_Report_Settings__c.getall().values();
        if(emailServiceReportSettings != null){
			for(Email_Service_Report_Settings__c emailSettings : emailServiceReportSettings){
				if(emailSettings.Name.equals('Send To Default Email Address')){
					emailSettings.Value__c = 'false';
				}else if(emailSettings.Name.equals('Default_Contact_ID')){
					emailSettings.Value__c = contact.Id;
				}
			}
        }
		
		update emailServiceReportSettings;
		
		Test.StartTest();
		RAPID_SVMXC_AttachmentHelper.isTestFlag = true;
	    RAPID_SVMXC_AttachmentHelper_UT.createData(false);
	    Test.StopTest();
	}
	
	/**
	 * This method tests that emails should Default email address and Custom Filed
	 */
	static testMethod void testEmailHandler4() {
		Account acct = new Account();
        acct.Name='Apex Test Sold To Account 2';
        insert acct;
        
		Contact contact = new Contact();
        contact.FirstName = 'Testing1';
        contact.LastName = 'Testing2';
        contact.AccountId = acct.Id;
        contact.Email = 'obama@whitehouse.com';
        insert contact;
        
        List<SVMX_Services_Register__c> serviceRegisterSettings = SVMX_Services_Register__c.getall().values();
        if(ServiceRegisterSettings != null){
			for(SVMX_Services_Register__c serviceSettings : serviceRegisterSettings){
				if(serviceSettings.Name.equals('Email Service Report')){
					serviceSettings.is_Active__c = true;
				}
			}
        }
		
		update serviceRegisterSettings;
		
		List<Email_Service_Report_Settings__c> emailServiceReportSettings = Email_Service_Report_Settings__c.getall().values();
        if(emailServiceReportSettings != null){
			for(Email_Service_Report_Settings__c emailSettings : emailServiceReportSettings){
			    if(emailSettings.Name.equals('Default_Email_Address')){
				    emailSettings.Value__c = 'testemailAddress@rapidorg.com';
				}else if(emailSettings.Name.equals('Send To Default Email Address')){
					emailSettings.Value__c = 'True';
				}else if(emailSettings.Name.equals('Default_Contact_ID')){
					emailSettings.Value__c = contact.Id;
				}
			}
        }
		
		update emailServiceReportSettings;
		
		Test.StartTest();
		RAPID_SVMXC_AttachmentHelper.isTestFlag = true;
	    RAPID_SVMXC_AttachmentHelper_UT.createData(true);
	    Test.StopTest();
	}
	
	/**
	 * This method tests that emails should default email address with custom field 
	 * And this does not have email template
	 */
	static testmethod void testEmailHandler5() {
		Account acct = new Account();
        acct.Name='Apex Test Sold To Account 2';
        insert acct;
        
		Contact contact = new Contact();
        contact.FirstName = 'Testing1';
        contact.LastName = 'Testing2';
        contact.AccountId = acct.Id;
        contact.Email = 'obama@whitehouse.com';
        insert contact;
        
        List<SVMX_Services_Register__c> serviceRegisterSettings = SVMX_Services_Register__c.getall().values();
        if(ServiceRegisterSettings != null){
			for(SVMX_Services_Register__c serviceSettings : serviceRegisterSettings){
				if(serviceSettings.Name.equals('Email Service Report')){
					serviceSettings.is_Active__c = true;
				}
			}
        }
		
		update serviceRegisterSettings;
		
		List<Email_Service_Report_Settings__c> emailServiceReportSettings = Email_Service_Report_Settings__c.getall().values();
        if(emailServiceReportSettings != null){
			for(Email_Service_Report_Settings__c emailSettings : emailServiceReportSettings){
			    if(emailSettings.Name.equals('Default_Email_Address')){
				    emailSettings.Value__c = 'testemailAddress@rapidorg.com';
				}else if(emailSettings.Name.equals('Send To Default Email Address')){
					emailSettings.Value__c = 'True';
				}else if(emailSettings.Name.equals('Default_Contact_ID')){
					emailSettings.Value__c = contact.Id;
				}else if(emailSettings.Name.equals('Email Template Name')){
						emailSettings.Value__c = '';
				}
			}
        }
		
		update emailServiceReportSettings;
		
		Test.StartTest();
		RAPID_SVMXC_AttachmentHelper.isTestFlag = true;
	    RAPID_SVMXC_AttachmentHelper_UT.createData(true);
	    Test.StopTest();
	}
    
   static void createData(boolean sendServiceReportToCustomer) {          
        Account acct = new Account();
        acct.Name='Apex Test Sold To Account 1';
        insert acct;
        
        Contact contact = new Contact();
        contact.FirstName = 'Dave';
        contact.LastName = 'Testing';
        contact.AccountId = acct.Id;
        contact.Email = 'obama@whitehouse.com';
        insert contact;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'test';
        site.SVMXC__Account__c = acct.Id;
        insert site;
        
        RecordType rt1 = [Select Id from RecordType where sObjectType='SVMXC__Service_Group__c' and DeveloperName='Technician' limit 1];
        
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c();
        team.Name = 'West';
        team.SVMXC__Active__c=true;
        team.SVMXC__Group_Code__c = '111';
        team.SVMXC__Group_Type__c ='Both';
        team.RecordTypeId = rt1.Id;
        insert team;
        
        User usr = new User();
        usr.FirstName = 'Test';
        usr.LastName = 'Tester';
        usr.Email='test@rapidorgtester.com';
        usr.Alias = 'svmxtst3';
        usr.username = 'tester2@rapidorgtester.com';
        usr.CommunityNickname = 'svmxtst3';
        usr.TimeZoneSidKey = 'America/Los_Angeles';
        usr.LocaleSidKey = 'en_US';
        usr.ProfileId = UserInfo.getProfileId(); 
        usr.EmailEncodingKey = 'UTF-8';
        usr.LanguageLocaleKey = 'en_US';
        usr.EmployeeNumber = '123456';
        insert usr;
       
        Case cs = new Case();
        cs.Status ='New';
        cs.Priority = 'Medium';
        cs.Origin = 'Email';                                       
        cs.ContactId =contact.Id;
        cs.AccountId=acct.Id;
        insert cs;      
                        
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
        wo.SVMXC__Company__c = acct.Id;
        wo.SVMXC__Order_Status__c = 'New';
        wo.SVMXC__Order_Type__c='Field Service';
        wo.SVMXC__Street__c = '16260 Monterey St.';
        wo.SVMXC__City__c = 'Morgan Hill';
        wo.SVMXC__State__c = 'California';
        wo.SVMXC__Zip__c = '95037';
        wo.SVMXC__Country__c = 'USA';
        wo.SVMXC__Site__c = site.Id;
        wo.SVMXC__Priority__c = 'Medium';
        wo.SVMXC__Case__c = cs.Id;
        wo.Email_Service_Report__c = 'test@rapidOrgTester.com';
        wo.Send_Service_Report_to_Customer__c = sendServiceReportToCustomer;
        wo.Send_Service_Report_to_Current_User__c = sendServiceReportToCustomer;
        insert wo;
 
        Attachment attach=new Attachment();     
        attach.Name='Create_Service_Report_' + wo.Id + '.pdf';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=wo.id;
        insert attach;
    } 
}