/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FetchOpportunityRequest {
    global FetchOpportunityRequest(String requestXML) {

    }
    global String getLangCode() {
        return null;
    }
    global String getLongCode() {
        return null;
    }
    global Integer getNumberToReturn() {
        return null;
    }
    global String getQuery() {
        return null;
    }
    global String getSearchBy() {
        return null;
    }
    global String getShortCode() {
        return null;
    }
    global String toXml() {
        return null;
    }
}
