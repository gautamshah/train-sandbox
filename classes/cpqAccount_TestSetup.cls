@isTest
public class cpqAccount_TestSetup
{

    public static List<Account> generateAccounts (Integer quantity)
    {
        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < quantity; i++)
        {
            accs.add(new Account(
                Name = 'Test Account ' + i,
                BillingState = 'CO',
                BillingPostalCode = '12345',
                BillingCountry = 'US',
                Account_External_ID__c = Datetime.now().format('yyMMddhhmmssSSS') + '-' + i
            ));
        }
        return accs; 
    }

    
}