@isTest
private class UserSecurityPublicGroupUpdateTestInsert {

    static testMethod void myUnitTest() {
        Profile profileChatterFree = [SELECT Id from Profile where Name = 'Chatter Free User' LIMIT 1];
        Profile profileEURMS = [SELECT Id from Profile where Name = 'EU - One Covidien' LIMIT 1];
        
        List<User> userInsertList = new List<User>();
        
        Group g=new Group(name='FR_MCS');
        insert g;
        
        for(Integer i=0;i<10;i++){          
            User userToInsertChatter = new User();
            userToInsertChatter.FirstName = 'Chatty' + i;
            userToInsertChatter.LastName = 'Cathy' + i;
            userToInsertChatter.Alias = 'chattyc' + i;          
            userToInsertChatter.Email =  i + 'chatty.cathy@covidien.security';
            userToInsertChatter.Username = i + 'chatty.cathy@covidien.security.com';
            userToInsertChatter.ProfileId = profileChatterFree.Id;
            userToInsertChatter.TimeZoneSidKey = 'America/New_York';
            userToInsertChatter.LanguageLocaleKey= 'en_US';
            userToInsertChatter.LocaleSidKey = 'en_US';
            userToInsertChatter.EmailEncodingKey = 'ISO-8859-1';
            userToInsertChatter.User_Visibility__c = 'Region';
            
            userInsertList.add(userToInsertChatter);
        }   
        
        for(Integer r=0;r<10;r++){
            User userToInsertRegion = new User();
            userToInsertRegion.FirstName = 'Region' + r;
            userToInsertRegion.LastName = 'User' + r;
            userToInsertRegion.Alias = 'stanu' + r;         
            userToInsertRegion.Email =  r + 'region.user@covidien.security';
            userToInsertRegion.Username = r + 'region.user@covidien.security.com';
            userToInsertRegion.ProfileId = profileEURMS.Id;
            userToInsertRegion.TimeZoneSidKey = 'America/New_York';
            userToInsertRegion.LanguageLocaleKey= 'en_US';
            userToInsertRegion.LocaleSidKey = 'en_US';
            userToInsertRegion.EmailEncodingKey = 'ISO-8859-1';
            userToInsertRegion.User_Visibility__c = 'Region';
            userToInsertRegion.Country = 'FR';
            userToInsertRegion.Sales_Org_PL__c= 'MCS';
            userToInsertRegion.User_Role__c='Field Manager';
            
            userInsertList.add(userToInsertRegion);
        }
        
            for(Integer c=0;c<10;c++){          
            User userToInsertCountry = new User();
            userToInsertCountry.FirstName = 'Country' + c;
            userToInsertCountry.LastName = 'User' + c;
            userToInsertCountry.Alias = 'ctyu' + c;         
            userToInsertCountry.Email =  c + 'country.user@covidien.security';
            userToInsertCountry.Username = c + 'country.user@covidien.security.com';
            userToInsertCountry.ProfileId = profileEURMS.Id;
            userToInsertCountry.TimeZoneSidKey = 'America/New_York';
            userToInsertCountry.LanguageLocaleKey= 'en_US';
            userToInsertCountry.LocaleSidKey = 'en_US';
            userToInsertCountry.EmailEncodingKey = 'ISO-8859-1';
            userToInsertCountry.User_Visibility__c = 'Country';
            userToInsertCountry.Sales_Org_PL__c= 'MCS';
            userToInsertCountry.User_Role__c='Field Manager';
            userToInsertCountry.Country = 'FR';
            userToInsertCountry.Region__c = 'ANZ';           
           
            
            userInsertList.add(userToInsertCountry);
        }
        
            for(Integer p=0;p<10;p++){          
                User userToInsertConstrained = new User();
                userToInsertConstrained.FirstName = 'Private' + p;
                userToInsertConstrained.LastName = 'User' + p;
                userToInsertConstrained.Alias = 'privu' + p;            
                userToInsertConstrained.Email =  p + 'private.user@covidien.security';
                userToInsertConstrained.Username = p + 'private.user@covidien.security.com';
                userToInsertConstrained.ProfileId = profileEURMS.Id;
                userToInsertConstrained.TimeZoneSidKey = 'America/New_York';
                userToInsertConstrained.LanguageLocaleKey= 'en_US';
                userToInsertConstrained.LocaleSidKey = 'en_US';
                userToInsertConstrained.EmailEncodingKey = 'ISO-8859-1';
                userToInsertConstrained.User_Visibility__c = 'Private';
                userToInsertConstrained.Sales_Org_PL__c= 'MCS';
                userToInsertConstrained.User_Role__c='Field Manager';
            
                userInsertList.add(userToInsertConstrained);
            }
        
        for(Integer x=0;x<10;x++){          
            User userToInsertConstrained = new User();
            userToInsertConstrained.FirstName = 'Constrained' + x;
            userToInsertConstrained.LastName = 'User' + x;
            userToInsertConstrained.Alias = 'conu' + x;         
            userToInsertConstrained.Email =  x + 'constrained.user@covidien.security';
            userToInsertConstrained.Username = x + 'constrained.user@covidien.security.com';
            userToInsertConstrained.ProfileId = profileEURMS.Id;
            userToInsertConstrained.TimeZoneSidKey = 'America/New_York';
            userToInsertConstrained.LanguageLocaleKey= 'en_US';
            userToInsertConstrained.LocaleSidKey = 'en_US';
            userToInsertConstrained.EmailEncodingKey = 'ISO-8859-1';
            userToInsertConstrained.User_Visibility__c = 'Constrained';
            userToInsertConstrained.Country = 'FR';
            userToInsertConstrained.Sales_Org_PL__c= 'MCS';
            userToInsertConstrained.User_Role__c='Field Manager';
            
            userInsertList.add(userToInsertConstrained);
        }
        
          Test.startTest();   
        insert userInsertList;
          Test.stopTest();
        System.assertEquals([select Franchise__c from user where name='Constrained0User0' ],[select Sales_Org_PL__c from user where name='Constrained0User0' ]);
         
    }
}