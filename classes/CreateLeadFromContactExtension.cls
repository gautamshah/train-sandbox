public with sharing class CreateLeadFromContactExtension {
    /****************************************************************************************
    * Name    : CreateContactFromAccountExtension
    * Author  : Mike Melcher
    * Date    : 3-20-2012
    * Purpose : Create a referral lead from a Connected Contact record.
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
   *
    *****************************************************************************************/ 
    private final Contact c;
    
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String accountID {get; set;}
    public String masterID {get; set;}
    public String firstname {get; set;}
    public String lastname {get; set;}
    public String Phone {get; set;}
    public String Email {get; set;}
    public String BillingStreet {get; set;}



   
    public CreateLeadFromContactExtension(ApexPages.StandardController stdController) {
        this.c = (Contact)stdController.getRecord();
    }
   
    
    public pagereference createLead() {
         Contact currentc = [select id, AccountId, Name, LastName, FirstName, RecordTypeId, Salutation, Email, 
                                    Phone, MobilePhone, MailingStreet, Master_Contact_Record__c,
                                    mailingCity, MailingState, MailingPostalCode, MailingCountry
                             from Contact
                             where id = :c.Id];
         Account currentA = [select Id, Name, BillingStreet, BillingCity, BillingState,
                                    BillingPostalCode, BillingCountry
                             from Account where Id = :currentC.AccountId];      
         
         Contact masterC;
         
         if (currentC.Master_Contact_Record__c != null) {
            masterC = [select Id, Name, MobilePhone from Contact where Id = :currentC.Master_Contact_Record__c];
         }
                       
                             
      // retURL = ApexPages.currentPage().getParameters().get('retURL');
       rType = ApexPages.currentPage().getParameters().get('RecordType');
      // cancelURL = ApexPages.currentPage().getParameters().get('\001\' + currentA.Id');
       ent = ApexPages.currentPage().getParameters().get('ent');
       confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
       saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
      
       DataQuality dq = new DataQuality();       
       Map<String,Id> rtmap = dq.getRecordTypes(); 
       Id newRT = rtmap.get('Master Non Clinician');
       
       Id currentUser = UserInfo.getUserId();
       User u = [select Id, business_unit__c from User where Id = :currentUser];
       
       string bu;
       if (u.business_unit__c != null) {
          bu = u.business_unit__c;
       }
       
       Pagereference returnURL = new PageReference('/00Q/e');
       returnURL.getParameters().put('retURL', retURL);
       //returnURL.getParameters().put('RecordType', newRT);
       returnURL.getParameters().put('cancelURL', cancelURL);
       returnURL.getParameters().put('ent', ent);
       returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
       returnURL.getParameters().put('save_new_url', saveNewURL);
       returnURL.getParameters().put('nooverride', '1');

                 
       returnURL.getParameters().put('name_salutationlea2', currentc.Salutation);
       returnURL.getParameters().put('name_lastlea2', currentC.lastname);
       returnURL.getParameters().put('name_firstlea2', currentC.firstname);
       returnURL.getParameters().put('lea16street', currentA.BillingStreet);
       returnURL.getParameters().put('lea16city', currentA.BillingCity);
       returnURL.getParameters().put('lea16state', currentA.BillingState);
       returnURL.getParameters().put('lea16zip', currentA.BillingPostalCode);
       returnURL.getParameters().put('lea13', 'Open');
       returnURL.getParameters().put('00NU0000001prdY', 'Referral');
       returnURL.getParameters().put('lea16country', currentA.BillingCountry);
       returnURL.getParameters().put('CF00NU0000001prTt_lkid', currentC.AccountId);
       returnURL.getParameters().put('CF00NU0000001prTt', currentA.Name);
       if (masterC != null) {
       	  // Coedev
          returnURL.getParameters().put('CF00NK0000000V9xV_lkid', masterC.Id);
          returnURL.getParameters().put('CF00NK0000000V9xV', masterC.Name);
          
          // Rlsstaging
          returnURL.getParameters().put('CF00NK0000000PrG7_lkid', masterC.Id);
          returnURL.getParameters().put('CF00NK0000000PrG7', masterC.Name);
          
       }
       returnURL.getParameters().put('00NU0000001prdi', bu);
       returnURL.getParameters().put('lea11', currentC.Email);
       returnURL.getParameters().put('00NU0000001prdW','Covidien Referral');
       returnURL.getParameters().put('lea17','Created from ' + currentC.Name + ' at ' + currentA.Name);
       returnURL.setRedirect(true);
       return returnURL;
  }
 

}