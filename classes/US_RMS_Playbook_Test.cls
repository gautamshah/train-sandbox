@isTest(SeeAllData=true)
private class US_RMS_Playbook_Test {

    static testMethod void Test_isPM() {
     
        User user = [SELECT ID,IsActive FROM USER WHERE Franchise__c = 'Patient Monitoring' and  IsActive=true LIMIT 1];
        
        system.runAs(user) {
            US_RMS_Playbook playbook = new US_RMS_Playbook();
            system.assert(playbook.getisPM(), 'Expecting the user to be in Patient Monitoring');
            
            system.assert(!playbook.getisRS(), 'Expecting the user to not be Respiratory Solutions');
        }
    }

    static testMethod void Test_isRS() {
     
        User user =  [SELECT ID,IsActive FROM USER WHERE Franchise__c = 'Respiratory Solutions' and IsActive=true LIMIT 1];
        
        system.runAs(user) {
            US_RMS_Playbook playbook = new US_RMS_Playbook();
            system.assert(playbook.getisRS(), 'Expecting the user to be in Respiratory Solutions');
            
            system.assert(!playbook.getisPM(), 'Expecting the user to not be Patient Monitoring');
        }
    }
    
    static testMethod void Test_getDocument() {
        Document doc = US_RMS_Playbook.getDocument('US RMS Playbook Call Point Grid');
        
        system.assert((doc != null), 'Document should exist');
        
        doc = US_RMS_Playbook.getDocument('Does Not Exist');
        
        system.assert((doc.Id == null), 'Document should not exist');
    }
    
    static testMethod void Test_getDocumentWrapper() {
        DocumentWrapper wrapper = US_RMS_Playbook.getDocumentWrapper('US RMS Playbook RS CSE Comp Plan');
        
        system.assert((wrapper.RootId != null), 'Root document should exist');
        system.assert((wrapper.ZipId != null), 'Zip document should exist');
        
        wrapper = US_RMS_Playbook.getDocumentWrapper('Does Not Exist');
        
        system.assert((wrapper.RootId == null), 'Root document should not exist');
        system.assert((wrapper.ZipId == null), 'Zip document should not exist');
    }
    
    static testMethod void Test_getFolder() {
        Folder folder = US_RMS_Playbook.getFolder('US RMS Playbook Presidents Club Criteria');
        
        system.assert((folder.Id != null), 'Folder should exist');
        
        folder = US_RMS_Playbook.getFolder('Does Not Exist');
        
        system.assert((folder.Id == null), 'Folder should not exist');
    }
}