public class ERPAccount_c extends sObject_c
{

	//
	// ERPAccount_c or ERPAccount_Acount_c
	//
	@testVisible
	private static FINAL Set<string> VALID_ERP_ACCOUNT_TYPES = new Set<string>
	{
		'S', // Wish I knew what these mean
		'X',
		'TS',
		'TX'
	};

    public static void filterAccountsByERP(
    	List<Account> inputs,
    	Set<ID> outputHasNoERPs,
    	Set<ID> outputHasOnlyTracings)
    {
        // for given input account Ids, gets accountIds that have only Tracing ERPs or no valid ERPs
        Set<ID> SXs = new Set<ID>();
        Set<ID> hasERPs = new Set<ID>();
        for (ERP_Account__c erp : [SELECT Id,
                                          Name,
                                          ERP_Account_Type__c,
                                          Parent_Sell_To_Account__c
                                   FROM ERP_Account__c
                                   WHERE Parent_Sell_To_Account__c IN :inputs AND
                                         ERP_Account_Type__c IN :VALID_ERP_ACCOUNT_TYPES AND
                                         ERP_Source__c = 'E1' AND
                                         isActive__c = true
                                   ORDER BY Parent_Sell_To_Account__c])
        {
            hasERPs.add(erp.Parent_Sell_To_Account__c);

            if (erp.ERP_Account_Type__c == 'S' || erp.ERP_Account_Type__c == 'X')
                SXs.add(erp.Parent_Sell_To_Account__c);
        }

        for (Account a : inputs) {
            if (!hasERPs.contains(a.Id))
                outputHasNoERPs.add(a.Id);
            else if (!SXs.contains(a.Id))
                outputHasOnlyTracings.add(a.Id);
        }
    }
}