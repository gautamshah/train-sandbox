/****************************************************************************************
 Name    : Attachment_test - test all things Attachment
 Author  : Paul Berglund
 Date    : 08/17/2016
 
 ========================
 = MODIFICATION HISTORY =
 ========================
 DATE        AUTHOR           CHANGE
 ----        ------           ------
 08/17/2016  Paul Berglund    Initial deployment to production in support of Document
                              Management application
 *****************************************************************************************/
@isTest
public class Attachment_test
{
	// To test the Attachment code used by the Document Managment (dm) application
	// you must create an Application -> Folder -> Document -> add Attachment.
	// Because of that, the coverage code is in the dm_test class instead.
}