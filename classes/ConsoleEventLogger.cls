global class ConsoleEventLogger 
{
	@future
	public static void log(String CaseId, String eventType, DateTime timestamp, String toolbarButtonId, String currentToolURL)
	{
		Console_Event_Log__c cel = new Console_Event_Log__c();
		cel.Case__c = CaseId;
		cel.Event__c = eventType;
		cel.Timestamp__c = timestamp;
		cel.Toolbar_Button_Invoked__c = toolbarButtonId;
		cel.Tool_URL__c = currentToolURL;
		try
		{
			insert cel;
		}
		catch(DMLException e)
		{
			
		}
	}
}