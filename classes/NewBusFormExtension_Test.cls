@isTest
private class NewBusFormExtension_Test{ 
     static testMethod void testConstructor() {
         Account acc = new Account(Name = 'TestAcc');
         insert acc;
         Opportunity opp = new Opportunity(Name = 'TestOpp', AccountId = acc.id, Capital_Disposable__c = 'Capital', StageName = 'Identify', /*Revenue_Start_Date__c = System.Today(),*/ CloseDate = System.Today()+ 30);
         insert opp;
         New_Business_Form__c nbfRec = new New_Business_Form__c(Opportunity__c = opp.id, Item_Number__c = '10', Monthly_Case_Quantity__c = 10);
         insert nbfRec;
         ApexPages.StandardController controller = new ApexPages.StandardController (nbfRec);
         NewBusFormExtension classObj = new NewBusFormExtension(controller);
     }
     
     static testMethod void testSave() {
         Account acc = new Account(Name = 'TestAcc');
         insert acc;
         Opportunity opp = new Opportunity(Name = 'TestOpp', AccountId = acc.id, Capital_Disposable__c = 'Capital', StageName = 'Identify', /*Revenue_Start_Date__c = System.Today(),*/ CloseDate = System.Today()+ 30);
         insert opp;
         New_Business_Form__c nbfRec = new New_Business_Form__c(Opportunity__c = opp.id, Item_Number__c = '10', Monthly_Case_Quantity__c = 10);
         insert nbfRec;
         ApexPages.StandardController controller = new ApexPages.StandardController (nbfRec);
         NewBusFormExtension classObj = new NewBusFormExtension(controller);
         
         classObj.addRow();
         classObj.removeLastEmptyRecord();
         
         classObj.newBusRecs[0].Opportunity__c = opp.id;
         classObj.newBusRecs[0].Item_Number__c = '2';
         classObj.newBusRecs[0].Monthly_Case_Quantity__c = 5;
         
         Test.StartTest();
             classObj.saveRecords();
             classObj.cancel();
         Test.StopTest();
         
         New_Business_Form__c nbfInsertedRec = [SELECT Id FROM New_Business_Form__c WHERE Id =: nbfRec.id];
         System.assertEquals(nbfInsertedRec.Id, nbfRec.Id);
         
         List<Apexpages.Message> pageMessages = ApexPages.getMessages();
         PageReference pageRef = Page.New_Business_Form;
         Test.setCurrentPage(pageRef);     
         
     }
    
    
     static testMethod void testSave2() {
         Account acc = new Account(Name = 'TestAcc');
         insert acc;
         Opportunity opp = new Opportunity(Name = 'TestOpp', AccountId = acc.id, Reprocessing_Opportunity__c = true, Capital_Disposable__c = 'Capital', StageName = 'Identify', /*Revenue_Start_Date__c = System.Today(),*/ CloseDate = System.Today()+ 30);
         insert opp;
         New_Business_Form__c nbfRec = new New_Business_Form__c(Opportunity__c = opp.id, Reprocessing__c = true, Item_Number__c = '10', Monthly_Case_Quantity__c = 10);
         insert nbfRec;
         ApexPages.StandardController controller = new ApexPages.StandardController (nbfRec);
         NewBusFormExtension classObj = new NewBusFormExtension(controller);
         
         classObj.addRow();
         classObj.removeLastEmptyRecord();
         
         classObj.newBusRecs[0].Opportunity__c = opp.id;
         classObj.newBusRecs[0].Item_Number__c = '2';
         classObj.newBusRecs[0].Monthly_Case_Quantity__c = 5;
         
         Test.StartTest();
             classObj.saveRecords();
             classObj.cancel();
         Test.StopTest();
         
         New_Business_Form__c nbfInsertedRec = [SELECT Id FROM New_Business_Form__c WHERE Id =: nbfRec.id];
         System.assertEquals(nbfInsertedRec.Id, nbfRec.Id);
         
         List<Apexpages.Message> pageMessages = ApexPages.getMessages();
         PageReference pageRef = Page.New_Business_Form;
         Test.setCurrentPage(pageRef);     
         
     }    
    
    
}