@isTest
private class EmailRelatedListControllerTest
{

	@isTest
	static void itShouldGetChildRecords () {

		Account acc = new Account(Name = 'Test Account', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US', Account_External_ID__c = 'TEST_EXTERNAL');
		insert acc;

		List<Opportunity> opps = new List<Opportunity>();
		opps.add(new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acc.Id));
		opps.add(new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acc.Id));
		insert opps;

		Test.startTest();

		EmailRelatedListController ctrl = new EmailRelatedListController();
		ctrl.pId = acc.Id;
		ctrl.cName = 'Opportunity';
		ctrl.cFields = 'Id,Name,AccountId';
		ctrl.rName = 'Opportunities';

		List<Opportunity> children = (List<Opportunity>) ctrl.getChildren();
		System.assert(children != null);
		System.assert(children.size() == 2);
		System.assertEquals(true,ctrl.getHasChildren());

		Test.stopTest();

	}

	@isTest
	static void itShouldGetEmptyChildRecords () {

		Account acc = new Account(Name = 'Test Account', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US', Account_External_ID__c = 'TEST_EXTERNAL');
		insert acc;

		// No Opportunities

		Test.startTest();

		EmailRelatedListController ctrl = new EmailRelatedListController();
		ctrl.pId = acc.Id;
		ctrl.cName = 'Opportunity';
		ctrl.cFields = 'Id,Name,AccountId';
		ctrl.rName = 'Opportunities';

		List<Opportunity> children = (List<Opportunity>) ctrl.getChildren();
		System.assert(children != null);
		System.assert(children.size() == 0);
		System.assertEquals(false,ctrl.getHasChildren());

		Test.stopTest();

	}

	@isTest
    static void itShouldReturnEmptyOnNoParentId () {

        Test.startTest();

		EmailRelatedListController ctrl = new EmailRelatedListController();

		List<sObject> children = (List<sObject>) ctrl.getChildren();
		System.assert(children != null);
		System.assert(children.size() == 0);
		System.assertEquals(false,ctrl.getHasChildren());

		Test.stopTest();

    }


}