public with sharing class CPQ_Component_Agreement
{

    public CPQ_Component_Agreement() {
    }

    public Id AgreementId {get; set;}
    public boolean showButtons {get; set;}
    public boolean quoteText {get; set;}

    public class COOPPriceFile {
        public string Id {get; set;}
        public string AgreementId {get; set;}
        public string AgreementName {get; set;}
        public List<COOPPriceFileBlock1> Block1Records {get; set;}
        public List<COOPPriceFileBlock2> Block2Records {get; set;}
    }

    public class COOPPriceFileBlock1 {
        public Id ProductId {get; set;}
        public string ProductCode {get; set;}
        public string ProductDescription {get; set;}
        public decimal TemplateNetPrice {get; set;}
    }

    public class COOPPriceFileBlock2 {
        public Id ERPRecordId {get; set;}
        public string ERPRecordName {get; set;}
        public string Address1 {get; set;}
        public string City {get; set;}
        public string State {get; set;}
        public string Zip {get; set;}
        public string ShipToNumber {get; set;}
        public string ReportingNumber {get; set;}
    }

    public COOPPriceFile getCOOPPriceFile() {
        User user = cpqUser_c.CurrentUser;
        COOPPriceFile target = new COOPPriceFile();
        if(!CPQ_Utilities.getAllowedToSeeCOOPPricingFile(user.Profile.Name)) return target;

        Apttus__APTS_Agreement__c agreement = [SELECT Id,
                                                      AgreementID__c,
                                                      Name
                                               FROM Apttus__APTS_Agreement__c
                                               WHERE Id = :AgreementId];

        List<Apttus__AgreementLineItem__c> lineItems = [SELECT Product_Code__c,
                                                               Product_Description__c,
                                                               Template_Net_Price__c
                                                        FROM Apttus__AgreementLineItem__c
                                                        WHERE Apttus__AgreementId__c = :agreementId AND
                                                              Apttus_CMConfig__ChargeType__c = 'Consumable' AND
                                                              Is_Bundle_Header__c = false
                                                        ORDER BY Product_Code__c];

        Set<string> productExternalIds = new Set<string>();
        for(Apttus__AgreementLineItem__c lineItem : lineItems)
            productExternalIds.add('US:RMS:' + lineItem.Product_Code__c);

        List<Product2> products = [SELECT Id,
                                          Product_External_Id__c
                                   FROM Product2
                                   WHERE Product_External_Id__c IN :productExternalIds];

        Map<string, Id> externalId2Id = new Map<string, Id>();
        for(Product2 product : products)
            externalId2Id.put(product.Product_External_Id__c, product.Id);

        target.Id = agreement.Id;
        target.AgreementId = agreement.AgreementID__c;
        target.AgreementName = agreement.Name;
        target.Block1Records = new List<COOPPriceFileBlock1>();
        for(Apttus__AgreementLineItem__c lineItem : lineItems) {
            COOPPriceFileBlock1 product = new COOPPriceFileBlock1();
            product.ProductId = externalId2Id.get('US:RMS:' + lineItem.Product_Code__c);
            product.ProductCode = lineItem.Product_Code__c;
            product.ProductDescription = lineItem.Product_Description__c;
            product.TemplateNetPrice = lineItem.Template_Net_Price__c;
            target.Block1Records.add(product);
        }

        List<Agreement_Participating_Facility_Address__c> erpRecords = [SELECT Ship_to_Address_1__c,
                                                                               City_Template__c,
                                                                               State_Region_Template__c,
                                                                               Zip_Postal_Code_Template__c,
                                                                               Shipto_Number_Template__c,
                                                                               Reporting_Number_Template__c,
                                                                               ERP_Record__r.Id,
                                                                               ERP_Record__r.Name
                                                                        FROM Agreement_Participating_Facility_Address__c
                                                                        WHERE Agreement__c = :AgreementId
                                                                        ORDER BY ERP_Record__r.Name];

        target.Block2Records = new List<COOPPriceFileBlock2>();
        for(Agreement_Participating_Facility_Address__c erp : erpRecords) {
            COOPPriceFileBlock2 address = new COOPPriceFileBlock2();
            address.ERPRecordId = erp.ERP_Record__r.Id;
            address.ERPRecordName = erp.ERP_Record__r.Name;
            address.Address1 = erp.Ship_to_Address_1__c;
            address.City = erp.City_Template__c;
            address.State = erp.State_Region_Template__c;
            address.Zip = erp.Zip_Postal_Code_Template__c;
            address.ShipToNumber = erp.Shipto_Number_Template__c;
            address.ReportingNumber = erp.Reporting_Number_Template__c;
            target.Block2Records.add(address);
        }

        return target;
    }
}