//
// Description: This test class creates the necessary code coverage for the xTriggerDispatcher
//              class and the trigger definition associated with the trigger:  Proposal
//        Note: Do not put test methods for the classes you call, just cause the trigger to fire!
//              If you want to
//
//     Created: 02/02/2016
//      Author: Paul Berglund
//
//     History: 02/02/2016 Paul Berglund      Initial version
//              02/15/2016 Paul Berglund      Changed Setup() from @testSetup to regular
//                                            method and added call to each @isTest to
//                                            fix a problem with SF calling it multiple
//                                            times for each test method
//
/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest(seeAllData=false)
private class Proposal_Test {

    // Called by every test method
    static void Setup() {
 
        ////CPQ_Config_Setting__c setting = new CPQ_Config_Setting__c();
        ////setting.Name = 'System Properties';
        ////insert setting;

        // Required fields
        Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class);
        RecordType rt = rts.values().get(0);
        
        Apttus_Proposal__Proposal__c target = new Apttus_Proposal__Proposal__c();
        target.RecordTypeId = rt.Id;
        target.Apttus_Proposal__Proposal_Name__c = 'Insert Test';
        insert target;
    }

    @isTest
    static void testInsert() {
        Setup();

        // Insert
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c];
        system.assertNotEquals(target.Id, null, 'A record should have been created');
    }

    @isTest
    static void testUpsert() {
        Setup();

        // Upsert
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c];
        target.Apttus_Proposal__Proposal_Name__c = 'Upsert Test';
        upsert target;
        target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
        system.assertEquals(target.Apttus_Proposal__Proposal_Name__c, 'Upsert Test', 'Existing record should be updated');
    }

    @isTest
    static void testUpdate() {
        Setup();

        // Update
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c];
        target.Apttus_Proposal__Proposal_Name__c = 'Update Test';
        update target;
        target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
        system.assertEquals(target.Apttus_Proposal__Proposal_Name__c, 'Update Test', 'Existing record should be updated');
    }

    @isTest
    static void testDelete() {
        Setup();

        // Delete
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c];
        delete target;
        try {
            target = [SELECT Id FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
            system.assert(false, 'Record should have been deleted');
        }
        catch (Exception ex) {
        }
    }

    @isTest
    static void CPQ_Trigger_Profile() {
        Setup();

        // CPQ Trigger Profile
        CPQ_Trigger_Profile__c p = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, Proposal__c = true);
        upsert p;

        // Upsert
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c];
        target.Apttus_Proposal__Proposal_Name__c = 'Upsert Test';
        upsert target;
        target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
        system.assertEquals(target.Apttus_Proposal__Proposal_Name__c, 'Upsert Test', 'Existing record should be updated');
    }
}