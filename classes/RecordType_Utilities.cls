/*
	Class is designed to cache and return a record type map for all record types
	Slalom Consulting 3.31.14
	Christian Linenko
*/

public with sharing class RecordType_Utilities
{
	private static Map<String,Map<String,RecordType>> recordTypesMapByDeveloperNameByObject;
	private static List<RecordType> allRecordTypes;
	
	private static void LoadAllRecordTypesList() {
    	
    	
      if(allRecordTypes == null) {
      	allRecordTypes = [SELECT Id
                                ,DeveloperName
                                ,Name
                                ,SobjectType
                            FROM RecordType
                            ORDER BY SobjectType];     
      }
    }

	private static Map <String , Map <String , RecordType >>GetRecordTypesMapByDeveloperNameByObject() {
		String rtypeSObject;
		LoadAllRecordTypesList();
		if (recordTypesMapByDeveloperNameByObject == null) {
			recordTypesMapByDeveloperNameByObject = new Map <String , Map <String , RecordType >>();
			for (RecordType rtype :allRecordTypes) {
				if(recordTypesMapByDeveloperNameByObject.get(string.ValueOf(rtype.SobjectType)) == null) {//object not in the structure
					recordTypesMapByDeveloperNameByObject.put(string.ValueOf(rtype.SobjectType), new Map<String, RecordType>());
				}
				recordTypesMapByDeveloperNameByObject.get(string.ValueOf(rtype.SobjectType)).put(rtype.DeveloperName, rtype);
			}
		}
		return recordTypesMapByDeveloperNameByObject;
	}
	
	public static Map<String,RecordType> GetRecordTypesMap(String objectName)
	{
    	return GetRecordTypesMapByDeveloperNameByObject().get(objectName);
    }
	
	public static RecordType GetRecordTypeById(Id eyeDee)
	{
		for(String s: RecordType_Utilities.GetRecordTypesMapByDeveloperNameByObject().keySet())
		{
			for(RecordType r: GetRecordTypesMapByDeveloperNameByObject().get(s).Values())
			{
				if(r.Id == eyeDee)
					return r;
			}
		}
		return null;
	}
}