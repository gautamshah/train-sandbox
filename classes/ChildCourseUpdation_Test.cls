@istest
class ChildCourseUpdation_Test{
Static  testmethod void UpdateCourse (){

Course__c c1=new Course__c();
c1.Start_Date__c=system.Today();
c1.Status__c='Cancelled';
c1.Logistics_Survey__c='testurl';
c1.Follow_Up_Survey__c='testfollowurl';
insert c1;

Course__c c2=new Course__c();
c2.Start_Date__c=system.Today();
c2.Status__c='Postponed';
c2.Logistics_Survey__c='logistic';
c2.Follow_Up_Survey__c='followurl';
c2.Course__c=C1.Id;
insert c2;

Test.starttest();
update c1;
Test.stoptest();

System.assertEquals([select Status__c from Course__c where Id=:C1.Id].Status__c,[select Status__c from Course__c where Id=:C2.Id].Status__c);
System.assertEquals([select Logistics_Survey__c from Course__c where Id=:C1.Id].Logistics_Survey__c,[select Logistics_Survey__c from Course__c where Id=:C2.Id].Logistics_Survey__c);
System.assertEquals([select Follow_Up_Survey__c from Course__c where Id=:C1.Id].Follow_Up_Survey__c,[select Follow_Up_Survey__c from Course__c where Id=:C2.Id].Follow_Up_Survey__c);
}
}