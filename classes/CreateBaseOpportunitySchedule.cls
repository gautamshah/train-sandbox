/****************************************************************************************
 * Name    : CreateBaseOpportunitySchedule
 * Author  : Tatsuya Takesue
 * Date    : 03/12/2015
 *****************************************************************************************/
global class CreateBaseOpportunitySchedule implements Schedulable{

    global void execute(SchedulableContext sc){

        Create_Base_Opportunity_Batch_Settings__c batchSetting = Create_Base_Opportunity_Batch_Settings__c.getOrgDefaults();
        // 2016.03.22 ECS)kawada CVJ_SELLOUT_DEV-100 mod start
        // integer batchSize = 50;
        integer batchSize = 40;
        // 2016.03.22 ECS)kawada CVJ_SELLOUT_DEV-100 mod end
        if(batchSetting != null && batchSetting.BatchSize__c != null){
            batchSize = Integer.valueOf(batchSetting.BatchSize__c);
        }

        CreateBaseOpportunityBatch b= new CreateBaseOpportunityBatch();

        // バッチサイズをバッチクラスに渡す
        b.batchSize = batchSize;

        Database.executeBatch(b, batchSize);

    }
}