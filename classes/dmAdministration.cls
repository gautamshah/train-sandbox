public without sharing class dmAdministration extends dm
{
	public void fixsharing()
	{
		// Need to fix Admin access to each type of object
		// Assign Groups to Groups
		// Assign Groups to Applications
		// Assign Groups to Folders
		dmGroup.fixSharing();
		dmFolder.fixSharing();
		dmDocument.fixSharing();
	}
    
    public void fixGroupSharing() { dmGroup.fixSharing(); }
    public void fixFolderSharing() { dmFolder.fixSharing(); }
    public void fixDocumentSharing() { dmDocument.fixSharing(); }
    
    public void deletesharing()
    {
    	dmGroup.deleteSharing();
    	dmFolder.deleteSharing();
    	dmDocument.deleteSharing();
    }
}