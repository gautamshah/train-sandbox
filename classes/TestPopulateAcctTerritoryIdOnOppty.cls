@isTest
private class TestPopulateAcctTerritoryIdOnOppty {
/*********************************************************************
*
* Updates
*
*  Date      Author               Description
* ----------------------------------------------------------------------
*
*  8-3-2012   MJM                 Re-configured to used test account creation from TestUtility class
*  06-05-2015 Lakhan Dubey         Modified test class to test for update event,also deleted /changed earlier code instructions to run for the  trigger scenario 
*  14-05-2015 Lakhan Dubey         Removed assert statements for before insert as the logic for the same is commented in the trigger
*  5-July-2016 Amogh Ghodke        Commented whole code as PopulateAcctTerritoryIdOnOppty trigger is removed
*********************************************************************/
    static testMethod void myUnitTest() {
        
       /* String oid;
       
       UserTerritory t = [Select  TerritoryId, UserId from UserTerritory  where  IsActive = true and UserId IN :[Select id from user where isactive=true ] limit 1 ]; 
       UserTerritory t1 =[Select  TerritoryId, UserId from UserTerritory  where  Isactive=true and UserId!=:t.UserId  and UserId IN :[Select id from user where isactive=true ]   limit 1  ];
         
         
        
       user u=[Select id,name from user where id=:t.UserId and isactive=true ];
       user u1=[Select id,name from user where id=:t1.UserId and isactive=true ];
      
       
        system.runas(u){
        
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        Opportunity o = new Opportunity(AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!');
        insert o;
        oid=o .id;
              
        }
       
        //System.assertEquals([select TerritoryId from Opportunity where id=:oid].TerritoryId ,t.TerritoryId);
        
        Opportunity op=[Select id,TerritoryId,ownerID from opportunity where id=: oid];
        op.ownerID =u1.id;
        update op;
        
        System.assertEquals([select TerritoryId from Opportunity where id=:op.id].TerritoryId ,t1.TerritoryId);
        */
    }
}