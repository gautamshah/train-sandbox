/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MPM4_Button_Utility {
    global MPM4_Button_Utility() {

    }
    webService static Integer getSeats() {
        return null;
    }
    webService static String retrieveNextSuccessorMilestone(String msID) {
        return null;
    }
    webService static String retrieveNextSuccessorMilestoneName(String msID) {
        return null;
    }
    webService static String retrieveNextSuccessorTask(String tskID) {
        return null;
    }
    webService static String retrieveNextSuccessorTaskName(String tskID) {
        return null;
    }
}
