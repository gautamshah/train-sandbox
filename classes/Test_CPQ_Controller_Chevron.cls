/*
Test class

CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
2015-01-26  Yuli Fintescu   Created
06/14/2016  Paul Berglund   Updated to work with updated trigger dispatcher
10/31/2016  Paul Berglund   Remediation with CPQ
===============================================================================
*/
@isTest
private class Test_CPQ_Controller_Chevron {
    static List<Apttus_Proposal__Proposal__c> testProposals;
    static List<Apttus__APTS_Agreement__c> testAgreements;
    static void createTestData()
    {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        Map<String,Id> mapAgreementRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values()) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        for(RecordType R: RecordType_u.fetch(Apttus__APTS_Agreement__c.class).values()) {
            mapAgreementRType.Put(R.DeveloperName, R.Id);
        }

        testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'), Apttus_QPApprov__Approval_Status__c = 'Pending Approval', Apttus_Proposal__Approval_Stage__c = 'Draft'),
            new Apttus_Proposal__Proposal__c(RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'), Other_Non_Standard_Term_Requests__c = true, Other_Non_Standard_Request_Details__c = 'Requires Contract Dev'),
            new Apttus_Proposal__Proposal__c(RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'), Apttus_Proposal__Approval_Stage__c = 'XXXX')
        };
        insert testProposals;

        cpqAgreement_t.executedMethods.add('capture');
        testAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Apttus__Status_Category__c = 'Request', Apttus__Status__c = 'Request', Contract_Dev_Team_Review_Required__c = false),
            new Apttus__APTS_Agreement__c(Apttus_QPComply__RelatedProposalId__c = testProposals[1].ID, Apttus__Status_Category__c = 'Request', Apttus__Status__c = 'Request', Contract_Dev_Team_Review_Required__c = false),
            new Apttus__APTS_Agreement__c(Apttus_QPComply__RelatedProposalId__c = testProposals[1].ID, Apttus__Status_Category__c = 'In Signatures', Apttus__Status__c = 'Ready for Signatures', Contract_Dev_Team_Review_Required__c = true)
        };
        insert testAgreements;
    }

    static testMethod void myUnitTest() {
        createTestData();

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalChevron;
            Test.setCurrentPage(pageRef);

            List<Apttus_Proposal__Proposal__c> proposals = [Select Apttus_Proposal__Approval_Stage__c, RecordTypeId, RecordType.Name, RecordType.DeveloperName, Apttus_QPApprov__Approval_Status__c From Apttus_Proposal__Proposal__c Where ID in: testProposals];
            List<Apttus__APTS_Agreement__c> agreements = [Select Apttus__Status__c, RecordTypeId, RecordType.Name, RecordType.DeveloperName, Apttus_QPComply__RelatedProposalId__r.Contract_Dev_Team_Review_Required__c, Contract_Dev_Team_Review_Required__c From Apttus__APTS_Agreement__c Where ID in: testAgreements];

            CPQ_Controller_Chevron p = new CPQ_Controller_Chevron(new ApexPages.StandardController(proposals[0]));
            p = new CPQ_Controller_Chevron(new ApexPages.StandardController(proposals[1]));
            p = new CPQ_Controller_Chevron(new ApexPages.StandardController(proposals[2]));
            p.getChevrons();

            pageRef = Page.CPQ_AgreementChevron;
            Test.setCurrentPage(pageRef);

            p = new CPQ_Controller_Chevron(new ApexPages.StandardController(agreements[0]));
            p = new CPQ_Controller_Chevron(new ApexPages.StandardController(agreements[1]));
            p = new CPQ_Controller_Chevron(new ApexPages.StandardController(agreements[2]));
        Test.stopTest();
    }
}