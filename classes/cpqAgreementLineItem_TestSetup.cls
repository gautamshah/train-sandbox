@isTest
public class cpqAgreementLineItem_TestSetup
{

    public static Apttus__AgreementLineItem__c generateAgreementLineItem (
        Apttus__APTS_Agreement__c agreement,
        Product2 product
    ) {
        Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c();
        ali.Apttus__AgreementId__c = agreement.ID;
        ali.Apttus__ProductId__c = product.ID;
        return ali;
    }

}