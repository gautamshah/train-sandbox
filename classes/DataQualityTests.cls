@isTest(SeeAllData=true)
private class DataQualityTests { 
 /****************************************************************************************
    * Name    : Data Quality Tests
    * Author  : Mike Melcher
    * Date    : 11-20-2011
    * Purpose : Test methods for triggers and classes used in Data Quality functionality.
    * 
    * Dependancies: 
    *     - Requires class testUtility          
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 2-7-2012     MJM          UPdated to make use of testUtility class
    * 2-24-2012    MJM          Added method for testing Field Visit Share trigger
    * 4-23-2012    MJM          Removed Lead Conversion tests and placed them in separate class
    * 4-24-2012    MJM          Added coverage for CreateLeadExtension, CreateLeadFromContact, increased
    *                           coverage for CreteOpportunityExtension
    * 6-14-2012    MJM          Added coverage for AccountValidation and AccountCreateDQCase triggers, added Account_External_Id
    *                           for Accounts in ERP test to accomodate new validation rule.
    * 7-06-2012    MJM          Added coverage for Contact replication triggers.
    *
    * 8-02-2012    MJM          Added Billing Address information to test Accounts to pass new validation rule
    *
    *3-01-2013     MHS          Comment line 773, As the Department__c.Account__c  Field is not writeable.
    * 1-29-2014     Gautam Shah     Changes to lines: 36, 39, 56-68
    * 6/29/2015    Amogh Ghodke         Removed US - VT from line 180 to remove US - VT profile.
    *****************************************************************************************/
  
   static testMethod void testAccountCreation() 
   {
   
       DataQuality dq = new DataQuality();       
       Map<String,Id> rtmap = dq.getRecordTypes('Account');    
        
       Id usersubmitted = rtmap.get('User Submitted Healthcare Facility');
       User u = [select Id, Region__c from User where Profile.Name = 'System Administrator' And isActive = true Limit 1];
       u.Region__c = 'US';
       u.Country = 'US';
       u.State = 'NJ';
       u.PostalCode = '07029';
       update u;
       
       System.runAs(u)
       {
           Account aUS = new Account();
           aUS.Name = 'Test US';
           aUS.RecordtypeId = usersubmitted;
           aUS.BillingStreet = 'Test Billing Street';
           aUS.BillingCity = 'Test Billing City';
           aUS.BillingState = 'Test';
           aUS.BillingPostalCode = '12345';
           aUS.BillingCountry = 'CN';
           insert aUS;
           /*
           u.Region__c = 'CA';
           update u;
           
           Account aCA = new Account();
           aCA.Name = 'Test CA';
           aCA.RecordtypeId = usersubmitted;
           aCA.BillingStreet = 'Test Billing Street';
           aCA.BillingCity = 'Test Billing City';
           aCA.BillingState = 'Test';
           aCA.BillingPostalCode = '12345';
           insert aCA;
           */
       }
   }
   static testMethod void testFieldVisitShare() {
   
   // Select three users to serve as Manager and Sales Rep for the Field Visit record
   List<Id> pList = new List<Id>();
   
   for (Profile p : [select Id, Name from Profile where name = 'US - SUS' 
                                                     or name = 'US - All'
                                                     or name = 'US - RMS'
                                                     or name = 'US - AST']) {
      pList.add(p.Id);
   }
   
   List<User> Users = [select Id, username from User where IsActive = true and ProfileId in :pList];
   
   if (Users.size() > 2) {
      id ManagerId = Users[0].Id;
      id SalesRepId = Users[1].Id;
      id SalesRep2Id = Users[2].Id;
      
      Field_Visit__c fv = new Field_Visit__c();
      
      fv.Manager__c = ManagerId;
      fv.Sales_Rep__c = SalesRepId;
      
      insert fv;
      
      // verify that shares were created
      
      List<Field_Visit__Share> Shares = [select Id, parentId from Field_Visit__Share where parentId = :fv.Id];
      
      //System.AssertEquals(Shares.size(),3);
      
      // Update the Field Visit and verify trigger created a new share
      
      fv.Sales_Rep__c = SalesRep2Id;
      update fv;
      
      Shares = [select Id, parentId from Field_Visit__Share where parentId = :fv.Id];
      
      //System.AssertEquals(Shares.size(),4);
      
      
      
   }
}
 

   
   static testMethod void testOpportunityCreation() {
        
      //create test data
      
     
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();    
    
      Id connectedRT = rtmap.get('Connected Clinician');
      System.debug('DQ test: Opportunity Creation: connectedRT: ' + connectedRT);
      testUtility tu = new testUtility();
       
      Account usAccount = tu.testAccount('Country Accounts','xx-Account');
      Account sellto = tu.testAccount('US-Healthcare Facility','Test Sell-To');
      usAccount = [select Id, recordtypeId from Account where Id = :usAccount.Id];      
      Contact masterC = tu.testContact('Master Clinician',usAccount.Id,'TestMaster',null);
      System.debug('DQ test: Opportunity Creation: masterC: ' + masterC.Id + ' recordtype: ' + masterC.recordtypeId);
     
      Contact physician = tu.testContact('Connected Clinician',sellTo.Id,'Test Connected',masterC.Id);
      
      // The connected Contact will have its recordtype changed to Unverified by the DQ process.  It needs to be 
      // changed back to "Connected Clinician" so it can be used as an Opportunity Contact later
      
      physician = [select Id, FirstName, LastName, recordtypeId from Contact where Id = :physician.Id];
      physician.recordtypeId = connectedRT;
      physician.firstname = 'TestFirstName';
      update physician;
      
      System.debug('Physician ' + physician.Id + ' rt: ' + physician.recordtypeId + ' connected rt: ' + connectedRT);
      System.debug('DQ test: Opportunity Creation: Physician: ' + physician.Id + ' recordtype: ' + physician.recordtypeId);
      Product_Preference_Card__c ppc = new Product_Preference_Card__c();
      ppc.Physician__c = physician.Id;
      ppc.Procedure__c = 'test';
      //ppc.Physician__c = masterC.Id;
      ppc.AffiliatedAccount__c = sellto.Id;
      ppc.VT_Opportunity_Product_Group__c = 'US:SD:HERNIA';
      ppc.EbD_Opportunity_Product_Group__c = 'US:SD:HERNIA';
      ppc.SD_Opportunity_Product_Group__c = 'US:SD:HERNIA';
      
      insert ppc;
      
      Prod_Preference_Card_Product__c ppp = new Prod_Preference_Card_Product__c();
      ppp.Product_Preference_Card__c = ppc.Id;
      for(PricebookEntry p : [select id, product2id from pricebookentry where IsActive = true limit 1]) {
         ppp.Target_Covidien_Product__c = p.Product2Id;
      }
      
      insert ppp;
      
      PageReference pageRef = new PageReference('/apex/createOpportunityFromProductPreference');
      ApexPages.currentPage().getParameters().put('id', ppc.id);
      ApexPages.StandardController lController = new ApexPages.StandardController(ppc);
                    
      // Open extension
      CreateOpportunityExtension Ext = new CreateOpportunityExtension(lController);
      
      PageReference resultPage = Ext.createOpportunity();

   
   }
  /***** static testMethod void testContactReplication() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id conRT = rtmap.get('Connected Clinician');
      Id accountRT = rtMap.get('US-Healthcare Facility');
         
      testUtility tu = new testUtility();
        
      Account usAccount = tu.testAccount('Country Accounts','Test Account');
      Account hf1 = tu.testAccount('US-Healthcare Facility','test hf1');
      Account hf2 = tu.testAccount('US-Healthcare Facility','test hf2');
      Contact master = tu.testContact('Master Clinician',usAccount.Id,'test master',null);
      //Contact connected1 = tu.testContact('Connected Clinician',hf1.Id,'test contact1',master.Id);
      Contact connected1 = new Contact();
      connected1.accountId = hf1.Id;
      connected1.Master_Contact_Record__c = master.Id;
      connected1.recordtypeId = conRT;
      connected1.firstname = 'con1first';
      connected1.lastname = 'con1last';
      insert connected1;
      
      master = [select recordtypeId, firstname, lastname, hobbies__c from Contact where Id = :master.Id];
      System.debug('Connected1 inserted, master hobbies: ' + master.hobbies__c);
     
      contact connected2 = new Contact();
      
      connected2.accountId = hf2.Id;
      connected2.Master_Contact_Record__c = master.Id;
      connected2.recordtypeId = conRT;
      connected2.FirstName = 'NewFirstName';
      connected2.LastName = 'NewLastName';
      DataQuality.clearContactReplication();
      insert connected2;
      
      master = [select recordtypeId, firstname, lastname, hobbies__c from Contact where Id = :master.Id];
      System.debug('Connected2 inserted, master hobbies: ' + master.hobbies__c);
      
      connected2 = [select Id, FirstName, LastName, Title, Salutation,
                                   Suffix__c, Contact_Photograph__c, Phonetic_Pronunciation__c,
                                   TranslatedName__c, NPI_Code__c, State_Region_License__c, Specialty1__c,
                                   Specialty_2__c, Specialty_3__c, Type__c, Gender__c, MobilePhone, Phone,
                                   LinkedIn_Profile__c, Preferred_Contact_Method__c, Contact_Character_Profile__c,
                                   Contact_Segment__c, Contact_Motivations__c, MaritalStatus__c, PreferredLanguage__c,
                                   LanguagesKnown__c, Hobbies__c, Birthdate
                            from Contact where Id = :connected2.Id];
      
      // UPdate Connected2                      
      connected2.accountId = hf2.Id;
      connected2.Master_Contact_Record__c = master.Id;
      connected2.recordtypeId = conRT;
      connected2.FirstName = 'NewFirstName';
      connected2.LastName = 'NewLastName';
      connected2.Title = 'NewTitle';
      connected2.Salutation = 'NewS';
      connected2.Suffix__c ='NS';
      //connect2ed.Contact_Photograph__c;
      connected2.Phonetic_Pronunciation__c = 'NewPhonetic';
      connected2.TranslatedName__c = 'NewTranslated';
      connected2.NPI_Code__c = 'NewNPI';
      connected2.State_Region_License__c = 'NewLic';
      connected2.Specialty1__c = 'NewSpec1';
      connected2.Specialty_2__c = 'NewSpec2';
      connected2.Specialty_3__c = 'NewSpec3';
      connected2.Type__c = 'NewType';
      connected2.Gender__c ='NewG';
      connected2.MobilePhone = '1111111';
      connected2.Phone = '1111111';
      connected2.LinkedIn_Profile__c = 'NewLinked';
      connected2.Preferred_Contact_Method__c = 'NewContactMethod';
      connected2.Contact_Character_Profile__c = 'NewCharacterProfile';
      connected2.Contact_Segment__c = 'NewSegment';
      connected2.Contact_Motivations__c = 'NewMotivations';
      connected2.MaritalStatus__c = 'NewMarital';
      connected2.PreferredLanguage__c = 'NewPreferredLang';
      connected2.LanguagesKnown__c = 'NewLangKnown';
      connected2.Hobbies__c = 'NewHobbies';
      DataQuality.clearContactReplication();
      update connected2;
      //connected1.Birthdate 
      
      master = [select recordtypeId, firstname, lastname, hobbies__c from Contact where Id = :master.Id];
      System.debug('Connected2 updated, master hobbies: ' + master.hobbies__c);
     
     
      
      // Update the master
      master.hobbies__c = 'Hobbies';
      DataQuality.clearContactReplication();
      update master;
      
      // Verify the update to the master was replicated to the connected contacts
      
      Connected1 = [select firstname, lastname, hobbies__c from Contact where Id = :connected1.Id];
      System.debug('Master updated, connected1 hobbies: ' + connected1.hobbies__c);
      //System.assertEquals(Connected1.hobbies__c, 'Hobbies');
      
      Connected2 = [select firstname, lastname, hobbies__c from Contact where Id = :connected2.Id];
      System.debug('Master updated, connected2 hobbies: ' + connected2.hobbies__c);
      
      //System.assertEquals(Connected2.hobbies__c, 'Hobbies');
      
      
   }****/
   
   
   static testMethod void testTaskContactValidation() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id newRT = rtmap.get('Connected Clinician');
      Id accountRT = rtMap.get('US-Healthcare Facility');
         
      testUtility tu = new testUtility();
        
      Account usAccount = tu.testAccount(accountRT,'Test Account');
     
      
      Contact c = tu.testContact('newRT',usAccount.Id,'test contact',null);
     
      
      Task t = new Task();
      
      t.Subject = 'Test';
      t.WhoId = c.Id;
      t.WhatId = usAccount.Id;
      
      insert t;
   }

static testMethod void testEventContactValidation() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id newRT = rtmap.get('Connected Clinician');
      Id accountRT = rtmap.get('US-Healthcare Facility');
         
         
      testUtility tu = new testUtility();
        
      Account usAccount = tu.testAccount(accountRT,'Test Account');
      
      Contact c = tu.testContact('newRT',usAccount.Id,'test contact',null);
     
      Event t = new Event();
      
      t.Subject = 'Test';
      t.WhoId = c.Id;
      t.WhatId = usAccount.Id;
      
      datetime s = datetime.newInstance(2020,1,1,11,0,0);
      datetime e = datetime.newInstance(2020,1,1,12,0,0);
      t.StartDateTime = s;
      t.EndDateTime = e;
      
      insert t;
   }

   
   
  static testMethod void testCreateAffiliatedContact() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id newRT = rtmap.get('Master Clinician');
      ID accountRT = rtmap.get('US-Healthcare Facility');
         
      testUtility tu = new testUtility();
        
      Account usAccount = tu.testAccount(accountRT,'Test Account');
      
      Contact physician = tu.testContact('newRT',usAccount.Id,'test contact',null);
     
            
      ApexPages.currentPage().getParameters().put('id', physician.id);
      ApexPages.StandardController lController = new ApexPages.StandardController(physician);
                    
      // Open extension
      CreateAffiliatedContactExtension Ext = new CreateAffiliatedContactExtension(lController);
      
      PageReference resultPage = Ext.createAffiliatedContact();

   
   }

static testMethod void testCreateLeadFromContact() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id newRT = rtmap.get('Master Clinician');
      ID accountRT = rtmap.get('US-Healthcare Facility');
         
      testUtility tu = new testUtility();
        
      Account usAccount = tu.testAccount('Country Accounts','xx-Test Account');
      
      Contact masterC = tu.testContact('Master Clinician',usAccount.Id,'test contact',null);
     
            
      ApexPages.currentPage().getParameters().put('id', masterC.id);
      ApexPages.StandardController lController = new ApexPages.StandardController(masterC);
                    
      // Open extension
      CreateLeadFromContactExtension Ext = new CreateLeadFromContactExtension(lController);
      
      PageReference resultPage = Ext.createLead();

   
   }
   
static testMethod void testCreateLeadExtension() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id newRT = rtmap.get('Master Clinician');
      ID accountRT = rtmap.get('US-Healthcare Facility');
         
      testUtility tu = new testUtility();
        
      Account usAccount = tu.testAccount('Country Accounts','xx-Test Account');
      
      Contact masterC = tu.testContact('Master Clinician',usAccount.Id,'test contact',null);
      Lead l = new Lead();
            
      ApexPages.currentPage().getParameters().put('id', masterC.id);
      ApexPages.StandardController lController = new ApexPages.StandardController(l);
                    
      // Open extension
      CreateLeadExtension Ext = new CreateLeadExtension(lController);
      
      PageReference resultPage = Ext.createLead();

   
   }
static testMethod void testCreateMasterContacts() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();               
      Id newRT = rtmap.get('Country Accounts');
        
      testUtility tu = new testUtility();
        
      Account countryAccount = tu.testAccount(newRT,'xx-Test Account');
      
      
      ApexPages.currentPage().getParameters().put('id', countryAccount.id);
      ApexPages.StandardController lController = new ApexPages.StandardController(countryAccount);
                    
      // Open extension
      CreateContactFromAccountExtension Ext = new CreateContactFromAccountExtension(lController);
      
      PageReference resultPage = Ext.createMasterClinician();
      PageReference resultPage2 = Ext.createMasterNonClinician();
      PageReference resultPage3 = Ext.createConnectedNonClinician();
      PageReference resultPage4 = Ext.createConnectedClinician();



   
   }

 
   static testMethod void testERPAccountFunctionality() {
        
      //create test data
      
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();    
      Id caRT = rtmap.get('Country Accounts');           
      Id usRT = rtmap.get('US-Healthcare Facility');
      Id masterRT = rtmap.get('Master Clinician');
      Id connectedRT = rtmap.get('Connected Clinician');
      //Id noparentRT = rtmap.get('ERP Account No Parent Sell-To');
      Id noparentRT = rtmap.get('Master ERP Account');
      
      
      testUtility tu = new testUtility();
         
      Account sellto = tu.testAccount(usRT,'Test Sell-to');
      sellto = [select Id, Account_External_Id__c, recordtypeId from Account where Id = :sellto.Id];
      System.debug('Created sellto account, recordtype: ' + sellto.recordtypeId);
      sellto.Account_External_Id__c = 'a;lskdjf';
      update sellto;
      ERP_Account__c erp1 = new ERP_Account__c();
      erp1.Name = 'Test Method ERP1';
      erp1.Address_1__c = 'Test address';
      erp1.Address_2__c = 'line 2';
      erp1.City__c = 'City';
      erp1.State_Region__c = 'FL';
      erp1.Zip_Postal_Code__c = '32951';
      erp1.Country__c = 'US';
      erp1.RecordTypeId = noparentRT;
     // erp1.Parent_Sell_To_Account__c = sellto.Id;
      insert erp1;
      
      // Set the Sell To Parent for the ERP Account
      erp1 = [select Id, Parent_Sell_To_Account__c, Name, RecordTypeId from ERP_Account__c where Id = :erp1.Id];
      erp1.Parent_Sell_To_Account__c = sellto.Id;
      update erp1;
      
      // Verify recordtype was changed and case was created
      
      erp1 = [select Id, Parent_Sell_To_Account__c, Name, RecordTypeId from ERP_Account__c where Id = :erp1.Id];
      System.debug('Parent Sell To: ' + erp1.Parent_Sell_To_Account__c);
      //System.AssertEquals(erp1.RecordTypeId, pendingRT);
      
      
      
     // Create an ERP Account that will be copied to a Sell-To Account
      
      ERP_Account__c erp2 = new ERP_Account__c();
      erp2.Name = 'Test Method ERP2';
      erp2.Address_1__c = 'Test address';
      erp2.Address_2__c = 'line 2';
      erp2.City__c = 'City';
      erp2.State_Region__c = 'FL';
      erp2.Zip_Postal_Code__c = '32951';
      erp2.Country__c = 'US';
      erp2.RecordTypeId = noparentRT;
      erp2.Parent_Sell_To_Account__c = sellto.Id;
      insert erp2;
      
      ApexPages.StandardController lController = new ApexPages.StandardController(erp2);
                    
      // Open extension
      CopyAccountExtension Ext = new CopyAccountExtension(lController);
      
      PageReference resultPage = Ext.copyAccount();
      
      // Verify ERP Account record type updated and new Sell-To Account created
      
      List<Account> accList = [select Id, name, RecordTypeId from Account where Name = 'Test Method ERP2' limit 1];
//      System.assertNotEquals(accList.size(),0);
//      System.assertEquals(accList[0].RecordTypeId,usRT);
      
      erp2 = [select Id, RecordTypeId from ERP_Account__c where Id = :erp2.Id];
      //System.AssertEquals(erp2.RecordTypeId, pendingRT);
      
      
      // Create an ERP Account  for Canada that will be copied to a Sell-To Account
      
      ERP_Account__c erp3 = new ERP_Account__c();
      erp3.Name = 'Test Method ERP3';
      erp3.Address_1__c = 'Test address';
      erp3.Address_2__c = 'line 2';
      erp3.City__c = 'City';
      erp3.State_Region__c = 'AB';
      erp3.Zip_Postal_Code__c = '32951';
      erp3.Country__c = 'CA';
      erp3.RecordTypeId = noparentRT;
      erp3.Parent_Sell_To_Account__c = sellto.Id;
      insert erp3;
      
      lController = new ApexPages.StandardController(erp3);
                    
      // Open extension
      Ext = new CopyAccountExtension(lController);
      
      resultPage = Ext.copyAccount();
      
      // Verify ERP Account record type updated and new Sell-To Account created
      
      accList = [select Id, name, RecordTypeId from Account where Name = 'Test Method ERP3' limit 1];
      //System.assertNotEquals(accList.size(),0);
//      System.assertEquals(accList[0].RecordTypeId,caRT);
      
      erp3 = [select Id, RecordTypeId from ERP_Account__c where Id = :erp3.Id];
      //System.AssertEquals(erp3.RecordTypeId, pendingRT);
      
   }
   static testMethod void testERPSalesRollupTrigger() {
        
      //create test data
      
      
         
      DataQuality dq = new DataQuality();       
      Map<String,Id> rtmap = dq.getRecordTypes();    
      Id caRT = rtmap.get('Country Accounts');           
      Id usRT = rtmap.get('US-Healthcare Facility');
      Id masterRT = rtmap.get('Master Clinician');
      Id connectedRT = rtmap.get('Connected Clinician');
      //Id noparentRT = rtmap.get('ERP Account No Parent Sell-To');
      Id noparentRT = rtmap.get('Master ERP Account');
      
      testUtility tu = new testUtility();
         
      Account sellto = tu.testAccount(usRT,'Test Sell-to');
      sellto = [select Id, Account_External_Id__c, recordtypeId from Account where Id = :sellto.Id];
      sellto.Account_External_Id__c = 'a;lskdjf';
      update sellto;   
      
      
      ERP_Account__c erp1 = new ERP_Account__c();
      erp1.Name = 'Test Method ERP1';
      erp1.Address_1__c = 'Test address';
      erp1.Address_2__c = 'line 2';
      erp1.City__c = 'City';
      erp1.State_Region__c = 'FL';
      erp1.Zip_Postal_Code__c = '32951';
      erp1.Country__c = 'US';
      erp1.RecordTypeId = noparentRT;
      erp1.Parent_Sell_To_Account__c = sellto.Id;
      insert erp1;
       
//SC Adds 

      Account a7 = new Account();
      a7.name = 'Test method workplace 777';
      a7.RecordTypeId = usRT;
      a7.BillingStreet = 'Test Billing Street';
      a7.BillingCity = 'Test Billing City';
      a7.BillingState = 'Test';
      a7.BillingPostalCode = '12345';
      insert a7;       
       
      
//      Sales_History_SS__c s2 = new Sales_History_SS__c();
        Sales_History__c s2 = new Sales_History__c(); 
      s2.ERP_Account__c = erp1.Id;
      s2.Name = 'Test';
      s2.Customer__c = a7.id;
      insert s2;
      
//      s2.Prior_Month_1_Sales__c = 10;
        s2.name='Test';
        s2.CurrencyIsoCode='GBP';
        s2.PERIOD_1_SALES__c = 10;
        update s2;
      
      // Verify the roll-up amount on the Healthcare Facility was updated
      
      sellto = [select Id, S2_Total_Prior_12_Mos_Sales__c from Account where Id = :sellto.Id];
      
      //System.assertEquals(sellto.S2_Total_Prior_12_Mos_Sales__c, 10);
      
      
      
      
   }

    static testMethod void testCreateReciprocalAffiliationTrigger() {
   
   //create test data
      Id workplaceRT;
      Id usRT;
      for (RecordType rt : [select Id, Name from RecordType where Name = 'Affiliated WP Contact']) {
         workplaceRT = rt.Id;
      }
      for (RecordType rt : [select Id, Name from RecordType where Name = 'US-Healthcare Facility']) {
         usRT = rt.Id;
      }
      Account a1 = new Account();
      a1.name = 'Test method workplace 1';
      a1.RecordTypeId = usRT;
      a1.BillingStreet = 'Test Billing Street';
      a1.BillingCity = 'Test Billing City';
      a1.BillingState = 'Test';
      a1.BillingPostalCode = '12345';
      insert a1;
      
      Account a2 = new Account();
      a2.name = 'Test method workplace 2';
      a2.RecordTypeId = usRT;
      a2.BillingStreet = 'Test Billing Street';
      a2.BillingCity = 'Test Billing City';
      a2.BillingState = 'Test';
      a2.BillingPostalCode = '12345';
      insert a2;
      
      Account_Affiliation__c af = new Account_Affiliation__c();
      af.AffiliationMember__c = a1.Id;
      af.Affiliated_With__c = a2.Id;
      insert af;
      
      List<Account_Affiliation__c> afList = [select Id from Account_Affiliation__c where AffiliationMember__c = :a2.Id];
      
      //Verify a reciprocal Affiliation was created
      
      //System.AssertNotEquals(0,afList.Size());
  }

   static testMethod void testClinicalEvaluationTrigger() {
   
    //create test data
      Id usRT;
      Id usOppRT;
      
      for (RecordType rt : [select Id, Name from RecordType where Name = 'US-Healthcare Facility']) {
         if (rt.name == 'US-Healthcare Facility') {
            usRT = rt.Id;
         }
         if (rt.name == 'US - EbD - Opportunity') {
            usOppRT = rt.Id;
         }
      }
      Account a1 = new Account();
      a1.name = 'Test method workplace 1';
      a1.RecordTypeId = usRT;
      a1.BillingStreet = 'Test Billing Street';
      a1.BillingCity = 'Test Billing City';
      a1.BillingState = 'Test';
      a1.BillingPostalCode = '12345';
      insert a1;
        
      Opportunity o = new Opportunity();
      o.Name = 'TestOpp';
      o.Closedate = date.newinstance(2020,1,1);
      o.Stagename = 'New';
      o.AccountId = a1.Id;
      
      insert o;
      
      Clinical_Evaluation__c cl = new Clinical_Evaluation__c();
      cl.Name = 'Test CL';
      cl.Opportunity_Name__c = o.Id;
      insert cl;
      
      cl = [select id, Account__c from Clinical_Evaluation__c where Id = :cl.Id];
      //System.assertEquals(cl.Account__c, a1.Id);
   }
 
 /*   Test method removed -- trigger inactive
   
    static testMethod void testContactLinkWorkplaceTrigger() {
   
   //create test data
      Id workplaceRT;
      Id usRT;
      for (RecordType rt : [select Id, Name from RecordType where Name = 'Affiliated WP Contact']) {
         workplaceRT = rt.Id;
      }
      for (RecordType rt : [select Id, Name from RecordType where Name = 'US-Healthcare Facility']) {
         usRT = rt.Id;
      }
      Account a1 = new Account();
      a1.name = 'Test method workplace 1';
      a1.RecordTypeId = usRT;
      insert a1;
      
      Contact c1 = new Contact();
      c1.Lastname = 'TriggerTest';
      c1.Workplace_Name__c = 'Workplace';
      insert c1;
      
      c1 = [select Id, AccountId, Link_To_Healthcare_Facility__c from Contact where Id = :c1.Id];
      c1.Link_To_Healthcare_Facility__c = a1.Id;  
      
      update c1;
      
      c1 = [select Id, AccountId, Link_To_Healthcare_Facility__c from Contact where Id = :c1.Id];
      System.AssertEquals(c1.Link_To_Healthcare_Facility__c, c1.AccountId);
    }
      
  */
      
      
   //      Commented out -- triggers disabled
   
   
   static testMethod void testDepartmentUpdateFunctionality() {
        
      //create test data
      Id workplaceRT;
      Id usRT;
      
      for (RecordType rt : [select Id, Name from RecordType where Name = 'US-Healthcare Facility']) {
         usRT = rt.Id;
      }
      Account a1 = new Account();
      a1.name = 'Test method workplace 1';
      a1.RecordTypeId = usRT;
      a1.BillingStreet = 'Test Billing Street';
      a1.BillingCity = 'Test Billing City';
      a1.BillingState = 'Test';
      a1.BillingPostalCode = '12345';
      insert a1;
      
      Account a2 = new Account();
      a2.name = 'Test method workplace 2';
      a2.RecordTypeId = usRT;
      a2.BillingStreet = 'Test Billing Street';
      a2.BillingCity = 'Test Billing City';
      a2.BillingState = 'Test';
      a2.BillingPostalCode = '12345';
      insert a2;
            
      Account a3 = new Account();
      a3.name = 'Test method workplace 1';
      a3.RecordTypeId = usRT;
      a3.BillingStreet = 'Test Billing Street';
      a3.BillingCity = 'Test Billing City';
      a3.BillingState = 'Test';
      a3.BillingPostalCode = '12345';
      insert a3;
      
      Department__c d = new Department__c();
      d.Name = 'Test method1';
      d.Account__c = a3.Id;
      insert d;
      
     // d.Account__c = a2.Id;
      update d;
      
      
      
      
   }
    static testMethod void testReplicateContact() {
        
     DataQuality dq = new DataQuality();

     Contact  src = new Contact();
     src.FirstName = 'Fname';
     src.LastName = 'Lname';
     src.Title = 'Title';
     src.Salutation = 'MR';
     src.Website__c = 'http://Test.com';
     src.Personal_Email__c = 'Test@Test.com';
     src.OtherPhone = '123456';
     src.Suffix__c = 'Test Suffix';
     src.Contact_Photograph__c = '';
     src.Phonetic_Pronunciation__c = 'test';
     src.TranslatedName__c = 'test Name';
     src.NPI_Code__c = 'Test Code';
     src.State_Region_License__c = 'Test License';
     src.Specialty1__c = 'Test Speciality';
     src.Specialty_2__c = 'Test Speciality 2';
     src.Specialty_3__c = 'Test Specialtiy 3';
     src.Type__c = 'Test Type';
     src.Gender__c = 'Male';
     src.MobilePhone = '12345';
     src.Phone = '123345';
     src.LinkedIn_Profile__c = '';
     src.Preferred_Contact_Method__c = 'Test';
     src.Contact_Character_Profile__c = 'Test Profile';
     src.Contact_Segment__c = 'Test Segment';
     src.Contact_Motivations__c = 'Contact Motivations';
     src.MaritalStatus__c = 'Test';
     src.PreferredLanguage__c = 'Test';
     src.LanguagesKnown__c = 'Test';
     src.Hobbies__c = 'Test';
     //src.Birthdate = 'Test';
     src.KOL_Status__c = true ;
     src.KOL_Type__c = 'Test';
     src.KOL_Procedures__c = 'Test';
     

     Contact  target = new Contact();
     
     dq.ReplicateContact(src,target);
     

   }
   
   
}