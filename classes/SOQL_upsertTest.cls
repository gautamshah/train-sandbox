/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20170123        IL         AV-280     Created. To increase coverage on dependencies blocking AV-280.
20170126    A   PAB         AV-268      Same logic needed for all of the child classes so it was moved to here
*/
@isTest
public class SOQL_upsertTest
{
    @isTest
    static void constructor()
    {
        
    }
}