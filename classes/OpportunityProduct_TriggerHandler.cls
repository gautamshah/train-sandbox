/****************************************************************************************
 * Name    : OpportunityProduct_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 22/09/2014 
 * Purpose : Contains all the logic coming from Opportunity Product trigger
 * Dependencies: OpportunityProductTrigger Trigger
 *             , OpportunityLineItem Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
public with sharing class OpportunityProduct_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    private String strUserProfileName;
    
    private static boolean lineitemupdated = false;
    
    public static boolean hasLineItemUpdated() {
    	return lineitemupdated;
    }
    
    public static void setLineItemUpdated() {
        lineitemupdated = true;
    }
    
    public void OnBeforeInsert(List<OpportunityLineItem> newObjects){
    	for(OpportunityLineItem oli : newObjects)
    	{
    		if(oli.All_Lost__c)
	    	{
	    		oli.Original_Price__c = oli.UnitPrice;
	    		oli.UnitPrice = 0;
	    	}
    	}
    }
    
    public void OnBeforeUpdate(List<OpportunityLineItem> newObjects, Map<Id, OpportunityLineItem> oldMap){
    	for(OpportunityLineItem oli : newObjects)
    	{
    		if(oli.All_Lost__c && !oldMap.get(oli.Id).All_Lost__c)
	    	{
	    		oli.Original_Price__c = oli.UnitPrice;
	    		oli.UnitPrice = 0;
	    	}
	    	else if(!oli.All_Lost__c && oldMap.get(oli.Id).All_Lost__c)
	    	{
	    		oli.UnitPrice = oli.Original_Price__c;
	    	}
    	}
    }

    public OpportunityProduct_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void setAllLost(List<OpportunityLineItem> newObjects)
    {
    	Set<Id> oliIDs = new Set<Id>();
    	for(OpportunityLineItem oli : newObjects)
    	{
    		if(oli.All_Lost__c)
    			oliIDs.add(oli.Id);
    	}
    	
    	List<OpportunityLineItemSchedule> oliSchedule = [Select Id from OpportunityLineItemSchedule where OpportunityLineItemId = :oliIDs];
    	if(oliSchedule.size()>0)
    		delete oliSchedule;    	
    }

}