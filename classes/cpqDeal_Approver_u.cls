/*
Name    : cpqDeal_Approver_u
sObject : Apttus__APTS_Agreement__c,
          Apttus_Proposal__Proposal__c

Author  : Paul Berglund
Date    : 10/11/2016
Purpose : This code assigns Manager approvers to an Agreement or Proposal based on
          the Owner Id, unless Seller Approver is populated - then it will
          use the Seller Approver to determine who needs to approve.
          This replaces the Territory based approvals.

========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE
----        ------            ------
*/
public class cpqDeal_Approver_u extends sObject_u
{
    //
    // This class contains the methods to assign the diffrent approvers
    //
    public class Assignment
    {
        public void main(List<sObject> newList)
        {
            Map<Id, Id> ObjectId2UserId = DetermineAppropriateStartingUser(newList);
            AssignManagementApprovers(ObjectId2UserId, newList);
            AssignOfferDevelopmentApprover(ObjectId2UserId, newList);
        }

        //
        // Handles Proposals and Agreements
        //
        private Map<Id, Id> DetermineAppropriateStartingUser(List<sObject> newList)
        {
            Map<Id, Id> sObjectId2UserId = new Map<Id, Id>();

            for(sObject sobj : newList)
                sObjectId2UserId.put(sobj.Id, getCorrectUserForAssigningApprovers(sobj));

            return sObjectId2UserId;
        }

        private Id getCorrectUserForAssigningApprovers(sObject sobj)
        {
            return (sobj.get('SellerApprover__c') != null) ?
                        DATATYPES.castToId(sobj.get('SellerApprover__c')) :
                        DATATYPES.castToId(sobj.get('OwnerId'));
        }

        //
        //
        //
        private void AssignManagementApprovers(Map<Id, Id> ObjectId2UserId, List<sObject> newList)
        {
            SalesOrganizationLevel_g.Levels REGIONAL = SalesOrganizationLevel_g.Levels.Regional;
            SalesOrganizationLevel_g.Levels ZONE = SalesOrganizationLevel_g.Levels.Zone;
            SalesOrganizationLevel_g.Levels ORGANIZATION = SalesOrganizationLevel_g.Levels.Organization;

            Map<Id, User> cpqUsers = cpqUser_u.fetch(); //// This is no longer "costly"

            for(sObject sobj : newList)
            {
                if (ObjectId2UserId.containsKey(sobj.Id) &&
                    cpqUsers.containsKey(ObjectId2UserId.get(sobj.Id)))
                {
                    User rep = cpqUsers.get(ObjectId2UserId.get(sobj.Id));

                    sobj.put('RSM_Approver__c',
                        is(rep.Manager, REGIONAL) ?
                            rep.Manager.Id :
                            null);

                    sobj.put('ZVP__c',
                        is(rep.Manager, ZONE) ?
                            rep.Manager.Id :
                            is(rep.Manager.Manager, ZONE) ?
                                rep.Manager.Manager.Id :
                                null);

                    sobj.put('SVP_User__c',
                        is(rep.Manager, ORGANIZATION) ?
                            rep.Manager.Id :
                            is(rep.Manager.Manager, ORGANIZATION) ?
                                rep.Manager.Manager.Id :
                                is(rep.Manager.Manager.Manager, ORGANIZATION) ?
                                    rep.Manager.Manager.Manager.Id :
                                    null);
                }
            }
        }

        private void AssignOfferDevelopmentApprover(Map<Id, Id> objectId2UserId, List<sObject> newList)
        {
            Map<string, Id> Territory2ODA = cpqOfferDevelopmentAssignment_u.ExternalTerritoryName2OfferDevelopmentAnalystId;

            Map<Id, User> cpqUsers = cpqUser_u.fetch(); //// This is no longer "costly"

            Set<Id> managerIds = new Set<Id>();
            for(User u : cpqUsers.values())
                if (u.Manager != null)
                    managerIds.add(u.Manager.Id);

            Map<object, Map<Id, UserTerritory>> ManagerId2UserTerritories =
                UserTerritory_u.fetch(UserTerritory.field.UserId, DATATYPES.castToObject(managerIds));

            Set<Id> territoryIds = new Set<Id>();
            for(object obj : ManagerId2UserTerritories.keySet())
                for(UserTerritory ut : ManagerId2UserTerritories.get(obj).values())
                    territoryIds.add(ut.TerritoryId);

            Map<Id, Territory> territories = Territory_u.fetch(territoryIds);

           for(sObject sobj : newList)
            {
                Id rep = getCorrectUserForAssigningApprovers(sobj);

                if (cpqUsers.containsKey(rep) &&
                    cpqUsers.get(rep).Manager != null)
                {
                    Id managerId = cpqUsers.get(rep).Manager.Id;

                    if (ManagerId2UserTerritories.containsKey(managerId))
                    {
                        Map<Id, UserTerritory> uts = ManagerId2UserTerritories.get(managerId);

                        for(UserTerritory ut : uts.values())
                        {
                            Id tid = ut.TerritoryId;

                            if (territories.containsKey(tid))
                            {
                                Territory t = territories.get(tid);

                                if (Territory2ODA.containsKey(t.Custom_External_TerritoryID__c))
                                    sobj.put('OD_Approver__c', Territory2ODA.get(t.Custom_External_TerritoryID__c));

                            }
                        }
                    }
                }
            }
        }
    }

    private static boolean is(User u, SalesOrganizationLevel_g.Levels level)
    {
        return (u != null) && (level != null) &&
               (u.SalesOrganizationLevel__c != null) &&
               (SalesOrganizationlevel_g.Value2Level.containsKey(u.SalesOrganizationLevel__c)) &&
               (SalesOrganizationLevel_g.Value2Level.get(u.SalesOrganizationLevel__c) == level);
    }
}