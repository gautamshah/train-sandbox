global class DaysSinceLastLogin implements Database.Batchable<sObject>, Schedulable
{
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query ='';
        if(Test.isRunningTest())
            query = 'SELECT id,LastLoginDate,LastLoginDate__c FROM User LIMIT 10';
        else
            query = 'SELECT id,LastLoginDate,LastLoginDate__c FROM User where IsActive =True';
        
        return Database.getQueryLocator(query);
    }
    global void execute (Schedulablecontext ctx)    {
        DaysSinceLastLogin  batchExecute = new DaysSinceLastLogin();
        Database.executeBatch(batchExecute  , 10);
    }
    global void execute (Database.BatchableContext bc, List <User> scope )
    {
        List <User> lstUpdateUser = new List <User>();
        for (User userData: scope)
        {
            userData.LastLoginDate__c = userData.LastLoginDate;
            lstUpdateUser.add(userData);
        }
        update lstUpdateUser;
        //Database.SaveResult[] srList = Database.update(lstUpdateUser, false)
    }
    
    global void finish (Database.BatchableContext bc)
    {
    }
}