@isTest(SeeAllData=true)
public class ActivityReferencerTest
{
    static testmethod void testEventReferencer()
    {
        
        RecordType eventMigration = [Select Id From RecordType Where SobjectType = 'Event' And DeveloperName = 'Event_Migration' Limit 1];
        RecordType rmsEvent = [Select Id From RecordType Where SobjectType = 'Event' And Name = 'RMS & MS Events - EU/ANZ' Limit 1];
        Test.startTest();
        String objName = 'Event';
        String query = 'select Id, Activity_Detail__c, RecordTypeId from Event Where IsPrivate = false And IsChild = false And IsGroupEvent = false And IsArchived = false And IsRecurrence = false And RecordTypeId = \'' + eventMigration.Id + '\' Limit 10';
        String indicatorField = 'RecordTypeId';
        String indicatorValue = rmsEvent.Id;
        ActivityReferencerBatch arb = new ActivityReferencerBatch(objName,query,indicatorField,indicatorValue);
        Database.executeBatch(arb);
        Test.stopTest();
        Integer migrationCount = [Select count() From Event Where RecordTypeId = :eventMigration.Id];
        //System.assertEquals(migrationCount, 0);
    }
    
    static testmethod void testTaskReferencer()
    {
        
        RecordType taskMigration = [Select Id From RecordType Where SobjectType = 'Task' And DeveloperName = 'Task_Migration' Limit 1];
        RecordType emeaTask = [Select Id From RecordType Where SobjectType = 'Task' And Name = 'EMEA EM Tasks' Limit 1];
        Test.startTest();
        String objName = 'Task';
        String query = 'select Id, Activity_Detail__c, RecordTypeId from Event Where IsPrivate = false And IsChild = false And IsGroupEvent = false And IsArchived = false And IsRecurrence = false And RecordTypeId = \'' + taskMigration.Id + '\' Limit 10';
        String indicatorField = 'RecordTypeId';
        String indicatorValue = emeaTask.Id;
        ActivityReferencerBatch arb = new ActivityReferencerBatch(objName,query,indicatorField,indicatorValue);
        Database.executeBatch(arb);
        Test.stopTest();
        Integer migrationCount = [Select count() From Task Where RecordTypeId = :taskMigration.Id];
        //System.assertEquals(migrationCount, 0);
    }
}