global class AuditTrailReportSchedular implements Schedulable {
    global void execute(SchedulableContext sc) {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        String messageBody;
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();

        attach.setFileName('AuditTrailReport.xls');
        
        pagereference Pg = Page.AuditTrailReportPage;
        Blob body = null;
        if(Test.isRunningTest()){
            body = Blob.valueOf('Test Method');
        }else{
            body = pg.getcontent();
        }
        
        attach.Body = body;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] { 'ramya.purohit@cognizant.com','Gautam.Shah@medtronic.com' });
        mail.setSubject('Audit Trail Report');
        messageBody = '<html><body>Hi Gautam,<br><br>PFA, of the Audit Trail Report.<br><br>Thanks and Regards,<br><br>Ramya Purohit</body></html>';
        mail.setHtmlBody(messageBody);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });    
       
        mails.add(mail);
       
        if(!mails.isEmpty()) {
            Messaging.SendEmail(mails);
        }
    }
}