global class EMS_Trigger_Controller{
 
    @Future(callout=true)
    public static void addPDFAttach(string sessionId, list<id> EMSIds,string filetype){
        EMS_WSDL_FinalAddPDF.SessionHeader_element sessionIdElement= new EMS_WSDL_FinalAddPDF.SessionHeader_element();
        sessionIdElement.sessionId = sessionId;
     
        EMS_WSDL_FinalAddPDF.EMS_AddPdfonApproval stub= new EMS_WSDL_FinalAddPDF.EMS_AddPdfonApproval ();
        stub.SessionHeader= sessionIdElement;
        if(!test.isRunningTest())//Callouts not supported in Test Class. Added by Vinoth Kumar
        stub.addPDF(EMSIds,filetype);
    }
}