public with sharing class dmFolder extends dm
{
	//
	// TRIGGER METHODS (BULKIFIED)
	//
	public static void main(
			boolean isExecuting,
		 	boolean isInsert,
		 	boolean isUpdate,
		 	boolean isDelete,
		 	boolean isBefore,
		 	boolean isAfter,
		 	boolean isUndelete,
		 	List<dmFolder__c> newList,
		 	Map<Id, dmFolder__c> newMap,
		 	List<dmFolder__c> oldList,
		 	Map<Id, dmFolder__c> oldMap,
		 	integer size
		)
	{
		system.debug('isInsert: ' + isInsert);
		system.debug('isAfter: ' + isAfter);
		
        if (isBefore)
        {
            if (isInsert)
            {
  				dm.onInsert(newList);
            }
        }
        else if (isAfter)
        {
        	if (isInsert)
        	{
        		afterInsert(newList);
        	}
        }
	}
	
   	private static void afterInsert(List<dmFolder__c> newList)
  	{
  		if (newList == null || newList.size() == 0) return;
  		
  		List<dmFolder__c> applications = new List<dmFolder__c>();
  		
  		for (dmFolder__c f : newList)
  			if (f.isApplication__c)
  				applications.add(f);
			
		if (applications.size() > 0)
		{
			List<dmGroup__c> AdminRecords = dmGroup.createAdminRecords(applications);
			insert AdminRecords;
		}

	    List<dmFolder__Share> shares = createAdminSharesForRecords(newList);
	    insert shares;
  	}
  	
	//
	// CONSTRUCTORS
	//
    public dmFolder() { }

    public dmFolder(ApexPages.StandardSetController setcontroller)
    {
        super(setcontroller);
    }
    
    public dmFolder(ApexPages.StandardController controller)
    {
        super(controller);
        
//        if (mode == 'New')
//             this.controller.addFields(new List<String>(Schema.sObjectType.dmFolder__c.fields.getmap().keySet()));
    }

    //
    // PROPERTIES
    //
    public dmFolder__c record
    {
        get { return (dmFolder__c)base; }     
        set { base = (dmFolder__c)value; }
    }

//    private static Map<string, Id> RecordTypes
//    {
//    	get { return dm.fetchRecordTypes(dmFolder__c.class); }
//    }

//	public static Id applicationRecordTypeId = RecordTypes.get('Application');
	
	//
	// COLLECTION METHODS (BULKIFIED)
	//

	//
	// CREATE
	//
	
	//
	// RECORD METHODS
	//
	// Need the Id and Name of the Application folder
	//
	// FETCH 
	//
	public static dmFolder__c fetch(Id id)
	{
		string whereClause = ' WHERE Id = :id LIMIT 1';
    	string query = dm.buildSOQL(dmFolder__c.getSObjectType(), null, whereClause);
    	try
    	{
    		return (dmFolder__c)Database.query(query);
    	}
    	catch (Exception ex)
    	{
    		return null;
    	}
	}

	public static dmFolder__c TopLevelFolder(Id id) { return (id != null) ? TopLevelFolder(fetch(id)) : null; }
    public dmFolder__c TopLevelFolder { get { return TopLevelFolder(record); } }
    private static dmFolder__c TopLevelFolder(dmFolder__c f)
    {
   		return (f != null) ? (f.TopLevel__c == true) ? f : TopLevelFolder(fetch(f.Parent__c)) : null;
    }
    
    public static dmFolder__c ApplicationFolder(Id id) { return (id != null) ? ApplicationFolder(fetch(id)) : null; }
    public dmFolder__c ApplicationFolder { get { return ApplicationFolder(record); } }
    private static dmFolder__c ApplicationFolder(dmFolder__c f)
    {
   		return (f != null) ? (f.isApplication__c == true) ? f : ApplicationFolder(fetch(f.Parent__c)) : null;
    }
	//
	// SHARING
	//

	//
	// CREATE
	//
	private static List<dmFolder__Share> createAdminSharesForRecords(List<dmFolder__c> records)
	{
		List<dmFolder__Share> shares = new List<dmFolder__Share>();
		
		if (records == null || records.size() == 0) return shares;
		
		Map<Id, List<dmGroup__c>> ApplicationId2AdminRecords = new Map<Id, List<dmGroup__c>>();
		
		for(dmGroup__c g : dmGroups.All.values())
		{
			system.debug('record: ' + g.Name + ' ' + g.Application__c);
			if (!ApplicationId2AdminRecords.containsKey(g.Application__c))
				ApplicationId2AdminRecords.put(g.Application__c, new List<dmGroup__c>());
			
			if (g.isAdministrator__c)	
				ApplicationId2AdminRecords.get(g.Application__c).add(g);
		}
		
		Map<string, Group> GUID2SObject = dmGroup.fetchSObjects(dm.extractGUIDs(dmGroups.All.values()));
		system.debug('ApplicationId2AdminRecords: ' + ApplicationId2AdminRecords);
		system.debug('records: ' + records);

		for(dmFolder__c f : records)
		{
			system.debug('Application: ' + ApplicationFolder(f).Id);

			for(dmGroup__c admin : ApplicationId2AdminRecords.get(ApplicationFolder(f).Id))
			{
				Group g = GUID2SObject.get(admin.GUID__c);
				dmFolder__Share share = new dmFolder__Share();
			
				if (g.Name.endsWith(' - View'))
					share.AccessLevel = 'Read';
				else
					share.AccessLevel = 'Edit';
					
				share.put('ParentId', f.Id);
				share.put('UserOrGroupId', g.Id);
				
				shares.add(share);
			}
		}	
		return shares;
	}

	@future
	public static void deleteSharing()
	{
		List<dmFolder__Share> shares =
			[SELECT Id
			 FROM dmFolder__Share
			 WHERE AccessLevel IN ('Read', 'Edit')];
			 
		delete shares;
	}		

	@future
	public static void fixSharing()
	{
		List<dmFolder__c> records = dmFolders.All.values();

	    List<dmFolder__Share> shares = createAdminSharesForRecords(records);
	    insert shares;
	}

 	//
	// VISUAL FORCE PROPERTIES & METHODS
	//
    // Properties & Methods that support the VF component or page
    //
    public void onLoad()
    {
    	if (record != null && record.Id != null)
    		record = fetch(record.Id);
    }

    public PageReference view()
    {
        return (new ApexPages.StandardController(record)).view();
    }

	public static string buildBreadcrumb(Id folderId)
	{
		dmFolder__c folder = dmFolders.All.get(folderId);
		if (folder != null)
			return buildBreadcrumb(folder);
		else
			return null;
	}
	
	private static string buildBreadcrumb(dmFolder__c folder)
	{
		string link = null;
		
		if (folder != null)
		{
			link = '<a href="' + dm.baseURL + '/apex/dmObject?folderId=' + folder.Id + '">' + folder.Name + '</a>';
			
			Id parentId = folder.Parent__c;
			if (parentId != null)
			{
				dmFolder__c parent = dmFolders.All.get(parentId);
		
				if (parent != null)
				{
					string result = buildBreadcrumb(parent);

					if (result != null)
						link = result + ' > ' + link;
				}
			}
			else
				link = '<a href="' + dm.baseURL + '/apex/dmObject">Home</a> > ' + link;
		}
		return link;
	}
}