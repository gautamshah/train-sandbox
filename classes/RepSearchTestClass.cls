@isTest(seeAllData=true)

// I cannot create a UserTerritory Record so I have to query it. Not ideal

private class RepSearchTestClass {
    
    
    //Create Wrapper Class Object
    private class AccountSalesReps 
    {
        string userguid;
        string name;
        string therapy;
        string bu;
        string title;
        string email;
        string workphone;
        string cellphone;
        string photo;
        string mgrguid;
        string mgrname;
    }   
    
    
    
    static testMethod void TestAccountRepPage() {    
        
        // Get 1 User that has a Territory Assignment
        List<UserTerritory> ut = [Select UserId,  TerritoryId From UserTerritory where isActive = True LIMIT 1];  
        
        // Retrieve the Group ID associated with the users Territory
        ID gr = [Select ID From Group where RelatedId = :ut.get(0).TerritoryId LIMIT 1].ID;  
        
        // Create an Account
        Account act = New Account();
        act.Name = 'MyTestAccount';
        act.BillingCity = 'Boston';
        act.BillingState = 'MA'; 
        act.BillingPostalCode = '02018';
        
        insert act;
        
        string SelectedAct2 = act.id;   
        
        // Create an AccountShare to Assign the above account to the users territory group 
        // RowCause is not writeable but will automatically be set to Territory Manual
        
        AccountShare acts = new AccountShare();
        acts.AccountId = act.id; 
        acts.UserOrGroupId = gr;
        
        insert acts;
        
        
        // Create a single Rep To Therapy Record for the First User
        Rep_To_Therapy__c rt = new Rep_To_Therapy__c();
        rt.User_ID__c = ut.get(0).UserId; 
        rt.Therapy__c = 'TestTherapy';
        
        insert rt;
        
        
        // Create MDT_Rep_Detail Record
        MDT_RepDetail__c mdt = new MDT_RepDetail__c( BU__c = 'Spine',
                                                    City__c = 'Boston',
                                                    State__c = 'MA',
                                                    Org__c = 'MDT',
                                                    Cust_Name__c = 'Boston Hospital',
                                                    Cust_Num__c = '99999',
                                                    Postal_Code_5_Digit__c = '02018');  
        insert mdt;   
        
        
        // Create Sales Definition Record
        Sales_Definitions__c sales_def = new Sales_Definitions__c( 
            Definition__c = 'Definiton Goes Here',
            Examples__c = 'My Examples',
            Resources__c = 'URL For additional Info',
            Term__c = 'Sales Definition #1');  
        insert sales_def;   
        
        // Create Zip To Account Record
        ZipToAccount__c Z_to_A = new  ZipToAccount__c( 
            City__c = 'Boston',
            Cust_Name__c = 'Boston Hospital',
            Cust_Num__c = '99999',
            Org__c = 'MDT',
            Postal_Code_5_Digit__c = '02018',
            State__c = 'MA');  
        insert Z_to_A;        
        
        
        //Page Messages and PageReference
        List<Apexpages.Message> pageMessages1 = ApexPages.getMessages();
        PageReference pageRef1 = Page.RepSearchByCityState;
        Test.setCurrentPage(pageRef1);  
        
        List<Apexpages.Message> pageMessages2 = ApexPages.getMessages();
        PageReference pageRef2 = Page.RepSearchByAccount;
        Test.setCurrentPage(pageRef2);      
        
        List<Apexpages.Message> pageMessages3 = ApexPages.getMessages();
        PageReference pageRef3 = Page.RepSearchByPostal;
        Test.setCurrentPage(pageRef3);   
        
        List<Apexpages.Message> pageMessages4 = ApexPages.getMessages();
        PageReference pageRef4 = Page.RepSearchDefinitions;
        Test.setCurrentPage(pageRef4);       
        
        //Run for MITG
        RepSearch rep = new RepSearch(); 
        
        //Set Variables
        rep.selectedOrg = 'MITG';
        rep.SelectedAct2 = act.id;   
        rep.selectedAccountCity = act.BillingCity;
        rep.selectedAccountState = act.BillingState;
        rep.PostalCodeInput = act.BillingPostalCode;   
        
        //Execute Methods
        
        rep.getOrgs();
        rep.getStates();
        rep.getUniqueCity();
        RepSearch.searchAccount(rep.selectedAccountState,rep.selectedOrg ); //Remoting Method must be called directly 
        rep.getAccountsByPostalCode();
        rep.getAccountsByState();
        rep.getDefinitions();
        rep.UpdateForm();
        rep.ShowAccounts();
        rep.ShowCities();
        rep.RenderResults();
        rep.trackVisits();
        rep.getAccountTeams();
        rep.getMyTherapy();
        rep.ResetPage();
        
        //Run for MDT
        RepSearch rep2 = new RepSearch(); 
        
        //Set Variables
        rep2.selectedOrg = 'MDT';
        rep2.SelectedAct2 = mdt.Cust_Num__c;   
        rep2.selectedAccountState = mdt.State__c;
        rep2.PostalCodeInput = mdt.Postal_Code_5_Digit__c; 
        rep2.selectedAccountCity = mdt.City__c;
        
        //Execute Methods
        rep2.getUniqueCity();
        RepSearch.searchAccount(rep2.selectedAccountState,rep2.selectedOrg ); //Remoting Method must be called directly 
        rep2.getAccountsByPostalCode();
        rep2.getAccountsByState();
        rep2.getAccountTeams();
        rep2.getMyTherapy();
    }     
}