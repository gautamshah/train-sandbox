public with sharing class AccountTeamMemberListCtl_Asia {
     /****************************************************************************************
     * Name    : AccountTeamMemberListCtl_Asia 
     * Author  : Lakhan Dubey
     * Date    : 16/07/2014 
     * Purpose : Lists The account Team Members as well as the Account Terrirories for a selected
     *           account.China team wanted to exclude Corpaorate GBU users from showing up.Existing functionality doesn't allow this(apex class 'AccountTeamMemberListCtl').
                 So this class is almost same but it excludes users with 'Corporate' GBU to show up in the list displayed.
     * Dependencies: AccountTeamMember Object
     *             , Account_Territory__c Object
     *             , UserTerritory Object
     *             , Territory Object
     *             , User Object
     *
     * 9/14/2015 Amogh Ghodke Added lines:350,351 to compare user regions with APAC-ASIA & GC
     *****************************************************************************************/
    public class AccountTeamRec 
    {
        public string name{get;set;}
        public string role{get;set;}
        public string gbu{get;set;}
        public string title{get;set;}
        public string franchise{get;set;}
        public string mobilephone{get;set;}
        public set<string> territory{get;set;}
        public list<Territory> userTerritories{get;set;}
        public Id atmId{get;set;}
        public Id userId{get;set;}
        public String deleteAtmLabel{get;set;}
    }
    public Id atmToDelete{get;set;}
    public Class userPickList
    {
        Public String Business_Unit{get;set;}
        Public String Franchise{get;set;}
    }
    public userPickList userPickLists{get;set;}
    public Account acct{get;set;}
    public AccountTeamMemberListCtl_Asia(ApexPages.StandardController ctlr)
    {
        acct = (Account)ctlr.getRecord();
        userPickLists = new userPickList();
    
    }
        
    public list<AccountTeamRec> getAccountTeams()
    {
        list<AccountTeamRec> aList = new list<AccountTeamRec>();
        
       
        /**get the custom territory Ids associated with the account
         **/
        Set<String> AK_TerritoryIds = new set<String>();
       
        /*for(Account_Territory__c at : [Select Territory_ID__c
                                         From Account_Territory__c 
                                        where AccountID__c = :acct.Id])
        {
            AK_TerritoryIds.add(at.Territory_ID__c);
         
        } */
        
        for(Group at : [Select RelatedId From Group 
                        where Type = 'Territory' and Id in 
                        (Select UserOrGroupId From AccountShare 
                         Where RowCause in ('Territory', 'TerritoryManual') 
                               and AccountId = :acct.Id)])
        {
            AK_TerritoryIds.add(at.RelatedId);
         
        }
        
       
         /**Retrieve the actual territory Ids from the standard Territory Object
            Using the territory Ids retrieved from the Custom Account_Territory__C object 
          **/
         set<String> TerritoryIds = new set<String>();
         map<Id,Territory> TerritoryNamesMap = new map<Id,Territory>();
         
         for(Territory t : [select Id, Name 
                              from Territory 
                             where Id in :AK_TerritoryIds])
         {
            TerritoryIds.add(t.Id);
            TerritoryNamesMap.put(t.Id,t);
         } 
         
         /**Retrieve the Users in the Territories associated with the selected account and 
            map the all territories that the user belongs to that the account also belongs to
          **/                    
         set<Id> userSet = new set<Id>();
         map<Id, List<Territory>> userTerritroyNames = new map<Id, list<Territory>>();
         list<Territory> TerritoryNames = new list<Territory>();
         
         for(UserTerritory ut :[Select u.UserId,  u.TerritoryId, u.Id 
                                  From UserTerritory u
                                 where TerritoryId in :territoryIds
                                 And isActive = True
                                 order by UserId])
         {
            userSet.add(ut.UserId);
            if(userTerritroyNames.get(ut.UserId) ==  null)
            {
                TerritoryNames = new List<Territory>();
                TerritoryNames.add(TerritoryNamesMap.get(ut.TerritoryId));
                userTerritroyNames.put(ut.UserId, TerritoryNames);
            }
            else
            {
                TerritoryNames.add(TerritoryNamesMap.get(ut.TerritoryId));
                userTerritroyNames.put(ut.UserId, TerritoryNames);              
            }
         }
        
         /**Alter the GBU and Franchise Filter selections for use in a "Like" Clause
          **/ 
         String GBU_Like ;
         if(userPickLists.Business_Unit != null)
           GBU_Like = '%'+ userPickLists.Business_Unit +'%';
           
         String Franchise_Like;
         
         if(userPickLists.Franchise != null)
           Franchise_Like = '%' + userPickLists.Franchise + '%';
           
         //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '>>> GBU_Like >> :' +GBU_Like + '>>> Franchise_Like >> :' +Franchise_Like) ); 
         /**Retrieve the Account Team Member Records associated with the selected account
            filtering by user associations to the GBU and Franchise Filters selected by the user.
          **/ 
         if(userPickLists.Business_Unit == null )
         {
             for (AccountTeamMember atm : [select User.Name
                                               , User.Business_Unit__c
                                               , User.Franchise__c
                                               , User.User_Role__c
                                               , User.Title
                                               , User.MobilePhone
                                               , TeamMemberRole
                                               , AccountId
                                               , Id 
                                               , UserId                                      
                                            from AccountTeamMember
                                           where AccountId = :acct.Id and (User.Business_Unit__c  != 'Corporate')//
                                             //and (User.Business_Unit__c like :GBU_Like or User.Business_Unit__c = :userPickLists.Business_Unit__c )
                                             //and (User.Franchise__c like :Franchise_Like or User.Franchise__c = :userPickLists.Franchise__c)
                                            ])
            {
                
                
                AccountTeamRec atr = new AccountTeamRec();
                atr.name = atm.User.Name;
                atr.role = atm.User.User_Role__c;
                atr.gbu = atm.User.Business_Unit__c;
                atr.franchise = atm.User.Franchise__c;
                atr.title = atm.User.Title;
                atr.mobilephone = atm.User.MobilePhone;
                atr.atmId = atm.Id;
                atr.deleteAtmLabel = 'delete';
                atr.userId = atm.userId;
                aList.add(atr);
                
            }
            /**Retrieve the Users associated with the territories in common with the Selected Account's Territories.
             **/ 
            for (User  atm : [select Name
                                   , Business_Unit__c
                                   , Franchise__c
                                   , User_Role__c
                                   , Title
                                   , MobilePhone
                                   , Id                                     
                                from User
                               where Id in :userSet and (User.Business_Unit__c  != 'Corporate')])
            {
                AccountTeamRec atr = new AccountTeamRec();
                atr.name = atm.Name;
                atr.gbu = atm.Business_Unit__c;
                atr.franchise = atm.Franchise__c;
                atr.role = atm.User_Role__c;
                atr.title = atm.Title;
                atr.mobilephone = atm.MobilePhone;
                atr.userId = atm.Id;
                atr.userTerritories = userTerritroyNames.get(atm.Id);
                
                aList.add(atr);
                
            }
         }
         if(userPickLists.Business_Unit != null)
         {
            if(userPickLists.Franchise != '')
            {
                 for (AccountTeamMember atm : [select User.Name
                                                   , User.Business_Unit__c
                                                   , User.Franchise__c
                                                   , User.User_Role__c
                                                   , User.Title
                                                   , User.MobilePhone
                                                   , TeamMemberRole
                                                   , AccountId
                                                   , Id 
                                                   , UserId                                      
                                                from AccountTeamMember
                                               where AccountId = :acct.Id
                                                 and (User.Business_Unit__c  = :userPickLists.Business_Unit )
                                                 and (User.Franchise__c  = :userPickLists.Franchise)
                                                ])
                {
                    
                    
                    AccountTeamRec atr = new AccountTeamRec();
                    atr.name = atm.User.Name;
                    atr.role = atm.User.User_Role__c;
                    atr.gbu = atm.User.Business_Unit__c;
                    atr.franchise = atm.User.Franchise__c;
                    atr.mobilephone = atm.User.MobilePhone;
                    atr.userId = atm.userId;
                    aList.add(atr);
                    
                }
                for (User  atm : [select Name
                                   , Business_Unit__c
                                   , Franchise__c
                                   , User_Role__c
                                   , MobilePhone
                                   , Id                                     
                                from User
                               where Id in :userSet
                                 and (Business_Unit__c  = :userPickLists.Business_Unit )
                                 and (Franchise__c  = :userPickLists.Franchise)])
                 {
                    AccountTeamRec atr = new AccountTeamRec();
                    atr.name = atm.Name;
                    atr.gbu = atm.Business_Unit__c;
                    atr.franchise = atm.Franchise__c;
                    atr.role = atm.User_Role__c;
                    atr.title = atm.Title;
                    atr.mobilephone = atm.MobilePhone;
                    atr.userId = atm.Id;
                    atr.userTerritories = userTerritroyNames.get(atm.Id);
                    
                    aList.add(atr);
                    
                 }
            }
            else
            {
                 for (AccountTeamMember atm : [select User.Name
                                                   , User.Business_Unit__c
                                                   , User.Franchise__c
                                                   , User.User_Role__c
                                                   , User.Title
                                                   , User.MobilePhone
                                                   , TeamMemberRole
                                                   , AccountId
                                                   , Id 
                                                   , UserId                                      
                                                from AccountTeamMember
                                               where AccountId = :acct.Id
                                                 and (User.Business_Unit__c  = :userPickLists.Business_Unit )
                                                ])
                {
                    
                    
                    AccountTeamRec atr = new AccountTeamRec();
                    atr.name = atm.User.Name;
                    atr.role = atm.User.User_Role__c;
                    atr.gbu = atm.User.Business_Unit__c;
                    atr.franchise = atm.User.Franchise__c;
                    atr.mobilephone = atm.User.MobilePhone;
                    atr.userId = atm.Id;
                    aList.add(atr);
                    
                }
                
                for (User  atm : [select Name
                                   , Business_Unit__c
                                   , Franchise__c
                                   , User_Role__c
                                   , Title
                                   , MobilePhone
                                   , Id                                     
                                from User
                               where Id = :userSet
                                 and (Business_Unit__c  = :userPickLists.Business_Unit )
                                 ])
                 {
                    AccountTeamRec atr = new AccountTeamRec();
                    atr.name = atm.Name;
                    atr.gbu = atm.Business_Unit__c;
                    atr.franchise = atm.Franchise__c;
                    atr.role = atm.User_Role__c;
                    atr.title = atm.Title;
                    atr.mobilephone = atm.MobilePhone;
                    atr.userTerritories = userTerritroyNames.get(atm.Id);
                    atr.userId = atm.Id;
                    aList.add(atr);
                    
                 }
                
            }
         }
        
        /**return the unified set of users both in the account team as well as users who are in 
           territories common to the account's territories.
          **/
        return aList;
    }
    
    /** Return the GBUs configured in a custom setting
     **/
    public List<SelectOption>  getGBU()
    {
        List<SelectOption> options = new List<SelectOption>();
        map<string, string> gbuMap = new map<string,string>();
        options.add(new SelectOption('','--None--'));
        for (GBU_Franchises__c gbu : GBU_Franchises__c.getAll().values())
        {
             if(gbuMap.get(gbu.GBU__c) == null)   
                options.add(new SelectOption(gbu.GBU__c,gbu.GBU__c));
                
             gbuMap.put(gbu.GBU__c,gbu.GBU__c);
             
        }
            
        return options;
        
    }
    
    /** Filters Franchises based on the selected GBU filter
        using a custom setting to determine the relationship
     **/
    public List<SelectOption>  getFranchise()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--None--'));
        for (GBU_Franchises__c gbu : GBU_Franchises__c.getAll().values())
        {
            /* if(gbu.Franchise__c != null && gbu.Franchise__c != '' && userPickLists.Business_Unit == gbu.GBU__c )   
                options.add(new SelectOption(gbu.Franchise__c,gbu.Franchise__c));*/
                             
        }
        return options;
        
        
    }
    
    public Boolean getFullLayout()
    {   string userRegion =  [Select Region__c from User where Id = :UserInfo.getUserId() limit 1].Region__c;
        if( userRegion == 'APAC-ASIA' || userRegion == 'GC')
            return false;
        return true;
    }
    
    public PageReference deleteATM()
    {
        for( AccountTeamMember atm :[select Id from AccountTeamMember where Id = :atmToDelete])
          delete atm;
        
        return null;
    }
    

}