@isTest
private class RecordType_gTest
{

	static RecordType_g cache = new RecordType_g();

	static
	{
		System.debug('RecordType_gTest');
	}

	@isTest static void castException()
	{

		try
		{
			Map<Id,sObject> result = RecordType_g.cast((Map<Id,SObject>) null);
			System.assert(false,'Should not return a result');
		}
		catch (Exception e){}

	}

	@isTest static void fetchAll()
	{
		System.debug(CONSTANTS.NBSP(10) + 'fetchAll');

		Map<Id,RecordType> EX;
		Map<Id,RecordType> AC;

		EX = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType]);
		cache.put(EX.values());

		Test.startTest();

			// Start with no queries
			System.assertEquals(0, Limits.getQueries());

			System.debug('Fetch all RecordTypes');
			AC = cache.fetch();
			System.assertEquals(EX.size(), AC.size(),'RecordTypes not returned');
			System.assertEquals(EX.size(), cache.fetch().size());
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchById()
	{
		System.debug(CONSTANTS.NBSP(10) + 'fetchById');

		RecordType EX;
		RecordType AC;

		EX = [SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType LIMIT 1];
		cache.put(EX);

		Test.startTest();

			// Start with no queries
			System.assertEquals(0, Limits.getQueries());

			System.debug('Fetch RecordType');
			AC = cache.fetch(EX.Id);
			System.assertNotEquals(NULL, AC,'RecordType not returned');
			System.assertEquals(EX.Id, AC.Id,'Correct RecordType not returned');
			System.assertEquals(1, cache.fetch().size());
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchByIdSet()
	{
		System.debug(CONSTANTS.NBSP(10) + 'fetchByIdSet');

		Map<Id,RecordType> ES; // RecordTypes expected (small)
		Map<Id,RecordType> EL; // RecordTypes expected (large)
		Map<Id,RecordType> AC; // RecordTypes actual

		ES = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType LIMIT 5]);
		cache.put(ES.values());
		EL = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType LIMIT 10]);

		Test.startTest();

			// Start with no queries
			System.assertEquals(0, Limits.getQueries());

			System.debug('Fetch a set of RecordTypes');
			AC = cache.fetch(ES.keySet());
			System.assertEquals(5, AC.size());
			System.assertEquals(5, cache.fetch().size());
			System.assertEquals(ES.size(), AC.size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

			System.debug('Fetch the same RecordTypes');
			AC = cache.fetch(ES.keySet());
			System.assertEquals(cache.fetch().size(), 5);
			System.assertEquals(ES.size(), AC.size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

			System.debug('Fetch additional RecordTypes');
			cache.put(EL.values());
			AC = cache.fetch(EL.keySet());
			System.assertEquals(cache.fetch().size(), 10);
			System.assertEquals(EL.size(), AC.keySet().size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

			System.debug('Fetch a subset of RecordTypes');
			AC = cache.fetch(ES.KeySet());
			System.assertEquals(cache.fetch().size(), 10);
			System.assertEquals(ES.size(), AC.keySet().size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchByIdList()
	{

		System.debug(CONSTANTS.NBSP(10) + 'fetchByIdList');

		Map<Id,RecordType> EX;
		Map<Id,RecordType> AC;

		EX = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType LIMIT 5]);
		cache.put(EX.values());

		Test.startTest();

			AC = cache.fetch(EX.keySet());

			System.assertEquals(5, AC.size());
			System.assertEquals(5, cache.fetch().size());
			System.assertEquals(EX.size(), AC.size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchBySObjectField()
	{

		System.debug(CONSTANTS.NBSP(10) + 'fetchBySObjectField');

		Map<Id,RecordType> ALL;
		Map<Id,RecordType> EX;
		Map<Id,RecordType> AC;

		ALL = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType]);

		EX = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType
			WHERE sObjectType = 'Account']);

		cache.put(ALL.values());

		Test.startTest();

			AC = cache.fetch(RecordType.sObjectType).get('Account');

			System.assertEquals(ALL.size(), cache.fetch().size(), 'RecordTypes not cached');
			System.assertEquals(EX.size(), AC.size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchBySObjectFieldAndValue()
	{

		System.debug(CONSTANTS.NBSP(10) + 'fetchBySObjectFieldAndValue');

		Map<Id,RecordType> EX;
		Map<Id,RecordType> AC;

		EX = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType
			WHERE sObjectType = 'Account']);

		cache.put(EX.values());

		Test.startTest();

			AC = cache.fetch(RecordType.sObjectType,'Account');

			System.assertEquals(AC.size(), cache.fetch().size(), 'RecordTypes not cached');
			System.assertEquals(EX.size(), AC.size(),'RecordTypes not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchBySObjectFieldAndValues()
	{

		System.debug(CONSTANTS.NBSP(10) + 'fetchBySObjectFieldAndValue');

		Map<Id,RecordType> EX;
		Map<Object, Map<Id,RecordType>> ACO;
		Set<String> sObjectTypes = new Set<String>{'Account','Opportunity'};

		EX = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName, sObjectType, isActive FROM RecordType
			WHERE sObjectType = 'Account' OR sObjectType = 'Opportunity']);

		cache.put(EX.values());

		Test.startTest();

			ACO = cache.fetch(RecordType.sObjectType,DATATYPES.castToObject(sObjectTypes));
			System.assertEquals(ACO.size(), 2);
			System.assertEquals(ACO.get('Account').size() + ACO.get('Opportunity').size(), EX.size());
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

}