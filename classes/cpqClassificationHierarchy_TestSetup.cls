public class cpqClassificationHierarchy_TestSetup extends cpq_TestSetup
{
	@testVisible
	static Map<Id, Apttus_Config2__ClassificationHierarchy__c> loadData()
	{
/*
		{
		List<sObject> name = Test.loadData(Apttus_Config2__ClassificationName__c.sObjectType, 'cpqClassificationName');
		//system.assert(false, (List<Apttus_Config2__ClassificationName__c>)name);
		
		List<sObject> hierarchy = Test.loadData(Apttus_Config2__ClassificationHierarchy__c.sObjectType, 'cpqClassificationHierarchy');
		system.assert(false, (List<Apttus_Config2__ClassificationHierarchy__c>)hierarchy);
		}
*/
		
		Apttus_Config2__ClassificationName__c name = new Apttus_Config2__ClassificationName__c();
		name.Apttus_Config2__Active__c = true;
		name.Apttus_Config2__HierarchyLabel__c = 'Test Category';
		name.Apttus_Config2__Type__c = 'Offering';
		name.Name = 'Test Category';
		insert name;
		
		Map<Id, Apttus_Config2__ClassificationHierarchy__c> hierarchies =
			new Map<Id, Apttus_Config2__ClassificationHierarchy__c>();
			
		Apttus_Config2__ClassificationHierarchy__c hierarchy = new Apttus_Config2__ClassificationHierarchy__c();
		hierarchy.Name = 'Test Category';
		hierarchy.Apttus_Config2__Label__c = 'Test Category';
		hierarchy.Apttus_Config2__HierarchyId__c = name.Id;
		hierarchy.Apttus_Config2__AncestorId__c = null;
		hierarchy.Apttus_Config2__PrimordialId__c = null;
		hierarchy.Apttus_Config2__Level__c = 0;
		hierarchy.Apttus_Config2__Left__c = 1;
		hierarchy.Apttus_Config2__Right__c = 6;
		hierarchy.Apttus_Config2__MaxOptions__c = 1;
		hierarchy.Apttus_Config2__MinOptions__c = 1;
		hierarchy.Apttus_Config2__Modifiable__c = true;
		insert hierarchy;
		hierarchies.put(hierarchy.Id, hierarchy);
		
		Id primordialId = hierarchy.Id;
		Id ancestorId = hierarchy.Id;
		
		hierarchy = new Apttus_Config2__ClassificationHierarchy__c();
		hierarchy.Name = 'Hardware';
		hierarchy.Apttus_Config2__Label__c = 'Hardware';
		hierarchy.Apttus_Config2__HierarchyId__c = name.Id;
		hierarchy.Apttus_Config2__AncestorId__c = ancestorId;
		hierarchy.Apttus_Config2__PrimordialId__c = primordialId;
		hierarchy.Apttus_Config2__Level__c = 1;
		hierarchy.Apttus_Config2__Left__c = 2;
		hierarchy.Apttus_Config2__Right__c = 3;
		hierarchy.Apttus_Config2__MaxOptions__c = 1;
		hierarchy.Apttus_Config2__MinOptions__c = 1;
		hierarchy.Apttus_Config2__Modifiable__c = true;
		insert hierarchy;
		hierarchies.put(hierarchy.Id, hierarchy);

		hierarchy = new Apttus_Config2__ClassificationHierarchy__c();
		hierarchy.Name = 'Sensors';
		hierarchy.Apttus_Config2__Label__c = 'Sensors';
		hierarchy.Apttus_Config2__HierarchyId__c = name.Id;
		hierarchy.Apttus_Config2__AncestorId__c = ancestorId;
		hierarchy.Apttus_Config2__PrimordialId__c = primordialId;
		hierarchy.Apttus_Config2__Level__c = 1;
		hierarchy.Apttus_Config2__Left__c = 4;
		hierarchy.Apttus_Config2__Right__c = 6;
		hierarchy.Apttus_Config2__MaxOptions__c = 1;
		hierarchy.Apttus_Config2__MinOptions__c = 1;
		hierarchy.Apttus_Config2__Modifiable__c = true;
		insert hierarchy;
		hierarchies.put(hierarchy.Id, hierarchy);

		return hierarchies;
	}
    
    // So, if I have time to make this recursive, I can set the Left on the way down and then the Right on the way up
    
}