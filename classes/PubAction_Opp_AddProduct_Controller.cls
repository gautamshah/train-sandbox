/****************************************************************************************
* Name    : Class: PubAction_Opp_AddProduct_Controller
* Author  : Gautam Shah
* Date    : 11/17/2014
* Purpose : Controller for Salesforce1 custom publisher action, VF page: PubAction_Opp_AddProduct.page
* 
* Dependancies: 
* 	Called By: PubAction_Opp_AddProduct.page
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
public with sharing class PubAction_Opp_AddProduct_Controller 
{
	public Opportunity o {get; private set;}
	public String OPG {get; set;}
	public String OPGName {get; private set;}
	public List<Product> products {get; private set;}
	public Boolean isOPGSet {get; private set;}
	
	public PubAction_Opp_AddProduct_Controller(ApexPages.StandardController stdController)
	{
		stdController.addFields(new List<String>{'Pricebook2Id','Pricebook2.Name','CurrencyIsoCode'});
		this.o = (Opportunity)stdController.getRecord();
		this.OPG = this.o.Pricebook2Id;
		this.isOPGSet = false;
		if(this.OPG != null)
		{
			fetchProducts();
			this.isOPGSet = true;
			this.OPGName = this.o.Pricebook2.Name;
		}
	}
	
	public List<SelectOption> getOPGList()
    {
    	List<SelectOption> options = new List<SelectOption>();
    	options.add(new SelectOption('--None--', '--None--'));
    	for(Pricebook2 opg : [Select Id, Name From Pricebook2 Where IsActive = true and CurrencyIsoCode = :o.CurrencyIsoCode Order By Name])
    	{
    		options.add(new SelectOption(opg.Id, opg.Name));
    	}
    	return options;
    }
    
    public void fetchProducts()
	{
		products = new List<Product>();
		if(!String.isBlank(OPG))
		{
	    	List<PricebookEntry> prodList = new List<PricebookEntry>([Select Id, Name, UnitPrice From PricebookEntry Where isActive = true And Pricebook2Id = :OPG And CurrencyIsoCode = :o.CurrencyIsoCode Order By Name]);
	    	for(PricebookEntry prod : prodList)
	    	{
	    		Product p = new Product(prod.Id, prod.Name, 0, prod.UnitPrice, '');
	    		products.add(p);
	    	}
		}
    }
    
    @RemoteAction
    public static String addOLIs(List<Product> prodList)
    {
    	List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
    	String errMessage = '';
    	for(Product p : prodList)
    	{
    		OpportunityLineItem oli = new OpportunityLineItem();
    		oli.Description = p.Description;
    		oli.PricebookEntryId = p.Id;
    		oli.Quantity = p.Quantity;
    		oli.UnitPrice = p.UnitPrice;
    		oli.OpportunityId = p.oppID;
    		oliList.add(oli);
    	}
    	try
    	{
    		insert oliList;
    		return errMessage;
    	}
    	catch(Exception err)
    	{
    		errMessage = err.getMessage();
    		return errMessage;
    	}
    }
    
    public class Product
    {
    	public String Id {get;set;}
    	public String Name {get;set;}
    	public Integer Quantity {get;set;}
    	public Decimal UnitPrice {get;set;}
    	public String Description {get;set;}
    	public String oppID {get;set;}
    	
    	public Product(String i, String n, Integer q, Decimal u, String d)
    	{
    		this.Id = i;
    		this.Name = n;
    		this.Quantity = q;
    		this.UnitPrice = u;
    		this.Description = d;
    	}
    }
}