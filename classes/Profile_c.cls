public class Profile_c extends sObject_c
{
    public Profile_c(Id profileId)
    {
    	super(Profile.sObjectType, profileId);
    }
    
    private static Profile_g cache = new Profile_g();

    public static set<string> fieldsToInclude =
        new set<string>
        {
            'Id',
            'Description',
            'Name',
            'UserLicenseId',
            'UserType'
        };
  
    static
    {
        load();
    }
    
    ////
    //// load
    ////
    public static void load()
    {
        if (cache.isEmpty())
        {
            List<Profile> fromDB = fetchFromDB();
            cache.put(fromDB);
        }
    }
  
    ////
    //// fetch
    ////
    public static Profile fetch(String name)
    {

      return cache.fetch(Profile.field.Name, name);
    }

    
    
    ////
    //// fetchFromDB
    ////
    @testVisible
    private static List<Profile> fetchFromDB()
    {
        string query =
            SOQL_select.buildQuery(
                Profile.sObjectType,
                fieldsToInclude,
                null,
                null,
                null);
        
        return (List<Profile>)Database.query(query);
    }

}