@isTest
private class User_gTest 
{
	static User_g cache = new User_g();
	
	static
	{
		system.debug('User_gTest');
	}
	
	@isTest static void fetchByIdSet()
	{
		system.debug(CONSTANTS.NBSP(10) + 'fetchByIdSet'); 

		Map<Id,User> uES; // Users expected (small)
		Map<Id,User> uEL; // Users expected (large)
		Map<Id,User> uA; // Users actual

 		uES = new Map<Id,User>([SELECT Id, OrganizationName__c, SalesOrganizationLevel__c FROM User LIMIT 5]);
 		cache.put(uES.values());
		uEL = new Map<Id,User>([SELECT Id, OrganizationName__c, SalesOrganizationLevel__c FROM User LIMIT 10]);

		Test.startTest();

			// Start with no queries
			System.assertEquals(0, Limits.getQueries());

			System.debug('Fetch a set of users');
			uA = cache.fetch(uES.keySet());
			system.assertEquals(5, uA.size());
			System.assertEquals(5, cache.fetch().size());
			System.assertEquals(uES.size(), uA.size(),'Users not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

			System.debug('Fetch the same users');
			uA = cache.fetch(uES.keySet());
			System.assertEquals(cache.fetch().size(), 5);
			System.assertEquals(uES.size(), uA.size(),'Users not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

			System.debug('Fetch additional users');
			cache.put(uEL.values());
			uA = cache.fetch(uEL.keySet());
			System.assertEquals(cache.fetch().size(), 10);
			System.assertEquals(uEL.size(), uA.keySet().size(),'Users not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

			System.debug('Fetch a subset of users');
			uA = cache.fetch(uES.KeySet());
			System.assertEquals(cache.fetch().size(), 10);
			System.assertEquals(uES.size(), uA.keySet().size(),'Users not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}

	@isTest static void fetchByIdList() {

		system.debug(CONSTANTS.NBSP(10) + 'fetchByIdList'); 

		Map<Id,User> uE;
		Map<Id,User> uA;

		uE = new Map<Id,User>([SELECT Id, OrganizationName__c, SalesOrganizationLevel__c FROM User LIMIT 5]);
		cache.put(uE.values());

		Test.startTest();

			uA = cache.fetch(uE.keySet());

			System.assertEquals(5, uA.size());
			system.assertEquals(5, cache.fetch().size());
			System.assertEquals(uE.size(), uA.size(),'Users not returned');
			System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

		Test.stopTest();

	}
}