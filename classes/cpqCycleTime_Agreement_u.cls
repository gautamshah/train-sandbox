public class cpqCycleTime_Agreement_u extends cpqCycleTime_u
{
    // Capture the cycle times associated with changes to an Agreement
    protected override void capture(
        List<sObject> objNewList,
        Map<Id,sObject> objOldMap,
        Boolean isInsert,
        Boolean isUpdate)
    {
        
    	List<Apttus__APTS_Agreement__c> newList = (List<Apttus__APTS_Agreement__c>)objNewList;
    	Map<Id,Apttus__APTS_Agreement__c> oldMap = (Map<Id,Apttus__APTS_Agreement__c>)objOldMap;
    	
        Map<Id, CPQ_Cycle_Time__c> Id2CycleTime = new Map<Id, CPQ_Cycle_Time__c>();
        List<CPQ_Cycle_Time__c> ModifiedCycleTimes = new List<CPQ_Cycle_Time__c>();
        
        DateTime NOW = system.now();
        //Set<CPQ_Cycle_Time__c> cycleTimesToUpsert = new Set<CPQ_Cycle_Time__c>();
        
        List<Id> opportunityIds = new List<Id>();
        List<Id> proposalIds = new List<Id>();
        List<Id> agreementIds = new List<Id>();

        ////Map<Id,Id> ObjectId2CycleTimeId = new Map<Id,Id>();
        ////for (CPQ_Cycle_Time__c ct : Id2CycleTime.values()) {
        ////    if (ct.Agreement__c != null) {
        ////        ObjectId2CycleTimeId.put(ct.Agreement__c, ct.Id);
        ////    }
        ////    if (ct.Proposal__c != null) {
        ////        ObjectId2CycleTimeId.put(ct.Proposal__c, ct.Id);
        ////    }
        ////}

        //Map<Id,Id> ObjectId2CycleTimeId = new Map<Id,Id>();
        for (Apttus__APTS_Agreement__c agreement: newList)
        {
        	if (agreement.Apttus_QPComply__RelatedProposalId__c != null) proposalIds.add(agreement.Apttus_QPComply__RelatedProposalId__c);
        	if (agreement.Apttus__Related_Opportunity__c != null) opportunityIds.add(agreement.Apttus__Related_Opportunity__c);
        	agreementIds.add(agreement.Id);
            ////if (agreement.Apttus_QPComply__RelatedProposalId__c != null) {
            ////    proposalIds.add(agreement.Apttus_QPComply__RelatedProposalId__c);
            ////} else if (agreement.Apttus__Related_Opportunity__c != null) {
            ////    opportunityIds.add(agreement.Apttus__Related_Opportunity__c);
            ///}
            ////agreementIds.add(agreement.Id);
        }

        // Find existing Cycle Time rows related to the Opportunity, Proposal, or Agreement Id
        Id2CycleTime.putAll(new Map<id, CPQ_Cycle_Time__c>(
            [SELECT Id,
                    Opportunity__c,
                    Proposal__c,
                    Agreement__c,
                    Opportunity_Created_Date__c,
                    Agreement_Enter_Signatures_Date__c,
                    Agreement_Route_Signatures_Date__c
             FROM CPQ_Cycle_Time__c
             WHERE Opportunity__c IN :opportunityIds OR
                   Agreement__c IN :agreementIds OR
                   Proposal__c IN :proposalIds]));

        /*
        List<CPQ_Cycle_Time__c> cycleTimesByOpportunity =
            [SELECT Id,
                    Opportunity__c,
                    Proposal__c,
                    Agreement__c,
                    Opportunity_Created_Date__c,
                    Agreement_Enter_Signatures_Date__c,
                    Agreement_Route_Signatures_Date__c
             FROM CPQ_Cycle_Time__c
             WHERE Opportunity__c IN :opportunityIds];
             
        List<CPQ_Cycle_Time__c> cycleTimesByProposal =
            [SELECT Id,
                    Opportunity__c,
                    Proposal__c,
                    Agreement__c,
                    Opportunity_Created_Date__c,
                    Agreement_Enter_Signatures_Date__c,
                    Agreement_Route_Signatures_Date__c
             FROM CPQ_Cycle_Time__c
             WHERE Proposal__c IN :proposalIds];
        List<CPQ_Cycle_Time__c> cycleTimesByAgreement =
            [SELECT Id,
                    Opportunity__c,
                    Proposal__c,
                    Agreement__c,
                    Opportunity_Created_Date__c,
                    Agreement_Enter_Signatures_Date__c,
                    Agreement_Route_Signatures_Date__c
              FROM CPQ_Cycle_Time__c
              WHERE Agreement__c IN :agreementIds];
        */

        Map<Id,Id> ObjectId2CycleTimeId = new Map<Id,Id>();

        ////Map<Id, Id> OpportunityId2CycleTimeId = new Map<Id, Id>();
        ////Map<Id, Id> AgreementId2CycleTimeId = new Map<Id, Id>();
        ////Map<Id, Id> ProposaId2CycleTimeId = new Map<Id, Id>();
        /*
        Map<Id,CPQ_Cycle_Time__c> cycleTimeOpportunityMap = new Map<Id,CPQ_Cycle_Time__c>();
        Map<Id,CPQ_Cycle_Time__c> cycleTimeProposalMap = new Map<Id,CPQ_Cycle_Time__c>();
        Map<Id,CPQ_Cycle_Time__c> cycleTimeAgreementMap = new Map<Id,CPQ_Cycle_Time__c>();
        */
        for (CPQ_Cycle_Time__c ct: Id2CycleTime.values())
        {
            if (ct.Opportunity__c != null) ObjectId2CycleTimeId.put(ct.Opportunity__c, ct.Id);
            if (ct.Agreement__c != null) ObjectId2CycleTimeId.put(ct.Agreement__c, ct.Id);
            if (ct.Proposal__c != null) ObjectId2CycleTimeId.put(ct.Proposal__c, ct.Id);
        }
        /*
        for (CPQ_Cycle_Time__c ct: cycleTimesByOpportunity) {
            cycleTimeOpportunityMap.put(ct.Opportunity__c, ct);
        }
        for (CPQ_Cycle_Time__c ct: cycleTimesByProposal) {
            cycleTimeProposalMap.put(ct.Proposal__c, ct);
        }
        for (CPQ_Cycle_Time__c ct: cycleTimesByAgreement) {
            cycleTimeAgreementMap.put(ct.Agreement__c, ct);
        }
        */
        integer ndx = 1;
        for (Apttus__APTS_Agreement__c a: newList)
        {
            CPQ_Cycle_Time__c cycleTime = null;
            ////Boolean addToList = false;

			if (ObjectId2CycleTimeId.containsKey(a.Id))
				cycleTime = Id2CycleTime.get(ObjectId2CycleTimeId.get(a.Id));
			else
			{
				if (ObjectId2CycleTimeId.containsKey(a.Apttus_QPComply__RelatedProposalId__c))
					cycleTime = Id2CycleTime.get(ObjectId2CycleTimeId.get(a.Apttus_QPComply__RelatedProposalId__c));
				else if (ObjectId2CycleTimeId.containsKey(a.Apttus__Related_Opportunity__c))
					cycleTime = Id2CycleTime.get(ObjectId2CycleTimeId.get(a.Apttus__Related_Opportunity__c));
				else
				{
        	        cycleTime = new CPQ_Cycle_Time__c();
        	        ModifiedCycleTimes.add(cycleTime); // Need to add it so it gets upserted
				}
				
				cycleTime.Agreement__c = a.Id;
                cycleTime.Agreement_Created_Date__c = NOW;
			}
			
            // Find an existing Cycle Time row to update or create a new one if none is found
            ////if (AgreementId2CycleTimeId.containsKey(a.Id)){
            ////    cycleTime = cycleTimeAgreementMap.get(a.Id);
            ////} else if (ProposaId2CycleTimeId.containsKey(a.Apttus_QPComply__RelatedProposalId__c)) {
            ////    cycleTime = cycleTimeProposalMap.get(a.Apttus_QPComply__RelatedProposalId__c);
            ////    cycleTime.Agreement__c = a.Id;
            ////    cycleTime.Agreement_Created_Date__c = NOW;
            ////    addToList = true;
            ////} else if (OpportunityId2CycleTimeId.containsKey(a.Apttus__Related_Opportunity__c)) {
            ////    cycleTime = cycleTimeOpportunityMap.get(a.Apttus__Related_Opportunity__c);
            ////    cycleTime.Agreement__c = a.Id;
            ////    cycleTime.Agreement_Created_Date__c = NOW;
            ////    addToList = true;
            ////} else {
            ////    cycleTime = new CPQ_Cycle_Time__c();
            ////    cycleTime.Agreement__c = a.Id;
            ////    cycleTime.Agreement_Created_Date__c = NOW;
            ////    addToList = true;
            ////}

            // For updates, set any needed fields on the Cycle Time row for specific stages
            if (isUpdate && cycleTime != null)
            {
            	boolean wasModified = false;
                Apttus__APTS_Agreement__c oldAgreement = oldMap.get(a.Id);
                
                // Enter Signatures Date
                if (oldAgreement.Apttus__Status__c != READY_FOR_SIGNATURES &&
                    a.Apttus__Status__c == READY_FOR_SIGNATURES)
                {
                    if (cycleTime.Agreement_Enter_Signatures_Date__c == null)
                    {
                        cycleTime.Agreement_Enter_Signatures_Date__c = NOW;
                        wasModified = true;
                    }
                }

                // Route Signatures Date
                if ((oldAgreement.Apttus__Status__c != INTERNAL_SIGNATURES &&
                     oldAgreement.Apttus__Status__c != OTHER_PARTY_SIGNATURES) && 
                    (a.Apttus__Status__c == INTERNAL_SIGNATURES ||
                     a.Apttus__Status__c == OTHER_PARTY_SIGNATURES))
                {
                    if (cycleTime.Agreement_Route_Signatures_Date__c == null)
                    {
                        cycleTime.Agreement_Route_Signatures_Date__c = NOW;
                        wasModified = true;
                    }
                }

                // Exit Signatures Date
                if (oldAgreement.Apttus__Status__c != FULLY_SIGNED &&
                    a.Apttus__Status__c == FULLY_SIGNED)
                {
                    cycleTime.Agreement_Exit_Signatures_Date__c = NOW;
                    wasModified = true;
                }

                // Activate Date
                if (oldAgreement.Apttus__Status__c != ACTIVATED &&
                    a.Apttus__Status__c == ACTIVATED)
                {
                    cycleTime.Agreement_Activate_Date__c = NOW;
                    wasModified = true;
                }

                if (wasModified)
                { 
                    boolean existsInList = false;
                    for (CPQ_Cycle_Time__c obj : ModifiedCycleTimes)
                    {
                        //AV-574; only way to prevent having duplicate is compare pointer
                        if (obj === cycleTime)
                        {
                            existsInList = true;
                        }
                    }
                    
                    if (!existsInList)
                    {
                      ModifiedCycleTimes.add(cycleTime);   
                    }

                } 
            }

            ////if (addToList) {
            ////    cycleTimesToUpsert.add(cycleTime);
            ////}
        }

		upsert ModifiedCycleTimes;
        ////if (!cycleTimesToUpsert.isEmpty()) {
        ////    upsert new List<CPQ_Cycle_Time__c>(cycleTimesToUpsert);
        ////}
    }
}