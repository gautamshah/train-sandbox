public with sharing class RAPID_SVMXC_ServiceMaxUtils {

    public static List<SObject> getCustomObjects(String objectName, String whereClause, Set<Id> idSet)
    {
        String query = 'SELECT ';
        Date todayDate = system.today();
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();

        // Grab the fields from the describe method and append them to the queryString one by one.
        Integer count = 0;
        for(String s : objectFields.keySet()) {
            if (count++ > 0)
            {
                query += ' ,';
            }
           query += s;
        }

        // Add FROM statement
        query += ' FROM ' + objectName;

        // Add on a WHERE/ORDER/LIMIT statement as needed
        query += ' ' + whereClause;
  
  
        return database.query(query);
    }
       
    public static List<SObject> getCustomObjects(String objectName, String whereClause, String additionalFields, Set<Id> idSet)
    {
        String query = 'SELECT ';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();

        // Grab the fields from the describe method and append them to the queryString one by one.
        Integer count = 0;
        for(String s : objectFields.keySet()) {
            if (count++ > 0)
            {
                query += ' ,';
            }
           query += s;
        }
        
        

        // Add FROM statement
        query += ', ' + additionalFields + ' FROM ' + objectName;

        // Add on a WHERE/ORDER/LIMIT statement as needed
        query += ' ' + whereClause;
  
        system.debug(query);
        return database.query(query);
    }
    
}