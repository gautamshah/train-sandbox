@istest(seeAlldata=true)
public class distributorHomecls_test{
    
    
    static testmethod void m1(){
         Id pId = [select Id 
                      from Profile 
                     where name like 'Asia Distributor%' limit 1].Id;
         
        
        testUtility tu = new testUtility();
        Account a = tu.testAccount('ASIA - Account - Distributor (DIST)','New Account Test Name 1');
        
        Contact c = new Contact();
        c.LastName = 'x';
        c.AccountId = a.Id;
        List<RecordType> recordTypes = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND Name = 'Distributor Contact' LIMIT 1]; 
        c.RecordTypeId = recordTypes[0].Id;
        insert c;
        
        User u2mgr = new User();
         u2mgr.LastName = 'Shmoe Test';
         u2mgr.Business_Unit__c = 'The Unit';
         u2mgr.Franchise__c = 'Burger King';
         u2mgr.email = 'testZXmgr1@covidien.com';
         u2mgr.alias = 'testmgr1';
         u2mgr.username = 'testZXmgr1@covidien.com';
         u2mgr.communityNickName = 'testZXmgr1@covidien.com';
         u2mgr.ProfileId = pId;
         u2mgr.CurrencyIsoCode='SGD'; 
         u2mgr.EmailEncodingKey='ISO-8859-1';
         u2mgr.TimeZoneSidKey='America/New_York';
         u2mgr.LanguageLocaleKey='en_US';
         u2mgr.LocaleSidKey ='en_US';
         u2mgr.Asia_Team_Asia_use_only__c ='PRM Team';
         u2mgr.Country = 'SG';
         
        u2mgr.contactId = c.Id;
        //u2mgr.FullPhotoUrl='https://www.salesforce.com';
        insert u2mgr;
        
        Employee__c emp = new Employee__c();
        emp.Name='test employee';
        emp.Country__c='China';
        emp.Department__c='Manager';
        emp.Email__c='c@k.com';
        emp.Phonetic_Title__c='Mr';
        emp.Phonetic_Title__c='Test';
        emp.Phonetic_EmpName__c='Test Emo';
        emp.Employee_Code__c='12345';
        emp.Status__c='Active';
        emp.First_Name__c='test';
        emp.Last_Name__c='employee';
        emp.Employee_ID__c='12345';
        emp.Franchise__c='test';
        emp.Phonetic_Franchise__c='test';
        emp.Phonetic_FirstName__c='test';
        emp.Phonetic_LastName__c='employee';
        emp.Telephone_Number__c='1234567890';
        emp.User_Details__c = u2mgr.id;
        insert emp; 
        
        Convidien_Contact__c cc1 = new Convidien_Contact__c();
        cc1.Distributor_Name__c = a.Id;
        cc1.Employee__c = emp.Id;
        //cc1.Country__c='India';
        cc1.Type__c='test';
        insert cc1;
        system.runas(u2mgr){
        
            System.debug(u2mgr.contactId+'>>>>>>>>>>>>>>>>>>>>>>>'+u2mgr.profileId);
        
            
            distributorHomecls obj = new distributorHomecls();
        }
        
    
    }

}