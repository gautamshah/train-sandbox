public class cpqProductConfiguration_u extends sObject_u
{
	public override void main(
		boolean isExecuting,
		boolean isInsert,
		boolean isUpdate,
		boolean isDelete,
		boolean isBefore,
		boolean isAfter,
		boolean isUndelete,
		List<sObject> newList,
		Map<Id, sObject> newMap,
		List<sObject> oldList,
		Map<Id, sObject> oldMap,
		integer size)
	{
		
		
      
		if (isBefore)
		{
			if (isInsert)
				CPQ_ProposalProcesses.UpdateConfig(newList);

			if (isInsert || isUpdate)
			{
			    //calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
    			for (sObject sObj : newList)
    			{
    				Apttus_Config2__ProductConfiguration__c p = (Apttus_Config2__ProductConfiguration__c)sObj;
			        p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);
    			}
			}
		}

		if (isAfter && isUpdate)
		{
		    CPQ_ProposalProcesses.FinalizingConfig((List<Apttus_Config2__ProductConfiguration__c>)newList, (Map<ID, Apttus_Config2__ProductConfiguration__c>)oldMap);
    		CPQ_SSG_Proposal_Processes.RFACalculateUsage((List<Apttus_Config2__ProductConfiguration__c>)newList, (List<Apttus_Config2__ProductConfiguration__c>)oldList);
    		CPQ_AgreementProcesses.FinalizingConfig((List<Apttus_Config2__ProductConfiguration__c>)newList, (Map<ID, Apttus_Config2__ProductConfiguration__c>)oldMap);
		}
    }

	//=========================================================
	//	Update Config with proposal COOP fields
	//	When Config is create
	//	When proposal COOP fields changed, update only Saved config
	//	Requirement: When users setup some COOP fields on proposal,
	//				then go in to the cart, the Award Credit Calculator page
	//				should show the changes
	//=========================================================

	static final Map<sObjectField, sObjectField> Proposal2ProductConfigurationCOOPFields =
		new Map<sObjectField, sObjectField>
		{
			cpqProposal_FIELD.Duration => cpqProductConfiguration_FIELD.Duration,
			cpqProposal_FIELD.BuyoutAsOfDate => cpqProductConfiguration_FIELD.BuyoutAsOfDate,
			cpqProposal_FIELD.BuyoutAmount => cpqProductConfiguration_FIELD.BuyoutAmount,
			cpqProposal_FIELD.TaxGrossNetOfTradeIn => cpqProductConfiguration_FIELD.TaxGrossNetOfTradeIn,
			cpqProposal_FIELD.SalesTaxPercent => cpqProductConfiguration_FIELD.SalesTaxPercent,
			cpqProposal_FIELD.TBDAmount => cpqProductConfiguration_FIELD.TBDAmount,
			cpqProposal_FIELD.TotalAnnualSensorCommitmentAmount =>
				cpqProductConfiguration_FIELD.TotalAnnualSensorCommitmentAmount
		};

	//call from CPQ_ProductConfiguration_Before, Update Config with proposal COOP fields When Config is create
	public static void beforeInsert(List<Apttus_Config2__ProductConfiguration__c> inputConfigs)
	{
		Set<ID> pIds = new Set<ID>();
		for (Apttus_Config2__ProductConfiguration__c pc : inputConfigs)
			pIds.add((Id)pc.get(cpqProductConfiguration_FIELD.Proposal));

		//cpqProposal_u ps = new cpqProposal_u(pIds);

		Map<ID, Apttus_Proposal__Proposal__c> ps = new Map<Id, Apttus_Proposal__Proposal__c>(); // = cpqProposal_u.fetchCOOPFields(pIds);

		for (Apttus_Config2__ProductConfiguration__c pc : inputConfigs)
		{
			if(pc.Apttus_QPConfig__Proposald__c != null &&
			   ps.containsKey((Id)pc.get(cpqProductConfiguration_FIELD.Proposal)))
			{
				Apttus_Proposal__Proposal__c p = ps.get((Id)pc.get(cpqProductConfiguration_FIELD.Proposal));

				sObject_c.copy((sObject)p, (sObject)pc, Proposal2ProductConfigurationCOOPFields, true, false);
				//if (proposal.Duration__c != null)
				//	p.Duration__c = proposal.Duration__c;

				//if (proposal.Buyout_as_of_Date__c != null)
				//	p.Buyout_as_of_Date__c = proposal.Buyout_as_of_Date__c;

				//if (proposal.Buyout_Amount__c != null)
				//	p.Buyout_Amount__c = proposal.Buyout_Amount__c;

				//if (proposal.Tax_Gross_Net_of_TradeIn__c != null)
				//	p.Tax_Gross_Net_of_TradeIn__c = proposal.Tax_Gross_Net_of_TradeIn__c;

				//if (proposal.Apttus_Proposal__Sales_Tax_Percent__c != null)
				//	p.Sales_Tax_Percent__c = proposal.Apttus_Proposal__Sales_Tax_Percent__c;

				//if (proposal.TBD_Amount__c != null)
				//	p.TBD_Amount__c = proposal.TBD_Amount__c;

				//if (proposal.Total_Annual_Sensor_Commitment_Amount__c != null)
				//	p.Total_Annual_Sensor_Commitment_Amount__c = proposal.Total_Annual_Sensor_Commitment_Amount__c;
			}
		}
	}

	// Call from CPQ_Proposal_After, Update Config with proposal
	// When proposal COOP fields changed, update only Saved config
	static Map<sObjectField, sObjectField> fieldsToCompare = new Map<sObjectField, sObjectField>
	{
		cpqProposal_FIELD.Duration => cpqProductConfiguration_FIELD.Duration,
		cpqProposal_FIELD.BuyoutAsOfDate => cpqProductConfiguration_FIELD.BuyoutAsOfDate,
		cpqProposal_FIELD.TaxGrossNetOfTradeIn => cpqProductConfiguration_FIELD.TaxGrossNetOfTradeIn,
		cpqProposal_FIELD.SalesTaxPercent => cpqProductConfiguration_FIELD.SalesTaxPercent,
		cpqProposal_FIELD.TBDAmount => cpqProductConfiguration_FIELD.TBDAmount,
		cpqProposal_FIELD.TotalAnnualSensorCommitmentAmount =>
								cpqProductConfiguration_FIELD.TotalAnnualSensorCommitmentAmount
	};

	public static void afterUpdate(Map<Id, Apttus_Proposal__Proposal__c> ps)
	{
		if (!ps.isEmpty())
		{
			List<Apttus_Config2__ProductConfiguration__c> pcsUpdated =
				new List<Apttus_Config2__ProductConfiguration__c>();

			for (Apttus_Config2__ProductConfiguration__c pc : [SELECT Id
		                                                              ,Name
		                                                              ,Duration__c
		                                                              ,Buyout_as_of_Date__c
		                                                              ,Buyout_Amount__c
		                                                              ,Tax_Gross_Net_of_TradeIn__c
		                                                              ,Sales_Tax_Percent__c
		                                                              ,TBD_Amount__c
		                                                              ,Total_Annual_Sensor_Commitment_Amount__c
		                                                              ,Apttus_QPConfig__Proposald__c
														       FROM Apttus_Config2__ProductConfiguration__c
														       WHERE Apttus_QPConfig__Proposald__c IN :ps.keySet()
														         AND Apttus_Config2__Status__c = 'Saved'])
			{
				Apttus_Proposal__Proposal__c p = ps.get((Id)pc.get(cpqProductConfiguration_FIELD.Proposal));

				if (!sObject_c.equal(p, pc, fieldsToCompare))
				{
					sObject_c.copy(p, pc, fieldsToCompare);
					pcsUpdated.add(pc);
				}
			}

			update pcsUpdated;
		}
	}
}