public with sharing class PersonalInitiative_TriggerHandler extends TriggerFactoryHandlerBase{
	
	PersonalInitiative_Utilities piUtils = new PersonalInitiative_Utilities();
	
	public override void OnBeforeInsert(List<SObject> newMappings)
	{
		System.debug('In PersonalInitiative_TriggerHandler.OnBeforeInsert');
		piUtils.OnBeforeInsert((List<Personal_Initiative__c>) newMappings);
	}
	
	public override void OnBeforeUpdate(List<SObject> oldMappings, List<SObject> updatedMappings, Map<ID, SObject> mappingOldMap, Map<ID, SObject> mappingNewMap)
	{
		System.debug('In PersonalInitiative_TriggerHandler.OnBeforeUpdate');
		piUtils.OnBeforeUpdate((List<Personal_Initiative__c>) updatedMappings);
	}

}