/**
Test class

All WS callout mock response

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
===============================================================================
*/
@isTest
global class Test_CPQ_WebSvcCalloutMock implements WebServiceMock {
    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
        if (requestName == 'PartnerQueue') {
            CPQ_ProxyServices.PartnerQueueResponse_element respElement = new CPQ_ProxyServices.PartnerQueueResponse_element();
            respElement.PartnerQueueResult = new CPQ_ProxyiDealServiceDataType.Response1();
            respElement.PartnerQueueResult.resultField = 'Successful';
            
            response.put('response_x', respElement); 
        } else if (requestName == 'ValidationCheck') {
            CPQ_ProxyServices.ValidationCheckResponse_element respElement = new CPQ_ProxyServices.ValidationCheckResponse_element();
            respElement.ValidationCheckResult = new CPQ_ProxyiDealServiceDataType.Response();
            respElement.ValidationCheckResult.affiliationsField = new CPQ_ProxyiDealServiceDataType.ArrayOfAffiliation();
            respElement.ValidationCheckResult.auditsField = new CPQ_ProxyiDealServiceDataType.ArrayOfAudit();
            respElement.ValidationCheckResult.auditsField.Audit = new CPQ_ProxyiDealServiceDataType.Audit[]{};
                CPQ_ProxyiDealServiceDataType.Audit audit = new CPQ_ProxyiDealServiceDataType.Audit();
                audit.auditIdField = 12003;
                audit.messageField = 'Item XXX is invalid.';
            respElement.ValidationCheckResult.auditsField.Audit.add(audit);
            
            response.put('response_x', respElement); 
        } else if (requestName == 'RecallDeal') {
            CPQ_ProxyServices.RecallDealResponse_element respElement = new CPQ_ProxyServices.RecallDealResponse_element();
            respElement.RecallDealResult = new CPQ_ProxyiDealServiceDataType.Response2();
            respElement.RecallDealResult.dealRecalledField = true;
            
            response.put('response_x', respElement); 
        } else if (requestName == 'ContractInfo') {
            CPQ_ProxyServices.ContractInfo_element reqElement = (CPQ_ProxyServices.ContractInfo_element)request;
            CPQ_ProxyContractInfoDataType.ContractInfoRequestType req = reqElement.request;
            System.Debug('*** req ' + req);
            
            CPQ_ProxyServices.ContractInfoResponse_element respElement = new CPQ_ProxyServices.ContractInfoResponse_element();
            respElement.ContractInfoResult = new CPQ_ProxyContractInfoDataType.ContractInfoResponseType();
            respElement.ContractInfoResult.contractInfoField = new CPQ_ProxyContractInfoDataType.ContractInfoType();
            respElement.ContractInfoResult.errorCodeField = '0';
            respElement.ContractInfoResult.errorDescriptionField = 'Successful';
            respElement.ContractInfoResult.contractInfoField.pricingIdField = req.contractNumberField;
            respElement.ContractInfoResult.contractInfoField.nameField = 'D00112233';
            respElement.ContractInfoResult.contractInfoField.rootContractField = '00112233';
            respElement.ContractInfoResult.contractInfoField.effectiveDateField = System.Today();
            respElement.ContractInfoResult.contractInfoField.expirationDateField = System.Today().addYears(1);
            respElement.ContractInfoResult.contractInfoField.commitmentTypeField = 'IP';
            
            respElement.ContractInfoResult.dealersField = new CPQ_ProxyContractInfoDataType.ArrayOfContractDealerType();
            respElement.ContractInfoResult.dealersField.ContractDealerType = new CPQ_ProxyContractInfoDataType.ContractDealerType[]{};
            
            CPQ_ProxyContractInfoDataType.ContractDealerType dealer = new CPQ_ProxyContractInfoDataType.ContractDealerType();
            dealer.shipToField = 333333;
            respElement.ContractInfoResult.dealersField.ContractDealerType.add(dealer);
            dealer = new CPQ_ProxyContractInfoDataType.ContractDealerType();
            dealer.shipToField = 333333;
            respElement.ContractInfoResult.dealersField.ContractDealerType.add(dealer);
            
            respElement.ContractInfoResult.itemsField = new CPQ_ProxyContractInfoDataType.ArrayOfContractItemType();
            respElement.ContractInfoResult.itemsField.ContractItemType = new CPQ_ProxyContractInfoDataType.ContractItemType[]{};
            
            CPQ_ProxyContractInfoDataType.ContractItemType item = new CPQ_ProxyContractInfoDataType.ContractItemType();
            item.itemNumberField = 'ITEM1';
            item.dealerNetField = 100.00;
            respElement.ContractInfoResult.itemsField.ContractItemType.add(item);
            item = new CPQ_ProxyContractInfoDataType.ContractItemType();
            item.itemNumberField = 'ITEM1';
            item.directPriceField = 120.00;
            respElement.ContractInfoResult.itemsField.ContractItemType.add(item);
            
            System.Debug('*** respElement ' + respElement);
            
            response.put('response_x', respElement); 
        } else if (requestName == 'GetSalesHistory') {
            CPQ_ProxyServices.GetSalesHistoryResponse_element respElement = new CPQ_ProxyServices.GetSalesHistoryResponse_element();
            respElement.GetSalesHistoryResult = 0.1599;
            
            response.put('response_x', respElement); 
        }
        else if (requestName == 'GetContractEligibilityByRoot') {
            CPQ_ProxyServices.GetContractEligibilityByRootResponse_element respElement = new CPQ_ProxyServices.GetContractEligibilityByRootResponse_element();
            respElement.GetContractEligibilityByRootResult = new CPQ_ProxyContractType.Proxy_CustomerWrapper();
            List<CPQ_ProxyContractType.Contract> customers = new List<CPQ_ProxyContractType.Contract>();
            CPQ_ProxyContractType.Contract contract = new CPQ_ProxyContractType.Contract();
            contract.ShipTo = 542910;
            customers.add(contract);
            respElement.GetContractEligibilityByRootResult.Customers = new CPQ_ProxyContractType.ArrayOfContract();
            respElement.GetContractEligibilityByRootResult.Customers.Contract = customers;
            respElement.GetContractEligibilityByRootResult.TotalCount = 1;
            
            response.put('response_x', respElement);
        }
    }
}