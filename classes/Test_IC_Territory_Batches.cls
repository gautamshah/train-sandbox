/**
Test class for 
IC_Batch_Territory_Relation_Schedule
IC_Batch_Territory_Relation
IC_Batch_Territory_Deletion
IC_Execute_Territory_Deletion
IC_ETL_Process_Log_After trigger

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-06-25      Yuli Fintescu       Created
2015-08-12		Bill Shan			Added new test method TestTerritoryDeleteBatch
2015-08-18		Bill Shan 			Combined the test method TestRelationScheduler into myUnitTest
===============================================================================
*/
@isTest
private class Test_IC_Territory_Batches {
    static testMethod void myUnitTest() {
        Test.startTest();
        
		        List<Territory> testTerritories = new List<Territory> {
		            new Territory(Name = 'National 1', ParentTerritoryId = null, ExternalParentTerritoryID__c = '', Custom_External_TerritoryID__c = 'Natn1', Source__c = 'US_MANS' , flagForProcessing__c = false),
		            new Territory(Name = 'Zone 1', ParentTerritoryId = null, ExternalParentTerritoryID__c = 'Natn1', Custom_External_TerritoryID__c = 'Zone1', Source__c = 'US_MANS' , flagForProcessing__c = false)
		        };
		        insert testTerritories;
		        
                Database.BatchableContext bc;
	            IC_Batch_Territory_Relation batchApex = new IC_Batch_Territory_Relation();
                batchApex.start(bc);
        		batchApex.execute(bc, testTerritories);
                batchApex.finish(bc);
             
        Test.stopTest();
    }

    
    static testMethod void TestRelationScheduler() {
    	Test.startTest();
        Schedulablecontext sc;
        IC_Batch_Territory_Relation_Schedule srcalc = new IC_Batch_Territory_Relation_Schedule();
        srcalc.execute(sc);
    	Test.stopTest();
    }
    
    static testMethod void TestICETLProcessLogTrigger() {
    	Test.startTest();
	    		List<IC_ETL_Process_Log__c> testLogs = new List<IC_ETL_Process_Log__c>();
	    		
	    		IC_ETL_Process_Log__c testLog = new IC_ETL_Process_Log__c();
	    		testLogs.add(testLog);
	    		testLog.Log_Type__c = 'Controller';
	    		testLog.Operation__c = 'IC_Batch_Territory';
	    		try {
	    			insert testLog;
	    		} catch (Exception e) {}
	    		
	    		testLog = new IC_ETL_Process_Log__c();
	    		testLogs.add(testLog);
	    		testLog.Log_Type__c = 'Controller';
	    		testLog.Operation__c = 'IC_Batch_Territory_Relation';
	    		try {
	    			insert testLog;
	    		} catch (Exception e) {}
	    		
	    		testLog = new IC_ETL_Process_Log__c();
	    		testLogs.add(testLog);
	    		testLog.Log_Type__c = 'Controller';
	    		testLog.Operation__c = 'Abort Job';
	    		testLog.Message__c = 'a job';
	    		try {
	    			insert testLog;
	    		} catch (Exception e) {}
	    		
	    		testLog = new IC_ETL_Process_Log__c();
	    		testLogs.add(testLog);
	    		testLog.Log_Type__c = 'Controller';
	    		testLog.Operation__c = 'Schecule IC_Batch_Territory_Relation';
	    		testLog.Additional_Info__c = 'Test====Test ';
	    		try {
	    			insert testLog;
	    		} catch (Exception e) {}
	    		
	    		testLog = new IC_ETL_Process_Log__c();
	    		testLogs.add(testLog);
	    		testLog.Log_Type__c = 'Controller';
	    		testLog.Operation__c = 'Unschecule Task';
	    		testLog.Message__c = 'a job';
	    		try {
	    			insert testLog;
	    		} catch (Exception e) {}
	    		
	    		try {
	    			insert testLogs;
	    		} catch (Exception e) {}
    	Test.stopTest();
    }
    
    static testMethod void TestTerritoryDeleteBatch() {
    	Test.startTest();
        Territory t1 = new Territory(Name = 'National 2', ParentTerritoryId = null, ExternalParentTerritoryID__c = '', Custom_External_TerritoryID__c = 'Natn2', Source__c = 'US_MANS' , flagForProcessing__c = false, flagToDelete__c = true);
        insert t1;
        
        Territory t2 = new Territory(Name = 'Zone 2', ParentTerritoryId = t1.ID, ExternalParentTerritoryID__c = 'Natn2', Custom_External_TerritoryID__c = 'Zone2', Source__c = 'US_MANS' , flagForProcessing__c = false, flagToDelete__c = true);
        insert t2;
        IC_Execute_Territory_Deletion.execute();
    	Test.stopTest();
    }
}