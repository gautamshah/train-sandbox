public virtual class Product2_c extends sObject_c
{
	protected Product2_c()
	{
		super();
	}
	
    public Product2_c(Map<sObjectField, object> fieldValues)
    {
    	super(Product2.class, null, false);
    	
    	// Set default values before we set the fields
    	// so they are set if not included, but can be overridden
    	// if they are included
    	this.put(Product2.field.IsActive, true);
    	
    	// Add the field values
    	for(sObjectField field : fieldValues.keySet())
    		this.put(field, fieldValues.get(field));
    	
    	// Create some fields if they are still blank
    	this.putIfNull(Product2.field.ProductCode, this.get(Product2.field.Name));
    	this.putIfNull(Product2.field.Product_External_ID__c,
    		'US:' +
    		this.get(Product2.field.OrganizationName__c) +
    		this.get(Product2.field.ProductCode));
    }
}