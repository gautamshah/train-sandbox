/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A    IL         AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/     
public class cpqParticipatingFacility_c
{
    public static List<Participating_Facility__c> fetch(Id proposalId)
    {
        return [Select Id, Account__c, Proposal__c, Account__r.Name
                From Participating_Facility__c
                Where Proposal__c = :proposalId];
    }
    
    
}