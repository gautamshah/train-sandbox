global with sharing class US_RMS_Playbook {

    private Id userid = UserInfo.getUserid();

    @RemoteAction
    public static Folder getFolder(string name) {
        Folder folder = null;
        
        try {
            folder = [SELECT Id, Name FROM Folder WHERE Name = :name];
        }
        catch (Exception ex) {
            return new Folder();
        }
        
        return folder;
    }
    
    @RemoteAction
    public static Document getDocument(string name) {
        Document document = null;
    
        try {
            document = [SELECT Id, Name FROM Document WHERE Name = :name];
        }
        catch (Exception ex) {
            return new Document();
        }
        
        return document;
    }
    
    @RemoteAction
    public static DocumentWrapper getDocumentWrapper(string name) {
        DocumentWrapper doc = new DocumentWrapper();
        Document root = null;
        Document zip = null;
        
        try {
            root = [SELECT Id FROM Document WHERE Name = :name];
        }
        catch (Exception ex) {
            return new DocumentWrapper();
        }
        
        doc.RootId = root.Id;
        
        try {
            zip = [SELECT Id FROM Document WHERE Name = :name + ' ZIP'];
        }
        catch (Exception ex) {
            return new DocumentWrapper();
        }
        
        doc.ZipId = zip.Id;
        
        system.debug('ROOT ID: ' + doc.RootId + ', ZIP ID: ' + doc.ZipId);
        
        return doc;
    }
    
    public boolean getisPM() {
        system.debug('USERID: ' + userid);
        
        User user = [SELECT Id, Franchise__c FROM User WHERE Id = :userid];
        
        system.debug('FRANCHISE: ' + user.Franchise__c);
        
        if (user.Franchise__c == 'Patient Monitoring') {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean getisRS() {
        system.debug('USERID: ' + userid);
        
        User user = [SELECT Id, Franchise__c FROM User WHERE Id = :userid];
        
        system.debug('FRANCHISE: ' + user.Franchise__c);
        
        if (user.Franchise__c == 'Respiratory Solutions') {
            return true;
        }
        else {
            return false;
        }
    }
}