public class ManageReportOfInActiveUserC {
/****************************************************************************************
    * Name    : ManageReportOfInActiveUserCTest
    * Author  : Brajmohan Sharma
    * Date    : 22-Mar-2017
    * Purpose :
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
****************************************************************************************/ 
      //check job report is present or not
  public static Boolean checkReportAvail(String uid){
       List<CronTrigger> listcount = [SELECT Id FROM CronTrigger WHERE CronJobDetail.JobType = '8' AND OwnerId =: uid];
       System.debug('User that have run report'+listcount.size());
    if(listcount.size()>0)
       return true;
       return false;
       }
       //check inactive user who have run report jobs
       Public static List<User> getUser(){
       System.debug('counting run report for inactive user');
       return [Select id from user where ID IN (SELECT OwnerId FROM CronTrigger WHERE CronJobDetail.JobType = '8') AND (isActive=False)];
       }
      //fetch list of run report  jobs Of given user
       public static List<CronTrigger> getJobs(String User_id){
       System.debug('fetching list of run report for inactive user');
       return [SELECT Id, CronJobDetail.Id, CronJobDetail.Name,CronExpression, CronJobDetail.JobType,EndTime,NextFireTime,PreviousFireTime,StartTime,State FROM CronTrigger where OwnerId =: User_id AND CronJobDetail.JobType = '8'];
       }
     //care about unsheduled  run report jobs 
  public static void unSheduleReport(String idd){
       List<CronTrigger> jobu = ManageReportOfInActiveUserC.getJobs(idd);
       System.debug('inside unsheduling report');
       List<User> Name = [select name from user where id =:idd];
       List<SheduleRunReport__c> jobs1 = [select RUserId__c,Name,CreatedUserName__c,EndDate__c,StartDate__c,CronExpression__c,NextFireDate__c from SheduleRunReport__c where RUserId__c =: Name.get(0).name];
       if(jobs1.size() >= jobu.size())
    for(CronTrigger ct :jobu)
       system.abortJob(ct.id);
       system.debug('jobs has unsheduled'); 
       }
      //to save shedule report job parameter
    @future(callout = true)
  public static void sdlJobInsert(String id){
    try{
        List<SheduleRunReport__c> srrlist = new List<SheduleRunReport__c>();
        List<Report> reportlist = new List<Report>();
        List<user> createdlist = new List<User>();
        system.debug('control inside sdlJobInsert');
        system.debug('control inside sdlJobInsert useridlist value'+id);
        List<CronTrigger> jbs = ManageReportOfInActiveUserC.getJobs(id);
        system.debug('control inside sdlJobInsert jbs size '+jbs.size());
    for(CronTrigger ct : jbs){
        system.debug('control inside sdlJobInsert ct.CronJobDetail.Name '+ct.CronJobDetail.Name);
        List<Report> repot = [select DeveloperName,CreatedById, CreatedBy.Name FROM Report where ID =:ct.CronJobDetail.Name];
        system.debug('control inside sdlJobInsert repot'+repot.size());
        system.debug('control inside sdlJobInsert name1'+repot.get(0).CreatedBy.Name);
        SheduleRunReport__c srr = new SheduleRunReport__c();   //persist data to schedule report
        List<User> userlist = [select Name FROM User where ID =:id];
        srr.RUserId__c = userlist.get(0).name;
        srr.Name = repot.get(0).DeveloperName;
        srr.CreatedUserName__c = repot.get(0).CreatedBy.Name;   
        srr.EndDate__c = ct.EndTime;
        srr.StartDate__c = ct.StartTime;
        srr.CronExpression__c = ct.CronExpression;
        srr.NextFireDate__c = ct.NextFireTime;  
        srrlist.add(srr);
      }
        system.debug('control inside sdlJobInsert srrlist'+srrlist.size());       
    if(srrlist.size()>0)     //check to schedule list have reocrd or not
       insert srrlist;          //dml operation to persist record with schedule report object 
    }catch(Exception e){
       system.debug('issue in scheduled job');
     }//catch
    finally{
            ManageReportOfInActiveUserC.unSheduleReport(id);
     }
     }
       //Reminder to admin team to shedule report again as user has activate
    @future(callout = true)
   public static void SendMailToSheduleReport(String uidd){
       system.debug('inside mail method');
       List<User> Name = [select name from user where id =: uidd];
       System.debug('inside seding mail to user'+Name.size());
       System.debug('inside seding mail to user'+Name.get(0).name);
       List<SheduleRunReport__c> jobs = [select RUserId__c,Name,CreatedUserName__c,EndDate__c,StartDate__c,CronExpression__c,NextFireDate__c from SheduleRunReport__c where RUserId__c =: Name.get(0).name];
    for(SheduleRunReport__c sc : jobs){
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
       String[] toAddresses = new String[] {'brajmohan.sharma@cognizant.com'};  
       mail.setToAddresses(toAddresses);  
       mail.setSubject('Please Schedule Report for '+sc.RUserId__c+' Running User');  
       mail.setPlainTextBody('Report Name '+sc.Name+'\n Created By '+sc.CreatedUserName__c+'\n StartDateTime'+sc.StartDate__c+'\n EndDateTime '+sc.EndDate__c+'\n CronExpression '+sc.CronExpression__c+'\n NextFireTime '+sc.NextFireDate__c);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       delete sc;
      }// iterate loop for all job
   }//SendMailToSheduleReport    
}//ManageReportOfInActiveUserC