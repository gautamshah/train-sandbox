/*
CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-09-17      Bryan Fry           Created.
2016-02-29      Isaac Lewis         Pointed to Line Item instead of Agreement Line Item to make edits.
2016-03-07      Isaac Lewis         Update ALI on saves from VF page (Only updates from Line Item after first save)
2016-03-09      Isaac Lewis         Update query to check for finalized or saved configurations (necessary for new UI)
===============================================================================
*/

public class CPQ_SSG_Controller_ScrubPOLots {

    public Apttus__APTS_Agreement__c agreement {get;set;}
    public List<Apttus_Config2__LineItem__c> lineItems {get;set;}

    public CPQ_SSG_Controller_ScrubPOLots(ApexPages.StandardController controller) {
        agreement = (Apttus__APTS_Agreement__c)controller.getRecord();
        lineItems = [
            SELECT Id, Lot_Number__c, Apttus_Config2__ProductId__r.Name, Apttus_Config2__ProductId__r.ProductCode, Apttus_Config2__Quantity__c, 
            (SELECT Id, Lot_Number__c FROM Apttus_CMConfig__DerivedAgreementLineItems__r) 
            FROM Apttus_Config2__LineItem__c 
            WHERE ( Apttus_Config2__ConfigurationId__r.Apttus_Config2__Status__c = 'Finalized' OR (Apttus_Config2__ConfigurationId__r.Apttus_Config2__Status__c = 'Saved' AND Apttus_Config2__ConfigurationId__r.Apttus_Config2__VersionNumber__c = 1) ) 
            AND Apttus_Config2__ConfigurationId__r.Apttus_CMConfig__AgreementId__c = :agreement.Id 
            ORDER BY Apttus_Config2__ProductId__r.Name
        ];
    }

    public PageReference doSaveLotNumbers() {
        
        List<Apttus__AgreementLineItem__c> alis = new List<Apttus__AgreementLineItem__c>();
        
        for(Apttus_Config2__LineItem__c li : lineItems){
            for(Apttus__AgreementLineItem__c ali : li.Apttus_CMConfig__DerivedAgreementLineItems__r){
                ali.Lot_Number__c = li.Lot_Number__c;
                alis.add(ali);
            }
        }

        update lineItems;
        update alis;
        return new PageReference('/' + agreement.Id);

    }

    public PageReference doCancel() {
        return new PageReference('/' + agreement.Id);
    }
    
}