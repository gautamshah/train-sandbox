@isTest
private class MasterContactSearchController_TestClass {


   static testMethod void TestPageFunctionality() {
   
        // Setup Variables
        SObject parent;
        RecordType rt = [SELECT Id FROM RecordType where SobjectType = 'Contact' and DeveloperName = 'Affiliated_Contact_US' LIMIT 1];  
        RecordType Master = [SELECT Id FROM RecordType where SobjectType = 'Contact' and RecordType.DeveloperName = 'Master_Contact' LIMIT 1];
        RecordType ct = [SELECT Id FROM RecordType where SobjectType = 'Account' AND RecordType.DeveloperName = 'Country_Accounts' LIMIT 1];
        RecordType hf = [SELECT Id FROM RecordType where SobjectType = 'Account' AND RecordType.DeveloperName = 'US_Account_Hospital' LIMIT 1];
        
        Account act = new Account(RecordTypeId = hf.id, 
                                  name='Test');
       
        insert act;
       
       // Create Country Account
        Account CountryAccount2 = new Account (RecordTypeId = ct.id,
                                              name='US-Account');
        
        insert CountryAccount2;
       
        string CountryAccount = [select ID from Account where Name = 'US-Account' LIMIT 1].ID;
        
       Contact mcon = new Contact();
       mcon.RecordTypeId = Master.id;
       mcon.Salutation = 'Mr.';
       mcon.FirstName = 'Bob';
       mcon.LastName = 'Smith';
       mcon.Email = 'Smitty@a.com';
       mcon.Created_via_Salesforce1__c = true;
       mcon.Phone_Number_At_Account__c = '334234234234';
       mcon.AccountId = CountryAccount;
       
       Contact con = new Contact();
      con.RecordTypeId = rt.id;  
      con.Salutation = 'Mr.';               
      con.FirstName = 'Bobby';
      con.LastName = 'Smitty';
      con.Email = 'bbsmit@aaa.com';
      con.Phone_Number_At_Account__c = '23423423423';
      con.Specialty1__c = 'Allergy';
      con.Department_picklist__c = 'ICU';
      con.Other_Department__c = null;
      con.Affiliated_Role__c = 'Analyst';
      con.Title = null;
      con.Created_via_Salesforce1__c = true;
      con.AccountId = act.id;
      con.Master_Contact_Record__c = mcon.id;
       
        // Instantiate the standard controller
        Apexpages.StandardController sc = new Apexpages.standardController(act);

 
        // Instantiate the extension
        MasterContactSearch_Controller ext = new MasterContactSearch_Controller(sc);
       
       // Setup Data
        Ext.ReturnAccount = act.Id;
        Ext.searchstring = 'BobTest9999';
        Ext.IndexNum = 0;
        Ext.M_Salutation = 'Mr.';
        Ext.M_FirstName = 'Bob';
        Ext.M_LastName = 'Smith';
        Ext.M_Email = 'bsmith99@abc.com';
        Ext.M_Phone =   '9998883333';
        Ext.M_Specialty = 'Allergy';
        Ext.M_Department = 'ICU';
        Ext.M_ConnectedAs =  'Analyst';
        Ext.CountryAccount = CountryAccount;
        Ext.RecName = 'Affiliated_Contact_US';
        Ext.RecName2 = 'In_Process_Connected_Contact';
        Ext.searchstring2 = '';
        Ext.searchstring3 = '';
        Ext.SelectedGUID = con.id;
       
       
       
       
       
       //Test converage for the MasterContactSearchvisualforce page
       PageReference pageRef = Page.MasterContactSearch;
       pageRef.getParameters().put('id', String.valueOf(act.Id));
       Test.setCurrentPage(pageRef);
       PageReference pageRef2 = Page.MasterContactSearchLookup;
       Test.setCurrentPage(pageRef2);
       
       
        //Execute Methods
        

        ext.NoResultsMatch();
        ext.getDepartmentList();
        ext.getUniqueDepartment();
        ext.getConnectedAsList();
        ext.getUniqueConnectedList();
        ext.getSpecialty();
        ext.getSalutation();
        ext.OtherDeptCheck();
        ext.OtherConnectedAsCheck();
        ext.RefreshForm();
        ext.ReviseSearch();
        ext.SearchMain();
        
        
        ext.searchstring = 'aaddddddd';
        ext.SearchMain();
        

        ext.M_Department = 'Other';
        ext.M_ConnectedAs =  'Other';
        ext.OtherDeptCheck();
        ext.OtherConnectedAsCheck();
        
        
        
        ext.CreateMasteAndConnected();
   
   }
     
}