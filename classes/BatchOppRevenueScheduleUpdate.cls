/****************************************************************************************
* Name    : Class: BatchOppRevenueScheduleUpdate -> Batchable
* Author  : Gautam Shah
* Date    : 4/5/2014
* Purpose : Determines which existing Opportunity Product Schedules need to be re-established.
* 
* Dependancies: 
* 	Class: BatchOLIS_Update -> 	Should be run and completed before this class is run
*							->	This class may need to be invoked several times because the AggregateResult query must have an explicit limit defined
*	Class: Opportunity_Main	->	Performs the work of re-establishing the product schedules
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
public class BatchOppRevenueScheduleUpdate implements Database.Batchable<Opportunity> 
{
	public Iterable<Opportunity> start(Database.BatchableContext bc)
	{
		return new OpportunityIterable();
	}
	public void execute(Database.BatchableContext bc, List<Opportunity> scope)
	{
		Opportunity_Main.reEstablishSchedule(scope);
	}
	public void finish(Database.BatchableContext bc){}
	
	public class OpportunityIterable implements Iterable<Opportunity>
	{
		public Iterator<Opportunity> iterator()
		{
			return new OpportunityIterator();
		}
	}
	public class OpportunityIterator implements Iterator<Opportunity>
	{
		List<Opportunity> oppList {get;set;}
		Integer index {get;set;}
		
		public OpportunityIterator()
		{
			index = 0;
			oppList = new List<Opportunity>();
			List<Id> queriedOpps = new List<Id>();
			List<Id> oppsToFix = new List<Id>();
			Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>();
			for(AggregateResult ar : [Select MIN(ScheduleDate) minDate, OpportunityLineItem.OpportunityId oppID, Description From OpportunityLineItemSchedule Where Description Like 'CloseDate:%' Group By OpportunityLineItem.OpportunityId, Description Limit 200])
			{
				String strCloseDate = (String)ar.get('Description');
				Date oppCloseDate = Date.valueOf(strCloseDate.substring(10));
				System.debug('OppID: ' + (String)ar.get('oppID') + ', CloseDate: ' + oppCloseDate + ', MinDate: ' + ar.get('minDate'));
				if(oppCloseDate != ar.get('minDate'))
				{
					System.debug('CloseDate != minDate for OppId: ' + (String)ar.get('oppID'));
					oppsToFix.add((Id)ar.get('oppID'));
				}
				queriedOpps.add((Id)ar.get('oppID'));
			}
			//clear out the description for records we've already processed.
			List<OpportunityLineItemSchedule> olisList = new List<OpportunityLineItemSchedule>([Select Id, Description From OpportunityLineItemSchedule Where OpportunityLineItem.OpportunityId in :queriedOpps Limit 10000]);
			for(OpportunityLineItemSchedule olis : olisList)
			{
				olis.Description = '';
			}
			List<Database.SaveResult> srList = Database.update(olisList, false);
			for(Database.SaveResult sr : srList)
			{
				if(!sr.isSuccess())
				{
					System.debug('Error in clearing OLIS.Description field');
					for(Database.Error e : sr.getErrors())
					{
						System.debug('OLIS ID: ' + sr.getId() + '  -->  ' + e.getMessage());
					}
				}
			}
			//fetch the opportunities to fix
			oppList = [Select Id, CloseDate From Opportunity Where Id in :oppsToFix];
		}
		public boolean hasNext()
		{
			return oppList != null && !oppList.isEmpty() && index < oppList.size();
		}
		public Opportunity next()
		{
			return oppList[index++];
		}
	}
}