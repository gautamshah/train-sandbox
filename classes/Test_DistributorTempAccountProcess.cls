/**
 * Name    : Test_DistributorTempAccountProcess
 * Author  : Gautam Shah
 * Date    : October 21, 2013
 * Purpose : Provides test coverage for the distributor portal user account creation process which involves Account and Case objects 
 * Apex Tested: 
 *              Account_TriggerHandler.cls
 *              Case_UpdateSalesOutAccountIDs.trigger
 *              SendDistributorEmail.cls
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 7/1/14       Gautam Shah         Modified to correct error
 */
@isTest (SeeAllData=true)
private class Test_DistributorTempAccountProcess {

    static testMethod void myUnitTest() {
        Test.startTest();
        //User runningUser = [select Id From User Where UserType = 'PowerPartner' and IsActive = true Limit 1];
        RecordType distAcctRT = [Select Id From RecordType Where Name = 'ASIA-Distributor' Limit 1];
        RecordType crt=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        Account distAcct = new Account();
        distAcct.Name = 'dist account';
        distAcct.RecordTypeId = distAcctRT.Id;
        distAcct.BillingStreet = '1 Main';
        distAcct.BillingCity = 'Seoul';
        distAcct.BillingCountry = 'US';
        distAcct.BillingState = 'MA';
        distAcct.BillingPostalCode = '12345';
        insert distAcct;
        Contact portalContact = new Contact();
        portalContact.FirstName = 'Tester';
        portalContact.LastName = 'Tester';
        portalContact.AccountId = distAcct.Id;
        portalContact.MailingStreet = '1 Main';
        portalContact.MailingCity = 'Seoul';
        portalContact.MailingState = 'MA';
        portalContact.MailingCountry = 'US';
        portalContact.MailingPostalCode = '12345';
        portalContact.RecordTypeID = crt.id;
        portalContact.Type__c = 'DIS Contact';
        insert portalContact;
        Profile asiaDist = [Select Id From Profile Where Name = 'Asia Distributor - KR' Limit 1];
        //UserRole asiaRole = [Select Id From UserRole Where Name = 'China' Limit 1];
        User runningUser = new User();
        runningUser.FirstName = 'Tester';
        runningUser.LastName = 'Tester';
        runningUser.Username = 'tester20131025@test.com';
        runningUser.Email = 'tester@test.com';
        runningUser.Alias = 'Tester';
        runningUser.CommunityNickname = 'Tester';
        runningUser.TimeZoneSidKey = 'America/New_York';
        runningUser.EmailEncodingKey = 'UTF-8';
        runningUser.LocaleSidKey = 'en_US';
        runningUser.LanguageLocaleKey = 'en_US';
        runningUser.ProfileId = asiaDist.Id;
        runningUser.Asia_Team_Asia_use_only__c = 'PRM Team';
        runningUser.ContactId = portalContact.Id;
        //runningUser.UserRoleId = asiaRole.Id;
        insert runningUser;
        Account a = new Account();
        System.runAs(runningUser)
        {
            a.Name = 'Test Account';
            a.BillingCountry = 'US';
            a.BillingState = 'test';
            a.BillingCity = 'test';
            a.BillingPostalCode = 'test';
            a.BillingStreet = 'test';
            insert a;
            Cycle_Period__c cp = new Cycle_Period__c();
            cp.Distributor_Name__c = a.Id;
            insert cp;
            Sales_Out__c so = new Sales_Out__c();
            so.Sell_To__c = 'TEMPUS-' + a.Account_Auto_Number__c;
            so.Cycle_Period__c = cp.Id;
            insert so;
        }
        List<Case> newCase = new List<Case>([Select Id From Case Where CreatedById = :runningUser.Id Order By CreatedDate DESC Limit 1]);
        if(newCase.size() > 0)
        {
            newCase[0].Status = 'Closed - Resolved';
            newCase[0].Master_Account_Number__c = a.Account_External_ID__c;
            update newCase;
        }
        Test.stopTest();
    }
}