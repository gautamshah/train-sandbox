public class cpqTask_t
{
    //to prevent a method from executing a second time add the name of the method to the 'executedMethods' set
    public static Set<String> executedMethods = new Set<String>();

    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<Task> newList,
            Map<Id, Task> newMap,
            List<Task> oldList,
            Map<Id, Task> oldMap,
            integer size
        )
    {
        if(!executedMethods.contains('capture'))
        {
            new cpqCycleTime_Task_u().capture(
                isExecuting,
                isInsert,
                isUpdate,
                isDelete,
                isBefore,
                isAfter,
                isUndelete,
                (List<sObject>)newList,
                (Map<Id, sObject>)newMap,
                (List<sObject>)oldList,
                (Map<Id, sObject>)oldMap,
                size
            );
			executedMethods.add('capture');
        }
    }
}