/**
Mock HTTP Callout response to allow testing CPQ_SSG_BatchUpdateProductImages
and CPQ_SSG_ScheduleUpdateProductImages.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
04/18/2016      Bryan Fry           Created
===============================================================================
*/
@isTest
global class Test_CPQ_HTTPCalloutMock implements HttpCalloutMock {
	public static final String IMAGE_URL = 'http://www.covidien.com/imageServer.aspx/';

	global HTTPResponse respond(HttpRequest req) {
		HTTPResponse res = new HTTPResponse();
		if (req.getEndpoint().startsWith(IMAGE_URL)) {
			res.setHeader('Content-Type','image/jpeg');
			res.setBodyAsBlob(Blob.valueOf('12345'));
			res.setStatusCode(200);
		}
		return res;
	}
}