/**

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016/10/21      Paul Berglund       Created.
2016/10/25      Isaac Lewis         Updated references from OrganizationNames_u
                                    to OrganizationNames_g
===============================================================================
*/
@isTest
public class cpqPriceList_cTest
{
	static cpqPriceList_g cache = new cpqPriceList_g();
	
	@isTest
	static void MapInitialization()
	{
		cache.dump('MapInitialization - start');

		for(string pln : cpqPriceList_c.PriceListName2VariableName.keySet())
			system.assertEquals(
				cpqPriceList_c.VariableName2PriceListName.keySet().contains(cpqPriceList_c.PriceListName2VariableName.get(pln)),
				true, 'Values should exist in both locations');
			
		for(string pln : cpqPriceList_c.PriceListName2OrganizationName.keySet())
			system.assertEquals(
				cpqPriceList_c.OrganizationName2PriceListName.keySet().contains(cpqPriceList_c.PriceListName2OrganizationName.get(pln)),
				true, 'Values should exist in both locations');

		cache.dump('MapInitialization - finish');
	}	

	@isTest
	static void create()
	{
		cache.dump('create - start');

		insert cpqPriceList_c.createMasters();
		
		system.assertNotEquals(null, cpqPriceList_c.RMS);
		system.assertNotEquals(null, cpqPriceList_c.SSG);
		system.assertNotEquals(null, cpqPriceList_c.MS);

		cache.dump('create - finish');
	}
	
	@isTest
	static void reload()
	{
		cache.dump('reload - start');

		cpqPriceList_c.load();

		assert();

		cache.dump('reload - finish');
	}

	static void assert()
	{
		Map<Id, Apttus_Config2__PriceList__c> priceLists =
			new Map<Id, Apttus_Config2__PriceList__c>
				(
					[SELECT Id
					 FROM Apttus_Config2__PriceList__c]
				);
				
		system.assertEquals(priceLists.size(), OrganizationNames_g.Abbreviations.values().size());
	}
}