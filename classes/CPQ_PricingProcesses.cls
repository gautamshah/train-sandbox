/**
Handler class for CPQ_PricingCallBack

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-12-13      Yuli Fintescu		Created
2016-12-15		Bryan Fry			AV-238 - HPG GPO accounts should use the pricing web service to override
									results of price matrices. Move logic for choosing whether to use pricing
									web service into method usePricingWebService().
===============================================================================
*/
public with sharing class CPQ_PricingProcesses {
	public static final Set<String> HPG_GPO_NAMES = new Set<String> {'HEALTHTRUST PURCHASING GROUP',
																	 'HEALTHTRUST HPG',
																	 'HPG LEGACY'};

	// Determine if a price list item for a given configuration should use the pricing web service 
	public static Boolean usePricingWebService(Apttus_Config2__ProductConfiguration__c config, Apttus_Config2__PriceListItem__c pli) {
		// Use GPO from proposal account or agreement account
		String GPOName = config.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__r.Name;
		if (GPOName == null) {
			GPOName = config.Apttus_CMConfig__AgreementId__r.Apttus__Account__r.Primary_GPO_Buying_Group_Affiliation__r.Name;
		}
		if (GPOName == null) {
			GPOName = '';
		}

        if (pli.Apttus_Config2__PriceMatrices__r.size() == 0 || 
        	config.Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName =='Scrub_PO' ||
        	config.Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName =='Custom_Kit' ||
        	config.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName =='Hardware_or_Product_Quote' ||
        	config.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName =='Agreement_Proposal' ||
        	config.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName =='Rentals' ||
        	config.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName =='Royalty_Free_Agreement' ||
        	HPG_GPO_NAMES.contains(GPOName))
        {
        	return true;
    	} else {
    		return false;
    	}
	}

/*============================
	Prepare data for call back
==============================*/	
	public static void prepareCallBack(ID ConfigID, 
								Map<String, Apttus_Config2__PriceListItem__c> priceListItems,
								Map<String, CPQ_Customer_Specific_Pricing__c> csps,
								List<CPQ_Error_Log__c> errors) {
		System.debug('**** prepareCallBack');
		
		String rmsPriceListID = CPQ_Utilities.getRMSPriceListID();
		String ssgPriceListID = CPQ_Utilities.getSSGPriceListID();
		if (String.isEmpty(rmsPriceListID) && String.isEmpty(ssgPriceListID))
			return;
		
		csps.clear();
		errors.clear();
if (UserInfo.getLastName() == 'Fry') {
	errors.add(new CPQ_Error_Log__c(Log_Type__c = 'Audit', Operation__c = 'CPQ_PricingCallback 2 1'));
}
    //get related config
    //====================
        Apttus_Config2__ProductConfiguration__c config;
        List<Apttus_Config2__ProductConfiguration__c> configs = [Select ID, Name,
        	Apttus_Config2__AccountId__c, 
        	Apttus_Config2__AccountId__r.ID,
        	Apttus_Config2__AccountId__r.Account_External_ID__c, 
        	Apttus_QPConfig__Proposald__c, 
        	Apttus_QPConfig__Proposald__r.Name,
        	Apttus_QPConfig__Proposald__r.RecordType.DeveloperName,
        	Apttus_CMConfig__AgreementId__c,
        	Apttus_CMConfig__AgreementId__r.Name,
        	Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName,
        	Apttus_Config2__PriceListId__c,
        	Apttus_Config2__PriceListId__r.Apttus_Config2__Type__c,
        	Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__c,
        	Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__r.Account_External_ID__c,
        	Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__r.Name,
        	Apttus_CMConfig__AgreementId__r.Apttus__Account__c,
        	Apttus_CMConfig__AgreementId__r.Apttus__Account__r.Account_External_ID__c,
        	Apttus_CMConfig__AgreementId__r.Apttus__Account__r.Primary_GPO_Buying_Group_Affiliation__r.Name,
        	(Select ID, Name, Product_ID__c, Apttus_Config2__ProductId__c, Price_Mode__c, Last_Price_Mode__c From Apttus_Config2__LineItems__r) 
        From Apttus_Config2__ProductConfiguration__c
        Where ID =: ConfigID and 
            //(Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__r.Account_External_ID__c like 'US-%' or
            // Apttus_CMConfig__AgreementId__r.Apttus__Account__r.Account_External_ID__c like 'US-%') and
            (Apttus_Config2__PriceListId__c =: rmsPriceListID or Apttus_Config2__PriceListId__c =: ssgPriceListID)];
        System.debug('*** configs ' + configs);
        
        if (configs.size() == 0) 
        	return;
		
        config = configs[0];
        
    	Account account = config.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Account__r;//we are not using config account. it is not realiable.
    	if (account == null)
    		account = config.Apttus_CMConfig__AgreementId__r.Apttus__Account__r;
		System.debug('*** account ' + account);
        
    //get all products in the config, including options
    //=================================================
        Set<ID> prodIds = new Set<ID>();
        for (Apttus_Config2__LineItem__c l : config.Apttus_Config2__LineItems__r) {
	        String product_option = l.Product_ID__c;
	        if (product_option != null)
	            prodIds.add(product_option);
        }
        System.debug('*** prodIds ' + prodIds);
if (UserInfo.getLastName() == 'Fry') {
	errors.add(new CPQ_Error_Log__c(Log_Type__c = 'Audit', Operation__c = 'CPQ_PricingCallback 2 2'));
}
    //get price list items
    //====================
        for (Apttus_Config2__PriceListItem__c pli : [Select ID, 
		            Apttus_Config2__ListPrice__c,
		            Apttus_Config2__Cost__c,
		            Apttus_Config2__ProductId__c, 
		            Apttus_Config2__ProductId__r.ProductCode, 
		            Apttus_Config2__ProductId__r.UOM_Desc__c, 
		            Apttus_Config2__ProductId__r.Rep_Floor_Price__c, 
		            Apttus_Config2__ProductId__r.RM_Floor_Price__c, 
		            Apttus_Config2__ProductId__r.Zone_VP_Floor_Price__c, 
		            Apttus_Config2__ProductId__r.Apttus_Config2__HasOptions__c, 
		            Apttus_Config2__PriceListId__c,
		            Apttus_Config2__ChargeType__c,
		            Apttus_Config2__PriceType__c,
		            Apttus_Config2__PriceMethod__c ,
		            Apttus_Config2__PriceUom__c,
		            (Select Id From Apttus_Config2__PriceMatrices__r)
		        From Apttus_Config2__PriceListItem__c 
		        Where Apttus_Config2__ProductId__c in :prodIds and 
		    		Apttus_Config2__ProductId__r.ProductCode != null and
		    		Apttus_Config2__ProductId__r.ProductCode != '0' and
		    		Apttus_Config2__PriceType__c = 'One Time' and
		            Apttus_Config2__PriceListId__c = :config.Apttus_Config2__PriceListId__c and
		            Apttus_Config2__ChargeType__c Not in: CPQ_Utilities.getCallbackChargeTypeIgnoreList()]) {
            
            if (usePricingWebService(config, pli)) {
            	priceListItems.put(pli.Apttus_Config2__ProductId__c, pli);
            }
        }
        System.debug('*** priceListItems ' + priceListItems);
		
		if (account == null || String.isEmpty(account.ID) || String.isEmpty(account.Account_External_ID__c) || !account.Account_External_ID__c.startsWith('US-')) {
			System.debug('*** invalid account or account CRN, skip multi-price WS.');
			return;
		}

if (UserInfo.getLastName() == 'Fry') {
	errors.add(new CPQ_Error_Log__c(Log_Type__c = 'Audit', Operation__c = 'CPQ_PricingCallback 2 3'));
}
		
    //get commitment type codes
    //===========================
    	Map<String, Commitment_Type__c> commTypes = new Map<String, Commitment_Type__c>();
		for (Commitment_Type__c c : [Select Name, ID From Commitment_Type__c]) {
			commTypes.put(c.Name.toLowerCase(), c);
		}
		System.debug('*** commTypes ' + commTypes);
		
    //get line items that need to call ws to get contract price
    //=========================================================
    	for (CPQ_Customer_Specific_Pricing__c csp : [Select Root_Contract__c, 
					Product__c, 
					Product__r.ProductCode,
					Product__r.UOM_Desc__c,
					Non_Committed_Price__c, 
					Name, Id, 
					GPO_Price__c, 
					Expiration_Date__c, 
					Effective_Date__c, 
					Contract_Price__c, 
					Commitment_Type__c, 
					Account__c,
					LastRetrievedDateTime__c 
				From CPQ_Customer_Specific_Pricing__c 
				Where Account__c =: account.ID and 
					Product__c in: priceListItems.keySet() and
					LastRetrievedDateTime__c >=: System.Today().addDays(-1)]) {
			csps.put(csp.Product__c, csp);
		}
    	System.debug('*** csps ' + csps);
    	
    	Map<String, Product2> skusToWS = new Map<String, Product2>();
    	for (String key : priceListItems.keySet()) {
    		Apttus_Config2__PriceListItem__c pli = priceListItems.get(key);
    		
    		if (!csps.containsKey(key)) {
    			skusToWS.put(pli.Apttus_Config2__ProductId__r.ProductCode, pli.Apttus_Config2__ProductId__r);
    		}
    	}
    	System.debug('*** skusToWS ' + skusToWS);

if (UserInfo.getLastName() == 'Fry') {
	errors.add(new CPQ_Error_Log__c(Log_Type__c = 'Audit', Operation__c = 'CPQ_PricingCallback 2 4'));
}

    //get customer specific price
    //===========================
    	//invoke customer specific pricing ws 
	    if (skusToWS.size() > 0) {
			InvokeCustomerSpecificPricingWS(skusToWS, commTypes, account, csps, errors, config);
		}

if (UserInfo.getLastName() == 'Fry') {
	errors.add(new CPQ_Error_Log__c(Log_Type__c = 'Audit', Operation__c = 'CPQ_PricingCallback 2 5'));
}
		
		return;
	}
	
	public static void SetFloorPrice(Apttus_Config2__LineItem__c line, Decimal price) {
        Integer quantity = Integer.valueOf(line.Apttus_Config2__Quantity__c == null ? 0 : line.Apttus_Config2__Quantity__c);
		
		line.Apttus_Config2__AdjustmentType__c = 'Base Price Override';
		line.Apttus_Config2__BasePriceOverride__c = price;		
		line.Apttus_Config2__BaseExtendedPrice__c = line.Apttus_Config2__BasePriceOverride__c * quantity;
		line.Apttus_Config2__ExtendedPrice__c = line.Apttus_Config2__BaseExtendedPrice__c;
		line.Last_Price_Mode__c = line.Price_Mode__c;
		
		line.Apttus_Config2__AdjustmentAmount__c = line.Apttus_Config2__BasePriceOverride__c;
		line.Apttus_Config2__AdjustedPrice__c = line.Apttus_Config2__AdjustmentAmount__c * quantity;
		line.Apttus_Config2__NetPrice__c = line.Apttus_Config2__AdjustedPrice__c;
		line.Apttus_Config2__NetAdjustmentPercent__c = 0;
	}
	
	public static void ResetBasePrice(Apttus_Config2__LineItem__c line) {
        Integer quantity = Integer.valueOf(line.Apttus_Config2__Quantity__c == null ? 0 : line.Apttus_Config2__Quantity__c);
		
		line.Apttus_Config2__AdjustmentType__c = null;
		line.Apttus_Config2__BasePriceOverride__c = null;		
		line.Apttus_Config2__BaseExtendedPrice__c = line.Apttus_Config2__BasePrice__c * quantity;
		line.Apttus_Config2__ExtendedPrice__c = line.Apttus_Config2__BaseExtendedPrice__c;
		line.Last_Price_Mode__c = null;
		
		line.Apttus_Config2__AdjustmentAmount__c = null;
		line.Apttus_Config2__AdjustedPrice__c = line.Apttus_Config2__BasePrice__c * quantity;
		line.Apttus_Config2__NetPrice__c = line.Apttus_Config2__AdjustedPrice__c;
		line.Apttus_Config2__NetAdjustmentPercent__c = 0;
	}
	
/*===================================
	Customer Specific Pricing WS Call
=====================================*/	
	private static void InvokeCustomerSpecificPricingWS(Map<String, Product2> skuToProducts, 
								Map<String, Commitment_Type__c> commTypes,
								Account account,
								Map<String, CPQ_Customer_Specific_Pricing__c> csps,
								List<CPQ_Error_Log__c> errors,
								Apttus_Config2__ProductConfiguration__c config) {
		System.debug('**** InvokeCustomerSpecificPricingWS');
		
		if (skuToProducts.size() == 0)
			return;
		
		CPQ_ProxyMultiPriceService.PricingRequestType request;
        CPQ_ProxyMultiPriceService.PricingRespType response;
		try {
        	Integer requestId = Math.abs(Integer.valueOF((System.now().getTime() / 32767)));
        	
			String accountCRN = account.Account_External_ID__c.substring(3);
			
        	CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
	        service.timeout_x = 120000;
	        
	        request = new CPQ_ProxyMultiPriceService.PricingRequestType();
	        //request.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
	        request.countryField = 'US';
	        request.currencyField = '';
	        request.designatorField = 'D';
	        request.gBUField = 'All';
	        request.requestDateField = System.Today();
	        request.requestDateFieldSpecified = true;
	        request.requestIDField = String.valueOf(requestId);
	        request.showOnePriceField = 'One';
	        String u = userInfo.getUserName();
	        request.userIDField = u.substring(0, u.indexOf('@'));
	        
	        request.shipToDSField = new CPQ_ProxyMultiPriceService.ArrayOfShipTo();
		   	request.shipToDSField.ShipTo = new CPQ_ProxyMultiPriceService.ShipTo[]{};
		   	
			CPQ_ProxyMultiPriceService.ShipTo Ship_To = new CPQ_ProxyMultiPriceService.ShipTo();
			//Ship_To.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
        	Ship_To.shipTo1Field = accountCRN;
			request.shipToDSField.ShipTo.add(Ship_To);
			
	        request.sKUsField = new CPQ_ProxyMultiPriceService.ArrayOfSKUType();
	        request.sKUsField.SKUType = new CPQ_ProxyMultiPriceService.SKUType[]{};

   		 	List<String> codes = new List<String>(skuToProducts.keySet());
   			Integer size = codes.size();
	        for(integer i = 0; i < size; i++) {
	        	String key = codes[i];
	            Product2 prd = skuToProducts.get(key);
	            
	            CPQ_ProxyMultiPriceService.SKUType t = new CPQ_ProxyMultiPriceService.SKUType();
        		//t.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
	            t.quantityField = 1;
		        t.quantityFieldSpecified = true;
	            t.sKUCodeField = key;
	            t.uOMField = prd.UOM_Desc__c == null ? 'EA' : prd.UOM_Desc__c; //prod.UOM__c;
				
	            request.sKUsField.SKUType.add(t);
	        }
			
			System.Debug('*** request ' + request);
			
			if (Test.isRunningTest()) {
	    		response = Test_CPQ_PricingCallBack.createModkResponse();
			} else {
	        	response = service.MultiPrice(request);
			}
			
			System.Debug('*** response.PropertyChanged ' + response.PropertyChanged);
			System.Debug('*** response.countryField ' + response.countryField);
			System.Debug('*** response.currencyField ' + response.currencyField);
			System.Debug('*** response.designatorField ' + response.designatorField);
			System.Debug('*** response.gBUField ' + response.gBUField);
			System.Debug('*** response.requestDateField ' + response.requestDateField);
			System.Debug('*** response.requestDateFieldSpecified ' + response.requestDateFieldSpecified);
			System.Debug('*** response.requestErrorDescriptionField ' + response.requestErrorDescriptionField);
			System.Debug('*** response.requestErrorField ' + response.requestErrorField);
			System.Debug('*** response.requestIDField ' + response.requestIDField);
			System.Debug('*** response.showOnePriceField ' + response.showOnePriceField);
			System.Debug('*** response.userIDField ' + response.userIDField);
			
			for (CPQ_ProxyMultiPriceService.ShipToResp s : response.shipToDSField.ShipToResp) {
			    System.Debug('*** s.PropertyChanged ' + s.PropertyChanged);
			    System.Debug('*** s.billToField ' + s.billToField);
			    System.Debug('*** s.shipToErrorDescriptionField ' + s.shipToErrorDescriptionField);
			    System.Debug('*** s.shipToErrorField ' + s.shipToErrorField);
			    System.Debug('*** s.shipToField ' + s.shipToField);
			    
			    for (CPQ_ProxyMultiPriceService.SKUTypeResp k : s.sKUsField.SKUTypeResp) {
			        System.Debug('*** k.PropertyChanged ' + k.PropertyChanged);
			        System.Debug('*** k.quantityField ' + k.quantityField);
			        System.Debug('*** k.quantityFieldSpecified ' + k.quantityFieldSpecified);
			        System.Debug('*** k.sKUCodeField ' + k.sKUCodeField);
			        System.Debug('*** k.sKUErrorDescriptionField ' + k.sKUErrorDescriptionField);
			        System.Debug('*** k.sKUErrorField ' + k.sKUErrorField);
			        System.Debug('*** k.salesClassField ' + k.salesClassField);
			        System.Debug('*** k.uOMField ' + k.uOMField);
			        
			    	if (k.sKUErrorField != '0') {
			        	errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', k.sKUErrorField + ': ' + k.sKUErrorDescriptionField, 
			        												'Proposal: ' + config.Apttus_QPConfig__Proposald__r.Name + 
			        												', Agreement: ' + config.Apttus_CMConfig__AgreementId__r.Name +
			        												', Config: ' + config.Name + 
			        												', sKUCode: ' + k.sKUCodeField + 
			        												', sKUUOM: ' + k.uOMField + 
			        												', shipTo: ' + s.shipToField + 
			        												', user: ' + request.userIDField, 
			        												String.valueOf(request)));
			        	continue;
			    	}
			    	
			        if (k.pricesField == null || k.pricesField.PriceTypeResp == null) {
			        	errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', 'No Price returned.', 
			        												'Proposal: ' + config.Apttus_QPConfig__Proposald__r.Name + 
			        												', Agreement: ' + config.Apttus_CMConfig__AgreementId__r.Name +
			        												', Config: ' + config.Name + 
			        												', sKUCode: ' + k.sKUCodeField + 
			        												', sKUUOM: ' + k.uOMField + 
			        												', shipTo: ' + s.shipToField + 
			        												', user: ' + request.userIDField, 
			        												String.valueOf(request)));
			        	continue;
			        }
			        
			        for (CPQ_ProxyMultiPriceService.PriceTypeResp p : k.pricesField.PriceTypeResp) {
				        System.Debug('*** p.PropertyChanged ' + p.PropertyChanged);
				        System.Debug('*** p.contractDescriptionField ' + p.contractDescriptionField);
				        System.Debug('*** p.contractDesignatorField ' + p.contractDesignatorField);
				        System.Debug('*** p.contractNumberField ' + p.contractNumberField);
				        System.Debug('*** p.currencyField ' + p.currencyField);
				        System.Debug('*** p.effectiveDateField ' + p.effectiveDateField);
				        System.Debug('*** p.endCustomerPriceField ' + p.endCustomerPriceField);
				        System.Debug('*** p.endCustomerPriceFieldSpecified ' + p.endCustomerPriceFieldSpecified);
				        System.Debug('*** p.expirationDateField ' + p.expirationDateField);
				        System.Debug('*** p.gPONonCommittedPriceField ' + p.gPONonCommittedPriceField);
				        System.Debug('*** p.gPONonCommittedPriceFieldSpecified ' + p.gPONonCommittedPriceFieldSpecified);
				        System.Debug('*** p.gPOTierPriceField ' + p.gPOTierPriceField);
				        System.Debug('*** p.gPOTierPriceFieldSpecified ' + p.gPOTierPriceFieldSpecified);
				        System.Debug('*** p.gPOTierPriceRootContractField ' + p.gPOTierPriceRootContractField);
				        System.Debug('*** p.markupField ' + p.markupField);
				        System.Debug('*** p.markupFieldSpecified ' + p.markupFieldSpecified);
				        System.Debug('*** p.pricContCommitmentTypeDescField ' + p.pricContCommitmentTypeDescField);
				        System.Debug('*** p.pricContRootContractField ' + p.pricContRootContractField);
				        System.Debug('*** p.priceErrorDescriptionField ' + p.priceErrorDescriptionField);
				        System.Debug('*** p.priceErrorField ' + p.priceErrorField);
				        System.Debug('*** p.quantityRangeField ' + p.quantityRangeField);
				        System.Debug('*** p.sequenceNumberField ' + p.sequenceNumberField);
				        System.Debug('*** p.sequenceNumberFieldSpecified ' + p.sequenceNumberFieldSpecified);
				        System.Debug('*** p.unitPriceField ' + p.unitPriceField);
			    		
			    		Product2 prod = skuToProducts.get(k.sKUCodeField);
			    		
			    		if (csps.containsKey(prod.ID)) {
			    			CPQ_Customer_Specific_Pricing__c csp = csps.get(prod.ID);
			    			
			    			if (csp.Contract_Price__c > p.unitPriceField) {
			        			csp.GPO_Price__c = p.gPOTierPriceField;
			        			csp.Non_Committed_Price__c = p.gPONonCommittedPriceField;
			        			csp.Contract_Price__c = p.unitPriceField;
			        			csp.Root_Contract__c = p.pricContRootContractField;
			        			csp.CommTypeDesc_INT__c = p.pricContCommitmentTypeDescField;
								String commL = csp.CommTypeDesc_INT__c.toLowerCase();
								if (commTypes.containsKey(commL))
			        				csp.Commitment_Type__c = commTypes.get(commL).ID;
			        			
			        			csp.Effective_Date__c = Date.valueOf(p.effectiveDateField);
			        			csp.Expiration_Date__c = Date.valueOf(p.expirationDateField);
			        			csp.LastRetrievedDateTime__c = System.now();
			    			}
			    		} else {
			    			CPQ_Customer_Specific_Pricing__c csp = new CPQ_Customer_Specific_Pricing__c();
				        	csp.Account__c = account.ID;
				        	csp.Product__c = prod.ID;
				        	csp.External_ID__c = accountCRN + ':::' + k.sKUCodeField;
				        	csp.GPO_Price__c = p.gPOTierPriceField;
				        	csp.Non_Committed_Price__c = p.gPONonCommittedPriceField;
			    			csp.Contract_Price__c = p.unitPriceField;
			    			csp.Root_Contract__c = p.pricContRootContractField;
			    			csp.CommTypeDesc_INT__c = p.pricContCommitmentTypeDescField;
							String commL = csp.CommTypeDesc_INT__c.toLowerCase();
							if (commTypes.containsKey(commL))
		        				csp.Commitment_Type__c = commTypes.get(commL).ID;
		        			
			    			csp.Effective_Date__c = Date.valueOf(p.effectiveDateField);
			    			csp.Expiration_Date__c = Date.valueOf(p.expirationDateField);
			        		csp.LastRetrievedDateTime__c = System.now();
			        		csps.put(csp.Product__c, csp);
			    		}
			        }
			    }
			}
		} catch (Exception e) {
        	errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', e.getMessage(), 
	        												'Proposal: ' + config.Apttus_QPConfig__Proposald__r.Name + 
	        												', Agreement: ' + config.Apttus_CMConfig__AgreementId__r.Name +
	        												', Config: ' + config.Name + 
	        												', sKUCodes: ' + skuToProducts + 
	        												', shipTo: ' + Account.Account_External_ID__c, String.valueOf(request)));
		}
	}
	
/*=========================================================
	When users select overall quantity on the bundle, set the correct quantity for its options
===========================================================*/        
    //organize line items bundle LINE => option LINEs
    //	include all lines: for standalone product line, will be line ID => empty Set<String>
	public static Map<String, Set<String>> buildLineBundleToOptions(List<Apttus_Config2__LineItem__c> lines) {
        Map<String, Set<String>> result = new Map<String, Set<String>>();
		
        Map<String, String> headers = new Map<String, String>();
        
		for(Apttus_Config2__LineItem__c line : lines) {
            if (line.Apttus_Config2__IsPrimaryLine__c == true && 
            		line.Apttus_Config2__LineType__c == 'Product/Service' && 
            		line.Apttus_Config2__ProductId__c != null && 
            		line.Apttus_Config2__LineNumber__c != null) {
				headers.put(line.Apttus_Config2__LineNumber__c + ': ' + line.Apttus_Config2__ProductId__c, line.ID);
				result.put(line.ID, new Set<String>());
            }
        }
        
        //the way to tell an option belonging to a bundle is that they all have the same LineNumber
        for(Apttus_Config2__LineItem__c line : lines) {
            //organize line items bundle LINE => option LINEs
            if (line.Apttus_Config2__IsPrimaryLine__c == true && 
        			line.Apttus_Config2__OptionId__c != null && 
        			headers.containsKey(line.Apttus_Config2__LineNumber__c + ': ' + line.Apttus_Config2__ProductId__c)) {
            	String bundleLineID = headers.get(line.Apttus_Config2__LineNumber__c + ': ' + line.Apttus_Config2__ProductId__c);
        		result.get(bundleLineID).add(line.ID);
            }
        }
        
        return result;
	}
}


/*
List<CPQ_Customer_Specific_Pricing__c> toupdate = new List<CPQ_Customer_Specific_Pricing__c>();
for (CPQ_Customer_Specific_Pricing__c csps : [Select ID, Product__r.ProductCode, Account__r.Account_External_ID__c, External_ID__c, LastRetrievedDateTime__c From CPQ_Customer_Specific_Pricing__c]) {
	String crn = csps.Account__r.Account_External_ID__c.substring(3);
	String key = crn + ':::' + csps.Product__r.ProductCode;
	System.Debug(key);
	csps.External_ID__c = key;
	//csps.LastRetrievedDateTime__c = System.now();
	toupdate.add(csps);
}

update toupdate;

List<Apttus_Config2__LineItem__c> toUpdate = new List<Apttus_Config2__LineItem__c>();
for (Apttus_Config2__LineItem__c l :[SELECT ID, Apttus_Config2__PricingStatus__c, 
															Apttus_Config2__IsCustomPricing__c 
														FROM Apttus_Config2__LineItem__c 
														Where Apttus_Config2__LineType__c != 'Misc' and
															Apttus_Config2__ChargeType__c Not in: CPQ_Utilities.getCallbackChargeTypeIgnoreList() and 
															Apttus_Config2__ConfigurationId__c = 'a4YK0000000DQKB']) {
	l.Apttus_Config2__PricingStatus__c = 'Pending';
	l.Apttus_Config2__IsCustomPricing__c = true;
	toUpdate.add(l);
}

update toUpdate;


CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
service.timeout_x = 60000;

CPQ_ProxyMultiPriceService.PricingRespType response;
 
CPQ_ProxyMultiPriceService.PricingRequestType request = new CPQ_ProxyMultiPriceService.PricingRequestType();
//request.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
request.countryField = 'US';
request.currencyField = '';
request.designatorField = 'B';
request.gBUField = 'SD';
request.requestDateField = System.Today();
request.requestDateFieldSpecified = true;
request.requestIDField = '';
request.showOnePriceField = 'All';
request.userIDField = 'yuli.fintescu';
 
request.sKUsField = new CPQ_ProxyMultiPriceService.ArrayOfSKUType();
request.sKUsField.SKUType = new CPQ_ProxyMultiPriceService.SKUType[]{};

CPQ_ProxyMultiPriceService.SKUType t = new CPQ_ProxyMultiPriceService.SKUType();
//t.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXA';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXI';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXN';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXP';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'DOC10';
t.uOMField = 'EA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXAR';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXIR';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXNR';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXPR';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'DS100A-1';
t.uOMField = 'BX';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'MAXFAST';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'SC-A';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'SC-NEO';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'SC-PR';
t.uOMField = 'CA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'GEMAXDASH';
t.uOMField = 'EA';
request.sKUsField.SKUType.add(t);

t = new CPQ_ProxyMultiPriceService.SKUType();
t.quantityField = 1;
t.quantityFieldSpecified = true;
t.sKUCodeField = 'N65-NA3';
t.uOMField = 'EA';
request.sKUsField.SKUType.add(t);

request.shipToDSField = new CPQ_ProxyMultiPriceService.ArrayOfShipTo();
request.shipToDSField.ShipTo = new CPQ_ProxyMultiPriceService.ShipTo[]{};
CPQ_ProxyMultiPriceService.ShipTo Ship_To = new CPQ_ProxyMultiPriceService.ShipTo();
//Ship_To.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
Ship_To.shipTo1Field = '543002';
request.shipToDSField.ShipTo.add(Ship_To);
 
System.Debug('*** request ' + request);
 
response = service.MultiPrice(request);
System.Debug('*** response.PropertyChanged ' + response.PropertyChanged);
System.Debug('*** response.countryField ' + response.countryField);
System.Debug('*** response.currencyField ' + response.currencyField);
System.Debug('*** response.designatorField ' + response.designatorField);
System.Debug('*** response.gBUField ' + response.gBUField);
System.Debug('*** response.requestDateField ' + response.requestDateField);
System.Debug('*** response.requestDateFieldSpecified ' + response.requestDateFieldSpecified);
System.Debug('*** response.requestErrorDescriptionField ' + response.requestErrorDescriptionField);
System.Debug('*** response.requestErrorField ' + response.requestErrorField);
System.Debug('*** response.requestIDField ' + response.requestIDField);
System.Debug('*** response.showOnePriceField ' + response.showOnePriceField);
System.Debug('*** response.userIDField ' + response.userIDField);
 
for (CPQ_ProxyMultiPriceService.ShipToResp s : response.shipToDSField.ShipToResp) {
    System.Debug('*** s.PropertyChanged ' + s.PropertyChanged);
    System.Debug('*** s.billToField ' + s.billToField);
    System.Debug('*** s.shipToErrorDescriptionField ' + s.shipToErrorDescriptionField);
    System.Debug('*** s.shipToErrorField ' + s.shipToErrorField);
    System.Debug('*** s.shipToField ' + s.shipToField);
    
    for (CPQ_ProxyMultiPriceService.SKUTypeResp k : s.sKUsField.SKUTypeResp) {
        System.Debug('*** k.PropertyChanged ' + k.PropertyChanged);
        System.Debug('*** k.quantityField ' + k.quantityField);
        System.Debug('*** k.quantityFieldSpecified ' + k.quantityFieldSpecified);
        System.Debug('*** k.sKUCodeField ' + k.sKUCodeField);
        System.Debug('*** k.sKUErrorDescriptionField ' + k.sKUErrorDescriptionField);
        System.Debug('*** k.sKUErrorField ' + k.sKUErrorField);
        System.Debug('*** k.salesClassField ' + k.salesClassField);
        System.Debug('*** k.uOMField ' + k.uOMField);
        
		if (k.pricesField == null || k.pricesField.PriceTypeResp == null)
			System.Debug('*** k.pricesField == null');
		else {
	        for (CPQ_ProxyMultiPriceService.PriceTypeResp p : k.pricesField.PriceTypeResp) {
		        System.Debug('*** p.PropertyChanged ' + p.PropertyChanged);
		        System.Debug('*** p.contractDescriptionField ' + p.contractDescriptionField);
		        System.Debug('*** p.contractDesignatorField ' + p.contractDesignatorField);
		        System.Debug('*** p.contractNumberField ' + p.contractNumberField);
		        System.Debug('*** p.currencyField ' + p.currencyField);
		        System.Debug('*** p.effectiveDateField ' + p.effectiveDateField);
		        System.Debug('*** p.endCustomerPriceField ' + p.endCustomerPriceField);
		        System.Debug('*** p.endCustomerPriceFieldSpecified ' + p.endCustomerPriceFieldSpecified);
		        System.Debug('*** p.expirationDateField ' + p.expirationDateField);
		        System.Debug('*** p.gPONonCommittedPriceField ' + p.gPONonCommittedPriceField);
		        System.Debug('*** p.gPONonCommittedPriceFieldSpecified ' + p.gPONonCommittedPriceFieldSpecified);
		        System.Debug('*** p.gPOTierPriceField ' + p.gPOTierPriceField);
		        System.Debug('*** p.gPOTierPriceFieldSpecified ' + p.gPOTierPriceFieldSpecified);
		        System.Debug('*** p.gPOTierPriceRootContractField ' + p.gPOTierPriceRootContractField);
		        System.Debug('*** p.markupField ' + p.markupField);
		        System.Debug('*** p.markupFieldSpecified ' + p.markupFieldSpecified);
		        System.Debug('*** p.pricContCommitmentTypeDescField ' + p.pricContCommitmentTypeDescField);
		        System.Debug('*** p.pricContRootContractField ' + p.pricContRootContractField);
		        System.Debug('*** p.priceErrorDescriptionField ' + p.priceErrorDescriptionField);
		        System.Debug('*** p.priceErrorField ' + p.priceErrorField);
		        System.Debug('*** p.quantityRangeField ' + p.quantityRangeField);
		        System.Debug('*** p.sequenceNumberField ' + p.sequenceNumberField);
		        System.Debug('*** p.sequenceNumberFieldSpecified ' + p.sequenceNumberFieldSpecified);
		        System.Debug('*** p.unitPriceField ' + p.unitPriceField);
	        }
		}
    }
}

*/