public with sharing class AddOpportunityPlayerExtension{

    public String committeeName {get;Set;}
    public Boolean isSFOne {get;Set;}
    
    Map<String, Opportunity_Player__c> committeePlayers = null;
    public Opportunity opp { 
        get
        {
            if(opp == null)
            {
                String eyeDee = ApexPages.currentPage().getParameters().get('oppId');
                if(eyeDee == null || eyeDee == '')
                    eyeDee = ApexPages.currentPage().getParameters().get('id');
                opp = [SELECT Id, Name, AccountId, Account.Name FROM Opportunity WHERE Id =: eyeDee LIMIT 1];
            }
            return opp;
        }
        set;
    }
    
    private Map<String, Opportunity_Player__c> committeeContactsMap;
    public List<Opportunity_Player__c> committeeContacts
    {
        get
        {
            if(committeeContactsMap == null)
            {
                committeeName = 'Account Committee';
                List<Committee_Member__c> cms = [SELECT Id, Contact__r.AccountId, Contact__r.Id, Contact__r.Name, Committee_Name__c, Committee_Name__r.Name, Committee_Name__r.Committee_Type__c, Contact__r.Degree_of_Influence__c, Profile_Name__c, Committee_Role__c, Contact__r.Affiliated_Role__c, Committee_Type__c FROM Committee_Member__c WHERE Committee_Name__r.Account__c = :opp.AccountId AND Contact__r.Id NOT IN: existingContactIds];
                committeeContactsMap = new Map<String, Opportunity_Player__c>();
                for(Committee_Member__c c: cms)
                {
                    if(committeeContactsMap.get(c.Contact__r.Id) == null)
                    {
                        committeeContactsMap.put(c.Contact__r.Id, new Opportunity_Player__c(Opportunity__c = opp.Id, Contact__c = c.Contact__r.Id, Committee_Member__c = c.Id, Profile_Type__c = c.Committee_Name__r.Committee_Type__c, Influence__c = c.Contact__r.Degree_of_Influence__c, Connected_As__c = c.Contact__r.Affiliated_Role__c ));
                        committeeName = c.Profile_Name__c;
                    }
                }
            }
            return committeeContactsMap.values();
        }
        Set;
    }
    
    private Map<String, Opportunity_Player__c> accountContactsMap;
    public List<Opportunity_Player__c> accountContacts
    {
        get
        {
            if(accountContactsMap == null)
            {
                List<Contact> cons = [SELECT Id, Name, Title FROM Contact WHERE AccountId = :opp.AccountId AND Id NOT IN: existingContactIds];
                accountContactsMap = new Map<String, Opportunity_Player__c>();
                for(Contact c: cons)
                {
                    if(accountContactsMap.get(c.Id) == null)
                        accountContactsMap.put(c.Id, new Opportunity_Player__c(Opportunity__c = opp.Id, Contact__c = c.Id));
                }
            }
            return accountContactsMap.values();
        }
        Set;
    }
    
    @RemoteAction
    public static PageReference SubmitAndExit() {
        return null;//this.SaveRecords();
    }
    
    private Map<String, Opportunity_Player__c> existingContactsMap;
    public List<Opportunity_Player__c> existingContacts
    {
        get
        {
            if(existingContactsMap == null)
            {
                List<Opportunity_Player__c> players = [SELECT Id, Name, Influence__c , Connected_As__c, Contact__r.Id, Opportunity_Role__c, Committee_Member__r.Committee_Type__c, Committee_Member__r.Committee_Name__c,Of_Cases__c,Comment__c,Notes_2__c,Notes3__c,Notes4__c  FROM Opportunity_Player__c WHERE Opportunity__c = :opp.Id and Contact_RecordType__c in('Connected Contact US','Master Clinician','In Process Connected Contact')];
                existingContactsMap = new Map<String, Opportunity_Player__c>();
                for(Opportunity_Player__c c: players)
                {
                    if(existingContactsMap.get(c.Contact__r.Id) == null)
                        existingContactsMap.put(c.Contact__r.Id, c);
                }
            }
            return existingContactsMap.values();
        }
        Set;
    }
    
    
    private Map<String, Opportunity_Player__c> existingContactsMapEconomic;
    public List<Opportunity_Player__c> existingContactsEconomic
    {
        get
        {
            if(existingContactsMapEconomic == null)
            {
                List<Opportunity_Player__c> players = [SELECT Id, Name, Influence__c , Connected_As__c, Contact__r.Id, Opportunity_Role__c, Committee_Member__r.Committee_Type__c, Committee_Member__r.Committee_Name__c,Comment__c,Notes_2__c,Notes3__c,Notes4__c  FROM Opportunity_Player__c WHERE Opportunity__c = :opp.Id and Contact_RecordType__c not in('Connected Contact US','Master Clinician','In Process Connected Contact')];
                existingContactsMapEconomic = new Map<String, Opportunity_Player__c>();
                for(Opportunity_Player__c c: players)
                {
                    if(existingContactsMapEconomic.get(c.Contact__r.Id) == null)
                        existingContactsMapEconomic.put(c.Contact__r.Id, c);
                }
            }
            return existingContactsMapEconomic.values();
        }
        Set;
    }
    
    public List<Opportunity_Player__c> blankOppPlayers
    {
        get
        {
            if(blankOppPlayers == null)
            {
                blankOppPlayers = new List<Opportunity_Player__c>{new Opportunity_Player__c(Opportunity__c = opp.Id), new Opportunity_Player__c(Opportunity__c = opp.Id), new Opportunity_Player__c(Opportunity__c = opp.Id), new Opportunity_Player__c(Opportunity__c = opp.Id), new Opportunity_Player__c(Opportunity__c = opp.Id)};
            }
            return blankOppPlayers;
        }
        Set;
    }
    
    private Set<Id> existingContactIds
    {
        get
        {
            if(existingContactIds == null)
            {
                existingContactIds = new Set<Id>();
                for(Opportunity_Player__c p : existingContacts)
                    existingContactIds.add(p.Contact__r.Id);
            }
            return existingContactIds;
        }
        Set;
    }
    
    public AddOpportunityPlayerExtension(ApexPages.StandardController controller)
    {
        List<Opportunity_Player__c> opps = committeeContacts;
        String one = ApexPages.currentPage().getParameters().get('onSFONE');
        isSFOne = one != null && one == '0';
    }
    
    public PageReference OppPage()
    {
        PageReference pr = new PageReference('/' + opp.Id);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference SaveRecords()
    {
        List<Opportunity_Player__c> oppPlayersToInsert = new List<Opportunity_Player__c>();
        List<Opportunity_Player__c> oppPlayersToUpdate = new List<Opportunity_Player__c>();
        List<Opportunity_Player__c> oppPlayersToDelete = new List<Opportunity_Player__c>();
        
        for(Opportunity_Player__c o : existingContacts)
        {
            if(o.Opportunity_Role__c == null || o.Contact__c == null)
                oppPlayersToDelete.add(o);
            else
                oppPlayersToUpdate.add(o);
        }
        for(Opportunity_Player__c o : existingContactsEconomic)
        {
            if(o.Opportunity_Role__c == null || o.Contact__c == null)
                oppPlayersToDelete.add(o);
            else
                oppPlayersToUpdate.add(o);
        }
        for(Opportunity_Player__c o: blankOppPlayers)
        {
            if(o.Contact__c != null && o.Opportunity_Role__c != null && !existingContactIds.contains(o.Contact__c))
            {
                oppPlayersToInsert.add(o);
                existingContactIds.add(o.Contact__c);
            }
        }
        for(Opportunity_Player__c o : accountContacts)
        {
            if(o.Opportunity_Role__c != null && !existingContactIds.contains(o.Contact__c))
            {
                oppPlayersToInsert.add(o);
                existingContactIds.add(o.Contact__c);
            }
        }
        for(Opportunity_Player__c o : committeeContacts)
        {
            if(o.Opportunity_Role__c != null && !existingContactIds.contains(o.Contact__c))
            {
                oppPlayersToInsert.add(o);
                existingContactIds.add(o.Contact__c);
            }
        }
        
        System.debug('oppPlayersToInsert: ' + oppPlayersToInsert);
        System.debug('oppPlayersToUpdate: ' + oppPlayersToUpdate);
        System.debug('oppPlayersToDelete: ' + oppPlayersToDelete);
        Boolean bExceptionCaught = false;
        try
        {
            List<Database.SaveResult> results = Database.insert(oppPlayersToInsert, false);
            for(Database.SaveResult result : results)
            {
                if (!result.isSuccess())
                {
                    List <Database.Error >error = result.getErrors();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error [0].getMessage());
                    ApexPages.addmessage(myMsg);
                    //return null;
                    bExceptionCaught = true;
                }
            }
        }
        catch(DmlException exc)
        {
            ApexPages.addMessages(exc);
            //return null;
            bExceptionCaught = true;
        }
        
        try
        {
            List<Database.SaveResult> results = Database.update(oppPlayersToUpdate, false);
            for(Database.SaveResult result : results)
            {
                if (!result.isSuccess())
                {
                    List <Database.Error >error = result.getErrors();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error [0].getMessage());
                    ApexPages.addmessage(myMsg);
                    //return null;
                    bExceptionCaught = true;
                }
            }
        }
        catch(DmlException exc)
        {
            ApexPages.addMessages(exc);
            //return null;
            bExceptionCaught = true;
        }
        
        try
        {
            List<Database.DeleteResult> results = Database.delete(oppPlayersToDelete, false);
            for(Database.DeleteResult result : results)
            {
                if (!result.isSuccess())
                {
                    List <Database.Error >error = result.getErrors();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error [0].getMessage());
                    ApexPages.addmessage(myMsg);
                    //return null;
                    bExceptionCaught = true;
                }
            }
        }
        catch(DmlException exc)
        {
            ApexPages.addMessages(exc);
            //return null;
            bExceptionCaught = true;
        }
        return bExceptionCaught ? null : OppPage();
    }
    
    public PageReference Cancel()
    {
        return OppPage();
    }
}