/**
Validation CallBack function invoked when clicking in Reprice, Save and Finalize button

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-13      Yuli Fintescu       Created
===============================================================================
*/
global class CPQ_ValidationCallBack implements Apttus_Config2.CustomClass.IValidationCallback2 {
    global Apttus_Config2.CustomClass.ValidationResult validateCart(Apttus_Config2.ProductConfiguration cart) {
        Apttus_Config2.CustomClass.ValidationResult result = new Apttus_Config2.CustomClass.ValidationResult(true);
        
        DEBUG.constructorTitleBlock('CPQ_ValidationCallBack.validateCart');
        
        //Yuli, 1/28/2016: Looks like callback is no longer fired out from vf page in new UI.
        //The code should still be here to support old UI
        if (ApexPages.currentPage() != null) {
            String str = ApexPages.currentPage().getUrl();
            if (str.indexOf('CartDetailView') < 0)
                return result;
        }
        
        
        DEBUG.write(new List<string>{ 'step 1' });
        
        //ID proposalID = cart.getConfigSO().Apttus_QPConfig__Proposald__c;
        
        Decimal tradeInQuantity = 0;
        Decimal hardwareQuantity = 0;
        
        Set<String> lnaRootConts = new Set<String>();
        
        List<Apttus_Config2__LineItem__c> lineSOs = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__ProductConfiguration__c configSO;
        if (Test.isRunningTest()) { 
            List<Apttus_Config2__ProductConfiguration__c> configSOs = new List<Apttus_Config2__ProductConfiguration__c>(); 
            Test_CPQ_ValidationCallBack.createTestData(lineSOs, configSOs);
            configSO = configSOs[3];
        } else {
            configSO = cart.getConfigSO();
            for(Apttus_Config2.LineItem lineItemMO : cart.getlineItems()) {
                Apttus_Config2__LineItem__c line = lineItemMO.getLineItemSO();
                lineSOs.add(line);
            }
        }

        DEBUG.write(new List<string>{ 'step 2' });
        Apttus_Config2__Pricelist__c ssgPriceList = cpqPriceList_c.SSG;
            
        if (ssgPriceList != null && configSO.Apttus_Config2__PriceListId__c == ssgPriceList.Id)
        {
            return result;
        }

        DEBUG.write(new List<string>{ 'step 3' });
        String HPG = 'SG0150';
        CPQ_Variable__c cs = CPQ_Variable__c.getValues('HPG Group Code');
        if (cs != null)
            HPG = cs.Value__c;
        Set<String> ignores = CPQ_Utilities.getCallbackChargeTypeIgnoreList();
        
        Set<string> results = CPQ_Utilities.getDealTypesNotAllowingPriceAdjustment();
        Set<String> DealTypesNotAllowingPriceAdjustment = new set<string>();
        
        for(string str : results)
        	DealTypesNotAllowingPriceAdjustment.add(str.replace('_', ' '));
        
		DEBUG.dump(
            new Map<object, object>
            {
                'ignores' => ignores,
                'notAllowed' => DealTypesNotAllowingPriceAdjustment
            });

        for(Apttus_Config2__LineItem__c line : lineSOs) {
            //skip non primary line
            if (line.Apttus_Config2__IsPrimaryLine__c == false) 
                continue;
            
            String dealTypeName = '';
            if (line.Record_Type__c != null)
                dealTypeName = CPQ_Utilities.getCoreDealTypeLabel(line.Record_Type__c);
            else if (line.Agreement_Record_Type__c != null)
                dealTypeName = CPQ_Utilities.getCoreDealTypeLabel(line.Agreement_Record_Type__c);
            
            //Trade-in quantity < Hardware quantity.
            if (line.Apttus_Config2__ChargeType__c == 'Trade-In')// || line.Apttus_Config2__ChargeType__c == 'Promotion - Trade-In'
                tradeInQuantity = tradeInQuantity + (line.Apttus_Config2__Quantity__c == null ? 0 : line.Apttus_Config2__Quantity__c);
            
            if (line.Apttus_Config2__ChargeType__c == 'Hardware')// || line.Apttus_Config2__ChargeType__c == 'Promotion - Hardware'
                hardwareQuantity = hardwareQuantity + (line.Apttus_Config2__Quantity__c == null ? 0 : line.Apttus_Config2__Quantity__c);
            
            //calculate unitprice
            decimal unitprice = line.Apttus_Config2__BasePrice__c;
            
            if (line.Apttus_Config2__AdjustmentType__c == 'Base Price Override')
                unitprice = line.Apttus_Config2__BasePriceOverride__c;
            else if (line.Apttus_Config2__AdjustmentType__c == 'Price Factor' && line.Apttus_Config2__BasePrice__c != null && line.Apttus_Config2__AdjustmentAmount__c != null)
                unitprice = line.Apttus_Config2__BasePrice__c * line.Apttus_Config2__AdjustmentAmount__c;
            
            else if ((line.Apttus_Config2__AdjustmentType__c == '% Discount' || line.Apttus_Config2__AdjustmentType__c == '% Markup' || line.Apttus_Config2__AdjustmentType__c == '% Uplift') && line.Apttus_Config2__BasePrice__c != null && line.Apttus_Config2__AdjustmentAmount__c != null)
                unitprice = line.Apttus_Config2__BasePrice__c * (1 - line.Apttus_Config2__AdjustmentAmount__c/100.00);
            else if ((line.Apttus_Config2__AdjustmentType__c == 'Discount Amount' || line.Apttus_Config2__AdjustmentType__c == 'Markup Amount') && line.Apttus_Config2__BasePrice__c != null && line.Apttus_Config2__AdjustmentAmount__c != null && line.Apttus_Config2__Quantity__c != null && line.Apttus_Config2__Quantity__c != 0)
                unitprice = line.Apttus_Config2__BasePrice__c - line.Apttus_Config2__AdjustmentAmount__c / line.Apttus_Config2__Quantity__c;
                
            else if (line.Apttus_Config2__AdjustmentType__c == 'Price Override' && line.Apttus_Config2__AdjustmentAmount__c != null && line.Apttus_Config2__Quantity__c != null && line.Apttus_Config2__Quantity__c != 0)
                unitprice = line.Apttus_Config2__AdjustmentAmount__c / line.Apttus_Config2__Quantity__c;
            
            //option item price is adjusted in option subtotal - won't work Apttus_Config2__NetPrice__c value is not set at validation callback
            //else if (line.Apttus_Config2__AdjustmentType__c == null && line.Apttus_Config2__AdjustmentAmount__c == null && line.Apttus_Config2__NetPrice__c != null && line.Apttus_Config2__Quantity__c != null && line.Apttus_Config2__Quantity__c != 0)
            //  unitprice = line.Apttus_Config2__NetPrice__c / line.Apttus_Config2__Quantity__c;
            
            //check if Price Offer Letter contract
            //Net Price cannot be lower than FSS Extended Price.
            if (dealTypeName.startsWith('Price Offer Letter') && line.Apttus_Config2__ChargeType__c != 'Trade-In' && line.FSS_Price__c > 0 && line.FSS_Price__c > unitprice) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, line.Product_Name__c + ': Unit Price (' + unitprice.setScale(2) + ') cannot be lower than FSS Price (' + line.FSS_Price__c.setScale(2) + ')'));
                result.isSuccess = false;
            }
            
            DEBUG.write(new List<string>{ dealTypeName });
            
            if (DealTypesNotAllowingPriceAdjustment.contains(dealTypeName) && line.Apttus_Config2__AdjustmentType__c != null && line.Apttus_Config2__AdjustmentAmount__c != null && line.Apttus_Config2__AdjustmentAmount__c != 0) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, line.Product_Name__c + ': No pricing adjustments are allowed for this Deal Type.'));
                result.isSuccess = false;
            }
            
            if (!ignores.contains(line.Apttus_Config2__ChargeType__c) && line.Is_Bundle_Header__c == false && line.Apttus_Config2__Cost__c != null && line.Apttus_Config2__Cost__c > unitprice) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, line.Product_Name__c + ': Unit Price (' + unitprice.setScale(2) + ') cannot be lower than Cost (' + line.Apttus_Config2__Cost__c.setScale(2) + ')'));
                result.isSuccess = false;
            }
            
            if (line.Apttus_Config2__ChargeType__c == 'Trade-In' && line.Min_Price__c != null && line.Min_Price__c != 0 && line.Min_Price__c > unitprice) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, line.Product_Name__c + ': Trade-In Price (' + unitprice.setScale(2) + ') cannot be lower than Min Price (' + line.Min_Price__c.setScale(2) + ')'));
                result.isSuccess = false;
            }
            
            if (line.Apttus_Config2__ChargeType__c == 'Trade-In' && unitprice > 0) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, line.Product_Name__c + ': Trade-in value cannot exceed $0.'));
                result.isSuccess = false;
            }
            
            //check if LNA contract
            //No HPG allowed in LNA
            if (line.Current_Root_Contract__c != null && dealTypeName.startsWith('Locally Negotiated Agreement (LNA)')) {
                lnaRootConts.add(line.Current_Root_Contract__c);
            }
        }
        
        if (tradeInQuantity > hardwareQuantity) {
            result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Trade-in quantity must be less than total Hardware quantity.'));
            result.isSuccess = false;
        }
        
        if (lnaRootConts.size() > 0) {
            if (lnaRootConts.size() > 1) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only one product line can be included on this LNA.  Remove products as necessary to ensure only one Root Contract is represented in the Cart.'));
                result.isSuccess = false;
            }
            List<Partner_Root_Contract__c> roots = [Select Name, Group_Code__c From Partner_Root_Contract__c Where Name in: lnaRootConts and Group_Code__c =: HPG];
            if (roots.size() > 0) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'LNA proposal cannot include the items that are currently in HPG contract: ' + rootListToString(roots)));
                result.isSuccess = false;
            }
        }
        
        String dealTypeName = '';
        if (configSO.Record_Type__c != null)
            dealTypeName = CPQ_Utilities.getCoreDealTypeLabel(configSO.Record_Type__c);
        else if (configSO.Agreement_Record_Type__c != null)
            dealTypeName = CPQ_Utilities.getCoreDealTypeLabel(configSO.Agreement_Record_Type__c);
        
        Boolean isCOOP = dealTypeName.startsWith('COOP Plus Program') || dealTypeName.startsWith('Respiratory Solutions COOP');
        Boolean isLease = dealTypeName.startsWith('PM Equipment Lease Program');
        
        if (isCOOP || isLease) {
            if (isCOOP && (configSO.Duration__c == null || configSO.Total_Annual_Sensor_Commitment_Amount__c == null)) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must enter Duration and Total Annual Sensor Commitment Amount values in Award Credit Calculator.'));
                result.isSuccess = false;
            }
            
            CPQ_RollupFieldEngine rollupEngine = new CPQ_RollupFieldEngine();
            rollupEngine.ClassifyLineItems(lineSOs);
            if (isCOOP && (rollupEngine.EquipmentSkus.size() == 0 && (configSO.TBD_Amount__c == null || configSO.TBD_Amount__c == 0) && (configSO.Promotion_Subtotal__c == null || configSO.Promotion_Subtotal__c == 0))) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing equipment products, TBD dollars or Promotion Equipments.'));
                result.isSuccess = false;
            }
            if (isCOOP && rollupEngine.DisposaleSkus.size() == 0) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing consumable products.'));
                result.isSuccess = false;
            }
            
            //at least one consumable item that matches the product line of the equipment added.
            if (rollupEngine.EquipmentHLVL1Skus.containsKey('BL') && !rollupEngine.DisposableHLVL2Skus.containsKey('BLA')) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'At least one consumable item should match the BIS equipment(s) - ' + rollupEngine.EquipmentHLVL1Skus.get('BL') + '.'));
                result.isSuccess = false;
            }
            
            if (rollupEngine.EquipmentHLVL1Skus.containsKey('RF') && !rollupEngine.DisposableHLVL2Skus.containsKey('RFB')) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'At least one consumable item should match the NELLCOR equipment(s) - ' + rollupEngine.EquipmentHLVL1Skus.get('RF') + '.'));
                result.isSuccess = false;
            }
            
            if (rollupEngine.EquipmentHLVL1Skus.containsKey('RS') && !rollupEngine.DisposableHLVL2Skus.containsKey('RSA')) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'At least one consumable item should match the INVOS equipment(s) - ' + rollupEngine.EquipmentHLVL1Skus.get('RS') + '.'));
                result.isSuccess = false;
            }
            
            if (rollupEngine.EquipmentHLVL1Skus.containsKey('CP') && !rollupEngine.DisposableHLVL2Skus.containsKey('CPC') && (isLease || !rollupEngine.DisposableHLVL2Skus.containsKey('RFB'))) {
                result.Messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'At least one consumable item should match the Capnography equipment(s) - ' + rollupEngine.EquipmentHLVL1Skus.get('CP') + '.'));
                result.isSuccess = false;
            }
        }
        return result;
    }
    
    global Apttus_Config2.CustomClass.ValidationResult validateRampLineItems(Apttus_Config2.ProductConfiguration cart, List<Apttus_Config2.LineItem> rampLineItems) {
        Apttus_Config2.CustomClass.ValidationResult result = new Apttus_Config2.CustomClass.ValidationResult(true);
        return result;
    }
    
    global Apttus_Config2.CustomClass.ValidationResult validateAssetItems(Apttus_Config2.ProductConfiguration cart, List<Apttus_Config2__TempRenew__c> assetItems) {
        Apttus_Config2.CustomClass.ValidationResult result = new Apttus_Config2.CustomClass.ValidationResult(true);
        return result;
    }
    
    private String rootListToString(List<Partner_Root_Contract__c> roots) {
        String s = '';
        for (Partner_Root_Contract__c r : roots) {
            if (String.isEmpty(s)) {
                s = r.Name;
            } else {
                s = s + ', ' + r.Name;
            }
        }
        
        return s;
    }
}