/****************************************************************************************
 * Name    : Case_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 19/12/2013 
 * Purpose : Contains all the logic coming from Case trigger
 * Dependencies: CaseTrigger Trigger
 *             , Case Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        		AUTHOR               	CHANGE
 * 09/01/1014  		Bill Shan            	Set AccountID of the case to null when the accont creation case is closed. 
 *                                  		This is to remove the unauthorized distributor account visibility from distributor users.
 * 3/31/14      	Gautam Shah         	Commented out SOQL query to RecordType and called Utilities instead (line 29, 89)
 * 6/1/2015 		Bill Shan				Add logic to set entitlement for EMEA EM CS cases
 * 4/22/2015    	Gaurav Gulanjkar    	Add logic to set recordType as consignment for Email - ZA - Enquiry
 * May 13, 2015		Gautam Shah				Moved the logic from the methods in this class to static methods in Case_Main.cls
 *****************************************************************************************/

public without sharing class Case_TriggerHandler {
//
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    private static Map<String,Id> mapEntitlements = new Map<String,Id>();

    static {
        
        for(Entitlement e : [Select Id, Name, CS_Center__c from Entitlement])
        {
        	if(!mapEntitlements.containsKey(e.CS_Center__c+e.Name))
        	{
        		mapEntitlements.put(e.CS_Center__c+e.Name, e.Id);
        	}
        }
    }
    
    public Case_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void OnBeforeInsert(List<Case> newObjects)
    {
    	Case_Main.setSouthAfricaAttributes(newObjects);
		Case_Main.setExportOrderCaseType(newObjects);
		Case_Main.setSalesOutEmail(newObjects);
		Case_Main.DefaultEntitlement3(newObjects);
    
    /*	
    	String caseRTName; 
    	for(Case c : newObjects){
    		
    		caseRTName = Utilities.recordTypeMap_Id_Name.get(c.RecordTypeId);
    		//ZA CS center cases
    		if(((c.Origin!=null && c.Origin.startsWith('Email - ZA')) || Utilities.CurrentUser.Country == 'ZA') && 
    			Label.ZACSCenterCaseRTs.contains(caseRTName)){
    				
    			c.CS_Center__c = 'ZA';
    			c.BusinessHours = Utilities.businessHourMap_Name_Id.get('ZA CS');
    			setEntitlement(c,caseRTName);
    		
    			if(UserInfo.getUserRoleId()==Label.ZACSAgentRoleID && c.Status =='New')
    				c.Status='In Process';
             	   
                if(c.Origin == 'Email - ZA - Enquiry' && 
                   ((c.Description != null && (c.Description.containsIgnoreCase('credit') || c.Description.containsIgnoreCase('uplift') || c.Description.containsIgnoreCase('up lift'))) ||
                    (c.Subject!=null && (c.Subject.containsIgnoreCase('credit') || c.Subject.containsIgnoreCase('uplift') || c.Subject.containsIgnoreCase('up lift')))
                   ))
                {
                    c.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Returns');
                }
                if(c.origin =='Email - ZA - Enquiry' &&
                        ((c.Description != null && (c.Description.containsIgnoreCase('Consignment') || c.Description.containsIgnoreCase('Consign'))) ||
                    	(c.Subject!=null && (c.Subject.containsIgnoreCase('Consignment') || c.Subject.containsIgnoreCase('Consign')))
                   		))
                {
                    c.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Consignment');
                }    
    		}
    	}
    */
    }
    
    public void OnAfterInsert(List<Case> newObjects)
    {
    	Case_Main.CaseTeamBuild(newObjects);
	/*
        //Set Export Order Case record type
        If(BatchSize==1 && newObjects[0].Origin=='Email - ZA - Enquiry' && newObjects[0].AccountId != null)
        {
            Case c = [Select Id, Account.RecordType.DeveloperName, RecordTypeId From Case where Id = :newObjects[0].Id limit 1];
            if(c.Account.RecordType.DeveloperName == 'EU_Distributor')
            {
                c.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Export_Order');
                update c;
            }
        }
        //Set Sales out admin email address
        else
        {
            ID uploadInvoiceRTID = Utilities.recordTypeMap_DeveloperName_Id.get('Upload_Invoice_Request');
            
            Set<ID> accIDs = new Set<ID>();
            Set<ID> caseIDs = new Set<ID>();
            for(Case c : newObjects)
            {
                if(c.RecordTypeId == uploadInvoiceRTID)
                {
                    accIDs.add(c.AccountId);
                    caseIDs.add(c.Id);
                }
            }
            
            if(caseIDs.size()>0)
            {
                Map<ID, String> acc_SOAEmail = new Map<ID, String>(); 
                if(accIDs.size()>0)
                {
                    for(Convidien_Contact__c cc : [SELECT Distributor_Name__c, Email__c FROM Convidien_Contact__c WHERE Type__c = 'Sales Out Administrator' 
                                                    and Distributor_Name__c = :accIDs])
                    {
                        if(!acc_SOAEmail.containsKey(cc.Distributor_Name__c))
                        {
                            acc_SOAEmail.put(cc.Distributor_Name__c, cc.Email__c);
                        }
                    }
                }
                
                Map<String,String> country_email = new Map<String,String>();
                for(Country_Default_Sales_Out_Admin__c defaultAdmin : Country_Default_Sales_Out_Admin__c.getall().values())
                    if(!country_email.containsKey(defaultAdmin.Name))
                        country_email.put(defaultAdmin.Name, defaultAdmin.Email__c);
                
        		List<Case> casesToUpdate = new List<Case>();
                
                for(Case c : [SELECT AccountId, Billing_Country__c, Sales_Out_Admin_Email__c FROM Case WHERE Id = :caseIDs])
                {
                    if(c.AccountId!=null && acc_SOAEmail.containsKey(c.AccountId))
                    {
                        c.Sales_Out_Admin_Email__c = acc_SOAEmail.get(c.AccountId);
                        casesToUpdate.add(c);
                    }
                    else if(c.Billing_Country__c!=null && country_email.containsKey(c.Billing_Country__c))
                    {
                        c.Sales_Out_Admin_Email__c = country_email.get(c.Billing_Country__c);
                        casesToUpdate.add(c);
                    }
                    else if(country_email.containsKey('Master'))
                    {
                        c.Sales_Out_Admin_Email__c = country_email.get('Master');
                        casesToUpdate.add(c);
                    }
                }
                
                if(casesToUpdate.size()>0)
                    update casesToUpdate;
            }
        }
	*/
    }
 
    public void OnBeforeUpdate(List<Case> newObjects, Map<Id, case> oldMap)
    {
    	Case_Main.setEntitlement(newObjects, oldMap);
		Case_Main.setMasterAccount(newObjects);
		Case_Main.DefaultEntitlement3(newObjects);
    /*	
        Set<String> accIDs = new Set<String>();
        Map<String, Account> accMaps = new Map<String, Account>();
        Id accountCreationRT = Utilities.recordTypeMap_DeveloperName_Id.get('Account_Creation_Case');
                
        for(Case c:newObjects)
        {
        	if(c.RecordTypeId != oldMap.get(c.Id).RecordTypeId)
        	{
        		setEntitlement(c,Utilities.recordTypeMap_Id_Name.get(c.RecordTypeId));
        	}
        	
            if(c.RecordTypeId == accountCreationRT && c.Master_Account_Number__c != null)
            {
                if(!accIDs.contains(c.Temporary_Account_Number__c))
                    accIDs.add(c.Temporary_Account_Number__c);
                    
                if(!accIDs.contains(c.Master_Account_Number__c))
                    accIDs.add(c.Master_Account_Number__c);
            }
        }
        
        if(accIDs.size()>0)
        {
            for(Account acc : [SELECT Id, Account_External_ID__c,Business_Registration_Number__c,phone,Comments__c,Country__c,type,Email_Address__c,Website  
                               FROM Account WHERE Account_External_ID__c in :accIDs])
            {
                accMaps.put(acc.Account_External_ID__c, acc);
            }   
            
            for(Case c : newObjects)
            {               
                if(c.RecordTypeId == accountCreationRT && c.Master_Account_Number__c != null && c.Master_Account_Number__c != '')
                {
                    if(accMaps.containsKey(c.Master_Account_Number__c))
                    {   
                        c.Master_Account__c = accMaps.get(c.Master_Account_Number__c).Id;
                        
                        if((c.status=='Closed-No Action Necessary' || c.status=='Closed-Resolved') && c.Master_Account_Number__c != c.Temporary_Account_Number__c)
                        {               
                            if(accMaps.get(c.Temporary_Account_Number__c) != null){
                            mergeTempAccount(accMaps.get(c.Master_Account_Number__c),accMaps.get(c.Temporary_Account_Number__c));
                            c.AccountId = c.CreatedByAccountID__c;
                            }
                        }   
                    }
                    else
                    {
                        c.Master_Account_Number__c.addError(Label.Account_Code_Not_Valid);
                    }
                }
            }
        }
	*/
    }
    
    public void OnAfterUpdate(List<Case> newObjects, Map<Id, case> oldMap)
    {
    	Case_Main.setCaseMilestones(newObjects, oldMap);
		Case_Main.CaseTeamBuild(newObjects);
	/*
        Set<Id> acceptedCaseIDs = new Set<Id>(); 
        Set<Id> resolvedCaseIDs = new Set<Id>(); 
        Set<Id> warehouseCompleteCaseIDs = new Set<Id>();
        for(Case c:newObjects)
        {
        	if(c.EntitlementId != null)
        	{
        		if(oldMap.get(c.Id).Case_Owner_Text__c != null && oldMap.get(c.Id).Case_Owner_Text__c != '' && c.Case_Owner_Text__c == null)
	        	{
	        		acceptedCaseIDs.add(c.Id);
	        	}
	        	if(c.IsClosed==true && oldMap.get(c.Id).isClosed==false)
	        	{
	        		resolvedCaseIDs.add(c.Id);
	        	}
	        	if(oldMap.get(c.Id).Awaiting_Response_From__c=='Warehouse' && c.Awaiting_Response_From__c != 'Warehouse')
	        	{
	        		warehouseCompleteCaseIDs.add(c.Id);
	        	}
        	}
        }   
        
        List<CaseMilestone> milestonesToUpdate = new List<CaseMilestone>();  
                
        if(acceptedCaseIDs.size()>0)
        {
        	List<CaseMilestone> acceptedMilestones = [Select CompletionDate from CaseMilestone 
        			where CaseId = :acceptedCaseIDs and IsCompleted = false and 
        				MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Acceptance Time')];
        			
        	for(CaseMilestone cm : acceptedMilestones)
        	{
        		cm.CompletionDate = DateTime.now();
        	}
        	
        	milestonesToUpdate.addAll(acceptedMilestones);
        }   
                
        if(resolvedCaseIDs.size()>0)
        {
        	List<CaseMilestone> resolvedMilestones = [Select CompletionDate from CaseMilestone 
        			where CaseId = :resolvedCaseIDs and IsCompleted = false and 
        				MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Resolution Time')];
        			
        	for(CaseMilestone cm : resolvedMilestones)
        	{
        		cm.CompletionDate = DateTime.now();
        	}
        	
        	milestonesToUpdate.addAll(resolvedMilestones);
        }
                
        if(warehouseCompleteCaseIDs.size()>0)
        {
        	List<CaseMilestone> warehouseMilestones = [Select CompletionDate from CaseMilestone 
        			where CaseId = :warehouseCompleteCaseIDs and IsCompleted = false and 
        				MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Warehouse Response')];
        			
        	for(CaseMilestone cm : warehouseMilestones)
        	{
        		cm.CompletionDate = DateTime.now();
        	}
        	
        	milestonesToUpdate.addAll(warehouseMilestones);
        }
        if(milestonesToUpdate.size()>0)
        	update milestonesToUpdate;
	*/
    }
/*    
    private void mergeTempAccount(Account masterAcc, Account tempAcc)
    {
        if(tempAcc.phone != null)   
        masterAcc.Phone = tempAcc.phone;
        if(tempAcc.Country__c != null)  
        masterAcc.Country__c = tempAcc.Country__c ;
        if(tempAcc.Website!= null)  
        masterAcc.Website = tempAcc.Website;
        if(tempAcc.EMail_address__c!= null)  
        masterAcc.Email_address__c = tempAcc.EMail_address__c;
        if(tempAcc.Type!= null)  
        masterAcc.Type = tempAcc.Type;
        merge masterAcc tempAcc;
    }
    
    private void setEntitlement(Case c, String strRTName)
    {
    	if(mapEntitlements.containsKey(c.CS_Center__c+strRTName))
    		c.EntitlementId = mapEntitlements.get(c.CS_Center__c+strRTName); 
    }
*/
}