/*

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-08-05      Bryan Fry           Created
2016-11-02      Isaac Lewis         Added cpqConfigSetting_cTest to test methods
===============================================================================
*/

@isTest
private class Test_CPQ_SSG_Agreements {
    /* Will not work in production until correct user and territory data is setup
    @isTest static void Test_CPQ_SSG_Agreement_Approvers() {
        RecordType scrubRT = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,'Scrub_PO');
        User testUser = [Select Id, ManagerId From User Where ManagerId != null And Id in (Select UserId From UserTerritory) And IsActive = true limit 1];
        User testOD = [Select Id From User Where ManagerId != null And Id in (Select UserId From UserTerritory) And IsActive = true And Id = :testUser.ManagerId Limit 1];
        UserTerritory userTerr = [Select Id, TerritoryId From UserTerritory Where UserId = :testOD.Id Limit 1];
        Territory terr = [Select Id, Custom_External_TerritoryID__c From Territory Where Id = :userTerr.TerritoryId Limit 1];
        CPQ_SSG_OD_Assignments__c odAssignment = new CPQ_SSG_OD_Assignments__c(CPQ_SSG_OD_Analyst__c = testOD.Id, External_Territory_Id__c = terr.Custom_External_TerritoryID__c);
        insert odAssignment;

        Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1');
        insert product;
        Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
        insert acct;
        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;
        Opportunity opp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acct.Id);
        insert opp;

        Test.startTest();
            Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(OwnerId = testUser.Id, Name = 'Test Agreement', Apttus__Related_Opportunity__c = opp.Id, Apttus__Account__c = acct.Id, RecordTypeId = scrubRT.Id, Scrub_PO_Type__c = 'Non-Transfer', Primary_Contact2__c = c.Id, SSG_Product_Usage__c = 'Clinically by the hospital', Apttus_Approval__Approval_Status__c = 'Approved', Apttus__Status__c = 'Requested');
            insert agreement;

            agreement.OD_Approver__c = null;
            Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num++;
            update agreement;
        Test.stopTest();
    }
    */

    @isTest static void Test_CPQ_SSG_Agreement_ERP() {

        //////insert cpqConfigSetting_cTest.create();
		//////cpqConfigSetting_c.reload();

        RecordType scrubRT = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,'Scrub_PO');

        Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1');
        insert product;
        Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US', Account_External_ID__c = 'TEST_EXTERNAL');
        insert acct;
        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;
        Opportunity opp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acct.Id);
        insert opp;

        List<ERP_Account__c> erps = new List<ERP_Account__c>();
        erps.add(new ERP_Account__c(Name = 'Test ERP', RMS_Total_Prior_12_Mos_Sales1__c = 100, Parent_Sell_To_Account__c = acct.Id, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1'));
        erps.add(new ERP_Account__c(Name = 'Test ERP', RMS_Total_Prior_12_Mos_Sales1__c = 200, Parent_Sell_To_Account__c = acct.Id, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1'));
        insert erps;

        Test.startTest();
            Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Related_Opportunity__c = opp.Id, RecordTypeId = scrubRT.Id, Scrub_PO_Type__c = 'Non-Transfer', Primary_Contact2__c = c.Id, SSG_Product_Usage__c = 'Clinically by the hospital', Apttus_Approval__Approval_Status__c = 'Approved', Apttus__Status__c = 'Requested');
            insert agreement;

            Apttus_Agreement_Trigger_Manager.UpdateERPBySales_Exec_Num++;
            agreement.Apttus__Account__c = acct.Id;
            update agreement;
        Test.stopTest();
    }

    @isTest static void Test_CPQ_SSG_Controller_ScrubPOLots_LI() {

      //////  insert cpqConfigSetting_cTest.create();
      //////  cpqConfigSetting_c.reload();

      // Configure Product Data

      Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

      Product2 testProduct = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1');
      insert testProduct;

      Apttus_Config2__PriceListItem__c testPriceListItem = new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProduct.ID, Apttus_Config2__Active__c = true);
      insert testPriceListItem;

      // Configure Account Data

      Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US', Account_External_ID__c = 'TEST_EXTERNAL');
      insert acct;

      Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
      insert c;

      // Configure Agreement Data

      Opportunity testOpp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acct.Id);
      insert testOpp;

      RecordType scrubRT = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,'Scrub_PO');

      Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Related_Opportunity__c = testOpp.Id, RecordTypeId = scrubRT.Id, Scrub_PO_Type__c = 'Non-Transfer', Primary_Contact2__c = c.Id, SSG_Product_Usage__c = 'Clinically by the hospital', Apttus_Approval__Approval_Status__c = 'Approved', Apttus__Status__c = 'Requested');
      insert testAgreement;

      // Configure Cart Data

      Apttus_Config2__ProductConfiguration__c testConfig = new Apttus_Config2__ProductConfiguration__c(Apttus_CMConfig__AgreementId__c = testAgreement.ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__Status__c = 'Finalized', Apttus_Config2__VersionNumber__c = 1);
      insert testConfig;

      Apttus_Config2__LineItem__c li = new Apttus_Config2__LineItem__c(Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ConfigurationId__c = testConfig.ID, Apttus_Config2__ProductId__c = testProduct.ID, Apttus_Config2__PriceListItemId__c = testPriceListItem.ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1, Apttus_Config2__Quantity__c = 5);
      insert li;

      Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Apttus__AgreementId__c = testAgreement.Id, Apttus_CMConfig__DerivedFromId__c = li.Id);
      insert ali;

      Test.startTest();
        CPQ_SSG_Controller_ScrubPOLots con = new CPQ_SSG_Controller_ScrubPOLots(new ApexPages.StandardController(testAgreement));
        con.doCancel();
        li = [Select Id, Lot_Number__c From Apttus_Config2__LineItem__c Where Id = :li.Id];
        System.assertEquals(null, li.Lot_Number__c);
        con.lineItems[0].Lot_Number__c = '12345';
        con.doSaveLotNumbers();
        li = [Select Id, Lot_Number__c, (SELECT Id, Lot_Number__c FROM Apttus_CMConfig__DerivedAgreementLineItems__r) From Apttus_Config2__LineItem__c Where Id = :li.Id];
        System.assertEquals('12345', li.Lot_Number__c);
        System.assertEquals('12345', li.Apttus_CMConfig__DerivedAgreementLineItems__r[0].Lot_Number__c);
      Test.stopTest();

    }


}