public abstract class sObject_v
{
    public enum Mode
    {
        Edit,
        View
    }

    public void addWarning(string error)
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, error));
    }
    
    public Map<string, string> getParameters() { return ApexPages.currentPage().getParameters(); }
    
    public Map<string, string> getHeaders() { return ApexPages.currentPage().getHeaders(); }

    public string tabLabel
    {
        get
        {
            String tabId = ApexPages.currentPage().getParameters().get('sfdc.tabName');

            for(Schema.DescribeTabSetResult dtsr : Schema.describeTabs())
                if(dtsr.isSelected())
                    if(String.isNotBlank(tabId))
                        for(Schema.DescribeTabResult dtr : dtsr.tabs)
                            if(String.isNotBlank(dtr.url) && dtr.url.contains(tabId))
                                return dtr.label;
            return null;
        }
    }

    public string baseURL
    {
        get
        {
            URL current = URL.getCurrentRequestUrl();
            return current.getProtocol() + '://' + current.getAuthority();
        }
    }
    
    public Mode determineMode()
    {
        return Mode.Edit;
    }
}