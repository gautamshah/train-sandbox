@isTest(SeeAllData=true)
public class IncoPPDCreation_Test 
{
	@isTest
    static void doTest()
    {
        Account a = [Select Id From Account Limit 1];
        Contact c = new Contact(AccountId = a.Id, FirstName = 'Inco', LastName = 'Tester', MailingCountry = 'US', 
                                Inco_Customer_Contact__c = TRUE, Email='inco@test.com', IncoPortalEmail__c = 'inco@test.com');
        insert c;
        User u = new User(ContactId = c.Id, Username = c.Email, FirstName = c.FirstName, LastName = c.LastName, Email = c.Email, communityNickname = c.LastName, 
                          Alias = c.FirstName, TimeZoneSidKey = 'America/New_York', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US',
                          ProfileId = Utilities.ProfileMap_Name_Id.get('Inco Customer'));
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = false;
        Database.insert(u, dlo);
        Inco_Item__c ii = new Inco_Item__c(Name = 'Test Item', Case_Qty__c = 6, Description__c = 'Test Desc', 
                                           Product_SKU__c = '01234', Series__c = 'Test Series', Size__c = 'Large');
		Inco_Product_PAR__c ipp = new Inco_Product_PAR__c(Name = 'Test Prod PAR', Account__c = a.Id, Daytime_PAR__c = 6, 
                                                          Nighttime_PAR__c = 6, Product_Family__c = 'Test Family', Product_Series__c = 'Test Series');
        
        insert ii;
        insert ipp;
        batchPPDCreation bpc = new batchPPDCreation();
        Test.startTest();
        Database.executeBatch(bpc);
        Test.stopTest();
        //
		//Incontinence_Submissions__c incSub = new Incontinence_Submissions__c(Name = 'Test Inco Sub', Account__c = a.Id, Submitted__c = False, 
		//			For_the_week_of__c = System.today(), Total_Residents_SKU__c = 50, Num_Residents_SKU_Day__c = 50, Num_Residents_SKU_Night__c = 50, Current_Cases_on_Hand__c = 10);
        //insert incSub;
        System.runAs(u)
        {
            PageReference pageRef = Page.PPDCreation;
            Test.setCurrentPage(pageRef);
            PPDCreationControllerFinal ppdcc = new PPDCreationControllerFinal();
            ppdcc.lstIncoPPD[0].Total_Residents_SKU__c = 50;
            ppdcc.lstIncoPPD[0].Num_Residents_SKU_Day__c = 50;
            ppdcc.lstIncoPPD[0].Num_Residents_SKU_Night__c = 50;
            ppdcc.lstIncoPPD[0].Current_Cases_on_Hand__c = 10;
            ppdcc.save();
            ppdcc.cancel();
            //Incontinence_Submissions__c incSub2 = [Select Submitted__c From Incontinence_Submissions__c Where Id = :incSub.Id Limit 1];
        	System.assertEquals(True, ppdcc.lstAllIncoPPD[0].Submitted__c);
        }
    }
}