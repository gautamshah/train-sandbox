/*
Name    : apttusDeal_SalesRepLevel_u
Type    : Picklist
sObject : Apttus__APTS_Agreement__c,
          Apttus_Proposal__Proposal__c
          
Author  : Paul Berglund
Date    : 10/11/2016
Purpose : Define supporting properties for the SalesRepLevel field
     
========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE
----        ------            ------
*/
public class cpqDeal_SalesRepLevel_u
{
    // Assign the SalesRepLevel based on the Owner's SalesOrganizationLevel
    public static void assign(
    	List<sObject> newList,
    	boolean isInsert,
    	boolean isUpdate,
    	boolean isBefore)
    {
    	system.debug('cpqDeal_SalesRepLevel_u.assign.newList: ' + newList);
    	
    	if ((isInsert || isUpdate) && isBefore)
    	{
			Set<Id> ownerIds = new Set<Id>();
			for(sObject obj : newList)
				ownerIds.add((Id)obj.get('OwnerId'));
				
    	system.debug('cpqDeal_SalesRepLevel_u.assign.ownerIds: ' + ownerIds);
    	
			Map<Id, User> owners = cpqUser_u.fetch(ownerIds);
			
    	system.debug('cpqDeal_SalesRepLevel_u.assign.owners: ' + owners);
    	
			for(sObject obj : newList)
			{
				Id ownerId = (Id)obj.get('OwnerId');
				if (owners.containsKey(ownerId))
				{
					User owner = owners.get(ownerId);
					string sol = owner.SalesOrganizationLevel__c;
					integer ndx = SalesOrganizationLevel_g.convert(sol);	
					obj.put('SalesRep_Level__c', convert(ndx));
					
    	system.debug('cpqDeal_SalesRepLevel_u.assign.sobj: ' + obj);
    	
				}
			}
    	}    	
	}

	@testVisible
	private static integer defaultCode = 0;
	
	@testVisible
	private static string defaultText = 'Rep';
	
	@testVisible
	private static Map<integer, string> Code2Text =
		new Map<integer, string>
	{
		defaultCode => defaultText,
   		1 => 'RSM',
   		2 => 'ZVP',
   		3 => 'SVP'
	};
	
	@testVisible
	private static Map<string, integer> Text2Code =
		new Map<string, integer>();
	
	static
	{
		for(integer key : Code2Text.keySet())
			Text2Code.put(Code2Text.get(key), key);
	}
	
	// Convert Code to Text
	public static string convert(integer level)
	{
		return (Code2Text.containsKey(level)) ?
				Code2Text.get(level) :
				defaultText;
	}

	// Convert Text to Code
	public static integer convert(string level)
	{
		return (Text2Code.containsKey(level)) ?
				Text2Code.get(level) :
				defaultCode;
	}	
}