/****************************************************************************************
Name    : Class: TaskConfigSetting
Author  : Paul Berglund
Date    : 03/08/2016
Purpose : Wraps the TaskConfigSetting__c object so every user has Read access

Dependancies: 
  Object:  TaskConfigSetting__c
     
========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE
----        ------            ------
03/08/2016  Paul Berglund     Moved CPQ logic into TaskConfigSetting
05/03/2016  Paul Berglund     Added isCPQuser method - only executes initialization
                              time per transaction, should fix issue reported by COE
05/11/2016  Paul Berglund     Changed profileMap and recTypeMap to static maps - only
                              needs to be populated 1 time
*****************************************************************************************/
public without sharing class TaskConfigSetting
{
    @testVisible
	private static final string DEFAULT_CONFIG_RECORD_NAME = 'System Properties';
		
	private static final string DEFAULT_USER_PROFILE_NAMES =
		'-All' + CONSTANTS.CRLF + // This clears the array initially
		'+APTTUS - RMS US Sales (RS+PM)' + CONSTANTS.CRLF +
		'+APTTUS - SSG US Account Executive';
	
	private static final Set<string> ADMIN_PROFILE_NAMES = new Set<string>
	{
		'System Administrator',
		'CRM Admin Support'
	};

	private static final Map<Id, Profile> profileMap
		= new Map<Id, Profile>(
			[SELECT Id,
			        Name
			 FROM Profile]);

    private static Map<Id, RecordType> recTypeMap =
    	RecordType_u.fetch(Apttus__APTS_Agreement__c.class);
        
    // Set this to True before you call any of the
    // public methods if you want the Exceptions
    // to make it to your code.  It defaults to
    // False if you are running a test.
    //
    //This property will return True under two
    // conditions:
    //    Previously set to True (either before
    //    a public method is called or set by the getter)
    //    Not Previously set and Not Running a Test
    // Otherwise it returns False
    @testVisible
    private static boolean throwException
    {
        get
        {
        	if (throwException == null)
                throwException = !Test.isRunningTest();
            return throwException;
        }
        set;
    }
    
    private static void ThrowException(string msg)
    {
    	if (throwException)
    		throw new LocalException(msg);
    }
    
    private static void ThrowException(Exception ex)
    {
    	if (throwException)
    		throw ex;
    }
    
    // New method to determine if this is a CPQ user
    public static boolean isCPQuser
    {
    	get
    	{
           	system.debug('TaskConfigSetting.isCPQuser ' + Limits.getQueries());
    		if (isCPQuser == null)
    		{
    			// To determine this, I need to see if this
    			// user's Profile is in the list of Profile names
    			// found in the System Properties record in
    			// the TaskConfigSetting object.
    			Initialize();
    						
				// Extract and check supported profiles
    	        try
    	        {
        	    	Set<Id> profileIds
        	    		= ProfileIds('TaskConfigSetting.isCPQuser', obj.CPQ_Profile_s__c);
	        	    if (profileIds.contains(UserInfo.getProfileId()))
	            		isCPQuser = true;
	            	else
	            		isCPQuser = false;
            	}
            	catch (Exception ex)
            	{
            		isCPQuser = false;
            		// TODO:
            		// Since we are hiding that there was a bad Profile name in
            		// the System Properites record, we should create a Case
            	}
    		}
    		return isCPQuser;
    	}
    	private set;
    }
    
	// This class has no public constructors, just properties associated
	// with the application being supported
    public static CPQ CPQ
    {
        get
        {
            if (isCPQuser)
            {
            	if (CPQ == null)
            	{
            		CPQ = new CPQ();
            	}
            }
            return CPQ;
        }
        private set;
    }
    
    @testVisible
    private static void Reset()
    {
    	isCPQuser = null;
    	CPQ = null;
    	obj = null;
    }

	@testVisible
	private static void Clear()
	{
       	system.debug('TaskConfigSetting.Clear ' + Limits.getQueries());
		List<TaskConfigSetting__c> targets
			= new List<TaskConfigSetting__c>(
				[SELECT Id
			     FROM TaskConfigSetting__c
			     WHERE Name = :DEFAULT_CONFIG_RECORD_NAME]);
		delete targets;
	}    

    // Everything is private from here on
    private class LocalException extends Exception { }

    private static string exceptionMessage
    	= '<fieldCount> \'' +
          '<field>\'' +
          ' set in the \'' +
          '<section>\' section of TaskConfigSetting does not exist: ' +
          '<values>';
                                          
    private static string singleFieldValue = 'The';
    private static string multiFieldValue = 'At least one of the';
    
    @testVisible
    private static TaskConfigSetting__c obj = null;
    
    private static void Initialize()
    {
        Initialize(DEFAULT_CONFIG_RECORD_NAME);
    }

    private static void Initialize(string name)
    {
    	system.debug('TaskConfigSetting.Initialize ' + Limits.getQueries());
    	if (obj == null || obj.Name != name) // If it doesn't exist or the record name is different
    	{
	        List<TaskConfigSetting__c> records
	            = [SELECT Id,
	               		  Name,
	                      CPQ_Profile_s__c,
	                      Org_Wide_Address_Display_Name__c,
	                      Notify_Owner_when_CD_done_Rcrd_Type_s__c,
	                      Notify_Owner_when_CD_done_Subject_s__c,
	                      Notify_Owner_when_CD_done_Template__c,
	                      Notify_CS_when_Agrmnt_Actvtd_Assigned__c,
	                      Notify_CS_when_Agrmnt_Actvtd_Template__c,
	                      Notify_CS_when_Agrmnt_Actvtd_To_s__c,
	                      Notify_CS_when_Agrmnt_Actvtd_Rcrd_Type__c,
	                      Notify_Owner_Ag_Implemented_Template__c,
	                      Notify_Owner_Ag_Implemented_Rcrd_Type__c
	                 FROM TaskConfigSetting__c
	                 WHERE Name = :Name
	                 ORDER BY Id DESC];

	        // This query will return 0, 1, or more records
	        // If it's 0 we add a default System Properties record
	        // If it's 1, great, keep going
	        // If it's 2 or more, there is a n issue, but we will keep
	        // going and use the latest record
	        if (records.size() == 0)
	        	obj = CreateSystemProperties();
	        else
	        	obj = records[0]; // Just use the latest record

			// If the System Properties record doesn't contain any profiles,
			// populate it with some defaults				
			if (obj.CPQ_Profile_s__c == null)
				InitializeProfiles();
				
			// Only need 1 call using upsert if we had
			// to create the obj and/or add profiles
			upsert obj;
    	}
    }

	@testVisible
	private static TaskConfigSetting__c CreateSystemProperties()
	{
		return new TaskConfigSetting__c(Name = DEFAULT_CONFIG_RECORD_NAME);
	}
	
	@testVisible
	private static void InitializeProfiles()
	{
		obj.CPQ_Profile_s__c = DEFAULT_USER_PROFILE_NAMES;
	}
	
    // Internal classes
    public class CPQ
    {
        public Settings Settings
        {
        	get
        	{
        		if (Settings == null) Settings = new Settings();
        		return Settings;
        	}
        	private set;
        }
        		
        public NotifyOwnerWhenCDDone NotifyOwnerWhenCDDone
        {
        	get
        	{
        		if (NotifyOwnerWhenCDDone == null)
        			NotifyOwnerWhenCDDone = new NotifyOwnerWhenCDDone();
        		return NotifyOwnerWhenCDDone;
        	}
        	private set;
        }

        public NotifyCSWhenAgreementActivated NotifyCSWhenAgreementActivated
        {
        	get
        	{
        		if (NotifyCSWhenAgreementActivated == null)
        			NotifyCSWhenAgreementActivated = new NotifyCSWhenAgreementActivated();
        		return NotifyCSWhenAgreementActivated;
        	}
        	private set;
        }
        
        public NotifyOwnerWhenAgreementImplemented NotifyOwnerWhenAgreementImplemented
        {
        	get
        	{
        		if (NotifyOwnerWhenAgreementImplemented == null)
        			NotifyOwnerWhenAgreementImplemented = new NotifyOwnerWhenAgreementImplemented();
        		return NotifyOwnerWhenAgreementImplemented;
        	}
        	private set;
        }
    }

    public class Settings
    {
        string section = 'CPQ - Fields Used Globally';
        string field;
	
        public Set<Id> ProfileIds
        {
        	get
        	{
        		if (ProfileIds == null)
        			ProfileIds = ProfileIds(section, obj.CPQ_Profile_s__c);
        		return ProfileIds;
        	}
        	private set;
        }
        
        public Id OrgWideEmailAddressId
        {
        	get
        	{
        		if (OrgWideEmailAddressId == null)
        		{
		            field = 'Org Wide Email Address - Display Name';
		            
		            if(obj.Org_Wide_Address_Display_Name__c != null)
		            {
		                try
		                {
		                	system.debug('TaskConfigSetting.OrgWideEmailAddressId ' + Limits.getQueries());
		                	
		                    OrgWideEmailAddress owea
		                    	= [SELECT Id
		                           FROM OrgWideEmailAddress
		                           WHERE DisplayName = :obj.Org_Wide_Address_Display_Name__c
		                           LIMIT 1];
		                                                
		                    OrgWideEmailAddressId = owea.Id;
		                }
		                catch (QueryException ex)
		                {
		                	string msg = exceptionMessage
		                				 .replace('<fieldCount>', singleFieldValue)
		                				 .replace('<field>', field)
		                				 .replace('<section>', section)
		                				 .replace('<values>', obj.Org_Wide_Address_Display_Name__c);
		                    ThrowException(msg);
		                }
		            }
		            else
		            {
		            	string msg = 'No \'' +
		            				 field +
		            				 '\' set in the \'' +
		            				 section +
		            				 '\' section of TaskConfigSetting';
		            	ThrowException(msg);
		            }
        		}
        		return OrgWideEmailAddressId;
        	}
        	private set;
        }
    }
    
    public class NotifyOwnerWhenCDDone
    {
        string section = 'NotifyOwnerWhenCDDone';

        public Id TemplateId
        {
        	get
        	{
	            if (TemplateId == null) TemplateId =
	            	TemplateId(section, obj.Notify_Owner_when_CD_done_Template__c);
	            return TemplateId;
        	}
        	private set;
        }            	

		public Set<Id> RecordTypeIds
		{
			get
			{
	            if (RecordTypeIds == null) RecordTypeIds =
	            	RecordTypeIds(section, obj.Notify_Owner_when_CD_done_Rcrd_Type_s__c);
				return RecordTypeIds;
			}
			private set;
        }        

        public Set<string> Subjects
        {
        	get
        	{
        		if (Subjects == null)
        			Subjects = DATATYPES.GetValues(obj.Notify_Owner_when_CD_done_Subject_s__c, null);
        		return Subjects;
        	}
        	private set;
        }
    }

    public class NotifyCSWhenAgreementActivated
    {
        string section = 'NotifyCSWhenAgreementActivated';
        string field;

        public Id AssignedToId
        {
        	get
        	{
				if (AssignedToId == null)
				{
					field = 'Assigned To';
		            if(obj.Notify_CS_when_Agrmnt_Actvtd_Assigned__c != null)
		            {
		                try
		                {
		                	system.debug('TaskConfigSetting.AssignedToId ' + Limits.getQueries());

		                    User user
		                    	= [SELECT Id
		                           FROM User
		                           WHERE Name = :obj.Notify_CS_when_Agrmnt_Actvtd_Assigned__c];
		                    AssignedToId = user.Id;
		                }
		                catch (QueryException ex)
		                {
				        	string msg
				        		= exceptionMessage
				        			.replace('<fieldCount>', singleFieldValue)
				        			.replace('<field>', field)
				        			.replace('<section>', section)
				        			.replace('<values>', obj.Notify_CS_when_Agrmnt_Actvtd_Assigned__c);
		                    ThrowException(msg);
		                }
		            }
		            else
		            {
		            	string msg = 'No \'' + field +
		            				 '\' set in the \'' + section +
		            				 '\' section of TaskConfigSetting';
		            	ThrowException(msg);
		            }
	            }
	            return AssignedToId;
            }
            private set;
        }

        public Set<string> ToAddresses
        {
        	get
        	{
        		if (ToAddresses == null)
        		{
               		ToAddresses
               			= DATATYPES.GetValues(obj.Notify_CS_when_Agrmnt_Actvtd_To_s__c, null);
               	}
                return ToAddresses;
			}
        	private set;
        }
		
        public Id TemplateId
        {
        	get
        	{
	            if (TemplateId == null) TemplateId =
	            	TemplateId(section, obj.Notify_CS_when_Agrmnt_Actvtd_Template__c);
	            return TemplateId;
        	}
        	private set;
        }            	

		public Set<Id> RecordTypeIds
		{
			get
			{
	            if (RecordTypeIds == null) RecordTypeIds =
	            	RecordTypeIds(section, obj.Notify_CS_when_Agrmnt_Actvtd_Rcrd_Type__c);
				return RecordTypeIds;
			}
			private set;
        }        
    }

    public class NotifyOwnerWhenAgreementImplemented
    {
        string section = 'NotifyOwnerWhenAgreementImplemented';
        string field;

        public Id TemplateId
        {
        	get
        	{
	            if (TemplateId == null) TemplateId = 
	            	TemplateId(section, obj.Notify_Owner_Ag_Implemented_Template__c);
	            return TemplateId;
        	}
        	private set;
        }            	

		public Set<Id> RecordTypeIds
		{
			get
			{
	            if (RecordTypeIds == null) RecordTypeIds =
	            	RecordTypeIds(section, obj.Notify_Owner_Ag_Implemented_Rcrd_Type__c);
				return RecordTypeIds;
			}
			private set;
        }        
    }
    
    //
    // 
    //
    private static Set<Id> ProfileIds(string section, string names)
    {
		string field = 'Profile Name(s)';

		Set<Id> ids = new Set<Id>();
		
		Set<string> profileNames = DATATYPES.GetValues(names, profileMap);
		for(Profile p : profileMap.values())
		{
			if (profileNames.contains(p.Name))
				ids.add(p.Id);
		}
		
		// This following check would only occur if one of the Profile names
		// in the System Properties was misspelled or didn't exist
		if(ids.size() != profileNames.size())
		{
		    List<string> values = new List<string>(profileNames);
		    values.sort();
		    string msg = exceptionMessage
		   			     .replace('<fieldCount>', multiFieldValue)
		   			     .replace('<field>', field)
		   			     .replace('<section>', section)
		   			     .replace('<values>', string.join(values, ', '));
            ThrowException(msg);
		}

		// The following prevents nulls from being added to the profileIds
		// set - if the Language is non-English, the System Administrator
		// profile will not be found AND if the User doesn't have
		// permission, the CRM Admin Support profile will not be returned
		for(Profile p : profileMap.values())
		{
			if (ADMIN_PROFILE_NAMES.contains(p.Name))
				ids.add(p.Id);
		}

	    return ids;
    }
    
    private static Set<Id> RecordTypeIds(string section, string names)
    {
	    string field = 'Record Type Name(s)';
	            
        Set<string> recordNames
        	= DATATYPES.GetValues(names, recTypeMap);

		Set<Id> ids = new Set<Id>();
		for(RecordType rt : recTypeMap.values())
		{
			if(recordNames.contains(rt.Name))
				ids.add(rt.Id);
		}

		// This following check would only occur if one of the Record Type names
		// in the System Properties was misspelled or didn't exist
		if(ids.size() != recordNames.size())
		{
		    List<string> values = new List<string>(recordNames);
		    values.sort();
		    string msg = exceptionMessage
		   			     .replace('<fieldCount>', multiFieldValue)
		   			     .replace('<field>', field)
		   			     .replace('<section>', section)
		   			     .replace('<values>', string.join(values, ', '));
            ThrowException(msg);
		}

       	return ids;
    }
    
    private static Id TemplateId(string section, string name)
    {
        string field = 'Template';
	            
        if(name != null)
        {
	        try
	        {
               	system.debug('TaskConfigSetting.TemplateId ' + Limits.getQueries());
	        	EmailTemplate template
	        		= [SELECT Id
	                   FROM EmailTemplate
	                   WHERE Name = :name];
	
		        return template.Id;
			}
	        catch (QueryException ex)
	        {
	        	string msg
	        		= exceptionMessage
	        			.replace('<fieldCount>', singleFieldValue)
	        			.replace('<field>', field)
	        			.replace('<section>', section)
	        			.replace('<values>', name);
	            ThrowException(msg);
	        }
        }
        else
        {
            string msg
            	= 'No \'' + field +
           		  '\' set in the \'' + section +
           		  '\' section of TaskConfigSetting';
            ThrowException(msg);
        }
        return null;
    }
}