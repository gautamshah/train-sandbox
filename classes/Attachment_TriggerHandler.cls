/****************************************************************************************
 * Name    : Attachment_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 02/04/2015 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 *****************************************************************************************/
public class Attachment_TriggerHandler {

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public Attachment_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void OnAfterInsert(List<Attachment> newObjects){
        
        Set<ID> emailMessageIDs = new Set<ID>();
        
        for(Attachment a : newObjects)
        {
            if(String.valueOf(a.ParentId).startsWith('02s') && 
               (a.ContentType.StartsWith('application/')))
            	emailMessageIDs.add(a.ParentId);
        }
        
        if(emailMessageIDs.size()>0)
        {
            Set<ID> caseIDs = new Set<ID>();
            for(EmailMessage em : [Select ParentId from EmailMessage Where Id = :emailMessageIDs and Incoming = true])
            {
                if(String.valueOf(em.ParentId).startsWith('500'))
                    caseIDs.add(em.ParentId);
            }
            
            if(caseIDs.size()>0)
            {
                ID enquiryRTId = Utilities.recordTypeMap_DeveloperName_Id.get('Enquiry');
                ID newOrderRTId = Utilities.recordTypeMap_DeveloperName_Id.get('New_Order');
                List<Case> cases2Update = new List<Case>();
                
                for(Case c : [Select Id, RecordTypeId, CreatedDate from Case where Id = :caseIDs and CS_Center__c = 'ZA' and 
                              RecordTypeId = :enquiryRTId and Origin in ('Email','Fax')])
                {
                    if(c.CreatedDate.addSeconds(10)>datetime.now())
                    {
                        c.RecordTypeId = newOrderRTId;
                        cases2Update.add(c);
                    }
                }
                if(cases2Update.size()>0)
                    update cases2Update;
            }
        }
        
    }
}