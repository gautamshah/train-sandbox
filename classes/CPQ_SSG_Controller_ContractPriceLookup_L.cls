/****************************************************************************************
 * Name    : CPQ_SSG_Controller_ContractPriceLookup_L
 * Author  : Isaac Lewis
 * Date    : 03/31/2016
 * Purpose : Server API methods for Lightning Component
 * Dependencies: CPQ_SSG_ContractPriceLookup.cmp
 * See: https://medtronic-mitg--cpq.lightning.force.com/auradocs/reference.app#reference?descriptor=c:CPQ_SSG_ContractPriceLookup&defType=component
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 04/04/2016  Isaac Lewis          Added qtyUnitPrice field for presentation of unitPrice (Price) / qty (Unit Qty)
 * 06/09/2016  Isaac Lewis          Modified getProductPrice filter criteria to evaluate business unit of "Surgical Innovations (SI)" as "S2"
 * 02/07/2016  Isaac Lewis          Replaced user query in getProductPrice with central User_u query.
 *****************************************************************************************/
public with sharing class CPQ_SSG_Controller_ContractPriceLookup_L {

	/**
	 * Fetches Account selected in component Account lookup field
	 *
	 * @param		accountId 	record Id for Account sObject
	 * @return		account			Account sObject
	 */
	@AuraEnabled
	public static Account getAccount(String accountId){
		return [SELECT Id, Name, Account_External_Id__c, Status__c, AccountType__c FROM Account WHERE Id = :accountId];
	}

	/**
	 * Fetches Product2 selected in component Product2 lookup field.
	 * Validates Product2 and attempts smart match due to lack of lookup filter.
	 * Wraps Product2 sObject in custom apex class to hold external pricing.
	 *
	 * @param			productId 						record Id for Product2 sObject
	 * @return			productPrice					wrapper class for Product2 sObject
	 * @exception	AuraHandledException	notifies client of unsuitable product
	 * @see        [Lookup Filter Criteria](https://cs9.salesforce.com/00NK0000001gB1m)
	 */
	@AuraEnabled
	public static ProductPrice getProductPrice(String productId){

		User usr = cpqUser_c.CurrentUser;
		Product2 prod = [SELECT Id, Name, ProductCode, Family, Apttus_Surgical_Product__c, UOM_Desc__c, GBU__c, Catalog_Price__c, Unit_Quantity__c FROM Product2 WHERE Id = :productId];
		Product2 replace;
		ProductPrice prodP;

		Boolean isMatchProduct = false;

		if( prod.Apttus_Surgical_Product__c &&
				// 2016.06.09 - Changed reference from prod.Family to prod.GBU__c
				( prod.GBU__c == usr.Business_Unit__c ||
				  ( prod.GBU__c == 'S2' &&
					  usr.Business_Unit__c == 'Surgical Innovations (SI)'
					)
				)
			){
			isMatchProduct = true;
		}

		if(!isMatchProduct){
			try {
				String pc = prod.ProductCode;
				String bu = usr.Business_Unit__c;

				String queryString = 'SELECT Id, Name, ProductCode, Apttus_Surgical_Product__c, UOM_Desc__c, Catalog_Price__c, Unit_Quantity__c FROM Product2';
				queryString += ' WHERE ProductCode = :pc AND Apttus_Surgical_Product__c = TRUE';
				if(usr.Business_Unit__c == 'Surgical Innovations (SI)'){
					queryString += ' AND GBU__c = \'S2\'';
				} else {
					queryString += ' AND GBU__c LIKE :bu';
				}
				queryString += ' LIMIT 1';
				System.debug('Query: ' + queryString);
				replace = (Product2) Database.query(queryString);

			} catch (QueryException qe){
				throw new AuraHandledException('The product "' + prod.Name + '" cannot be used');
				System.debug(qe);
			}
			if(replace != NULL){
				prod = replace;
			}
		}

		prodP = new ProductPrice(prod.Id, prod.Name, prod.ProductCode, prod.UOM_Desc__c, prod.Unit_Quantity__c, prod.Catalog_Price__c);
		return prodP;
	}

	/**
	 * Calls web service with account id and product codes to get prices
	 *
	 * @param  acct                Account sObject with populated External_Id__c
	 * @param  productList         ProductPrice list with populated product information
	 * @return List<ProductPrice>  ProductPrice list with pricing information added
	 */
	@AuraEnabled
	public static List<ProductPrice> getCalloutPricing(Account acct, String productListJSON) {

			List<ProductPrice> productList = (List<ProductPrice>) JSON.deserialize(productListJSON, List<ProductPrice>.class);
			System.debug('productList');
			System.debug(productList);

			if (productList.size() == 0) {
					return productList;
			}

			CPQ_ProxyMultiPriceService.PricingRequestType request;
			CPQ_ProxyMultiPriceService.PricingRespType response;
			List<CPQ_Error_Log__c> errors = new List<CPQ_Error_Log__c>();
			Map<String,ProductPrice> productSkuMap = new Map<String,ProductPrice>();

			// Populate Product SKU Map
			for(ProductPrice p : productList){
				productSkuMap.put(p.ProductCode, p);
			}

			// Attempt callout for pricing
			try {

					Integer requestId = Math.abs(Integer.valueOF((System.now().getTime() / 32767)));
					String accountCRN = acct.Account_External_Id__c.substring(3);
					CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();

					//  set the web service timeout
					service.timeout_x = 30000;
					request = new CPQ_ProxyMultiPriceService.PricingRequestType();
					request.countryField = 'US';
					request.currencyField = '';
					// request.designatorField = 'B';
					request.designatorField = 'D';
					request.gBUField = '';
					request.requestDateField = System.Today();
					request.requestDateFieldSpecified = true;
					request.requestIDField = String.valueOf(requestId);

					// One will get the lowest sequence number price, all will return multiple that need to be sorted through.
					request.ShowOnePriceField = 'One';
					//request.showOnePriceField = 'All';
					String u = userInfo.getUserName();
					request.userIDField = u.substring(0, u.indexOf('@'));

					request.shipToDSField = new CPQ_ProxyMultiPriceService.ArrayOfShipTo();
					request.shipToDSField.ShipTo = new CPQ_ProxyMultiPriceService.ShipTo[]{};

					CPQ_ProxyMultiPriceService.ShipTo shipTo = new CPQ_ProxyMultiPriceService.ShipTo();
					shipTo.shipTo1Field = accountCRN;
					request.shipToDSField.ShipTo.add(shipTo);

					request.sKUsField = new CPQ_ProxyMultiPriceService.ArrayOfSKUType();
					request.sKUsField.SKUType = new CPQ_ProxyMultiPriceService.SKUType[]{};

					List<String> codes = new List<String>(productSkuMap.keySet());
					Integer size = codes.size();
					for(integer i = 0; i < size; i++) {
							String key = codes[i];
							ProductPrice prd = productSkuMap.get(key);

							CPQ_ProxyMultiPriceService.SKUType t = new CPQ_ProxyMultiPriceService.SKUType();
							//t.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
							t.quantityField = 1;
							t.quantityFieldSpecified = true;
							t.sKUCodeField = key;
							t.uOMField = prd.uom == null ? 'EA' : prd.uom; //prod.UOM__c;

							request.sKUsField.SKUType.add(t);
					}

					System.Debug('*** request ' + request);

					if (Test.isRunningTest()) {
							response = Test_CPQ_PricingCallBack.createModkResponse();
					}
					else {
							response = service.MultiPrice(request);
					}

					System.Debug('*** response.PropertyChanged ' + response.PropertyChanged);
					System.Debug('*** response.countryField ' + response.countryField);
					System.Debug('*** response.currencyField ' + response.currencyField);
					System.Debug('*** response.designatorField ' + response.designatorField);
					System.Debug('*** response.gBUField ' + response.gBUField);
					System.Debug('*** response.requestDateField ' + response.requestDateField);
					System.Debug('*** response.requestDateFieldSpecified ' + response.requestDateFieldSpecified);
					System.Debug('*** response.requestErrorDescriptionField ' + response.requestErrorDescriptionField);
					System.Debug('*** response.requestErrorField ' + response.requestErrorField);
					System.Debug('*** response.requestIDField ' + response.requestIDField);
					System.Debug('*** response.showOnePriceField ' + response.showOnePriceField);
					System.Debug('*** response.userIDField ' + response.userIDField);

					for (CPQ_ProxyMultiPriceService.ShipToResp s : response.shipToDSField.ShipToResp) {
							System.Debug('*** s.PropertyChanged ' + s.PropertyChanged);
							System.Debug('*** s.billToField ' + s.billToField);
							System.Debug('*** s.shipToErrorDescriptionField ' + s.shipToErrorDescriptionField);
							System.Debug('*** s.shipToErrorField ' + s.shipToErrorField);
							System.Debug('*** s.shipToField ' + s.shipToField);

							for (CPQ_ProxyMultiPriceService.SKUTypeResp k : s.sKUsField.SKUTypeResp) {
									System.Debug('*** k.PropertyChanged ' + k.PropertyChanged);
									System.Debug('*** k.quantityField ' + k.quantityField);
									System.Debug('*** k.quantityFieldSpecified ' + k.quantityFieldSpecified);
									System.Debug('*** k.sKUCodeField ' + k.sKUCodeField);
									System.Debug('*** k.sKUErrorDescriptionField ' + k.sKUErrorDescriptionField);
									System.Debug('*** k.sKUErrorField ' + k.sKUErrorField);
									System.Debug('*** k.salesClassField ' + k.salesClassField);
									System.Debug('*** k.uOMField ' + k.uOMField);

									if (k.sKUErrorField != '0') {
											errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', k.sKUErrorField + ': ' + k.sKUErrorDescriptionField,
																																	'sKUCode: ' + k.sKUCodeField +
																																	', sKUUOM: ' + k.uOMField +
																																	', shipTo: ' + s.shipToField +
																																	', user: ' + request.userIDField,
																																	String.valueOf(request)));
											continue;
									}

									if (k.pricesField == null || k.pricesField.PriceTypeResp == null) {
											errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', 'No Price returned.',
																																	'sKUCode: ' + k.sKUCodeField +
																																	', sKUUOM: ' + k.uOMField +
																																	', shipTo: ' + s.shipToField +
																																	', user: ' + request.userIDField,
																																	String.valueOf(request)));
											continue;
									}

									for (CPQ_ProxyMultiPriceService.PriceTypeResp p : k.pricesField.PriceTypeResp) {
											System.Debug('*** p.PropertyChanged ' + p.PropertyChanged);
											System.Debug('*** p.contractDescriptionField ' + p.contractDescriptionField);
											System.Debug('*** p.contractDesignatorField ' + p.contractDesignatorField);
											System.Debug('*** p.contractNumberField ' + p.contractNumberField);
											System.Debug('*** p.currencyField ' + p.currencyField);
											System.Debug('*** p.effectiveDateField ' + p.effectiveDateField);
											System.Debug('*** p.endCustomerPriceField ' + p.endCustomerPriceField);
											System.Debug('*** p.endCustomerPriceFieldSpecified ' + p.endCustomerPriceFieldSpecified);
											System.Debug('*** p.expirationDateField ' + p.expirationDateField);
											System.Debug('*** p.gPONonCommittedPriceField ' + p.gPONonCommittedPriceField);
											System.Debug('*** p.gPONonCommittedPriceFieldSpecified ' + p.gPONonCommittedPriceFieldSpecified);
											System.Debug('*** p.gPOTierPriceField ' + p.gPOTierPriceField);
											System.Debug('*** p.gPOTierPriceFieldSpecified ' + p.gPOTierPriceFieldSpecified);
											System.Debug('*** p.gPOTierPriceRootContractField ' + p.gPOTierPriceRootContractField);
											System.Debug('*** p.markupField ' + p.markupField);
											System.Debug('*** p.markupFieldSpecified ' + p.markupFieldSpecified);
											System.Debug('*** p.pricContCommitmentTypeDescField ' + p.pricContCommitmentTypeDescField);
											System.Debug('*** p.pricContRootContractField ' + p.pricContRootContractField);
											System.Debug('*** p.priceErrorDescriptionField ' + p.priceErrorDescriptionField);
											System.Debug('*** p.priceErrorField ' + p.priceErrorField);
											System.Debug('*** p.quantityRangeField ' + p.quantityRangeField);
											System.Debug('*** p.sequenceNumberField ' + p.sequenceNumberField);
											System.Debug('*** p.sequenceNumberFieldSpecified ' + p.sequenceNumberFieldSpecified);
											System.Debug('*** p.unitPriceField ' + p.unitPriceField);

											ProductPrice prod = productSkuMap.get(k.sKUCodeField);
											prod.unitPrice = p.unitPriceField.setScale(2);
											prod.quantityRange = p.quantityRangeField;
											prod.contractNumber = p.contractNumberField;
											prod.description = p.contractDescriptionField;
											prod.directIndirect = p.contractDesignatorField;
											prod.effectiveDate = p.effectiveDateField;
											prod.expirationDate = p.expirationDateField;
											prod.qtyUnitPrice = prod.unitPrice / prod.qty;
											prod.qtyUnitPrice = prod.qtyUnitPrice.setScale(2);

									}
							}
					}
			}
			catch (Exception e) {
					errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', e.getMessage(),
																													'sKUCodes: ' + productSkuMap +
																													', shipTo: ' + Account.Account_External_ID__c, String.valueOf(request)));
					System.debug(e.getMessage());
			}

			// Return Product List
			System.debug(productSkuMap.values());
			return (List<ProductPrice>) productSkuMap.values();

	}

	/**
	 * Wrapper to contain info from Product2 sObject and external pricing.
	 *
	 * @See	CPQ_SSG_Controller_ContractPriceLookup.ProductPrice
	 */
	public class ProductPrice {

			@AuraEnabled public Id productId {get;set;}
			@AuraEnabled public String productName {get;set;}
			@AuraEnabled public String productCode {get;set;}
			@AuraEnabled public String uom {get;set;}
			@AuraEnabled public Decimal unitPrice {get;set;}
			@AuraEnabled public String quantityRange {get;set;}
			@AuraEnabled public String contractNumber {get;set;}
			@AuraEnabled public String description {get;set;}
			@AuraEnabled public String directIndirect {get;set;}
			@AuraEnabled public Datetime effectiveDate {get;set;}
			@AuraEnabled public Datetime expirationDate {get;set;}
			@AuraEnabled public Decimal qty {get;set;}
			@AuraEnabled public Decimal qtyUnitPrice {get;set;}
			@AuraEnabled public Decimal listPrice {get;set;}

			public ProductPrice(Id productId, String productName, String productCode, String uom, Decimal qty, Decimal listPrice) {
					this.productId = productId;
					this.productName = productName;
					this.productCode = productCode;
					this.uom = uom;
					this.qty = qty;
					this.listPrice = listPrice;
			}

	}

}