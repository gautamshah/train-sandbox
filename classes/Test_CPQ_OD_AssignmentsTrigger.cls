@isTest
private class Test_CPQ_OD_AssignmentsTrigger {
	
	@isTest static void Test_CPQ_OD_AssignmentsTrigger() {
		Territory t = [Select Id, Custom_External_TerritoryID__c, Description From Territory Where Custom_External_TerritoryID__c != null limit 1];
		CPQ_SSG_OD_Assignments__c odAssignment1 = new CPQ_SSG_OD_Assignments__c(External_Territory_Id__c = t.Custom_External_TerritoryID__c);
		CPQ_SSG_OD_Assignments__c odAssignment2 = new CPQ_SSG_OD_Assignments__c();
		Test.startTest();
			insert odAssignment1;
			odAssignment1 = [Select Id, Territory_Description__c From CPQ_SSG_OD_Assignments__c Where Id = : odAssignment1.Id];
			System.assertEquals(t.Description, odAssignment1.Territory_Description__c);
			try {
				insert odAssignment2;
			} catch (Exception e) {}
		Test.stopTest();
	}
}