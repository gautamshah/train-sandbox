public class cpqOfferDevelopmentAssignment_cache extends sObject_cache
{
	//// This is public so that it can be called by the appropriate
	//// _g constructor to instatiate 1 version of the _cache
	public static cpqOfferDevelopmentAssignment_cache get() { return cache; }
	
	//// Must be private to force the use of the get()
	//// The static cache variable is defined in this class so
	//// that you can have a separate instance of a cache for each
	//// sObject (if you did this in the sObject_cache class, you would
	//// overwrite the value everytime you created a new _cache
	private static cpqOfferDevelopmentAssignment_cache cache
	{
		get
		{
			if (cache == null)
				cache = new cpqOfferDevelopmentAssignment_cache();
				
			return cache;
		}
		
		private set;
	}
	
	//// Must be private to force the use of the get() and to make
	//// this a singleton using this classes cache variable
	private cpqOfferDevelopmentAssignment_cache()
	{
		super(CPQ_SSG_OD_Assignments__c.sObjectType, CPQ_SSG_OD_Assignments__c.field.Id);
		this.addIndex(CPQ_SSG_OD_Assignments__c.field.External_Territory_Id__c);
	}   
}