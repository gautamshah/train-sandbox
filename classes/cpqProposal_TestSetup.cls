@isTest
public class cpqProposal_TestSetup
{
    public static Apttus_Proposal__Proposal__c generateProposal(
            Account acc,
            Contact ct,
            Opportunity opp,
            String recordTypeName)
    {

        RecordType rt = cpqDeal_RecordType_c.getRecordTypesByName().get(recordTypeName);

        // Universal Agreement Information

        Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c ();
            // TODO: Add generic record information
            prop.Apttus_Proposal__Account__c = acc.Id;
            prop.Apttus_Proposal__Opportunity__c = opp.Id;
            prop.Primary_Contact2__c = ct.Id;
            prop.RecordTypeId = rt.Id;

        // Record-Type Specific Information
        if (rt.DeveloperName == CPQ_ProposalProcesses.QUICK_QUOTE_RT) {
            // TODO: Add record-type specific field information
        }

        return prop;

    }


}