/****************************************************************************************
* Name    : DG_Lead_Class
* Author  : DemandGen: Augusto Bisda
* Date    : 11/6/13
* Purpose : Assigns leads to reps from inbound integration with Eloqua, invoked by scheduled class: Eloqua_AssignLeads
* 
* Dependancies: 
*
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
*   DATE           AUTHOR                  CHANGE
*   ----            ------                  ------
*  5/13/14      Gautam Shah        Line 426-432, commented out the system based notification invocation because it's replaced by custom emails from class: LeadSchedule
*  6/17/14      Augusto Bisda      Changes on LeadHasInterest_OnUpdate and LeadHasInterest_OnInsert to assign newly created child leads
                      from countries defined in the Lead Country/Queue Mapping custom settings.
   10/23/15     Jason Rayner        Added code to copy Contact_Us_Reason__c to child lead
*****************************************************************************************/ 
public without sharing class DG_Lead_Class {
    
    public static void LeadHasInterest_OnUpdate(List<Lead> triggerNew,List<Lead> triggerOld)
    {
      List<String> flds = new DG_Lead_Class().ChildFields();
      
    Group WaitingAssignmentQueue = [Select g.id From Group g where type = 'Queue' and name = 'Waiting Assignment Queue' LIMIT 1];
      map<ID,Lead> mapPLead = new map<ID,Lead>(); 
      list<Lead> listPLead = new list<Lead>(); 
      map<string,Lead> mapChildOPG = new map<string,Lead>();
      
      integer i;
      for (i=0; i<triggerNew.size(); i++)
      {
        Lead o = triggerOld[i];
        Lead n = triggerNew[i];
        if (n.Is_Parent__c == true && o.Has_Interest__c == false && n.Has_Interest__c == true && n.Opportunity_Product_Group__c <> Null){
          mapPLead.put(n.Id,n); 
          listPLead.add(n);
        }
        n.Has_Interest__c = false;
      } 
      
      Map<Id,Lead> mapLead2Update = new Map<Id,Lead>();
        
        if (mapPLead.size()> 0) {
          //Retrieve custom setting related to mapping lead's country and queue mapping      
      map<String,String> mapLeadCountryQueue = getLeadCountryQueueMapping();
          
          //Retrieve existing children leads related to parent leads.                    
          for (Lead c : [Select 
            c.Id, 
            c.Parent_Lead__c,  
            c.Opportunity_Product_Group__c, 
            c.Product_Solution_of_Interest__c 
          From Lead c 
          WHERE c.Parent_Lead__c IN :mapPLead.keyset() 
            and c.Is_Parent__c = false 
            and c.Opportunity_Product_Group__c <> Null]){  
              String vKey = c.Parent_Lead__c + '~' + c.Opportunity_Product_Group__c;
          mapChildOPG.put(vKey,c);
            }
                    
          List<Lead> cLeadToCreate = new List<Lead>(); 
          
          for(Lead t : listPLead) 
          {   
            //Parse values separated by comma in the Opportunity Product Group field
        List<String> listOPG = t.Opportunity_Product_Group__c.split('\\,');

        //Loop thru the values in the Opportunity Product Group
        for (String o : listOPG) {
              String vKey = t.Id + '~' + o;
              if(mapChildOPG.containsKey(vKey)){
                //update Child Lead
              Lead cLeadMatch = mapChildOPG.get(vKey);
            
            mapLead2Update.put(cLeadMatch.Id, new Lead(Id=cLeadMatch.Id));
            for(String field:flds) {
                  try {
                    mapLead2Update.get(cLeadMatch.Id).put(field,t.get(field));
                  } catch(exception e) {}
              }
              mapLead2Update.get(cLeadMatch.Id).put('Product_Solution_of_Interest__c',AppendProductInterest(t.Most_Recent_Product_Solution_of_Interest__c ,cLeadMatch.Product_Solution_of_Interest__c));
              mapLead2Update.get(cLeadMatch.Id).put('Most_Recent_Product_Solution_of_Interest__c',t.Most_Recent_Product_Solution_of_Interest__c);

              } else {
                
              //create Child Lead
              Lead cLeadNew = new Lead();
              cLeadNew.Opportunity_Product_Group__c = o;
              cLeadNew.RecordTypeId = t.RecordTypeId;
              cLeadNew.Product_Solution_of_Interest__c = t.Most_Recent_Product_Solution_of_Interest__c ;
              cLeadNew.Most_Recent_Product_Solution_of_Interest__c = t.Most_Recent_Product_Solution_of_Interest__c;
              cLeadNew.Parent_Lead__c = t.id;
              cLeadNew.Contact_Us_Reason__c=t.Contact_Us_Reason__c;
              if(mapLeadCountryQueue.containsKey(t.Country)){
                cLeadNew.OwnerId = mapLeadCountryQueue.get(t.Country);
              }else{
                cLeadNew.OwnerId = WaitingAssignmentQueue.Id;   
              }      
              
              for(String field:flds) {
                  try {
                    cLeadNew.put(field,t.get(field));
                  } catch(exception e) {}
              }      
            
              cLeadToCreate.add(cLeadNew);

              }
            } 
          }
          
          if (cLeadToCreate.size()> 0) 
      {
        Database.SaveResult[] srInsert = Database.Insert(cLeadToCreate, false);  
        
        for (Database.SaveResult sr : srInsert) {
            if (!sr.isSuccess()) {             
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Child Creation Error: ' + err.getMessage());
                }
            }
        }
      }
      
      Database.SaveResult[] srUpdate = Database.Update(mapLead2Update.values(),false);
      
      for (Database.SaveResult sr : srUpdate) {
          if (!sr.isSuccess()) {             
              for(Database.Error err : sr.getErrors()) {
                  System.debug('Child Update Error: ' + err.getMessage());
              }
          }
      }          
         
    }
  
    }
    
    public static void ResetHasInterest_OnInsert(List<Lead> triggerNew)
    {
      for(Lead n : triggerNew)
      {
        n.Has_Interest__c = false;
      }
    }
    
    
    
    public static void LeadHasInterest_OnInsert(List<Lead> triggerNew)
    {
      Group WaitingAssignmentQueue = [Select g.id From Group g where type = 'Queue' and name = 'Waiting Assignment Queue' LIMIT 1];
      list<Lead> listPLead = new list<Lead>(); 
      
      for(Lead n : triggerNew)
      {
        if (n.Is_Parent__c == true && n.Opportunity_Product_Group__c <> Null){
          listPLead.add(n);          
        }
      }
      
      List<String> flds = new DG_Lead_Class().ChildFields();
              
        if (listPLead.size()> 0) {
          //Retrieve custom setting related to mapping lead's country and queue mapping      
      map<String,String> mapLeadCountryQueue = getLeadCountryQueueMapping();                    
                    
          List<Lead> cLeadToCreate = new List<Lead>(); 
          
          for(Lead t : listPLead) 
          {   
            //Parse values separated by comma in the Opportunity Product Group field
        List<String> listOPG = t.Opportunity_Product_Group__c.split('\\,');
        
        //Loop thru the values in the Opportunity Product Group
        for (String o : listOPG) 
        {
            //create Child Lead
            Lead cLeadNew = new Lead();
            cLeadNew.Opportunity_Product_Group__c = o;
            cLeadNew.RecordTypeId = t.RecordTypeId;
            cLeadNew.Product_Solution_of_Interest__c = t.Most_Recent_Product_Solution_of_Interest__c ;
            cLeadNew.Most_Recent_Product_Solution_of_Interest__c = t.Most_Recent_Product_Solution_of_Interest__c;
            cLeadNew.Parent_Lead__c = t.id;
            cLeadNew.Contact_Us_Reason__c=t.Contact_Us_Reason__c;
            if(mapLeadCountryQueue.containsKey(t.Country)){
              cLeadNew.OwnerId = mapLeadCountryQueue.get(t.Country);
            }else{
              cLeadNew.OwnerId = WaitingAssignmentQueue.Id;   
            }   
            
            for(String field:flds) 
            {
                try 
                {
                  cLeadNew.put(field,t.get(field));
                } catch(exception e) {}
            }      
          
            cLeadToCreate.add(cLeadNew);
            } 
          }
          
          if (cLeadToCreate.size()> 0) 
      {
        Database.SaveResult[] srInsert = Database.Insert(cLeadToCreate, false);  
        
        for (Database.SaveResult sr : srInsert) {
            if (!sr.isSuccess()) {             
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Child Creation Error: ' + err.getMessage());
                }
            }
        }
      }       
         
    }
  
    }
    
    public static void LeadAddCM_OnInsert(List<Lead> triggerNew)
    {
      List<CampaignMember> campMemToCreate = new List<CampaignMember>(); 
      for(Lead n : triggerNew)
      {
        if(n.Is_Parent__c == false && n.Campaign_Most_Recent__c != Null){
          CampaignMember CMNew = new CampaignMember();
          CMNew.Status = 'Responded';
          CMNew.LeadId = n.id;
          CMNew.CampaignId = n.Campaign_Most_Recent__c;
          campMemToCreate.add(CMNew);
        }
      }
      Database.Insert(campMemToCreate, false);
    }
    
    public static void LeadAddCM_OnUpdate(List<Lead> triggerNew, List<Lead> triggerOld)
    
    {
      List<CampaignMember> campMemToCreate = new List<CampaignMember>(); 
      List<ID> listLeadID = new List<ID>(); 
      
      integer i;
      for (i=0; i<triggerNew.size(); i++)
      {
        Lead o = triggerOld[i];
        Lead n = triggerNew[i];
        if(n.Is_Parent__c == false && n.Campaign_Most_Recent__c <> o.Campaign_Most_Recent__c && n.Campaign_Most_Recent__c != Null){
          listLeadID.add(n.Id);
        }
      }
      
      if(listLeadID.size()>0){
        
        Map<String,CampaignMember> mapCM = new Map<String,CampaignMember>();
        
        for(CampaignMember cm:[Select c.id, c.LeadId, c.CampaignId From CampaignMember c where c.LeadId in :listLeadID]){
          String vKey = cm.LeadId + '~' + cm.CampaignId;
          mapCM.put(vKey, cm);
        }
        
        integer h;
        for (h=0; h<triggerNew.size(); h++)
        {
          Lead o = triggerOld[h];
          Lead n = triggerNew[h];
          if(n.Is_Parent__c == false && n.Campaign_Most_Recent__c <> o.Campaign_Most_Recent__c && n.Campaign_Most_Recent__c != Null){
            String vKey = n.id + '~' + n.Campaign_Most_Recent__c;
            if(!(mapCM.containsKey(vKey))){ 
              CampaignMember CMNew = new CampaignMember();
              CMNew.Status = 'Responded';
              CMNew.LeadId = n.id;
              CMNew.CampaignId = n.Campaign_Most_Recent__c;
              campMemToCreate.add(CMNew);
            }
          }
        }
        Database.Insert(campMemToCreate, false);
      }
    }
    
    private static String AppendProductInterest(String pProdInterest, String cProdInterest){
      String CombinedProdInterest;

      if(pProdInterest==Null){
        CombinedProdInterest = cProdInterest;
      } else if(cProdInterest==Null){
        CombinedProdInterest = pProdInterest;
      } else{
        List<String> listP = pProdInterest.split('\\,');
        List<String> listC = cProdInterest.split('\\,');
        List<String> listCombine = cProdInterest.split('\\,');
        for (String p : listP) {
          Boolean vDup = false;
          for (String c : listC){
            if(p == c){
              vDup = true;
            }
          }
          if (vDup == false){
            listCombine.add(p); 
          }
      }
      CombinedProdInterest = string.join(listCombine, ',');
      }
      return CombinedProdInterest;
    }
    
    public void AssignLeads() {
      map<String, Id> mapQueue = new map<String, Id>();
      for(Group g : [Select g.id, g.name From Group g where g.type = 'Queue' 
      and g.name in ('Waiting Assignment Queue','Unassigned RMS Lead Queue','Unassigned Surgical Lead Queue')]){
        mapQueue.put(g.name,g.id);
      }
      
      Id WaitingAssignmentQID;
      Id UnassignedRMSQID;
      Id UnassignedSurgicalQID;
      
      if(mapQueue.containsKey('Waiting Assignment Queue')){
        WaitingAssignmentQID = mapQueue.get('Waiting Assignment Queue');
      }
      
      if(mapQueue.containsKey('Unassigned RMS Lead Queue')){
        UnassignedRMSQID = mapQueue.get('Unassigned RMS Lead Queue');
      }
      
      if(mapQueue.containsKey('Unassigned Surgical Lead Queue')){
        UnassignedSurgicalQID = mapQueue.get('Unassigned Surgical Lead Queue');
      }
            
      list<Lead> listLead = new list<Lead>(); 
      map<String,String> mapPostal = new map<String,String>(); 
      
      /* Lead Assignment using Covidien Account Number
      list<String> listAccountNo = new list<String>();
      */
      
      if(WaitingAssignmentQID <> Null){ 
        listLead = [Select l.RecordTypeId, l.PostalCode, l.OwnerId, l.Opportunity_Product_Group__c, l.Id, l.Country, l.RecordType.Name, l.CovidienAccountNumber__c From Lead l
        where l.OwnerId = :WaitingAssignmentQID and Is_Parent__c = false and l.RecordType.Name in ('RMS Lead Type','Surgical Lead Type')
        LIMIT 5000];
        
        for (Lead l : listLead){
          String vPostal;
          if(l.PostalCode == Null){
            vPostal = 'empty';
          }else if(l.Country == 'US'){
            vPostal = l.PostalCode.left(5);
          }else{
            vPostal = l.PostalCode;
          }
          
          //system.debug('***' + vPostal);
          mapPostal.put(vPostal,vPostal);
          
          /* Lead Assignment using Covidien Account Number
          if(l.CovidienAccountNumber__c <> Null){
            listAccountNo.add(l.CovidienAccountNumber__c);
          }
          */
          
        }
        
        /* Lead Assignment using Covidien Account Number
        map<String,Account> mapAccountPostal = new map<String,Account>();
        
        if(listAccountNo.size() > 0){
          for(Account a: [Select a.CovidienAccountNumber__c, a.BillingPostalCode, a.BillingCountry from Account a where a.CovidienAccountNumber__c in :listAccountNo]){
            String vPostal;
            if(a.BillingPostalCode == Null){
              vPostal = 'empty';
            }else if(a.BillingCountry == 'US'){
              vPostal = a.BillingPostalCode.left(5);
            }else{
              vPostal = a.BillingPostalCode;
            }
          
            mapPostal.put(vPostal,vPostal);
            mapAccountPostal.put(a.CovidienAccountNumber__c,a);
          }
        }
        */        
        
        map<String,ID> mapAssignment = new map<String,ID>();
        
        for(Lead_Assignment__c la :  [Select l.User__c, l.Postal_Code__c, l.Opportunity_Product_Group__c, l.Name, l.Id, l.Country__c 
        From Lead_Assignment__c l where l.User__r.IsActive = true and l.Opportunity_Product_Group__c <> NULL and l.Postal_Code__c in :mapPostal.keyset()]){
          String vKey = la.Postal_Code__c.toUpperCase() + '~' + la.Opportunity_Product_Group__c.toUpperCase();
          //system.debug('***' + vKey);
          mapAssignment.put(vKey, la.User__c);
        }
        
        for (Lead l : listLead){
          Boolean IsAssigned = false;
          String vPostal = l.PostalCode;
          String vCountry = l.Country;
          
          /* Lead Assignment using Covidien Account Number
          if(l.CovidienAccountNumber__c <> Null){
            if(mapAccountPostal.containsKey(l.CovidienAccountNumber__c)){
              Account acct = mapAccountPostal.get(l.CovidienAccountNumber__c);
              vPostal = acct.BillingPostalCode;
              vCountry = acct.BillingCountry;
              
            }
          }
          */
          
          if(vPostal!=Null && l.Opportunity_Product_Group__c!=Null && vCountry == 'US'){
            
            String vPostal5;
            
            /* For future enhancement of extending lead assignment to non-US countries
            if(vPostal == Null){
              vPostal5 = 'empty';
            }else if(vCountry == 'US'){
              vPostal5 = vPostal.left(5);
            }else{
              vPostal5 = vPostal;
            }
            */
            
            vPostal5 = vPostal.left(5);
            
            String vKey;
            vKey = vPostal5.toUpperCase() + '~' + l.Opportunity_Product_Group__c.toUpperCase();
     
            if(mapAssignment.containsKey(vKey)){
              l.OwnerId = mapAssignment.get(vKey);
              IsAssigned = true;
            }
          }
          
          if(IsAssigned == false){
            if(l.RecordType.Name == 'RMS Lead Type'){  //Unassigned RMS Lead Queue
              l.OwnerID = UnassignedRMSQID;
          }else if(l.RecordType.Name == 'Surgical Lead Type'){  //Unassigned Surgical Lead Queue
              l.OwnerID = UnassignedSurgicalQID;
            }
          }
        }
        //set options
        Database.Dmloptions dmo = new Database.Dmloptions();
        dmo.EmailHeader.triggerUserEmail = false;
        dmo.EmailHeader.triggerAutoResponseEmail = false;
        dmo.EmailHeader.triggerOtherEmail = false;
        dmo.optAllOrNone = false;
        Database.update(listLead,dmo);
        //
        //update listLead;
      }
      
    }
    
    public List<String> ChildFields(){
      List<String> listLeadFields = new List<String>();
      Map<String, Schema.SObjectField> mapSchema = Schema.SObjectType.Lead.fields.getMap();
      //Map<String, Child_Lead_Fields__c > mapChildFields = Child_Lead_Fields__c.getAll();
      Map<String, Child_Lead_Fields__c > mapChildFields = DG_Lead_Class.getChildFields();
      Map<String, Integer> mapExcludeFields = new Map<String, Integer>{
        'Opportunity_Product_Group__c' => 1,
        'RecordTypeId' => 1,
        'Product_Solution_of_Interest__c' => 1,
        'Most_Recent_Product_Solution_of_Interest__c' => 1,
        'Parent_Lead__c' => 1,
        'OwnerId' => 1
      };

      for(String k:mapChildFields.keySet()){  
        if(mapSchema.containsKey(k) && !mapExcludeFields.containsKey(k)){
          Schema.DescribeFieldResult fieldDescribe = mapSchema.get(k).getDescribe();
          if(fieldDescribe.isUpdateable()){
            listLeadFields.Add(k);
          }
        }
      }
      return listLeadFields;
    }
    
    public static map<String,Child_Lead_Fields__c > getChildFields(){
     
    map<String,Child_Lead_Fields__c> mapChildFields = new map<String,Child_Lead_Fields__c>();
    
    if(!test.isRunningTest()){      
      mapChildFields = Child_Lead_Fields__c.getAll();
    }else{
      mapChildFields.put('FirstName',new Child_Lead_Fields__c (name = 'FirstName'));
      mapChildFields.put('LastName',new Child_Lead_Fields__c (name = 'LastName'));
      mapChildFields.put('Email',new Child_Lead_Fields__c (name = 'Email'));
      mapChildFields.put('Company',new Child_Lead_Fields__c (name = 'Company'));
      mapChildFields.put('Country',new Child_Lead_Fields__c (name = 'Country'));
      mapChildFields.put('Campaign_Most_Recent__c',new Child_Lead_Fields__c (name = 'Campaign_Most_Recent__c'));
    }
                     
    return mapChildFields;
  }
  
  public static map<String,String> getLeadCountryQueueMapping(){
    Map<String, String> mapCountryQueue = new Map<String, String>();  

    if(!test.isRunningTest()){
      map<String, Lead_Country_Queue_Mapping__c> Lead_Country_Queue_Mapping = Lead_Country_Queue_Mapping__c.getAll();
            for(String k: Lead_Country_Queue_Mapping.keySet()){
              mapCountryQueue.put(k,Lead_Country_Queue_Mapping.get(k).Queue_ID__c);
          }
    }else{
      mapCountryQueue =  new Map<String,String>{
      'Australia' => userinfo.getUserId(),
      'New Zealand'=> userinfo.getUserId(),
      'AUS'=> userinfo.getUserId(),
      'NZL'=> userinfo.getUserId()};
    }
    
    return mapCountryQueue;
  }
}