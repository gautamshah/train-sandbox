@isTest
public class cpqOpportunity_TestSetup extends cpq_TestSetup
{
    // Creates Account, Contact, and Opportunity for use with CPQ scenarios
    public static void buildScenario_CPQ_Opportunity()
    {
        cpq_TestSetup.testAccounts = cpqAccount_TestSetup.generateAccounts(1);
        insert cpq_TestSetup.testAccounts;
        
        cpq_TestSetup.testContacts = cpqContact_TestSetup.generateContacts(cpq_TestSetup.testAccounts,1);
        insert cpq_TestSetup.testContacts;
        
        cpq_TestSetup.testOpportunities = generateOpportunities(cpq_TestSetup.testAccounts,1);
        insert cpq_TestSetup.testOpportunities;
    }

   public static List<Opportunity> generateOpportunities (List<Account> accounts, Integer quantity)
   {
        List<Opportunity> opportunities = new List<Opportunity>();
        Integer aIdx = 0;
        for (Account acc : accounts)
        {
            for (Integer i = 0; i < quantity; i++)
            {
                opportunities.add(new Opportunity(
                    Name = 'Test Opportunity ' + aIdx + '-' + i,
                    CloseDate = System.today(),
                    AccountId = acc.Id,
                    // Are these fields for RMS or Surgical opportunities only?
                    Capital_Disposable__c = 'Capital',
                    Type = 'New Customer',
                    Financial_Program__c = 'Advanced Tech Bridge',
                    Promotion_Program__c = 'Capnography Customer Care-PM',
                    StageName = 'Draft'
                ));
            }
            aIdx++;
        }
        return opportunities;
    }
}