public class UploadAttachmentsController {
    
 
    //Picklist of tnteger values to hold file count
    public List<SelectOption> filesCountList {get; set;}
    //Selected count
    public String FileCount {get; set;}
   
    public List<Attachment> allFileList {get; set;}
    
    public UploadAttachmentsController(ApexPages.StandardController controller)
    {
        //Initialize  
        allFileList = new List<Attachment>() ;
        
        //Adding values count list - you can change this according to your need
        for(Integer i = 1 ; i < 11 ; i++)
            allFileList.add(new Attachment()) ;
    }
    
    public Pagereference SaveAttachments()
    {
        String caseId = System.currentPagereference().getParameters().get('id');
        if(caseId == null || caseId == '')
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'No record is associated. Please pass record Id in parameter.'));

        List<Attachment> listToInsert = new List<Attachment>() ;
        
        //Attachment a = new Attachment(parentId = caseid, name=myfile.name, body = myfile.body);
        for(Attachment a: allFileList)
        {
            if(a.name != '' && a.name != null){
            	if(a.body != null)
                listToInsert.add(new Attachment(parentId = caseId, name = a.name, body = a.body)) ;
            }    
        }
        
        //Inserting attachments
        if(listToInsert.size() > 0)
        {
            insert listToInsert ;
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, listToInsert.size() + ' file(s) are uploaded successfully'));
            allFileList= new List<Attachment>() ;
        }
        
            
        return new pagereference('/'+caseId);
    }
    
    
    
    
    
    
    
    
    
    /*
    public case caseRecord{get; set;}
    public List<Attachment> lstAttach{get; set;}
    public UploadAttachmentsController(ApexPages.StandardController sc){
        caseRecord=[Select id,caseNumber from case where id=:ApexPages.CurrentPage().getParameters().get('id')];
        lstAttach= new List<Attachment>();
        lstAttach.add(new Attachment(parentId=caseRecord.Id));
    }
    
    public void Add5Rows(){
        integer rowsToadd = 5;
        if(lstAttach.size()==1)
          rowsToadd = 4;  
        
        for(integer i = 0; i<rowsToadd; i++){
            lstAttach.add(new Attachment(parentId=caseRecord.Id));
        }
    }
    
    public pagereference saveData(){
        List<Attachment> lstToInsert = new List<Attachment>();
        for(Attachment att: lstAttach){
            if(att.name == null && att.name == '' ){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'Please enter file names for all attachments.');
                ApexPages.addMessage(msg);
                return null;
            }
            if(att.body == null && att.name != null && att.name != ''){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'Please upload attachments.');
                ApexPages.addMessage(msg);
                return null;
            }
            else if(att.name != null && att.body != null)
            lstToInsert.add(att);
        }
        
        insert lstToInsert;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info,'Records inserted successfully.');
        ApexPages.addMessage(msg);
        return null;    
    }*/
    
}