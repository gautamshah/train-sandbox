global class BatchEmptyRecycleBin implements Database.Batchable<sObject> {
 /****************************************************************************************
    * Name    : BatchEmptyRecycleBin
    * Author  : Mihir Shah
    * Date    : 8-6-2012
    * Purpose : Performs a deletion job to empty the recycle bin.
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    *
    *****************************************************************************************/ 
   public String query;
   public String obj;   
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      DataBase.emptyRecycleBin(scope);
   }

    global void finish(Database.BatchableContext BC)  
    {  
        //Send an email to the User after your batch completes  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        String[] toAddresses = new String[] {'mihir.shah@covidien.com'};  
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Deletion job is done');  
        mail.setPlainTextBody('The job to empty recycle bin for '+obj+' is completed');  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }
}