public class Territory_g extends sObject_g
{
	public Territory_g()
	{
		super(Territory_cache.get());
	}

	////
	//// cast
	////
	public static Map<Id, Territory> cast(Map<Id, sObject> sobjs)
	{
		try
		{
			return new Map<Id, Territory>((List<Territory>)sobjs.values());
		}
		catch(Exception e)
		{
			return new Map<Id, Territory>();
		}
	}

	public static Map<object, Map<Id, Territory>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, Territory>> target = new Map<object, Map<Id, Territory>>();
		if (source != null)
			for(object key : source.keySet())
				target.put(key, cast(source.get(key)));
			
		return target;
	}

	////
	//// fetch sets
	////
	public Map<object, Map<Id, Territory>> fetch(sObjectField field)
	{
		Map<object, Map<Id, sObject>> result = this.cache.fetch(field);
		return cast(result);
	}
	
	public Map<object, Map<Id, Territory>> fetch(sObjectField field, set<object> keys)
	{
		Map<object, Map<Id, sObject>> result = this.cache.fetch(field, keys);
		return cast(result);
	}
	
	public Map<Id, Territory> fetch(Set<Id> ids)
	{
		return cast(this.cache.fetch(ids));
	}
	
	////
	//// Additional methods
	////
}