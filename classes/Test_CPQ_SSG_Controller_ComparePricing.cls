@isTest
global class Test_CPQ_SSG_Controller_ComparePricing
{
	@isTest static void Test_CPQ_SSG_Controller_ComparePricing()
	{
		PageReference pageRef = Page.CPQ_SSG_ComparePricing;
		Test.setCurrentPage(pageRef);

        Id currentUserId = cpqUser_c.CurrentUser.Id;
        User currentUser = [Select Id From User Where Id = :currentUserId];

		Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
		insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        Product2 product = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG);
        product.Name = 'Test Product 1';
        product.Apttus_Surgical_Product__c = true;
        product.Category__c = 'Test';

        //Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1', Apttus_Surgical_Product__c = true, Category__c = 'Test');
        //Product2 dummyProduct = new Product2(ProductCode = 'TestProduct2', Name = 'Test COT', Apttus_Surgical_Product__c = true, Category__c = 'Test');

        Product2 dummyProduct = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG);
        dummyProduct.Name = 'Test COT';
        dummyProduct.Apttus_Surgical_Product__c = true;
        dummyProduct.Category__c = 'Test';

		List<Product2> products = new List<Product2>();
		products.add(product);
		products.add(dummyProduct);
		insert products;

		Apttus_Config2__ClassificationName__c className =
			new Apttus_Config2__ClassificationName__c(Name = 'Test Category', 
													  Apttus_Config2__HierarchyLabel__c = 'Test Category Label',
													  Apttus_Config2__Type__c = 'Offering');
		insert className;

		Apttus_Config2__ClassificationHierarchy__c classHierarchy =
			new Apttus_Config2__ClassificationHierarchy__c(Name = 'Test Category',
														   Apttus_Config2__HierarchyId__c = className.Id,
														   Apttus_Config2__Label__c = 'Test Hierarchy Label',  
														   Apttus_Config2__Level__c = 0, 
														   Apttus_Config2__Left__c = 2, 
														   Apttus_Config2__Right__c = 3);
		insert classHierarchy;

		List<Apttus_Config2__ProductClassification__c> productClasses = new List<Apttus_Config2__ProductClassification__c>();
		productClasses.add(new Apttus_Config2__ProductClassification__c(Apttus_Config2__ProductId__c = product.Id, Apttus_Config2__ClassificationId__c = classHierarchy.Id));
		productClasses.add(new Apttus_Config2__ProductClassification__c(Apttus_Config2__ProductId__c = dummyProduct.Id, Apttus_Config2__ClassificationId__c = classHierarchy.Id));
		insert productClasses;

		Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
		insert opp;

		Apttus_Config2__PriceList__c priceList = cpqPriceList_c.SSG;

		RecordType agreementProposal = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class, 'Agreement_Proposal');

        // Create Quote/Proposal record
        Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acct.Id, Apttus_Proposal__Opportunity__c = opp.Id, Apttus_Proposal__Approval_Stage__c='In Review', RecordTypeId = agreementProposal.Id, Apttus_QPApprov__Approval_Status__c='Approved', Apttus_Proposal__Payment_Term__c = '1 Year', Committed_Stapling_Vessel_Sealing__c = 'Yes');
        insert prop;

        // Create CPQ SSG Class of Trade record
        List<CPQ_SSG_Class_of_Trade__c> cots = new List<CPQ_SSG_Class_of_Trade__c>();
        cots.add(new CPQ_SSG_Class_of_Trade__c(Code__c = 'E02', Name = 'Test'));
        cots.add(new CPQ_SSG_Class_of_Trade__c(Code__c = 'E03', Name = 'Test2'));
        cots.add(new CPQ_SSG_Class_of_Trade__c(Code__c = 'E04', Name = 'Test3'));
        cots.add(new CPQ_SSG_Class_of_Trade__c(Code__c = 'E05', Name = 'Test4'));
        insert cots;

        // Create Pricing Tier
        List<CPQ_SSG_Pricing_Tier__c> tiers = new List<CPQ_SSG_Pricing_Tier__c>();
        tiers.add(new CPQ_SSG_Pricing_Tier__c(CPQ_SSG_Class_of_Trade__c = cots[0].Id, Sort_Order__c = 22, Pricing_Tier_Type__c = 'Listerine', Is_Active__c = true, Description__c = 'Test COT Tier 22', Min_Dollar_Commitment__c = 0, Max_Dollar_Commitment__c = 10000000, Min_Percent_Commitment__c = 0, Max_Percent_Commitment__c = 100));
        tiers.add(new CPQ_SSG_Pricing_Tier__c(CPQ_SSG_Class_of_Trade__c = cots[1].Id, Sort_Order__c = 1, Pricing_Tier_Type__c = 'Manual', Is_Active__c = true, Description__c = 'Test2 COT Tier 1'));
        tiers.add(new CPQ_SSG_Pricing_Tier__c(CPQ_SSG_Class_of_Trade__c = cots[2].Id, Sort_Order__c = 2, Pricing_Tier_Type__c = 'Listerine', Is_Active__c = true, Description__c = 'Test3 COT Tier 2', Min_Dollar_Commitment__c = 0, Max_Dollar_Commitment__c = 10000000, Min_Percent_Commitment__c = 0, Max_Percent_Commitment__c = 100));
        tiers.add(new CPQ_SSG_Pricing_Tier__c(CPQ_SSG_Class_of_Trade__c = cots[2].Id, Sort_Order__c = 3, Pricing_Tier_Type__c = 'Manual', Is_Active__c = true, Description__c = 'Test3 COT Tier 3'));
        insert tiers;

        List<CPQ_SSG_Price_List__c> cpqPriceLists = new List<CPQ_SSG_Price_List__c>();
        cpqPriceLists.add(new CPQ_SSG_Price_List__c(Price_List_Code__c = 'DTST22'));
        cpqPriceLists.add(new CPQ_SSG_Price_List__c(Price_List_Code__c = 'DTST1'));
        insert cpqPriceLists;

        List<CPQ_SSG_Pricing_Tier_Price_list__c> tierLists = new List<CPQ_SSG_Pricing_Tier_Price_List__c>();
        tierLists.add(new CPQ_SSG_Pricing_Tier_Price_list__c(CPQ_SSG_Pricing_Tier__c = tiers[0].Id, CPQ_SSG_Price_List__c = cpqPriceLists[0].Id));
        tierLists.add(new CPQ_SSG_Pricing_Tier_Price_list__c(CPQ_SSG_Pricing_Tier__c = tiers[1].Id, CPQ_SSG_Price_List__c = cpqPriceLists[1].Id));
        tierLists.add(new CPQ_SSG_Pricing_Tier_Price_list__c(CPQ_SSG_Pricing_Tier__c = tiers[2].Id, CPQ_SSG_Price_List__c = cpqPriceLists[0].Id));
        tierLists.add(new CPQ_SSG_Pricing_Tier_Price_list__c(CPQ_SSG_Pricing_Tier__c = tiers[3].Id, CPQ_SSG_Price_List__c = cpqPriceLists[1].Id));
        insert tierLists;

        // Create Product Configuration record
        Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c(Apttus_Config2__Status__c='Saved', Apttus_Config2__VersionNumber__c=1, Apttus_Config2__BusinessObjectType__c='Proposal', Apttus_Config2__PriceListId__c = priceList.Id, Apttus_Config2__EffectivePriceListId__c = priceList.Id);
        insert pc;

        // Create CPQ SSG QP Class of Trade
        List<CPQ_SSG_QP_Class_of_Trade__c> qpCots = new List<CPQ_SSG_QP_Class_of_Trade__c>();
        qpCots.add(new CPQ_SSG_QP_Class_of_Trade__c(CPQ_SSG_Class_of_Trade__c = cots[0].Id, Quote_Proposal__c = prop.Id, Expected_Annual_Compliance__c = 75, Expected_Annual_Customer_Spend__c = 20000, List_Type__c = 'Listerine', Qualified_Pricing_Tier__c = tiers[0].Id, Proposed_Pricing_Tier__c = tiers[2].Id));
        qpCots.add(new CPQ_SSG_QP_Class_of_Trade__c(CPQ_SSG_Class_of_Trade__c = cots[1].Id, Quote_Proposal__c = prop.Id, Expected_Annual_Compliance__c = 75, Expected_Annual_Customer_Spend__c = 20000, List_Type__c = 'Manual', Qualified_Pricing_Tier__c = tiers[1].Id, Proposed_Pricing_Tier__c = tiers[3].Id));
        insert qpCots;

        prop.Compare_Class_of_Trade__c = qpCots[0].Id;
        update prop;

		Commitment_Type__c commitmentType = new Commitment_Type__c (Name = 'TST', Description__c = 'Test Commitment Type');
		insert commitmentType;

        Partner_Root_Contract__c prc = new Partner_Root_contract__c(Root_Contract_Number__c = 'TEST123', Root_Contract_Name__c = 'Test Root Contract', Account__c = acct.Id, Expiration_Date__c = System.today().addDays(2), Commitment_Type__c = commitmentType.Id);
        insert prc;

        Contract_Item_Price__c itemPrice = new Contract_Item_Price__c(Conversion_Factor__c = 10, Direct_Price__c = 100.0, Item__c = 'TestProduct1', Pricing_Identifier__c = 'DTST22', Selling_UOM__c = 'BX');
        insert itemPrice;

        Sales_History__c salesHistory = new Sales_History__c(Customer__c = acct.Id, SKU__c = Product.ProductCode, Period_1_Eaches__c = 12);
        insert salesHistory;

		Apttus_Approval__ApprovalsCustomConfig__c approvalsConfig = new Apttus_Approval__ApprovalsCustomConfig__c(Name = 'Apttus_Proposal__Proposal__c', Apttus_Approval__ApprovalStatusField__c = 'Apttus_Approval__Approval_Status__c', Apttus_Approval__ApprovalContextType__c = 'Single');
		insert approvalsConfig;

		//Test.setMock(WebServiceMock.class, new MockContractResponse());
		Test.startTest();
			CPQ_SSG_Controller_ComparePricing con = new CPQ_SSG_Controller_ComparePricing(new ApexPages.StandardController(prop));
			con.compareTierId1 = tiers[0].Id;
			con.changeCompareTier1();
			con.compareTierId2 = tiers[0].Id;
			con.changeCompareTier2();
			con.uom = 'EA';
			con.changeUom();
			con.selectCompareTier1();
			con.selectCompareTier2();
			con.selectQualifiedTier();
			con.doNext();
			con.doPrevious();
			con.getHasNext();
			con.getHasPrevious();
			con.getComparisons();
			con.getPageNumber();
			con.getTotalPages();
			con.getStartIdx();
			con.getEndIdx();
			con.getResultsSize();
		Test.stopTest();
	}

	global class MockContractResponse implements WebServiceMock {
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
			CPQ_ProxyServices.ContractInfoResponse_element respElement = new CPQ_ProxyServices.ContractInfoResponse_element();
			CPQ_ProxyContractInfoDataType.ContractInfoResponseType contractInfoResponse = new CPQ_ProxyContractInfoDataType.ContractInfoResponseType();
			CPQ_ProxyContractInfoDataType.ContractInfoType contractInfo = new CPQ_ProxyContractInfoDataType.ContractInfoType();
			contractInfo.commitmentTypeField = 'TST';
			contractInfoResponse.contractInfoField = contractInfo;
			CPQ_ProxyContractInfoDataType.ArrayOfContractItemType itemsField = new CPQ_ProxyContractInfoDataType.ArrayOfContractItemType();
			CPQ_ProxyContractInfoDataType.ContractItemType[] contractItems = new CPQ_ProxyContractInfoDataType.ContractItemType[] {};
			CPQ_ProxyContractInfoDataType.ContractItemType contractItem = new CPQ_ProxyContractInfoDataType.ContractItemType();
			contractItem.itemNumberField = 'TestProduct1';
			contractItems.add(contractItem);
			itemsField.ContractItemType = contractItems;
			contractInfoResponse.itemsField = itemsField;
			respElement.ContractInfoResult =  contractInfoResponse;
			response.put('response_x', respElement);
		}
    }
}