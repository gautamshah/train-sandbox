public abstract class sObject_g
{
	public sObject_cache cache { get; private set; }

	protected sObject_g()
	{
		
	}
	
	public sObject_g(sObject_cache cache)
	{
		cache.dump('init');
		this.cache = cache;
	}
	
	public void put(sObject sobj)
	{
		this.cache.put(sobj);
	}

	public void put(List<sObject> sobjs)
	{
		this.cache.put(sobjs);
	}
	
	public void put(Map<Id, sObject> sobjs)
	{
		this.cache.put(sobjs);
	}
	
	////
	//// Cache related methods
	////
	public void clear()
	{
		this.cache.clear();
	}
	
	public boolean isEmpty()
	{
		return this.cache.isEmpty();
	}

	public void dump(string context)
	{
		this.cache.dump(context);
	}
}