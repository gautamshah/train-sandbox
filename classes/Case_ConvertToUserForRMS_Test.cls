@isTest
public class Case_ConvertToUserForRMS_Test {

    public static testMethod void Convert()
    {
         List<Case> caseListInsert = new List<Case>();
        List<User> userList = new List<User>([SELECT Id from User where UserType = 'Standard' and IsActive = True LIMIT 2]);
        
        //Create Test Compensation/Sales Inquiry Cases to Insert
        for(Integer i=0; i<1; i++){
          Case testCompCase = new Case(Subject='Test Compensation Case' + i);
          testCompCase.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Compensation_Case' LIMIT 1].Id;
          testCompCase.Requested_By__c = userList[0].Id;
          testCompCase.Last_Name_NUPP__c='test';
          testCompCase.First_Name_NUPP__c='test';
          testCompCase.Covidien_Department_NUPP__c= 'test';
          testCompCase.Business_Card_Title_NUPP__c = 'test';
          testCompCase.Email_Address_NUPP__c = 'test';
          testCompCase.Mobile_Phone_NUPP__c ='test';
          testCompCase.Employee_Number_NUPP__c ='test';
          testCompCase.GBU__c = 'test';
          testCompCase.Region__c = 'test';
          testCompCase.Employee_Number_NUPP__c = '1';
          testCompCase.Cost_Center_NUPP__c = 'test';
          testCompCase.Email_Address_NUPP__c = 'test@test.com';
          testCompCase.Manager_of_Requested_By__c = userList[1].Id;
          testCompCase.Country_Code_NUPP__c='test';
          testCompCase.Asia_Team_Asia_use_only__c='EMS Team';
          caseListInsert.add(testCompCase);
        }  
        insert caseListInsert;
        Case_ConvertToUserForRMS myClass = new Case_ConvertToUserForRMS();
        myClass.Id = caseListInsert[0].id;
        myClass.TimeZone='America/Los_Angeles';
        PageReference aPage = myClass.Convert();
        
        //User aUser = [SELECT Email FROM User WHERE Email = 'aaardvark@test.com' LIMIT 1];
        
        //System.assertNotEquals(null, aUser);
    }
    
    public static testMethod void getTimeZones()
    {
        Case_ConvertToUserForRMS myClass = new Case_ConvertToUserForRMS();
        List<SelectOption> options = myClass.getTimeZones();
    
    }
    
    public static testMethod void getUserRoles()
    {
        Case_ConvertToUserForRMS myClass = new Case_ConvertToUserForRMS();
        List<SelectOption> options = myClass.getUserRoles();
    
        //System.assertEquals(4, options.size());
    }
    
    
    public static testMethod void getFunctions()
    {
        Case_ConvertToUserForRMS myClass = new Case_ConvertToUserForRMS();
        List<SelectOption> options = myClass.getFunctions();
   
    }
    
    public static testMethod void getUserProfiles()
    {
        Case_ConvertToUserForRMS myClass = new Case_ConvertToUserForRMS();
        List<SelectOption> options = myClass.getUserProfiles();
   
    }
}