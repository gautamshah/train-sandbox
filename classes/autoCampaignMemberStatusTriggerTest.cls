@istest
public class autoCampaignMemberStatusTriggerTest 
{
	private static testmethod void AddCampaignMemberStatus()
    {
        //Create User
        Profile p = [SELECT Id FROM Profile WHERE Name='US - RMS']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles', UserName='standardu@testorg.com',ProfileId = p.Id);
        
        system.runAs(u)
        {
            //Insert Account and Contact to be added as campaign members
            Account acc = new Account (Name = 'TestAccount', BillingStreet = 'Hinjawadi', BillingCity= 'Pune',BillingState = 'MH',BillingCountry = 'India', BillingPostalCode ='411057');
            insert acc;
            
            Contact con1 = new Contact();
            con1.Lastname = 'Testlast';
            con1.FirstName = 'Testfirst';
            con1.AccountId = acc.id;
            insert con1;
                
            Test.startTest();
            //Create campaign
            ID rectypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('US MITG Campaign').getRecordTypeId();
            List<Campaign> allCamList = new List<Campaign>();
            Campaign camp1 = new Campaign(Name='Test', Business_Unit__c = 'All', RecordTypeId = rectypeid);
            Campaign camp2 = new Campaign(Name='Test2', Business_Unit__c = 'All', RecordTypeId = rectypeid);
            insert camp1;
            insert camp2;
                    
            //Add Campaign member to campaigns
            CampaignMember newMember = new CampaignMember(ContactId = con1.id, status='Active', campaignid = camp1.id);
            insert newMember;
                
            Test.stopTest();
        }
    }
}