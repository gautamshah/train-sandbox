@isTest
private class Test_CPQ_SSG_DM_Batch_COT
{
    static testmethod void COT_Method1()
    {
        // Create account
        Account acc=new Account();
        acc.Name='Deal_Account';
        acc.Account_External_ID__c='US-342881';
        insert acc;

        // Create Opportunity
        Opportunity opp=new Opportunity();
        opp.Name='COT Opportunity';
        opp.AccountId=acc.Id;
        opp.Capital_Disposable__c='Capital';
        opp.Type='New Custome';
        opp.Financial_Program__c='Advanced Tech Bridge';
        opp.Promotion_Program__c='Capnography Customer Care- PM';
        opp.CloseDate=Date.Today()+50;
        opp.StageName='Draft';
        insert opp;

/** Commented out because the code cannot complete in production without a properly setup user in an Apttus surgical profile to use
        // Create User information
        Profile p= new Profile();
        p = [SELECT Id FROM Profile WHERE Name='APTTUS - SSG US Account Executive'];

        User u = [Select Id From User Where Profile.Name = 'APTTUS - SSG US Account Executive' And Sales_Org_PL__c != null limit 1];

        // Create CPQ DM Variable
        CPQ_DM_Variable__c cpqDMVar=new CPQ_DM_Variable__c();
        cpqDMVar.Value__c=u.Id;
        cpqDMVar.Type__c='User';
        
        cpqDMVar.Name='StateraDeal';
        insert cpqDMVar;
*/
        Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class);
        
        RecordType rType = rts.values().get(0);

        // Create Quote/Proposal record
        Apttus_Proposal__Proposal__c app=new Apttus_Proposal__Proposal__c();
        app.Apttus_Proposal__Account__c=acc.Id;
        app.Committed_Stapling_Vessel_Sealing__c='True';
        //app.OwnerId=u.Id;
        app.Term_Months__c='24';
        app.Legacy_External_Id__c='1649';
        app.Apttus_Proposal__ExpectedStartDate__c=Date.Today()+1;
        app.Apttus_Proposal__Opportunity__c=opp.Id;
        app.Apttus_Proposal__Approval_Stage__c='In Review';
        app.RecordTypeId=rtype.Id;
        app.Apttus_QPApprov__Approval_Status__c='Approved';
        insert app;

        // Create CPQ SSG Class of Trade record
        CPQ_SSG_Class_of_Trade__c cs=new CPQ_SSG_Class_of_Trade__c();
        cs.Code__c='E02';
        insert cs;

        Product2 p2=new product2();
        p2.Name='Electrosurgery COT';
        p2.Apttus_Product_External_ID__c='1649';
        insert p2;

        // Create Pricing Tier
        CPQ_SSG_Pricing_Tier__c tier = new CPQ_SSG_Pricing_Tier__c();
        tier.CPQ_SSG_Class_of_Trade__c = cs.Id;
        tier.Sort_Order__c = 10;
        tier.Pricing_Tier_Type__c = 'Listerine';
        tier.Is_Active__c = true;
        insert tier;

        // Create Price List Records
        Apttus_Config2__PriceList__c acpList = cpqPriceList_c.SSG;
        acpList.Price_List_External_ID__c='1649';
        update acpList;

        // Create Price List Item records
        Apttus_Config2__PriceListItem__c acpItem=new Apttus_Config2__PriceListItem__c();
        acpItem.Apttus_Config2__PriceListId__c=acpList.Id;
        acpItem.Apttus_Config2__ProductId__c=p2.Id;
        insert acpItem;

        // Create Product Configuration record
        Apttus_Config2__ProductConfiguration__c acp=new Apttus_Config2__ProductConfiguration__c();
        //acp.OwnerId=u.Id;
        acp.Legacy_External_Id__c='1649';
        acp.Apttus_Config2__Status__c='Saved';
        acp.Apttus_Config2__VersionNumber__c=1;
        acp.Apttus_Config2__BusinessObjectType__c='Proposal';
        acp.Apttus_Config2__PriceListId__c=acpList.Id;
        acp.Apttus_Config2__EffectivePriceListId__c=acpList.Id;
        insert acp;

        // Create CPQ_SSG_STG Deal Record
        CPQ_SSG_STG_Deal__c StgDeal = new CPQ_SSG_STG_Deal__c();
        StgDeal.STG_DealId__c = '1649';
        StgDeal.STG_Process__c = true;
        insert StgDeal;

        // Create CPQ SSG STG Deal COT Record
        CPQ_SSG_STG_Deal_COT__c StgCot=New CPQ_SSG_STG_Deal_COT__c();
        StgCot.STG_AllProductsRebate__c=12;
        StgCot.STG_ClassOfTradeCode__c='E02';
        StgCot.STG_ComplianceDollars__c=100;
        StgCot.STG_CompliancePercent__c=50;
        StgCot.STG_CreatedBy__c='StateraDeal';
        StgCot.STG_DealId__c='1649';
        StgCot.STG_GrowthRebate__c=15;
        StgCot.STG_NewBusinessRebate__c=10;
        StgCot.STG_PropsedPriceBookCode__c = 'E02T010';
        StgCot.STG_QualifyingPriceBookCode__c = 'E02T010';
        insert StgCot;

        CPQ_SSG_STG_Deal_COT__c StgCot2 = StgCot.clone();
        STGCot2.STG_AllProductsRebate__c = null;
        StgCot2.STG_GrowthRebate__c = null;
        StgCot2.STG_NewBusinessRebate__c = null;
        StgCot2.STG_PropsedPriceBookCode__c = 'DELESUR10';
        insert StgCot2;

        Test.startTest();
            CPQ_SSG_DM_Batch_COT cpqCOT=new CPQ_SSG_DM_Batch_COT();
            Database.executebatch(cpqCOT);
        Test.stopTest();
    }
}