public class cpqVariable_TestSetup extends cpq_TestSetup
{
	public enum Transmit
	{
		ENABLED,
		DISABLED
	}

	public static string setTransmitState(Transmit t)
	{
		return t == null || t == Transmit.ENABLED ? 'On' : 'Off';
	}
	
	public static List<CPQ_Variable__c> create(Transmit partner)
	{
		List<CPQ_Variable__c> testVars = new List<CPQ_Variable__c>
        {
			CPQVariable_c.create('Proxy EndPoint', 'Test Proxy EndPoint'),
			CPQVariable_c.create('HPG Group Code', 'SG0150'),
			CPQVariable_c.create(
				'PricingCallBack ChargeType Ignore List',
				'Promotion|Promotion - Hardware|Promotion - Consumable|Trade-In'),
	        // Surgical
    		CPQVariable_c.create('Send Extra Smart Cart Activation Email', 'true'),
        	CPQVariable_c.create('SURGICAL_DEBUG_ACTIVATION_EMAIL', 'testemail@test.com'),
        	// RMS
        	CPQVariable_c.create(Pricebook2_c.DEFAULT_OPPORTUNITY_PRICEBOOK, Test.getStandardPriceBookId()),
        	CPQVariable_c.create('Send COOP Deals to Partner', setTransmitState(partner)),
        	CPQVariable_c.create('Send COOP ADJ Deals to Partner', setTransmitState(partner))
		};
		
		        	
        for(string pln : cpqPriceList_c.PriceListName2VariableName.keySet())
        {
        	OrganizationNames_g.Abbreviations orgName = cpqPriceList_c.PriceListName2OrganizationName.get(pln);
        	Apttus_Config2__PriceList__c pl = cpqPriceList_c.OrganizationName2PriceList.get(orgName);
        	testVars.add(CPQVariable_c.create(cpqPriceList_c.PriceListName2VariableName.get(pln), pl.Id));
        }        		
		
		return testVars;
	}
}