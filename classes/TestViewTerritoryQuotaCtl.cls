@isTest
private class TestViewTerritoryQuotaCtl {

    static testMethod void runTest() {
         Territory_Quota__c tq = new Territory_Quota__c ( Name = 'test'
                                                       , YTD_Sales__c =  100
                                                       , YTD_Quota__c = 200
                                                       , TerritoryId__c = '1234'
                                                       , Quota_Bucket__c = 'x'
                                                       , Parent_Territory__c = null
                                                       , Annual_Quota__c  = 400);
        insert tq;
        
        Territory_Quota__c tq2 = new Territory_Quota__c ( Name = 'test2'
                                                       , YTD_Sales__c =  100
                                                       , YTD_Quota__c = 200
                                                       , TerritoryId__c = '12345'
                                                       , Quota_Bucket__c = 'x'
                                                       , Parent_Territory__c = '1234'
                                                       , Annual_Quota__c  = 400);
        insert tq2;
        
        Test.setCurrentPage(Page.ViewTerritoryQuota);
        
        ViewTerritoryQuotaCtl ctlr = new ViewTerritoryQuotaCtl();
        
        ctlr.SearchForTerritory();
        ctlr.getPreviousQuotas();
        ctlr.selectedParentId = tq.TerritoryId__c;
        ctlr.getChildQuotas(); 
    }
}