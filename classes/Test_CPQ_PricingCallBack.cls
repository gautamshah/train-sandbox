/*
Test Class

CPQ_PricingCallBack
CPQ_PricingProcesses

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-05      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
public class Test_CPQ_PricingCallBack
{
    static List<Apttus_Config2__ProductConfiguration__c> testConfigs;
    static List<Apttus_Config2__LineItem__c> testLines;
    
    public static void createTestData(List<Apttus_Config2__LineItem__c> lineSOs, List<Apttus_Config2__ProductConfiguration__c> configSOs)
    {
        try {
        CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, CPQ_Product2_After__c = true);
        insert testTrigger;
        } catch (Exception e) {}
        
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;
        
		insert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        Account testAccount = new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111');
        insert testAccount;
      
        User personRunningTest = cpqUser_c.CurrentUser;
        personRunningTest.OrganizationName__c = 'RMS';
        update personRunningTest;

        Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(ownerId = personRunningTest.Id, Apttus_Proposal__Proposal_Name__c = 'Test Proposal', Apttus_Proposal__Account__c = testAccount.ID, Deal_Type__c = 'Renewal Deal');
        insert testProposal;
        
        testConfigs = new List<Apttus_Config2__ProductConfiguration__c> {
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposal.ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__Status__c = 'New', Apttus_Config2__VersionNumber__c = 1, Apttus_Config2__AccountId__c = testAccount.ID)
        };
        insert testConfigs;
        configSOs.addAll([Select ID, Name, Apttus_QPConfig__Proposald__c, 
                            Apttus_Config2__PriceListId__c,
                            Apttus_Config2__Status__c ,
                            Apttus_Config2__VersionNumber__c,
                            Deal_Type__c
                        From Apttus_Config2__ProductConfiguration__c
                        Where ID in: testConfigs]);

        OrganizationNames_g.Abbreviations OrgRMS = OrganizationNames_g.Abbreviations.RMS;

        List<Product2> testProducts = new List<Product2>
        {
            cpqProduct2_c.create(OrgRMS, 'ITEM Has CSP', 1), // Rep_Floor_Price__c = 100 - not sure why this was commented out?
            cpqProduct2_c.create(OrgRMS, 'ITEM Has CSP', 1), // RM_Floor_Price__c = 100 - not sure why this was commented out?
            cpqProduct2_c.create(OrgRMS, 'ITEM WS CSP ', 1),
            cpqProduct2_c.create(OrgRMS, 'ITEM WS CSP ', 2),
            
            cpqProduct2_c.create(OrgRMS, 'ITEM Bundle Header', 1, cpqProduct2_c.ConfigurationType.Bundle.name()),
            cpqProduct2_c.create(OrgRMS, 'ITEM Bundle Option', 1, cpqProduct2_c.ConfigurationType.Option.name()) 
        };
        insert testProducts;
        
        Apttus_Config2__ClassificationName__c testCategory = new Apttus_Config2__ClassificationName__c(Apttus_Config2__Active__c = true, Apttus_Config2__HierarchyLabel__c = 'Test Category', Apttus_Config2__Type__c = 'Option Group');
        insert testCategory;
        
        Apttus_Config2__ClassificationHierarchy__c testHierarchy = new Apttus_Config2__ClassificationHierarchy__c (Name = 'Test Option Group', Apttus_Config2__Label__c = 'Test Option Group', Apttus_Config2__HierarchyId__c = testCategory.Id);
        insert testHierarchy;
        
        Apttus_Config2__ProductOptionGroup__c testOptionGroup = new Apttus_Config2__ProductOptionGroup__c(Apttus_Config2__OptionGroupId__c = testHierarchy.ID, Apttus_Config2__Sequence__c = 1);
        insert testOptionGroup;
        
        List<Apttus_Config2__ProductOptionComponent__c> testPOCs = new List<Apttus_Config2__ProductOptionComponent__c> {
            new Apttus_Config2__ProductOptionComponent__c(Apttus_Config2__ProductOptionGroupId__c = testOptionGroup.ID, Apttus_Config2__DefaultQuantity__c = 3, Apttus_Config2__ParentProductId__c = testProducts[4].ID, Apttus_Config2__ComponentProductId__c = testProducts[5].ID, Apttus_Config2__Sequence__c = 1)
        };
        insert testPOCs;
        
        List<Apttus_Config2__PriceListItem__c> testPriceListItems = new List<Apttus_Config2__PriceListItem__c> {
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[0].ID, Apttus_Config2__Active__c = true, Apttus_Config2__ListPrice__c = 200),
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[1].ID, Apttus_Config2__Active__c = true, Apttus_Config2__ListPrice__c = 200),
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[2].ID, Apttus_Config2__Active__c = true, Apttus_Config2__ListPrice__c = 200),
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[3].ID, Apttus_Config2__Active__c = true, Apttus_Config2__ListPrice__c = 200),
            
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[4].ID, Apttus_Config2__Active__c = true, Apttus_Config2__ListPrice__c = 200),
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[5].ID, Apttus_Config2__Active__c = true, Apttus_Config2__ListPrice__c = 200)
        };
        insert testPriceListItems;
        
        testLines = new List<Apttus_Config2__LineItem__c> {
            new Apttus_Config2__LineItem__c(Apttus_Config2__LineType__c = 'Product/Service', Apttus_Config2__Quantity__c = 2, Price_Mode__c = 'Rep Floor', Apttus_Config2__BasePrice__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[0].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[0].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1),
            new Apttus_Config2__LineItem__c(Apttus_Config2__LineType__c = 'Product/Service', Apttus_Config2__Quantity__c = 2, Price_Mode__c = 'RM Floor', Apttus_Config2__BasePrice__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[1].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[1].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 2, Apttus_Config2__PrimaryLineNumber__c = 2),
            new Apttus_Config2__LineItem__c(Apttus_Config2__LineType__c = 'Product/Service', Apttus_Config2__Quantity__c = 2, Price_Mode__c = 'AVP Floor', Apttus_Config2__BasePrice__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[2].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[2].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 3, Apttus_Config2__PrimaryLineNumber__c = 3),
            new Apttus_Config2__LineItem__c(Apttus_Config2__LineType__c = 'Product/Service', Apttus_Config2__Quantity__c = 2, Price_Mode__c = 'Reset', Apttus_Config2__BasePrice__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[3].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[3].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 4, Apttus_Config2__PrimaryLineNumber__c = 4),

            new Apttus_Config2__LineItem__c(Apttus_Config2__LineType__c = 'Product/Service', Apttus_Config2__Quantity__c = 2, Apttus_Config2__BasePrice__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[4].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[4].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 5, Apttus_Config2__PrimaryLineNumber__c = 5),
            new Apttus_Config2__LineItem__c(Apttus_Config2__LineType__c = 'Option', Apttus_Config2__Quantity__c = 2, Apttus_Config2__BasePrice__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[4].ID, Apttus_Config2__OptionId__c = testProducts[5].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[5].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 5, Apttus_Config2__PrimaryLineNumber__c = 5, Apttus_Config2__ProductOptionId__c = testPOCs[0].ID)
        };
        insert testLines;
        
        testProducts[0].Rep_Floor_Price__c = 100;
        testProducts[1].RM_Floor_Price__c = 100;
        testProducts[2].Zone_VP_Floor_Price__c = 100;
        update testProducts;
        
        lineSOs.addAll([Select ID, Name, Apttus_Config2__ChargeType__c, 
                            Apttus_Config2__Quantity__c,
                            Apttus_Config2__BasePrice__c,
                            Apttus_Config2__AdjustmentType__c,
                            Apttus_Config2__BasePriceOverride__c,
                            Apttus_Config2__AdjustmentAmount__c,
                            Current_Root_Contract__c,
                            Apttus_Config2__LineType__c,
                            Price_Mode__c,
                            Apttus_Config2__Cost__c,
                            Proposal_Record_Type_Developer_Name__c,
                            Product_Name__c,
                            Product_ID__c,
                            Apttus_Config2__ConfigurationId__c,
                            Apttus_Config2__IsPrimaryLine__c,
                            Apttus_Config2__ProductId__c,
                            Apttus_Config2__PriceListItemId__c,
                            Apttus_Config2__ItemSequence__c,
                            Apttus_Config2__LineNumber__c,
                            Option_DefaultQuantity__c,
                            is_Promotional__c,
                            Apttus_Config2__OptionId__c
                        From Apttus_Config2__LineItem__c
                        Where ID in: testLines]);
        
        Commitment_Type__c testCommitmentType = new Commitment_Type__c(Name = 'IP', Description__c = 'IP');
        insert testCommitmentType;
        
        Partner_Root_Contract__c testRoot = new Partner_Root_Contract__c(Name = '011111', Root_Contract_Name__c = '011111', Root_Contract_Number__c = '011111', Direction__c = 'I', Effective_Date__c = System.Today());
        insert testRoot;
        
        List<CPQ_Customer_Specific_Pricing__c> testCSPs = new List<CPQ_Customer_Specific_Pricing__c>{
            new CPQ_Customer_Specific_Pricing__c(Product__c = testProducts[0].ID, Root_Contract__c = testRoot.Name, Non_Committed_Price__c = 150, Contract_Price__c = 100, GPO_Price__c = 120, Account__c = testAccount.ID, Commitment_Type__c = testCommitmentType.ID, LastRetrievedDateTime__c = System.today().addDays(1)),
            new CPQ_Customer_Specific_Pricing__c(Product__c = testProducts[1].ID, Root_Contract__c = testRoot.Name, Non_Committed_Price__c = 150, Contract_Price__c = 100, GPO_Price__c = 120, Account__c = testAccount.ID, Commitment_Type__c = testCommitmentType.ID, LastRetrievedDateTime__c = System.today().addDays(1)),
            new CPQ_Customer_Specific_Pricing__c(Product__c = testProducts[2].ID, Root_Contract__c = testRoot.Name, Non_Committed_Price__c = 150, Contract_Price__c = 100, GPO_Price__c = 120, Account__c = testAccount.ID, Commitment_Type__c = testCommitmentType.ID, LastRetrievedDateTime__c = System.today().addDays(-20))
        };
        insert testCSPs;
    }
    
    public static CPQ_ProxyMultiPriceService.PricingRespType createModkResponse() {
        CPQ_ProxyMultiPriceService.PricingRespType response = new CPQ_ProxyMultiPriceService.PricingRespType();
        response.shipToDSField = new CPQ_ProxyMultiPriceService.ArrayOfShipToResp();
        response.shipToDSField.ShipToResp = new CPQ_ProxyMultiPriceService.ShipToResp[]{
            new CPQ_ProxyMultiPriceService.ShipToResp()
        };
        response.shipToDSField.ShipToResp[0].sKUsField = new CPQ_ProxyMultiPriceService.ArrayOfSKUTypeResp();
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp = new CPQ_ProxyMultiPriceService.SKUTypeResp[] {
            new CPQ_ProxyMultiPriceService.SKUTypeResp(),
            new CPQ_ProxyMultiPriceService.SKUTypeResp(),
            
            new CPQ_ProxyMultiPriceService.SKUTypeResp(),
            new CPQ_ProxyMultiPriceService.SKUTypeResp()
        };
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].sKUCodeField = 'ITEM WS CSP 1';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].sKUErrorField = '0';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].uOMField = 'EA';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField = new CPQ_ProxyMultiPriceService.ArrayOfPriceTypeResp();
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp = new CPQ_ProxyMultiPriceService.PriceTypeResp[]{
            new CPQ_ProxyMultiPriceService.PriceTypeResp()
        };
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].effectiveDateField = System.Today();
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].expirationDateField = System.Today().addYears(1);
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].unitPriceField = 100.00;
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].gPOTierPriceField = 110.00;
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].gPONonCommittedPriceField = 120.00;
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].contractNumberField = 'D11111';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].pricContCommitmentTypeDescField = 'IP';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[0].pricesField.PriceTypeResp[0].pricContRootContractField = '11111';
        
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].sKUCodeField = 'ITEM WS CSP 2';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].sKUErrorField = '0';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].uOMField = 'EA';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField = new CPQ_ProxyMultiPriceService.ArrayOfPriceTypeResp();
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp = new CPQ_ProxyMultiPriceService.PriceTypeResp[]{
            new CPQ_ProxyMultiPriceService.PriceTypeResp()
        };
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].effectiveDateField = System.Today();
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].expirationDateField = System.Today().addYears(1);
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].unitPriceField = 100.00;
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].gPOTierPriceField = 110.00;
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].gPONonCommittedPriceField = 120.00;
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].contractNumberField = 'D11111';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].pricContCommitmentTypeDescField = 'IP';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[1].pricesField.PriceTypeResp[0].pricContRootContractField = '11111';
        
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[2].sKUErrorField = '1';
        
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[3].sKUCodeField = 'ITEMNOPRICE';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[3].sKUErrorField = '0';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[3].uOMField = 'EA';
        response.shipToDSField.ShipToResp[0].sKUsField.SKUTypeResp[3].pricesField = null;
        
        return response;
    }
    
    static testMethod void TestCallBack() {
        Test.startTest();
            CPQ_PricingCallBack testCallBack = new CPQ_PricingCallBack();
            testCallBack.start(null);
            testCallBack.setMode(Apttus_Config2.CustomClass.PricingMode.BASEPRICE);
            testCallBack.beforePricing(null);
            testCallBack.setMode(Apttus_Config2.CustomClass.PricingMode.ADJUSTMENT);
            testCallBack.beforePricing(null);
            
            testCallBack.finish();
            
            testCallBack.setMode(Apttus_Config2.CustomClass.PricingMode.ROLLDOWN);
            testCallBack.beforePricing(null);
            
            testCallBack.beforePricingLineItem(null, null);
            testCallBack.afterPricing(null);
            testCallBack.afterPricingLineItem(null, null);
        Test.stopTest();
    }
}