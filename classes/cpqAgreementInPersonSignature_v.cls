public class cpqAgreementInPersonSignature_v {
	Apttus__APTS_Agreement__c agmt;
	public Boolean success {get;set;}
	public cpqAgreementInPersonSignature_v(ApexPages.StandardController stdController) {
        this.agmt = (Apttus__APTS_Agreement__c)stdController.getRecord();
    }

    public void setToFullysigned() {
    	success = cpqAgreement_c.setToFullySigned(agmt);
    }

    public PageReference returnToAgreement() {
    	return new PageReference('/' + agmt.Id);
    }
}