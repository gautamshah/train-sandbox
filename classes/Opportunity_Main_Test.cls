@isTest(SeeAllData=true)
public class Opportunity_Main_Test
{
    @isTest
    public static void testA()
    {
        Test.startTest();
        User u = [SELECT Id FROM User WHERE Profile.Name ='CRM Admin Support' AND isActive = TRUE LIMIT 1];
        System.runas(u){
        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();
        List<Opportunity> updateOppList = new List<Opportunity>();
        List<Opportunity> oppList = new List<Opportunity>([Select Id,CloseDate, OCR_Email__c, OwnerId, Franchise__c, Business_Unit__c From Opportunity WHERE Id IN (SELECT OpportunityId FROM OpportunityLineItem) Limit 2]);
        List<Contact> contactList = new List<Contact>([SELECT Id,Email FROM Contact WHERE Email !='' Limit 2]);
        if(oppList != NULL && !oppList.isEmpty()){
        for(Opportunity oppVar : oppList)
        {   if(contactList != NULL && !contactList.isEmpty()){
            for(Contact conVar : contactList)
            {   
                OpportunityContactRole ocr = new OpportunityContactRole();
                oppVar.OCR_Email__c = 'testing@tesingtest.com';
               // if(oppVar.StageName.Contains('Closed'))
                 //  oppVar.WonLossReasons__c = 'Clinical Data';
                ocr.OpportunityId  = oppVar.Id;
                ocr.ContactId  = conVar.Id;
                ocr.Role = 'Business User';
                ocrList.add(ocr);
                update oppVar;
            }
            
          }  
        }
        }
        insert ocrList;
        oppList = Opportunity_Main.UpdateOpportunityGBU_Franchise(oppList);
        oppList = Opportunity_Main.OwnerRegionAssignment(oppList);
        //oppList[0].By_Pass_ProductRequiredForUSMedSuppVali__c = false;
        //oppList = Opportunity_Main.ProductReqForUSMedSupplies(oppList);
        Opportunity_Main.reEstablishSchedule(oppList);
        Opportunity_Main.AssignTerritory(oppList);
        oppList = Opportunity_Main.PopulateOCREmail(oppList);
        Test.stopTest();
        }
        
    }
}