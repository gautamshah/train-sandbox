public class NewBusinessForm_FileUploader_Controller
{

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<New_Business_Form__c> NBFlstupload;
    public Id OppId{get;set;}
    public Opportunity sfopp {get;set;}    
    
    public NewBusinessForm_FileUploader_Controller() 
    {
        OppId =  Apexpages.currentPage().getParameters().get('id');
        sfopp = [Select ID, Name from Opportunity  where ID =: OppId Limit 1];
    }
    public Pagereference ReadFile()
    {
        if(nameFile != '' && nameFile != null)
        {
            String fileType = nameFile.substring(nameFile.lastIndexOf('.')+1,nameFile.length());
            if(fileType != 'csv')  
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, label.BFLError);
                ApexPages.addMessage(errormsg);  
                nameFile = null;        
                return null;
            }
        }
        else
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, label.FileUploadError);
            ApexPages.addMessage(errormsg);  
            return null;
        }
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        NBFlstupload = new List<New_Business_Form__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            New_Business_Form__c nbf = new New_Business_Form__c();
            nbf.Opportunity__c = OppId;
            nbf.Monthly_Case_Quantity__c= Decimal.ValueOf(inputvalues[1].trim());
            nbf.Item_Number__c = inputvalues[0].trim();
            NBFlstupload.add(nbf);
        }
        try{
        insert NBFlstupload;
         return new PageReference('/'+OppId + '?nooverride=1');
        
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,Label.BFLError);
            ApexPages.addMessage(errormsg);
        }    
        nameFile = null;        
        return null;
    }
    public PageReference Cancel() {
        return new PageReference('/'+OppId + '?nooverride=1');
    }
}