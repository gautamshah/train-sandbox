/**
Test class for
MobilePRSOpportunityTransitController

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-12-10      Hidetoshi kawada    Created
2016-03-29      Hidetoshi Kawada    modified in CVJ_SELLOUT_DEV-98
===============================================================================
*/
@isTest
private class MobilePRSOppTransitControllerTest{
    static testMethod void getRecordTypes() {
        String sobjectType = 'Event';
        MobilePRSOpportunityTransitController.getRecordTypes(sobjectType);
    }
}