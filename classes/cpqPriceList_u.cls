/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date		Id   Initials   Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD	A	PAB			CPR-000		Updated comments section
							AV-000
20161103	A	IL			AV-001		Title of the Jira – Some changes needed to be made to support Medical Supplies
20161115	A	PAB			AV-253		Copied code from cpqPriceList_c to here - the fetchIds is a bulk method
20170207        IL          AV-280      Converted query to static so price lists are only queried once.
                                        Added priceListsByOrgName for retrieval from CPQ_ProposalProcesses
20170213	A	PAB			AV-280		Having issues with Price Lists being created for testing
Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
public class cpqPriceList_u extends sObject_u 
{
	private static cpqPriceList_g cache = new cpqPriceList_g();

	public static Map<Id, Apttus_Config2__PriceList__c> fetch(set<Id> ids)
	{
		Map<Id, Apttus_Config2__PriceList__c> existing = cache.fetch(ids);
		if (existing.size() < ids.size())
		{
			List<Apttus_Config2__PriceList__c> fromDB = fetchFromDB(existing.keySet());
			cache.put(fromDB);
		}
		
		return cache.fetch(ids);
	}
	
    ////
    //// fetchFromDb
    ////
	@testVisible
    private static List<Apttus_Config2__PriceList__c> fetchFromDB(set<id> existingIds)
    {
    	string query =
			SOQL_select.build(
				Apttus_Config2__PriceList__c.sObjectType,
				null,
				' WHERE Apttus_Config2__Active__c = true AND Id NOT IN :existingIds ',
				null,
				null);
    	
		return (List<Apttus_Config2__PriceList__c>)Database.query(query);
    }
}