public class cpqPriceListItem_cache extends sObject_cache
{
	public static cpqPriceListItem_cache get() { return cache; }
	
	private static cpqPriceListItem_cache cache
	{
		get
		{
			if (cache == null)
				cache = new cpqPriceListItem_cache();
				
			return cache;
		}
		
		private set;
	}
	
	private cpqPriceListItem_cache()
	{
		super(Apttus_Config2__PriceListItem__c.sObjectType, Apttus_Config2__PriceListItem__c.field.Id);
		this.addIndex(Apttus_Config2__PriceListItem__c.field.Apttus_Config2__ProductId__c);
		this.addIndex(Apttus_Config2__PriceListItem__c.field.Apttus_Config2__ChargeType__c);
		this.addIndex(Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c);
	}
}