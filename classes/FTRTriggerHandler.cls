/****************************************************************************************
 * Name    : FTRTriggerHandler 
 * Author  : Bill Shan
 * Date    : 17/06/2013 
 * Purpose : Contains all the logic coming from FTR trigger
 * Dependencies: FTRTrigger Trigger
 *             , FTR Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE                AUTHOR               CHANGE
 * 11 December 2015  Amogh Ghodke    Added setBusinessUnit method as part of Case 679346
 *****************************************************************************************/
public with sharing class FTRTriggerHandler {
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public FTRTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
 
    public void OnBeforeInsert(List<Field_Technical_Report__c> newObjects){
        setDistributorPhoneticName(newObjects);
        setBusinessUnit(newObjects);
    }
 
    public void OnBeforeUpdate(List<Field_Technical_Report__c> newObjects){
        setDistributorPhoneticName(newObjects);
        setBusinessUnit(newObjects);
    }
    
    //Set Distributor Phonetic Name
    private void setDistributorPhoneticName(List<Field_Technical_Report__c> FTRs)
    {
        set<ID> DitributorID = new Set<ID>();
        set<ID> RepId = new Set<ID>();
        for(Field_Technical_Report__c ftr: FTRs)
        {
            if(ftr.Sales_Rep__c != null)
                RepId.add(ftr.Sales_Rep__c);
            if(ftr.Distributor__c != null)
                DitributorID.add(ftr.Distributor__c);
        }
            
        Map<Id,String> distributorPName = new Map<ID, String>();
        for(Account dis : [select Id, Phonetic_Name__c from Account where Id = :DitributorID])
            distributorPName.put(dis.Id, dis.Phonetic_Name__c);
        
        Map<Id,Id> repManager = new Map<Id, String>();
        for(User u: [Select Id, ManagerId from User where Id = :RepId])
            repManager.put(u.Id, u.ManagerId);
        
        for(Field_Technical_Report__c ftr: FTRs)
        {
            if(ftr.Distributor__c != null)
                ftr.Distributor_Phonetic_Name__c = distributorPName.get(ftr.Distributor__c);
            if(ftr.Sales_Rep__c != null)
                ftr.Manager__c = repManager.get(ftr.Sales_Rep__c);
        }
    }
    
    //Set Business Unit 
    private void setBusinessUnit(List<Field_Technical_Report__c> listFTR)
    {   String userBU = [select id,Business_Unit__c from user where id =: UserInfo.getUserId()].Business_Unit__c;
        for(Field_Technical_Report__c ftrVar: listFTR)
        {
           ftrVar.Business_Unit__c = userBU;
        }
        
        
    }

}