@isTest
private class Test_CPQ_SSG_Controller_GenerateAgmt {

    static List<Apttus__APTS_Agreement__c> agmts = new List<Apttus__APTS_Agreement__c>();

    static void setup(){

        Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
        insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1', Apttus_Surgical_Product__c = true, Category__c = 'Test');
        insert product;

        Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
        insert opp;

        Apttus_Config2__PriceList__c priceList = cpqPriceList_c.SSG;

        // Generate Agreement
        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
        agmt.Name = 'Test Agreement';
        agmt.Apttus__Account__c=acct.Id;
        agmt.Apttus__Related_Opportunity__c = opp.Id;
        agmt.Primary_Contact2__c = c.Id;
        agmt.Apttus_Approval__Approval_Status__c = 'Approval Required';
        agmt.Apttus__Status_Category__c = 'Request';
        agmt.Apttus__Status__c = 'Request';
        agmt.RecordTypeId = CPQ_AgreementProcesses.getAgreementRecordTypesByNameMap().get(CPQ_AgreementProcesses.SMART_CART_RT).Id;
        agmt.SSG_Stocking_Order_PO__c = '';
        agmt.SSG_Stocking_Order_Confirmation__c = '';
        agmt.SSG_Stocking_Order_Type__c = '';

        agmts.add(agmt);
        agmts.add(agmt.clone(false,false,false,false));
        agmts[1].SSG_Stocking_Order_PO__c = 'Usage Report';
        agmts[1].SSG_Stocking_Order_Confirmation__c = '12345';
        agmts[1].SSG_Stocking_Order_Type__c = '12345';
        agmts[1].SSG_Tri_Staple_Core_Reloads__c = 10;
        agmts[1].SSG_Tri_Staple_Specialty_Reloads__c = 10;
        agmts[1].SSG_Legacy_Reloads__c = 80;        
        insert agmts;

    }

    @isTest static void doLaunchGenerate() {
        setup();
        Test.startTest();

            PageReference pageRef = Page.CPQ_SSG_GenerateAgreement;
            Test.setCurrentPage(pageRef);

            CPQ_SSG_Controller_GenerateAgreement p;

            // On Fail, Display Error Message
            p = new CPQ_SSG_Controller_GenerateAgreement(new ApexPages.StandardController(agmts[0]));
            PageReference errorRef = p.doLaunchGenerate();
            System.assertEquals(null,errorRef);
            System.debug('Messages: ' + ApexPages.getMessages());

        Test.stopTest();
    }

    @isTest static void doLaunchGenerate_Success() {
        setup();
        Test.startTest();

            PageReference pageRef = Page.CPQ_SSG_GenerateAgreement;
            pageRef.getParameters().put('k1','v1');
            Test.setCurrentPage(pageRef);

            CPQ_SSG_Controller_GenerateAgreement p;

            // On Success, Pass Parameters, Open Apttus Page
            p = new CPQ_SSG_Controller_GenerateAgreement(new ApexPages.StandardController(agmts[1]));
            PageReference successRef = p.doLaunchGenerate();
            System.debug('successRef: ' + successRef);
            PageReference expectedRef = Page.Apttus__SelectTemplate;
            System.debug('expectedRef: ' + expectedRef);
            System.assertEquals(expectedRef.getUrl() + '?k1=v1',successRef.getUrl());
            // System.debug('Parameters: ' + successRef.getParameters());

        Test.stopTest();
    }

    @isTest static void doReturn() {

        setup();
        Test.startTest();
            CPQ_SSG_Controller_GenerateAgreement p = new CPQ_SSG_Controller_GenerateAgreement(new ApexPages.StandardController(agmts[0]));
            PageReference returnRef = p.doReturn();
            PageReference assertRef = new ApexPages.StandardController(agmts[0]).view();
            System.assertEquals(returnRef.getUrl(),assertRef.getUrl());
        Test.stopTest();

    }

}