/**
Test class

CPQ_DM_Batch_Proposal

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-08      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_DM_Batch_LineItem
{
/*
    static List<User> testUsers;
    static void createTestData() {
        testUsers = new List<User> {
            new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest', 
                         Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr', 
                         ProfileId = '00eU0000000dLrtIAE', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1', 
                         LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US')
        };
        insert testUsers;
        
        try {
        CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = '00eU0000000dLrtIAE', CPQ_Proposal_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Proposal_After__c = true);
        insert testTrigger;
        } catch (Exception e) {}
        
        List<CPQ_Variable__c> testVars = new List<CPQ_Variable__c> {
            new CPQ_Variable__c(Name = 'Proxy EndPoint', Value__c = 'Test Proxy EndPoint'),
            new CPQ_Variable__c(Name = 'RMS PriceList ID', Value__c = testPriceList.ID),
            new CPQ_Variable__c(Name = 'HPG Group Code', Value__c = 'SG0150')
        };
        insert testVars;
        
        List<Product2> testProducts = new List<Product2> {
            new Product2(Name = 'ITEM1', ProductCode = 'ITEM1', Product_External_ID__c = 'US:RMS:ITEM1', Apttus_Product__c = true, IsActive = true),
            new Product2(Name = 'ITEM2', ProductCode = 'ITEM2', Product_External_ID__c = 'US:RMS:ITEM2', Apttus_Product__c = true, IsActive = true)
        };
        insert testProducts;
        
        Apttus_Config2__ClassificationName__c testCategory = new Apttus_Config2__ClassificationName__c(Name = 'Root', Apttus_Config2__Type__c = 'Offering', Apttus_Config2__Active__c = true, Apttus_Config2__HierarchyLabel__c = 'Root');
        insert testCategory;
        
        List<CPQ_DM_Variable__c> testDMVars = new List<CPQ_DM_Variable__c> {
            new CPQ_DM_Variable__c(Name = 'Root', Type__c = 'Category', Value__c = testCategory.ID),            
            new CPQ_DM_Variable__c(Name = 'Covidien RMS US Master', Type__c = 'PriceList ID', Value__c = testPriceList.ID)
        };
        insert testDMVars;
        
        Apttus_Config2__ClassificationHierarchy__c testCategoryHierarchy = new Apttus_Config2__ClassificationHierarchy__c(Name = 'OneUnder', Apttus_Config2__Label__c = 'OneUnder', Apttus_Config2__HierarchyId__c = testCategory.ID);
        insert testCategoryHierarchy;
        
        List<Apttus_Config2__ProductClassification__c> testClasses = new List<Apttus_Config2__ProductClassification__c> {
            new Apttus_Config2__ProductClassification__c (Apttus_Config2__ClassificationId__c = testCategoryHierarchy.ID, Apttus_Config2__ProductId__c = testProducts[0].ID),
            new Apttus_Config2__ProductClassification__c (Apttus_Config2__ClassificationId__c = testCategoryHierarchy.ID, Apttus_Config2__ProductId__c = testProducts[1].ID)
        };
        insert testClasses;
        
        String catHier = '{' +
                            '\"LowestTotalingCategoryId\" : \"value\",' +
                            '\"CategoryPropsValue\" : \"Root | OneUnder\",' +
                            '\"CategoryNamesValue\" : \"Root | OneUnder\"' +
                        '}';
        
        List<Apttus_Config2__ProductHierarchyView__c> testHierarchyViews = new List<Apttus_Config2__ProductHierarchyView__c> {
            new Apttus_Config2__ProductHierarchyView__c (Apttus_Config2__ParentClassificationId__c = testCategoryHierarchy.ID, Apttus_Config2__ChildProductId__c = testProducts[0].ID, Apttus_Config2__CategoryHierarchy__c = catHier, Apttus_Config2__HierarchyId__c = testCategory.ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ChildLevel__c = 2, Apttus_Config2__ParentLevel__c = 1),
            new Apttus_Config2__ProductHierarchyView__c (Apttus_Config2__ParentClassificationId__c = testCategoryHierarchy.ID, Apttus_Config2__ChildProductId__c = testProducts[1].ID, Apttus_Config2__CategoryHierarchy__c = catHier, Apttus_Config2__HierarchyId__c = testCategory.ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ChildLevel__c = 2, Apttus_Config2__ParentLevel__c = 1)
        };
        insert testHierarchyViews;
        
        List<Apttus_Config2__PriceListItem__c> testPriceListItems = new List<Apttus_Config2__PriceListItem__c> {
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[0].ID, Apttus_Config2__Active__c = true),
            new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__ProductId__c = testProducts[1].ID, Apttus_Config2__Active__c = true)
        };
        insert testPriceListItems;
        
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
        List<Account> testAccounts = new List<Account>{
            new Account(Status__c = 'Active', Name = 'Test Pristine', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
        };
        insert testAccounts;
        
        List<Apttus_Proposal__Proposal__c> testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Deal', Apttus_Proposal__Account__c = testAccounts[0].ID, Legacy_External_ID__c = 'D-111', Legacy_Scenario_ID__c = 111),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Quote', Apttus_Proposal__Account__c = testAccounts[0].ID, Legacy_External_ID__c = 'PB-222')
        };
        insert testProposals;
        
        List<Apttus_Config2__ProductConfiguration__c> testConfigs = new List<Apttus_Config2__ProductConfiguration__c> {
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[0].ID, Apttus_Config2__PriceListId__c = testPriceList.ID),
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[1].ID, Apttus_Config2__PriceListId__c = testPriceList.ID)
        };
        insert testConfigs;
        
        List<CPQ_STG_Proposal__c> testStageProposals = new List<CPQ_STG_Proposal__c> {
            new CPQ_STG_Proposal__c(Name = 'Test Stage Deal', STGPS_External_ID__c = 'D-111', STGPS_Account_External_ID__c = 'US-111111', STGPS_Proposal__c = testProposals[0].ID, STGPS_Configuration__c = testConfigs[0].ID, STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(Name = 'Test Stage Quote',STGPS_External_ID__c = 'PB-222',STGPS_Account_External_ID__c = 'US-111111', STGPS_Proposal__c = testProposals[1].ID, STGPS_Configuration__c = testConfigs[1].ID, STGPS_To_Test__c = true)
        };
        insert testStageProposals;
        
        List<CPQ_STG_Scenario__c> testStageScenarios = new List<CPQ_STG_Scenario__c> {
            new CPQ_STG_Scenario__c(Name = 'Test Stage Scenario', STGSC_Deal_External_Id__c = 'D-111', STGSC_ScenarioId__c = 111, STGSC_Configuration__c = testConfigs[0].ID)
        };
        insert testStageScenarios;
        
        List<CPQ_STG_LineItem__c> testStageLines = new List<CPQ_STG_LineItem__c> {
            new CPQ_STG_LineItem__c(STGPSI_ScenarioId__c = 111, STGPSI_ScenarionItemId__c = 1111, STGPSI_Quantity__c = 1, STGPSI_ItemNumber__c = 'ITEM1', STGPSI_ContractPrice__c = 200, STGPSI_Price__c = 150),
            new CPQ_STG_LineItem__c(STGPSI_ScenarioId__c = 111, STGPSI_ScenarionItemId__c = 1112, STGPSI_Quantity__c = 2, STGPSI_ItemNumber__c = 'ITEM2'),
            new CPQ_STG_LineItem__c(STGPSI_ScenarioId__c = 111, STGPSI_ScenarionItemId__c = 1113, STGPSI_Quantity__c = 3, STGPSI_ItemNumber__c = 'ITEM3'),
            
            new CPQ_STG_LineItem__c(STGPSI_Quote_External_Id__c = 'PB-222', STGPSI_QuoteId__c = 222, STGPSI_ItemQuoteId__c = 2221, STGPSI_Quantity__c = 1, STGPSI_ItemNumber__c = 'ITEM1', STGPSI_ContractPrice__c = 200, STGPSI_Price__c = 150),
            new CPQ_STG_LineItem__c(STGPSI_Quote_External_Id__c = 'PB-222', STGPSI_QuoteId__c = 222, STGPSI_ItemQuoteId__c = 2222, STGPSI_Quantity__c = 2, STGPSI_ItemNumber__c = 'ITEM2')
        };
        insert testStageLines;
    }
    
    static testMethod void myUnitTest() {
        createTestData();
        
        System.runAs(testUsers[0]) {
            Test.startTest();
                CPQ_DM_Batch_LineItem p = new CPQ_DM_Batch_LineItem();
                Database.executeBatch(p);
                
                CPQ_DM_Batch_LineItem.JSONCategoryHierarchy j = new CPQ_DM_Batch_LineItem.JSONCategoryHierarchy();
                j.LowestTotalingCategoryId = 'LowestTotalingCategoryId';
                j.CategoryPropsValue = 'CategoryPropsValue';
                j.CategoryNamesValue = 'CategoryNamesValue';
            Test.stopTest();
        }
    }
*/
}