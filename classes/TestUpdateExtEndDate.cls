@isTest (seeAllData=true)
public class TestUpdateExtEndDate {   
        static testMethod void testUpdateExtEndDate() {
        RecordType rt=[Select Id from RecordType where Name='EU - GPO' Limit 1];
        Account acc=new Account();
        acc.RecordTypeId=rt.Id;
        acc.Name='Test1';
        insert acc;
         Tenders__c tender=new  Tenders__c();
         tender.Extension_No_of_Years__c='1';
         Tender.Price__c=1;
         tender.Tender_Start_Date__c= system.today()-5;
         tender.Account_GPO__c=acc.Id;
         tender.Tender_End_Date__c=system.today();
         tender.Price__c=50;
         tender.Quality__c=50;
         system.debug('**tender=' + tender);
         if(tender!=null)
         insert tender;
        }
 }