/****************************************************************************************
* Name    : Class: Case_Main
* Author  : Gautam Shah
* Date    : May 12, 2015
* Purpose : A wrapper for static functionality for Case records
* 
* Dependancies: 
*   Called By: CaseTriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
*   DATE                AUTHOR                  CHANGE
*   ----                ------                  ------
*   May 13, 2015        Gautam Shah             Commented out the calls to CaseTriggerDispatcher.executedMethods to disable recursion protection
*****************************************************************************************/
public without sharing class Case_Main 
{
    private static Map<String,Id> mapEntitlements = new Map<String,Id>();
    
    static 
    {
        for(Entitlement e : [Select Id, Name, CS_Center__c from Entitlement])
        {
            if(!mapEntitlements.containsKey(e.CS_Center__c+e.Name))
            {
                mapEntitlements.put(e.CS_Center__c+e.Name, e.Id);
            }
        }
    }
    
    public static void setSouthAfricaAttributes(List<Case> newObjects)//code copied from "Case_TriggerHandler.cls", OnBeforeInsert method
    {
        System.debug('Case_Main.setSouthAfricaAttributes');
        String caseRTName; 
        for(Case c : newObjects)
        {
            caseRTName = Utilities.recordTypeMap_Id_Name.get(c.RecordTypeId);
            //ZA CS center cases
            if(((c.Origin!=null && c.Origin.startsWith('Email - ZA')) || Utilities.CurrentUser.Country == 'ZA') && Label.ZACSCenterCaseRTs.contains(caseRTName))
            {
                c.CS_Center__c = 'ZA';
                c.BusinessHours = Utilities.businessHourMap_Name_Id.get('ZA CS');
                
                if(UserInfo.getUserRoleId()==Label.ZACSAgentRoleID && c.Status =='New')
                    c.Status='In Process';
                   
                if(c.Origin == 'Email - ZA - Enquiry' && 
                   ((c.Description != null && (c.Description.containsIgnoreCase('credit') || c.Description.containsIgnoreCase('uplift') || c.Description.containsIgnoreCase('up lift'))) ||
                    (c.Subject!=null && (c.Subject.containsIgnoreCase('credit') || c.Subject.containsIgnoreCase('uplift') || c.Subject.containsIgnoreCase('up lift')))
                   ))
                {
                    c.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Returns');
                }
                if(c.origin =='Email - ZA - Enquiry' &&
                        ((c.Description != null && (c.Description.containsIgnoreCase('Consignment') || c.Description.containsIgnoreCase('Consign'))) ||
                        (c.Subject!=null && (c.Subject.containsIgnoreCase('Consignment') || c.Subject.containsIgnoreCase('Consign')))
                        ))
                {
                    c.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Consignment');
                }    
                
                setEntitlement(c,Utilities.recordTypeMap_Id_Name.get(c.RecordTypeId));
            }
        }
        //CaseTriggerDispatcher.executedMethods.add('setSouthAfricaAttributes');
    }
    
    public static void setExportOrderCaseType(List<Case> newObjects)//code copied from "Case_TriggerHandler.cls", OnAfterInsert method
    {
        System.debug('Case_Main.setExportOrderCaseType');
        Set<ID> exportOrderCases = new Set<ID>();
        for(Case c : newObjects)
        {
            if(c.Origin == 'Email - ZA - Enquiry' && c.AccountId != null)
            {
                exportOrderCases.add(c.Id);
            }
        }
        List<Case> candidateCases = new List<Case>([Select Id, Account.RecordType.DeveloperName, RecordTypeId From Case where Id In :exportOrderCases]);
        for(Case c : candidateCases)
        {
            if(c.Account.RecordType.DeveloperName == 'EU_Distributor')
            {
                c.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Export_Order');
                
                setEntitlement(c,Utilities.recordTypeMap_Id_Name.get(c.RecordTypeId));
            }
        }
        //CaseTriggerDispatcher.executedMethods.add('setExportOrderCaseType');
    }
    
    public static void setSalesOutEmail(List<Case> newObjects)//code copied from "Case_TriggerHandler.cls", OnAfterInsert method
    {
        System.debug('Case_Main.setSalesOutEmail');
        ID uploadInvoiceRTID = Utilities.recordTypeMap_DeveloperName_Id.get('Upload_Invoice_Request');
        Set<ID> accIDs = new Set<ID>();
        Set<ID> caseIDs = new Set<ID>();
        
        for(Case c : newObjects)
        {
            if(c.RecordTypeId == uploadInvoiceRTID)
            {
                accIDs.add(c.AccountId);
                caseIDs.add(c.Id);
            }
        }
        
        if(!caseIDs.isEmpty())
        {
            Map<ID, String> acc_SOAEmail = new Map<ID, String>(); 
            if(!accIDs.isEmpty())
            {
                for(Convidien_Contact__c cc : [SELECT Distributor_Name__c, Email__c FROM Convidien_Contact__c WHERE Type__c = 'Sales Out Administrator' and Distributor_Name__c In :accIDs])
                {
                    if(!acc_SOAEmail.containsKey(cc.Distributor_Name__c))
                    {
                        acc_SOAEmail.put(cc.Distributor_Name__c, cc.Email__c);
                    }
                }
            }
            
            Map<String,String> country_email = new Map<String,String>();
            for(Country_Default_Sales_Out_Admin__c defaultAdmin : Country_Default_Sales_Out_Admin__c.getall().values())
            {
                if(!country_email.containsKey(defaultAdmin.Name))
                {
                    country_email.put(defaultAdmin.Name, defaultAdmin.Email__c);
                }
            }
            List<Case> casesToUpdate = new List<Case>();
            
            for(Case c : [SELECT AccountId, Billing_Country__c, Sales_Out_Admin_Email__c FROM Case WHERE Id In :caseIDs])
            {
                if(c.AccountId!=null && acc_SOAEmail.containsKey(c.AccountId))
                {
                    c.Sales_Out_Admin_Email__c = acc_SOAEmail.get(c.AccountId);
                    casesToUpdate.add(c);
                }
                else if(c.Billing_Country__c!=null && country_email.containsKey(c.Billing_Country__c))
                {
                    c.Sales_Out_Admin_Email__c = country_email.get(c.Billing_Country__c);
                    casesToUpdate.add(c);
                }
                else if(country_email.containsKey('Master'))
                {
                    c.Sales_Out_Admin_Email__c = country_email.get('Master');
                    casesToUpdate.add(c);
                }
            }
            
            //if(casesToUpdate.size()>0)
            //    update casesToUpdate;
        }
        //CaseTriggerDispatcher.executedMethods.add('setSalesOutEmail');
    }
    

    
    public static void setEntitlement(Case c, String strRTName)
    {
        if(mapEntitlements.containsKey(c.CS_Center__c+strRTName))
            c.EntitlementId = mapEntitlements.get(c.CS_Center__c+strRTName); 
    }
    
    public static void setEntitlement(List<Case> newObjects, Map<Id, Case> oldMap)//code copied from "Case_TriggerHandler.cls", OnBeforeUpdate method
    {
        System.debug('Case_Main.setEntitlement');
        for(Case c:newObjects)
        {
            if(c.RecordTypeId != oldMap.get(c.Id).RecordTypeId)
            {
                String strRTName = Utilities.recordTypeMap_Id_Name.get(c.RecordTypeId);
                if(mapEntitlements.containsKey(c.CS_Center__c + strRTName))
                {
                    c.EntitlementId = mapEntitlements.get(c.CS_Center__c + strRTName);
                }
            }
        }
        //CaseTriggerDispatcher.executedMethods.add('setEntitlement');
    }
    
    
    
    
    
    public static void setMasterAccount(List<Case> newObjects)//code copied from "Case_TriggerHandler.cls", OnBeforeUpdate method
    {
        System.debug('Case_Main.setMasterAccount');
        Set<String> accIDs = new Set<String>();
        Map<String, Account> accMaps = new Map<String, Account>();
        Id accountCreationRT = Utilities.recordTypeMap_DeveloperName_Id.get('Account_Creation_Case');
                
        for(Case c:newObjects)
        {
            if(c.RecordTypeId == accountCreationRT && c.Master_Account_Number__c != null)
            {
                if(!accIDs.contains(c.Temporary_Account_Number__c))
                    accIDs.add(c.Temporary_Account_Number__c);
                    
                if(!accIDs.contains(c.Master_Account_Number__c))
                    accIDs.add(c.Master_Account_Number__c);
            }
        }
        System.debug('Account Ids:'+accIDs);
        if(accIDs.size()>0)
        {
            //Commented below code to consider Account SAP Id instead of Account External ID
            /*for(Account acc : [SELECT Id, Account_External_ID__c,Business_Registration_Number__c,phone,Comments__c,Country__c,type,Email_Address__c,Website FROM Account WHERE Account_External_ID__c in :accIDs])
            {
                accMaps.put(acc.Account_External_ID__c, acc);
            } */  
            
            //Fetching SAP Id instead of external id from Line 210 to 213
            for(Account acc : [SELECT Id, Account_SAP_ID__c,Business_Registration_Number__c,phone,Comments__c,Country__c,type,Email_Address__c,Website FROM Account WHERE Account_SAP_ID__c in :accIDs])
            {
                accMaps.put(acc.Account_SAP_ID__c, acc);
            } 
            
            System.debug('SAP Accounts:'+accMaps);
            
            for(Case c : newObjects)
            {               
                if(c.RecordTypeId == accountCreationRT && c.Master_Account_Number__c != null && c.Master_Account_Number__c != '')
                {
                    if(accMaps.containsKey(c.Master_Account_Number__c))
                    {   
                        c.Master_Account__c = accMaps.get(c.Master_Account_Number__c).Id;
                        
                        if((c.status=='Closed-No Action Necessary' || c.status=='Closed-Resolved') && c.Master_Account_Number__c != c.Temporary_Account_Number__c)
                        {               
                            if(accMaps.get(c.Temporary_Account_Number__c) != null)
                            {
                                Account masterAcc = accMaps.get(c.Master_Account_Number__c);
                                Account tempAcc = accMaps.get(c.Temporary_Account_Number__c);
                                if(tempAcc.phone != null)
                                {
                                    masterAcc.Phone = tempAcc.phone;
                                }
                                if(tempAcc.Country__c != null)
                                {
                                    masterAcc.Country__c = tempAcc.Country__c;
                                }
                                if(tempAcc.Website!= null)
                                {
                                    masterAcc.Website = tempAcc.Website;
                                }
                                if(tempAcc.EMail_address__c!= null)
                                {
                                    masterAcc.Email_address__c = tempAcc.EMail_address__c;
                                }
                                if(tempAcc.Type!= null)
                                {
                                    masterAcc.Type = tempAcc.Type;
                                }
                                merge masterAcc tempAcc;
                                c.AccountId = c.CreatedByAccountID__c;
                            }
                        }   
                    }
                    else
                    {
                        c.Master_Account_Number__c.addError(Label.Account_Code_Not_Valid);
                    }
                }
            }
        }
        //CaseTriggerDispatcher.executedMethods.add('setMasterAccount');
    }
    
    public static void setCaseMilestones(List<Case> newObjects, Map<Id, case> oldMap)
    {
        System.debug('Case_Main.setCaseMilestones');
        Set<Id> acceptedCaseIDs = new Set<Id>(); 
        Set<Id> responsedCaseIDs = new Set<Id>(); 
        Set<Id> resolvedCaseIDs = new Set<Id>(); 
        Set<Id> warehouseCompleteCaseIDs = new Set<Id>();
        for(Case c:newObjects)
        {
            if(c.EntitlementId != null)
            {
                if(oldMap.get(c.Id).Case_Owner_Text__c != null && oldMap.get(c.Id).Case_Owner_Text__c != '' && c.Case_Owner_Text__c == null)
                {
                    acceptedCaseIDs.add(c.Id);
                }
                if(c.IsClosed==true && oldMap.get(c.Id).isClosed==false)
                {
                    acceptedCaseIDs.add(c.Id);
                    responsedCaseIDs.add(c.Id);
                    resolvedCaseIDs.add(c.Id);
                }
                if(oldMap.get(c.Id).Awaiting_Response_From__c=='Warehouse' && c.Awaiting_Response_From__c != 'Warehouse')
                {
                    warehouseCompleteCaseIDs.add(c.Id);
                }
            }
        }   
        
        List<CaseMilestone> milestonesToUpdate = new List<CaseMilestone>();  
                
        if(!acceptedCaseIDs.isEmpty())
        {
            List<CaseMilestone> acceptedMilestones = [Select CompletionDate from CaseMilestone where CaseId = :acceptedCaseIDs and IsCompleted = false and MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Acceptance Time')];
                    
            for(CaseMilestone cm : acceptedMilestones)
            {
                cm.CompletionDate = DateTime.now();
            }
            
            milestonesToUpdate.addAll(acceptedMilestones);
        }   
        
        if(!responsedCaseIDs.isEmpty())
        {
            List<CaseMilestone> respondedMilestones = [Select CompletionDate from CaseMilestone where CaseId = :responsedCaseIDs and IsCompleted = false and MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Response Time')];
                    
            for(CaseMilestone cm : respondedMilestones)
            {
                cm.CompletionDate = DateTime.now();
            }
            
            milestonesToUpdate.addAll(respondedMilestones);
        }   
                
        if(!resolvedCaseIDs.isEmpty())
        {
            List<CaseMilestone> resolvedMilestones = [Select CompletionDate from CaseMilestone where CaseId = :resolvedCaseIDs and IsCompleted = false and MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Resolution Time')];
                    
            for(CaseMilestone cm : resolvedMilestones)
            {
                cm.CompletionDate = DateTime.now();
            }
            
            milestonesToUpdate.addAll(resolvedMilestones);
        }
                
        if(!warehouseCompleteCaseIDs.isEmpty())
        {
            List<CaseMilestone> warehouseMilestones = [Select CompletionDate from CaseMilestone where CaseId = :warehouseCompleteCaseIDs and IsCompleted = false and MilestoneTypeId = :Utilities.milestoneMap_Name_Id.get('Warehouse Response')];
                    
            for(CaseMilestone cm : warehouseMilestones)
            {
                cm.CompletionDate = DateTime.now();
            }
            
            milestonesToUpdate.addAll(warehouseMilestones);
        }
        
        //CaseTriggerDispatcher.executedMethods.add('setCaseMilestones');
        
        if(!milestonesToUpdate.isEmpty())
            update milestonesToUpdate;
    }
    
    public static void CaseTeamBuild(List<Case> newList)
    {
        System.debug('Case_Main.CaseTeamBuild');
        // Build a Set of IDs for Active, Standard users
        Map<Id, User> userMap = Utilities.activeStandardUsers_Id_User;
        Set<Id> userIdSet = userMap.keySet();
        
        //Build a Map to reference the CaseTeamRoles
        Map <String, Id> caseTeamRoleMap = new Map<String, ID>();
        for (CaseTeamRole role : Utilities.lstCaseTeamRole)
        {
            caseTeamRoleMap.put(role.Name, role.Id);
        }
        
        List<CaseTeamMember> cTeamMemberListInsert = new List<CaseTeamMember>();  
        List<CaseTeamMember> caseTeamMemberList = [SELECT ParentId, MemberId FROM CaseTeamMember Where ParentId IN :newList];
        Boolean requestedByOnCaseTeam = FALSE;
        Boolean requestedByManagerOnCaseTeam = FALSE;
        
        for (Case c : newList)
        {
            //Create Case Team Members only if the Case is a Compensation/Sales Inquiry Case or R&T Case
            //if (c.RecordTypeId == caseRTMap.get('Compensation_Case') || c.RecordTypeId == caseRTMap.get('R_T_Case')){
            if (c.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('Compensation_Case') || c.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('R_T_Case'))
            {
                //Determine if the user in the Requested By and Manager of Requested By field is already a team member
                for (CaseTeamMember ctm : caseTeamMemberList)
                {
                    if (c.Id == ctm.ParentId)
                    {
                        if(c.Requested_By__c == ctm.MemberId)
                            requestedByOnCaseTeam = TRUE;
                            
                        if (c.Manager_of_Requested_By__c == ctm.MemberId)
                            requestedByManagerOnCaseTeam = TRUE;
                    }
                }
                
                if (c.Requested_By__c != null && !requestedByOnCaseTeam && userIdSet.contains(c.Requested_By__c))
                {
                    CaseTeamMember caseTeamMemberRequestedBy = new CaseTeamMember();
                    caseTeamMemberRequestedBy.MemberId = c.Requested_By__c;
                    caseTeamMemberRequestedBy.ParentId = c.Id;
                    caseTeamMemberRequestedBy.TeamRoleId = caseTeamRoleMap.get('Sales Rep');
        
                    cTeamMemberListInsert.add(caseTeamMemberRequestedBy);
                }
                    
                if (c.Manager_of_Requested_By__c != null && !requestedByManagerOnCaseTeam && c.Requested_By__c != c.Manager_of_Requested_By__c && userIdSet.contains(c.Manager_of_Requested_By__c))
                {
                    CaseTeamMember caseTeamMemberManagerOfRequestedBy = new CaseTeamMember();
                    caseTeamMemberManagerOfRequestedBy.MemberId = c.Manager_of_Requested_By__c;
                    caseTeamMemberManagerOfRequestedBy.ParentId = c.Id;
                    caseTeamMemberManagerOfRequestedBy.TeamRoleId = caseTeamRoleMap.get('Sales Manager');
        
                    cTeamMemberListInsert.add(caseTeamMemberManagerOfRequestedBy);      
                }
            }   
        }
        
        //CaseTriggerDispatcher.executedMethods.add('CaseTeamBuild');
        
        if (!cTeamMemberListInsert.isEmpty())
            insert cTeamMemberListInsert;
    }
    
    public static void DefaultEntitlement3(List<Case> newList)
    {
        System.debug('Case_Main.DefaultEntitlement3');
        Map<String, Id> entitlementMap = new Map<String, Id>();
        for(Entitlement e : [Select Id, Name from Entitlement Where Name Like 'Case SLA%'])
        {
            entitlementMap.put(e.Name, e.Id);
        }
        String rtCrmSupport = Utilities.recordTypeMap_DeveloperName_Id.get('CRM_Support');
       
        for(Case c : newList)
        {
            //for default Entitlement  value
            if(c.RecordTypeID == rtCrmSupport && c.Entitlement == null && (c.Type =='Problem' || c.Type == 'Service Request' || c.Type == 'Question'))
            {
                if(entitlementMap.containsKey('Case SLA Medium'))
                {
                    c.EntitlementId = entitlementMap.get('Case SLA Medium');
                }
            }
         
            if(c.RecordTypeID == rtCrmSupport && (c.Type == 'Problem' || c.Type == 'Service Request' || c.Type == 'Question'))//if(true) To change Admin_Priority__c as per Entitlement Name
            {
                if(c.Admin_Priority__c == 'Low')
                {
                    if(entitlementMap.containsKey('Case SLA Low'))
                    {
                        c.EntitlementId = entitlementMap.get('Case SLA Low');
                    }
                    system.debug('c.EntitlementId' + '>>>>>>>>>>>>>>>>'+c.EntitlementId);
                }
    
                if(c.Admin_Priority__c == 'Medium')
                {
                    if(entitlementMap.containsKey('Case SLA Medium'))
                    {
                        c.EntitlementId = entitlementMap.get('Case SLA Medium');
                    }
                }
        
                if(c.Admin_Priority__c == 'High')
                {
                    if(entitlementMap.containsKey('Case SLA High'))
                    {
                        c.EntitlementId = entitlementMap.get('Case SLA High');
                    }
                }
    
                if(c.Admin_Priority__c == 'Critical')
                {  
                    if(entitlementMap.containsKey('Case SLA Critical'))
                    {
                        c.EntitlementId = entitlementMap.get('Case SLA Critical');
                    }
                }
            }
        }
        //CaseTriggerDispatcher.executedMethods.add('DefaultEntitlement3');
    }
    
    public static void setEMSMasterAccount(List<Case> newObjects)//code copied from "Case_TriggerHandler.cls", OnBeforeUpdate method
    {
        System.debug('Case_Main.setMasterAccount');
        Set<String> accIDs = new Set<String>();
        Map<String, Account> accMaps = new Map<String, Account>();
        Id accountCreationRT = Utilities.recordTypeMap_DeveloperName_Id.get('Account_Creation_Case');
         for(Case c:newObjects)
        {
            if(c.RecordTypeId == accountCreationRT && c.Master_Account_Number__c != null)
            {
                if(!accIDs.contains(c.Temporary_Account_Number__c))
                    accIDs.add(c.Temporary_Account_Number__c);
                    
                if(!accIDs.contains(c.Master_Account_Number__c))
                    accIDs.add(c.Master_Account_Number__c);
            }
        }
        System.debug('Account Ids:'+accIDs);
        
        for(Account acc : [SELECT Id, Account_SAP_ID__c,Business_Registration_Number__c,phone,Comments__c,Country__c,type,Email_Address__c,Website FROM Account WHERE Account_SAP_ID__c in :accIDs])
            {
                
                accMaps.put(acc.Account_SAP_ID__c, acc);
            } 
            Set<String> NewaccIDs = new Set<String>();
            System.debug('SAP Accounts:'+accMaps);
            Integer i=0;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
             i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;i++;
            
            
     }   
    
}