@isTest
public class EMS_Test_Controller_AddCCIExpenses{
     public static testmethod void testApplicationPageController(){
      Account acc =new Account();
        acc.name='test acc';
        acc.BillingCountry='Country';
        insert acc;
        Approver_matrix__c app = new Approver_matrix__c();
        app.Name='SG';
        app.Marketing_Country_Controller_1__c = UserInfo.getUserId(); 
        app.Marketing_Country_Controller_2__c = UserInfo.getUserId();
        app.CCP_Coordinator_1__c = UserInfo.getUserId();
        app.CCP_Coordinator_2__c = UserInfo.getUserId();             
        app.GCC_Finance_1__c = UserInfo.getUserId();
        app.GCC_Finance_2__c = UserInfo.getUserId(); 
        app.GCC_Legal_1__c = UserInfo.getUserId();
        app.GCC_Legal_2__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_1__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_2__c = UserInfo.getUserId();
        app.GCC_PACE_1__c = UserInfo.getUserId(); 
        app.GCC_PACE_2__c = UserInfo.getUserId();
        app.GCC_R_D_1__c = UserInfo.getUserId();
        app.GCC_R_D_2__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_1__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_2__c = UserInfo.getUserId();
        app.Regional_Business_Head_1__c = UserInfo.getUserId();
        app.Regional_Business_Head_2__c = UserInfo.getUserId();
        app.Regional_PACE_Manager_1__c =UserInfo.getUserId(); 
        app.Regional_PACE_Manager_2__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_1__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_2__c = UserInfo.getUserId(); 
        app.ARO_CA_Director_1__c=UserInfo.getUserId();
        app.ARO_CA_Director_2__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_1__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_2__c= UserInfo.getUserId();
        insert app;
        
        Contact con = new Contact();
        con.FirstName='First Name';
        con.LastName='Test Contact';
        con.accountid= acc.id;
        con.Country__c='Country';
        insert con;
        
        Contact con1 = new Contact();
        con1.FirstName='FirstName';
        con1.LastName='LastName';
        con1.Country__c='Country';
        con1.accountid= acc.id;
        insert con1;
        
        Employee__c emp = new Employee__c();
        emp.Name = 'Test';
        emp.User_Details__c= UserInfo.getUserId();
        insert emp;
        
         Cost__c objCE = new Cost__c();
         objCE.Code__c = '1234';
         objCE.Cost_Element_Type__c = 'Product Package';
         objCE.Country__c = 'KR';
         objCE.Status__c = 'Active';
         objCE.Name__c = 'Test Name';
         objCE.Unit_Price__c = 10000;
         objCE.Table_Type__c = 'Test';
         objCE.Description__c = 'Test desc';
         insert objCE;
         
         Cost__c objCE1 = new Cost__c();
         objCE1.Code__c = 'ABC';
         objCE1.Cost_Element_Type__c = 'Product Package';
         objCE1.Country__c = 'KR';
         objCE1.Status__c = 'Active';
         objCE1.Name__c = 'Test Name';
         objCE1.Unit_Price__c = 10000;
         objCE1.Table_Type__c = 'Test';
         objCE1.Description__c = 'Test desc';
         insert objCE1;
         
         Cost__c objCE2 = new Cost__c();
         objCE2.Code__c = 'DEF';
         objCE2.Cost_Element_Type__c = 'Product Package';
         objCE2.Country__c = 'KR';
         objCE2.Status__c = 'Active';
         objCE2.Name__c = 'Test Name';
         objCE2.Unit_Price__c = 10000;
         objCE2.Table_Type__c = 'Test';
         objCE2.Description__c = 'Test desc';
         insert objCE2;
         
         Cost__c objCE3 = new Cost__c();
         objCE3.Code__c = 'GHI';
         objCE3.Cost_Element_Type__c = 'Product Package';
         objCE3.Country__c = 'KR';
         objCE3.Status__c = 'Active';
         objCE3.Name__c = 'Test Name';
         objCE3.Unit_Price__c = 10000;
         objCE3.Table_Type__c = 'Test';
         objCE3.Description__c = 'Test desc';
         insert objCE3;
         
        Profile p = [select id from profile where name='System Administrator'];   
        
        User u = new User(profileId = p.id, username = 'c@mixmaster.com', email = 'c@k.com', 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', country='SG',IsEMSEventOwner__c=true,
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname');
        
        System.runAs(u){
            EMSMarketingGBU__c mycs = new EMSMarketingGBU__c(Name= 'SG;EBD');
            mycs.ARO_Marketing_Head_1__c= u.id;
            mycs.ARO_Marketing_Head_2__c=u.id;
            insert mycs;
            EMS_EVent__c EMSRec = new EMS_Event__c();
            EMSrec.Upload_External_Unique_Id__c= '1248';         
            EMSRec.GBU_Primary__c='test';
            EMSRec.Division_Primary__c='test';
            EMSRec.Start_Date__c = System.Today().adddays(22);
            EMSRec.End_Date__c = System.Today().addDays(45);
            EMSRec.Plan_Budget_Amt__c=50000;
            EMSRec.CPA_Required__c='Yes';
            EMSRec.Name= 'test event';
            EMSrec.Event_Native_Name__c='test event name';
            EMSRec.Location_Country__c='China';  
            EMSRec.Location_State__c='Shanghai';
            EMSRec.Expense_Type__c='Sales';
            EMSRec.Event_Type__c='CME';
            EMSRec.Training_conducted_in_Hosp_Training_Cent__c='Yes';
            EMSRec.HTC_Name__c='Test';
            EMSRec.Primary_COT__c='AB';
            EMSRec.Reoccurring_or_New__c='New';
            EMSRec.Event_Host__c='Covidien';
            EMSRec.Location_City__c='Test City';
            EMSRec.Venue__c= 'Test Venue'; 
            EMSRec.First_Payment_Date__c=System.Today();
            EMSRec.Max_Attendees__c=400;
            EMSRec.Any_recipient_s_who_are_KOL_s_HCP_s__c='YES';
            EMSRec.Any_HCP_Invitees_from_Outside_Asia__c='YES';
            EMSRec.Benefit_Summary__c='Test Summary';
            EMSRec.CCI__c='YES';
            EMSRec.Event_Owner_User__c = u.Id;
            EMSrec.Event_Status__c='Planned';
            EMSrec.GBU_Primary__c='EBD';
            EMSrec.Is_Event_21_days_and_GCC_Approval_done__c=true;
            insert EMSrec;
        
        
        Apexpages.CurrentPage().getParameters().put('Id',objCE.Id);
        ApexPages.currentPage().getParameters().put('EventId',EMSrec.Id);
        ApexPages.currentPage().getParameters().put('step','2');
        ApexPages.StandardController SC = new ApexPages.standardController(objCE);
        EMS_Controller_AddCCIExpenses objAddCCIExp = new EMS_Controller_AddCCIExpenses(SC);
        
        EMS_Controller_AddCCIExpenses.CCIExpenseWrapper objWrap = new EMS_Controller_AddCCIExpenses.CCIExpenseWrapper(null,null,null,null);
        
        objAddCCIExp.selectedsearchcriteria=1;
        ApexPages.StandardSetController ssc1 = objAddCCIExp.setCon;
        
        objAddCCIExp.selectedsearchcriteria=2;
        ApexPages.StandardSetController ssc2 = objAddCCIExp.setCon;
        
        objAddCCIExp.selectedsearchcriteria=3;
        ApexPages.StandardSetController ssc3 = objAddCCIExp.setCon;
        
        objAddCCIExp.selectedsearchcriteria=1;
        objAddCCIExp.sampleCode='JKL';
        objAddCCIExp.sampleDescription= 'Test desc';       
        objAddCCIExp.country='KR';
        objAddCCIExp.SearchContacts();
        
        objAddCCIExp.selectedsearchcriteria=1;
        objAddCCIExp.sampleCode='XYZ';
        objAddCCIExp.sampleDescription= 'Test desc';       
        objAddCCIExp.country='KR';
        objAddCCIExp.SearchContacts();
        
        objAddCCIExp.selectedsearchcriteria=2;
        objAddCCIExp.sampleCode='ABC';
        objAddCCIExp.sampleDescription= 'Test desc';       
        objAddCCIExp.country='KR';
        objAddCCIExp.SearchContacts();
        
        objAddCCIExp.selectedsearchcriteria=2;
        objAddCCIExp.sampleCode='UVW';
        objAddCCIExp.sampleDescription= 'Test desc';       
        objAddCCIExp.country='KR';
        objAddCCIExp.SearchContacts();
         objAddCCIExp.selectedsearchcriteria=1;
        objAddCCIExp.contextitem=objCE.Id;
        objAddCCIExp.doselectItem();
        objAddCCIExp.AddCCISamples(); 
        
        objAddCCIExp.selectedsearchcriteria=2;
        objAddCCIExp.contextitem=objCE.Id;
        objAddCCIExp.doselectItem();
        objAddCCIExp.AddCCISamples(); 
        
        objAddCCIExp.selectedsearchcriteria=3;
        objAddCCIExp.contextitem=objCE.Id;
        objAddCCIExp.doselectItem();
        objAddCCIExp.AddCCISamples(); 
        
        objAddCCIExp.Back();
        objAddCCIExp.rendersearchfields();
        objAddCCIExp.AddCCISamples();
        objAddCCIExp.SearchContacts();
        objAddCCIExp.selectedsearchcriteria=1;
        objAddCCIExp.getContacts();

        objAddCCIExp.selectedsearchcriteria=2;
        objAddCCIExp.getContacts();
        
        objAddCCIExp.selectedsearchcriteria=3;
        objAddCCIExp.getContacts();
        
        objAddCCIExp.doSelectItem();
        objAddCCIExp.doDeselectItem();
        objAddCCIExp.getSelectedCount();
        objAddCCIExp.doNext();
        objAddCCIExp.doPrevious();
        objAddCCIExp.getHasPrevious();
        objAddCCIExp.getHasNext();
        objAddCCIExp.getPageNumber();
        objAddCCIExp.getTotalPages();
        
        objAddCCIExp.ReciepientId= objCE.Id;
        objAddCCIExp.deleteContact();
       
        }
     }
}