/**
Test the controller extension for the CPQ_SSG_SendToCustomerService page

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
05/12/2016      Bryan Fry           Created
===============================================================================
*/

@isTest
global class Test_CPQ_SSG_Controller_SendToCustServ
{
    @isTest static void Test_CPQ_SSG_Controller_ComparePricing()
    {
        PageReference pageRef = Page.CPQ_SSG_ComparePricing;
        Test.setCurrentPage(pageRef);

        RecordType sellToAccount = RecordType_u.fetch(Account.class,'US_Account_Hospital');

        Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US', RecordTypeId = sellToAccount.Id, Account_External_Id__c = '1234');
        insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1', Apttus_Surgical_Product__c = true, Category__c = 'Test');
        insert product;

        Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
        insert opp;

        Apttus_Config2__PriceList__c priceList = cpqPriceList_c.SSG;

        RecordType quickQuote = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,'Hardware_or_Product_Quote');

        ERP_Account__c erp = new ERP_Account__c(Parent_Sell_To_Account__c = acct.Id, Address_1__c = '1', Address_2__c = '2', Address_3__c = '3', Address_4__c = '4', City__c = 'c', State_Region__c = 'sr', Zip_Postal_Code__c = '11111', ERP_Account_Type__c = 'S');
        insert erp;

        // Create Quote/Proposal record
        Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acct.Id, Apttus_Proposal__Opportunity__c = opp.Id, Apttus_Proposal__Approval_Stage__c='In Review', RecordTypeId = quickQuote.Id, Apttus_QPApprov__Approval_Status__c='Approved', Apttus_Proposal__Payment_Term__c = '1 Year', Committed_Stapling_Vessel_Sealing__c = 'Yes', Select_Customer_Service_Inbox__c = 'Test: not@a.real.emal.com');
        prop.Trade_In_Amount_ForceTriad__c = 'Other';
        prop.Trade_In_Amount_ForceTriad_Other__c = 5000;
        prop.Trade_In_Quantity_ForceTriad__c = 5;
        prop.Trade_In_Serial_Numbers_ForceTriad__c = '1,2,3';
        prop.Trade_In_Amount_ForceFXCS__c = 'Other';
        prop.Trade_In_Amount_ForceFXCS_Other__c = 2000;
        prop.Trade_In_Quantity__c = 5;
        prop.Trade_In_Serial_Numbers_ForceFXCS__c = '1,2,3,4';
        prop.Trade_In_Amount_Competitor_Unit__c = 'Other';
        prop.Trade_In_Amount_Competitor_Unit_Other__c = 4000;
        prop.Trade_In_Quantity_Competitor_Unit__c = 5;
        prop.Trade_In_Serial_Numbers_Competitor_Unit__c = '1,2,3,4,5';
        prop.ERP_Ship_to_Address__c = erp.Id;
        insert prop;

        // Create attachments
        List<Attachment> attachments = new List<Attachment>();
        attachments.add(new Attachment(Name = 'file1.txt', Body = Blob.valueOf('12345'), ParentId = prop.Id));
        attachments.add(new Attachment(Name = 'file2.txt', Body = Blob.valueOf('54321'), ParentId = prop.Id));
        insert attachments;

        // Create Price List Item records
        List<Apttus_Config2__PriceListItem__c> priceListItems = new List<Apttus_Config2__PriceListItem__c>();
        priceListItems.add(new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = priceList.Id, Apttus_Config2__ProductId__c = product.Id));
        insert priceListItems;

        // Create Product Configuration record
        Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c(Apttus_Config2__Status__c='Saved', Apttus_Config2__VersionNumber__c=1, Apttus_Config2__BusinessObjectType__c='Proposal', Apttus_Config2__PriceListId__c = priceList.Id, Apttus_Config2__EffectivePriceListId__c = priceList.Id);
        insert pc;

        Test.startTest();
            CPQ_SSG_Controller_SendToCustomerService con = new CPQ_SSG_Controller_SendToCustomerService(new ApexPages.StandardController(prop));
            con.selectedAttachments.add(attachments[0].Id);
            con.selectedAttachments.add(attachments[1].Id);
            con.sendToCustomerService();
        Test.stopTest();
    }
}