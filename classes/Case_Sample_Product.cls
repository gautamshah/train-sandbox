global with sharing class Case_Sample_Product {
 
 /****************************************************************************************
   * Name    : Case_Sample_Product
   * Author  : Shawn Clark
   * Date    : 04/20/2015 
   * Purpose : Extend the Case--> Sample relationship to display Sample Product List
 *****************************************************************************************/
 

    public final Case caseDetails {get;set;}
    public list <Sample_Product__c> cd {get;set;}
    public Boolean ShowMobile {get; set;} 
    public Boolean ShowDesktop {get; set;} 
    
    // SOQL query loads the case, with related Sample & Sample Product Data
    public Case_Sample_Product(ApexPages.StandardController controller) {
        String qid = ApexPages.currentPage().getParameters().get('id');
        String theQuery = 'SELECT Sample_Product__c.ID, Sample_Product__c.Product_SKU__c, Sample_Product__c.Product_SKU__r.Name, Sample_Product__c.Product_SKU_ANZ__c, Sample_Product__c.Product_SKU_ANZ__r.Name, Sample_Product__c.Units_Of_Measure__c, Sample_Product__c.Quantity__c, Sample_Product__c.Other_Comments__c, Sample_Product__c.Sample_Request__r.Name, Sample_Product__c.Sample_Request__r.Case__r.id FROM Sample_Product__c WHERE Sample_Product__c.Sample_Request__r.Case__r.id = :qid';
        this.cd = Database.query(theQuery);
        ShowDesktop = false;
        ShowMobile = false;  
      }
      
   //Set ShowMobile
   public Void ShowMobile(){  
       ShowMobile = true;  
       ShowDesktop = false;
       }    
         
    //Set ShowDesktop  
   public Void ShowDesktop(){  
       ShowDesktop = true;  
       ShowMobile = false;
       }     
      
  }