@isTest

Public Class AgreementNotificationTest {
    static testmethod void UpdateAgreement(){
 
   Id pId = [select Id from Profile where name = 'CRM Admin Support'].Id;   
   List<Agreement__c> Agreements = new List<Agreement__c>();
    
    User u = new User();
         u.LastName = 'ABC';
         u.Business_Unit__c = 'RMS';
         u.Franchise__c = 'ABC Company';
         u.email = 'testZXmgr1@medtronic.com';
         u.alias = 'tzr9342';
         u.username = 'test1234234234@medtronic.com';
         u.communityNickName = 'test1234234234@medtronic.com';
         u.ProfileId = pId;
         u.CurrencyIsoCode='USD'; 
         u.EmailEncodingKey='ISO-8859-1';
         u.TimeZoneSidKey='America/New_York';
         u.LanguageLocaleKey='en_US';
         u.LocaleSidKey ='en_US';
         u.Country = 'US';
         insert u;
    
    Date myDate = Date.newInstance(2017, 2, 01);
    
    //Create Agreement in Two Week Notification Stage
    Agreement__c agr = new Agreement__c();
    agr.Name = 'R999999999';
    agr.Status__c = 'Open (Assets Still At Customer Site)';
    agr.Act_Number__c = 'R999999999';
    agr.Act_Number__c = 'R999999999';
    agr.Reminder_Sent__c = FALSE;
    agr.Contract_Start_Date__c = MyDate.AddDays(-30);
    agr.Contract_Completion_Date__c = MyDate.AddDays(30);
    agr.Overdue_Alert_Count__c = 0;
    agr.Contract_Start_Date__c = MyDate.AddDays(-30);
    agr.Contract_Completion_Date__c = MyDate.AddDays(30);

    Agreements.add(agr);
    insert Agreements;
    
    //Create a Child Asset
    Demo_Product__c a = new Demo_Product__c();
    a.Name = 'US_MANS_99999';
    a.Status__c = 'At Customer Site';
    a.Serial_No__c = 'XYZ12323445';
    a.Asset_Operations_Email__c = 'RMS@medtronic.com.test';
    a.Order_Type__c = '25';
    a.Act_Number__c = 'R999999999';
    a.Act_Number2__c = agr.id;
    a.OwnerId = u.id;
    insert a;
    
    
    //Updates to fire the trigger
    
    //Change Status to Two Week Reminder
    for (integer i = 0; i < Agreements.size(); i++){
        Agreements[i].Trigger_Email_Alert__c = TRUE;
        Agreements[i].Notification_Stage__c = 'Two_Week_Reminder';
      }
    update Agreements; 
    
    //Change Status to One Day Reminder
    for (integer i = 0; i < Agreements.size(); i++){
        Agreements[i].Trigger_Email_Alert__c = TRUE;
        Agreements[i].Notification_Stage__c = 'One_Day_Reminder';
      }
    update Agreements;
    
    //Change Status to Overdue Alert Cycle
    for (integer i = 0; i < Agreements.size(); i++){
        Agreements[i].Trigger_Email_Alert__c = TRUE;
        Agreements[i].Notification_Stage__c = 'Overdue_Alert_Cycle';
        Agreements[i].Next_Overdue_Alert_Date__c = MyDate.AddDays(32);
      }
    update Agreements;
    
   //Change Status to Final Alert
    for (integer i = 0; i < Agreements.size(); i++){
        Agreements[i].Trigger_Email_Alert__c = TRUE;
        Agreements[i].Notification_Stage__c = 'Final_Alert';
        Agreements[i].Next_Overdue_Alert_Date__c = MyDate.AddDays(35);
      }
    update Agreements;
    
    
    // Verify that Agreement ReminderSent is updated
    Agreement__c agr5 = [SELECT Two_Week_Notification__c FROM Agreement__c WHERE Id = :agr.Id];
    System.assert(TRUE, agr5.Two_Week_Notification__c); 
    System.Debug(agr5.Two_Week_Notification__c);
  }
  
}