/**

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016/10/21      Paul Berglund       Created.
===============================================================================
*/

@isTest
public class Product2_UOM_gTest
{
    // Test going from Apttus Text to Product Abbreviations
    @isTest
    static void translateText()
    {
        system.assertEquals(Product2_UOM_g.defaultCode,
                            Product2_UOM_g.translateText('random'),
                            'The default value should have been return');
        
        system.assertEquals(null, Product2_UOM_g.translateText(null),
                            'A null value should return a null value');
        
        for(string s : Product2_UOM_g.Text2Code.keySet())
            system.assertEquals(Product2_UOM_g.Text2Code.get(s),
                                Product2_UOM_g.translateText(s),
                                'The values should match - ' + s);
    }
    
    // Test going from Product Abbreviations to Apttus Text
    @isTest
    static void translateCode()
    {
        system.assertEquals(Product2_UOM_g.defaultText,
                            Product2_UOM_g.translateCode('random'),
                            'The default value should have been return');
        
        system.assertEquals(null, Product2_UOM_g.translateCode(null),
                            'A null value should return a null value');
        
        for(string s : Product2_UOM_g.Code2Text.keySet())
            system.assertEquals(Product2_UOM_g.Code2Text.get(s),
                                Product2_UOM_g.translateCode(s),
                                'The values should match - ' + s);
    }
}