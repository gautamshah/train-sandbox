/**
Functions that operate on both CPQ Proposal records.

CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
2016-11-01  Isaac Lewis     Created. Migrated logic from CPQ_Proposal_After.trigger
2016-11-03  Unknown         Moved logic from main() method to setup()
2016-11-17  Unknown         Added setAgreementOnParticipatingFacility_LineItems()
2017-01-10  Isaac Lewis     Restored setup() method to auto-add ERP address
===============================================================================
*/
public class cpqProposal_u extends cpqDeal_u
{
    //
    // Apttus_Proposal__Proposal__c
    //
    public cpqProposal_u()
    {
        super();
    }

    public List<Apttus_Proposal__Proposal__c> sobjs
    {
        get { return (List<Apttus_Proposal__Proposal__c>)super.get(); }
        set { super.set(value); }
    }

    public void clearConfigurationFinalizedDate()
    {
        system.debug(string.valueOf(this));
        for(Apttus_Proposal__Proposal__c p : this.sobjs)
            p.Apttus_QPConfig__ConfigurationFinalizedDate__c = null;
    }

    public cpqProposal_u(Set<Id> ids)
    {
        super(Apttus_Proposal__Proposal__c.getSObjectType(), ' WHERE Id IN ' + ids);
    }

    public override void main(
        boolean isExecuting,
        boolean isInsert,
        boolean isUpdate,
        boolean isDelete,
        boolean isBefore,
        boolean isAfter,
        boolean isUndelete,
        List<sObject> newList,
        Map<Id, sObject> newMap,
        List<sObject> oldList,
        Map<Id, sObject> oldMap,
        integer size)
    {
        super.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size
        );

        // Logic custom to the Proposal

        // Cast the base types into the specific types
        List<Apttus_Proposal__Proposal__c> newVersions = cast(newList);
        Map<Id, Apttus_Proposal__Proposal__c> oldVersions = (Map<Id, Apttus_Proposal__Proposal__c>)oldMap;

        if (isAfter && (isInsert || isUpdate)) {
            setAgreementOnParticipatingFacility_LineItems(newVersions, oldVersions);
            setup(newVersions,oldVersions);
        }
        
        if (isBefore && (isInsert || isUpdate))
        {
            for (Apttus_Proposal__Proposal__c p : cast(newList))
                p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);

            if (isInsert)
                //Populate "Covidien RMS US Master" PriceList when created
                setPriceList(newVersions);

            if (isUpdate)
            {
                //Update recordtype to locked when accepted for Demo and Current Pricing Quote proposal
                CPQ_ProposalProcesses.UpdateRecordType(cast(newList), cast(oldMap));
        
                //Update approval status to Not Submitted from Cancelled
                //Set Proposal Exp Date 45 days out after presented date
                CPQ_ProposalProcesses.UpdateStatusWhenCancelled(cast(newList), cast(oldMap));
        
                //Update approval status to Not Submitted after approved if any fields are changed
                CPQ_ProposalProcesses.ResetApprovalStatusWhenFieldChanged(cast(newList), cast(oldMap));

                // Create new contact and associate it as the primary contact on the proposal if needed
                if (CPQ_ProposalProcesses.CreatePrimaryContact_Exec_Num > 0)
                {
                    CPQ_ProposalProcesses.CreatePrimaryContact(cast(newList), cast(oldMap));
                    CPQ_ProposalProcesses.CreatePrimaryContact_Exec_Num--;
                }
            }
            
            //Update ERP_Ship_to_Address__c with ERP that has the highest sales
            CPQ_ProposalProcesses.UpdateERPBySales(cast(newList), cast(oldMap));

            // Set fields needed for Apttus standard quote approval email
            CPQ_SSG_Proposal_Processes.SetProposalApprovalTemplateFields(cast(newList), cast(oldMap));      
        }
    }

    //=========================================================
    //  Populate Proposal Apttus_QPConfig__PriceListId__c field
    //  with the Price List associated with the Owner of the
    //  Proposal - if the Owner doesn't have a Organization Name
    //  assigned to them, we default to the RMS price list
    //===========================================================
    public static void setPriceList(List<Apttus_Proposal__Proposal__c> newList)
    {
        Set<Id> ownerIds = new Set<Id>();
        for(Apttus_Proposal__Proposal__c p : newList)
        {
            User u = cpqUser_c.fetch(p.OwnerId);
            
            OrganizationNames_g.Abbreviations OrgName =
                OrganizationNames_g.valueOf(u.OrganizationName__c);
                
            Apttus_Config2__PriceList__c pl = cpqPriceList_c.fetch(OrgName);
            
           	p.Apttus_QPConfig__PriceListId__c = (pl == null) ? cpqPriceList_c.RMS.Id : pl.Id;
        }
    }

    private Apttus_Proposal__Proposal__c cast(sObject sobj)
    {
        return (Apttus_Proposal__Proposal__c)sobj;
    }

    private List<Apttus_Proposal__Proposal__c> cast(List<sObject> sobjs)
    {
        return (List<Apttus_Proposal__Proposal__c>)sobjs;
    }

    private Map<Id, Apttus_Proposal__Proposal__c> cast(Map<Id, sObject> sobjs)
    {
        return (Map<Id, Apttus_Proposal__Proposal__c>)sobjs;
    }


    private void setAgreementOnParticipatingFacility_LineItems(
        List<Apttus_Proposal__Proposal__c> newVersions,
        Map<Id, Apttus_Proposal__Proposal__c> oldVersions)
    {
        Map<Id, Id> ProposalId2MasterAgreementId = new Map<Id, Id>();

        for(Apttus_Proposal__Proposal__c newVersion : newVersions)
        {
            if (sObject_c.fieldChanged(
                              newVersion,
                              cpqProposal_c.oldVersion(newVersion, oldVersions),
                              cpqProposal_c.FIELD_MasterAgreementId))
            {
                ProposalId2MasterAgreementId.put(newVersion.Id, (Id)newVersion.get(cpqProposal_c.FIELD_MasterAgreementId));
            }
        }

        cpqParticipatingFacility_Lineitem_u.setAgreement(ProposalId2MasterAgreementId);
    }

    public override Set<Id> extractOwners(List<sObject> newList)
    {
        Set<Id> userIds = new Set<Id>();
        if (newlist != null && !newList.isEmpty())
            for(Apttus_Proposal__Proposal__c p : (List<Apttus_Proposal__Proposal__c>)newList)
                userIds.add(p.OwnerId);

        return userIds;
    }

    public override Set<Id> extractApprovers(List<sObject> newList)
    {
        Set<Id> userIds = new Set<Id>();
        if (newlist != null && !newList.isEmpty())
            for(Apttus_Proposal__Proposal__c p : cast(newList))
            {
                userIds.add(p.SellerApprover__c);
                userIds.add(p.RSM_Approver__c);
                userIds.add(p.ZVP__c);
                userIds.add(p.SVP_User__c);
            }

        return userIds;
    }

    private static boolean fieldChanged(
        Apttus_Proposal__Proposal__c newVersion,
        Map<Id, Apttus_Proposal__Proposal__c> oldVersions,
        Schema.sObjectField field)
    {
        return sObject_c.fieldChanged((sObject)newVersion, (Map<Id, sObject>)oldVersions, field);
    }

    private static void setup(
        List<Apttus_Proposal__Proposal__c> newList,
        Map<Id, Apttus_Proposal__Proposal__c> oldMap)
    {
        if (CPQ_Trigger_Profile__c.getInstance().CPQ_Proposal_After__c == true) return;

        Set<ID> setOpptyPriceBook = new Set<ID>();
        Set<ID> loadERPAddresses = new Set<ID>();
        Map<ID, Apttus_Proposal__Proposal__c> updateConfig = new Map<ID, Apttus_Proposal__Proposal__c>();

        for (Apttus_Proposal__Proposal__c p : newList)
        {
            //Auto select opp price book when apttus proposal is created
            if (p.get(cpqProposal_c.FIELD_Opportunity) != null &&
                !fieldChanged(p, oldMap, cpqProposal_c.FIELD_Opportunity))
            // Replaces
            //   oldMap != null &&
            //     p.get(cpqProposal_c.FIELD_Opportunity) == oldMap.get(p.Id).get(cpqProposal_c.FIELD_Opportunity)))
            {
                setOpptyPriceBook.add(p.Id);
            }

            //Load all ERPs of Primary account and in proposal when account lookup updated
            if (p.Apttus_Proposal__Account__c != null &&
                !fieldChanged(p, oldMap, cpqProposal_c.FIELD_Account))
            // Replaces
            //    !(oldMap != null &&
            //      p.Apttus_Proposal__Account__c == oldMap.get(p.Id).Apttus_Proposal__Account__c))
            {
                loadERPAddresses.add(p.Id);
            }

            //Update Config with proposal COOP fields, When proposal COOP field changed
            if (oldMap != null)
            {
                Apttus_Proposal__Proposal__c oldp = oldMap.get(p.Id);

                List<Schema.SObjectField> fieldsToCompare = new List<Schema.SObjectField>
                {
                    cpqProposal_c.FIELD_Duration,
                    cpqProposal_c.FIELD_BuyoutAsOfDate,
                    cpqProposal_c.FIELD_BuyoutAmount,
                    cpqProposal_c.FIELD_TaxGrossNetOfTradeIn,
                    cpqProposal_c.FIELD_SalesTaxPercent,
                    cpqProposal_c.FIELD_TBDAmount,
                    cpqProposal_c.FIELD_TotalAnnualSensorCommitmentAmount
                };

                if (!sObject_c.equal(p, oldp, fieldsToCompare)) updateConfig.put(p.ID, p);

                ////if (p.Duration__c != oldp.Duration__c ||
                ////        p.Buyout_as_of_Date__c != oldp.Buyout_as_of_Date__c ||
                ////        p.Buyout_Amount__c != oldp.Buyout_Amount__c ||
                ////        p.Tax_Gross_Net_of_TradeIn__c != oldp.Tax_Gross_Net_of_TradeIn__c ||
                ////        p.Apttus_Proposal__Sales_Tax_Percent__c != oldp.Apttus_Proposal__Sales_Tax_Percent__c ||
                ////        p.TBD_Amount__c != oldp.TBD_Amount__c ||
                ////        p.Total_Annual_Sensor_Commitment_Amount__c != oldp.Total_Annual_Sensor_Commitment_Amount__c) {
                ////    updateConfig.put(p.ID, p);
                ////}
            }
        }

        //system.debug('cpqProposal_u.setup.before: ' + Limits.getQueries());
        CPQ_PriceListProcesses.SelectOpportunityPriceBook(setOpptyPriceBook);
        CPQ_ProposalProcesses.LoadERPAddresses(loadERPAddresses);
        CPQ_ProposalProcesses.UpdateConfig(updateConfig);
        //system.debug('cpqProposal_u.setup.after: ' + Limits.getQueries());
    }
}