/**
Test class

CPQ_Proposal_Distributor_After
CPQ_ProposalProcesses.UpdateDistributorSelected

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_Proposal_Distributor_Triggger {
    static Proposal_Distributor__c testDistributor;
    
    static void createTestData() {
        List<Account> testAccounts = new List<Account>{
            new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111'),
            new Account(Status__c = 'Active', Name = 'Test Facilitiy', Account_External_ID__c = 'US-222222')
        };
        insert testAccounts;
        
        Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', Apttus_Proposal__Account__c = testAccounts[0].ID);
        insert testProposal;
        
        testDistributor = new Proposal_Distributor__c(Proposal__c = testProposal.ID, Account__c = testAccounts[1].ID);
    }
    
    static testMethod void myUnitTest() {
        createTestData();
        
        Test.startTest();
            insert testDistributor;
            delete testDistributor;
        Test.stopTest();
    }

    static testMethod void TestDisabledTrigger() {
        try {
        CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Participating_Facility_After__c = true, CPQ_Product2_After__c = true, CPQ_Product2_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_ProductConfiguration_Before__c = true, CPQ_Proposal_After__c = true, CPQ_Proposal_Before__c = true, CPQ_Proposal_Distributor_After__c = true, CPQ_Rebate_After__c = true, CPQ_Rebate_Agreement_After__c = true);
        insert testTrigger;
        } catch (Exception e) {}
        
        createTestData();
        
        Test.startTest();
            insert testDistributor;         
        Test.stopTest();
    }
}