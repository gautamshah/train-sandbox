global class ScheduleCreateCyclePeriod implements Schedulable{
    
    public Boolean testMode=false;
    String batchQuery;  
   global void execute(SchedulableContext sc) {
        //String RecordTypeDevName='\'ASIA_Distributor\',\'Country_Accounts\'';
        Date dt=Date.today();
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
                
        //batchQuery='select Country__c,Id, BillingCountry,Email_Address__c ,Submission_Offset_Day__c,Date_Format__c,(select ID, email from contacts where type__c = \'DIS Contact\' limit 1),(Select Id from Cycle_Periods__r where Cycle_Period_Reference__r.Month__c=\''+CurrentCyclePeriodMonth+'\' and Cycle_Period_Reference__r.Year__c=\''+CurrentCyclePeriodYear+'\') from Account where (RecordType.DeveloperName in ('+RecordTypeDevName+') or (RecordType.DeveloperName = \'ASIA_Sub_Dealer\' and  Country__c = \'CN\' and Co_operative__c = true)) and Status__c = \'Active\'';        
        batchQuery='select Country__c,Id, BillingCountry,Email_Address__c ,Submission_Offset_Day__c,Date_Format__c,(select ID, email from contacts where type__c = \'DIS Contact\' limit 1),(Select Id from Cycle_Periods__r where Cycle_Period_Reference__r.Month__c=\''+CurrentCyclePeriodMonth+'\' and Cycle_Period_Reference__r.Year__c=\''+CurrentCyclePeriodYear+'\') from Account where IsPartner = true and Status__c = \'Active\'';
        batchQuery += this.Testmode ? ' LIMIT 200':'';
        System.debug('===batchQuery==='+batchQuery);
      BatchCreateCyclePeriod b = new BatchCreateCyclePeriod(batchQuery); 
      database.executebatch(b);
   }

}