public class EMS_WeeklyReminder {



    public Id CurrentUserId{get;set;}
    public List<EMS_Event__c> LEvents = new List<EMS_Event__c>();
    Set<Id> AllEventIds = new Set<Id>();
    
    public List<EMS_Event__c> getProcessInssteps(){
    EMS_GCC_Approvers_Delegated__c myMCC = EMS_GCC_Approvers_Delegated__c.getvalues(CurrentUserId);
    Set<String> Status = new Set<String>{'Rejected','Approved','Recalled'};
    List <ProcessInstanceWorkitem> Lpes = new List<ProcessInstanceWorkitem>([SELECT Id,ActorId,ProcessInstance.TargetObjectId,ProcessInstance.Id,ProcessInstance.LastActorId,ProcessInstance.Status FROM ProcessInstanceWorkitem where ActorId= :myMCC.Delegated_Approval__c and ProcessInstance.TargetObjectId!=null and ProcessInstance.Status ='Pending']);
    if(!Lpes.IsEMpty()){
    for(ProcessInstanceWorkitem p : Lpes){
     
       AllEventIds.add(p.ProcessInstance.TargetObjectId);  
    }
    LEvents = new List<EMS_Event__c>([Select Owner_Country__c,Days_Before_Event_Start__c,Name, Benefit_Summary__c,CPA_Status__c,Total_HCP_Expenses__c,ecpa_URL__c,Year__c,Month__c,Start_Date__c,CPA_Request_Date__c from EMS_Event__c where Id in:(AllEventIds)]);
    }
    return LEvents;
    }    
    
    
   /* public string generate18CharId(string id){
        
// This method will take a 15 character ID and return its 18 character ID:
        
     if (id == null){ 
          return null;
     }
     if (id.length() != 15) {
          return null;
     }
     string suffix = '';
     integer flags;
        
     for (integer i = 0; i < 3; i++) {
          flags = 0;
          for (integer j = 0; j < 5; j++) {
               string c = id.substring(i * 5 + j,i * 5 + j + 1);
               //Only add to flags if c is an uppercase letter:
               if (c.toUpperCase().equals(c) && c >= 'A' && c <= 'Z') {
                    flags = flags + (1 << j);
               }
          }
          if (flags <= 25) {
               suffix = suffix + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.substring(flags,flags+1);
          }else{
               suffix = suffix + '012345'.substring(flags-25,flags-24);
          }
     }
     return id + suffix;
}
*/
}