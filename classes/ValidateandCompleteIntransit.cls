public class ValidateandCompleteIntransit {
    
    public String hasError='F';
	public String ErrorAlert{get;set;}
	public String ChecktIfvalidate{get;set;}
	public String CyclePeriodId{get;set;}
	public Id distributorId{get;set;}
	List<Cycle_Period__c> lstPendingCyclePeriod{get;set;}
	public String CyclePeriodChoice{get;set;}
    public ValidateandCompleteIntransit(ApexPages.StandardController stdController) {
    	CyclePeriodId=stdController.getId();
    	List<Cycle_Period__c> lstPendingCyclePeriodQuery=[Select Id,Sales_In_Acknowledgement_Date__c,Distributor_Name__c,Name,Cycle_Period_Reference__r.Name, (Select Id from In_Transit__r limit 1) from Cycle_Period__c where Id=:CyclePeriodId];
    	distributorId=lstPendingCyclePeriodQuery[0].Distributor_Name__c;
    	if(lstPendingCyclePeriodQuery==null||lstPendingCyclePeriodQuery.size()==0)
    	{
    		ErrorAlert='N';
    //		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
    		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
    		
            ApexPages.addMessage(errormsg);
    	}
    	else
    	{
    		 List<Cycle_Period__c> lstPendingCyclePeriodTemp=new List<Cycle_Period__c>();
    		for(Cycle_Period__c cpv:lstPendingCyclePeriodQuery){
    			if(cpv.In_Transit__r!=null&&cpv.In_Transit__r.size()>0)
    				lstPendingCyclePeriodTemp.add(cpv);
    		
    		}
    		if(lstPendingCyclePeriodTemp==null||lstPendingCyclePeriodTemp.size()==0)
	    	{
	    		ErrorAlert='N';
	    //		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
	    		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
	            ApexPages.addMessage(errormsg);
	    	}
	    	else
	    	{
	    		lstPendingCyclePeriod=lstPendingCyclePeriodTemp;
	    	}
    		
    	}
    }

    public Pagereference validateAndComplete()
    {
        Pagereference pgRef=null;
        
       //System.debug('List Size: '+lstPendingCyclePeriod.size());
        If(lstPendingCyclePeriod==null||lstPendingCyclePeriod.size()==0)
        {
            ErrorAlert='N';
      //      ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
            ApexPages.addMessage(errormsg);
        }
        else
        {
        	System.debug('List Size: '+lstPendingCyclePeriod.size());
            String tempErrorCheck='';
            String ErrorMessages='';
			
                for(Cycle_Period__c cp:lstPendingCyclePeriod)
                {
                   ErrorAlert='F';
                   if(cp.Sales_In_Acknowledgement_Date__c==null)
                    	cp.Sales_In_Acknowledgement_Date__c=Date.today();
                }
                update lstPendingCyclePeriod;
            //    ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'Data Submitted Successfuly');
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.Data_submitted_successfully);
                ApexPages.addMessage(errormsg);
          
            
        }//End of if(lstSalesOuttoValidate!=null&&lstSalesOuttoValidate.size()>0)
               
        
        return null;
    }
    
	

}