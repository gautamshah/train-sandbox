/**
Controller class for the Visualforce page CPQ_SSG_SendToCustomerService.
Get additional fields needed for a proposal and call code to send an email to
customer service.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
04/29/2016      Bryan Fry           Created
===============================================================================
*/
public with sharing class CPQ_SSG_Controller_SendToCustomerService {
	public Apttus_Proposal__Proposal__c prop {get;set;}
	public List<SelectOption> attachmentOptions {get;set;}
	public List<String> selectedAttachments {get;set;}

	public CPQ_SSG_Controller_SendToCustomerService(ApexPages.StandardController stdCon) {
		prop = (Apttus_Proposal__Proposal__c)stdCon.getRecord();
		if (prop != null && prop.Id != null) {
			prop = [Select Id, Name, Apttus_Proposal__Account__r.Name, Owner.Name, Owner.Email, OwnerId, toLabel(Select_Customer_Service_Inbox__c), E1_Customer_Number__c,
                		   ERP_Ship_to_Address__r.Address_1__c, ERP_Ship_to_Address__r.Address_2__c, ERP_Ship_to_Address__r.Address_3__c,
                		   ERP_Ship_to_Address__r.Address_4__c, ERP_Ship_to_Address__r.City__c, ERP_Ship_to_Address__r.State_Region__c,
                		   ERP_Ship_to_Address__r.Zip_Postal_Code__c,
			               Trade_In_Amount_ForceTriad__c, Trade_In_Amount_ForceTriad_Other__c, Trade_In_Quantity_ForceTriad__c, Trade_In_Serial_Numbers_ForceTriad__c,
			               Trade_In_Amount_ForceFXCS__c, Trade_In_Amount_ForceFXCS_Other__c, Trade_In_Quantity__c, Trade_In_Serial_Numbers_ForceFXCS__c,
			               Trade_In_Amount_Competitor_Unit__c, Trade_In_Amount_Competitor_Unit_Other__c, Trade_In_Quantity_Competitor_Unit__c, Trade_In_Serial_Numbers_Competitor_Unit__c,
			               Apttus_Proposal__Proposal_Name__c
			        From Apttus_Proposal__Proposal__c
			        Where Id = :prop.Id];
		}

		List<Attachment> attachments = [Select Id, Name, CreatedDate From Attachment Where ParentId = :prop.Id Order By CreatedDate DESC];
		attachmentOptions = new List<SelectOption>();

		if (attachments != null && !attachments.isEmpty()) {
			for (Attachment att: attachments) {
				attachmentOptions.add(new SelectOption(att.Id, att.Name + '(' + att.CreatedDate + ')'));
			}
		}

		selectedAttachments = new List<String>();
	}

	public PageReference sendToCustomerService() {
		CPQ_ProposalProcesses.SendQuickQuoteActivationEmail(prop, selectedAttachments);
		return new PageReference('/' + prop.Id);
	}
}