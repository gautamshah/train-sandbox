public with sharing class dmFolders
{
	public static Map<Id, dmFolder__c> All { get; set; }
	public static Map<Id, dmFolder__Share> Shares { get; set; }
	public static Map<Id, dmFolder__c> Applications { get; set; }
	public static Map<Id, dmFolder__c> Folders { get; set; }
 
	static
	{
    	string query = dm.buildSOQL(dmFolder__c.getSObjectType(), null, null);
    	All = new Map<Id, dmFolder__c>((List<dmFolder__c>)Database.query(query));

    	query = dm.buildSOQL(dmFolder__Share.getSObjectType(), null, null);
    	Shares = new Map<Id, dmFolder__Share>((List<dmFolder__Share>)Database.query(query));

		Applications = new Map<Id, dmFolder__c>();
		Folders = new Map<Id, dmFolder__c>();
		for (dmFolder__c f : All.values())
		{
			if (f.isApplication__c)
				Applications.put(f.Id, f);
			else
				Folders.put(f.Id, f);
		}
	}	    
}