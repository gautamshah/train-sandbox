public class CEP_LibraryFiles {
 
 
 public List<ContentVersion> CurrentMonthdocuments;
 Set<Id> SAllContVersionsIds = new Set<Id>();
 Map<Id,ContentDistribution> MCVidtoHostedURLS = new Map<Id,ContentDistribution>(); 
 public boolean IsCurrentMonthRecords{
 get
 {
 If(getCurrentMonthdocuments().size()==0)
     IsCurrentMonthRecords=false;
     else
     IsCurrentMonthRecords=true;
     
     return IsCurrentMonthRecords;
 }
 set;}
 
 public List<ContentVersion> alldocuments;
 public List<ContentVersion> getalldocuments(){
     Contentworkspace c =[select Id, Name, Description from ContentWorkspace where name='ASIA CEP' limit 1];

     Set<Id> SortedCVIds = new Set<Id>();
     alldocuments = new List<ContentVersion>(); 
     Map<Id,ContentDocument> MCEPContentDocs = new Map<Id,ContentDocument>([Select Id,Title,ParentId,(Select ContentDocumentId,Title,GBU__c,Date_of_Completion__c,Language,Media_Type__c,Approved__c,Country_Initiated__c,HCP_Affiliation__c,Name_of_HCP__c,Product_Name__c,Link__c from ContentVersions order by CreatedDate DESC LIMIT 1),(Select Id,DistributionPublicUrl,ViewCount,ContentVersionId from ContentDistributions) from ContentDOcument where ParentId= :c.id]);
     for(ContentDocument eachCD : MCEPContentDocs.values()){
         if(!eachCD.ContentDistributions.isEmpty()){
             SortedCvIds.add(eachCD.ContentVersions[0].Id);    
             } 
     }
     alldocuments= [Select Id,ContentDocumentId,Title,GBU__c,Date_of_Completion__c,Language,Media_Type__c,Approved__c,Country_Initiated__c,HCP_Affiliation__c,Name_of_HCP__c,Product_Name__c,Link__c from ContentVersion WHERE Id in :SortedCVIds ORDER BY GBU__c ASC];
     return alldocuments;
 }
 
 
 
 public List<ContentVersion> getCurrentMonthdocuments(){
     CurrentMonthdocuments = new List<ContentVersion>();
     Contentworkspace c =[select Id, Name, Description from ContentWorkspace where name='ASIA CEP' limit 1];
 
     Set<Id> SortedCVIds = new Set<Id>();
     Integer mnth = System.Today().MOnth();
     Integer yr = System.Today().Year();
     Map<Id,ContentDocument> MCEPCurrentMonthContentDocs = new Map<Id,ContentDocument>([Select Id,Title,ParentId,(Select ContentDocumentId,Title,GBU__c,Media_Type__c,Date_of_Completion__c,Language ,Approved__c,Country_Initiated__c,HCP_Affiliation__c,Name_of_HCP__c,Product_Name__c,Link__c from ContentVersions order by CreatedDate DESC LIMIT 1),(Select Id,DistributionPublicUrl,ViewCount,ContentVersionId from ContentDistributions) from ContentDocument where ParentId=:c.id]);
     for(ContentDocument eachCD : MCEPCurrentMonthContentDocs .values()){
         if(!eachCD.ContentDistributions.isEmpty()){
             SortedCvIds.add(eachCD.ContentVersions[0].Id);  
         }   
     }
     CurrentMonthDocuments = [Select Id,ContentDocumentId,Title,GBU__c,Date_of_Completion__c,Language,Media_Type__c,Approved__c,Country_Initiated__c,HCP_Affiliation__c,Name_of_HCP__c,Product_Name__c,Link__c from ContentVersion WHERE Id in :SortedCVIds AND CreatedDate=LAST_N_DAYS:30 ORDER BY GBU__c ASC];
     return CurrentMonthdocuments ;
 }


 public Map<Id,ContentDistribution> getMCVidtoHostedURLS(){
 for(ContentVersion EachCV : Getalldocuments()){
 
     SAllContVersionsIds.add(eachCV.Id);
 }
 List<ContentDistribution> LConDistr = new List<ContentDistribution>([Select Id,CreatedDate,DistributionPublicUrl,ViewCount,ContentVersionId from ContentDistribution where ContentVersionId in :SAllContVersionsIds]);
 for(ContentDistribution eachCD : LConDistr ){
 
        if(MCVidtoHostedURLS==null){
        
            MCVidtoHostedURLS.put(eachCD.ContentVersionId,eachCD);    
        }
 
        else if(!(MCVidtoHostedURLS==null) && !MCVidtoHostedURLS.keyset().Contains(eachCD.ContentVersionId)){
        
            MCVidtoHostedURLS.put(eachCD.ContentVersionId,eachCD);    
        }  
        else if(MCVidtoHostedURLS.keyset().Contains(eachCD.ContentVersionId) && MCVidtoHostedURLS.get(eachCD.ContentVersionId).createdDate < eachCd.CreatedDate){
            
            MCVidtoHostedURLS.put(eachCD.ContentVersionId,eachCD);        
        }
       
 }
 System.debug('*******'+MCVidtoHostedURLS);
 return MCVidtoHostedURLS;
 }
}