/**
* @Name: CustomAgreementLaunchController
* @Description: Controller for launching Apttus pages via mobile actions
* @UsedBy: All visualforce pages named CustomAgreementLaunch<ACTION_NAME>
*
* CHANGE HISTORY
* ===============================================================================
* DATE            NAME                DESC
* 2016-03-09      Isaac Lewis         Created.
* ===============================================================================
*/

public with sharing class CustomAgreementLaunchController {

	private final Apttus__APTS_Agreement__c agreement;

	public CustomAgreementLaunchController(ApexPages.StandardController stdController) {
		this.agreement = (Apttus__APTS_Agreement__c)stdController.getRecord();
	}

	// public PageReference doLaunchConfigureProducts(){
	//	// Scrub PO: Configure_Products
	//	// Custom Kit: Configure_Products_SSG_NoAdj_NoGroup
	// 	// redirect to page with ConfigureProducts action
	// 	PageReference pageRef = Page.Apttus_QPConfig__ProposalConfiguration;
	// 	pageRef.getParameters().put('id', this.agreement.Id);
	// 	pageRef.getParameters().put('flow', 'SSG Cart');
	// 	pageRef.getParameters().put('useAdvancedApproval','true');
	// 	pageRef.getParameters().put('useDealOptimizer','false');
	// 	// pageRef.getParameters().put('','');
	// 	pageRef.setRedirect(true);
	// 	return pageRef;
	// }

	public PageReference doLaunchPreview(){
		PageReference pageRef = Page.Apttus__SelectTemplate;
		pageRef.getParameters().put('id',this.agreement.Id);
		pageRef.getParameters().put('action','Preview_Agreement');
		pageRef.getParameters().put('templateType','Agreement');
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchGenerate(){
		PageReference pageRef = Page.Apttus__SelectTemplate;
		pageRef.getParameters().put('id',this.agreement.Id);
		pageRef.getParameters().put('action','Generate_Agreement');
		pageRef.getParameters().put('templateType','Agreement');
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchRegenerate(){
		PageReference pageRef = Page.Apttus__SelectTemplate;
		pageRef.getParameters().put('id',this.agreement.Id);
		pageRef.getParameters().put('action','Regenerate_Agreement');
		pageRef.getParameters().put('templateType','Agreement');
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchActivate(){
		PageReference pageRef = Page.Apttus__AgreementActivate;
		pageRef.getParameters().put('id',this.agreement.Id);
		pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference doLaunchSendForReview(){
		PageReference pageRef = Page.Apttus__SendEmail;
		pageRef.getParameters().put('id',this.agreement.Id);
		pageRef.getParameters().put('action','Send_To_Other_Party_For_Review');
		pageRef.setRedirect(true);
		return pageRef;
	}

}