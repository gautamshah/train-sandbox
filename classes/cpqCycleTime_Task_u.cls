/****************************************************************************************
Name    : Class: cpqCycleTime_Task_logic
Author  : Paul Berglund
Date    : 10/03/2016
Purpose : Logic to handle Cycle Time-related timestamps for Tasks
     
========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE OR REF ID
----------  ----------------  ------------------------------
10/03/2016  Paul Berglund     Fixed issue where 2 Tasks caused duplicate object in upsert
                              Moved Task related code to cpqCycleTime_Task
10/05/2016  Paul Berglund     20161005-A
03/09/2017  Bryan Fry         Commented out all code to help test coverage for AV-280 deploy
*****************************************************************************************/
public class cpqCycleTime_Task_u extends cpqCycleTime_u
{
    // Capture the cycle times associated with changes to a Task related to Proposals or Agreements
    protected override void capture(
        List<sObject> objNewList,
        Map<Id,sObject> objOldMap,
        Boolean isInsert,
        Boolean isUpdate)
    {
/*
    	List<Task> newList = (List<Task>)objNewList;
    	Map<Id, Task> oldMap = (Map<Id, Task>)objOldMap;

        DateTime NOW = system.Now();
        
        Map<TaskTypes, Set<Id>> agreementTaskIds = new Map<TaskTypes, Set<Id>>();
        agreementTaskIds.put(TaskTypes.CDQueueDates, new Set<Id>());
        agreementTaskIds.put(TaskTypes.EquipmentAdminDates, new Set<Id>());
        agreementTaskIds.put(TaskTypes.CustomerServiceDates, new Set<Id>());
        agreementTaskIds.put(TaskTypes.CustomerRebateDates, new Set<Id>());
        agreementTaskIds.put(TaskTypes.PricingDates, new Set<Id>());
        agreementTaskIds.put(TaskTypes.SentForReviewDate, new Set<Id>());

        Map<TaskTypes, Set<Id>> proposalTaskIds = new Map<TaskTypes, Set<Id>>();
        proposalTaskIds.put(TaskTypes.CDQueueDates, new Set<Id>());

        // Get Tasks associated with Agreements or Proposals that match a subject or Owner we are tracking
        for (Task t: newList)
        {
            if (t.WhatId != null)
            {
                string sObjectTypeName = t.WhatId.getSobjectType().getDescribe().getName();
                TaskTypes source = null;
                if (sObjectTypeName == 'Apttus__APTS_Agreement__c')
                {
                    if (t.Subject == NON_STANDARD_AGREEMENT_SUBJECT)
                        source = TaskTypes.CDQueueDates;
                    else if (t.OwnerId == cpqCycleTime_u.UserQueueName2Id.get(EQUIP_ADMIN_QUEUE))
                        source = TaskTypes.EquipmentAdminDates;
                    else if (t.OwnerId == cpqCycleTime_u.UserQueueName2Id.get(CUSTOMER_SERVICE_QUEUE))
                        source = TaskTypes.CustomerServiceDates;
                    else if (t.OwnerId == cpqCycleTime_u.UserQueueName2Id.get(END_CUSTOMER_REBATES_QUEUE))
                        source = TaskTypes.CustomerRebateDates;
                    else if (t.OwnerId == cpqCycleTime_u.UserQueueName2Id.get(PRICING_QUEUE))
                        source = TaskTypes.PricingDates;
                    else if (t.Subject == SENT_FOR_REVIEW_SUBJECT)
                        source = TaskTypes.SentForReviewDate;
                    
                    if (source != null) agreementTaskIds.get(source).add(t.WhatId);
                }
                else if (sObjectTypeName == 'Apttus_Proposal__Proposal__c')
                {
                    if (t.OwnerId == cpqCycleTime_u.UserQueueName2Id.get(CONTRACT_DEV_QUEUE))
                        source = TaskTypes.CDQueueDates;
                        
                    if (source != null) proposalTaskIds.get(source).add(t.WhatId);
                }
            }
        }

        Set<Id> agreementIds = new Set<Id>();
        Set<Id> proposalIds = new Set<Id>();
        for(TaskTypes key : agreementTaskIds.keySet())
            agreementIds.addAll(agreementTaskIds.get(key));
            
        for(TaskTypes key : proposalTaskIds.keySet())
            proposalIds.addAll(proposalTaskIds.get(key));

        system.debug('agreementIds: ' + agreementIds);
        system.debug('proposalIds: ' + proposalIds);

        // Get associated Cycle Time rows
        Map<Id, CPQ_Cycle_Time__c> Id2CycleTime = new Map<Id, CPQ_Cycle_Time__c>();
        Map<Id, CPQ_Cycle_Time__c> ModifiedCycleTimes = new Map<Id, CPQ_Cycle_Time__c>();
        
        Id2CycleTime.putAll(new Map<Id, CPQ_Cycle_Time__c>(
            [SELECT Id,
                    Proposal__c,
                    Agreement__c,
                    Agreement_Enter_CD_Queue_Date__c,
                    Agreement_In_Progress_CD_Queue_Date__c,
                    Agreement_Enter_CD_Queue_Counter__c,
                    Agreement_Exit_CD_Queue_Date__c,
                    Equipment_Admin_Start_Date__c,
                    Equipment_Admin_In_Progress_Date__c,
                    Equipment_Admin_End_Date__c,
                    Customer_Service_Start_Date__c,
                    Customer_Service_In_Progress_Date__c,
                    Customer_Service_End_Date__c,
                    End_Customer_Rebates_Start_Date__c,
                    End_Customer_Rebates_In_Progress_Date__c,
                    End_Customer_Rebates_End_Date__c,
                    Pricing_Start_Date__c,
                    Pricing_In_Progress_Date__c,
                    Pricing_End_Date__c,
                    Agreement_Sent_for_Review_Date__c,
                    Proposal_Enter_CD_Queue_Date__c,
                    Proposal_In_Progress_CD_Queue_Date__c,
                    Proposal_Enter_CD_Queue_Counter__c,
                    Proposal_Exit_CD_Queue_Date__c
             FROM CPQ_Cycle_Time__c
             WHERE Agreement__c in :agreementIds OR
                   Proposal__c in :proposalIds]));
        
        // 
        // 20161005-A
        // Little bit of logic change:
        //
        // The .values() of a Map<Id, CPQ_Cycle_Time__c> are a List that could have duplicate
        // entries for the same sObject.
        //
        // Instead, created a Map<Id, CPQ_Cycle_Time__c> with the results of a SELECT, this will
        // have 1 copy of each sObject
        // Then map Agreement.Id to CycleTime.Id with Map<Id, Id> 
        // And Proposal.Id to CycleTime.Id with Map<Id, Id>
        //
        // We are still do modifications to 1 sObject, we just access that object a little
        // differently.
        //
        // This allows us to do an upsert on the .values() of the Map of the sObjects and
        // guaranteed that there will be only one entry for any DML
        //
        
        Map<Id,Id> ObjectId2CycleTimeId = new Map<Id,Id>();
        for (CPQ_Cycle_Time__c ct : Id2CycleTime.values()) {
            if (ct.Agreement__c != null) {
                ObjectId2CycleTimeId.put(ct.Agreement__c, ct.Id);
            }
            if (ct.Proposal__c != null) {
                ObjectId2CycleTimeId.put(ct.Proposal__c, ct.Id);
            }
        }

        // Set any needed cycle time fields for inserts, where a queue start time is set
        for(Task t : newList)
        {
            if (ObjectId2CycleTimeId.containsKey(t.WhatId))
            {
                Task o;
                if (!isInsert) o = oldMap.get(t.Id);

                CPQ_Cycle_Time__c ct = Id2CycleTime.get(ObjectId2CycleTimeId.get(t.WhatId));
                system.debug('ct: ' + ct);
            	
            	boolean wasModified = false;
                if (ct != null)
                {
                    if (agreementTaskIds.get(TaskTypes.CDQueueDates).contains(t.WhatId))
                        wasModified = AgreementCDQueueDates(ct, t, o, NOW, isInsert);
                    else if (agreementTaskIds.get(TaskTypes.EquipmentAdminDates).contains(t.WhatId))
                        wasModified = AgreementEquipmentAdminDates(ct, t, o, NOW, isInsert);
                    else if (agreementTaskIds.get(TaskTypes.CustomerServiceDates).contains(t.WhatId))
                        wasModified = AgreementCustomerServiceDates(ct, t, o, NOW, isInsert);
                    else if (agreementTaskIds.get(TaskTypes.CustomerRebateDates).contains(t.WhatId))
                        wasModified = AgreementCustomerRebateDates(ct, t, o, NOW, isInsert);
                    else if (agreementTaskIds.get(TaskTypes.PricingDates).contains(t.WhatId))
                        wasModified = AgreementPricingDates(ct, t, o, NOW, isInsert);
                    else if (agreementTaskIds.get(TaskTypes.SentForReviewDate).contains(t.WhatId))
                        wasModified = AgreementSentForReviewDate(ct, t, o, NOW, isInsert);
                    else if (proposalTaskIds.get(TaskTypes.CDQueueDates).contains(t.WhatId))
                        wasModified = ProposalCDQueueDates(ct, t, o, NOW, isInsert);
                }
                if (wasModified) ModifiedCycleTimes.put(ct.Id, ct);
            }
        }
        
        system.debug('Task: ' + Id2CycleTime.values());
        upsert ModifiedCycleTimes.values();
*/
    }

/*    
    private enum TaskTypes
    { 
        CDQueueDates,
        EquipmentAdminDates,
        CustomerServiceDates,
        CustomerRebateDates,
        PricingDates,
        SentForReviewDate 
    }
    
    private boolean StatusChangedTo(string newStatus, string oldStatus, string status)
    {
		return (newStatus == status && oldStatus != status);
    }
    
    private boolean AgreementCDQueueDates(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
            if (cycleTime.Agreement_Enter_CD_Queue_Date__c == null)
            {
                cycleTime.Agreement_Enter_CD_Queue_Date__c = NOW;
                cycletime.Agreement_Enter_CD_Queue_Counter__c = 1;
            }
            else
                cycleTime.Agreement_Enter_CD_Queue_Counter__c++;

            wasModified = true; // The above always modifies
        }
        else
        {
            if (StatusChangedTo(t.Status, o.Status, COMPLETED))
            {
				cycleTime.Agreement_Exit_CD_Queue_Date__c = NOW;
                wasModified = true;
            }

			if (StatusChangedTo(t.Status, o.Status, IN_PROGRESS) &&
                 cycleTime.Agreement_In_Progress_CD_Queue_Date__c == null)
            {
            	cycleTime.Agreement_In_Progress_CD_Queue_Date__c = NOW;
                wasModified = true;
            }
        }
		return wasModified;
    }

    private boolean AgreementEquipmentAdminDates(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
            // Equipment Admin Start Date
            if (cycleTime.Equipment_Admin_Start_Date__c == null)
            {
            	cycleTime.Equipment_Admin_Start_Date__c = NOW;
	            wasModified = true;
            }
        }
        else
        {
            // Equipment Admin End Date
            
            if (StatusChangedTo(t.Status, o.Status, COMPLETED))
            {
            	cycleTime.Equipment_Admin_End_Date__c = NOW;
                wasModified = true;
            }
                    
            if (StatusChangedTo(t.Status, o.Status, IN_PROGRESS) &&
                cycleTime.Equipment_Admin_In_Progress_Date__c == null)
            {
            	cycleTime.Equipment_Admin_In_Progress_Date__c = NOW;
                wasModified = true;
            }
        }
		return wasModified;
    }

    private boolean AgreementCustomerServiceDates(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
            // Customer Service Start Date
            if (cycleTime.Customer_Service_Start_Date__c == null)
            {
            	cycleTime.Customer_Service_Start_Date__c = NOW;
	            wasModified = true;
            }
        }
        else
        {
            // Customer Service End Date
            if (StatusChangedTo(t.Status, o.Status, COMPLETED))
            {
            	cycleTime.Customer_Service_End_Date__c = NOW;
	            wasModified = true;
            }
                 
            if (StatusChangedTo(t.Status, o.Status, IN_PROGRESS) &&
                cycleTime.Customer_Service_In_Progress_Date__c == null)
            {
            	cycleTime.Customer_Service_In_Progress_Date__c = NOW;
	            wasModified = true;
            }
        }
		return wasModified;
    }

    private boolean AgreementCustomerRebateDates(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
            // End Customer Rebates Start Date
            if (cycleTime.End_Customer_Rebates_Start_Date__c == null)
			{
            	cycleTime.End_Customer_Rebates_Start_Date__c = NOW;
	            wasModified = true;
			}
        }
        else
        {
            // End Customer Rebates End Date
            if (StatusChangedTo(t.Status, o.Status, COMPLETED))
            {
            	cycleTime.End_Customer_Rebates_End_Date__c = NOW;
            	wasModified = true;
            }

			if (StatusChangedTo(t.Status, o.Status, IN_PROGRESS) &&
                cycleTime.End_Customer_Rebates_In_Progress_Date__c == null)
            {
            	cycleTime.End_Customer_Rebates_In_Progress_Date__c = NOW;
	            wasModified = true;
            }
        }
		return wasModified;
    }

    private boolean AgreementPricingDates(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
            // Pricing Start Date
            if (cycleTime.Pricing_Start_Date__c == null)
            {
                cycleTime.Pricing_Start_Date__c = NOW;
	            wasModified = true;
            }
        }
        else
        {
            // Pricing End Date
            if (StatusChangedTo(t.Status, o.Status, COMPLETED))
            {
            	cycleTime.Pricing_End_Date__c = NOW;
	            wasModified = true;
            }

            if (StatusChangedTo(t.Status, o.Status, IN_PROGRESS) &&
                cycleTime.Pricing_In_Progress_Date__c == null)
            {
            	cycleTime.Pricing_In_Progress_Date__c = NOW;
	            wasModified = true;
            }
        }
		return wasModified;
    }
    
    private boolean AgreementSentForReviewDate(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
        	if (cycleTime.Agreement_Sent_for_Review_Date__c == null)
        	{
        		cycleTime.Agreement_Sent_for_Review_Date__c = NOW;
				wasModified = true;
        	}
        }
		return wasModified;
    }

    private boolean ProposalCDQueueDates(CPQ_Cycle_Time__c cycleTime, Task t, Task o, DateTime now, boolean isInsert)
    {
    	boolean wasModified = false;
        if (isInsert)
        {
            // Proposal Enter CD Queue Date
            if (cycleTime.Proposal_Enter_CD_Queue_Date__c == null)
            {
                cycleTime.Proposal_Enter_CD_Queue_Date__c = NOW;
                cycleTime.Proposal_Enter_CD_Queue_Counter__c = 1;
                wasModified = true;
            }
            else
            {
                cycleTime.Proposal_Enter_CD_Queue_Counter__c++;
                wasModified = true;
            }
        }
        else
        {
            // Proposal Exit CD Queue Date
            if (StatusChangedTo(t.Status, o.Status, COMPLETED))
            {
            	cycleTime.Proposal_Exit_CD_Queue_Date__c = NOW;
	            wasModified = true;
            }

            if (StatusChangedTo(t.Status, o.Status, IN_PROGRESS) &&
                cycleTime.Proposal_In_Progress_CD_Queue_Date__c == null)
            {
            	cycleTime.Proposal_In_Progress_CD_Queue_Date__c = NOW;
	            wasModified = true;
            }
        }
		return wasModified;
    }
*/
}