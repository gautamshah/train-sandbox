/**
Batch class to create contact from staging table

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11/21      Yuli Fintescu       Created
11/02/2016   Paul Berglund   Commented out to save space, but keep in case we
                             do something similar again
===============================================================================

To Run
-------
        ****TURN OFF EMAILS!!!****
        CPQ_DM_Batch_Contact p = new CPQ_DM_Batch_Contact();
        Database.executeBatch(p, 2000);
        
Data clean up
-------------

CPQ: 005K0000002GNNs
QA: 005K0000002GHCZ
IC: 005U0000000eoT2
Prod:

run once. test in force.com explorer first
===============================================
for (List<CPQ_STG_Quote_Contact__c> contacts : [Select ID, STGQC_DM_Error__c, STGQC_Contact__c, STGQC_Contact__r.CreatedByID, STGQC_Contact__r.CreatedDate 
                                                From CPQ_STG_Quote_Contact__c 
                                                Where STGQC_Contact__c <> NULL]) {
                                                
    List<Contact> toDelete = new List<Contact>();
    
    for (CPQ_STG_Quote_Contact__c l : contacts) {
        if (l.STGQC_DM_Error__c == null)
            toDelete.add(new Contact(ID = l.STGQC_Contact__c));
    }
    
    if (toDelete.size() > 0) {
        delete toDelete;
    }
}

        To Bring them back
        ---------------------
        List<Contact> toUndelete = new List<Contact>();
        
        for (Contact c : [SELECT ID, ISDeleted, Name, CreatedByID, CreatedDate, LastModifiedByID, LastModifiedDate FROM Contact Where ISDeleted = true and LastModifiedDate = TODAY ALL ROWS]) {
            toUndelete.add(c);
        }
        
        undelete toUndelete;

run once
===============================================
for (List<CPQ_STG_Quote_Contact__c> contacts : [Select ID, 
                                STGQC_DM_Error__c, 
                                STGQC_Merge_To__c,
                                STGQC_Contact__c
                            From CPQ_STG_Quote_Contact__c]) {
    List<CPQ_STG_Quote_Contact__c> updates = new List<CPQ_STG_Quote_Contact__c>();
    
    for (CPQ_STG_Quote_Contact__c l : contacts) {
        if (l.STGQC_DM_Error__c != null || l.STGQC_Merge_To__c != null || l.STGQC_Contact__c != null) {
            l.STGQC_DM_Error__c = null;
            l.STGQC_Merge_To__c = null;
            l.STGQC_Contact__c = null;
            updates.add(l);
        }
    }
    
    if (updates.size() > 0)
        update updates;
}

Clean up Stages:
----------------
ddelete [SELECT ID FROM CPQ_STG_Quote_Contact__c LIMIT 10000];

*/

global class CPQ_DM_Batch_Contact // implements Database.Batchable<sObject>
{
/*
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select STGPS_CustomerContactId__c, ' + 
                            'STGPS_Proposal__c, ' + 
                            'STGPS_DM_Error__c, ' + 
                            'Name, Id ' +
                        'From CPQ_STG_Proposal__c ' + 
                        'Where STGPS_To_Test__c = true and STGPS_Proposal__c <> NULL and STGPS_CustomerContactId__c <> NULL';       
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<CPQ_STG_Proposal__c> scope) {
        //only handle the contacts we have proposal for
        Set<String> contNums = new Set<String>();
        for (CPQ_STG_Proposal__c c : scope) {
            contNums.add(c.STGPS_CustomerContactId__c);
        }
        
        List<CPQ_STG_Quote_Contact__c> stgContacts = [Select STGQC_SurName__c,
                            STGQC_RoomNumber__c, 
                            STGQC_Pristine__c, 
                            STGQC_PhoneNumber__c, 
                            STGQC_IsConverted__c, 
                            STGQC_GivenName__c, 
                            STGQC_FaxNumber__c, 
                            STGQC_EmailAddress__c, 
                            STGQC_DM_Error__c, 
                            STGQC_CustomerContactId__c, 
                            STGQC_Contact__c, 
                            STGQC_ContactTitle__c, 
                            STGQC_Account_External_ID__c, 
                            Name, Id 
                        From CPQ_STG_Quote_Contact__c
                        Where STGQC_CustomerContactId__c in: contNums]; 
        
//=========================================================
//    Extract data from stages to build linkage structures
//=========================================================
        Map<String, CPQ_STG_Quote_Contact__c> stages = new Map<String, CPQ_STG_Quote_Contact__c> ();
        
        Set<String> acctExternalIDs = new Set<String>();
        for (CPQ_STG_Quote_Contact__c c : stgContacts) {
            stages.put(c.STGQC_CustomerContactId__c , c);
            
            if (c.STGQC_Account_External_ID__c != null)
                acctExternalIDs.add(c.STGQC_Account_External_ID__c);
        }
        
        //account records by Account_External_ID__c
        Map<String, Account> accounts = new Map<String, Account> ();
        for (Account a : [Select Account_External_ID__c, ID From Account Where Account_External_ID__c in: acctExternalIDs]) {
            accounts.put(a.Account_External_ID__c, a);
        }        
        
        //contact records by Account_External_ID__c
        Map<String, List<Contact>> acctToContacts = new Map<String, List<Contact>> ();
        for (Contact c : [Select ID, FirstName, LastName, Email, AccountId, Account.Account_External_ID__c From Contact Where Account.Account_External_ID__c in: acctExternalIDs]) {
            List<Contact> contacts;
            if (acctToContacts.containsKey(c.Account.Account_External_ID__c)) {
                contacts = acctToContacts.get(c.Account.Account_External_ID__c);
            } else {
                contacts = new List<Contact>();
                acctToContacts.put(c.Account.Account_External_ID__c, contacts);
            }
            contacts.add(c);
        }
        
        Schema.RecordTypeInfo conRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('In Process Connected Contact');//'Connected Contact US'
        System.Debug('*** conRT ' + conRT);
        
        List<Contact> conToInsert = new List<Contact>();
        List<String> conOneToOne = new List<String>();
        
//=========================================================
//    Creating contacts
//=========================================================
        //used for dedup records within the stage table
        Map<String, Map<String, String>> stageDupsByEmail = new Map<String, Map<String, String>>();
        Map<String, Map<String, String>> stageDupsByName = new Map<String, Map<String, String>>();
        
        for (CPQ_STG_Quote_Contact__c c : stgContacts) {
            if (!accounts.containsKey(c.STGQC_Account_External_ID__c)) {
                c.STGQC_DM_Error__c = (c.STGQC_DM_Error__c == null ? '' : c.STGQC_DM_Error__c) + 'Account does not exist. Skip.\n';
                continue;
            }
            
            if (c.STGQC_SurName__c == null) {
                c.STGQC_DM_Error__c = (c.STGQC_DM_Error__c == null ? '' : c.STGQC_DM_Error__c) + 'Missing Last Name. Skip.\n';
                continue;
            }
            
            //stage contact dedup keys
            String emailKey = (c.STGQC_EmailAddress__c == null ? '' : c.STGQC_EmailAddress__c.toLowerCase());
            String nameKey = (c.STGQC_SurName__c == null ? '' : c.STGQC_SurName__c.toLowerCase()) + ', ' + 
                            (c.STGQC_GivenName__c == null ? '' : c.STGQC_GivenName__c.toLowerCase());
            
            //check if contact already exists in the system for the same account
            //if does, link to the existing contact
            map<String, Contact> mapContactsConcatWithEmail = new map<String, Contact>();
            map<String, Contact> mapContactsConcatWithName = new map<String, Contact>();
            Account a = accounts.get(c.STGQC_Account_External_ID__c);
            
            if (acctToContacts.containsKey(c.STGQC_Account_External_ID__c)) {
                List<Contact> contacts = acctToContacts.get(c.STGQC_Account_External_ID__c);
                for (Contact existing : contacts) {
                    if(existing.Email != null)
                        mapContactsConcatWithEmail.put(existing.Email.toLowerCase(), existing);
                    
                    if(existing.LastName != null || existing.FirstName != null) {
                        String nameKey1 = (existing.LastName == null ? '' : existing.LastName.toLowerCase()) + ', ' + 
                                (existing.FirstName == null ? '' : existing.FirstName.toLowerCase());
                        
                        mapContactsConcatWithName.put(nameKey1, existing);
                    }
                }
            }
            
            if (!String.isEmpty(emailKey) && mapContactsConcatWithEmail.containsKey(emailKey)) {
                c.STGQC_DM_Error__c = (c.STGQC_DM_Error__c == null ? '' : c.STGQC_DM_Error__c) + 'Existing Contact by Email.\n';
                c.STGQC_Contact__c = mapContactsConcatWithEmail.get(emailKey).ID;
                continue;
            } else if (!String.isEmpty(nameKey) && mapContactsConcatWithName.containsKey(nameKey)) {
                c.STGQC_DM_Error__c = (c.STGQC_DM_Error__c == null ? '' : c.STGQC_DM_Error__c) + 'Existing Contact by Name.\n';
                c.STGQC_Contact__c = mapContactsConcatWithName.get(nameKey).ID;
                continue;
            } 
            
            //check if the records are duplicated within the stage table
            Map<String, String> dupsByEmail, dupsByName;
            if (stageDupsByEmail.containsKey(c.STGQC_Account_External_ID__c)) {
                dupsByEmail = stageDupsByEmail.get(c.STGQC_Account_External_ID__c);
            } else {
                dupsByEmail = new Map<String, String>();
                stageDupsByEmail.put(c.STGQC_Account_External_ID__c, dupsByEmail);
            }
            
            if (stageDupsByName.containsKey(c.STGQC_Account_External_ID__c)) {
                dupsByName = stageDupsByName.get(c.STGQC_Account_External_ID__c);
            } else {
                dupsByName = new Map<String, String>();
                stageDupsByName.put(c.STGQC_Account_External_ID__c, dupsByName);
            }
            
            if (!String.isEmpty(emailKey)) {
                if (dupsByEmail.containsKey(emailKey)) {
                    c.STGQC_DM_Error__c = (c.STGQC_DM_Error__c == null ? '' : c.STGQC_DM_Error__c) + 'Duplicated Contact by Email. Merge.\n';
                    c.STGQC_Merge_To__c = dupsByEmail.get(emailKey);
                    continue;
                } else {
                    dupsByEmail.put(emailKey, c.STGQC_CustomerContactId__c);
                }
            }
            
            if (!String.isEmpty(nameKey)) {
                if (dupsByName.containsKey(nameKey)) {
                    c.STGQC_DM_Error__c = (c.STGQC_DM_Error__c == null ? '' : c.STGQC_DM_Error__c) + 'Duplicated Contact by Name. Merge.\n';
                    c.STGQC_Merge_To__c = dupsByName.get(nameKey);
                    continue;
                } else {
                    dupsByName.put(nameKey, c.STGQC_CustomerContactId__c);
                }
            }
            
            String email = c.STGQC_EmailAddress__c;
            if(!String.isEmpty(emailKey) && !Pattern.matches('^[^.<][\\./\\-_a-zA-Z0-9]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)+$', c.STGQC_EmailAddress__c)) {
                email = '';
            }
            
            Contact con = new Contact();
            con.AccountId = a.ID;
            con.LastName = c.STGQC_SurName__c;
            con.Phone_Number_at_Account__c = c.STGQC_PhoneNumber__c; 
            con.Fax = c.STGQC_FaxNumber__c;
            con.Title = c.STGQC_ContactTitle__c;
            con.FirstName = c.STGQC_GivenName__c;
            con.RecordTypeId = conRT.getRecordTypeId();
            con.Email = email;
            con.Source__c = 'US_CPQ_DM';
            con.Contact_Creation_Status__c = 'Contact Creation Approved';
            con.OwnerID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
            //con.CreatedByID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
            //con.LastModifiedByID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
            
            conToInsert.add(con);
            conOneToOne.add(c.STGQC_CustomerContactId__c);
        }
        
        if (conToInsert.size() > 0) {
            Database.SaveResult[] results = database.insert(conToInsert, false);
            
            for (Integer i = 0; i < results.size(); i ++) {
                Database.SaveResult result = results[i];
                CPQ_STG_Quote_Contact__c stage = stages.get(conOneToOne[i]);
                
                if (!result.isSuccess()) {
                    String error = '';
                    for(Database.Error err : result.getErrors())
                        error = error + (err.getStatusCode() + ' - ' + err.getMessage()) + '\r\n';
                    stage.STGQC_DM_Error__c = (stage.STGQC_DM_Error__c == null ? '' : stage.STGQC_DM_Error__c) + 'Error in creating Contact: ' + error + '\r\n';
                } else {
                    //update the stage record with the generated contact ID 
                    stage.STGQC_Contact__c = result.getId();
                }
            }
        }
        
        for (CPQ_STG_Quote_Contact__c c : stgContacts) {
            if (c.STGQC_Merge_To__c != null) {
                CPQ_STG_Quote_Contact__c stage = stages.get(c.STGQC_Merge_To__c);
                c.STGQC_Contact__c = stage.STGQC_Contact__c;
            }
        }
        
        if (stgContacts.size() > 0)
            database.update(stgContacts, false);
        
        //link contacts to proposals
        List<Apttus_Proposal__Proposal__c> proposalsToUpdate = new List<Apttus_Proposal__Proposal__c>();
        for (CPQ_STG_Proposal__c c : scope) {
            CPQ_STG_Quote_Contact__c stage = stages.get(c.STGPS_CustomerContactId__c);
            if (stage != null && stage.STGQC_Contact__c != null)
                proposalsToUpdate.add(new Apttus_Proposal__Proposal__c (Id = c.STGPS_Proposal__c, Apttus_Proposal__Primary_Contact__c = stage.STGQC_Contact__c, Primary_Contact2__c = stage.STGQC_Contact__c));
            else
                c.STGPS_DM_Error__c = (c.STGPS_DM_Error__c == null ? '' : c.STGPS_DM_Error__c) + 'Contact does not exist.\n';
        }
        
        if (proposalsToUpdate.size() > 0)
            database.update(proposalsToUpdate, false);
        
        if (scope.size() > 0)
            database.update(scope, false);
    }
  
    global void finish(Database.BatchableContext BC) {}
*/
}