@isTest
public class cpqCycleTime_Task_uTest
{
	// This will give us access to the CONSTANTS
	private static cpqCycleTime_Task_u ct = new cpqCycleTime_Task_u();
	
    @isTest
    static void testCycleTimes()
    {
    	cpqCycleTime_uTest.setupCycleTimeData();
    	
        Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c Limit 1];
        Test.startTest();
            
            // Move to cpqCycleTime_Task_uTest
            Task t1 = new Task(WhatId = agreement.Id, Subject = ct.NON_STANDARD_AGREEMENT_SUBJECT, Status = ct.IN_PROGRESS);
            Task t2 = new Task(WhatId = agreement.Id, Subject = ct.SENT_FOR_REVIEW_SUBJECT, Status = ct.IN_PROGRESS);
            Task t3 = new Task(WhatId = agreement.Id, Subject = 'Test 1', Status = ct.IN_PROGRESS, OwnerId = cpqCycleTime_u.UserQueueName2Id.get(ct.EQUIP_ADMIN_QUEUE));
            Task t4 = new Task(WhatId = agreement.Id, Subject = 'Test 2', Status = ct.IN_PROGRESS, OwnerId = cpqCycleTime_u.UserQueueName2Id.get(ct.CUSTOMER_SERVICE_QUEUE));
            Task t5 = new Task(WhatId = agreement.Id, Subject = 'Test 3', Status = ct.IN_PROGRESS, OwnerId = cpqCycleTime_u.UserQueueName2Id.get(ct.PRICING_QUEUE));
            Task t6 = new Task(WhatId = agreement.Id, Subject = 'Test 4', Status = ct.IN_PROGRESS, OwnerId = cpqCycleTime_u.UserQueueName2Id.get(ct.END_CUSTOMER_REBATES_QUEUE));
            insert t1;
            insert t2;
            insert t3;
            insert t4;
            insert t5;
            insert t6;

            t1.Status = ct.COMPLETED;
            t2.Status = ct.COMPLETED;
            t3.Status = ct.COMPLETED;
            t4.Status = ct.COMPLETED;
            t5.Status = ct.COMPLETED;
            t6.Status = ct.COMPLETED;

            update t1;
            update t2;
            update t3;
            update t4;
            update t5;
            update t6;
        Test.stopTest();
    }
  
  	@isTest
  	static void testCapture()
  	{
  		
  		List<sObject> newList = new List<sObject>();
         Map<Id, sObject> newMap = new Map<Id, sObject>();
         List<sObject> oldList = null;
        Map<Id, sObject> oldMap = null;
       integer size = 10;
      
  		cpqCycleTime_Task_u unit = new cpqCycleTime_Task_u();
  		boolean isExecuting = true;
        boolean isInsert = false;
        boolean isUpdate = true;
        boolean isDelete = false;
        boolean isBefore = false;
        boolean isAfter = true;
        boolean isUndelete = false;
      
  		unit.capture(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size
        );
  	}
}