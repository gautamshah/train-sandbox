/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20170123        IL         AV-280     Created. To increase coverage on dependencies blocking AV-280.
20170126	A	PAB			AV-268		Same logic needed for all of the child classes so it was moved to here
*/
public abstract class SOQL
{
    protected Id jobId;
    protected List<sObject> sObjs;
    protected boolean isQueuable { get { return system.isQueueable() || jobId != null; } }
    
    protected integer queueThreshold = 100;
    protected integer batchSize = 1000;

	////SOQL_delete.ERR_NONE_FOUND
	
	protected SOQL() { }
	
	public SOQL(List<sObject> sObjs)
	{
		this.sObjs = sObjs;
	}
	
    public static Set<string> fieldsToIgnore = new Set<string>
    {
        'createdbyid',
        'createddate',
        'currencyisocode',
        'isdeleted',
        'lastmodifiedbyid',
        'lastmodifieddate',
        'lastreferenceddate',
        'lastvieweddate',
        'ownerid',
        'systemmodstamp',
        'userlicenseid',
        'mediumphotourl',
        'smallbannerphotourl',
        'mediumbannerphotourl' 
    };

    public void execute(QueueableContext context)
    {
        this.execute();
    }
    
    public List<string> execute()
    {
        if (this.sobjs != null)
            return processList();
        else
            return null;    
    }
    
    protected List<string> processList()
    {
        List<string> errors = new List<string>();
        
        if (this.sobjs.isEmpty())
            errors.add('None found');
        else
        {
            integer total = this.sobjs.size();

            errors.add('total ' + total);
            
            if (!this.isQueuable && total > queueThreshold)
            {
                jobID = System.enqueueJob(this);
                errors.add('JobId ' + jobId);
            }
            else
            {
                List<sObject> sources = createSubSet(this.sobjs);
                
                errors.add('number to be processed ' + sources.size());
                
                integer previousCount = 0;
                SOQL_Result[] results;
                do
                {
                    results = executeDML(sources, false);
                    
                    for(integer ndx = results.size() - 1; ndx >= 0; ndx--)
                    {
                        SOQL_Result result = results[ndx];
                        if (result.isSuccess())
                            sources.remove(ndx);
                    }
                    
                    if (sources.size() == previousCount) break;
                    
                    previousCount = sources.size();
                    
                } while(!sources.isEmpty());
    
                errors.add('not processed ' + sources.size());
            
                for(SOQL_Result result : results)
                    for(Database.Error err : result.getErrors())
                        errors.add(string.valueOf(this) + result + '  *****  ' + err);
            }
        }
        
        system.debug(string.valueOf(this) + errors);
        
        return errors;
    }
	
    protected virtual List<SOQL_Result> executeDML(List<sObject> sObjects, boolean allOrNone)
    {
    	return null;
    }
    
    protected virtual integer numberOfRecords()
    {
    	return batchSize;
    }
    
    protected List<sObject> createSubSet(List<sObject> sobjs)
    {
        List<sObject> sources = new List<sObject>();
        
        integer ndx = 1;
        for(sObject sobj : this.sobjs)
        {
            sources.add(sobj);
            if (ndx++ >= numberOfRecords())
                break;
        }
        
        return sources;
    }
    
    public AsyncApexJob status()
    {
    	return (this.jobId == null) ?
    		null :
    		[SELECT Status,
    		        ExtendedStatus,
    		        CompletedDate,
    		        NumberOfErrors
    		 FROM AsyncApexJob
    		 WHERE Id = :this.jobId];		        
    }
}