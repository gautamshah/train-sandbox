/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessBuilderController {
    @RemoteAction
    global static void deleteAction(String actionID) {

    }
    @RemoteAction
    global static void deleteStatus(String statusID) {

    }
    @RemoteAction
    global static List<vlc_pro.ProcessBuilderController.FieldValue> getStatusFieldsWithLabel(String statusId) {
        return null;
    }
    @Deprecated
    @RemoteAction
    global static List<vlc_pro__Field_Value__c> getStatusFields(String statusId) {
        return null;
    }
    @RemoteAction
    global static String saveAction(String actionJSON) {
        return null;
    }
    @RemoteAction
    global static String saveStatus(String statusJSON) {
        return null;
    }
global class FieldValue {
    global FieldValue() {

    }
}
}
