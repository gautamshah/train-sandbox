global class boxBuilderRemoteActionController {

    public boxBuilderRemoteActionController(ngForceController controller) {

    }
    
    @RemoteAction
    global static void sendMail(String emailSubject, String emailBody, String plainBody, String report, String[] recipients, String opportunityId, String boxName) {
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setFileName('report.csv');
        Blob bodyBlob=Blob.valueOf(report);
        attachment.setBody(bodyBlob);
        attachment.setContentType('text/csv');
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment } );
        message.setSubject(emailSubject);
        message.setHtmlBody(emailBody);
        message.setToAddresses(recipients);
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );
        
        Note n = new Note();
        n.parentId = opportunityId;
        n.title = boxName;
        n.body = plainBody;

        Attachment attach = new Attachment();    
        attach.Name = boxName;
        attach.body = bodyBlob;
        attach.parentId = opportunityId;

        Database.SaveResult sr = Database.insert(n, false);
        Database.SaveResult srAttach = Database.insert(attach, false);
    }
}