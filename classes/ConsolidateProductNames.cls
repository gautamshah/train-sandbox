/****************************************************************************************
* Name    : Class: ConsolidateProductNames
* Author  : Gautam Shah
* Date    : 8/4/2014
* Purpose : Populate the custom fields Product_Name_List__c and Product_Family_Name__c
*           with the list of products associated with an Opportunity.  This is being
*           used by Eloqua for a marketing campaign.
* 
* Dependancies: 
* Called by: OpportunityTriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 8/5/2014      Paul Berglund           Set the two fields to blank because we are
*                                       concatenating data to them
*****************************************************************************************/
public with sharing class ConsolidateProductNames {

    public static void Main(List<Opportunity> opps)
    {
        OpportunityTriggerDispatcher.executedMethods.add('ConsolidateProductNames.Main');
        
        List<OpportunityLineItem> olis = new List<OpportunityLineItem>([SELECT Id, PricebookEntryId, OpportunityId, PricebookEntry.Product2Id, PricebookEntry.Product2.Name, PricebookEntry.Product2.Family FROM OpportunityLineItem WHERE OpportunityId In :opps]);
        Map<Id, Set<String>> productNames = new Map<Id, Set<String>>();
        Map<Id, Set<String>> familyNames = new Map<Id, Set<String>>();
        
        for(OpportunityLineItem oli : olis)
        {
            Set<String> oliNames = null;
            Set<String> oliFamilies = null;
            //create list of distinct product names
            oliNames = productNames.get(oli.OpportunityId);
            if(oliNames == null)
            {
                oliNames = new Set<String>();
            }
            if(oli.PricebookEntry.Product2.Name != null)
            {
                oliNames.add(oli.PricebookEntry.Product2.Name);
            }
            productNames.put(oli.OpportunityId, oliNames);
            //create list of distinct family names
            oliFamilies = familyNames.get(oli.OpportunityId);
            if(oliFamilies == null)
            {
                oliFamilies = new Set<String>();
            }
            if(oli.PricebookEntry.Product2.Family != null)
            {
                oliFamilies.add(oli.PricebookEntry.Product2.Family);
            }
            familyNames.put(oli.OpportunityId, oliFamilies);
        }
        
        for(Opportunity opp : opps)
        {
            opp.Product_Name_List__c = '';
            opp.Product_Family_List__c = '';
        
            Set<String> prodNamesByOpp = productNames.get(opp.Id);
            Set<String> prodFamiliesByOpp = familyNames.get(opp.Id);
            if(prodNamesByOpp != null && prodNamesByOpp.size() > 0)
            {
                for(String n : prodNamesByOpp)
                {
                    opp.Product_Name_List__c += n + ', ';
                }
            }
            if(prodFamiliesByOpp != null && prodFamiliesByOpp.size() > 0)
            {
                for(String f : prodFamiliesByOpp)
                {
                    opp.Product_Family_List__c += f + ', ';
                }
            }
            opp.Product_Name_List__c = opp.Product_Name_List__c.RemoveEnd(', ');
            opp.Product_Family_List__c = opp.Product_Family_List__c.RemoveEnd(', ');
            
            System.Debug(opp.Product_Name_List__c);
            System.Debug(opp.Product_Family_List__c);
        }
    }
}