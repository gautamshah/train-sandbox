@isTest
private class Opportunity_TriggerHandler_Test 
{
    private static List<Sales_Navigation_Stage_Template__c> GetTemplates()
    {
        List<Sales_Navigation_Stage_Template__c> temps = new List<Sales_Navigation_Stage_Template__c>();
        for(Integer i = 0; i < 5; i++)
        {
            Sales_Navigation_Stage_Template__c s = new Sales_Navigation_Stage_Template__c(
                Name = 'Temp Name' + i,
                Record_Type__c = 'Develop',
                Critical_Steps__c = 4,
                Opportunity_Record_Type__c = 'US_AST_Opportunity'
            );
            temps.Add(s);
        }
        insert temps;
        return temps;
    }
    
    private static Opportunity GetOpportunityTestRecord(string recordTypeName)
    {
        Opportunity testOp = new Opportunity(
            Name = 'Test Opp',
            RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Opportunity').Get(recordTypeName).Id,
            Type = 'New Customer - Conversion',
            Capital_Disposable__c = 'Disposable',
            StageName = 'Identify',
            Opportunity_Type__c = 'Multi Physician Sale',
            CloseDate = Date.today()
        );
        insert testOp;
        return testOp;
    }
    
    static testMethod void Test_InsertSalesNavStages_ShouldInsert() 
    {
        Test.startTest();
        GetTemplates();
        GetOpportunityTestRecord('US_AST_Opportunity');
        Test.stopTest();
        Integer count = [select COUNT() from Sales_Navigator_Stage__c];
        System.assert(count != 0, 'Should have inserted Sales_Navigator_Stage__c: ' + count);
    }
    
    static testMethod void Test_InsertSalesNavStages_ShouldNotInsert() 
    {
        Test.startTest();
        GetTemplates();
        GetOpportunityTestRecord('Japan_Base_Opportunity');
        Test.stopTest();
        Integer count = [select COUNT() from Sales_Navigator_Stage__c];
        System.assert(count == 0, 'Should not have inserted Sales_Navigator_Stage__c: ' + count);
    }
    
    static testMethod void Test_AfterInsert_ShoudInsert()
    {
        Test.startTest();
        GetTemplates();
        Opportunity o = GetOpportunityTestRecord('US_AST_Opportunity');
        List<Sales_Navigator_Stage__c> sns = [SELECT Id FROM Sales_Navigator_Stage__c WHERE Opportunity__c =: o.Id];
        delete sns;
        Integer count = [select COUNT() from Sales_Navigator_Stage__c];
        System.assert(count != 0, 'Sales_Navigator_Stage__c shoudl be 0: ' + count);
        o.Name = 'Testier Opp';
        update o;
        count = [select COUNT() from Sales_Navigator_Stage__c];
        System.assert(count != 0, 'Should not have inserted Sales_Navigator_Stage__c: ' + count);
        Test.stopTest();
    }
}