@isTest(seeAllData=true)

// I cannot create a UserTerritory Record so I have to query it. Not ideal

private class ControllerX_AccountSalesReps_TestClass {



    private class AccountSalesReps 
    {
        string userguid;
        string name;
        string therapy;
        string bu;
        string title;
        string email;
        string workphone;
        string cellphone;
        string photo;
        string mgrguid;
        string mgrname;
    }   
    

    
 static testMethod void TestAccountRepPage() {    

 // Get 1 User that has a Territory Assignment
 List<UserTerritory> ut = [Select UserId,  TerritoryId From UserTerritory where isActive = True LIMIT 1];  

 // Retrieve the Group ID associated with the users Territory
 ID gr = [Select ID From Group where RelatedId = :ut.get(0).TerritoryId LIMIT 1].ID;  
 
 // Create an Account
 Account act = New Account();
 act.Name = 'MyTestAccount';

 insert act;

 // Create an AccountShare to Assign the above account to the users territory group 
 // RowCause is not writeable but will automatically be set to Territory Manual
 
 AccountShare acts = new AccountShare();
 acts.AccountId = act.id; 
 acts.UserOrGroupId = gr;

 insert acts;
     
  // Create a single Rep To Therapy Record for the First User
  Rep_To_Therapy__c rt = new Rep_To_Therapy__c();
  rt.User_ID__c = ut.get(0).UserId; 
  rt.Therapy__c = 'TestTherapy';
     
  insert rt;

  List<Apexpages.Message> pageMessages = ApexPages.getMessages();
  PageReference pageRef = Page.AccountSalesReps;
  Test.setCurrentPage(pageRef);     

  // Instantiate the standard controller
  Apexpages.StandardController sc = new Apexpages.standardController(act);

  // Instantiate the extension
  ControllerX_AccountSalesReps ext = new ControllerX_AccountSalesReps(sc);
     
  //Execute Methods
  ext.ResetPage();
  ext.getAccountTeams();
  ext.getMyTherapy();
       
   }     
 }