@isTest(SeeAllData=false)
Private class UserDeactivation_TEST 
{
    static testmethod void VerifyDeactive()
    {
    ID ProfileID = [SELECT Id from Profile where UserLicense.Name = 'Salesforce' Limit 1].id;
    String LicenseType = 'Salesforce';
    List<User> TotalDeactivate = new List<User>();
    Set<id> ManagerIds = new Set<id>();
    List<User> TestUsers = new List<User>();
    String CompanyName = 'UserDeactivationTestFactory';
        
        
    //Manager 1
    User M1 = new user(alias='mgr1z', LastName='Manager1', Username='mymgrtest2342345219@medtronic.com', Email='mymgrtest2342345219@medtronic.com',
    LocaleSidKey='en_US', TimeZoneSidKey= 'America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', ProfileId= ProfileID,
    Country='US', CompanyName= CompanyName, Sales_Org_PL__c='MCS', Do_Not_Deactivate__c = FALSE, Auto_Deactivate_Date__c = NULL, 
    IsActive = TRUE, ManagerID = NULL, User_Manager__c = NULL, User_Role__c='Field Manager'); 
                                    
    Insert M1;   
        
    //Manager 2
    User M2 = new user(alias='mgr2z', LastName='Manager2', Username='mymgrtest22342323423@medtronic.com', Email='mymgrtest22342323423@medtronic.com',
    LocaleSidKey='en_US', TimeZoneSidKey= 'America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', ProfileId= ProfileID,
    Country='US', CompanyName= CompanyName, Sales_Org_PL__c='MCS', Do_Not_Deactivate__c = FALSE, Auto_Deactivate_Date__c = NULL,
    IsActive = TRUE, ManagerID = NULL, User_Manager__c = NULL, User_Role__c='Field Manager'); 
                                           
    Insert M2;
        
    //User 1 - Reports to Manager 1 using Standard Manager Field
    User U2 = new user(alias='emp1z', LastName='User1', Username='myemployeetest1555555@medtronic.com', Email='myemployeetest1555555@medtronic.com',
    LocaleSidKey='en_US', TimeZoneSidKey= 'America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', ProfileId= ProfileID,
    ManagerID = M1.ID, User_Manager__c = NULL, Country='US', CompanyName= CompanyName, Sales_Org_PL__c='MCS', Do_Not_Deactivate__c = FALSE,
    Auto_Deactivate_Date__c = NULL, IsActive = TRUE, User_Role__c='Field Manager'); 
                                    
    TestUsers.add(U2); 
        
    //User 2 - Reports to Manager 2 using Custom Manager Field
    User U5 = new user(alias='emp1z', LastName='User2', Username='myemployeetest11111111@medtronic.com', Email='myemployeetest11111111@medtronic.com',
    LocaleSidKey='en_US', TimeZoneSidKey= 'America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', ProfileId= ProfileID,
    ManagerID = NULL, User_Manager__c = M2.ID, Country='US', CompanyName= CompanyName, Sales_Org_PL__c='MCS', Do_Not_Deactivate__c = FALSE,
    Auto_Deactivate_Date__c = NULL, IsActive = TRUE, User_Role__c='Field Manager'); 
                                    
    TestUsers.add(U5);         
        
    //User 3 - Flagged as Do_Not_Deactivate__c = TRUE
    User U3 = new user(
    alias='emp2z', LastName='User3', Username='myemployeetest22222222@medtronic.com', Email='myemployeetest22222222@medtronic.com',
    LocaleSidKey='en_US', TimeZoneSidKey= 'America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', ProfileId= ProfileID,
    Country='US', CompanyName='UserDeactivationTestFactory', Sales_Org_PL__c='MCS', Do_Not_Deactivate__c = TRUE, Auto_Deactivate_Date__c = NULL,
    IsActive = TRUE, ManagerID = NULL, User_Manager__c = NULL, User_Role__c='Field Manager'); 
                                    
    TestUsers.add(U3); 

    //User 4 - InActive User
    User U4 = new user(alias='emp3z', LastName='User4', Username='myemployeetest33333333@medtronic.com', Email='myemployeetest33333333@medtronic.com',
    LocaleSidKey='en_US', TimeZoneSidKey= 'America/Los_Angeles', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', ProfileId= ProfileID,
    Country='US', CompanyName= CompanyName, Sales_Org_PL__c='MCS', Do_Not_Deactivate__c = TRUE, Auto_Deactivate_Date__c = NULL, IsActive = FALSE,
    ManagerID = NULL, User_Manager__c = NULL, User_Role__c='Field Manager'); 
                                    
    TestUsers.add(U4);  
    Insert TestUsers;

        UserDeactivation ud = new UserDeactivation();
        ud.runDeactivation();
        
    //ASSERTS    
        
    // Verify that a user flagged as Do Not Deactivate does not get deactivated
    User CheckDeact1 = [SELECT Auto_Deactivate_Date__c FROM User WHERE Username = 'myemployeetest22222222@medtronic.com']; 
    system.debug('The first user is: ' + CheckDeact1.Auto_Deactivate_Date__c);
    System.assertEquals(true, CheckDeact1.Auto_Deactivate_Date__c == null);   
        
   // Verify that inactive users are not picked up
    User CheckDeact2 = [SELECT Auto_Deactivate_Date__c FROM User WHERE Username = 'myemployeetest33333333@medtronic.com']; 
    system.debug('The second user is: ' + CheckDeact2.Auto_Deactivate_Date__c);
    System.assertEquals(true, CheckDeact2.Auto_Deactivate_Date__c == null);       
    
    // Verify that a user with no login activity will be deactivated
    User CheckDeact3 = [SELECT Auto_Deactivate_Date__c FROM User WHERE Username = 'myemployeetest1555555@medtronic.com'];   
    system.debug('The third user is: ' + CheckDeact3.Auto_Deactivate_Date__c);
    System.assertEquals(true, CheckDeact3.Auto_Deactivate_Date__c != null);  
    
   
    }
    
}