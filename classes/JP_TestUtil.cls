public class JP_TestUtil{
     /****************************************************************************************
     * Name    : JP_TestUtil 
     * Author  : Hiroko Kambayashi
     * Date    : 09/21/2012 
     * Purpose : Test Data for Japan Development's test methods
     *           
     * Dependencies: RecordType Object
     *             , Account Object
     *             , Opportunity Object
     *             , Pricebook2 Object
     *             , Product2 Object
     *             , PricebookEntry Object
     *             , OpportunityLineItem Object
     *             , JP_Target_Product_Category__c Object
     *             , Account_Extended_Profile__c Object
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 02/25/2013  Hiroko Kambayashi    Delete a needless method and modify createOpportunity methods.
     * 12/10/2015  Syu    Iken          Add the method "createTestProductQuantity" 
     * 12/10/2015  Syu    Iken          Add the method "createTestProductQuantityRevenue" 
     *****************************************************************************************/
     
    /*
     * Get RecordType id for test
     * @param  devName        RecordType DeveloperName
     * @return exsitRecType   Acquired Record type
     */
	public static RecordType selectRecordType(String devName){
		
		RecordType exsitRecType = [SELECT Id FROM RecordType WHERE DeveloperName = :devName];
		
		return exsitRecType;
	}
	
    /*
     * Create Account for test
     * @param  recTypeId      RecordType id
     * @param  accName        Account Name
     * @return testacc        Account for test
     */
	public static Account createTestAccount(Id recTypeId, String accName){
		
		List<Account> exsitAccounts = [SELECT Id FROM Account WHERE Name = :accName limit 1];
		
		if(!exsitAccounts.isEmpty()){
			return exsitAccounts[0];
		}
		
		Account testacc = new Account(Name = accName, RecordTypeId = recTypeId);
		insert testacc;
		return testacc;
	}
    /*
     * Create Contact for test
     * @param  recTypeId      RecordType id
     * @param  accId          Related AccountId
     * @param  conLastName    Contact Last name
     * @return testcon        Contact for test
     */
	public static Contact createTestContact(Id recTypeId, Id accId, String conLastName){
		
		Contact testcon = new Contact(RecordTypeId = recTypeId, AccountId = accId, LastName = conLastName);
		insert testcon;
		return testcon;
	}
    /*
     * Create Intimacy for test
     * @param  conId                   Related Contact id
     * @param  selectedIntimacy        Piclist value of the Intimacy
     * @param  staffId                 SR(user) id
     * @return testintimacy            Intimacy for test
     */
	public static JP_Intimacy__c createTestIntimacy(Id conId, String selectedIntimacy, Id srId){
		
		JP_Intimacy__c testintimacy = new JP_Intimacy__c(JP_Contacts__c = conId, JP_Intimacy__c = selectedIntimacy, JP_SR__c = srId);
		insert testintimacy;
		return testintimacy;
	}
	
    /*
     * Create Opportunity for test
     * @param  recTypeId      RecordType id
     * @param  accId          Related Account id
     * @param  oppName        Opportunity name
     * @param  stName         StageName
     * @param  capDispo       Capital_Disposable__c
     * @param  closeYear      Year of CloseDate(yyyy)
     * @param  closeMonth     Month of CloseDate(MM)
     * @param  closeDay       Day of CloseDate(dd)
     * @param  tgtId          Target Product Category id
     * @return testopp        Opportunity for test
     */
	public static Opportunity createTestOpportunity(Id recTypeId, Id accId, String oppName, 
													String stName, String capDispo, 
													Integer closeYear, Integer closeMonth, Integer closeDay, 
													Id tgtId){
		
		Opportunity testopp = new Opportunity(RecordTypeId = recTypeId, 
												AccountId = accId, 
												Name = oppName, 
												StageName = stName, 
												Capital_Disposable__c = capDispo, 
												CloseDate = Date.newInstance(closeYear, closeMonth, closeDay), 
												JP_Target_Product_Category__c = tgtId, 
												CurrencyIsoCode = 'JPY');
		insert testopp;
		return testopp;
	}
    /*
     * Create Opportunity for test(Intimacy input by Key Dr)
     * @param  recTypeId      RecordType id
     * @param  accId          Related Account id
     * @param  oppName        Opportunity name
     * @param  stName         StageName
     * @param  capDispo       Capital_Disposable__c
     * @param  closeYear      Year of CloseDate(yyyy)
     * @param  closeMonth     Month of CloseDate(MM)
     * @param  closeDay       Day of CloseDate(dd)
     * @param  doctorId       JP_Key_Dr__c(Contact) Id
     * @return testopp        Opportunity for test
     */
	public static Opportunity createIntimacyTestOpportunity(Id recTypeId, Id accId, String oppName,
															String stName, String capDispo, 
															Integer closeYear, Integer closeMonth, Integer closeDay, 
															Id doctorId){
		Opportunity testopp = new Opportunity(RecordTypeId = recTypeId, 
												AccountId = accId, 
												Name = oppName, 
												Stagename = stName, 
												Capital_Disposable__c = capDispo, 
												CloseDate = Date.newInstance(closeYear, closeMonth, closeDay), 
												JP_Key_Dr__c = doctorId, 
												CurrencyIsoCode = 'JPY');
		insert testopp;
		return testopp;
	}
    /*
     * Get standard Pricebook2 for test
     * @param  none
     * @return teststd        Pricebook2(standard) for test
     */
	public static Pricebook2 selectStandardPricebook(){
		Pricebook2 teststd = [SELECT Id, IsActive FROM Pricebook2 WHERE IsStandard = true AND IsActive = true limit 1];
		return teststd;
	}
    /*
     * Create custom Pricebook2 for test
     * @param  pbName         Pricebook2 Name
     * @return teststd        Pricebook2 for test
     */
	public static Pricebook2 createCustomPricebook(String pbName){
		
		Pricebook2 testbook = new Pricebook2(Name = pbName, IsActive = true);
		insert testbook;
		return testbook;
	}
    /*
     * Create Product2 for test
     * @param  pdName         Product2 Name
     * @param  canScd         CanUseRevenueSchedule
     * @param  tgtId          Target Product Category Id
     * @param  bunit          JP_Business_unit__c
     * @return testprd        Product2 for test
     */
	public static Product2 createTestProduct(String pdName, Boolean canScd, Id tgtId, String bunit){
		
		Product2 testprd = new Product2(Name = pdName, 
										CurrencyIsoCode = 'JPY', 
										CanUseRevenueSchedule = canScd, 
										JP_PRProductCTG__c = tgtId, 
										JP_Business_Unit__c = bunit);
		insert testprd;
		return testprd;
	}
    
    /*
     * Create Product2 for test
     * @param  pdName         Product2 Name
     * @param  canScd         CanUseQuantitySchedule
     * @param  tgtId          Target Product Category Id
     * @param  bunit          JP_Business_unit__c
     * @return testprd        Product2 for test
     */
	public static Product2 createTestProductQuantity(String pdName, Boolean canScd, Id tgtId, String bunit){
		
		Product2 testprd = new Product2(Name = pdName, 
										CurrencyIsoCode = 'JPY', 
										CanUseQuantitySchedule = canScd, 
										JP_PRProductCTG__c = tgtId, 
										JP_Business_Unit__c = bunit);
		insert testprd;
		return testprd;
	}
    
    /*
     * Create Product2 for test
     * @param  pdName         Product2 Name
     * @param  canRScd         CanUseRevenueSchedule
     * @param  canQScd         CanUseQuantitySchedule
     * @param  tgtId          Target Product Category Id
     * @param  bunit          JP_Business_unit__c
     * @return testprd        Product2 for test
     */
	public static Product2 createTestProductQuantityRevenue(String pdName, Boolean canRScd, Boolean canQScd, Id tgtId, String bunit){
		
		Product2 testprd = new Product2(Name = pdName, 
										CurrencyIsoCode = 'JPY', 
										CanUseRevenueSchedule = canRScd,
                                        CanUseQuantitySchedule = canQScd,
										JP_PRProductCTG__c = tgtId, 
										JP_Business_Unit__c = bunit);
		insert testprd;
		return testprd;
	}
	

    /*
     * Create PricebookEntry for test
     * @param  pbookId        Pricebook2Id
     * @param  prdId          Product2Id
     * @param  useStd         UseStandardPrice
     * @return testentry      PricebookEntry for test
     */
	public static PricebookEntry createTestPricebookEntry(Id pbookId, Id prdId, Boolean useStd){
		
		PricebookEntry testentry = new PricebookEntry(Pricebook2Id = pbookId, Product2Id = prdId, UseStandardPrice = useStd, 
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'JPY');
		insert testentry;
		return testentry;
	}
    /*
     * Create OpportunityLineItem for test
     * @param  oppId          related Opportunity Id
     * @param  entryId        PricebookEntryId
     * @param  qu             Quantity
     * @param  uniPrice       Unit Price
     * @return testitem       OpportunityLineItem for test
     */
	public static OpportunityLineItem createTestOpportunityLineItem(Id oppId, Id entryId, Double qu, Decimal uniPrice){
		
		OpportunityLineItem testitem = new OpportunityLineItem(OpportunityId = oppId, PricebookEntryId = entryId, 
																Quantity = qu, UnitPrice = uniPrice);
		insert testitem;
		return testitem;
	}
    /*
     * Create Target Product Category for test
     * @param  tgtPdName      Target Product Category Name
     * @param  bunit          JP_Business_Unit__c
     * @return testprd        Target Product Category for test
     */
	public static JP_Target_Product_Category__c createTestTargetProductCategory(String tgtPdName, String bunit){
		
		JP_Target_Product_Category__c testtarget = new JP_Target_Product_Category__c(Name = tgtPdName, JP_Business_Unit__c = bunit);
		
		insert testtarget;
		return testtarget;
	}
    /*
     * Create Account Extended Profile for test
     * @param  bu             JP_Business_Unit__c
     * @param  accId          AccountId
     * @return testaccprof    Account_Extended_Profile__c for test
     */
	public static Account_Extended_Profile__c createTestAccountExtendedProfile(String bu, Id accId){
		
		Account_Extended_Profile__c testaccprof = new Account_Extended_Profile__c(JP_Business_Unit__c = bu, Account__c = accId);
		
		insert testaccprof;
		return testaccprof;
	}
}