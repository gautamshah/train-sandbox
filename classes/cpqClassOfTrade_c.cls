/**
Abstract parent for ClassOfTrade filter classes

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-12/06      Henry Noerdlinger   Created

===============================================================================
*/

public abstract class cpqClassOfTrade_c
{

	protected String value {get; set;}
    public boolean exclude { get; set; }

	public cpqClassOfTrade_c(String row)
	{
		exclude = row.substring(0,1).equals('-') ? true : false;
	  	value = row.replaceAll('[\\+\\-]','').toUpperCase();
	  	value = value.trim();
	}

  	public abstract String buildParticipatingFacilityExpression(String obj);
  	
    public abstract String buildDistributorExpression(String obj);

}