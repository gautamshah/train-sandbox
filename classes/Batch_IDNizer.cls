/****************************************************************************************
* Name    : Batch_IDNizer
* Author  : Gautam Shah
* Date    : 4/17/2014
* Purpose : Batch processing of the IDNizer class
* 
* Dependancies: 
* 	class: IDNizer
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* 	DATE         	AUTHOR                  CHANGE
* 	----          	------                  ------
*	
*****************************************************************************************/ 
public class Batch_IDNizer implements Database.Batchable<sObject>
{
	private String query;
	
	public Batch_IDNizer()
	{
		query = 'Select Id, Account_Sell_To__c, ERP_ID_Ship_To__c From Temp_IDN_Mapping__c';
	}
	public Database.Querylocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<Temp_IDN_Mapping__c> scope)
	{
		IDNizer iz = new IDNizer(scope);
	}
	public void finish(Database.BatchableContext BC){}
}