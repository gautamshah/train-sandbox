/****************************************************************************************
Name    : Class: TaskTriggerDispatcher
Author  : Paul Berglund
Date    : 07/23/2015
Purpose : Dispatches to necessary classes when invoked by Task trigger

Dependancies: 
   Class: Task_Main

========================
= MODIFICATION HISTORY =
========================
DATE       AUTHOR            CHANGE
----       ------            ------
10/15/2015 Bryan Fry         Added method to capture Cycle Times
10/21/2015 Paul Berglund     Added call to Task_Main.NotifyOwnerwhenCDdone
11/01/2015 Paul Berglund     Added myTriggerManager code
02/01/2016 Paul Berglund     Commented out call to CPQ_Cycles so we can promote
                             to production
02/11/2016 Paul Berglund     Added call to NotifyOwnerAgreementImplemented back
03/08/2016 Paul Berglund     Moved CPQ logic into TaskConfigSetting
03/18/2016 Paul Berglund     Uncommented Cycle Time call
05/02/2016 Paul Berglund     Refactored to meet Medtronic coding standards, redeploying
                             to fix issue reported by COE
                             Commented out Cycle Time so we could get this deployed
05/11/2016 Paul Berglund     Removed calls to myTriggerManager - don't have time to debug
                             Too many SOQL queries problem at the moment.
06/14/2016   Paul Berglund      Moved when to execute Cycle Time logic into class
*****************************************************************************************/
public with sharing class TaskTriggerDispatcher
{
	// To prevent a method from executing a second time,
	// add the name of the method to the 'executedMethods' set
	public static Set<String> executedMethods = new Set<String>();
  
  	public static void Main(
  		List<Task> newList,
  		Map<Id, Task> newMap,
  		List<Task> oldList,
  		Map<Id, Task> oldMap,
  		Boolean isBefore,
  		Boolean isAfter,
  		Boolean isInsert,
  		Boolean isUpdate,
  		Boolean isDelete,
  		Boolean isExecuting)
  	{
       	system.debug('TaskTriggerDispatcher.Main ' + Limits.getQueries());

      	// Since Task triggers run for everyone and at this time
      	// these methods are only for CPQ users, we see if CPQ properties
      	// exist before we call them
		if(TaskConfigSetting.CPQ != null)
		{
          	if (isAfter)
          	{
            	if (isInsert)
            	{
	            	if (!executedMethods.contains('NotifyCSAgreementActivated'))
    	            {
        	        	Task_Main.NotifyCSAgreementActivated(newMap);
            	  	}
            	}
            	else if (isUpdate)
            	{
	            	if (!executedMethods.contains('NotifyOwnerwhenCDdone'))
    	            {
        	        	Task_Main.NotifyOwnerwhenCDdone(newMap, oldMap);
            	  	}

            		if (!executedMethods.contains('NotifyOwnerAgreementImplemented'))
                	{
                		Task_Main.NotifyOwnerAgreementImplemented(newMap, oldMap);
              		}
            	}
          	}
          
          	if (isBefore && (isInsert || isUpdate))
          	{
              	if (!executedMethods.contains('ChangeChildsOwnerIdOnChangeofTaskOwner'))
				{
                  	//Task_Main.ChangeChildsOwnerIdOnChangeofTaskOwner(newList);
              	}
          	}
      	}
  	}
}