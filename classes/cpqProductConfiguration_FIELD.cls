public class cpqProductConfiguration_FIELD
{
 	public static final sObjectField
 		Proposal = Apttus_Config2__ProductConfiguration__c.fields.Apttus_QPConfig__Proposald__c,
		Duration = Apttus_Config2__ProductConfiguration__c.fields.Duration__c,
		BuyoutAsOfDate = Apttus_Config2__ProductConfiguration__c.fields.Buyout_as_of_Date__c,
		BuyoutAmount   = Apttus_Config2__ProductConfiguration__c.fields.Buyout_Amount__c,
		TBDAmount      = Apttus_Config2__ProductConfiguration__c.fields.TBD_Amount__c,
		
		SalesTaxPercent = Apttus_Config2__ProductConfiguration__c.fields.Sales_Tax_Percent__c,
		
		TaxGrossNetOfTradeIn = Apttus_Config2__ProductConfiguration__c.fields.Tax_Gross_Net_of_TradeIn__c,
		
		TotalAnnualSensorCommitmentAmount =
						Apttus_Config2__ProductConfiguration__c.fields.Total_Annual_Sensor_Commitment_Amount__c
	;
}