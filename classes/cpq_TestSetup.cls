/** 

Utilities to produce test data and test data scenarios for cpq.

- setOrg = Controls configuration of sales org-specific data
- get[SObject] = Retrieves visible system data, like User, RecordType, Profile, Organization
- generate[SObject] = Returns configured sObject not inserted into database.
- insert[SObject] = Returns configured sObject and inserts into database.
- buildScenario_[ScenarioName] = Generates a preconfigured set of sObject data and inserts into database.
    Data is accessible in test[sObjectName] variables.

- Methods are listed by category in alphabetical order. New methods should be added in the same manner.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
10/19/2016      Isaac Lewis         Created.
10/31/2016      Isaac Lewis         Refactored for CPQDEV environment.
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
01/23/2017      Isaac Lewis         Updated getStandardPriceBookId() to use Test.getStandardPriceBookId()
02/08/2017      Isaac Lewis         Replaced logic in generatePriceLists() with cpqPriceList_u.create()
===============================================================================
*/

// IDEA: Create External Id on every object so we can insert 10 levels of dependencies deep in one sObject
// transaction
// https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/langCon_apex_dml_foreign_keys.htm

// IDEA: Print system limit footprint after every scenario
// public static void printConfigStatus () {}

/*
Test Data Factory Articles
https://trailhead.salesforce.com/en/apex_testing/apex_testing_data
https://developer.salesforce.com/forums/?id=906F000000090j3IAA
https://developer.salesforce.com/releases/release/Spring15/TestClasses
https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_utility_classes.htm
https://github.com/dhoechst/Salesforce-Test-Factory
http://jessealtman.com/2013/09/proper-unit-test-structure-in-apex/
*/
public virtual class cpq_TestSetup 
{
    // Test Data Cache
    public static OrganizationNames_g.Abbreviations org = OrganizationNames_g.Abbreviations.RMS; // Default to RMS
    public static CPQ_Config_Setting__c testCPQConfigSetting = null;
    public static List<CPQ_Variable__c> testCPQVariables = new List<CPQ_Variable__c>();
    public static List<Account> testAccounts = new List<Account>();
    public static List<Contact> testContacts = new List<Contact>();
    public static List<Opportunity> testOpportunities = new List<Opportunity>();
    public static List<Apttus_Config2__PriceList__c> testPriceLists = new List<Apttus_Config2__PriceList__c>();
    public static List<Product2> testProducts = new List<Product2>();
    public static Map<Id, Apttus_Config2__PriceListItem__c> testPriceListItems;
    public static List<Apttus_Proposal__Proposal__c> testProposals = new List<Apttus_Proposal__Proposal__c>();
    public static List<Apttus__APTS_Agreement__c> testAgreements = new List<Apttus__APTS_Agreement__c>();
    public static List<Apttus_Config2__ProductConfiguration__c> testProductConfigurations = new List<Apttus_Config2__ProductConfiguration__c>();
    public static List<CPQ_Rollup_Field__c> testRollups;
    public static List<Participating_Facility__c> testFacilities;
    public static Apttus_Config2__ClassificationName__c testCategory;
    public static List<Apttus_Config2__ClassificationHierarchy__c> testCategoryHierarchies;
    public static List<Apttus_Config2__ProductClassification__c> testClasses;
    public static List<Apttus_Config2__LineItem__c> testLines;
    public static List<Apttus_Config2__SummaryGroup__c> testSummaries;
    public static Opportunity testOpp;
    public static List<ERP_Account__c> testERPs;
    public static List<Apttus__APTS_Agreement__c> testOldAgreements;
    public static List<Participating_Facility__c> testProposalFacilities;
    public static List<Proposal_Distributor__c> testDistributors;
    public static Apttus_QPConfig__ProposalSummaryGroup__c testProposalSummaryGroup;
    public static List<Apttus_Proposal__Proposal_Line_Item__c> testPLIs;
    public static List<Apttus__AgreementLineItem__c> testAgLines;
    public static List<Agreement_Participating_Facility__c> testSourceAgreementFacilities;
    public static List<Agreement_Distributor__c> testSourceAgreementDistributors;
    public static Product2 testProduct;
    public static Account testGPO;
    public static Partner_Root_Contract__c testRoot;
    public static Apttus_Config2__AdHocGroup__c testTotaling;
    public static Apttus_Config2__LineItem__c testLine;
    public static Apttus_Config2__SummaryGroup__c testSummaryGroup;
    public static Apttus_Proposal__Proposal_Line_Item__c testProposalLine;
    public static Apttus_QPConfig__ProposalSummary__c testProposalSummary;
    public static Participating_Facility__c testFacility;
    public static Proposal_Distributor__c testDistributor;
    public static Rebate__c testRebate;
    
}