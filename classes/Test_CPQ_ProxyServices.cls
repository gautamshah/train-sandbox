/**
Test Class

CPQ_ProxyServices
CPQ_ProxyPropertyChangedEvent
CPQ_ProxyiDealServiceDataType
AsyncCPQ_ProxyServices

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-03-06      Yuli Fintescu		Created
===============================================================================
*/
@isTest
private class Test_CPQ_ProxyServices {

    static testMethod void myUnitTest() {
        CPQ_ProxyServices.GetTerritoryManagerTerritoryResponse_element e0 = new CPQ_ProxyServices.GetTerritoryManagerTerritoryResponse_element();
        CPQ_ProxyServices.GetSalesHistoryResponse_element e1 = new CPQ_ProxyServices.GetSalesHistoryResponse_element();
        CPQ_ProxyServices.MultiPriceResponse_element e2 = new CPQ_ProxyServices.MultiPriceResponse_element();
        CPQ_ProxyServices.GetDistributorClassOfTradesResponse_element e3 = new CPQ_ProxyServices.GetDistributorClassOfTradesResponse_element();
        CPQ_ProxyServices.ListPriceResponse_element e4 = new CPQ_ProxyServices.ListPriceResponse_element();
        CPQ_ProxyServices.ListPrice_element e5 = new CPQ_ProxyServices.ListPrice_element();
        CPQ_ProxyServices.MultiPrice_element e6 = new CPQ_ProxyServices.MultiPrice_element();
        CPQ_ProxyServices.GetTerritoryManagerTerritory_element e7 = new CPQ_ProxyServices.GetTerritoryManagerTerritory_element();
        CPQ_ProxyServices.GetDistributorClassOfTrades_element e9 = new CPQ_ProxyServices.GetDistributorClassOfTrades_element();
    	
    	CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();

        try {
        	service.MultiPrice(new CPQ_ProxyMultiPriceService.PricingRequestType());
        } catch (Exception e) {}
        try {
        	service.GetTerritoryManagerTerritory('111111');
        } catch (Exception e) {}
        try {
        	service.GetDistributorClassOfTrades();
        } catch (Exception e) {}
        try {
			service.ListPrice('COT','Item', System.Today(), 1, 'UOM');
        } catch (Exception e) {}
        
        CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler p = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
		CPQ_ProxyiDealServiceDataType.DictionaryEntry d = new CPQ_ProxyiDealServiceDataType.DictionaryEntry();
		CPQ_ProxyiDealServiceDataType.Affiliation d1 = new CPQ_ProxyiDealServiceDataType.Affiliation();
		CPQ_ProxyiDealServiceDataType.PartnerRebate d4 = new CPQ_ProxyiDealServiceDataType.PartnerRebate();
		CPQ_ProxyiDealServiceDataType.PartnerProgram d5 = new CPQ_ProxyiDealServiceDataType.PartnerProgram();
		
		try {
		AsyncCPQ_ProxyServices.MultiPriceResponse_elementFuture ee0 = new AsyncCPQ_ProxyServices.MultiPriceResponse_elementFuture();
		ee0.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.RecallDealResponse_elementFuture ee1 = new AsyncCPQ_ProxyServices.RecallDealResponse_elementFuture();
		ee1.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.ListPriceResponse_elementFuture ee2 = new AsyncCPQ_ProxyServices.ListPriceResponse_elementFuture();
		ee2.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.PartnerQueueResponse_elementFuture ee3 = new AsyncCPQ_ProxyServices.PartnerQueueResponse_elementFuture();
		ee3.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.GetDistributorClassOfTradesResponse_elementFuture ee4 = new AsyncCPQ_ProxyServices.GetDistributorClassOfTradesResponse_elementFuture();
		ee4.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.ValidationCheckResponse_elementFuture ee5 = new AsyncCPQ_ProxyServices.ValidationCheckResponse_elementFuture();
		ee5.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.ContractInfoResponse_elementFuture ee6 = new AsyncCPQ_ProxyServices.ContractInfoResponse_elementFuture();
		ee6.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.GetSalesHistoryResponse_elementFuture ee7 = new AsyncCPQ_ProxyServices.GetSalesHistoryResponse_elementFuture();
		ee7.getValue();
		} catch (Exception e) {}
		try {
		AsyncCPQ_ProxyServices.GetTerritoryManagerTerritoryResponse_elementFuture ee8 = new AsyncCPQ_ProxyServices.GetTerritoryManagerTerritoryResponse_elementFuture();
		ee8.getValue();
		} catch (Exception e) {}
		
		AsyncCPQ_ProxyServices.AsyncBasicHttpBinding_IProxy service2 = new AsyncCPQ_ProxyServices.AsyncBasicHttpBinding_IProxy();
		
		try {
        	service2.beginMultiPrice(null,new CPQ_ProxyMultiPriceService.PricingRequestType());
        } catch (Exception e) {}
        try {
        	service2.beginGetTerritoryManagerTerritory(null,'111111');
        } catch (Exception e) {}
        try {
        	service2.beginRecallDeal(null,null,null);
        } catch (Exception e) {}
        try {
        	service2.beginListPrice(null,'COT','Item', System.Today(), 1, 'UOM');
        } catch (Exception e) {}
        try {
        	service2.beginPartnerQueue(null,null, null);
        } catch (Exception e) {}
        try {
        	service2.beginGetDistributorClassOfTrades(null);
        } catch (Exception e) {}
        try {
        	service2.beginValidationCheck(null,null, null);
        } catch (Exception e) {}
        try {
        	service2.beginContractInfo(null,null);
        } catch (Exception e) {}
        try {
        	service2.beginGetSalesHistory(null,null,null,null,null,null,null,null);
        } catch (Exception e) {}
        
	}
}