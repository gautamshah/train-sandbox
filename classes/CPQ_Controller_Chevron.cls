/**
Controller for CPQ_ProposalChevron and CPQ_AgreementChevron page

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-07-31      Yuli Fintescu		Created.
===============================================================================
*/
public with sharing class CPQ_Controller_Chevron {
	//all chevron pieces
    private static List<Chevron> ChevronPool = new List<Chevron> {
        new Chevron ('Draft'),
        new Chevron ('Generated'),
        new Chevron ('Approved'),
        new Chevron ('Presented'),
        new Chevron ('Accepted'),
        new Chevron ('Agreement Created'),
        
        new Chevron ('Initiated'),
        new Chevron ('Contract Dev. Reviewed'),
        new Chevron ('Document Generated'),
        new Chevron ('Sent for Review or eSignature'),
        new Chevron ('Activated')
    };
    
    //define how chevron is displayed by stage or status
    private static Map<String, Map<String, String>> statusToElement = new Map<String, Map<String, String>> {
		'Apttus_Proposal__Proposal__c' => new Map<String, String> {
            'Draft'						=>   '0,selected,',//index in ChevronPool, style class, override default button face
            'Generated'					=>   '1,selected,',
            'Approved'					=>   '2,selected,',
            'In Review'					=>   '2,red,In Approval',
            'Presented'					=>   '3,selected,',
            'Accepted'					=>   '4,selected,',
            'Denied'					=>   '4,red,Denied',
            'Submitted to Contracting'	=>   '5,selected,'//show if certain criteria met
		},
		'Apttus__APTS_Agreement__c' => new Map<String, String> {
            'Request'					=>   '6,selected,',
            'Request Approval'			=>   '6,selected,',
            'Submitted Request'			=>   '6,selected,',
            'Approved Request'			=>   '6,selected,',
            'Cancelled Request'			=>   '6,selected,',
            'In Amendment'				=>   '6,selected,',
            'In Renewal'				=>   '6,selected,',
            'Expired'					=>   '6,selected,',
            'Terminated'				=>   '6,selected,',
            'Amended'					=>   '6,selected,',
            'Cancelled'					=>   '6,selected,',
            
            'Contract Dev. Reviewed'	=>   '7,selected,',//show if certain criteria met
            
            'Author Contract'			=>   '8,selected,',
            'Language Approval'			=>   '8,selected,',
            'Language Approved'			=>   '8,selected,',
            'Ready for Signatures'		=>   '8,selected,',
            
            'Internal Review'			=>   '9,selected,',
            'Internal Signatures'		=>   '9,selected,',
            'Signature Declined'		=>   '9,red,Signature Declined',
            'Other Party Signatures'	=>   '9,selected,',
            'Other Party Review'		=>   '9,selected,',
            'Fully Signed'				=>   '9,selected,',
			'In Reconciliation'			=>   '9,selected,',
			'Reconciled'				=>   '9,selected,',
			
            'Activated'					=>   '10,selected,'
		}
    };
	
    public CPQ_Controller_Chevron(ApexPages.StandardController controller) {
        DrawChevrons(controller.getRecord());
    }
    
	//Chevron list for displaying in VF page
    public List<Chevron> getChevrons() {
    	List<Chevron> elements = new List<Chevron>();
    	for (Chevron c : ChevronPool) {
    		if (c.State != 'hidden') {
    			elements.add(c);
    		}
    	}
    	
    	return elements;
    }
    
    //build chevron
    private static void DrawChevrons(SObject theRecord) {
		Map<String, String> elements;
		String recordType;
		String status;
		String defaultLand;
		String ProposalStatus;
		Boolean proposalCD = false;
		Boolean AgreementCD = false;
		
		//proposal or agreement?
		if (theRecord instanceof Apttus_Proposal__Proposal__c) {
			Apttus_Proposal__Proposal__c proposal = (Apttus_Proposal__Proposal__c)theRecord;
			
	        elements = statusToElement.get('Apttus_Proposal__Proposal__c');
	        status = proposal.Apttus_Proposal__Approval_Stage__c;
	        recordType = CPQ_Utilities.getCoreDealTypeName(proposal);
	        defaultLand = '0,selected,';
	        
	        //special stuff
	        ProposalStatus = proposal.Apttus_QPApprov__Approval_Status__c;
		} else if (theRecord instanceof Apttus__APTS_Agreement__c) {
			Apttus__APTS_Agreement__c agreement = (Apttus__APTS_Agreement__c)theRecord;
			
			elements = statusToElement.get('Apttus__APTS_Agreement__c');
			status = agreement.Apttus__Status__c;
			recordType = CPQ_Utilities.getCoreDealTypeName(agreement);
			defaultLand = '6,selected,';
			
			//special stuff
		    proposalCD = agreement.Apttus_QPComply__RelatedProposalId__r.Contract_Dev_Team_Review_Required__c;
		    AgreementCD = agreement.Contract_Dev_Team_Review_Required__c;
		}
		
		if (elements == null)
			return;
		
		//set state from 'hidden' to '', set which chevron pieces should displayed
		for (String k : elements.keySet()) {
			String v = elements.get(k);
        	String[] a = v.split(',');
			Chevron c = ChevronPool[Integer.valueOf(a[0])];
			
			/*except for some situations...*/
			if (!(k == 'Submitted to Contracting' && recordType == 'Current_Price_Quote') &&		//Proposal Chevron: Current_Price_Quote: hide create agreement step
				!(k == 'Contract Dev. Reviewed' && proposalCD == false)) {							//Agreement Chevron: "Contract Dev. Reviewed" is not a status. Non-standard requires contract dev review. 
																									//	Hide "Contract Dev. Reviewed" chevron if related proposal is not non-standard
				
				c.state = '';
			}
		}
		
	    String chevronAttr = elements.get(status);
        if (String.isEmpty(chevronAttr)) {
            chevronAttr = defaultLand;
        }
        
        /*Special steps here*/
        //if proposal stage is not specified (NULL or None), but status is Pending Approval: treated as In Review
		if (theRecord instanceof Apttus_Proposal__Proposal__c) {
			if ((String.isEmpty(chevronAttr) || chevronAttr.startsWith('0,') || chevronAttr.startsWith('1,')) && ProposalStatus == 'Pending Approval') {
				chevronAttr = elements.get('In Review');
			}
		}
		
		/*Special steps here*/
		//contract dev Reviewed
		String chevronAttr2;
        if (theRecord instanceof Apttus__APTS_Agreement__c) {
        	if ((String.isEmpty(chevronAttr) || chevronAttr.startsWith('6,')) && (proposalCD == true && AgreementCD == false)){//agreement currently in chevron "6" by status, and contract dev checkbox has been unchecked by contract dev team
				chevronAttr = elements.get('Contract Dev. Reviewed');
        	}
        	
        	if (chevronAttr != null && !chevronAttr.startsWith('6,') && !chevronAttr.startsWith('7,') && (proposalCD == true && AgreementCD == true)) {//agreement currently in chevron "6" by status, and contract dev checkbox has been unchecked by contract dev team
				chevronAttr2 = '7,red,Contract Dev.<BR/>In Review';
        	}
        }
        
        //draw selected piece
        String[] a = chevronAttr.split(',');
        Chevron c = ChevronPool[Integer.valueOf(a[0])];
        c.state = a[1];
        if (a.size() > 2 && !String.isEmpty(a[2]))
            c.ButtonFace = a[2];
            
        if (!String.isEmpty(chevronAttr2)) {
	        //draw selected piece
	        String[] a2 = chevronAttr2.split(',');
	        Chevron c2 = ChevronPool[Integer.valueOf(a2[0])];
	        c2.state = a2[1];
	        if (a2.size() > 2 && !String.isEmpty(a2[2]))
	            c2.ButtonFace = a2[2];
        }
    }
    
/**********************************
	Inner class. Define Chevron
**********************************/
    public class Chevron {
        public String ButtonFace {get; set;}
        public String State {get; set;}
        
        Chevron(String ButtonFace) {
            this.ButtonFace = ButtonFace;
            this.State = 'hidden';
        }
    }
}

/*
Other Chevron

#crumbs {
    text-align: center;
}

#crumbs ul {
    list-style: none;
    display: inline-table;
}

#crumbs ul li {
    display: inline;
}

#crumbs ul li a {
    display: block;
    float: left;
    height: 50px;
    background: #3498db;
    text-align: center;
    padding: 30px 40px 0 80px;
    position: relative;
    margin: 0 10px 0 0; 
    font-size: 20px;
    text-decoration: none;
    color: #fff;
}

#crumbs ul li a:before {
    content: "";  
    border-top: 40px solid transparent;
    border-bottom: 40px solid transparent;
    border-left: 40px solid white;
    position: absolute; left: 0; top: 0;
}
        
#crumbs ul li a:after {
    content: "";  
    border-top: 40px solid transparent;
    border-bottom: 40px solid transparent;
    border-left: 40px solid #3498db;
    position: absolute; right: -40px; top: 0;
    z-index: 1;
    
    box-shadow: 2px 2px 3px #888888;
}

<div id="crumbs">
<ul>
    <li><a href="#">Draft</a></li>
    <li><a href="#">Generated</a></li>
</ul>
</div>

*/