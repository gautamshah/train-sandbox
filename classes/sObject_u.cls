public abstract class sObject_u
{
    ////
    //// Instance Properties & Methods
    ////
    private Map<UUID, sObject_c> objs = new Map<UUID, sObject_c>();

    public void add(sObject_c obj)
    {
        objs.put(obj.UUID, obj);
    }

    public void addAll(List<sObject_c> objs)
    {
        for(sObject_c obj : objs)
            add(obj);
    }

    @testVisible
    protected List<sObject> sobjs;
    @testVisible
    protected List<sObject> get() { return sobjs; }
    @testVisible
    protected void set(List<sObject> value) { sobjs = value; }
    @testVisible
    protected void add(sObject value) { sobjs.add(value); }
    @testVisible
    protected void addAll(List<sObject> value) { sobjs.addAll(value); }

    @testVisible
    protected sObjectType sobjType;

    //private class sObjectRequiredException extends Exception { }
    //@testVisible
    //private static string sObjectRequiredMessage = 'A List<sObject> is required to use this class';

    @testVisible
    protected sObject_u()
    {
        this.set(new List<sObject>());
    }

    @testVisible
    protected sObject_u(sObjectType sObjType)
    {
        this();
        this.sObjType = sObjType;
    }

    @testVisible
    protected sObject_u(List<sObject> sobjs)
    {
        this();
        this.addAll(sobjs);
        this.sObjType = sobjs.getSObjectType();
    }

    @testVisible
    protected sObject_u(sObjectType sObjType, string whereClause)
    {
        this();

        string query =
            SOQL_select.buildQuery
            (
                sObjType,
                whereClause,
                null,
                null);

        this.addAll(Database.query(query));
    }

    SOQL_insert save;
    public string saveStatus()
    {
        if (save != null)
        {
            AsyncApexJob job = save.status();
            return (job != null) ? job.Status : null;
        }
        else
            return null;
    }

    public virtual List<string> save()
    {
        save = new SOQL_insert(this.sobjs);
        return save.execute();
    }

/*
    public virtual List<string> save()
    {
        integer previousCount = 0;
        Database.SaveResult[] results;

        // We need to make a copy because we will be removing the ones that are successfully
        // saved to keep retrying
        // Some are dependent on others being saved first, so we go through the list
        // several times
        List<sObject> sources = new List<sObject>(this.sobjs);

        do
        {
            results = Database.insert(sources, false);

            for(integer ndx = results.size() - 1; ndx >= 0; ndx--)
            {
                Database.SaveResult result = results[ndx];
                if (result.isSuccess())
                    sources.remove(ndx);
                else
                {
                    Database.Error[] errs = results[ndx].getErrors();
                    for(Database.Error err : errs)
                        if (err.getStatusCode() == StatusCode.DUPLICATE_VALUE)
                            sources.remove(ndx);
                }
            }

            if (sources.size() == previousCount) break;

            previousCount = sources.size();

        } while(!sources.isEmpty());

        List<string> errors = new List<string>();
        for(integer ndx = 0; ndx < sources.size(); ndx++)
        {
            Database.Error[] errs = results[ndx].getErrors();
            for(Database.Error err : errs)
                if (err.getStatusCode() != StatusCode.DUPLICATE_VALUE)
                    errors.add(string.valueOf(this) + sources[ndx] + '  *****  ' + err);
        }

        return errors;
    }
*/
    public Set<object> extract(Schema.SObjectField field)
    {
        Set<object> objs = new Set<object>();
        for(sObject sobj : this.sObjs)
            objs.add(sobj.get(field));

        return objs;
    }

    SOQL_delete del;
    public string checkStatus()
    {
        if (del != null)
        {
            AsyncApexJob job = del.status();
            return (job != null) ? job.Status : null;
        }
        else
            return null;
    }

    public virtual List<string> remove()
    {
        del = new SOQL_delete(this.sobjs);
        return del.execute();
    }

    public virtual void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<sObject> newList,
            Map<Id, sObject> newMap,
            List<sObject> oldList,
            Map<Id, sObject> oldMap,
            integer size
        ) { }

    ////
    //// Class Properties & Methods
    ////
    private static Map<string, sObjectType> GlobalDescribe;
    public static Map<string, sObjectType> Name2sObjectType;
    public static Map<sObjectType, string> sObjectType2Name;
    public static Map<type, sObjectType> Type2sObjectType;
    public static Map<sObjectType, type> sObjectType2Type;

    static
    {
        // 457.59 ms
        ////
        //// The getGlobalDescribe() includes deleted sObjects, so you
        //// may run into situations where the count of sObjects or 
        //// other components (like RecordType) will not be correct
        ////
        GlobalDescribe = schema.getGlobalDescribe();
        // 510.12 ms
        Name2sObjectType = GlobalDescribe;
        sObjectType2Name = new Map<sObjectType, string>();
        Type2sObjectType = new Map<type, sObjectType>();
        sObjectType2Type = new Map<sObjectType, type>();

        for(string key : GlobalDescribe.keySet())
        {
            // 472.69
            sObjectType2Name.put(GlobalDescribe.get(key), key);
            // 790.37
            Type2sObjectType.put(Type.forName(key), GlobalDescribe.get(key));
            // 1062.80
            sObjectType2Type.put(GlobalDescribe.get(key), Type.forName(key));

            // // 1062.60 -> 809.35
            // // 7,713.41 -> 8,109.97
            // Type keyType = Type.forName(key);
            // sObjectType2Name.put(GlobalDescribe.get(key), key);
            // Type2sObjectType.put(keyType, GlobalDescribe.get(key));
            // sObjectType2Type.put(keySOType, keyType);
        }
    }

    public static DescribeSObjectResult getSObjectDescribe(sObjectType sObjType)
    {
        return sObject_c.getSObjectDescribe(sObjType);
    }

    public static Map<string, sObjectField> getSObjectFields(sObjectType sObjType)
    {
        return sObject_c.getSObjectFields(sObjType);
    }

    public static sObjectType getSObjectType(string key)
    {
        return Name2sObjectType.containsKey(key) ? Name2sObjectType.get(key) : null;
    }

    // Returns the sObjectType for an sObject.class reference
    public static sObjectType getSObjectType(type key)
    {
        return Type2sObjectType.containsKey(key) ? Type2sObjectType.get(key) : null;
    }

    public static Type getType(sObjectType key)
    {
        return sObjectType2Type.containsKey(key) ? sObjectType2Type.get(key) : null;
    }
}