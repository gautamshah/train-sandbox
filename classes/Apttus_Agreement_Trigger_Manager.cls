/*
CHANGE HISTORY
===============================================================================
DATE       NAME                DESC
02/23/2016 Paul Berglund       The code from CPQ_Agreement_Before and CPQ_Agreement_After
                               had been merged into this class and they were both
                               inactivated.  The two triggers had been updated after
                               this and the code needed to be added here. These are
                               the migrated methods:
                               calculateDurationInMonth (Before)
                               UpdateRecordType (Before)
                               UpdateStatusWhenCancelled (Before)
                               ResetApprovalStatusWhenFieldChanged (Before)
                               RMSAgreementDirect (Before)
                               SendToPartner (After)
                               NOTE: Need to make sure the original triggers
                               are set inactive or you will fire these twice
02/29/2016 Paul Berglund       Based on email from Sarah - only set the
                               Price List and Expiration Date during Insert,
                               leave it alone on Update
03/03/2016 Bryan Fry           Added code for SendScrubPOActivationEmail
04/06/2016 Bryan Fry           Added code for sending activation emails for
                               custom kit and smart cart.
05/25/2016 Bryan Fry           Moved Scrub PO activation email call to Agreement
                               Document manager.
09/27/2016 Paul Berglund   Moved PopulateAnalystAndManager to cpqApprovers_logic
===============================================================================
*/
public class Apttus_Agreement_Trigger_Manager
{
    //@TestVisible Static Integer PopulateAnalystAndManager_Exec_Num =1;
    @TestVisible Static Integer SendToPartner_Exec_Num =1;
    @TestVisible Static Integer UpdateRecordType_Exec_Num =1;
    @TestVisible Static Integer UpdateStatusWhenCancelled_Exec_Num =1;
    @TestVisible Static Integer ResetApprovalStatusWhenFieldChanged_Exec_Num =1;
    @TestVisible Static Integer UpdateERPBySales_Exec_Num = 1;
    @TestVisible Static Integer AssociateWithQPCOT_Exec_Num = 1;
    @TestVisible Static Integer SetTotalContractValue_Exec_Num = 1;
    @TestVisible Static Integer CreatePrimaryContact_Exec_Num = 1;
    @testVisible Static Integer RMSAgreementDirect_Exec_Num = 1;
    @TestVisible Static Integer SendScrubPOActivationEmail_Exec_Num = 1;
    @TestVisible Static Integer SendCustomKitActivationEmail_Exec_Num = 1;
    @TestVisible Static Integer SendSmartCartActivationEmail_Exec_Num = 1;
    
    Boolean beforeInsert, beforeUpdate, afterInsert, afterUpdate;
    
    List<Apttus__APTS_Agreement__c> newList,oldList; 
    Map<Id,Apttus__APTS_Agreement__c> oldMap, newMap;
    
    public Apttus_Agreement_Trigger_Manager(Boolean isInsert, 
                                            Boolean isUpdate, 
                                            Boolean isBefore, 
                                            Boolean isAfter, 
                                            List<Apttus__APTS_Agreement__c> triggerNew, 
                                            List<Apttus__APTS_Agreement__c> triggerOld, 
                                            Map<Id,Apttus__APTS_Agreement__c> triggerOldMap, 
                                            Map<Id,Apttus__APTS_Agreement__c> triggerNewMap){
        beforeInsert = ((isInsert) && (isBefore));
        beforeUpdate = ((isUpdate) && (isBefore));
        afterInsert = ((isInsert) && (isAfter));
        afterUpdate = ((isUpdate) && (isAfter));
        
        newList = triggerNew;
        oldList = triggerOld;
        oldMap = triggerOldMap;
        newMap = triggerNewMap;
    }
    
    public void execute(){
        if(beforeInsert){
            for (Apttus__APTS_Agreement__c p : newList) {
                p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);
            }
            
            if(!CPQ_Trigger_Profile__c.getInstance().CPQ_Agreement_Before__c){

            	// 09/27/2016 Moved to cpqAgreement_logic
                //if (Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num > 0) {
                //    Apttus_Agreement_Trigger_Helper.PopulateAnalystAndManager(oldMap,newList);
                //    Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num --;
                //}

                // Update ERP_Ship_to_Address__c with ERP that has the highest sales 
                if (Apttus_Agreement_Trigger_Manager.UpdateERPBySales_Exec_Num > 0) {
                    CPQ_AgreementProcesses.UpdateERPBySales(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.UpdateERPBySales_Exec_Num--;
                }

                // Add pricelist and expiration date on rms agreement direct
                if (RMSAgreementDirect_Exec_Num > 0) {
                    CPQ_AgreementProcesses.RMSAgreementDirect(newList);
                    RMSAgreementDirect_Exec_Num--;
                }
            }
        }
        if(beforeUpdate){
            for (Apttus__APTS_Agreement__c p : newList) {
                p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);
            }
            
            if(!CPQ_Trigger_Profile__c.getInstance().CPQ_Agreement_Before__c){

            	// 09/27/2016 Moved to cpqAgreement_logic
                //if (Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num > 0) {
                //    Apttus_Agreement_Trigger_Helper.PopulateAnalystAndManager(oldMap,newList);
                //    Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num --;
                //}
                
                //Update record when activated
                if(Apttus_Agreement_Trigger_Manager.UpdateRecordType_Exec_Num > 0){
                    CPQ_AgreementProcesses.UpdateRecordType(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.UpdateRecordType_Exec_Num --;
                }
                
                //Update approval status to Not Submitted from Cancelled
                if(Apttus_Agreement_Trigger_Manager.UpdateStatusWhenCancelled_Exec_Num > 0){
                    CPQ_AgreementProcesses.UpdateStatusWhenCancelled(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.UpdateStatusWhenCancelled_Exec_Num --;
                }
        
                //Update approval status to Not Submitted after approved if any fields are changed
                if(Apttus_Agreement_Trigger_Manager.ResetApprovalStatusWhenFieldChanged_Exec_Num > 0){
                    CPQ_AgreementProcesses.ResetApprovalStatusWhenFieldChanged(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.ResetApprovalStatusWhenFieldChanged_Exec_Num --;
                }

                // Update ERP_Ship_to_Address__c with ERP that has the highest sales
                if (Apttus_Agreement_Trigger_Manager.UpdateERPBySales_Exec_Num > 0) {
                    CPQ_AgreementProcesses.UpdateERPBySales(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.UpdateERPBySales_Exec_Num--;
                }
                // Update Total Contract Value for Custom Kits and Scrub POs
                if (Apttus_Agreement_Trigger_Manager.SetTotalContractValue_Exec_Num > 0) {
                    CPQ_AgreementProcesses.SetTotalContractValue(newList);
                    Apttus_Agreement_Trigger_Manager.SetTotalContractValue_Exec_Num--;
                }
                // Create and associate primary comtact for Custom Kits and Scrub POs
                if (Apttus_Agreement_Trigger_Manager.CreatePrimaryContact_Exec_Num > 0) {
                    CPQ_AgreementProcesses.CreatePrimaryContact(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.CreatePrimaryContact_Exec_Num--;
                }

                // Don't do this as of 02/29/2016
                // //Update pricelist and expiration date on rms agreement direct
                // if (RMSAgreementDirect_Exec_Num > 1) {
                //      CPQ_AgreementProcesses.RMSAgreementDirect(newList, oldMap, false, true);
                //      RMSAgreementDirect_Exec_Num--;              
                // }
            }
        }
        if(afterInsert) {
            if(!CPQ_Trigger_Profile__c.getInstance().CPQ_Agreement_Before__c) {
                if (Apttus_Agreement_Trigger_Manager.AssociateWithQPCOT_Exec_Num > 0) {
                    CPQ_AgreementProcesses.AssociateWithQPCOT(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.AssociateWithQPCOT_Exec_Num--;
                }
            }
        }
        if(afterUpdate){
            if(!CPQ_Trigger_Profile__c.getInstance().CPQ_Agreement_After__c){
                if(Apttus_Agreement_Trigger_Manager.SendToPartner_Exec_Num > 0){
                    CPQ_AgreementProcesses.SendToPartner(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.SendToPartner_Exec_Num --;
                }
                if(Apttus_Agreement_Trigger_Manager.SendScrubPOActivationEmail_Exec_Num > 0){
                    CPQ_AgreementProcesses.CheckScrubPOActivation(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.SendScrubPOActivationEmail_Exec_Num --;
                }
                if(Apttus_Agreement_Trigger_Manager.SendCustomKitActivationEmail_Exec_Num > 0){
                    CPQ_AgreementProcesses.SendCustomKitActivationEmail(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.SendCustomKitActivationEmail_Exec_Num --;
                }
                if(Apttus_Agreement_Trigger_Manager.SendSmartCartActivationEmail_Exec_Num > 0){
                    CPQ_AgreementProcesses.SendSmartCartActivationEmail(newList, oldMap);
                    Apttus_Agreement_Trigger_Manager.SendSmartCartActivationEmail_Exec_Num --;
                }
            }
        }
    }
}