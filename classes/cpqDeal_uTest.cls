/**

Tests methods in cpqDeal_u class by simply creating an Agreement or Proposal.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
10/10/2016      Paul Berglund       Created
10/18/2016      Isaac Lewis         Added methods to test record types and deal types
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
11/02/2016      Isaac Lewis         Restored aRT and pRT references
===============================================================================
*/


@isTest
public class cpqDeal_uTest
{
	static cpq_TestSetup setup = new cpq_TestSetup();
	
    private Static RecordType aRT;
    private Static RecordType pRT;

    private Static void setup ()
    { 
        aRT = cpqRecordType_TestSetup.getRecordType(cpq_u.AGREEMENT_DEV_NAME);
        pRT = cpqRecordType_TestSetup.getRecordType(cpq_u.PROPOSAL_DEV_NAME);
    }

    // Test methods

    @isTest static void itShouldGetRecordTypesById () {

        Integer queries;

        queries = Limits.getQueries();
        System.assert(cpqDeal_RecordType_c.getRecordTypesById().keySet().size() > 0, 'No record types returned');
        // TODO: Reduce to queries + 1 after fixing cpq_u.SystemProperties reference.
        System.assertEquals(queries + 1, Limits.getQueries(), 'Used more than one query');
        RecordType rt = cpqRecordType_TestSetup.getRecordType(cpq_u.AGREEMENT_DEV_NAME);

        queries = Limits.getQueries();
        System.assert(cpqDeal_RecordType_c.getRecordTypesById().keySet().contains(rt.Id), 'Record type not found');
        System.assertEquals(queries,Limits.getQueries(), 'Used more than one query');

    }

    @isTest static void itShouldGetRecordTypesByName () {

        System.assert(cpqDeal_RecordType_c.getRecordTypesByName().keySet().size() > 0, 'No record types returned');
        System.assert(cpqDeal_RecordType_c.getRecordTypesByName().keySet()
            .contains(cpqRecordType_TestSetup.getRecordType(cpq_u.AGREEMENT_DEV_NAME).DeveloperName),'Named Record Type Not Found');

    }

    @isTest static void itShouldGetRecordTypesBySObjectAndId () {

        setup();
        System.assertEquals(pRT.Name,cpqDeal_RecordType_c.getRecordTypesBySObjectAndId()
        	.get(cpq_u.PROPOSAL_SOBJECT_TYPE).get(pRT.Id).Name,
            'Proposal Record Type Not Found');
        System.assertEquals(aRT.Name,cpqDeal_RecordType_c.getRecordTypesBySObjectAndId()
        	.get(cpq_u.AGREEMENT_SOBJECT_TYPE).get(aRT.Id).Name,
            'Agreement Record Type Not Found');

    }

    @isTest static void itShouldGetRecordTypesBySObjectAndName ()
    {
        setup();
        System.assertEquals(pRT.Id,cpqDeal_RecordType_c.getRecordTypesBySObjectAndName()
        	.get(cpq_u.PROPOSAL_SOBJECT_TYPE).get(pRT.DeveloperName).Id,
            'Proposal Record Type Not Found');
        System.assertEquals(aRT.Id,cpqDeal_RecordType_c.getRecordTypesBySObjectAndName()
        	.get(cpq_u.AGREEMENT_SOBJECT_TYPE).get(aRT.DeveloperName).Id,
            'Agreement Record Type Not Found');

    }

    @isTest static void itShouldGetRecordTypesByOrgAndName()
    {
        for(OrganizationNames_g.Abbreviations abbr : OrganizationNames_g.Abbreviations.values())
        {
            System.assert(cpqDeal_RecordType_c.getRecordTypesByOrgAndName().containsKey(abbr), abbr.name() + ' not found in map');
            System.assert(cpqDeal_RecordType_c.getRecordTypesByOrgAndName().get(abbr).size() > 0, 'No record types returned for ' + cpq_TestSetup.org.name());
        }
    }

    @isTest static void itShouldGetAgreementRecordTypesById () {
        Map<Id,RecordType> result = cpqDeal_RecordType_c.getAgreementRecordTypesById();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.PROPOSAL_DEV_NAME,rt.SObjectType, 'Pulled Proposal Record Type');
        }
    }

    @isTest static void itShouldGetAgreementRecordTypesByName () {
        Map<String,RecordType> result = cpqDeal_RecordType_c.getAgreementRecordTypesByName();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.PROPOSAL_DEV_NAME,rt.SObjectType, 'Pulled Proposal Record Type');
        }
    }

    @isTest static void itShouldGetProposalRecordTypesById () {
        Map<Id,RecordType> result = cpqDeal_RecordType_c.getProposalRecordTypesById();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.AGREEMENT_DEV_NAME,rt.SObjectType, 'Pulled Agreement Record Type');
        }
    }

    @isTest static void itShouldGetProposalRecordTypesByName () {
        Map<String,RecordType> result = cpqDeal_RecordType_c.getProposalRecordTypesByName();
        System.assert(result.keySet().size() > 0);
        for (RecordType rt : result.values()) {
            System.assertNotEquals(cpq_u.AGREEMENT_DEV_NAME,rt.SObjectType, 'Pulled Agreement Record Type');
        }
    }

    @isTest static void itShouldGetDealTypes () {
        Set<String> result = cpqDeal_Type_c.getDealTypes();
        for (String dt : result) {
            System.assert(!dt.endsWith('Locked'),'Contains Locked Suffix');
            System.assert(!dt.endsWith('AG'),'Contains AG Suffix');
        }
    }

    @isTest static void itShouldGetDealTypesForSObject () {
        Set<String> aResult = cpqDeal_Type_c.getDealTypesForSObject(cpq_u.AGREEMENT_SOBJECT_TYPE);
        System.assert(aResult.size() > 0, 'No Agreement Record Types Returned');
        Set<String> pResult = cpqDeal_Type_c.getDealTypesForSObject(cpq_u.PROPOSAL_SOBJECT_TYPE);
        System.assert(pResult.size() > 0, 'No Proposal Record Types Returned');
        System.assert(!pResult.containsAll(aResult), 'Same Record Type Set Returned For Different Business Groups');
    }

    // TODO: Fix so it always returns name without underscores
    @isTest static void itShouldGetDealType () {
        String dt = 'Deal Type';

        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+' - Locked'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'-Locked'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'_Locked'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+' Locked'));

        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+' - AG'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'-AG'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'_AG'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+' AG'));

        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'- AG - Locked'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'-AG-Locked'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+'_AG_Locked'));
        System.assertEquals(dt,cpqDeal_Type_c.getDealType(dt+' AG Locked'));

    }

    @isTest static void itShouldGetDealTypeFromRecordTypeId () {
        setup();
        System.assertEquals(cpqDeal_Type_c.getDealType(aRT.DeveloperName),
        	cpqDeal_Type_c.getDealTypeFromRecordTypeId(aRT.Id));
    }

    @isTest static void itShouldGetDealTypeFromRecordTypeLabel () {
        setup();
        System.assertEquals(cpqDeal_Type_c.getDealType(aRT.DeveloperName),
        	cpqDeal_Type_c.getDealTypeFromRecordTypeLabel(aRT.Name));
    }

    @isTest static void itShouldGetDealTypesFromRecordTypeLabels () {

        Set<String> rtLabels = new Set<String>();
        List<RecordType> rts = cpqDeal_RecordType_c.getRecordTypesById().values();
        for (RecordType rt : rts) {
            rtLabels.add(rt.Name);
        }
        Set<String> dealTypes = cpqDeal_Type_c.getDealTypesFromRecordTypeLabels(rtLabels);
        System.assert(dealTypes.size() > 0, 'No Deal types Found');
        for (String dt : dealTypes) {
            System.assert(!dt.contains(' '), 'Deal Type is not based on Developer Name');
            System.assert(!dt.endsWith('AG'), 'Deal Type contains AG suffix');
            System.assert(!dt.endsWith('Locked'), 'Deal Type contains Locked suffix');
        }

    }

    @isTest static void itShouldGetDealTypesByOrg()
    {
        for(OrganizationNames_g.Abbreviations abbr : OrganizationNames_g.Abbreviations.values())
        {
            System.assert(
            	cpqDeal_Type_c.getDealTypesByOrg().containsKey(abbr),
            	abbr.name() + ' not found in map');
            	
            if(abbr != OrganizationNames_g.Abbreviations.MS)
                System.assert(
                	cpqDeal_Type_c.getDealTypesByOrg().get(abbr).size() > 0,
                	'No deal types returned for ' + abbr.name());
        }
    }

    @isTest static void itShouldGetDealTypesForOrg () {
        System.assert(cpqDeal_Type_c.getDealTypesForOrg(OrganizationNames_g.Abbreviations.RMS).size() > 0);
        System.assert(cpqDeal_Type_c.getDealTypesForOrg(OrganizationNames_g.Abbreviations.SSG).size() > 0);
        System.assert(!cpqDeal_Type_c.getDealTypesForOrg(OrganizationNames_g.Abbreviations.SSG)
        	.containsAll(cpqDeal_Type_c.getDealTypesForOrg(OrganizationNames_g.Abbreviations.RMS)));
    }

    @isTest static void itShouldGetDirectDealTypes () {
        System.assert(cpqDeal_Type_c.getDirectDealTypes().size() > 0);
    }

    @isTest static void itShouldGetDirectDealTypesForOrg () {
        System.assert(cpqDeal_Type_c.getDirectDealTypesForOrg(OrganizationNames_g.Abbreviations.RMS).size() < cpqDeal_Type_c.getDirectDealTypes().size());
    }

    @isTest static void itShouldGetDealTypesAllowingTracingCustomer () {
        System.assert(cpqDeal_Type_c.getDealTypesAllowingTracingCustomer().size() > 0);
    }

    @isTest static void itShouldGetDealTypesRequireOpportunityByAmount () {
        System.assert(cpqDeal_Type_c.getDealTypesRequireOpportunityByAmount().size() > 0);
    }

    @isTest static void itShouldGetDealTypesRequiringNoCart () {
        System.assert(cpqDeal_Type_c.getDealTypesRequiringNoCart().size() > 0);
    }

    @isTest static void itShouldGetDealTypesRequiringNoOpportunity () {
        System.assert(cpqDeal_Type_c.getDealTypesRequiringNoOpportunity().size() > 0);
    }

    @isTest static void itShouldGetDealTypesNotAllowingPriceAdjustment () {
        System.assert(cpqDeal_Type_c.getDealTypesNotAllowingPriceAdjustment().size() > 0);
    }

    @isTest static void itShouldAssertIsDeal()
    {
        System.assertEquals(true, cpqDeal_u.isDeal(new Apttus_Proposal__Proposal__c()));
        System.assertEquals(true, cpqDeal_u.isDeal(new Apttus__APTS_Agreement__c()));
        System.assertEquals(false, cpqDeal_u.isDeal(new Account()));
    }

    @isTest static void itShouldAssertIsAgreement()
    {
        System.assertEquals(false, cpqDeal_u.isAgreement(new Apttus_Proposal__Proposal__c()));
        System.assertEquals(true, cpqDeal_u.isAgreement(new Apttus__APTS_Agreement__c()));
    }

    @isTest static void itShouldAssertIsProposal()
    {
        System.assertEquals(true, cpqDeal_u.isProposal(new Apttus_Proposal__Proposal__c()));
        System.assertEquals(false, cpqDeal_u.isProposal(new Apttus__APTS_Agreement__c()));
    }

    // TODO: Build scenario for proposals and agreements
    @isTest static void itShouldGetUnlockedRecordTypeName()
    {
        String name;

        Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c();
        name = cpqDeal_RecordType_c.getUnlockedRecordTypeName(prop);
        System.assert(!name.contains('Locked'));

        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
        name = cpqDeal_RecordType_c.getUnlockedRecordTypeName(agmt);
        System.assert(!name.contains('Locked'));

    }

    @isTest static void itShouldGetDealTypeConcrete()
    {
        String name;

        Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c();
        name = cpqDeal_Type_c.getDealType(prop);
        System.assert(!name.contains('Locked'));

        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
        name = cpqDeal_Type_c.getDealType(agmt);
        System.assert(!name.contains('Locked'));

        // Test with dealname starting with 'Amendment_to_Add_Facilities'

    }

    // @isTest static void itShouldGetDealTypeLabel () {
    //
    //     String label;
    //
    //     Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c();
    //     label = cpqDeal_u.getDealTypeLabel(prop);
    //     System.assert(!label.contains('Locked'));
    //
    //     Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
    //     label = cpqDeal_u.getDealTypeLabel(agmt);
    //     System.assert(!label.contains('Locked'));
    //
    //     // Test with dealname starting with 'Amendment to Add Facilities'
    //
    // }
}