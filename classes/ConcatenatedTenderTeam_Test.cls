@istest

Public class ConcatenatedTenderTeam_Test {
/****************************************************************************************
    * Name    : ConcatenatedTenderTeam_Test
    * Author  : Lakhan Dubey
    * Date    : 10/14/2014
    * Purpose :Test class for  trigger 'ConcatenatedTenderTeam'.
  
    *****************************************************************************************/

     static  testmethod  void createtenderteam (){
 
     Profile p =[SELECT Id FROM Profile WHERE Name='EU - One Covidien'];             
      User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', FirstName='TestingL', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, Region__c='EU',Business_Unit__c='SGD',User_Role__c='Rep',Function__c='Sales',User_Visibility__c='Region',
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduserre@testorg.com');
      Id tenderId=null;
      insert u;
      User  testuser=[Select id, name from User  where ProfileId !=: p.Id limit 1];
      Id RId=[Select id from recordtype where name='EU-Healthcare Facility'].id;
      Account a=new Account();
      a.name='testacc';
      a.CurrencyIsoCode='EUR';
      a.OwnerId = u.Id;
      a.recordtypeId=RId;
      insert a;
       
      System.runas(u){
   
      Tenders__c t = new Tenders__c();
      t.Tender_Start_Date__c=system.today();
      t.Account_GPO__c=a.Id;
      t.CurrencyIsoCode='EUR';
      t.Multiple_Criteria_Sets__c=true;
      insert t;
      
      Test.starttest();
      Tender_Team__c tt=new Tender_Team__c();
      tt.Tender__c=t.Id;
      tt.Team_Member__c=u.id;
      insert tt;
      Tender_Team__c tt1 =new Tender_Team__c();
      tt1.Tender__c=t.Id;
      tt1.Team_Member__c=testuser.Id;
      insert tt1;     
      delete tt1;
      Test.stoptest();
      tenderId=t.Id;
      
      
      }
 System.assert([Select  Tender_Team__c from Tenders__c where id=:tenderId ].Tender_Team__c.contains('TestingL'+' '+'Testing'));
 System.assert(![Select  Tender_Team__c from Tenders__c where id=:tenderId ].Tender_Team__c.contains(testuser.name));
 
 
  }

}