public with sharing class SalesNavigatorStage_TriggerHandler extends TriggerFactoryHandlerBase
{
	SalesNavigatorStage_Utilities utils = new SalesNavigatorStage_Utilities();
	
	public override void OnBeforeInsert(List<SObject> newMappings)
	{
		utils.OnBeforeInsert((List<Sales_Navigator_Stage__c>) newMappings);
	}
	
	public override void OnBeforeUpdate(List<SObject> oldMappings, List<SObject> updatedMappings, Map<ID, SObject> mappingOldMap, Map<ID, SObject> mappingNewMap)
	{
		utils.OnBeforeUpdate((List<Sales_Navigator_Stage__c>) updatedMappings);
	}
	
	public override void OnBeforeDelete(List<SObject> MappingsToDelete, Map<ID, SObject> MappingMap)
	{
		utils.OnBeforeDelete((List<Sales_Navigator_Stage__c>) MappingsToDelete, (Map<ID, Sales_Navigator_Stage__c>) MappingMap);
	}
}