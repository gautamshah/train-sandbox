public with sharing class CreateContactFromAccountExtension {
    /****************************************************************************************
    * Name    : CreateContactFromAccountExtension
    * Author  : Mike Melcher
    * Date    : 12-16-2011
    * Purpose : Creates Master and Connected Contacts on an Account.  The methods in this class
    *           are called by detail page buttons on the Account object.  The buttons call Visualforce pages.
    *          
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE          AUTHOR              CHANGE
    * ----          ------              ------
    * 1/24/2012     MJM                 Added methods for creating Connected Clinician and Connected Non Clinician Contacts
    * 8/23/2013     Gautam Shah         Added the method 'getRTByProfile' and modified the method 'createConnectedClinician' to use the new method
    *****************************************************************************************/ 
    private final Account a;
    
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String accountID {get; set;}
    public String masterID {get; set;}
    public String firstname {get; set;}
    public String lastname {get; set;}


   
    public CreateContactFromAccountExtension(ApexPages.StandardController stdController) {
        this.a = (Account)stdController.getRecord();
    }
   
    
    public pagereference createMasterNonClinician() {
         Account currentA = [select id, Name, RecordTypeId
                             from Account
                             where id = :a.Id];
                             
                             
      // retURL = ApexPages.currentPage().getParameters().get('retURL');
       rType = ApexPages.currentPage().getParameters().get('RecordType');
      // cancelURL = ApexPages.currentPage().getParameters().get('\001\' + currentA.Id');
       ent = ApexPages.currentPage().getParameters().get('ent');
       confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
       saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
       //accountID = ApexPages.currentPage().getParameters().get('def_account_id');
       masterId = a.Id;
      // firstname = currentC.FirstName;
      // lastname = currentC.LastName;
       DataQuality dq = new DataQuality();       
       Map<String,Id> rtmap = dq.getRecordTypes(); 
       Id newRT = rtmap.get('Master Non Clinician');
       Pagereference returnURL = new PageReference('/003/e');
       returnURL.getParameters().put('retURL', retURL);
       returnURL.getParameters().put('RecordType', newRT);
       returnURL.getParameters().put('cancelURL', cancelURL);
       returnURL.getParameters().put('ent', ent);
       returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
       returnURL.getParameters().put('save_new_url', saveNewURL);
       returnURL.getParameters().put('nooverride', '1');

                 
       returnURL.getParameters().put('con4', currentA.Name);      
       returnURL.getParameters().put('con4_lkid', currentA.Id);            
            
       returnURL.setRedirect(true);
       return returnURL;
  }
  
  public pagereference createMasterClinician() {
         Account currentA = [select id, Name, RecordTypeId
                             from Account
                             where id = :a.Id];
                             
                             
      // retURL = ApexPages.currentPage().getParameters().get('retURL');
       rType = ApexPages.currentPage().getParameters().get('RecordType');
      // cancelURL = ApexPages.currentPage().getParameters().get('\001\' + currentA.Id');
       ent = ApexPages.currentPage().getParameters().get('ent');
       confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
       saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
       //accountID = ApexPages.currentPage().getParameters().get('def_account_id');
       masterId = a.Id;
      // firstname = currentC.FirstName;
      // lastname = currentC.LastName;
       DataQuality dq = new DataQuality();       
       Map<String,Id> rtmap = dq.getRecordTypes(); 
       Id newRT = rtmap.get('Master Clinician');
       Pagereference returnURL = new PageReference('/003/e');
       returnURL.getParameters().put('retURL', retURL);
       returnURL.getParameters().put('RecordType', newRT);
       returnURL.getParameters().put('cancelURL', cancelURL);
       returnURL.getParameters().put('ent', ent);
       returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
       returnURL.getParameters().put('save_new_url', saveNewURL);
       returnURL.getParameters().put('nooverride', '1');

                 
       returnURL.getParameters().put('con4', currentA.Name);      
       returnURL.getParameters().put('con4_lkid', currentA.Id);            
            
       returnURL.setRedirect(true);
       return returnURL;
  }
 
    public pagereference createConnectedClinician() 
    {
        Account currentA = [select id, Name, RecordTypeId from Account where id = :a.Id];

       ent = ApexPages.currentPage().getParameters().get('ent');
       confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
       saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
       masterId = a.Id;
       Id newRT = getRTByProfile('Contact');
       System.debug('newRT: ' + newRT);
       Pagereference returnURL = new PageReference('/003/e');
       returnURL.getParameters().put('retURL', retURL);
       returnURL.getParameters().put('RecordType', newRT);
       returnURL.getParameters().put('cancelURL', cancelURL);
       returnURL.getParameters().put('ent', ent);
       returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
       returnURL.getParameters().put('save_new_url', saveNewURL);
       returnURL.getParameters().put('nooverride', '1');
       returnURL.getParameters().put('con4', currentA.Name);      
       returnURL.getParameters().put('con4_lkid', currentA.Id);            
       returnURL.setRedirect(true);
       return returnURL;
    }
  
  public pagereference createConnectedNonClinician() {
         Account currentA = [select id, Name, RecordTypeId
                             from Account
                             where id = :a.Id];
                             
                             
      // retURL = ApexPages.currentPage().getParameters().get('retURL');
       rType = ApexPages.currentPage().getParameters().get('RecordType');
      // cancelURL = ApexPages.currentPage().getParameters().get('\001\' + currentA.Id');
       ent = ApexPages.currentPage().getParameters().get('ent');
       confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
       saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
       //accountID = ApexPages.currentPage().getParameters().get('def_account_id');
       masterId = a.Id;
      // firstname = currentC.FirstName;
      // lastname = currentC.LastName;
       DataQuality dq = new DataQuality();       
       Map<String,Id> rtmap = dq.getRecordTypes(); 
       Id newRT = rtmap.get('Connected Non Clinician Contact');
       Pagereference returnURL = new PageReference('/003/e');
       returnURL.getParameters().put('retURL', retURL);
       returnURL.getParameters().put('RecordType', newRT);
       returnURL.getParameters().put('cancelURL', cancelURL);
       returnURL.getParameters().put('ent', ent);
       returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
       returnURL.getParameters().put('save_new_url', saveNewURL);
       returnURL.getParameters().put('nooverride', '1');

                 
       returnURL.getParameters().put('con4', currentA.Name);      
       returnURL.getParameters().put('con4_lkid', currentA.Id);            
            
       returnURL.setRedirect(true);
       return returnURL;
  }
    //Below method added by Gautam Shah on 8/23/2013 and is called from createConnectedClinician()
    private Id getRTByProfile(String objectType)
    {
        List<RecordType> rtList = new List<RecordType>([Select Id, Name From RecordType Where Sobjecttype = :objectType]);
        Profile userProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
        System.debug('Current Users Profile: ' + userProfile.Name);
        //create the list of profiles on which to evaluate
        Set<String> US_Profiles_GroupA = new Set<String>{'US - All','US - AST','US - Health Systems (Read Only)','US - Med Supplies','US - RMS','US - SUS','US - VT'};
        Map<String,Id> rtMap = new Map<String,Id>();
        for(RecordType rt : rtList)
        {
            rtMap.put(rt.Name,rt.Id);
        }
        Id rt;
        if(US_Profiles_GroupA.contains(userProfile.Name))
        {
            rt = rtMap.get('Connected Contact US');
        }
        else
        {
            rt = rtMap.get('Connected Clinician');
        }
        return rt;
    }

}