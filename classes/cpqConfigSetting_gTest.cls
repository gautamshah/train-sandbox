@isTest
public class cpqConfigSetting_gTest
{
	@isTest
	static void constructor()
	{
		cpqConfigSetting_g cache = new cpqConfigSetting_g();
		system.assertNotEquals(null, cache);
	}
	
	@isTest
	static void cast()
	{
		CPQ_Config_Setting__c setting = new CPQ_Config_Setting__c();
		
		Map<Id, sObject> source = new Map<Id, sObject>();
		Id id = sObject_c.dummyId(CPQ_Config_Setting__c.sObjectType, 1);
		setting.Id = id;
		source.put(id, setting);
		
		Map<Id, CPQ_Config_Setting__c> result = cpqConfigSetting_g.cast(source);
		system.assertEquals(CPQ_Config_Setting__c.sObjectType, result.values()[0].getSObjectType());
	}
	
	@isTest
	static void put()
	{
		cpqConfigSetting_g cache = new cpqConfigSetting_g();

		system.debug('cpqConfigSetting_gTest');
		system.debug(CONSTANTS.NBSP(15) + '====> put');
		CPQ_Config_Setting__c testdata = cpqConfigSetting_c.create();
		insert testdata;

		system.debug(CONSTANTS.NBSP(15) + '====> 1st put');
		cache.put(testdata);

		CPQ_Config_Setting__c testdata2 = cpqConfigSetting_c.create();
		testdata2.Name = 'Test Data 2';
		insert testdata2;

		system.debug(CONSTANTS.NBSP(15) + '====> 2nd put');
		cache.put(testdata2);
	}
}