global class Schedule_BatchOppRevenueScheduleUpdate implements Schedulable
{
	global void execute(SchedulableContext sc)
	{
		BatchOppRevenueScheduleUpdate batchTask = new BatchOppRevenueScheduleUpdate();
		Database.executeBatch(batchTask);
	}
}