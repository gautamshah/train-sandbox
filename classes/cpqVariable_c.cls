public class cpqVariable_c
{
	// CPQ Variable
	//
	// Custom Setting
	//
	// Represents a key/value pair using string/string
	//
	public static CPQ_Variable__c create(
		string name,
		string value)
	{
		return (name == null) ?
			null :
			new CPQ_Variable__c(Name = name, Value__c = value);
	}
	
    public static string getValue(string name)
    {
        CPQ_Variable__c var = CPQ_Variable__c.getValues(name);

        // You have to check to see if the name value null
        // If you don't, passing null to getValues() will return
        // inconsistent results
    	return (name != null && var != null) ? var.Value__c : null;
    } 

    public static Set<String> getValue(string name, string separator)
    {
        string result = getValue(name);
		return (string.isBlank(result)) ? null :
        			(string.isBlank(separator)) ?
                        new Set<string>{ result } :
        				new Set<string>(result.split(separator));
    }
}