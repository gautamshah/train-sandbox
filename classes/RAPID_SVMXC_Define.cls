public class RAPID_SVMXC_Define
{
    //Part Consumption Constants
    public static final string WORK_ORDER_LINE_STATUS = 'Open';
    public static final string USAGE_CONSUMPTION = 'Usage/Consumption';
    public static final string PART_LINE_TYPE = 'Parts';
    public static final string OPEN_LINE_STATUS = 'Open';
    public static final string CLOSED_LINE_STATUS = 'Closed';
    
    public static final String AVAIL = 'Available';
    public static final String DEFT = 'Defective';
    public static final String CONS = 'Consumption';
    public static final String RETN = 'Returned';
    public static final String RET = 'Returned';
    
}