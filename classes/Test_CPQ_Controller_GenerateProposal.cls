/*
Test Class

CPQ_Controller_GenerateProposal
CPQ_ProposalGenerator

CHANGE HISTORY
===============================================================================
DATE        NAME             DESC
2015-03-01  Yuli Fintescu    Created
05/06/2016  Paul Berglund    Added test for BeforeGenerate and additional tests
                             to increase code coverage.
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
11/07/2015   Isaac Lewis     Replaced record type maps with calls to cpqDeal_RecordType_c
===============================================================================
*/
@isTest
private class Test_CPQ_Controller_GenerateProposal
{
    static List<Apttus_Proposal__Proposal__c> testProposals;
    static Opportunity testOpp;

    static void createTestData()
    {
System.debug('Start createTestData');

DEBUG.debugQueries();

        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.SSG;

        upsert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        cpq_TestSetup.testRollups = new List<CPQ_Rollup_Field__c>
        {
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'RF', Field_API_Name__c = 'T_HLVL_RF__c', Purpose__c = 'Selected', Source__c = 'Hier Level'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'QAB', Field_API_Name__c = 'T_HLVL_QAB__c', Purpose__c = 'Selected', Source__c = 'Hier Level'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'RFC0', Field_API_Name__c = 'T_HLVL_RFC0__c', Purpose__c = 'Selected', Source__c = 'Hier Level'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'RFC6C', Field_API_Name__c = 'T_HLVL_RFC6C__c', Purpose__c = 'Selected', Source__c = 'Hier Level'),

            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'Hardware', Field_API_Name__c = 'Hardware_Selected__c', Purpose__c = 'Selected', Source__c = 'Charge Type'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = '080-0201', Field_API_Name__c = 'T_PRDCD_080_02__c', Purpose__c = 'Selected', Source__c = 'Product Code'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'QS840FLEX', Field_API_Name__c = 'T_PRMCD_QS840FLEX__c', Purpose__c = 'Selected', Source__c = 'Promotion Code'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'Patient Monitoring', Field_API_Name__c = 'Patient_Monitoring_Templates__c', Purpose__c = 'Selected', Source__c = 'Product2 Category'),

            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'Trade-In', Field_API_Name__c = 'Trade_In_Subtotal__c', Purpose__c = 'Subtotal', Source__c = 'Charge Type'),
            new CPQ_Rollup_Field__c(SObject__c = 'Proposal', Code_Group__c = 'BIS Consumables', Field_API_Name__c = 'Compliance_Amount_1__c', Purpose__c = 'Subtotal', Source__c = 'Category Hierarchy')
        };
        insert cpq_TestSetup.testRollups;

        cpq_TestSetup.testProduct = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG, 'TEST', 1);
        insert cpq_TestSetup.testProduct;

        cpq_TestSetup.testPriceListItems =
            cpqPriceListItem_u.fetch(
                Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
                testPriceList.Id);

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values())
        {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

        cpq_TestSetup.testGPO = new Account(Name = 'Test GPO', Status__c = 'Active', Account_External_ID__c = 'USGG-SG0150', RecordTypeID = gpoRT.getRecordTypeId());
        insert cpq_TestSetup.testGPO;

        cpq_TestSetup.testAccounts = new List<Account>
        {
            new Account(Status__c = 'Active', Name = 'Test Account', Primary_GPO_Buying_Group_Affiliation__c = cpq_TestSetup.testGPO.ID,  RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-111111'),

            new Account(Status__c = 'Active', Name = 'Facilitiy', Primary_GPO_Buying_Group_Affiliation__c = cpq_TestSetup.testGPO.ID,     RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-222222'),
            new Account(Status__c = 'Active', Name = 'Distributor', Primary_GPO_Buying_Group_Affiliation__c = cpq_TestSetup.testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-333333')
        };
        insert cpq_TestSetup.testAccounts;

DEBUG.debugQueries(); 

        cpq_TestSetup.testOpp = new Opportunity(Name = 'Test Opp', AccountID = cpq_TestSetup.testAccounts[0].ID, StageName = 'Identify', CloseDate = System.today());
        insert cpq_TestSetup.testOpp;

DEBUG.debugQueries();

        cpq_TestSetup.testRoot = new Partner_Root_Contract__c(Name = '011111', Root_Contract_Name__c = '011111', Root_Contract_Number__c = '011111', Direction__c = 'I', Effective_Date__c = System.Today());
        insert cpq_TestSetup.testRoot; 

        //Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.SendToPartner_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.UpdateRecordType_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.UpdateStatusWhenCancelled_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.ResetApprovalStatusWhenFieldChanged_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.UpdateERPBySales_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.AssociateWithQPCOT_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.SetTotalContractValue_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.CreatePrimaryContact_Exec_Num = 0;
        Apttus_Agreement_Trigger_Manager.SendScrubPOActivationEmail_Exec_Num = 0;
        cpqAgreement_t.executedMethods.add('capture');
        //Original and 1st, 2nd Amendment agreements
        cpq_TestSetup.testOldAgreements =
            new List<Apttus__APTS_Agreement__c>
        {
            new Apttus__APTS_Agreement__c(Name = 'Original New',    Deal_Type__c = 'New Deal',                            
                                          Apttus__Status_Category__c = 'In Effect', Apttus__Status__c = 'Activated', Apttus__Contract_End_Date__c = System.Today()),
            new Apttus__APTS_Agreement__c(Name = '1st Amendment',   Deal_Type__c = 'Amendment', Amendment_Sequence__c = 1,
                                          Apttus__Status_Category__c = 'In Effect', Apttus__Status__c = 'Activated', Apttus__Contract_End_Date__c = System.Today()),
            new Apttus__APTS_Agreement__c(Name = '2nd Amendment',   Deal_Type__c = 'Amendment', Amendment_Sequence__c = 2,
                                          Apttus__Status_Category__c = 'In Effect', Apttus__Status__c = 'Activated', Apttus__Contract_End_Date__c = System.Today()),

            new Apttus__APTS_Agreement__c(Name = 'Test Proposal\'s Agreement',  Deal_Type__c = 'New Deal',
                                          Apttus__Status_Category__c = 'In Effect', Apttus__Status__c = 'Activated', Apttus__Contract_End_Date__c = System.Today())
        };
        insert cpq_TestSetup.testOldAgreements;

DEBUG.debugQueries();

        cpqProposal_t.executedMethods.add('PopulateAnalyst');
        cpqProposal_t.executedMethods.add('capture');
        cpq_TestSetup.testProposals = new List<Apttus_Proposal__Proposal__c>
        {
            //good proposal
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'),
                                             Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID),

            //no config
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test No Config', RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'),
                                             Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID),

            //Renew No master
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Renew No Agreement', RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'),
                                             Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID),

            //amend source
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Original New',   RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement_Locked'),      
                                             Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID, Apttus_Proposal__Opportunity__c = cpq_TestSetup.testOpp.ID, Deal_Type__c = 'New Deal',
                                             Apttus_QPComply__MasterAgreementId__c = cpq_TestSetup.testOldAgreements[0].Id),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = '1st Amendment',   RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement_Locked'),     
                                             Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID, Apttus_Proposal__Opportunity__c = cpq_TestSetup.testOpp.ID, Deal_Type__c = 'Amendment',
                                             Apttus_QPComply__MasterAgreementId__c = cpq_TestSetup.testOldAgreements[0].Id, Original_Agreement__c = cpq_TestSetup.testOldAgreements[0].ID, Amendment_Sequence__c = 1),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = '2nd Amendment',        RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement_Locked'),
                                             Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID, Apttus_Proposal__Opportunity__c = cpq_TestSetup.testOpp.ID, Deal_Type__c = 'Amendment',
                                             Apttus_QPComply__MasterAgreementId__c = cpq_TestSetup.testOldAgreements[1].Id, Original_Agreement__c = cpq_TestSetup.testOldAgreements[0].ID, Amendment_Sequence__c = 2)

        };
        insert cpq_TestSetup.testProposals;

DEBUG.debugQueries();

        //partner: amend source
        cpq_TestSetup.testOldAgreements[0].Apttus_QPComply__RelatedProposalId__c = cpq_TestSetup.testProposals[3].ID;
        cpq_TestSetup.testOldAgreements[1].Apttus_QPComply__RelatedProposalId__c = cpq_TestSetup.testProposals[4].ID;
        cpq_TestSetup.testOldAgreements[2].Apttus_QPComply__RelatedProposalId__c = cpq_TestSetup.testProposals[5].ID;
        cpq_TestSetup.testOldAgreements[3].Apttus_QPComply__RelatedProposalId__c = cpq_TestSetup.testProposals[0].ID;
        update cpq_TestSetup.testOldAgreements;

DEBUG.debugQueries();

        system.assertNotEquals(null, testPriceList.ID);
        system.assertNotEquals(null, cpq_TestSetup.testProposals[0].ID);

        cpq_TestSetup.testProductConfigurations =
            new List<Apttus_Config2__ProductConfiguration__c>
            {
                new Apttus_Config2__ProductConfiguration__c(
                    Apttus_QPConfig__Proposald__c = cpq_TestSetup.testProposals[0].ID,
                    Apttus_Config2__PriceListId__c = testPriceList.ID,
                    Apttus_Config2__Status__c = 'Saved',
                    Apttus_Config2__VersionNumber__c = 1)
            };
        insert cpq_TestSetup.testProductConfigurations;
        
DEBUG.debugQueries();

        cpq_TestSetup.testSummaryGroup =
            new Apttus_Config2__SummaryGroup__c(
                Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                Apttus_Config2__LineType__c = 'Category Total', Apttus_Config2__ItemSequence__c = 1,
                Apttus_Config2__LineNumber__c = 1,
                Apttus_Config2__NetPrice__c = 100);
        insert cpq_TestSetup.testSummaryGroup;

        cpq_TestSetup.testTotaling = new Apttus_Config2__AdHocGroup__c(Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID);
        insert cpq_TestSetup.testTotaling;

        cpq_TestSetup.testLine = 
            new Apttus_Config2__LineItem__c(
                Apttus_Config2__IsPrimaryLine__c = true,
                Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                Apttus_Config2__ProductId__c = cpq_TestSetup.testProduct.ID,
                Apttus_Config2__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[0].Id,
                Apttus_Config2__ItemSequence__c = 1,
                Apttus_Config2__LineNumber__c = 1,
                Apttus_Config2__PrimaryLineNumber__c = 1,
                Apttus_Config2__SummaryGroupId__c = cpq_TestSetup.testSummaryGroup.ID,
                Apttus_Config2__AdHocGroupId__c = cpq_TestSetup.testTotaling.ID); 
        insert cpq_TestSetup.testLine;
        
DEBUG.debugQueries();

        cpq_TestSetup.testProposalSummaryGroup =
            new Apttus_QPConfig__ProposalSummaryGroup__c(
                Apttus_QPConfig__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                Apttus_QPConfig__ProposalId__c = cpq_TestSetup.testProposals[0].ID, Apttus_QPConfig__ItemSequence__c = 1, Apttus_QPConfig__LineNumber__c = 1);
        insert cpq_TestSetup.testProposalSummaryGroup;

        cpq_TestSetup.testProposalLine = 
            new Apttus_Proposal__Proposal_Line_Item__c(
                Apttus_QPConfig__IsPrimaryLine__c = true,
                Apttus_Proposal__Proposal__c = cpq_TestSetup.testProposals[0].ID,
                Apttus_QPConfig__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                Apttus_Proposal__Product__c = cpq_TestSetup.testProduct.ID,
                Apttus_QPConfig__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[0].Id,
                Apttus_QPConfig__ItemSequence__c = 1,
                Apttus_QPConfig__LineNumber__c = 1,
                Apttus_QPConfig__DerivedFromId__c = cpq_TestSetup.testLine.ID,
                Apttus_QPConfig__SummaryGroupId__c = cpq_TestSetup.testProposalSummaryGroup.ID);
        insert cpq_TestSetup.testProposalLine;

DEBUG.debugQueries();
 
        cpq_TestSetup.testProposalSummary =
            new Apttus_QPConfig__ProposalSummary__c(
                Apttus_QPConfig__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                Apttus_QPConfig__ProposalId__c = cpq_TestSetup.testProposals[0].ID,
                Apttus_QPConfig__PriceListId__c = testPriceList.ID,
                Apttus_QPConfig__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[0].Id,
                Apttus_QPConfig__ProductId__c = cpq_TestSetup.testProduct.ID,
                Apttus_QPConfig__DerivedFromId__c = cpq_TestSetup.testLine.ID,
                Apttus_QPConfig__LineItemId__c = cpq_TestSetup.testProposalLine.ID,
                Apttus_QPConfig__SummaryGroupId__c = cpq_TestSetup.testProposalSummaryGroup.ID,
                Apttus_QPConfig__ItemSequence__c = 1,
                Apttus_QPConfig__LineNumber__c = 1);
        insert cpq_TestSetup.testProposalSummary;

        cpq_TestSetup.testFacility = new Participating_Facility__c(Proposal__c = cpq_TestSetup.testProposals[0].ID, Account__c = cpq_TestSetup.testAccounts[1].ID);
        insert cpq_TestSetup.testFacility;

DEBUG.debugQueries();

        cpq_TestSetup.testDistributor = new Proposal_Distributor__c(Proposal__c = cpq_TestSetup.testProposals[0].ID, Account__c = cpq_TestSetup.testAccounts[2].ID);
        insert cpq_TestSetup.testDistributor;

        Map<String,Id> mapRebateRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Rebate__c.class).values()) {
            mapRebateRType.Put(R.DeveloperName, R.Id);
        }

DEBUG.debugQueries();

        cpq_TestSetup.testRebate = new Rebate__c(Related_Proposal__c = cpq_TestSetup.testProposals[0].ID, RecordTypeID = mapRebateRType.get('Early_Signing_Incentive'));
        insert cpq_TestSetup.testRebate;

DEBUG.debugQueries();

System.debug('End createTestData');
DEBUG.debugQueries();

    }

    static testMethod void TestClone() {
        createTestData();

        Test.startTest();
            PageReference pageRef = Page.CPQ_GenerateProposal;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_GenerateProposal p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            p.doCancel();

            //no config
            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[1].ID);
            p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            boolean disbleButton = p.disableButton;

            //Renew No Agreement
            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[2].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'renew');
            p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            System.Debug(p.ObjectType);
            System.Debug(p.SourceTitle);
            System.Debug(p.SourcePurpose);

            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[3].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'amend');
            p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[3]));

            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[4].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'amend');
            p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[4]));

            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[0].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'clone');
            p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            //no opp
            p.doGenerate();

            p.theRecord.Apttus_Proposal__Opportunity__c = cpq_TestSetup.testOpp.ID;
            p.doGenerate();

            Test.stopTest();
    }

    static testMethod void TestRenew() {
        createTestData();

        Test.startTest();
            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[0].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'renew');
            CPQ_Controller_GenerateProposal p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            //no opp
            p.doGenerate();

            System.Debug(p.OpportunityOptions);
            p.SelectedOpportunity = cpq_TestSetup.testOpp.ID;
            p.doGenerate();
        Test.stopTest();
    }

    static testMethod void TestAmendment() {
        createTestData();

        Test.startTest();
            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[0].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'amend');
            CPQ_Controller_GenerateProposal p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            p.SelectedOpportunity = cpq_TestSetup.testOpp.ID;
            p.doGenerate();

            p.doCancel();
        Test.stopTest();
    }

    static testMethod void TestAmendmentPricing() {
        createTestData();

        Test.startTest();
            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[0].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'amendpricing');
            CPQ_Controller_GenerateProposal p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));
            p.SelectedOpportunity = cpq_TestSetup.testOpp.ID;
            p.doGenerate();
        Test.stopTest();
    }

    static testMethod void TestAmendmentFacility() {

        System.debug('Start TestAmendmentFacility');
        System.debug('SOQL Queries: ' + Limits.getQueries());

        createTestData();

        Test.startTest();

            System.debug('TestAmendmentFacility.startTest()');
            System.debug('SOQL Queries: ' + Limits.getQueries());

            ApexPages.currentPage().getParameters().put('proposal', cpq_TestSetup.testProposals[0].ID);
            ApexPages.currentPage().getParameters().put('purpose', 'amendfacility');
            CPQ_Controller_GenerateProposal p = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(cpq_TestSetup.testProposals[0]));

            System.debug('SOQL Queries after constructor = ' + Limits.getQueries());
            p.SelectedOpportunity = cpq_TestSetup.testOpp.ID;
            p.doGenerate();

            System.debug('SOQL Queries after doGenerate = ' + Limits.getQueries());

        Test.stopTest();
    }

    static testMethod void TestBeforeGenerate_renew()
    {
        createTestData();
        Apttus__APTS_Agreement__c a = [SELECT Id, RecordType.DeveloperName FROM Apttus__APTS_Agreement__c ORDER BY Id DESC LIMIT 1];
        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c();
        Apttus_Config2__ProductConfiguration__c c = new Apttus_Config2__ProductConfiguration__c();
        List<Apttus_Config2__LineItem__c> li = new List<Apttus_Config2__LineItem__c>();

        ApexPages.currentPage().getParameters().put('purpose', 'renew');

        List<Apttus__APTS_Agreement__c> records =
            [SELECT ID, Name, RecordTypeID, RecordType.Name,
                    RecordType.DeveloperName, Apttus__Related_Opportunity__c,
                    Apttus__Account__c, Apttus_QPComply__RelatedProposalId__c,
                    Apttus_QPComply__RelatedProposalId__r.Amendment_Sequence__c,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordTypeId,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.Name,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                    AgreementId__c
             FROM Apttus__APTS_Agreement__c];

        system.debug('TestBeforeGenerate: ' + a);

        CPQ_Controller_GenerateProposal gp = new CPQ_Controller_GenerateProposal();
        boolean isvalid = gp.validQueryString;
        gp.inputAgreement = records[0];

        gp.BeforeGenerate(
            a,
            p,
            c,
            li
            );
    }

    static testMethod void TestBeforeGenerate_amendpricing()
    {
        createTestData();
        Apttus__APTS_Agreement__c a = [SELECT Id, RecordType.DeveloperName FROM Apttus__APTS_Agreement__c ORDER BY Id DESC LIMIT 1];
        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c();
        Apttus_Config2__ProductConfiguration__c c = new Apttus_Config2__ProductConfiguration__c();
        List<Apttus_Config2__LineItem__c> li = new List<Apttus_Config2__LineItem__c>();

        ApexPages.currentPage().getParameters().put('purpose', 'amendpricing');

        List<Apttus__APTS_Agreement__c> records =
            [SELECT ID, Name, RecordTypeID, RecordType.Name,
                    RecordType.DeveloperName, Apttus__Related_Opportunity__c,
                    Apttus__Account__c, Apttus_QPComply__RelatedProposalId__c,
                    Apttus_QPComply__RelatedProposalId__r.Amendment_Sequence__c,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordTypeId,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.Name,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                    AgreementId__c
             FROM Apttus__APTS_Agreement__c];

        system.debug('TestBeforeGenerate: ' + a);

        CPQ_Controller_GenerateProposal gp = new CPQ_Controller_GenerateProposal();
        boolean isvalid = gp.validQueryString;
        gp.inputAgreement = records[0];

        gp.BeforeGenerate(
            a,
            p,
            c,
            li
            );
    }

    @isTest
    static void debugOn()
    {
        CPQ_Controller_GenerateProposal gp = new CPQ_Controller_GenerateProposal();
        boolean result = gp.debugOn;
    }

    @isTest
    static void URL()
    {
        CPQ_Controller_GenerateProposal gp = new CPQ_Controller_GenerateProposal();
        string result = gp.URL;
    }

    @isTest
    static void Scope()
    {
        CPQ_Controller_GenerateProposal gp = new CPQ_Controller_GenerateProposal();
        CPQ_Controller_GenerateProposal.eScope result = gp.Scope;

        ApexPages.currentPage().getParameters().put('scope', 'full');
        gp = new CPQ_Controller_GenerateProposal();
        boolean isvalid = gp.validQueryString;
        result = gp.Scope;

        boolean isFull = gp.isFullScope;
    }

    @isTest
    static void isAgreement()
    {
        System.debug('in Test_CPQ_Controller_GenerateProposal.isAgreement()');
        createTestData();
        System.debug('queries after createTestData()');
        Apttus__APTS_Agreement__c a = [SELECT Id, RecordType.DeveloperName FROM Apttus__APTS_Agreement__c ORDER BY Id DESC LIMIT 1];
        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c();
        Apttus_Config2__ProductConfiguration__c c = new Apttus_Config2__ProductConfiguration__c();
        List<Apttus_Config2__LineItem__c> li = new List<Apttus_Config2__LineItem__c>();

        List<Apttus__APTS_Agreement__c> records =
            [SELECT ID, Name, RecordTypeID, RecordType.Name,
                    RecordType.DeveloperName, Apttus__Related_Opportunity__c,
                    Apttus__Account__c, Apttus_QPComply__RelatedProposalId__c,
                    Apttus_QPComply__RelatedProposalId__r.Amendment_Sequence__c,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordTypeId,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.Name,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                    AgreementId__c
             FROM Apttus__APTS_Agreement__c];

        ApexPages.currentPage().getParameters().put('agreement', records[0].Id);
        ApexPages.currentPage().getParameters().put('scope', 'full');

        System.debug('queries before instantiating CPQ_Controller_GenerateProposal');
        CPQ_Controller_GenerateProposal gp = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(p));

        test.startTest();
            gp.doGenerate();
        test.stopTest();
    }

    @isTest
    static void addMessage()
    {
        createTestData();

        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c();

        ApexPages.currentPage().getParameters().put('agreement', '000000000000000');

        CPQ_Controller_GenerateProposal gp
            = new CPQ_Controller_GenerateProposal(new ApexPages.StandardController(p));
        gp.doGenerate();
    }
}