/****************************************************************************************************************************************
Author Name    Date         Change Made
Amogh Ghodke  8 July 2016   Added getprofName method to verify logged in user's profile and currency
*****************************************************************************************************************************************
*/

public class AsiaAffiliatedSitesController{
  public String profileOfUser{get;set;}
  public String currencyOfUser{get;set;}   
    
  public Integer getprofName(){ 
    
     integer x = 1;
     profileOfUser = [SELECT Id,Profile.name from User where Id=:UserInfo.getUserId()].Profile.name;
     currencyOfUser= [SELECT Id,LocaleSidKey from User where Id=:UserInfo.getUserId()].LocaleSidKey;
     
     if(profileOfUser == 'Asia - HK / SG' && (currencyOfUser == 'en_SG'||currencyOfUser == 'zh_SG'))
         x = 1;
     else if(profileOfUser == 'Asia - HK / SG' && currencyOfUser == 'zh_HK')
         x = 5;
     else if(profileOfUser == 'Asia - TW' && (currencyOfUser == 'zh_TW_STROKE'||currencyOfUser == 'zh_TW'))
         x = 8;
     else if(currencyOfUser == 'en_ID')
        x = 18;
     else if(currencyOfUser  == 'en_MY' || currencyOfUser  == 'en_IN' || currencyOfUser == 'vi_VN') 
        x = 99;
     else if(profileOfUser == 'Asia - PH' && currencyOfUser == 'en_PH') 
        x = 25;
     else if(profileOfUser == 'ASIA - TH')   
        x =  22; 
      
    return x;  
     
     
    } 
 }