/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed  80 => *                            120 => *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry

YYYYMMDD-A  PAB         CPR-000 Updated comments section

7.  Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com
Gautam Shah             GS              Medtronic.com

MODIFICATION HISTORY
====================
Date-Id     Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
Sep 7, 2017-A   PAB                 Created at 2:28:18 PM by paul.berglund

*/
public class cpqSalesHistoryConstants
{
    public static final string
        arrayURL = 'http://schemas.microsoft.com/2003/10/Serialization/Arrays',
        contractURL = 'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Sale',
        endpointURL = cpqVariable_c.getValue('Proxy EndPoint')
    ;
}