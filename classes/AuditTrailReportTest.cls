@isTest
private class AuditTrailReportTest {
    static testmethod void test() {
        Test.startTest();
        AuditTrailReportSchedular a = new AuditTrailReportSchedular();
        String sch = '0 0 23 * * ?'; 
        system.schedule('AuditTrailReport', sch, a);
        
        AuditTrailReport objAuditTrailReport = new AuditTrailReport();
        objAuditTrailReport.init();
        Test.stopTest();
   }
}