global with sharing class Activities_Mobile_US {
 
 /****************************************************************************************
   * Name    : Activities_Mobile_US
   * Author  : Shawn Clark
   * Date    : 03/23/2016 
   * Purpose : Custom Activities Interface for mobile
 *****************************************************************************************/
    public Event e {get; private set;}
    public String ReccType {get;set;}
    public transient List<SelectOption> sol {get;set;}          //Holds RecordType Picklist Values
    public List<ApexPickListValLookup__c> apexpick {get; set;}  //Holds ActivityType Picklist Values based on Country
    public List <Contact> acc {get;set;}              //Holds Contacts in Lookup Window
    public List <Opportunity> opp {get;set;}          //Holds Opportunities in Lookup Window
    public List <Account> act {get;set;}             //Holds Accounts in Lookup Window
    public Boolean IsRecTypeDisabled {get;set;}
    public String Subject {get;set;}
    public String ActivityType {get;set;}
    public String CurrentUser {get; set;} 
    public String UserCountry {get; set;}  //Used by Autocomplete
    public String WhichAccounts {get; set;}  
    
    public transient Integer Size {get; set;}
    public transient Integer OppSize {get; set;}
    public transient Integer ActSize {get; set;}
    public Boolean LockAccountSelection {get; set;}
    //public Boolean ShowAsiaAccountSearch  {get; set;}
    public String NewEvent{get; set;}
    public Boolean DisableContactLookup {get; set;}
    public Boolean DisableOpportunityLookup {get; set;}
   //public Boolean ShowKoreaOnlyFields {get; set;}
    public transient String searchTerm {get; set;}  //Used by Autocomplete
    public transient String searchquery {get; set;}
    public transient String oppsearchquery {get; set;}
    public Integer IndexNum {get;set;}
    public Integer OppIndexNum {get;set;}
    public Integer AccIndexNum {get;set;}
    public Integer PageLoadCount {get;set;}
    
    //Account Variables
    public String selectedAccountGuidId {get; set;} 
    public String selectedAccountName {get; set;} 
    public String selectedAccountState {get; set;} 
    public String selectedAccountCity {get; set;} 
    public String selectedExternalAccountId {get; set;} 

    public String selectedNotes {get; set;}

    //Contact Variables
    public String selectedContactId {get; set;}
    public String selectedContactName {get; set;}
    
    //Opportunity Variables
    public String selectedOppId {get; set;}
    public String selectedOppName {get; set;}
    Public String OppStageFilter {get;set;}

    //Date Time Math & Parsing
    public transient Datetime OriginalDateTime {get;set;}
    public transient Datetime UpdatedDateTime {get;set;}
    public transient Integer currTimeMinute {get;set;}
    public transient Integer currTimeSecond {get;set;}
    public transient String currTimeMinuteStr {get;set;}
    public transient String currTimeMinuteRightOne {get;set;}
    
    public Date currStartDate {get;set;}
    public Time currStartTime {get;set;}  
    public Date currEndDate {get;set;}
    public Time currEndTime {get;set;}
    
    public Datetime StartDate {get;set;}
    public Datetime EndDate {get;set;}


    // SOQL query loads the case, with related Sample & Sample Product Data
    public Activities_Mobile_US () {
    
    
    
    //Get Users Country
    CurrentUser = userinfo.getuserid();
    UserCountry = [select Country from user where id=:userinfo.getuserid()].Country;
    apexpick = [SELECT PickListVal__c FROM ApexPickListValLookup__c WHERE Country__c = :UserCountry];
    //Default - Allow user to select account
    LockAccountSelection     = false;
    DisableContactLookup     = true;
    DisableOpportunityLookup = true;


    
    
    //Only Set the Datetime on Initial Load, dont want to override user modifications
    IF(PageLoadCount==null) 
    {  
    WhichAccounts = 'MyAccounts';
       OriginalDateTime = System.now(); // returns date time value in GMT time zone.

       //Zero out seconds
       currTimeSecond = OriginalDateTime.Second();
       currTimeSecond = currTimeSecond*-1; 
       OriginalDateTime = OriginalDateTime.addSeconds(currTimeSecond);

       //Identify What the current Minute is
       currTimeMinute = OriginalDateTime.Minute();
       currTimeMinuteStr = String.ValueOf(currTimeMinute);
       currTimeMinuteRightOne = currTimeMinuteStr.Right(1);

       //Round Time Down to Nearest 5 minutes
       IF (currTimeMinuteRightOne == '1' || currTimeMinuteRightOne == '6') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-1);
       }
       ELSE IF (currTimeMinuteRightOne == '2' || currTimeMinuteRightOne == '7') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-2);
       }
       ELSE IF (currTimeMinuteRightOne == '3' || currTimeMinuteRightOne == '8') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-3);
       }
       ELSE IF (currTimeMinuteRightOne == '4' || currTimeMinuteRightOne == '9') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-4);
       }
       ELSE 
       {
          UpdatedDateTime = OriginalDateTime;
       }
   
       //Now Set the Current End Date & Time
       currEndDate = UpdatedDateTime.date();
       currEndTime = UpdatedDateTime.time();
       EndDate = datetime.newinstance(currEndDate,currEndTime);
            
       //Start Date is 0.5 Hours before End
       UpdatedDateTime = UpdatedDateTime.addMinutes(-30);
       currStartDate = UpdatedDateTime.date();
       currStartTime = UpdatedDateTime.time();
       StartDate = datetime.newinstance(currStartDate,currStartTime);
    
       //If there is only One Record Type, Set it, and Disable the field    
       getOpportunityRecTypes();
       IF (sol.size() < 2) 
       {
          IsRecTypeDisabled = true;
          ReccType = sol[0].getValue();
       }
       Else 
       {
          IsRecTypeDisabled = false;
       }
    }
    //Otherwise just add a PageLoad Count and don't recalc the date/time
    ELSE 
    {
       PageLoadCount = PageLoadCount+1;
    }  
   }
           



    //Get AccountsType by Country
    public list<SelectOption> getActivityTypeList() {
       list<SelectOption> options = new list<SelectOption>();
       options.add(new SelectOption('','-- Select Activity Type --')); 
              
       //If the User belongs to a Country that has Picklist Values Defined, then use them
       if(String.IsEmpty(UserCountry) || apexpick.size() == 0)
       {
       Schema.DescribeFieldResult fieldResult = Event.Activity_Type__c.getDescribe(); 
       List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
          for(Schema.picklistEntry f:ple)    
          {    
          options.add(new selectOption(f.getLabel(),f.getValue()));                  
          }    
          return Options; 
       } 
       Else  
       {  
       for (list<ApexPickListValLookup__c> rts : [SELECT PickListVal__c FROM ApexPickListValLookup__c WHERE Country__c = :UserCountry ORDER BY PickListVal__c]) 
          {
          for (ApexPickListValLookup__c rt : rts) 
             {
             options.add(new SelectOption(rt.PickListVal__c, rt.PickListVal__c));
             } 
          } 
          return options;
       }  
    } 


    //Get Opportunity Stages
        public List<SelectOption> getOppStage() {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- Show All ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
           for(Schema.picklistEntry f:ple)    
           {    
               options.add(new selectOption(f.getLabel(),f.getValue()));                  
           }    
           return Options;    
    }  

    


    //Get Record Types for the Current User
    public List<SelectOption> getOpportunityRecTypes() {
    List <RecordType> rl;
    sol = new List<SelectOption>();
    rl = [select Id, Name from RecordType where sObjectType='Event' and isActive=true];
       Schema.DescribeSObjectResult d = Schema.SObjectType.Event;
       Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
       
        for (RecordType r : rl) {
                if(rtMapById.get(r.id).isAvailable()){
                    sol.add(new SelectOption(r.id,r.Name));
                }
         }
           return sol;
        }  
  
   //Search Contact
   
      public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('MyAccounts','My Accounts')); 
        options.add(new SelectOption('AllAccounts','All Accounts')); 
        return options; 
    }
   
    public void EndDateCheck(){  
    IF (currEndDate < currStartDate) 
       {
       currStartDate = currEndDate;
       }
    }
    
    public void StartDateCheck(){  
    IF (currStartDate > currEndDate) 
       {
       currEndDate = currStartDate;
       }
    }
    
    public List<SelectOption> getStates() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select a State --'));  
        options.add(new SelectOption('AK', 'AK'));
        options.add(new SelectOption('AL', 'AL'));
        options.add(new SelectOption('AR', 'AR'));
        options.add(new SelectOption('AZ', 'AZ'));
        options.add(new SelectOption('CA', 'CA'));
        options.add(new SelectOption('CO', 'CO'));
        options.add(new SelectOption('CT', 'CT'));
        options.add(new SelectOption('DC', 'DC'));
        options.add(new SelectOption('DE', 'DE'));
        options.add(new SelectOption('FL', 'FL'));
        options.add(new SelectOption('GA', 'GA'));
        options.add(new SelectOption('HI', 'HI'));
        options.add(new SelectOption('IA', 'IA'));
        options.add(new SelectOption('ID', 'ID'));
        options.add(new SelectOption('IL', 'IL'));
        options.add(new SelectOption('IN', 'IN'));
        options.add(new SelectOption('KS', 'KS'));
        options.add(new SelectOption('KY', 'KY'));
        options.add(new SelectOption('LA', 'LA'));
        options.add(new SelectOption('MA', 'MA'));
        options.add(new SelectOption('MD', 'MD'));
        options.add(new SelectOption('ME', 'ME'));
        options.add(new SelectOption('MI', 'MI'));
        options.add(new SelectOption('MN', 'MN'));
        options.add(new SelectOption('MO', 'MO'));
        options.add(new SelectOption('MS', 'MS'));
        options.add(new SelectOption('MT', 'MT'));
        options.add(new SelectOption('NC', 'NC'));
        options.add(new SelectOption('ND', 'ND'));
        options.add(new SelectOption('NE', 'NE'));
        options.add(new SelectOption('NH', 'NH'));
        options.add(new SelectOption('NJ', 'NJ'));
        options.add(new SelectOption('NM', 'NM'));
        options.add(new SelectOption('NV', 'NV'));
        options.add(new SelectOption('NY', 'NY'));
        options.add(new SelectOption('OH', 'OH'));
        options.add(new SelectOption('OK', 'OK'));
        options.add(new SelectOption('OR', 'OR'));
        options.add(new SelectOption('PA', 'PA'));
        options.add(new SelectOption('RI', 'RI'));
        options.add(new SelectOption('SC', 'SC'));
        options.add(new SelectOption('SD', 'SD'));
        options.add(new SelectOption('TN', 'TN'));
        options.add(new SelectOption('TX', 'TX'));
        options.add(new SelectOption('UT', 'UT'));
        options.add(new SelectOption('VA', 'VA'));
        options.add(new SelectOption('VT', 'VT'));
        options.add(new SelectOption('WA', 'WA'));
        options.add(new SelectOption('WI', 'WI'));
        options.add(new SelectOption('WV', 'WV'));
        options.add(new SelectOption('WY', 'WY'));
        return options;
    } 
        
    
    //Search Account
    public PageReference searchAccount(){  
    selectedAccountName = selectedAccountName;
    IF (String.isEmpty(selectedAccountName)) 
    {
    system.debug('Account Guid For Yale is: ' + selectedAccountName);
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter an Account Name!'));
    return null;
    }
    Else
    {

    Set<String> GroupIds = new set<String>();
            
    // Get all RelatedIds from Group object for Groups aligned to Account
            
            for(Group at : [Select Id From Group 
                            where Type = 'Territory' and RelatedId in 
                            (Select TerritoryId From UserTerritory 
                             Where UserId = :CurrentUser
                             and IsActive = true)])
            {
                GroupIds.add(at.Id);
                
            }
            

     //Get a distinct set of accounts
            set<Id> MyAccounts = new set<Id>();
            
            for(AccountShare actsh :[Select AccountId From AccountShare 
                                     Where RowCause in ('Territory', 'TerritoryManual') 
                                     and UserOrGroupId IN :GroupIds])
            {
                MyAccounts.add(actsh.AccountId);
            }       
            
    String TheCountry = 'US';

    searchquery = 'SELECT Name, Id, Account_External_ID__c, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE BillingCountry = :TheCountry AND Name like \'%' + String.escapeSingleQuotes(selectedAccountName ) + '%\'';
    
    //If State is selected Add Billing State Filter
    if (!String.isEmpty(selectedAccountState)) 
    
    searchquery += ' AND BillingState = :selectedAccountState';
    
    //If City is entered Add Billing City Filter
    if (!String.isEmpty(selectedAccountCity)) 
    
    searchquery += ' AND BillingCity like \'%' + String.escapeSingleQuotes(selectedAccountCity) + '%\'';
    
    //If External Account ID is Entered Add  Filter
    if (!String.isEmpty(selectedExternalAccountId)) 
    
    searchquery += ' AND Account_External_ID__c like \'%' + String.escapeSingleQuotes(selectedExternalAccountId) + '%\'';
    
    //If My Accounts is selected Add Filter for only Account in Reps Territory
    if ( WhichAccounts== 'MyAccounts')
        searchquery += ' AND ID IN :MyAccounts';
        
    //Always add sort order    
    searchquery += ' order by Name ASC';
     system.debug('My Search Query is: ' + searchquery);
    act = Database.query(searchquery); 
    ActSize = act.size();    
    IF (Size == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }                       
    PageReference pageRef= new PageReference('/apex/Activities_AccountLookup_US');
    pageRef.setredirect(false);  
    return pageRef;   
    } 
    } 
    
    
  
    //Search Contact
    public PageReference searchMain(){  
    selectedAccountGuidId = selectedAccountGuidId;
    IF (String.isEmpty(selectedAccountGuidId)) 
    {
    system.debug('Account Guid For Yale is: ' + selectedAccountGuidId);
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You cannot lookup a contact, until you select an account'));
    return null;
    }
    Else
    {
    system.debug('Account Guid For Yale is: ' + selectedAccountGuidId);
    searchquery = 'select Salutation, firstname, lastname, name, id, AccountID, Contact.Account.Name, Personal_Email__c, ' +
                   'MobilePhone, Phone_Number_at_Account__c, Department_picklist__c, Other_Department__c, ' +
                   'Affiliated_Role__c, Title, Specialty1__c ' +
                   'from contact where AccountId = :selectedAccountGuidId and name like \'%' + String.escapeSingleQuotes(selectedContactName ) + '%\' order by Name ASC LIMIT 100';
       
    acc= Database.query(searchquery); 
    Size = acc.size();    
    IF (Size == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }                       
    PageReference pageRef= new PageReference('/apex/ContactLookUp_US');
    pageRef.setredirect(false);  
    return pageRef;   
    } 
    }
   
    //Search Opportunities  
    public PageReference searchOpp(){  
    selectedAccountGuidId = selectedAccountGuidId;
        IF (String.isEmpty(selectedAccountGuidId)) 
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You cannot lookup a opportunity, until you select an account'));
    return null;
    }
    Else
    {
    OppStageFilter = OppStageFilter;
    selectedOppName = selectedOppName;
    system.debug('Account Guid For Yale is: ' + selectedAccountGuidId);
    oppsearchquery = 'select Name, StageName, Type, id, AccountID, Opportunity.Account.Name, Amount ' +
                   'from opportunity where AccountId = :selectedAccountGuidId and name like \'%' + String.escapeSingleQuotes(selectedOppName ) + '%\' order by Name ASC LIMIT 100';
       
    //Dynamically Append additional conditions to the base searchquery if fields are populated
    if (!String.isEmpty(OppStageFilter))
        oppsearchquery += ' and StageName = :OppStageFilter';
       
    opp = Database.query(oppsearchquery); 
    OppSize = opp.size();  
    IF (OppSize == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }                          
    PageReference pageRef= new PageReference('/apex/Activities_OpportunityLookup_US');
    pageRef.setredirect(false);  
    return pageRef;   
    }
    }    
  
   //Search Opportunities No Redir - Allows user to search again from Opp Popup without being redirected
   public void searchOppNoRedir(){  
   selectedAccountGuidId = selectedAccountGuidId;
   OppStageFilter = OppStageFilter;
   selectedOppName = selectedOppName;
   oppsearchquery = 'select Name, StageName, Type, id, AccountID, Opportunity.Account.Name, Amount ' +
                   'from opportunity where AccountId = :selectedAccountGuidId and name like \'%' + String.escapeSingleQuotes(selectedOppName ) + '%\'';
       
   //Dynamically Append additional conditions to the base searchquery if fields are populated
   if (!String.isEmpty(OppStageFilter))
   {
        oppsearchquery += ' and StageName = :OppStageFilter order by Name ASC LIMIT 100';
   }
   else 
   {
       oppsearchquery += ' order by Name ASC LIMIT 100';
   }
   
   opp = Database.query(oppsearchquery); 
   OppSize = opp.size();
   IF (OppSize == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }    
   }
    

   //Back to Main Form 
   public PageReference ReviseSearch(){  
     PageReference pageRef= new PageReference('/apex/Activities_Mobile_US');
     pageRef.setredirect(false);       
     return pageRef; 
     }   
     
   //Used by Javascript to refresh PageMessages
   public PageReference Refresh(){   
     return null; 
     }   
     
   //Capture Account Index Number Selection  
   public PageReference processActSelection() {
   selectedAccountGuidId = act[AccIndexNum].Id; 
   selectedAccountName = act[AccIndexNum].Name; 
   PageReference pageRef= new PageReference('/apex/Activities_Mobile_US');
   pageRef.setredirect(false);  
   return pageRef; 
   } 
   
   //Capture Contact Index Number Selection  
   public PageReference processButtonClick() {
   LockAccountSelection = true;
   selectedContactId = acc[IndexNum].Id; //This is actually Contact Guid 
   selectedContactName = acc[IndexNum].Name; //This is actually Contact Guid
   PageReference pageRef= new PageReference('/apex/Activities_Mobile_US');
   pageRef.setredirect(false);  
   return pageRef; 
   } 
    
   //Capture Opportunity Index Number Selection  
   public PageReference processOppSelection() {
   selectedOppId = opp[OppIndexNum].Id; 
   selectedOppName = opp[OppIndexNum].Name; 
   PageReference pageRef= new PageReference('/apex/Activities_Mobile_US');
   pageRef.setredirect(false);  
   return pageRef; 
   } 
   


   //Return Validation Error
   public PageReference ValidationMsgReturn() 
   {
   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please fill out all required (*) fields'));
   return null;
   }

   //Return Validation Error2
   public PageReference ValidationMsgReturn2() 
   {
   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Start Time must be earlier than End Time!'));
   return null;
   }


   //Create Event
   public PageReference CreateEvent() {
   
   System.debug('Activity: ' + ActivityType + ' Contact: ' + selectedContactId + ' Account: ' + selectedAccountGuidId + ' Country: ' + UserCountry);
   
   StartDate = datetime.newinstance(currStartDate,currStartTime);
   EndDate = datetime.newinstance(currEndDate,currEndTime);
   Event e = new Event();
   e.RecordTypeId = ReccType;
   e.Subject = Subject;
   e.Activity_Type__c = ActivityType;
   e.Created_via_Salesforce1__c = true;
   e.ActivityDateTime = StartDate;
   e.StartDateTime = StartDate;
   e.EndDateTime = EndDate;
   
   //WhoIS cannot be passed if blank/null, but is not required
      IF (String.isNotEmpty(selectedContactId)) 
   e.WhoId = selectedContactId;
      
   //WhatID cannot be passed if blank/null, but is not required
      IF (String.isNotEmpty(selectedOppId)) 
      {
      e.WhatId = selectedOppId; 
      }
      ELSE IF (String.isEmpty(selectedOppId) && String.isNotEmpty(selectedAccountGuidId)) 
      {
      e.WhatId = selectedAccountGuidId; 
      }  
      
   e.Description = selectedNotes;
      try{ 
         insert e; 
         NewEvent = e.Id;
         System.debug('Event was sucessfully Created: ' + NewEvent);
         } 
         catch (Exception ee) 
         {}
         return null;
         
     }
     
   }