/**
Functions that operate on both CPQ Proposal and Agreement records.

CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
10/10/2016  Paul Berglund   Created
10/13/2016  Isaac Lewis     Moved getSystemProperties logic from CPQ_Utilities
                            to cpq_u
10/24/2016  Isaac Lewis     Moved all logic from CPQ_Utilities to cpq_u
10/25/2016  Paul Berglund   Moved Pricebook2 related logic to Pricebook2_c
11/07/2016  Isaac Lewis     Added getSObjectTypeFromSObjectName to improve Schema.getGlobalDescribe references
01/20/2017  Isaac Lewis     Added isValidId method, to separate regular strings from ID strings
01/24/2017  Isaac Lewis     Moved `buildObjectQuery` to SOQL_select
===============================================================================
*/

public class cpq_u
{
	///////////////////////////////////////////////////////////////////////////////
	/// CONSTANTS
	///////////////////////////////////////////////////////////////////////////////

    public static FINAL String AGREEMENT_DEV_NAME = 'Apttus__APTS_Agreement__c';
    public static FINAL Schema.sObjectType AGREEMENT_SOBJECT_TYPE = Apttus__APTS_Agreement__c.SObjectType;
    public static FINAL Type AGREEMENT_TYPE = Type.forName(AGREEMENT_DEV_NAME);
    public static FINAL String PROPOSAL_DEV_NAME = 'Apttus_Proposal__Proposal__c';
    public static FINAL Schema.sObjectType PROPOSAL_SOBJECT_TYPE = Apttus_Proposal__Proposal__c.SObjectType;
    public static FINAL Type PROPOSAL_TYPE = Type.forName(PROPOSAL_DEV_NAME);

    public static String getValidCRN (String customerRelationshipNumber) {
        try {
            String s = customerRelationshipNumber.substring(3);
            return String.valueOF(Integer.valueOf(s.trim()));
        } catch (Exception e) {
            return null;
        }
    }

    public static Boolean isLocalBusinessDay (DateTime localDt) {
        // returns true if given dateTime is a workday
        DateTime gmtDt = DateTime.newInstance(localDt.dateGMT(), localDt.timeGMT());
        //TODO: holidays
        String dayOfWeek = gmtDt.format('EEE');//format converts GMT to local
        if (dayOfWeek == 'Sat' || dayOfWeek == 'Sun') {
            // weekend
            return false;
        }
        return true;
    }

	private static Map<integer, string> remainder2fraction =
		new Map<integer, string>
		{
			3 => '1/4',
			6 => '1/2',
			9 => '3/4'
		};
		
	private static Map<string, integer> fraction2remainder =
		new Map<string, integer>();
		
	static
	{
		for(integer key : remainder2fraction.keySet())
			fraction2remainder.put(remainder2fraction.get(key), key);
	}

    public static String getDurationOption (Integer durationMonths) {
        // translates number of months to duration picklist value

        Integer quotient = Integer.valueOf(durationMonths/12);
        Integer remainder = Integer.valueOf(Math.mod(durationMonths, 12));

		string fraction ='';
		if (remainder2fraction.containsKey(remainder))
			fraction = remainder2fraction.get(remainder);
		
        String option;
        if (quotient == 1 && fraction == '')
            option = '1 year';
        else if (quotient == 0 && fraction == '')
            option = null;
        else if (quotient == 0 && fraction != '')
            option = fraction + ' years';
        else if (fraction == '')
            option = String.valueOf(quotient) + ' years';
        else
            option = String.valueOf(quotient) + ' ' + fraction + ' years';

        return option;
    }

    public static Integer getDurationMonths (String durationOption)
	{
        // translates duration picklist value to number of months
        if (durationOption == null)
            return NULL;

        else
        {
            String duration = durationOption.toUpperCase();

            integer y = duration.indexOf(' YEAR');
            if (y > 0)
            {
                String n = duration.substring(0, y);
                String[] a = n.split(' ');

                try
                {
                    Integer t = Integer.valueOf(a[0]);
                    Integer m = 0;
                    if (a.size() > 1)
                    {
                        String f = a[1].trim();
                        if (fraction2remainder.containsKey(f))
                        	m = fraction2remainder.get(f);
                    }
					return t * 12 + m;
                }
                catch (Exception e)
                {
                    String f = a[0].trim();
                    if (fraction2remainder.containsKey(f))
                    	return fraction2remainder.get(f);
                    else
                    	return null;
                }
            } 

            else
                return NULL;
        }
    }
}