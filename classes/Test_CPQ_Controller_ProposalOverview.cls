/*
Test Class

CPQ_Controller_ProposalOverview

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-09-14      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
private class Test_CPQ_Controller_ProposalOverview { 
    static List<Apttus_Proposal__Proposal__c> proposals;
    static void createTestData() {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;
        
        CPQ_Variable__c testVar = new CPQ_Variable__c(Name = 'RMS PriceList ID', Value__c = testPriceList.ID);
        insert testVar;

        Product2 testProduct = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.RMS, 'SENSOR', 1);
        testProduct.Hier_Level_2__c = 'QAB';
        insert testProduct;

        List<Apttus_Config2__PriceListItem__c> testPriceListItems =
            cpqPriceListItem_u.fetch(
                Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
                testPriceList.Id).values();

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values()) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        List<Apttus_Proposal__Proposal__c> testProposals = new List<Apttus_Proposal__Proposal__c>{
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Respiratory_Solutions_COOP')),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement'))
        };
        insert testProposals;

        List<Apttus_Proposal__Proposal_Line_Item__c> testLines = new List<Apttus_Proposal__Proposal_Line_Item__c> {
            new Apttus_Proposal__Proposal_Line_Item__c(Apttus_QPConfig__IsPrimaryLine__c = true, Apttus_Proposal__Proposal__c = testProposals[0].ID, Apttus_Proposal__Product__c = testProduct.ID, Apttus_QPConfig__ChargeType__c = 'Consumable'),
            new Apttus_Proposal__Proposal_Line_Item__c(Apttus_QPConfig__IsPrimaryLine__c = true, Apttus_Proposal__Proposal__c = testProposals[1].ID, Apttus_Proposal__Product__c = testProduct.ID, Apttus_QPConfig__ChargeType__c = 'Consumable')
        };
        insert testLines;

        Set<String> proposalFields = CPQ_Utilities.getCustomFieldNames(Apttus_Proposal__Proposal__c.SObjectType, true);
        Set<String> lineFields = CPQ_Utilities.getCustomFieldNames(Apttus_Proposal__Proposal_Line_Item__c.SObjectType, false);

        String proposalQueryStr = CPQ_Utilities.buildObjectQuery(proposalFields, 'RecordType.Name, RecordType.DeveloperName', 'ID in: testProposals', 'Apttus_Proposal__Proposal__c');
        String lineQueryStr = CPQ_Utilities.buildObjectQuery(lineFields, null, 'Apttus_Proposal__Proposal__c in: testProposals', 'Apttus_Proposal__Proposal_Line_Item__c');

        proposals = (List<Apttus_Proposal__Proposal__c>)Database.query(proposalQueryStr);
        List<Apttus_Proposal__Proposal_Line_Item__c> lines = (List<Apttus_Proposal__Proposal_Line_Item__c>)Database.query(lineQueryStr);
    }

    static testMethod void myUnitTest() {
        createTestData();

        Test.startTest();
        PageReference pageRef = Page.CPQ_ProposalOveriew;
        Test.setCurrentPage(pageRef);

        CPQ_Controller_ProposalOverview p1 = new CPQ_Controller_ProposalOverview(new ApexPages.StandardController(proposals[0]));
        CPQ_Controller_ProposalOverview p2 = new CPQ_Controller_ProposalOverview(new ApexPages.StandardController(proposals[1]));
        System.Debug(p2.QuoteLineItems);
        Test.stopTest();
    }
}