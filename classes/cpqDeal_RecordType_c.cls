public class cpqDeal_RecordType_c extends sObject_c
{
    public static Map<Id,RecordType>
    	recordTypesById { get; private set; }
    public static Map<String,RecordType>
    	recordTypesByName { get; private set; }
    public static Map<Schema.SObjectType,Map<Id,RecordType>>
    	recordTypesBySObjectAndId { get; private set; }
    public static Map<Schema.SObjectType,Map<String,RecordType>>
    	recordTypesBySObjectAndName { get; private set; }
    public static Map<OrganizationNames_g.Abbreviations, Map<String,RecordType>>
    	recordTypesByOrgAndName { get; private set; }

    public static FINAL String REGEX_SUFFIX_LOCKED = '\\s?(-|_)?\\s?Locked$';
    public static FINAL String REGEX_SUFFIX_AG = '\\s?(-|_)?\\s?AG$';

    // -----------------------------------------------------------------------------------------------------------------
    // Methods for working with record types

    public static Map<Id,RecordType> getRecordTypesById ()
    {
        if (recordTypesById == null)
        {
            recordTypesById = new Map<Id,RecordType>();
            recordTypesById.putAll(RecordType_u.fetch(Apttus__APTS_Agreement__c.class));
            recordTypesById.putAll(RecordType_u.fetch(Apttus_Proposal__Proposal__c.class));
        }
        // system.debug('recordTypesById: ' + recordTypesById);
        return recordTypesById;
    }

    public static Map<String,RecordType> getRecordTypesByName ()
    {
        if (recordTypesByName == null)
        {
            recordTypesByName = new Map<String,RecordType>();

            for(RecordType rt : getRecordTypesById().values())
            {
                recordTypesByName.put(rt.DeveloperName,rt);
            }
        }
        return recordTypesByName;
    }

    public static Map<Schema.SObjectType,Map<Id,RecordType>> getRecordTypesBySObjectAndId ()
    {
        if (recordTypesBySObjectAndId == null)
        {
            recordTypesBySObjectAndId = new Map<Schema.SObjectType,Map<Id,RecordType>>();

            for ( RecordType rt : getRecordTypesById().values() )
            {
                Schema.SObjectType soType = sObject_u.getSObjectType(rt.SObjectType);
                if (recordTypesBySObjectAndId.get(soType) == null)
                {
                    recordTypesBySObjectAndId.put(soType,new Map<Id,RecordType>());
                }
                recordTypesBySObjectAndId.get(soType).put(rt.Id,rt);
            }
        }
        return recordTypesBySObjectAndId;
    }

    public static Map<Schema.SObjectType,Map<String,RecordType>> getRecordTypesBySObjectAndName ()
    {
        if (recordTypesBySObjectAndName == null)
        {
            recordTypesBySObjectAndName = new Map<Schema.SObjectType,Map<String,RecordType>>();

            for ( RecordType rt : getRecordTypesById().values() )
            {
                Schema.SObjectType soType = sObject_u.getSObjectType(rt.SObjectType);

                if (recordTypesBySObjectAndName.get(soType) == null)
                {
                    recordTypesBySObjectAndName.put(soType,new Map<String,RecordType>());
                }

                recordTypesBySObjectAndName.get(soType).put(rt.DeveloperName,rt);
            }
        }
        return recordTypesBySObjectAndName;
    }

    public static Map<OrganizationNames_g.Abbreviations, Map<String,RecordType>> getRecordTypesByOrgAndName()
    {
        if (recordTypesByOrgAndName == null)
        {
            recordTypesByOrgAndName = new Map<OrganizationNames_g.Abbreviations, Map<String,RecordType>>();

            // Populate Orgs with Record Types
            for (RecordType rt : getRecordTypesById().values())
            {
                for (Integer i = 0; i < OrganizationNames_g.Abbreviations.values().size(); i++)
                {
                    OrganizationNames_g.Abbreviations org = OrganizationNames_g.Abbreviations.values()[i];

                    if (!recordTypesByOrgAndName.containsKey(org))
                        recordTypesByOrgAndName.put(org, new Map<String,RecordType>());

                    recordTypesByOrgAndName.get(org).put(rt.DeveloperName,rt);
                }
            }
        }
        return recordTypesByOrgAndName;
    }

    public static Map<Id,RecordType> getAgreementRecordTypesById(){
        return getRecordTypesBySObjectAndId().get(cpq_u.AGREEMENT_SOBJECT_TYPE);
    }

    public static Map<String,RecordType> getAgreementRecordTypesByName(){
        return getRecordTypesBySObjectAndName().get(cpq_u.AGREEMENT_SOBJECT_TYPE);
    }

    public static Map<Id,RecordType> getProposalRecordTypesById(){
        return getRecordTypesBySObjectAndId().get(cpq_u.PROPOSAL_SOBJECT_TYPE);
    }

    public static Map<String,RecordType> getProposalRecordTypesByName(){
        return getRecordTypesBySObjectAndName().get(cpq_u.PROPOSAL_SOBJECT_TYPE);
    }

    public static String getUnlockedRecordTypeName (Apttus_Proposal__Proposal__c proposal) {
        String dealTypeName = proposal.RecordType.DeveloperName == null ? '' : proposal.RecordType.DeveloperName;
        return getUnlockedRecordTypeName(dealTypeName);
    }

    public static String getUnlockedRecordTypeName (Apttus__APTS_Agreement__c agreement) {
        String dealTypeName = agreement.RecordType.DeveloperName == null ? '' : agreement.RecordType.DeveloperName;
        return getUnlockedRecordTypeName(dealTypeName);
    }

    public static String getUnlockedRecordTypeName (String recordTypeName) {
        return recordTypeName.replaceAll(REGEX_SUFFIX_LOCKED,'');
    }
}