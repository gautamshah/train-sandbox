/*
Test Class

CPQ_Controller_GenerateAgreement
CPQ_ProposalAgreement

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-11-05      Yuli Fintescu       Created
===============================================================================
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
private class Test_CPQ_Controller_GenerateAgreement
{
    static List<Apttus__APTS_Agreement__c> testAgreements;

    static void createTestData()
    {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        upsert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        Product2 testProduct =
            cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG, 'ITEM', 1);
        insert testProduct;

        Apttus_Config2__PriceListItem__c testPriceListItem =
            [SELECT Id
             FROM Apttus_Config2__PriceListItem__c
             WHERE Apttus_Config2__ProductId__c = :testProduct.Id
             LIMIT 1];

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus__APTS_Agreement__c.class).values()) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        } 

        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
        Account testGPO = new Account(Name = 'Test GPO', Status__c = 'Active', Account_External_ID__c = 'USGG-SG0150', RecordTypeID = gpoRT.getRecordTypeId());
        insert testGPO;

        List<Account> testAccounts = new List<Account> {
            new Account(Status__c = 'Active', Name = 'Test Account', Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,  RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-111111'),

            new Account(Status__c = 'Active', Name = 'Facilitiy', Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,     RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-222222'),
            new Account(Status__c = 'Active', Name = 'Distributor', Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-333333')
        };
        insert testAccounts;

        Opportunity testOpp = new Opportunity(Name = 'Test Opp', AccountID = testAccounts[0].ID, StageName = 'Identify', CloseDate = System.today());
        insert testOpp;

        Partner_Root_Contract__c testRoot = new Partner_Root_Contract__c(Name = '011111', Root_Contract_Name__c = '011111', Root_Contract_Number__c = '011111', Direction__c = 'I', Effective_Date__c = System.Today());
        insert testRoot;

        testAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeID = mapProposalRType.get('Sales_and_Pricing_Agreement_AG'), Apttus__Account__c = testAccounts[0].ID, Apttus__Related_Opportunity__c = testOpp.ID)
        };
        insert testAgreements;

        List<Apttus_Config2__ProductConfiguration__c> testConfigs = new List<Apttus_Config2__ProductConfiguration__c> {
            new Apttus_Config2__ProductConfiguration__c(Apttus_CMConfig__AgreementId__c = testAgreements[0].ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__Status__c = 'Saved', Apttus_Config2__VersionNumber__c = 1)
        };
        insert testConfigs;

        Apttus_Config2__LineItem__c testLine =
            new Apttus_Config2__LineItem__c(
                Apttus_Config2__IsPrimaryLine__c = true, 
                Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, 
                Apttus_Config2__ProductId__c = testProduct.ID, 
                Apttus_Config2__PriceListItemId__c = testPriceListItem.ID, 
                Apttus_Config2__ItemSequence__c = 1, 
                Apttus_Config2__LineNumber__c = 1, 
                Apttus_Config2__PrimaryLineNumber__c = 1);
        insert testLine;

        Agreement_Participating_Facility__c testFacility = new Agreement_Participating_Facility__c(Agreement__c = testAgreements[0].ID, Account__c = testAccounts[1].ID);
        insert testFacility;

        Agreement_Distributor__c testDistributor = new Agreement_Distributor__c(Agreement__c = testAgreements[0].ID, Account__c = testAccounts[2].ID);
        insert testDistributor;

        Map<String,Id> mapRebateRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Rebate_Agreement__c.class).values()) {
            mapRebateRType.Put(R.DeveloperName, R.Id);
        }

        Rebate_Agreement__c testRebate = new Rebate_Agreement__c(Related_Agreement__c = testAgreements[0].ID, RecordTypeID = mapRebateRType.get('Early_Signing_Incentive'));
        insert testRebate;
    }

    static testMethod void TestClone() {
        createTestData();

        Test.startTest();
            PageReference pageRef = Page.CPQ_GenerateProposal;
            Test.setCurrentPage(pageRef);

            List<Apttus__APTS_Agreement__c> records = [Select ID, Name, RecordTypeID, RecordType.Name, RecordType.DeveloperName, Apttus__Related_Opportunity__c, Apttus__Account__c, AgreementId__c From Apttus__APTS_Agreement__c Where ID in: testAgreements];

            ApexPages.currentPage().getParameters().put('pageMode', 'edit');
            CPQ_Controller_GenerateAgreement p = new CPQ_Controller_GenerateAgreement(new ApexPages.StandardController(records[0]));
            p.doCancel();
            p.getDisableButton();
            System.Debug(p.SourceType);
            System.Debug(p.SourceTitle);
            System.Debug(p.SourcePurpose);
            System.Debug(p.pageMode);
            System.Debug(p.theRecord);
            p.doGenerate();

        Test.stopTest();
    }
}