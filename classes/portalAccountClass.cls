public class portalAccountClass {

    public portalAccountClass(ApexPages.StandardController controller) {
        User u = [Select id,Accountid from User where Id=:UserInfo.getUserId()];
        if(u.accountId != null)
        recid = u.accountid;
    }

    public string recid{get; set;}
    
    static testMethod void m1(){
    
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        ID profileID = [ Select id from Profile where name='Customer Community User'].id;
         RecordType rt = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
         Contact c = new Contact();
          c.LastName = 'Tester';
          c.AccountId = a.Id;
          c.Connected_As__c = 'Administrator';
          c.RecordTypeId = rt.Id;
          c.Department_picklist__c = 'Other';
          c.Other_Department__c = 'Accounting';
          insert c;
          
        User u = new User( FirstName = 'Test-01'
                         , LastName = 'Test-02'
                         , Title = 'Rep'
                         , email='testinguser@fakeemail.com'
                         , profileid = profileid
                         , UserName='testinguser1212@fakeemail.com'
                         , alias='tusary'
                         , CommunityNickName='tusary'
                         , TimeZoneSidKey='America/New_York'
                         , LocaleSidKey='en_US'
                         , EmailEncodingKey='ISO-8859-1'
                         , LanguageLocaleKey='en_US'
                         ,contactid= c.Id);
        
        insert u;
        
        System.runAs(u){
            portalAccountClass obj = new portalAccountClass(new ApexPages.StandardController(new Account()));
        }
        
    }    



}