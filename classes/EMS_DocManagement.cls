public with sharing class EMS_DocManagement {
    public string Eventname{get;set;}
    public string EventId{get;set;}
    public List<Attachment> attachments{get;set;}
    public list<attachment> legalattachments{get;set;}

    Public list<EMS_Event__c> lstEvents;
    
    public EMS_DocManagement(ApexPages.StandardController controller) {
    lstEvents=new List<Ems_Event__c>();
    }
    public id selectedId;
    
    public pagereference selectevent(){
        selectedid=Apexpages.currentPage().getParameters().get('evid');
        system.debug('---->'+selectedid);
        return null;
    }
    
    public list<EMS_Event__c> getlstEvents(){
    
    return lstEvents;
    }
    public pagereference SearchEvents(){
    String query='Select Name,Id,Event_Id__c from EMS_Event__c where Name LIKE \'%'+ EventName + '%\''+' and Event_Id__c LIKE \'%'+EventId+'%\'';
    lstEvents=database.query(query);
    return null;
    
    }
    
    
     public pagereference getlist()
    {
        
        attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:selectedid];
        
        Map<Id,Event_Document__c> MEventDocuments= new Map<Id,Event_Document__c>([select id,EMS_Event__c from Event_Document__c where EMS_Event__c=:selectedid]);
        legalattachments= [select Id, ParentId, Name, Description from Attachment where parentId in :MEventDocuments.keyset()];
        if(attachments.isEmpty() && legalAttachments.isEMpty()){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No attachments found!!');
           ApexPages.addMessage(myMsg);
           return null; 
        
        }
        return null;
    }
}