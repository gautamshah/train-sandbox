global class BatchInsertLoginHistory implements Database.Batchable<sObject>{
    String Query;
    global BatchInsertLoginHistory(String q){
       
            Query=q;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
     List<CustomLoginHistory__c> newLoginHistoryIdsList = [select LoginHistoryID_Ref__c from CustomLoginHistory__c where logindate__c=YESTERDAY]; 
            System.debug(newLoginHistoryIdsList.size()); 
            String[] newLoginHistoryIds = new String[newLoginHistoryIdsList.size()]; 
            Integer i=0; 
            try{ 
                for(CustomLoginHistory__c loginHistoryId : newLoginHistoryIdsList){ 
                    newLoginHistoryIds[i++] = (String)(loginHistoryId.LoginHistoryID_Ref__c); 
                } 
            }catch(Exception e){ 
                System.debug('Exception1'); 
                throw e; 
            } 
            System.debug(newLoginHistoryIds); 
            if(newLoginHistoryIds.size()>0)
            {
                Query =  Query + ' and id not in : newLoginHistoryIds';
            }
        return Database.getQueryLocator(Query);  
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Map <Id,User> userList = new Map<Id, User>([SELECT FIRSTNAME,LASTNAME,EMAIL,UserRole.Name FROM USER]); 
        List<CustomLoginHistory__c> loginHistoryList = new List<CustomLoginHistory__c>();
        
        try{ 
            for (LoginHistory rec: (List<LoginHistory>)scope){
                System.debug('Entered Loop'); 
                CustomLoginHistory__c objclh = new CustomLoginHistory__c(); 
                objclh.FirstName__c = userList.get(rec.userId).firstname; 
                objclh.LastName__c = userList.get(rec.userId).LastName; 
                objclh.Email__c = userList.get(rec.userId).email; 
                objclh.loginapplication__c = rec.application; 
                objclh.logindatetime__c = rec.logintime; 
                objclh.logindate__c = (rec.logintime).date(); 
                objclh.logintype__c = rec.logintype; 
                objclh.LoginHistoryID_Ref__c = rec.id; 
                objclh.loginstatus__c = rec.Status; 
                objclh.User_Id__c = rec.UserId; 
                objclh.UserRole__c = userList.get(rec.userId).UserRole.name; 
                System.debug(loginHistoryList); 
                loginHistoryList.add(objclh); 
            }
        }catch(Exception e){ 
            System.debug('Exception2'); 
            throw e; 
        }
        if(loginHistoryList.size()>0)
        { 
            try{ 
                insert loginHistoryList; 
            }catch(Exception e){ 
                System.debug('Exception3'); 
                throw e; 
            } 
        } 
        System.debug('Apex Class Execution Completed');  
        
    }
    global void finish(Database.BatchableContext BC){  
        
    }
        

}