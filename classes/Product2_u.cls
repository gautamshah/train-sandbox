/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date		Id   Initials   Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD	A	PAB			CPR-000		Updated comments section
							AV-000
20161103	A	IL			AV-001		Title of the Jira – Some changes needed to be made to support Medical Supplies

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/    
public virtual class Product2_u extends sObject_u
{
	private Product2_PriceListItem_u PriceListItem = new Product2_PriceListItem_u(this);
	
    public void main(
            //here is a comment
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<Product2> newList,
            Map<Id, Product2> newMap,
            List<Product2> oldList,
            Map<Id, Product2> oldMap,
            integer size
        )
    {
    	if (isInsert || isUpdate)
    	{
    		if (isBefore)
    		{
	        	SyncUOMs(newList, oldMap);
    		}
	        else if (isAfter)
	        {
	        	PriceListItem.Sync(newList, oldMap);
	        }
    	}
    }
  
    // Keep the Apttus UOM in sync with the Product UOM
    public void SyncUOMs(List<Product2> newList, Map<Id, Product2> oldMap)
    {
        for (Product2 p : newList)
        {
            if (oldMap == null) // Insert
                if (p.Apttus_Product__c && string.isNotBlank(p.UOM_Desc__c)) // If it's an Apttus product &
                                                                             // a value exists, sync them
                    p.Apttus_Config2__Uom__c = Product2_UOM_g.translateCode(p.UOM_Desc__c);
                    
            else // Update - little trickier 
                 //    If the Apttus_Product__c is null, we have to see if it was already set - is there any way that a value could not be set?
                 //    
            {
                boolean isApttusProduct = p.Apttus_Product__c || (p.Apttus_Product__c == null && oldMap.get(p.Id).Apttus_Product__c);
                if (isApttusProduct && (oldMap != null && p.UOM_Desc__c != oldMap.get(p.Id).UOM_Desc__c)) // If the value has changed
                    p.Apttus_Config2__Uom__c = (string.isNotBlank(p.UOM_Desc__c)) ?
                                               Product2_UOM_g.translateCode(p.UOM_Desc__c) : null;
            }
        }
    }
}