@isTest
private class NewOppMobileFlow_TestClass {


   static testMethod void TestPageFunctionality() {
   
        // Setup Variables
        RecordType rt = [SELECT ID FROM RecordType WHERE SObjectType = 'Opportunity' and Name = 'US - Med Supplies - Opportunity'  LIMIT 1];
        RecordType acrt = [SELECT ID FROM RecordType WHERE SObjectType = 'Account' AND Name = 'US-Healthcare Facility' LIMIT 1];
        
        
        Account act = new Account(RecordTypeId = acrt.id, 
                                  name='Test');
       
       insert act;
       
        // Instantiate the standard controller
        Apexpages.StandardController sc = new Apexpages.standardController(act);

 
        // Instantiate the extension
        NewOppMobileFlow ext = new NewOppMobileFlow(sc);
       
       // Setup Data
       ext.Opportunity_Name = 'ABC Opportunity';
       ext.Type = 'New Business';
       ext.Stage = 'Indentify';
       ext.Amount = 10;
       ext.Forecast = 'Omit (0% Probability)';
       ext.Close_Date = System.today();
       ext.Dealers = 'IMS';
       ext.Description = 'Test';
       
       //Test converage for the MasterContactSearchvisualforce page
       PageReference pageRef = Page.New_Opportunity_Mobile;
       pageRef.getParameters().put('id', String.valueOf(act.Id));
       Test.setCurrentPage(pageRef);
       
       
        //Execute Methods
        
        ext.getOpportunityRecTypes(); 
        ext.getTypeList(); 
        ext.getStageList(); 
        ext.getDealersList(); 
        ext.getForecastList();
        ext.LockForecastCheck();
        ext.Save(); 
   
   }
     
}