/****************************************************************************************
 * Name    : Test_FTRTriggerHandler 
 * Author  : Bill Shan
 * Date    : 17/06/2013 
 * Purpose : Test FTR trigger and trigger handler
 * Dependencies: FTRTrigger Trigger
 *             , FTRTriggerHandler
 *             , FTR Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE    AUTHOR    CHANGE
 * 021013  GCC       Line 58 - Changed 'Asia - 2' to 'Asia - KR'
 * 9/14/2015 Amogh   Line 71 - Changed 'Asia' to 'APAC-ASIA'
 *****************************************************************************************/
@isTest
private class Test_FTRTriggerHandler {

    static testMethod void myUnitTest() {
        
        createTestUser();
        testingMethods();
    }
    
    
    //Test methods in 
    private static void testingMethods()
    {
        Test.startTest();
        System.runAs(krUser){
            
            testUtility tu = new testUtility();
           
            Account a = tu.testAccount('Asia-Healthcare Facility','TestAcct');  
            Account d = tu.testAccount('Asia-Distributor', 'TestDis');
            
            Field_Technical_Report__c FTR = new Field_Technical_Report__c(
                                                Facility_Hospital_Name__c = a.Id, 
                                                Distributor__c = d.Id, 
                                                Ready_To_Submit__c = 'Yes',
                                                Reason__c = 'Test',
                                                Reason_for_No_Return__c = 'Disposed',
                                                Contact_Phone_Num__c = '123456');
            insert FTR;
            
            FTR.Ready_To_Submit__c = 'No';
            update FTR;
            
            FTR.Ready_To_Submit__c = 'Yes';
            update FTR;
        }
        
        Test.stopTest();
    }

    //Create Testing User
    private static User krUser; 
    private static void createTestUser(){
        
        ID krProfileID = [SELECT Id FROM Profile WHERE Name = 'Asia - KR'].ID;
        
        krUser = new User(username='KR20121217@testmdt.com',
                            alias = 'K1217',
                            email='KR20121217@test.com', 
                            emailencodingkey='UTF-8',
                            lastname='Covidien',
                            languagelocalekey='en_US',
                            localesidkey='en_US',                                             
                            profileid = krProfileID,
                            timezonesidkey='Europe/Berlin',
                            Region__c = 'APAC-ASIA',
                            Asia_Team_Asia_use_only__c = 'CRM Team',
                            Business_Unit__c = 'All'
                            );
        insert krUser;
    }
    
}