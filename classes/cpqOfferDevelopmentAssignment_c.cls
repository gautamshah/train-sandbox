public class cpqOfferDevelopmentAssignment_c extends sObject_c
{
    private static cpqOfferDevelopmentAssignment_g cache =
    	new cpqOfferDevelopmentAssignment_g();
    
	public static set<string> fieldsToInclude =
		new set<string>
		{
			'Id',
			'Name',
			'External_Territory_Id__c',
			'CPQ_SSG_OD_Analyst__c'
		};

	static
	{
		load();
	}
	
	////
	//// load
	////
	public static void load()
	{
		if (cache.isEmpty())
		{
			List<CPQ_SSG_OD_Assignments__c> fromDB = fetchFromDB();
			cache.put(fromDB);
		}
	}

	////
	//// fetchFromDB
	////
    private static List<CPQ_SSG_OD_Assignments__c> fetchFromDB()
    {
    	string query =
			SOQL_select.buildQuery(
				CPQ_SSG_OD_Assignments__c.sObjectType,
				fieldsToInclude,
				null,
				null,
				null);
    	
		return (List<CPQ_SSG_OD_Assignments__c>)Database.query(query);
    }
}