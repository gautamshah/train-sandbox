/*****************************************************************************************
* Name    : OpportunityCopyWebService
* Author  : Hidetoshi kawada (ECS)
* Date    : 01/27/2016
* Purpose : 案件コピーカスタムボタン処理(WebService)
* Outline :
*
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE        AUTHOR               CHANGE
* ----        ------               ------
*****************************************************************************************/

global class OpportunityCopyWebService{
    
    // 案件に紐づく案件製品がない場合の通知文字列
    private static String noLineItemStr = 'noLineitem';
    
    // 「製品と共にコピー」ボタン処理
    webService static String oppCopyWithLineItem(String id){   

        // 変数初期化
        String retId = '';
        String argId = id;
        String oppQuery = '';
        String oppLineItemQuery = '';
        List<Opportunity> originOppSearchResult = New List<Opportunity>();
        List<OpportunityLineItem> originOppLineItemSearchResult = New List<OpportunityLineItem>();
        List<OpportunityLineItem> copyOppLineitemList = New List<OpportunityLineItem>();
        
        // コピー元案件情報を取得
        oppQuery = getSelectAllQuery('Opportunity');
        oppQuery += ' WHERE Id = \'' + argId + '\'';
        oppQuery += ' Limit 1';
        originOppSearchResult = Database.query(oppQuery);
        
        // 案件情報に紐づく案件製品情報を取得
        oppLineItemQuery = getSelectAllQuery('OpportunityLineItem');
        oppLineItemQuery +=  ' WHERE OpportunityId = \'' + (string) originOppSearchResult[0].Id + '\'';
        originOppLineItemSearchResult = Database.query(oppLineItemQuery);
        
        // 案件製品が存在するかチェック
        if(originOppLineItemSearchResult.size() < 1){
            return noLineItemStr;
        }

        // 案件情報をコピーしてインサート
        List<Opportunity> copyOppList = New List<Opportunity>(originOppSearchResult);
        copyOppList[0].Id = null;
        insert copyOppList;
        
        // 案件を紐づけた状態で、コピーした案件製品をインサート
        OpportunityLineItem tmpOppLineItem = new OpportunityLineItem();
        for(OpportunityLineItem li : originOppLineItemSearchResult){
            tmpOppLineItem = li.clone(false,true);
            tmpOppLineItem.OpportunityId = copyOppList[0].Id;
            // 合計金額を削除
            tmpOppLineItem.TotalPrice = null;
            // OppIdPricebookentryId(ユニーク項目) [CVJ_SELLOUT_DEV-92]
            // tmpOppLineItem.OppIdPricebookentryId__c = null;
            copyOppLineitemList.add(tmpOppLineItem);
            tmpOppLineItem = new OpportunityLineItem();
        }
        insert copyOppLineitemList;
        
        retId = (string) copyOppList[0].Id;
        return retId;
    }

    // 「製品なしでコピー」ボタン処理    
    webService static String oppCopyWithoutLineItem(String id){

        // 変数初期化
        String retId = '';
        String argId = id;
        String oppQuery = '';
        List<Opportunity> originOppSearchResult = New List<Opportunity>();
        
        // コピー元案件情報を取得
        oppQuery = getSelectAllQuery('Opportunity');
        oppQuery += ' WHERE Id = \'' + argId + '\'';
        oppQuery += ' Limit 1';
        originOppSearchResult = Database.query(oppQuery);
        
        // 案件情報をコピーしてインサート
        List<Opportunity> copyOppList = New List<Opportunity>(originOppSearchResult);
        copyOppList[0].Id = null;
        // Type_of_Products__c を初期化(CVJ_SELLOUT_DEV-93)
        copyOppList[0].Type_of_Products__c = null;
        // 金額関連項目値を初期化(CVJ_SELLOUT_DEV-94)
        copyOppList[0].Amount = 0;
        copyOppList[0].TotalOpportunityQuantity = 0;
        copyOppList[0].StageName = 'Identify';
        copyOppList[0].Probability = 0;
        
        insert copyOppList;
        
        retId = (string) copyOppList[0].Id;
        return retId;
    }
    
    // 指定したオブジェクトの全項目を取得するクエリ作成
    private static string getSelectAllQuery(String objName) {

        String fieldsString = '';
        String commaString = '';

        // SObject Describe
        Schema.DescribeSObjectResult obj = Schema.getGlobalDescribe().get(objName).getDescribe();

        // Fieldマップ取得
        Map<String, Schema.SObjectField> fieldMap = obj.fields.getMap();
        for(Schema.SObjectField item : fieldMap.values()) {
            fieldsString += commaString + item.getDescribe().getName();
            commaString = ',';
        }
        return 'SELECT ' + fieldsString + ' FROM ' + objName + ' ';
    }
    
}