/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global abstract class VirtualCard {
    global String columnId {
        get;
        set;
    }
    global String cssClasses {
        get;
        set;
    }
    global Boolean hasChanged {
        get;
        set;
    }
    global Boolean hasMoved {
        get;
        set;
    }
    global Boolean isNew {
        get;
    }
    global SObject item {
        get;
        set;
    }
    global VirtualCard() {

    }
    global VirtualCard(SObject item) {

    }
    global virtual String getCssClasses() {
        return null;
    }
    global abstract SObject newItem();
}
