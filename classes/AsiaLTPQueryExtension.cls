public with sharing class AsiaLTPQueryExtension {

    private final Account acct;
    public String searchText {get; set;}
    public String searchPrd {get; set;}
    public List<Product_SKU__c> AllSearchPrd {get;set;}
    public Date invoiceDate {get; set;}
    public String prodName {get; set;}
    public decimal lastTransactedAmount {get; set;}
    public decimal lastTransactedAmountOriginal {get; set;}
    public string  lastTransactedAmountCurrency {get; set;}
    public string  lastTransactedAmountString {get; set;}
    public decimal lastTransactedQty {get; set;}
    public decimal recommendedListPrice {get; set;}
    public decimal lastTransactedPrice {get; set;}
    public decimal lastTransactedPriceOriginal {get; set;}
    public string  lastTransactedPriceString {get; set;}
    private string userCountry {get;set;}
    public string userCurrency {get;set;}
    private string userFranchise {get;set;}
    private List<Pricebook2> pbList {get;set;}
    public integer sellingQty {get; set;}
    public decimal retailPrice {get; set;}
    
    public AsiaLTPQueryExtension (ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        prodName = '-';
        lastTransactedAmount = null;
        lastTransactedQty = null;
        invoiceDate = null;
        lastTransactedPrice = null;
        
        userCountry = [SELECT Country FROM User WHERE id = :UserInfo.getUserId()].Country;
        userCurrency =  userInfo.getDefaultCurrency();
        
        userFranchise = [SELECT Business_Unit__c FROM User WHERE id = :UserInfo.getUserId()].Business_Unit__c;
        userFranchise = userFranchise.replaceAll(' & ', '\',\''); //e.g. changing VT & MS into VT','MS
        if (userFranchise == 'S2')
            userFranchise = 'SD\',\'EbD';
        System.debug('userFranchise: '+ userFranchise);
        
        String countryQuery = userCountry + '%';
        System.debug('userCountry: '+userCountry);    
        pbList = [SELECT id FROM Pricebook2 WHERE Name like :countryQuery 
                    AND IsActive = true AND IsStandard = false]; 
    }

    public PageReference doPrdSearch() {
        if(String.isNotBlank(searchPrd) && String.isNotEmpty(searchPrd)){
            String modifiedSearchPdt = '%' + searchPrd + '%' ;    
            System.debug('modifiedSearchPdt : '+ modifiedSearchPdt );
            String queryString = 'SELECT Name, SKU__c, Vendor_Item_Code__c, Selling_UOM__c, Sales_Class__c' +
                                ' FROM Product_SKU__c' +
                                ' WHERE Country__c = \'' + userCountry + '\' AND Name LIKE \'' + modifiedSearchPdt + '\'';
               
            queryString = queryString + franchiseQuery('GBU__c', userFranchise);              
            queryString = queryString + ' LIMIT 1000'; 
            System.debug('doPrdSearch Query: ' + queryString); 
                           
            AllSearchPrd = Database.query(queryString);                        
        }
        return null;
    } 

    public PageReference doSearch() {
        
        String searchText2 = 'T' + searchText;
        System.debug('searchText: '+ searchText);
        System.debug('searchText2: '+ searchText2);
        
        try{
            String skuQueryString = 'SELECT Name, Shipped_Quantity_Eaches__c, convertCurrency(Net_Sales_Amount__c)' +
                                    ' , Invoice_Date__c, CurrencyIsoCode FROM Sales_Transaction__c' +
                                    ' WHERE (SKU__C = \'' + searchText + '\' OR SKU__c = \'' + searchText2 + '\')' +
                                    ' AND Shipped_Quantity_Eaches__c>0 AND SellToAccount__c = \'' +acct.id + '\'';

            skuQueryString = skuQueryString + franchiseQuery('Business_Unit__c', userFranchise);
                             
            skuQueryString = skuQueryString + ' ORDER BY Invoice_Date__c DESC NULLS LAST LIMIT 1'; 
            System.debug('doSearch skuQueryString : ' + skuQueryString); 
            
            String skuQueryString2 = skuQueryString;
            skuQueryString2 = skuQueryString2.replace('convertCurrency(Net_Sales_Amount__c)', 'Net_Sales_Amount__c');
                    
            Sales_Transaction__c result = Database.query(skuQueryString);
            Sales_Transaction__c result2 = Database.query(skuQueryString2);
            
            prodName = result.Name;
            lastTransactedAmount = result.Net_Sales_Amount__c;
            lastTransactedAmountOriginal = result2.Net_Sales_Amount__c;
            lastTransactedAmountCurrency = result.CurrencyIsoCode;
            lastTransactedAmountString = userCurrency + ' ' + lastTransactedAmount + ' (' + lastTransactedAmountCurrency + ' ' + lastTransactedAmountOriginal + ')'; 
            
            lastTransactedQty = result.Shipped_Quantity_Eaches__c;
            invoiceDate = result.Invoice_Date__c;
            
            lastTransactedPrice = (lastTransactedAmount / lastTransactedQty).setScale(2, RoundingMode.HALF_UP);
            lastTransactedPriceOriginal = (lastTransactedAmountOriginal / lastTransactedQty).setScale(2, RoundingMode.HALF_UP);
            lastTransactedPriceString = userCurrency + ' ' + lastTransactedPrice + ' (' + lastTransactedAmountCurrency + ' ' + lastTransactedPriceOriginal + ')'; 
            
        } catch (exception e)
        {
            System.debug('Sales Transaction Query Error: '+e);
            prodName = '-';
            lastTransactedAmount = null;
            lastTransactedQty = null;
            invoiceDate = null;
            lastTransactedPrice = null;
            lastTransactedAmountString = '';
            lastTransactedPriceString = '';
        }
            
        try{
        
            String prdQueryString = 'SELECT id, Selling_Quantity__c FROM Product2' +
                                    ' WHERE (ProductCode = \'' + searchText + '\' OR ProductCode = \'' + searchText2 + '\')' +
                                    ' AND Country__c = \'' + userCountry + '\' AND IsActive = true';
            
            prdQueryString = prdQueryString + franchiseQuery('GBU__c', userFranchise);
                              
            prdQueryString = prdQueryString + ' LIMIT 1'; 
            System.debug('doSearch prdQueryString : ' + prdQueryString); 
        
            Product2 prod = Database.query(prdQueryString);
            sellingQty = prod.Selling_Quantity__c.intValue();
          
            PriceBookEntry pbe = [SELECT UnitPrice FROM PriceBookEntry 
                                    WHERE UseStandardPrice = false AND Product2Id = :prod.id
                                    AND Pricebook2Id IN :pbList AND IsActive = true
                                    ORDER BY LastModifiedDate DESC 
                                    LIMIT 1];
            
            recommendedListPrice = pbe.UnitPrice;
            retailprice = sellingQty * recommendedListPrice;
        } catch (exception e){
            System.debug('doSearch Prd Query Error: '+e);
            recommendedListPrice = null;
            retailprice = null;
        }
        
        return null;
    }
    
    private string franchiseQuery (String queryField, String franchise){
        string queryString = '';
        
        if(franchise == 'EbD' || franchise == 'SD' || franchise == 'RMS' || franchise == 'SD\',\'EbD'
            || franchise == 'VT\',\'MS' || franchise== 'VT' || franchise == 'MS' || franchise == 'RMS\',\'MS'){
            queryString = ' AND ' + queryField + ' IN (\'' + franchise + '\')'; 
        }
        
        return queryString;
    }

}