global class CPQ_SSG_DM_Batch_COT implements Database.Batchable<sObject>
{
    /****************************************************************************************
    * Name    : CPQ SSG DM Batch COT 
    * Author  : Subba Reddy Muchumari
    * Date    : 6/18/2015 
    * Purpose : Batch Job used to SSG Staging Table CPQ_SSG_STG_Deal_COT__c Mapping to Apttus tables.
    * Dependencies: 
    *
    *****************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //string query='select STG_AllProductsRebate__c,STG_ClassOfTradeCode__c,STG_ComplianceDollars__c,STG_CompliancePercent__c,STG_CreatedBy__c,STG_DealId__c,STG_GrowthRebate__c,STG_NewBusinessRebate__c from CPQ_SSG_STG_Deal_COT__c where Name=\'C-00001\'';
        String query='select STG_AllProductsRebate__c,STG_ClassOfTradeCode__c,STG_ComplianceDollars__c,STG_CompliancePercent__c,STG_CreatedBy__c,STG_DealId__c,STG_GrowthRebate__c,STG_NewBusinessRebate__c,STG_ProposedPriceBook__c,STG_QualifyingPriceBook__c,STG_PropsedPriceBookCode__c,STG_QualifyingPriceBookCode__c from CPQ_SSG_STG_Deal_COT__c';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<CPQ_SSG_STG_Deal_COT__c> scope)
    {
        List<CPQ_SSG_STG_Deal__c> processDeals = [Select Id, STG_DealId__c From CPQ_SSG_STG_Deal__c Where STG_Process__c = true];
        Set<String> processDealIds = new Set<String>();
        for (CPQ_SSG_STG_Deal__c processDeal: processDeals) {
            processDealIds.add(processDeal.STG_DealId__c);
        }

        System.debug('scope******'+scope);
        Set<String> STGClassTradeCodeSet = new Set<String>();
        Set<String> createdBySet = new Set<String>();
        Set<String> STGDealIds = new Set<String>();
        Set<Integer> STGTiers = new Set<Integer>();
        List<CPQ_Error_Log__c> errorLogs = new List<CPQ_Error_Log__c>();

        List<CPQ_SSG_STG_Deal_COT__c> processScope = new List<CPQ_SSG_STG_Deal_COT__c>();

        for(CPQ_SSG_STG_Deal_COT__c c:scope)
        {
            if (processDealIds.contains(c.STG_DealId__c)) {
                processScope.add(c);
                STGClassTradeCodeSet.add(c.STG_ClassOfTradeCode__c); 
                createdBySet.add(c.STG_CreatedBy__c);
                STGDealIds.add(c.STG_DealId__c);
                Integer tierNumber = getTierNumber(c.STG_PropsedPriceBookCode__c);
                if (tierNumber != null) {
                    STGTiers.add(tierNumber);
                }
                tierNumber = getTierNumber(c.STG_QualifyingPriceBookCode__c);
                if (tierNumber != null) {
                    STGTiers.add(tierNumber);
                }
            }
        }
System.debug('stg tiers = ' + STGTiers);
System.debug('stg class of trades = ' + STGClassTradeCodeSet);
        Map<string,CPQ_SSG_Class_of_Trade__c> cpqSCTMap=new Map<string,CPQ_SSG_Class_of_Trade__c>();
        List<CPQ_SSG_Class_of_Trade__c> cpqSCTlist=[select Code__c,Id from CPQ_SSG_Class_of_Trade__c where Code__c IN:STGClassTradeCodeSet];
        for(CPQ_SSG_Class_of_Trade__c cp:cpqSCTlist)
        {
            cpqSCTMap.put(cp.Code__c,cp);
        }

        Map<String,CPQ_SSG_Pricing_Tier__c> cpqPricingTierMap = new Map<String,CPQ_SSG_Pricing_Tier__c>();
        List<CPQ_SSG_Pricing_Tier__c> cpqPricingTierList = [SELECT Id, Sort_Order__c, CPQ_SSG_Class_of_Trade__r.Code__c FROM CPQ_SSG_Pricing_Tier__c Where Pricing_Tier_Type__c = 'Listerine' And Is_Active__c = true And CPQ_SSG_Class_of_Trade__r.Code__c in :STGClassTradeCodeSet And Sort_Order__c in :STGTiers];
        for (CPQ_SSG_Pricing_Tier__c pricingTier: cpqPricingTierList) {
            cpqPricingTierMap.put(pricingTier.CPQ_SSG_Class_of_Trade__r.Code__c + '_' + String.valueOf(pricingTier.Sort_Order__c), pricingTier);
        }
        
        List<CPQ_DM_Variable__c> CPQDMVarList=[Select Value__c,Name From CPQ_DM_Variable__c Where Type__c= 'User' And Name IN:createdBySet];
        Map<string,string> CPQDMVarMap=new Map<string,string>();
        for(CPQ_DM_Variable__c cdv:CPQDMVarList)
        {
            CPQDMVarMap.put(cdv.Name,cdv.Value__c);
        }
        
        List<Apttus_Config2__ProductConfiguration__c> apttusConfPrdList=[select Id,Legacy_External_Id__c from Apttus_Config2__ProductConfiguration__c where Legacy_External_Id__c IN:STGDealIds];
        Map<string,string> apttusConfPrdMap=new Map<string,string>();
        for(Apttus_Config2__ProductConfiguration__c ac:apttusConfPrdList)
        {
            apttusConfPrdMap.put(ac.Legacy_External_Id__c,ac.Id);
        }
        
        List<Apttus_Proposal__Proposal__c> apttusProProList=[select Legacy_External_Id__c,Id from Apttus_Proposal__Proposal__c where Legacy_External_Id__c IN:STGDealIds];
        Map<string,string> apttusProProMap=new Map<string,string>();
        for(Apttus_Proposal__Proposal__c app:apttusProProList)
        {
            apttusProProMap.put(app.Legacy_External_Id__c,app.Id);
        } 
        
        //Getting Attus Config PriceList Data
        Apttus_Config2__PriceList__c apptusConfPriceList = cpqPriceList_c.SSG;
        //Mapping STG_ClassOfTradeCode__c to get a product name
        Map<string,string> prdNameMap=new Map<string,string>();
        prdNameMap.put('S13','Access COT');
        prdNameMap.put('E02','Electrosurgery COT');
        prdNameMap.put('S01','Instrumentation COT');
        prdNameMap.put('S02','Wound Closure COT');
        //Getting Products data in to List
        List<product2> productList=new list<product2>();
        if(prdNameMap.values().size()>0)
        {
            productList=[select Id,Name from product2 where Name IN:prdNameMap.values()];
        }
        Map<string,string> prdIdsMap=new Map<string,string>();
        set<Id> productIdSet = new set<Id>();
        if(productList.size()>0)
        {
            for(product2 p:productList)
            {
                prdIdsMap.put(p.Name,p.Id);
                productIdSet.add(p.Id);
            }
        }
        //Getting Apttus Config PriceListItem records
        List<Apttus_Config2__PriceListItem__c> apttusConfPriceListItemList =
            new List<Apttus_Config2__PriceListItem__c>();
        if(productIdSet.size()>0)
        {
            apttusConfPriceListItemList.addAll( 
                [select Id,
                        Apttus_Config2__ProductId__c
                 from Apttus_Config2__PriceListItem__c
                 where Apttus_Config2__PriceListId__c = :apptusConfPriceList.Id AND
                       Apttus_Config2__ProductId__c IN :productIdSet]);
        }
        Map<string,string> appConfPicListItemMap=new Map<string,string>();
        if(apttusConfPriceListItemList.size()>0)
        {
            for(Apttus_Config2__PriceListItem__c acp:apttusConfPriceListItemList)
            {
                appConfPicListItemMap.put(acp.Apttus_Config2__ProductId__c,acp.Id);
            }
        }
        
        List<CPQ_SSG_QP_Class_of_Trade__c> cpqSSGQPCOTNewRecList=new List<CPQ_SSG_QP_Class_of_Trade__c>();
        List<Apttus_Config2__LineItem__c> apttusConfLineItNewRecList=new List<Apttus_Config2__LineItem__c>();
        for(CPQ_SSG_STG_Deal_COT__c c:processScope)
        { 
            //creation of CPQ_SSG_QP_Class_of_Trade__c records 
            
            CPQ_SSG_QP_Class_of_Trade__c cst=new CPQ_SSG_QP_Class_of_Trade__c();
            cst.All_Products_Rebate__c=c.STG_AllProductsRebate__c; 
            if(cpqSCTMap.containskey(c.STG_ClassOfTradeCode__c))
            {
                cst.CPQ_SSG_Class_of_Trade__c=cpqSCTMap.get(c.STG_ClassOfTradeCode__c).Id;
                cst.Expected_Annual_Customer_Spend__c=c.STG_ComplianceDollars__c;
                cst.Expected_Annual_Compliance__c=c.STG_CompliancePercent__c;
                //cst.OwnerId=CPQDMVarMap.get(c.STG_CreatedBy__c);
                cst.Legacy_External_Id__c=c.STG_DealId__c;    
                cst.Growth_Rebate__c=c.STG_GrowthRebate__c;
                cst.New_Business_Rebate__c=c.STG_NewBusinessRebate__c;
                cst.List_Type__c = 'Listerine';

                String proposedTierKey;
                String qualifyingTierKey;
                try {
                    Integer tierNumber = getTierNumber(c.STG_PropsedPriceBookCode__c);
                    if (tierNumber != null) {
                        proposedTierKey = c.STG_ClassOfTradeCode__c + '_' + String.valueOf(tierNumber);
                    }
                    tierNumber = getTierNumber(c.STG_QualifyingPriceBookCode__c);
                    if (tierNumber != null) {
                        qualifyingTierKey = c.STG_ClassOfTradeCode__c + '_' + String.valueOf(tierNumber);
                    }
                } catch (Exception e) {
                }

                CPQ_SSG_Pricing_Tier__c proposedTier = cpqPricingTierMap.get(proposedTierKey);
                CPQ_SSG_Pricing_Tier__c qualifyingTier = cpqPricingTierMap.get(qualifyingTierKey);
                if (proposedTier != null) {
                    cst.Proposed_Pricing_Tier__c = proposedTier.Id;
                }
                if (qualifyingTier != null) {
                    cst.Qualified_Pricing_Tier__c = qualifyingTier.Id;
                }

                if(c.STG_AllProductsRebate__c!=Null || c.STG_GrowthRebate__c!=Null || c.STG_NewBusinessRebate__c!=Null)
                {
                    cst.Include_Rebates__c=True;
                }
                else
                {
                    cst.Include_Rebates__c=False;
                }
                if(apttusProProMap.containskey(c.STG_DealId__c))
                {
                    cst.Quote_Proposal__c=apttusProProMap.get(c.STG_DealId__c);
                    cpqSSGQPCOTNewRecList.add(cst);
                } else {
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_COT', 'Error', 'Parent Deal ' + c.STG_DealId__c + ' not found', 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
                }
            } else {
                errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_COT', 'Error', 'Class of Trade ' +  c.STG_ClassOfTradeCode__c + ' not found', 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
            }
            
            
            //Creation of Line Item (Apttus_Config2__LineItem__c) records
            
            Apttus_Config2__LineItem__c acl=new Apttus_Config2__LineItem__c();
            //acl.CPQ_SSG_Class_of_Trade__c=cpqSCTMap.get(c.STG_ClassOfTradeCode__c).Id;
            acl.Apttus_Config2__ListPrice__c=c.STG_ComplianceDollars__c;
            acl.Apttus_Config2__AdjustedPrice__c=c.STG_ComplianceDollars__c;
            acl.Apttus_Config2__BasePrice__c=c.STG_ComplianceDollars__c;
            acl.Apttus_Config2__BaseExtendedPrice__c=c.STG_ComplianceDollars__c;
            acl.Apttus_Config2__ExtendedPrice__c=c.STG_ComplianceDollars__c;
            acl.Apttus_Config2__NetPrice__c=c.STG_ComplianceDollars__c;
            //acl.OwnerId=CPQDMVarMap.get(c.STG_CreatedBy__c);
            if(apttusConfPrdMap.containskey(c.STG_DealId__c))
            {
                acl.Apttus_Config2__ConfigurationId__c=apttusConfPrdMap.get(c.STG_DealId__c);
                if(apptusConfPriceList != null)
                {
                    acl.Apttus_Config2__PriceListId__c=apptusConfPriceList.Id;
                }
                if(prdNameMap.ContainsKey(c.STG_ClassOfTradeCode__c))
                {
                    if(prdIdsMap.ContainsKey(prdNameMap.get(c.STG_ClassOfTradeCode__c)))
                    {
                        
                        acl.Apttus_Config2__ProductId__c=prdIdsMap.get(prdNameMap.get(c.STG_ClassOfTradeCode__c));
                        if(appConfPicListItemMap.ContainsKey(prdIdsMap.get(prdNameMap.get(c.STG_ClassOfTradeCode__c))))
                        {
                            acl.Apttus_Config2__PriceListItemId__c=appConfPicListItemMap.get(prdIdsMap.get(prdNameMap.get(c.STG_ClassOfTradeCode__c)));
                        }
                    }
                    acl.Apttus_Config2__Description__c=prdNameMap.get(c.STG_ClassOfTradeCode__c);
                }
                
                
                acl.Apttus_Config2__PrimaryLineNumber__c=1;
                acl.Apttus_Config2__LineNumber__c=1;
                acl.Apttus_Config2__TotalQuantity__c =1;
                acl.Apttus_Config2__AllowableAction__c='Unrestricted';
                acl.Apttus_Config2__LineStatus__c='New';
                acl.Apttus_Config2__ChargeType__c='Standard Price';
                acl.Apttus_Config2__PriceMethod__c='Flat Price';
                acl.Apttus_Config2__BasePriceMethod__c='Flat Price';
                acl.Apttus_Config2__SyncStatus__c='Synchronized';
                acl.Apttus_Config2__IsPrimaryLine__c=true;
                acl.Apttus_Config2__LineType__c='Product/Service';
                acl.Apttus_Config2__TotalQuantity__c=1;
                acl.Apttus_Config2__Quantity__c=1;
                acl.Apttus_Config2__Uom__c='Each';
                acl.Apttus_Config2__PriceUom__c='Each';
                acl.Apttus_Config2__Frequency__c='One Time';
                acl.Apttus_Config2__PriceType__c='One Time';
                acl.Apttus_Config2__SellingFrequency__c='One Time';
                acl.Apttus_Config2__PricingStatus__c='Complete';
                acl.Apttus_Config2__ConstraintCheckStatus__c='NA';
                acl.Apttus_Config2__SellingTerm__c=1;
                acl.Apttus_Config2__ItemSequence__c=1;
                acl.Apttus_Config2__Term__c=1;
                acl.Apttus_Config2__ConfigStatus__c='NA';
                acl.Class_of_Trade_Tier_Item__c=true;
                apttusConfLineItNewRecList.add(acl);
            } else {
                errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_COT', 'Error', 'Parent Deal ' + c.STG_DealId__c + ' not found', 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
            }
 
        }
        //Inserting Create CPQ_SSG_QP_Class_of_Trade__c List
        //Insert cpqSSGQPCOTNewRecList;
        
        List<Database.SaveResult> srsCPQSSGQPTradeList = Database.insert(cpqSSGQPCOTNewRecList, FALSE);
        for (Database.SaveResult sr : srsCPQSSGQPTradeList) 
        {
            if (sr.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted CPQ Trade Records: ' + sr.getId());
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug('Fields that affected this error: ' + err.getFields());
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_COT', 'Error', 'Insert CPQ_SSG_QP_Class_of_Trade failed ' + err.getMessage(), null, null));
                }
            }
        }
        system.debug('cpqSSGQPCOTNewRecList*******'+cpqSSGQPCOTNewRecList);
        //Inserting Line Item (Apttus_Config2__LineItem__c) records
        //Insert apttusConfLineItNewRecList;
       
        List<Database.SaveResult> srsApttusLineItemList = Database.insert(apttusConfLineItNewRecList, FALSE);
        for (Database.SaveResult sr : srsApttusLineItemList ) 
        {
            if (sr.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Apttus LineItem Records: ' + sr.getId());
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug('Fields that affected this error: ' + err.getFields());
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_COT', 'Error', 'Insert Line Item failed ' + err.getMessage(), null, null));
                }
            }
        }
        system.debug('apttusConfLineItNewRecList*****'+apttusConfLineItNewRecList);

        if (!errorLogs.isEmpty()) {
            insert errorLogs;
        }
    }

    private static Integer getTierNumber(String priceBookCode) {
        Integer returnTier = null;
        if (priceBookCode != null) {
            String tierString;
            if (priceBookCode.startsWith('DACCESS')) {
                tierString = priceBookCode.substringAfter('DACCESS');
            } else if (priceBookCode.startsWith('DINSLIG')) {
                tierString = priceBookCode.substringAfter('DINSLIG');
            } else if (priceBookCode.startsWith('DWOUND')) {
                tierString = priceBookCode.substringAfter('DWOUND');
            } else if (priceBookCode.startsWith('DELESUR')) {
                tierString = priceBookCode.substringAfter('DELESUR');
            } else {
                tierString = priceBookCode.substringAfter('T');
            }
            if (tierString != null) {
                    returnTier = Integer.valueOf(tierString);
            }
        }
        return returnTier;
    }

    global void finish(Database.BatchableContext BC) {}
}