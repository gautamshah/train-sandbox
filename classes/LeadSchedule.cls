/****************************************************************************************
* Name    : LeadSchedule
* Author  : Tejas Kardile
* Date    : 4/16/2014
* Purpose : Sends email notifications based on lead status and time elapsed
* 
* Dependancies: 
*   Email Templates: 'Email_Notification_1_to_Rep', 'Email_Notification_2_to_Rep', 'Lead_Notification_Reminder_to_RM'
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
*   DATE            AUTHOR                  CHANGE
*   ----            ------                  ------
*   4/17/14         Gautam Shah             modified to have more conditional processing and more selective queries
*   5/13/14         Gautam Shah             Lines 44-45: commented out the DateTime variable in favor of the SOQL Date literal; made the inclusion time frame 7 days instead of 3
                                            Lines 74-94, 110-130, 146-165:  added code to prevent null pointers, absence of manager email and made the GBU admin the default sender if no manager specified --> HARDCODED!!
                                            Lines 23-54: SingleEmailMessage TargetObject cannot be Lead otherwise the lead will receive the email as well, however can't set it User since WhatId is not supported if doing so; created a workaround using a dummy Contact
*   6/17/14         Gautam Shah             Lines 132, 169:  changed the time comparison condition to evaluate against lead CreatedDate rather than CreationEmailDate__c            
*****************************************************************************************/ 
global class LeadSchedule implements Schedulable
{
    
    public Messaging.SingleEmailMessage sendemail(Id leadID, Id templateID, String to_email, String replyTo_Email)
    {
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(templateID);
        msg.setTargetObjectId(leadID);
        msg.setCcAddresses(new List<String>{to_email});
        msg.setReplyTo(replyTo_Email);
        return msg;
    }
    public List<Messaging.SingleEmailMessage> compileEmails(List<Messaging.SingleEmailMessage> lstMsgs)
    {
        // Cannot have TargetObject be Lead else the lead will receive the email
        // This is a workaround since WhatID cannot be Lead
        List<Messaging.SingleEmailMessage> compEmail = new List<Messaging.SingleEmailMessage>();
        // Send the emails to force the merge, but in a transaction so we can roll it back
        Savepoint sp = Database.setSavepoint();
        if(!test.isRunningTest())
        {
            Messaging.sendEmail(lstMsgs);
        }
        Database.rollback(sp);
        // copy the contents of the merged email to a new SingleEmailMessage. Then send those new messages.
        for (Messaging.SingleEmailMessage email : lstMsgs) 
        {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getCcAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            emailToSend.setSenderDisplayName('Salesforce.com Leads Administrator');
            emailToSend.setReplyTo(email.getReplyTo());
            compEmail.add(emailToSend);
        }
        return compEmail;
    }
   
    global void execute(SchedulableContext ctx) 
    {
        List<Id> selectedRecordTypeIDs = new List<Id>{Utilities.recordTypeMap_DeveloperName_Id.get('RMS_Lead_Type'), Utilities.recordTypeMap_DeveloperName_Id.get('Surgical_Lead_Type')};
        Map<Id,Lead> updateLeadMap = new Map<Id,Lead>();
        Map<String,ID> map_et = new Map<String,ID>();
        List<Messaging.SingleEmailMessage> lstemail = new List<Messaging.SingleEmailMessage>();
        for(EmailTemplate et: [SELECT developerName,id FROM EmailTemplate WHERE developerName in ('Email_Notification_1_to_Rep', 'Email_Notification_2_to_Rep', 'Lead_Notification_Reminder_to_RM')])            
        {
            map_et.put(et.developerName,et.id);
        }
        if(map_et.size() > 0)
        {
            //find the new leads within the past 3 days
            //DateTime dt4 = DateTime.Now().addHours(-72);
            List<Lead> lstlead = new List<Lead>();
            if(!test.isRunningTest())
            {
                lstlead = [Select CreationEmail__c, CreationEmailDate__c, CreatedDate, id, OwnerID, RecordTypeId from Lead where Status = 'Not Attempted' and CreationEmail__c = false and FirstReminderSent__c = false and SecondReminderSent__c = false and isConverted = false and CreatedDate >= LAST_N_DAYS:7 and RecordTypeId in :selectedRecordTypeIDs];
            }
            else
            {
                lstlead = [Select CreationEmail__c, CreationEmailDate__c, CreatedDate, id, OwnerID, RecordTypeId from Lead where Status = 'Not Attempted' ];            
            }
            
            //find leads that are not new but have not yet received their first reminder
            datetime dt1 = DateTime.Now();
            List<Lead> lstleadreminder = new List<Lead>();
            if(!test.isRunningTest())
            {
                lstleadreminder = [Select SecondReminderSent__c, FirstReminderSent__c, CreationEmail__c, CreationEmailDate__c, CreatedDate, id, OwnerID, RecordTypeId from Lead where Status = 'Not Attempted' and CreationEmail__c = true and FirstReminderSent__c = false and SecondReminderSent__c = false and isConverted = false and RecordTypeId in :selectedRecordTypeIDs];
            }
            else
            {
                lstleadreminder = [Select SecondReminderSent__c, FirstReminderSent__c, CreationEmail__c, CreationEmailDate__c, CreatedDate, id, OwnerID, RecordTypeId from Lead where Status = 'Not Attempted' ];
            }
            //find leads where the first reminder has been sent but not the second
            List<Lead> lstleadsecondreminder = new List<Lead>();
            if(!test.isRunningTest()){
                lstleadsecondreminder = [Select SecondReminderSent__c, FirstReminderSent__c, CreationEmail__c, CreationEmailDate__c, CreatedDate, id, OwnerID, RecordTypeId from Lead where Status = 'Not Attempted' and CreationEmail__c = true and FirstReminderSent__c = true and SecondReminderSent__c = false and isConverted = false and RecordTypeId in :selectedRecordTypeIDs];
            }
            else{
                lstleadsecondreminder = [Select SecondReminderSent__c, FirstReminderSent__c, CreationEmail__c, CreationEmailDate__c, CreatedDate, id, OwnerID, RecordTypeId from Lead where Status = 'Not Attempted'];            
            }
            //get distinct lead owners
            Set<Id> userIDSet = new Set<Id>();
            for(Lead l : lstlead)
            {
                userIDSet.add(l.OwnerID);
            }
            for(Lead l : lstleadreminder)
            {
                userIDSet.add(l.OwnerID);
            }
            for(Lead l : lstleadsecondreminder)
            {
                userIDSet.add(l.OwnerID);
            }
            Map<id,User> usermap = new Map<id,User>([select id, Email, User_Manager__c, User_Manager_Email__c from User where IsActive = true And Email != '' And Id in :userIDSet]);

            //set-up the emails
            if(map_et.containsKey('Email_Notification_1_to_Rep'))
            {
                for(Lead l : lstlead)
                {
                    if(usermap.containsKey(l.OwnerID))
                    {
                        if(!String.isBlank(usermap.get(l.OwnerID).Email))
                        {
                            String managerEmail = usermap.get(l.OwnerID).User_Manager_Email__c;
                            if(String.isBlank(managerEmail))
                            {
                                if(l.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('RMS_Lead_Type'))
                                {
                                    managerEmail = 'Corri.Finizza@covidien.com';//RMS GBU Admin
                                }
                                else
                                {
                                    managerEmail = 'Derek.Carless@covidien.com';//Surgical GBU Admin
                                }
                            }
                            lstemail.add(SendEmail(l.Id, map_et.get('Email_Notification_1_to_Rep'), usermap.get(l.OwnerID).Email, managerEmail));
                            l.CreationEmail__c = true;
                            l.CreationEmailDate__c = DateTime.Now();
                        }
                    }
                }
                updateLeadMap.putAll(lstlead);
            }
            else
            {
                System.debug('Email Template Email_Notification_1_to_Rep not found');
            }
                
            if(map_et.containsKey('Email_Notification_2_to_Rep'))
            {
                for(Lead l : lstleadreminder)
                {
                    //datetime dt = l.CreationEmailDate__c.addHours(48);
                    DateTime dt = l.CreatedDate.addHours(48);
                    if(dt <= dt1)
                    {
                        if(usermap.containsKey(l.OwnerID))
                        {
                            if(!String.isBlank(usermap.get(l.OwnerID).Email))
                            {
                                String managerEmail = usermap.get(l.OwnerID).User_Manager_Email__c;
                                if(String.isBlank(managerEmail))
                                {
                                    if(l.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('RMS_Lead_Type'))
                                    {
                                        managerEmail = 'Corri.Finizza@covidien.com';//RMS GBU Admin
                                    }
                                    else
                                    {
                                        managerEmail = 'Derek.Carless@covidien.com';//Surgical GBU Admin
                                    }
                                }
                                lstemail.add(SendEmail(l.id, map_et.get('Email_Notification_2_to_Rep'), usermap.get(l.OwnerID).Email, managerEmail));
                                l.FirstReminderSent__c = true;    
                                updateLeadMap.put(l.Id, l);
                            }
                        }
                    }
                }
            }
            else
            {
                System.debug('Email Template Email_Notification_2_to_Rep not found');
            }
            
            if(map_et.containsKey('Lead_Notification_Reminder_to_RM'))
            {
                for(Lead l : lstleadsecondreminder)
                {
                    //DateTime dt = l.CreationEmailDate__c.addHours(72);
                    DateTime dt = l.CreatedDate.addHours(72);
                    if(dt <= dt1)
                    {
                        if(usermap.containsKey(l.OwnerID))
                        {
                            String managerEmail = usermap.get(l.OwnerID).User_Manager_Email__c;
                            if(String.isBlank(managerEmail))
                            {
                                if(l.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('RMS_Lead_Type'))
                                {
                                    managerEmail = 'Corri.Finizza@covidien.com';//RMS GBU Admin
                                }
                                else
                                {
                                    managerEmail = 'Derek.Carless@covidien.com';//Surgical GBU Admin
                                }
                            }
                            String ownerEmail = usermap.get(l.OwnerID).Email;
                            lstemail.add(SendEmail(l.id, map_et.get('Lead_Notification_Reminder_to_RM'), managerEmail, ownerEmail));
                            l.SecondReminderSent__c = true;    
                            updateLeadMap.put(l.Id, l);
                        }
                    }
                }
            }
            else
            {
                System.debug('Email Template Lead_Notification_Reminder_to_RM not found');
            }
            //compile the emails
            List<Messaging.SingleEmailMessage> compEmail = compileEmails(lstemail);
            //send the emails that have been set-up
            if(compEmail.size() > 0)
            {
                try 
                {
                    if(!test.isRunningTest())
                    {
                        List<Messaging.SendEmailResult> results = Messaging.sendEmail(compEmail, false);
                        for(Messaging.SendEmailResult r : results)
                        {
                            if(!r.isSuccess())
                            {
                                List<Messaging.SendEmailError> errors = r.getErrors();
                                for(Messaging.SendEmailError err : errors)
                                {
                                    System.debug('Lead Notification Error for LeadId: ' + err.getTargetObjectId() + ' ; message: ' + err.getMessage());
                                    updateLeadMap.remove(err.getTargetObjectId());//don't update the lead if there was an error
                                }
                            }
                        }
                        update updateLeadMap.values();
                    }
                 }
                 catch(exception ex)
                 {
                    System.debug('ERROR: ' + ex.getMessage());
                 }
            }
        }
    }
}