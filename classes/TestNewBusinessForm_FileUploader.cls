@isTest(seeAllData=true)
private class TestNewBusinessForm_FileUploader
{
    static testMethod void Upload_test() 
    {
         RecordType rt=[Select Id from RecordType where DeveloperName='US_Med_Supplies_Opportunity' Limit 1];
         UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest2@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {

            Account acc = new Account(Name = 'TestAcc');
            insert acc;
            Opportunity opp = new Opportunity( RecordtypeID=rt.id,Name = 'TestOpp', AccountId = acc.id, Capital_Disposable__c = 'Capital', StageName = 'Identify', CloseDate = System.Today()+ 30);
            insert opp;
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='TestUpload'];
            Blob b=resourceList[0].Body;
            Test.startTest();
            Apexpages.currentPage().getParameters().put('id',opp.ID);
            PageReference pageRef= new PageReference('/apex/UploadNewBusinessForm?id='+opp.ID);
            
            Test.setCurrentPage(pageRef);
            NewBusinessForm_FileUploader_Controller controller = new NewBusinessForm_FileUploader_Controller();
            controller.contentFile = b;
            controller.nameFile = 'NBF Test File2.csv';
            controller.OppId = opp.ID;
            controller.ReadFile();
            controller.Cancel();
        }
         
                  
    }
    static testMethod void Upload_test1() 
    {
         RecordType rt=[Select Id from RecordType where DeveloperName='US_Med_Supplies_Opportunity' Limit 1];
         UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest3@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {

            Account acc = new Account(Name = 'TestAcc');
            insert acc;
            Opportunity opp = new Opportunity( RecordtypeID=rt.id,Name = 'TestOpp', AccountId = acc.id, Capital_Disposable__c = 'Capital', StageName = 'Identify', CloseDate = System.Today()+ 30);
            insert opp;
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='TestUpload'];
            Blob b=resourceList[0].Body;
            Test.startTest();
            Apexpages.currentPage().getParameters().put('id',opp.ID);
            PageReference pageRef= new PageReference('/apex/UploadNewBusinessForm?id='+opp.ID);
            
            Test.setCurrentPage(pageRef);
            NewBusinessForm_FileUploader_Controller controller = new NewBusinessForm_FileUploader_Controller();
            controller.contentFile = b;
            controller.nameFile = 'NBF Test File2.xlsx';
            controller.OppId = opp.ID;
            controller.ReadFile();
            controller.Cancel();
        }
         
                  
    }
    static testMethod void Upload_test2() 
    {
         RecordType rt=[Select Id from RecordType where DeveloperName='US_Med_Supplies_Opportunity' Limit 1];
         UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdttest4@mdttest.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) 
        {

            Account acc = new Account(Name = 'TestAcc');
            insert acc;
            Opportunity opp = new Opportunity( RecordtypeID=rt.id,Name = 'TestOpp', AccountId = acc.id, Capital_Disposable__c = 'Capital', StageName = 'Identify', CloseDate = System.Today()+ 30);
            insert opp;
            List<StaticResource> resourceList=[Select Name,Body from StaticResource where Name='TestUpload'];
            Blob b=resourceList[0].Body;
            Test.startTest();
            Apexpages.currentPage().getParameters().put('id',opp.ID);
            PageReference pageRef= new PageReference('/apex/UploadNewBusinessForm?id='+opp.ID);
            
            Test.setCurrentPage(pageRef);
            NewBusinessForm_FileUploader_Controller controller = new NewBusinessForm_FileUploader_Controller();
            controller.contentFile = null;
            controller.nameFile = '';
            controller.OppId = opp.ID;
            controller.ReadFile();
            controller.Cancel();
        }
         
                  
    }
}