@isTest
public class EMS_Test_Controller_Enter_Details {
    
    static testmethod void m1(){
        Approver_matrix__c app = new Approver_matrix__c();
        app.Name='SG';
        app.Marketing_Country_Controller_1__c = UserInfo.getUserId(); 
        app.Marketing_Country_Controller_2__c = UserInfo.getUserId();
        app.CCP_Coordinator_1__c = UserInfo.getUserId();
        app.CCP_Coordinator_2__c = UserInfo.getUserId();             
        app.GCC_Finance_1__c = UserInfo.getUserId();
        app.GCC_Finance_2__c = UserInfo.getUserId(); 
        app.GCC_Legal_1__c = UserInfo.getUserId();
        app.GCC_Legal_2__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_1__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_2__c = UserInfo.getUserId();
        app.GCC_PACE_1__c = UserInfo.getUserId(); 
        app.GCC_PACE_2__c = UserInfo.getUserId();
        app.GCC_R_D_1__c = UserInfo.getUserId();
        app.GCC_R_D_2__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_1__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_2__c = UserInfo.getUserId();
        app.Regional_Business_Head_1__c = UserInfo.getUserId();
        app.Regional_Business_Head_2__c = UserInfo.getUserId();
        app.Regional_PACE_Manager_1__c =UserInfo.getUserId(); 
        app.Regional_PACE_Manager_2__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_1__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_2__c = UserInfo.getUserId(); 
        app.ARO_CA_Director_1__c=UserInfo.getUserId();
        app.ARO_CA_Director_2__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_1__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_2__c= UserInfo.getUserId();
        EMSEndPointURL__c ed = new EMSEndPointURL__c(name='EMS_CreateCPAPDF',URL__c='https://cs11.salesforce.com');
        insert ed;
        insert app;
        
        Account acc =new Account();
        acc.name='test acc';
        acc.BillingCountry='Country';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName='First Name';
        con.LastName='Test Contact';
        con.accountid= acc.id;
        con.Country__c='Country';
        insert con;
        
        Contact con1 = new Contact();
        con1.FirstName='FirstName';
        con1.LastName='LastName';
        con1.Country__c='Country';
        con1.accountid= acc.id;
        insert con1;
        
        Employee__c emp = new Employee__c();
        emp.Name = 'Test';
        emp.User_Details__c= UserInfo.getUserId();
        insert emp;
        
        Profile p = [select id from profile where name='System Administrator'];   
        
        User u = new User(profileId = p.id, username = 'c@mixmaster.com', email = 'c@k.com', 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', country='SG',IsEMSEventOwner__c=true,
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                           alias='cspu', lastname='lastname');
        
        System.runAs(u){
        EMSMarketingGBU__c mycs = new EMSMarketingGBU__c(Name= 'SG;EBD');
            mycs.ARO_Marketing_Head_1__c= u.id;
            mycs.ARO_Marketing_Head_2__c=u.id;
            insert mycs;
        EMS_EVent__c EMSRec = new EMS_Event__c();
        EMSrec.Upload_External_Unique_Id__c= '1248';         
        EMSRec.GBU_Primary__c='EBD';
        EMSRec.Division_Primary__c='test';
        EMSRec.Start_Date__c = System.Today().addDays(22);
        EMSRec.End_Date__c = System.Today().addDays(45);
        EMSRec.Plan_Budget_Amt__c=50000;
        EMSRec.CPA_Required__c='Yes';
        EMSRec.Name= 'test event';
        EMSrec.Event_Native_Name__c='test event name';
        EMSRec.Location_Country__c='China';  
        EMSRec.Location_State__c='Shanghai';
        EMSRec.Expense_Type__c='Sales';
        EMSRec.Event_Type__c='CME';
        EMSRec.Training_conducted_in_Hosp_Training_Cent__c='Yes';
        EMSRec.HTC_Name__c='Test';
        EMSRec.Primary_COT__c='AB';
        EMSRec.Reoccurring_or_New__c='New';
        EMSRec.Event_Host__c='Covidien';
        EMSRec.Location_City__c='Test City';
        EMSRec.Venue__c= 'Test Venue'; 
        EMSRec.First_Payment_Date__c=System.Today();
        EMSRec.Max_Attendees__c=400;
        EMSRec.Any_recipient_s_who_are_KOL_s_HCP_s__c='YES';
        EMSRec.Any_HCP_Invitees_from_Outside_Asia__c='YES';
        EMSRec.Benefit_Summary__c='Test Summary';
        EMSRec.CCI__c='YES';
        EMSRec.Event_Owner_User__c = u.Id;
        EMSrec.Event_Status__c='Planned';
        EMSrec.GBU_Primary__c='EBD';
        EMSrec.currencyISOcode='USD';
        EMSrec.Is_Event_21_days_and_GCC_Approval_done__c=true;
        insert EMSrec;
        
                                  
        Event_Beneficiaries__c eb = new Event_Beneficiaries__c();
        eb.EMS_Event__c = emsrec.Id;
        eb.Invitee_Type__c='Budgeted Beneficiary';
        eb.type__c='Individual Beneficiary';
        eb.no_of_pax__c = 25;
        insert eb;
        
        Event_Beneficiaries__c eb1 = new Event_Beneficiaries__c();
        eb1.EMS_Event__c = emsrec.Id;
        eb1.Invitee_Type__c='Budgeted Beneficiary';
        eb1.type__c='Individual Beneficiary';
        eb1.no_of_pax__c = 25;
        insert eb1;
        
        Event_Beneficiaries__c eb2 = new Event_Beneficiaries__c();
        eb2.EMS_Event__c = emsrec.Id;
        eb2.Invitee_Type__c='Budgeted Beneficiary';
        eb2.type__c='Individual Beneficiary';
        eb2.no_of_pax__c = 25;
        insert eb2;
        
        Event_Beneficiaries__c eb3 = new Event_Beneficiaries__c();
        eb3.EMS_Event__c = emsrec.Id;
        eb3.Invitee_Type__c='Budgeted Beneficiary';
        eb3.type__c='Individual Beneficiary';
        eb3.no_of_pax__c = 25;
        insert eb3;
        
        Attachment att = new Attachment();
        att.parentId = EMSrec.Id;
        att.name='Event_Invitees_Budgeted.xls';
        att.body = blob.valueOf('1234');
        insert att;  
        
        EMSrec.No_of_Approved_eCPAs__c=1;
        EMSrec.CPA_Status__c='Completed';
        update EMSrec;
        
        try{
        EMSrec.No_of_Approved_eCPAs__c=1;
        EMSrec.CPA_Status__c='New Request';
        update EMSrec;
        }
        catch(Exception e){
        }
        
        
        EMSrec.CPA_Status__c='Pending CCP Coordinator Approval';
        update EMSrec;
        DivisionCountry__c div = new DivisionCountry__c(Name='All',Divisions__c='MAYA,GA,NV,RMS,ISS,Suture,Newport,BARRX,RA,GeneralMkt,EMID,Commercial,Other,Oridion,ANBR,Strategy & Commercialization,SYNETURE,SUPERD,PACE,HERN,PV');
        insert div;
        DivisionCountry__c div1 = new DivisionCountry__c(Name='SG',Divisions__c='MAYA,GA,NV,RMS,ISS,Suture,Newport,BARRX,RA,GeneralMkt,EMID,Commercial,Other,Oridion,ANBR,Strategy & Commercialization,SYNETURE,SUPERD,PACE,HERN,PV');
        insert div1;
        GBUCountry__c gblist = new GBUCountry__c(Name='All',GBUs__c='EBD,RMS,Others,VTMS,SURGICAL,NVPV');
        insert gblist;
        GBUCountry__c gblist1 = new GBUCountry__c(Name='SG',GBUs__c='EBD,RMS,Others,VTMS,SURGICAL,NVPV');
        insert gblist1;
        EMSrec.CPA_Status__c='Declined Approval';
        update EMSrec;
        
        Cost_Center__c cc = new Cost_center__c(name='Singapore');
        insert cc;
        
        Cost_Center__c cc1 = new Cost_center__c(name='India');
        insert cc1;
        
        GBU_Division_Ems__c gde = new GBU_Division_Ems__c();
        gde.EMS_Event__c = EMSrec.Id;
        gde.Cost_Center__c = cc.Id;
        insert gde; 
        
        GBU_Division_Ems__c gde1 = new GBU_Division_Ems__c();
        gde1.EMS_Event__c = EMSrec.Id;
        gde1.Cost_Center__c = cc.Id;
        insert gde1; 
        
           
        ApexPages.currentPage().getParameters().put('EventId',EMSrec.Id);
        ApexPages.currentPage().getParameters().put('step','2');
        ApexPAges.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
        EMS_Controller_EnterDetails obj = new EMS_Controller_EnterDetails(sc);
        obj.setSection1();
        obj.setSection2();
        obj.setSection3();
        obj.back();
        obj.Chosenstep=5;
        ApexPAges.StandardSetcontroller ssc = obj.setConbenef;
        obj.Chosenstep=2;
        ssc = obj.setConbenef;
        List<EMS_Controller_EnterDetails.EmpConWrapper> lstwrap = obj.Contacts;
        obj.getlstApportionReciepients();
        obj.copyActuals();
        obj.renderSearchFields();
        obj.option='1';
        
        obj.refresh();
        obj.getCountries();
        obj.getDivisionList();
        obj.getDivisionValues();
        obj.getGBUList();
        obj.getGBUValues();
        obj.getHasNext();
        obj.getHasPrevious();
        
        /*
        System.Test.Starttest();
        try{
            obj.getlstAddedBudgetReciepients();
        }catch(Exception e){
            
        }
        System.Test.stopTest();
        */
        
        obj.getlstApportionReciepients();
        
        
        obj.getPageNumber();
        obj.getSelectedCount();
        obj.getTotalPages();
        obj.getUploadedEBF();
        
        obj.deleteContact();
        
        obj.expandLTcolumns();
        obj.expandLodgingColumns();
        obj.expandATcolumns();
        obj.expandNameColumns();
        obj.expandOthersColumns();
        
        obj.collapseATColumns();
        obj.collapseLodgingColumns();
        obj.collapseLTColumns();
        obj.collapseNameColumns();
        obj.collapseOthersColumns();
        obj.showPopup();
        obj.showOthers();
        obj.showPopuplodging();
        obj.showPopupairtravel();
        obj.showPopuplodg();
        obj.showGifts();
        
        obj.costCenter='India';
        obj.SearchCostCenter();
        
        obj.checkAll();
        obj.checkAllbenef();
        obj.savecCentres();
        obj.savecCPercentages();
        obj.getCostCenters();
        obj.gbudivid=gde1.Id;
        obj.deletecc();
        obj.selectedsearchcriteria=1;
        obj.lastname='test';
        obj.SearchContacts();
        
        obj.selectedsearchcriteria=2;
        obj.Organization='test';
        obj.SearchContacts();
        
        obj.AddContacts();
        obj.doNext();
        obj.doPrevious();
        obj.first();
        obj.previous();
        obj.next();
        obj.last();
        
        obj.Exportcsvfile();
        obj.closePopup();
        
        obj.CopyActuals();
        obj.InsertInvitees();
        obj.LUIRecipients = new List<EMS_Controller_EnterDetails.EventBeneficiariesWrapper>();
        obj.LUIRecipients.add(new EMS_Controller_EnterDetails.EventBeneficiariesWrapper(eb1,true));
        obj.LUIRecipients.add(new EMS_Controller_EnterDetails.EventBeneficiariesWrapper(eb2,false));
        
        obj.CheckAllbenef();
        
        
        obj.option='2';
        obj.redirectpopuplodging();
        obj.redirectpopuptransport();
        obj.redirectpopupairtravel();
        List<EMS_Controller_EnterDetails.BeneficiariesWrapper> lstWraps = new List<EMS_Controller_EnterDetails.BeneficiariesWrapper>();
        lstWraps.add(new EMS_Controller_EnterDetails.BeneficiariesWrapper(eb1,true));
        lstWraps.add(new EMS_Controller_EnterDetails.BeneficiariesWrapper(eb2,false));
        
        List<EMS_Controller_EnterDetails.EmpConWrapper> lstEmp = new List<EMS_Controller_EnterDetails.EmpConWrapper>();
        lstEmp.add(new EMS_Controller_EnterDetails.EmpConWrapper(con,new Employee__c(),acc,true));
        
        test.startTest();
        obj.option='1';
        obj.redirectPopup();
        obj.redirectpopuplodging();
        obj.redirectpopupairtravel();
        //obj.redirectpopupgifts();
        obj.redirectpopupOthers();
        obj.redirectpopuptransport();
        obj.option='2';
        obj.redirectPopup();
        obj.redirectpopuplodging();
        obj.redirectpopupairtravel();
        
        obj.currentEMSRecord = EMSRec;
        obj.UpdatedRecord = EMSRec;
        obj.currentEMSRecord.Conversion_factor__c = 10;
        obj.gifts = 1000;
        obj.option = '1';
        obj.getlstApportionReciepients();
        obj.redirectpopupgifts();
        
        obj.option = '2';
        obj.redirectpopupOthers();
        
        //obj.redirectpopupOthers();
        //obj.redirectpopuptransport();
        
        obj.refresh();
        obj.eCPAPreview();
        obj.SearchContacts();
        
        obj.LUIRecipients = new List<EMS_Controller_EnterDetails.EventBeneficiariesWrapper>();
        obj.LUIRecipients.add(new EMS_Controller_EnterDetails.EventBeneficiariesWrapper(eb1,true));
        obj.LUIRecipients.add(new EMS_Controller_EnterDetails.EventBeneficiariesWrapper(eb2,false));
        obj.next();
        obj.last();
        obj.LUIRecipients.add(new EMS_Controller_EnterDetails.EventBeneficiariesWrapper(eb1,true));
        obj.LUIRecipients.add(new EMS_Controller_EnterDetails.EventBeneficiariesWrapper(eb2,false));
        obj.first();
        obj.previous();
        
        try{
            obj.SaveGBUDivisionData();
        }
        catch(Exception e){
        
        }
        try{
            obj.csvFileBody=blob.valueOf('FirstName,LastName,Country,Speaker,Meal,Check in date,Check out date,Rate per night,Bed Type,Taxi,Train,Coach,Pickup,Others,Air Travel,Tciket Type,Consultancy Fees,Gifts,Sponsor,Internal Order fee,Meeting Package,Meeting Room Rental,Booth Space,Advertisemnt,Miscellaneous\n,,,,,,,,,,,,,,,,,,,,,,,,\nFirstName,LastName,Country,true,25,1/1/2014,2/1/2014,35,Single,25,35,25,25,25,25,Economy,25,25,25,25,20,25,25,25,25');
            obj.readInviteescsvFile();
        }catch(Exception e){
        
        }
        
        obj.InsertInvitees();
        
        obj.SaveCCPercentages();
        obj.SaveFundRDescription();
        obj.SaveNonHCPExpenses();
        obj.SaveTransactions();
        obj.SaveFundRDescription();
        test.stoptest();
      }
    }
    static testmethod void m2(){
        Account acc =new Account();
        acc.name='test acc';
        acc.BillingCountry='Country';
        insert acc;
        
        EMSEndPointURL__c ed = new EMSEndPointURL__c(name='EMS_CreateCPAPDF',URL__c='https://cs11.salesforce.com');
        insert ed;
        
        Approver_matrix__c app = new Approver_matrix__c();
        app.Name='SG';
        app.Marketing_Country_Controller_1__c = UserInfo.getUserId(); 
        app.Marketing_Country_Controller_2__c = UserInfo.getUserId();
        app.CCP_Coordinator_1__c = UserInfo.getUserId();
        app.CCP_Coordinator_2__c = UserInfo.getUserId();             
        app.GCC_Finance_1__c = UserInfo.getUserId();
        app.GCC_Finance_2__c = UserInfo.getUserId(); 
        app.GCC_Legal_1__c = UserInfo.getUserId();
        app.GCC_Legal_2__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_1__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_2__c = UserInfo.getUserId();
        app.GCC_PACE_1__c = UserInfo.getUserId(); 
        app.GCC_PACE_2__c = UserInfo.getUserId();
        app.GCC_R_D_1__c = UserInfo.getUserId();
        app.GCC_R_D_2__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_1__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_2__c = UserInfo.getUserId();
        app.Regional_Business_Head_1__c = UserInfo.getUserId();
        app.Regional_Business_Head_2__c = UserInfo.getUserId();
        app.Regional_PACE_Manager_1__c =UserInfo.getUserId(); 
        app.Regional_PACE_Manager_2__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_1__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_2__c = UserInfo.getUserId(); 
        app.ARO_CA_Director_1__c=UserInfo.getUserId();
        app.ARO_CA_Director_2__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_1__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_2__c= UserInfo.getUserId();
        insert app;
        
        Contact con = new Contact();
        con.FirstName='First Name';
        con.LastName='Test Contact';
        con.accountid= acc.id;
        con.Country__c='Country';
        insert con;
        
        Contact con1 = new Contact();
        con1.FirstName='FirstName';
        con1.LastName='LastName';
        con1.Country__c='Country';
        con1.accountid= acc.id;
        insert con1;
        
        Employee__c emp = new Employee__c();
        emp.Name = 'Test';
        emp.User_Details__c= UserInfo.getUserId();
        insert emp;
        
        Profile p = [select id from profile where name='System Administrator'];   
        
        User u = new User(profileId = p.id, username = 'c@mixmaster.com', email = 'c@k.com', 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', country='SG',IsEMSEventOwner__c=true,
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname');
        
        System.runAs(u){
        EMSMarketingGBU__c mycs = new EMSMarketingGBU__c(Name= 'SG;EBD');
            mycs.ARO_Marketing_Head_1__c= u.id;
            mycs.ARO_Marketing_Head_2__c=u.id;
            insert mycs;
        EMS_EVent__c EMSRec = new EMS_Event__c();
        EMSrec.Upload_External_Unique_Id__c= '1248';         
        EMSRec.GBU_Primary__c='EBD';
        EMSRec.Division_Primary__c='test';
        EMSRec.Start_Date__c = System.Today().addDays(22);
        EMSRec.End_Date__c = System.Today().addDays(45);
        EMSRec.Plan_Budget_Amt__c=50000;
        EMSRec.CPA_Required__c='Yes';
        EMSRec.Name= 'test event';
        EMSrec.Event_Native_Name__c='test event name';
        EMSRec.Location_Country__c='China';  
        EMSRec.Location_State__c='Shanghai';
        EMSRec.Expense_Type__c='Sales';
        EMSRec.Event_Type__c='CME';
        EMSRec.Training_conducted_in_Hosp_Training_Cent__c='Yes';
        EMSRec.HTC_Name__c='Test';
        EMSRec.Primary_COT__c='AB';
        EMSRec.Reoccurring_or_New__c='New';
        EMSRec.Event_Host__c='Covidien';
        EMSRec.Location_City__c='Test City';
        EMSRec.Venue__c= 'Test Venue'; 
        EMSRec.First_Payment_Date__c=System.Today();
        EMSRec.Max_Attendees__c=400;
        EMSRec.Any_recipient_s_who_are_KOL_s_HCP_s__c='YES';
        EMSRec.Any_HCP_Invitees_from_Outside_Asia__c='YES';
        EMSRec.Benefit_Summary__c='Test Summary';
        EMSRec.CCI__c='YES';
        EMSRec.Event_Owner_User__c = u.Id;
        EMSrec.Event_Status__c='Planned';
        EMSrec.GBU_Primary__c='EBD';
        EMSrec.currencyISOcode='USD';
        EMSrec.Is_Event_21_days_and_GCC_Approval_done__c=true;
        insert EMSrec;
        
                               
        Event_Beneficiaries__c eb = new Event_Beneficiaries__c();
        eb.EMS_Event__c = emsrec.Id;
        eb.Invitee_Type__c='Budgeted Beneficiary';
        eb.type__c='Individual Beneficiary';
        eb.no_of_pax__c = 25;
        insert eb;
        
        Event_Beneficiaries__c eb1 = new Event_Beneficiaries__c();
        eb1.EMS_Event__c = emsrec.Id;
        eb1.Invitee_Type__c='Budgeted Beneficiary';
        eb1.type__c='Individual Beneficiary';
        eb1.no_of_pax__c = 25;
        insert eb1;
        
        Event_Beneficiaries__c eb2 = new Event_Beneficiaries__c();
        eb2.EMS_Event__c = emsrec.Id;
        eb2.Invitee_Type__c='Budgeted Beneficiary';
        eb2.type__c='Individual Beneficiary';
        eb2.no_of_pax__c = 25;
        insert eb2;
        
        Event_Beneficiaries__c eb3 = new Event_Beneficiaries__c();
        eb3.EMS_Event__c = emsrec.Id;
        eb3.Invitee_Type__c='Budgeted Beneficiary';
        eb3.type__c='Individual Beneficiary';
        eb3.no_of_pax__c = 25;
        insert eb3;
        
        Attachment att = new Attachment();
        att.parentId = EMSrec.Id;
        att.name='Event_Invitees_Budgeted.xls';
        att.body = blob.valueOf('1234');
        insert att;  
        
        EMSrec.No_of_Approved_eCPAs__c=1;
        EMSrec.CPA_Status__c='Completed';
        update EMSrec;
        
        try{
            EMSrec.No_of_Approved_eCPAs__c=1;
            EMSrec.CPA_Status__c='New Request';
            update EMSrec;
        }
        catch(Exception e){
        }
        
        
        EMSrec.CPA_Status__c='Pending CCP Coordinator Approval';
        update EMSrec;
        
        EMSrec.CPA_Status__c='Declined Approval';
        update EMSrec;
        
        Cost_Center__c cc = new Cost_center__c(name='Singapore');
        insert cc;
        
        Cost_Center__c cc1 = new Cost_center__c(name='India');
        insert cc1;
        
        GBU_Division_Ems__c gde = new GBU_Division_Ems__c();
        gde.EMS_Event__c = EMSrec.Id;
        gde.Cost_Center__c = cc.Id;
        insert gde; 
        
        GBU_Division_Ems__c gde1 = new GBU_Division_Ems__c();
        gde1.EMS_Event__c = EMSrec.Id;
        gde1.Cost_Center__c = cc.Id;
        insert gde1; 
        
           
            ApexPages.currentPage().getParameters().put('EventId',EMSrec.Id);
            ApexPages.currentPage().getParameters().put('step','1');
            ApexPAges.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
            EMS_Controller_EnterDetails obj = new EMS_Controller_EnterDetails(sc);
            obj.option='1';
            Integer a=obj.countofApportionment;
            obj.option='2';
            Integer b=obj.countofApportionment;
            obj.LUIRecipients = obj.lstUIRecipients;
            obj.savecCentres();
            obj.getCostCenters();
            obj.gbudivid=gde1.Id;
            obj.deletecc();
            obj.selectedsearchcriteria=1;
            obj.lastname='test';
            obj.SearchContacts();
            EMS_Event__c evt= obj.UpdatedRecord;
            obj.selectedsearchcriteria=2;
            obj.Organization='test';
            obj.SearchContacts();
            
            obj.AddContacts();
            obj.doNext();
            obj.doPrevious();
            obj.first();
            obj.previous();
            obj.next();
            obj.last();
            
            obj.Exportcsvfile();
            obj.closePopup();
        }
    }
}