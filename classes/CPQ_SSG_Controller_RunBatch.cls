public class CPQ_SSG_Controller_RunBatch {

    public void runDealBatch() {
        CPQ_SSG_DM_Batch_Deal batch = new CPQ_SSG_DM_Batch_Deal();
        Database.executeBatch(batch, 1);
    }

    public void runCOTBatch() {
        CPQ_SSG_DM_Batch_COT batch = new CPQ_SSG_DM_Batch_COT();
        Database.executeBatch(batch, 1);
    }

    public void runPriceBatch() {
        CPQ_SSG_DM_Batch_Price batch = new CPQ_SSG_DM_Batch_Price();
        Database.executeBatch(batch, 1);
    }
}