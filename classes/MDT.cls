public with sharing class MDT {
 
 /****************************************************************************************
   * Name    : MDT
   * Author  : Shawn Clark
   * Date    : 07/20/2015 
   * Purpose : Allows MITG Reps to search for their counterparts at MDT
 *****************************************************************************************/
 

    public transient list <MDT_RepDetail__c> acc {get;set;}
    public transient list <MDT_RepDetail__c> TherapyList {get;set;}
    public transient list <String> Therapy {get;set;}
    public transient list <Sales_Definitions__c> Terms {get;set;}
    public string searchstring {get;set;} 
    Public String SelectedAct2 {get;set;}
    Public String SelectedAct3 {get;set;}



    public transient String searchTerm {get; set;}  //Used by Autocomplete
    public String SelectedOrg {get; set;}  //Used by Autocomplete
    public transient String searchquery {get; set;}
    public String selectedAccountId {get; set;} 
    public String selectedAccountName {get; set;} 
    public String selectedZip {get; set;}
    public String selectedAccountCity {get; set;}
    public String selectedAccountState {get; set;}
    public String CurrentPlatform {get; set;}

   
    
    
    //ShowHide Toggle Variables
    Public boolean IsAccountPicklistVisible {get;set;}
    Public boolean IsAccountPicklistVisible2 {get;set;}
    Public boolean IsCityPicklistVisible {get;set;}
    Public boolean SearchBySplashScreen {get;set;}
    Public boolean SearchByPostalCode {get;set;}
    Public boolean SearchByAccountName {get;set;}
    Public boolean SearchByState {get;set;}
    Public boolean ShowResults {get;set;}
    Public boolean ShowOrgDefinitions {get;set;}
    Public boolean IsDisabled {get;set;}
    

    //Constructor
    Public MDT() {
        IsAccountPicklistVisible = False;
        IsAccountPicklistVisible2 = False;
        SearchBySplashScreen = True;
        SearchByPostalCode = False;
        SearchByAccountName = False;   
        SearchByState = False;
        IsCityPicklistVisible = False;
        ShowResults = False;
        ShowOrgDefinitions = False;
        IsDisabled = True;
        CurrentPlatform = 'Unknown';
    }
    
    //Autocomplete JSRemoting Account Name
    @RemoteAction
    public static List<ZipToAccount__c> searchAccount(String searchTerm, String selectedOrg) {
        System.debug('Account Name is: '+searchTerm );
        String Orgs = selectedOrg;
        List<ZipToAccount__c> accounts = Database.query('SELECT Cust_Num__c, Cust_Name__c, City__c, State__c, Postal_Code_5_Digit__c FROM ZipToAccount__c WHERE Org__c = :Orgs and  Cust_Name__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'  ORDER BY Cust_Name__c');
        return accounts;
    }     
    
    
    Public Void ResetPage() {
        IsAccountPicklistVisible = False;
        IsAccountPicklistVisible2 = False;
        SearchBySplashScreen = True;
        SearchByPostalCode = False;
        SearchByAccountName = False;   
        SearchByState = False;
        IsCityPicklistVisible = False;  
        ShowResults = False;   
        ShowOrgDefinitions = False; 
        selectedAccountState = '';
    }

    Public PageReference MainMenu() {
      PageReference main = new PageReference('/apex/MDT');
      selectedAccountState = '';
        return main;
    } 
    
      Public PageReference  IsMobile() {
        CurrentPlatform = 'Mobile';
        return null;
    } 
    
      Public PageReference IsDesktop() {
       CurrentPlatform = 'Desktop';
        return null;
    }        

    public List<SelectOption> getStates() {
        List<SelectOption> options = new List<SelectOption>();
           options.add(new SelectOption('','-- Select a State --'));  
           options.add(new SelectOption('AK', 'AK'));
           options.add(new SelectOption('AL', 'AL'));
           options.add(new SelectOption('AR', 'AR'));
           options.add(new SelectOption('AZ', 'AZ'));
           options.add(new SelectOption('CA', 'CA'));
           options.add(new SelectOption('CO', 'CO'));
           options.add(new SelectOption('CT', 'CT'));
           options.add(new SelectOption('DC', 'DC'));
           options.add(new SelectOption('DE', 'DE'));
           options.add(new SelectOption('FL', 'FL'));
           options.add(new SelectOption('GA', 'GA'));
           options.add(new SelectOption('HI', 'HI'));
           options.add(new SelectOption('IA', 'IA'));
           options.add(new SelectOption('ID', 'ID'));
           options.add(new SelectOption('IL', 'IL'));
           options.add(new SelectOption('IN', 'IN'));
           options.add(new SelectOption('KS', 'KS'));
           options.add(new SelectOption('KY', 'KY'));
           options.add(new SelectOption('LA', 'LA'));
           options.add(new SelectOption('MA', 'MA'));
           options.add(new SelectOption('MD', 'MD'));
           options.add(new SelectOption('ME', 'ME'));
           options.add(new SelectOption('MI', 'MI'));
           options.add(new SelectOption('MN', 'MN'));
           options.add(new SelectOption('MO', 'MO'));
           options.add(new SelectOption('MS', 'MS'));
           options.add(new SelectOption('MT', 'MT'));
           options.add(new SelectOption('NC', 'NC'));
           options.add(new SelectOption('ND', 'ND'));
           options.add(new SelectOption('NE', 'NE'));
           options.add(new SelectOption('NH', 'NH'));
           options.add(new SelectOption('NJ', 'NJ'));
           options.add(new SelectOption('NM', 'NM'));
           options.add(new SelectOption('NV', 'NV'));
           options.add(new SelectOption('NY', 'NY'));
           options.add(new SelectOption('OH', 'OH'));
           options.add(new SelectOption('OK', 'OK'));
           options.add(new SelectOption('OR', 'OR'));
           options.add(new SelectOption('PA', 'PA'));
           options.add(new SelectOption('RI', 'RI'));
           options.add(new SelectOption('SC', 'SC'));
           options.add(new SelectOption('SD', 'SD'));
           options.add(new SelectOption('TN', 'TN'));
           options.add(new SelectOption('TX', 'TX'));
           options.add(new SelectOption('UT', 'UT'));
           options.add(new SelectOption('VA', 'VA'));
           options.add(new SelectOption('VT', 'VT'));
           options.add(new SelectOption('WA', 'WA'));
           options.add(new SelectOption('WI', 'WI'));
           options.add(new SelectOption('WV', 'WV'));
           options.add(new SelectOption('WY', 'WY'));
           return options;
    }
      
  public List<SelectOption> getOrgs() {
        List<SelectOption> options = new List<SelectOption>();
           options.add(new SelectOption('','-- Select an Org --'));  
           options.add(new SelectOption('MDT', 'MDT'));   
           options.add(new SelectOption('MITG', 'MITG')); 
           return options;
   }   
        
        
    public list<SelectOption> getUniqueCity() {
       list<SelectOption> options = new list<SelectOption>();
       list<ZipToAccount__c> CityList = new List<ZipToAccount__c>();
       set<String> setCities = new Set<String>();
       list<String> UniqueCities = new List<String>();
      
       options.add(new SelectOption('','-- Select a City--'));
 
       CityList = [SELECT City__c FROM ZipToAccount__c WHERE Org__c = :selectedOrg and State__c like :selectedAccountState  ORDER BY City__c] ;
 
       for (Integer i = 0; i< CityList.size(); i++)
       {
       setCities.add(CityList[i].City__c); // Set dedups city values in list
       }
   
       UniqueCities.addAll(setCities);   

       for (Integer i = 0; i< UniqueCities.size(); i++)
       {
       options.add(new SelectOption(UniqueCities[i],UniqueCities[i] )); // contains distict accounts
       }
       return options; 
    } 


    //Get Accounts By Postal Code
    public list<SelectOption> getAccounts() {
       list<SelectOption> options = new list<SelectOption>();
       options.add(new SelectOption('','-- Select an Account --'));        
    
       for (list<ZipToAccount__c> rts : [SELECT Cust_Num__c, Cust_Name__c FROM ZipToAccount__c WHERE Org__c = :selectedOrg and Postal_Code_5_Digit__c = :searchstring  ORDER BY Cust_Name__c]) {
           for (ZipToAccount__c rt : rts) {
               options.add(new SelectOption(rt.Cust_Name__c, rt.Cust_Name__c));
            } 
       } 
       return options;
    } 
    
     //Get Accounts By City State

   public list<SelectOption> getAccountsByState() {
       searchstring = searchstring;
       String newSearchText = '%'+searchstring+'%';
   list<SelectOption> options = new list<SelectOption>();
    options.add(new SelectOption('','-- Select an Account --'));
   
   //Assign SOQL to this initially duplicated list
   List<ZipToAccount__c> lstAccount = new List<ZipToAccount__c>();
   
   // Convert the List above into a unique set of account strings
   Set<String> setAccountName = new Set<String>();

   //Convert the set back to a list of now unique strings
   List<String> UniqueActs = new List<String>();
 
   //Before we can add to a set, we must add to a List
   lstAccount = [SELECT Cust_Name__c FROM ZipToAccount__c WHERE Org__c = :selectedOrg and State__c = :selectedAccountState  and City__c = :selectedAccountCity  ORDER BY Cust_Name__c] ;
 
   //Add all the List values to the set (essentially deduping the Account Names)
   for (Integer i = 0; i< lstAccount.size(); i++)
   {
    setAccountName.add(lstAccount[i].Cust_Name__c); // contains distict accounts
   }
   
   //Shortcut command to add all Set values to a List
   UniqueActs.addAll(setAccountName);   

   //Final Step is to add the List values to the Options list
   for (Integer i = 0; i< UniqueActs.size(); i++)
   {
    options.add(new SelectOption(UniqueActs[i],UniqueActs[i] )); // contains distict accounts
   }

    
   //   options.add(new SelectOption(UniqueActs[0],UniqueActs[0] ));
       
   return options; 
   }  
    
    

   //**************Search By Postal Code ********************
   
    public void searchMain(){  
       set<String> setTherapies = new Set<String>();
       
       TherapyList = [SELECT Therapy__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Postal_Code_5_Digit__c = :searchstring and Cust_Name__c = :SelectedAct2 Order by Therapy__c];

       for (Integer i = 0; i< TherapyList.size(); i++)
       {
       setTherapies.add(TherapyList[i].Therapy__c); // Set to dedup Therapy List
       }
      
       // convert the set into a string array  
       Therapy = new String[setTherapies.size()];
       Integer i = 0;
         for (String t : setTherapies) { 
             Therapy[i] = t;
             i++;
       }
      searchquery='SELECT BU__c,City__c,Cust_Name__c,Job_Title__c,Name__c,Postal_Code_5_Digit__c ,State__c, Therapy__c, Work_Phone__c, Cell_Phone__c, Email__c, Therapy_Description__c, Org__c, Photo__c, User_GUID__c, MGR_Guid__c, MGR_Name__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Postal_Code_5_Digit__c = :searchstring and Cust_Name__c = :SelectedAct2 Order by Therapy__c, Name__c  LIMIT 1000';
      acc= Database.query(searchquery);
      ShowResults = True;  
    }
    
   //**************Search By City/State ********************
   
    public void searchByState(){  
       set<String> setTherapies = new Set<String>();
       
       TherapyList = [SELECT Therapy__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Cust_Name__c = :SelectedAct2 and State__c = :selectedAccountState  and City__c = :selectedAccountCity Order by Therapy__c] ;
   
       for (Integer i = 0; i< TherapyList.size(); i++)
       {
       setTherapies.add(TherapyList[i].Therapy__c); // contains distict accounts
       }
 
      Therapy = new String[setTherapies.size()];
      Integer i = 0;
         for (String t : setTherapies) { 
             Therapy[i] = t;
             i++;
      }

       searchquery='SELECT City__c,Cust_Name__c,Job_Title__c,Name__c,Postal_Code_5_Digit__c ,State__c, Therapy__c, Work_Phone__c, Cell_Phone__c, Email__c, Therapy_Description__c, Org__c, Photo__c, User_GUID__c, MGR_Guid__c, MGR_Name__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Cust_Name__c = :SelectedAct2 and State__c = :selectedAccountState  and City__c = :selectedAccountCity Order by Therapy__c, Name__c  LIMIT 1000';
       acc= Database.query(searchquery); 
       ShowResults = True; 
    }   
    
    

   //**************Search By Account Name ********************
   
    public void RetrieveRepByAccount(){  
       set<String> setTherapies = new Set<String>();
       
       TherapyList = [SELECT Therapy__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Cust_Num__c = :selectedAccountId and City__c = :selectedAccountCity and State__c = :selectedAccountState and Cust_Name__c = :selectedAccountName and Postal_Code_5_Digit__c = :selectedZip Order by Therapy__c, Name__c];

       for (Integer i = 0; i< TherapyList.size(); i++)
       {
       setTherapies.add(TherapyList[i].Therapy__c); // Set to dedup Therapy List
       }
      
       // convert the set into a string array  
       Therapy = new String[setTherapies.size()];
       Integer i = 0;
         for (String t : setTherapies) { 
             Therapy[i] = t;
             i++;
       }
      searchquery='SELECT BU__c,City__c,Cust_Name__c,Job_Title__c,Name__c,Postal_Code_5_Digit__c ,State__c, Therapy__c, Work_Phone__c, Cell_Phone__c, Email__c, Therapy_Description__c, Org__c, Photo__c, User_GUID__c, MGR_Guid__c, MGR_Name__c FROM MDT_RepDetail__c Where Org__c = :selectedOrg and Cust_Num__c = :selectedAccountId and City__c = :selectedAccountCity and State__c = :selectedAccountState and Cust_Name__c = :selectedAccountName and Postal_Code_5_Digit__c = :selectedZip Order by Therapy__c, Name__c  LIMIT 1000';
      acc= Database.query(searchquery);
      ShowResults = True;  
    }   
    


   //Toggle Sections Methods
    
    public Void ShowPostalCodeSearch() {
       SearchByPostalCode = True;
       SearchBySplashScreen = False;
       
       VF_Tracking__c newTracking = new VF_Tracking__c(
       User__c = UserInfo.getUserId(),
       VF_Page_Visited__c = 'MDT - By Postal Code',
       Platform__c = CurrentPlatform,
       Date_Time_Visited__c = datetime.now());
       insert newTracking;
       
    }
    
    public Void ShowOrgDefinitions() {
       ShowOrgDefinitions = True;
       SearchBySplashScreen = False;
       searchquery='SELECT Term__c, Definition__c, Examples__c, Resources__c FROM Sales_Definitions__c';
       Terms = Database.query(searchquery); 
       
       VF_Tracking__c newTracking = new VF_Tracking__c(
       User__c = UserInfo.getUserId(),
       VF_Page_Visited__c = 'MDT - Definitions',
       Platform__c = CurrentPlatform,
       Date_Time_Visited__c = datetime.now());
       insert newTracking;
       
    }
    
    public Void ShowSearchByCityandState() {
       SearchByState = True;
       SearchBySplashScreen = False;
       
       VF_Tracking__c newTracking = new VF_Tracking__c(
       User__c = UserInfo.getUserId(),
       VF_Page_Visited__c = 'MDT - By State',
       Platform__c = CurrentPlatform,
       Date_Time_Visited__c = datetime.now());
       insert newTracking;
       
    } 

    public Void TrackAccountSearch() {
       VF_Tracking__c newTracking = new VF_Tracking__c(
       User__c = UserInfo.getUserId(),
       VF_Page_Visited__c = 'MDT - By Account',
       Platform__c = CurrentPlatform,
       Date_Time_Visited__c = datetime.now());
       insert newTracking;
       
    }  
    
           //Tracking 
   
       public pageReference trackVisits(){
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1]; 
        String strurl_abb = strurl.substringBefore('?');
        
        VF_Tracking__c newTracking = new VF_Tracking__c(
            User__c = UserInfo.getUserId(),
            VF_Page_Visited__c = strurl_abb,
            Date_Time_Visited__c = datetime.now());
        insert newTracking;
        return null;
    }    
       
    
    public Void UpdateForm() {
       IsAccountPicklistVisible = True;
    }
    
    public Void ShowAccounts() {
       IsAccountPicklistVisible2 = True;
    }   
      
    public Void ShowCities() {
       IsCityPicklistVisible = True;
    }      

}