/*
    DO NOT PUT ANY LOGIC IN THIS CLASS!!!!
    - Creat a main() method in your class and call it from this
      class and pass ALL of the trigger context values.

COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.	Id is just a unique value to distinguish changes made on the same date
5.	If there are multiple related Jiras, add them on the next line
6.	Combine the Date & Id and use it to mark changes related to the entry
	// YYYYMMDD	A   PAB			CPR-000	Updated comments section
    Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                 	Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        	PAB             Medtronic.com
Isaac Lewis				IL              Statera.com
Bryan Fry			 	BF				Statera.com
Henry Noerdlinger		HN				Medtronic.com

MODIFICATION HISTORY
====================
Date        Id  Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161118	A	PAB			AV-1	Created

*/     
public class cpqParticipatingFacility_Lineitem_t extends sObject_t
{
	private static cpqParticipatingFacility_Lineitem_u bulkifiedPFL = new cpqParticipatingFacility_Lineitem_u();
	
    public override void entry(
        boolean isExecuting,
        boolean isInsert,
        boolean isUpdate,
        boolean isDelete,
        boolean isBefore,
        boolean isAfter,
        boolean isUndelete,
        List<sObject> newList,
        Map<Id, sObject> newMap,
        List<sObject> oldList,
        Map<Id, sObject> oldMap,
        integer size
    )
    {
        bulkifiedPFL.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size);
    }
}