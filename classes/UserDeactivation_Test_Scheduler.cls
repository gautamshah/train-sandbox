@isTest
 private class UserDeactivation_Test_Scheduler {


	static testMethod void testschedule() 
		{
		
			Test.StartTest();
			
			UserDeactivation_ScheduledClass sh1 = new UserDeactivation_ScheduledClass();
		
					String CRON_EXP = '0 0 0 1 1 ? 2025';  
                    String jobId = System.schedule('testScheduledApex', CRON_EXP, new UserDeactivation_ScheduledClass() );

                    CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];

                    System.assertEquals(CRON_EXP, ct.CronExpression); 
                    System.assertEquals(0, ct.TimesTriggered);
                    System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));

			
			Test.stopTest(); 
		
		}

}