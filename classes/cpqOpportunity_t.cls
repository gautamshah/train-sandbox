public class cpqOpportunity_t
{
    //to prevent a method from executing a second time add the name of the method to the 'executedMethods' set
    public static Set<String> executedMethods = new Set<String>();

    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<Opportunity> newList,
            Map<Id, Opportunity> newMap,
            List<Opportunity> oldList,
            Map<Id, Opportunity> oldMap,
            integer size
        )
    {
        if(!executedMethods.contains('capture'))
        {
            new cpqCycleTime_Opportunity_u().capture(
                isExecuting,
                isInsert,
                isUpdate,
                isDelete,
                isBefore,
                isAfter,
                isUndelete,
                (List<sObject>)newList,
                (Map<Id, sObject>)newMap,
                (List<sObject>)oldList,
                (Map<Id, sObject>)oldMap,
                size
            );

			executedMethods.add('capture'); 
        }
    }
}