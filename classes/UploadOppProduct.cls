/****************************************************************************************
 * Name    : UploadOppProduct 
 * Author  : Bill Shan
 * Date    : 15/05/2014 
 * Purpose : Upload products into opportunity
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE            AUTHOR               CHANGE
 * 1 December 2015  Amogh Ghodke  Updated logic to have extra 2 columns for Upload file
 *****************************************************************************************/
public with sharing class UploadOppProduct {

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public List<IssueRecord> issueItems{get; set;}
        
    private final Opportunity opp;
    private String[] filelines = new String[]{};
    private string fileData;
    
    class IssueRecord{
        public string SKU{get;set;}
        public string Quantity{get;set;}
        public string SalesPrice{get;set;}
        public string BiddingNumber{get;set;}
        public string Error{get;set;}
        public IssueRecord(string str1, String str2, String str3)
        {
           SKU = str1;
           Quantity = str2;
           Error = str3;
        }
        public IssueRecord(string str1, String str2, String str3, String str4, String str5)
        {
            SKU = str1;
            Quantity = str2;
            Error = str3;
            SalesPrice = str4;
            BiddingNumber = str5;
            
        }
    }
    
    public UploadOppProduct(ApexPages.StandardController stdController) {
        
        String oppId = ApexPages.currentPage().getParameters().get('oppId');
        this.opp=[Select Id,Pricebook2Id,CurrencyIsoCode,Type From Opportunity where Id=:oppId];
        this.issueItems = new List<IssueRecord>();
    }
    
    public Pagereference ReadFile()
    {
        //declare a array of Upload records
            
        if(contentFile==null)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_choose_a_valid_file);
            ApexPages.addMessage(errormsg);
        }
        else
        {
            try{
                fileData=contentFile.toString();
            }
            catch (Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later));
                return null;
            }
            // Break the content by CRLF
            
            filelines = fileData.split('\n');
            
            if(filelines.size()<2||(filelines[0].split('\t').size()<2))
            {   system.debug('filelines.size()'+filelines.size()+'**'+filelines[0].split('\t').size()+'**');
                system.debug('filelines ##'+filelines);
                system.debug('filelines.[0].split #'+filelines[0].split('\t'));
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,label.Please_check_data_in_file_for_correct_format));
            }
            else
            {
                List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
                Map<String, PricebookEntry> sku_pbeId = new Map<String, PricebookEntry>(); 
                
                for(PricebookEntry pe : [Select Id, ProductCode,UnitPrice From PricebookEntry 
                                         Where IsActive = true and CurrencyIsoCode = :opp.CurrencyIsoCode and Pricebook2Id = :opp.Pricebook2Id])
                {
                    if(!sku_pbeId.containskey(pe.ProductCode))
                        sku_pbeId.put(pe.ProductCode, pe);
                }
                
                for (Integer i=0;i<filelines.size();i++)
                {
                    if(i==0)
                        continue;
                    
                    String[] inputvalues = new String[]{};
           
                    //split the Record by fields based on the delimiter
                    inputvalues = filelines[i].split('\t');
                    system.debug('Number of Columns *'+inputvalues.size());
                    if(inputvalues.size()<2)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,label.File_format_Error_Number_of_columns_are_less_than_expected));
                        return null;
                    }
                    else
                    {
                        string strError = '';
                        decimal q = 0;
                        decimal c = 0;
                        decimal b = 0;
                                                
                        if(!sku_pbeId.containskey(inputvalues[0].trim()))
                        {
                            strError = Label.Product_Code_not_available_in_Pricebook;
                        }
                        if(inputvalues[1].trim()!=null && inputvalues[1].trim()!='')
                        {
                            try{
                                q = decimal.valueOf(inputvalues[1].trim());
                                if(opp.Type == 'At Risk' && q >0)
                                    strError += Label.Quantity_should_be_negative_for_at_risk_opportunity;
                            }
                            catch(Exception e)
                            {
                                strError += Label.Quantity_is_invalid;  
                            }
                        }
                        if(inputvalues.size()>2)
                        {
                          if(inputvalues[2].trim()!=null && inputvalues[2].trim()!='')
                          { 
                            try{
                                c = decimal.valueOf(inputvalues[2].trim());
                                if(c < 0)
                                    strError += Label.Sales_Price_cannot_be_negative_for_opportunity_product;
                            }
                            catch(Exception e)
                            {
                                strError += Label.Sales_Price_is_invalid;  
                            }
                           
                         }
                        }
                        if(inputvalues.size()>3)
                        {
                           if(inputvalues[3].trim()!=null && inputvalues[3].trim()!='')
                           { 
                            try{
                                b = decimal.valueOf(inputvalues[3].trim());
                                if(b < 0)
                                    strError += Label.Bidding_Number_cannot_be_negative_for_opportunity_product;
                            }
                            catch(Exception e)
                            {
                                strError += Label.Bidding_Number_is_invalid;  
                            }
                           
                          }
                        }
                        
                        
                        if(strError=='')
                        {
                            OpportunityLineItem oli = new OpportunityLineItem();
                            oli.OpportunityId = opp.Id;
                            oli.PricebookEntryId = sku_pbeId.get(inputvalues[0].trim()).Id;
                            oli.Quantity =  q;
                            
                            if(c > 0)
                               oli.UnitPrice = c;
                            else
                                oli.UnitPrice = sku_pbeId.get(inputvalues[0].trim()).UnitPrice;
                            
                            if(b > 0)
                               oli.Bidding_Number__c = b;
                            
                            oppLineItems.add(oli);
                        }
                        else
                        {  if(inputvalues.size()==2)
                               issueItems.add(new IssueRecord(inputvalues[0].trim(),inputvalues[1].trim(),strError));
                           if(inputvalues.size()==4)
                               issueItems.add(new IssueRecord(inputvalues[0].trim(),inputvalues[1].trim(),strError,inputvalues[2].trim(),inputvalues[3].trim()));
                        }
                    }
                }
                try 
                {
                    if (oppLineItems.size()>0)
                    {
                        insert oppLineItems;
                    }
                }
                catch (Exception e)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
                    return null;
                    
                }
                    
                if(issueItems.size()>=0)
                    return new PageReference('/'+opp.Id);
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Below_data_is_not_valid));
                    return null;
                }
                
            }
        }
        return null;
    }
    
    public boolean gethasInvalidRecords()
    {
        if(issueItems.size()>0) return true; 
        return false;
    }
    
    public PageReference goBacktoOpp()
    {
        return new PageReference('/'+opp.Id);
    }
}