@isTest
private class TestEstablishTerrRelatOnCreateUpdate {

    static testMethod void runTest() {
    
       Test.startTest();
        Territory t = new Territory(Name = 'test 1 x'
                                   , ExternalParentTerritoryID__c = 'test1-12345'
                                   , Custom_External_TerritoryID__c = 'test1-54321'); 
        insert t;
        
        System.assertEquals(true, [Select flagForProcessing__c from Territory where name = 'test 1 x'].flagForProcessing__c);
        
        t.flagForProcessing__c = false;
        
        update t;
        
        System.assertEquals(false, [Select flagForProcessing__c from Territory where name = 'test 1 x'].flagForProcessing__c);
        
        t.ExternalParentTerritoryID__c = 'test1-67891';
        
        update t; 
        
        System.assertEquals(true, [Select flagForProcessing__c from Territory where name = 'test 1 x'].flagForProcessing__c);
        
        Test.stopTest();
        
        
       
        
    }
}