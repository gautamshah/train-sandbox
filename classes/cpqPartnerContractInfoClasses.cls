/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed  80 => *                            120 => *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry

YYYYMMDD-A	PAB			CPR-000	Updated comments section

7.  Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com
Gautam Shah				GS				Medtronic.com

MODIFICATION HISTORY
====================
Date-Id		Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
Nov 21, 2017-A	PAB					Created at 11:44:37 AM by paul.berglund

*/
public class cpqPartnerContractInfoClasses
{

        public class ArrayOfContractDealerType {
        public cpqPartnerContractInfoClasses.ContractDealerType[] ContractDealerType;
        private String[] ContractDealerType_type_info = new String[]{'ContractDealerType','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'ContractDealerType'};
    }
    public class ContractInfoType {
        public webServiceConstants.PropertyChangedEventHandler PropertyChanged;
        public String AcquisitionCode;
        public String agreementNumberField;
        public String alternateSiteField;
        public String commitmentFormRequiredField;
        public String commitmentTypeField;
        public String companyField;
        public String contactField;
        public String corporateContractField;
        public String currencyCodeField;
        public DateTime dateCreatedField;
        public String divisionField;
        public DateTime downloadDateField;
        public DateTime effectiveDateField;
        public DateTime expirationDateField;
        public String groupCodeField;
        public DateTime lastMaintenanceDateField;
        public String lastMaintenanceUserField;
        public Integer levelField;
        public String lineOfBusinessField;
        public String nameField;
        public String nextContractField;
        public DateTime originalEndDateField;
        public String pricingGroupField;
        public String pricingIdField;
        public String priorContractField;
        public String renewableFlagField;
        public String restrictDealerServiceField;
        public String rootContractField;
        public String statusField;
        public String territoryField;
        public String tierGroupField;
        public String typeField;
        public DateTime uploadDateField;
        private String[] PropertyChanged_type_info = new String[]{'PropertyChanged','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] AcquisitionCode_type_info = new String[]{'acquisitionCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] agreementNumberField_type_info = new String[]{'agreementNumberField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] alternateSiteField_type_info = new String[]{'alternateSiteField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] commitmentFormRequiredField_type_info = new String[]{'commitmentFormRequiredField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] commitmentTypeField_type_info = new String[]{'commitmentTypeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] companyField_type_info = new String[]{'companyField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] contactField_type_info = new String[]{'contactField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] corporateContractField_type_info = new String[]{'corporateContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] currencyCodeField_type_info = new String[]{'currencyCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] dateCreatedField_type_info = new String[]{'dateCreatedField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] divisionField_type_info = new String[]{'divisionField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] downloadDateField_type_info = new String[]{'downloadDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] effectiveDateField_type_info = new String[]{'effectiveDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] expirationDateField_type_info = new String[]{'expirationDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] groupCodeField_type_info = new String[]{'groupCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] lastMaintenanceDateField_type_info = new String[]{'lastMaintenanceDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] lastMaintenanceUserField_type_info = new String[]{'lastMaintenanceUserField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] levelField_type_info = new String[]{'levelField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] lineOfBusinessField_type_info = new String[]{'lineOfBusinessField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] nameField_type_info = new String[]{'nameField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] nextContractField_type_info = new String[]{'nextContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] originalEndDateField_type_info = new String[]{'originalEndDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] pricingGroupField_type_info = new String[]{'pricingGroupField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] pricingIdField_type_info = new String[]{'pricingIdField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] priorContractField_type_info = new String[]{'priorContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] renewableFlagField_type_info = new String[]{'renewableFlagField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] restrictDealerServiceField_type_info = new String[]{'restrictDealerServiceField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] rootContractField_type_info = new String[]{'rootContractField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] statusField_type_info = new String[]{'statusField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] territoryField_type_info = new String[]{'territoryField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] tierGroupField_type_info = new String[]{'tierGroupField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] typeField_type_info = new String[]{'typeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] uploadDateField_type_info = new String[]{'uploadDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info =
            new String[]
            {
                'PropertyChanged',
                'AcquisitionCode','agreementNumberField','alternateSiteField','commitmentFormRequiredField','commitmentTypeField','companyField','contactField','corporateContractField','currencyCodeField','dateCreatedField','divisionField','downloadDateField','effectiveDateField','expirationDateField','groupCodeField','lastMaintenanceDateField','lastMaintenanceUserField','levelField','lineOfBusinessField','nameField','nextContractField','originalEndDateField','pricingGroupField','pricingIdField','priorContractField','renewableFlagField','restrictDealerServiceField','rootContractField','statusField','territoryField','tierGroupField','typeField','uploadDateField'};
    }
    public class ContractInfoRequestType {
        public webServiceConstants.PropertyChangedEventHandler PropertyChanged;
        public String contractNumberField;
        public Boolean getContractItemsField;
        public Boolean getDealerServiceField;
        public String identityTokenField;
        public String maximumResponseRecordsField;
        private String[] PropertyChanged_type_info = new String[]{'PropertyChanged','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] contractNumberField_type_info = new String[]{'contractNumberField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] getContractItemsField_type_info = new String[]{'getContractItemsField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] getDealerServiceField_type_info = new String[]{'getDealerServiceField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] identityTokenField_type_info = new String[]{'identityTokenField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] maximumResponseRecordsField_type_info = new String[]{'maximumResponseRecordsField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'PropertyChanged','contractNumberField','getContractItemsField','getDealerServiceField','identityTokenField','maximumResponseRecordsField'};
    }
    public class ContractDealerType {
        public webServiceConstants.PropertyChangedEventHandler PropertyChanged;
        public Integer billToField;
        public String cityField;
        public DateTime effectiveDateField;
        public DateTime expirationDateField;
        public String nameField;
        public Integer shipToField;
        public String stateField;
        public String zipCodeField;
        private String[] PropertyChanged_type_info = new String[]{'PropertyChanged','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] billToField_type_info = new String[]{'billToField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] cityField_type_info = new String[]{'cityField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] effectiveDateField_type_info = new String[]{'effectiveDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] expirationDateField_type_info = new String[]{'expirationDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] nameField_type_info = new String[]{'nameField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] shipToField_type_info = new String[]{'shipToField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] stateField_type_info = new String[]{'stateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] zipCodeField_type_info = new String[]{'zipCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'PropertyChanged','billToField','cityField','effectiveDateField','expirationDateField','nameField','shipToField','stateField','zipCodeField'};
    }
    public class ArrayOfContractItemType {
        public cpqPartnerContractInfoClasses.ContractItemType[] ContractItemType;
        private String[] ContractItemType_type_info = new String[]{'ContractItemType','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'ContractItemType'};
    }
    public class ContractItemType {
        public webServiceConstants.PropertyChangedEventHandler PropertyChanged;
        public Integer alternateUOMCaseQuantityField;
        public String alternateUOMField;
        public Decimal dealerNetField;
        public Decimal directPriceField;
        public DateTime effectiveDateField;
        public DateTime expirationDateField;
        public String itemDescriptionField;
        public String itemNumberField;
        public Decimal marginField;
        public Integer quantityBreakField;
        public String salesClassField;
        public Decimal suggestedResaleField;
        public String unitOfMeasureField;
        private String[] PropertyChanged_type_info = new String[]{'PropertyChanged','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] alternateUOMCaseQuantityField_type_info = new String[]{'alternateUOMCaseQuantityField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] alternateUOMField_type_info = new String[]{'alternateUOMField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] dealerNetField_type_info = new String[]{'dealerNetField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] directPriceField_type_info = new String[]{'directPriceField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] effectiveDateField_type_info = new String[]{'effectiveDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] expirationDateField_type_info = new String[]{'expirationDateField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] itemDescriptionField_type_info = new String[]{'itemDescriptionField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] itemNumberField_type_info = new String[]{'itemNumberField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] marginField_type_info = new String[]{'marginField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] quantityBreakField_type_info = new String[]{'quantityBreakField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','false'};
        private String[] salesClassField_type_info = new String[]{'salesClassField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] suggestedResaleField_type_info = new String[]{'suggestedResaleField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] unitOfMeasureField_type_info = new String[]{'unitOfMeasureField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'PropertyChanged','alternateUOMCaseQuantityField','alternateUOMField','dealerNetField','directPriceField','effectiveDateField','expirationDateField','itemDescriptionField','itemNumberField','marginField','quantityBreakField','salesClassField','suggestedResaleField','unitOfMeasureField'};
    }
    
    public class ContractInfoResponseType {
        public webServiceConstants.PropertyChangedEventHandler PropertyChanged;
        public cpqPartnerContractInfoClasses.ContractInfoType contractInfoField;
        public cpqPartnerContractInfoClasses.ArrayOfContractDealerType dealersField;
        public String errorCodeField;
        public String errorDescriptionField;
        public cpqPartnerContractInfoClasses.ArrayOfContractItemType itemsField;
        private String[] PropertyChanged_type_info = new String[]{'PropertyChanged','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] contractInfoField_type_info = new String[]{'contractInfoField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] dealersField_type_info = new String[]{'dealersField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] errorCodeField_type_info = new String[]{'errorCodeField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] errorDescriptionField_type_info = new String[]{'errorDescriptionField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] itemsField_type_info = new String[]{'itemsField','http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.Proxy.Services.ContractInfo','true','false'};
        private String[] field_order_type_info = new String[]{'PropertyChanged','contractInfoField','dealersField','errorCodeField','errorDescriptionField','itemsField'};
        }

        public class Item 
	    {
	        public Integer alternateUOMCaseQuantityField {get; set;}
	        public String alternateUOMField {get; set;}
	        public Decimal priceField {get; set;}
	        public DateTime effectiveDateField {get; set;}
	        public DateTime expirationDateField {get; set;}
	        public String itemDescriptionField {get; set;}
	        public String itemNumberField {get; set;}
	        public Decimal marginField {get; set;}
	        public Integer quantityBreakField {get; set;}
	        public String salesClassField {get; set;}
	        public Decimal suggestedResaleField {get; set;}
	        public String unitOfMeasureField {get; set;}
	        
	        public Item(cpqPartnerContractInfoClasses.ContractItemType item)
	        {
                this.alternateUOMCaseQuantityField = item.alternateUOMCaseQuantityField;
                this.alternateUOMField = item.alternateUOMField;
                this.itemDescriptionField = item.itemDescriptionField;
                this.itemNumberField = item.itemNumberField;
                this.marginField = item.marginField;
                this.quantityBreakField = item.quantityBreakField;
                this.salesClassField = item.salesClassField;
                this.suggestedResaleField = item.suggestedResaleField;
                this.effectiveDateField = item.effectiveDateField;
                this.expirationDateField = item.expirationDateField;
                this.unitOfMeasureField = item.unitOfMeasureField;
                this.priceField = item.dealerNetField;
  
                if (this.priceField != item.directPriceField && item.directPriceField != 0)
                {
                  this.priceField = item.directPriceField; 
                }
	        }
	    }
    
        public class ContractInfo { 
	        public String ContractName {get; set;}
	        public String RootContractNum {get; set;}
	        public String CommitmentType {get; set;}
	        public String errorCodeField {get; set;}
	        public Date startDate {get; set;}
	        public Date endDate {get; set;}
            public List<String> relatedContracts {get; set;}
            
            public Map<Integer, cpqPartnerContractInfoClasses.ContractDealerType> dealerMap {get; set;} 
	        public List<cpqPartnerContractInfoClasses.ContractDealerType> dealerList 
            {
                get
                {
                   return new List<cpqPartnerContractInfoClasses.ContractDealerType>(dealerMap.values());
               }
             }

            public Map<String, cpqPartnerContractInfoClasses.Item> itemMap {get; set;} 
            public List<cpqPartnerContractInfoClasses.Item> itemList 
            {
                get
                {
                    return new List<cpqPartnerContractInfoClasses.Item>(itemMap.values());
                }
            }
            
            public String effectiveDateField 
            {
		        get
		        {
		            if (this.startDate != null || this.endDate != null)
		            {
		                return '<B>' + (this.startDate == null ? '' : this.startDate.format()) 
		                                          + ' - ' + (this.endDate == null ? '' : this.endDate.format()) + '</B>';  
		            }
		            return null;
		        }
		        set;
            }

        //constructors
        public ContractInfo() {
            relatedContracts = new List<String>();
            dealerMap = new Map<Integer, cpqPartnerContractInfoClasses.ContractDealerType>();
            itemMap = new Map<String, cpqPartnerContractInfoClasses.Item>();
        }
            
        public ContractInfo(cpqPartnerContractInfoClasses.ContractInfoResponseType response)
        {
            this();
            this.ContractName = response.contractInfoField.nameField;
            this.RootContractNum = response.contractInfoField.rootContractField;
            this.CommitmentType = response.contractInfoField.commitmentTypeField;
            this.errorCodeField = response.errorCodeField;
            
            //dates
            if (response.contractInfoField.effectiveDateField != null && response.contractInfoField.effectiveDateField.year() != 1)
            {
                this.startDate = response.contractInfoField.effectiveDateField.dateGMT();
            }
                
            if (response.contractInfoField.expirationDateField != null && response.contractInfoField.expirationDateField.year() != 1)
            {
                this.endDate = response.contractInfoField.expirationDateField.dateGMT();
            }
            
            //dealers
            if (response.dealersField.ContractDealerType != null) 
            {
                for (cpqPartnerContractInfoClasses.ContractDealerType t : response.dealersField.ContractDealerType) 
                {
                   this.dealerMap.put(t.shipToField, t);
                }
            }
            //items
            if (response.itemsField.ContractItemType != null) 
            {
                for (cpqPartnerContractInfoClasses.ContractItemType t : response.itemsField.ContractItemType) 
                {
                   if (t != null)
                   {
	                   Item myItem = new Item(t);
	                   this.itemMap.put(t.itemNumberField, myItem);  
                   }

                }
            }

        }
        
        public void mergeResults(cpqPartnerContractInfoClasses.ContractInfo that)
        {
            if (that == null || that.errorCodeField != '0')
            { 
                return;
            }
            
            //related contracts
            this.relatedContracts.addAll(that.relatedContracts);
            
            //header
            if (!String.isEmpty(that.ContractName))
            {
                this.ContractName = that.ContractName;  
            }

            if (!String.isEmpty(that.RootContractNum))
            {
                this.RootContractNum = that.RootContractNum;  
            }
            
            if (!String.isEmpty(that.CommitmentType))
            {
                this.CommitmentType = that.CommitmentType;  
            }
            //dates
            if (that.startDate != null)
            {
                this.startDate = that.startDate;  
            }
            if (that.endDate != null)
            {
                this.endDate = that.endDate;  
            }
            //dealers
            if (that.dealerMap.size() > 0) 
            {
                this.dealerMap.putAll(that.dealerMap);
            }
            //items
            if (that.itemMap.size() > 0) 
            {
                this.itemMap.putAll(that.itemMap);
            }
        }
        
    }
    
     public class Customer_Wrapper {
        public cpqPartnerContractInfoClasses.ArrayOfInformation Customers;
        public Integer TotalCount;
        private String[] Customers_type_info = new String[]{'Customers','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] TotalCount_type_info = new String[]{'TotalCount','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract','true','false'};
        private String[] field_order_type_info = new String[]{'Customers','TotalCount'};
    }
    
    public class Information {
        public String Address;
        public Decimal BillTo;
        public String City;
        public String ClassofTrade;
        public String CommitmentType;
        public String ContractName;
        public String ContractType;
        public DateTime CustomerEffDt;
        public DateTime CustomerExpDt;
        public String CustomerName;
        public String CustomerType;
        public String CustomerTypeDsc;
        public String Group_x;
        public String Number_x;
        public Decimal Seq;
        public Decimal ShipTo;
        public String State;
        public String Zip;
        private String[] Address_type_info = new String[]{'Address','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] BillTo_type_info = new String[]{'BillTo','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','false'};
        private String[] City_type_info = new String[]{'City','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] ClassofTrade_type_info = new String[]{'ClassofTrade','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] CommitmentType_type_info = new String[]{'CommitmentType','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] ContractName_type_info = new String[]{'ContractName','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] ContractType_type_info = new String[]{'ContractType','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] CustomerEffDt_type_info = new String[]{'CustomerEffDt','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','false'};
        private String[] CustomerExpDt_type_info = new String[]{'CustomerExpDt','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','false'};
        private String[] CustomerName_type_info = new String[]{'CustomerName','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] CustomerType_type_info = new String[]{'CustomerType','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] CustomerTypeDsc_type_info = new String[]{'CustomerTypeDsc','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] Group_x_type_info = new String[]{'Group','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] Number_x_type_info = new String[]{'Number','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] Seq_type_info = new String[]{'Seq','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','false'};
        private String[] ShipTo_type_info = new String[]{'ShipTo','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','false'};
        private String[] State_type_info = new String[]{'State','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] Zip_type_info = new String[]{'Zip','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract','true','false'};
        private String[] field_order_type_info = new String[]{'Address','BillTo','City','ClassofTrade','CommitmentType','ContractName','ContractType','CustomerEffDt','CustomerExpDt','CustomerName','CustomerType','CustomerTypeDsc','Group_x','Number_x','Seq','ShipTo','State','Zip'};
    }
    public class ArrayOfInformation {
        public cpqPartnerContractInfoClasses.Information[] Information;
        private String[] Information_type_info = new String[]{'Information','http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract','true','false'};
        private String[] field_order_type_info = new String[]{'Information'};
    }

    
}