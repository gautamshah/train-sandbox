@isTest
public class sObject_cacheTest
{
	// The problem is that the sObject_cache is no abstract, so we need to add a Concrete
	// class to test it with

	/*
    @isTest
    static void constructor()
    {
    	sObject_cache cache = new sObject_cache(Account.sObjectType);
    	
    	system.assertEquals(Account.sObjectType, cache.sObjType);
    	system.assertEquals(true, cache.cache.isEmpty());
    	system.assertEquals(true, cache.indexes.isEmpty());
    	system.assertEquals(true, cache.indexedFields().isEmpty());
    	system.assertEquals(true, cache.sObjectFields.size() > 0);
    	system.assertEquals(true, cache.sObjectFields.contains(Account.field.Name));
    }
    
    @isTest
    static void addIndexNoDataInCache()
    {
	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
	   	cache.addIndex(Account.field.Name);
	   	system.assertEquals(true, cache.indexes.size() == 1);
	   	system.assertEquals(true, cache.indexes.containsKey(Account.field.Name));
	   	system.assertEquals(true, cache.indexes.get(Account.field.Name).isEmpty());
    }
    
    @isTest
    static void addIndexWithDataInCache()
    {
	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
	   	Account a = new Account();
	   	a.Name = 'addIndexWithDataInCache';
	   	insert a;
		cache.put(a);

	   	cache.addIndex(Account.field.Name);
	   	system.assert(true, cache.indexes.size() == 1);
	   	system.assertEquals(true, cache.indexes.containsKey(Account.field.Name));
	   	system.assertEquals(false, cache.cache.isEmpty());
	   	system.assertEquals(1, cache.indexes.get(Account.field.Name).size());
    }
    
    @isTest
    static void sObjectNotValidException()
    {
	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
	   	
	   	Account a = new Account();
	   	a.Name = 'sObjectNotValidException';
	   	insert a;
	   	Contact c = new Contact();
	   	c.LastName = 'sObjectNotValidException';
	   	c.AccountId = a.Id;
	   	insert c;
	   	
	   	try
	   	{
		   	cache.put(c);
		   	system.assert(false, 'Should have thrown an error;');
	   	}
	   	catch(Exception e)
	   	{
	   		Type t = sObject_cache.sObjectNotValidException.class;
	   		system.assertEquals(t.getName(), e.getTypeName());
	   	}
    }
    
    @isTest
    static void sObjectNeedsIdException()
    {
	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
	   	
	   	Account a = new Account();
	   	a.Name = 'sObjectNotValidException';
	   	
	   	try
	   	{
		   	cache.put(a);
		   	system.assert(false, 'Should have thrown an error;');
	   	}
	   	catch(Exception e)
	   	{
	   		Type t = sObject_cache.sObjectNeedsIdException.class;
	   		system.assertEquals(t.getName(), e.getTypeName());
	   	}
    }
    
    @isTest
    static void putWithNoIndex()
    {
 	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
	   	Account a = new Account();
	   	a.Name = 'addIndexWithDataInCache';
	   	insert a;
	   	
	   	system.assertEquals(true, cache.cache.isEmpty());
		cache.put(a);

		system.assertEquals(1, cache.cache.size());
    }
    
    @isTest
    static void putWithIndex()
    {
 	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
 	   	cache.addIndex(Account.field.Name);
 	   	
	   	Account a = new Account();
	   	a.Name = 'addIndexWithDataInCache';
	   	insert a;
	   	
	   	system.assertEquals(true, cache.cache.isEmpty());
	   	system.assertEquals(true, cache.indexes.get(Account.field.Name).isEmpty());
		cache.put(a);

		system.assertEquals(1, cache.cache.size());
		system.assertEquals(1, cache.indexes.get(Account.field.Name).size());
    }
    
    @isTest
    static void putExternalField()
    {
 	   	sObject_cache cache = new sObject_cache(Account.sObjectType);
 	   	cache.addIndex(User.field.Name);
 	   	
 	   	Account a = new Account();
	   	a.Name = 'addIndexWithDataInCache';
	   	insert a;
 
	   	system.assertEquals(true, cache.cache.isEmpty());
	   	system.assertEquals(true, cache.indexes.get(User.field.Name).isEmpty());
 		cache.put(a, User.field.Name, 'lastname');
 		
 		system.assertEquals(1, cache.cache.size());
 		system.assertEquals(1, cache.indexes.get(User.field.Name).size());	   	
    }
    
    static sObject_cache cache = new sObject_cache(Account.sObjectType);
    static Id setupFetchNoIndex()
    {
	   	Account a = new Account();
	   	a.Name = 'addIndexWithDataInCache';
	   	insert a;
	   	
		cache.put(a);
		
		return a.Id;
    }
    
    @isTest
    static void fetch()
    {
 	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id = setupFetchNoIndex();
		
		system.assertEquals(1, cache.fetch().size());
		system.assertEquals(true, cache.fetch().keySet().contains(id));
    }
    
    @isTest
    static void fetchById()
    {
	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id = setupFetchNoIndex();
    	
		system.assertEquals(id, cache.fetch(id).Id);
    }
    
    @isTest
    static void fetchByIds()
    {
	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id1 = setupFetchNoIndex();
    	Id id2 = setupFetchNoIndex();
    	
    	system.assertEquals(2, cache.fetch(new Set<Id>{ id1, id2 }).size());
    	system.assertNotEquals(cache.fetch(id1), cache.fetch(id2));	
    }
    
    @isTest
    static void sObjectFieldNotIndexedException()
    {
	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id = setupFetchNoIndex();
    	
    	try
    	{
    		Map<Id, sObject> results = cache.fetch(Account.field.Name, 'addIndexWithDataInCache');
    		system.assert(false, 'Should have failed because the field is not indexed');
    	}
    	catch(Exception e)
	   	{
	   		Type t = sObject_cache.sObjectFieldNotIndexedException.class;
	   		system.assertEquals(t.getName(), e.getTypeName());
	   	}
    }
    
    @isTest
    static void fetchByFieldValueAddIndexBefore()
    {
    	cache.addIndex(Account.field.Name);
	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id1 = setupFetchNoIndex();
    	Id id2 = setupFetchNoIndex();
    	
    	system.assertEquals(2, cache.fetch(Account.field.Name, 'addIndexWithDataInCache').size());
    }

    @isTest
    static void fetchByFieldValueAddIndexAfter()
    {
	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id1 = setupFetchNoIndex();
    	Id id2 = setupFetchNoIndex();
    	cache.addIndex(Account.field.Name);
    	
    	system.assertEquals(2, cache.fetch(Account.field.Name, 'addIndexWithDataInCache').size());
    }
    
    @isTest
    static void fetchByFieldValueNotFound()
    {
	   	system.assertEquals(true, cache.fetch().isEmpty());
    	Id id1 = setupFetchNoIndex();
    	Id id2 = setupFetchNoIndex();
    	cache.addIndex(Account.field.Name);
    	
    	system.assertEquals(sObject_cache.NOTFOUND, cache.fetch(Account.field.Name, ''));
    }
    */
}