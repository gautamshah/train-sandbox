/****************************************************************************************
 * Name    : DemoProductValidationTestClass
 * Author  : Fenny Saputra
 * Date    : 26/Jun/2014  
 * Apex Involved: DemoProductValidation.trigger
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 * 
 *****************************************************************************************/
 
@isTest
public class DemoProductValidationTestClass{
    static testMethod void DPValidationTestClass(){
         Id pId = [select Id from Profile where name = 'CRM Admin Support'].Id;
         
         Id tu1Id = [SELECT Id FROM Territory where Name='REGION TERRITORY ASIA'].Id;
         Id tu2Id = [SELECT Id FROM Territory where Name='REGION TERRITORY ASIA'].Id;
         
         Id tu1userId = [SELECT Id, UserId FROM UserTerritory where TerritoryId=:tu1Id LIMIT 1].UserId;
         Id tu2userId = [SELECT Id, UserId FROM UserTerritory where TerritoryId=:tu2Id LIMIT 1].UserId;
/*                 
        List<User> userList = new List<User>();
        List<UserTerritory> userterrList = new List<UserTerritory>();
        
        User u1 = new User(
            LastName = 'Rep1',
            Business_Unit__c = 'The Unit',
            Franchise__c = 'Burger King',
            email = 'testFSrep1@covidien.com',
            alias = 'trep1',
            username = 'testFSrep1@covidien.com',
            communityNickName = 'testFSrep1@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='SGD',
            EmailEncodingKey='ISO-8859-1',
            TimeZoneSidKey='America/New_York',
            LanguageLocaleKey='en_US',
            LocaleSidKey ='en_US',
            Country = 'SG');
         userList.add(u1);
         
         User u2 = new User(
            LastName = 'Rep2',
            Business_Unit__c = 'The Unit',
            Franchise__c = 'Burger King',
            email = 'testFSrep2@covidien.com',
            alias = 'trep2',
            username = 'testFSrep2@covidien.com',
            communityNickName = 'testFSrep2@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='SGD',
            EmailEncodingKey='ISO-8859-1',
            TimeZoneSidKey='America/New_York',
            LanguageLocaleKey='en_US',
            LocaleSidKey ='en_US',
            Country = 'SG');
         userList.add(u2);
         
         insert userList;
         
         UserTerritory tu1terr = new UserTerritory(
             TerritoryId=tu1Id,
             UserId=u1.Id);
         userterrList.add(tu1terr);
         
         UserTerritory tu2terr = new UserTerritory(
             TerritoryId=tu2Id,
             UserId=u2.Id);
         userterrList.add(tu2terr);
         
         insert userterrList;
*/                  
         Product_SKU__c sku;
         try{
             sku = [SELECT id FROM Product_SKU__c WHERE Country__c ='CN' LIMIT 1];
         }catch (Exception e){
             System.debug('DemoProductValidation Test Class: Getting Asia Account Exception: ' + e);
             if( sku == null){
                 sku = new Product_SKU__c(Name='Test SKU', Country__c='CN');
                 insert sku;
             }
         }
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('DemoProductValidation Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         } 
         
         Demo_Product__c dp;
         try{
             dp = [SELECT id FROM Demo_Product__c LIMIT 1];
         }catch (Exception e){
             System.debug('DemoProductValidation Test Class: Getting Asia Account Exception: ' + e);
             if( dp == null){
                 dp = new Demo_Product__c(
                     Name='New Request', 
                     Asset_Name__c=sku.Id,
                     Approval_Status__c='Approved',
                     OwnerId=tu1userId,
                     Country__c='SG');
                 insert dp;
             }
         }
         
        Movement__c mov;
        try{
             mov = [SELECT id FROM Movement__c LIMIT 1];
         }catch (Exception e){
             System.debug('DemoProductValidation Test Class: Getting Asia Account Exception: ' + e);
             if( mov == null){
                 mov = new Movement__c(
                     Demo_Product_Name__c=dp.Id, 
                     Hospital__c=a.Id,
                     Start_Date__c=system.today(),
                     End_Date__c=system.today()+90,
                     Approval_Status__c='Approved');
                 insert mov;
             }
         }
         
         dp.Current_Location__c='Hospital';
         dp.OwnerId=tu2userId;
         update dp;
        
    }
}