/****************************************************************************************
* Name    : Class: Proposal_Main
* Author  : Paul Berglund
* Date    : 12/08/2014
* Purpose : A wrapper for static functionality for Proposal records
* 
* Dependancies: 
*   Called by: ProposalTriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE        AUTHOR                  CHANGE
* ----        ------                  ------
* 04/29/2015  Paul Berglund           Updated to select the correct SVP for each level
* 06/01/2015  Clerval Fokle - Statera Added logic for Surgical to determine the Offer Development (OD) approver
* 01/22/2016  Paul Berglund           Commented out SSG code so we can promote to production
* 01/22/2016  Paul Berglund           Uncommented after promoting to RlsStg
* 09/27/2016  Paul Berglund    Moved logic to cpqApprover_xxxxx classes 
*****************************************************************************************/
public with sharing class Proposal_Main 
{
}