public class cpqAgreement_FIELD
{
 	public static final sObjectField
 		
 		Duration 							= Apttus__APTS_Agreement__c.fields.Duration__c,
 		BuyoutAsOfDate 						= Apttus__APTS_Agreement__c.fields.Buyout_as_of_Date__c,
 		BuyoutAmount 						= Apttus__APTS_Agreement__c.fields.Buyout_Amount__c,
 		TaxGrossNetOfTradein 				= Apttus__APTS_Agreement__c.fields.Tax_Gross_Net_of_TradeIn__c,
 		SalesTaxPercent 					= Apttus__APTS_Agreement__c.fields.Sales_Tax_Percent__c,
 		TBDAmount 							= Apttus__APTS_Agreement__c.fields.TBD_Amount__c,
 		TotalAnnualSensorCommitmentAmount =
 						Apttus__APTS_Agreement__c.fields.Total_Annual_Sensor_Commitment_Amount__c,
 		Opportunity 						= Apttus__APTS_Agreement__c.fields.Apttus__Related_Opportunity__c,
 		Account 							= Apttus__APTS_Agreement__c.fields.Apttus__Account__c,
		OwnerId								= Apttus__APTS_Agreement__c.fields.OwnerId,
		Approver_Seller 					= Apttus__APTS_Agreement__c.fields.SellerApprover__c,
		Approver_RSM 						= Apttus__APTS_Agreement__c.fields.RSM_Approver__c,
		Approver_ZVP 						= Apttus__APTS_Agreement__c.fields.ZVP__c,
		Approver_SVP 						= Apttus__APTS_Agreement__c.fields.SVP_User__c,
		ApprovalIndicator					= Apttus__APTS_Agreement__c.fields.Approval_Indicator__c,
		Name								= Apttus__APTS_Agreement__c.fields.Name,
    	ElectrosurgeryEaches				= Apttus__APTS_Agreement__c.fields.Electrosurgery_Eaches__c,
    	ElectrosurgerySales					= Apttus__APTS_Agreement__c.fields.Electrosurgery_Sales__c,
    	ElectrosurgeryIncrementalSpend =
    					Apttus__APTS_Agreement__c.fields.Electrosurgery_Incremental_Spend__c,
    	ElectrosurgeryIncrementalQuanitity =
    					Apttus__APTS_Agreement__c.fields.Electrosurgery_Incremental_Quantity__c,
    	ElectrosurgeryIncrementalTotalSpend =
    					Apttus__APTS_Agreement__c.fields.Electrosurgery_Total_Incremental_Spend__c,
    	LigasureEaches						= Apttus__APTS_Agreement__c.fields.Ligasure_Eaches__c,
    	LigasureSales						= Apttus__APTS_Agreement__c.fields.Ligasure_Sales__c,
    	LigasureIncrementalSpend			= Apttus__APTS_Agreement__c.fields.Ligasure_Incremental_Spend__c,
    	LigasureIncrementalQuanitity 		= Apttus__APTS_Agreement__c.fields.Ligasure_Incremental_Quantity__c,
    	LigasureIncrementalTotalSpend 		= Apttus__APTS_Agreement__c.fields.Ligasure_Total_Incremental_Spend__c,
    	IncrementalTotal					= Apttus__APTS_Agreement__c.fields.Total_Incremental__c;
}