public class Case_RemoveMessageFlag {
	public string strRecordType{get;set;}
	public final Case currentCase;
    
    public Case_RemoveMessageFlag(ApexPages.StandardController stdController) {
        this.currentCase = [Select Id, SystemNote__c from Case where Id = :stdController.getRecord().Id limit 1];
    }
    
    public boolean getHasMessage()
    {
    	if(this.currentCase.SystemNote__c!=null)
    	{
	        if(this.currentCase.SystemNote__c.contains('#NewMessage#') || this.currentCase.SystemNote__c.contains('#TaskCompleted#'))
	            return true;
    	}    
        return false;
    }
    
    public PageReference RemoveMessageFlag()
    {
        if(this.currentCase.SystemNote__c.contains('NewMessage#') || this.currentCase.SystemNote__c.contains('#TaskCompleted#'))
        {
            this.currentCase.SystemNote__c = '#';
            update this.currentCase;
        }
        return null;
    }
}