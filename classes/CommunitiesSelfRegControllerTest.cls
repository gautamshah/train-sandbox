/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class CommunitiesSelfRegControllerTest {
    @IsTest(SeeAllData=true) 
    public static void testCommunitiesSelfRegController() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.password = '1234';
        controller.confirmPassword = '1234';
     	System.assertEquals(null, controller.registerUser());
     	
        controller.confirmPassword = '1';
     	System.assertEquals(null, controller.registerUser());
    }    
}