/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A    IL         AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies
20161118   A    IL		   AV-001     RFA Deal Type - Updated misspelled Electrosurgery_Total_Increm[em]ental_Spend__c
20161118   A    BF         AV-240     Create new Proposal for Cancelled Agreement - Add cancel()) and StatusIndicators
20170123        IL         AV-280     Commented out isOpen method (not used) to improve test coverage

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
public virtual class cpqProposal_c extends cpqDeal_c
{
	// Namespace:  Apttus_Proposal__
	// Common Name:  Proposal
	// API:  Apttus_Proposal__Proposal__c

	//public static final Apttus_Proposal__Proposal__c.Type OBJECT_Proposal;

	//public static final object FIELDS = Apttus_Proposal__Proposal__c.fields;

 	public static final sObjectField
 		FIELD_MasterAgreementId 				= Apttus_Proposal__Proposal__c.fields.Apttus_QPComply__MasterAgreementId__c,
 		FIELD_Duration 							= Apttus_Proposal__Proposal__c.fields.Duration__c,
 		FIELD_BuyoutAsOfDate 					= Apttus_Proposal__Proposal__c.fields.Buyout_as_of_Date__c,
 		FIELD_BuyoutAmount 						= Apttus_Proposal__Proposal__c.fields.Buyout_Amount__c,
 		FIELD_TaxGrossNetOfTradein 				= Apttus_Proposal__Proposal__c.fields.Tax_Gross_Net_of_TradeIn__c,
 		FIELD_SalesTaxPercent 					= Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Sales_Tax_Percent__c,
 		FIELD_TBDAmount 						= Apttus_Proposal__Proposal__c.fields.TBD_Amount__c,
 		FIELD_TotalAnnualSensorCommitmentAmount = Apttus_Proposal__Proposal__c.fields.Total_Annual_Sensor_Commitment_Amount__c,
 		FIELD_Opportunity 						= Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Opportunity__c,
 		FIELD_Account 							= Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Account__c,
		FIELD_OwnerId							= Apttus_Proposal__Proposal__c.fields.OwnerId,
		FIELD_Approver_Seller 					= Apttus_Proposal__Proposal__c.fields.SellerApprover__c,
		FIELD_Approver_RSM 						= Apttus_Proposal__Proposal__c.fields.RSM_Approver__c,
		FIELD_Approver_ZVP 						= Apttus_Proposal__Proposal__c.fields.ZVP__c,
		FIELD_Approver_SVP 						= Apttus_Proposal__Proposal__c.fields.SVP_User__c,
		FIELD_ApprovalIndicator					= Apttus_Proposal__Proposal__c.fields.Approval_Indicator__c,
		FIELD_Name								= Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Proposal_Name__c,
   	    FIELD_ElectrosurgeryEaches				= Apttus_Proposal__Proposal__c.fields.Electrosurgery_Eaches__c,
   	    FIELD_ElectrosurgerySales				= Apttus_Proposal__Proposal__c.fields.Electrosurgery_Sales__c,
   	    FIELD_ElectrosurgeryIncrementalSpend	= Apttus_Proposal__Proposal__c.fields.Electrosurgery_Incremental_Spend__c,
   	    FIELD_ElectrosurgeryIncrementalQuanitity= Apttus_Proposal__Proposal__c.fields.Electrosurgery_Incremental_Quantity__c,
   	    FIELD_ElectrosurgeryIncrementalTotalSpend=Apttus_Proposal__Proposal__c.fields.Electrosurgery_Total_Incremental_Spend__c,
   	    FIELD_LigasureEaches					= Apttus_Proposal__Proposal__c.fields.Ligasure_Eaches__c,
   	    FIELD_LigasureSales						= Apttus_Proposal__Proposal__c.fields.Ligasure_Sales__c,
   	    FIELD_LigasureIncrementalSpend			= Apttus_Proposal__Proposal__c.fields.Ligasure_Incremental_Spend__c,
   	    FIELD_LigasureIncrementalQuanitity 		= Apttus_Proposal__Proposal__c.fields.Ligasure_Incremental_Quantity__c,
   	    FIELD_LigasureIncrementalTotalSpend 	= Apttus_Proposal__Proposal__c.fields.Ligasure_Total_Incremental_Spend__c,
   	    FIELD_IncrementalTotal					= Apttus_Proposal__Proposal__c.fields.Total_Incremental__c;

	public enum ApprovalIndicators
	{
		Approved
	}

 	public static Map<ApprovalIndicators, string> enum2string = new Map<ApprovalIndicators, string>
 	{
 		ApprovalIndicators.Approved => 'Approved'
 	};

 	public static string convert(ApprovalIndicators ai)
 	{
 		return enum2string.get(ai);
 	}

	public static List<RecordType> getRecordTypes()
	{
		return cpqDeal_RecordType_c.getRecordTypesBySObjectAndId().get(cpq_u.PROPOSAL_SOBJECT_TYPE).values();
	}

    public static Apttus_Proposal__Proposal__c create(Id rtId, integer ndx)
	{
		Apttus_Proposal__Proposal__c sobj = new Apttus_Proposal__Proposal__c();
		sobj.put(FIELD_Name, 'Test Proposal ' + string.valueOf(ndx).leftPad(10));
		sobj.RecordTypeId = rtId;

		return sobj;
	}

    public static Apttus_Proposal__Proposal__c fetchApprovers(Id id)
    {
    	return [SELECT OwnerId,
    			       SellerApprover__c,
    		           RSM_Approver__c,
    		           ZVP__c,
    		           SVP_User__c
    		    FROM Apttus_Proposal__Proposal__c
    		    WHERE Id = :id];
    }

    public static Apttus_Proposal__Proposal__c oldVersion(
		Apttus_Proposal__Proposal__c newVersion,
        Map<Id, Apttus_Proposal__Proposal__c> oldVersions)
	{
		return (Apttus_Proposal__Proposal__c)sObject_c.oldVersion((sObject)newVersion, (Map<Id, sObject>)oldVersions);
	}

    public static boolean isLocked(Apttus_Proposal__Proposal__c proposal, ID id)
    {
		//If Apttus_QPComply__MasterAgreementId__c changed and is != null
   	    return  (proposal.apttus_qpcomply__masteragreementid__c != null && proposal.apttus_qpcomply__masteragreementid__c != id) ? true : false;
    }

    public static boolean isRestricted(Apttus_Proposal__Proposal__c proposal, Map<Id, sObject> oldVersions)
    {
        return sObject_c.fieldChangedTo(proposal, oldVersions, FIELD_ApprovalIndicator,
                                        convert(ApprovalIndicators.Approved)) ? true : false;

    }

    // 2017.01.23 - IL - Not used
    // public static boolean isOpen(Apttus_Proposal__Proposal__c proposal, Map<Id, sObject> oldVersions, Id id)
    // {
    //     //PAB - I am not sure if this is correct.
 //   	    return (!isRestricted(proposal, oldVersions) && !isLocked(proposal, id));
    // }

    public static void unlock(Apttus_Proposal__Proposal__c prop)
    {
        // Unlock the proposal by changing its record type to not be locked and resetting its Approval Stage
        Map<String,Id> mapProposalRType = new Map<String,Id>();

        for(RecordType rt: getRecordtypes()) {
            mapProposalRType.put(rt.DeveloperName, rt.Id);
        }

        String devName = prop.RecordType.DeveloperName;
        String unlockedRecordTypeId = mapProposalRType.get(devName.substringBefore('_Locked'));

        prop.Apttus_Proposal__Approval_Stage__c = 'Accepted';
        if (!String.isEmpty(unlockedRecordTypeId)) {
            prop.RecordTypeId = unlockedRecordTypeId;
        }
        update prop;
    }

 	public static final sObjectField
    	FIELD_ConfigurationFinalizedDate	= Apttus_Proposal__Proposal__c.fields.Apttus_QPConfig__ConfigurationFinalizedDate__c;

    public class ProductConfigurationFinalizedDate
    {
    	// Apttus_QPConfig__ConfigurationFinalizedDate__c

    	Apttus_Proposal__Proposal__c parent;

    	public ProductConfigurationFinalizedDate(Apttus_Proposal__Proposal__c parent)
    	{
    		this.parent = parent;
    	}

    	public void clear()
    	{
    		parent.put(FIELD_ConfigurationFinalizedDate, null);
    	}
    }
}