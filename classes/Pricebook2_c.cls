/*
2016-MM-DD  Paul Berglund  Created
2016-11-07  Isaac Lewis    Made DEFAULT_OPPORTUNITY_PRICEBOOK visible for testing
2016-11-07  Isaac Lewis    Made getPriceBookId return string
 */
public class Pricebook2_c
{
   	public static FINAL string DEFAULT_OPPORTUNITY_PRICEBOOK = 'Default Opportunity PriceBook';

    private static String getPriceBookId()
    {
        try
        {
            return ([SELECT Id
                     FROM   Pricebook2
                     WHERE  IsStandard = true]).Id;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static String getStandardPriceBookId()
    {
        // If there isn't a value in CPQ Variable, use the one from Pricebook2
        string standardPriceBookId = cpqVariable_c.getValue(DEFAULT_OPPORTUNITY_PRICEBOOK);
        return (standardPriceBookId == null) ? getPriceBookId() : standardPriceBookId;
    }
}