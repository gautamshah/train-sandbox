/**
Controller for AwardCreditCalculator Page

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11/21      Yuli Fintescu       Created
2014-12/09      Tony Do             Edited
2015-10/08      Paul Berglund       GetSalesHistory was hardcoded to 'PUR' for
                                    the RepType field.  This changed for FY16
                                    to be 'NEL'.
===============================================================================
*/
public with sharing class CPQ_Controller_AwardCreditCalculator {
    public Apttus_Config2__ProductConfiguration__c theRecord {get; set;}
    private CPQ_RollupFieldEngine rollupEngine;
    
    public PageReference onLoad() {
        if (theRecord.ID == null)
            return null;
        
        Set<String> configFields = CPQ_Utilities.getCustomFieldNames(Apttus_Config2__ProductConfiguration__c.SObjectType, false); 
        Set<String> lineFields = CPQ_Utilities.getCustomFieldNames(Apttus_Config2__LineItem__c.SObjectType, false); 
        Set<String> summaryFields = CPQ_Utilities.getCustomFieldNames(Apttus_Config2__SummaryGroup__c.SObjectType, false); 
        
        String configQueryStr = CPQ_Utilities.buildObjectQuery(configFields, 'Apttus_QPConfig__Proposald__r.RecordType.DeveloperName', 'ID = \'' + theRecord.Id + '\'', 'Apttus_Config2__ProductConfiguration__c'); 
        String lineQueryStr = CPQ_Utilities.buildObjectQuery(lineFields, null, 'Apttus_Config2__ConfigurationId__c = \'' + theRecord.Id + '\'', 'Apttus_Config2__LineItem__c'); 
        String summaryQueryStr = CPQ_Utilities.buildObjectQuery(summaryFields, 'Apttus_Config2__ClassificationId__r.Name', 'Apttus_Config2__ConfigurationId__c = \'' + theRecord.Id + '\'', 'Apttus_Config2__SummaryGroup__c'); 
        
        theRecord = ((List<Apttus_Config2__ProductConfiguration__c>)Database.query(configQueryStr))[0];
        List<Apttus_Config2__LineItem__c> lines = (List<Apttus_Config2__LineItem__c>)Database.query(lineQueryStr);
        List<Apttus_Config2__SummaryGroup__c> summaries = (List<Apttus_Config2__SummaryGroup__c>)Database.query(summaryQueryStr);
        
        //populate rollup fields by rollupfield engine
        //================================================
        rollupEngine = new CPQ_RollupFieldEngine();
        rollupEngine.AssignProposal(theRecord, configFields, summaries, lines);
        
        //formula fields are only updated after saving
        update theRecord;
        
        //repopulate theRecord
        theRecord = [Select ID, Name, 
                        Apttus_QPConfig__Proposald__c,
                        Total_Balance__c,
                        X3rd_Party_Admin_Fee__c, 
                        X3rd_Party_Products_Subtotal__c,
                        Buyout_as_of_Date__c, 
                        Buyout_Amount__c, 
                        Duration__c, 
                        Equipment_Subtotal__c,
                        Tax_Gross_Net_of_TradeIn__c,
                        Sales_Tax_Percent__c,
                        Semi_Annual_Payment__c,
                        TBD_Amount__c,
                        Trade_In_Subtotal__c,
                        Total_Annual_Sensor_Commitment_Amount__c,
                        Promotion_Subtotal__c,
                        Equipment_Sales_Tax__c,
                        Award_Credit__c,
                        Equipment_Lines_Subtotal__c,
                        Apttus_QPConfig__Proposald__r.RecordType.DeveloperName,
                        Buyout_Contract_Effective_Date__c 
                    From Apttus_Config2__ProductConfiguration__c
                    Where ID =: theRecord.ID];
        
        return null;
    }
    

    public CPQ_Controller_AwardCreditCalculator(ApexPages.StandardController controller) {
        theRecord = (Apttus_Config2__ProductConfiguration__c)controller.getRecord();
    }
    
    public PageReference doCalculate() {
        if (theRecord.Duration__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter duration.'));
            return null;
        }
        
        if (theRecord.Total_Annual_Sensor_Commitment_Amount__c == null || theRecord.Total_Annual_Sensor_Commitment_Amount__c == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter valid Total annual commitment amount.'));
            return null;
        }
        
        if (theRecord.Apttus_QPConfig__Proposald__r.RecordType.DeveloperName.startsWith('Respiratory_Solutions_COOP')) {
            Integer m = CPQ_Utilities.calculateDurationInMonth(theRecord.Duration__c);
            if (m != null && m > 36) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duration in COOP RS Cannot exceed 3 years'));
                return null;
            }
        }
        
        //formula fields are only updated after saving
        update theRecord;
        
        //repopulate theRecord
        theRecord = [Select ID, Name, 
                        Apttus_QPConfig__Proposald__c,
                        Total_Balance__c,
                        X3rd_Party_Admin_Fee__c, 
                        X3rd_Party_Products_Subtotal__c,
                        Buyout_as_of_Date__c, 
                        Buyout_Amount__c, 
                        Duration__c, 
                        Equipment_Subtotal__c,
                        Tax_Gross_Net_of_TradeIn__c,
                        Sales_Tax_Percent__c,
                        Semi_Annual_Payment__c,
                        TBD_Amount__c,
                        Trade_In_Subtotal__c,
                        Total_Annual_Sensor_Commitment_Amount__c,
                        Promotion_Subtotal__c,
                        Equipment_Sales_Tax__c,
                        Award_Credit__c,
                        Equipment_Lines_Subtotal__c,
                        Apttus_QPConfig__Proposald__r.RecordType.DeveloperName,
                        Buyout_Contract_Effective_Date__c 
                    From Apttus_Config2__ProductConfiguration__c
                    Where ID =: theRecord.ID];        
        
        return null;
    }
    
    //Call WS to get 12 month sensor usage
    public PageReference doUsage() {
        Set<String> SensorsInCart = rollupEngine.DisposaleSkus;
        System.debug('$$$ SensorsInCart $$$: ' + SensorsInCart + ' SIZE: ' + SensorsInCart.size());
        if (SensorsInCart.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No sensor products in the cart.'));
            return null;
        }
        
        //Apttus_Proposal__Account__r.Account_External_ID__c, and all Account_External_ID__c of Participating_Facilities__r
        Set<String> Facilities = new Set<String>();
        List<Apttus_Proposal__Proposal__c> records = [Select Name, Id, 
                                                        Apttus_Proposal__Account__c,
                                                        Apttus_Proposal__Account__r.Account_External_ID__c,
                                                        (Select Id, Name, Account__r.Account_External_ID__c From Participating_Facilities__r)
                                                     From Apttus_Proposal__Proposal__c
                                                    Where ID =: theRecord.Apttus_QPConfig__Proposald__c];
        if (records.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The proposal record does not exist.'));
            return null;
        }
        
        Apttus_Proposal__Proposal__c pp = records[0];
        if(pp.Apttus_Proposal__Account__r.Account_External_ID__c != null && pp.Apttus_Proposal__Account__r.Account_External_ID__c.startsWith('US-')) {
            Facilities.add(pp.Apttus_Proposal__Account__r.Account_External_ID__c.substring(3));
        }
        
        for(Participating_Facility__c pf : pp.Participating_Facilities__r){
            if(pf.Account__r.Account_External_ID__c != null && pf.Account__r.Account_External_ID__c.startsWith('US-')) {
                Facilities.add(pf.Account__r.Account_External_ID__c.substring(3));
            }
        }
        
        System.debug('!!!! Facilities !!!!: ' + Facilities + ' SIZE: ' + Facilities.size());
        if (Facilities.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No valid customer or facility in the proposal.'));
            return null;
        }
        
        //invoke WS
        CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
        
        CPQ_ProxySerializationArray.ArrayOfstring customers = new CPQ_ProxySerializationArray.ArrayOfstring();
        customers.string_x = new List<String>(Facilities);
        
        CPQ_ProxySerializationArray.ArrayOfstring products = new CPQ_ProxySerializationArray.ArrayOfstring();
        products.string_x = new List<String>(SensorsInCart);
        
        Date startDate = System.today().addMonths(-12);
        Date endDate = System.today();
        
        System.Debug('*** customers ' + customers);
        System.Debug('*** products ' + products);

        try {
            Decimal usage = service.GetSalesHistory(customers, products, startDate, endDate, null, 'NEL', null);
            System.Debug('*** usage ' + usage);
            theRecord.Total_Annual_Sensor_Commitment_Amount__c = usage;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        return null;
    }
}


/*
String[] skus = new String[] {'A','ADH-P/I','D-YS','D-YSE','DS100A','DS100A-1','D20','D25','D25L','FOAM A/N',
'FOAM P/I','MAXAI','MAXAJ','MAXALI','MAXALJ','MAXII','MAXN','MAXNJ','MAXP','MAXPI',
'MAXPJ','MAXR','MAXRI','MC10','I','I20','MAXA','N25','V-SAT','OXI-A/N',
'VSC-L','VSC-S','RS10','SC-A','SC-NEO','10004954','10005063','5-18141','5-18241','5-18437',
'5-18541','8888247015','8888247031','8888247049','8888247056','8888247064','96225','96226','96222','96223',
'901558','86289','86349','86043','85588','125028','126035','18780','MAXFASTI','010994',
'012530','XS04624','P','OXI-P/I','86547','86445','N','IS','86646','86226',
'186-0106','86108','86110','86470','186-0200','86048','76275','76280','76285','125137',
'5-18537','110860','86656','008181','110875','009818','012495','86461','86545','86229',
'86109','86492','86387','86271','86389','86010','18765','86291','86083','86266',

'86005','110870','86652','010341','012539','D-YSPD','96224','96227','MAXI','86645',
'IS-S','86227','86442','86207','18765S','125141','007768','010987','85006','86443',
'86542','86543','86644','86054','86097','86199','86200','86224','86284','86394',
'86395','86544','IS-C','86079','86201','86285','86397','86463','86050','SAFB-SM/USA',
'86052','86202','86286','MAXPACI','MAXPAC','86400','86401','86464','86546','86648',
'SPFB/USA','86098','86203','86204','86228','86287','86402','86465','PDSLV','86221',
'86099','86205','86269','86385','86455','86524','86525','126135','126137','126139',
'86288','86444','86466','86467','86548','86212','86265','86642','18865','18870',
'18875','18880','18885','18890','86013','86491','96360','96365','96370','96375',
'96380','86084','86057','86107','186-0212','86222','86270','125032','125035','125037',

'125039','126037','126039','126041','125235','125237','125239','125241','125128','125132',
'125135','86230','86446','86096','86113','86460','86541','86643','86468','86549',
'86114','86213','86353','86452','18755S','18760S','86056','18770S','18775S','18785S',
'18790S','18710S','86555','76251','76260','76265','76270','86122','86086','86231',
'86051','86078','86223','125139','86290','86447','86448','86469','86550','M407252002',
'86115','86214','86215','86055','86355','86453','86494','86080','85865','86208',
'86263','86449','86488','86081','86117','86046','86552','86047','86049','86118',
'86216','86454','86045','86521','86522','124160','124165','124170','86111','86264',
'86450','18860','86016','86553','86015','85863','86009','96114','96115','96117',
'86655','85864','86014','96228','86657','86012','96107','POSEY','86658','96109',

'96110','109850','109855','109860','109865','109870','109875','109880','96111','18790',
'18785','MAXPACAI','96112','96113','86003','109885','109890','109810','110865','110885',
'110890','18770','18760','18755','18750','86017','85590','85591','86520','85592',
'85593','86523','85589','006324','006325','006912','006913','007266','007269','007606',
'007609','007610','007737','007738','007739','008174','008175','008177','008179','008180',
'009822','009826','010172','010177','010209','010210','010212','010213','010339','010340',
'010344','010555','010579','010580','010582','010625','010787','010807','010976','010980',
'010982','010986','010991','012111','012463','012464','012465','012528','012529','012531',
'012532','012537','012538','012542','015100','SC-PR','007743','010304','010989','XS04476',
'015101','5-18237','5-18441','MAXIJ','MAXNI','MAXRJ','86462','86225','86398','86206',

'86209','86490','86112','86451','86554','8888247023','86456','18780S','125041','86654',
'86268','76255','86283','067167','126141','18775','86011','86526','10068119','76290',
'86647','86085','86551','186-0160','86351','86493','MAXAL','MAXFAST','86082','86100',
'18750S','18710','86653','86053','86267','86007','010433','010977','XS04620','015096',
'ADH-A/N','110880'};

CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
CPQ_ProxySerializationArray.ArrayOfstring customers = new CPQ_ProxySerializationArray.ArrayOfstring();
customers.string_x = new String[]{'343593'};
CPQ_ProxySerializationArray.ArrayOfstring products = new CPQ_ProxySerializationArray.ArrayOfstring();

products.string_x = new String[]{};
for (Integer i = 431; i < 442; i++)
    products.string_x.add(skus[i]);

DateTime startDate = DateTime.newInstance(2014, 2, 1, 0, 0, 0);
DateTime endDate = DateTime.newInstance(2015, 2, 1, 0, 0, 0);

Decimal usage = service.GetSalesHistory(customers, products, startDate, endDate, null, 'NEL', null);
System.Debug(usage);




String[] skus = new String[] {'MAXPJ','MAXR','MAXRI','MC10','I','I20','MAXA','N25','V-SAT','OXI-A/N',
'IS-S','86227','86442','86207','18765S','125141','007768','010987','85006','86443',
'SPFB/USA','86098','86203','86204','86228','86287','86402','86465','PDSLV','86221',
'18875','18880','18885','18890','86013','86491','96360','96365','96370','96375',
'96380','86084','86057','86107','186-0212','86222','86270','125032','125035','125037',
'125039','126037','126039','126041','125235','125237','125239','125241','125128','125132',
'86114','86213','86353','86452','18755S','18760S','86056','18770S','18775S','18785S',
'86051','86078','86223','125139','86290','86447','86448','86469','86550','M407252002',
'86115','86214','86215','86055','86355','86453','86494','86080','85865','86208',
'86263','86449','86488','86081','86117','86046','86552','86047','86049','86118',
'86450','18860','86016','86553','86015','85863','86009','96114','96115','96117',
'86209','86490','86112','86451','86554','8888247023','86456','18780S','125041','86654',
'86647','86085','86551','186-0160','86351','86493','MAXAL','MAXFAST','86082','86100'};

CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
CPQ_ProxySerializationArray.ArrayOfstring customers = new CPQ_ProxySerializationArray.ArrayOfstring();
customers.string_x = new String[]{'343593'};
CPQ_ProxySerializationArray.ArrayOfstring products = new CPQ_ProxySerializationArray.ArrayOfstring();

DateTime startDate = DateTime.newInstance(2014, 2, 1, 0, 0, 0);
DateTime endDate = DateTime.newInstance(2015, 2, 1, 0, 0, 0);


for (Integer i = 0; i < 10; i++) {
    String s = skus[i];
    products.string_x = new String[]{s};
    
    Decimal usage = service.GetSalesHistory(customers, products, startDate, endDate, null, 'NEL', null);
    System.Debug(s + ': ' + usage);
}


MAXA:       13500.0000000
85006:      96.3400000
86228:      8.8500000
96360:      40.2600000
96365:      84.9900000
96370:      84.9900000
96375:      56.0000000
86057:      13.3500000
125035:     130.0500000
125037:     693.6000000
125039:     867.0000000
126037:     152.6400000
126039:     152.6400000
86056:      13.3500000
86550:      572.9500000
85865:      56.8000000
86552:      245.5500000
85863:      36.1400000
125041:     173.4000000
86551:      1227.7500000


18206.6500000

*/