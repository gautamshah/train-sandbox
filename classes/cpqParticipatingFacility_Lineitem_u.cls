/*
    
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.	Id is just a unique value to distinguish changes made on the same date
5.	If there are multiple related Jiras, add them on the next line
6.	Combine the Date & Id and use it to mark changes related to the entry
	// YYYYMMDD	A   PAB			CPR-000	Updated comments section
    Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                 	Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        	PAB             Medtronic.com
Isaac Lewis				IL              Statera.com
Bryan Fry			 	BF				Statera.com
Henry Noerdlinger		HN				Medtronic.com

MODIFICATION HISTORY
====================
Date        Id  Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161118   	A  	BF         	AV-001	Royalty Free Agreements - Added validation methods for quantities

*/     
public class cpqParticipatingFacility_Lineitem_u extends sObject_u
{
    private static cpqParticipatingFacility_Lineitem_c instance = new cpqParticipatingFacility_Lineitem_c();
   /* 
    public override void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<sObject> newList,
            Map<Id, sObject> newMap,
            List<sObject> oldList,
            Map<Id, sObject> oldMap,
            integer size
        )
    {
    	// 20161115 - This logic is not relevant at this time
    	return;
    	
        system.debug('cpqParticipatingFacility_Lineitem_u.main.newList: ' + newList);
        system.debug('cpqParticipatingFacility_Lineitem_u.main.oldMap: ' + oldMap);
        
        if (isBefore && (isInsert || isUpdate))
        {
            //setLookups(newList, oldMap);
            //setRecordType(newList, oldMap);
        }
    }

    
    private static cpqParticipatingFacility_Lineitem__c cast(sObject sobj)
    {
    	return (cpqParticipatingFacility_Lineitem__c)sobj;
    }
    
    private static cpqParticipatingFacility_Lineitem__c oldVersion(
        cpqParticipatingFacility_Lineitem__c newVersion,
        Map<Id, cpqParticipatingFacility_Lineitem__c> oldVersions)
    {
        return cast(sObject_c.oldVersion(newVersion, oldVersions));
    }
    
    private static void lock(
    	List<cpqParticipatingFacility_Lineitem__c> newList,
        Map<Id, cpqParticipatingFacility_Lineitem__c> oldVersions)
    {
    	
    }
    
    private static void unlock(
    	List<cpqParticipatingFacility_Lineitem__c> newList,
        Map<Id, cpqParticipatingFacility_Lineitem__c> oldVersions)
    {
    	
    	
    }
    
    // cpqParticipatingFacility_Lineitem_SerialNumber_c
    // Count in field
    // Count <= Total Trade-In Quantity
   
    private static void restricted(
    	List<cpqParticipatingFacility_Lineitem__c> newList,
        Map<Id, cpqParticipatingFacility_Lineitem__c> oldVersions)
    {
    	// FunnyFieldName - Profile/Permission Set, Object, ObjectField - don't give Read access to Rep Profile/Permission if record.FunnyFieldName is null
    }
  */
    // This method would be called whenever the Master Agreement
    // field changes on a Proposal
    public static void setAgreement(Map<Id, Id> proposalId2MasterAgreementId)
    {
    	List<cpqParticipatingFacility_Lineitem__c> entries =
    		[SELECT Id
    			    ,Proposal__c
    		        ,Agreement__c
    		 FROM cpqParticipatingFacility_Lineitem__c
    		 WHERE Proposal__c IN :proposalId2MasterAgreementId.keySet()];
    		 
    	for(cpqParticipatingFacility_Lineitem__c entry : entries)
        {
        	entry.Agreement__c = proposalId2MasterAgreementId.get(entry.Proposal__c);
        }
        
        update entries;
    }
/*
    private static void setLookups(
        List<cpqParticipatingFacility_Lineitem__c> newList,
        Map<Id, cpqParticipatingFacility_Lineitem__c> oldMap)
    {
    	// 20161115 - This logic is not relevant at this time
    	return;
    	
        Set<Id> agreementLineItemIds = new Set<Id>();
        Set<Id> proposalLineItemIds = new Set<Id>();
        for(cpqParticipatingFacility_Lineitem__c now : newList)
        {
            agreementLineItemIds.add(now.AgreementLineItem__c);
            proposalLineItemIds.add(now.ProposalLineItem__c);
        }
        
        Map<Id, Apttus__AgreementLineItem__c> agreementLineItems = new Map<Id, Apttus__AgreementLineItem__c>(
            [SELECT Id,
                    Apttus__AgreementId__c
             FROM Apttus__AgreementLineItem__c
             WHERE Id IN :agreementLineItemIds]);
             
        Map<Id, Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>(
            [SELECT Id,
                    Apttus_Proposal__Proposal__c
             FROM Apttus_Proposal__Proposal_Line_Item__c
             WHERE Id IN :proposalLineItemIds]);
        
        //for(cpqParticipatingFacility_Lineitem__c now : newList)
        //    instance.setLookups(now, before(now, oldMap), proposalLineItems.get(now.ProposalLineItem__c), agreementLineItems.get(now.AgreementLineItem__c));
    }
    
    private static void setRecordType(
        List<cpqParticipatingFacility_Lineitem__c> newList,
        Map<Id, cpqParticipatingFacility_Lineitem__c> oldMap)
    {
    	// 20161115 - This logic is not relevant at this time
    	return;
    	
        //for(cpqParticipatingFacility_Lineitem__c now : newList)
        //    instance.setRecordType(now, before(now, oldMap));
    }
*/
    public static Set<String> validate(
        List<cpqParticipatingFacility_Lineitem__c> facilityLineItems,
        Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemMap,
        Map<Id,Participating_Facility_Address__c> facilitiesMap)
    {
        Set<String> errorMessages = new Set<String>();
        errorMessages.addAll(cpqParticipatingFacility_Lineitem_u.validateNonZero(facilityLineItems, 
                                                                                    proposalLineItemMap,
                                                                                    facilitiesMap));

        errorMessages.addAll(cpqParticipatingFacility_Lineitem_u.validateDistinctFacilities(facilityLineItems, 
                                                                                            proposalLineItemMap,
                                                                                            facilitiesMap));

        errorMessages.addAll(cpqParticipatingFacility_Lineitem_u.validateHardwareQuantities(facilityLineItems, 
                                                                                            proposalLineItemMap));


        errorMessages.addAll(cpqParticipatingFacility_Lineitem_u.validateTradeInSerialNumbers(facilityLineItems, 
                                                                                                proposalLineItemMap));

        return errorMessages;
    }

    private static Set<String> validateNonZero(
        List<cpqParticipatingFacility_Lineitem__c> facilityLineItems,
        Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemMap,
        Map<Id,Participating_Facility_Address__c> facilitiesMap)
    {
        Set<String> errorMessages = new Set<String>();
        for (cpqParticipatingFacility_Lineitem__c lineItem: facilityLineItems) {
            if (lineItem.QuantityToBePlacedAtShipTo__c <= 0) {
            	System.debug('quantity was less than 0: ' + lineItem.id);
                String facilityName = cpqParticipatingFacilityAddress_c.getDisplayName(
                        facilitiesMap.get(lineItem.ParticipatingFacilityAddress__c));
                String productName = proposalLineItemMap.get(lineItem.ProposalLineItem__c).Apttus_QPConfig__OptionId__r.Name;
                String messageString = 'Row for product "' + productName +
                                        '" and Participating Facility "' + facilityName  +
                                        '" must be greater than 0.';
                errorMessages.add(messageString);
            }
        }
        return errorMessages;
    }

    private static Set<String> validateDistinctFacilities(
        List<cpqParticipatingFacility_Lineitem__c> facilityLineItems,
        Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemMap,
        Map<Id,Participating_Facility_Address__c> facilitiesMap)
    {
        Set<String> errorMessages = new Set<String>();
        Map<Id,Set<Id>> lineItemFacilities = new Map<Id,Set<Id>>();

        for (cpqParticipatingFacility_Lineitem__c lineItem: facilityLineItems) {
            if (!lineItemFacilities.containsKey(lineItem.ProposalLineItem__c)) {
                lineItemFacilities.put(lineItem.ProposalLineItem__c, new Set<Id>());
            }
            Set<Id> currentFacilities = lineItemFacilities.get(lineItem.ProposalLineItem__c);
            if (currentFacilities.contains(lineItem.ParticipatingFacilityAddress__c)) {
                String productName = proposalLineItemMap.get(lineItem.ProposalLineItem__c).Apttus_QPConfig__OptionId__r.Name;
                String facilityName = cpqParticipatingFacilityAddress_c.getDisplayName(
                        facilitiesMap.get(lineItem.ParticipatingFacilityAddress__c));
                String messageString = 'Multiple rows are specified for product "' + productName +
                                        '" and Participating Facility "' + facilityName  + 
                                        '". There should only be 1 row for each combination.';
                errorMessages.add(messageString);
            } else {
                currentFacilities.add(lineItem.ParticipatingFacilityAddress__c);
            }
        }

        return errorMessages;
    }

    private static Set<String> validateHardwareQuantities(
        List<cpqParticipatingFacility_Lineitem__c> facilityLineItems,
        Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemMap)
    {
        Set<String> errorMessages = new Set<String>();
        Map<Id,Integer> lineItemQuantityTotals = getLineItemQuantityTotals(facilityLineItems);

        for (Id lineItemId: lineItemQuantityTotals.keySet()) {
            Integer total = lineItemQuantityTotals.get(lineItemId);
            if (total == null) {
                total = 0;
            }
            Apttus_Proposal__Proposal_Line_Item__c pli = proposalLineItemMap.get(lineItemId);
            if (total != pli.Apttus_QPConfig__Quantity2__c) {
                String productName = proposalLineItemMap.get(lineItemId).Apttus_QPConfig__OptionId__r.Name;
                Integer intQuantity = Integer.valueOf(pli.Apttus_QPConfig__Quantity2__c);
                String messageString = 'The sum of quantities for the product "' + productName +
                                        '" (' + total + ') does not match the total for that product from the cart (' +
                                        intQuantity + ')';
                errorMessages.add(messageString);
            }
        }

        return errorMessages;
    }

    private static Set<String> validateTradeInSerialNumbers(
        List<cpqParticipatingFacility_Lineitem__c> facilityLineItems,
        Map<Id,Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemMap)
    {
        Set<String> errorMessages = new Set<String>();

        List<Apttus_Proposal__Proposal_Line_Item__c> tradeInItems =
            cpqProposalLineitem_u.filterByCategory(proposalLineItemMap.values(), cpqProposalLineitem_u.TRADE_IN);
        List<Apttus_Proposal__Proposal_Line_Item__c> hardwareItems =
            cpqProposalLineitem_u.filterByCategory(proposalLineItemMap.values(), cpqProposalLineitem_u.HARDWARE);

        // If there are no Trade-In lines, there is nothing to validate so return
        if (tradeInItems == null || tradeInItems.isEmpty()) {
            return errorMessages;
        }

        // Currently there is only support for one Trade-In line.  Data model changes would be needed
        // to link trade-ins with specific products if more are added
        Integer tradeInQuantity = Integer.valueOf(tradeInItems[0].Apttus_QPConfig__Quantity2__c);
        Apttus_Proposal__Proposal_Line_Item__c hardwareTradeInItem;
        Integer serialNumberQuantity = 0;
        for (Apttus_Proposal__Proposal_Line_Item__c pli: hardwareItems) {
            if (cpqProposalLineItem_c.hasTradeIns(pli)) {
                hardwareTradeInItem = pli;
            }
        }

        // Get the count of serial numbers entered for participating facilities
        for (cpqParticipatingFacility_Lineitem__c lineItem: facilityLineItems) {
            if (lineItem.ProposalLineItem__c == hardwareTradeInItem.Id) {
                if (lineItem.TradeinSerialNumbers__c != null && lineItem.TradeinSerialNumbers__c != '') {
                    List<String> serialNumbers = lineItem.TradeinSerialNumbers__c.split('\n');
                    serialNumberQuantity += serialNumbers.size();
                }
            }
        }

        if (tradeInQuantity != serialNumberQuantity) {
            String productName = hardwareTradeInItem.Apttus_QPConfig__OptionId__r.Name;
            String messageString = 'The number of Serial Numbers entered for the product "' + productName +
                                    '" (' + serialNumberQuantity + 
                                    ') does not match the number of Trade-Ins specified (' +
                                    tradeInQuantity + ')';
            errorMessages.add(messageString);
        }

        return errorMessages;
    }

    private static Map<Id,Integer> getLineItemQuantityTotals(
        List<cpqParticipatingFacility_Lineitem__c> facilityLineItems)
    {
        Map<Id,Integer> lineItemQuantityTotals = new Map<Id,Integer>();

        for (cpqParticipatingFacility_Lineitem__c lineItem: facilityLineItems) {
            Integer intQuantity = Integer.valueOf(lineItem.QuantityToBePlacedAtShipTo__c); 
            if (!lineItemQuantityTotals.containsKey(lineItem.ProposalLineItem__c)) {
                lineItemQuantityTotals.put(lineItem.ProposalLineItem__c, intQuantity);
            } else if (lineItemQuantityTotals.containsKey(lineItem.ProposalLineItem__c)) {
                Integer newTotal = lineItemQuantityTotals.get(lineItem.ProposalLineItem__c) + intQuantity;
                lineItemQuantityTotals.put(lineItem.ProposalLineItem__c, newTotal);
            }
        }

        return lineItemQuantityTotals;
    }
}