global class BatchCreateCyclePeriod  implements Database.Batchable<sObject>{
    String Query;
    global BatchCreateCyclePeriod(String q){
             Query=q;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);  
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('=====1');
        Date dt=Date.today();
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        List<Cycle_Period_Reference__c> lstRef=[Select Name, Id, (Select Country__c,Submission_Date__c from Country_Calendar__r) from Cycle_Period_Reference__c where Month__c=:CurrentCyclePeriodMonth and Year__c=:CurrentCyclePeriodYear];
        List<Cycle_Period__c> lstCP=new List<Cycle_Period__c>();
        List<Case> lstCase=new List<Case>();
        ID caserecordtypeid = [Select ID from RecordType where DeveloperName = 'Upload_Invoice_Request'].ID;
        ID queueID = [select id, Name from Group where Type = 'Queue' and name = 'Sales Out COV Contact - CN'].ID;
        if(lstRef!=null&&lstRef.size()>0)
        {
            System.debug('=====2');
            Id refId=lstRef[0].Id;
            String refName=lstRef[0].Name;
            Map<String,Date> mapCountry=new Map<String,Date>();
            if(lstRef[0].Country_Calendar__r!=null&&lstRef[0].Country_Calendar__r.size()>0)
            {
                for(Country_Calendar__c cc:lstRef[0].Country_Calendar__r)
                    if(mapCountry.keyset().contains(cc.Country__c)==false)
                        mapCountry.put(cc.Country__c,cc.Submission_Date__c);
            }
            Set<ID> set_con_id = new Set<ID>();
            Map<ID,ID> User_id = new Map<ID,ID>();
            for (Account acc1: (List<Account>)scope)
            {
                if (acc1.contacts.size()!=0)
                  {
                      Contact c1 = acc1.contacts;
                      set_con_id.add(c1.ID);  
                  }
            }
            if(set_con_id.size()>0)
            {
                for(User u: [Select contactid, id from User where IsActive = true and contactid in: set_con_id])
                {
                    User_id.put(u.contactid, u.id);
                }
            }
            
            for (Account acc: (List<Account>)scope)
            {
                System.debug('=====3');
                ID contactID = null;
                if(acc.Cycle_Periods__r==null||acc.Cycle_Periods__r.size()==0)
                {
                    System.debug('=====4');
                    Cycle_Period__c cp=new Cycle_Period__c();
                    cp.Cycle_Period_Reference__c=refId;
                    cp.Distributor_Name__c=acc.Id;
                    if(acc.Date_Format__c != null)
                        cp.Date_Format__c = acc.Date_Format__c;
                    
                    if (acc.contacts.size()==0)
                      {
                        cp.DIS_Contact_Email__c = acc.Email_Address__c;
                      }
                     else
                     {
                        Contact c = acc.contacts;
                        cp.DIS_Contact_Email__c=c.Email;
                        contactID = c.id;
                        
                     }
                     cp.Distributor_Offset__c  = acc.Submission_Offset_Day__c;
                     
                    if(mapCountry.keyset().contains(acc.BillingCountry))
                    {
                        Date d = mapCountry.get(acc.BillingCountry);
                        if (acc.Submission_Offset_Day__c==Null)
                        {
                            acc.Submission_Offset_Day__c = 0;
                        }
                        Date newdate = d.addDays(acc.Submission_Offset_Day__c.intValue());
                        cp.Submission_Due_Date__c=newdate;
                        cp.Country_Submission_Due_Date__c=mapCountry.get(acc.BillingCountry);
                    }
                    lstCP.add(cp);
                    /*Account Country is CN then add create Case record*/
                    if(contactID != null)
                    {
                        if(acc.Country__c == 'CN' && User_id.containsKey(contactID))
                        //if(acc.Country__c == 'CN')
                        {
                            Case objcase=new Case();
                            objcase.recordtypeID = caserecordtypeid;
                            objcase.Status = 'Draft';
                            objcase.ContactID = contactID;  
                            system.debug('***' + User_id.get(contactID));
                            objcase.OwnerID = User_id.get(contactID);
                            //objcase.Account_Country_Code__c = 'CN';
                            //if (contactID != null)
                            //{
                                //Contact con = acc.contacts; 
                                //objcase.ContactID = con.ID;  
                                //objcase.OwnerID = User_id.get(con.id);
                            //}
                            /*else
                            {
                                objcase.OwnerID = UserInfo.getUserId();
                            }*/
                            objcase.AccountID = acc.ID;
                            //objcase.Submission__r = cp;
                            objcase.Subject = refName + ' “ 发票”';
                            lstCase.add(objcase);
                        }
                    }
                }
            }
            
           try{
                if(lstCP.size()>0)
                 insert lstCP;
                
                for(Cycle_Period__c objcp : lstCP)
                {
                    for(Case objcase : lstCase)
                    {
                        if(objcase.AccountID == objcp.Distributor_Name__c)
                        {
                            objcase.Submission__c = objcp.ID;
                            break;
                        }
                    }
                }
             
                if(lstCase.size()>0)
                {
                 insert lstCase;
                System.Debug('++++Success Case++++++');    
                }                  
             }
            Catch(Exception ex)
            {
                System.Debug('===== Error =====' + ex);
            } 
        }
    }
    global void finish(Database.BatchableContext BC){  
        
    }
        

}