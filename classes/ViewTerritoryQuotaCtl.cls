public with sharing class ViewTerritoryQuotaCtl {
    /****************************************************************************************
	 * Name    : ViewTerritoryQuotaCtl
	 * Author  : Nathan Shinn
	 * Date    : 07-25-2011
	 * Purpose : Search and display a list of Territory Quota Objects and allow for viewing 
	 *           the child quotas - Controller for the ViewTerritoryQuota Page.
	 *                 
	 *
	 * ========================
	 * = MODIFICATION HISTORY =
	 * ========================
	 * DATE        AUTHOR               CHANGE
	 * ----        ------               ------
	 * 07-25-2011  Nathan Shinn        Created
	 *
	 *****************************************************************************************/
	
	//variables used by the page to control the list view
	public string SearchString {get;set;}
	public boolean SearchComplete {get;set;}
	public List<List<SObject>> searchList {get;set;}
	public List<Territory_Quota__c> lSearchTerritory{get;set;}
	public list<String> previousParentId{get;set;}
	public String selectedParent{get;set;}
	public String selectedParentId{get;set;}
	public Integer idx = -1;
	
	public ViewTerritoryQuotaCtl(){/** Constructor. Initializes the search and navigation parameters **/
		SearchComplete = false;
		SearchString = ApexPages.currentPage().getParameters().get('SearchString');
		previousParentId = new list<String>();
	}
	
	public pagereference SearchForTerritory(){
		
		try{
			if(SearchString==null || SearchString==''){//Check SearchString first and error if its blank
				ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO , 'Please enter a value into the search field');
				ApexPages.addMessage(infoMsg);
				SearchComplete=false;
			} else {//execute the SOSL
				string sSearch = '*' + SearchString + '*';//fuzzy search
				List<List<SObject>> searchList 
				   = [FIND :sSearch IN ALL FIELDS RETURNING Territory_Quota__c (id, Name, YTD_Sales__c, YTD_Quota__c, TerritoryId__c
                                                                              , Quota_Bucket__c, Parent_Territory__c, Annual_Quota__c 
                      ORDER BY name)];
				//cast the results
				Territory_Quota__c [] territories = ((List<Territory_Quota__c>)searchList[0]);
				lSearchTerritory = territories;
				SearchComplete=true;
					
			}
		}
		catch(Exception e)
		{
			//surface the SOSL-speciffic error
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()) );
		}
		
		selectedParent = null;
		return null;
	
	}
	
	public PageReference getChildQuotas()
	{
		//when the user selects a territory quota, display the children
		
		lSearchTerritory 
		    =  [select id
		             , Name
		             , YTD_Sales__c
		             , YTD_Quota__c
		             , TerritoryId__c
		             , Quota_Bucket__c
		             , Parent_Territory__c
		             , Annual_Quota__c 
		          from Territory_Quota__c
		         where Parent_Territory__c = :selectedParentId
		          ORDER BY name];
		 selectedParent = [select Name from Territory_Quota__c where TerritoryId__C = :selectedParentId limit 1].Name;         
		 
		 previousParentId.add(selectedParentId);//add the parent territory to the list of previous Ids to enable the "Back" link
		 idx++; //increment the counter that indexes the back link's context
		 
		 return null;
	}
	
	public PageReference getPreviousQuotas()
	{
		//retrieve the previous results
		
		try
		{
			lSearchTerritory 
			    =  [select id
			             , Name
			             , YTD_Sales__c
			             , YTD_Quota__c
			             , TerritoryId__c
			             , Quota_Bucket__c
			             , Parent_Territory__c
			             , Annual_Quota__c 
			          from Territory_Quota__c
			         where Parent_Territory__c = :previousParentId.get(idx -1)
			          ORDER BY name];
			 selectedParent = [select Name from Territory_Quota__c where TerritoryId__C = :previousParentId.get(idx -1) limit 1].Name;         
		
		     idx--; //manage the back link list's index 
		}
		catch(Exception e)
		{
			//list index out of bounds. Can't go back any further. Requery the original search results
			idx = -1;
			previousParentId.clear();
			SearchForTerritory();
		}
		 return null;
	}
	
	

}