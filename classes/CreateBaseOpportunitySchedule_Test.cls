/**
Test class for
CreateBaseOpportunitySchedule

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-12-04      Tatsuya Takesue     Created
2016-03-22      Hidetoshi kawada    Modified(CVJ_SELLOUT_DEV-100)
===============================================================================
*/
@isTest
private class CreateBaseOpportunitySchedule_Test{

    static List<User> testUsers;

    static testMethod void runTest() {
        createTestData();
        Test.startTest();
        CreateBaseOpportunitySchedule tb = new CreateBaseOpportunitySchedule();
        System.schedule('TEST--CreateBaseOpportunitySchedule--'+System.Now(), '0 0 23 * * ?', tb);
        Test.stopTest();
    }

    static void createTestData() {
        Create_Base_Opportunity_Batch_Settings__c batchSetting = Create_Base_Opportunity_Batch_Settings__c.getOrgDefaults();
        // 2016.03.22 ECS)kawada CVJ_SELLOUT_DEV-100 mod start
        // batchSetting.BatchSize__c = 50;
        batchSetting.BatchSize__c = 40;
        // 2016.03.22 ECS)kawada CVJ_SELLOUT_DEV-100 mod end
        insert batchSetting;

        // Create Custom Setting
        Create_Base_Opportunity_Batch_LastDate__c batchSettingDate = Create_Base_Opportunity_Batch_LastDate__c.getOrgDefaults();
        batchSettingDate.Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().day());
        insert batchSettingDate;

        // 2016.03.22 add start CVJ_SELLOUT_DEV-100
        Product2 prdcap = JP_TestUtil.createTestProduct('当年実績(H)', true, null, null);
        Product2 prddis = JP_TestUtil.createTestProduct('当年実績(D)', true, null, null);
        Product2 prdbase = JP_TestUtil.createTestProduct('ベース金額', true, null, null);
        prdcap.CanUseQuantitySchedule = false;
        prdcap.CanUseRevenueSchedule = true;
        prddis.CanUseQuantitySchedule = false;
        prddis.CanUseRevenueSchedule = true;
        prdbase.CanUseQuantitySchedule = false;
        prdbase.CanUseRevenueSchedule = true;
        update prdcap;
        update prddis;
        update prdbase;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry stdentrycap = new PricebookEntry(Pricebook2Id = pricebookId,
                                                        Product2Id = prdcap.Id,
                                                        CurrencyIsoCode = 'JPY',
                                                        UnitPrice = 10000,
                                                        IsActive = true);
        PricebookEntry stdentrydis = new PricebookEntry(Pricebook2Id = pricebookId,
                                                        Product2Id = prddis.Id,
                                                        CurrencyIsoCode = 'JPY',
                                                        UnitPrice = 10000,
                                                        IsActive = true);
        PricebookEntry stdentrybase = new PricebookEntry(Pricebook2Id = pricebookId,
                                                        Product2Id = prdbase.Id,
                                                        CurrencyIsoCode = 'JPY',
                                                        UnitPrice = 10000,
                                                        IsActive = true);
        insert stdentrycap;
        insert stdentrydis;
        insert stdentrybase;
        Pricebook2 ctbook = new Pricebook2(Name='JP:CMN', isActive=true);
        insert ctbook;
        PricebookEntry testentrycap = new PricebookEntry(Pricebook2Id = ctbook.Id,
                                                        Product2Id = prdcap.Id,
                                                        CurrencyIsoCode = 'JPY',
                                                        UnitPrice = 12000,
                                                        IsActive = true);
        PricebookEntry testentrydis = new PricebookEntry(Pricebook2Id = ctbook.Id,
                                                        Product2Id = prddis.Id,
                                                        CurrencyIsoCode = 'JPY',
                                                        UnitPrice = 12000,
                                                        IsActive = true);
        PricebookEntry testentrybase = new PricebookEntry(Pricebook2Id = ctbook.Id,
                                                        Product2Id = prdbase.Id,
                                                        CurrencyIsoCode = 'JPY',
                                                        UnitPrice = 12000,
                                                        IsActive = true);
        insert testentrycap;
        insert testentrydis;
        insert testentrybase;
        Create_Base_Opportunity_Batch_Pd2Capital__c basePd2CapitalNameSetting = Create_Base_Opportunity_Batch_Pd2Capital__c.getOrgDefaults();
        Create_Base_Opportunity_Batch_Pd2Dispo__c basePd2DispoNameSetting = Create_Base_Opportunity_Batch_Pd2Dispo__c.getOrgDefaults();
        Create_Base_Opportunity_Batch_Pd2Base__c basePd2BaseNameSetting = Create_Base_Opportunity_Batch_Pd2Base__c.getOrgDefaults();
        basePd2CapitalNameSetting.Product2Name__c = prdcap.Name;
        basePd2DispoNameSetting.Product2Name__c = prddis.Name;
        basePd2BaseNameSetting.Product2Name__c = prdbase.Name;
        insert basePd2CapitalNameSetting;
        insert basePd2DispoNameSetting;
        insert basePd2BaseNameSetting;
        // 2016.03.22 add end CVJ_SELLOUT_DEV-100

        // Create Base Hospital Data
        RecordType accRec = JP_TestUtil.selectRecordType('Japan_Base');
        Account testacc = new Account(Name = 'TestAccountName1', RecordTypeId = accRec.Id, Account_External_ID__c = 'JP-Base');
        insert testacc;

        // Create User Data
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        testUsers = new List<User> {
            new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest',
                         Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr',
                         ProfileId = profile1.Id, TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                         LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US')
        };
        insert testUsers;
        testUsers = [select Id from User];

        // Create Territory
        System.runAs (testUsers[0]) {
            Territory t = new Territory(Name = 'test 1 x'
                                       , ExternalParentTerritoryID__c = 'test1-12345'
                                       , Custom_External_TerritoryID__c = 'test1-54321');
            insert t;
        }
        Territory tt = [select Id from Territory limit 1];

        // Create Seller Quota Data
        List<Seller_Quota__c> sellerList = new List<Seller_Quota__c>();
        Seller_Quota__c seller =  new Seller_Quota__c();
        seller.CurrencyIsoCode = 'JPY';
        // 2016.03.22 mod start CVJ_SELLOUT_DEV-100
        //seller.Location_ID__c = (string) tt.Id;
        seller.Location_ID__c = 'test1-54321';
        seller.Name = 'testName';
        // 2016.03.22 mod end CVJ_SELLOUT_DEV-100
        seller.Seller_Name_del__c = 'SellerName00001';
        seller.JP_Last_FY_Actual_Amount_Capital__c = 10000;
        seller.JP_Last_FY_Actual_Amount_Dispo__c = 10000;
        seller.Sellout_Date__c = Date.newInstance(2015,12,4);
        // seller.RecordTypeId = 'JPY';
        seller.LastModifiedById = testUsers[0].Id;
        seller.CreatedById = testUsers[0].Id;
        seller.OwnerId = testUsers[0].Id;
        seller.Actual_Amount_Capital__c = 10000;
        seller.Actual_Amount_Dispo__c = 10000;
        seller.Eval_Class__c = '1';
        seller.MONTH_Quota__c = 1000;
        seller.Sales_Class__c = '1';

        insert seller;

    }

}