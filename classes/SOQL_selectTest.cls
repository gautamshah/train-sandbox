// NOTE: Must keep apiVersion in sync with apiVersion of SOQL_select.cls

@isTest
private class SOQL_selectTest
{
    private static List<Account> accs;

    static void setup() {
        accs = cpqAccount_TestSetup.generateAccounts(5);
        Integer i = 1;
        for (Account acc : accs) {
            acc.Name = 'Account ' + i;
            acc.BillingState = 'CO';
            acc.BillingPostalCode = '12345';
            i++;
        }
        insert accs;
    }
    
    @isTest
    static void constructor()
    {
        List<sObject> sobjs = new List<sObject>();
        
        SOQL_select s = new SOQL_select(sobjs);
    }

    @isTest static void build () {

        setup();
        Set<String> fields = new Set<String>{'Name','BillingState','OwnerId'};

        String query;
        List<Account> result;

        // Empty query
        query = SOQL_Select.build(Schema.Account.sObjectType, null, null, null, null);
        System.debug('Empty Query: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size(), result.size());
        // Ignored field not available
        try {
            System.assertEquals(null, result.get(0).OwnerId);
            System.assert(false,'Query accessed ignored field');
        }
        catch (Exception e) {
            // Should throw exception
        }

        // Query with ignored fields 
        query = SOQL_select.build(Account.sObjectType, fields, null, null, null);
        System.debug('Query With Ignored Fields: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size(), result.size());
        // Ignored field available
        System.assertNotEquals(null, result.get(0).OwnerId);

        // Query with condition
        query = SOQL_Select.build(Schema.Account.SObjectType, fields, ' WHERE Name != \'Account 1\' ', null, null);
        System.debug('Query With Condition: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size()-1, result.size());

        // Query with order
        query = SOQL_Select.build(Schema.Account.SObjectType, fields, null, ' ORDER BY Name DESC ' , null);
        System.debug('Query With Order: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size(), result.size());
        System.assertEquals('Account ' + result.size(), result.get(0).Name);
        System.assertEquals('Account 1', result.get(result.size()-1).Name);

        // Query with limit
        Integer limitSize = 2;
        query = SOQL_Select.build(Schema.Account.SObjectType, fields, null, null,limitSize);
        result = Database.query(query);
        System.assertEquals(limitSize, result.size());
    }

    @isTest static void buildObjectQuery () {
        setup();
        Set<String> fields = new Set<String>{'Name','BillingState','OwnerId'};

        String query;
        List<Account> result;

        // Empty Query
        query = SOQL_Select.buildObjectQuery(null,null,null,'Account');
        System.debug('Empty Query: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size(), result.size());

        // Basic Query
        query = SOQL_Select.buildObjectQuery(fields,null,null,'Account');
        System.debug('Basic Query: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size(), result.size());

        // Special Field Query
        query = SOQL_Select.buildObjectQuery(fields,'BillingPostalCode',null,'Account');
        System.debug('Basic Query: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size(), result.size());
        System.assertNotEquals(null, accs.get(0).BillingPostalCode);

        // Conditional Query
        query = SOQL_Select.buildObjectQuery(fields,'BillingPostalCode','Name != \'Account 1\'','Account');
        System.debug('Conditional Query: ' + query);
        result = Database.query(query);
        System.assertEquals(accs.size()-1, result.size());
    }
}