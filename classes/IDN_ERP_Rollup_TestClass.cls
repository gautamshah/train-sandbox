@isTest
private class IDN_ERP_Rollup_TestClass {

   static testMethod void TestPageFunctionality() {
   
   
        
        // Instantiate the standard controller
        Account a = new Account();
        Apexpages.StandardController sc = new Apexpages.standardController(a);
 

        // Instantiate the extension
        IDN_ERP_Rollup ext = new IDN_ERP_Rollup(sc);
        

       
        //Test converage for the MasterContactSearchvisualforce page
        PageReference pageRef = Page.IDN_ERP_Rollup; 
        Test.setCurrentPageReference(pageRef);
  
        //Execute Methods
       
        ext.Export();
        

  }    
     
}