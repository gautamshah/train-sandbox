@isTest
public class cpqAgreement_TestSetup
{
    public static Apttus__APTS_Agreement__c generateAgreement (Account acc, Contact ct, Opportunity opp, String recordTypeName)
    {
        
        RecordType rt = RecordType_u.fetch(cpq_u.AGREEMENT_TYPE, recordTypeName);

        // Universal Agreement Information

        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c ();
            agmt.Apttus__Account__c = acc.Id;
            agmt.Apttus__Related_Opportunity__c = opp.Id;
            agmt.Primary_Contact2__c = ct.Id;
            agmt.RecordTypeId = rt.Id;

        // Record-Type Specific Information

        if (rt.DeveloperName == CPQ_AgreementProcesses.SCRUB_PO_LOCKED_RT)
        {
            agmt.Apttus__Status_Category__c = 'In Signatures';
            agmt.Apttus__Status__c = 'Fully Signed';
            agmt.PO__c = '12345';
        }

        if (rt.DeveloperName == CPQ_AgreementProcesses.CUSTOM_KIT_LOCKED_RT)
        {
            agmt.Apttus__Status_Category__c = 'In Signatures';
            agmt.Apttus__Status__c = 'Fully Signed';
            agmt.PO__c = '12345';
            agmt.Notes__c = 'Hello World';
        }

        if (rt.DeveloperName == CPQ_AgreementProcesses.SMART_CART_LOCKED_RT)
        {
            agmt.Apttus__Status_Category__c = 'In Signatures';
            agmt.Apttus__Status__c = 'Fully Signed';
            agmt.PO__c = '12345';
            agmt.Notes__c = 'Hello World';
            agmt.SSG_Tri_Staple_Core_Reloads__c = 1;
            agmt.SSG_Tri_Staple_Specialty_Reloads__c = 1;
            agmt.SSG_Legacy_Reloads__c = 98;
            agmt.SSG_Stocking_Order_PO__c = '12345';
            agmt.SSG_Stocking_Order_Confirmation__c = '12345';
            agmt.SSG_Stocking_Order_Type__c = 'Test';
        }

        return agmt;

    }


}