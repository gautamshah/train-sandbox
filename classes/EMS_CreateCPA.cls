public with sharing class EMS_CreateCPA {

    public EMS_CreateCPA(ApexPages.StandardController controller) {
         section1=true; 
         section9=false;
         IsPreview=ApexPages.currentPage().getParameters().get('IsPreview');
         EventId= ApexPages.currentPage().getParameters().get('EventId');
         pdfpage= ApexPages.currentPage().getParameters().get('pdf');
         //selectedaction=ApexPages.currentPage().getParameters().get('step');
         //system.debug('---->cons'+selectedaction+'......'+ApexPages.currentPage().getParameters().get('step'));
         CurrentEMSRecord=[Select Id,Name,CurrencyIsocode,Start_Date__c,Event_Native_Name__c,Expense_Type__c ,GBU_Primary__c,Owner_Country__c,Budget_External_Recipient_URL__c ,Budget_Internal_Recipient_URL__c ,Lodging_Description_Remarks__c ,Lodging_per_Person__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,CPA_Remarks__c ,Any_HCP_from_outside_of_ANZ__c ,Printing_Fee_Remarks__c,No_of_Completed_eCPAs__c ,CPA_Ref__c,Total_Lodging_Count__c,Total_Air_Travel_Count__c,Total_Air_Travel__c,Total_Air_travel_per_person__c ,Total_Gifts_Count__c ,Total_Gifts_amount__c,Gifts_value_per_person__c ,
                                  Printing_Fee__c,Printing_Fee_Description__c,Total_Consultancy_Count__c,Meeting_Video_Translation__c ,Total_Consultancy_Fee__c ,Consultancy_Per_person__c,Total_Others_Count__c ,Total_Others_Amount__c ,Others_per_person__c ,
                                  Design_Fee__c,Design_Fee_Description__c,Agency_Fee__c,Any_benefits_involving_govt_officials__c,Other_Non_Benefit_Type_Fee__c,Product_or_other_forms_of_charitable_don__c,Training_conducted_by_HCPs__c ,Design_Fee_Remarks__c,Gifts_Description_Remarks__c,Air_Travel_Description_Remarks__c ,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Fee__c,Manager1__c,Others__c ,Location_City__c,In_Country_CPA_Routing_USD_1000__c ,Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Total_LodA__c ,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,ISR_CSR__c,Days_before_Event_Start__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Local_GTA__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,Other_Meeting_Facility_Fee__c ,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Venue__c,Publication_grant__c,Any_recipient_s_who_are_KOL_s_HCP_s__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Any_speaker_consultant_from_outside_Asia__c,Manager_Certification_Date__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,Division_Multiple__c,GBU_Multiple__c,
                                  Total_No_of_Recipients__c,No_of_Approved_eCPAs__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,Event_Owner_Approval_Date__c ,CCI_Total_Expense__c,VOC_interviews_discussions__c,Event_Status__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Agency_Name__c,Event_URL__c,Any_HCP_Invitees_from_Outside_Asia__c,CPA_Status__c,
                                  Consultation_Fees_Description_Remarks__c,Consultancy_Description_Remarks__c ,Others_Description_Remarks__c,Others_Sample_Decription_Remarks__c,Other_Relevant_Information__c,
                                  CPA_Request_Date__c,Others_Please_specify__c,Total_Meal_Count__c,Others_SKU_Sample_NCE_Amount__c,Others_SKU_Sample_Covidien_Event_Amount__c,Total_LGT_Count__c ,Covidien_Solo_Sponsorship__c,Sponsorship_Pay_To_Agency__c,Track_Meal_Flag__c,
                                  Track_LT_Flag__c,Track_Lodging__c,Track_Air_Travel__c,Division_Primary__c ,Requestor_Name_1__r.Title,Requestor_Name_1__r.Phone,CCI__c,Requestor_Name_1__r.Fax,KMDIA__c,Any_India_HCP_travelling_to_China__c,
                                  Track_Consultancy_Fees__c,Track_Gifts__c,Track_Others__c,
                                  (Select Id,Comments,Stepstatus,TargetObjectId,ActorId,OriginalActorId from ProcessSteps)                                  
                                  from EMS_Event__c where Id=:EventId];
                                  
        LChecklistDocs = new Set<String>();
        
        c = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and Isocode=:currentEMSRecord.CurrencyISOCode limit 1];
        system.debug('---->'+c.Conversionrate);

    }

    
    public EMS_Event__c CurrentEMSRecord{get;set;}
    private Id EventId;
    public  String IsPreview{get;set;}
    public boolean section1{get;set;}
    public boolean section2{get;set;}
    public boolean section3{get;set;}
    public boolean section4{get;set;}
    public boolean section5{get;set;}
    public boolean section6{get;set;}
    public boolean section7{get;set;}
    public boolean section8{get;set;}
    public boolean section9{get;set;}
    public String pdfpage{get;set;}
    public CurrencyType c{get;set;}
    
    public EMS_CreateCPA() {
         
         
        
    }
    
    public Set<String> LChecklistDocs{
    get{
    if (currentEMSRecord.Booth_Space__c){
    
        LChecklistDocs.add('Official brochure/meeting agenda');
        LChecklistDocs.add('Exhibition layout');
        
    }
    if(currentEMSRecord.Workshop_conducted_by_Covidien_sales__c){
        LChecklistDocs.add('Workshop agenda/brochure');
        LChecklistDocs.add('HCP activities list/itinerary');
    }
    if(currentEMSRecord.Advertisement__c){
        LChecklistDocs.add('Final product of advertising material attached');        
    }
    if(currentEMSRecord.Stand_alone_gift__c){
        LChecklistDocs.add('Quotation and description');
    }
    if(currentEMSRecord.Training_conducted_by_HCPs__c){
        LChecklistDocs.add('Event agenda');
        LChecklistDocs.add('HCP activity list  /itinerary');
    }
    if(currentEMSRecord.Education_Grant__c){
        LChecklistDocs.add('Official proposal from requestor');
        LChecklistDocs.add('CV of speaker/consultant');
        LChecklistDocs.add('DRAFT sponsorship agreement/letter');
        LChecklistDocs.add('FINAL sponsorship agreement/letter *Legal use only:');
    }
    if(currentEMSRecord.CEP__c){
        LChecklistDocs.add('Official proposal from requestor');
        LChecklistDocs.add('CV of speaker/consultant');
        LChecklistDocs.add('DRAFT sponsorship agreement/letter');
        LChecklistDocs.add('FINAL sponsorship agreement/letter *Legal use only:');
    }
    if(currentEMSRecord.ISR_CSR__c){
        LChecklistDocs.add('Official proposal from requestor');
        LChecklistDocs.add('CV of speaker/consultant');
        LChecklistDocs.add('DRAFT sponsorship agreement/letter');
        LChecklistDocs.add('FINAL sponsorship agreement/letter *Legal use only:');
    }
    if(currentEMSRecord.Speaker_or_other_consulting_services__c){
        LChecklistDocs.add('CV of speaker/consultant');
        LChecklistDocs.add('Draft agreement');
        
       
    }
    if(currentEMSRecord.Any_benefits_involving_govt_officials__c  ){
        LChecklistDocs.add('Official request');
        LChecklistDocs.add('Official document detailing purpose of event');
        
    }
    
    
    return LChecklistDocs;
    }
    set;    
    }
    
    
    
    public List<Attachment> attachments;
    public List<Attachment> getAttachments()
    {
        // only execute the SOQL if the list hasn't been initialised
        if (null==attachments)
        {
            attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:EventId];
        }
        
        return attachments;
    }
    
    public list<attachment> eCPAAttachments;
    public List<Attachment> geteCPAAttachments()
    {
        // only execute the SOQL if the list hasn't been initialised
        if (null==attachments)
        {
            eCPAAttachments=[select Id, ParentId, Name, Description from Attachment where parentId=:EventId and Name like 'eCPA%' ];
        }
        
        return eCPAAttachments;
    }
     
    public pagereference setsection1(){
    
    section1=true;
    section2=false;
    section3=false;
    section4=false;
    section5=false;
    section6=false;
    section7=false;
    section8=false;
    
    return null;   
    }
    
    public pagereference setsection2(){
    
    section1=false;
    section2=true;
    section3=false;
    section4=false;
    section5=false;
    section6=false;
    section7=false;
    section8=false;

    return null;   
    }
    public pagereference setsection3(){
    
    section1=false;
    section2=false;
    section3=true;
    section4=false;
    section5=false;
    section6=false;
    section7=false;
    section8=false;
    
    return null;   
    }
    
    public pagereference setsection4(){
    
    section1=false;
    section2=false;
    section3=false;
    section4=true;
    section5=false;
    section6=false;
    section7=false;
    section8=false;
    
    return null;   
    }
    
    
    
    public pagereference setsection5(){
    
    section1=false;
    section2=false;
    section3=false;
    section4=false;
    section5=true;
    section6=false;
    section7=false;
    section8=false;
    
    return null;   
    }
    
    public pagereference setsection6(){
    
    section1=false;
    section2=false;
    section3=false;
    section4=false;
    section5=false;
    section6=true;
    section7=false;
    section8=false;
    
    return null;   
    }
    
    //Method to redirect back to Event Detail Page Record
    Public Pagereference Back(){
    
    Pagereference P = new Pagereference('/'+EventId);
    P.setredirect(true);
    return P;    
    }
    /*
    public Pagereference Previous(){
    
    if(section2==true){
    section1=true;
    section2=false;
    }
    else if(section3==true){
    section2=true;
    section3=false;
    }
    else if(section4==true){
    section3=true;
    section4=false;
    }
    else if(section5==true){
    section4=true;
    section5=false;
    
    }
    else if(section6==true){
    section5= true;
    section6=false;
    }
    else if(section7==true){
    section6=true;
    section7=false;
    }
    else if(section8==true){
    section7=true;
    section8=false;
    }
    
    return null;
    }
    
    public Pagereference Next(){
    
    if(section2==true){
    section3=true;
    section2=false;
    }
    else if(section3==true){
    section4=true;
    section3=false;
    }
    else if(section4==true){
    section5=true;
    section4=false;
    }
    else if(section5==true){
    section6=true;
    section5=false;
    
    }
    else if(section6==true){
    section7= true;
    section6=false;
    }
    else if(section7==true){
    section8=true;
    section7=false;
    }
    else if(section1==true){
    section2=true;
    section1=false;
    }
    
    return null;
    
    } 
    */
    public pagereference setsection7(){
    
    section1=false;
    section2=false;
    section3=false;
    section4=false;
    section5=false;
    section6=false;
    section7=true;
    section8=false;
    
    return null;   
    }
    
    public pagereference setsection8(){
    
    section1=false;
    section2=false;
    section3=false;
    section4=false;
    section5=false;
    section6=false;
    section7=false;
    section8=true;
    
    return null;   
    }
    
    public pagereference setsection9(){
    section1=false;
    section2=false;
    section3=false;
    section4=false;
    section5=false;
    section6=false;
    section7=false;
    section8=false;
    section9=true;
    
    return null;   
    }

    Public Pagereference exportpdf(){
    
    Pagereference P= Page.EMS_CPAFormpdf;
    P.getParameters().put('EventId',currentEMSRecord.Id);
    return P;
    }
}