@isTest
private class TestTrigger_Task_Send_Email  {

    static testMethod void runTest() {
     Profile p = [SELECT Id FROM Profile WHERE Name='GBU Administrator']; 
          User u = new User(
          Email='standarduser@testorg.com', 
          EmailEncodingKey='UTF-8', 
          LastName='1Testing', 
          LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', 
          ProfileId = p.Id, 
          //Business_Unit__c = 'X', 
          //Franchise__c = 'X',
          TimeZoneSidKey='America/Los_Angeles', 
          Username = 'tester20131025@test.com',
          Alias = 'Tester',
          CommunityNickname = 'Tester');
    
          System.runAs(u) {
              Utilities.isSysAdminORAPIUser = false;
              Account a = new Account(Name='Sample Account', billingcountry='US',BillingStreet='Rosina Street Park',BillingPostalCode='28758',BillingCity='US',BillingState='Lakeview Rosiland');
              insert a;
              List<Task> tasks = new List<Task>();
              tasks.add(new Task(
              ActivityDate = Date.today().addDays(7),
              Subject='Sample Task',
              WhatId = a.Id,
              OwnerId = UserInfo.getUserId(),
              Status='In Progress'));
              insert tasks;
              
          }
    
    }
}