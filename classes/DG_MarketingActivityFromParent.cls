public without sharing class DG_MarketingActivityFromParent {
    public List<Marketing_Activity__c> MA;
    private Lead ld; 
    public DG_MarketingActivityFromParent(ApexPages.StandardController controller) {
        this.ld= (Lead)controller.getRecord();
    }
    public List<Marketing_Activity__c> getMA()
    {
    	try{
	        list<Lead> l = [Select id, Parent_Lead__c FROM Lead where id = :ld.id];
	        if(l.size() > 0){
		        if (l[0].Parent_Lead__c == null){
		            return null;
		        }else{
		            MA = [Select m.id, m.Name, m.LastModifiedDate, m.ELOQUA_Activity_Type__c, m.ELOQUA_Activity_Date__c, 
		            m.ELOQUA_Activity_Detail__c From Marketing_Activity__c m 
		            where m.Lead__c = :l[0].Parent_Lead__c
		            Order By m.LastModifiedDate DESC];
		            return MA; 
		        }
	        }else{
	        	return null;
	        }
        } catch(Exception e) {
        	return null;
        }
    }
}