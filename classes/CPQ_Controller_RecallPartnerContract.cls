/**
Controller for CPQ_RecallPartnerContract Page

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-05      Yuli Fintescu       Created
===============================================================================
*/
public with sharing class CPQ_Controller_RecallPartnerContract {
    public Apttus__APTS_Agreement__c theAgreement {get; set;}
    public CPQ_Controller_RecallPartnerContract(ApexPages.StandardController controller) {
        theAgreement = (Apttus__APTS_Agreement__c)controller.getRecord();
    }
    
    public PageReference onLoad() {
        if (ApexPages.hasMessages())
            return null;
        
        Boolean result = CPQ_PartnerContractWSInvoke.InvokeRecallServiceWS(Integer.valueOf(theAgreement.AgreementID__c));
        if (result) {
            
            theAgreement.Apttus__Status_Category__c = 'Cancelled';
            theAgreement.Apttus__Status__c = null;
            update theAgreement;
            
            return new ApexPages.StandardController(theAgreement).View();
        }
        return null;
    }
    
    public Boolean getHasError() {
        return ApexPages.hasMessages();
    }
}