/****************************************************************************************
 * Name    : Test_Contact_CreateDepartment_Trigger
 * Author  : Gautam Shah
 * Date    : 06/25/2013 
 * Purpose : Unit test of Contact_CreateDepartment.trigger
 * Dependencies:
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 *****************************************************************************************/
@isTest(SeeAllData=true)
private class Test_Contact_CreateDepartment_Trigger {

    static testMethod void myUnitTest() {
        Test.startTest();
        User u = [Select Id From User Where Name = 'Jill Adams' And User_External_ID__c = '000135535' Limit 1];
        RecordType rt = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
        Account a = [Select Id From Account Where Name = 'Lancaster General Hospital' Limit 1];
        System.runAs(u)
        {
            Contact c = new Contact();
            c.LastName = 'Tester';
            c.AccountId = a.Id;
            c.Connected_As__c = 'Administrator';
            c.RecordTypeId = rt.Id;
            c.Department_picklist__c = 'Other';
            c.Other_Department__c = 'Accounting';
            insert c;
            System.debug('New ContactId: ' + c.Id);
        }
        List<Department__c> deptList = new List<Department__c>([Select Id From Department__c Where Account__c = :a.Id And Name like 'Accounting%']);
        Test.stopTest();
    }
}