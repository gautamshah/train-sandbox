/**
Batch class to tweak migrated proposals after batch loads.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-02      Yuli Fintescu       Created
11/02/2016   Paul Berglund   Commented out to save space, but keep in case we
                             do something similar again
===============================================================================

To Run
-------
        ****TURN OFF EMAILS!!!****
        CPQ_DM_Execute_Proposal p = new CPQ_DM_Execute_Proposal();
        Database.executeBatch(p);

*/
global class CPQ_DM_Execute_Proposal // implements Database.Batchable<sObject>
{
/*
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select STGPS_Configuration__c, ' + 
                            'STGPS_Proposal__c, ' + 
                            'STGPS_Proposal__r.RecordTypeID, ' + 
                            'STGPS_External_ID__c, ' + 
                            'STGPS_DM_Error__c, ' + 
                            'Name, Id ' +
                        'From CPQ_STG_Proposal__c ' + 
                        'Where STGPS_To_Test__c = true and STGPS_Proposal__c <> NULL and STGPS_External_ID__c like \'D-%\'';        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<CPQ_STG_Proposal__c > scope) {
        Map<String, CPQ_STG_Proposal__c> stages = new Map<String, CPQ_STG_Proposal__c> ();
        Map<String, CPQ_STG_Proposal__c> stagesByDealNum = new Map<String, CPQ_STG_Proposal__c> ();
        
        Map<String, String> rtIDs = new Map<String, String> ();
        for (CPQ_DM_Variable__c v : [Select Value__c, Type__c, Name, Id From CPQ_DM_Variable__c Where Type__c = 'RecordType ID']) {
            rtIDs.put(v.Name, v.Value__c);
        }
        
//=========================================================
//    Extract data from stages to build linkage structures
//=========================================================
        Set<String> configIds = new Set<String>();
        Map<String, String> dealNums = new Map<String, String>();
        for (CPQ_STG_Proposal__c c : scope) {
            configIds.add(c.STGPS_Configuration__c);
            dealNums.put(c.STGPS_External_ID__c, c.STGPS_Proposal__c);
            
            stages.put(c.STGPS_Proposal__c, c);
            stagesByDealNum.put(c.STGPS_External_ID__c, c);
        }
        
        //get Deal Contracts: master contracts for renews or amendmends
        Set<String> contractNums = new Set<String>();
        List<CPQ_STG_DealContract__c> dealContracts = [Select Name, Id, STGDC_IsRoot__c, 
                                                        STGDC_External_ID__c, 
                                                        STGDC_Direction__c, 
                                                        STGDC_ContractNumber__c
                                                    From CPQ_STG_DealContract__c
                                                    Where STGDC_External_ID__c in: dealNums.keySet()];
        for (CPQ_STG_DealContract__c c : dealContracts) {
            contractNums.add(c.STGDC_ContractNumber__c);
        }
        
        //get asscociated master agreement from partner contract table
        Map<String, CPQ_STG_Partner_Contract__c> partnerContracts = new Map<String, CPQ_STG_Partner_Contract__c>();
        for (CPQ_STG_Partner_Contract__c pc : [Select STGPC_ContractNumber__c, STGPC_Agreement__c  
                                                From CPQ_STG_Partner_Contract__c 
                                                Where STGPC_IsRoot__c = true and STGPC_ContractNumber__c in: contractNums and STGPC_Agreement__c <> NULL
                                                Order by STGPC_Deal_External_ID__c asc, STGPC_ContractNumber__c desc]) {
            partnerContracts.put(pc.STGPC_ContractNumber__c, pc);
        }
        
        //proposals records will be tweaked
        Map<ID, Apttus_Proposal__Proposal__c> proposalsToTweak = new Map<ID, Apttus_Proposal__Proposal__c>();
        
//=========================================================
//    populate Master Agreement field
//=========================================================
        for (CPQ_STG_DealContract__c c : dealContracts) {
            if (partnerContracts.containsKey(c.STGDC_ContractNumber__c) && dealNums.containsKey(c.STGDC_External_ID__c)) {
                ID proposalID = dealNums.get(c.STGDC_External_ID__c);
                
                Apttus_Proposal__Proposal__c p;
                if (proposalsToTweak.containsKey(proposalID)) {
                    p = proposalsToTweak.get(proposalID);
                } else {
                    p = new Apttus_Proposal__Proposal__c(ID = proposalID);
                    proposalsToTweak.put(proposalID, p);
                }
                p.Apttus_QPComply__MasterAgreementId__c = partnerContracts.get(c.STGDC_ContractNumber__c).STGPC_Agreement__c;
                //p.LastModifiedByID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
            } else {
                CPQ_STG_Proposal__c stage = stagesByDealNum.get(c.STGDC_External_ID__c);
                stage.STGPS_DM_Error__c = (stage.STGPS_DM_Error__c == null ? '' : stage.STGPS_DM_Error__c) + '\r\nMaster contract ' + c.STGDC_ContractNumber__c + ', or agreement of the master contract is not found.\r\n';
            }
        }
        
//=========================================================
//    if non hardward present in Oximetry Cash Quote, set record type to Current Price Quote
//=========================================================
        for (Apttus_Config2__ProductConfiguration__c c : [Select ID, Name, Apttus_QPConfig__Proposald__c,
                                                            (Select Id, Apttus_Config2__ChargeType__c From Apttus_Config2__LineItems__r Where Apttus_Config2__ChargeType__c in ('Hardware', 'Extended Service', 'Accessory'))
                                                        From Apttus_Config2__ProductConfiguration__c
                                                        Where ID in: configIds and Apttus_QPConfig__Proposald__r.RecordType.DeveloperName = 'Outright_Quote_Hardware']) {
            //no hardware at all present in oximetry cash quote, change the migrated proposal to Current Price Quote
            if (c.Apttus_Config2__LineItems__r.size() == 0) {
                Apttus_Proposal__Proposal__c p;
                if (proposalsToTweak.containsKey(c.Apttus_QPConfig__Proposald__c)) {
                    p = proposalsToTweak.get(c.Apttus_QPConfig__Proposald__c);
                } else {
                    p = new Apttus_Proposal__Proposal__c(ID = c.Apttus_QPConfig__Proposald__c);
                    proposalsToTweak.put(c.Apttus_QPConfig__Proposald__c, p);
                }
                p.RecordTypeId = rtIDs.get('Proposal Current Price Quote');
                //p.LastModifiedByID = Test.isRunningTest() ? UserInfo.getUserId() : '005U0000000eoT2IAI';
            }
        }
        
//=========================================================
//    Update proposals
//=========================================================
        if (proposalsToTweak.size() > 0) {
            Database.SaveResult[] results = database.update(proposalsToTweak.values(), false);
            for (Integer i = 0; i < results.size(); i ++) {
                Database.SaveResult result = results[i];
                
                if (!result.isSuccess()) {
                    CPQ_STG_Proposal__c stage = stages.get(result.getId());
                    String error = '';
                    for(Database.Error err : result.getErrors())
                        error = error + (err.getStatusCode() + ' - ' + err.getMessage()) + '\r\n';
                    stage.STGPS_DM_Error__c = (stage.STGPS_DM_Error__c == null ? '' : stage.STGPS_DM_Error__c) + 'Error in creating Proposal: ' + error + '\r\n';
                }
            }
        }
        
        if (scope.size() > 0)
            database.update(scope, false);
    }
    
    global void finish(Database.BatchableContext BC) {}
*/
}