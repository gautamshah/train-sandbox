public interface CPQ_AgreementGeneratorCallBack {
    void BeforeGenerate(Apttus__APTS_Agreement__c sourceAgreement,
        Apttus__APTS_Agreement__c outputAgreement,
        Apttus_Config2__ProductConfiguration__c outputConfig,
        List<Apttus_Config2__LineItem__c> outputLineItems);
}