public class DEBUG 
{
    @InvocableMethod
    public static void write(List<string> msgs)
    {
        system.debug(msgs);
    }

    public static void logLimits()
    {
        dump(
            new Map<object, object>
                {
                    'Limits.getQueries(): ' => Limits.getQueries() + '/' + Limits.getLimitQueries(),
                    'Limits.getHeapSize(): ' => Limits.getHeapSize() + '/' + Limits.getLimitHeapSize(),
                    'Limits.getCpuTime(): ' => Limits.getCpuTime() + '/' + Limits.getLimitCpuTime(),
                    'Limits.getQueryRows(): ' => limits.getQueryRows() + '/' + limits.getLimitQueryRows()
                });
    }
    
    public static void constructorTitleBlock(string title)
    {
        integer blockWidth = 50;
        integer blockHeight = 7;
        integer titleWidth = title.trim().length();
        integer titleVerticalOffset = blockHeight/2;
        integer titleHorizontalOffset = (blockWidth - titleWidth)/2;

        string msg = CONSTANTS.CRLF;
        
        for(integer ndx = 0; ndx < blockHeight; ndx++)
            msg += CONSTANTS.NBSP(blockWidth) + ' ' + title + CONSTANTS.CRLF;
        
        system.debug(msg);
    }
    
    public static void dump(Map<object, object> data)
    {
        integer blockWidth = 50;

        string msg = CONSTANTS.CRLF;

        msg += CONSTANTS.NBSP(blockWidth) + CONSTANTS.CRLF;
        
        for(object obj : data.keySet())
            msg += obj + ' ===> ' + data.get(obj) + CONSTANTS.CRLF;
        
        msg += CONSTANTS.NBSP(blockWidth);
        
        system.debug(msg);
    }

    public static Integer currentLineNumber()
    {
        try
        {
            Integer x = 0 / 0;
        }
        catch(Exception e)
        {
            String line2 = e.getStackTraceString().split('\n')[1];
            Pattern patt = Pattern.compile('([a-z0-9_.]+): line (\\d+)');
            Matcher match = patt.matcher(line2);
            match.find();
            return Integer.valueOf(match.group(2));
        }
        return null;
    }

    public static String debugQueries(Integer lineNumber)
    {
        return '>>>>>>> SOQL Queries, Line ' + lineNumber + ': ' + Limits.getQueries();
    }

    public static void debugQueries()
    {
        System.debug(debugQueries(currentLineNumber()));
    }
}