@isTest(SeeAllData=true)
private class TestLeadSchedule {

    static testMethod void runTest() {
      Test.startTest(); 
        ID RtID = [Select id from RecordType where Name =: 'EU S2 Lead PILOT' limit 1].id;
       // Campaign c = [Select Id From Campaign Limit 1];
        Lead l = new Lead(LastName='Tester', Company='Test Co.', Country='US', Status = 'Not Attempted');
        insert l;
        datetime dt1 = DateTime.Now().addHours(-48);
        //Status = 'Not Attempted' and CreationEmail__c = true and FirstReminderSent__c = false and SecondReminderSent__c = false and isConverted = false 
        Lead l1 = new Lead(CreationEmail__c = true, CreationEmailDate__c = dt1, LastName='Tester', Company='Test Co.', Country='US', Status = 'Not Attempted',FirstReminderSent__c = false , SecondReminderSent__c = false  );
        insert l1;
        datetime dt2 = DateTime.Now().addHours(-72);        
        //Status = 'Not Attempted' and CreationEmail__c = true and FirstReminderSent__c = true and SecondReminderSent__c = false and isConverted = false 
        Lead l2 = new Lead(CreationEmail__c = true, CreationEmailDate__c = dt2,FirstReminderSent__c=true,LastName='Tester', Company='Test Co.',  Country='US', Status = 'Not Attempted'
          ,SecondReminderSent__c = false ,isConverted = false );
        insert l2;
        
         LeadSchedule tb = new LeadSchedule();
           System.schedule('TEST--LeadSchedule--'+System.Now(), '0 0 23 * * ?', tb);
           
        Test.stopTest();
    }
}