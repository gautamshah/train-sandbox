@isTest
private class TestManageAccountTerritoriesTrigger {
/*********************************************************************
*
* Updates
*
*  Date      Author               Description
* ----------------------------------------------------------------------
*
*  8-3-2012   MJM         Re-configured to used test account creation from TestUtility class
*
*********************************************************************/
    static testMethod void runTest() {
        
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
      
        //insertTerritory();
        
        Territory t = [Select name, Id from Territory limit 1 ];            
        
        
        Opportunity o = new Opportunity(AccountId = a.Id, TerritoryId = t.id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!');
        insert o;
        
        Account_Territory__c at = new Account_Territory__c(Territory_ID__c =  t.Id
                                                          ,PreviousTerritory__c = null
                                                          ,AccountID__c = a.Id);
                                                          
        insert at;
        
        
        at.Delete__c = true;
        
        update at;
        
    }
    

}