/****************************************************************************************
* Name    : Class: Task_Main_Test
* Author  : Paul Berglund
* Date    : 03/08/2016
* Purpose : Test class for Task_Main
*
* Dependancies:
*   Class:  TaskConfigSetting
*
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 03/08/2016    Paul Berglund           Created to accomodate CPQ logic moved into
*                                       TaskConfigSetting
* 08/17/2016    Paul Berglund           Added additional tests to increase coverage > 72%
*****************************************************************************************/
@isTest
public class Task_Main_Test
{

	public class testException extends Exception {}

    private static final string CRLF = '\r\n';

	static Set<string> languages = new Set<string>{ 'en_US', 'en_GB', 'zh_CN', 'fr' };

    @isTest
    public static void throwException() {
        Task_Main.throwException = true;
        boolean result = Task_Main.throwException;
        system.assertEquals(result, true, 'Should be the same');

        Task_Main.throwException = null;
        result = Task_Main.throwException;
        system.assertEquals(result, false, 'Should be the same');

        Task_Main.throwException = true;
        try
        {
        	Task_Main.ThrowException('Test');
        }
        catch (Exception ex)
        {
        	system.assertEquals('Test', ex.getMessage(), 'Should be the same');
        }

        Task_Main.throwException = true;
        try
        {
        	Task_Main.ThrowException(new testException('Test'));
        }
        catch (Exception ex)
        {
        	system.assertEquals('Test', ex.getMessage(), 'Should be the same');
        }
    }

	@isTest
	public static void SendEmails()
	{
        /////insert cpqConfigSetting_cTest.create();
        /////cpqConfigSetting_c.reload();

		Apttus__APTS_Agreement__c a = createAgreement();
		Apttus__Agreement_Document__c ad = createAgreementDocument(a.Id);

		Map<Id, Task_Main.myEMailMessage> mailToSend = new Map<Id, Task_Main.myEMailMessage>();
		Task_Main.myEMailMessage msg = new Task_Main.myEMailMessage();
		msg.Description = 'http://mytestdocument';
		mailToSend.put(a.Id, msg);
		string emails = JSON.serialize(mailToSend);
		Task_Main.SendEmails(20, emails);
	}

	private static Apttus__APTS_Agreement__c createAgreement()
	{
		Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
		a.Name = 'http://mytestdocument';

		insert a;
		return a;
	}

	private static Apttus__Agreement_Document__c createAgreementDocument(Id id)
	{
		Apttus__Agreement_Document__c ad = new Apttus__Agreement_Document__c();
		ad.Apttus__Agreement__c = id;
		ad.Name = 'http://mytestdocument';
		ad.Apttus__Path__c = 'http://mytestdocument';

		insert ad;
		return ad;
	}

    @isTest
    public static void CreateAccountAndContactRecords(){
		Task_Main.throwException = true;

        Task_Main.CreateAccountAndContactRecords(); 

        Account a = [SELECT Id FROM Account WHERE Name = 'Task Activity Account - do not delete' LIMIT 1];
        Contact c = [SELECT Id, AccountId FROM Contact WHERE Name = 'Task Activity Contact - do not delete' LIMIT 1];

        system.assertNotEquals(null, a, 'Account should exist after calling this method');
        system.assertNotEquals(null, c, 'Contact should exist after calling this method');
        system.assertEquals(a.Id, c.AccountId, 'Contact should belong to this Account after calling this method');
    }

    @isTest
    public static void Link1() {
		Task_Main.throwException = true;

        system.assertEquals(Task_Main.Link('ID', 'NAME'), '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/ID" target="_blank">NAME</a>', 'These link definitions should match');
    }

    @isTest
    public static void Link2() {
		Task_Main.throwException = true;

        Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
        a.Id = Id.valueOf(Schema.sObjectType.Apttus__APTS_Agreement__c.getKeyPrefix() + '000000000001');
        a.Name = 'Agreement';

        system.assertEquals(Task_Main.Link(a), '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + a.Id + '" target="_blank">' + a.Name + '</a>', 'These link definitions should match');
    }

    @isTest
    public static void Link3() {
		Task_Main.throwException = true;

        Task t = new Task();
        t.Id = Id.valueOf(Schema.sObjectType.Task.getKeyPrefix() + '000000000001');
        t.Subject = 'Task Subject';

        system.assertEquals(Task_Main.Link(t), '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.Id + '" target="_blank">' + t.Subject + '</a>', 'These link definitions should match');
    }

    @isTest
    public static void ISNULL1() {
		Task_Main.throwException = true;

        string src = null;
        string sub = 'Substitute';

        system.assertEquals(Task_Main.ISNULL(src, sub), sub, 'If the value is null, it should return the substitute string');
        src = 'Source';
        system.assertEquals(Task_Main.ISNULL(src, sub), src, 'If the value is not null, it should return the value');
    }

    @isTest
    public static void ISNULL2() {
		Task_Main.throwException = true;

        datetime dt = null;
        string sub = 'Substitute';

        system.assertEquals(Task_Main.ISNULL(dt, sub), sub, 'If the datetime is null, return the substitute string');
        dt = system.now();
        system.assertEquals(Task_Main.ISNULL(dt, sub), dt.format(), 'If the datetime is not null, return the formatted version of it');
    }

    @isTest
    public static void StatusIsCompleted() {
		Task_Main.throwException = true;

        Task t = new Task();

        system.assertEquals(false, Task_Main.StatusIsCompleted(t), 'If Status is null, return false');
        t.Status = 'New';
        system.assertEquals(false, Task_Main.StatusIsCompleted(t), 'If Status is not completed, return false');
        t.Status = 'Completed';
        system.assertEquals(true, Task_Main.StatusIsCompleted(t), 'If Status is completed, return true');
    }

    @isTest
    public static void StatusHasChanged() {
		Task_Main.throwException = true;

        Task a = new Task();
        Task b = new Task();

        a.Status = 'Completed';
        b.Status = 'Completed';
        system.assertEquals(false, Task_Main.StatusHasChanged(a, b), 'Should be false if they are the same');
        a.Status = 'New';
        system.assertEquals(true, Task_Main.StatusHasChanged(a, b), 'Should be true if they are not the same');
    }

    @isTest
    private static Map<Id, Apttus__APTS_Agreement__c> SetupAgreement() {
		Task_Main.throwException = true;

        Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
        a.Id = Id.valueOf(Schema.sObjectType.Apttus__APTS_Agreement__c.getKeyPrefix() + '000000000001');
        Map<Id, Apttus__APTS_Agreement__c> aa = new Map<Id, Apttus__APTS_Agreement__c>();
        aa.put(a.Id, a);
        return aa;
    }

    @isTest
    public static void isAgreementTask() {
		Task_Main.throwException = true;

        Task_Main.Agreements = SetupAgreement();

        Task t = new Task();
        system.assertEquals(false, Task_Main.isAgreementTask(t), 'Should be false if there is no WhatId assigned');
        t.WhatId = Id.valueOf(Schema.sObjectType.Apttus__APTS_Agreement__c.getKeyPrefix() + '000000000002');
        system.assertEquals(false, Task_Main.isAgreementTask(t), 'Should be false if the WhatId doesn\'t match');
        t.WhatId = Task_Main.Agreements.values()[0].Id;
        system.assertEquals(true, Task_Main.isAgreementTask(t), 'Should be true if the WhatId exists');
    }

/*
    @isTest
    public static void isValidSubject() {
        Task t = new Task();

        system.assertEquals(false, Task_Main.isValidSubject(t), 'If there is no subject it should return false');
        Task_Main.Subjects = new Set<string>{ 'All' };
        system.assertEquals(true, Task_Main.isValidSubject(t), 'If the Subjects contains All then it should return true');
        Task_Main.Subjects = new Set<string>{ 'Test' };
        t.Subject = 'DNE';
        system.assertEquals(false, Task_Main.isValidSubject(t), 'If the subject doesn\'t exist it should return false');
        t.Subject = 'Test';
        system.assertEquals(true, Task_Main.isValidSubject(t), 'If the subject exists it should return true');
    }
*/

    @isTest 
    public static void CreateNewAgreementImplementationTask()
    {
		User u = cpqUser_c.CurrentUser;

      	Task_Main.Agreements = SetupAgreement();
       	Task t = new Task();

		for (string language : languages) {
			u.LanguageLocaleKey = language;
			update u;

			system.runas(u) {
		    	if (TaskConfigSetting.CPQ != null) {
					Task_Main.throwException = true;

		    		TaskConfigSetting__c setting = [SELECT Id
		    	    	                            FROM TaskConfigSetting__c
		    	        	                        WHERE Name = 'System Properties' LIMIT 1];
		    		setting.Notify_CS_when_Agrmnt_Actvtd_Assigned__c = 'NASSC - Customer Service Queue';
		    		update setting;

		        	t.WhatId = Task_Main.Agreements.values()[0].Id;

		    	    Task_Main.CreateNewAgreementImplementationTask(t.WhatId);

			        system.assertEquals(1,
			        					Task_Main.tasksToCreate.size(),
			        					language + ': Should be one task in the list to create');

					Task_Main.tasksToCreate.clear();
			        TaskConfigSetting.Reset();
			        TaskConfigSetting.Clear();
		    	}
			}
		}
    }

    @isTest
    static void NotifyOwnerwhenCDdone() {
		Task_Main.throwException = true;
		TaskConfigSetting.throwException = true;

    	if (TaskConfigSetting.CPQ != null) {
            //////insert cpqConfigSetting_cTest.create();
            //////cpqConfigSetting_c.reload();

	    	Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
	    	insert a;

	    	List<Task> tasks = new List<Task>();

	    	Task t1 = new Task();
	    	t1.WhatId = a.Id;
	    	t1.Subject = 'Request for a Non Standard Agreement';
	    	tasks.add(t1);
	    	Task t2 = new Task();
	    	t2.WhatId = a.Id;
	    	t2.Subject = 'Should not be added';
	    	tasks.add(t2);
	    	insert tasks;

	   		TaskConfigSetting__c setting = [SELECT Id
	   	    	                            FROM TaskConfigSetting__c
	   	        	                        WHERE Name = 'System Properties' LIMIT 1];
	   		setting.Notify_Owner_when_CD_done_Subject_s__c = 'Request for a Non Standard Agreement';
	   		setting.Notify_Owner_when_CD_done_Template__c = 'Notify Owner when CD done';
	   		update setting;
			TaskConfigSetting.Reset();

			Task_Main.throwException = true;
			TaskConfigSetting.throwException = true;

	    	t1.Status = 'Completed';
	    	update t1;
    	}
    }

    @isTest
    static void NotifyCSAgreementActivated() {
		Task_Main.throwException = true;
		TaskConfigSetting.throwException = true;

    	if (TaskConfigSetting.CPQ != null) {
            //////insert cpqConfigSetting_cTest.create();
            //////cpqConfigSetting_c.reload();

	   		TaskConfigSetting__c setting = [SELECT Id
	   	    	                            FROM TaskConfigSetting__c
	   	        	                        WHERE Name = 'System Properties' LIMIT 1];
	   	    setting.Org_Wide_Address_Display_Name__c = 'Medtronic - CPQ Task';
		    setting.Notify_CS_when_Agrmnt_Actvtd_Assigned__c = 'NASSC - Customer Service Queue';
		    setting.Notify_CS_when_Agrmnt_Actvtd_Template__c = 'Customer Service Notification Email Outright Quote VF';
		    setting.Notify_CS_when_Agrmnt_Actvtd_To_s__c = 'paul.berglund@medtronic.com' + CRLF +
														   'sarah.j.roth@medtronic.com' + CRLF +
														   'capitalequipment.customerservice@covidien.com';
	   		update setting;
			TaskConfigSetting.Reset();

			Task_Main.throwException = true;
			TaskConfigSetting.throwException = true;

	    	Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
            a.Using_TBD_Dollars__c = false;
            a.Using_CPP_Funds__c = false;
            a.T_HLVL_VA__c = false;
	    	insert a;

			system.assertNotEquals(null, a.Id, 'Should have created an Agreement');

	    	Task t = new Task();
	    	t.WhatId = a.Id;
	    	t.Subject = 'Activated Agreement';
	    	t.Status = 'Completed';
	    	insert t;

	    	system.assertNotEquals(null, t.Id, 'Should have create a Task');
    	}
    }

    @isTest
    static void NotifyOwnerAgreementImplemented() {
		Task_Main.throwException = true;
		TaskConfigSetting.throwException = true;

    	if (TaskConfigSetting.CPQ != null) {
            ///////insert cpqConfigSetting_cTest.create();
            ///////cpqConfigSetting_c.reload();

	    	Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
	    	insert a;

	    	List<Task> tasks = new List<Task>();

	    	Task t1 = new Task();
	    	t1.WhatId = a.Id;
	    	t1.Subject = 'Agreement Implementation';
	    	tasks.add(t1);
	    	Task t2 = new Task();
	    	t2.WhatId = a.Id;
	    	t2.Subject = 'Should not be added';
	    	tasks.add(t2);
	    	insert tasks;

	   		TaskConfigSetting__c setting = [SELECT Id
	   	    	                            FROM TaskConfigSetting__c
	   	        	                        WHERE Name = 'System Properties' LIMIT 1];
	   		setting.Notify_Owner_Ag_Implemented_Template__c = 'Notify Owner Agreement Implemented';
	   		update setting;
			TaskConfigSetting.Reset();

			Task_Main.throwException = true;
			TaskConfigSetting.throwException = true;

	    	t1.Status = 'Completed';
	    	update t1;
    	}
    }
/*
    @isTest
    public static void testChangeChildsOwnerIdOnChangeofTaskOwner() {

        //Account a = new Account(Name = 'test');
        //insert a;

        //Opportunity o = new Opportunity(AccountId = a.Id, Forecast_Category = 'Develop', Opportunity_Name = 'test', OwnerId = UserInfo.getUserId(), Probability = 50.0, Revenue_Start_Date = system.Now(), Stage = 'Develop');
        //insert o;

        Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c();
        insert ag;

        User u = [SELECT Id FROM User WHERE EmployeeNumber = '144341'];

        Task t = new Task(WhatId = ag.Id, Priority = 'Normal', Status = 'In Progress', Type = 'Other');
        system.runAs(u) {
            insert t;
        }

        Task r = [SELECT OwnerId FROM Task WHERE Id = :t.Id];

        system.AssertEquals(r.OwnerId, UserInfo.getUserId());
    }
*/
}