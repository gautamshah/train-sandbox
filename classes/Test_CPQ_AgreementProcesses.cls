/*

@UsedBy: CPQ_AgreementProcesses

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
09/14/2016      Isaac Lewis         Created
09/14/2016      Isaac Lewis         Refactored test from Test_CPQ_SSG_AgreementDocumentTrigger to Test_SendScrubPOActivationEmail
10/21/2016      Isaac Lewis         Added Test_RMSAgreementDirect_NoActionOnSSGDirectDealTypes
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
01/11/2016      Isaac Lewis         Patched Test_RMSAgreementDirect_NoActionOnSSGDirectDealTypes to apply SSG Price List
===============================================================================
*/

@isTest
public class Test_CPQ_AgreementProcesses
{

    static List<Apttus__APTS_Agreement__c> agmts;
    static List<Document> documents;

    static void createTestData()
    {
        CPQ_Variable__c sendExtraSmartCartActivationEmail = new CPQ_Variable__c(Name = 'Send Extra Smart Cart Activation Email', Value__c = 'true');
        insert sendExtraSmartCartActivationEmail;

        Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
        insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        Product2 product = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG, 'TEST', 1);
        insert product;

        Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), AccountId = acct.Id, Capital_Disposable__c = 'Capital', Type = 'New Customer', Financial_Program__c = 'Advanced Tech Bridge', Promotion_Program__c = 'Capnography Customer Care- PM', StageName='Draft');
        insert opp;

        List<Apttus_Config2__PriceList__c> priceLists = cpqPriceList_c.PriceLists.values();

        // TODO: Test logic to ensure Surgical deals don't set to RMS Price List Id
        Map<string, RecordType> agreementRecordTypes = cpqDeal_RecordType_c.getRecordTypesBySObjectAndName().get(cpq_u.AGREEMENT_SOBJECT_TYPE);
        RecordType scrubPOLocked = agreementRecordTypes.get('Scrub_PO_Locked');
        RecordType customKitLocked = agreementRecordTypes.get('Custom_Kit_Locked');
        RecordType smartCartLocked = agreementRecordTypes.get('Smart_Cart_Locked');

        Id userId  = cpqUser_c.CurrentUser.Id;

        agmts = new List<Apttus__APTS_Agreement__c>();
        agmts.add(cpqAgreement_cTest.create(1, null, userId, opp.Id, acct.Id, scrubPOLocked.Id, null, c.Id, null, null, null, null, null, null));
        agmts.add(cpqAgreement_cTest.create(2, null, userId, opp.Id, acct.Id, customKitLocked.Id, null, c.Id, null, null, null, null, null, null));
        agmts.add(cpqAgreement_cTest.addSSGvalues(cpqAgreement_cTest.create(3, null, userId, opp.Id, acct.Id, smartCartLocked.Id, null, c.Id, null, null, null, null, null, null)));

        insert agmts;

        Apttus_Config2__PriceListItem__c priceListItem = new Apttus_Config2__PriceListItem__c(Apttus_Config2__PriceListId__c = priceLists.get(0).Id, Apttus_Config2__ProductId__c = product.Id);
        insert priceListItem;

        Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c(Apttus_Config2__Status__c='Saved', Apttus_Config2__VersionNumber__c=1, Apttus_Config2__BusinessObjectType__c='Agreement', Apttus_Config2__BusinessObjectId__c = agmts.get(0).Id, Apttus_Config2__BusinessObjectRefId__c = agmts.get(0).Id, Apttus_CMConfig__AgreementId__c = agmts.get(0).Id, Apttus_Config2__PriceListId__c = priceLists.get(0).Id, Apttus_Config2__EffectivePriceListId__c = priceLists.get(0).Id);
        insert pc;

        CPQ_Variable__c surgicalDebugActivationEmail = new CPQ_Variable__c(Name = CPQ_AgreementProcesses.SURGICAL_DEBUG_ACTIVATION_EMAIL, Value__c = 'testemail@test.com');
        insert surgicalDebugActivationEmail;

        documents = new List<Document>();
        documents.add(new Document(Name = 'Test_File_1', FolderId = cpqUser_c.CurrentUser.Id, Body = Blob.valueOf('123')));
        documents.add(new Document(Name = 'Test_File_2', FolderId = cpqUser_c.CurrentUser.Id, Body = Blob.valueOf('321')));
        insert documents;
    }

    @isTest static void Test_SendScrubPOActivationEmail() {

        createTestData();
        Apttus__APTS_Agreement__c agmt = agmts.get(0);

        Test.startTest();
            Apttus__Agreement_Document__c agmtDoc1 = new Apttus__Agreement_Document__c(Apttus__Agreement__c = agmt.Id, Name = 'Test_File_1', Apttus__URL__c = '/servlet/servlet.FileDownload/Send_to_Customer_Service.png?file=' + documents[0].Id, Apttus__Type__c = 'Final Electronic Copy', Apttus__Path__c = 'http://Test_File_1');
            insert agmtDoc1;

            Apttus__Agreement_Document__c agmtDoc2 = new Apttus__Agreement_Document__c(Apttus__Agreement__c = agmt.Id, Name = 'Test_File_2', Apttus__URL__c = '/servlet/servlet.FileDownload/Send_to_Customer_Service.png?file=' + documents[1].Id, Apttus__Type__c = 'Final Electronic Copy', Apttus__Path__c = 'http://Test_File_2');
            insert agmtDoc2;
        Test.stopTest();

    }

    @isTest static void Test_SendCustomKitActivationEmail() {

        createTestData();

        Test.startTest();
            Apttus_Agreement_Trigger_Manager.SendCustomKitActivationEmail_Exec_Num ++;
            for (Apttus__APTS_Agreement__c a : agmts)
                a.Apttus__Status__c = 'Activated';
            try {
                update agmts;
            } catch (Exception e){}
        Test.stopTest();

    }

    @isTest static void Test_SendSmartCartActivationEmail () {

        createTestData();
        Apttus__APTS_Agreement__c agmt = agmts.get(2);

        Test.startTest();
            Apttus_Agreement_Trigger_Manager.SendSmartCartActivationEmail_Exec_Num ++;
            for (Apttus__APTS_Agreement__c a : agmts)
                a.Apttus__Status__c = 'Activated';
            try {
                update agmts;
            } catch (Exception e){}
        Test.stopTest();

    }

    @isTest static void Test_RMSAgreementDirect_NoActionOnSSGDirectDealTypes()
    {
        // WHEN direct surgical agreement is created
        createTestData();
        Apttus__APTS_Agreement__c agmt = agmts.get(0);
        // TODO: Centralize price list assignment logic. Surgical logic is in process builder (for proposals) and CPQ_SSG_CreateDeal.cls (for agreements)
        agmt.Apttus_CMConfig__PriceListId__c = cpqPriceList_c.SSG.Id;
        update agmt;

        Test.startTest();

            System.debug('Test Agreement: ' + agmt);
            agmt = [SELECT Id, Name, Apttus_CMConfig__PriceListId__c, Apttus_CMConfig__PriceListId__r.Name FROM Apttus__APTS_Agreement__c WHERE Id = :agmts.get(0).Id LIMIT 1];
            System.debug('Test Agreement SOQL: ' + agmt);

        Test.stopTest();

        // THEN Surgical price list is assigned
        System.debug('agmt:' + agmt);
        System.debug('agmt.Apttus_CMConfig__PriceListId__r.Name:' + agmt.Apttus_CMConfig__PriceListId__r.Name);
        System.assert(agmt.Apttus_CMConfig__PriceListId__r.Name.contains('SSG'));
        // AND RMS price list does not override it
        System.assert(!agmt.Apttus_CMConfig__PriceListId__r.Name.contains('RMS'));

    }

}