/**
Test class

CPQ_DM_Batch_Contract

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-16      Yuli Fintescu       Created
11/02/2016   Paul Berglund   Commented out to save space, but keep in case we
                             do something similar again
===============================================================================
*/
@isTest
private class Test_CPQ_DM_Batch_Contract
{
/*
    static List<User> testUsers;
    static void createTestData() {
        testUsers = new List<User> {
            new User(FirstName = 'API', LastName = 'Data Loader', Username = 'apidataloader@covidien.com.unittest', 
                         Email = 'apidataloader@covidien.com.unittest', Alias = 'apidtldr', CommunityNickname = 'apidtldr', 
                         ProfileId = '00eU0000000dLrtIAE', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1', 
                         LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US')
        };
        insert testUsers;
        
        try {
        CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = '00eU0000000dLrtIAE', CPQ_Proposal_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Proposal_After__c = true);
        insert testTrigger;
        } catch (Exception e) {}
        
        List<CPQ_Variable__c> testVars = new List<CPQ_Variable__c> {
            new CPQ_Variable__c(Name = 'Proxy EndPoint', Value__c = 'Test Proxy EndPoint'),
            new CPQ_Variable__c(Name = 'RMS PriceList ID', Value__c = testPriceList.ID),
            new CPQ_Variable__c(Name = 'HPG Group Code', Value__c = 'SG0150')
        };
        insert testVars;
        
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
        List<Account> testAccounts = new List<Account>{
            new Account(Status__c = 'Active', Name = 'Test Pristine', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
        };
        insert testAccounts;
        
        List<Apttus__APTS_Agreement__c> testAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Account__c = testAccounts[0].ID, Legacy_External_ID__c = 'D-111111'),
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement 2', Apttus__Account__c = testAccounts[0].ID)
        };
        insert testAgreements;
        
        List<Partner_Root_Contract__c> testRoots = new List<Partner_Root_Contract__c> {
            new Partner_Root_Contract__c(Name = '011111', Root_Contract_Name__c = '011111', Root_Contract_Number__c = '011111', Direction__c = 'I')
        };
        insert testRoots;
        
        List<CPQ_STG_Partner_Contract__c> testStageContracts = new List<CPQ_STG_Partner_Contract__c> {
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = true, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = '011111', STGPC_Direction__c = 'I'),
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = true, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = '011111', STGPC_Direction__c = 'O'),
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = false, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = 'D011111', STGPC_ContractName__c = 'Test Contract', STGPC_RootContractNumber__c = '011111', STGPC_EffectiveDate__c = System.Today(), STGPC_ExpirationDate__c = System.Today().addYears(1), STGPC_Direction__c = 'I'),
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = false, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = 'R011111', STGPC_ContractName__c = 'Test Contract', STGPC_RootContractNumber__c = '011111', STGPC_EffectiveDate__c = System.Today(), STGPC_ExpirationDate__c = System.Today().addYears(1), STGPC_Direction__c = 'O'),

            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = true, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = '022222', STGPC_Direction__c = 'I'),
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = false, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = 'D022222', STGPC_ContractName__c = 'Supersede Test Contract', STGPC_RootContractNumber__c = '022222', STGPC_EffectiveDate__c = System.Today(), STGPC_ExpirationDate__c = System.Today().addYears(1), STGPC_Direction__c = 'I'),
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = false, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = 'R022222', STGPC_ContractName__c = 'Supersede Test Contract', STGPC_RootContractNumber__c = '022222', STGPC_EffectiveDate__c = System.Today(), STGPC_ExpirationDate__c = System.Today().addYears(1), STGPC_Direction__c = 'O'),
            
            //agreement not found
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = true, STGPC_Deal_External_ID__c = 'D-333333', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = '033333', STGPC_Direction__c = 'I'),
            //account not found
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = true, STGPC_Deal_External_ID__c = 'D-444444', STGPC_Account_External_ID__c = 'US-222222', STGPC_ContractNumber__c = '044444', STGPC_Direction__c = 'I'),
            //root not found
            new CPQ_STG_Partner_Contract__c(STGPC_IsRoot__c = false, STGPC_Deal_External_ID__c = 'D-111111', STGPC_Account_External_ID__c = 'US-111111', STGPC_ContractNumber__c = 'R055555', STGPC_ContractName__c = 'No Root', STGPC_RootContractNumber__c = '055555')
        };
        insert testStageContracts;
    }
    
    static testMethod void myUnitTest() {
        createTestData();
        
        System.runAs(testUsers[0]) {
            Test.startTest();
                CPQ_DM_Batch_Contract p = new CPQ_DM_Batch_Contract();
                Database.executeBatch(p);
            Test.stopTest();
        }
    }
*/
}