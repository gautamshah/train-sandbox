/****************************************************************************************
* Name    : Class: IDNSC_Relationship_TriggerDispatcher
* Author  : Gautam Shah
* Date    : 8/12/2014
* Purpose : Dispatches to necessary classes when invoked by IDNSC_Relationship trigger
* 
* Dependancies: 
* 	Class: IDNSC_Relationship_Main
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
public with sharing class IDNSC_Relationship_TriggerDispatcher 
{
	public static Set<String> executedMethods = new Set<String>();	
	public static void Main(List<IDNSC_Relationship__c> newList, Map<Id, IDNSC_Relationship__c> newMap, List<IDNSC_Relationship__c> oldList, Map<Id, IDNSC_Relationship__c> oldMap, Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isExecuting)
	{
		if(isUpdate && isAfter)
		{
			if(!executedMethods.contains('createChatterGroup'))
			{
				Map<String, IDNSC_Relationship__c> relNameMap = new Map<String, IDNSC_Relationship__c>();
				for(IDNSC_Relationship__c newRel : newList)
				{
					IDNSC_Relationship__c oldRel = oldMap.get(newRel.Id);
					if(oldRel != null && newRel.Current_Event_State__c == 'Preparation Activity' && oldRel.Current_Event_State__c != 'Preparation Activity')
					{
						relNameMap.put(newRel.Name, newRel);
					}
				}
				IDNSC_Relationship_Main.createChatterGroup(relNameMap);
			}
			if(!executedMethods.contains('setTaskAssignedDate'))
			{
				List<IDNSC_Relationship__c> rels = new List<IDNSC_Relationship__c>();
				for(IDNSC_Relationship__c newRel : newList)
				{
					IDNSC_Relationship__c oldRel = oldMap.get(newRel.Id);
					if(oldRel != null && oldRel.Current_Event_State__c != newRel.Current_Event_State__c)
					{
						rels.add(newRel);
					}
				}
				IDNSC_Relationship_Main.setTaskAssignedDate(rels);
			}
		}
	}
}