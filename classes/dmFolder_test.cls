@isTest
public class dmFolder_test
{
	@isTest
	public static void constructors()
	{
		dmFolder g = new dmFolder();
		
		dmFolder__c sg = new dmFolder__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(sg);
		g = new dmFolder(sc);
		
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<dmFolder__c>{ sg });
		g = new dmFolder(ssc);
	}
	
	@isTest static void record()
	{
		Id a = dm_test.createApplication();
		Id f = dm_test.createSubFolder(a);
		dmFolder dm = new dmFolder();
		dm.record = dmFolder.fetch(f);
		dmFolder__c r = dm.record;
		
		dmFolder__c t = dmFolder.TopLevelFolder(f);
		dmFolder__c tl = dm.TopLevelFolder;
		dmFolder__c app = dm.ApplicationFolder;
		app = dmFolder.ApplicationFolder(f);
		
		string breadcrumb = dmFolder.buildBreadCrumb(f);
	}
	
	@isTest
	public static void sharingmethods()
	{
		Id a = dm_test.createApplication();
		dmFolder.deleteSharing();
		dmFolder.fixSharing();
	}
    
}