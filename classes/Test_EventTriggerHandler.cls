/****************************************************************************************
 * Name    : Test_EventTriggerHandler 
 * Author  : Tejas Kardile
 * Date    : 15/1/2014 
 * Purpose : Test UpdateEventLocationField trigger and trigger handler
 * Dependencies: UpdateEventLocationField Trigger
 *             , Event_TriggerHandler
 *             , Event Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
@isTest
private class Test_EventTriggerHandler {

  static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account a = new Account(Name='TestAcctc');
        insert a;
        RecordType rt = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
         Contact c = new Contact();
          c.LastName = 'Tester';
          c.AccountId = a.Id;
          c.Connected_As__c = 'Administrator';
          c.RecordTypeId = rt.Id;
          c.Department_picklist__c = 'Other';
          c.Other_Department__c = 'Accounting';
          insert c;
        List<Event> lsteve= new List<Event>();
        Event event = new Event(WhatId = a.ID,whoID = c.id, StartDateTime = datetime.now(),EndDateTime = datetime.now(),Subject = 'Go Live');
        lsteve.add(event);
        insert lsteve;
        
        Event_TriggerHandler controller = new Event_TriggerHandler(TRUE,10);
        controller.OnbeforeInsertUpdate(lsteve);
    }
        
}