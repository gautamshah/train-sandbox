/**
Test class

CPQ_ValidationCallBack

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-04      Yuli Fintescu       Created
===============================================================================
*/ 
@isTest
public class Test_CPQ_ValidationCallBack
{
    public static void createTestData(List<Apttus_Config2__LineItem__c> lineSOs, List<Apttus_Config2__ProductConfiguration__c> configSOs)
    {
        insert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        List<Partner_Root_Contract__c> testContracts = new List<Partner_Root_Contract__c> {
            new Partner_Root_Contract__c(Name = '11111', Group_Code__c = 'SG0150', Root_Contract_Name__c = '11111'),
            new Partner_Root_Contract__c(Name = '22222', Group_Code__c = 'SG0150', Root_Contract_Name__c = '22222')
        };
        insert testContracts;

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values())
        {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }
      
        User personRunningTest = cpqUser_c.CurrentUser;
        personRunningTest.OrganizationName__c = 'RMS';
        update personRunningTest;
      
        List<Apttus_Proposal__Proposal__c> testProposals = new List<Apttus_Proposal__Proposal__c>
        {
            new Apttus_Proposal__Proposal__c(ownerId = personRunningTest.Id, Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Price_Offer_Letter')),
            new Apttus_Proposal__Proposal__c(ownerId = personRunningTest.Id, Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Current_Price_Quote')),
            new Apttus_Proposal__Proposal__c(ownerId = personRunningTest.Id, Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Locally_Negotiated_Agreement_LNA')),
            new Apttus_Proposal__Proposal__c(ownerId = personRunningTest.Id, Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('COOP_Plus_Program'))
        };
        insert testProposals;

        List<Apttus_Config2__ProductConfiguration__c> testConfigs = new List<Apttus_Config2__ProductConfiguration__c>
        {
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[0].ID, Apttus_Config2__PriceListId__c = cpqPriceList_c.RMS.Id, Apttus_Config2__Status__c = 'New', Apttus_Config2__VersionNumber__c = 1),
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[1].ID, Apttus_Config2__PriceListId__c = cpqPriceList_c.RMS.Id, Apttus_Config2__Status__c = 'New', Apttus_Config2__VersionNumber__c = 1),
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[2].ID, Apttus_Config2__PriceListId__c = cpqPriceList_c.RMS.Id, Apttus_Config2__Status__c = 'New', Apttus_Config2__VersionNumber__c = 1),
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[3].ID, Apttus_Config2__PriceListId__c = cpqPriceList_c.RMS.Id, Apttus_Config2__Status__c = 'New', Apttus_Config2__VersionNumber__c = 1)
        };
        insert testConfigs;

        OrganizationNames_g.Abbreviations OrgRMS = OrganizationNames_g.Abbreviations.RMS;

        List<Product2> testProducts =
            new List<Product2>
            {
                cpqProduct2_c.create(OrgRMS, 'ITEM Trade-In', 1),
                cpqProduct2_c.create(OrgRMS, 'ITEM Trade-In', 2),
                cpqProduct2_c.create(OrgRMS, 'ITEM Hardware', 1), // (Hier_Level_1__c = 'BL')
    
                cpqProduct2_c.create(OrgRMS, 'ITEM Consumable', 1),
                cpqProduct2_c.create(OrgRMS, 'ITEM Consumable', 2), // (FSS_Price__c = 150, Cost__c = 120),
                cpqProduct2_c.create(OrgRMS, 'ITEM Consumable', 3),
                cpqProduct2_c.create(OrgRMS, 'ITEM Consumable', 4),
                cpqProduct2_c.create(OrgRMS, 'ITEM Consumable', 5),
    
                cpqProduct2_c.create(OrgRMS, 'ITEM Hardware RF', 1), //(Hier_Level_1__c = 'RF'),
                cpqProduct2_c.create(OrgRMS, 'ITEM Hardware CP', 1), //(Hier_Level_1__c = 'CP'),
                cpqProduct2_c.create(OrgRMS, 'ITEM Hardware RS', 1) //(Hier_Level_1__c = 'RS')
            };
            
        testProducts[2].Hier_Level_1__c = 'BL';
        testProducts[4].FSS_Price__c = 150;
        testProducts[4].Cost__c = 120;
        testProducts[8].Hier_Level_1__c = 'RF';
        testProducts[9].Hier_Level_1__c = 'CP';
        testProducts[10].Hier_Level_1__c = 'RS';
        
        insert testProducts;

        List<Apttus_Config2__PriceListItem__c> testPriceListItems =
            cpqPriceListItem_u.fetch(
                Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
                cpqPriceList_c.RMS.Id).values();

        Apttus_Config2__ClassificationName__c testCategory =
            new Apttus_Config2__ClassificationName__c(
                Name = 'Root',
                Apttus_Config2__Type__c = 'Offering',
                Apttus_Config2__Active__c = true,
                Apttus_Config2__HierarchyLabel__c = 'Root');
        insert testCategory;

        List<Apttus_Config2__ClassificationHierarchy__c> testCategoryHierarchies =
            new List<Apttus_Config2__ClassificationHierarchy__c>();
      
      Map<Id, Apttus_Config2__ClassificationHierarchy__c> hierarchyMap = cpqClassificationHierarchy_TestSetup.loadData();
      testCategoryHierarchies.addAll(hierarchyMap.values());
        update testCategoryHierarchies;

        List<Apttus_Config2__ProductClassification__c> testClasses =
            new List<Apttus_Config2__ProductClassification__c>
            {
                //ITEM Hardware
                new Apttus_Config2__ProductClassification__c(
                    Apttus_Config2__ClassificationId__c = testCategoryHierarchies[0].ID,
                    Apttus_Config2__ProductId__c = testProducts[2].ID),
                //ITEM Hardware RF
                new Apttus_Config2__ProductClassification__c(
                    Apttus_Config2__ClassificationId__c = testCategoryHierarchies[0].ID,
                    Apttus_Config2__ProductId__c = testProducts[8].ID),
                //ITEM Hardware CP
                new Apttus_Config2__ProductClassification__c(
                    Apttus_Config2__ClassificationId__c = testCategoryHierarchies[0].ID,
                    Apttus_Config2__ProductId__c = testProducts[9].ID),
                //ITEM Hardware RS
                new Apttus_Config2__ProductClassification__c(
                    Apttus_Config2__ClassificationId__c = testCategoryHierarchies[0].ID,
                    Apttus_Config2__ProductId__c = testProducts[10].ID)
            };
        insert testClasses;

        List<Apttus_Config2__LineItem__c> testLines = new List<Apttus_Config2__LineItem__c>
        {
            //trade-in < min-price
            new Apttus_Config2__LineItem__c(
                Apttus_Config2__ChargeType__c = 'Trade-In',
                Apttus_Config2__Quantity__c = 3,
                Apttus_Config2__BasePrice__c = -20,
                Apttus_Config2__ConfigurationId__c = testConfigs[0].ID,
                Apttus_Config2__IsPrimaryLine__c = true,
                Apttus_Config2__ProductId__c = testProducts[0].ID,
                Apttus_Config2__PriceListItemId__c = testPriceListItems[0].ID,
                Apttus_Config2__ItemSequence__c = 1,
                Apttus_Config2__LineNumber__c = 1,
                Apttus_Config2__PrimaryLineNumber__c = 1),
            //trade-in quantity > hardware quantity
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Trade-In',     Apttus_Config2__Quantity__c = 3, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[1].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[1].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 2, Apttus_Config2__PrimaryLineNumber__c = 2),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Hardware',     Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[2].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[2].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 3, Apttus_Config2__PrimaryLineNumber__c = 3),

            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Consumable',   Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__AdjustmentType__c = 'Base Price Override', Apttus_Config2__BasePriceOverride__c = 100, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[3].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[3].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 4, Apttus_Config2__PrimaryLineNumber__c = 4),
            //cost > unit price, fss > unit price
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Consumable',   Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__AdjustmentType__c = 'Price Factor',        Apttus_Config2__AdjustmentAmount__c = 0.5, Apttus_Config2__Cost__c = 120, Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[4].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[4].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 5, Apttus_Config2__PrimaryLineNumber__c = 5),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Consumable',   Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__AdjustmentType__c = '% Discount',          Apttus_Config2__AdjustmentAmount__c = 50,   Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[5].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[5].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 6, Apttus_Config2__PrimaryLineNumber__c = 6),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Consumable',   Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__AdjustmentType__c = 'Discount Amount',     Apttus_Config2__AdjustmentAmount__c = 100,  Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[6].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[6].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 7, Apttus_Config2__PrimaryLineNumber__c = 7),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Consumable',   Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__AdjustmentType__c = 'Price Override',      Apttus_Config2__AdjustmentAmount__c = 100,  Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[7].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[7].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 8, Apttus_Config2__PrimaryLineNumber__c = 8),

            //no adjustment
            new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = testConfigs[1].ID, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__AdjustmentType__c = 'Price Factor', Apttus_Config2__AdjustmentAmount__c = 2, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[3].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[3].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1),

            //lna, 2 different roots, hpg
            new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = testConfigs[2].ID, Current_Root_Contract__c = '11111', Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[3].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[3].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = testConfigs[2].ID, Current_Root_Contract__c = '22222', Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[4].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[4].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 2, Apttus_Config2__PrimaryLineNumber__c = 2),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = testConfigs[2].ID, Current_Root_Contract__c = '22222', Apttus_Config2__IsPrimaryLine__c = false, Apttus_Config2__ProductId__c = testProducts[4].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[5].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 3, Apttus_Config2__PrimaryLineNumber__c = 3),

            //coop, no consumable, no bla, rfb, rsa, cpc consumables
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Hardware',     Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__ConfigurationId__c = testConfigs[3].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[8].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[8].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Hardware',     Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__ConfigurationId__c = testConfigs[3].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[9].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[9].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 2, Apttus_Config2__PrimaryLineNumber__c = 2),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ChargeType__c = 'Hardware',     Apttus_Config2__Quantity__c = 1, Apttus_Config2__BasePrice__c = 200, Apttus_Config2__ConfigurationId__c = testConfigs[3].ID, Apttus_Config2__IsPrimaryLine__c = true, Apttus_Config2__ProductId__c = testProducts[10].ID, Apttus_Config2__PriceListItemId__c = testPriceListItems[10].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 3, Apttus_Config2__PrimaryLineNumber__c = 3)
        };
        insert testLines;

        lineSOs.addAll([Select ID, Name, Apttus_Config2__ChargeType__c,
                            Apttus_Config2__Quantity__c,
                            Apttus_Config2__BasePrice__c,
                            Apttus_Config2__AdjustmentType__c,
                            Apttus_Config2__BasePriceOverride__c,
                            Apttus_Config2__AdjustmentAmount__c,
                            Current_Root_Contract__c,
                            FSS_Price__c,
                            Min_Price__c,
                            Apttus_Config2__Cost__c,
                            Proposal_Record_Type_Developer_Name__c,
                            Product_Name__c,
                            Product_ID__c,
                            Apttus_Config2__ConfigurationId__c,
                            Apttus_Config2__IsPrimaryLine__c,
                            Apttus_Config2__ProductId__c,
                            Apttus_Config2__PriceListItemId__c,
                            Apttus_Config2__ItemSequence__c,
                            Apttus_Config2__LineNumber__c,
                            Is_Bundle_Header__c,
                            Product_Code__c,
                            Hier_Level_1__c,
                            Hier_Level_2__c,
                            Record_Type__c,
                            Agreement_Record_Type__c
                        From Apttus_Config2__LineItem__c
                        Where ID in: testLines]);

        configSOs.addAll([Select ID, Name, Apttus_QPConfig__Proposald__c,
                            Apttus_Config2__PriceListId__c,
                            Apttus_Config2__Status__c ,
                            Promotion_Subtotal__c,
                            Apttus_Config2__VersionNumber__c,
                            Duration__c,
                            Total_Annual_Sensor_Commitment_Amount__c,
                            TBD_Amount__c,
                            Proposal_Record_Type_Developer_Name__c,
                            Record_Type__c
                        From Apttus_Config2__ProductConfiguration__c
                        Where ID in: testConfigs]);
    }

    static testMethod void myUnitTest() {
        Test.startTest();
            PageReference pageRef = new PageReference('http://yahoo.com');
            Test.setCurrentPage(pageRef);

            CPQ_ValidationCallBack testCallBack = new CPQ_ValidationCallBack();
            testCallBack.validateCart(null);

            pageRef = new PageReference('http://yahoo.com?p=CartDetailView');
            Test.setCurrentPage(pageRef);
            testCallBack = new CPQ_ValidationCallBack();
            testCallBack.validateCart(null);

            testCallBack.validateRampLineItems(null, null);
            testCallBack.validateAssetItems(null, null);
        Test.stopTest();
    }
}