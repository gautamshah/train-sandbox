public with sharing class DeleteAllRecords {
	
	public String strParentRecordId{get;set;}
	
	public DeleteAllRecords(ApexPages.StandardController stdController) {
		
		strParentRecordId=ApexPages.currentPage().getParameters().get('Id');
	}


    public PageReference deleteAllSalesOut()
    {
    	List<Sales_Out__c> lsSalesOut = [SELECT Id FROM Sales_Out__c WHERE Cycle_Period__c = :strParentRecordId and Can_be_Deleted__c = true limit 10000];
    	if(lsSalesOut.size()>0)
    		delete lsSalesOut;
    		
    	return new PageReference('/' + strParentRecordId);
    }

    public PageReference deleteAllChannelInventory()
    {
    	List<Channel_Inventory__c> lsChannelInventory = [SELECT Id FROM Channel_Inventory__c WHERE Cycle_Period__c = :strParentRecordId limit 10000];
    	if(lsChannelInventory.size()>0)
    		delete lsChannelInventory;
    		
    	return new PageReference('/' + strParentRecordId);
    }

}