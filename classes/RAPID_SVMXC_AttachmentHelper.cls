/**
 *  Custom attachment work order service report email functionality.
 *  20150113 - Updates to address case where no contact exists yet, per ENG
 *  20150220 - Following changes are done:-
 *               1. Added check that if Org Wide Address and Default Contact Id are null, do not send emails
 *               2. Moved Code from Custom Labels to Custom Settings (Org Wide Address, Default Email Address, Default Contact Id)
 *               3. Added Service Check to stop email functionality
 *               4. Added functionality to add comma seperated list of email addresses in Work Order field and sent the reports to all
 *               5. Added check to stop email to default email address
 *               6. Added functionality to CC'ed Email to Current User
 */
public without sharing class RAPID_SVMXC_AttachmentHelper {
    //flag to indicate that code is executing Salesforce Code Coverage Tests
    public static boolean isTestFlag = false;
    
    public static void emailServiceWorkOrder(List<Attachment> attachmentList){
        String serviceName = 'Email Service Report';
        String defaultEmailAddress = null;
        String orgWideEmailAddress = null;
        String defaultContactId = null;
        String sendToDefaultEmailAddress = null;
        String defaultEmailTemplateName = null;
        String currentUserEmailAddress = null;
        EmailTemplate emailTemplateBase = null;
        
        List<Messaging.SingleEmailMessage> messagesList = new List<Messaging.SingleEmailMessage>(); 
        List<Attachment> validAttachmentList = new List<Attachment>();
        Set<Id> workOrderIdSet = new Set<Id>();
            
        //Check if the service is active or not    
        if (RAPID_SVMXC_ServiceRegister.isActive(serviceName)){
            
            if(System.UserInfo.getUserEmail() != null){
                currentUserEmailAddress = System.UserInfo.getUserEmail();
            }
            
            //Get All custom settings
            List<Email_Service_Report_Settings__c> emailServiceReportSettings = Email_Service_Report_Settings__c.getall().values();
            if(emailServiceReportSettings != null){
                for(Email_Service_Report_Settings__c emailSettings : emailServiceReportSettings){
                    if(emailSettings.Name.equals('Org_Wide_Email_Address_Record')){
                        orgWideEmailAddress = emailSettings.Value__c;
                    }else if(emailSettings.Name.equals('Default_Email_Address')){
                        defaultEmailAddress = emailSettings.Value__c;
                    }else if(emailSettings.Name.equals('Send To Default Email Address')){
                        sendToDefaultEmailAddress = emailSettings.Value__c;
                    }else if(emailSettings.Name.equals('Default_Contact_ID')){
                        defaultContactId = emailSettings.Value__c;
                    }else if(emailSettings.Name.equals('Email Template Name')){
                        defaultEmailTemplateName = emailSettings.Value__c;
                    }
                }
            }
            
            //Check if org wide email address is added or else return
            if(!RAPID_SVMXC_AttachmentHelper.isTestFlag && (orgWideEmailAddress == null || orgWideEmailAddress.trim().equals(''))){
                System.debug('Email Service Report Service - Default Org Wide Address not set, please go to Custom Settings - Email Service Report to add it');
                System.debug('Email Service Report Service - Hence, no report will be sent');
                return;
            }
            
            //Check if default contact id is added or else return
            if(defaultContactId == null || defaultContactId.trim().equals('')){
                System.debug('Email Service Report Service - Default Contact Id not set, please go to Custom Settings - Email Service Report to add it');
                System.debug('Email Service Report Service - Hence, no report will be sent');
                return;
            }
            
            //Load email template used for all e-mails.
            if(defaultEmailTemplateName != null  && !defaultEmailTemplateName.trim().equals('')){
                emailTemplateBase = [Select Id, Name, Subject, Body, HtmlValue from EmailTemplate
                                                             where DeveloperName =:defaultEmailTemplateName limit 1]; 
                System.debug('Email Template Base Name:' + emailtemplatebase.name);
            }
            
            //Send only those which are Service Reports.            
            for (Attachment reportAttachment : attachmentList)  {
                if (reportAttachment.ParentId != null && reportAttachment.ParentId.getSobjectType() == SVMXC__Service_Order__c.SobjectType &&
                    reportAttachment.Name != null && reportAttachment.Name.contains('.pdf') && reportAttachment.Name.contains(reportAttachment.ParentId)) {
                        workOrderIdSet.add(reportAttachment.ParentId);
                        validAttachmentList.add(reportAttachment);
                }
            }
            
            //Get Work Order Details            
            Map<Id, SVMXC__Service_Order__c> workOrderMap = new Map<Id, SVMXC__Service_Order__c>(
                                                        [Select Id, Name, Email_Service_Report__c, Send_Service_Report_to_Customer__c,
                                                                Send_Service_Report_to_Current_User__c 
                                                                from SVMXC__Service_Order__c where Id in :workOrderIdSet]);
            
            for (Attachment reportAttachment : validAttachmentList)  {                         
                SVMXC__Service_Order__c workOrder = workOrderMap.get(reportAttachment.ParentId);
                if (workOrder != null) {
                    //Create Attachment                        
                    Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                    emailAttachment.setFileName(reportAttachment.Name);
                    emailAttachment.setBody(reportAttachment.Body);
                    
                    // Construct the Email Message
                    Messaging.SingleEmailMessage message = null;
                    if(emailTemplateBase != null){
                        System.debug('Email Service Report: Create Dummy Email using template : ' + emailTemplateBase.Name );
                        message = new Messaging.SingleEmailMessage();
                        message.setTemplateId(emailTemplateBase.Id);
                        message.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
                        message.setSaveAsActivity(false);
                        message.setWhatId(reportAttachment.ParentId);
                        message.setTargetObjectId(defaultContactId);  
                        
                        // Send the emails in a transaction, then roll it back in order to use the template with external email addresses
                        // This will count against DML limits so we must be certain that an iPad/offline user will sync (insert/update)
                        // no more than 75 Service Report attachments in a single sync.
                        Savepoint sp = Database.setSavepoint();
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message } );
                        Database.rollback(sp);
                    }
                    
                    //Sent Message to Default Email Address
                    if (defaultEmailAddress != null && sendToDefaultEmailAddress != null && sendToDefaultEmailAddress.equalsIgnoreCase('True')){
                            Messaging.SingleEmailMessage message1 = new Messaging.SingleEmailMessage();
                            message1.setOrgWideEmailAddressId(orgWideEmailAddress);
                            message1.setToAddresses(new String[] {defaultEmailAddress});
                            message1.setFileAttachments(new Messaging.EmailFileAttachment[] { emailAttachment } );
                            message1.setSaveAsActivity(false);
                            
                            if(message != null){
                                message1.setSubject(message.getSubject());
                                message1.setPlainTextBody(message.getPlainTextBody());
                                message1.setHtmlBody(message.getHTMLBody());
                            }else{
                                message1.setSubject('Work Order Service Report');
                                message1.setPlainTextBody('');
                                message1.setHtmlBody('');
                            }
                            
                            messagesList.add(message1);
                    }else{
                        System.debug('Email Service Report Service - Default Email Address is not set or flag for sending is set to false');
                        System.debug('Email Service Report Service - Hence, no report will be sent to Default Email Address');
                    }
             
                    //Sent to email addresses mentioned in custom field on Work Order
                    if (workOrder.Email_Service_Report__c != null && workOrder.Send_Service_Report_to_Customer__c == true)     { 
                        Messaging.SingleEmailMessage message2 = new Messaging.SingleEmailMessage();
                        message2.setOrgWideEmailAddressId(orgWideEmailAddress);
                        message2.setFileAttachments(new Messaging.EmailFileAttachment[] { emailAttachment } );
                        message2.setSaveAsActivity(false);
                        message2.setToAddresses(new String[] {workOrder.Email_Service_Report__c});
                        
                        if(workOrder.Send_Service_Report_to_Current_User__c && currentUserEmailAddress != null && !currentUserEmailAddress.trim().equals('')){
                            message2.setCcAddresses(new String[] {currentUserEmailAddress});
                        }
                        
                        if(message != null){
                            message2.setSubject(message.getSubject());
                            message2.setPlainTextBody(message.getPlainTextBody());
                            message2.setHtmlBody(message.getHTMLBody());
                        }else{
                            message2.setSubject('Work Order Service Report');
                            message2.setPlainTextBody('');
                            message2.setHtmlBody('');
                        }
                                                
                        messagesList.add(message2);
                    }else{
                        System.debug('Email Service Report Service - Custom Field is not set or flag for sending is set to false');
                        System.debug('Email Service Report Service - Hence, no report will be sent to Email Address provided in custom field');
                    }
                }else{
                    System.debug('Email Service Report Service : No Work Order found for Attachment : ' + reportAttachment.Name);
                    System.debug('Email Service Report Service - Hence, no report will be sent');
                }
            }
            
            if (!RAPID_SVMXC_AttachmentHelper.isTestFlag && messagesList.size() > 0){
                System.debug('Email handler: Total Emails to be sent : ' + messagesList.size());
                Messaging.sendEmail(messagesList);
                System.debug('Email handler: Email Sent');
            }
        }else{
            System.debug('Email Service Report Service is in-active, please go to Custom Settings - SVMX Service Register to activate it');
        }
    }
}