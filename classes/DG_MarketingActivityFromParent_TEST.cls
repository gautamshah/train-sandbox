@isTest
private class DG_MarketingActivityFromParent_TEST {
    
    static testMethod void MATest() {
    	
    	Group RMSParentQueue = [Select g.id, g.name From Group g where g.type = 'Queue' 
    	and g.name = 'RMS Parent Lead Queue' LIMIT 1];
    	
    	RecordType RMSRType = [Select r.Name, r.Id From RecordType r
    	where r.SobjectType = 'Lead' and r.Name = 'RMS Lead Type' LIMIT 1];
        
        Lead l1 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = '94080',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        Product_Solution_of_Interest__c = 'Prod1', 
        LastName = 'test', 
        Is_Parent__c = true, 
        Has_Interest__c = true,
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test' 
        );
        
        insert l1;
        
        Marketing_Activity__c ma1 = new Marketing_Activity__c(
        ELOQUA_Activity_Type__c = 'Website Visit', 
        ELOQUA_Activity_Detail__c = 'Detail', 
        ELOQUA_Activity_Date__c = date.today(),
        Lead__c = l1.id
        );
        
        insert ma1;
        
        Lead cl1 = new Lead(
        RecordTypeId = RMSRType.id, 
        PostalCode = '94080',
        OwnerId = RMSParentQueue.id,
        Opportunity_Product_Group__c = 'OPG1', 
        Product_Solution_of_Interest__c = 'Prod1', 
        LastName = 'test', 
        FirstName = 'test', 
        Email = 'test@test.com', 
        Country = 'US', 
        Company = 'test',
        Parent_Lead__c = l1.id
        );
        
        insert cl1;
                
        ApexPages.StandardController sc = new ApexPages.standardController(cl1);        
        DG_MarketingActivityFromParent mactrl = new DG_MarketingActivityFromParent(sc);
        
        List<Marketing_Activity__c> ma = mactrl.getMA();
        
        System.assertEquals(1, ma.size());      
    }
}