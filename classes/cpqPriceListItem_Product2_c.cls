/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date		Id   Initials   Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD	A	PAB			CPR-000		Updated comments section
							AV-000
20161103	A	IL			AV-001		Title of the Jira – Some changes needed to be made to support Medical Supplies

20161115	A	PAB			AV-253		Moved logic to Product2_PriceListItem_u because it's part of the Product2
                                        logic that needs to occur for Product2 trigger

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/    
public class cpqPriceListItem_Product2_c
{

}