/**
Facilitates IC Territory Deletion ETL. Recursive deletion of territories if marked as flagToDelete = TRUE
Collects and passes the list of territories need to be deleted in the right order to IC_Batch_Territory_Deletion batch class for deletion
Territory Deletion runs faster if one at time - batch size should set to 1.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-05-13      Yuli Fintescu       Created
===============================================================================
*/
public class IC_Execute_Territory_Deletion{
    public static void execute() {
        String query = 'Select flagToDelete__c, ' + 
                            'ParentTerritoryId, ' + 
                            'Id, Name, ' + 
                            'ExternalParentTerritoryID__c, ' + 
                            'Custom_External_TerritoryID__c, ' + 
                            'Source__c ' + 
                        'From Territory ' + 
                        'Where flagToDelete__c = TRUE and ' + 
                            'Source__c in (' + includedSourcesInList() + ')';
        List<Territory> scope = Database.query(query);
        
        Set<String> ParentTerritoryNames = new Set<String> ();
        Map<String, String> DeleteTerritoryIDs = new Map<String, String> ();
        for(Territory terr : scope) {
            DeleteTerritoryIDs.put(terr.ID, terr.ParentTerritoryId);
        }
        
        //oganize records to be deleted: DeleteLevel = number layers of ancestors. the records that has most of ancestors are deleted first.
        Map<String, Integer> deleteLevels = new Map<String, Integer> ();
        for (String key : DeleteTerritoryIDs.keySet()) {
            deleteLevels.put(key, 0);
            
            getDeleteLevel(DeleteTerritoryIDs.get(key), 
                DeleteTerritoryIDs, 
                key, 
                deleteLevels);
        }
        
        if (deleteLevels.size() > 0) {
            List<Integer> sorted = deleteLevels.values();
            sorted.sort();
            Integer lowest = sorted[sorted.size()-1];
            
            //collect to-be-deleted records from leafs up
            Map<Integer, List<String>> toDelete = new Map<Integer, List<String>>();
            for (Integer j = lowest; j >= 0; j--) {
            	List<String> l =  new List<String>();
            	
                for (String key : deleteLevels.keySet()) {
                    if (deleteLevels.get(key) == j) {
                        l.add(key);
                        deleteLevels.remove(key);
                    }
                }
                
                toDelete.put(j, l);
	            System.Debug('*** toDelete.get(j) ' + toDelete.get(j));
            }
            
            if (toDelete.size() > 0) {
			    Integer batchSize = 1;
				IC_Config_Setting__c cs = IC_Config_Setting__c.getValues('TerritoryDeletion Batch Size');
				if (cs == null && Test.isRunningTest()) {
					cs = new IC_Config_Setting__c(Name = 'TerritoryDeletion Batch Size', Value__c = '1');
				}
				
			    if (cs != null && cs.Value__c != null) {
			    	try {
			    		batchSize = Integer.valueOf(cs.Value__c);
			    	} catch (Exception e) {}
			    }
			    System.Debug('*** batchSize: ' + batchSize);
    			
                IC_Batch_Territory_Deletion p = new IC_Batch_Territory_Deletion();
                p.toDelete = toDelete;
                p.deleteLevel = lowest;
                p.batchSize = batchSize;
	            System.Debug('*** p.deleteLevel ' + p.deleteLevel);
                Database.executeBatch(p, batchSize);
            }
        }
    }
    
    //recursion calculate number layers of ancestors of each record
    private static void getDeleteLevel(String parentID, Map<String, String> DeleteTerritoryIDs, String terrID, Map<String, Integer> deleteLevels) {
        Boolean hasParent = DeleteTerritoryIDs.containsKey(parentID);
        if (!hasParent) {
            return;
        }
        
        deleteLevels.put(terrID, deleteLevels.get(terrID) + 1);
        getDeleteLevel(DeleteTerritoryIDs.get(parentID), DeleteTerritoryIDs, terrID, deleteLevels);
    }
    
 	public static String includedSourcesInList() {
	    //exclude records from certain sources
	    Set<String> includeds = new Set<String>();
		IC_Config_Setting__c cs = IC_Config_Setting__c.getValues('TerritoryBatch Include Sources');
		if (cs == null && Test.isRunningTest()) {
			cs = new IC_Config_Setting__c(Name = 'TerritoryBatch Include Sources', Value__c = 'US_MANS|YTRE');
		}
		
	    if (cs != null && cs.Value__c != null) {
	    	String[] a = cs.Value__c.split('\\|');
	    	for (String s : a) {
	    		if (!String.isEmpty(s))
	    			includeds.add(s);
	    	}
	    }
    	System.Debug('*** includeds sources: ' + includeds);
    	
 		String s = '';
		for (String a : includeds) {
			if (String.isEmpty(s))
				s = '\'' + a + '\'';
			else
				s = s + ', \'' + a + '\'';
		}
		
		if (String.isEmpty(s))
			s = '\'^^THERE IS NOTHING IN THE INLIST^^\'';
		return s;
 	}
 	
    
}