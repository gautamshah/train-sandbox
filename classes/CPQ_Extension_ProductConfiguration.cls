/**
Extension for Apttus_Config2__ProductConfiguration__c controller, used by
documentUSRMSPromotions VF page.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-10-13      Paul Berglund       Created - originaly used Sales Org to
                                    determine which file to display.  Changed
                                    to display 1 file for both Sales Orgs
                                    because we didn't want to use Profiles
                                    to allow Admin and RS + PM approvers to
                                    see them.
2016-09-02      Isaac Lewis         Added method to return Surgical promo doc
===============================================================================
*/
public with sharing class CPQ_Extension_ProductConfiguration {

    public CPQ_Extension_ProductConfiguration() { }

    public CPQ_Extension_ProductConfiguration(ApexPages.StandardController controller) {
    }

    public PageReference displayPromotions() {
        //User user = null;
        //try {
        //    user = [SELECT Sales_Org_PL__c FROM User WHERE Id = :UserInfo.getUserId()];
        //}
        //catch(Exception ex) {
        //    return null;
        //}

        Id documentId = null;
        try {
            documentId = [SELECT Id FROM Document WHERE Name = 'US RMS Promotions' LIMIT 1].Id;

            //if (user.Sales_Org_PL__c == 'Respiratory Solutions')
            //    documentId = [SELECT Id FROM Document WHERE Name = 'Respiratory Solutions Promotions' LIMIT 1].Id;
            //else if (user.Sales_Org_PL__c == 'Patient Monitoring')
            //    documentId = [SELECT Id FROM Document WHERE Name = 'Patient Monitoring Promotions' LIMIT 1].Id;
            //else
            //    return null;
        }
        catch(Exception ex) {
            system.debug('displayPromotions: ' + ex);
            return null;
        }

        return new PageReference('/servlet/servlet.FileDownload?file=' + documentId);
    }

    public PageReference displayPromotionsSSG() {
        ID documentId = null;
        try {
            documentId = [SELECT Id FROM Document WHERE Name = 'US SSG Promotions' LIMIT 1].Id;
        }
        catch (Exception ex) {
            System.debug('displayPromotionsSSG: ' + ex);
            return null;
        }
        return new PageReference('/servlet/servlet.FileDownload?file=' + documentId);
    }

}