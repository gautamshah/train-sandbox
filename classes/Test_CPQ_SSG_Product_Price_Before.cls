@isTest
private class Test_CPQ_SSG_Product_Price_Before {
	
	@isTest static void Test_CPQ_SSG_Product_Price_Before() {
		Product2 product = new Product2(ProductCode = 'TestProduct1', Name = 'Test Product 1');
		insert product;
		CPQ_SSG_Price_List__c priceList = new CPQ_SSG_Price_List__c(Price_List_Code__c = 'TestPriceList1', Name = 'Test Product 1');
		insert priceList;
		CPQ_SSG_Product_Price__c productPrice = new CPQ_SSG_Product_Price__c(Product_Code__c = 'TestProduct1', Price_List_Code__c = 'TestPriceList1');

		Test.startTest();
			insert productPrice;
			productPrice = [Select Product__c, CPQ_SSG_Price_List__c From CPQ_SSG_Product_Price__c Where Id = :productPrice.Id];
			System.assertEquals(product.Id, productPrice.Product__c);
			System.assertEquals(priceList.Id, productPrice.CPQ_SSG_Price_List__c);
		Test.stoptest();
	}
}