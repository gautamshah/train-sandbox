@isTest
public class DATATYPES_Test
{
	@isTest
	static void isValidId()
	{
		string strId;
		system.assert(!DATATYPES.isValidId(strId));
		
		strId = '000000000000000';
		system.assert(!DATATYPES.isValidId(strId));

		strId = '000000000000000AAA';
		system.assert(DATATYPES.isValidId(strId));

		strId = '001000000000';
		system.assert(!DATATYPES.isValidId(strId));

		strId = '001000000000000';
		system.assert(DATATYPES.isValidId(strId));

		strId = '001000000000000AAA';
		system.assert(DATATYPES.isValidId(strId));

		Id id;
		system.assert(!DATATYPES.isValidId(id));

		id = '000000000000000AAA';		
		system.assert(DATATYPES.isValidId(id));
		
		Account a = new Account();
		a.Name = 'isValidId';
		insert a;

		system.assert(DATATYPES.isValidId(a.Id));
		
		strId = a.Id;
		system.assert(DATATYPES.isValidId(strId));
	}
	
	@isTest
	static void boolToInt()
	{
		system.assertEquals(CONSTANTS.intTRUE, DATATYPES.boolToInt(true));
		system.assertEquals(CONSTANTS.intFALSE, DATATYPES.boolToInt(false));
	}
	
	@isTest
	static void toLowercaseSet()
	{
		Set<string> testdata = new Set<string>();
		testdata.add('A');
		testdata.add('B');
		testdata.add('C');
		
		Set<string> lowercase = DATATYPES.toLowercase(testdata);
		
		for(string member : testdata)
			system.assert(lowercase.contains(member.toLowercase()));
	}
	
	@isTest
	static void toLowercaseList()
	{
		List<string> testdata = new List<string>();
		testdata.add('A');
		testdata.add('B');
		testdata.add('C');
		
		List<string> lowercase = DATATYPES.toLowercase(testdata);
		
		system.assertEquals(testdata[0].toLowercase(), lowercase[0], 'Should both be lowercase');
	}
	
    @isTest static void itShouldReturnToLowercase () {
        Set<String> uppercase = new Set<String>{'AAA','BBb','Ccc','ddd'};
        Set<String> lowercase = DATATYPES.toLowercase(uppercase);
        System.assert(lowercase.contains('aaa'),'aaa: String not made lowercase');
        System.assert(lowercase.contains('bbb'),'bbb: String not made lowercase');
        System.assert(lowercase.contains('ccc'),'ccc: String not made lowercase');
        System.assert(lowercase.contains('ddd'),'ddd: String not made lowercase');
    }

	@isTest
	static void getMultiPicklistValueSet()
	{
		string source = 'a,b,c';
		Set<string> target = new Set<string>{ 'a', 'b', 'c' };
		
		Set<string> resultSet = DATATYPES.getMultiPicklistValueSet(source, ',');
		
		system.assert(target.containsAll(resultSet));
	}
	
	@isTest
	static void getMultiPicklistValueSetWithSeparator()
	{
		string source = 'a;b;c';
		Set<string> target = new Set<string>{ 'a', 'b', 'c' };
		
		Set<string> resultSet = DATATYPES.getMultiPicklistValueSet(source);
		
		system.assert(target.containsAll(resultSet));
	}
	
	@isTest
	static void getSetInnerJoin()
	{
		Set<string> setA = new Set<string>
		{
			'a',
			'b'
		};
		
		Set<string> setB = new Set<string>
		{
			'c',
			'd'
		};
		
		Set<string> setC = new Set<string>
		{
			'e',
			'f'
		};
		
		Set<string> one = new Set<string>();
		one.addAll(setA);
		one.addAll(setB);
		
		Set<string> two = new Set<string>();
		two.addAll(setB);
		two.addAll(setC);
		
		Set<string> innerjoin = DATATYPES.getSetInnerJoin(one, two);
		
		for(string member : setA)
			system.assert(!innerjoin.contains(member));
			
		for(string member : setB)
			system.assert(innerjoin.contains(member));
			
		for(string member : setC)
			system.assert(!innerjoin.contains(member));
	}
}