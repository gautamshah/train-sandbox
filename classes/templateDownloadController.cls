public with sharing class templateDownloadController {
	public pagereference downloadTemplate(){
		pagereference ref;
		String userId = ApexPages.currentPage().getParameters().get('id');
		string tempType = ApexPages.currentPage().getParameters().get('type');
		
		if(userId != null){
			User u = [Select id, LanguageLocaleKey from User where id=:userId];
			if(u.LanguageLocaleKey != null){
				string query = 'Select id from Document where name != null';
				string s='';
				if(tempType=='sales'){
					if(u.LanguageLocaleKey=='zh_TW' || u.LanguageLocaleKey=='zh_CN'){
						//Chinese
						s='SO_Chinese';
					}
					else if(u.LanguageLocaleKey=='ko'){
						//Korean
						s='SO_Korean';
					}                    
					else if(u.LanguageLocaleKey=='en_US'){
						//English
						s='SO_English';
					}
					else{
						s='SO_English';
					}
				}
				
				if(tempType=='channel'){
					if(u.LanguageLocaleKey=='zh_TW' || u.LanguageLocaleKey=='zh_CN'){
						//Chinese
						s='CI_Chinese';
					}
					else if(u.LanguageLocaleKey=='ko'){
						//Korean
						s='CI_Korean';
					}
					else if(u.LanguageLocaleKey=='en_US'){
						//English
						s='CI_English';
					}
					else{
						s='CI_English';
					}
				}
				query = query+' and DeveloperName =\''+s+'\'';
				document doc = database.query(query);
				ref = new pagereference('/distributor/servlet/servlet.FileDownload?file='+doc.Id);
				return ref;							
			}
		}
		
		return ref;
	
	}
	
	static testmethod void m1(){
		 Id pId = [select Id 
                      from Profile 
                     where name = 'System Administrator'].Id;
		 User u2mgr = new User();
         u2mgr.LastName = 'Shmoe Test';
         u2mgr.Business_Unit__c = 'The Unit';
         u2mgr.Franchise__c = 'Burger King';
         u2mgr.email = 'testZXmgr1@covidien.com';
         u2mgr.alias = 'testmgr1';
         u2mgr.username = 'testZXmgr1@covidien.com';
         u2mgr.communityNickName = 'testZXmgr1@covidien.com';
         u2mgr.ProfileId = pId;
         u2mgr.CurrencyIsoCode='SGD'; 
         u2mgr.EmailEncodingKey='ISO-8859-1';
         u2mgr.TimeZoneSidKey='America/New_York';
         u2mgr.LanguageLocaleKey='en_US';
         u2mgr.LocaleSidKey ='en_US';
         u2mgr.Country = 'SG';
         insert u2mgr;
         
        
         Document docSOC = new Document();
         docSOC.Name='SO Chinese';
         docSOC.DeveloperName='SO_Chinese';
         docSOC.Body=blob.valueOf('test');
         docSOC.folderId = UserInfo.getUserId();
         insert docSOC; 
         
         Document docSOK = new Document();
         docSOK.Name='SO Korean';
         docSOK.developerName='SO_Korean';
         docSOK.Body=blob.valueOf('test');
         docSOK.folderId = UserInfo.getUserId();
         insert docSOK;
         
         Document docEn = new Document();
         docEn.Name='SO English';
         docEn.DeveloperName ='SO_English';
         docEn.Body=blob.valueOf('test');
         docEn.folderId = UserInfo.getUserId();
         insert docEn;
         
         Document docCIC = new Document();
         docCIC.Name='CI Chinese';
         docCIC.DeveloperName='CI_Chinese';
         docCIC.Body=blob.valueOf('test');
         docCIC.folderId = UserInfo.getUserId();
         insert docCIC; 
         
         Document docCIK = new Document();
         docCIK.Name='CI Korean';
         docCIK.DeveloperName='CI_Korean';
         docCIK.Body=blob.valueOf('test');
         docCIK.folderId = UserInfo.getUserId();
         insert docCIK;
         
         Document docCIEn = new Document();
         docCIEn.Name='CI English';
         docCIEn.DeveloperName='CI_English';
         docCIEn.Body=blob.valueOf('test');
         docCIEn.folderId = UserInfo.getUserId();
         insert docCIEn;
         
         ApexPages.currentPage().getParameters().put('id',u2mgr.Id);
         ApexPages.currentPage().getParameters().put('type','sales');
         
         templateDownloadController tDC = new templateDownloadController();
         tDC.downloadTemplate();
         
         u2mgr.LanguageLocaleKey='zh_TW';
         update u2mgr;
         tDC.downloadTemplate();  
         
         u2mgr.LanguageLocaleKey='ko';
         update u2mgr;
         tDC.downloadTemplate();
         
         u2mgr.LanguageLocaleKey='en_CA';
         update u2mgr;
         tDC.downloadTemplate();
         
         ApexPages.currentPage().getParameters().put('id',u2mgr.Id);
         ApexPages.currentPage().getParameters().put('type','channel');
         
         templateDownloadController tDC1 = new templateDownloadController();
         tDC1.downloadTemplate();
         
         u2mgr.LanguageLocaleKey='ko';
         update u2mgr;
         tDC.downloadTemplate();
         
         u2mgr.LanguageLocaleKey='zh_TW';
         update u2mgr;
         tDC1.downloadTemplate();
         
         u2mgr.LanguageLocaleKey='en_US';
         update u2mgr;
         tDC1.downloadTemplate();
         
         
	}
}