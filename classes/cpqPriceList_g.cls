/*
MODIFICATION HISTORY
====================
Date		Id	Initials	Jira(s)	Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20170213	A	PAB			AV-280  Created to optimize query usage and resolve issues with
									the creation of the Price Lists
*/
global class cpqPriceList_g extends sObject_g
{
	global cpqPriceList_g()
	{
		super(cpqPriceList_cache.get());
	}

	////
	//// cast
	////
	global static Map<Id, Apttus_Config2__PriceList__c> cast(Map<Id, sObject> sobjs)
	{
		//DEBUG.dump(
		//	new Map<object, object>
		//	{
		//		'method' => 'PriceList_g.cast(Map<Id, sObject> sobjs)',
		//		'sobjs' => sobjs
		//	});
			
		try
		{
			return new Map<Id, Apttus_Config2__PriceList__c>((List<Apttus_Config2__PriceList__c>)sobjs.values());
		}
		catch(Exception e)
		{
			return new Map<Id, Apttus_Config2__PriceList__c>();
		}
	}

	global static Map<object, Map<Id, Apttus_Config2__PriceList__c>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, Apttus_Config2__PriceList__c>> target = new Map<object, Map<Id, Apttus_Config2__PriceList__c>>();
		for(object key : source.keySet())
			target.put(key, cast(source.get(key)));
			
		return target;
	}

	////
	//// put
	////
	global void put(Apttus_Config2__PriceList__c sobj)
	{
		this.cache.put(sobj);
	}
	
	global void put(List<Apttus_Config2__PriceList__c> sobjs)
	{
		this.cache.put(sobjs);
	}

	////
	//// fetch
	////
	global Map<Id, Apttus_Config2__PriceList__c> fetch()
	{
		return cast(this.cache.fetch());
	}
	
	global Apttus_Config2__PriceList__c fetch(Id id)
	{
		return (Apttus_Config2__PriceList__c)this.cache.fetch(id);
	}

	public Map<Id, Apttus_Config2__PriceList__c> fetch(Set<Id> ids)
	{
		return cast(this.cache.fetch(ids));
	}
	
	public Map<Id, Apttus_Config2__PriceList__c> fetch(sObjectField index, object value)
	{
		return cast(this.cache.fetch(index, value));
	}

	global Map<object, Map<Id, Apttus_Config2__PriceList__c>> fetch(sObjectField field)
	{
		return cast(this.cache.fetch(field));
	}
	
	public Map<object, Map<Id, Apttus_Config2__PriceList__c>> fetch(sObjectField index, set<object> values)
	{
		return cast(this.cache.fetch(index, values));
	}
	////
	//// Additional methods
	////
}