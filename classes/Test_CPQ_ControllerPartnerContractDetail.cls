/**
Test class

CPQ_Controller_PartnerContract_Detail
CPQ_ContractInfoInvoke

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
2016-11-02      Isaac Lewis         Added cpqConfigSetting_cTest initialization
===============================================================================
*/
@isTest
private class Test_CPQ_ControllerPartnerContractDetail {
    static void createTestData() {

        ///////insert cpqConfigSetting_cTest.create();
		///////cpqConfigSetting_c.reload();

        CPQ_Variable__c testEndPoint = new CPQ_Variable__c(Name = 'Proxy EndPoint', Value__c = 'Test Proxy EndPoint');
        insert testEndPoint;

        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
        Schema.RecordTypeInfo agRT = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('COOP Plus Program-AG');

        List<Commitment_Type__c> testCommitTypes = new List<Commitment_Type__c> {
            new Commitment_Type__c (Name = 'IP', Description__c = 'IP'),
            new Commitment_Type__c (Name = 'G3', Description__c = 'G3')
        };
        insert testCommitTypes;

        List<Account> testAccounts = new List<Account>{
            new Account(Name = 'Test GPO', Status__c = 'Active', Account_External_ID__c = 'US-999999_GPO', RecordTypeID = gpoRT.getRecordTypeId()),
            new Account(Name = 'Test Account', Status__c = 'Active', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Name = 'Test Facility', Status__c = 'Active', Account_External_ID__c = 'US-222222', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Name = 'Test Distributor', Status__c = 'Active', Account_External_ID__c = 'US-333333', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
        };
        insert testAccounts[0];
        testAccounts[1].Primary_GPO_Buying_Group_Affiliation__c = testAccounts[0].ID;
        upsert testAccounts Account_External_ID__c;

        List<Partner_Root_Contract__c> testRoots = new List<Partner_Root_Contract__c> {
            new Partner_Root_Contract__c (Name = '00112233', Account__c = testAccounts[1].ID, Commitment_Type__c = testCommitTypes[0].ID, Effective_Date__c = System.Today(), Expiration_Date__c = System.Today().addYears(1), Root_Contract_Name__c = 'Test Root', Root_Contract_Number__c = '00112233'),
            new Partner_Root_Contract__c (Name = '00998877', Account__c = testAccounts[1].ID, Commitment_Type__c = testCommitTypes[0].ID, Effective_Date__c = System.Today(), Expiration_Date__c = System.Today().addYears(1), Root_Contract_Name__c = 'Test Root No Related Contracts', Root_Contract_Number__c = '00998877')
        };
        insert testRoots;

        List<Related_Partner_Contract__c> testRelatedContracts = new List<Related_Partner_Contract__c> {
            new Related_Partner_Contract__c (Name = 'D00112233', Root_Contract__c = testRoots[0].ID),
            new Related_Partner_Contract__c (Name = 'R00112233', Root_Contract__c = testRoots[0].ID)
        };
        insert testRelatedContracts;

        Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeID = agRT.getRecordTypeId(), Apttus__Status__c = 'Activated', Apttus__Account__c = testAccounts[1].ID, Partner_Root_Contract__c = testRoots[0].ID, Duration__c = '3 Years');
        insert testAgreement;

        List<Agreement_Participating_Facility__c> testFacilities = new List<Agreement_Participating_Facility__c> {
            new Agreement_Participating_Facility__c(Agreement__c = testAgreement.ID, Account__c = testAccounts[2].ID)
        };
        insert testFacilities;

    }

    static testMethod void myUnitTest() {

        createTestData();

        Test.setMock(WebServiceMock.class, new cpqPartnerContractInfoServiceMock());

            PageReference pageRef = Page.CPQ_PartnerContract_Deail;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('rootNo', '00998877');
            
        //have to isolate web service call out in transaction that is separate from DML.
        Test.startTest();
            CPQ_Controller_PartnerContract_Deail p = new CPQ_Controller_PartnerContract_Deail();
            system.assertEquals(1, p.Customers.size());
            system.assertEquals('Test Distributor', p.Customers[0].Name);
        Test.stopTest();
            ApexPages.currentPage().getParameters().put('rootNo', 'NONEXIST');
            p = new CPQ_Controller_PartnerContract_Deail();

    }
}