global class Profile_g extends sObject_g
{
	global Profile_g()
	{
		super(Profile_cache.get());
	}

	////
	//// cast
	////
	global static Map<Id, Profile> cast(Map<Id, sObject> sobjs)
	{
	   try
	   {
	        return (sobjs != null) ?
	            new Map<Id, Profile>((List<Profile>)sobjs.values()) :
	            new Map<Id, Profile>();
        
	   }
	   catch(Exception ex)
       {
       	  System.debug(LoggingLevel.ERROR, ex);
          return new Map<Id, Profile>();
       }
	}

	global static Map<object, Map<Id, Profile>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, Profile>> target = new Map<object, Map<Id, Profile>>();
		for(object key : source.keySet())
			target.put(key, cast(source.get(key)));
			
		return target;
	}
	
	////
	//// fetch
	////
	global Map<Id, Profile> fetch()
	{
	   return cast(this.cache.fetch());
	}
	
	global Map<object, Map<Id, Profile>> fetch(sObjectField field, set<object> values)
	{
		return cast(this.cache.fetch(field, values));
	}

	////
	//// fetch sets
	////
	global Profile fetch(sObjectField field, object value)
	{
		Map<Id, Profile> profileMap =  cast(this.cache.fetch(field, value));
		return !profileMap.isEmpty() ? profileMap.values()[0] : null;

    }

	////
	//// Additional methods
	////
}