@istest
private class CustomQuoteMyApprovalsLaunControllerTest extends CustomApprovalsConstants
{
    static testmethod void method_One()
    {

        User usr = cpqUser_TestSetup.getSysAdminUser();
        if (usr == null) {
            usr = cpqUser_c.CurrentUser;
        }

        cpqOpportunity_TestSetup.buildScenario_CPQ_Opportunity();
        Account acc = cpq_TestSetup.testAccounts.get(0);
        Contact ct = cpq_TestSetup.testContacts.get(0);
        Opportunity opp = cpq_TestSetup.testOpportunities.get(0);

        Apttus_Proposal__Proposal__c prop = cpqProposal_TestSetup.generateProposal(acc,ct,opp,
            cpqRecordType_TestSetup.getRecordType('Apttus_Proposal__Proposal__c').DeveloperName);
        prop.ownerId = usr.Id;
        prop.Approval_Preview_Status__c='Pending';
        insert prop;

        sObject record = prop;
        CustomQuoteMyApprovalsLaunchController ctrl = new CustomQuoteMyApprovalsLaunchController(new ApexPages.StandardController(record));
        PageReference pgrf = ctrl.doLaunchMyApprovals();
        PageReference pgref = ctrl.doReturn();
        Id objId = ctrl.getCtxObjectId();
        Boolean errorMsg = ctrl.hasErrors;

    }

}