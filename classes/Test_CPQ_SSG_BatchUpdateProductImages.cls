/**
Test class for the CPQ_SSG_BatchUpdateProductImages and CPQ_SSG_ScheduleUpdateProductImages classes
that update images on surgical products.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
04/18/2016      Bryan Fry           Created
===============================================================================
*/
@isTest
public with sharing class Test_CPQ_SSG_BatchUpdateProductImages {
	public static String CRON_EXP = '0 0 0 1 1 ? 2022';

	static testMethod void Test_CPQ_SSG_BatchUpdateProductImages() {
		Product2 surgicalProduct = new Product2(ProductCode = 'Test_Coverage_Product', Name = 'Test_Coverage_Product', Apttus_Surgical_Product__c = true);
		Product2 nonSurgicalProduct = new Product2(ProductCode = 'Test_Coverage_Product', Name = 'Test_Coverage_Product', Apttus_Surgical_Product__c = false);

		List<Product2> products = new List<Product2>();
		products.add(surgicalProduct);
		products.add(nonSurgicalProduct);
		insert products;

		Attachment att = new Attachment(ParentId = surgicalProduct.Id, Body = Blob.valueOf('12345'), Name = 'Test Attachment Image.jpg');
		insert att;

		surgicalProduct.Apttus_Config2__IconId__c = att.Id;
		update surgicalProduct;

		Product_SKU__c usProductSKU = new Product_SKU__c(SKU__c = 'Test_Coverage_Product', Country__c = 'US', Product_Image_URL__c = 'http://www.covidien.com/imageServer.aspx/doc.gif?contentID=1&contenttype=image/jpeg');
		Product_SKU__c caProductSKU = new Product_SKU__c(SKU__c = 'Test_Coverage_Product', Country__c = 'CA', Product_Image_URL__c = 'http://www.covidien.com/imageServer.aspx/doc.gif?contentID=2&contenttype=image/jpeg');
		List<Product_SKU__c> productSKUs = new List<Product_SKU__c>();
		productSKUs.add(usProductSKU);
		productSKUs.add(caProductSKU);
		insert productSKUs;

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new Test_CPQ_HTTPCalloutMock());

			CPQ_SSG_BatchUpdateProductImages batch = new CPQ_SSG_BatchUpdateProductImages();
			Database.executeBatch(batch);
		Test.stopTest();

		List<Attachment> attachments = [Select Id, Name From Attachment Where ParentId = :surgicalProduct.Id];
		System.assertEquals(attachments.size(), 1);
		System.assertEquals(attachments[0].Name, 'Test_Coverage_Product Image.jpg');

		surgicalProduct = [Select Id, ProductCode, Apttus_Config2__IconId__c From Product2 Where Id = :surgicalProduct.Id];
		System.assertEquals(surgicalProduct.Apttus_Config2__IconId__c, attachments[0].Id);
	}

	static testMethod void Test_CPQ_SSG_ScheduleUpdateProductImages() {
		Product2 surgicalProduct = new Product2(ProductCode = 'Test_Coverage_Product', Name = 'Test_Coverage_Product', Apttus_Surgical_Product__c = true);
		Product2 nonSurgicalProduct = new Product2(ProductCode = 'Test_Coverage_Product', Name = 'Test_Coverage_Product', Apttus_Surgical_Product__c = false);

		List<Product2> products = new List<Product2>();
		products.add(surgicalProduct);
		products.add(nonSurgicalProduct);
		insert products;

		Attachment att = new Attachment(ParentId = surgicalProduct.Id, Body = Blob.valueOf('12345'), Name = 'Test Attachment Image.jpg');
		insert att;

		surgicalProduct.Apttus_Config2__IconId__c = att.Id;
		update surgicalProduct;

		Product_SKU__c usProductSKU = new Product_SKU__c(SKU__c = 'Test_Coverage_Product', Country__c = 'US', Product_Image_URL__c = 'http://www.covidien.com/imageServer.aspx/doc.gif?contentID=1&contenttype=image/jpeg');
		Product_SKU__c caProductSKU = new Product_SKU__c(SKU__c = 'Test_Coverage_Product', Country__c = 'CA', Product_Image_URL__c = 'http://www.covidien.com/imageServer.aspx/doc.gif?contentID=2&contenttype=image/jpeg');
		List<Product_SKU__c> productSKUs = new List<Product_SKU__c>();
		productSKUs.add(usProductSKU);
		productSKUs.add(caProductSKU);
		insert productSKUs;

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new Test_CPQ_HTTPCalloutMock());

			String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new CPQ_SSG_ScheduleUpdateProductImages());

			CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

			System.assertEquals(CRON_EXP, ct.CronExpression);
			System.assertEquals(0, ct.TimesTriggered);
			System.assertEquals('2022-01-01 00:00:00', String.valueOf(ct.NextFireTime));

		Test.stopTest();
	}
}