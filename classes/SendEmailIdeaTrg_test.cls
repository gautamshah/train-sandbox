@isTest
private class SendEmailIdeaTrg_test{

    static testmethod void m1(){
        
         testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        
        RecordType rt = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        c.Connected_As__c = 'Administrator';
        c.RecordTypeId = rt.Id;
        c.Department_picklist__c = 'Other';
        c.Other_Department__c = 'Accounting';
        insert c;
        
        Employee__c emp = new Employee__c();
        emp.name='test employee';
        emp.email__c='c@k.com';
        insert emp;
        
        Convidien_Contact__c cc = new Convidien_Contact__c();
        cc.Employee__c = emp.Id;
        cc.Distributor_Name__c = a.Id;
        cc.Type__c='Customer Service Agent';
        insert cc;
        
        ID profileID = [ Select id from Profile where name like '%Distributor%' Limit 1].id;
        User u = new User( FirstName = 'Test-01'
                         , LastName = 'Test-02'
                         , Title = 'Rep'
                         , email='testinguser@fakeemail.com'
                         , profileid = profileid
                         , UserName='testinguser1212@fakeemail.com'
                         , alias='tusary'
                         , CommunityNickName='tusary'
                         , TimeZoneSidKey='America/New_York'
                         , LocaleSidKey='en_US'
                         , EmailEncodingKey='ISO-8859-1'
                         , contactId= c.Id
                         , Asia_Team_Asia_use_only__c='PRM Team'
                         , LanguageLocaleKey='en_US');
        
        insert u;
        List<Community> listCommunity = [Select c.Name, c.IsActive, c.Id From Community c where name='Asia Distributor Community Zone' and IsActive=true limit 1];
        System.runAs(u){
        
            Idea ideaObj = new Idea();
            ideaObj.title='testIdea';
            ideaObj.body='testBody';           
            ideaObj.CommunityId = listCommunity[0].Id;
            insert ideaObj;
            
            IdeaComment ic = new IdeaComment();
            ic.CommentBody= 'test';
            ic.IdeaId = ideaObj.Id;
            insert ic;
            
        }
    }
    
    static testmethod void m2(){
        
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        
        RecordType rt = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = a.Id;
        c.Connected_As__c = 'Administrator';
        c.RecordTypeId = rt.Id;
        c.Department_picklist__c = 'Other';
        c.Other_Department__c = 'Accounting';
        insert c;
        
        Employee__c emp = new Employee__c();
        emp.name='test employee';
        emp.email__c='c@k.com';
        insert emp;
        /*
        Convidien_Contact__c cc = new Convidien_Contact__c();
        cc.Employee__c = emp.Id;
        cc.Distributor_Name__c = a.Id;
        cc.Type__c='Customer Service Agent';
        insert cc;
        */
        ID profileID = [ Select id from Profile where name like '%Distributor%' Limit 1].id;
        User u = new User( FirstName = 'Test-01'
                         , LastName = 'Test-02'
                         , Title = 'Rep'
                         , email='testinguser@fakeemail.com'
                         , profileid = profileid
                         , UserName='testinguser1212@fakeemail.com'
                         , alias='tusary'
                         , CommunityNickName='tusary'
                         , TimeZoneSidKey='America/New_York'
                         , LocaleSidKey='en_US'
                         , EmailEncodingKey='ISO-8859-1'
                         , contactId= c.Id
                         , Asia_Team_Asia_use_only__c='PRM Team'
                         , LanguageLocaleKey='en_US');
        
        insert u;
        List<Community> listCommunity = [Select c.Name, c.IsActive, c.Id From Community c where name='Asia Distributor Community Zone' and IsActive=true limit 1];
        System.runAs(u){
        
            Idea ideaObj = new Idea();
            ideaObj.title='testIdea';
            ideaObj.body='testBody';          
            ideaObj.CommunityId = listCommunity[0].Id;
            insert ideaObj;
            
        }
    }


}