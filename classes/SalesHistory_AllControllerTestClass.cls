@isTest 
private class SalesHistory_AllControllerTestClass
{
static testMethod void validateSalesHistory()
{
Account a = new Account(Name='Test Account');
Insert a;
ApexPages.StandardController sc = new ApexPages.StandardController(a);
SalesHistory_AllController h = new SalesHistory_AllController(sc);
list<Sales_History__c > ssList = new list<Sales_History__c>();
Sales_History__c ss = new Sales_History__c(Name='Test S2 Sales History', Business_Unit__c='SS',Customer__c=a.Id);
ssList.add(ss);
Sales_History__c ss1 = new Sales_History__c(Name='Test S2 Sales History', Business_Unit__c='SS',Customer__c=a.Id);
ssList.add(ss1);
Sales_History__c ss2 = new Sales_History__c(Name='Test S2 Sales History', Business_Unit__c='SS',Customer__c=a.Id);
ssList.add(ss2);

list<Sales_History__c > vtList = new list<Sales_History__c>();
Sales_History__c vt = new Sales_History__c(Name='Test VT Sales History', Business_Unit__c='VT',Customer__c=a.Id);
vtList.add(vt);
Sales_History__c vt1 = new Sales_History__c(Name='Test VT Sales History', Business_Unit__c='VT',Customer__c=a.Id);
vtList.add(vt1);
Sales_History__c vt2 = new Sales_History__c(Name='Test VT Sales History', Business_Unit__c='VT',Customer__c=a.Id);
vtList.add(vt2);

list<Sales_History__c > rmsList = new list<Sales_History__c>();
Sales_History__c rms = new Sales_History__c(Name='Test RMS Sales History', Business_Unit__c='RMS',Customer__c=a.Id);
rmsList.add(rms);
Sales_History__c rms1 = new Sales_History__c(Name='Test RMS Sales History', Business_Unit__c='RMS',Customer__c=a.Id);
rmsList.add(rms1);
Sales_History__c rms2 = new Sales_History__c(Name='Test RMS Sales History', Business_Unit__c='RMS',Customer__c=a.Id);
rmsList.add(rms2);

insert ssList;
insert vtList;
insert rmsList;

list<Sales_History__c > SalesHistoryAll = [Select Id, Name, Business_Unit__c, Source__c, SalePrice__c, InvoiceDate__c, TotalPrice__c from Sales_History__c where Customer__c=:a.Id];
list<Sales_History__c > SalesHistorySS = [Select Id, Name, Business_Unit__c, Source__c, SalePrice__c, InvoiceDate__c, TotalPrice__c from Sales_History__c where Customer__c=:a.Id and Business_Unit__c = 'SS'];
list<Sales_History__c > SalesHistoryVT = [Select Id, Name, Business_Unit__c, Source__c, SalePrice__c, InvoiceDate__c, TotalPrice__c from Sales_History__c where Customer__c=:a.Id and Business_Unit__c = 'VT'];
list<Sales_History__c > SalesHistoryRMS = [Select Id, Name, Business_Unit__c, Source__c, SalePrice__c, InvoiceDate__c, TotalPrice__c from Sales_History__c where Customer__c=:a.Id and Business_Unit__c = 'RMS'];


}
}