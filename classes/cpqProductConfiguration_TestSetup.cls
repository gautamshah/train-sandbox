@isTest
public class cpqProductConfiguration_TestSetup
{
    public static Apttus_Config2__ProductConfiguration__c generateProductConfiguration(
            Apttus_Config2__PriceList__c priceList,
            sObject businessObject)
    {

        // Universal Product Configuration Information
            Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c();
            pc.Apttus_Config2__Status__c = 'Saved';
            pc.Apttus_Config2__VersionNumber__c = 1;
            pc.Apttus_Config2__PriceListId__c = priceList.Id;
            pc.Apttus_Config2__EffectivePriceListId__c = priceList.Id;

        // Business Object Information
            pc.Apttus_Config2__BusinessObjectId__c = businessObject.Id;
            pc.Apttus_Config2__BusinessObjectRefId__c = businessObject.Id;
            if (businessObject.getSObjectType() == cpq_u.PROPOSAL_SOBJECT_TYPE) {
                // Proposal Logic
                // pc.Apttus_Config2__BusinessObjectType__c = 'Agreement';
                // pc.Apttus_CMConfig__AgreementId__c = businessObject.Id;
            }
            if (businessObject.getSObjectType() == cpq_u.AGREEMENT_SOBJECT_TYPE) {
                pc.Apttus_Config2__BusinessObjectType__c = 'Agreement';
                pc.Apttus_CMConfig__AgreementId__c = businessObject.Id;
            }

        return pc;

    }
    
}