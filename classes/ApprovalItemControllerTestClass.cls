@isTest

public class ApprovalItemControllerTestClass {
    static testMethod void ApprovalItemControllerTestClass () {
        
        RecordType rt = [SELECT id FROM RecordType WHERE Name='ASIA - Opportunity' AND SobjectType='Opportunity'];
        RecordType rtSample = [SELECT id FROM RecordType WHERE Name='HK Sample Request' AND SobjectType='Samples__c'];

        List<User> userList = new List<User>();
        Id pId = [select Id from Profile where name = 'Asia - HK'].Id;
        
        User mgr = new User(
            LastName = 'Manager',
            Region__c = 'ASIA',
            email = 'tmgr@covidien.com',
            alias = 'tmgr',
            username = 'tmgr@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='SGD',
            EmailEncodingKey='UTF-8',
            TimeZoneSidKey='Asia/Singapore',
            LanguageLocaleKey='en_SG',
            LocaleSidKey ='en_SG',
            Country = 'SG');
        userList.add(mgr);
        
        User u = new User(
            LastName = 'Rep',
            Region__c = 'ASIA',
            email = 'trep@covidien.com',
            alias = 'trep',
            username = 'trep@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='SGD',
            EmailEncodingKey='UTF-8',
            TimeZoneSidKey='Asia/Singapore',
            LanguageLocaleKey='en_SG',
            LocaleSidKey ='en_SG',
            Country = 'SG');
        userList.add(u);
        
        insert userList;
        
        u.ManagerId = mgr.Id;
        update u;
//        User u = [SELECT id FROM User WHERE Profile.Name = 'Asia - HK' LIMIT 1];
        
        Account a = new Account(Name = 'Test Account');
        insert a;
        Opportunity testOp = new Opportunity(
          Name = 'Test Opp',
          RecordTypeId = rt.Id,
          Type = 'New Customer - Conversion',
          Capital_Disposable__c = 'Disposable',
          StageName = 'Identify',
          CloseDate = Date.today(),
          AccountId = a.Id
        );
        insert testOp;
        
        Samples__c sample = new Samples__c(Opportunity__c=testOp.id, Approval_Status__c = 'Draft', 
                                           Purpose_of_Request__c='TEST', RecordTypeId=rtSample .id);
        insert sample;
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(sample.Id);
            
        req1.setSubmitterId(u.Id); 
        req1.setProcessDefinitionNameOrId('HK_Sample_Request_Approval_Process_1_0');
        req1.setSkipEntryCriteria(true);
        Approval.ProcessResult result1 = Approval.process(req1);  
            
        ApprovalItemController aic = new ApprovalItemController();
        aic.relatedRecordId = sample.id;
        aic.getAllApprovalStep();
        
    }
}