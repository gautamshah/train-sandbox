/****************************************************************************************
 * Name    : CPQ_SSG_Controller_ContractPriceLookup
 * Author  : Suresh Somepalli
 * Date    : 04/09/2015
 * Purpose : Contains the logic for CPQ_SSG_ContractPriceLookup visualforce page
 * Dependencies: CPQ_SSG_ContractPriceLookup.page
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 4/20/2015   Bryan Fry            Changed class name and added code to call pricing web service.
 * 5/27/2015   Clay Hartman         added logic to retrive the business unit for the current user and to incude that in the product queries
 * 12/15/2015  Bryan Fry            Added code to get products where product family matches user's region and business unit
 * 04/04/2016  Isaac Lewis          Added qtyUnitPrice field for presentation of unitPrice (Price) / qty (Unit Qty)
 * 06/10/2016  Isaac Lewis          Added userProductGBU to resolve "Surgical Innovations (SI)" user business unit to "S2" product GBU
 * 07/19/2016  Isaac Lewis          Updated fetchMemberDetailsByName to update account for multiple account lookups on the same page
 * 02/07/2017  Isaac Lewis          Replaced user query in getProductPrice with central User_u query.
 *****************************************************************************************/
public class CPQ_SSG_Controller_ContractPriceLookup{

    public CPQ_SSG_Search_Filter__c searchFilter {get;set;}
    public Account acct {get;set;}
    public String accName {get;set;}
    public String accountExternalId {get;set;}
    public String accountDisplay {get;set;}
    public String productSKU {get;set;}

    public User user {get;set;}
    public String userGBU {get;set;}
    public String userRegionAndUnit {get;set;}
    private String userProductGBU;

    public List<String> productNames {get;set;}
    public List<String> productNameSingle {get;set;}

    public Map<String,ProductPrice> productSkuMap {get;set;}
    public Map<Id,ProductPrice> productIdMap {get;set;}

    //CONTROLLER
    public CPQ_SSG_Controller_ContractPriceLookup(){
        searchFilter = new CPQ_SSG_Search_Filter__c(
            Account__c = (ApexPages.CurrentPage().getParameters().containsKey('account') &&
                          ApexPages.CurrentPage().getParameters().get('account') instanceof ID)?
                Id.valueOf(ApexPages.CurrentPage().getParameters().get('account')):
                null);


        if (searchFilter.Account__c != null)
        {
           acct = [SELECT Id, Name, Account_External_Id__c from Account where id =:searchFilter.Account__c];
           accname = acct.Name;
           accountExternalId = acct.Account_External_Id__c;
           accountDisplay = accname + ' (' + accountExternalId + ')';
        }


        //   acct = [SELECT Id, Name, Account_External_Id__c from Account where id =:searchFilter.Account__c];

        productNames = new List<String>();

        productNameSingle = new List<String>();

        productSkuMap = new Map<String,ProductPrice>();
        productIdMap = new Map<Id,ProductPrice>();

        userGBU=null;
        user = cpqUser_c.CurrentUser;
        userGBU = user.Business_Unit__c;

        userProductGBU = user.Business_Unit__c;
        if(user.Business_Unit__c == 'Surgical Innovations (SI)'){
          userProductGBU = 'S2';
        }

        userRegionAndUnit = user.Region__c + ':' + userProductGBU + '%';

        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Account= ' + Account.id));

    } //END of Controller

    //Action Function by Account Name
    public void fetchMemberDetailsByname(){
        try {
            accName=null;
            acct = [SELECT Id, Name, Account_External_Id__c from Account where id =:searchFilter.Account__c];
            accName = acct.name;
            accountExternalId = acct.Account_External_Id__c;
            accountDisplay = accname + ' (' + accountExternalId + ')';
        } catch(System.QueryException e) {
            system.debug('====>'+accName);
            accName='No Matches Found';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'No Matches Found'));
            System.debug('List has no Rows ');
        }
    } //END of fetchMemberDetailsByname


    public void fetchProductNames(){
        String SearchText = '%'+searchFilter.Name+'%';
        List<Product2> products = [SELECT Id,Name,ProductCode,Apttus_Surgical_Product__c,UOM_Desc__c,Catalog_Price__c,Unit_Quantity__c
                                   FROM Product2
                                   WHERE GBU__c = :userProductGBU
                                   AND id =:searchFilter.Product__c
                                   And Family like :userRegionAndUnit
                                   And Apttus_Surgical_Product__c = true];

        if(products.size() > 0) {
            Product2 prod = products[0];
            ProductPrice pp = new ProductPrice(prod.Id, prod.Name, prod.ProductCode, prod.UOM_Desc__c,prod.Unit_Quantity__c, prod.Catalog_Price__c);
            productSkuMap.put(prod.ProductCode, pp);
            productIdMap.put(prod.Id, pp);
        } else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'No Matches Found.  GBU = ' + userProductGBU + ', Region = ' + user.Region__c));
        }
    } //End of fetchProductNames



    public void DeleteProd(){
        String prodId = apexpages.currentpage().getparameters().get('ProdID');
        System.debug('product id to delete' + prodId);
        ProductPrice productToRemove = productIdMap.get(prodId);
        System.debug('product to remove = ' + productToRemove);
        System.debug('productSkuMap keys = ' + productSkuMap.keySet());
        productSkuMap.remove(productToRemove.productCode);
        productIdMap.remove(prodId);

     } //End of DeleteProd



    public void getPrices() {
        // Call web service with account Id and product codes to get prices.
        if (productSkuMap.size() == 0) {
            return;
        }

        CPQ_ProxyMultiPriceService.PricingRequestType request;
        CPQ_ProxyMultiPriceService.PricingRespType response;
        List<CPQ_Error_Log__c> errors = new List<CPQ_Error_Log__c>();

        try {
            Integer requestId = Math.abs(Integer.valueOF((System.now().getTime() / 32767)));

            String accountCRN = acct.Account_External_Id__c.substring(3);
            CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
            //  set the web service timeout
            service.timeout_x = 30000;
            request = new CPQ_ProxyMultiPriceService.PricingRequestType();
            request.countryField = 'US';
            request.currencyField = '';
       //     request.designatorField = 'B';
            request.designatorField = 'D';
            request.gBUField = '';
            request.requestDateField = System.Today();
            request.requestDateFieldSpecified = true;
            request.requestIDField = String.valueOf(requestId);

            // One will get the lowest sequence number price, all will return multiple that need to be sorted through.
            request.ShowOnePriceField = 'One';
            //request.showOnePriceField = 'All';
            String u = userInfo.getUserName();
            request.userIDField = u.substring(0, u.indexOf('@'));

            request.shipToDSField = new CPQ_ProxyMultiPriceService.ArrayOfShipTo();
            request.shipToDSField.ShipTo = new CPQ_ProxyMultiPriceService.ShipTo[]{};

            CPQ_ProxyMultiPriceService.ShipTo shipTo = new CPQ_ProxyMultiPriceService.ShipTo();
            shipTo.shipTo1Field = accountCRN;
            request.shipToDSField.ShipTo.add(shipTo);

            request.sKUsField = new CPQ_ProxyMultiPriceService.ArrayOfSKUType();
            request.sKUsField.SKUType = new CPQ_ProxyMultiPriceService.SKUType[]{};

            List<String> codes = new List<String>(productSkuMap.keySet());
            Integer size = codes.size();
            for(integer i = 0; i < size; i++) {
                String key = codes[i];
                ProductPrice prd = productSkuMap.get(key);

                CPQ_ProxyMultiPriceService.SKUType t = new CPQ_ProxyMultiPriceService.SKUType();
                //t.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
                t.quantityField = 1;
                t.quantityFieldSpecified = true;
                t.sKUCodeField = key;
                t.uOMField = prd.uom == null ? 'EA' : prd.uom; //prod.UOM__c;

                request.sKUsField.SKUType.add(t);
            }

            System.Debug('*** request ' + request);

            if (Test.isRunningTest()) {
                response = Test_CPQ_PricingCallBack.createModkResponse();
            } else {
                response = service.MultiPrice(request);
            }

            System.Debug('*** response.PropertyChanged ' + response.PropertyChanged);
            System.Debug('*** response.countryField ' + response.countryField);
            System.Debug('*** response.currencyField ' + response.currencyField);
            System.Debug('*** response.designatorField ' + response.designatorField);
            System.Debug('*** response.gBUField ' + response.gBUField);
            System.Debug('*** response.requestDateField ' + response.requestDateField);
            System.Debug('*** response.requestDateFieldSpecified ' + response.requestDateFieldSpecified);
            System.Debug('*** response.requestErrorDescriptionField ' + response.requestErrorDescriptionField);
            System.Debug('*** response.requestErrorField ' + response.requestErrorField);
            System.Debug('*** response.requestIDField ' + response.requestIDField);
            System.Debug('*** response.showOnePriceField ' + response.showOnePriceField);
            System.Debug('*** response.userIDField ' + response.userIDField);

            for (CPQ_ProxyMultiPriceService.ShipToResp s : response.shipToDSField.ShipToResp) {
                System.Debug('*** s.PropertyChanged ' + s.PropertyChanged);
                System.Debug('*** s.billToField ' + s.billToField);
                System.Debug('*** s.shipToErrorDescriptionField ' + s.shipToErrorDescriptionField);
                System.Debug('*** s.shipToErrorField ' + s.shipToErrorField);
                System.Debug('*** s.shipToField ' + s.shipToField);

                for (CPQ_ProxyMultiPriceService.SKUTypeResp k : s.sKUsField.SKUTypeResp) {
                    System.Debug('*** k.PropertyChanged ' + k.PropertyChanged);
                    System.Debug('*** k.quantityField ' + k.quantityField);
                    System.Debug('*** k.quantityFieldSpecified ' + k.quantityFieldSpecified);
                    System.Debug('*** k.sKUCodeField ' + k.sKUCodeField);
                    System.Debug('*** k.sKUErrorDescriptionField ' + k.sKUErrorDescriptionField);
                    System.Debug('*** k.sKUErrorField ' + k.sKUErrorField);
                    System.Debug('*** k.salesClassField ' + k.salesClassField);
                    System.Debug('*** k.uOMField ' + k.uOMField);

                    if (k.sKUErrorField != '0') {
                        errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', k.sKUErrorField + ': ' + k.sKUErrorDescriptionField,
                                                                    'sKUCode: ' + k.sKUCodeField +
                                                                    ', sKUUOM: ' + k.uOMField +
                                                                    ', shipTo: ' + s.shipToField +
                                                                    ', user: ' + request.userIDField,
                                                                    String.valueOf(request)));
                        continue;
                    }

                    if (k.pricesField == null || k.pricesField.PriceTypeResp == null) {
                        errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', 'No Price returned.',
                                                                    'sKUCode: ' + k.sKUCodeField +
                                                                    ', sKUUOM: ' + k.uOMField +
                                                                    ', shipTo: ' + s.shipToField +
                                                                    ', user: ' + request.userIDField,
                                                                    String.valueOf(request)));
                        continue;
                    }

                    for (CPQ_ProxyMultiPriceService.PriceTypeResp p : k.pricesField.PriceTypeResp) {
                        System.Debug('*** p.PropertyChanged ' + p.PropertyChanged);
                        System.Debug('*** p.contractDescriptionField ' + p.contractDescriptionField);
                        System.Debug('*** p.contractDesignatorField ' + p.contractDesignatorField);
                        System.Debug('*** p.contractNumberField ' + p.contractNumberField);
                        System.Debug('*** p.currencyField ' + p.currencyField);
                        System.Debug('*** p.effectiveDateField ' + p.effectiveDateField);
                        System.Debug('*** p.endCustomerPriceField ' + p.endCustomerPriceField);
                        System.Debug('*** p.endCustomerPriceFieldSpecified ' + p.endCustomerPriceFieldSpecified);
                        System.Debug('*** p.expirationDateField ' + p.expirationDateField);
                        System.Debug('*** p.gPONonCommittedPriceField ' + p.gPONonCommittedPriceField);
                        System.Debug('*** p.gPONonCommittedPriceFieldSpecified ' + p.gPONonCommittedPriceFieldSpecified);
                        System.Debug('*** p.gPOTierPriceField ' + p.gPOTierPriceField);
                        System.Debug('*** p.gPOTierPriceFieldSpecified ' + p.gPOTierPriceFieldSpecified);
                        System.Debug('*** p.gPOTierPriceRootContractField ' + p.gPOTierPriceRootContractField);
                        System.Debug('*** p.markupField ' + p.markupField);
                        System.Debug('*** p.markupFieldSpecified ' + p.markupFieldSpecified);
                        System.Debug('*** p.pricContCommitmentTypeDescField ' + p.pricContCommitmentTypeDescField);
                        System.Debug('*** p.pricContRootContractField ' + p.pricContRootContractField);
                        System.Debug('*** p.priceErrorDescriptionField ' + p.priceErrorDescriptionField);
                        System.Debug('*** p.priceErrorField ' + p.priceErrorField);
                        System.Debug('*** p.quantityRangeField ' + p.quantityRangeField);
                        System.Debug('*** p.sequenceNumberField ' + p.sequenceNumberField);
                        System.Debug('*** p.sequenceNumberFieldSpecified ' + p.sequenceNumberFieldSpecified);
                        System.Debug('*** p.unitPriceField ' + p.unitPriceField);

                        ProductPrice prod = productSkuMap.get(k.sKUCodeField);
                        prod.unitPrice = p.unitPriceField.setScale(2);
                        prod.quantityRange = p.quantityRangeField;
                        prod.contractNumber = p.contractNumberField;
                        prod.description = p.contractDescriptionField;
                        prod.directIndirect = p.contractDesignatorField;
                        prod.effectiveDate = p.effectiveDateField;
                        prod.expirationDate = p.expirationDateField;
                        prod.qtyUnitPrice = prod.unitPrice / prod.qty;
                        prod.qtyUnitPrice = prod.qtyUnitPrice.setScale(2);

                    }
                }
            }
        } catch (Exception e) {
            errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PricingProcesses.InvokeCustomerSpecificPricingWS', 'Error', e.getMessage(),
                                                            'sKUCodes: ' + productSkuMap +
                                                            ', shipTo: ' + Account.Account_External_ID__c, String.valueOf(request)));
            System.debug(e.getMessage());
        }
    }


    public class ProductPrice {

        public Id productId {get;set;}
        public String productName {get;set;}
        public String productCode {get;set;}
        public String uom {get;set;}
        public Decimal unitPrice {get;set;}
        public String quantityRange {get;set;}
        public String contractNumber {get;set;}
        public String description {get;set;}
        public String directIndirect {get;set;}
        public Datetime effectiveDate {get;set;}
        public Datetime expirationDate {get;set;}
        public Decimal qty {get;set;}
        public Decimal qtyUnitPrice {get;set;}
        public Decimal listPrice {get;set;}

        public ProductPrice(Id productId, String productName, String productCode, String uom, Decimal qty, Decimal listPrice) {
            this.productId = productId;
            this.productName = productName;
            this.productCode = productCode;
            this.uom = uom;
            this.qty = qty;
            this.listPrice = listPrice;
        }

    }

}