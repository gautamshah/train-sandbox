@isTest
private class TestUpdateOpportunityGBU_Franchise {
/*********************************************************************
*
* Updates
*
*  Date      Author               Description
* ----------------------------------------------------------------------
*
*  8-3-2012   MJM         Re-configured to used test account creation from TestUtility class
*   7/1/2014    Gautam Shah     Modified to correct errors
*********************************************************************/
    static testMethod void runTest() {
        // Create the oppty
        User u = [Select Id, Business_Unit__c, Franchise__c From User Where Business_Unit__c != null and Franchise__c != null and isActive = true and Profile.Name = 'US - RMS' Limit 1];
        //User u = [Select Id from User where Id = :UserInfo.getUserId()];
        
       // u.Business_Unit__c = 'The Unit';
       // u.Franchise__c = 'Burger King';
        
       // update u;
        
        //testUtility tu = new testUtility();
        //Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        Opportunity o;
        System.runAs(u)
        {   
            testUtility tu = new testUtility();
            Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
           // Account a = [Select Id From Account Limit 1];
            o = new Opportunity(AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!');
            insert o;
        }
        System.debug('Biz Unit: ' + o.Business_Unit__c);
        
       // System.assertEquals('Burger King', [Select Franchise__c from Opportunity where Id = :o.Id].Franchise__c);
        System.assertEquals(u.Business_Unit__c, [Select Business_Unit__c from Opportunity where Id = :o.Id].Business_Unit__c);
        
        /*
        u.Business_Unit__c = '';
        u.Franchise__c = '';
        
        update u;
        update o;
        
        //System.assertEquals(null, [Select Franchise__c from Opportunity where Id = :o.Id].Franchise__c);
        System.assertEquals(null, [Select Business_Unit__c from Opportunity where Id = :o.Id].Business_Unit__c);
        
        
            Id pId = [select Id 
                        from Profile 
                       where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id;
                       
             User u2 = new User();
             u2.LastName = 'Shmoe Test';
             u2.Business_Unit__c = 'The Unit';
             u2.Franchise__c = 'Burger King';
             u2.email = 'testShmoe@covidian.com';
             u2.alias = 'testShmo';
             u2.username = 'testShmoe@covidian.com';
             u2.communityNickName = 'testShmoe@covidian.com';
             u2.ProfileId = pId;
             u2.CurrencyIsoCode='USD'; 
             u2.EmailEncodingKey='ISO-8859-1';
             u2.TimeZoneSidKey='America/New_York';
             u2.LanguageLocaleKey='en_US';
             u2.LocaleSidKey ='en_US';
             insert u2;
             System.runAs(u2){
                Opportunity o2 = new Opportunity(AccountId = a.Id, OwnerId = u.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!');
                insert o2;
               // System.assertEquals(null, [Select Franchise__c from Opportunity where Id = :o2.Id].Franchise__c);
                System.assertEquals(null, [Select Business_Unit__c from Opportunity where Id = :o2.Id].Business_Unit__c);
             }
        
        */
    }
}