public class UserTerritory_u extends sObject_u
{
	private static UserTerritory_g cache = new UserTerritory_g(); 
	
	////
	//// fetch sets 
	////
	public static Map<object, Map<Id, UserTerritory>> fetch(sObjectField indexField, set<object> keys)
	{
		Map<object, Map<Id, UserTerritory>> existingIndex = cache.fetch(indexField, keys);
		
		if (existingIndex.size() != keys.size())
		{
			set<object> toFetch = new set<object>();
			for(object key : keys)
				if (!existingIndex.containsKey(key))
					toFetch.add(key);

			List<UserTerritory> fromDB = fetchFromDB(indexField, toFetch);
			
			cache.put(fromDB); //// UserId is already included to be indexed					

			return cache.fetch(indexField, keys);
		}
		else
			return existingIndex;
		
	}

	////
	//// fetchFromDB
	////
	@testVisible
	private static List<UserTerritory> fetchFromDB(sObjectField field, set<object> ovalues)
	{
		set<string> values = DATATYPES.castToString(ovalues);
		
		// All the fields are Ids, so convert the values to Ids
		string whereStr = ' WHERE ' + field.getDescribe().getName() + ' IN :values AND isActive = true ';

		string query =
			SOQL_select.buildQuery(
				UserTerritory.sObjectType,
				UserTerritory_c.fieldsToInclude,
				whereStr,
				null,
				null);
					
		return (List<UserTerritory>)Database.query(query);
	}
}