/**
Controller of Generate Proposal page
The page is used for proposal cloning, renewing and amending.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-01      Yuli Fintescu       Created
2015-09-11      Bryan Fry           Added record type change for hardware proposals
2016-03-01      Bryan Fry           Reset I_Accept__c flag to false on clone to avoid Scrub PO workflow rule locking record
===============================================================================
*/
public with sharing class CPQ_Controller_GenerateAgreement implements CPQ_AgreementGeneratorCallBack {
    public Apttus__APTS_Agreement__c theRecord {get; set;}
    public String SourceType {get; set;}
    public String SourceTitle {get; set;}
    public String SourcePurpose {get; set;}

    public String inputAgreementId {get; set;}
    private Apttus__APTS_Agreement__c inputAgreement;
    public String pageMode {get; set;}
    public Boolean hasErrors {get;set;}

    private Map<String,Id> mapAgreementRType;

    private CPQ_AgreementGenerator generator;

    public CPQ_Controller_GenerateAgreement(ApexPages.StandardController controller) {
        theRecord = (Apttus__APTS_Agreement__c)controller.getRecord();

        pageMode = ApexPages.currentPage().getParameters().get('pageMode');

        inputAgreementId = theRecord.ID;
		inputAgreement = theRecord;
		SourcePurpose = 'Clone';
        SourceType = 'Agreement';
        SourceTitle = inputAgreement.AgreementId__c + ' - ' + inputAgreement.Name;

        generator = new CPQ_AgreementGenerator();
        generator.addCallBackListener(this);

        //gather record types from agreement objects
        mapAgreementRType = New Map<String,Id>();
        Map<Id, RecordType> rts = RecordType_u.fetch(Apttus__APTS_Agreement__c.class);
        for(RecordType R: rts.values()) {
            mapAgreementRType.Put(R.DeveloperName, R.Id);
        }
    }

    public Boolean getDisableButton() {
        return ApexPages.hasMessages();
    }

    public PageReference doCancel() {
		return new PageReference('/' + inputAgreementId);
    }

    public PageReference doGenerate() {
        try {
            if (!String.isEmpty(inputAgreementId)) {
                generator.doGenerate(inputAgreementId, inputAgreement.Apttus__Related_Opportunity__c, CPQ_AgreementGenerator.SOURCETYPE.AGREEMENT);
            }
            theRecord = generator.getOutputAgreement();
            hasErrors = false;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            hasErrors = true;
        }
        return null;
    }

    public void BeforeGenerate(Apttus__APTS_Agreement__c sourceAgreement,
        Apttus__APTS_Agreement__c outputAgreement,
        Apttus_Config2__ProductConfiguration__c outputConfig,
        List<Apttus_Config2__LineItem__c> outputLineItems) {
        //change recordtype from locked to unlocked
        //=========================================
        string CPRTId =  mapAgreementRType.get(CPQ_Utilities.getRecordTypeDeveloperName(sourceAgreement));
        if (CPRTId != Null) {
            outputAgreement.RecordTypeId = CPRTId;
        }

        outputAgreement.Apttus__Status__c = 'Request';
        outputAgreement.Apttus__Status_Category__c = 'Request';
        outputAgreement.Apttus_Approval__Approval_Status__c = 'Not Submitted';
        outputAgreement.I_Accept__c = false;
    }
}