public with sharing class dmGroups
{
	public static Map<Id, dmGroup__c> All { get; set; }
	public static Map<Id, dmGroup__Share> Shares { get; set; }
	public static Map<Id, Group> sObjects { get; set; }
	public static Map<string, Group> GUID2sObject { get; set; }
	public static Map<Id, dmGroup__c> Administrator { get; set; }
	public static Map<Id, List<dmGroup__c>> ApplicationId2Groups { get; set; }
 
	static
	{
    	string query = dm.buildSOQL(dmGroup__c.getSObjectType(), null, null);
    	system.debug(query);
    	
    	All = new Map<Id, dmGroup__c>((List<dmGroup__c>)Database.query(query));
    	
    	Administrator = new Map<Id, dmGroup__c>();
		ApplicationId2Groups = new Map<Id, List<dmGroup__c>>();
		for (dmGroup__c g : All.values())
			if (g.isAdministrator__c)
			{
    			Administrator.put(g.Id, g);

				if (!ApplicationId2Groups.containsKey(g.Application__c))
					ApplicationId2Groups.put(g.Application__c, new List<dmGroup__c>());
			
				ApplicationId2Groups.get(g.Application__c).add(g);
			}
			
    	query = dm.buildSOQL(dmGroup__Share.getSObjectType(), null, null);
    	Shares = new Map<Id, dmGroup__Share>((List<dmGroup__Share>)Database.query(query));

    	query = dm.buildSOQL(Group.getSObjectType(), null, null);
    	sObjects = new Map<Id, Group>((List<Group>)Database.query(query));
    	
    	GUID2sObject = new Map<string, Group>();
    	for (Group g : sObjects.values())
    		GUID2sObject.put(g.DeveloperName, g);
	}    
}