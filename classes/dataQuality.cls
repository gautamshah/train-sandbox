public class dataQuality {
    /****************************************************************************************
    * Name    : dataQuality Class
    * Author  : Mike Melcher
    * Date    : 03-16-2012
    * Purpose : Commonly used code for Data Quality
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR      CHANGE
    * ----        ------      ------
    *
    * 05-16-2012   MJM        Updated createCase to determine Queue for case based on the Region from User record
    * 06-14-2012   MJM        Add additional regional queues for Case owners
    * 06-22-2012   MJM        Added Description for "New User Submitted Healthcare facility" Cases
    * 06-26-2012   MJM        Added method to replicate values from one Contact to another
    * 06-27-2012   MJM        Added variable and method for tracking/setting when a trigger-driven Contact replication
    *                         is in progress.
    * 07-24-2012   MJM        Added Website, Personal Email, Other Phone, and Email to Contact Replication
    * 09-27-2012   MJB        Commented out "Email" field.  Removed comment out from "Personal Email" field
    * 10-11-2012   MJB        Commented out "Phone" field.
    * 14-11-2012   BSH        Add Asia for case assignment.
    * 20-11-2012   BSH        Add fields "KOL_Status__c" and "KOL_Type__c" into contact replication logic.
    * 02-12-2012   BSH        Add EMEA Emerging Market
    * 01/28/2013   BSH        Add field "S2_KOL__c", "RMS_KOL__c", "VT_KOL__c" and "MS_KOL__c"
    * 03/25/2013   MHS        Added JAPAN for case assignment.
    * 04/04/2013   BSH        Add field "Agreed_on_PIPA__c"
    * 04/08/2013   BSH        Add Korea for case assignment
    * 06/18/2013   JLR        Alteration to the queues for Singapore & Korea Case 187809 
    * 06/23/2013   BSH        Add Hong Kong for case assignment   
    * 07/08/2013   BSH        Add China for case assignment
    * 08/05/2013   GCC        Modified the ASIA Data Quality queue names
    * 08/28/2013   Gautam Shah      Modified the class to use a singleton rtMap to avoid needless queries. 
                                    Added methods "convertRecordTypeForDQProcessing" and "evalForContactReplication" to decouple criteria from processing in Contact triggers "ContactValidation" and "UpdateMasterWhenConnectedContactChanges", respectively.
    * 18/Dec/2013  Tejas Kardile    Modified the class to Case-00281435. 
    * 01/18/2014    Gautam Shah     Overloaded "createCase" and "getRecordTypes" methods to accomodate sObject-specific recordtypes; moved actual case creation code into "buildCase" method; added "rtMapType" member; (lines 44, 60-66, 74-145, 161-177)
    * 3/31/14       Gautam Shah     Modified "getRecordTypes" to use Utilities class (line 161)
    * 4/29/14       Jason Rayner    Added code to cater for Taiwan Data Quality cases
    * 6/10/14       Jason Rayner    Added code to cater for LATAM Data Quality cases
    * 2/10/15       Lakhan          Set bIsClosedCase to true for Indonesia to the close the case created(line 145).
    * 4/24/15       Lakhan         Added code to cater for Malaysia Data Quality cases
    * 9/9/2015     Amogh       Modified Region ASIA to APAC-ASIA for countries ID,KR,MY,SG & Region ASIA to GC for TW, HK and CN countries     
    * 9/14/2014    Amogh       Modified line 162 to 165 for ASIA - Thailand - Data Quality cases
    * 10/20/2015   Namita      Modified for Asia - India - Data Quality cases
    * 11/9/2015    Namita      Modified for Asia - Philippines - Data Quality cases
    * 15 March 2016 Amogh    Added createCaseForKR method
    * 8/31/2016     Bryan Fry       Added static record type, region, and country to help cut down on SOQL needed from test code that calls this
    *****************************************************************************************/
   
    private Map<String,Id> rtMap = new Map<String,Id>();
    private String rtMapType = null;

    private static Map<String,String> acctRTMap = null;
    private static String region = null;
    private static String country = null;
    private static Id userSubmittedHealthcareFacilityRT = null;

    public static Map<String,String> getAccountRTMap() {
        if (acctRTMap == null) {
            acctRTMap = new Map<String,String>();
            List<RecordType> accountRTList = new List<RecordType>([Select Id, DeveloperName From RecordType Where SobjectType = 'Account']);
            for(RecordType rt : accountRTList) {
                acctRTMap.put(rt.DeveloperName,rt.Id);
            }
        }
        return acctRTMap;
    }

    public static String getRegion() {
      if (region == null) {
          region = [select Region__c from User where Id = :userInfo.getUserId()].Region__c;
      }
      return region;
    }

    public static String getCountry() {
      if (country == null) {
          country = [select Country from User where Id = :userInfo.getUserId()].Country;
      }
      return country;
    }

    public static boolean ContactReplication = false;
   
    public static boolean ContactReplicationInProgress() {
        return ContactReplication;
    }
   
    public static void setContactReplication() {
        ContactReplication = true;
    }
   
    public static void clearContactReplication() {
        ContactReplication = false;
    }
    
    public void createCase(string subject, id refId, string category, string priority, id RequestedBy, string Type, string Status, String RecordTypeSobjectType, String RecordTypeName) 
    {
        Case c = buildCase(subject, refId, category, priority, RequestedBy, Type, Status);
        rtMap = getRecordTypes(RecordTypeSobjectType);
        c.RecordTypeId = rtMap.get(RecordTypeName);
        System.debug('Case RecordTypeId: ' + c.RecordTypeId);
        insert c;
    }
    
    public void createCaseForKR(string subject, id refId, string category, string priority, id RequestedBy, string Type, string Status, String RecordTypeSobjectType, String RecordTypeName, id AccountName, String AccName) 
    {
        Case c = buildCase(subject, refId, category, priority, RequestedBy, Type, Status);
        rtMap = getRecordTypes(RecordTypeSobjectType);
        c.RecordTypeId = rtMap.get(RecordTypeName);
        c.AccountId = AccountName;
        c.TEMP_Account_Name__c = AccName;
        c.TEMP_Account_Record_Type__c = 'ASIA-Healthcare Facility';
        System.debug('Case RecordTypeId: ' + c.RecordTypeId);
        insert c;
    }
    
    public void createEMSCase(string subject,id refId,string category,string priority,id RequestedBy,string Type,string Status,String ContactCountry)
    {
    
        Case c = buildEMSCase(subject, refId, category, priority, RequestedBy, Type, Status, ContactCountry);
        insert c;
    }
    
    public void createCase(string subject, id refId, string category, string priority, id RequestedBy, string Type, string Status) 
    {
        Case c = buildCase(subject, refId, category, priority, RequestedBy, Type, Status);
        insert c;
    }
    
    private Case buildEMSCase(string subject, id refId, string category, string priority, id RequestedBy, string Type, string Status,String ContactCountry) 
    {   
        System.debug('=====>'+ContactCountry);
        Case c = new Case();
        c.Subject = subject;
        if (subject.startsWith('New User Submitted Healthcare')) {
            c.Description = 'Verify that this User Submitted Healthcare Facility record is not a duplicate or ERP Record';
            c.AccountId = refId;
        }
        string host = URL.getSalesforceBaseUrl().toExternalForm() + '/' + refId;
        c.Reference__c = host;
        if (priority != null) {
            c.Priority = priority;
        } 
        if (RequestedBy != null) {
            c.Requested_By__c = RequestedBy;
            c.Requested_By_Email__c = [select Email from User where id = :RequestedBy].Email;
            System.debug('[DataQuality.buildCase] Requested by: ' + requestedby + ' email: ' + c.requested_by_email__c);
        }
        if (Type != null) {
            c.Type = Type;
        }
        if (Status != null) {
            c.Status = Status;
        }
        c.Case_Category__c = category;
        id owner;
        boolean bIsClosedCase = false;
        boolean setdataqualityrecordtype = false;
        // Call static methods to make sure they're populated from the database
        getRegion();
        getCountry();
        if (region == null) {
            owner = [select QueueId from QueueSObject where Queue.Name = 'COE - Support'].QueueId;
        } else if (region == 'US') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'US - Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else if (region == 'CA') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'CA - Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else if (region == 'EU') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'EMEA Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else if (region == 'ANZ') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ANZ Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else if (region == 'APAC-ASIA' & country == 'SG' && ContactCountry =='SG') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Singapore - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        } else if (region == 'APAC-ASIA' & country == 'KR') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Korea - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        } else if (region == 'GC' & country == 'HK') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Hong Kong - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        } else if (region == 'GC' & country == 'CN') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - China - Data Quality'].QueueId;
            bIsClosedCase = true;           
        } else if (region == 'GC' & country == 'TW') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Taiwan - Data Quality'].QueueId;
            bIsClosedCase = true;
        } else if (region == 'APAC-ASIA' & country == 'MY') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Malaysia - Data Quality'].QueueId;
            bIsClosedCase = true; 
            setdataqualityrecordtype =true;          
        }  else if (region == 'APAC-ASIA' & country == 'SG' && ContactCountry!='SG') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - IS - Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        }  else if (region == 'APAC-ASIA' & country == 'ID') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Indonesia - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        } else if (region == 'APAC-ASIA' & country == 'IN') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - India - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        } else if (region == 'APAC-ASIA' & country == 'TH'){
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Thailand - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        } else if (region == 'APAC-ASIA' & country == 'PH') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Philippines - Data Quality'].QueueId;
            bIsClosedCase = true;
            setdataqualityrecordtype =true;
        }  else if (region == 'LATAM') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'LATAM - Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else if (region == 'EM') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'EMEA Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else if (region == 'JAPAN') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Japan Data Quality'].QueueId;
            setdataqualityrecordtype =true;
        } else {
            owner = [select QueueId from QueueSObject where Queue.Name = 'COE - Support'].QueueId;
           
        }
        /*Code for Case- 00281435*/
        if(bIsClosedCase)
        {
            c.Status = 'Closed-Resolved';
            c.Resolution_Desc__c = System.Label.Case_Resolution_Desc;
            bIsClosedCase = false;
        }

        c.OwnerId = owner;
        c.Admin_Priority__c = 'low';
        //c.RelatedTo = recordId;
        if(setdataqualityrecordtype ==true)
        c.recordtypeID=[SELECT Id from RecordType where name='Data Quality Case'].Id;
      
        return c;
    }
    
    private Case buildCase(string subject, id refId, string category, string priority, id RequestedBy, string Type, string Status) 
    {
        Case c = new Case();
        c.Subject = subject;
        if (subject.startsWith('New User Submitted Healthcare')) {
            c.Description = 'Verify that this User Submitted Healthcare Facility record is not a duplicate or ERP Record';
            c.AccountId = refId;
        }
        string host = URL.getSalesforceBaseUrl().toExternalForm() + '/' + refId;
        c.Reference__c = host;
        if (priority != null) {
            c.Priority = priority;
        } 
        if (RequestedBy != null) {
            c.Requested_By__c = RequestedBy;
            c.Requested_By_Email__c = [select Email from User where id = :RequestedBy].Email;
            System.debug('[DataQuality.buildCase] Requested by: ' + requestedby + ' email: ' + c.requested_by_email__c);
        }
        if (Type != null) {
            c.Type = Type;
        }
        if (Status != null) {
            c.Status = Status;
        }
        c.Case_Category__c = category;
        id owner;
        boolean bIsClosedCase = false;
        // Call static methods to make sure they're populated from the database
        getRegion();
        getCountry();
        if (region == null) {
            owner = [select QueueId from QueueSObject where Queue.Name = 'COE - Support'].QueueId;
        } else if (region == 'US') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'US - Data Quality'].QueueId;
        } else if (region == 'CA') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'CA - Data Quality'].QueueId;
        } else if (region == 'EU') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'EMEA Data Quality'].QueueId;
        } else if (region == 'ANZ') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ANZ Data Quality'].QueueId;
        } else if (region == 'APAC-ASIA' & country == 'SG') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Singapore - Data Quality'].QueueId;
            bIsClosedCase = true;
        } else if (region == 'APAC-ASIA' & subject.startsWith('Account Creation Case of Korea') && country == 'KR') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'DIS Healthcare Facility DQ - KR'].QueueId;
            bIsClosedCase = false;
        } else if (region == 'APAC-ASIA' & country == 'KR') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Korea - Data Quality'].QueueId;
            bIsClosedCase = true;
        } else if (region == 'GC' & country == 'HK') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Hong Kong - Data Quality'].QueueId;
            bIsClosedCase = true;
        } else if (region == 'GC' & country == 'CN') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - China - Data Quality'].QueueId;
            bIsClosedCase = true;           
        } else if (region == 'GC' & country == 'TW') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Taiwan - Data Quality'].QueueId;
            bIsClosedCase = true; 
         } else if (region == 'APAC-ASIA' & country == 'MY') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Asia - Malaysia - Data Quality'].QueueId;
            bIsClosedCase = true; 
        } else if (region == 'APAC-ASIA' & country == 'ID') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Indonesia - Data Quality'].QueueId;
            bIsClosedCase = true;           
        } else if (region == 'APAC-ASIA' & country == 'IN') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - India - Data Quality'].QueueId;
            bIsClosedCase = true;           
        } else if (region == 'APAC-ASIA' & country == 'PH') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'ASIA - Philippines - Data Quality'].QueueId;
            bIsClosedCase = true;           
        } else if (region == 'LATAM') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'LATAM - Data Quality'].QueueId;
        } else if (region == 'EM') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'EMEA Data Quality'].QueueId;
        } else if (region == 'JAPAN') {
            owner = [select QueueId from QueueSObject where Queue.Name = 'Japan Data Quality'].QueueId;
        } else {
            owner = [select QueueId from QueueSObject where Queue.Name = 'COE - Support'].QueueId;
        }
        /*Code for Case- 00281435*/
        if(bIsClosedCase)
        {
            c.Status = 'Closed-Resolved';
            c.Resolution_Desc__c = System.Label.Case_Resolution_Desc;
            bIsClosedCase = false;
        }

        c.OwnerId = owner;
        c.Admin_Priority__c = 'low';
        //c.RelatedTo = recordId;
      
        return c;
    }
   
    public Map<String,Id> getRecordTypes() 
    {
        /* Map recordtypes
        if(rtMap.isEmpty())
        {
            //Map<String,Id> rtMap = new Map<String,Id>(); 
            for (Recordtype rt : [select Id, Name from Recordtype]) 
            {
                rtMap.put(rt.Name,rt.Id);
            }
        }
        return rtMap;
        */
        return Utilities.recordTypeMap_Name_Id;
    }
    
    public Map<String,Id> getRecordTypes(String sObjectType) 
    {
        // Map recordtypes
        if(rtMapType == sObjectType && !rtMap.isEmpty())
        {
            return rtMap;
        }
        else
        {
            rtMapType = sObjectType; 
            for (Recordtype rt : [select Id, Name from Recordtype Where sObjectType = :sObjectType]) 
            {
                rtMap.put(rt.Name,rt.Id);
            }
            return rtMap;
        }
    }

   Public boolean ReplicateContact(Contact Source, Contact Target) {
   //
   // Update fields in Target Contact with values from Source
   // Track whether any fields were updated
   //
          boolean Updated = false;
          
          if (Source.FirstName != null) {
             if (Target.FirstName == null || Source.FirstName != Target.FirstName) {
                Target.FirstName = Source.FirstName;
                Updated = true;
             }
          }
          if (Source.LastName != null) {
             if (Target.LastName == null || Source.LastName != Target.LastName) {
                Target.LastName = Source.LastName;
                Updated = true;
             }
          }
          if (Source.Title != null) {
             if (Target.Title == null || Source.Title != Target.Title) {
                Updated = true;
                Target.Title = Source.Title;
             }
          }
          if (Source.Salutation != null) {
             if (Target.Salutation == null || Source.Salutation != Target.Salutation) {
                Updated = true;
                Target.Salutation = Source.Salutation;
             }
          }
          if (Source.Website__c != null) {
             if (Target.Website__c == null || Source.Website__c != Target.Website__c) {
                Updated = true;
                Target.Website__c = Source.Website__c;
             }
          }
          if (Source.Personal_Email__c != null) {
             if (Target.Personal_Email__c == null || Source.Personal_Email__c != Target.Personal_Email__c) {
                Updated = true;
                Target.Personal_Email__c = Source.Personal_Email__c;
             }
          }
         /**** if (Source.Email != null) {
             if (Target.Email == null || Source.Email != Target.Email) {
                Updated = true;
                Target.Email = Source.Email;
             }
          }****/
          if (Source.OtherPhone != null) {
             if (Target.OtherPhone == null || Source.OtherPhone != Target.OtherPhone) {
                Updated = true;
                Target.OtherPhone = Source.OtherPhone;
             }
          }
          if (Source.Suffix__c != null) {
              if (Target.Suffix__c == null || Source.Suffix__c != Target.Suffix__c) {
                Updated = true;
                Target.Suffix__c = Source.Suffix__c;
             }
          }
          if (Source.Contact_Photograph__c != null) {
              if (Target.Contact_Photograph__c == null || Source.Contact_Photograph__c != Target.Contact_Photograph__c) {
                Updated = true;
                Target.Contact_Photograph__c = Source.Contact_Photograph__c;
              }
          }
          if (Source.Phonetic_Pronunciation__c != null) {
              if (Target.Phonetic_Pronunciation__c == null || Source.Phonetic_Pronunciation__c != Target.Phonetic_Pronunciation__c) {
                Updated = true;
                Target.Phonetic_Pronunciation__c = Source.Phonetic_Pronunciation__c;
             }
          }
          if (Source.TranslatedName__c != null) {
              if (Target.TranslatedName__c == null || Source.TranslatedName__c != Target.TranslatedName__c) {
                Updated = true;
                Target.TranslatedName__c = Source.TranslatedName__c;
             }
          }
          if (Source.NPI_Code__c != null) {
              if (Target.NPI_Code__c == null || Source.NPI_Code__c != Target.NPI_Code__c) {
                Updated = true;
                Target.NPI_Code__c = Source.NPI_Code__c;
             }
          }
          if (Source.State_Region_License__c != null) {
              if (Target.State_Region_License__c == null || Source.State_Region_License__c != Target.State_Region_License__c) {
                Updated = true;
                Target.State_Region_License__c = Source.State_Region_License__c;
             }
          }
          if (Source.Specialty1__c != null) {
              if (Target.Specialty1__c == null || Source.Specialty1__c != Target.Specialty1__c) {
                Updated = true;
                Target.Specialty1__c = Source.Specialty1__c;
             }
          }
          if (Source.Specialty_2__c != null) {
              if (Target.Specialty_2__c == null || Source.Specialty_2__c != Target.Specialty_2__c) {
                Updated = true;
                Target.Specialty_2__c = Source.Specialty_2__c;
             }
          }
          if (Source.Specialty_3__c != null) {
              if (Target.Specialty_3__c == null || Source.Specialty_3__c != Target.Specialty_3__c) {
                Updated = true;
                Target.Specialty_3__c = Source.Specialty_3__c;
             }
            }
           if (Source.Type__c != null) {
              if (Target.Type__c == null || Source.Type__c != Target.Type__c) {
                Updated = true;
                Target.Type__c = Source.Type__c;
             }
          }
          if (Source.Gender__c != null) {
              if (Target.Gender__c == null || Source.Gender__c != Target.Gender__c) {
                Updated = true;
                Target.Gender__c = Source.Gender__c;
              }
          }
          if (Source.MobilePhone != null) {
              if (Target.MobilePhone == null || Source.MobilePhone != Target.MobilePhone) {
                Updated = true;
                Target.MobilePhone = Source.MobilePhone;
              }
          }
          /*if (Source.Phone != null) {
              if (Target.Phone == null || Source.Phone != Target.Phone) {
                Updated = true;
                Target.Phone = Source.Phone;
             }
          }*/
          if (Source.LinkedIn_Profile__c != null) {
              if (Target.LinkedIn_Profile__c == null || Source.LinkedIn_Profile__c != Target.LinkedIn_Profile__c) {
                Updated = true;
                Target.LinkedIn_Profile__c = Source.LinkedIn_Profile__c;
              }
          }
          if (Source.Preferred_Contact_Method__c != null) {
              if (Target.Preferred_Contact_Method__c == null || Source.Preferred_Contact_Method__c != Target.Preferred_Contact_Method__c) {
                Updated = true;
                Target.Preferred_Contact_Method__c = Source.Preferred_Contact_Method__c;
              }
          }
          if (Source.Contact_Character_Profile__c != null) {
              if (Target.Contact_Character_Profile__c == null || Source.Contact_Character_Profile__c != Target.Contact_Character_Profile__c) {
                Updated = true;
                Target.Contact_Character_Profile__c = Source.Contact_Character_Profile__c;
              }
          }
          if (Source.Contact_Segment__c != null) {
              if (Target.Contact_Segment__c == null || Source.Contact_Segment__c != Target.Contact_Segment__c) {
                Updated = true;
                Target.Contact_Segment__c = Source.Contact_Segment__c;
              }
          }
          if (Source.Contact_Motivations__c != null) {
              if (Target.Contact_Motivations__c == null || Source.Contact_Motivations__c != Target.Contact_Motivations__c) {
                Updated = true;
                Target.Contact_Motivations__c = Source.Contact_Motivations__c;
              }
          }
          if (Source.MaritalStatus__c != null) {
              if (Target.MaritalStatus__c == null || Source.MaritalStatus__c != Target.MaritalStatus__c) {
                Updated = true;
                Target.MaritalStatus__c = Source.MaritalStatus__c;
              }
          }
          if (Source.PreferredLanguage__c != null) {
              if (Target.PreferredLanguage__c == null || Source.PreferredLanguage__c != Target.PreferredLanguage__c) {
                Updated = true;
                Target.PreferredLanguage__c = Source.PreferredLanguage__c;
              }
          }
          if (Source.LanguagesKnown__c != null) {
              if (Target.LanguagesKnown__c == null || Source.LanguagesKnown__c != Target.LanguagesKnown__c) {
                Updated = true;
                Target.LanguagesKnown__c = Source.LanguagesKnown__c;
              }
          }
          if (Source.Hobbies__c != null) {
              if (Target.Hobbies__c == null || Source.Hobbies__c != Target.Hobbies__c) {
                Updated = true;
                Target.Hobbies__c = Source.Hobbies__c;
              }
          }
          if (Source.Birthdate != null) {
              if (Target.Birthdate == null || Source.Birthdate != Target.Birthdate) {
                Updated = true;
                Target.Birthdate = Source.Birthdate;
              }
          }
          if (Source.KOL_Status__c != null) {
              if (Target.KOL_Status__c == null || Source.KOL_Status__c != Target.KOL_Status__c) {
                Updated = true;
                Target.KOL_Status__c = Source.KOL_Status__c;
              }
          }
          if (Source.Request_EbD_KOL__c != null) {
              if (Target.Request_EbD_KOL__c == null || Source.Request_EbD_KOL__c != Target.Request_EbD_KOL__c) {
                Updated = true;
                Target.Request_EbD_KOL__c = Source.Request_EbD_KOL__c;
              }
          }
          if (Source.Request_EMID_KOL__c != null) {
              if (Target.Request_EMID_KOL__c == null || Source.Request_EMID_KOL__c != Target.Request_EMID_KOL__c) {
                Updated = true;
                Target.Request_EMID_KOL__c = Source.Request_EMID_KOL__c;
              }
          }
          if (Source.Request_RMS_KOL__c != null) {
              if (Target.Request_RMS_KOL__c == null || Source.Request_RMS_KOL__c != Target.Request_RMS_KOL__c) {
                Updated = true;
                Target.Request_RMS_KOL__c = Source.Request_RMS_KOL__c;
              }
          }
          if (Source.Request_STI_KOL__c != null) {
              if (Target.Request_STI_KOL__c == null || Source.Request_STI_KOL__c != Target.Request_STI_KOL__c) {
                Updated = true;
                Target.Request_STI_KOL__c = Source.Request_STI_KOL__c;
              }
          }
          if (Source.Request_VT_KOL__c != null) {
              if (Target.Request_VT_KOL__c == null || Source.Request_VT_KOL__c != Target.Request_VT_KOL__c) {
                Updated = true;
                Target.Request_VT_KOL__c = Source.Request_VT_KOL__c;
              }
          }
          if (Source.KOL_Type__c != null) {
              if (Target.KOL_Type__c == null || Source.KOL_Type__c != Target.KOL_Type__c) {
                Updated = true;
                Target.KOL_Type__c = Source.KOL_Type__c;
              }
          }
          if (Source.KOL_Type__c == null) {
              if (Target.KOL_Type__c != null) {
                Updated = true;
                Target.KOL_Type__c = null;
              }
          }
          if (Source.KOL_Procedures__c != null) {
              if (Target.KOL_Procedures__c == null || Source.KOL_Procedures__c != Target.KOL_Procedures__c) {
                Updated = true;
                Target.KOL_Procedures__c = Source.KOL_Procedures__c;
              }
          }
          if (Source.KOL_Procedures__c == null) {
              if (Target.KOL_Procedures__c != null) {
                Updated = true;
                Target.KOL_Procedures__c = null;
              }
          }
          if (Source.Agreed_on_PIPA__c != null) {
              if (Target.Agreed_on_PIPA__c == null || Source.Agreed_on_PIPA__c != Target.Agreed_on_PIPA__c) {
                Updated = true;
                Target.Agreed_on_PIPA__c = Source.Agreed_on_PIPA__c;
              }
          }
          
          return Updated;
    }
    public boolean convertRecordTypeForDQProcessing(Id recordTypeID)
    {
        Map<String,Id> rtmap = getRecordTypes(); 
        Set<Id> recordTypesToConvert = new Set<Id>
        {
            rtMap.get('Connected Clinician'), 
            rtMap.get('Connected Non Clinician Contact'), 
            rtMap.get('Connected Contact ANZ'), 
            rtMap.get('Connected Contact Asia'), 
            rtMap.get('Connected Contact CA'), 
            rtMap.get('Connected Contact EM'), 
            rtMap.get('Connected Contact EU'), 
            rtMap.get('Connected Contact Japan'), 
            rtMap.get('Connected Contact LATAM'),
            rtMap.get('Connected Contact US'),
            rtMap.get('Distributor Contact')
        };
        if(recordTypesToConvert.contains(recordTypeID))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public boolean evalForContactReplication(String recordTypeID)
    {
        Map<String,Id> rtmap = getRecordTypes(); 
        Set<Id> recordTypesToEval = new Set<Id>
        {
            rtMap.get('In Process Connected Contact'),
            rtMap.get('Connected Clinician'), 
            rtMap.get('Connected Non Clinician Contact'), 
            rtMap.get('Connected Contact ANZ'), 
            rtMap.get('Connected Contact Asia'), 
            rtMap.get('Connected Contact CA'), 
            rtMap.get('Connected Contact EM'), 
            rtMap.get('Connected Contact EU'), 
            rtMap.get('Connected Contact Japan'), 
            rtMap.get('Connected Contact LATAM'), 
            rtMap.get('Connected Contact US')
        };
        if(recordTypesToEval.contains(recordTypeID))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}