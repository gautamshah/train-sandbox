@isTest
public class cpqRebate_TestSetup
{
    public static Rebate__c generateRebate (String recordTypeName, Apttus_Proposal__Proposal__c proposal)
    {
        RecordType rt = RecordType_u.fetch(Rebate__c.class,recordTypeName);
        return new Rebate__c(
            RecordTypeId = rt.Id,
            Related_Proposal__c = proposal.Id
        );
    }

    
}