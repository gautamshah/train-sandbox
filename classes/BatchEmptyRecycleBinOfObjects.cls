global class BatchEmptyRecycleBinOfObjects implements Database.Batchable<sObject>{
/****************************************************************************************
    * Name    : BatchEmptyRecycleBinOfObjects
    * Author  : Brajmohan Sharma
    * Date    : 17-Feb-2017
    * Purpose : Performs a deletion job to empty the recycle bin of the given object.
    * 
    * Dependancies: Need to send object name while calling the class
    *       
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/ 
     global final String Obname;
     global BatchEmptyRecycleBinOfObjects (String obj){

     Obname=obj;
      
    }

     global Database.QueryLocator start(Database.BatchableContext BC){
          String query = 'select id,name from '+Obname+' where isDeleted = true All Rows';
          return Database.getQueryLocator(query);

     }

     global void execute(Database.BatchableContext BC,List<sObject> scope){
         if(scope.size()>0)
         DataBase.emptyRecycleBin(scope);
     }
     
     global void finish(Database.BatchableContext BC)
     {      //Send an email to the User after your batch completes  
            //List<StoreIdToDelete__c> lstOfIds = [SELECT Id from StoreIdToDelete__c];
            //Delete lstOfIds;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
            //String[] toAddresses = new String[] {'ramya.purohit@cognizant.com','brajmohan.sharma@cognizant.com'};  
            String[] toAddresses = new String[] {'jason.rayner@medtronic.com'}; 
            mail.setToAddresses(toAddresses);  
            mail.setSubject('Deletion job is done in Production');  
            mail.setPlainTextBody('It has successfully deleted all '+Obname+' records from Recycle Bin.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }
   
}