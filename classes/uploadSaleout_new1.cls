public class uploadSaleout_new1
{    
    public transient String blobvalue {get; set;}
    public transient String fileType {get; set;}
    public Boolean hideObj {get; set;}
    public Boolean bconnectedcall {get; set;}
    List<Channel_Inventory__c> ciupload; 
    public Attachment attachment 
    {
        get 
        {
            if (attachment == null)
            attachment = new Attachment();
            return attachment;
        }
        set;
    }   
    
    public transient string nameFile{get;set;}
    public transient string fileText{get;set;}    
    public Id salesUploadId{get;set;}
    public String selectObjType{get;set;}
    public String cyclePeriodId{get;set;}
    String[] filelines = new String[]{};
    List<Sales_Out__c> salesoutupload;
    SalesOut_uploads__c upl;
    Integer currentLine;
    Integer noColumns;
    String thousandSep;
    String decimalSep;
    
    private final Cycle_Period__c sfcp;
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public uploadSaleout_new1(ApexPages.StandardController stdController) 
    {       
        this.sfcp=[Select Id,Distributor_Name__c,Channel_Inventory_Validated__c,Sales_Out_Validated__c,
                        Status__c,No_Sales_This_Month__c,Cycle_Locked__c, Cycle_Period_Reference__c  
                    From Cycle_Period__c where Id=:stdController.getId()];
                    
        selectObjType=ApexPages.currentPage().getParameters().get('objName');
        
        Decimal value = 1000.10;
        String formattedValue = value.format();
        thousandSep = formattedValue.substring(1,2);
        decimalSep = formattedValue.substring(5,6);
        
        if(thousandSep =='0')
        	thousandSep = '';
        
        cyclePeriodId = sfcp.Id;
        blobvalue = null;
        fileType = null;
        hideObj = false;
        bconnectedcall = false;        
    }
    
    public PageReference Upload_Attachment() 
    {
        if(selectObjType==''||selectObjType==null)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_select_Salesout_or_Channel_Inventory_for_Upload);
            ApexPages.addMessage(errormsg);
            attachment = new Attachment(); 
        }
        else if(((selectObjType=='Sales Out')&&sfcp.no_sales_this_month__c!=false))
        {
            sfcp.No_Sales_This_Month__c = False;
            update sfcp;
        }
        else if(sfcp.cycle_locked__C != False)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, label.Cycle_Period_is_Locked);
            ApexPages.addMessage(errormsg);
            attachment = new Attachment(); 
        }
        else if(attachment.Name != null || attachment.body != null)
        { 
            fileType = attachment.Name.substring(attachment.Name.lastIndexOf('.')+1,attachment.Name.length());
            hideObj = true;
            if(fileType == 'xls' || fileType == 'xlsx' )
            {
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = cyclePeriodId; // the record the file is attached to
                attachment.IsPrivate = true;
                nameFile = attachment.Name;               
                
                try 
                {
                    insert attachment;
                    blobvalue = EncodingUtil.base64Encode(attachment.body);                     
                    attachment.body = null;
                } 
                catch (DMLException e) 
                {
                    hideObj = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later));
                    return null;
                }
                finally 
                {
                    attachment = new Attachment(); 
                }            
            }
            else if(fileType == 'txt' || fileType == 'csv')
            {
                fileText = attachment.body.tostring();            
                nameFile = attachment.name;
                attachment.body = null;
                ReadFile();                
            }
            else
            {   
                hideObj = false;             
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, label.File_type_to_upload);
                ApexPages.addMessage(errormsg);  
                attachment = new Attachment();     
            }
        }
        else if(attachment.Name == null || attachment.body == null)
        {
            hideObj = false;
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, label.Please_choose_a_valid_file);
            ApexPages.addMessage(errormsg); 
            attachment = new Attachment();      
        }
        return null;        
    }
    
    public Pagereference ReadFile()
    {
        //declare a array of Upload records
        // get The Distributor Id
        currentLine = 0;  
        String left = '';
        String middle = '';
        String right = '';
        // Break the content by CRLF
        if(nameFile.contains('.xlsx') || nameFile.contains('.xls'))
        {
            Integer pivot1 = fileText.indexOf( '"\r', Integer.valueOf(Math.floor(fileText.length() / 3)) );
            Integer pivot2 = fileText.indexOf( '"\r', Integer.valueOf(Math.floor(fileText.length() * 2 / 3)) );
            left = fileText.substring(0,pivot1);
            middle = fileText.substring(pivot1,pivot2);
            right = fileText.substring(pivot2);            
            
            filelines = left.split('"\r');
            filelines.addAll(middle.split('"\r'));
            filelines.addAll(right.split('"\r'));
            noColumns = filelines[0].split('\t').size();            
        }
        else
        {            
            Integer pivot1 = fileText.indexOf( '\n', Integer.valueOf(Math.floor(fileText.length() / 3)) );
            Integer pivot2 = fileText.indexOf( '\n', Integer.valueOf(Math.floor(fileText.length() * 2 / 3)) );
            left = fileText.substring(0,pivot1);
            middle = fileText.substring(pivot1,pivot2);
            right = fileText.substring(pivot2);
            
            filelines = left.split('\n');
            filelines.addAll(middle.split('\n'));
            filelines.addAll(right.split('\n'));        
            noColumns = filelines[0].split('\t').size();
        }
        
        if(filelines.size()<2||(selectObjType=='Sales Out' && noColumns<11)||(selectObjType=='Channel Inventory' && noColumns<6))
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_check_data_in_file_for_correct_format);
            ApexPages.addMessage(errormsg);
            hideObj = false;
        }
        else
        {
            upl = new SalesOut_uploads__c();
            upl.date_uploaded__c =Date.today(); //myDate;
            upl.Upload_file_name__c = nameFile;
            upl.upload_status__C = 'Uploaded';
            upl.account__c = sfcp.Distributor_Name__c;
            upl.cycle_period__c = sfcp.Id;
            upl.Upload_Object__c=selectObjType;
            upl.Total_Number_of_Records__c = filelines.size()-1;
            insert upl;
            
            salesUploadId=upl.id;
            
            bconnectedcall = true;          
        }       
        return null;
    }
    
    public Pagereference InsertSalesout()
    {
        bconnectedcall = false;
        if(selectObjType=='Channel Inventory')
        {
        	processChannelInventoryUpload();
        	if(bconnectedcall)
        		return null;
            
            if(UserInfo.getUserType()=='Standard')
                return new PageReference('/apex/ValidateChannelInventory?Id='+cyclePeriodId+'&Validate=Y');
            else
                return new PageReference(Label.distributorapex+'ValidateChannelInventory?Id='+cyclePeriodId+'&Validate=Y');
        }
        else
        {
        	processSaleOutUpload();
        	if(bconnectedcall)
        		return null;
        		
            if(UserInfo.getUserType()=='Standard')
                return new PageReference('/apex/ValidateSalesOut?Id='+cyclePeriodId+'&Validate=Y');
            else
                return new PageReference(Label.distributorapex+'ValidateSalesOut?Id='+cyclePeriodId+'&Validate=Y');
        }
        hideObj = false;
        bconnectedcall = false;
        return null;
    } 
      
    public void processSaleOutUpload()
    {
        salesoutupload = new List<Sales_Out__c>();
        String strSpliter = '\t';
        
        Integer batchSize = 3999;
        for (Integer i=currentLine;i<filelines.size();i++)
        {            
            if(i==0)
                continue;
            String[] inputvalues = new String[]{};
           
            //split the Record by fields based on the delimiter           
            inputvalues = filelines[i].split(strSpliter); 
            
            if(inputvalues.size()<11)
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.File_format_Error_Number_of_columns_are_less_than_expected);
                //ApexPages.addMessage(errormsg);
                continue;
            }
            else
            {
            	String temp = filelines[i].replaceAll(strSpliter, '').replaceAll('"','').trim();
            	if(temp=='')
            		continue;
                Sales_Out__c so = new Sales_Out__c();
                
                so.cycle_period__c = sfcp.Id;
                so.Document_No__c=inputvalues[0].trim().replaceAll('"','');
                so.Document_Date_Text__c=inputvalues[1].trim().replaceAll('"','');
                so.Sell_From__c=inputvalues[2].trim().replaceAll('"','');
                so.Sell_To__c=inputvalues[3].trim().replaceAll('"','');
                so.Product_Code__c=inputvalues[4].trim().replaceAll('"','');
                so.UOM__c=inputvalues[5].trim().replaceAll('"','');
                so.Quantity_Text__c = inputvalues[6].trim().replaceAll('"','').replace(thousandSep,'').replace(decimalSep,'.').deleteWhitespace();
                so.Currency_Text__c=inputvalues[7].trim().replaceAll('"','');
                so.Selling_Price_Text__c=inputvalues[8].trim().replaceAll('"','').replace(thousandSep,'').replace(decimalSep,'.').deleteWhitespace();
                so.Lots_Serial_Number__c=inputvalues[9].trim().replaceAll('"','');
                so.comments__c = inputvalues[10].replaceAll('"','');                
                so.salesout_uploads__c = upl.Id;
                salesoutupload.add(so);
                batchSize--;
                if(batchSize<0)
                {
                	currentLine = i+1;
                	bconnectedcall = true;
                	break;
                }
            }
        }
        
        if(salesoutupload.size()>0)
        {
	    	try
	    	{
	    		insert salesoutupload;
	    	}
	        catch (Exception e)
	        {
	            hideObj = false;
	            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later);
	            ApexPages.addMessage(errormsg);            
	        }
        }
    }
    
    //Process Channel Inventory Upload
    public void processChannelInventoryUpload()
    {
        ciupload = new List<Channel_Inventory__c>();
        String strSpliter = '\t';
		
		Integer batchSize = 5999;
          
        //for everyline in the File 
        for (Integer i=currentLine;i<filelines.size();i++)
        {
            if(i==0)
                continue;
            String[] inputvalues = new String[]{};           
            
            //split the Record by fields based on the delimiter
            inputvalues = filelines[i].split(strSpliter); 
            
            if(inputvalues.size()<6)
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.File_format_Error_Number_of_columns_are_less_than_expected);
                //ApexPages.addMessage(errormsg);
                continue;
            }
            else
            {
            	String temp = filelines[i].replaceAll(strSpliter, '').replaceAll('"','').trim();
            	if(temp=='')
            		continue;
                Channel_Inventory__c so = new Channel_Inventory__c();
                so.cycle_period__c = sfcp.Id;                
                so.Product_Code_Text__c=inputvalues[0].trim().replaceAll('"','');
                so.Quantity_Text__c = inputvalues[1].trim().replaceAll('"','').replace(thousandSep,'').replace(decimalSep,'.').deleteWhitespace();
                so.UOM__c=inputvalues[2].trim().replaceAll('"','');
                so.Lots_Serial_Number__c=inputvalues[3].trim().replaceAll('"','');
                so.Inventory_Status__c=inputvalues[4].trim().replaceAll('"','');
                so.comments__c = inputvalues[5].replaceAll('"','');                
                so.Distributor_Name__c=upl.account__c;
                so.Upload_Statistics__c = upl.Id;
                ciupload.add(so);
                batchSize--;
                if(batchSize<0)
                {
                	currentLine = i+1;
                	bconnectedcall = true;
                	break;
                }       
            }
        }
        
        if(ciupload.size()>0)
        {
	    	try
	    	{
	    		insert ciupload;
	    	}
	        catch (Exception e)
	        {
                hideObj = false;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.An_error_has_occured_Please_check_the_file_template_or_try_again_later);            
                ApexPages.addMessage(errormsg);           
	        }
        }
    }

    public List<SelectOption> getObjList() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Sales Out','Sales Out'));
        options.add(new SelectOption('Channel Inventory','Channel Inventory'));
        return options;
    }    
}