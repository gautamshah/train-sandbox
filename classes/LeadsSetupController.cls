/***********************
Aurthor: Yap Zhen-Xiong
Email: Zhenxiong.Yap@covidien.com
Description: Controller for Leads Entry Module (Internal Facing)
***********************/

public with sharing class LeadsSetupController {

    public Lead newLead{get;set;}
    public String selectedCampaign {get;set;}
    public String ownerId {get;set;}
    public List<selectOption> campaignOptions{get;set;}
    public CampaignMember cm {get;set;}
   
    public LeadsSetupController () {
        
        campaignOptions= new List<selectOption>();
           
        //Populate Default owner
        newLead = new Lead();
        ownerId  = UserInfo.getUserId();
        newLead.OwnerId = ownerId;
        
        cm = new CampaignMember();
        cm.LeadId = newLead.id;
        
        //Populate Campaign selection
        
        Date d = System.today();
          
        List<Campaign> campaignList = [SELECT Name, ID FROM Campaign WHERE IsActive=True AND StartDate<=:d AND EndDate>=:d ];
        System.debug('Campaign Count: '+ campaignList.size());
        for (Campaign c : campaignList ){
            campaignOptions.add(new selectOption(c.id, c.Name)); 
            System.debug('Campaign Name: '+ c.Name);
        }
        
    }


    public PageReference launchLeadsCollection() {
        System.debug('Ownerid: '+newLead.OwnerId);
        ownerId = newLead.OwnerId;
        cm.CampaignId = selectedCampaign;
        
        PageReference pageRef = new PageReference('/apex/LeadsEntryForm?oid='+ownerId+'&cid='+cm.CampaignId);
        pageRef .setRedirect(true);
        return pageRef;
    }

}