/*
Test class

CPQ_Controller_ProposalDistributor_Config
CPQ_FacilityItem

CHANGE HISTORY
===============================================================================
DATE         NAME            DESC
2015-01-26   Yuli Fintescu   Created
10/31/2016   Paul Berglund   Remediated with CPQ
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
private class Test_CPQ_ControllerPropDistConfig {
    static List<Apttus_Proposal__Proposal__c> testProposals;
    static List<Apttus__APTS_Agreement__c> testSourceAgreements;
    static void createTestData()
    {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

		insert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.ENABLED);

        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

        List<Account> testAccounts = new List<Account>{
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Test Distributor', Account_External_ID__c = 'US-222222', RecordTypeID = acctRT.getRecordTypeId(), Secondary_Class_of_Trade__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Test Distributor 1', Account_External_ID__c = 'US-444444', RecordTypeID = acctRT.getRecordTypeId(), Secondary_Class_of_Trade__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Test Distributor 1', Account_External_ID__c = 'US-333333', RecordTypeID = acctRT.getRecordTypeId(), Secondary_Class_of_Trade__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Test Distributor 2', Account_External_ID__c = 'US-555555', RecordTypeID = acctRT.getRecordTypeId(), Secondary_Class_of_Trade__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Top6', Account_External_ID__c = 'US-666666', RecordTypeID = acctRT.getRecordTypeId(), Secondary_Class_of_Trade__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main'),
            new Account(Class_of_Trade__c = '003', Status__c = 'Active', Name = 'Top7', Account_External_ID__c = 'US-777777', RecordTypeID = acctRT.getRecordTypeId(), Secondary_Class_of_Trade__c = 'Distributor', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
        };
        insert testAccounts;

        List<CPQ_Top_Distributor__c> testTops = new List<CPQ_Top_Distributor__c> {
            new CPQ_Top_Distributor__c(Name = 'US-666666', Account__c = testAccounts[5].Id),
            new CPQ_Top_Distributor__c(Name = 'US-777777', Account__c = testAccounts[6].Id)
        };
        insert testTops;

        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Apttus_Proposal__Proposal__c.class).values()) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }

        testSourceAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Name = 'Test Source Agreement', Apttus__Account__c = testAccounts[0].ID, Duration__c = '7 1/2 Years'),
            new Apttus__APTS_Agreement__c(Name = 'Test Source Agreement No Facilities', Apttus__Account__c = testAccounts[0].ID, Duration__c = '8 Years'),
            new Apttus__APTS_Agreement__c(Name = 'Test Master Agreement', Apttus__Account__c = testAccounts[0].ID, Duration__c = '8 Years')
        };
        insert testSourceAgreements;

        List<Agreement_Distributor__c> testSourceAgreementFacilities = new List<Agreement_Distributor__c> {
            new Agreement_Distributor__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[1].ID),
            new Agreement_Distributor__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[2].ID),
            new Agreement_Distributor__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[3].ID),
            new Agreement_Distributor__c(Agreement__c = testSourceAgreements[0].ID, Account__c = testAccounts[4].ID),

            new Agreement_Distributor__c(Agreement__c = testSourceAgreements[2].ID, Account__c = testAccounts[2].ID)
        };
        insert testSourceAgreementFacilities;

        testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Source Proposal', Apttus_Proposal__Account__c = testAccounts[0].ID, Duration__c = '6 Years'),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Source Proposal No Facilities', Apttus_Proposal__Account__c = testAccounts[0].ID, Duration__c = '6 1/2 Years'),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', 
                                             Apttus_Proposal__Account__c = testAccounts[0].ID, 
                                             Duration__c = '7 Years', 
                                             Apttus_QPComply__MasterAgreementId__c = testSourceAgreements[2].ID, 
                                             Deal_Type__c = 'Amendment', 
                                             Legacy_External_ID__c = 'LEG-EXT-ID',
                                             RecordTypeID = mapProposalRType.get('Locally_Negotiated_Agreement_LNA'))
        };
        insert testProposals;

        List<Proposal_Distributor__c> testSourceProposalFacilities = new List<Proposal_Distributor__c> {
            new Proposal_Distributor__c(Proposal__c = testProposals[0].ID, Account__c = testAccounts[1].ID),
            new Proposal_Distributor__c(Proposal__c = testProposals[0].ID, Account__c = testAccounts[2].ID),
            new Proposal_Distributor__c(Proposal__c = testProposals[0].ID, Account__c = testAccounts[3].ID),
            new Proposal_Distributor__c(Proposal__c = testProposals[0].ID, Account__c = testAccounts[4].ID)
        };
        insert testSourceProposalFacilities;

        List<Proposal_Distributor__c> testProposalFacilities = new List<Proposal_Distributor__c> {
            new Proposal_Distributor__c(Proposal__c = testProposals[2].ID, Account__c = testAccounts[1].ID)
        };
        insert testProposalFacilities;
    }

    static testMethod void myUnitTest() {
        System.Debug('*** myUnitTest ***');
        createTestData();

        List<Apttus_Proposal__Proposal__c> props = [Select Name From Apttus_Proposal__Proposal__c Where ID in: testProposals];
        List<Apttus__APTS_Agreement__c> agms = [Select AgreementId__c From Apttus__APTS_Agreement__c Where ID in: testSourceAgreements];

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalDistributor_Config;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_ProposalDistrbutor_Config p = new CPQ_Controller_ProposalDistrbutor_Config(new ApexPages.StandardController(testProposals[2]));

            //copy from proposal. no proposal specified
            p.SearchSource = 'Proposal';
            p.doSearchByCopy();

            //no distributor
            p.SearchProposalSourceID = props[1].Name;
            p.doSearchByCopy();

            //copy from proposal
            p.SearchProposalSourceID = props[0].Name;
            p.doSearchByCopy();

            p.SelectedResultMap.clear();

            //copy from agreement. no agreement specified
            p.SearchSource = 'Agreement';
            p.doSearchByCopy();

            //no distributor
            p.SearchAgreementSourceID = agms[1].AgreementId__c;
            p.doSearchByCopy();

            //copy from agreement
            p.SearchAgreementSourceID = agms[0].AgreementId__c;
            p.doSearchByCopy();

            //search by Top distributors
            p.doSearchByTopDistributors();

            //search by CRN, no CRN specified
            p.doSearchByCRNs();

            //search by CRN
            p.SearchCRNs = '888888';
            p.doSearchByCRNs();

            //search by CRN
            p.SearchCRNs = '222222, 333333, 444444, 555555';
            p.doSearchByCRNs();

            p.SearchCRNs = 'US-222222, US-333333, US-444444, US-555555';
            p.doSearchByCRNs();

            //search by string, no string specified
            p.doSearch();

            //search by string
            p.SearchString = 'CO';
            p.doSearch();

        //test return results
            CPQ_FacilityItem f = p.SearchResults[0];
            f.doGoToAccount();

            System.Debug(p.getShowSearchResult());
            //no search result is checked
            System.Debug(p.getAddCheckedEnabled());

            //check all search results
            p.SearchAllChecked = true;
            p.doSearchCheckAll();

            //all search results are checked
            System.Debug(p.getAddCheckedEnabled());

            p.doAddChecked();

            //no selected result is checked
            System.Debug(p.getRemoveCheckedEnabled());

            //check all selected results
            p.SelectAllChecked = true;
            p.doSelectCheckAll();

            System.Debug(p.getShowSelectedResult());
            //all selected results are checked
            System.Debug(p.getRemoveCheckedEnabled());

            p.doRemoveChecked();
        Test.stopTest();
   }


    static testMethod void TestSave() {
        System.Debug('*** TestSave ***');
        createTestData();

        List<Apttus_Proposal__Proposal__c> props = [Select Name From Apttus_Proposal__Proposal__c Where ID in: testProposals];

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalDistributor_Config;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_ProposalDistrbutor_Config p = new CPQ_Controller_ProposalDistrbutor_Config(new ApexPages.StandardController(testProposals[2]));

            //copy from proposal
            p.SearchSource = 'Proposal';
            p.SearchProposalSourceID = props[0].Name;
            p.doSearchByCopy();

            //check all search results
            p.SearchAllChecked = true;
            p.doSearchCheckAll();
            p.doAddChecked();

            //remove the one originally in proposal
            p.SelectedResults[0].checked = true;
            p.doRemoveChecked();
            System.Debug('*** p.SelectedResults ' + p.SelectedResults);

            p.doSave();
        Test.stopTest();
    }
}