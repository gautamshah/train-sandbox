/****************************************************************************************
* Name    : Utilities
* Author  : Gautam Shah
* Date    : 2/11/2014
* Purpose : Static utility methods for the purpose of reusing queries and logic common to multiple triggers and classes
* 
* Dependancies: 
*              
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE              AUTHOR              CHANGE
* ----              ------              ------
* 2/24/2014         Bill Shan           Add the logic to check if the current user is sys admin, api user or portal user.
* 3/27/2014         Gautam Shah         Removed methods specific to the IDNizer to it's own class "IDNizer.cls"
* 3/30/2014         Gautam Shah         Added and populated the "profileMap" static member (lines 30-33)
* 6/2372014         Bill Shan           Added activeStandardUsers_Id_User & lstCaseTeamRole to resolve the Too many query rows issue in CaseTeamBuild trigger
* 2/5/2015          Bill Shan           Added business hours and milestones map 
* Jan 27, 2016      Gautam Shah         Added new map "profileMap_Name_Id", addl fields to User query
* June 14, 2016     Gautam Shah         Added new map "currentUserPermissionSetAssignmentMap_DeveloperName_Id" (line 95-98)
*****************************************************************************************/ 
public without sharing class Utilities {
    public static boolean executeOnce = false;
    public static boolean isSysAdminORAPIUser {get; private set;}
    public static boolean isPortalUser {get; private set;}
    public static User CurrentUser {get; private set;}
    public static String CurrentUserProfileName {get; private set;}
    public static Map<Id, String> profileMap {get; private set;}
    public static Map<String, Id> profileMap_Name_Id {get; private set;}
    public static Map<Id, String> recordTypeMap_Id_Name {get; private set;}
    public static Map<String, Id> recordTypeMap_Name_Id {get; private set;}
    public static Map<Id, String> recordTypeMap_Id_DeveloperName {get; private set;}
    public static Map<String, Id> recordTypeMap_DeveloperName_Id {get; private set;}
    public static Map<Id, User> activeStandardUsers_Id_User {get; private set;}
    public static Map<String, Id> milestoneMap_Name_Id {get; private set;}
    public static Map<String, BusinessHours> businessHourMap_Name_Id {get; private set;}
    public static List<CaseTeamRole> lstCaseTeamRole {get; private set;}
    public static Map<String, String> countryRegionMap_Country_Region {get; private set;}
    public static Map<String, String> countryRegionMap_Country_Currency {get; private set;}
    public static Map<String, OPG_RecordType_Mapping__c> opgRTMap {get; private set;}
    public static Map<String, Id> currentUserPermissionSetAssignmentMap_DeveloperName_Id {get; private set;}
        
    static
    {   
        System.debug('language: ' + UserInfo.getLanguage());
        System.debug('locale: ' + UserInfo.getLocale());
        isSysAdminORAPIUser = false;
        isPortalUser = false;
        profileMap = new Map<Id, String>();
        profileMap_Name_Id = new Map<String, Id>();
        recordTypeMap_Id_Name = new Map<Id, String>();
        recordTypeMap_Name_Id = new Map<String, Id>();
        recordTypeMap_Id_DeveloperName = new Map<Id, String>();
        recordTypeMap_DeveloperName_Id = new Map<String, Id>();
        milestoneMap_Name_Id = new Map<String, Id>(); 
        businessHourMap_Name_Id = new Map<String, BusinessHours>();
        activeStandardUsers_Id_User = new Map<Id, User>([SELECT Id from User where IsActive = TRUE and UserType='Standard']);
        lstCaseTeamRole = new List<CaseTeamRole>([Select Id, Name from CaseTeamRole where (Name = 'Sales Rep' or Name = 'Sales Manager')]);
        currentUserPermissionSetAssignmentMap_DeveloperName_Id = new Map<String, Id>();

        for(Profile p : [Select Id, Name From Profile])
        {
            profileMap.put(p.Id, p.Name);
            profileMap_Name_Id.put(p.Name, p.Id);
        }
        
        if(profileMap.size() > 0)
        {
            String currentProfileName = profileMap.get(userinfo.getProfileId());
            if(currentProfileName == 'System Administrator' || currentProfileName == 'API Data Loader')
                    isSysAdminORAPIUser = true;
        }
        
        String userType = userinfo.getUserType();
        if(userType=='PowerPartner' || userType=='CSPLitePortal' || userType=='CustomerSuccess' || userType=='PowerCustomerSuccess')
            isPortalUser = true;
        
        for (Recordtype rt : [select Id, Name, DeveloperName from Recordtype]) 
        {
            recordTypeMap_Id_Name.put(rt.Id, rt.Name);
            recordTypeMap_Name_Id.put(rt.Name, rt.Id);
            recordTypeMap_Id_DeveloperName.put(rt.Id, rt.DeveloperName);
            recordTypeMap_DeveloperName_Id.put(rt.DeveloperName, rt.Id);
        }
        
        for(MilestoneType mt : [Select Name, Id from MilestoneType])
            milestoneMap_Name_Id.put(mt.Name,mt.Id);
        for(BusinessHours bh : [Select Name, Id from BusinessHours])
            businessHourMap_Name_Id.put(bh.Name,bh);
        
        CurrentUser = [Select Id, Profile.Name, UserType, Name, Country, Region__c, DailyReportChatterGroupID__c, DailyReportManagerUserID__c, Sales_Org_PL__c, LocaleSidKey, LanguageLocaleKey From User where Id = :userinfo.getUserId() limit 1];
        CurrentUserProfileName = CurrentUser.Profile.Name;
        countryRegionMap_Country_Region = new Map<String, String>();
        countryRegionMap_Country_Currency = new Map<String, String>();
        opgRTMap = new Map<String, OPG_RecordType_Mapping__c>();
        
        for (PermissionSetAssignment psa : [Select PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId = :userinfo.getUserId()]) 
        {
            currentUserPermissionSetAssignmentMap_DeveloperName_Id.put(psa.PermissionSet.Name, psa.PermissionSetId);
        }
        
        System.debug('CurrentUserProfileName: ' + CurrentUserProfileName);
        System.debug('isSysAdminORAPIUser: ' + isSysAdminORAPIUser);
        //ProfileFetcher pf = new ProfileFetcher();
        //System.debug('pf: ProfileName: ' + pf.CurrentProfileName);
    }
    
    public static String getRegion(String country)
    {
        if(countryRegionMap_Country_Region.isEmpty())
        {
            Map<String, Region_Country_Mapping__c> mapRegionCountry = Region_Country_Mapping__c.getAll();
            for(Region_Country_Mapping__c rcm : mapRegionCountry.values())
            {
                countryRegionMap_Country_Region.put(rcm.Name, rcm.Region__c);
            }
        }
        String region;
        if(countryRegionMap_Country_Region.containsKey(country))
        {
            region = countryRegionMap_Country_Region.get(country);
        }
        else
        {
            region = null;
        }
        return region;
    }
    
    public static String getCurrency(String country)
    {
        if(countryRegionMap_Country_Currency.isEmpty())
        {
            Map<String, Region_Country_Mapping__c> mapRegionCountry = Region_Country_Mapping__c.getAll();
            for(Region_Country_Mapping__c rcm : mapRegionCountry.values())
            {
                countryRegionMap_Country_Currency.put(rcm.Name, rcm.CurrencyIsoCode__c);
            }
        }
        String currencyCode;
        if(countryRegionMap_Country_Currency.containsKey(country))
        {
            currencyCode = countryRegionMap_Country_Currency.get(country);
        }
        else
        {
            currencyCode = 'USD';
        }
        return currencyCode;
    }
    
    public static Id getOppRecordTypeIdByOPG(String OPG)
    {
        if(opgRTMap.isEmpty())
        {
            opgRTMap = OPG_RecordType_Mapping__c.getAll();
        }
        Id oppRTId;
        if(opgRTMap.containsKey(OPG))
        {
            String rtDevName = opgRTMap.get(OPG).Opp_RecordType_API_Name__c;
            System.debug('Utilities: rtDevName: ' + rtDevName);
            if(recordTypeMap_DeveloperName_Id.containsKey(rtDevName))
            {
                oppRTId = recordTypeMap_DeveloperName_Id.get(rtDevName);
            }
            else
            {
                oppRTId = null;
            }
        }
        else
        {
            oppRTId = null;
        }
        return oppRTId;
    }
}