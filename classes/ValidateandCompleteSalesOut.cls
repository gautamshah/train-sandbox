public class ValidateandCompleteSalesOut {
    
    public String hasError='F';
    public String ErrorAlert{get;set;}
    public String CyclePeriodChoice{get;set;}
    public String ChecktIfvalidate{get;set;}
    public String CyclePeriodId{get;set;}
    List<Cycle_Period__c> lstPendingCyclePeriod{get;set;}
    public ValidateandCompleteSalesOut(ApexPages.StandardController stdController) {
        ChecktIfvalidate=ApexPages.currentPage().getParameters().get('Validate');
        CyclePeriodId=ApexPages.currentPage().getParameters().get('Id');
        List<Cycle_Period__c> lstPendingCyclePeriodQuery=[Select Id,Name,Sales_out_submission_date__c,Cycle_Period_Reference__r.Name, (Select Id from Sales_Out__r limit 1) from Cycle_Period__c where Id=:CyclePeriodId];
        if(lstPendingCyclePeriodQuery==null||lstPendingCyclePeriodQuery.size()==0)
        {
            ErrorAlert='N';
            //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
            ApexPages.addMessage(errormsg);
        }
        else
        {
             List<Cycle_Period__c> lstPendingCyclePeriodTemp=new List<Cycle_Period__c>();
            for(Cycle_Period__c cpv:lstPendingCyclePeriodQuery){
                if(cpv.Sales_Out__r!=null&&cpv.Sales_Out__r.size()>0)
                    lstPendingCyclePeriodTemp.add(cpv);
            
            }
            if(lstPendingCyclePeriodTemp==null||lstPendingCyclePeriodTemp.size()==0)
            {
                ErrorAlert='N';
            //  ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
                ApexPages.addMessage(errormsg);
            }
            else
            {
                lstPendingCyclePeriod=lstPendingCyclePeriodTemp;
            }
            
        }
    }
    public ValidateandCompleteSalesOut() {
        ChecktIfvalidate=ApexPages.currentPage().getParameters().get('Validate');
        
        
    }
    public Pagereference validateAndComplete()
    {
        FileUploadErrors.setValidationHasRan();
        
        Set<Id> CIProcessed=new set<Id>();
        
        If(lstPendingCyclePeriod==null||lstPendingCyclePeriod.size()==0)
        {
            ErrorAlert='N';
    //      ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
            ApexPages.addMessage(errormsg);
        }
        else
        {
        List<Sales_Out__c> lstSalesOuttoValidate=[Select Id,Product_Code__c,Sell_To__c,Sell_From__c,Submitted__c from Sales_Out__c where Cycle_Period__c in :lstPendingCyclePeriod and Submitted__c = False];
        
        if(lstSalesOuttoValidate!=null&&lstSalesOuttoValidate.size()>0)
        {
            System.debug('lstPendingCyclePeriod:'+lstPendingCyclePeriod);
            
            
           // List<Sales_Out__c> lstSalesOutFailed=[Select Id,Product_Code1__r.Product_Sku_SAP_ID__c,Error_messages__c,Product_Code__c,Sell_To__c,Sell_From__c from Sales_Out__c where Cycle_Period__c in :lstPendingCyclePeriod and Validation_Message__c!='' limit 1];
            List<Sales_Out__c> lstSalesOutFailed=[Select Id,Product_Code1__r.SKUSAPID__c,Error_messages__c,Product_Code__c,Sell_To__c,Sell_From__c from Sales_Out__c where Cycle_Period__c in :lstPendingCyclePeriod and Validation_Message__c!='' limit 1];
            System.debug('lstSalesOutFailed:'+lstSalesOutFailed);
            if(lstSalesOutFailed!=null&&lstSalesOutFailed.size()>0)
            {
                ErrorAlert='T';
                //update lstSalesOuttoValidate;
            //  ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please Review Data Validation Errors on Records');
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_Review_Data_Validation_Errors_on_Records);
                ApexPages.addMessage(errormsg);
            }
            else
            {
                ErrorAlert='F';
                if(ChecktIfvalidate!='Y'&& lstSalesOuttoValidate!=null && lstSalesOuttoValidate.size()>0)
                {
                    for(Sales_Out__c so: lstSalesOuttoValidate)
                        so.Submitted__c = True;
                
                    update lstSalesOuttoValidate;
                }
                
                for(Cycle_Period__c cp:lstPendingCyclePeriod)
                {
                    //cp.Status__c='Submitted';
                    if(ChecktIfvalidate!='Y'&&cp.Sales_out_submission_date__c==null&&(cp.Sales_Out__r!=null&&cp.Sales_Out__r.size()>0))
                        cp.Sales_out_submission_date__c=Date.today();
                        //cp.Sales_Out_Validated__c=true;
                }
                update lstPendingCyclePeriod;
            //  ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'Data Submitted Successfuly');
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO, label.Data_Submitted_Successfully);
                ApexPages.addMessage(errormsg);
              //  update lstSalesOuttoValidate;
            }
            
        }//End of if(lstSalesOuttoValidate!=null&&lstSalesOuttoValidate.size()>0)
        else
        {
            ErrorAlert='N';
    //      ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
            ApexPages.addMessage(errormsg);
        }
        
        }
        return null;
    }
    public static void runValidations(List<Sales_Out__c> lstSalesOuttoValidate)
    {
        map<String,List<Sales_Out__c>> mapProductCode=new map<String,List<Sales_Out__c>>();
        Map<String,Id> mapSellAccounts= new Map<String,Id>();
        set<String> setSellfrom=new set<String>();
        set<String> setSellTo=new set<String>();
        set<String> skuset=new set<String>();
        set<String> accset=new set<String>();
        for(Sales_Out__c sot:lstSalesOuttoValidate)
        {
            if(mapProductCode.keyset().contains(sot.Product_Code__c))
                mapProductCode.get(sot.Product_Code__c).add(sot);
            else
            {
                List<Sales_Out__c> tmpLst=new List<Sales_Out__c>();
                tmpLst.add(sot);
                mapProductCode.put(sot.Product_Code__c,tmpLst);
            }       
            if(setSellfrom.contains(sot.Sell_From__c)==false)
                setSellfrom.add(sot.Sell_From__c);
            if(setSellTo.contains(sot.Sell_To__c)==false)
                setSellTo.add(sot.Sell_To__c);
        } 
        Map<String,Id> mapPartCode= new Map<String,Id>();
        //for(Product_SKU__c sku:[select Id,Product_Sku_SAP_ID__c from Product_SKU__c where Product_Sku_SAP_ID__c in :mapProductCode.keyset()])
        for(Product_SKU__c sku:[select Id,SKUSAPID__c from Product_SKU__c where SKUSAPID__c in :mapProductCode.keyset()])
        {
            /*if(mapPartCode.keyset().contains(sku.Product_Sku_SAP_ID__c)==false)
                mapPartCode.put(sku.Product_Sku_SAP_ID__c,sku.ID);*/
            if(mapPartCode.keyset().contains(sku.SKUSAPID__c)==false)
                mapPartCode.put(sku.SKUSAPID__c,sku.ID);
        } 
        
        for(Account acc:[select Id,Account_SAP_ID__c from Account where Account_SAP_ID__c in :setSellfrom or Account_SAP_ID__c in :setSellTo])
        {
            if(mapSellAccounts.keyset().contains(acc.Account_SAP_ID__c)==false)
                mapSellAccounts.put(acc.Account_SAP_ID__c,acc.ID);
        } 
        System.debug('map product key set:'+mapPartCode);
        skuset=mapPartCode.keyset();
        accSet=mapSellAccounts.keyset();
        for(Sales_Out__c sot:lstSalesOuttoValidate)
        {
            sot.Error_messages__c='';
            if(skuset.contains(sot.Product_Code__c)==false)
            {
        //      sot.Error_messages__c='Product Code Not Found,';
                sot.Error_messages__c=label.product_code_not_found + ',';
                //hasError='T';
            }
            else
            {
                sot.Product_Code1__c=mapPartCode.get(sot.Product_Code__c);
            }
            if(accSet.contains(sot.Sell_From__c)==false)
            {
            //  sot.Error_messages__c=sot.Error_messages__c+' Sell From Account Not Found,';
                sot.Error_messages__c=sot.Error_messages__c+ label.sell_from_account_not_found + ',';
                //hasError='T';
            }
            else
            {
                sot.Sell_From_Account__c=mapSellAccounts.get(sot.Sell_From__c);
            }
            if(accSet.contains(sot.Sell_To__c)==false)
            {
            //  sot.Error_messages__c=sot.Error_messages__c+' Sell To Account Not Found,';
                sot.Error_messages__c=sot.Error_messages__c+ label.sell_to_account_not_found + ',';
                //hasError='T';
            }
            else
            {
                sot.Sell_To_Account__c=mapSellAccounts.get(sot.Sell_To__c);
            }
            
        }
    }
    public static void runCycleValidations(List<Sales_Out__c> lstSalesOuttoValidate)
    {
        for(Sales_Out__c sot:lstSalesOuttoValidate)
        {
            if(sot.Run_Validations__c==true)
            {
                sot.Error_messages__c='';
                if(sot.Product_Code1__c==null)
                {
                    System.debug('test 1');
                    sot.Error_messages__c='Product Code Not Found,';
                    //hasError='T';
                }
                
                if((sot.Sell_From__c!=null&&sot.Sell_From__c!='')&&sot.Sell_From_Account__c==null)
                {
                //  sot.Error_messages__c=sot.Error_messages__c+' Sell From Account Not Found,';
                    sot.Error_messages__c=sot.Error_messages__c+ label.sell_from_account_not_found + ',';
                    //hasError='T';
                }
                
                if(sot.Sell_To_Account__c==null)
                {
                //  sot.Error_messages__c=sot.Error_messages__c+' Sell To Account Not Found,';
                    sot.Error_messages__c=sot.Error_messages__c+ label.sell_to_account_not_found + ',';
                    //hasError='T';
                }
            }
            //sot.Run_Validations__c=false;
        }
    
    }
    
    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            If(lstPendingCyclePeriod!=null&&lstPendingCyclePeriod.size()>0)
            {
                for(Cycle_Period__c cpForList:lstPendingCyclePeriod)
                {
                    options.add(new SelectOption(cpForList.Id,cpForList.Cycle_Period_Reference__r.Name));
                }
            }
            
           // options.add(new SelectOption('Append','Append'));
            return options;
        }

}