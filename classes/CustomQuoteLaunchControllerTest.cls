/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
private class CustomQuoteLaunchControllerTest {

	@testSetup static void createData() {
		Account acc = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
		insert acc;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acc.Id);
		insert c;

		Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.SSG;

		Apttus_Approval__ApprovalsCustomConfig__c approvalsConfig = new Apttus_Approval__ApprovalsCustomConfig__c(Name = 'Apttus__APTS_Agreement__c', Apttus_Approval__ApprovalStatusField__c = 'Apttus_Approval__Approval_Status__c', Apttus_Approval__ApprovalContextType__c = 'Single');
		insert approvalsConfig;

		//Create Quick Quote Proposal Record
		RecordType rtype = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,'Hardware_or_Product_Quote');

		//Create Quote/Proposal record
		Apttus_Proposal__Proposal__c app = new Apttus_Proposal__Proposal__c();
		app.RecordTypeId = rtype.Id;
		app.Apttus_Proposal__Account__c = acc.Id;
		app.OwnerId = UserInfo.getUserId();
		app.Apttus_Proposal__Proposal_Name__c = 'Deal_Proposal';
		app.Primary_Contact2__c = c.Id;
		app.Apttus_Proposal__Approval_Stage__c='In Review';
		app.Apttus_QPApprov__Approval_Status__c='Approved';
		Insert app;
	}

	// @isTest static void test_doLaunchConfigureProducts() {
	// 	System.debug('DML Usage: ' + Limits.getDMLStatements());
	// 	Apttus_Proposal__Proposal__c app = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
	// 	CustomQuoteLaunchController ctrl = new CustomQuoteLaunchController(new ApexPages.StandardController(app));
	// 	PageReference pgref = ctrl.doLaunchConfigureProducts();
	// }

	@isTest static void test_doLaunchPreview() {
		Apttus_Proposal__Proposal__c app = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
		CustomQuoteLaunchController ctrl = new CustomQuoteLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchPreview();
	}

	@isTest static void test_doLaunchGenerate() {
		Apttus_Proposal__Proposal__c app = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
		CustomQuoteLaunchController ctrl = new CustomQuoteLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchGenerate();
	}

	@isTest static void test_doLaunchPresent() {
		Apttus_Proposal__Proposal__c app = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
		CustomQuoteLaunchController ctrl = new CustomQuoteLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchPresent();
	}

	@isTest static void test_doLaunchAccept() {
		Apttus_Proposal__Proposal__c app = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
		CustomQuoteLaunchController ctrl = new CustomQuoteLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchAccept();
	}

	@isTest static void test_doLaunchDeny() {
		Apttus_Proposal__Proposal__c app = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
		CustomQuoteLaunchController ctrl = new CustomQuoteLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchDeny();
	}

}