public with sharing class JP_OpportunityLineItemBusiness{
     /****************************************************************************************
     * Name    : JP_OpportunityLineItemBusiness
     * Author  : Hiroko Kambayashi
     * Date    : 09/18/2012
     * Purpose : Business logic to insert OpportunityLineItemSchedule record by Opportunity
     *                       and to update Opportunity record by JP_Target_Product_Category__c
     *
     * Dependencies: Opportunity Object
     *             , OpportunityLineItem Object
     *             , OpportunityLineItemSchedule Object
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 10/03/2012  Hiroko Kambayashi    Adding the method "inputTargetProductCategoryToOpportunity"
     * 11/09/2012  Hiroko Kambayashi    Adding the mtehod "inputOriginalTotalPrice"
     * 11/09/2015  Syu    Iken          Correcting the mtehod "divideProfitScheduleByOpportunity"
     * 02/23/2016  Hidetoshi Kawada     Correcting the method "divideProfitScheduleByOpportunity"
     *****************************************************************************************/

    /*
     * Set the value of UnitPrice * Quantity before insert RevenueSchedule to Field JP_OriginalTotalPrice__c
     *
     * @param  List<OpportunityLineItem> oppLineItemList
     * @return none
     */
    public void inputOriginalTotalPrice(List<OpportunityLineItem> oppLineItemList){
        for(OpportunityLineItem lineItem : oppLineItemList){
            //Case: OpportunityLineItem is inserted(UnitPrice != null)
            if(lineItem.UnitPrice != null){
                lineItem.JP_OriginalTotalPrice__c = lineItem.UnitPrice * lineItem.Quantity;
            }
        }
    }

    /*
     * Revenue auto OpportunityLineItemSchedule records by Opportunity info
     * @param  Map<Id, OpportunityLineItem> oppLineItemMap
     * @return none
     */
    public void divideProfitScheduleByOpportunity(Map<Id, OpportunityLineItem> oppLineItemMap){
        //The Map for new OpportunityLineItem
        Map<Id, OpportunityLineItem> targetMap = new Map<Id, OpportunityLineItem>();
        for(OpportunityLineItem litem : [SELECT Id,
                                                JP_RevenuePerMonth__c,
                                                Opportunity.JP_RemainMonth__c,
                                                //2015.11.09 ecs i-shu add start
                                                QuantityPerMonth__c,
                                                JP_Product_CanUseRevenueSchedule__c,//CanUseRevenueSchedule
                                                Product_CanUseQuantitySchedule__c,//CanUseQuantitySchedule
                                                //2015.11.09 ecs i-shu add end
                                                //2016.02.23 ecs h-kawada add start
                                                Opportunity.JP_Logic_Type__c,
                                                TotalPrice,
                                                Quantity,
                                                //2016.02.23 ecs h-kawada add end
                                                Opportunity.JP_Sales_start_Date__c
                                            FROM OpportunityLineItem
                                            WHERE Id IN : oppLineItemMap.keyset()]){
            targetMap.put(litem.Id, litem);
        }
        //Get OpportunityLineItem Id which is inserted Schedule by Default settings in Product2
        Set<Id> oppLiIds = new Set<Id>();
        for(OpportunityLineItemSchedule sch : [SELECT Id,
                                                        OpportunityLineItemId
                                                    FROM OpportunityLineItemSchedule
                                                    WHERE OpportunityLineItemId IN : targetMap.keyset()]){
            oppLiIds.add(sch.OpportunityLineItemId);
        }
        //Remove OpportunityLineItemId having OpportunityLineItemSchedule from targetmap
        if(oppLiIds.size() > 0){
            for(String sid : oppLiIds){
                targetMap.remove(sid);
            }
        }

        //OpportunityLineItemSchedule List for insert
        List<OpportunityLineItemSchedule> insOppLiList = new List<OpportunityLineItemSchedule>();
        for(String lid : targetMap.keyset()){
            Integer repeatCount = targetMap.get(lid).Opportunity.JP_RemainMonth__c.intValue();
            Decimal revenuePerMonth = targetMap.get(lid).JP_RevenuePerMonth__c;
            Date baseDate = targetMap.get(lid).Opportunity.JP_Sales_start_Date__c;
            //2015.11.09 ecs i-shu add start
            /**CVJ_SELLOUT_DEV-44 of fixing**/
            String JP_Product_CanUseRevenueScheduleFlag = targetMap.get(lid).JP_Product_CanUseRevenueSchedule__c;
            Boolean Product_CanUseQuantityScheduleFlag = targetMap.get(lid).Product_CanUseQuantitySchedule__c;
            Decimal quantityPerMonth = targetMap.get(lid).QuantityPerMonth__c;
            //2016.02.23 ecs h-kawada mod start
            /**CVJ_SELLOUT_DEV-97 of fixing**/
            Decimal revenue = targetMap.get(lid).TotalPrice;
            Decimal quantity = targetMap.get(lid).Quantity;
            Decimal logicType = targetMap.get(lid).Opportunity.JP_Logic_Type__c;
            Integer roopCnt = 0;
            if(logicType == 1){
                roopCnt = 1;
            }
            else if(logicType == 0){
                roopCnt = 12;
            }
            //for(Integer i = 0; i < 12; i++){
            for(Integer i = 0; i < roopCnt; i++){
                if(JP_Product_CanUseRevenueScheduleFlag == 'TRUE' && Product_CanUseQuantityScheduleFlag == TRUE){
                    OpportunityLineItemSchedule oppSch = new OpportunityLineItemSchedule();
                    oppSch.OpportunityLineItemId = lid;
                    if(logicType > 0){
                        oppSch.Revenue = revenue;
                        oppSch.Quantity = quantity;
                    }
                    else {
                        oppSch.Revenue = revenuePerMonth;
                        oppSch.Quantity = quantityPerMonth;
                    }
                    oppSch.ScheduleDate = baseDate.addMonths(i);
                    oppSch.Type = 'both';

                    insOppLiList.add(oppSch);
                }

                if(JP_Product_CanUseRevenueScheduleFlag == 'TRUE' && Product_CanUseQuantityScheduleFlag == FALSE){
                    OpportunityLineItemSchedule oppSch = new OpportunityLineItemSchedule();
                    oppSch.OpportunityLineItemId = lid;
                    if(logicType > 0){
                        oppSch.Revenue = revenue;
                    }
                    else {
                        oppSch.Revenue = revenuePerMonth;
                    }
                    oppSch.ScheduleDate = baseDate.addMonths(i);
                    oppSch.Type = 'Revenue';

                    insOppLiList.add(oppSch);
                }

                if(Product_CanUseQuantityScheduleFlag == TRUE && JP_Product_CanUseRevenueScheduleFlag == 'FALSE'){
                    OpportunityLineItemSchedule oppSch = new OpportunityLineItemSchedule();
                    oppSch.OpportunityLineItemId = lid;
                    if(logicType > 0){
                        oppSch.Quantity = quantity;
                    }
                    else {
                        oppSch.Quantity = quantityPerMonth;
                    }
                    oppSch.ScheduleDate = baseDate.addMonths(i);
                    oppSch.Type = 'Quantity';

                    insOppLiList.add(oppSch);
                }
            }
            //2016.02.23 ecs h-kawada mod End

            //2015.11.09 ecs i-shu mod start
            /*
            for(Integer i = 0; i < repeatCount; i++){
                OpportunityLineItemSchedule oppSch = new OpportunityLineItemSchedule();
                oppSch.OpportunityLineItemId = lid;
                oppSch.Revenue = revenuePerMonth;
                oppSch.ScheduleDate = baseDate.addMonths(i);
                oppSch.Type = 'Revenue';

                insOppLiList.add(oppSch);
            }*/
            //2015.11.09 ecs i-shu mod end
            //2015.11.09 ecs i-shu add end
        }
        //insert OpportunityLineItemSchedule records
        if(!insOppLiList.isEmpty()){
            insert insOppLiList;
        }
    }

    /*
     * Set Product2.JP_Target_Product_Category__c to Field JP_Target_Product_Category__c of Opportunity
     * and update Opportunity record when OpportunityLineItem is inserted.
     * @param  Map<Id, OpportunityLineItem> oppLineItemMap
     * @return none
     *
    public void inputTargetProductCategoryToOpportunity(Map<Id, OpportunityLineItem> oppLineItemMap){
        //Opportunity List for update
        List<Opportunity> updOppList = new List<Opportunity>();
        for(OpportunityLineItem lineItem : oppLineItemMap.values()){
            Opportunity opp = new Opportunity(Id = lineItem.OpportunityId,
                                            JP_Target_Product_Category__c= lineItem.JP_Product_TargetProductCategory__c);
            updOppList.add(opp);
        }
        //Update Opportunity
        if(!updOppList.isEmpty()){
            update updOppList;
        }
    }*/
}