/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date		Id   Initials   Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD	A	PAB			CPR-000		Updated comments section
							AV-000
20161103	A	IL			AV-001		Title of the Jira – Some changes needed to be made to support Medical Supplies

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/    
public class cpqPriceListItem_c extends sObject_c
{ 
    private static cpqPriceListItem_g cache = new cpqPriceListItem_g();

	public static set<string> fieldsToInclude =
        new set<string>
        {
            'Id',
            'Apttus_Config2__ProductId__c',
            'Apttus_Config2__ChargeType__c',
            'Apttus_Config2__PriceListId__c',
            'Apttus_Config2__RollupPriceToBundle__c',
            'Apttus_Config2__PriceType__c',
            'Apttus_Config2__PriceMethod__c',
            'Apttus_Config2__AllowManualAdjustment__c',
            'Apttus_Config2__AllocateGroupAdjustment__c',
            'Apttus_Config2__Active__c'
        };


	////
	//// Legacy methods
	////
	public static void copy(
		Product2 source,
        Apttus_Config2__PriceListItem__c target,
        Map<Schema.SObjectField, Schema.SObjectField> fields)
  	{
		sObject_c.copy(source, target, fields);
  	}
  
  	public static Id dummyId(integer value)
  	{
    	return sObject_c.dummyId(Apttus_Config2__PriceListItem__c.class, value);
  	}
  
  	public static Apttus_Config2__PriceListItem__c create(
    	Id priceListId,
    	Id productId)
  	{
    	Apttus_Config2__PriceListItem__c pli = new Apttus_Config2__PriceListItem__c();
    
    	pli.Apttus_Config2__ProductId__c = productId;
    	pli.Apttus_Config2__PriceListId__c = priceListId;
    	pli.Apttus_Config2__RollupPriceToBundle__c = true;
    	pli.Apttus_Config2__PriceType__c = 'One Time';
    	pli.Apttus_Config2__PriceMethod__c = 'Per Unit';
    	pli.Apttus_Config2__AllowManualAdjustment__c = true;
    	pli.Apttus_Config2__AllocateGroupAdjustment__c = true;
    	pli.Apttus_Config2__Active__c = true;
    
	    return pli;
  	}
}