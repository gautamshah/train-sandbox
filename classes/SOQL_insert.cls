public class SOQL_insert extends SOQL implements Queueable 
{
    protected override integer numberOfRecords()
    {
        return 50000;
    }
    
    public SOQL_insert(List<sObject> sobjs)
    {
        super(sobjs); 
    }

    protected override List<SOQL_Result> executeDML(List<sObject> sObjects, boolean allOrNone)
    {
        return SOQL_Result.convert(Database.insert(sObjects, allOrNone));
    }
}