public with sharing class CreateOpportunityExtension {
    /****************************************************************************************
    * Name    : CreateOpportunityExtension
    * Author  : Mike Melcher
    * Date    : 12-8-2011
    * Purpose : Creates an Opportunity from a Product Reference Card
    *          
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 1-5-2012    PeteR                Comment/Uncomment out EbD_Target_Sales_Amount__c, SD_Target_Sales_Amount__c, 
    *                                    VT_Target_Sales_Amount__c to change field type to formula (net zero change)
    * 1-30-2012   MJM                  Update to create Opportunity_Contact_Role (custom object) record
    *                                  instead of the standard Contact Role
    * 4-26-2012   MJM                  Updated to use Total_Estimated_Revenue fields for Opportunity Amount.
    * 5-2-2012    MJM                  Updated to use a calculated product price instead of the standard unit price.    
    *
    ******************************************************************************************/
    private final Product_Preference_Card__c prc;
    

   
    public CreateOpportunityExtension(ApexPages.StandardController stdController) {
        this.prc = (Product_Preference_Card__c)stdController.getRecord();
    }
   
    
    public pagereference createOpportunity() {
         Product_Preference_Card__c currentPRC = [select id, name, Physician__c, Procedure__c, Affiliated_Account_f__c, SD_Target_Sales_Amount__c,
                                                         Procedures_Year_SD_f__c, Procedures_Year_VT_f__c, Procedures_Year_EbD_f__c,
                                                         RMS_Target_Sales_Amount__c , EbD_Target_Sales_Amount__c, VT_Target_Sales_Amount__c,
                                                         Total_Estimated_Revenue_SD__c, Total_Estimated_Revenue_EbD__c, Total_Estimated_Revenue_VT__c,
                                                         EbD_Opportunity_Product_Group__c, SD_Opportunity_Product_Group__c, VT_Opportunity_Product_Group__c,
                                                         Total_Estimated_Revenue_RMS__c
                                                                             
                                                  from Product_Preference_Card__c
                                                  where id = :prc.Id];
                                                  
                  
         Decimal TotalProceduresEbD = 0;
         Decimal TotalProceduresSD = 0;
         Decimal TotalProceduresVT = 0;
         if (currentPRC.Procedures_Year_SD_f__c != null) {
            TotalProceduresSD += currentPRC.Procedures_Year_SD_f__c;
         }
          if (currentPRC.Procedures_Year_VT_f__c != null) {
            TotalProceduresVT += currentPRC.Procedures_Year_VT_f__c;
         }
          if (currentPRC.Procedures_Year_EbD_f__c != null) {
            TotalProceduresEbD += currentPRC.Procedures_Year_EbD_f__c;
         }
         
         set<ID> prodIDs = new Set<ID>();
         Map<Id,decimal> prodPerMap = new Map<Id,decimal>();
         Map<Id,decimal> prodPriceMap = new Map<Id,decimal>();
         Map<Id,decimal> ProdPriceCalcMap = new Map<Id,decimal>();
                     
         for (Prod_Preference_Card_Product__c ppp :[select target_covidien_product__c, Calculated_Covidien_Spend__c, 
                                                           Number_of_Products_per_procedure_target__c, Unit_Price_Target__c
                                                    from Prod_Preference_Card_Product__c 
                             where Product_Preference_Card__c = :currentPRC.Id]) {
              prodIDs.add(ppp.target_covidien_product__c);
              prodPerMap.put(ppp.target_covidien_product__c,ppp.Number_of_Products_per_procedure_target__c);
              prodPriceMap.put(ppp.target_covidien_product__c,ppp.Unit_Price_Target__c);
              decimal calcprice = 0;
              if (ppp.Unit_Price_Target__c != null) {
                 calcprice = ppp.Unit_Price_Target__c;
              }
              if (ppp.Calculated_Covidien_Spend__c != null) {
                 calcprice = calcprice - ppp.Calculated_Covidien_Spend__c;
              }
                 
              prodPriceCalcMap.put(ppp.target_covidien_product__c,calcprice);
         }
         boolean Capital = true;
         boolean Disposable = true;
         List<Product2> Products = new List<Product2>();
         
         if (prodIDs.size() > 0) {
            Products = [select id, Name, Product_Type__c from Product2 where Id in :prodIds];
            System.Debug('Products on card: ' + Products);
            if (Products.size() == 0) {
               Capital = false;
               Disposable = false;
            }
            for (product2 p : Products) {
            
               if (p.Product_Type__c == null) {
                  Capital = false;
                  Disposable = false;
               } else {
                  if (p.Product_Type__c == 'Capital') {
                     Disposable = false;
                  }
                  if (p.Product_Type__c == 'Disposable') {
                     Capital = false;
                  }
               }
            }
         }
         
         id userid = UserInfo.getUserId();
         
         string bu = [select id, business_unit__c from User where id = :userId].business_unit__c;
         System.debug('User Business Unit: ' + bu);                                                
         Contact c;
         if (currentPRC.Physician__c != null) {
            c = [select id, LastName, FirstName, AccountId from Contact where Id = :currentPRC.Physician__c]; 
         }
         
         Opportunity newOpp = new Opportunity();
         if (c != null && c.AccountId != null) {
            newOpp.AccountId = c.AccountId;
         }
                  
         date closedate = date.Today();
         closedate = closedate.addMonths(3);
         
         newOpp.CloseDate = closedate;
         
              
         // Get the proper total based on the users Business Unit
         newOpp.Amount = 0;
         string PricebookName = 'none';
         
         if (bu != null) { // User has no Business Unit
             if (bu == 'EbD') {
                newOpp.Amount = currentPRC.Total_Estimated_Revenue_EbD__c;
                PricebookName = currentPRC.EbD_Opportunity_Product_Group__c;
             }
         
             if (bu == 'RMS') {
                newOpp.Amount = currentPRC.Total_Estimated_Revenue_RMS__c;
             }
         
             if (bu == 'SD') {
                newOpp.Amount = currentPRC.Total_Estimated_Revenue_SD__c;
                PricebookName = currentPRC.SD_Opportunity_Product_Group__c;
             }
         
             if (bu == 'VT') {
                newOpp.Amount = currentPRC.Total_Estimated_Revenue_VT__c;
                PricebookName = currentPRC.VT_Opportunity_Product_Group__c;
             }
             
          }
          System.debug('Pricebook name: ' + PricebookName);
          string oppName =  ':';
         
         newOpp.StageName = 'Identify';
         if (c != null) {
                              
            if (c.FirstName != null) {
               oppName = c.FirstName;
            }
            oppName += ' ' + c.LastName;

            if (currentPRC.Procedure__c != null) {
              oppName += ':' + currentPRC.Procedure__c;
            }
            //oppName += ':' + currentPRC.Affiliated_Account_f__c; 
         }
        
         newOpp.Name = oppName;
 /*        if (Capital) {
            newOpp.Capital_Disposable__c = 'Capital';
         }
         if (Disposable) {
            newOpp.Capital_Disposable__c = 'Disposable';
         }
*/
         newOpp.Probability = 0;
         newOpp.Related_Product_Preference_Card__c = currentPRC.Id;
         
         List<PricebookEntry> PBEs = new List<PricebookEntry>();
         
         if (Products.size() > 0) {
            Map<String,Id> pbMap = new Map<String,Id>();
            for (Pricebook2 p : [select Id,Name from Pricebook2 where IsActive=true]) {
               pbMap.put(p.Name,p.Id);
               System.debug('Building pbMap: ' + p.Name + ' ' + p.Id);
            }
            System.debug('Pricebook: ' + pbMap.get(PricebookName));
            if (pbMap.get(PricebookName) != null) {
               newOpp.Pricebook2Id = pbMap.get(PricebookName);
               
               PBEs = [select Id, Pricebook2id, Product2Id from PriceBookEntry 
                       where Product2Id in :ProdIds
                         and Pricebook2Id = :newOpp.Pricebook2Id];
               if (PBEs.size() > 0) {
                  newOpp.Pricebook2Id = PBEs[0].Pricebook2Id;
                  System.debug('PricebookEntries: ' + PBEs);
               }
            }
         }
         
         insert newOpp;
         
         System.debug('Opportunity created, Physician: ' + currentPRC.Physician__c);
         if (currentPRC.Physician__c != null) {
            Opportunity_Contact_Role__c ocr = new Opportunity_Contact_Role__c();
            ocr.Opportunity_Contact__c = currentPRC.Physician__c;
            ocr.Opportunity_del__c = newOpp.Id;
            ocr.Contact_Role__c = 'Decision Maker';
            insert ocr;
            System.debug('Opportunity Contact Role Created: ' + ocr.Id);
         }

          // Insert Opportunity Products if any
         
         for (PricebookEntry pbe :PBEs) {
            OpportunityLineItem oli = new OpportunityLineItem();
            oli.OpportunityId = newOpp.Id;
            oli.PriceBookEntryId = pbe.Id;
            oli.Quantity = 0;
            if (prodPerMap.get(pbe.Product2Id) != null) {
               if (bu != null) {
                  if (bu == 'SD') {
                     oli.Quantity = TotalProceduresSD * prodPerMap.get(pbe.Product2Id);
                  }
                  if (bu == 'VT') {
                     oli.Quantity = TotalProceduresVT * prodPerMap.get(pbe.Product2Id);
                  }
                  if (bu == 'EbD') {
                     oli.Quantity = TotalProceduresEbD * prodPerMap.get(pbe.Product2Id);
                  }
               }
            }
            // Quantity cannot be zero -- set to 1 if none of the fields that go 
            // into calculating the quantity are filled in
            if (oli.Quantity == 0) {
                oli.Quantity = 1;
            }
            //oli.TotalPrice = 0;
            System.debug('Before setting unit price: ' + oli.UnitPrice);
            oli.UnitPrice = 0;
            System.debug('Before setting list price: ' + oli.ListPrice);
            
            if (prodPriceCalcMap.get(pbe.Product2Id) != null) {
               oli.UnitPrice = prodPriceCalcMap.get(pbe.Product2Id);
               
               System.debug('Creating line item, unit price: ');
               System.debug('Creating line item, list price: ' + oli.ListPrice);

            }
            //oli.TotalPrice = oli.Quantity * oli.UnitPrice;
            System.debug('Before line item insert, unit price: ');
            System.debug('PBEs: ' + PBEs);
            System.debug('Opp pricebook: ' + newOpp.Pricebook2Id + ' PBE Pricebook: ' + pbe.Pricebook2Id);
            System.debug('Before line item insert, unit price: ' + oli.UnitPrice);
            System.debug('Before line item insert, list price: ' + oli.ListPrice);
            
            insert oli;
            
            
         }
        
                                            
         PageReference oppPage = new ApexPages.StandardController(newOpp).view();
         oppPage.setRedirect(true);
         return oppPage;
     }
}