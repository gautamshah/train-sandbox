/****************************************************************************************
* Name    : Class: OpportunityTriggerDispatcher
* Author  : Gautam Shah
* Date    : 4/5/2014
* Purpose : Dispatches to necessary classes when invoked by Opportunity trigger
* 
* Dependancies: 
*   Class: Opportunity_Main
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 15/10/15      Bill Shan               Updated the territory assignment logic
* 11/01/16      Bryan Fry               Added method to capture Cycle Times
06/14/2016   Paul Berglund      Moved when to execute Cycle Time logic into class
20161111	A	PAB		N/A		Removed ProductReqForUSMedSupplies for per David M
*****************************************************************************************/
public with sharing class OpportunityTriggerDispatcher {
    
    //to prevent a method from executing a second time add the name of the method to the 'executedMethods' set
    public static Set<String> executedMethods = new Set<String>();
    
    public static void Main(List<Opportunity> newList, Map<Id, Opportunity> newMap, List<Opportunity> oldList, Map<Id, Opportunity> oldMap, Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isExecuting)
    {
        if(isUpdate && isAfter)
        {
            //re-establish OpportunityLineItemSchedule if the Opportunity's Revenue Start Date (Close Date) has changed.
            List<Opportunity> reSchedOpps = new List<Opportunity>();
            for(Id oid : newMap.keySet())
            {
                //only process Surgical Opps
                if(Utilities.recordTypeMap_Id_DeveloperName.get(newMap.get(oid).RecordTypeId) == 'US_AST_Opportunity' || Utilities.recordTypeMap_Id_DeveloperName.get(newMap.get(oid).RecordTypeId) == 'US_SUS_Opportunity')
                {
                    if(newMap.get(oid).CloseDate != oldMap.get(oid).CloseDate)
                    {
                        reSchedOpps.add(newMap.get(oid));
                    }
                }
            }
            if(reSchedOpps.size() > 0)
            {
                if(!executedMethods.contains('reEstablishSchedule'))
                    Opportunity_Main.reEstablishSchedule(reSchedOpps);
            }
            
        }
        if(isBefore && (isInsert || isUpdate))
        {
            if(!executedMethods.contains('PopulateOCREmail'))
                Opportunity_Main.PopulateOCREmail(newList);
                
            if(!executedMethods.contains('OwnerRegionAssignment'))
                Opportunity_Main.OwnerRegionAssignment(newList);
             
            // 20161111-A      
            /////if(Utilities.CurrentUserProfileName == 'US - Med Supplies')
            /////{
            /////    if(!executedMethods.contains('ProductReqForUSMedSupplies'))
            /////        Opportunity_Main.ProductReqForUSMedSupplies(newList);
            /////}
            
            if(!executedMethods.contains('UpdateOpportunityGBU_Franchise'))
                Opportunity_Main.UpdateOpportunityGBU_Franchise(newList);
            
            //if(!executedMethods.contains('AssignTerritory'))
            //{
                List<Opportunity> lsOppsToUpdateTerr = new List<Opportunity>();
                
                if(isUpdate)
                {
                    for(Opportunity o : newList)
                    {
                        if(o.OwnerId != oldMap.get(o.Id).OwnerId)
                            lsOppsToUpdateTerr.add(o);
                    }
                    
                    if(lsOppsToUpdateTerr.size()>0)
                        Opportunity_Main.AssignTerritory(lsOppsToUpdateTerr);
                }
                else
                {
                    //Opportunity_Main.AssignTerritory(newList);
                    
                    for(Opportunity o : newList)
                    {
                        o.DoNotByPassTerritorySelection__c = true;
                    }
                }
            //}
        }
    }
}