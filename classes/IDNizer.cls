/****************************************************************************************
* Name    : IDNizer
* Author  : Gautam Shah
* Date    : 2/11/2014
* Purpose : Methods used for the creation of IDN records for Health Systems.  Sell-To Accounts are discovered via the parent field of ERP records.  These Sell-Tos are then associated to IDNs through the creation of Connected Account records (Account_Affiliation__c)
* 
* Dependancies: 
*              
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* 	DATE         	AUTHOR                  CHANGE
* 	----          	------                  ------
*	4/10/14			Gautam Shah				No longer using Account_Affiliation__c as many-to-many membership of Sell-Tos to IDNs.  Removed all references and logic associated with this.
*	4/11/14			Gautam Shah				Split "createIDNAccountsandMembership" into two methods to address CPU limit issue.
*	4/17/14			Gautam Shah				Made most methods not static and made the class batch friendly.
*****************************************************************************************/ 
public with sharing class IDNizer {
	
	private Map<Id, String> sellToMap {get;set;}
	private Map<String, Account> existingIDNmap {get;set;}
	private List<Temp_IDN_Mapping__c> IDNList {get;set;}
	
    public IDNizer(List<Temp_IDN_Mapping__c> IDNs){
    	sellToMap = new Map<Id, String>();
    	existingIDNmap = new Map<String, Account>();
    	this.IDNList = IDNs;
    	if(IDN_ShipTo_SellTo_Mapping())
    	{
    		if(createIDNAccounts())
    		{
    			createMembership();
    		}
    	}
    }
    
    public Boolean IDN_ShipTo_SellTo_Mapping()
	{
		//String response;
		//List<Temp_IDN_Mapping__c> IDNList = new List<Temp_IDN_Mapping__c>([Select Id, Account_Sell_To__c, ERP_ID_Ship_To__c From Temp_IDN_Mapping__c]);
		Map<String, Temp_IDN_Mapping__c> ShipMap = new Map<String, Temp_IDN_Mapping__c>();
		
		for(Temp_IDN_Mapping__c t : IDNList)
		{
			ShipMap.put(t.ERP_ID_Ship_To__c, t);
		}
		List<ERP_Account__c> ERPList = new List<ERP_Account__c>([Select ERP_ID__c, Parent_Sell_To_Account__c From ERP_Account__c Where ERP_ID__c in :ShipMap.keySet()]);
		for(ERP_Account__c e : ERPList)
		{
			Temp_IDN_Mapping__c t = ShipMap.get(e.ERP_ID__c);
			t.Account_Sell_To__c = e.Parent_Sell_To_Account__c;
		}
		try
		{
			System.debug('Attempting ShipTo - SellTo Mapping');
			update ShipMap.values();
			//response = 'SUCCESS';
			return true;
		}
		catch (System.DmlException e) 
		{
			String response = '';
		    for (Integer i = 0; i < e.getNumDml(); i++) 
		    {
		        response += e.getDmlMessage(i) + '; ID: ' + e.getDmlId(i) + '\n'; 
		    }
		    System.debug('IDN_ShipTo_SellTo_Mapping() ERROR: ' + response);
		    return false;
		}
		//return response;
	}
	
	public Boolean createIDNAccounts()
	{
		//String response;
		List<Temp_IDN_Mapping__c> IDNs = new List<Temp_IDN_Mapping__c>([Select Name, ERP_ID_Ship_To__c, Account_Sell_To__c, Business_Unit__c From Temp_IDN_Mapping__c Where Account_Sell_To__c != null]);
		Map<String, Temp_IDN_Mapping__c> IDNmap = new Map<String, Temp_IDN_Mapping__c>();
		//sellToMap = new Map<Id, String>();
		for(Temp_IDN_Mapping__c t : IDNs)
		{
			IDNmap.put(t.Name,t);
			sellToMap.put(t.Account_Sell_To__c, t.Name);
		}
		//get existing IDNs created by this process
		RecordType IDNrt = [Select Id From RecordType Where SobjectType = 'Account' and DeveloperName = 'US_GPO' Limit 1];
		List<Account> IDNList = new List<Account>([Select Id, Name From Account Where RecordTypeId = :IDNrt.Id And Source__c = 'HS-IDN' And Name In :IDNmap.keySet()]);
		//existingIDNmap = new Map<String, Account>();
		for(Account a : IDNList)
		{
			existingIDNmap.put(a.Name, a);
		}
		for(String s : IDNmap.keySet())
		{
			Account accIDN;
			if(!existingIDNmap.keySet().contains(s))
			{
				accIDN = new Account();
				Temp_IDN_Mapping__c t = IDNmap.get(s);
				accIDN.Name = t.Name;
				accIDN.Source__c = 'HS-IDN';
				accIDN.RecordTypeId = IDNrt.Id;
				IDNList.add(accIDN);
				existingIDNmap.put(accIDN.Name, accIDN);
			}
		}
		try
		{
			System.debug('Attempting IDN Upsert');
			upsert IDNList;
			//response = 'SUCCESS';
			return true;
		}
		catch(System.DmlException e) 
		{
			String response = '';
		    for(Integer i = 0; i < e.getNumDml(); i++) 
		    {
		        response += e.getDmlMessage(i) + '; ID: ' + e.getDmlId(i) + '\n'; 
		    }
		    System.debug('createIDNAccounts() ERROR: ' + response);
		    return false;
		}
		//return response;
	}
	
	public Boolean createMembership()
	{
		//System.debug('sellToMap size: ' + sellToMap.size());
		//String response;
		List<Account> sellTos = new List<Account>([Select Id, Covidien_IDN_Relationship__c From Account Where Id in :sellToMap.keySet()]);
		for(Account a : sellTos)
		{
			a.Covidien_IDN_Relationship__c = existingIDNmap.get(sellToMap.get(a.Id)).Id;
		}
		try
		{
			update sellTos;
			//response = 'SUCCESS';
			return true;
		}
		catch(System.DmlException e) 
		{
			String response = '';
		    for(Integer i = 0; i < e.getNumDml(); i++) 
		    {
		        response += e.getDmlMessage(i) + '; ID: ' + e.getDmlId(i) + '\n'; 
		    }
		    System.debug('createMembership() ERROR: ' + response);
		    return false;
		}
		//return response;
	}
	
	public static void calculateOrphanERPs()
	{
		List<Temp_IDN_Mapping__c> allOrphans = new List<Temp_IDN_Mapping__c>([select ERP_ID_Ship_To__c From Temp_IDN_Mapping__c Where Account_Sell_To__c = '']);
		List<String> erpIDs = new List<String>();
		for(Temp_IDN_Mapping__c tim : allOrphans)
		{
			erpIDs.add(tim.ERP_ID_Ship_To__c);
		}
		List<ERP_Account__c> erpList = new List<ERP_Account__c>([select Id from ERP_Account__c Where ERP_ID__c in :erpIDs]);
		
		System.debug('Total number of orphans: ' + allOrphans.size());
		System.debug('Number of SFDC ERP records w/o a parent: ' + erpList.size());
		System.debug('Number of ERPs not found: ' + (allOrphans.size() - erpList.size()));
	}
	
	public static Boolean deleteTempIdnMappingRecords()
	{
		//String response;
		List<Temp_IDN_Mapping__c> mappingRecords = new List<Temp_IDN_Mapping__c>([Select Id From Temp_IDN_Mapping__c]);
		try
		{
			delete mappingRecords;
			//response = 'SUCCESS';
			return true;
		}
		catch(System.DmlException e)
		{
			String response = e.getMessage();
			System.debug('deleteTempIdnMappingRecords() ERROR: ' + response);
			return false;
		}
		//return response;
	}
	
	@isTest(SeeAllData=true)
	public static void runTest()
	{
		Test.startTest();
		IDNizer iz = new IDNizer([Select Id, Account_Sell_To__c, ERP_ID_Ship_To__c From Temp_IDN_Mapping__c Limit 100]);
		calculateOrphanERPs();
		deleteTempIdnMappingRecords();
		Test.stopTest();
	}
}