/****************************************************************************************
 * Name    : LicenseRedirect 
 * Author  : Gautam Shah
 * Date    : November 4, 2014 
 * Purpose : Redirects a user to the appropriate object page based on user's package license status
 * Dependencies: LicenseRedirect.page
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 *****************************************************************************************/
public with sharing class LicenseRedirect {

    public static final string NAMESPACE = 'vlc_pro';

    public LicenseRedirect(ApexPages.StandardSetController setCtl){}
    public LicenseRedirect(ApexPages.StandardController ctl) {}

    public boolean getLicensed()
    {
        return UserInfo.isCurrentUserLicensed(NAMESPACE);
    }
}