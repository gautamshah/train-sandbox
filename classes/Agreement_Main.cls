/****************************************************************************************
* Name    : Class: Agreement_Main
* Author  : Paul Berglund
* Date    : 10/13/2015
* Purpose : A wrapper for static functionality for Agreement records
* 
* Dependancies: 
*   Called by: AgreementTriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 02/20/2016    Paul Berglund           Fixed a looping problem - had added the executeMethods
*                                       value to ProposalTriggerDispatcher.
*****************************************************************************************/
public with sharing class Agreement_Main 
{
}