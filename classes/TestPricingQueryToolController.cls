@isTest
private class TestPricingQueryToolController {

    static testMethod void runTest() {
        // Create Data
        List<RecordType> rtList = [select id from RecordType where Name = 'US-Healthcare Facility'];
        Id rt = rtList[0].Id;  
       // Account a = new Account(name = 'New Account Test Name 1', BillingCountry='LaLaLand', RecordTypeId = rt);
        
     //   insert a;
        testUtility tu = new testUtility();
         
      Account a = tu.testAccount(RT,'Test Sell-to');
      a = [select Id, Account_External_Id__c, recordtypeId from Account where Id = :a.Id];
      System.debug('Created sellto account, recordtype: ' + a.recordtypeId);
      a.Account_External_Id__c = 'a;lskdjf';
      update a;
     
        ERP_Account__c l = new ERP_Account__c(Parent_Sell_To_Account__c = a.Id, External_ID__c = '1234');
        insert l;
        
        //Add SKU Products 
        Product_SKU__c ps = new Product_SKU__c(Name = '6256LF8989898'
                                             , SKU__c = '12345'
                                        //     , Country__c = 'LaLaLand'
                                             , Contract_Type__c = 'x'
                                             , GBU__c = 'x'
                                             , Group__c = 'x');
        insert ps;
        
        Kit__c k = new Kit__c();
        k.Account__c = a.Id;
        k.Complete__c = false;
        k.Kit_Quantity_Ordered__c = 7;
        k.Pricing_Date__c = system.today();
        k.Purchase_Order__c = '222';
        
       Opportunity o = new Opportunity(AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!');      
        insert o; 
        k.Opportunity__c = o.id;
        insert k;
        
        /**Set up and get locations**/
        Test.startTest();
        Test.setCurrentPage(Page.PricingQueryTool);
        ApexPages.currentPage().getParameters().put( 'account', a.id );
        PricingQueryToolController ctl = new PricingQueryToolController();
        ctl.isTest = true;//put the controller in test mode because there is a call-out
        ctl.acct = a;
        ctl.location = l;
        
//        ctl.sellToAccount.ParentId = a.Id;
        //list<ERP_Account__c> loc = ctl.getLocations();
        //System.assertEquals(1,loc.size());
        
        //cover te service stub
        pricingCovidienCom2.PriceType pt = new pricingCovidienCom2.PriceType();
        
        
        /**Test Search**/
        ctl.ProductParameter.SKU__c = '12345';
        //ctl.ProductParameter.Contract_Type__c = 'x';
        ctl.locationId = l.Id;
        ctl.ProductParameter.GBU__c = 'x';
        //ctl.ProductParameter.Group__c = 'x';
        ctl.ProductParameter.Name = '6256LF8989898';
        ctl.kitId = k.Id;
         
         //Search With SKU
        ctl.searchProducts();
       // System.assertEquals(1,ctl.pageMap.size());
       // System.assertEquals(1, ctl.ProductSearchResults.size());
        
         //Search Without SKU
        ctl.ProductParameter.SKU__c = '';
        ctl.searchProducts();
        System.assertEquals(1,ctl.pageMap.size());
        System.assertEquals(1, ctl.ProductSearchResults.size());
        
        /**Test add and remove products**/
        ctl.ProductSearchResults.get(0).checked = true; //check a product in the list
        ctl.addSelectedProducts();
        System.assertEquals(1, ctl.selectedProducts.size());
        //get the selected products and check one to remove
        ctl.selectedProducts.get(0).checked = true;
        
        /**test scrolling**/
        ctl.next();
        ctl.last();
        ctl.first();
        ctl.previous();
        
        /**test retrieving the pricing**/
        ctl.ProductSearchResults.get(0).checked = true; //check a product in the list
        ctl.addSelectedProducts(); //add a product
        System.assertEquals(1, ctl.selectedProducts.size());
        ctl.PricingRequestID = 'test123x';
        PRICING_INTEGRATION_SKU_TYPE__c p = new PRICING_INTEGRATION_SKU_TYPE__c();
        p.UOM__c = '1';
        p.SalesClass__c = '1';
        p.SKUCode__c = '12345';
        p.Name = '1';
        p.SKUErrorDescription__c= '1';
        p.PricingRequestID__c = ctl.PricingRequestID;
        
        insert p;
        
        PRICING_INTEGRATION__c pi = new PRICING_INTEGRATION__c 
        (Name = 'x'
                 , QuantityRange__c = 'x'
                 , EffectiveDate__c = system.today()
                 , ExpirationDate__c = system.today()
                 , UnitPrice__c = 1
                 , Markup__c = 1
                 , EndCustomerPrice__c = 2
                 , ContractNumber__c = '1'
                 , ContractDesignator__c = 'd'
                 , ContractDescription__c = 'f'
                 , SequenceNumber__c = 2
                 , PriceCurrency__c = 'g'
                 //, SKUCode__c = '12345'
                 , PRICING_INTEGRATION_SKU_TYPE__c = p.Id);
        insert pi;
        
        ctl.getQueryResults();
        
        //commented out by Gautam Shah May 15, 2016
        //List<PricingQueryToolController.webServiceResult> res = ctl.getPricingResults();
        //System.assertEquals(1, res.size());
        //added below line because by commenting the call to getPricingResults() this variable was not initalized 
        ctl.results = new List<PricingQueryToolController.webServiceResult>();
        
        //process the Kit quote
        for(PricingQueryToolController.webServiceResult r :ctl.results)
        {
          if(r.product != null)
            r.checked = true;//check the products for the kit
          else
            r.checked = false;
          //r.product = ps;
        }
          
        ctl.addToKitQuote();//run the kit quote.
        ctl.removeProducts();
        Test.stopTest();
        System.assertEquals(0, ctl.selectedProducts.size());
        
        
    }
}