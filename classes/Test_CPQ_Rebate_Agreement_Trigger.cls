/**
Test class

CPQ_AgreementProcesses.UpdateRebateSelected()

CPQ_Rebate_Agreement_After

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_Rebate_Agreement_Trigger {
	static List<Rebate_Agreement__c> testRebates;
	static Apttus__APTS_Agreement__c testAgreement;
	
	static void createTestData() {
		CPQ_Approval_Retrigger_Field__c testRetrigger = new CPQ_Approval_Retrigger_Field__c(SObject__c = 'Agreement', Related_SObject__c = 'Rebate Agreement', Field_API_Name__c = 'Rebate_Frequency__c');
		insert testRetrigger;

        ///////insert cpqConfigSetting_cTest.create();
        //////cpqConfigSetting_c.reload();

        testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement',  Apttus_Approval__Approval_Status__c = 'Approved');
        insert testAgreement;
        
        testRebates = new List<Rebate_Agreement__c> {
        	new Rebate_Agreement__c(Related_Agreement__c = testAgreement.ID, Rebate_Frequency__c = 'Anual'),
        	new Rebate_Agreement__c(Related_Agreement__c = testAgreement.ID, Rebate_Frequency__c = 'Anual')
        };
        insert testRebates;
	}
	
    static testMethod void myUnitTest() {
        createTestData();
        
        testAgreement.Apttus_Approval__Approval_Status__c = 'Approved';
        update testAgreement;
        
        Test.startTest();
			for (Rebate_Agreement__c r : testRebates) {
	        	r.Rebate_Threshold__c = 20;
			}
	        update testRebates;
	        
			for (Rebate_Agreement__c r : testRebates) {
	        	r.Rebate_Frequency__c = 'Quarterly';
			}
	        update testRebates;
		Test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
        createTestData();
        
        testAgreement.Apttus_Approval__Approval_Status__c = 'Approved';
        update testAgreement;

        Test.startTest();
	        Rebate_Agreement__c newRebate = new Rebate_Agreement__c(Related_Agreement__c = testAgreement.ID, Rebate_Frequency__c = 'Anual');
	        insert newRebate;
		Test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        createTestData();
        
        testAgreement.Apttus_Approval__Approval_Status__c = 'Approved';
        update testAgreement;
        
        Test.startTest();
	        delete testRebates;
	        
	        testAgreement.Apttus_Approval__Approval_Status__c = 'Approved';
	        update testAgreement;
	        
	        undelete testRebates;
        Test.stopTest();
    }

    static testMethod void TestDisabledTrigger() {
    	try {
		CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Participating_Facility_After__c = true, CPQ_Product2_After__c = true, CPQ_Product2_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_ProductConfiguration_Before__c = true, CPQ_Proposal_After__c = true, CPQ_Proposal_Before__c = true, CPQ_Proposal_Distributor_After__c = true, CPQ_Rebate_After__c = true, CPQ_Rebate_Agreement_After__c = true);
 		insert testTrigger;
		} catch (Exception e) {}
 		
    	createTestData();
    	
        Test.startTest();
			for (Rebate_Agreement__c r : testRebates) {
	        	r.Rebate_Threshold__c = 20;
			}
	        update testRebates;
        Test.stopTest();
    }
}