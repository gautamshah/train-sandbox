public class CPQ_SSG_Controller_ComparePricing {
    public static final String MISSING_PRODUCT_NAME = 'Not in Product Master';
    public static final String EACH_CODE = 'EA';
    public static final String BOX_CODE = 'BX';
    public static final Integer DECIMAL_SCALE = 2;

    public Apttus_Proposal__Proposal__c qp {get;set;}
    public CPQ_SSG_QP_Class_of_Trade__c qpCot {get;set;}
    public CPQ_SSG_Pricing_Tier__c compareTier1 {get;set;}
    public CPQ_SSG_Pricing_Tier__c compareTier2 {get;set;}
    public String compareTierId1 {get;set;}
    public String compareTierId2 {get;set;}
    public String uom {get;set;}
    public CPQ_SSG_Pricing_Tier__c qualifiedTier {get;set;}
    public CPQ_SSG_Pricing_Tier__c proposedTier {get;set;}
    public List<CPQ_SSG_Pricing_Tier__c> tiers {get;set;}
    public Map<String,CPQ_SSG_Pricing_Tier__c> tierMap {get;set;}
    public Map<Id,CPQ_SSG_Pricing_Tier__c> tierIdMap {get;set;}
    public List<SelectOption> tierOptions {get;set;}
    public List<SelectOption> uomOptions {get;set;}
    public transient Map<String,List<ProductPriceComparison>> uomComparisonListMap;
    public transient Map<String,ProductPriceComparison> productPriceComparisonsMap;
    public Decimal qualifiedAmount {get;set;}
    public Decimal compareAmount1 {get;set;}
    public Decimal compareAmount2 {get;set;}
    public String listType {get;set;}
    public String qualifiedTierName {get;set;}
    public Id qualifiedTierId {get;set;}
    public String compareTier1Name {get;set;}
    public String compareTier2Name {get;set;}
    public String proposedTierName {get;set;}
    public String attachmentId {get;set;}

    private SetController setCon;

    public CPQ_SSG_Controller_ComparePricing(ApexPages.StandardController stdController) {
        this.qp = (Apttus_Proposal__Proposal__c)stdController.getRecord();
        qp = [Select Id, Name, Committed_Stapling_Vessel_Sealing__c, Qualified_Family_Value_Percent__c, Compare_Class_of_Trade__c, Apttus_Proposal__Account__c, Apttus_Proposal__Account__r.Name from Apttus_Proposal__Proposal__c Where Id = :qp.Id];

        List<CPQ_SSG_QP_Class_of_Trade__c> qpCotList = [Select CPQ_SSG_Class_of_Trade__c, CPQ_SSG_Class_of_Trade__r.Name, Expected_Annual_Compliance__c, Expected_Annual_Customer_Spend__c, Proposed_Pricing_Tier__c, Qualified_Pricing_Tier__c, Quote_Proposal__c, Quote_Proposal__r.Name, List_Type__c, Include_Rebates__c, All_Products_Rebate__c, New_Business_Rebate__c, Growth_Rebate__c, Compare_Tier_1__c, Compare_Tier_2__c From CPQ_SSG_QP_Class_of_Trade__c Where Id = :qp.Compare_Class_of_Trade__c];

        if (!qpCotList.isEmpty()) {
            qpCot = qpCotList[0];
        } else {
            qpCot = new CPQ_SSG_QP_Class_of_Trade__c();
        }

        // Get all Pricing Tiers in the system for the current class of trade and list type
        tiers = [Select Id, Name, Description__c, Max_Dollar_Commitment__c, Max_Percent_Commitment__c, Min_Dollar_Commitment__c, Min_Percent_Commitment__c, Pricing_Tier_Type__c, Sort_Order__c From CPQ_SSG_Pricing_Tier__c Where CPQ_SSG_Class_of_Trade__c = :qpCot.CPQ_SSG_Class_of_Trade__c And Pricing_Tier_Type__c = :qpCot.List_Type__c And Is_Active__c = true Order By CPQ_SSG_Class_of_Trade__c, Sort_Order__c, Name];

        tierMap = new Map<String,CPQ_SSG_Pricing_Tier__c>();
        tierIdMap = new Map<Id,CPQ_SSG_Pricing_Tier__c>();
        tierOptions = new List<SelectOption>();
        for (CPQ_SSG_Pricing_Tier__c tier: tiers) {
            tierMap.put(tier.Name, tier);
            tierIdMap.put(tier.Id, tier);
            tierOptions.add(new SelectOption(tier.Id, tier.Description__c));
        }

        qualifiedTier = tierIdMap.get(qpCot.Qualified_Pricing_Tier__c);
        if (qualifiedTier == null && tiers != null && !tiers.isEmpty()) {
            qualifiedTier = tiers[0];
        }
        proposedTier = tierIdMap.get(qpCot.Proposed_Pricing_Tier__c);
        if (proposedTier == null && tiers != null && !tiers.isEmpty()) {
            proposedTier = tiers[0];
        }

        if (tiers != null && !tiers.isEmpty()) {
            compareTier1 = tiers[0];
            compareTier2 = tiers[0];
        }

        listType = qpCot.List_Type__c;
        qualifiedTierName = qualifiedTier.Name;
        qualifiedTierId = qualifiedTier.Id;
        compareTier1Name = compareTier1.Name;
        compareTier2Name = compareTier2.Name;
        proposedTierName = proposedTier.Name;

        uom = BOX_CODE;
        getProductPriceComparisons();
    }

    public void getProductPriceComparisons() {
        uomComparisonListMap = new Map<String,List<ProductPriceComparison>>();
        productPriceComparisonsMap = new Map<String,ProductPriceComparison>();
        setupQualifiedPrices();
        setupComparePrices1();
        setupComparePrices2();
        getQuantities();
        calculateQualifiedAmount();
        calculateCompareAmount1();
        calculateCompareAmount2();
        uomOptions = new List<SelectOption>();
        for (String key: uomComparisonListMap.keySet()) {
            uomOptions.add(new SelectOption(key, key));
        }
        this.setCon = new SetController(uomComparisonListMap.get(uom));
    }

    public void setupQualifiedPrices() {
        List<ProductPriceComparison> qualifiedPrices = getPrices(qualifiedTier.Id);
        ProductPriceComparison ppc;
        List<ProductPriceComparison> uomComparisonList;

        for (ProductPriceComparison price: qualifiedPrices) {
            if (!uomComparisonListMap.containsKey(price.uom)) {
                uomComparisonListMap.put(price.uom, new List<ProductPriceComparison>());
            }
            uomComparisonList = uomComparisonListMap.get(price.uom);

            if (!productPriceComparisonsMap.containsKey(price.productCode + '_' + price.uom)) {
                ppc = new ProductPriceComparison();
                ppc.productCode = price.productCode;
                ppc.productName = price.productName == null ? MISSING_PRODUCT_NAME : price.productName;
                ppc.uom = price.uom;
                Decimal unitQuantity = price.unitQuantity;
                if (unitQuantity != null) {
                    ppc.unitQuantity = Integer.valueOf(unitQuantity);    
                }
                
                productPriceComparisonsMap.put(ppc.productCode + '_' + ppc.uom, ppc);
                uomComparisonList.add(ppc);
            }
            ppc = productPriceComparisonsMap.get(price.productCode + '_' + price.uom);
            ppc.qualifiedPrice = price.qualifiedPrice;
            ppc.qualifiedPrice = ppc.qualifiedPrice.setScale(DECIMAL_SCALE);
        }
    }

    public void setupComparePrices1() {
        List<ProductPriceComparison> comparePrices1 = getPrices(compareTier1.Id);
        ProductPriceComparison ppc;
        List<ProductPriceComparison> uomComparisonList;

        for (ProductPriceComparison price: comparePrices1) {
            if (!uomComparisonListMap.containsKey(price.uom)) {
                uomComparisonListMap.put(price.uom, new List<ProductPriceComparison>());
            }
            uomComparisonList = uomComparisonListMap.get(price.uom);

            if (!productPriceComparisonsMap.containsKey(price.productCode + '_' + price.uom)) {
                ppc = new ProductPriceComparison();
                ppc.productCode = price.productCode;
                ppc.productName = price.productName == null ? MISSING_PRODUCT_NAME : price.productName;
                ppc.uom = price.uom;
                Decimal unitQuantity = price.unitQuantity;
                if (unitQuantity != null) {
                    ppc.unitQuantity = Integer.valueOf(unitQuantity);    
                }
                
                productPriceComparisonsMap.put(ppc.productCode + '_' + ppc.uom, ppc);
                uomComparisonList.add(ppc);
            }
            ppc = productPriceComparisonsMap.get(price.productCode + '_' + price.uom);
            ppc.comparePrice1 = price.qualifiedPrice;
            ppc.comparePrice1 = ppc.comparePrice1.setScale(DECIMAL_SCALE);
        }
    }

    public void setupComparePrices2() {
        List<ProductPriceComparison> comparePrices2 = getPrices(compareTier2.Id);
        ProductPriceComparison ppc;
        List<ProductPriceComparison> uomComparisonList;

        for (ProductPriceComparison price: comparePrices2) {
            if (!uomComparisonListMap.containsKey(price.uom)) {
                uomComparisonListMap.put(price.uom, new List<ProductPriceComparison>());
            }
            uomComparisonList = uomComparisonListMap.get(price.uom);

            if (!productPriceComparisonsMap.containsKey(price.productCode + '_' + price.uom)) {
                ppc = new ProductPriceComparison();
                ppc.productCode = price.productCode;
                ppc.productName = price.productName == null ? MISSING_PRODUCT_NAME : price.productName;
                ppc.uom = price.uom;
                Decimal unitQuantity = price.unitQuantity;
                if (unitQuantity != null) {
                    ppc.unitQuantity = Integer.valueOf(unitQuantity);    
                }
                
                productPriceComparisonsMap.put(ppc.productCode + '_' + ppc.uom, ppc);
                uomComparisonList.add(ppc);
            }
            ppc = productPriceComparisonsMap.get(price.productCode + '_' + price.uom);
            ppc.comparePrice2 = price.qualifiedPrice;
            ppc.comparePrice2 = ppc.comparePrice2.setScale(DECIMAL_SCALE);
        }
    }

    public void getQuantities() {
        AggregateResult[] quantityResults = [select sku__c, sum(period_1_eaches__c) p1sum, sum(period_2_eaches__c) p2sum, sum(period_3_eaches__c) p3sum, sum(period_4_eaches__c) p4sum, sum(period_5_eaches__c) p5sum, sum(period_6_eaches__c) p6sum, sum(period_7_eaches__c) p7sum, sum(period_8_eaches__c) p8sum, sum(period_9_eaches__c) p9sum, sum(period_10_eaches__c) p10sum, sum(period_11_eaches__c) p11sum, sum(period_12_eaches__c) p12sum from sales_history__c where customer__c = :qp.Apttus_Proposal__Account__c  group by sku__c];
        Map<String,Integer> quantityMap = new Map<String,Integer>();
        Integer quantity;
        Integer sum;
        for (AggregateResult ar : quantityResults)  {
            quantity = getSumValue(ar.get('p1sum')) + getSumValue(ar.get('p2sum')) + getSumValue(ar.get('p3sum')) + getSumValue(ar.get('p4sum')) + getSumValue(ar.get('p5sum')) + getSumValue(ar.get('p6sum')) + getSumValue(ar.get('p7sum')) + getSumValue(ar.get('p8sum')) + getSumValue(ar.get('p9sum')) + getSumValue(ar.get('p10sum')) + getSumValue(ar.get('p11sum')) + getSumValue(ar.get('p12sum'));
            quantityMap.put((String)ar.get('SKU__c'), quantity);
        }

        for (ProductPriceComparison ppc: productPriceComparisonsMap.values()) {
            quantity = quantityMap.get(ppc.productCode);

            if (ppc.uom == EACH_CODE) {
                ppc.quantity = quantity;
            } else if (ppc != null && ppc.unitQuantity != null && quantity != null) {
                ppc.quantity = (Decimal)quantity/(Decimal)ppc.unitQuantity;
            }
        }
    }

    private static Integer getSumValue(Object o) {
        Integer sum = 0;
        try {
            if (o != null) {
                sum = Integer.valueOf(o);
            }
        } catch (Exception e) {}
        return sum;
    }

    public void calculateQualifiedAmount() {
        qualifiedAmount = 0.0;
        List<ProductPriceComparison> priceComparisons;
        if (uomComparisonListMap != null) {
            priceComparisons = uomComparisonListMap.get(BOX_CODE);
            if (priceComparisons != null) {
                for (ProductPriceComparison ppc: priceComparisons) {
                    if (ppc.quantity != null && ppc.quantity != 0) {
                        qualifiedAmount += ppc.quantity * ppc.qualifiedPrice;
                    }
                }
            }
        }
        qualifiedAmount = qualifiedAmount.setScale(DECIMAL_SCALE);
    }

    public void calculateCompareAmount1() {
        compareAmount1 = 0.0;
        List<ProductPriceComparison> priceComparisons;
        if (uomComparisonListMap != null) {
            priceComparisons = uomComparisonListMap.get(BOX_CODE);
            if (priceComparisons != null) {
                for (ProductPriceComparison ppc: priceComparisons) {
                    if (ppc.quantity != null && ppc.quantity != 0) {
                        compareAmount1 += ppc.quantity * ppc.comparePrice1;
                    }
                }
            }
        }
        compareAmount1 = compareAmount1.setScale(DECIMAL_SCALE);
    }

    public void calculateCompareAmount2() {
        compareAmount2 = 0.0;
        List<ProductPriceComparison> priceComparisons;
        if (uomComparisonListMap != null) {
            priceComparisons = uomComparisonListMap.get(BOX_CODE);
            if (priceComparisons != null) {
                for (ProductPriceComparison ppc: priceComparisons) {
                    if (ppc.quantity != null && ppc.quantity != 0) {
                        compareAmount2 += ppc.quantity * ppc.comparePrice2;
                    }
                }
            }
        }
        compareAmount2 = compareAmount2.setScale(DECIMAL_SCALE);
    }

    public static List<ProductPriceComparison> getPrices(Id pricingTierId) {
        // Get Price List information
        List<CPQ_SSG_Pricing_Tier_Price_list__c> pricingTierPriceLists = [Select Id, CPQ_SSG_Pricing_Tier__c, CPQ_SSG_Price_List__c, CPQ_SSG_Price_List__r.Price_List_Code__c From CPQ_SSG_Pricing_Tier_Price_List__c Where CPQ_SSG_Pricing_Tier__c = :pricingTierId];
        List<String> priceListCodes = new List<String>();
        for (CPQ_SSG_Pricing_Tier_Price_list__c pricingTierPriceList: pricingTierPriceLists) {
            priceListCodes.add(pricingTierPriceList.CPQ_SSG_Price_List__r.Price_List_Code__c);
        }

        // Get item prices by price list
        List<Contract_Item_Price__c> itemPrices = [SELECT Conversion_Factor__c, Direct_Price__c, Effective_Date__c, Expiration_Date__c, Item__c, Pricing_Identifier__c, Selling_UOM__c FROM Contract_Item_Price__c Where Pricing_Identifier__c in :priceListCodes Order By Item__c];
        Set<String> productCodes = new Set<String>();
        Map<String,Contract_Item_Price__c> boxProductCodeMap = new Map<String,Contract_Item_Price__c>();
        Set<String> eachProductCodes = new Set<String>();
        for (Contract_Item_Price__c itemPrice: itemPrices) {
            productCodes.add(itemPrice.Item__c);
            if (itemPrice.Selling_UOM__c == BOX_CODE) {
                boxProductCodeMap.put(itemPrice.Item__c, itemPrice);
            } else if (itemPrice.Selling_UOM__c == EACH_CODE) {
                eachProductCodes.add(itemPrice.Item__c);
            }
        }

        // Get products used in the price list
        List<Product2> products = [Select Id, ProductCode, Name From Product2 Where Apttus_Surgical_Product__c = true And ProductCode in :productCodes];
        Map<String,String> productCodeToNameMap = new Map<String,String>();
        if (products != null) {
            for (Product2 product: products) {
                productCodeToNameMap.put(product.ProductCode, product.Name);
            }
        }

        // Construct QP product price objects for all prices in this price list and add them to a list to insert.
        List<ProductPriceComparison> ppcs = new List<ProductPriceComparison>();
        ProductPriceComparison ppc;
        for (Contract_Item_Price__c price: itemPrices) {
            ppc = new ProductPriceComparison();
            ppc.productCode = price.Item__c;
            ppc.productName = productCodeToNameMap.get(price.Item__c);
            ppc.uom = price.Selling_UOM__c;
            ppc.qualifiedPrice = price.Direct_Price__c;
            ppc.unitQuantity = Integer.valueOf(price.Conversion_Factor__c);
            ppcs.add(ppc);

            if (boxProductCodeMap.containsKey(price.Item__c) && !eachProductCodes.contains(price.Item__c)) {
                ppc = new ProductPriceComparison();
                ppc.productCode = price.Item__c;
                ppc.productName = productCodeToNameMap.get(price.Item__c);
                ppc.uom = 'EA';
                ppc.qualifiedPrice = price.Direct_Price__c/price.Conversion_Factor__c;
                ppc.unitQuantity = 1;
                ppcs.add(ppc);
            }
        }

        return ppcs;
    }

    public void changeQualifiedTier() {
        qualifiedTier = tierIdMap.get(qualifiedTierId);
        getProductPriceComparisons();
        calculateQualifiedAmount();
        qualifiedTierName = qualifiedTier.Name;
    }

    public void changeCompareTier1() {
        compareTier1 = tierIdMap.get(compareTierId1);
        getProductPriceComparisons();
        calculateCompareAmount1();
        compareTier1Name = compareTier1.Name;
    }

    public void changeCompareTier2() {
        compareTier2 = tierIdMap.get(compareTierId2);
        getProductPriceComparisons();
        calculateCompareAmount2();
        compareTier2Name = compareTier2.Name;
    }

    public void changeUom() {
        getProductPriceComparisons();
        setCon.items = uomComparisonListMap.get(uom);
    }

    public void selectCompareTier1() {
        if (compareTier1 != null) {
            this.proposedTier = compareTier1;
            qpCot.Proposed_Pricing_Tier__c = compareTier1.Id;
            update qpCot;
            proposedTierName = proposedTier.Name;
        }
    }

    public void selectCompareTier2() {
        if (compareTier2 != null) {
            this.proposedTier = compareTier2;
            qpCot.Proposed_Pricing_Tier__c = compareTier2.Id;
            update qpCot;
            proposedTierName = proposedTier.Name;
        }
    }

    public void selectQualifiedTier() {
        if (qualifiedTier != null) {
            this.proposedTier = qualifiedTier;
            qpCot.Proposed_Pricing_Tier__c = qualifiedTier.Id;
            update qpCot;
            proposedTierName = proposedTier.Name;
        }
    }

    public void doNext() {
        if(this.setCon.hasNext()) {
            this.setCon.next();
        }
    }
 
    public void doPrevious() {
        if(this.setCon.hasPrevious()) {
            this.setCon.previous();
        }
    }

    public void generateCSV() {
        getProductPriceComparisons();
        String csv = '"Account Name","' + qp.Apttus_Proposal__Account__r.Name + '","","","","","","","",""\n';
        csv += '"Quote/Proposal Name","' + qp.Name + '","","","","","","","",""\n';
        csv += '"Proposed Pricing Tier","' + proposedTierName.replace('"','""') + '","","","","","","","",""\n';
        csv += '"","","","","","","","","",""\n';
        csv += '"","Tier","Estimated Spend (last 12 month usage)","","","","","","",""\n';
        csv += '"Qualified Tier","' + qualifiedTier.Description__c.replace('"','""') + '","' + qualifiedAmount + '","","","","","","",""\n';
        csv += '"Compare Tier 1","' + compareTier1.Description__c.replace('"','""') + '","' + compareAmount1 + '","","","","","","",""\n';
        csv += '"Compare Tier 2","' + compareTier2.Description__c.replace('"','""') + '","' + compareAmount2 + '","","","","","","",""\n';
        csv += '"Units of Measure",' + uom + ',"","","","","","","",""\n';
        csv += '"","","","","","","","","",""\n';
        csv += '"Product Code","Product Name","Quantity","' + qualifiedTierName.replace('"','""') + ' Unit Price","' + qualifiedTierName.replace('"','""') + ' Extended Price","';
        csv += compareTier1Name.replace('"','""') + ' Unit Price","' + compareTier1Name.replace('"','""') + ' Extended Price","' + compareTier2Name.replace('"','""') + ' Unit Price","';
        csv += compareTier2Name.replace('"','""') + ' Extended Price"\n';
        for (ProductPriceComparison ppc: uomComparisonListMap.get(uom)) {
            String quantityStr = ppc.quantity == null ? '' : String.valueOf(ppc.quantity);
            String qualifiedPriceStr = ppc.qualifiedprice == null ? '' : String.valueOf(ppc.qualifiedPrice.setScale(2));
            String comparePrice1Str = ppc.comparePrice1 == null ? '' : String.valueOf(ppc.comparePrice1.setScale(2));
            String comparePrice2Str = ppc.comparePrice2 == null ? '' : String.valueOf(ppc.comparePrice2.setScale(2));
            String qualifiedExtPriceStr = ppc.qualifiedPrice == null || ppc.quantity == null ? '' : String.valueOf((ppc.qualifiedPrice*ppc.quantity).setScale(2));
            String compareExtPrice1Str = ppc.comparePrice1 == null || ppc.quantity == null ? '' : String.valueOf((ppc.comparePrice1*ppc.quantity).setScale(2));
            String compareExtPrice2Str = ppc.comparePrice2 == null || ppc.quantity == null ? '' : String.valueOf((ppc.comparePrice2*ppc.quantity).setScale(2));

            csv += ppc.productCode + ',"' + ppc.productName.replace('"','""') + '",' + quantityStr + ',' + qualifiedPriceStr + ',';
            csv += qualifiedExtPriceStr + ',' + comparePrice1Str + ',' + compareExtPrice1Str + ',';
            csv += comparePrice2Str + ',' + compareExtPrice2Str + '\n';
        }

        Attachment att = new Attachment();
        att.Body = Blob.valueOf(csv);
        att.Name = 'comparepricing_' + System.now().format('yyyy-MM-dd-hh-mm-ss') + '.csv';
        att.parentId = qp.id;
        insert att;
        attachmentId = att.Id;
    }

    public PageReference downloadCSV() {
        PageReference ref = new PageReference('/servlet/servlet.FileDownload?file=' + attachmentId);
        return ref;
    }
 
    public List<ProductPriceComparison> getComparisons() {
        return this.setCon.getRecords();
    }

    private static String pad(Integer inNum) {
        String pad = '';
        if (inNum != null) {
            if (inNum <10) {
                pad += '0';
            }
            pad += inNum;
        }

        return pad;
    }

    public String getTimeStamp() {
        Datetime now = System.now();
        String timeStamp = now.year() + '-' + pad(now.month()) + '-' + pad(now.day()) + 'T' + pad(now.hour()) + '-' + pad(now.minute()) + '-' + pad(now.second());
        return timeStamp;
    }
 
    public Boolean getHasPrevious() {
        return this.setCon.hasPrevious();
    }
 
    public Boolean getHasNext() {
        return this.setCon.hasNext();
    }
 
    public Integer getPageNumber() {
        return this.setCon.pageNumber;
    }
 
    public Integer getTotalPages() {
        return this.setCon.getTotalPages();
    }

    public Integer getStartIdx() {
        return this.setCon.getStartIdx();
    }

    public Integer getEndIdx() {
        return this.setCon.getEndIdx();
    }

    public Integer getResultsSize() {
        return this.setCon.getResultsSize();
    }

    // Screen display row with all values for product price comparison
    public class ProductPriceComparison {
        public String productCode {get;set;}
        public String productName {get;set;}
        public String uom {get;set;}
        public Decimal qualifiedPrice {get;set;}
        public Decimal comparePrice1 {get;set;}
        public Decimal comparePrice2 {get;set;}
        public Decimal quantity {get;set;}
        public Integer unitQuantity {get;set;}
    }

    // Like ApexPages.StandardSetController, but able to handle Objects, not just sObjects.
    public class SetController {
        public List<ProductPriceComparison> items;
        public Integer pageSize {get;set;}
        public Integer pageNumber {get;set;}

        public Integer getStartIdx() {
            return ((pageNumber - 1) * pageSize) + 1;
        }

        public Integer getEndIdx() {
            Integer pageEnd = (pageNumber) * pageSize;
            if (pageEnd > getResultsSize()) {
                pageEnd = getResultsSize();
            }
            return pageEnd;
        }

        public Integer getResultsSize() {
            if (items == null || items.isEmpty()) {
                return 0;
            } else {
                return items.size();
            }
        }

        public void next() {
            if (this.hasNext()) {
                pageNumber++;
            }
        }

        public void previous() {
            if (this.hasPrevious()) {
                pageNumber--;
            }
        }

        public Boolean hasNext() {
            if (pageNumber >= getTotalPages()) {
                return false;
            } else {
                return true;
            }
            
        }

        public Boolean hasPrevious() {
            if (pageNumber <= 1) {
                return false;
            } else {
                return true;
            }
        }

        public List<ProductPriceComparison> getRecords() {
            List<ProductPriceComparison> records = new List<ProductPriceComparison>();
            for (Integer index = getStartIdx() - 1; index < getEndIdx(); index++) {
                records.add(items.get(index));
            }

            return records;
        }

        public Integer getTotalPages(){
            Decimal totalSize = getResultsSize();
            Decimal pageSize = this.pageSize;
     
            Decimal pages = totalSize/pageSize;
     
            return (Integer)pages.round(System.RoundingMode.CEILING);
        }

        public SetController(List<ProductPriceComparison> items) {
            this.items = items;
            this.pageSize = 1000;
            this.pageNumber = 1;
        }
    }
}