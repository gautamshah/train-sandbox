@isTest(SeeAllData=true)
private class JP_OpportunityBusinessTest{
     /****************************************************************************************
     * Name    : JP_OpportunityBusinessTest 
     * Author  : Hiroko Kambayashi
     * Date    : 10/12/2012 
     * Purpose : Test class for JP_OpportunityBusiness
     *           
     * Dependencies: RecordType Object
     *             , Account Object
     *             , Opportunity Object
     *             , Contact Object
     *             , JP_Intimacy__c Object
     *             , Account_Extended_Profile__c Object
     *
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 12/10/2015  Syu    Iken          Comment out the mtehod "Test_InputIntimacyByKeyDoctor_01" and "Test_InputIntimacyByKeyDoctor_02" and "Test_InputBUInfoByBusinessUnit_01"
     *****************************************************************************************/
     
    /*
     * Intimacy registration Trigger test01 by Field JP_Key_Dr__c on Opportunity 
     *    (Case: extracted Intimacy= 1 recod and then update JP_Key_Dr__c to Null)
     */
    /*12/10/2015 Comment out
    static testMethod void Test_InputIntimacyByKeyDoctor_01(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1012');
        //Create Test Contact
        Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1012');
        //Get Test User Id
        User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
        //Create Test Intimacy
        JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1012', 
                                                                    System.Label.JP_Opportunity_StageName_Develop, 
                                                                    System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
        Opportunity insOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
        System.assertEquals(insOpp.JP_Intimacy__c, null);//System.Label.JP_Intimacy_Intimacy_Bgood
        //Update JP_Key_Dr__c of Opportunity to Null
        insOpp.JP_Key_Dr__c = null;
        update insOpp;
        Opportunity updOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
        System.assertEquals(updOpp.JP_Intimacy__c, null);
        Test.stopTest();
    }*/
    /*
     * Intimacy registration Trigger test02 by Field JP_Key_Dr__c on Opportunity
     *   (Case:JP_Intimacy__c is not extracted by JP_Key_Dr__c and Ownerid of Opportunity)
     */
    /*12/10/2015 Comment out
    static testMethod void Test_InputIntimacyByKeyDoctor_02(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1012');
        //Create Test Contact(Without Intimacy)
        Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1012');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1012', 
                                                                    System.Label.JP_Opportunity_StageName_Develop, 
                                                                    System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
        Opportunity insertedOpp = [SELECT Id, JP_Intimacy__c FROM Opportunity WHERE Id = : opp.Id];
        System.assertEquals(insertedOpp.JP_Intimacy__c, null);//System.Label.JP_Opportunity_Intimacy_Nothing
        Test.stopTest();
    }*/
    /*
     * Account_Extended_Profile__c.Id registration Trigger test01
     *   (Case:Account_Extended_Profile__c.Id is extracted by AccountId and JP_Business_Unit__c of Opportunity)
     */
    /*12/10/2015 Comment out
    static testMethod void Test_InputBUInfoByBusinessUnit_01(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1108');
        //Create Test Account Extended Profile
        Account_Extended_Profile__c accprof = JP_TestUtil.createTestAccountExtendedProfile(System.Label.JP_Product2_BusinessUnit_EM, acc.Id);
        //Create Test Opportunity(*Default of the values of JP_Business_Unit__c is 'EM')
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_BUInputTest1108', 
                                                            System.Label.JP_Opportunity_StageName_Develop, 
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable, 
                                                            2012, 11, 8, null);
        Opportunity insertedOpp = [SELECT Id, JP_Account_Extended_Profile__c FROM Opportunity WHERE Id = : opp.Id];
        System.assertEquals(null, insertedOpp.JP_Account_Extended_Profile__c);//accprof.Id
        Test.stopTest();
    }*/
    /*
     * OpportunityLineItem records delete and insert Trigger test01
     *   (Case:JP_Sales_start_Date__c of Opportunity is updated)
     */
    static testMethod void Test_ReDevideRevenueSchaduleBySalesStartDate_01(){
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1109');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_ReRevenueTest1109', 
                                                            System.Label.JP_Opportunity_StageName_Develop, 
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable, 
                                                            2012, 12, 9, null);
        Opportunity testop = [SELECT Id, JP_RemainMonth__c, JP_Sales_start_Date__c, JP_Logic_Type__c FROM Opportunity WHERE Id = : opp.Id];
        //Get standard price book
        Pricebook2 stbook = JP_TestUtil.selectStandardPricebook();
        //Create custom price book
        Pricebook2 ctbook = JP_TestUtil.createCustomPricebook('test_DemoPricebook');
        //Create product (CanUseRevenueSchedule=True)
        Product2 prd = JP_TestUtil.createTestProduct('test_product001', true, null, null);
        //Create Pricebook entry (standard)
        PricebookEntry stdentry = JP_TestUtil.createTestPricebookEntry(stbook.Id, prd.Id, false);
        //Create Pricebook entry (custom)
        PricebookEntry testentry = JP_TestUtil.createTestPricebookEntry(ctbook.Id, prd.Id, true);
        
        //Create Test OpportunityLineItem
        OpportunityLineItem opl = JP_TestUtil.createTestOpportunityLineItem(opp.Id, testentry.Id, 100, 1200000);
        //OpportunityLineItem info before run Trigger.isUpdate 
        OpportunityLineItem insertedOpl = [SELECT Id, TotalPrice FROM OpportunityLineItem WHERE OpportunityId = : opp.Id];
        //Run Trigger after update JP_Sales_start_Date__c
        Opportunity insertedOpp = [SELECT Id, CloseDate, JP_Sales_start_Date__c FROM Opportunity WHERE Id = : opp.Id];
        insertedOpp.CloseDate = Date.newInstance(2013, 6, 18);
        update insertedOpp;
        //OpportunityLineItem info after run Trigger.isUpdate 
        OpportunityLineItem updatedOpl = [SELECT Id, TotalPrice FROM OpportunityLineItem WHERE OpportunityId = : opp.Id];
        //System.assertNotEquals(insertedOpl.Id, updatedOpl.Id);
        System.assertEquals(insertedOpl.TotalPrice, updatedOpl.TotalPrice);
        Test.stopTest();
    }
    
}