public class ValidateAndCompleteCI {
	
	public String hasError='F';
	public String ErrorAlert{get;set;}
	public String ChecktIfvalidate{get;set;}
	public String CyclePeriodId{get;set;}
	List<Cycle_Period__c> lstPendingCyclePeriod{get;set;}
	public String CyclePeriodChoice{get;set;}
	public ValidateandCompleteCI(ApexPages.StandardController stdController) {
        ChecktIfvalidate=ApexPages.currentPage().getParameters().get('Validate');
        CyclePeriodId=ApexPages.currentPage().getParameters().get('Id');
        List<Cycle_Period__c> lstPendingCyclePeriodQuery=[Select Id,Name,Channel_Inventory_Submission_Date__c,Cycle_Period_Reference__r.Name, (Select Id from Channel_Inventories__r limit 1) from Cycle_Period__c where Id=:CyclePeriodId];
        if(lstPendingCyclePeriodQuery==null||lstPendingCyclePeriodQuery.size()==0)
    	{
    		ErrorAlert='N';
    	//	ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
    		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
            ApexPages.addMessage(errormsg);
    	}
    	else
    	{
    		 List<Cycle_Period__c> lstPendingCyclePeriodTemp=new List<Cycle_Period__c>();
    		for(Cycle_Period__c cpv:lstPendingCyclePeriodQuery){
    			if(cpv.Channel_Inventories__r!=null&&cpv.Channel_Inventories__r.size()>0)
    				lstPendingCyclePeriodTemp.add(cpv);
    		
    		}
    		if(lstPendingCyclePeriodTemp==null||lstPendingCyclePeriodTemp.size()==0)
	    	{
	    		ErrorAlert='N';
	    	//	ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
	    		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
	            ApexPages.addMessage(errormsg);
	    	}
	    	else
	    	{
	    		lstPendingCyclePeriod=lstPendingCyclePeriodTemp;
	    	}
    		
    	}
    }
    public ValidateandCompleteCI()
    {
    	ChecktIfvalidate=ApexPages.currentPage().getParameters().get('Validate');
    }
    public void processSelectedPeriod()
    {
    	if(CyclePeriodChoice==null||CyclePeriodChoice=='')
    	{
    		// ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please Choose One of the Cycle Periods');
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_Choose_One_of_the_Cycle_Periods);
            ApexPages.addMessage(errormsg);
    	}
    	else
    	{
    		lstPendingCyclePeriod=[Select Id,Name,Channel_Inventory_Submission_Date__c,Cycle_Period_Reference__r.Name, (Select Id from Channel_Inventories__r limit 1) from Cycle_Period__c where Channel_Inventory_Submission_Date__c=null and status__c='Open' and Id=:CyclePeriodChoice];
    		Pagereference pg=validateAndComplete();
    	}
    	
    }
    public Pagereference validateAndComplete()
    {
    	FileUploadErrors.setValidationHasRan();
    	Pagereference pgRef=null;
    	If(lstPendingCyclePeriod==null||lstPendingCyclePeriod.size()==0)
    	{
    		ErrorAlert='N';
    //		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
            
            ApexPages.addMessage(errormsg);
    	}
    	else
    	{
	    	List<Channel_Inventory__c> lstCItoValidate=[Select Id,Product_Code_Text__c,Submitted__c from Channel_Inventory__c where Cycle_Period__c in :lstPendingCyclePeriod];
	    	List<Channel_Inventory__c> lstCItoValidate1=new List<Channel_Inventory__c>();
	    	if(lstCItoValidate!=null&&lstCItoValidate.size()>0)
	    	{
	    	 	List<Channel_Inventory__c> lstCIFailed=[Select Id,Error_message__c,Product_Code_Text__c from Channel_Inventory__c where Cycle_Period__c in :lstPendingCyclePeriod and Validation_Messages__c !='' limit 1];
		   		//if(hasError=='T')
		    	if(lstCIFailed!=null&&lstCIFailed.size()>0)
		    	{
		    		ErrorAlert='T';
		    		//update lstCItoValidate;
		    //		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please Review Data Validation Errors on Records');
					ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,label.Please_Review_Data_Validation_Errors_on_Records);
		            ApexPages.addMessage(errormsg);
		    	}
		    	else
		    	{
		    		ErrorAlert='F';
		    		if(ChecktIfvalidate!='Y' && lstCItoValidate!=null && lstCItoValidate.size()>0)
		    		{
		    			for(Channel_Inventory__c ci: lstCItoValidate)
		    				ci.Submitted__c = True;
		    		
		    			update lstCItoValidate;
		    		}
		    		for(Cycle_Period__c cp:lstPendingCyclePeriod)
		    		{
		    			//cp.Status__c='Submitted';
		    			if(ChecktIfvalidate!='Y'&&cp.Channel_Inventory_Submission_Date__c==null&&(cp.Channel_Inventories__r!=null&&cp.Channel_Inventories__r.size()>0))
			    			cp.Channel_Inventory_Submission_Date__c=Date.today();
		    		}
		    		update lstPendingCyclePeriod;
		    	//	ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'Data Submitted Successfuly');
		    		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.Data_Submitted_Successfully);
		            ApexPages.addMessage(errormsg);
		          //  update lstCItoValidate;
		    	}
		    	
	    	}//End of if(lstCItoValidate!=null&&lstCItoValidate.size()>0)
	    	else
	    	{
	    		ErrorAlert='N';
	    	//	ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,'No Data to Validate');
	    		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.INFO,label.No_Data_to_Validate);
	            ApexPages.addMessage(errormsg);
	    	}
	    	
   		}
    	return pgRef;
    }
    
    public static void runCycleValidations(List<Channel_Inventory__c> lstCItoValidate)
    {
    	for(Channel_Inventory__c sot:lstCItoValidate)
    	{
    		//if(sot.Run_Validations__c==true)
    		//{
	    		sot.Error_message__c='';
	    		if(sot.Product_Code__c==null)
	    		{
	    		//	System.debug('test 1');
//	    			sot.Error_message__c='Product Code Not Found,';
	    			sot.Error_message__c = label.Product_code_not_found +',';
	    			//hasError='T';
	    		}
	    		
	    		
    		//}
    		//sot.Run_Validations__c=false;
    	}
    
    }
     public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            If(lstPendingCyclePeriod!=null&&lstPendingCyclePeriod.size()>0)
    		{
    			for(Cycle_Period__c cpForList:lstPendingCyclePeriod)
    			{
    				options.add(new SelectOption(cpForList.Id,cpForList.Cycle_Period_Reference__r.Name));
    			}
    		}
            
           // options.add(new SelectOption('Append','Append'));
            return options;
        }

}