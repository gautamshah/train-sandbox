//Generated by wsdl2apex

public class proxyPricingCovidienCom2 {
    public class BasicHttpBinding_ITwoWayAsync {
        public String endpoint_x = PricingServiceVariables__c.getInstance('ServiceEndpoint').Value__c ;
        //'http://tamans-ap64vd.thcg.net/Covidien.PricingProxy/Covidien_PricingProxy_Orchestrations_PricingLiteServiceType_ProxyPricingLite_RcvSndPrt.svc';
        //https://wsendpointsqa.covidien.com/Covidien.PricingProxy/Covidien_PricingProxy_Orchestrations_PricingLiteServiceType_ProxyPricingLite_RcvSndPrt.svc
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x; 
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://proxy.pricing.covidien.com', 'proxyPricingCovidienCom2', 'http://pricing.covidien.com', 'pricingCovidienCom2'};
        
        
        public pricingCovidienCom2.PricingRequestType PricingLiteProxy(pricingCovidienCom2.PricingRequestType req) {
            pricingCovidienCom2.PricingRequestType request_x = req;
            pricingCovidienCom2.PricingRequestType response_x;
            //request_x.SKUs = SKUs;
            Map<String, pricingCovidienCom2.PricingRequestType> response_map_x = new Map<String, pricingCovidienCom2.PricingRequestType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'PricingLiteProxy',
              'http://pricing.covidien.com',
              'PricingRequest',
              'http://pricing.covidien.com',
              'PricingRequest',
              'pricingCovidienCom2.PricingRequestType'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}