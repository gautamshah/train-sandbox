/****************************************************************************************
 * Name    : CPQ SSG_LOC_GPO_CustomButton
 * Author  : Clay Hartman
 * Date    : 7/10/2015
 * Purpose : Contains the logic for SSG_GPO_LOC visualforce page to create an agreement record
 * Dependencies: SSG_GPO_LOC Visualforce page & Create_GPO_LOC custom Button
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *
 *****************************************************************************************/


public class CPQ_SSG_LOC_GPO_CustomButton {

    Public Id Accid;
    string sCancelURL;
    public CPQ_SSG_LOC_GPO_CustomButton(ApexPages.StandardController controller) {
        Accid = system.currentPageReference().getParameters().get('Id');
        system.debug('Id: ' + Accid);
    }

    public PageReference save() {
        return null;
    }

    public Apttus__APTS_Agreement__c AptsAgreement{get;set;}
    public ID AccountId;

    public PageReference  CreateAgreement() {
        if(Accid!=NULL){

            RecordType scrubPOType = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,'GPO_LOC');
            Apttus_Config2__Pricelist__c ssgPriceList = cpqPriceList_c.SSG;

            List<Account> str = [select Name from Account where id =: Accid ];
            system.debug('&&&&&&&'+str);
            Apttus__APTS_Agreement__c apt = new Apttus__APTS_Agreement__c (Apttus__Account__c=Accid ,
                RecordTypeId = scrubPOType.Id, Apttus_CMConfig__PriceListId__c = ssgPriceList.Id,  name=(str[0].Name.left(71) +' '+'GPO LOC'+' '+date.today().format()));

            insert apt;
            // Insering new Agreement record

            PageReference agreementPage = new ApexPages.StandardController(apt).edit();

            agreementPage.getParameters().put('Id','/' + apt.Id);
            agreementPage.getParameters().put('retURL','/' + apt.Id);

            // build the cancel URL.  If the user clicks cancel on the new agreement record, call the Apttus method to delete the new agreement
            //  and place the user back on the account record.
            sCancelURL = '/apex/apttus_proposal__cancelactioninterceptor?actionName=create_oppty_proposal&objectId=' +
                Accid + '&accountId = ' + Accid+ '&agreementId=' + apt.id + '&rollbackId=' + apt.id;
            agreementPage.getParameters().put('cancelURL','/' + sCancelURL );


            return agreementPage;
        }
        return null;
    }// End of CreateAgreement method

 }