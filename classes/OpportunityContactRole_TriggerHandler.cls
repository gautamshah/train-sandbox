/****************************************************************************************
 * Name    : OpportunityContactRole_TriggerHandler 
 * Author  : Leena Khatri
 * Date    : 22/01/2015
 * Purpose : Contains all the logic coming from  OpportunityContactRole_Trigger trigger
 * Dependencies: OpportunityContactRole_Trigger
 *             , OpportunityContactRole
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------                          
 *****************************************************************************************/
public with sharing class OpportunityContactRole_TriggerHandler  
{
    List<OpportunityContactRole> lstOppcontactrole = new List<OpportunityContactRole>();
    public void OnAfterInsert(List<Opportunity_Contact_Role__c> newObjects)
    {
        for (Opportunity_Contact_Role__c customOppcontactrole : newObjects) 
        {
            OpportunityContactRole opp = new OpportunityContactRole();
            opp.OpportunityId = customOppcontactrole.Opportunity_del__c;
            opp.Role  = 'Other';
            opp.IsPrimary  = false;
            opp.ContactId= customOppcontactrole.Opportunity_Contact__c;            
            lstOppcontactrole.add(opp);
        }
        
        try 
        {
            insert lstOppcontactrole;
        }
        catch (DMLException e) 
        {
            trigger.new[0].addError(e.getDMLMessage(0));
            System.debug(' DML Exception: ' + e);
        }
           
    } 
        // Delete Logic
    public void OnBeforeDelete(List<Opportunity_Contact_Role__c> OldObjects,Map<Id,Opportunity_Contact_Role__c> oldMap) 
    {
        
        List<OpportunityContactRole> lstOppConRole=new List<OpportunityContactRole>();
        Integer i=0;               
        Set<Id> OppIDs = new Set<Id>();
        Set<Id> ContactIDs = new Set<Id>();
        Map<String,Id> mapDeleteOppContactRole=new Map<String,Id>();
        String strdelete=''; 
        String strOppContactId='';
        if(OldObjects!=null && OldObjects.size()>0)
        {
            for(i=0; i < OldObjects.size(); i++)
            {
                if(OldObjects[i].Opportunity_del__c!=null && OldObjects[i].Opportunity_Contact__c!=null)
                {     
                    strdelete='';  
                    strdelete=String.valueOf(OldObjects[i].Opportunity_del__c) + String.valueOf(OldObjects[i].Opportunity_Contact__c); 
                    mapDeleteOppContactRole.put(strdelete,OldObjects[i].Opportunity_Contact__c);                                
                    OppIDs.add(OldObjects[i].Opportunity_del__c);               
                    ContactIDs.add(OldObjects[i].Opportunity_Contact__c);                                              
                }            
            }
        }
        
        lstOppConRole=[SELECT Id,OpportunityId,ContactId from  OpportunityContactRole where OpportunityId IN:OppIDs and ContactId IN:ContactIDs];
         
        try 
        {      
            if(lstOppConRole!=null && lstOppConRole.size()>0)
            {
                for(OpportunityContactRole OppContactRole:lstOppConRole)
                {
                    
                    if(OppContactRole.OpportunityId!=null && OppContactRole.ContactId!=null)
                    {
                        strOppContactId='';
                        strOppContactId=String.valueOf(OppContactRole.OpportunityId) + String.valueOf(OppContactRole.ContactId);    
                        if(mapDeleteOppContactRole.get(strOppContactId)!=null && mapDeleteOppContactRole.get(strOppContactId)==OppContactRole.ContactId)
                        {                       
                            lstOppcontactrole.add(OppContactRole); 
                        }
                    }                 
                }
                
                if(lstOppcontactrole!=null && lstOppcontactrole.size()>0)
                delete lstOppcontactrole;  
            } 
        }
        catch (DMLException e) 
        {
            trigger.new[0].addError(e.getDMLMessage(0));
            System.debug(' Delete DML Exception: ' + e);
        }          
        
    }
    
    // Update Logic
    public void OnAfterUpdate(List<Opportunity_Contact_Role__c> newObjects,Map<Id,Opportunity_Contact_Role__c> oldMap, Map<Id,Opportunity_Contact_Role__c> newMap) 
    {
        Set<Id> OppIDs = new Set<Id>();
        Set<Id> ContactIDs = new Set<Id>();
        Map<String,Id> mapUpdateOppContactRole=new Map<String,Id>();
        List<OpportunityContactRole> lstOppConRole=new List<OpportunityContactRole>();
        String strupdate='';
        String strOppContactId='';
          
        for( Opportunity_Contact_Role__c oppcontactrole : newObjects )
        {
            
            if(oppcontactrole.Opportunity_Contact__c != oldMap.get(oppcontactrole.Id).Opportunity_Contact__c)
            {
                if(oppcontactrole.Opportunity_del__c!=null && oldMap.get(oppcontactrole.Id).Opportunity_Contact__c!=null)
                {
                strupdate='';
                strupdate=String.valueOf(oppcontactrole.Opportunity_del__c) + String.valueOf(oldMap.get(oppcontactrole.Id).Opportunity_Contact__c);
                if(newMap.get(oppcontactrole.Id).Opportunity_Contact__c!=null && strupdate!=null)
                mapUpdateOppContactRole.put(strupdate,newMap.get(oppcontactrole.Id).Opportunity_Contact__c);                
                OppIDs.add(oppcontactrole.Opportunity_del__c);                
                ContactIDs.add(oldMap.get(oppcontactrole.Id).Opportunity_Contact__c); 
                }                               
            }       
        }
        
        lstOppConRole=[SELECT Id,OpportunityId,ContactId from  OpportunityContactRole where OpportunityId IN :OppIDs and ContactId IN:ContactIDs];    
        
        try
        { 
            if(lstOppConRole!=null && lstOppConRole.size()>0)
            {
                for(OpportunityContactRole OppContactRole:lstOppConRole)
                {
                    if(OppContactRole.OpportunityId!=null && OppContactRole.ContactId!=null)
                    {
                        strOppContactId='';
                        strOppContactId=String.valueOf(OppContactRole.OpportunityId) + String.valueOf(OppContactRole.ContactId);
                        if(mapUpdateOppContactRole.get(strOppContactId)!=null)
                        {
                            OppContactRole.ContactId=mapUpdateOppContactRole.get(strOppContactId);
                            lstOppcontactrole.add(OppContactRole);  
                        }
                    }                
                }
                
                if(lstOppcontactrole!=null && lstOppcontactrole.size()>0)
                update lstOppcontactrole;  
            } 
        }
        catch(exception e)
        {
                    trigger.new[0].addError(e.getDMLMessage(0));
                    System.debug(' DML Exception: ' + e);
        }     
    }     
}