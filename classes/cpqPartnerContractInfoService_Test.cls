/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed  80 => *                            120 => *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry

YYYYMMDD-A  PAB         CPR-000 Updated comments section

7.  Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com
Gautam Shah             GS              Medtronic.com

MODIFICATION HISTORY
====================
Date-Id     Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20171212-A  HN                 Created at 4:02:15 PM by Henry

*/
@isTest
public with sharing class cpqPartnerContractInfoService_Test
{
    @isTest
    static void testGetTheContract()
    {
        Test.setMock(WebServiceMock.class, new cpqPartnerContractInfoServiceMock());
        cpqPartnerContractInfoService service = new cpqPartnerContractInfoService();
        List<String> contractNum = new List<String>();
        contractNum.add('One');
        contractNum.add('Two');
        
        cpqPartnerContractInfoClasses.ContractInfo contract = service.getTheContract(ContractNum);

        System.assert(contract.dealerList.get(0).shipToField == 333333);
        
    }
    
    @isTest
    static void testGetContractEligibilityByRoot()
    {
        String rootContract = '12333', startsWith;
        Test.setMock(WebServiceMock.class, new cpqPartnerContractInfoServiceMock());
        cpqPartnerContractInfoService service = new cpqPartnerContractInfoService();
        List<String> contractNum = new List<String>();
        contractNum.add('One');
        contractNum.add('Two');
        
        cpqPartnerContractInfoClasses.Customer_Wrapper customerWrapper 
            = service.GetContractEligibilityByRoot(rootContract, startsWith);

        System.assert(customerWrapper.Customers.Information[0].ShipTo == 333333);    
    }
    
}