public with sharing class dmDocuments
{
	public static Map<Id, dmDocument__c> All { get; set; }
	public static Map<Id, dmDocument__Share> Shares { get; set; }
	public static List<string> GUIDs { get; set; }
 
	static
	{
    	string query = dm.buildSOQL(dmDocument__c.getSObjectType(), null, null);
    	All = new Map<Id, dmDocument__c>((List<dmDocument__c>)Database.query(query));

    	query = dm.buildSOQL(dmDocument__Share.getSObjectType(), null, null);
    	Shares = new Map<Id, dmDocument__Share>((List<dmDocument__Share>)Database.query(query));

		GUIDs = new List<string>();
				
		for (dmDocument__c d : dmDocuments.All.values())
			GUIDs.add(d.GUID__c);
	}
}