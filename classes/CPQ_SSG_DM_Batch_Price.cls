global class CPQ_SSG_DM_Batch_Price implements Database.Batchable<sObject>
{
    /****************************************************************************************
    * Name    : CPQ SSG DM Batch Price 
    * Author  : Subba Reddy Muchumari
    * Date    : 6/19/2015 
    * Purpose : Batch Job used to SSG Staging Table CPQ_SSG_STG_Deal_Price__c Mapping to CPQ_SSG_QP_Product_Pricing__c table.
    * Dependencies: 
    *
    *****************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //string query='select Id,STG_ClassOfTradeCode__c,STG_DealId__c,STG_CreatedByName__c,STG_ProductCode__c,STG_QualifiedPrice__c from CPQ_SSG_STG_Deal_Price__c where Name=\'P-00001\'';
        string query='select Id,STG_ClassOfTradeCode__c,STG_DealId__c,STG_CreatedByName__c,STG_ProductCode__c,STG_QualifiedPrice__c from CPQ_SSG_STG_Deal_Price__c';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<CPQ_SSG_STG_Deal_Price__c> scope)
    {
        List<CPQ_SSG_STG_Deal__c> processDeals = [Select Id, STG_DealId__c From CPQ_SSG_STG_Deal__c Where STG_Process__c = true];
        Set<String> processDealIds = new Set<String>();
        for (CPQ_SSG_STG_Deal__c processDeal: processDeals) {
            processDealIds.add(processDeal.STG_DealId__c);
        }

        Set<String> stgDealIdset = new Set<String>();
        Set<String> stgClassTradeCodeset = new Set<String>();
        Set<String> createdByNameSet = new Set<String>();
        Set<String> stgProductCodeSet = new Set<String>();
        List<CPQ_Error_Log__c> errorLogs = new List<CPQ_Error_Log__c>();

        List<CPQ_SSG_STG_Deal_Price__c> processScope = new List<CPQ_SSG_STG_Deal_Price__c>();
        for(CPQ_SSG_STG_Deal_Price__c c:scope)
        {
            if (processDealIds.contains(c.STG_DealId__c)) {
                processScope.add(c);
                stgDealIdset.add(c.STG_DealId__c);
                stgClassTradeCodeset.add(c.STG_ClassOfTradeCode__c);
                createdByNameSet.add(c.STG_CreatedByName__c);
                stgProductCodeSet.add(c.STG_ProductCode__c);
            }
        }
        List<CPQ_SSG_QP_Class_of_Trade__c> cpqSSGQPTradeList=[Select Id,Legacy_External_Id__c,CPQ_SSG_Class_of_Trade__r.Code__c, Quote_Proposal__c From CPQ_SSG_QP_Class_of_Trade__c Where Legacy_External_Id__c IN:stgDealIdset And CPQ_SSG_Class_of_Trade__r.Code__c IN:stgClassTradeCodeset];
        Map<String,CPQ_SSG_QP_Class_of_Trade__c> cpqSSGQPTradeMap = new Map<String,CPQ_SSG_QP_Class_of_Trade__c>();
        for (CPQ_SSG_QP_Class_of_Trade__c cp:cpqSSGQPTradeList)
        {
            cpqSSGQPTradeMap.put(cp.Legacy_External_Id__c + cp.CPQ_SSG_Class_of_Trade__r.Code__c,cp);
        }
        
        List<CPQ_DM_Variable__c> CPQDMVarList=[Select Value__c,Name From CPQ_DM_Variable__c Where Type__c= 'User' And Name IN:createdByNameSet];
        Map<string,string> CPQDMVarMap=new Map<string,string>();
        system.debug('CPQDMVarList*******'+CPQDMVarList);
        for(CPQ_DM_Variable__c cdv:CPQDMVarList)
        {
            CPQDMVarMap.put(cdv.Name,cdv.Value__c);
        }
        
        List<product2> productsList=[select Id,productcode From Product2 Where ProductCode IN:stgProductCodeSet];
        Map<string,string> productMap=new Map<string,string>();
        for(product2 p:productsList)
        {
            productMap.put(p.productcode,p.Id);
        }
        //Inserting CPQ_SSG_QP_Product_Pricing__c records
        List<CPQ_SSG_QP_Product_Pricing__c> cpqSSGQPrdPricNewRecList = new List<CPQ_SSG_QP_Product_Pricing__c>();
        for(CPQ_SSG_STG_Deal_Price__c c:processScope)
        {
            CPQ_SSG_QP_Product_Pricing__c cpqSSGQPrdPric = new CPQ_SSG_QP_Product_Pricing__c();
            CPQ_SSG_QP_Class_of_Trade__c qpCot = cpqSSGQPTradeMap.get(c.STG_DealId__c+c.STG_ClassOfTradeCode__c);
            cpqSSGQPrdPric.CPQ_SSG_QP_Class_of_Trade__c = qpCot.Id;
            cpqSSGQPrdPric.Proposal_Product_Combined_Key_Text__c = qpCot.Quote_Proposal__c + '_' + cpqSSGQPrdPric.Product__c;
            if(CPQDMVarMap.Containskey(c.STG_CreatedByName__c))
            {
                cpqSSGQPrdPric.OwnerId=CPQDMVarMap.get(c.STG_CreatedByName__c);
                if(productMap.ContainsKey(c.STG_ProductCode__c))
                {
                    cpqSSGQPrdPric.Product__c=productMap.get(c.STG_ProductCode__c);
                    cpqSSGQPrdPric.Price__c=c.STG_QualifiedPrice__c;
                    cpqSSGQPrdPricNewRecList.add(cpqSSGQPrdPric);
                } else {
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Price', 'Error', 'Product ' + c.STG_ProductCode__c + ' not found', 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
                }
            } else {
                errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Price', 'Error', 'User ' + c.STG_CreatedByName__c + ' not found in CPQ_DM_Variable__c', 'Id: ' + c.Id + ', DealId: ' + c.STG_DealId__c, null));
            }
            
        }
        //Insert cpqSSGQPrdPricNewRecList;
        
        List<Database.SaveResult> srscpqSSGQPrdPricNewRecList= Database.insert(cpqSSGQPrdPricNewRecList, FALSE);
        for (Database.SaveResult sr : srscpqSSGQPrdPricNewRecList) 
        {
            if (sr.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted CPQ productPricing Records: ' + sr.getId());
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) 
                {
                    System.debug('The following error has occurred.');                    
                    System.debug('Fields that affected this error: ' + err.getFields());
                    errorLogs.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_SSG_DM_Batch_Price', 'Error', 'Insert CPQ_SSG_QP_Product_Pricing failed ' + err.getMessage(), null, null));
                }
            }
        }
        
        system.debug('cpqSSGQPrdPricNewRecList******'+cpqSSGQPrdPricNewRecList);

        if (!errorLogs.isEmpty()) {
            insert errorLogs;
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        
    }   
}