@isTest
private class TestActivityFilter {
/*********************************************************************
*
* Updates
*
*  Date      Author               Description
* ----------------------------------------------------------------------
*
*  8-3-2012   MJM         Re-configured to used test account creation from TestUtility class
*
*********************************************************************/     
     static testMethod void runTest() {
        
        //create test data
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        
        Opportunity o = new Opportunity (accountId = a.Id, name = 'TestOppty', StageName='x', CloseDate=System.Today() + 30);
        
        insert o;
        
        //create test data
        Event e = new Event();
        
        e.Subject = 'test';
        e.WhatId = a.Id;
        e.StartDateTime = System.today();
        e.Franchise__c = 'x';
        e.Business_Unit__c=  'y';
        e.ActivityDateTime = System.today();
        e.DurationInMinutes = 15;
        
        insert e;
        
        //create test data
        Task t = new Task();
        
        t.Subject = 'test';
        t.WhatId = a.Id;
        t.Franchise__c = 'x';
        t.Business_Unit__c=  'y';
        
        insert t;
        
        
        Test.setCurrentPage(Page.ActivityFilterPage);
        ApexPages.currentPage().getParameters().put( 'parentid', a.id );
        ActivityFilter af = new ActivityFilter();
        af.activityType = 'Both';
        af.getActivityTypeOptions();
        af.getEvents();
        af.getStatus();
        af.getTasks();
        af.search();
        af.myActivities = true;
        af.activityType = 'Event';
        af.getEvents();
        af.search();
        af.activityType = 'Task';
        af.search();
        af.getTasks();
        af.cancel();
        af.getGBU();
        af.getFranchise();
        af.getCountries();
        ApexPages.currentPage().getParameters().put( 'parentid', o.id );
    }

}