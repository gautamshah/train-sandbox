@isTest (seeAllData=true)
private class Test_C2_EmailService {
    
    static testMethod void testC2EmailService() {
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.fromAddress = 'c2testfrom@covidien.com';
        email.toAddresses = new List<String>{'c2testto@covidien.com'};
        email.ccAddresses = new List<String>{'c2testcc@covidien.com','c2testcc@covidien.com<cttest@covidien.com>'};
        env.fromAddress = 'c2testfrom@covidien.com';
   
        // call the class and test it with the data in the testMethod
        Gizmo_InboundEmailHandler emailServiceObj = new Gizmo_InboundEmailHandler();
        emailServiceObj.handleInboundEmail(email, env ); 
        
        /* Gautam Shah commented out below section to reduce test execution time
        Profile profile1 = [Select Id from Profile where name = 'System Administrator' limit 1];
        User seller = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdt1test2@mdt1test.com',
            Country='ZA',
            Alias = 'bshan',
            Email='c2testfrom@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='C2',
            Lastname='Test',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            CurrencyISOCode = 'CHF');
        
        insert seller;
        emailServiceObj.handleInboundEmail(email, env ); 
        */
		        
        email.subject = 'test';
        email.plainTextBody = 'test';
        
        //Gautam Shah commented out Account insert to reduce test execution time
        //Account a = new Account(Name='Test Account');
        //insert a;
        Account a = [Select Id From Account Limit 1];
        
        Contact con = new Contact(Lastname='test', FirstName='TEST', AccountId=a.Id, Email='c2testto@covidien.com');
        insert con;
        emailServiceObj.handleInboundEmail(email, env ); 
        
        Contact concc = new Contact(Lastname='test', FirstName='TEST', AccountId=a.Id, Email='c2testcc@covidien.com');
        insert concc;
        emailServiceObj.handleInboundEmail(email, env ); 
		
    }   
}