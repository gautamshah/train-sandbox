/****************************************************************************************
* Name    : Class: AgreementTriggerDispatcher
* Author  : Paul Berglund
* Date    : 10/13/2015
* Purpose : Dispatches to necessary classes when invoked by Proposal trigger
* 
* Dependancies: 
*   Class: Agreement_Main
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 10/15/2015    Bryan Fry               Added method to capture Cycle Times
* 01/20/2016    Paul Berglund           Commented out SSG calls so we can promote to production
*                                       captureAgreementCycleTimes
* 01/22/2016    Paul Berglund           Uncommented after promoting to RlsStg
* 03/18/2016    Paul Berglund           Uncommented Cycle Time call
06/14/2016   Paul Berglund      Moved when to execute Cycle Time logic into class
*****************************************************************************************/
public with sharing class AgreementTriggerDispatcher
{
}