global class EstablishTerritoryRelationship implements Database.Batchable<Sobject>{
	/****************************************************************************************
	 * Name    : EstablishTerritoryRelationship 
	 * Author  : Suchin Rengan
	 * Date    : 06/05/2011 
	 * Purpose : Batch Job used to build Hierarchical, Territory relationships based on territory 
	 *           names and external Terriroty Ids.
	 * Dependencies: Territory Object
	 *
	 * ========================
	 * = MODIFICATION HISTORY =
	 * ========================
	 * DATE        AUTHOR               CHANGE
	 * ----        ------               ------
	 * 06/05/2011  Suchin Rengan        Created
	 *
	 *****************************************************************************************/
	
	private final String query='select Id, Name, ParentTerritoryId, CUSTOM_EXTERNAL_TERRITORYID__C, ExternalParentTerritoryID__c from Territory where Source__c NOT IN (' + inListFromCS() + ') AND flagForProcessing__c = true';
	private Map<String, Id> TerritoryNameIds = new Map<String,Id>();
	private Set<String> ParentTerritoryNames = new Set<String>();
	private List<Territory> updateTerritory = new List<Territory>();
	global database.querylocator start(Database.BatchableContext BC)
	{
		System.debug('>>>>>>>>>>><<<<<<<<<<<<<<start>>>>>>>>>>>>>><<<<<<<<<<<');
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC, Sobject[] scope)
 	{
        System.debug('>>>>>>>>>>><<<<<<<<<<<<<<execute>>>>>>>>>>>>>><<<<<<<<<<<');
 		for(sobject s : scope)
			{
				Territory terr = (Territory)s;
				// Add the parent territory name to this set
				System.debug('The territory parent external Id is '+terr.ExternalParentTerritoryID__c);
				if(terr.ExternalParentTerritoryID__c != null && terr.ExternalParentTerritoryID__c != 'NOPARENT') {
					System.debug('Inside trying to add this territory to map***********'+terr.CUSTOM_EXTERNAL_TERRITORYID__C+'*****');
					System.debug('Inside trying to add this parent territory to map***********'+terr.ExternalParentTerritoryID__c+'*****');
					ParentTerritoryNames.add(terr.ExternalParentTerritoryID__c);
				}
				System.debug('Size of Set***********'+ParentTerritoryNames.size());
			}
		for(Territory t:[select Id, Custom_External_TerritoryID__c  from Territory where Custom_External_TerritoryID__c IN :ParentTerritoryNames]){
			//create a map of parent territories to map back to
			TerritoryNameIds.put(t.Custom_External_TerritoryID__c, t.Id);
			System.debug('Adding Ext Id*** '+t.Custom_External_TerritoryID__c+'  *****'+t.Id);
		}
		for(sobject s : scope)
			{
				Territory terr = (Territory)s;
				// Add the parent territory name to this set
				Id terrId = null;
				if(terr.ExternalParentTerritoryID__c != null && terr.ExternalParentTerritoryID__c != 'NOPARENT' ){
					System.debug('Inside trying to associate this territory to its parent territory');
					terrId = TerritoryNameIds.get(terr.ExternalParentTerritoryID__c);
					System.debug('Found its parent territory '+terrId);
				}	
				terr.ParentTerritoryId = terrId;
				terr.flagForProcessing__c = false;
				updateTerritory.add(terr);
			}
			update updateTerritory;
 	}
 	
 	private static String inListFromCS() {
	    //exclude records from certain sources
	    Set<String> ignores = new Set<String>();
		IC_Config_Setting__c cs = IC_Config_Setting__c.getValues('EstTerRel Batch Exclude Sources');
		
		if (cs == null && Test.isRunningTest()) {
			cs = new IC_Config_Setting__c(Name = 'EstTerRel Batch Exclude Sources', Value__c = 'POIU|YTRE');
		}
		
	    if (cs != null && cs.Value__c != null) {
	    	String[] a = cs.Value__c.split('\\|');
	    	for (String s : a) {
	    		if (!String.isEmpty(s))
	    			ignores.add(s);
	    	}
	    }
    	System.Debug('*** ignores sources: ' + ignores);
    	
 		String s = '';
		for (String a : ignores) {
			if (String.isEmpty(s))
				s = '\'' + a + '\'';
			else
				s = s + ', \'' + a + '\'';
		}
		
		if (String.isEmpty(s))
			s = '\'^^THERE IS NOTHING IN THE INLIST^^\'';
		return s;
 	}
 	
 	global void finish(Database.BatchableContext BC)
	{
		
	}
}