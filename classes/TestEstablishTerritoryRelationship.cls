@isTest
private class TestEstablishTerritoryRelationship {

    static testMethod void runTest() {
       
       
       Test.startTest();
       Territory t = new Territory(Name = 'test 1 x'
                                 , ParentTerritoryId = null
                                 , ExternalParentTerritoryID__c = ''
                                 , Custom_External_TerritoryID__c = 'test1-54321'
                                 , flagForProcessing__c = true);
                                 
       insert t;
       Territory t2 = new Territory(Name = 'test 2 x'
                                 , ParentTerritoryId = t.Id
                                 , ExternalParentTerritoryID__c = 'test1-54321'
                                 , Custom_External_TerritoryID__c = 'test1-67891'
                                 , flagForProcessing__c = true);
                                 
       insert t2;
       //ExternalParentTerritoryID__c
       update t2;
        
       System.assertEquals(true, [Select flagForProcessing__c from Territory where name = 'test 1 x'].flagForProcessing__c);
       
       EstablishTerritoryRelationship batchApex = new EstablishTerritoryRelationship();
       ID batchprocessid = Database.executeBatch(batchApex);
              
       Test.stopTest();
    }
}