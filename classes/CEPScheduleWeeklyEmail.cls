global class CEPScheduleWeeklyEmail Implements Schedulable
   {
     global void execute(SchedulableContext sc){
       
            Group CEP = [Select Id, Name From Group Where Name = 'CEP Group']; 
            Group CEPwc = [Select Id, Name From Group Where Name = 'CEP(without Count)'];
            List<GroupMember> groupMembers = [Select GroupId, UserOrGroupId From GroupMember Where GroupId = :CEP.Id];
            List<GroupMember> groupwcMembers = [Select GroupId, UserOrGroupId From GroupMember Where GroupId = :CEPwc.Id];
            for(GroupMember EachGroupmember : groupMembers){    
                sendCEPwithcountemail(EachgroupMember.UserorGroupId);     
            }
            for(GroupMember EachGroupmember : groupwcMembers ){    
                sendCEPwithoutcountemail(EachgroupMember.UserorGroupId);    
            }
    }
    
   public static void sendCEPwithcountEmail(ID ActualUser) {
   
   EmailTemplate templateId = [Select id from EmailTemplate where name = 'CEP Library Files'];
   
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
   mail.setTargetObjectId(ActualUser);
   mail.setTemplateId(templateId.Id);          
   mail.setBccSender(false);
   mail.setUseSignature(false);
   mail.setSenderDisplayName('CEP.DoNotReply');
   mail.setSaveAsActivity(false);   
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    } 
    
    public static void sendCEPwithoutcountEmail(ID ActualUser) {
   
   EmailTemplate templateId = [Select id from EmailTemplate where name = 'CEP Library Files(Without Count)'];
   
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
   mail.setTargetObjectId(ActualUser);
   mail.setTemplateId(templateId.Id);          
   mail.setBccSender(false);
   mail.setUseSignature(false);
   mail.setSenderDisplayName('CEP.DoNotReply');
   mail.setSaveAsActivity(false);   
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }  
  
 }