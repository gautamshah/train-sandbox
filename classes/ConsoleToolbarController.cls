global without sharing class ConsoleToolbarController 
{
	public List<ButtonSection> buttonTree;
	public ConsoleToolbarController(ApexPages.StandardController controller)
	{
		this.buttonTree = new List<ButtonSection>();
	}
	
	public List<ButtonSection> getButtonTree()
	{
		map<String, ButtonSection> buttonMap = new Map<String, ButtonSection>();
		for(Console_Toolbar_Button__c ctb : [Select Section__c, Portal_URL__c, Name, Id, Display_Order__c From Console_Toolbar_Button__c Where Active__c = true Order By Section__c, Display_Order__c ASC])
		{
			ButtonSection bs;
			if(buttonMap.containsKey(ctb.Section__c))
			{
				bs = buttonMap.get(ctb.Section__c);
			}
			else
			{
				bs = new ButtonSection(ctb.Section__c);
			}
			bs.addButton(new Button(ctb.Id, ctb.Name, ctb.Portal_URL__c));
			buttonMap.put(ctb.Section__c, bs);
		}
		return buttonMap.values();
	}
	
	@RemoteAction
	global static void logConsoleEvent(String CaseId, String eventType, String toolbarButtonId, String currentToolURL)
	{
		DateTime timestamp = DateTime.now();
		ConsoleEventLogger.log(CaseId, eventType, timestamp, toolbarButtonId, currentToolURL);
	}
	
	class ButtonSection
	{
		public String name {get;set;}
		public List<Button> buttonList {get;set;}
		public buttonSection(String name)
		{
			this.name = name;
			this.buttonList = new List<Button>();
		}
		public void addButton(Button button)
		{
			this.buttonList.add(button);
		}
	}
	class Button
	{
		public String id {get;set;}
		public String name {get;set;}
		public String URL {get;set;}
		public button(String id, String name, String URL)
		{
			this.id = id;
			this.name = name;
			this.URL = URL;
		}
	}
	
	@isTest(SeeAllData=true)
	static void test()
	{
		Test.startTest();
		PageReference pageRef = Page.ConsoleToolbar;
		Test.setCurrentPage(pageRef);
		ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(new Case());
		ConsoleToolbarController ctc = new ConsoleToolbarController(controller);
		ctc.getButtonTree();
		ConsoleToolbarController.logConsoleEvent(controller.getId(), 'Web View', null, null);
		Test.stopTest();
	}
}