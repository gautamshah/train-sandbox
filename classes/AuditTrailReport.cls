public class AuditTrailReport{
    public List<SetupAuditTrail> lstAuditTrail{get; set;}
    Set<String> setAuditTrailSection = new Set<String>{'Manage Users', 'Manage Territories', 'Partner Relationship Management'};
    Set <String> setAuditTrailAction = new Set<String>{'has granted salesforce.com login access through','has changed administrator login access through','has revoked salesforce.com login access','Changed membership of Group','groupMembership','loginasgrantedtosfdc' };
    Set<String> setAuditTrailCreatedByName = new Set<String>{'Gautam Shah','Judith Randall', 'Informatica Cloud', 'Namita Pai', 'Ramya Purohit', 'Tapas Chakraborty'};
    public AuditTrailReport()
    {
        lstAuditTrail = new List<SetupAuditTrail>();
    }
    
    public void init()
    {
        for (SetupAuditTrail objAuditTrail : [SELECT Action, Section, DelegateUser, CreatedDate, CreatedBy.Name
                                                FROM SetupAuditTrail
                                               WHERE CreatedDate = LAST_N_DAYS:7])
        {       
            if((!setAuditTrailSection.contains(objAuditTrail.Section)) && (!setAuditTrailAction.contains(objAuditTrail.Action)) && (!setAuditTrailCreatedByName.contains(objAuditTrail.CreatedBy.Name)) ){
                lstAuditTrail.add(objAuditTrail);
            }
        }
    }
}