public with sharing class JP_IntimacyTriggerHandler{
     /****************************************************************************************
     * Name    : JP_IntimacyTriggerHandler 
     * Author  : Hiroko Kambayashi
     * Date    : 10/17/2012 
     * Purpose : Trigger Handler to update Intimacy Information byIntimacy record.
     *           
     * Dependencies: JP_Intimacy__c Object
     *             
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 
     *
     *****************************************************************************************/
    private Boolean isExecuting = false;
    private Integer batchSize = 0;
    
    /**
     * JP_IntimacyTriggerHandler Constructor
     * @param Boolean isExecute
     * @param Integer size
     */
    public JP_IntimacyTriggerHandler(Boolean isExecute, Integer size){
        this.isExecuting = isExecute;
        this.batchSize = size;
    }
    
    /**
     * Handling of JP_Intimacy AfterInsert Trigger
     * @param triggerNewList   New Intimacy handled in  Trigger.new
     * @return none
     */
    public void onAfterInsert(List<JP_Intimacy__c> triggerNewList){
        
        JP_IntimacyBusiness business = new JP_IntimacyBusiness();
        Map<String, String> intimacyUniqueMap = new Map<String, String>();
        //Added 7/10/2013 Kan Hayashi
        List<ID> SRlist = new List<ID>();
        Set<ID> SRset = new Set<ID>();
        
        for(JP_Intimacy__c newIntm : triggerNewList){
            if(newIntm.JP_SR__c != null){
                intimacyUniqueMap.put(newIntm.JP_Contacts__c + '|' + newIntm.JP_SR__c, newIntm.JP_Intimacy__c);
                //Added 7/10/2013 Kan Hayashi
                SRset.add(newIntm.JP_SR__c);
            }
        }
        if(!intimacyUniqueMap.isEmpty()){
            //Added 7/10/2013 Kan Hayashi
            SRlist.addAll(SRset);
            
            business.inputIntimacyToOpportunity(intimacyUniqueMap, SRlist);
        }
    }
    
    /**
     * Handling of JP_Intimacy AfterUpdate Trigger
     * @param triggerOldMap   Intimacy before update handled in Trigger.oldMap
     * @param triggerNewList  New Intimacy handled in Trigger.new
     * @return none
     */
    public void onAfterUpdate(Map<Id, JP_Intimacy__c> triggerOldMap, List<JP_Intimacy__c> triggerNewList){
        
        JP_IntimacyBusiness business = new JP_IntimacyBusiness();
        Map<String, String> intimacyUniqueMap = new Map<String, String>();
        //Added 7/10/2013 Kan Hayashi
        List<ID> SRlist = new List<ID>();
        Set<ID> SRset = new Set<ID>();
        
        for(JP_Intimacy__c newIntm : triggerNewList){
            JP_Intimacy__c oldIntm = triggerOldMap.get(newIntm.Id);
            if(oldIntm.JP_Intimacy__c != newIntm.JP_Intimacy__c || oldIntm.JP_SR__c != newIntm.JP_SR__c){
                intimacyUniqueMap.put(newIntm.JP_Contacts__c + '|' + newIntm.JP_SR__c, newIntm.JP_Intimacy__c);
                //Added 7/10/2013 Kan Hayashi
                SRset.add(newIntm.JP_SR__c);
            }
            if(newIntm.JP_SR__c == null || oldIntm.JP_SR__c != newIntm.JP_SR__c){
                intimacyUniqueMap.put(oldIntm.JP_Contacts__c + '|' + oldIntm.JP_SR__c, null);
                //Added 7/10/2013 Kan Hayashi
                SRset.add(oldIntm.JP_SR__c);
            }
        }
        if(!intimacyUniqueMap.isEmpty()){
            //Added 7/10/2013 Kan Hayashi
            SRlist.addAll(SRset);
            
            business.inputIntimacyToOpportunity(intimacyUniqueMap, SRlist);
        }
    }
    
    /**
     * Handling of JP_Intimacy AfterDelete Trigger
     * @param triggerOldList  Old Intimacy handled in Trigger.old
     * @return none
     */
    public void onAfterDelete(List<JP_Intimacy__c> triggerOldList){
        
        JP_IntimacyBusiness business = new JP_IntimacyBusiness();
        Map<String, String> intimacyUniqueMap = new Map<String, String>();
        //Added 7/10/2013 Kan Hayashi
        List<ID> SRlist = new List<ID>();
        Set<ID> SRset = new Set<ID>();
        
        for(JP_Intimacy__c oldIntm : triggerOldList){
            if((oldIntm.JP_Intimacy__c != null || oldIntm.JP_Intimacy__c != '') && oldIntm.JP_SR__c != null){
                intimacyUniqueMap.put(oldIntm.JP_Contacts__c + '|' + oldIntm.JP_SR__c, null);
                //Added 7/10/2013 Kan Hayashi
                SRset.add(oldIntm.JP_SR__c);
            }
        }
        if(!intimacyUniqueMap.isEmpty()){
            //Added 7/10/2013 Kan Hayashi
            SRlist.addAll(SRset);
            
            business.inputIntimacyToOpportunity(intimacyUniqueMap, SRlist);
        }
    }

}