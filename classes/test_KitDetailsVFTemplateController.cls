@isTest
private class test_KitDetailsVFTemplateController{
    static testMethod void myUnitTest() {
        //Setting the page reference to the first KIT__C  Page
        PageReference pageRef = new PageReference('/apex/KitDetailsVFTemplate');
        Test.setCurrentPage(pageRef);
         //Inserting The Kit Record 
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
       Opportunity o = new Opportunity(AccountId = a.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!');
        insert o; 
        Kit__c KitRecord = new Kit__c(Name='test',Pricing_Date__c=date.today(),Kit_Quantity_Ordered__c = 2,Account__c = a.id, Opportunity__c =o.id);
        ApexPages.StandardController sc = new ApexPages.standardController(KitRecord);
        //Initializing the Controller
        KitDetailsVFTemplateController myPageCon = new KitDetailsVFTemplateController(sc);
        
    }
}