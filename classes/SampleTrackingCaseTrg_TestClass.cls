@isTest
private class SampleTrackingCaseTrg_TestClass {


   static testMethod void TestSampleUpdate() {
   
   RecordType rt  = [SELECT Id FROM RecordType where SobjectType = 'Samples__c' and DeveloperName = 'ANZ_Sample' LIMIT 1];
   RecordType rt1 = [select Id, Name from Recordtype where name='US-Healthcare Facility' limit 1];
   Id pId = [SELECT Id FROM Profile where name = 'ANZ - All' LIMIT 1].Id;  
   

// Setup Data
        
        // New User
         User NewUser = new User();
         NewUser.LastName = 'ProductMgrTest';
         NewUser.Business_Unit__c = 'BU1';
         NewUser.Franchise__c = 'PM';
         NewUser.email = 'abc123@covidien.com';
         NewUser.alias = 'abc123';
         NewUser.username = 'abc123@covidien.com';
         NewUser.communityNickName = 'abc123@covidien.com';
         NewUser.ProfileId = pid;
         NewUser.CurrencyIsoCode='SGD'; 
         NewUser.EmailEncodingKey='ISO-8859-1';
         NewUser.TimeZoneSidKey='America/New_York';
         NewUser.LanguageLocaleKey='en_US';
         NewUser.LocaleSidKey ='en_US';
         NewUser.Country = 'SG';
         insert newUser;
        
        // New Account
        Account newAccount = new Account();
        newAccount.name= 'sampleTest1';
        newAccount.RecordTypeId = rt1.id;
        newAccount.BillingStreet = 'Test Billing Street';
        newAccount.BillingCity = 'Test Billing City';  
        newAccount.BillingState = 'Test';  
        newAccount.BillingPostalCode = '12345';
        insert newAccount;
        
        // New Contact
        Contact newContact = new Contact();
        newContact.Lastname ='Sample Contact Test';
        newContact.accountId = newAccount.id;
        insert newContact;
        
        // New Opportunity
        Opportunity NewOpp = new Opportunity();
        NewOpp.AccountId = newAccount.Id; 
        NewOpp.name = 'Test Opportunity 101'; 
        NewOpp.CloseDate = System.Today(); 
        NewOpp.StageName = 'Identify';
        insert NewOpp;
        
        // New Sample
        Samples__c s = new Samples__c();
        s.Name = 'New Sample Test123';
        s.Contact_Lookup__c = newContact.Id;
        s.Opportunity__c = newOpp.Id;
        s.Product_Manager__c = newUser.Id;
        s.Sample_Date__c = System.Today();
        s.Sample__c = true;
        s.Demo__c = false;
        insert s; 
    
    // Update Create Case Boolean to trigger After Update
    s.CreateCase__c = true;
    update(s);

    // Verify that Sample is updated with Case # by trigger
    Samples__c s2 = [SELECT Case__c FROM Samples__c WHERE Id = :s.Id];
    System.assertNotEquals(null, s2.Case__c); 
    System.Debug(s2.Case__c);
    
    }
}