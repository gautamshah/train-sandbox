global class ScheduleBatchCopyInTransit implements Schedulable{
    
    public Boolean testMode=false;
    String batchQuery;  
   global void execute(SchedulableContext sc) {
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
   // Added quantity_remaining__C to the Select statement on 8-14 by srinivas
        batchQuery='select  name, quantity_remaining__c,Distributor_Name__c,(select Id from Sales_In_Ack__r where Cycle_Period_dist__r.Cycle_Period_Reference__r.Month__c=\''+CurrentCyclePeriodMonth+'\' and Cycle_Period_dist__r.Cycle_Period_Reference__r.Year__c=\''+CurrentCyclePeriodYear+'\') from sales_in__c where quantity_remaining__c > 0';
        batchQuery += this.Testmode ? ' LIMIT 200':'';
        System.debug(batchQuery);
      BatchCopyInTransit b = new BatchCopyInTransit(batchQuery); 
      database.executebatch(b);
   }

}