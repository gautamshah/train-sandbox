/*

Change History
01/20/2016 Paul Berglund          Added ORDER BY Id DESC to the territory seletion
                                  to make sure the last one assigned is used
02/15/2016 Paul Berglund          Removed the intialization of the Territories Map
                                  in the declaration section because we are over-
                                  writing it in the Setup method.
02/18/2016 Paul Berglund          Split the validRecordType logic into two
                                  methods:
                                  - ifLockedRecordType
                                  - recordTypeRequiresApprovers
                                  Orignally, the code wad clearing the approver
                                  fields if the record type name ended in 'Locked'
                                  It should have just ignored those types completely
02/19/2016 Paul Berglund          RMS needs to be able to do 3 level approvers for
                                  Tom Sinclair (skip ZVP).  The ZVP territory still
                                  exists, but there is no one assigned to it. Going
                                  to just ignore any 'Assigned Users Not Found' errors
                                  on the ZVP territories
                                  Cleared some of the static variables
                                  Refactored some of the layout so it would stay inside 132 columns
02/21/2016 Paul Berglund          Completely refactored how the Territories are setup
                                  - Starting at the top level and working down now
                                  - All territories are included in static setup
                                    so they are only done one time
                                  - All users associated with the territories are
                                    assigned one time, then the users assigned to
                                    Proposals & Agreements (Owner, RSM, ZVP, SVP) and
                                    the user running the code are added incrementally
                                  - Although this increases the number of Territories,
                                    we are not resetting the list for each batch
                                    of records
02/22/2016 Paul Berglund          Made Top Level Hierarchy Name populate from CPQ Config
                                  Setting                                    

     How to use this class:
     
     Proposal trigger: CPQApprovers.process(proposal trigger context variables);
     Agreement trigger: CPQApprovers.process(agreement trigger context variables);
     
     Class Methods & Properties and their descriptions
     CPQApprovers
     - Methods
         CreateId - This converts an Integer into an Id by left-padding with 0's.
                    I use this to generate dummy Ids in the BuildNewMap method
                    so I can match the original new sObjects with the processed
                    objects.
         process - This overloaded method takes either Proposal trigger context
                   variables or Agreement trigger context varibles and converts
                   them to myTrigger subclasses so they can be processed by
                   the myTrigger and Approver classes.
         BuildNewMap - This overloaded method takes either a Proposal trigger
                       new List or Agreement trigger new List and creates a
                       new Map of the values for when the class is called
                       during the Before Insert stage.  It combines dummy
                       Ids with each sObject so it can be matched with the
                       results of the DoWork methods.
         isIgnore - Determines if the user belongs to a Profile designated as
                    an "Ignore"
         getProfile - Converts the user's Profile Name into a Profile enum
      - Properties
      
      myTrigger
      
      ProposalTrigger
      - Constructors
      - Methods
      - Properties
      
      AgreementTrigger
      - Constructors
      - Methods
      - Properties
      
      
      Approvers
      
      ProposalApprovers
      
      AgreementApprovers
      
     
*/
public with sharing class CPQApprovers
{
}