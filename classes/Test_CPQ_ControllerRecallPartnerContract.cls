/**
Test Class

CPQ_ControllerRecallPartnerContract

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-05      Yuli Fintescu       Created
2016-11-02      Isaac Lewis         Added cpqConfigSetting_cTest initialization
===============================================================================
*/
@isTest
private class Test_CPQ_ControllerRecallPartnerContract {

    static testMethod void myUnitTest() {

		//////insert cpqConfigSetting_cTest.create();
		//////cpqConfigSetting_c.reload();

        Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Status__c = 'Activated');
        insert testAgreement;

        Apttus__APTS_Agreement__c agreement = [Select AgreementId__c, Apttus__Status_Category__c From Apttus__APTS_Agreement__c Where ID =: testAgreement.ID];

		Test.setMock(WebServiceMock.class, new Test_CPQ_WebSvcCalloutMock());

		Test.startTest();
			PageReference pageRef = Page.CPQ_RecallPartnerContract;
	        Test.setCurrentPage(pageRef);

			CPQ_Controller_RecallPartnerContract p = new CPQ_Controller_RecallPartnerContract(new ApexPages.StandardController(agreement));
			p.onLoad();

			p.getHasError();

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'some error'));
			p.onLoad();

		Test.stopTest();
    }
}