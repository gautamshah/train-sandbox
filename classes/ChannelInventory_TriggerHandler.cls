/****************************************************************************************
 * Name    : ChannelInventory_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 15/10/2013 
 * Purpose : Contains all the logic coming from Channel Inventory trigger
 * Dependencies: ChannelInventoryTrigger Trigger
 *             , Channel Inventory Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
public with sharing class ChannelInventory_TriggerHandler {
	private boolean m_isExecuting = false;
    private integer BatchSize = 0;

	public ChannelInventory_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
 	public void OnBeforeInsert(List<Channel_Inventory__c> newObjects){
    	runValidations.CILookupValidations(newObjects);
 	}
 	
 	public void OnBeforeUpdate(List<Channel_Inventory__c> newObjects){
    	runValidations.CILookupValidations(newObjects);
    	for(Channel_Inventory__c ci : newObjects)
    	{
    		if(ci.Submitted__c && ((ci.Validation_Messages__c!=null && ci.Validation_Messages__c.trim()!='')
    							|| (ci.Error_Message__c!=null && ci.Error_Message__c.trim()!='')))
    			ci.Submitted__c = false;
    	}
 	}

 	public void OnBeforeDelete(List<Channel_Inventory__c> oldObjects){
 		
 		//ID adminProfileID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].ID;
 		List<Profile> lsProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
 		for(Channel_Inventory__c item : oldObjects)
 		{
 			
 			if(!item.Can_be_Deleted__c && (lsProfile.size() == 0 || userinfo.getProfileId() <> lsProfile[0].ID))
 			{
 				item.addError(Label.Cannot_delete_Channel_Inventory);
 			}
 			/*
 			else if(!item.Can_be_Deleted__c && userinfo.getProfileId() <> lsProfile[0].ID)
 			{
 				item.addError(Label.Cannot_delete_Channel_Inventory);
 			}
 			if(!item.Can_be_Deleted__c && userinfo.getProfileId() <> adminProfileID)
 			{
 				item.addError(Label.Cannot_delete_Channel_Inventory);
 			}
 			*/
 		}
 	}

}