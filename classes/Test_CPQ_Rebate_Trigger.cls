/*
Test class

CPQ_ProposalProcesses.UpdateRebateSelected()

CPQ_Rebate_After

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
10/31/2016   Paul Berglund   Remediated with CPQ
2016-11-01      Isaac Lewis         Added cpq_test.buildScenario_CPQ_ConfigSettings to createTestData
===============================================================================
*/
@isTest
private class Test_CPQ_Rebate_Trigger
{
    static List<Rebate__c> testRebates;
    static List<Apttus_Proposal__Proposal__c> testProposals;
    static Map<String,Id> mapRebateRType;

    static void createTestData()
    {
        //cpq_test.buildScenario_CPQ_ConfigSettings();
        System.debug('Line 25, Queries: ' + Limits.getQueries());

		insert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        CPQ_Approval_Retrigger_Field__c testRetrigger = new CPQ_Approval_Retrigger_Field__c(SObject__c = 'Proposal', Related_SObject__c = 'Rebate', Field_API_Name__c = 'Rebate_Frequency__c');
        insert testRetrigger;

        cpqProposal_t.executedMethods.add('capture');
        testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(Apttus_QPApprov__Approval_Status__c = 'Approved'),
            new Apttus_Proposal__Proposal__c(Apttus_QPApprov__Approval_Status__c = 'Approved')
        };
        insert testProposals;

        mapRebateRType = New Map<String,Id>();
        for(RecordType R: RecordType_u.fetch(Rebate__c.class).values())
        {
            mapRebateRType.Put(R.DeveloperName, R.Id);
        }
        
        System.debug('Line 46, Queries: ' + Limits.getQueries());

        testRebates = new List<Rebate__c>
        {
            new Rebate__c(Related_Proposal__c = testProposals[0].ID, RecordTypeID = mapRebateRType.get('Early_Signing_Incentive'), Early_Signing_Rebate_Percentage__c = 7, Early_Signing_Frequency__c = 'Anual', BIS_Compliance_Percentage__c = '75%', INVOS_Compliance_Percentage__c = '75%', Oximetry_Compliance_Percentage__c = '75%', Endobronchial_Compliance_Percentage__c = '75%', Tracheostomy_Compliance__c = '75%'),
            new Rebate__c(Related_Proposal__c = testProposals[1].ID, RecordTypeID = mapRebateRType.get('APIP_Rebate_Option_1_Customer_Loyalty'), Rebate_Frequency__c = 'Anual')
        };
        insert testRebates;
    }

    static testMethod void myUnitTest() {
        createTestData();

        System.debug('Line 61, Queries: ' + Limits.getQueries());

        Test.startTest();
            testProposals[0].Apttus_QPApprov__Approval_Status__c = 'Approved';
            testProposals[1].Apttus_QPApprov__Approval_Status__c = 'Approved';
            update testProposals;

            try {
                testRebates[0].RecordTypeID = mapRebateRType.get('APIP_Rebate_Option_2_High_Growth_Optio');
                update testRebates[0];
            } catch (Exception e) {}

            testRebates[1].RecordTypeID = mapRebateRType.get('APIP_Rebate_Option_2_High_Growth_Optio');
            update testRebates[1];

            testProposals[0].Apttus_QPApprov__Approval_Status__c = 'Approved';
            update testProposals[0];

            delete testRebates;

            testProposals[0].Apttus_QPApprov__Approval_Status__c = 'Approved';
            update testProposals[0];

            undelete testRebates;

            testProposals[0].Apttus_QPApprov__Approval_Status__c = 'Approved';
            update testProposals[0];

            for (Rebate__c r : testRebates) {
                r.Rebate_Threshold__c = 20;
            }
            update testRebates;

            for (Rebate__c r : testRebates) {
                r.Rebate_Frequency__c = 'Quarterly';
            }
            update testRebates;

        Test.stopTest();
    }

    static testMethod void TestDisabledTrigger() {
        try {
        CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Participating_Facility_After__c = true, CPQ_Product2_After__c = true, CPQ_Product2_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_ProductConfiguration_Before__c = true, CPQ_Proposal_After__c = true, CPQ_Proposal_Before__c = true, CPQ_Proposal_Distributor_After__c = true, CPQ_Rebate_After__c = true, CPQ_Rebate_Agreement_After__c = true);
        insert testTrigger;
        } catch (Exception e) {}

        createTestData();

        Test.startTest();
            testRebates[1].RecordTypeID = mapRebateRType.get('APIP_Rebate_Option_2_High_Growth_Optio');
            update testRebates[1];
        Test.stopTest();
    }
}