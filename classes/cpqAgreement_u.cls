public class cpqAgreement_u extends cpqDeal_u
{
    //
    // Apttus__APTS_Agreement__c
    //

	public List<Apttus__APTS_Agreement__c> sobjs
	{
		get { return (List<Apttus__APTS_Agreement__c>)super.get(); }
		set { super.set(value); }
	}

	public cpqAgreement_u()
	{
		super();
	}

 	public cpqAgreement_u(Set<Id> ids)
	{
		super(Apttus__APTS_Agreement__c.getSObjectType(), ' WHERE Id IN ' + ids);
	}

    public override Set<Id> extractOwners(List<sObject> newList)
    {
    	Set<Id> userIds = new Set<Id>();
        if (newlist != null && !newList.isEmpty())
	    	for(Apttus__APTS_Agreement__c p : (List<Apttus__APTS_Agreement__c>)newList)
    			userIds.add(p.OwnerId);

    	return userIds;
    }

    public override Set<Id> extractApprovers(List<sObject> newList)
    {
    	Set<Id> userIds = new Set<Id>();
        if (newlist != null && !newList.isEmpty())
	    	for(Apttus__APTS_Agreement__c p : (List<Apttus__APTS_Agreement__c>)newList)
    		{
    			userIds.add(p.SellerApprover__c);
    			userIds.add(p.RSM_Approver__c);
	    		userIds.add(p.ZVP__c);
    			userIds.add(p.SVP_User__c);
    		}

    	return userIds;
    }
}