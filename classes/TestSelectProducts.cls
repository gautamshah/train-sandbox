/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestSelectProducts {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'mdttest2@mdttest.com',
        Country = 'US',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        
        Product_SKU__c sku = new Product_SKU__c(Name = 'test20131029', Country__c = 'US');
        insert sku;
        
        System.runAs ( portalAccountOwner1 )
        {
        	
        Test.startTest();
         PageReference pageRef= Page.SelectProducts;
	     Test.setCurrentPage(pageRef);
	     
	     SelectProductsForOrder spfo=new SelectProductsForOrder();
	     //SelectProductsForOrder.productWrapper pw=new SelectProductsForOrder.productWrapper();
	     spfo.searchString='test20131029';
	     ApexPages.StandardSetController std=spfo.setCon;
	     Pagereference pg=spfo.doSearch();
	     List<SelectProductsForOrder.productWrapper> lst=spfo.getProducts();
	     pg=spfo.getSelected();
	     pg=spfo.refresh();
	     pg=spfo.AddProductsToOrder();
	     pg=spfo.AddAndDone();
	     
        Test.stopTest();
        
        }
    }
}