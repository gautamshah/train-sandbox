@isTest
   private class Activities_MobileTest {
   static testMethod void TestPageFunctionality() {

    RecordType ce = [SELECT Id FROM RecordType where SobjectType = 'Contact' and RecordType.DeveloperName = 'Covidien_Employee' LIMIT 1];
    RecordType hf = [SELECT Id FROM RecordType where SobjectType = 'Account' AND RecordType.DeveloperName = 'US_Account_Hospital' LIMIT 1];
    RecordType rt = [SELECT Id FROM RecordType where SobjectType = 'Contact' and DeveloperName = 'Affiliated_Contact_US' LIMIT 1];  
    RecordType ct = [SELECT Id FROM RecordType where SobjectType = 'Account' AND RecordType.DeveloperName = 'Country_Accounts' LIMIT 1];
    
    String ReccType = [SELECT Id FROM RecordType where SobjectType = 'Event' LIMIT 1].Id;
    Integer PageLoadCount = 1;
    string Searchterm = 'Test';
       
       
    
       
    //Create Account
    Account act = new Account();
    act.RecordTypeId = hf.id;
    act.name='Test Account';
    act.billingPostalCode = '99999';
    act.billingCity = 'TestCity';
    act.billingState = 'MA'; 
    
    insert act;


       Contact con = new Contact();
       con.RecordTypeId = ce.id;
       con.FirstName = 'Bob';
       con.LastName = 'Smith';
       con.AccountId = act.id;
       
       insert con;   
       
       
       //Create New Opportunity
       Opportunity opp = new Opportunity();
          opp.name='Test Opp';
          opp.StageName = 'Identify';
          opp.Type = 'New Business';
          opp.AccountId = act.id; 
          opp.Closedate = System.Today(); 
       
       insert opp;       
       
       
       PageReference pageRef = Page.Activities_Mobile; 
        Test.setCurrentPageReference(pageRef);
        
        Activities_Mobile am = new Activities_Mobile(); 
        
        
               // Set Variable Value


        am.ActivityType = 'Business Trip';
        am.selectedAccountGuidId = act.id; 
        am.selectedContactId = con.id;
        am.selectedContactName = con.FirstName;
        am.selectedOppId = '';
        am.selectedOppName = '';
        am.ReccType = ReccType;
        am.selectedNotes = 'Test Note';
        am.IndexNum = 0;
        am.OppIndexNum = 0;
        am.searchTerm = 'Test';
       
        am.getActivityTypeList();
        am.getOppStage();
        am.searchMain();
        am.searchOpp();
        am.searchOppNoRedir();
        am.ReviseSearch();
        am.Refresh();
        am.processButtonClick();
        am.processOppSelection();
        am.CreateEvent();
       am.ActivityType = '';
       am.CreateEvent();
          }
       }