public with sharing class SalesHistory_AllController {

private Account a;
private string accId;
Public List<Sales_History__c> SalesHistoryAll{get;set;}
Public List<Sales_History__c> SalesHistorySS{get;set;}
Public List<Sales_History__c> SalesHistoryVT{get;set;}
Public List<Sales_History__c> SalesHistoryRMS{get;set;}
public SalesHistory_AllController(){}

public SalesHistory_AllController(ApexPages.StandardController controller) {
    
    this.a = (Account)controller.getRecord();
    accId = a.id;
    //SalesHistoryAll = [Select Id, Name, Product_Group__c, Objective__c, Product_Sub_Group__c, SKU__c, Business_Unit__c, Source__c, InvoiceDate__c, Year_1_Sales_c__c from Sales_History__c where Customer__c=:accId];
    SalesHistorySS =  [Select Id, Name, Product_Group__c, Objective__c, Product_Sub_Group__c, SKU__c, 
                       Total_Prior_Year_Eaches__c, Total_Prior_Year_Sales__c, Supplier_Debtor__c from Sales_History__c 
                       where Customer__c=:accId and Business_Unit__c = 'SS'];
                        
    SalesHistoryVT =  [Select Id, Name, PRODUCT_LEVEL1__c, PRODUCT_LEVEL1_DESCRIPTION__c, Total_Current_Year_Sales__c, Total_Prior_Year_Sales__c, 
                      Variance__c, Percent_Sales_Variance__c, Total_Current_Year_Eaches__c, Total_Prior_Year_Eaches__c from Sales_History__c 
                      where Customer__c=:accId and Business_Unit__c = 'VT'];
                                      
    SalesHistoryRMS =  [Select Id, Name from Sales_History__c where Customer__c=:accId and Business_Unit__c = 'RMS'];
    }
  

}