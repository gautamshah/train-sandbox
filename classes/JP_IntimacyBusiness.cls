public without sharing class JP_IntimacyBusiness{
     /****************************************************************************************
     * Name    : JP_IntimacyBusiness 
     * Author  : Hiroko Kambayashi
     * Date    : 10/17/2012 
     * Purpose : Business logic to input Intimacy Information by searching KeyDoctor  
     *           in JP_Intimacy__c record when JP_Intimacy__c record id insertted, updated, or deleted.
     *           
     * Dependencies: Opportunity Object
     *             , JP_Intimacy__c Object
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 
     *
     *****************************************************************************************/
     
    /*
     * Update Field JP_Intimacy__c of Opportunity in a record selected by JP_Contacts__c and JP_SR__c of JP_Intimacy__c object
     * @param  uniqueIntimacyMap  The map which has JP_Contacts__c and JP_SR__c as key, and JP_Intimacy__c as value
     * @return none
     */
    public void inputIntimacyToOpportunity(Map<String, String> uniqueIntimacyMap, List<ID> SRlist){
        //Opportunity List for update
        List<Opportunity> updOppList = new List<Opportunity>();
        //Extract condition of RecordType on Opportunity
        String recordTypeName = System.Label.JP_Opportunity_RecordTypeName_Prefix + '%';

        // ADDED 6/22/2013 Kan Hayashi        
        // MODIFIED 7/10/2013 Kan Hayashi
        list<RecordType> opp_RecordType = [SELECT Id 
                                             FROM RecordType 
                                            WHERE SobjectType = 'opportunity' 
                                              AND developerName LIKE :recordTypeName
                                              // ADDED 7/10/2013 Kan Hayashi
                                              AND (NOT developerName LIKE '%Base%')
                                          ];
        list<Id> opp_RecordTypeId = new list<Id>();
        for(RecordType rt : opp_RecordType) {
            opp_RecordTypeId.add(rt.id);
        }

        for(Opportunity opp : [SELECT Id, 
                                      JP_Intimacy__c, 
                                      JP_Key_Dr_Owner__c 
                                 FROM Opportunity 
                                WHERE JP_Key_Dr_Owner__c IN : uniqueIntimacyMap.keyset() 
                                  AND JP_Key_Dr__c != null 
                                  //AND JP_RecordTypeName_Formula__c like : recordTypeName
                                  // MODIFIED 6/22/2013 Kan Hayashi        
                                  //AND JP_RecordType_Name__c like : recordTypeName
                                  AND RecordTypeId IN :opp_RecordTypeId
                                  // ADDED 7/10/2013 Kan Hayashi
                                  AND OwnerId IN : SRlist
                                  ]){

            String intimacyValue = uniqueIntimacyMap.get(opp.JP_Key_Dr_Owner__c);
            if(intimacyValue == null || intimacyValue == ''){
                Opportunity updOpp = new Opportunity(Id = opp.Id, JP_Intimacy__c = System.Label.JP_Opportunity_Intimacy_Nothing);
                updOppList.add(updOpp);
            } else {
                Opportunity updOpp = new Opportunity(Id = opp.Id, JP_Intimacy__c = intimacyValue);
                updOppList.add(updOpp);
            }
        }
        if(!updOppList.isEmpty()){
            update updOppList;
        }
    }
}