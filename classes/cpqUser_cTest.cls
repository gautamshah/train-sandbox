@isTest
public class cpqUser_cTest
{
	@isTest
	static void currentUser()
	{
		User u = cpqUser_c.CurrentUser;
		
		User u2 = cpqUser_c.fetch(UserInfo.getUserId());
		
		system.assertEquals(u, u2);
	}

	@isTest
	static void validUser()
	{
		Map<Id, User> validUsers = cpqUser_u.fetchReps();
		system.assert(validUsers.size()>0);
	}
}