public class VFPageviewTracker {
    
    public final VF_Tracking__c vft {get; set;}
    
    public VFPageviewTracker() { } // This can be used by actions of other
                                   // controllers and extensions
                                   
    public VFPageviewTracker(ApexPages.StandardController controller) {
            vft = (VF_Tracking__c)controller.getRecord();
        }
    
    //used for urls that do not end in a tab name
    
    public pageReference trackVisits(){
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1];       
        User mgr = [select id, name, Manager.id from User where id = :UserInfo.getUserId()];
        VF_Tracking__c newTracking = new VF_Tracking__c(
            User__c = UserInfo.getUserId(),
            Manager__c = mgr.ManagerId,
            VF_Page_Visited__c = strurl,
            Date_Time_Visited__c = datetime.now());
        insert newTracking;
        return null;
    }
    
    //used for urls that DO end in a tab name
    
    public pageReference trackVisits2(){
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1];
        Integer index = strurl.indexOf('?');
        if (index > 0) {
            strurl = strurl.substring(0, index);
        }
        User mgr = [select id, name, Manager.id from User where id = :UserInfo.getUserId()];
        VF_Tracking__c newTracking = new VF_Tracking__c(
            User__c = UserInfo.getUserId(),
            Manager__c = mgr.ManagerId,
            VF_Page_Visited__c = strurl,
            Date_Time_Visited__c = datetime.now());
        insert newTracking;
        return null;
    }
    
    //  copy paste within <apex: page> tag to implement for individual Visualforce page
    //  standardController="VF_Tracking__c" extensions="VFPageviewTracker" action="{!trackVisits}"
}