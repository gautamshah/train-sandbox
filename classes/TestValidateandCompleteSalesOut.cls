@isTest
private class TestValidateandCompleteSalesOut {

static testMethod void myUnitTest() {
 // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        Account acc=new Account(Account_External_ID__c = '123',Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',BillingCountry='SG',Country__c = 'CN',Submission_Offset_Day__c=null,Co_operative__c = true);
        insert acc;
        
        RecordType crt=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        Contact c = new Contact(RecordTypeID = crt.id, AccountId = acc.Id,Type__c = 'DIS Contact', Email='test@covidien.com', LastName='test');
        insert c;
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;
        
   //  this should be commented out as this is will be created by the batch job
        Cycle_Period__c cp=new Cycle_Period__c(Sales_out_submission_date__c = dt,Cycle_Period_Reference__c=AdvCpRef.Id,Distributor_Name__c=acc.Id);
        insert cp;
        
         Sales_Out__c so=new Sales_Out__c(Submitted__c = true,Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
       
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Submitted__c = true,Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        List<Sales_Out__c> lstSalesout = new List<Sales_Out__c>();
        lstSalesout.add(so); 
        lstSalesout.add(so1);
        Date myDate = date.valueOf('2013-07-19');
        system.debug(PrevCpRef.Id);
        Country_Calendar__c ccal = new Country_Calendar__c(Cycle_Period_Reference__c=PrevCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal;
        Country_Calendar__c ccal1 = new Country_Calendar__c(Cycle_Period_Reference__c=AdvCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal1;
        Country_Calendar__c ccal2 = new Country_Calendar__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal2;
                Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        insert ps;
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('Validate', 'Y');
        ApexPages.currentPage().getParameters().put('Id', cp.id);   
        
        ApexPages.StandardController ctlr = new ApexPages.StandardController(cp);
        ValidateandCompleteSalesOut ctl = new ValidateandCompleteSalesOut(ctlr);
        ctl.ChecktIfvalidate ='N';
        ctl.CyclePeriodId = cp.id;
        ctl.validateAndComplete();
        ValidateandCompleteSalesOut.runValidations(lstSalesout);
        ValidateandCompleteSalesOut.runCycleValidations(lstSalesout);
        ctl.getItems();
        Test.stopTest();
        
}

static testMethod void myUnitTest1() {
 // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        
        Account acc=new Account(Account_SAP_ID__c = '123',Account_External_ID__c = '123',Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',BillingCountry='SG',Country__c = 'CN',Submission_Offset_Day__c=null,Co_operative__c = true);
        insert acc;
        
        RecordType crt=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        Contact c = new Contact(RecordTypeID = crt.id, AccountId = acc.Id,Type__c = 'DIS Contact', Email='test@covidien.com', LastName='test');
        insert c;
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;

   //  this should be commented out as this is will be created by the batch job
        Cycle_Period__c cp=new Cycle_Period__c( Cycle_Period_Reference__c=AdvCpRef.Id,Distributor_Name__c=acc.Id);
        insert cp;
        List<Cycle_Period__c> lstcp = new List<Cycle_Period__c>();
        lstcp.add(cp);

         Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
       // insert si;
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        List<Sales_Out__c> lstSalesout = new List<Sales_Out__c>();
        lstSalesout.add(so); 
        lstSalesout.add(so1);
        Date myDate = date.valueOf('2013-07-19');
        system.debug(PrevCpRef.Id);
        Country_Calendar__c ccal = new Country_Calendar__c(Cycle_Period_Reference__c=PrevCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal;
        Country_Calendar__c ccal1 = new Country_Calendar__c(Cycle_Period_Reference__c=AdvCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal1;
        Country_Calendar__c ccal2 = new Country_Calendar__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal2;
                Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',SKUSAPID__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        insert ps;
        Test.startTest();
        ApexPages.StandardController ctlr = new ApexPages.StandardController(cp);
        ValidateandCompleteSalesOut ctl = new ValidateandCompleteSalesOut(ctlr);
        ApexPages.currentPage().getParameters().put('Validate', 'Y');
        ApexPages.currentPage().getParameters().put('Id', cp.id);        
        ctl.ChecktIfvalidate ='Y';
        //ctl.lstPendingCyclePeriod = lstcp;
        ctl.CyclePeriodId = cp.id;
        ctl.validateAndComplete();
      //  ctl.getItems();
        ValidateandCompleteSalesOut.runValidations(lstSalesout);
        ValidateandCompleteSalesOut.runCycleValidations(lstSalesout);
       // ctl.getItems();
        Test.stopTest();
        
}
static testMethod void myUnitTest3() {
 // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        
        Account acc=new Account(Account_External_ID__c = '123',Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',BillingCountry='SG',Country__c = 'CN',Submission_Offset_Day__c=null,Co_operative__c = true);
        insert acc;
        
        RecordType crt=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        Contact c = new Contact(RecordTypeID = crt.id, AccountId = acc.Id,Type__c = 'DIS Contact', Email='test@covidien.com', LastName='test');
        insert c;
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;

   //  this should be commented out as this is will be created by the batch job
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=AdvCpRef.Id,Distributor_Name__c=acc.Id);
        insert cp;

         Sales_Out__c so=new Sales_Out__c(Submitted__c = true,Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
       // insert si;
        insert so;
        Sales_Out__c so1=new Sales_Out__c( Submitted__c = true,Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        List<Sales_Out__c> lstSalesout = new List<Sales_Out__c>();
        lstSalesout.add(so); 
        lstSalesout.add(so1);
        Date myDate = date.valueOf('2013-07-19');
        system.debug(PrevCpRef.Id);
        Country_Calendar__c ccal = new Country_Calendar__c(Cycle_Period_Reference__c=PrevCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal;
        Country_Calendar__c ccal1 = new Country_Calendar__c(Cycle_Period_Reference__c=AdvCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal1;
        Country_Calendar__c ccal2 = new Country_Calendar__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Country__C='SG',submission_date__C=myDate);
        insert ccal2;
        Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        insert ps;
        Test.startTest();
        //ApexPages.StandardController ctlr = new ApexPages.StandardController(cp);
        ValidateandCompleteSalesOut ctl = new ValidateandCompleteSalesOut();
        ApexPages.currentPage().getParameters().put('Validate', 'Y');
         ctl.ChecktIfvalidate ='Y';
       List<Cycle_Period__c> lstcp = new List<Cycle_Period__c>();
        lstcp.add(cp);
       // ctl.lstPendingCyclePeriod = lstcp;
        ctl.CyclePeriodId = cp.id;
        ctl.validateAndComplete();
      //  ctl.getItems();
        ValidateandCompleteSalesOut.runValidations(lstSalesout);
        ValidateandCompleteSalesOut.runCycleValidations(lstSalesout);
       // ctl.getItems();
        Test.stopTest();
        
}
}