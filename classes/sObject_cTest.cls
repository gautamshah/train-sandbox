/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYY.MM.DD   A    PAB        CPR-000    Updated comments section
2017.01.??   A    PAB        AV-280     Created. To increase coverage on dependencies related to AV-280.
2017.01.19   A    IL         AV-280     Added comment header.

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

@isTest
public class sObject_cTest
{
    public class Concrete extends sObject_c
    {
		public Concrete()
		{
			super();
		}
        
        public Concrete(sObjectType sObjType)
        {
        	super(sObjType);
        }
        
//        public Concrete(sObjectType sObjType, Id recordTypeId)
//        {
//        	system.assert(false, sObjType);
        	//super(sObjType, recordTypeId);
//        }
        
        public Concrete(sObjectType sObjType, boolean loadDefaults)
        {
        	super(sObjType, loadDefaults);
        }
        
        public Concrete(sObjectType sObjType, Map<sObjectField, object> fields)
        {
        	super(sObjType, fields);
        }
        
        public Concrete(type classType, Id recordTypeId, boolean loadDefaults)
        {
        	super(classType, recordTypeId, loadDefaults);
        }
        
        public Concrete(sObjectType sObjType, Id recordTypeId, boolean loadDefaults)
        {
        	super(sObjType, recordTypeId, loadDefaults);
        }
        
        public Concrete(sObjectType sObjType, string name)
        {
        	super(sObjType, name);
        }
        
        public Concrete(sObjectType sObjType, Id recordTypeId)
        {
        	super(sObjType, recordTypeId);
        }
        
        public Concrete(sObject sobj)
        {
            super(sobj);
        }
        
        public Concrete(Id id)
        {
        	super(id);
        }
        
		// Leveraging the Account sObject for testing
        public Account sobj
        {
        	get { return (Account)this.get(); }
        }
    }

    static Concrete concrete; // Used by setup and each test method

    // This should be moved to the Account_c class
    static Account createAccount(integer id, string name, string accountNumber, string site)
    {
        Account a = new Account();
        a.Id = sObject_c.dummyId(Account.class, id);
        a.Name = name;
        a.AccountNumber = accountNumber;
        a.Site = site;

        return a;
    }

    static User createUser(integer id, string lastName, string alias)
    {
        User u = new User();
        u.Id = sObject_c.dummyId(User.class, id);
        u.LastName = lastName;
        u.Alias = alias;

        return u;
    }

    @isTest
    static void contructors()
    {
        // Test null constructor
/*
		try
		{
			new Concrete();
            system.assert(false, 'This class requires an sObject');
		}
		catch(Exception e)
		{
            System.assertEquals(sObject_c.sObjectRequiredMessage, e.getMessage());
		}
		
        try
        {
            new Concrete((sObject)null);
            system.assert(false, 'This class requires an sObject');
        }
        catch (Exception e)
        {
            System.assertEquals(sObject_c.sObjectRequiredMessage, e.getMessage());
        }

		concrete = new Concrete(Account.sObjectType);
		system.assertEquals(Account.sObjectType, concrete.sobj.getSObjectType(), 'Should be the same');
*/		
		RecordType rt =
			[SELECT Id
			 FROM RecordType
			 WHERE DeveloperName = 'Master_Account_Record_Type' AND
			       sObjectType = 'Account'];
		
		concrete = new Concrete(Account.sObjectType, rt.Id);
		concrete.put(Account.field.Name, 'default');
		concrete.save();
		Concrete fetched = new Concrete(concrete.Id);
		system.assertEquals(concrete.Id, fetched.Id, 'Should be equal');
		
		
        // Test query constructor
        String testName = 'Test Name';
        Account accSO = cpqAccount_TestSetup.generateAccounts(1).get(0);
        accSO.Name = testName;
        insert accSO;
        // TODO: Automatically provide space between clauses in SOQL so we dont get `FROM AccountWHERE Name` errors
        String queryString = ' WHERE Name = \''+testName+'\'';
        Concrete accSOC_query = new Concrete(Schema.Account.sObjectType, testName);
        System.assertEquals(accSO.Id, accSOC_query.Id);

        // Test ID constructor
        Concrete accSOC_Id = new Concrete(accSO.Id);
        System.assertEquals(accSO.Id, accSOC_Id.Id);

        // Test main constructor
        try
        {
            Concrete accSOC = new Concrete(accSO);
            System.assertNotEquals(null, accSOC.Id, 'Main constructor did not instantiate correctly');
        }
        catch (Exception e) {
            System.assert(false, 'Main constructor did not instantiate correctly');
        }


    }

    @isTest
    static void properties()
    {
        Id testId = Id.ValueOf('001000000000001'); // Dummy Account Id, 001 is universal
        string testName = 'Test Name';

        concrete = new Concrete((sObject)new Account());
        concrete.Id = testId;
        concrete.Name = testName;

        system.assertEquals(testId, concrete.Id, 'Id should be identical');
        system.assertEquals(testName, concrete.Name, 'Name should be identical');
    }

    //@isTest
    //static void descriptionMethods()
    //{
    //    concrete = new Concrete((sObject)new Account());
    //    system.assertEquals('Account', concrete.sObjectDescribe.getName(), 'The name of an Account object is Account');
    //    system.assertEquals('001', concrete.sObjectDescribe.getKeyPrefix(), 'The keyPrefix of an Account object is alwasy 001');
    //}

    @isTest
    static void copy()
    {
        // Start with the Map (it's called by the List)
        Map<Schema.SObjectField, Schema.SObjectField> fieldMap = new Map<Schema.SObjectField, Schema.SObjectField>{
            Account.fields.Site => User.fields.Alias,
            Account.fields.Name => User.fields.LastName
        };

        Account a = createAccount(1, 'A', 'number A', 'site A');
        User u = createUser(1, null, 'alias');
        Id before = u.Id;

        sObject_c.copy(a, u, fieldMap);
        system.assertEquals(u.Alias, 'site A', 'The Site field was copied to the Alias field');
        system.assertEquals(u.Id, before, 'The Id field was no copied over');
        system.assertEquals(u.LastName, 'A', 'The Name field was copied to the LastName field');

        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Account.fields.Name,
            Account.fields.AccountNumber
        };

        Account b = createAccount(2, 'B', 'number B', 'site B');

        sObject_c.copy(a, b, fieldList);
        system.assertNotEquals(b.Id, a.Id, 'These should stay different');
        system.assertEquals(b.Site, 'site B', 'The Site field wasnt copied');
        system.assertEquals(b.Name, 'A', 'The Name field was copied');
        system.assertEquals(b.AccountNumber, 'number A', 'The Account Number field was copied');
    }

    @isTest
    static void copyDifferentTypes()
    {
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Account.fields.Name,
            Account.fields.AccountNumber
        };

        Account a = createAccount(2, 'B', 'number B', 'site B');
        User b = new User();

        try
        {
            sObject_c.copy(a, b, fieldList);
            system.assert(true, 'Should have thrown an exception');
        }
        catch (Exception ex)
        {
        }
    }

    @isTest
    static void equal()
    {
        // Start with the Map (it's called by the List)
        Map<Schema.SObjectField, Schema.SObjectField> fieldMap = new Map<Schema.SObjectField, Schema.SObjectField>{
            Account.fields.Site => User.fields.Alias,
            Account.fields.Name => User.fields.LastName
        };

        Account a = createAccount(1, 'A', 'number A', 'site A');
        User u = createUser(1, 'A', 'site A');

        boolean areEqual = sObject_c.equal(a, u, fieldMap);

        system.assertEquals(true, areEqual, 'These fields do match');

        u.LastName = 'B';

        areEqual = sObject_c.equal(a, u, fieldMap);

        system.assertNotEquals(true, areEqual, 'The Name fields should not match');

        Account b = createAccount(1, 'B', 'number B', 'site B');

        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Account.fields.Name,
            Account.fields.AccountNumber
        };

        areEqual = sObject_c.equal(a, b, fieldList);
        system.assertNotEquals(true, areEqual, 'They do not match');

        sObject_c.copy(a, b, fieldList);
        areEqual = sObject_c.equal(a, b, fieldList);
        system.assertEquals(true, areEqual, 'They should be equal after copying them');
    }

    @isTest
    static void equalDifferentTypes()
    {
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Account.fields.Name,
            Account.fields.AccountNumber
        };

        Account a = createAccount(2, 'B', 'number B', 'site B');
        User b = new User();

        try
        {
            sObject_c.equal(a, b, fieldList);
            system.assert(true, 'Should have thrown an exception - comparing a List of fields requires same object');
        }
        catch (Exception ex)
        {
        }
    }

    @isTest
    static void save()
    {
        // Test query constructor
        String testName = 'Test Name';
        Account accSO = cpqAccount_TestSetup.generateAccounts(1).get(0);
        accSO.Name = testName;

        Concrete accSOC = new Concrete(accSO);
        try
        {
            List<String> errors = accSOC.save();
            System.assertEquals(0, errors.size());
            System.assertNotEquals(null, accSOC.Id);
        }
        catch (Exception e)
        {
            System.assert(false, 'Record did not save correctly');
        }

        delete [SELECT Id FROM Account WHERE Id = :accSOC.Id];

        try
        {
            List<String> errors = accSOC.save();
            System.assertNotEquals(0, errors.size());
        }
        catch (Exception e)
        {
            System.assert(false, 'Save method did not catch error');
        }
    }

    @isTest
    static void fetch()
    {
        String testName = 'Test Name';
        String wrongName = 'Wrong Name';
        Account accSO = cpqAccount_TestSetup.generateAccounts(1).get(0);
        accSO.Name = testName;
        insert accSO;

        // Test fetch by name
        Account accByName = (Account) sObject_c.fetch(Schema.Account.getSObjectType(),accSO.Name);
        System.assertEquals(accSO.Id, accByName.Id);

        // Test fetch by wrong name
        Account accByWrongName = (Account) sObject_c.fetch(Schema.Account.getSObjectType(),wrongName);
        System.assertEquals(null, accByWrongName);

        // Test fetch by Id
        Account accById = (Account) sObject_c.fetch(Schema.Account.getSObjectType(),accSO.Id);
        System.assertEquals(accSO.Id, accById.Id);
    }

    @isTest
    static void oldVersion()
    {
        List<Account> accs = cpqAccount_TestSetup.generateAccounts(2);
        insert accs;
        Map<Id,Account> accMap = new Map<Id,Account>(accs);

        // Test null
        System.assertEquals(null, sObject_c.oldVersion(null,null));
        System.assertEquals(null, sObject_c.oldVersion(accs.get(0),null));

        // Test map with value;
        System.assertEquals(accs.get(0), sObject_c.oldVersion(accs.get(0),accMap));

        // Test map without value;
        accMap.remove(accs.get(0).Id);
        System.assertEquals(null, sObject_c.oldVersion(accs.get(0),accMap));
    }

    @isTest
    static void fieldChanged()
    {
        List<Account> accs = cpqAccount_TestSetup.generateAccounts(2);

        Account oldAcc = accs.get(0);
        oldAcc.Name = 'oldAcc';
        insert accs;

        Account newAcc = [SELECT Id, Name FROM Account WHERE Id = :oldAcc.Id];
        newAcc.Name = 'newAcc';

        Map<Id,Account> accMap = new Map<Id,Account>(accs);

        Schema.sObjectField accNameField = Account.field.Name;

        // Test field by value
        System.assertEquals(true, sObject_c.fieldChangedTo(newAcc,oldAcc,accNameField,newAcc.Name));
        System.assertEquals(false, sObject_c.fieldChangedTo(newAcc,oldAcc,accNameField,oldAcc.Name));

        // Test field
        System.assertEquals(true, sObject_c.fieldChanged(newAcc,oldAcc,accNameField));
        System.assertEquals(false, sObject_c.fieldChanged(newAcc,newAcc,accNameField));

        // Test list field by value
        System.assertEquals(true, sObject_c.fieldChangedTo(newAcc,accMap,accNameField,newAcc.Name));
        System.assertEquals(false, sObject_c.fieldChangedTo(newAcc,accMap,accNameField,oldAcc.Name));

        // Test list field
        System.assertEquals(true, sObject_c.fieldChanged(newAcc,accMap,accNameField));
        accMap.put(newAcc.Id,newAcc);
        System.assertEquals(false, sObject_c.fieldChanged(newAcc,accMap,accNameField));
    }

    @isTest
    static void sObjectDescribe()
    {
        Schema.DescribeSObjectResult result = sObject_c.getSObjectDescribe(Account.sObjectType);

        sObject_c.sObjectDescribe describeBySObjectType = new sObject_c.sObjectDescribe(Schema.Account.getSObjectType());
        System.assertEquals(result.getName(), describeBySObjectType.getName);
        System.assertEquals(result.getKeyPrefix(), describeBySObjectType.getKeyPrefix);

        sObject_c.sObjectDescribe describeByResult = new sObject_c.sObjectDescribe(result);
        System.assertEquals(result.getName(), describeByResult.getName);
        System.assertEquals(result.getKeyPrefix(), describeByResult.getKeyPrefix);
    }

    @isTest
    static void value()
    {
        String testName = 'Test Name';
        Account accSO = cpqAccount_TestSetup.generateAccounts(1).get(0);
        accSO.Name = testName;

        String newName = 'New Name';

        Schema.sObjectField accNameField = Account.field.Name;

        Concrete accSOC = new Concrete(accSO);
        accSOC.put(accNameField, newName);
        System.assertEquals(newName, accSOC.get(accNameField));
    }
}