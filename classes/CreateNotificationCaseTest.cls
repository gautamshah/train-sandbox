@isTest (SeeAllData=true)
private class CreateNotificationCaseTest {

    static testMethod void myUnitTest() {
        Test.startTest();
        
        list <User> asiaNonAdminUsers = new list <User>();
        asiaNonAdminUsers = [SELECT id FROM User WHERE Profile.name = 'Asia - CN' and isActive = true limit 1];
        system.debug('---non-Admin user-->'+asiaNonAdminUsers[0].id);
        if(asiaNonAdminUsers.size() > 0)
        {
            System.runAs(asiaNonAdminUsers[0])
            {
                list <user> newUserList = new list <user>();
                User newUser = new User();
                newUser.FirstName = 'new User';
                newUser.LastName = 'new User';
                newUser.Username = 'tester2088131025@test.com';
                newUser.Email = 'newUser@3dtest.com';
                newUser.Alias = 'test45';
                newUser.CommunityNickname = 'newUser';
                newUser.TimeZoneSidKey = 'America/New_York';
                newUser.EmailEncodingKey = 'UTF-8';
                newUser.LocaleSidKey = 'en_US';
                newUser.LanguageLocaleKey = 'en_US';
                newUser.Asia_Team_Asia_use_only__c = 'CRM Team';
                newUser.ProfileId = userinfo.getProfileId();
                newUserList.add(newUser);
                
                User newUser2 = new User();
                newUser2.FirstName = 'new User2';
                newUser2.LastName = 'new User2';
                newUser2.Username = 'tester20828131025@test.com';
                newUser2.Email = 'newUser2@3dtest.com';
                newUser2.Alias = 'test45';
                newUser2.CommunityNickname = 'newUser2';
                newUser2.TimeZoneSidKey = 'America/New_York';
                newUser2.EmailEncodingKey = 'UTF-8';
                newUser2.LocaleSidKey = 'en_US';
                newUser2.LanguageLocaleKey = 'en_US';
                newUser2.Asia_Team_Asia_use_only__c = 'CRM Team';
                newUser2.ProfileId = userinfo.getProfileId();
                newUserList.add(newUser2);
                
                insert newUserList;
                
            }
        }       
        Test.stopTest();
    }
}