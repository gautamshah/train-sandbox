public class cpqAll_c extends cpqClassOfTrade_c 
{
	
	   public cpqAll_c(String row)
	   {
	     super(row);
	   }

	private String buildExpressionInternal(Boolean exclude)
	{
		return exclude ? ' (Id = null) ' : ' (Id != null) ';
	}
	  public override  String buildParticipatingFacilityExpression(String obj)
      {
      	return buildExpressionInternal(this.exclude);
      }
  
	  public override String buildDistributorExpression(String obj)
      {
        return buildExpressionInternal(!this.exclude);
      }
    
}