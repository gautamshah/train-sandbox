public class cpqDeal_Type_c
{
    private static CPQ_Config_Setting__c SystemProperties = cpqConfigSetting_c.SystemProperties;

    private static Map<OrganizationNames_g.Abbreviations, Set<string>> dealTypesByOrg;

    // -----------------------------------------------------------------------------------------------------------------
    // Methods for working with deal types

    public static Set<String> getDealTypes () {
        Set<String> dealTypeSet = new Set<String>();
        for (RecordType rt : cpqDeal_RecordType_c.getRecordTypesById().values()) {
            dealTypeSet.add(getDealType(rt.DeveloperName));
        }
        return dealTypeSet;
    }

    public static Set<String> getDealTypesForSObject (Schema.SObjectType sObjectType) {
        Set<String> dealTypes = new Set<String>();
        for (String rtName : cpqDeal_RecordType_c.getRecordTypesBySObjectAndName().get(sObjectType).keySet()) {
            dealTypes.add(getDealType(rtName));
        }
        return dealTypes;
    }

    public static String getDealTypeFromRecordTypeId (String RecordTypeId) {
    	// system.debug('getDealTypeFromRecordTypeId: ' + RecordTypeId);
        return getDealType(cpqDeal_RecordType_c.getRecordTypesById().get(recordTypeId).DeveloperName);
    }

    public static String getDealTypeFromRecordTypeLabel (String recordTypeLabel) {
    	// system.debug('recordTypeLabel: ' + recordTypeLabel);
        for (RecordType rt : cpqDeal_RecordType_c.getRecordTypesById().values()) {
        	// system.debug('rt: ' + rt);

            if (getDealType(rt.Name) == getDealType(recordTypeLabel)) {
                return getDealType(rt.DeveloperName);
            }
        }
        return null;
    }

    public static Set<String> getDealTypesFromRecordTypeLabels (Set<String> recordTypeLabels) {
        Set<String> dealTypes = new Set<String>();
        for(String rtLabel : recordTypeLabels){
            dealTypes.add(getDealTypeFromRecordTypeLabel(rtLabel));
        }
        return dealTypes;
    }

    // TODO: Write test coverage
    public static String getDealType (sObject record) {
        if(record.getSObjectType() == cpq_u.PROPOSAL_SOBJECT_TYPE) {
            return getDealType( (Apttus_Proposal__Proposal__c) record );
        }
        if(record.getSObjectType() == cpq_u.AGREEMENT_SOBJECT_TYPE) {
            return getDealType( (Apttus__APTS_Agreement__c) record );
        }
        return null;
    }

    public static String getDealType(Apttus_Proposal__Proposal__c proposal) {

        String dealTypeName =
        	proposal.RecordType.DeveloperName == null ?
         	'Proposal' :
         	proposal.RecordType.DeveloperName;

        //every deal type can have Amendment to Add Facilities. the original record type counts
        if (dealTypeName.startsWith('Amendment_to_Add_Facilities'))
            dealTypeName = proposal.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName == null ?
            	'Proposal' :
            	proposal.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName;

        return getDealType(dealTypeName);

    }

    public static String getDealType(Apttus__APTS_Agreement__c agreement) {

        String dealTypeName = agreement.RecordType.DeveloperName == null ?
        	'Proposal' :
        	agreement.RecordType.DeveloperName;

        //every deal type can have Amendment to Add Facilities. the original record type counts
        if (dealTypeName.startsWith('Amendment_to_Add_Facilities'))
            dealTypeName = agreement.Apttus_QPComply__RelatedProposalId__r
                          		.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName == null ?
                          			'Proposal' :
                          			agreement.Apttus_QPComply__RelatedProposalId__r
                          				.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName;

        return getDealType(dealTypeName);

    }

    public static String getDealType (String recordTypeName) {
        // Uses regex to remove AG and Locked suffixes in Developer Names (works with Labels too)
        String DealTypeName = recordTypeName == null ? '' : recordTypeName;
        return dealTypeName.replaceAll(
        	cpqDeal_RecordType_c.REGEX_SUFFIX_LOCKED,'').replaceAll(cpqDeal_RecordType_c.REGEX_SUFFIX_AG,'');
    }

    public static Set<String> getDealTypesForOrg (OrganizationNames_g.Abbreviations org) {
        return getDealTypesByOrg().get(org);
    }

    public static Set<String> getDirectDealTypes()
    {
        // Returns deal types created directly as Agreements, not Proposals.
        return getDealTypesFromRecordTypeLabels(
        	cpqConfigSetting_c.getMultiPicklistSystemProperty(cpqConfigSetting_c.AGREEMENT_DIRECT));
    }

    public static Set<String> getDirectDealTypesForOrg (OrganizationNames_g.Abbreviations org)
    {
        // Returns deal types created directly as Agreements for a particular org.
        return DATATYPES.getSetInnerJoin(getDealTypesForOrg(org),getDirectDealTypes());
    }

    public static Set<String> getDealTypesAllowingTracingCustomer()
    {
        return getDealTypesFromRecordTypeLabels(
        	cpqConfigSetting_c.getMultiPicklistSystemProperty(cpqConfigSetting_c.ALLOWING_TRACING_CUSTOMERS));
    }

    public static Set<String> getDealTypesRequireOpportunityByAmount()
    {
        return getDealTypesFromRecordTypeLabels(
        	cpqConfigSetting_c.getMultiPicklistSystemProperty(cpqConfigSetting_c.REQUIRE_OPPORTUNITY_BY_AMOUNT));
    }

    public static Set<String> getDealTypesRequiringNoCart()
    {
        return getDealTypesFromRecordTypeLabels(
        	cpqConfigSetting_c.getMultiPicklistSystemProperty(cpqConfigSetting_c.REQUIRING_NO_CART));
    }

    public static Set<String> getDealTypesRequiringNoOpportunity()
    {
        return getDealTypesFromRecordTypeLabels(
        	cpqConfigSetting_c.getMultiPicklistSystemProperty(cpqConfigSetting_c.REQUIRING_NO_OPPORTUNITY));
    }

    public static Set<String> getDealTypesNotAllowingPriceAdjustment()
    {
        return getDealTypesFromRecordTypeLabels(
        	cpqConfigSetting_c.getMultiPicklistSystemProperty(cpqConfigSetting_c.NOT_ALLOWING_PRICE_ADJUSTMENT));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Methods for working with deal type system properties

    public static Map<OrganizationNames_g.Abbreviations, Set<String>> getDealTypesByOrg () {
		// system.debug('getDealTypesByOrg.dealTypesByOrg: ' + dealTypesByOrg);
        if (dealTypesByOrg == null) {
            dealTypesByOrg = new Map<OrganizationNames_g.Abbreviations, Set<String>>();
            for (Integer i = 0; i < OrganizationNames_g.Abbreviations.values().size(); i++) {
				// system.debug('OrganizationNames_g.Abbreviations.values()[i]: ' + OrganizationNames_g.Abbreviations.values()[i]);
				// system.debug('SystemProperties: ' + SystemProperties);
                // Set Org Name Key
                OrganizationNames_g.Abbreviations org = OrganizationNames_g.Abbreviations.values()[i];
                Set<String> recordTypeLabels = new Set<String>();

                // Populate Org Deal Type List
                if (org == OrganizationNames_g.Abbreviations.SSG) {
                    recordTypeLabels = DATATYPES.getMultiPicklistValueSet(SystemProperties.SSG_Deal_Types__c);
                }
                else if ( org == OrganizationNames_g.Abbreviations.RMS) {
                    recordTypeLabels = DATATYPES.getMultiPicklistValueSet(SystemProperties.RMS_Deal_Types__c);
                }

				// system.debug('recordTypeLabels: ' + recordTypeLabels);

                dealTypesByOrg.put(org,getDealTypesFromRecordTypeLabels(recordTypeLabels));

            }

        }
		system.debug('getDealTypesByOrg.dealTypesByOrg: ' + dealTypesByOrg);

        return dealTypesByOrg;
    }
}