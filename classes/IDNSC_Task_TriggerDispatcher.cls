/****************************************************************************************
* Name    : Class: IDNSC_Task_TriggerDispatcher
* Author  : Gautam Shah
* Date    : 8/12/2014
* Purpose : Dispatches to necessary classes when invoked by IDNSC_Task trigger
* 
* Dependancies: 
* 	Class: IDNSC_Task_Main
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
public with sharing class IDNSC_Task_TriggerDispatcher 
{
	public static Set<String> executedMethods = new Set<String>();
	
	public static void Main(List<IDNSC_Task__c> newList, Map<Id, IDNSC_Task__c> newMap, List<IDNSC_Task__c> oldList, Map<Id, IDNSC_Task__c> oldMap, Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isExecuting)
	{
		if(isAfter)
		{
			if(isInsert)
			{
				prep_UpdateDaysRemaining(newList,oldMap,isUpdate);
				//added 3/18/15
				prep_CalculateEstimatedStartDates(newList,isInsert);
			}
			else if(isUpdate)
			{
				prep_PostAssignmentToChatter(newList,oldMap);
				prep_PostCompletionToGroup(newList,oldMap);
				//added 3/18/15
				prep_CalculateEstimatedStartDates(newList,isInsert);
				//added 3/2/15 as "Go-Live Date - Projected" work around
				if(!executedMethods.contains('lastTaskEstimatedCompletionDate'))
				{
					IDNSC_Task_Main.lastTaskEstimatedCompletionDate(newList);
				}
				prep_UpdateDaysRemaining(newList,oldMap,isUpdate);
			}
		}
	}
	
	private static void prep_PostAssignmentToChatter(List<IDNSC_Task__c> newList, Map<Id, IDNSC_Task__c> oldMap)
	{
		List<IDNSC_Task__c> updatedAssignments = new List<IDNSC_Task__c>();
		for(IDNSC_Task__c task : newList)
		{
			if(task.Date_Assigned__c != oldMap.get(task.Id).Date_Assigned__c)
			{
				updatedAssignments.add(task);
			}
		}
		IDNSC_Task_Main.postAssignmentToChatter(updatedAssignments);
	}
	
	private static void prep_PostCompletionToGroup(List<IDNSC_Task__c> newList, Map<Id, IDNSC_Task__c> oldMap)
	{
		List<IDNSC_Task__c> completedAssignments = new List<IDNSC_Task__c>();
		for(IDNSC_Task__c task : newList)
		{
			if(task.Status__c == 'Completed' && task.Status__c != oldMap.get(task.Id).Status__c)
			{
				completedAssignments.add(task);
			}
		}
		IDNSC_Task_Main.postCompletionToGroup(completedAssignments);
	}
	
	private static void prep_UpdateDaysRemaining(List<IDNSC_Task__c> newList, Map<Id, IDNSC_Task__c> oldMap, Boolean isUpdate)
	{
		if(!executedMethods.contains('updateDaysRemaining'))
		{
			List<IDNSC_Task__c> changedStatusTasks = new List<IDNSC_Task__c>();
			for(IDNSC_Task__c task : newList)
			{
				if(isUpdate)
				{
					if(task.Status__c != oldMap.get(task.Id).Status__c)
					{
						changedStatusTasks.add(task);
					}
				}
				else
				{
					changedStatusTasks.add(task);
				}
			}
			if(!changedStatusTasks.isEmpty())
			{
				IDNSC_Task_Main.updateDaysRemaining(changedStatusTasks);
			}
		}
	}
	
	private static void prep_CalculateEstimatedStartDates(List<IDNSC_Task__c> newList, Boolean isInsert)
	{
		if(!executedMethods.contains('calculateEstimatedStartDates'))
		{
			Set<Id> relIDs = new Set<Id>();
			for(IDNSC_Task__c task : newList)
			{
				relIDs.add(task.IDNSC_Parent__c);
			}
			if(!relIDs.isEmpty())
			{
				Map<Id, List<IDNSC_Task__c>> relTaskMap = new Map<Id, List<IDNSC_Task__c>>();
				List<IDNSC_Task__c> lTasks = new List<IDNSC_Task__c>([Select Id, Critical_Path__c, Status__c, Task_Number__c, Duration_Days_Expected__c, Start_Date_Actual__c, Start_Date_Expected__c, Finish_Date_Estimated__c, Completion_Date_Actual__c, IDNSC_Parent__c, Completion_Date_Calc__c From IDNSC_Task__c Where IDNSC_Parent__c In :relIDs]);
				for(IDNSC_Task__c t : lTasks)
				{
					List<IDNSC_Task__c> tasksByRel;
					if(relTaskMap.containsKey(t.IDNSC_Parent__c))
					{
						tasksByRel = relTaskMap.get(t.IDNSC_Parent__c);
					}
					else
					{
						tasksByRel = new List<IDNSC_Task__c>();
					}
					tasksByRel.add(t);
					relTaskMap.put(t.IDNSC_Parent__c, tasksByRel);
				}
				if(isInsert)
				{
					IDNSC_Task_Main.calculateStartDates_Insert(relTaskMap);
				}
				else
				{
					IDNSC_Task_Main.calculateStartDates_Update(relTaskMap);
				}
			}
		}
	}
}