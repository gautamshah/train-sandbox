public with sharing class ControllerX_AccountSalesReps{
     /****************************************************************************************
     * Name    : ControllerX_AccountSalesReps
     * Author  : Shawn Clark
     * Date    : 05/23/2016 
     * Purpose : Returns Sales Reps associated with an Account
     * Dependencies: AccountTeamMember Object
     *             , Account_Territory__c Object
     *             , UserTerritory Object
     *             , Territory Object
     *             , User Object
     *****************************************************************************************/
    public class AccountSalesReps 
    {
        public string userguid {get;set;}
        public string name {get;set;}
        public string therapy {get;set;}
        public string bu {get;set;}
        public string title {get;set;}
        public string email {get;set;}
        public string workphone {get;set;}
        public string cellphone {get;set;}
        public string photo {get;set;}
        public string mgrguid {get;set;}
        public string mgrname {get;set;}
    }

    public Account acct{get;set;}
    public list <Rep_To_Therapy__c> TherapyList {get;set;}
    public list <String> Therapy {get;set;}
    public set <Id> userSet {get;set;}
    public list <AccountSalesReps> aList {get;set;}
    public Set <String> AK_TerritoryIds {get;set;}
    public String CurrentPlatform {get; set;}
    public String DesktopOrMobile {get; set;}
    public String AccountId {get; set;}
    
    public set<String> setTherapies {get;set;}
    
   //Constructor
    public ControllerX_AccountSalesReps(ApexPages.StandardController ctlr)
    {
        acct = (Account)ctlr.getRecord();  
        AccountId = acct.id;
        CurrentPlatform = 'Unknown';
        DesktopOrMobile = UserInfo.getUiThemeDisplayed();
        
        IF (DesktopOrMobile == 'Theme4t') {
        CurrentPlatform = 'Mobile';
        }
        Else {
        CurrentPlatform = 'Desktop';
        } 
    }
    
    
   
     Public PageReference  ResetPage() {
        return null;
    }  
     
 
    //Tracking 
    
    public pageReference trackVisits(){
        String strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1]; 
        String strurl_abb = strurl.substringBefore('?');
        
        VF_Tracking__c newTracking = new VF_Tracking__c(
            User__c = UserInfo.getUserId(),
            VF_Page_Visited__c = strurl_abb,
            Date_Time_Visited__c = datetime.now(),
            Platform__c = CurrentPlatform);
        insert newTracking;
        return null;
    }      
     
        
    public list<AccountSalesReps> getAccountTeams()
    {
        list<AccountSalesReps> aList = new list<AccountSalesReps>();
        Set<String> AK_TerritoryIds = new set<String>();
        
        // Get all RelatedIds from Group object for Groups aligned to Account
        
        for(Group at : [Select RelatedId From Group 
                        where Type = 'Territory' and Id in 
                        (Select UserOrGroupId From AccountShare 
                         Where RowCause in ('Territory', 'TerritoryManual') 
                               and AccountId = :acct.Id)])
        {
            AK_TerritoryIds.add(at.RelatedId);
         
        }

    //**************************************************************
    //Get a distinct set of users
    //***********************************
    set<Id> userSet = new set<Id>();
    
     for(UserTerritory ut :[Select u.UserId,  u.TerritoryId, u.Id 
                                  From UserTerritory u
                                 where TerritoryId in :AK_TerritoryIds
                                 And isActive = True
                                 order by UserId])
         {
            userSet.add(ut.UserId);
         }


    //User Map


    Map<id,User> UserDetails = new Map<id,User>([select Id, Name, Business_Unit__c, MobilePhone, Email, Title, Phone, SmallPhotoURL, ManagerID, Manager.Name
                  from User
                  where Id in :userSet]);

    for (Rep_to_Therapy__c  atm : [select User_ID__c, Therapy__c from Rep_to_Therapy__c where User_ID__c in :userSet])
            {
                AccountSalesReps atr = new AccountSalesReps();
                atr.userguid = atm.User_ID__c;
                atr.therapy = atm.Therapy__c; 
                atr.name = UserDetails.get(atr.userguid).Name;            
                atr.bu = UserDetails.get(atr.userguid).Business_Unit__c;
                atr.title = UserDetails.get(atr.userguid).title;
                atr.email = UserDetails.get(atr.userguid).email;
                atr.workphone = UserDetails.get(atr.userguid).phone;
                atr.cellphone = UserDetails.get(atr.userguid).mobilephone;
                atr.photo = UserDetails.get(atr.userguid).SmallPhotoURL;
                atr.mgrguid = UserDetails.get(atr.userguid).ManagerID;
                atr.mgrname = UserDetails.get(atr.userguid).Manager.Name;
     
                aList.add(atr);
                
            }
         
        return aList;
    }
    
    
      public String[] getMyTherapy()
    {
        list <Rep_To_Therapy__c> TherapyList = new list <Rep_To_Therapy__c>();
        set<String> setTherapies = new Set<String>();
        list <String> Therapy = new list <String>();
        
       Set<String> AK_TerritoryIds = new set<String>();
        
        // Get all RelatedIds from Group object for Groups aligned to Account
        
        for(Group at : [Select RelatedId From Group 
                        where Type = 'Territory' and Id in 
                        (Select UserOrGroupId From AccountShare 
                         Where RowCause in ('Territory', 'TerritoryManual') 
                               and AccountId = :acct.Id)])
        {
            AK_TerritoryIds.add(at.RelatedId);
         
        }

    //**************************************************************
    //Get a distinct set of users
    //***********************************
    set<Id> userSet = new set<Id>();
    
     for(UserTerritory ut :[Select u.UserId,  u.TerritoryId, u.Id 
                                  From UserTerritory u
                                 where TerritoryId in :AK_TerritoryIds
                                 And isActive = True
                                 order by UserId])
         {
            userSet.add(ut.UserId);
         }




        
        TherapyList = [SELECT Therapy__c FROM Rep_To_Therapy__c Where User_ID__c IN :userSet Order by Therapy__c]; 
        
        for (Integer i = 0; i< TherapyList.size(); i++)
       {
       setTherapies.add(TherapyList[i].Therapy__c); // Set to dedup Therapy List
       }

       // convert the set into a string array  
       Therapy = new String[setTherapies.size()];
       Integer i = 0;
         for (String t : setTherapies) { 
             Therapy[i] = t;
             i++;
       } 
  return Therapy;
    }  
    
    
    
  }