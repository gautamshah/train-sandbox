@isTest 
public class Territory_uTest
{
	@isTest
	static void fetchByFieldAndValue()
	{
		List<Territory> ts = [SELECT Custom_External_TerritoryID__c FROM Territory WHERE Custom_External_TerritoryID__c != null LIMIT 50];
		set<string> extIds = new set<string>();
		for(Territory t : ts)
			extIds.add(t.Custom_External_TerritoryID__c);
			
		Test.startTest();
		
		Map<object, Map<Id, Territory>> tsbyExtId = Territory_u.fetch(Territory.field.Custom_External_TerritoryID__c, DATATYPES.castToObject(extIds));

        Test.stopTest();
        
        system.assert(tsbyExtId != null && !tsbyExtId.isEmpty());
	}
}