@isTest
private class LatAccountsTableauControllerTest{
    static testMethod void verifyLatAccountsTableauController(){
        String recordTypeId = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('LATAM-Healthcare Facility').getRecordTypeId();
        Account acct = new Account(Name='Test Account', RecordTypeID=recordTypeId);
        Test.startTest();
        insert acct;
    
        PageReference pageRef = Page.LAT_Accounts_Tableau;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(acct);
        ApexPages.currentPage().getParameters().put('Id',acct.id);
        
        LatAccountsTableauController latc = new LatAccountsTableauController(sc);
        
        latc.viewAccountPotentialPlaybook();
        latc.viewAccountPerformancePlaybook();
        latc.viewDaySalesHistory();
        Test.stopTest();
    }
}