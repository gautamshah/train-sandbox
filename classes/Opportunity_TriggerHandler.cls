public with sharing class Opportunity_TriggerHandler extends TriggerFactoryHandlerBase{
	
	Opportunities_Utilities oppUtils = new Opportunities_Utilities();
	
	public override void OnBeforeInsert(List<SObject> newMappings)
	{
		oppUtils.OnBeforeInsert((List<Opportunity>) newMappings);
	}
	
	public override void OnBeforeUpdate(List<SObject> oldMappings, List<SObject> updatedMappings, Map<ID, SObject> mappingOldMap, Map<ID, SObject> mappingNewMap)
	{
		oppUtils.OnBeforeUpdate((List<Opportunity>)updatedMappings, (Map<ID, Opportunity>)mappingOldMap);
	}
	
	public override void OnAfterInsert(List<SObject> newMappings)
	{
		oppUtils.OnAfterInsert((List<Opportunity>) newMappings);
	}
	
	public override void OnAfterUpdate(List<SObject> oldMappings, List<SObject> updatedMappings, Map<ID, SObject> mappingOldMap, Map<ID, SObject> mappingMap)
	{
		oppUtils.OnAfterUpdate((List<Opportunity>) updatedMappings);
	}
	
	 public override void OnBeforeDelete(List<SObject> MappingsToDelete, Map<ID, SObject> MappingMap)
	{
		oppUtils.OnBeforeDelete((List<Opportunity>) MappingsToDelete, (Map<ID, Opportunity>) MappingMap);
	}
}