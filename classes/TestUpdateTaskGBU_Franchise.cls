@isTest
private class TestUpdateTaskGBU_Franchise {

    static testMethod void runTest() {
/*
        // Add Test Data
        User u = [Select Id from User where Id = :UserInfo.getUserId()];
        
        u.Business_Unit__c = 'The Unit';
        u.Franchise__c = 'Burger King';
        
        update u;
        
        Task t = new Task();
        t.Activity_Type__c = 'Test';
        t.Description = 'Test';
        t.OwnerId = u.Id;
        
        insert t;
        
        //System.assertEquals('Burger King', [Select Franchise__c from Task where Id = :t.Id].Franchise__c);
        System.assertEquals('The Unit', [Select Business_Unit__c from Task where Id = :t.Id].Business_Unit__c);
        
        
        u.Business_Unit__c = '';
        u.Franchise__c = '';
        
        update u;
        update t;
        
        //System.assertEquals(null, [Select Franchise__c from Task where Id = :t.Id].Franchise__c);
        System.assertEquals(null, [Select Business_Unit__c from Task where Id = :t.Id].Business_Unit__c);
*/
        /*
        
            Id pId = [select Id 
                        from Profile 
                       where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id;
                       
             User u2 = new User();
             u2.LastName = 'Shmoe Test';
             u2.Business_Unit__c = 'The Unit';
             u2.Franchise__c = 'Burger King';
             u2.email = 'testShmoe@covidian.com';
             u2.alias = 'testShmo';
             u2.username = 'testShmoe@covidian.com';
             u2.communityNickName = 'testShmoe@covidian.com';
             u2.ProfileId = pId;
             u2.CurrencyIsoCode='USD'; 
             u2.EmailEncodingKey='ISO-8859-1';
             u2.TimeZoneSidKey='America/New_York';
             u2.LanguageLocaleKey='en_US';
             u2.LocaleSidKey ='en_US';
             insert u2;
             System.runAs(u2){
                
                Task t2 = new Task();
                t2.Activity_Type__c = 'Test';
                t2.Description = 'Test';
                t2.OwnerId = u.Id;
                
                insert t2;  
                
                System.assertEquals(null, [Select Franchise__c from Task where Id = :t2.Id].Franchise__c);
                System.assertEquals(null, [Select Business_Unit__c from Task where Id = :t2.Id].Business_Unit__c);
             }
        
        */
    }
}