public with sharing class ContactTest{

    public String contactName {get;Set;}
    public Boolean isSFOne {get;Set;}
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    List<Contact_Affiliation__c> existing ;
    boolean check = true ;
    
    public Account acct { 
        get
        {
            if(acct == null)
            {
                String eyeDee = ApexPages.currentPage().getParameters().get('id');
                acct = [select id, name from Account where id=:eyeDee];
            }
            return acct;
        }
        set;
    }
    
    @RemoteAction
    public static PageReference SubmitAndExit() {
        return null;//this.SaveRecords();
    }
    
    public List<Contact_Affiliation__c> existingContacts
    {
        get
        {   If(check==true)
        {
            existing = con1.getRecords().deepClone(true, true, true);
            //existing = [select id,name,AffiliatedTo__c,Contact__c, Contact__r.Id, Contact__r.Account.Name, User_Login__c, Evaluating_Clinician__c, HospitalFacility__c from Contact_Affiliation__c where AffiliatedTo__c =:acct.id];
                system.debug('@@existing : ' +  existing);
                for(Contact_Affiliation__c c: existing)
                {
                    //c.AffiliatedTo__c = acct.Id;
                    existingContactIds.add(c.Id);
                }
         }
                
            return existing;
        }
        Set;
    }
    
    
    public List<Contact_Affiliation__c> blankAffiliatedContacts
    {
        get
        {
            if(blankAffiliatedContacts == null)
            {
                blankAffiliatedContacts = new List<Contact_Affiliation__c>{new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id), new Contact_Affiliation__c(AffiliatedTo__c = acct.Id)};
            }
            return blankAffiliatedContacts;
        }
        Set;
    }
    
    private Set<Id> existingContactIds
    {
        get
        { 
            if(existingContactIds == null)
            {
                existingContactIds = new Set<Id>();
                for(Contact_Affiliation__c p : existingContacts)
                    existingContactIds.add(p.Contact__r.Id);
           }
            return existingContactIds;
        }
        Set;
    }
    
    public ContactTest(ApexPages.StandardController controller)
    {
        size = 5;
        String one = ApexPages.currentPage().getParameters().get('id');
        isSFOne = one != null && one == '0';
        
    }
    
     
     public ApexPages.StandardSetController con1 {
         get {
            if(con1 == null) {
            system.debug('Account id>>>>>>>>>'+acct.id);
                con1 = new ApexPages.StandardSetController([select id,name,AffiliatedTo__c,Contact__c, Contact__r.Id, Contact__r.Account.Name, User_Login__c, Evaluating_Clinician__c, HospitalFacility__c from Contact_Affiliation__c where AffiliatedTo__c =:acct.id]);
                con1.setPageSize(5);
                noOfRecords=con1.getResultSize();                     
                }
            return con1;
        }
        set;
    }
    
    
    public PageReference AcctPage()
    {
        PageReference pr = new PageReference('/' + acct.Id);
        pr.setRedirect(true);
        return pr;
    }
    
    //public Id conAffiliationId {get; set;}
  
    public PageReference SaveRecords()
    {   
        check= false ;
        List<Contact_Affiliation__c> conAffiliationsToInsert = new List<Contact_Affiliation__c>();
        List<Contact_Affiliation__c> conAffiliationsToUpdate = new List<Contact_Affiliation__c>();
        
        for(Contact_Affiliation__c c : existingContacts)
        {
                    //if(c.Contact__c != null && (c.User_Login__c == false || c.Evaluating_Clinician__c == false)) {
                    
               if(c.Contact__c != null) {
                Contact_Affiliation__c c1 = new Contact_Affiliation__c();
                c1.Id = c.Id;
                c1.Contact__c = c.Contact__c;
                c1.User_Login__c = c.User_Login__c;
                c1.Evaluating_Clinician__c = c.Evaluating_Clinician__c;
                conAffiliationsToUpdate.add(c1);
               //make user login and evaluating clinician updatable
            }
        }
        for(Contact_Affiliation__c c: blankAffiliatedContacts)
        {
            if(c.Contact__c != null && !existingContactIds.contains(c.Contact__c))
            {
                existingContactIds.add(c.Contact__c);
                conAffiliationsToInsert.add(c);
            }
        }
        
        System.debug('conAffiliationsToInsert: ' + conAffiliationsToInsert);
        System.debug('conAffiliationsToUpdate: ' + conAffiliationsToUpdate);
        Boolean bExceptionCaught = false;
        try
        {
            List<Database.SaveResult> results = Database.insert(conAffiliationsToInsert, false);
            for(Database.SaveResult result : results)
            {
                if (!result.isSuccess())
                {
                    List <Database.Error >error = result.getErrors();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error [0].getMessage());
                    ApexPages.addmessage(myMsg);
                    //return null;
                    bExceptionCaught = true;
                }
            }
        }
        catch(DmlException exc)
        {
            ApexPages.addMessages(exc);
            //return null;
            bExceptionCaught = true;
        }
        
        try
        {
            List<Database.SaveResult> results = Database.update(conAffiliationsToUpdate, false);
            for(Database.SaveResult result : results)
            {
                if (!result.isSuccess())
                {
                    List <Database.Error >error = result.getErrors();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error [0].getMessage());
                    ApexPages.addmessage(myMsg);
                    //return null;
                    bExceptionCaught = true;
                }
            }
        }
        catch(DmlException exc)
        {
            ApexPages.addMessages(exc);
            //return null;
            bExceptionCaught = true;
        }
        
        return bExceptionCaught ? null : AcctPage();
    }
    
    public PageReference Cancel()
    {
        con1.save();
        return AcctPage();
    }
    
        public Boolean hasNext {
        get {
             return con1.getHasNext();
            }
        set;
    }
   
    public Boolean hasPrevious {
        get {
            return con1.getHasPrevious();
        }
        set;
    }
   
    public Integer pageNumber {
        get {
            return con1.getPageNumber();
        }
        set;
    }
   
    public void first() {
        con1.save();
        con1.first();
    }
   
    public void last() {
        con1.last();
    }

    public void previous() {
        con1.previous();
    }
    
    public void next() {
        con1.next();
    }
       
}