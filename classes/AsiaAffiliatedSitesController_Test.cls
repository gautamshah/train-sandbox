@isTest (SeeAllData=false)
private class AsiaAffiliatedSitesController_Test{
 static testMethod void AsiaAffiliatedSitesController_Test() {
        User rep = New User();
        rep.firstName = 'Rep';
        rep.LastName = 'Test';
        rep.Alias = 'RTest';
        rep.Email = 'Rep.test@Test.com';
        rep.Username = 'Rep.test@Test.com';
        rep.CommunityNickname = 'Rep.test';
        rep.ProfileID= [SELECT ID,Name from profile where name = 'ASIA - TH'].Id; 
        rep.Region__c = 'APAC';
        rep.Business_Unit__c  = 'EMID';
        rep.User_Role__c  = 'Rep';
        rep.Function__c= 'Sales';
        rep.TimeZoneSidKey ='Asia/Riyadh';
        rep.LocaleSidKey = 'en_US';
        rep.EmailEncodingKey = 'UTF-8';
        rep.LanguageLocaleKey = 'en_US';
        Insert rep;
        
        System.RunAs(rep){ 
        Integer result;
        AsiaAffiliatedSitesController var = new AsiaAffiliatedSitesController();
        result = var.getprofName();
        System.assertEquals(result,22);
       }
      
       rep.LocaleSidKey = 'en_MY';
       rep.Asia_Team_Asia_use_only__c = 'CRM Team' ;
       update rep;
       System.RunAs(rep){ 
        Integer result;
        AsiaAffiliatedSitesController var = new AsiaAffiliatedSitesController();
        result = var.getprofName();
        System.assertEquals(result,99);
       }
       
       rep.ProfileID = [SELECT ID,Name from profile where name = 'Asia - HK / SG'].Id; 
       rep.LocaleSidKey = 'zh_HK';
       rep.Asia_Team_Asia_use_only__c = 'CRM Team' ;
       update rep;
       System.RunAs(rep){ 
        Integer result;
        AsiaAffiliatedSitesController var = new AsiaAffiliatedSitesController();
        result = var.getprofName();
        System.assertEquals(result,5);
       }
      
       rep.ProfileID = [SELECT ID,Name from profile where name = 'Asia - PH'].Id; 
       rep.LocaleSidKey = 'en_PH';
       rep.Asia_Team_Asia_use_only__c = 'CRM Team' ;
       update rep;
       System.RunAs(rep){ 
        Integer result;
        AsiaAffiliatedSitesController var = new AsiaAffiliatedSitesController();
        result = var.getprofName();
        System.assertEquals(result,25);
       }
      
  }

}