/** 

CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
10/31/2016  Paul Berglund   OBSOLETE 20161031A - these items are no longer used, from what I can tell
11/03/2016  Isaac Lewis     Added cpqConfigSetting_cTest.create() when running tests so Apttus managed package tests
                                will have an initialized CPQ Config Setting Record for reference.
===============================================================================
*/
public class cpqConfigSetting_c extends sObject_c
{
	private static cpqConfigSetting_g cache = new cpqConfigSetting_g();
	
    private class NoSystemPropertiesException extends Exception { }

    private static FINAL string SYSTEM_PROPERTIES = 'System Properties';

    public static FINAL sObjectField AGREEMENT_DIRECT =
                            CPQ_Config_Setting__c.Agreement_Direct_Deal_Types__c;

    public static FINAL sObjectField ALLOWING_TRACING_CUSTOMERS =
                            CPQ_Config_Setting__c.Deal_Types_Allowing_Tracing_Customers__c;

    public static FINAL sObjectField REQUIRE_OPPORTUNITY_BY_AMOUNT =
                            CPQ_Config_Setting__c.Deal_Types_Require_Opportunity_by_Amount__c;

    public static FINAL sObjectField REQUIRING_NO_CART =
                            CPQ_Config_Setting__c.Deal_Types_Requiring_No_Cart__c;

    public static FINAL sObjectField REQUIRING_NO_OPPORTUNITY =
                            CPQ_Config_Setting__c.Deal_Types_Requiring_No_Opportunity__c;

    public static FINAL sObjectField NOT_ALLOWING_PRICE_ADJUSTMENT =
                            CPQ_Config_Setting__c.Deal_Types_Not_Allowing_Price_Adjustment__c;
                            
    ///////////////////////////////////////////////////////////////////////////////
    /// CPQ_CONFIG_SETTING__c UTILITIES (Singleton)
    ///////////////////////////////////////////////////////////////////////////////

    public static boolean getAllowedToSeeCOOPPricingFile (string profileName)
    {
        System.debug('profile: ' + profileName);
        System.debug('profiles allowed to see COOP price file: ' +
        			 SystemProperties.Profiles_allowed_to_see_COOP_Price_File__c);
        System.debug('profile index: ' +
                     SystemProperties.Profiles_allowed_to_see_COOP_Price_File__c.indexOf(profileName));
        return (SystemProperties.Profiles_allowed_to_see_COOP_Price_File__c != null &&
                SystemProperties.Profiles_allowed_to_see_COOP_Price_File__c.indexOf(profileName) != -1) ?
                	true : false;
    }

    public static Set<String> getMultiPicklistSystemProperty(Schema.sObjectField fieldName)
    {
        // Returns selected values for a multi-select picklist field on system properties record
        DEBUG.write(new List<string>{ fieldName.getDescribe().getName() });
        DEBUG.write(new List<string>{ (string)SystemProperties.get(fieldName) });
        Set<string> results = DATATYPES.getMultiPicklistValueSet((string)SystemProperties.get(fieldName));
        DEBUG.write(new List<string>(results));
		return (SystemProperties.get(fieldName) != null) ?
					DATATYPES.getMultiPicklistValueSet((string)SystemProperties.get(fieldName)) :
					null;

        //Set<String> values = new Set<String>();
        //if (SystemProperties.get(fieldName) != null) {
        //    values = getMultiPicklistValueSet((string)SystemProperties.get(fieldName));
        //}
        //return values;
    }

    public static Date getBusinessEndDate (DateTime gmtStartDate, Integer businessDays) {
        // Calculates end date of time interval in business days from a gmt start date, usually System.now()
        // Weekends are not business days.
        // Requests received after 3pm are considered next business day.
        DateTime localStartDate = DateTime.newInstanceGMT(gmtStartDate.date(), gmtStartDate.time());
        Date localEndDate = Date.valueOf(localStartDate);//exact copy of start date

        // Work requested after 3:00pm can't start until the next business day
        DateTime cutoff = getBusinessDayCutOffHour();
        System.Debug('*** cutoff ' + cutoff + ', localStartDate ' + localStartDate);

        if (localStartDate >= cutoff) {
            do {
                localEndDate = localEndDate.addDays(1);
            } while (!cpq_u.isLocalBusinessDay(localEndDate));
        }

        // Nothing gets done on the weekends
        while (!cpq_u.isLocalBusinessDay(localEndDate)) {
            localEndDate = localEndDate.addDays(1);
        }

        for (Integer dayCount = 0; dayCount < businessDays; dayCount++) {
            do {
                localEndDate = localEndDate.addDays(1);
            } while (!cpq_u.isLocalBusinessDay(localEndDate));
        }

        return localEndDate;
    }

    public static DateTime getBusinessDayCutOffHour ()
    {
    	// What is the Business Day Cut Off Hour?
    	Time defaultCutOffHour = Time.newInstance(15, 0, 0, 0); // 3:00 PM?
        DateTime defaultCutOffDateTime = DateTime.newInstanceGMT(System.Today(), defaultCutOffHour);

		try
		{
			string[] parts = SystemProperties.BusinessDay_Cutoff_Hour__c.split(':');

			return (parts.isEmpty() || parts.size() != 2) ?
				   		defaultCutOffDateTime :
				   		DateTime.newInstanceGMT(
                    			System.Today(),
                    			Time.newInstance(
                    				Integer.valueOf(parts[0]),
                    				Integer.valueOf(parts[1]),
                    				0,
                    				0));
		}
		catch (Exception ex)
		{
			return defaultCutOffDateTime;
		}
        //DateTime cutOff = DateTime.newInstanceGMT(System.Today(), Time.newInstance(15, 0, 0, 0));
        //if (!String.isEmpty(getSystemProperties().BusinessDay_Cutoff_Hour__c)) {
        //    String[] a = getSystemProperties().BusinessDay_Cutoff_Hour__c.split(':');
        //    if (a.size() == 2) {
        //        try {
        //            cutOff = DateTime.newInstanceGMT(
        //							System.Today(),
        //							Time.newInstance(
        //               				Integer.valueOf(a[0]),
        //								Integer.valueOf(a[1]),
        //								0,
        //								0));
        //        } catch (Exception e) {}
        //    }
        //}
        //return cutOff;
    }

    public static Integer getProposalValidForAfterPresented ()
    {
    	Integer defaultNumberOfDays = 45;
    	return (SystemProperties.Proposal_Valid_For_after_Presented__c != NULL) ?
					Integer.valueOf(SystemProperties.Proposal_Valid_For_after_Presented__c) :
        			defaultNumberOfDays;

        //Integer days = 45;
        //if (SystemProperties.Proposal_Valid_For_after_Presented__c != NULL)
        //{
        //    days = Integer.valueOf(SystemProperties.Proposal_Valid_For_after_Presented__c);
        //}
        //return days;
    }

    public static Decimal getRenewalUpliftPercentage ()
    {
        Decimal defaultRenewalUpliftPercentage = 3.00;
        return (SystemProperties.Renewal_Uplift_perc__c != null) ?
        			SystemProperties.Renewal_Uplift_perc__c :
        			defaultRenewalUpliftPercentage;

        //Decimal uplift = 3.00;
        //if (getSystemProperties().Renewal_Uplift_perc__c != null) {
        //    uplift = getSystemProperties().Renewal_Uplift_perc__c;
        //}
        //return uplift;
    }

    public static Decimal getRequireOpportunityAmountThreshold ()
    {
        Decimal defaultRequiredOpportunityAmountThreshold = 1500.00;
        return (SystemProperties.Require_Opportunity_Amount_Threshold__c != null) ?
            		SystemProperties.Require_Opportunity_Amount_Threshold__c :
            		defaultRequiredOpportunityAmountThreshold;

        //Decimal value = 1500.00;
        //if (getSystemProperties().Require_Opportunity_Amount_Threshold__c != null) {
        //    value = getSystemProperties().Require_Opportunity_Amount_Threshold__c;
        //}
        //return value;
    }

    public static boolean getSendEmailToAgreementOwner (string subject)
    {
        return (SystemProperties.Task_Subjects_to_trigger_email_to_Owner__c != null &&
                SystemProperties.Task_Subjects_to_trigger_email_to_Owner__c.indexOf(subject) != -1) ?
                	true : false;
    }

    public static boolean getSetTaskOwnerToAgreementOwner (string subject)
    {
        return (SystemProperties.Task_Subjects_to_trigger_setting_Owner__c != null &&
                SystemProperties.Task_Subjects_to_trigger_setting_Owner__c.indexOf(subject) != -1) ?
                	true : false;
    }

	//
	// cpqClassOfTrade_c
	//
    public static List<string> getCOTFilters(Apttus_Proposal__Proposal__c proposal)
    {
        List<string> COTs = new List<string>();

        if (proposal == null) return COTs;

        // Check that user profile is included in North Star program
        if(SystemProperties.North_Star_Profiles__c != null)
        {
            List<string> profileNames = SystemProperties.North_Star_Profiles__c.split('\r\n');
            List<User> user = [SELECT Id
                               FROM User
                               WHERE Profile.Name IN :profileNames AND
                               Id = :proposal.OwnerId];

            if(user.isEmpty()) return COTs;
        }

        List<string> proposalRecordTypes = new List<string>();
        if(SystemProperties.North_Star_Proposal_Record_Types__c != null)
            proposalRecordTypes.addAll(SystemProperties.North_Star_Proposal_Record_Types__c.split('\r\n'));

        List<string> rebateRecordTypes = new List<string>();
        if(SystemProperties.North_Star_Rebate_Record_Types__c != null)
            rebateRecordTypes.addAll(SystemProperties.North_Star_Rebate_Record_Types__c.split('\r\n'));

        Map<Id, RecordType> recordTypes = new Map<Id, RecordType>([SELECT Id
                                                                   FROM RecordType
                                                                   WHERE Name IN :proposalRecordTypes OR
                                                                         Name IN :rebateRecordTypes]);

        List<Rebate__c> rebates = [SELECT Id
                                   FROM Rebate__c
                                   WHERE RecordTypeId IN :recordTypes.keySet() AND
                                         Related_Proposal__r.RecordTypeId IN :recordTypes.keySet()];

        if(rebates.size() == 0) return COTs;

        // Fetch Classes of Trade
        if(SystemProperties.Participating_Facility_COTs__c != null)
            COTs.addAll(SystemProperties.Participating_Facility_COTs__c.split('\r\n'));

        return COTs;
    }

    public static CPQ_Config_Setting__c SystemProperties
    {
    	get
    	{
    		DEBUG.constructorTitleBlock('SystemProperties');
    		if (SystemProperties == null)
    		{
    			load();
    			SystemProperties = fetch();
    		}
    		
    		return SystemProperties;
    	}
    	
    	private set;
    }

    public static Map<string, object> FieldName2Value = new Map<string, object>();

	//// We only need to return a record with the Name of 'System Properties'
	private static CPQ_Config_Setting__c fetch()
	{
		Map<Id, CPQ_Config_Setting__c> found = cache.fetch(CPQ_Config_Setting__c.field.Name, SYSTEM_PROPERTIES);
		//// There needs to be 1 record for this to work
		if (found == null || found.isEmpty()) return null;
		
		return found.values()[0];
	}

    private static void load()
    {
        if (cache.isEmpty())
        {
        	CPQ_Config_Setting__c fromDB;
        	
            try
            {
            	fromDB = fetchFromDB();
            
				if (fromDB == null)
					if (Test.isRunningTest())
					{
						insert create();
						fromDB = fetchFromDB();
					}
            }
            catch(Exception e)
            {
				if (Test.isRunningTest())
				{
					insert create();
					fromDB = fetchFromDB();
				}
            }
					
			if (fromDB != null)
	            cache.put(fromDB);
        }
    }
    
    public static void reload()
    {
        cache.clear();
        load();
    }
    
    private static CPQ_Config_Setting__c fetchFromDB()
    {
	    string query =
	    	SOQL_select.buildQuery(
	    		CPQ_Config_Setting__c.sObjectType,
	    		' WHERE Name = :SYSTEM_PROPERTIES ',
	    		null,
	    		1);

		return (CPQ_Config_Setting__c)Database.query(query);
    }

    @testVisible
    private static CPQ_Config_Setting__c create()
    {
        CPQ_Config_Setting__c settings = new CPQ_Config_Setting__c();
        settings.Name = 'System Properties';

        settings.Agreement_Direct_Deal_Types__c =
            'Custom Kit' +
            ';Hardware Quote Direct' +
            ';Scrub PO' +
            ';Smart Cart';
        settings.Approver_Agreement_Record_Types__c = ''; // OBSOLETE 20161031A
        settings.BusinessDay_Cutoff_Hour__c = '15:00';
        settings.Consumable_Attributes__c =
            'Disposable / Consumable' +
            '; Non-Sterile' +
            '; Remanufactured / Recycled' +
            '; Sensor';
        settings.Deal_Types_Allowing_Tracing_Customers__c =
            'Locally Negotiated Agreement (LNA)' +
            '; Price Offer Letter' +
            '; Sales and Pricing Agreement';
        settings.Deal_Types_Not_Allowing_Price_Adjustment__c =
            'Current Price Quote' +
            ';Demo';
        settings.Deal_Types_Require_Opportunity_by_Amount__c =
            'Current Price Quote' +
            '; New Customer Proposal' +
            '; Hardware Quote Direct';
        settings.Deal_Types_Requiring_No_Cart__c =
            'Airway Prods Incentive Program (APIP)' +
            '; Rebate Agreement' +
            '; TaperGuard Seed Program' +
            '; TaperGuard Volume Pricing Program';
        settings.Deal_Types_Requiring_No_Opportunity__c =
            'Current Price Quote' +
            '; Hardware Quote Direct' +
            '; New Customer Proposal' +
            '; Demo';
        settings.Equipment_Attributes__c =
            'Cable; Equipment' +
            '; Hardwired Oxinet' +
            '; Monitor' +
            '; Software' +
            '; Third Party' +
            '; Wireless Oxinet';
        settings.Is_Hardware_Quote_Direct_Enabled__c = true; // logic it controls was activated
        settings.North_Star_COTs__c =
            '003' + CONSTANTS.CRLF +
            'DME';
        settings.North_Star_Profiles__c = 'APTTUS - RMS US Sales (RS+PM)';
        settings.North_Star_Proposal_Record_Types__c =
            'Rebate Agreement' + CONSTANTS.CRLF +
            'Rebate Agreement Locked';
        settings.North_Star_Rebate_Record_Types__c = 'CPP North Star';
        settings.Profiles_allowed_to_see_COOP_Price_File__c =
            'Support Ind Contracts Manager' + CONSTANTS.CRLF +
            'Support Ind Contracts Analyst' + CONSTANTS.CRLF +
            'APTTUS - RMS US Contract Dev' + CONSTANTS.CRLF +
            'CRM Admin Support';
        settings.Participating_Facility_COTs__c =
            '+All' + CONSTANTS.CRLF +
            '-001' + CONSTANTS.CRLF +
            '+001::00A' + CONSTANTS.CRLF +
            '+002::00A';
        settings.Approver_Proposal_Record_Types__c = ''; // OBSOLETE 20161031A
        settings.Proposal_Valid_For_after_Presented__c = 90;
        settings.Renewal_Uplift_perc__c = 3.00;
        settings.Require_Opportunity_Amount_Threshold__c = 15000;
        settings.Sales_Org_RMS__c = 'RMS'; // OBSOLETE 20161031A Replaced by OrganizationNames
        settings.RMS_Agreement_Direct_Deal_Types__c =
            'Hardware Quote Direct' +
            ';Rebate Agreement';
        settings.RMS_Deal_Types__c =
            'Locally Negotiated Agreement (LNA)' +
            ';Price Offer Letter' +
            ';Sales and Pricing Agreement';
        settings.SSG_Deal_Types__c =
            'Custom Kit' +
            ';Quick Quote' +
            ';Scrub PO;Smart Cart';
        settings.Sales_Org_Surgical__c  = 'Surgical'; // OBSOLETE 20161031A Replaced by OrganizationNames
        settings.Task_Subject_Notify_CS_when_EA_done__c = ''; // OBSOLETE 20161031A
        settings.Task_Subject_Notify_Owner_when_CD_done__c = ''; // OBSOLETE 20161031A
        settings.Task_Subject_Notify_Owner_when_CS_done__c = ''; // OBSOLETE 20161031A
        settings.Task_Subjects_to_trigger_email_to_Owner__c = 'Task_Subjects_to_trigger_email_to_Owner__c';
        settings.Task_Subjects_to_trigger_setting_Owner__c = 'Task_Subjects_to_trigger_setting_Owner__c';
        settings.Top_Level_Territory_Name_s__c = ''; // OBSOLETE 20161031A

        return settings;
    }
}