public with sharing virtual class dmObject extends dm implements Comparable
{
	//
	// CONSTRUCTORS
	//
	public dmObject(dm controller)
	{
		
	}
    public dmObject() { }
    public dmObject(sObject obj) { this.base = obj; }
    public dmObject(ApexPages.StandardSetController setcontroller) { super(setcontroller); }
    public dmObject(ApexPages.StandardController controller) { super(controller); }
    
    public PageReference onload()
    {
    	VFPageviewTracker tracker = new VFPageviewTracker();
    	tracker.trackVisits();
    	
    	if (folderId == null)
    	{
    		List<dmObject> apps = fetchApplications();
    		dmObject parent;
    		
    		while(apps != null && apps.size() == 1)
    		{
    			parent = apps[0];
    			apps = fetchChildren(apps[0].Id);
    		}
    	
    		if (parent != null)
    			return new PageReference(baseURL + '/apex/dmObject?folderId=' + parent.Id);
    	}

   		return null;
    }

	//
	// PROPERTIES 
	//
	public Id folderId { get { folderId = dm.querystring.get('folderId'); return folderId; } private set; }

	public Id Id { get { return (base != null) ? base.Id : null; } }
	public string Name { get { return (base != null) ? (base.get('Name') != null) ? (string)base.get('Name') : null : null; } }
	public string GUID { get { return (base != null) ? (base.get('GUID__c') != null) ? (string)base.get('GUID__c') : null : null; } }
	public decimal NumberOfColumns { get { return (decimal)((base != null) ? (base.get('NumberOfColumns__c') != null) ? base.get('NumberOfColumns__c') : 1 : 1); } }
	public decimal NumberOfLevelsToDisplay { get { return (decimal)((base != null) ? (base.get('NumberOfLevelsToDisplay__c') != null) ? base.get('NumberOfLevelsToDisplay__c') : 1 : 1); } }

	public decimal DisplayOrder { get { return (decimal)((base != null) ? (base.get('DisplayOrder__c') != null) ? base.get('DisplayOrder__c') : 0 : 0); } }

	public boolean isFolder { get {	return (base != null && base.getSObjectType() == dmFolder__c.getSObjectType()); } }
	public boolean isVisible { get { return (base != null && (base.get('Visible__c') != null) ? (boolean)base.get('Visible__c') : false); } }
//	public boolean displayWithFolder { get { return (base != null && (base.get('DisplayWithFolder__c') != null) ? (boolean)base.get('DisplayWithFolder__c') : false); } }
	public boolean displayTitle { get { return (base != null && (base.get('DisplayTitle__c') != null) ? (boolean)base.get('DisplayTitle__c') : false); } }
	public boolean displayDownloadLink { get { return (base != null && (base.get('DisplayDownloadLink__c') != null) ? (boolean)base.get('DisplayDownloadLink__c') : false); } }
	public boolean isTopLevelFolder { get { return (base != null && (base.get('TopLevel__c') != null) ? (boolean)base.get('TopLevel__c') : false); } }
	public boolean isApplicationFolder { get { return (base != null && (base.get('isApplication__c') != null) ? (boolean)base.get('isApplication__c') : false); } } 
	public boolean isDisabled { get { return (URL == null); } }

	public string URL
	{
		get 
		{
			if(isFolder)
				return baseURL + '/apex/dmObject?folderId=' + Id;
			else if (GUID != null && dmAttachment.fetch(GUID) != null)
			{
				Attachment attach = dmAttachment.fetch(GUID);
				if (attach != null)
					return baseURL + '/servlet/servlet.FileDownload?file=' + attach.Id;
			}
			return null;
		}
	}
	
	public User currentUser { get { return (User)dm.fetchRecord(UserInfo.getUserId()); } }
	public dmObject currentFolder { get { return (folderId != null) ? new dmObject((dmFolder__c)dm.fetchRecord(folderId)) : null; } }
//	public dmObject applicationFolder { get { return (folderId != null) ? new dmObject(dmFolder.ApplicationFolder(folderId)) : new dmObject(new dmFolder__c(Name = 'Applications')); } }
//	public dmObject topLevelFolder { get { return (folderId != null) ? new dmObject(dmFolder.ApplicationFolder(folderId)) : new dmObject(new dmFolder__c(Name = 'Applications')); } }

	public string pageTitle
	{
		get
		{
			if (folderId != null)
			{
				dmFolder__c f = dmFolder.TopLevelFolder(folderId);
				if (f != null)
					return f.Name;
				else
				{
					dmFolder__c a = dmFolder.ApplicationFolder(folderId);
					if (a != null)
						return a.Name;
				}
			}
			return 'Applications';
		}
	}
	
	public List<Territory> assignedTerritories
	{
		get
		{
			List<UserTerritory> uts = [SELECT TerritoryId FROM UserTerritory WHERE UserId = :UserInfo.getUserId() AND isActive = true];
			Set<Id> tids = new Set<Id>();
			for(UserTerritory ut : uts)
				tids.add(ut.TerritoryId);
				
			List<Territory> ts = [SELECT Id, Name, Custom_External_TerritoryID__c FROM Territory WHERE Id IN :tids];
			return  ts;
		}
	}
	
	//
	// INTERFACE IMPLEMENTATION
	//
	public integer compareTo(Object obj)
	{
		dmObject target = (dmObject)obj;
		
    	if (this.DisplayOrder > target.DisplayOrder)
		    return 1;
	    else if (this.DisplayOrder < target.DisplayOrder)
    		return -1;
    	else
    	{
    		if (this.Name > target.Name)
    			return 1;
    		else if (this.Name < target.Name)
    			return -1;
    		else
    		{
    			if (this.Id > target.Id)
    				return 1;
    			else if (this.Id < target.Id)
    				return -1;
    		}
    	}
    	// If all else fails, return -1 because VF ignores duplicates...
    	return -1;
    }
    
    //
    // COLLECTION METHODS
    //
    public dmObject application
    {
    	get
    	{
    		return new dmObject();
    	}
    }
    
	public string breadcrumb
	{
		get
		{
			return dmFolder.buildBreadcrumb(folderId);
		}
	}
	
	//
	//
	//
	public Component.Apex.PageBlockSection Children
	{
		get
		{
			if (Children == null)
			{
				dmObject currentFolder = this.currentFolder;
				integer levelsToDisplay = (currentFolder == null) ? 1 : (integer)currentFolder.NumberOfLevelsToDisplay;
				integer numberOfColumns = (currentFolder == null) ? 1 : (integer)currentFolder.NumberOfColumns;
				Children = new Component.Apex.PageBlockSection();
				Children.columns = numberOfColumns;
				Children.childComponents.addAll(Children(folderId, 1, levelsToDisplay, numberOfColumns));
			}
			return Children;
		}
		
		private set;
	}
	
	private List<ApexPages.Component> Children(Id folderId, integer level, integer levelsToDisplay, integer numberOfColumns)
	{
		List<ApexPages.Component> root = new List<ApexPages.Component>();

		List<dmObject> objects = new List<dmObject>();
		
		if (folderId != null)
			objects.addAll(fetchChildren(folderId));
		else
			objects.addAll(fetchApplications());
		
		integer ndx = 0;
		for(dmObject obj : objects)
		{
			if (obj.isVisible)
			{
				Component.Apex.PageBlockSection child = new Component.Apex.PageBlockSection();
				child.id = 'pbs';

				if (obj.isFolder)
				{
					integer columns = (integer)obj.NumberOfColumns;
					if (obj.DisplayTitle) child.title = obj.Name;
					child.collapsible = false;
					child.onclick = 'window.open(\'' + obj.URL + '\',\'_top\',\'\',1);';
					child.columns = columns;
					List<ApexPages.Component> displayWith = new List<ApexPages.Component>();
					
					if (level < levelsToDisplay) child.childComponents.addAll(Children(obj.Id, level + 1, levelsToDisplay, columns));
				}
				else
				{
					{
						Component.Apex.outputLink link = new Component.Apex.outputLink();
						link.disabled = obj.displayDownloadLink || obj.isDisabled;
						link.value = obj.URL;
						link.target = '_top';
							Component.Apex.outputText text = new Component.Apex.outputText();
							text.value = obj.Name;
							link.childComponents.add(text);
						child.childComponents.add(link);
					}
					
					if (obj.displayDownloadLink)
					{
						Component.Apex.outputPanel panel = new Component.Apex.outputPanel();
						panel.layout = 'block';
						panel.styleClass = 'button-blue';
							Component.Apex.outputLink link = new Component.Apex.outputLink();
							link.disabled = obj.isDisabled;
							link.value = obj.URL;
							link.target = '_top';
								Component.Apex.outputText text = new Component.Apex.outputText();
								text.styleClass = 'button-text';
								text.value = 'Download';
								link.childComponents.add(text);
						
								Component.Apex.image img = new Component.Apex.image();
								img.styleClass = 'button-img';
						
								String url_file_ref = '/resource/'
        		            		+ String.valueOf(((DateTime)dm.sr.SystemModStamp).getTime())
                		    		+ '/' 
                    				+ dm.sr.Name
		                    		+ '/images/download-icon.png';

								img.value = url_file_ref;
								link.childComponents.add(img);

							panel.childComponents.add(link);
						child.childComponents.add(panel);
					}
				}
				ndx++;
				root.add(child);
			}
		}
		for(integer i = 0; i < math.mod(ndx, numberOfColumns); i++)
			root.add(new Component.Apex.PageBlockSection());		

		return root;
	}
	
	public static List<dmObject> fetchChildren(Id parentId)
	{
		List<dmObject> targets = new List<dmObject>();
		
		if (!(parentId == null))
		{
			List<sObject> records = new List<sObject>();
			for (dmFolder__c f : dmFolders.All.values())
				if (f.Parent__c == parentId)
					records.add((sObject)f);

			for (dmDocument__c d : dmDocuments.All.values())
				if (d.Folder__c == parentId)
					records.add((sObject)d);

			for(SObject obj : records)
				targets.add(new dmObject(obj));
		}
		system.debug('dmObject.fetchChildren: ' + targets);

		targets.sort();
		return targets;
	}
	
	public static List<dmObject> fetchApplications()
	{
		List<dmObject> targets = new List<dmObject>();
		for(dmFolder__c folder : dmFolders.Applications.values())
			targets.add(new dmObject((sObject)folder));
		system.debug('dmObject.fetchApplications: ' + targets);

		targets.sort();
		return targets;
	}
}