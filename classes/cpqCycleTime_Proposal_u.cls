public class cpqCycleTime_Proposal_u extends cpqCycleTime_u
{
    // Capture the cycle times associated with changes to a Proposal
    protected override void capture(
        List<sObject> objNewList,
        Map<Id,sObject> objOldMap,
        Boolean isInsert,
        Boolean isUpdate)
    {
    	List<Apttus_Proposal__Proposal__c> newList = (List<Apttus_Proposal__Proposal__c>)objNewList;
    	Map<Id, Apttus_Proposal__Proposal__c> oldMap = (Map<Id, Apttus_Proposal__Proposal__c>)objOldMap;

        Set<CPQ_Cycle_Time__c> cycleTimesToUpsert = new Set<CPQ_Cycle_Time__c>();
        DateTime NOW = system.now();

        List<Id> opportunityIds = new List<Id>();
        List<Id> proposalIds = new List<Id>();
        for (Apttus_Proposal__Proposal__c prop: newList) {
            if (prop.Apttus_Proposal__Opportunity__c != null) {
                opportunityIds.add(prop.Apttus_Proposal__Opportunity__c);
            }
            proposalIds.add(prop.Id);
        }

        // Find existing Cycle Time rows that match on Opportunity or Proposal Id and determine whether to update existing rows or create new ones
        // For some deal types like quick quote there may not be an opportunity Id so only query by them if opportunity Ids are present.
        // Querying for all rows where Opportunity__c is null can get an error for more than 50000 rows returned.
        List<CPQ_Cycle_Time__c> cycleTimesByOpportunity = new List<CPQ_Cycle_Time__c>();
        if (opportunityIds != null && !opportunityIds.isEmpty()) {
            cycleTimesByOpportunity = [Select Id, Opportunity__c, Proposal__c, Opportunity_Created_Date__c, Proposal_Enter_Approvals_Date__c, Proposal_Enter_Approvals_Counter__c From CPQ_Cycle_Time__c Where Opportunity__c in :opportunityIds];
        }
        List<CPQ_Cycle_Time__c> cycleTimesByProposal = [Select Id, Opportunity__c, Proposal__c, Proposal__r.RecordType.Name, Opportunity_Created_Date__c, Proposal_Enter_Approvals_Date__c, Proposal_Enter_Approvals_Counter__c From CPQ_Cycle_Time__c Where Proposal__c in :proposalIds];
        Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([Select Id, CreatedDate From Opportunity Where Id in :opportunityIds]);

        Map<Id,Map<Id,CPQ_Cycle_Time__c>> cycleTimeOpportunityMap = new Map<Id,Map<Id,CPQ_Cycle_Time__c>>();
        Map<Id,CPQ_Cycle_Time__c> cycleTimeProposalMap = new Map<Id,CPQ_Cycle_Time__c>();

        for (CPQ_Cycle_Time__c ct: cycleTimesByOpportunity) {
            if (!cycleTimeOpportunityMap.containsKey(ct.Opportunity__c)) {
                cycleTimeOpportunityMap.put(ct.Opportunity__c, new Map<Id,CPQ_Cycle_Time__c>());
            }
            Map<Id,CPQ_Cycle_Time__c> oppMap = cycleTimeOpportunityMap.get(ct.Opportunity__c);
            oppMap.put(ct.Proposal__c, ct);
        }
        for (CPQ_Cycle_Time__c ct: cycleTimesByProposal) {
            cycleTimeProposalMap.put(ct.Proposal__c, ct);
        }

        // Get agreement record types
        Map<Id,RecordType> agreementRecordTypesMap = CPQ_AgreementProcesses.getAgreementRecordTypesByIdMap();

        for (Apttus_Proposal__Proposal__c prop: newList) {
            CPQ_Cycle_Time__c cycleTime = null;
            Boolean addToList = false;

            // Determine for each proposal whether an existing Cycle Time row should be updated based on matching Opportunity/Proposal Id, or
            // whether a new Cycle Time row should be created.
            if (prop.Apttus_Proposal__Opportunity__c != null) {
                Map<Id,CPQ_Cycle_Time__c> oppCycleTimesMap = cycleTimeOpportunityMap.get(prop.Apttus_Proposal__Opportunity__c);
                if (oppCycleTimesMap != null) {
                    if (oppCycleTimesMap.containsKey(prop.Id)) {
                        // Opp found and prop found - should be an update, just set fields and update
                        cycleTime = oppCycleTimesMap.get(prop.Id);
                    } else if (oppCycleTimesMap.containsKey(null)) {
                        // Opp found and null prop found - should be an insert, set proposal and fields and update
                        cycleTime = oppCycleTimesMap.get(null);
                        cycleTime.Proposal__c = prop.Id;
                        cycleTime.Proposal_Created_Date__c = NOW;
                        oppCycleTimesMap.remove(null);
                        oppCycleTimesMap.put(prop.Id, cycleTime);
                        addToList = true;
                    } else if (!oppCycleTimesMap.isEmpty()) {
                        // Opp found and only other prop found - should be an insert, need to copy this one and insert
                        CPQ_Cycle_Time__c oldCycleTime = oppCycleTimesMap.values()[0];
                        cycleTime = new CPQ_Cycle_Time__c();
                        cycleTime.Opportunity__c = oldCycleTime.Opportunity__c;
                        cycleTime.Opportunity_Created_Date__c = oldCycleTime.Opportunity_Created_Date__c;
                        cycleTime.Proposal__c = prop.Id;
                        cycleTime.Proposal_Created_Date__c = NOW;
                        addToList = true;
                    }
                }
            }

            if (cycleTime == null) {
                if (cycleTimeProposalMap.containsKey(prop.Id)) {
                    // Opp not found, proposal found - should be an update, just set fields and update
                    cycleTime = cycleTimeProposalMap.get(prop.Id);
                } else {
                    // Opp not found, proposal not found - should be an insert, create new row and insert
                    cycletime = new CPQ_Cycle_Time__c();
                    cycleTime.Proposal__c = prop.Id;
                    cycleTime.Proposal_Created_Date__c = NOW;
                    if (prop.Apttus_Proposal__Opportunity__c != null) {
                        cycleTime.Opportunity__c = prop.Apttus_Proposal__Opportunity__c;
                        cycleTime.Opportunity_Created_Date__c = opportunityMap.get(prop.Apttus_Proposal__Opportunity__c).CreatedDate;
                    }
                    addToList = true;
                }
            }

            // For updates, set any needed fields on the Cycle Time row for specific stages
            if (isUpdate && cycleTime != null) {
                Apttus_Proposal__Proposal__c oldProp = oldMap.get(prop.Id);

                // Enter Approvals Date
                if ((oldProp.Apttus_Proposal__Approval_Stage__c != IN_REVIEW && oldProp.Apttus_Proposal__Approval_Stage__c != PENDING_APPROVAL && oldProp.Apttus_Proposal__Approval_Stage__c != APPROVED) && 
                    (prop.Apttus_Proposal__Approval_Stage__c == IN_REVIEW || oldProp.Apttus_Proposal__Approval_Stage__c == PENDING_APPROVAL || prop.Apttus_Proposal__Approval_Stage__c == APPROVED)) {
                    if (cycleTime.Proposal_Enter_Approvals_Date__c == null) {
                        cycleTime.Proposal_Enter_Approvals_Date__c = NOW;
                        cycleTime.Proposal_Enter_Approvals_Counter__c = 1;
                        addToList = true;
                    } else {
                        cycleTime.Proposal_Enter_Approvals_Counter__c++;
                    }
                }
                // Exit Approvals Date
                if (oldProp.Apttus_Proposal__Approval_Stage__c != APPROVED && prop.Apttus_Proposal__Approval_Stage__c == APPROVED) {
                    cycleTime.Proposal_Exit_Approvals_Date__c = NOW;
                    addToList = true;
                }
                // Presented Date
                if (oldProp.Apttus_Proposal__Approval_Stage__c != PRESENTED && prop.Apttus_Proposal__Approval_Stage__c == PRESENTED) {
                    cycleTime.Proposal_Presented_Date__c = NOW;
                    addToList = true;
                }
                // Accepted Date
                if (oldProp.Apttus_Proposal__Approval_Stage__c != ACCEPTED && prop.Apttus_Proposal__Approval_Stage__c == ACCEPTED) {
                    cycleTime.Proposal_Accepted_Date__c = NOW;
                    addToList = true;
                }
                // Change from Locked to Unlocked record type
                RecordType oldRT = agreementRecordTypesMap.get(oldProp.RecordTypeId);
                RecordType newRT = agreementRecordTypesMap.get(prop.RecordTypeId);
                String oldRecordTypeName = oldRT != null ? oldRT.DeveloperName : '';
                String newRecordTypeName = oldRT != null ? newRT.DeveloperName : '';
                if (oldRecordTypeName.contains('Locked') && !newRecordTypeName.contains('Locked')) {
                    cycleTime.Proposal_Unlocked__c = true;
                    addToList = true;
                }
            }
            if (addToList) {
                cycleTimesToUpsert.add(cycleTime);
            }
        }

        if (!cycleTimesToUpsert.isEmpty()) {
            upsert new List<CPQ_Cycle_Time__c>(cycleTimesToUpsert);
        }
    }

}