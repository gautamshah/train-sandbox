@isTest
public class ShareDocumentsTrg_test {
    
    static testmethod void m1(){
        
        ID ProfileID = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].ID;
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User(username='20121217@mdttest.com',
                            alias = 'apitest',
                            userRoleId = portalRole.Id,
                            email='20121217@test.com', 
                            emailencodingkey='UTF-8',
                            lastname='Covidien',
                            languagelocalekey='en_US',
                            localesidkey='en_US',                                             
                            profileid = ProfileID,
                            timezonesidkey='Europe/Berlin',
                            Region__c = 'EU',                           
                            Business_Unit__c = 'All'
                            );
        insert u;      
        //User u = [SELECT Id, Name FROM User Where IsActive = true and Profile.Name = 'EU - S2' Limit 1];
        System.runAs(u)
        {   
            RecordType rt1=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
            Account a = new Account(Name='TestAcct',recordtypeId = rt1.Id);
            insert a;
            RecordType rt = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
            Contact c = new Contact();
            c.LastName = 'Tester';
            c.AccountId = a.Id;
            c.Connected_As__c = 'Administrator';
            c.RecordTypeId = rt.Id;
            c.Department_picklist__c = 'Other';
            c.Other_Department__c = 'Accounting';
            insert c;
            System.debug('New ContactId: ' + c.Id);
            
                
            Profile profile1 = [Select Id from Profile where name = 'Asia Distributor - CN'];
            User portalAccountOwner1 = new User(
                    ProfileId = profile1.Id,
                    Username = System.now().millisecond() + 'mdttest2@mdttest.com',
                    Alias = 'batman',
                    Email='bruce.wayne@wayneenterprises.com',
                    EmailEncodingKey='UTF-8',
                    Firstname='Bruce',
                    Lastname='Wayne',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago',
                    Asia_Team_Asia_use_only__c = 'PRM Team',
                    contactId = c.id
                    );
            Database.insert(portalAccountOwner1);
                            
            
            
            Distributor_Document__c dd1 = new Distributor_Document__c();
            dd1.Account_Name__c = a.Id;
            dd1.Name='Share1';
            dd1.Document_Type__c='Other - Please specify in comments';
            dd1.Expiry_Date__c = System.Today().addDays(45);
            dd1.Hide_from_Partner_User__c = true;
            insert dd1;
            
            Distributor_Document__c dd2 = new Distributor_Document__c();
            dd2.Account_Name__c = a.Id;
            dd2.Name='Share2';
            dd2.Document_Type__c='Quota Letter';
            dd2.Expiry_Date__c = System.Today().addDays(45);
            dd2.Hide_from_Partner_User__c = true;
            insert dd2;
        }
    
    }
    
}