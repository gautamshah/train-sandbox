/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A    IL         AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/     
public class cpqParticipatingFacility_LineItem_c
{
    public void setRecordType(
        cpqParticipatingFacility_Lineitem__c now,
        cpqParticipatingFacility_Lineitem__c before)
    {
        if (valueChanged(now, before, cpqParticipatingFacility_Lineitem__c.fields.AgreementLineItem__c))
        {
            now.RecordTypeId = (now.AgreementLineItem__c == null) ?
                cpqParticipatingFacility_LineItem_RT_c.RecordTypeId(cpqParticipatingFacility_LineItem_RT_c.RecordTypes.Unlocked) :
                cpqParticipatingFacility_LineItem_RT_c.RecordTypeId(cpqParticipatingFacility_LineItem_RT_c.RecordTypes.Locked);
        }
    }
    
    // This will set the Lookup fields needed for the Templates to be able to access
    // these records
    public void setLookups(
        cpqParticipatingFacility_Lineitem__c now,
        cpqParticipatingFacility_Lineitem__c before,
        Apttus_Proposal__Proposal_Line_Item__c proposalLineItem,
        Apttus__AgreementLineItem__c agreementLineItem)
    {
        if (valueChanged(now, before, cpqParticipatingFacility_Lineitem__c.fields.ProposalLineItem__c))
        {
            now.Proposal__c = (proposalLineItem == null) ? null : proposalLineItem.Apttus_Proposal__Proposal__c;
        }
            
        if (valueChanged(now, before, cpqParticipatingFacility_Lineitem__c.fields.AgreementLineItem__c))
            now.Agreement__c = (agreementLineItem == null) ? null : agreementLineItem.Apttus__AgreementId__c;
    }
    
    // Returns true if this is a new record (Insert)
    // or it was an Update and the values changed between old
    // and new records
    public boolean valueChanged(
        sObject now,
        sObject before,
        Schema.sObjectField field)
    {
        //system.debug('valueChanged.now: ' + now.get(field));
        //system.debug('valueChanged.before: ' + ((before == null) ? null : before.get(field)));
        
        return (before == null) || now.get(field) != before.get(field);
    }

    public static List<cpqParticipatingFacility_Lineitem__c> fetch(Id proposalId) {
        return [Select Id,
                        Proposal__c,
                        ProposalLineItem__c,
                        ERPRecord__c, 
                        QuantityToBePlacedAtShipTo__c,
                        TradeinSerialNumbersCount__c,
                        TradeinSerialNumbers__c,
                        ParticipatingFacilityAddress__c
                From cpqParticipatingFacility_Lineitem__c
                Where Proposal__c = :proposalId];
    }
}