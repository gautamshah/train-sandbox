@isTest
public class AccountExtTest
{
static testMethod void test1() 
{
RecordType rt=[select Id, Name from Recordtype where name='US-Hospital Community' limit 1];
Account a= new Account();
a.name='samleTest';
 a.RecordTypeId =rt.id;
a.BillingStreet = 'Test Billing Street';
a.BillingCity = 'Test Billing City';  
a.BillingState = 'Test';  
a.BillingPostalCode = '12345';

insert a;

RecordType rt1=[select Id, Name from Recordtype where name='US-Healthcare Facility' limit 1];

Account a1= new Account();
a1.name='sampleTest1';
 a1.RecordTypeId =rt1.id;
a1.BillingStreet = 'Test Billing Street';
a1.BillingCity = 'Test Billing City';  
a1.BillingState = 'Test';  
a1.BillingPostalCode = '12345';
test.starttest();
insert a1;

Account_Affiliation__c aaf= new Account_Affiliation__c();
aaf.Affiliated_with__c=a1.id;
aaf.AffiliationMember__c=a.id;
insert aaf;
Contact ct= new Contact();
ct.Lastname='ramesh';
ct.accountId=a1.id;
insert ct;
PageReference pageRef = Page.Hospital_Community_Contact_User;
        pageRef.getParameters().put('id', a.id);
        Test.setCurrentPage(pageRef);
 ApexPages.StandardController sc = new ApexPages.StandardController( a);
        AccountExt testAcc = new  AccountExt(sc);
        testAcc.getRecords();
        testAcc.first(); 
        testAcc.last() ;
        testAcc.previous() ;
        testAcc.next() ;
        Contact_Affiliation__c ca= new Contact_Affiliation__c();
        ca.AffiliatedTo__c=a1.id;
        ca.User_Login__c=true;
        ca.Evaluating_Clinician__c=true;
        ca.Contact__c=ct.id;
        insert ca;
                testAcc.customSave();
        testAcc.customCancel();
        test.stopTest();
        }
        static testMethod void test2() 
{
RecordType rt=[select Id, Name from Recordtype where name='US-Hospital Community' limit 1];
Account a= new Account();
a.name='samleTest';
 a.RecordTypeId =rt.id;
a.BillingStreet = 'Test Billing Street';
a.BillingCity = 'Test Billing City';  
a.BillingState = 'Test';  
a.BillingPostalCode = '12345';

insert a;

RecordType rt1=[select Id, Name from Recordtype where name='US-Healthcare Facility' limit 1];

Account a1= new Account();
a1.name='sampleTest1';
 a1.RecordTypeId =rt1.id;
a1.BillingStreet = 'Test Billing Street';
a1.BillingCity = 'Test Billing City';  
a1.BillingState = 'Test';  
a1.BillingPostalCode = '12345';
Test.starttest();
insert a1;

Account_Affiliation__c aaf= new Account_Affiliation__c();
aaf.Affiliated_with__c=a1.id;
aaf.AffiliationMember__c=a.id;
insert aaf;
Contact ct= new Contact();
ct.Lastname='ramesh';
ct.accountId=a1.id;
insert ct;
Contact_Affiliation__c cac= new Contact_Affiliation__c();
cac.AffiliatedTo__c =a.id;
cac.Contact__c=ct.id;
cac.User_Login__c=true;
cac.Evaluating_Clinician__c=true;
insert cac;

Contact_Affiliation__c cac1= new Contact_Affiliation__c();
cac1.AffiliatedTo__c =a.id;
cac1.Contact__c=ct.id;
cac1.User_Login__c=false;
cac1.Evaluating_Clinician__c=false;
insert cac1;


PageReference pageRef = Page.Hospital_Community_Contact_User;
        pageRef.getParameters().put('id', a.id);
        Test.setCurrentPage(pageRef);
 ApexPages.StandardController sc = new ApexPages.StandardController( a);
        AccountExt testAcc = new  AccountExt(sc);
        testAcc.getRecords();
        testAcc.first(); 
        testAcc.last() ;
        testAcc.previous() ;
        testAcc.next() ;
                 Contact_Affiliation__c ca= new Contact_Affiliation__c();
        ca.AffiliatedTo__c=a1.id;
        ca.User_Login__c=true;
        ca.Evaluating_Clinician__c=true;
        ca.Contact__c=ct.id;
        insert ca;
        AccountExt.wrapperAccount wa= new AccountExt.wrapperAccount (a,ct,true,true);
        wa.isselected =true;
        wa.isselected1 =true;

                testAcc.customSave();
                Test.stopTest();
                }
}