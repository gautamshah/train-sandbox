/**
Test class

CPQ_AgreementProcesses
CPQ_ApprovalRetriggerFieldEngine
CPQ_Controller_ProposalValidation.doValidationLite
CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement
CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement_LetExp
CPQ_PartnerContractWSInvoke.InvokePartnerQueueWS
CPQ_PartnerContractWSInvoke.getAgreementByID
CPQ_PartnerWSStruct.SetCaller(Agreement)
CPQ_PartnerWSStruct.SetFacilities(Agreement)
CPQ_PartnerWSStruct.SetItems(Agreement)
CPQ_PartnerWSStruct.SetCompliances(Agreement)
CPQ_PartnerWSStruct.SetDistributors(Agreement)

CPQ_Agreement_Before
CPQ_Agreement_After

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-26      Yuli Fintescu       Created
2016-09-02      Isaac Lewis         Patched createTestData2() to account for COOP amendment validation rule
===============================================================================
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
private class Test_CPQ_Agreement_Trigger
{
    static List<Apttus__APTS_Agreement__c> testAgreements;

    static void createTestData()
    {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);
        
        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
  
        List<Account> testAccounts = new List<Account>
        {
            new Account(Name = 'Test GPO', Status__c = 'Active', Account_External_ID__c = 'US-111111_GPO', RecordTypeID = gpoRT.getRecordTypeId()),
            new Account(Name = 'Test Account', Status__c = 'Active', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main')
        };
        
        insert testAccounts[0];
        testAccounts[1].Primary_GPO_Buying_Group_Affiliation__c = testAccounts[0].ID;
        insert testAccounts[1];

        List<CPQ_Approval_Retrigger_Field__c> testRetriggers = new List<CPQ_Approval_Retrigger_Field__c>
        {
            new CPQ_Approval_Retrigger_Field__c(SObject__c = 'Agreement', Field_API_Name__c = 'Duration__c'),
            new CPQ_Approval_Retrigger_Field__c(SObject__c = 'Agreement', Field_API_Name__c = 'RecordTypeId')
        };
        insert testRetriggers;

        Test.setMock(WebServiceMock.class, new Test_CPQ_WebSvcCalloutMock());
        
        system.debug('Apttus__APTS_Agreement__c.class ' + testAccounts[1].ID);

        testAgreements = new List<Apttus__APTS_Agreement__c>
        {
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'COOP_Plus_Program_AG').Id, Apttus__Status__c = 'Requested',  Apttus_Approval__Approval_Status__c = 'Approved', Apttus__Account__c = testAccounts[1].ID, Duration__c = '1 1/2 Years'),
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement 2', RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Hardware_Quote_Direct_AG').Id, Apttus__Status__c = 'Requested', Apttus_Approval__Approval_Status__c = 'Approved', Apttus__Account__c = testAccounts[1].ID, Duration__c = '1 Years')
        };
    }

    static testMethod void testAgreementStatus() {
        System.Debug('*** testAgreementStatus ***');
        createTestData();

        Test.startTest();
            insert testAgreements;

            Apttus_Agreement_Trigger_Manager.ResetApprovalStatusWhenFieldChanged_Exec_Num ++;
            for (Apttus__APTS_Agreement__c a : testAgreements)
                a.Duration__c = '3 Years';
            update testAgreements;

            Apttus_Agreement_Trigger_Manager.UpdateStatusWhenCancelled_Exec_Num ++;
            for (Apttus__APTS_Agreement__c a : testAgreements)
                a.Apttus_Approval__Approval_Status__c = 'Cancelled';
            update testAgreements;

            Apttus_Agreement_Trigger_Manager.UpdateRecordType_Exec_Num ++;
            for (Apttus__APTS_Agreement__c a : testAgreements)
                a.Apttus__Status__c = 'Activated';
            try {
                update testAgreements;
            } catch (Exception e){}

            testAgreements[0].Apttus__Status__c = 'Requested';
            update testAgreements[0];

            Apttus_Agreement_Trigger_Manager.SendToPartner_Exec_Num ++;
            testAgreements[0].Apttus__Status__c = 'Activated';
            try {
                update testAgreements[0];
            } catch (Exception e){}
        Test.stopTest();
    }

     static void createTestData2()
     {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.ENABLED);

        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

        Account testGPO = new Account(Name = 'Test GPO', Status__c = 'Active', Account_External_ID__c = 'USGG-SG0150', RecordTypeID = gpoRT.getRecordTypeId());
        insert testGPO;

        List<Account> testAccounts = new List<Account> {
            new Account(Status__c = 'Active', Name = 'Test Account',        Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-111111'),

            new Account(Status__c = 'Active', Name = 'Facilitiy 1',         Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-222221'),
            new Account(Status__c = 'Active', Name = 'Facilitiy 2',         Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-222222'),
            new Account(Status__c = 'Active', Name = 'Facilitiy 3',         Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-222223'),

            new Account(Status__c = 'Active', Name = 'Distributor 1',       Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-333331'),
            new Account(Status__c = 'Active', Name = 'Distributor 2',       Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-333332'),
            new Account(Status__c = 'Active', Name = 'Distributor 3',       Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-333333'),

            new Account(Status__c = 'Active', Name = 'Test Invalid CRN',    Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId()),
            new Account(Status__c = 'Active', Name = 'Test Invalid CRN 2',  Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-DUMMY')
        };
        insert testAccounts;

        Opportunity testOpp = new Opportunity(Name = 'Test Opp', AccountID = testAccounts[0].ID, StageName = 'Identify', CloseDate = System.today());
        insert testOpp;

        Partner_Root_Contract__c testRoot = new Partner_Root_Contract__c(Name = '011111', Root_Contract_Name__c = '011111', Root_Contract_Number__c = '011111', Direction__c = 'I', Effective_Date__c = System.Today());
        insert testRoot;

        Apttus__APTS_Agreement__c testOldAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Old Agreement for REW/ADJ to Send to Partner',
                RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'COOP_Plus_Program_AG').Id,
                Apttus__Account__c = testAccounts[0].ID,
                Partner_Root_Contract__c = testRoot.ID,
                Compliance_Amount_1__c = 100,
                Compliance_Amount_2__c = 200,
                Equipment_Lines_Subtotal__c = 1000,
                Total_Annual_Sensor_Commitment_Amount__c = 10000,
                Duration__c = '1 1/2 Years');
        insert testOldAgreement;

        Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c = testAccounts[0].ID, Apttus_QPComply__MasterAgreementId__c = testOldAgreement.ID, Deal_Type__c = 'Amendment', T_AMD_Equipment__c = TRUE, Other_Non_Standard_Term_Requests__c = true, Other_Non_Standard_Value_Details__c = 'Details', Other_Non_Standard_Request_Details__c = 'Details');
        insert testProposal;

        testAgreements = new List<Apttus__APTS_Agreement__c> {
            new Apttus__APTS_Agreement__c(Name = 'Test Amend To Add Facilities',
                RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Amendment_to_Add_Facilities_AG').Id,
                Apttus__Status__c = 'Requested',
                Apttus_Approval__Approval_Status__c = 'Requested',
                Apttus__Account__c = testAccounts[0].ID,
                Apttus_QPComply__RelatedProposalId__c = testProposal.ID,
                Deal_Type__c = 'Amendment'),
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement',
                RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'COOP_Plus_Program_AG').Id,
                Apttus__Status__c = 'Requested',
                Apttus_Approval__Approval_Status__c = 'Requested',
                Apttus__Account__c = testAccounts[0].ID,
                Apttus_QPComply__RelatedProposalId__c = testProposal.ID,
                Deal_Type__c = 'Amendment',
                Compliance_Amount_1__c = 200,
                Compliance_Amount_3__c = 300,
                Equipment_Lines_Subtotal__c = 2000,
                Total_Annual_Sensor_Commitment_Amount__c = 20000,
                Duration__c = '3 1/2 Years'),
            new Apttus__APTS_Agreement__c(Name = 'Test Agreement Not going to Partner',
                RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Outright_Quote_Hardware_AG').Id,
                Apttus__Status__c = 'Requested',
                Apttus_Approval__Approval_Status__c = 'Requested',
                Apttus__Account__c = testAccounts[0].ID,
                Apttus_QPComply__RelatedProposalId__c = testProposal.ID,
                Deal_Type__c = 'New',
                Compliance_Amount_1__c = 200,
                Compliance_Amount_3__c = 300),
            //no name
            new Apttus__APTS_Agreement__c(RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),

            //account
            new Apttus__APTS_Agreement__c(Name = 'Test No Account',     RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),
            new Apttus__APTS_Agreement__c(Name = 'Test Invalid CRN',    Apttus__Account__c = testAccounts[7].ID, Apttus__Related_Opportunity__c = testOpp.ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),
            new Apttus__APTS_Agreement__c(Name = 'Test Invalid CRN 2',  Apttus__Account__c = testAccounts[8].ID, Apttus__Related_Opportunity__c = testOpp.ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),

            //no opp
            new Apttus__APTS_Agreement__c(Name = 'Test No Opp OK',      Apttus__Account__c = testAccounts[0].ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Hardware_Quote_Direct_AG').Id),
            //no opp, has summary group > 15000
            new Apttus__APTS_Agreement__c(Name = 'Test No Opp not OK',  Apttus__Account__c = testAccounts[0].ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),

            //no cart
            new Apttus__APTS_Agreement__c(Name = 'Test No Cart',        Apttus__Account__c = testAccounts[0].ID, Apttus__Related_Opportunity__c = testOpp.ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),
            new Apttus__APTS_Agreement__c(Name = 'Test No Final Cart',  Apttus__Account__c = testAccounts[0].ID, Apttus__Related_Opportunity__c = testOpp.ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id),
            new Apttus__APTS_Agreement__c(Name = 'Test Final Cart',     Apttus__Account__c = testAccounts[0].ID, Apttus__Related_Opportunity__c = testOpp.ID, RecordTypeID = RecordType_u.fetch(Apttus__APTS_Agreement__c.class, 'Sales_and_Pricing_Agreement_AG').Id)
        };
        insert testAgreements;
 
        List<Product2> testProducts = cpqProduct2_TestSetup.create();
        insert testProducts;
            
        List<Apttus_Config2__PriceListItem__c> testPriceListItems =
            cpqPriceListItem_u.fetch(
                Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
                testPriceList.Id).values();

        system.assert(!testPriceListItems.isEmpty(), testProducts);
             
        List<Apttus_Config2__ClassificationHierarchy__c> testCategoryHierarchies =
            cpqClassificationHierarchy_TestSetup.loadData().values();

        List<Apttus__AgreementLineItem__c> testLines = new List<Apttus__AgreementLineItem__c> {
        //old Agreeemnt
            //hardware
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testOldAgreement.ID, Apttus__ProductId__c = testProducts[0].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[0].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1, Apttus_CMConfig__ChargeType__c = 'Hardware', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Case'),
            //sensors
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testOldAgreement.ID, Apttus__ProductId__c = testProducts[1].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[1].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 2, Apttus_CMConfig__ChargeType__c = 'Consumable', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Each'),
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testOldAgreement.ID, Apttus__ProductId__c = testProducts[1].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[1].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 3, Apttus_CMConfig__ChargeType__c = 'Consumable', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 20, Apttus_CMConfig__Uom__c = 'Each'),
            //in old, not in new
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testOldAgreement.ID, Apttus__ProductId__c = testProducts[2].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[2].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 4, Apttus_CMConfig__ChargeType__c = 'Consumable', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 1, Apttus_CMConfig__Uom__c = 'Each'),
            //primaryline = false
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = false, Apttus__AgreementId__c = testOldAgreement.ID, Apttus__ProductId__c = testProducts[0].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[0].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1, Apttus_CMConfig__ChargeType__c = 'Hardware', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Case'),

        //test amend to add facility
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testAgreements[0].ID, Apttus__ProductId__c = testProducts[0].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[0].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1, Apttus_CMConfig__ChargeType__c = 'Hardware', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Case'),

        //new Agreement
            //hardware
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testAgreements[1].ID, Apttus__ProductId__c = testProducts[0].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[0].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1, Apttus_CMConfig__ChargeType__c = 'Hardware', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Case'),
            //sensors
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testAgreements[1].ID, Apttus__ProductId__c = testProducts[1].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[1].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 2, Apttus_CMConfig__ChargeType__c = 'Consumable', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 20, Apttus_CMConfig__Uom__c = 'Each'),
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testAgreements[1].ID, Apttus__ProductId__c = testProducts[1].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[1].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 3, Apttus_CMConfig__ChargeType__c = 'Consumable', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 20, Apttus_CMConfig__Uom__c = 'Each'),
            //in new, not in old
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testAgreements[1].ID, Apttus__ProductId__c = testProducts[3].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[3].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 4, Apttus_CMConfig__ChargeType__c = 'Consumable', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 1, Apttus_CMConfig__Uom__c = 'Each'),
            //primaryline = false
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = false, Apttus__AgreementId__c = testAgreements[1].ID, Apttus__ProductId__c = testProducts[0].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[0].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1, Apttus_CMConfig__ChargeType__c = 'Hardware', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Case'),

        //Agreement not going to partner
            new Apttus__AgreementLineItem__c(Apttus_CMConfig__IsPrimaryLine__c = true, Apttus__AgreementId__c = testAgreements[2].ID, Apttus__ProductId__c = testProducts[0].ID, Apttus_CMConfig__PriceListItemId__c = testPriceListItems[0].ID, Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1, Apttus_CMConfig__ChargeType__c = 'Hardware', Apttus_CMConfig__PriceType__c = 'One Time', Apttus__Quantity__c = 1, Apttus__NetPrice__c = 10, Apttus_CMConfig__Uom__c = 'Case')
        };
        insert testLines;

        List<Agreement_Participating_Facility__c> testFacilities = new List<Agreement_Participating_Facility__c> {
            new Agreement_Participating_Facility__c(Agreement__c = testOldAgreement.ID, Account__c = testAccounts[1].ID),
            new Agreement_Participating_Facility__c(Agreement__c = testOldAgreement.ID, Account__c = testAccounts[2].ID),

            new Agreement_Participating_Facility__c(Agreement__c = testAgreements[1].ID, Account__c = testAccounts[1].ID),
            new Agreement_Participating_Facility__c(Agreement__c = testAgreements[1].ID, Account__c = testAccounts[3].ID)
        };
        insert testFacilities;

        List<Agreement_Distributor__c> testDistributors = new List<Agreement_Distributor__c> {
            new Agreement_Distributor__c(Agreement__c = testOldAgreement.ID, Account__c = testAccounts[1].ID),
            new Agreement_Distributor__c(Agreement__c = testOldAgreement.ID, Account__c = testAccounts[2].ID),

            new Agreement_Distributor__c(Agreement__c = testAgreements[1].ID, Account__c = testAccounts[1].ID),
            new Agreement_Distributor__c(Agreement__c = testAgreements[1].ID, Account__c = testAccounts[3].ID)
        };
        insert testDistributors;

        Apttus_CMConfig__AgreementSummaryGroup__c testSummaryGroup = new Apttus_CMConfig__AgreementSummaryGroup__c(Apttus_CMConfig__AgreementId__c = testAgreements[7].ID, Apttus_CMConfig__NetPrice__c = 20000, Apttus_CMConfig__LineType__c = 'Grand Total', Apttus_CMConfig__ItemSequence__c = 1, Apttus_CMConfig__LineNumber__c = 1);
        insert testSummaryGroup;

        List<Apttus_Config2__ProductConfiguration__c> testConfigs = new List<Apttus_Config2__ProductConfiguration__c> {
            new Apttus_Config2__ProductConfiguration__c(Apttus_CMConfig__AgreementId__c = testAgreements[10].ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__Status__c = 'Saved', Apttus_Config2__VersionNumber__c = 1),
            new Apttus_Config2__ProductConfiguration__c(Apttus_CMConfig__AgreementId__c = testAgreements[11].ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__Status__c = 'Finalized', Apttus_Config2__VersionNumber__c = 1)
        };
        insert testConfigs;

        Test.setMock(WebServiceMock.class, new Test_CPQ_WebSvcCalloutMock());
    }

    @isTest
    static void testInvokePartnerQueueWS()
    {
        createTestData2();

        Test.startTest();
            CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement(testAgreements[0].ID);
            CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement(testAgreements[1].ID);
            CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement(testAgreements[2].ID);

            CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement_LetExp(testAgreements[1].ID);
            CPQ_PartnerContractWSInvoke.InvokePartnerQueueWSFromAgreement_LetExp(testAgreements[2].ID);
        Test.stopTest();
    }

    @isTest 
    static void testInvokePartnerCompliant()
    {
        createTestData2();

        Test.startTest();
            CPQ_Controller_ProposalValidation p = new CPQ_Controller_ProposalValidation(new ApexPages.StandardController(testAgreements[3]));
            System.Debug(p.validationMessages);
            p.doValidationLite();
            p.getIsActivateAgreement();
            System.Debug(p.showCloseButton);

            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[3].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[4].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[5].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[6].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[7].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[8].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[9].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[10].ID);
            CPQ_PartnerContractWSInvoke.InvokeComplaintCheckFromAgreement(testAgreements[11].ID);
        Test.stopTest();
    }

    static testMethod void TestDisabledTrigger() {
        createTestData();

        Test.startTest();
            try {
                CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId  = cpqUser_c.CurrentUser.Id, CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Participating_Facility_After__c = true, CPQ_Product2_After__c = true, CPQ_Product2_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_ProductConfiguration_Before__c = true, CPQ_Proposal_After__c = true, CPQ_Proposal_Before__c = true, CPQ_Proposal_Distributor_After__c = true, CPQ_Rebate_After__c = true);
                insert testTrigger;
            } catch (Exception e) {}

            insert testAgreements;

            for (Apttus__APTS_Agreement__c a : testAgreements)
                a.Duration__c = '3 Years';
            update testAgreements;
       Test.stopTest();
    }
}