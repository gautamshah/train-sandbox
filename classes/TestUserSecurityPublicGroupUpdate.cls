@isTest
private class TestUserSecurityPublicGroupUpdate {

    static testMethod void runTest() {
    
    List<User> updateUserList = new List<User>();
    for (List<User> userList : [SELECT id, User_Visibility__c from User where user.UserType = 'Standard' and IsActive = TRUE and Region__c = 'US' and User_Visibility__c != 'Region' LIMIT 200]){
        for(User u : userList){
            u.User_Visibility__c = 'Region';
            updateUserList.add(u);
        }
    }
    
    update updateUserList;
        
        /*
        List<User> userList= new List<User>();
        for (User u : [SELECT Id, Region__c from User where CompanyName = 'Salesforce.com' and Department='COE']){
            u.Region__c = 'EU';
            u.User_Visibility__c = 'Region';
            userList.add(u);
        }
        
        for (User uc : [SELECT Id, Region__c from User where CompanyName = 'Salesforce.com' and Department='COE-CONSTRAINED']){
            uc.Region__c = 'EU';
            uc.User_Visibility__c = 'Constrained'; 
            userList.add(uc);
        }
        
        update userList;
        */
        
        User userToInsert = new User();
        userToInsert.FirstName = 'John';
        userToInsert.LastName = 'Test3';
        userToInsert.Alias = 'jtest3';
        userToInsert.Email = 'john.hogan@salesforce.com';
        userToInsert.Username = 'john.test3@covidien.com.securefull';
        userToInsert.CommunityNickname = 'jtest3cn';
        userToInsert.ProfileId = '00eU0000000dLswIAE';
        userToInsert.Business_Unit__c = 'Corporate';
        userToInsert.Region__c = 'EU';
        userToInsert.User_Visibility__c = 'Region';
        userToInsert.CompanyName = 'Salesforce.com';
        userToInsert.TimeZoneSidKey = 'America/New_York';
        userToInsert.LanguageLocaleKey= 'en_US';
        userToInsert.LocaleSidKey = 'en_US';
        userToInsert.EmailEncodingKey = 'ISO-8859-1';
    
    
        insert userToInsert;
    /*
    First Name (FirstName) John
    Last Name (LastName) Test1
    Alias (Alias) --> jtest1
    Email (Email) --> john.hogan@salesforce.com
    Username (Username) --> john.test1@covidien.com.securefull
    Community Nickname (CommunityNickname) --> jtest1cn
    Profile (ProfileId) --> EU All --> 00eU0000000dLswIAE
    Business Unit (Business_Unit__c) --> 'Corporate'
    Region (Region__c) --> 'EU'
    Company (CompanyName) --> 'Salesforce.com'
    TimeZoneSidKey --> 'America/New_York'
    LocaleSidKey --> 'en_US'
    EmailEncodingKey --> 'ISO-8859-1'
    LanguageLocalKey --> 'LanguageLocaleKey'
    */
    
    }
}