/**
 * Schedulable class to call the batch job that updates product images
 * for surgical products for the Apttus catalog. Due to HTTP callouts
 * for each row in the batch, the batch size should always be set lower
 * than 100.
 */
global class CPQ_SSG_ScheduleUpdateProductImages implements Schedulable {
	public static final Integer BATCH_SIZE = 100;
	global void execute(SchedulableContext sc) {
		CPQ_SSG_BatchUpdateProductImages batch = new CPQ_SSG_BatchUpdateProductImages();
		Database.executeBatch(batch, BATCH_SIZE);
	}
}