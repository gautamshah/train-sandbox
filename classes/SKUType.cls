global class SKUType{
        webservice String SKUCode{get;set;}
        webservice String SalesClass{get;set;}
        webservice String UOM{get;set;}
        webservice String SKUErrorDescription{get;set;}
        webservice String SKUError{get;set;}
        webservice List<PriceType> Price{get;set;}
    }