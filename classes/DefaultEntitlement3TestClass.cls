@isTest(SeeAllData=true)
private class DefaultEntitlement3TestClass {


  public static testmethod void mytest() 
   {
 //static methods  Test.startTest and Test.stopTest  allow a test method to separate the Apex resources and governor limits being used to prepare and initialize the dataset from the resources and limits used during the actual test execution.
   Test.startTest();
   List<Case> lsCases = new List<Case>();
   Recordtype cr= [SELECT Id, Name FROM RecordType WHERE SobjectType = 'case' and name='CRM Support'];
   Entitlement e= [select id from Entitlement where Name='Case SLA Medium' limit 1]; 
    Case c= new Case(status='new',EntitlementId=e.id, Admin_Priority__c='Case SLA Medium', RecordTypeid=cr.id, Object__c='case',Functional_Role__c='Admin', Type='Problem', Subject='test');
    c.EntitlementId  = e.id;   
    //insert c;  
       lsCases.add(c); 
       Case c1= new Case(status='new', Admin_Priority__c='Low', RecordTypeid=cr.id, Case_Owner_EQUALS_COE_Support__c=false, Object__c='case',Functional_Role__c='Admin', Type='Problem', Subject='test');
       Case c2= new Case(status='new', Admin_Priority__c='High', RecordTypeid=cr.id,Object__c='case',Functional_Role__c='Admin', Type='Problem', Subject='test1');
       Case c3= new Case(status='new', Admin_Priority__c='Critical', RecordTypeid=cr.id,Object__c='case',Functional_Role__c='Admin', Type='Problem', Subject='test2'); 
       Case c4= new Case(status='new', Admin_Priority__c='Medium', RecordTypeid=cr.id,Object__c='case',Functional_Role__c='Admin', Type='Problem', Subject='test3');
       Case c5= new Case(status='new',EntitlementId=e.id, Admin_Priority__c='Case SLA Medium', RecordTypeid=cr.id, Object__c='case',Functional_Role__c='Admin', Type='Problem', Subject='test'); 
   
    if(c1.Admin_Priority__c == 'Low')
   { Entitlement   e1 = [select id from Entitlement where Name='Case SLA Low' limit 1];
    c1.EntitlementId  = e1.id;
    //insert c1;
       lsCases.add(c1); 
   }
    if(c2.Admin_Priority__c == 'High')
   { Entitlement   e2 = [select id from Entitlement where Name='Case SLA High' limit 1];
    c2.EntitlementId  = e2.id;
    //insert c2;
    
       lsCases.add(c2); }
   
    if(c3.Admin_Priority__c == 'Critical')
    {
    Entitlement   e3 = [select id from Entitlement where Name='Case SLA Critical' limit 1];
    c3.EntitlementId  = e3.id;
       lsCases.add(c3); 
    //insert c3;
    }
    
    if(c4.Admin_Priority__c == 'Medium')
    {
      Entitlement    e4 = [select id from Entitlement where name=:'Case SLA Medium'];
      c4.EntitlementId  = e.id;
       lsCases.add(c4); 
      //insert c4;
     }
 
     group o=new group(); //creating group object
        o= [SELECT Id, Name FROM Group WHERE Name = 'CoE - Support' limit 1];      
     if (c5.ownerid== o.id)   
         {
      c5.Case_Owner_EQUALS_COE_Support__c=true;
       //insert c;
       lsCases.add(c5); 
         }
         insert lsCases;
      Test.stopTest();
}

}