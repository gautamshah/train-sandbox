@istest
private class CustomOpptyMyApprovalsControllerTest
{
    @istest(seealldata=true)
    static void method_One()
    {

        User usr = cpqUser_TestSetup.getSysAdminUser();
        if (usr == null) {
            usr = cpqUser_c.CurrentUser;
        }

        cpqOpportunity_TestSetup.buildScenario_CPQ_Opportunity();
        Account acc = cpq_TestSetup.testAccounts.get(0);
        Contact ct = cpq_TestSetup.testContacts.get(0);
        Opportunity opp = cpq_TestSetup.testOpportunities.get(0);

        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_SOBJECTID,opp.Id);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_SOBJECTTYPE,'Opportunity');
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNID,opp.Id);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNPAGE,'/'+opp.Id);

        //Apttus_Approval.MyApprovalsController baseController;

        Test.startTest();
        CustomOpptyMyApprovalsController ctrl = new CustomOpptyMyApprovalsController(null);
        ctrl = new CustomOpptyMyApprovalsController();
        ctrl.setInSf1Mode(True);
        ctrl.getInSf1Mode();
        ctrl.getPageLoaded();
        ctrl.getPageURL();
        ctrl.doLoadMyOpptyApprovals();
        //ctrl.doLoadMyApprovals();
        ctrl.doMyDone();

        //added to increase coverage
        ctrl.setInSf1Mode(false);
        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNPAGE,'');
        ctrl = new CustomOpptyMyApprovalsController();
        ctrl.doLoadMyOpptyApprovals();

        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNID,null);
        ctrl = new CustomOpptyMyApprovalsController();
        ctrl.doMyDone();

        ApexPages.currentPage().getParameters().put(CustomApprovalsConstants.PARAM_RETURNPAGE,'/'+opp.Id);
        ctrl = new CustomOpptyMyApprovalsController();
        ctrl.doMyDone();
        Test.stopTest();

    }

}