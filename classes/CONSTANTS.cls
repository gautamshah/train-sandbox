global class CONSTANTS
{
	global static final string CRLF = '\r\n';
	
	global static final string NBSP = '+'; //EncodingUtil.urlEncode('&nbsp;', 'UTF-8');
	
	global static string NBSP(integer count)
	{
		string line = '';
		for(integer i = 0; i < count; i++)
			line += NBSP;
			
		return line;
	}
	
	global static final integer intTRUE = 1;
	global static final integer intFALSE = 0;
}