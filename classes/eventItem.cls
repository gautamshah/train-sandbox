public class eventItem {
    public EMS_Event__c ev;
    public String formatedDate; 
    public eventItem(EMS_Event__c e) { 
        ev= e;
        // build formated date
        //9:00 AM - 1:00 PM
    //  system.debug(e.activitydatetime.format('MMM a'));
    //  system.debug(e.DurationInMinutes);
    Integer totalminutes=Integer.valueOf(e.DurationInMinutes__c);
        Datetime endd = e.Start_Date__c.addMinutes(totalminutes);
        //system.debug(e.activitydatetime.format('h:mm a '));
        formatedDate = e.Start_Date__c.format('MM/dd/yyyy h:mm a') + 
        ' - ' + e.End_Date__c.format('MM/dd/yyyy h:mm a');
        system.debug(formateddate);
    }
    public EMS_Event__c getEv() { return ev; }
    public String getFormatedDate() { return formatedDate; }
}