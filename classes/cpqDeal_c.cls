/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A               AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies
20170123        IL         AV-280     Commented out isRecordDestinedToPartner method to increase test coverage

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
public virtual class cpqDeal_c extends sObject_c
{
	protected cpqDeal_c() 
	{
		super();
	}
	
	public cpqDeal_c(sObject sobj)
	{
		super(sobj);
	}
	
	protected RecordType type;
	
	protected virtual boolean isDestinedForPartner() { return false; }
	
    // 2017.01.23 - IL - Method isn't called anywhere, and logic doesn't branch correctly. Commenting out to increase
    //                   code coverage
	
    public static Map<sObjectField, sObjectField> ProposalToAgreementFieldMap =
    	new Map<sObjectField, sObjectField>
    {
		cpqProposal_c.FIELD_ElectrosurgeryEaches => cpqAgreement_c.FIELD_ElectrosurgeryEaches
    };

    public static Map<sObjectField, sObjectField> RoyaltyFreeAgreement =
    	new Map<sObjectField, sObjectField>
    {
		cpqProposal_c.FIELD_ElectrosurgeryEaches 	=> cpqAgreement_c.FIELD_ElectrosurgeryEaches,
    	cpqProposal_c.FIELD_ElectrosurgerySales		=> cpqAgreement_c.FIELD_ElectrosurgerySales,
    	cpqProposal_c.FIELD_ElectrosurgeryIncrementalSpend		=> cpqAgreement_c.FIELD_ElectrosurgeryIncrementalSpend,
    	cpqProposal_c.FIELD_ElectrosurgeryIncrementalQuanitity	=> cpqAgreement_c.FIELD_ElectrosurgeryIncrementalQuanitity,
    	cpqProposal_c.FIELD_ElectrosurgeryIncrementalTotalSpend	=> cpqAgreement_c.FIELD_ElectrosurgeryIncrementalTotalSpend,
    	cpqProposal_c.FIELD_LigasureEaches			=> cpqAgreement_c.FIELD_LigasureEaches,
    	cpqProposal_c.FIELD_LigasureSales			=> cpqAgreement_c.FIELD_LigasureSales,
    	cpqProposal_c.FIELD_LigasureIncrementalSpend			=> cpqAgreement_c.FIELD_LigasureIncrementalSpend,
    	cpqProposal_c.FIELD_LigasureIncrementalQuanitity 		=> cpqAgreement_c.FIELD_LigasureIncrementalQuanitity,
    	cpqProposal_c.FIELD_LigasureIncrementalTotalSpend 		=> cpqAgreement_c.FIELD_LigasureIncrementalTotalSpend,
    	cpqProposal_c.FIELD_IncrementalTotal					=> cpqAgreement_c.FIELD_IncrementalTotal

    };

    public static void copy(Apttus_Proposal__Proposal__c p, Apttus__APTS_Agreement__c a, Map<sObjectField, sObjectField> fields)
    {
    	for(sObjectField s : fields.keySet())
    	{
    		sObjectField t = fields.get(s);
    		if (!t.getDescribe().isCalculated())
    			a.put(t, p.get(s));
    	}
    }
}