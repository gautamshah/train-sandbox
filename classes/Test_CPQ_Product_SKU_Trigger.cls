/**
Test class for the CPQ_Product_SKU_After trigger

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
04/18/2016      Bryan Fry           Created
===============================================================================
*/
@isTest
public with sharing class Test_CPQ_Product_SKU_Trigger {
	static testMethod void Test_CPQ_Product_SKU_Trigger() {
		Product2 surgicalProduct = new Product2(ProductCode = 'Test_Coverage_Product', Name = 'Test_Coverage_Product', Apttus_Surgical_Product__c = true);
		Product2 nonSurgicalProduct = new Product2(ProductCode = 'Test_Coverage_Product', Name = 'Test_Coverage_Product', Apttus_Surgical_Product__c = false);

		List<Product2> products = new List<Product2>();
		products.add(surgicalProduct);
		products.add(nonSurgicalProduct);
		insert products;

		Test.startTest();
			Product_SKU__c usProductSKU = new Product_SKU__c(SKU__c = 'Test_Coverage_Product', Country__c = 'US');
			Product_SKU__c caProductSKU = new Product_SKU__c(SKU__c = 'Test_Coverage_Product', Country__c = 'CA');
			List<Product_SKU__c> productSKUs = new List<Product_SKU__c>();
			productSKUs.add(usProductSKU);
			productSKUs.add(caProductSKU);
			insert productSKUs;

			surgicalProduct = [Select Id, Product_SKU__c From Product2 Where Id = :surgicalProduct.Id];
			nonSurgicalProduct = [Select Id, Product_SKU__c From Product2 Where Id = :nonSurgicalProduct.Id];

			// Surgical products should get linked to the US version of Product SKU records
			System.assertEquals(surgicalProduct.Product_SKU__c, usProductSKU.Id);

			// Non-surgical products should not get linked to any Product SKU records.
			System.assertEquals(nonSurgicalProduct.Product_SKU__c, null);
		Test.stopTest();
	}
}