@istest
Class EventPhoneNumberatAccountUpdate_Test {
/****************************************************************************************
    * Name    : EventPhoneNumberatAccountUpdate_Test   
    * Author  : Lakhan Dubey    
    * Date    : 13-2-2015    
    * Purpose :  Test class for  trigger 'EventPhoneNumberatAccountUpdate ' 
            
    * ========================    
    * = MODIFICATION HISTORY =    
    * ========================    
    * 
    *   
    * *****************************************************************************************/
static testmethod void CreateAnzEvent () {
      Profile p =[SELECT Id FROM Profile WHERE Name='ANZ - All'];             
      User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, Region__c='ANZ',Business_Unit__c='SGD',User_Role__c='Rep',Function__c='Sales',User_Visibility__c='Region',
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduserre@testorg.com');
      insert u;
      
      Account a=new Account();
      a.name='testacc';
      a.CurrencyIsoCode='AUD';
      a.OwnerId = u.Id;
      insert a;
    
   
      Contact c=new Contact();
      c.firstname='testname';
      c.Salutation = 'Mr.';
      c.lastname='testname';
      c.Specialty1__c='Abdominal Radiology';
      c.AccountId=a.Id;
      c.CurrencyIsoCode='AUD';
      c.Department_picklist__c='Administration';
      c.Affiliated_Role__c='Allied Health Practitioner';
      c.Phone_Number_at_Account__c='9090';
      c.OwnerId = u.Id;
      c.phone='100';
      insert c;
      
     
      Id eventId = null;
      system.runas(u){
      Event e=new Event();
      e.Subject='Free text';
      e.Activity_Type__c='Case';
      e.ANZ_Business_Unit__c='Early Technology';
      e.WhoId=c.Id;
      e.DurationInMinutes=36;
      e.ActivityDateTime=system.now()+30;
      e.Phone_number_at_account__c='9090';
      Test.starttest();
      insert e;
      eventId = e.Id;
      Test.stoptest();

                      }
System.assertEquals('9090',[select Phone_number_at_account__c from Event where Id =:eventId  limit 1].Phone_number_at_account__c);
}
}