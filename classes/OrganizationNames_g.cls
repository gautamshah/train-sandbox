/**

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016/10/21      Paul Berglund       Created.
===============================================================================
*/

global class OrganizationNames_g
{
	// Supports Organization Names global value list
  	// Contains any static data used globally

  	///////////////////////////////////////////////////////////////////////////
  	// Everything below here is so we can support the old code until
  	// we can refactor all the places using
  	//     getRMSPriceListID
  	//     getSSGPriceListID
  	//     getMSPriceListID
  	//
  	// methods in CPQ_Utilities
  	//
	global enum Abbreviations
 	{
 		MS,
 		RMS,
 		SSG
 	}

	//
	// https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_methods_system_enum.htm
	//
	private static Map<string, Abbreviations> string2enum =
		new Map<string, Abbreviations>();
		
	static
	{
		for(Abbreviations abbr : Abbreviations.values())
			string2enum.put(abbr.name(), abbr); 
	}

	global static Abbreviations valueOf(string str)
	{
		if (string2enum.containsKey(str))
			return string2enum.get(str);
			
		return null;
	}
}