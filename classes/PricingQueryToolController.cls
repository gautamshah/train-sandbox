public without sharing class PricingQueryToolController {
    /****************************************************************************************
     * Name    : PricingQueryToolController  
     * Author  : Nathan Shinn
     * Date    : 7/25/2011  
     * Purpose : interface for searching products for input into a web service callout that 
     *           retrieves pricing for each product.
     * Dependencies: Account Object
     *              ,Product_SKU__c Object
     *              ,User Object
     *              ,ERP_Account__c Object 
     *              ,TEB Web Service
     *
     * Development Web Service Endpoint:
     * Production  Web Service Endpoint:
     * MH44924,3531,3532,6176LL,6256LF,3515,6256,8989898,6175,6255
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 07/25/2011  Nathan Shinn         Created
     * 08/22/2011  Nathan Shinn         Revised to accommodate new web service Definition
     * 09/01/2011  Nathan Shinn         Revised to support Kit Quotes
     * 12/12.2011  Nathan Shinn         Revised for Async Web service
     * 03/22/2011  Mike Melcher         Update search for ERP Account in getQueryResults to use locationId instead of location.Id
     *                                  This prevents the ERP Account from being "Forgotten" when the record changes but a new 
     *                                  product search isn't done.
     * 03/22/2011  Mike Melcher         Modify the search query to do a "like" search on SKUs instead of an exact match on SKUs in the 
     *                                  set of values entered by the user
     *****************************************************************************************/
    public boolean isTest = false; //used to skip callout method durring testing
    public Account sellToAccount{get;set;}
    public Product_Sku_Parameter__c productParameter{get;set;}
    public User user{get;set;}
    public Id kitId{get;set;}//used for KitQuote functionality
    public Kit__c kit{get;set;}
    Integer RESULTS_PER_PAGE = Integer.valueOf(PricingServiceVariables__c.getInstance('RESULTS_PER_PAGE').Value__c) ;
    Integer PRODUCT_SELECTION_LIMIT =  Integer.valueOf(PricingServiceVariables__c.getInstance('PRODUCT_SELECTION_LIMIT').Value__c);
    Integer SKU_LIMIT = Integer.valueOf(PricingServiceVariables__c.getInstance('SKU_LIMIT').Value__c);
    public Integer pageNum {get;set;}
    public Integer maxPages {get;set;}
    public map<integer,list<Id>> pageMap = new map<integer,list<Id>>();
    public ERP_Account__c location{get;set;}
    public string locationName{get;set;}
    public string locationId{get;set;}
    public string locSearch{get;set;}
    public Account acct{get;set;}
    map<String, Product_SKU__c> productSkuMap = new map<String, Product_SKU__c>();
    public List<webServiceResult> results{get;set;}
    public String PricingRequestID = '';
    public boolean resultsPending{get;set;}
    public boolean productsSelected{get{
                                          if( productSkuMap.size() > 0)
                                            return true;
                                          else
                                             return false;
                                      }set;
    
    }
    
    public class webServiceResult
    {   /** Class used to display the products and their associated pricing results 
            retrieved from the web service 
         **/
        public boolean checked{get;set;}
        //public pricingCovidienComProxy32.SKUType wsResult{get;set;}
        public SKUType wsResult{get;set;}
        public Product_SKU__c product{get;set;}
    }
    
    //list<webServiceResult> results;
    
    public class productResult
    {   /** Class used to generate a list of selectable Product_SKU Objects 
         **/
        public boolean checked{get;set;}
        public Product_SKU__c product{get;set;}
    }
    
    public list<productResult> ProductSearchResults{get;set;}
    public list<productResult> selectedProducts{get;set;}
    
    public PricingQueryToolController()
    {
        /** Constructor. Initializes the search parameter form components 
         **/
        kitId = ApexPages.currentPage().getParameters().get('kitid');
        if(kitId != null)
           kit = [Select Id, name, type__c 
                   from Kit__c 
                  where Id = :kitId ];
           
        //location = new ERP_Account__c();//[select Id, name, ERP_ID__c from ERP_Account__c limit 1];
        locationName = '';
        pageNum = 1;
        maxPages = 1;
        //sellToAccount = new Account(Account__c = ApexPages.currentPage().getParameters().get('accountId'), CapitalBudgetAvailable__c = System.Today());
        User u = [Select Business_Unit__c
                       , Country
                    from User 
                   where Id = :UserInfo.getUserId()];
        
        String accountId = ApexPages.currentPage().getParameters().get('account');
        
        
        /*  
        ProductParameter = new Product_Sku_Parameter__c(GBU__c = u.Business_Unit__c
                                             //         ,Country__c = u.Country
                                                      ,Account__c = accountId
                                                      ,SKU__c = ApexPages.currentPage().getParameters().get('sku')
                                                      ,Effective_Date__c  = System.Today());
        */                                              
       	ProductParameter = new Product_Sku_Parameter__c(Account__c = accountId
                                                      ,SKU__c = ApexPages.currentPage().getParameters().get('sku')
                                                      ,Effective_Date__c  = System.Today());
        
        ProductSearchResults =  new list<productResult>();
        selectedProducts =  new list<productResult>();
        
            
    }
    
    public pageReference searchProducts()
    {
        /** Ccalled from the 'Go' button on the search form.
            Collects the form parameters, runs validations and
            Queries the product skew Object creating a paginated
            list of productResult sub-class objects.
         **/
        try{
            location = [select id, External_ID__c, Name , ERP_ID__c
                          from ERP_Account__c
                          where Id = :locationId];
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, location.Name) );
            
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose a Ship-to account ['+ locationId +']'));
            return null;
        } 
         
        pageMap.clear();
        pageNum = 1;
        string nameLike = '%'+ (ProductParameter.name== null ?'':ProductParameter.name)+'%';
        string countryLike = '%'+ (ProductParameter.country__c== null ?'':ProductParameter.country__c)+'%';
        
        //string contactTypeLike = '%'+(ProductParameter.Contract_Type__c == null ? '' : ProductParameter.Contract_Type__c)+'%';
        
        //string franchiseLike = '%'+(ProductParameter.Franchise__c== null ?'':ProductParameter.Franchise__c)+'%';
        
        string gbuLike = '%'+(ProductParameter.GBU__c == null ?'':ProductParameter.GBU__c)+'%';
       
        //string groupLike = '%'+(ProductParameter.Group__c== null ?'':ProductParameter.Group__c)+'%';
        
        //retrieve the billiong country of the selected account to filter the product results
        acct = [Select Id 
                      , Name
                      , BillingCountry
                      , ParentId
                      , CapitalBudgetAvailable__c  
                   from Account 
                  where Id = :ProductParameter.Account__c];
        
        if(productParameter.name == null && ProductParameter.SKU__c == null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Pricing_Error_Product_Name_or_SKU) );
           return null;
        }
        
        
        List<String> SKUs = new List<String>();
        set<String> SKU_Set = new set<String>();
        if(ProductParameter.SKU__c != null)
           ProductParameter.SKU__c = ((String)ProductParameter.SKU__c).replace(' ','');
            
        if(ProductParameter.SKU__c != null)
        {
            
            SKUs = ProductParameter.SKU__c.split(',');
            SKU_Set.addAll(SKUs);
            
        }
        
        if(SKU_Set.size() > SKU_LIMIT)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Pricing_Too_many_SKUs_specified+' '+SKU_LIMIT) );
           return null;
        }
        
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ' >>>SKUs '+SKUs) );
        
        string skuLike = '%'+(ProductParameter.SKU__c == null ?'':ProductParameter.SKU__c)+'%';
        
        //Retrieve the product SKU objects from the database
        List<Product_SKU__c> productList = new List<Product_SKU__c>();
        if(SKU_Set.size() > 0)
        {
            string qs = 'Select Id,SKU__c, name,Franchise__c from Product_SKU__c ';
            qs += 'where name like \'' + nameLike + '\' ';
            qs += 'and (GBU__c like \'' + gbuLike + '\' or GBU__c = \'' + ProductParameter.GBU__c + '\' ) ';
            qs += 'and (Country__c = \'' + acct.BillingCountry + '\') ';
            qs += 'and (SKU__c like \'%' + SKUs[0] + '\' ';
            for (integer i=1; i<SKU_Set.size();i++) {
               qs+= 'or SKU__c like \'%' + SKUs[i] + '\' ';
            }
            qs += ') ';
            qs += 'order by name ';
            System.debug('Query for Product Sku lookup: ' + qs);
            System.debug('Sku_Set: ' + SKU_Set);  
            for (Product_SKU__c ps : Database.query(qs))                         
            //for(Product_SKU__c ps :[Select Id,SKU__c, name,Franchise__c
            //                          from Product_SKU__c
            //                         where name like :nameLike
            //                           and SKU__c in :SKU_Set
            //                           and (GBU__c like :gbuLike
            //                                or GBU__c = :ProductParameter.GBU__c)
            //                           and (Country__c = :acct.BillingCountry)
            //                          // and (Contract_Type__c like :contactTypeLike 
            //                          //      or Contract_Type__c = :ProductParameter.Contract_Type__c )
            //                         order by name
            //                         ]) 
            {
                productList.add(ps);
            }
        }
        else
        {
            for(Product_SKU__c ps :[Select Id,SKU__c, name, Franchise__c
                                      from Product_SKU__c
                                     where name like :nameLike
                                       and (GBU__c like :gbuLike
                                            or GBU__c = :ProductParameter.GBU__c)
                                       and (Country__c = :acct.BillingCountry)
                                       //and (Contract_Type__c like :contactTypeLike 
                                       //     or Contract_Type__c = :ProductParameter.Contract_Type__c )
                                      order by name
                                     ]) 
            {
                productList.add(ps);
            }
            System.debug('Added products to list based on product name...namelike: ' + nameLike + ' gbuLike: ' + gbuLike + ' prodgbu: ' + productparameter.gbu__c);
        }
                                         
        maxPages = math.round(productList.size()/RESULTS_PER_PAGE) == 0 ? 1 : math.round(productList.size()/RESULTS_PER_PAGE);
        
        Integer pageIndex = 0;
        Integer counter = 0;
        List<Id> idList;
        
        //create a paginated map of product SKU objects
        for(Product_SKU__c p : productList)
        {
            if(pageMap.get(pageIndex) == null || counter >=RESULTS_PER_PAGE )
            {
                  counter  = 1;
                  pageIndex ++;
                  idList = new list<Id>();
                  idList.add(p.Id);
                  pageMap.put(pageIndex, idList);
            }
            else if(pageMap.get(pageIndex) != null)
            {
                counter ++;
                idList.add(p.Id);
                pageMap.put(pageIndex, idList);
            }
            
        }
        
        ProductSearchResults = getProductSearchResult();
        return null;
    }
    
    public list<productResult> getProductSearchResult()
    {
        /** returns the list of results associated with the selectd page from the paginated map**/
        list<productResult> res = new list<productResult>();
        if(pageMap.get(pageNum) != null)
        {
            for(Product_SKU__c p : [select Id
                                         , name
                                         , SKU__c 
                                         , Group__c 
                                         , GBU__c 
                                         , Franchise__c 
                                         , Contract_Type__c 
                                         , country__c
                                         , UOM__c
                                         , Sales_Class__c
                                     from Product_SKU__c
                                    where Id in :pageMap.get(pageNum)
                                   order by name])
            {
                productResult pr = new productResult();
                pr.checked = false;
                pr.product = p;
                res.add(pr);
            } 
        }
        return res;
    }
    
    public void next()
    {
        if(pageNum < maxPages)
            pageNum ++;
            
        ProductSearchResults = getProductSearchResult();
    }
    
    public void previous()
    {
        if(pageNum > 1)
            pageNum --;
            
        ProductSearchResults = getProductSearchResult();
    }
    
    public void first()
    {
        pageNum = 1;
    }
    
    public void last()
    {
        pageNum = maxPages;
    }

    public void addSelectedProducts()
    {
        /** moves the products from the search results into the selected products list **/
        Integer totalSelected = selectedProducts.size();
        for(Integer i = 0;  i < ProductSearchResults.size(); i++)
        {
            if(ProductSearchResults.get(i).checked)
            {
                boolean selected = false;
                for(productResult sr : selectedProducts)
                {
                    if(sr.product.Id == ProductSearchResults.get(i).product.Id)
                      selected = true;
                }
                if(!selected)
                {
                  try{
                      totalSelected ++;
                      if(totalSelected > PRODUCT_SELECTION_LIMIT)
                      {
                        //constrain the allowable selections to a configurable limit
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR
                                                                 , Label.Pricing_Too_many_Products_Selected +' '+PRODUCT_SELECTION_LIMIT+' '+ Label.Pricing_Products) );
                        break;
                      }
                      selectedProducts.add(ProductSearchResults.get(i));
                      productSkuMap.Put(ProductSearchResults.get(i).product.SKU__c, ProductSearchResults.get(i).product);
                      ProductSearchResults.get(i).checked=false;
                      
                  }
                  catch(Exception e)
                  {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR '+ e.getMessage()+' -- '+i) );
                        break;
                  }
            }
        }
      }
    }
    
    public list<productResult> getSelectedProducts()
    {
        /**getter for the selected products list**/
        return selectedProducts;
    }
    
    public void removeProducts()
    {
        /** removes products from the selected products list **/
        set<Id> delList = new set<Id>();
        for(Integer i = 0;  i < selectedProducts.size(); i++)
        {
            if(selectedProducts.get(i).checked)
            {
                delList.add(selectedProducts.get(i).product.Id);
                productSkuMap.remove(selectedProducts.get(i).product.SKU__c);
                //ProductSearchResults.add(selectedProducts.get(i));
            }
        }
        for(Id x : delList)
        {
          Integer delIndex = -1;
          for(Integer i = 0;  i < selectedProducts.size(); i++)
          {
            if(selectedProducts.get(i).checked && selectedProducts.get(i).product.Id == x)
            {
              delIndex = i;
            }
          }
          
          if(delIndex > -1)
          {
            selectedProducts.remove(delIndex);
          }
        }
    }
    
    public List<ERP_Account__c> getLocations()
    {
           /** used to list the locations associated with the 
               selected account in a selectable pop-up list
            **/
           if(ApexPages.currentPage().getParameters().get('account') == '' || ApexPages.currentPage().getParameters().get('account') == '000000000000000'){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Pricing_Please_Select_an_account_erp) );
              return null;
           }
               
           return   [select id
                          , Zip_Postal_Code__c
                          , State_Region__c
                          , Name
                          , Country__c
                          , City__c
                          //, Address_Type__c
                          , Address_1__c
                          , Address_2__c
                          , Address_3__c
                          , Parent_Sell_To_Account__c
                          , ERP_ID__c
                          , ERP_Source__c
                          , COV_ERP_Total_Sales__c
                          , External_Id__c
                       from ERP_Account__c
                      where Parent_Sell_To_Account__c = :ApexPages.currentPage().getParameters().get('account')];
          
    }
    
    public PageReference getQueryResults()
    {
        /** enables the button that the user clicks to retrieve the 
            pricing results from the sweb service by selecting the account
            and navigating to the Pricing Query Results page (that uses the same controller)
         **/
        if(productSkuMap.size() == 0)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Pricing_No_Products_selected_Error) );
            return null;
        }
          
        try{
            location = [select id, Parent_Sell_To_Account__c , Name, ERP_ID__c
                          from ERP_Account__c
                          where Id = :locationId];
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Pricing_Please_choose_a_Ship_to_account) );
            return null;
        }
        resultsPending = true;
        //make the web service csall-out
        pricingCovidienComProxy3.GetPriceSoap service = new pricingCovidienComProxy3.GetPriceSoap();
        
        pricingCovidienComProxy3.ArrayOfSKURequestType pricingArray = new pricingCovidienComProxy3.ArrayOfSKURequestType();
        list<pricingCovidienComProxy3.SKURequestType> skuList = new list<pricingCovidienComProxy3.SKURequestType>();
        for(Product_SKU__c p :productSkuMap.values())
        { 
                pricingCovidienComProxy3.SKURequestType t = new pricingCovidienComProxy3.SKURequestType();
            
                t.SKUCode = p.SKU__c;
                t.Quantity = 1;
                t.UOM = p.UOM__c;
                
                skuList.add(t);
        }
        pricingArray.SKU = skuList; 
        pricingCovidienComProxy3.PricingRequestType req = new pricingCovidienComProxy3.PricingRequestType();
        req.SKUs = pricingArray;
        req.SFDCSessionID = UserInfo.getSessionId();
        req.SFDCServiceCallbackUrl = PricingServiceVariables__c.getInstance('ReturnServiceEndpoint').Value__c;
        req.UserID = UserInfo.getUserId();
        req.Ship_To = location.ERP_ID__c;
        req.RequestDate = System.Today();
        if (productParameter.Contract_Type__c == 'Both')
             req.Designator = 'B' ;
        else if (productParameter.Contract_Type__c == 'Direct')
             req.Designator = 'D' ;
        else if (productParameter.Contract_Type__c == 'Indirect')
             req.Designator = 'R' ;
        else
             req.Designator = 'B' ;
             
       // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info, 'req.Designator: '+req.Designator ) );
            
             
        req.ShowOnePrice = 'One by Qty';
        req.GBU =  productParameter.GBU__c;// u.Business_Unit__c;//'SD'; 
        req.Country= acct.BillingCountry; 
        pricingCovidienComProxy3.PricingRequestAckType ack = new pricingCovidienComProxy3.PricingRequestAckType();
        if(!Test.isRunningTest() )
           ack = service.GetPrice(req); 
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'PricingRequestID: '+ack.PricingRequestID) );           
        //System.debug(ack);
        PricingRequestID = ack.PricingRequestID;
        return Page.PricingQueryResults;
    }
    
    public list<webServiceResult> getPricingResults()
    {
        results = new list<webServiceResult>();
        for (PRICING_INTEGRATION_SKU_TYPE__c p :  [ Select p.UOM__c
                                                         , p.SalesClass__c
                                                         , p.SKUCode__c
                                                         , p.Name
                                                         , p.Id
                                                         , p.SKUError__c
                                                         , p.SKUErrorDescription__c
                                                         , (Select Id
                                                                 , Name
                                                                 , CurrencyIsoCode
                                                                 , QuantityRange__c
                                                                 , EffectiveDate__c
                                                                 , ExpirationDate__c
                                                                 , UnitPrice__c
                                                                 , Markup__c
                                                                 , EndCustomerPrice__c
                                                                 , ContractNumber__c
                                                                 , ContractDesignator__c
                                                                 , ContractDescription__c
                                                                 , SequenceNumber__c
                                                                 , PriceCurrency__c
                                                                 , PriceError__c
                                                                 , PriceErrorDescription__c 
                                                              From PRICING_INTEGRATION_PRICING__r)
                                                     From PRICING_INTEGRATION_SKU_TYPE__c p
                                                    where PricingRequestID__c = :PricingRequestID])
        {
                webServiceResult result = new webServiceResult();
                result.product = productSkuMap.get(p.SKUCode__c);
                SKUType t = new SKUType();
                t.SKUCode = p.SKUCode__c;
                t.SalesClass = p.SalesClass__c;
                t.UOM = p.UOM__c;
                t.SKUErrorDescription = p.SKUErrorDescription__c;
                t.SKUError = p.SKUError__c;
                list<PriceType> priceList = new list<PriceType>();
                for(PRICING_INTEGRATION__c pp : p.PRICING_INTEGRATION_PRICING__r)
                {
                    PriceType pt = new PriceType();
                    pt.QuantityRange = pp.QuantityRange__c;
                    pt.EffectiveDate = pp.EffectiveDate__c;
                    pt.ExpirationDate = pp.ExpirationDate__c;
                    pt.UnitPrice = pp.UnitPrice__c;
                    pt.Markup = pp.Markup__c;
                    pt.EndCustomerPrice = pp.EndCustomerPrice__c;
                    pt.ContractNumber = pp.ContractNumber__c;
                    pt.ContractDesignator = pp.ContractDesignator__c;
                    pt.ContractDescription = pp.ContractDescription__c;
                    pt.SequenceNumber = Integer.ValueOf(pp.SequenceNumber__c);
                    pt.PriceCurrency = pp.PriceCurrency__c;
                    pt.PriceError = pp.PriceError__c;
                    pt.PriceErrorDescription = pp.PriceErrorDescription__c;
                    priceList.add(pt);
                }
                t.Price = priceList;
                result.wsResult = t;
                results.add(result);
        }
        
        if(results.size() > 0)
          resultsPending = false;
        return results;
    }
    
    public pageReference pollSearchSearchResults()
    {
        //resultsPending = false;
        return null;
    }
    
    /*
    public List<webServiceResult> getPricingResults()
    {
        
        /** Method for constructing the call-out to the web service and 
            parsiing the results for display in the UI
         **
         
         //retrieve the location data
         location = [select id, Parent_Sell_To_Account__c, Name, ERP_ID__c
                       from ERP_Account__c
                      where Id = :location.Id];

         results = new List<webServiceResult>();
            webServiceResult res = new webServiceResult();
        
        
        //prepare the web service request
    
        pricingCovidienComProxy32.SKUsType skus = new pricingCovidienComProxy32.SKUsType();
        List<pricingCovidienComProxy32.SKUType> skuList = new List<pricingCovidienComProxy32.SKUType>();
        for(productResult pr : selectedProducts)
        {
            pricingCovidienComProxy32.SKUType sku = new pricingCovidienComProxy32.SKUType();
            pricingCovidienComProxy32.PricesType Prices = new pricingCovidienComProxy32.PricesType();
            list<pricingCovidienComProxy32.PriceType> priceList= new list<pricingCovidienComProxy32.PriceType>();
            
            sku.Prices = Prices;
            sku.Quantity = 1;
            sku.SKUCode = pr.product.SKU__c;
            sku.SalesClass= '';//pr.product.Sales_Class__c;
            sku.UOM= '';//pr.product.UOM__c;
            sku.SKUError='0';
            sku.SKUErrorDescription='';
            skuList.add(sku);
        
        }
        skus.SKU = skuList;
        
        try{
            proxypricingCovidienComProxy32.BasicHttpBinding_ITwoWayAsync svc 
                = new proxypricingCovidienComProxy32.BasicHttpBinding_ITwoWayAsync();
            
            user u = [Select Id, Business_Unit__c, CurrencyIsoCode, email from User where Id = :UserInfo.getUserId()];
            
            pricingCovidienComProxy32.PricingRequestType request_x = new pricingCovidienComProxy32.PricingRequestType();
            
            
            request_x.SKUs = skus;
            request_x.RequestID=1; 
            request_x.UserID= u.Email; 
            request_x.Ship_To = Integer.ValueOf(location.ERP_ID__c); 
            //request_x.RequestDate= null;//sellToAccount.CapitalBudgetAvailable__c;
           // request_x.ExpirationDate = null;
            request_x.RequestDate = ProductParameter.Effective_Date__c;
            request_x.Designator = 'D';//PricingServiceVariables__c.getInstance(productParameter.Contract_Type__c).Value__c;
            request_x.ShowOnePrice='One by Qty';//supply the actaul value -- TBD 
            request_x.Currency_x = u.CurrencyIsoCode; //'USD'; 
            request_x.GBU = productParameter.GBU__c;// u.Business_Unit__c;//'SD'; 
            request_x.Country= acct.BillingCountry; 
            request_x.RequestError='0'; 
            request_x.RequestErrorDescription='';
            
            System.debug('+++++REQ++++++> '+ request_x);
            pricingCovidienComProxy32.SKUsType pt = new pricingCovidienComProxy32.SKUsType();
            if(!isTest)//avoid a call-out durring tewst execution
            {
               svc.timeout_x = Integer.ValueOf(PricingServiceVariables__c.getInstance('Request Timeout Interval').Value__c);
               pricingCovidienComProxy32.PricingRequestType resp = svc.PricingLiteProxy(request_x);
               
               if(resp.RequestErrorDescription != '')
               { //check for errors and the response level
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'WS ERROR: '+resp.RequestErrorDescription) );
                 return results;
               }
               pt = resp.SKUs;
               System.debug('+++++RES++++++> '+ pt);
            }
            else
            {  //testing only
                pt.SKU = skuList;//set it equal to the request for testing
                try{
                     svc.PricingLiteProxy(request_x);//gets coverage on the proxy
                }
                catch(Exception e){}
              
            }
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'WS RESPONSE: '+pt.SKU) );
            for( pricingCovidienComProxy32.SKUType st : pt.SKU)
            {
                webServiceResult result = new webServiceResult();
                result.product = productSkuMap.get(st.SKUCode);
                result.wsResult = st;
                results.add(result);
                
            }
            
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()) ); 
        }
        
        return results;
    }
    */
    public PageReference backToQuery()
    {
        
        return Page.PricingQueryTool;
    }
    
    public PageReference addToKitQuote()
    {
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Results: --> '+results.size()) );
        
        if(results.size() > 0)
        {
            list<Kit_Product__c> kqs = new list<Kit_Product__c>();
            list<Kit_Product__c> ukqs = new list<Kit_Product__c>();
            map<Id, Id> ExistingKitQuotes = new map<Id, Id>();
            
            for(Kit_Product__c k : [Select Id, Product_SKU__c 
                               from Kit_Product__c
                              where Kit__c = :kitId])
            {
                ExistingKitQuotes.put(k.Product_SKU__c, k.Id);
            }
            
            for(webServiceResult r : results)
            {
                if(r.checked)
                {
                    if(r.wsResult.Price == null)
                      continue;
                    
                    for(PriceType p : r.wsResult.Price)
                    {
                        if(ExistingKitQuotes.isEmpty() || ExistingKitQuotes.get(r.product.Id) == null)
                        {
                            system.debug('>>>>>>>>>>>>>>>>'+r);
                            Kit_Product__c kq = new Kit_Product__c(UoM__c = r.wsResult.UOM
                                                           , Quantity__c = 1
                                                           , Product_SKU__c = r.product.Id
                                                           , Price__c = p.UnitPrice
                                                           , Kit__c = kitId);
                                                          // , Extended_price__c = p.UnitPrice);
                            kqs.add(kq);
                        }
                        else
                        {
                            
                            Kit_Product__c ukq = new Kit_Product__c(UoM__c = r.wsResult.UOM
                                                            , Quantity__c = 1
                                                            , Product_SKU__c = r.product.Id
                                                            , Price__c = p.UnitPrice
                                                            , Kit__c = kitId
                                                           // , Extended_price__c = p.UnitPrice
                                                            , Id = ExistingKitQuotes.get(r.product.Id));
                            ukqs.add(ukq);
                        }
                    }
                }
            }
            
            if(ukqs.size() > 0)
              update ukqs;
              
            if(kqs.size() > 0)
              insert kqs;
            }
        
          
        return new PageReference('/'+kitId);
    }
}