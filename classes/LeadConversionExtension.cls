/****************************************************************************************
* Name    : LeadConversionExtension
* Author  : Gautam Shah
* Date    : 1/7/2014
* Purpose : Alternative to standard lead conversion
* 
* Dependancies: 
*              
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 1/29/14       Gautam Shah             Lines 66-68: changed DataQuality method to Sobject-specific record-type getter to avoid name collision.  Replaced references to "rtMap" with Sobject-specific references.
* 3/3/14    Gautam Shah        Changes throughout the code to utilize the native opportunity conversion and contact role attachment.
* 3/19/14    Gautam Shah        Lines 95-96: changes to map Lead fields to Contact fields.
* 6/5/14    Gautam Shah        Line 56: 'SD' record type has been changed to 'AST'; Lines 211, 243-248: changes to allow opportunities to be related to the campaign the lead was related to
* 10/10/14    Gautam Shah        Changes to enable ANZ lead types based on country: Line 44, 52-55, 81-84; There are also changes in CustomLeadConversion VF page
* 12/9/14    Gautam Shah        Changes in favor of using JS Remoting for the convertLead call due to the Winter '15 VF-AJAX post-back bug
* 5/12/15    Gautam Shah        Added a condition to set the "GBU" value if the lead recordtype is "NCPCS": lines 66-69, 161-164
* 7/23/15       Amogh Ghodke            Added line 43 and 137 to limit OPG options
*****************************************************************************************/ 
public with sharing class LeadConversionExtension 
{
    public Lead l {get;set;}
    public Account a {get;set;}
    public Contact c {get;set;}
    public Opportunity o {get;set;}
    public Boolean addOpp {get;set;}
    public List<SelectOption> typeOptions {get;set;}
    public List<SelectOption> stageOptions {get;set;}
    public List<SelectOption> departmentOptions {get;set;}
    public List<SelectOption> connectedAsOptions {get;set;}
    public List<Product> products {get;set;}
    public String OPG {get;set;}
    private transient Savepoint sp;
    public Boolean showHeader {get;set;}
    public String userAgent {get;set;}
    public String errorMessage {get;set;}
    private Set<String> anzCountries;
   
    String currentUserRegion = [Select Region__c from User where Id=:userinfo.getuserId()].Region__c;
    public LeadConversionExtension(ApexPages.StandardController stdController) 
    {
        this.showHeader = true;
        this.userAgent = System.currentPageReference().getHeaders().get('User-Agent');
        this.anzCountries = new Set<String>{'AU','AUS','Australia','New Zealand','NZ','NZL'};//matches the keys in the Lead Country/Queue Mapping custom setting table
        this.addOpp = false;
        String GBU;
        //Added test run method - Tejas Kardile
        if(!test.isRunningTest())
        {
            stdController.addFields(new List<String>{'Name', 'Account__c', 'Contact__c', 'Opportunity__c', 'OwnerId', 'RecordType.Name', 'FirstName', 'LastName', 'Phone', 'Email', 'Company', 'Street', 'City', 'State', 'PostalCode', 'Country', 'Department__c', 'Job_Role__c', 'Specialty__c', 'Opportunity_Product_Group__c', 'Campaign_Most_Recent__c'});
            this.l = (Lead)stdController.getRecord();
            if(anzCountries.contains(l.Country))
            {
              GBU = 'ANZ';
            }
            else if(l.RecordType.Name.contains('RMS'))
            {
                GBU = 'US - RMS';
            }
            else if(l.RecordType.Name.contains('Surgical'))
            {
                GBU = 'US - AST';
            }
            else if(l.RecordType.Name.contains('NCPCS'))
            {
                GBU = 'US - Med Supplies';
            }
        }
        else
        {
            this.l = (Lead)stdController.getRecord();
        }          
        String oppRT = GBU + ' - Opportunity';
        String contactRT;
        if(anzCountries.contains(l.Country))
        {
          contactRT = 'Connected Contact ANZ';
        }
        else
        {
          contactRT = 'Connected Contact US';
        }
        DataQuality dq = new DataQuality();
        Map<String,Id> acctRTMap = dq.getRecordTypes('Account'); 
        Map<String,Id> contactRTMap = dq.getRecordTypes('Contact');
        Map<String,Id> oppRTMap = dq.getRecordTypes('Opportunity');
        this.a = (Account)Account.sObjectType.newSObject(acctRTMap.get('User Submitted Healthcare Facility'), true);
        this.c = (Contact)Contact.sObjectType.newSObject(contactRTMap.get(contactRT), true);
        this.o = (Opportunity)Opportunity.sObjectType.newSObject(oppRTMap.get(oppRT), true);
        this.departmentOptions = getPicklistValues('Contact', contactRTMap.get(contactRT), 'Department_picklist__c');
        this.connectedAsOptions = getPicklistValues('Contact', contactRTMap.get(contactRT), 'Affiliated_Role__c');
        this.typeOptions = getPicklistValues('Opportunity', oppRTMap.get(oppRT), 'Type');
        this.stageOptions = getPicklistValues('Opportunity', oppRTMap.get(oppRT), 'StageName');
        //populate default values
        a.Name = l.Company;
        a.Phone = l.Phone;
        a.BillingStreet = l.Street;
        a.BillingCity = l.City;
        a.BillingState = l.State;
        a.BillingPostalCode = l.PostalCode;
        a.BillingCountry = l.Country;
         c.Salutation = l.Salutation;
        c.FirstName = l.FirstName;
        c.LastName = l.LastName;
        c.Phone = l.Phone;
        c.Email = l.Email;
        c.Department_picklist__c = l.Department__c;
        c.Affiliated_Role__c = l.Job_Role__c;
        c.Specialty1__c = l.Specialty__c;
        if(String.isBlank(l.Opportunity_Product_Group__c))
        {
          l.Opportunity_Product_Group__c = '--None--';
        }
        this.OPG = l.Opportunity_Product_Group__c;
        fetchProducts();
    }

    public List<SelectOption> getPicklistValues(String sObjType, Id rtID, String fld)
    {
        List<SelectOption> options = new List<SelectOption>();
        for(String a : PicklistDescriber.describe(sObjType, rtID, fld))
        { 
            options.add(new SelectOption(a, a));
        }
        return options;
    }
    
    public List<SelectOption> getOPGList()
    {
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('--None--', '--None--'));
      for(Pricebook2 opg : [Select Name From Pricebook2 Where IsDeleted = false And IsActive = true Order By Name])
      { if(opg.Name.startswith(currentUserRegion))
        options.add(new SelectOption(opg.Name, opg.Name));
      }
      return options;
    }
    
    @RemoteAction
    public static Response convertLead(String LeadId, String LeadOwnerId, String lookupAcctId, String AcctName, String AcctPhone, String AcctStreet, String AcctCity, String AcctState, String AcctZip, String AcctCountry, 
    String lookupContactId, String ContactSalutation, String ContactFirstName, String ContactLastName, String ContactPhone, String ContactEmail, String ContactSpecialty, String ContactConsent, String ContactDepartment, String ContactConnectedAs,
                Boolean addOpp, String lookupOppId, String OppName, String OppType, String OppStage, String OppCloseDate, String OPG, List<Product> prodArray)
    {
      Lead l = [Select Account__c, Company, Contact__c, Campaign_Most_Recent__c, RecordType.Name From Lead Where Id = :LeadId Limit 1];
      Set<String> anzCountries = new Set<String>{'AU','AUS','Australia','New Zealand','NZ','NZL'};//matches the keys in the Lead Country/Queue Mapping custom setting table
      String GBU;
      if(anzCountries.contains(AcctCountry))
        {
          GBU = 'ANZ';
        }
        else if(l.RecordType.Name.contains('RMS'))
        {
            GBU = 'US - RMS';
        }
        else if(l.RecordType.Name.contains('Surgical'))
        {
            GBU = 'US - AST';
        }
        else if(l.RecordType.Name.contains('NCPCS'))
        {
            GBU = 'US - Med Supplies';
        }
        String oppRT = GBU + ' - Opportunity';
        String contactRT;
        if(anzCountries.contains(AcctCountry))
        {
          contactRT = 'Connected Contact ANZ';
        }
        else
        {
          contactRT = 'Connected Contact US';
        }
        Id currentUserId = UserInfo.getUserId();
        DataQuality dq = new DataQuality();
        Map<String,Id> acctRTMap = dq.getRecordTypes('Account'); 
        Map<String,Id> contactRTMap = dq.getRecordTypes('Contact');
        Map<String,Id> oppRTMap = dq.getRecordTypes('Opportunity');
         Account a = (Account)Account.sObjectType.newSObject(acctRTMap.get('User Submitted Healthcare Facility'), true);
        Contact c = (Contact)Contact.sObjectType.newSObject(contactRTMap.get(contactRT), true);
        Opportunity o = (Opportunity)Opportunity.sObjectType.newSObject(oppRTMap.get(oppRT), true);
        
        List<User> owner = new List<User>([Select Id From User Where IsActive = true And Id = :LeadOwnerId Limit 1]);
      //
      Savepoint sp;
      try{
            sp = Database.setSavepoint();
            System.debug('Remote Action convertLead()');
          Database.LeadConvert lc = new database.LeadConvert();
          lc.setLeadId(LeadId);
          if(owner.size() == 0)
          {
              lc.setOwnerId(currentUserId);
          }
            System.debug('l.Account__c: ' + lookupAcctId);
            if(!String.isBlank(lookupAcctId))
            {
                System.debug('choosing existing account');
                lc.setAccountId(lookupAcctId);
            }   
            else 
            {
                System.debug('creating new account');
                //String AcctName, String AcctPhone, String AcctStreet, String AcctCity, String AcctState, String AcctZip, String AcctCountry
                a.Name = AcctName;
            a.Phone = AcctPhone;
            a.BillingStreet = AcctStreet;
            a.BillingCity = AcctCity;
            a.BillingState = AcctState;
            a.BillingPostalCode = AcctZip;
            a.BillingCountry = AcctCountry;
                //l.Company = a.Name;
                insert a;
                lc.setAccountId(a.Id); 
            }
            System.debug('l.Contact__c: ' + lookupContactId);
            if(!String.isBlank(lookupContactId))
            {
                System.debug('choosing existing contact');
                lc.setContactId(lookupContactId);
            }   
            else 
            {
                System.debug('creating new contact');
                //lookupContactId, String ContactSalutation, String ContactFirstName, String ContactLastName, String ContactPhone, String ContactEmail, String ContactSpecialty, String ContactConsent, String ContactDepartment, String ContactConnectedAs
                if(!String.isBlank(lookupAcctId))
                {
                    c.AccountId = lookupAcctId;
                }
                else
                {
                    c.AccountId = a.Id;
                }
                c.Salutation = ContactSalutation;
            c.FirstName = ContactFirstName;
            c.LastName = ContactLastName;
            c.Phone = ContactPhone;
            c.Email = ContactEmail;
            c.Department_picklist__c = ContactDepartment;
            c.Affiliated_Role__c = ContactConnectedAs;
            c.Specialty1__c = ContactSpecialty;
            c.Email_Marketing_Consent__c = ContactConsent;
                c.Phone_Number_at_Account__c = ContactPhone;
                insert c;
                lc.setContactId(c.Id); 
            }
            lc.setOverwriteLeadSource(false);
            lc.setSendNotificationEmail(false);
            lc.setConvertedStatus('Qualified');
            //lc.setDoNotCreateOpportunity(true);
            
            System.debug('addOpp: ' + addOpp);
            System.debug('lookupOppId: ' + lookupOppId);
            Boolean createNewOpp = false;
            if(addOpp && lookupOppId == null)
            {
              createNewOpp = true;
            }
            lc.setDoNotCreateOpportunity(!createNewOpp);
            if(createNewOpp)
            {
              lc.setOpportunityName(OppName);
            }
            //update l;
            Database.LeadConvertResult lcResult = database.convertLead(lc);
            System.debug('LeadConvertResult: ' + lcResult);
            
            //Opportunity
            List<Opportunity> opp = new List<Opportunity>();
            if(createNewOpp)
            {
              String CurrencyCode = 'USD';
              opp = [Select Id, RecordTypeId, Type, StageName, CloseDate, CampaignId, CurrencyIsoCode, Account.BillingCountry From Opportunity Where Id = :lcResult.getOpportunityId() Limit 1];
              if(opp.size() > 0)
              {
                if(opp[0].Account.BillingCountry == 'AU')
                {
                  CurrencyCode = 'AUD';
                }
                else if(opp[0].Account.BillingCountry == 'NZ')
                {
                  CurrencyCode = 'NZD';
                }
                //addOpp, String lookupOppId, String OppName, String OppType, String OppStage, String OppCloseDate, String OPG, List<Product> prodArray
                opp[0].CurrencyIsoCode = CurrencyCode;
                  opp[0].RecordTypeId = o.RecordTypeId;
                  opp[0].Type = OppType;
                  opp[0].StageName = OppStage;
                  opp[0].CloseDate = Date.valueOf(OppCloseDate);
                  opp[0].CampaignId = l.Campaign_Most_Recent__c;
                  update opp[0];
              }
                // Update Opportunity Contact Role
                List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>([Select Id, Role, isPrimary From OpportunityContactRole Where OpportunityId = :lcResult.getOpportunityId() And ContactId = :lcResult.getContactId() Limit 1]);
                if(ocrList.size() > 0)
                {
                  //new opp so make contact role primary
                  ocrList[0].Role = 'Decision Maker';
                  ocrList[0].isPrimary = true;
                  update ocrList[0];
                }
                // Add OpportunityLineItem Records     
                List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();           
              for(Product p : prodArray)
              {
                if(p.Quantity > 0 && p.UnitPrice >= 0)
                {
                  OpportunityLineItem oli = new OpportunityLineItem();
                  //oli.CurrencyIsoCode = CurrencyCode;
                  oli.Description = p.Description;
                  oli.OpportunityId = lcResult.getOpportunityId();
                  oli.PricebookEntryId = p.Id;
                  oli.Quantity = p.Quantity;
                  oli.UnitPrice = p.UnitPrice;
                  oliList.add(oli);
                }
              }
              insert oliList;
            }
            else if(addOpp && !String.isBlank(lookupOppId))
            {
              opp = [Select Id, CampaignId From Opportunity Where Id = :lookupOppId Limit 1];
              if(opp.size() > 0)
              {
                  opp[0].CampaignId = l.Campaign_Most_Recent__c;
                  update opp[0];
              }
              
              List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>([Select Id, Role, isPrimary From OpportunityContactRole Where OpportunityId = :lookupOppId And ContactId = :lcResult.getContactId() Limit 1]);
              if(ocrList.size() == 0)
              {
                //create new OCR but since it's existing Opp don't make OCR primary
                OpportunityContactRole ocr = new OpportunityContactRole();
                ocr.ContactId = lcResult.getContactId();
                  ocr.OpportunityId = lookupOppId;
                  ocr.Role = 'Decision Maker';
                  System.debug('inserting OCR');
                insert ocr;
              }
              else
              {
                //Contact Role exists, make sure they have a role, don't make primary
                if(String.isBlank(ocrList[0].Role))
                {
                  ocrList[0].Role = 'Decision Maker';
                  update ocrList[0];
                }
              }
            }
            
            //List<Contact> con = new List<Contact>([Select Id From Contact Where Id = :lcResult.getContactId() Limit 1]);
            //PageReference destination = null;
            System.debug('opp.size: ' + opp.size());
            String destID = '';
            if(opp.size() != 0)
            {
                destID = opp[0].Id;
            }
            else
            {
                destID = lcResult.getContactId();
            }
            Response r = new Response();
            r.forwardTo = '/' + destID + '/e?retURL=%2F' + destID + '&saveURL=%2F' + destID;
            r.success = true;
            return r;
        }
        catch(Exception e)
        {
            System.debug('caught problem');
            //problem = true;
            String errorMessage = 'Sorry, there was an error. The lead was not converted. Please notify support. \n' + e.getMessage() + ' Line #: ' + e.getLineNumber();
            Database.rollback(sp);
            Response r = new Response();
            r.success = false;
            r.message = errorMessage;
            r.forwardTo = null;
            return r;
        }
    }
   /*
    public pagereference convertLead() { 
        try{
            sp = Database.setSavepoint();
            System.debug('convertLead()');
            l.Opportunity_Product_Group__c = this.OPG;
            update l;
            // Get information on User converting Lead
            Id currentUserId = UserInfo.getUserId();
            User currentUser = [Select Id, Business_Unit__c, Region__c From User Where Id = :currentUserId Limit 1];
            List<User> owner = new List<User>([Select Id From User Where Id = :l.OwnerId Limit 1]);
           
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(l.id);
            if(owner.size() == 0)
            {
                lc.setOwnerId(currentUserId);
            }
            System.debug('l.Account__c: ' + l.Account__c);
            if(l.Account__c != null)
            {
                System.debug('choosing existing account');
                lc.setAccountId(l.Account__c);
            }   
            else 
            {
                System.debug('creating new account');
                l.Company = a.Name;
                insert a;
                lc.setAccountId(a.Id); 
            }
            System.debug('l.Contact__c: ' + l.Contact__c);
            if(l.Contact__c != null)
            {
                System.debug('choosing existing contact');
                lc.setContactId(l.Contact__c);
            }   
            else 
            {
                System.debug('creating new contact');
                if(l.Account__c != null)
                {
                    c.AccountId = l.Account__c;
                }
                else
                {
                    c.AccountId = a.Id;
                }
                c.Phone_Number_at_Account__c = c.Phone;
                insert c;
                lc.setContactId(c.Id); 
            }
            lc.setOverwriteLeadSource(false);
            lc.setSendNotificationEmail(false);
            lc.setConvertedStatus('Qualified');
            //lc.setDoNotCreateOpportunity(true);
            
            Boolean createNewOpp = false;
            if(this.addOpp && l.Opportunity__c == null)
            {
              createNewOpp = true;
            }
            lc.setDoNotCreateOpportunity(!createNewOpp);
            if(createNewOpp)
            {
              lc.setOpportunityName(o.Name);
            }
            update l;
            System.debug('c.Department_picklist__c: ' + c.Department_picklist__c);
            System.debug('c.Affiliated_Role__c: ' + c.Affiliated_Role__c);
            Database.LeadConvertResult lcResult = database.convertLead(lc);
            System.debug('LeadConvertResult: ' + lcResult);
            
            //Opportunity
            List<Opportunity> opp = new List<Opportunity>();
            if(createNewOpp)
            {
              String CurrencyCode = 'USD';
              opp = [Select Id, RecordTypeId, Type, StageName, CloseDate, CampaignId, CurrencyIsoCode, Account.BillingCountry From Opportunity Where Id = :lcResult.getOpportunityId() Limit 1];
              if(opp.size() > 0)
              {
                if(opp[0].Account.BillingCountry == 'AU')
                {
                  CurrencyCode = 'AUD';
                }
                else if(opp[0].Account.BillingCountry == 'NZ')
                {
                  CurrencyCode = 'NZD';
                }
                opp[0].CurrencyIsoCode = CurrencyCode;
                  opp[0].RecordTypeId = o.RecordTypeId;
                  opp[0].Type = o.Type;
                  opp[0].StageName = o.StageName;
                  opp[0].CloseDate = o.CloseDate;
                  opp[0].CampaignId = l.Campaign_Most_Recent__c;
                  update opp[0];
              }
                // Update Opportunity Contact Role
                List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>([Select Id, Role, isPrimary From OpportunityContactRole Where OpportunityId = :lcResult.getOpportunityId() And ContactId = :lcResult.getContactId() Limit 1]);
                if(ocrList.size() > 0)
                {
                  //new opp so make contact role primary
                  ocrList[0].Role = 'Decision Maker';
                  ocrList[0].isPrimary = true;
                  update ocrList[0];
                }
                // Add OpportunityLineItem Records     
                List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();           
              for(Product p : products)
              {
                if(p.Quantity > 0 && p.UnitPrice >= 0)
                {
                  OpportunityLineItem oli = new OpportunityLineItem();
                  //oli.CurrencyIsoCode = CurrencyCode;
                  oli.Description = p.Description;
                  oli.OpportunityId = lcResult.getOpportunityId();
                  oli.PricebookEntryId = p.Id;
                  oli.Quantity = p.Quantity;
                  oli.UnitPrice = p.UnitPrice;
                  oliList.add(oli);
                }
              }
              insert oliList;
            }
            else if(this.addOpp && l.Opportunity__c != null)
            {
              opp = [Select Id, CampaignId From Opportunity Where Id = :l.Opportunity__c Limit 1];
              if(opp.size() > 0)
              {
                  opp[0].CampaignId = l.Campaign_Most_Recent__c;
                  update opp[0];
              }
              
              List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>([Select Id, Role, isPrimary From OpportunityContactRole Where OpportunityId = :l.Opportunity__c And ContactId = :lcResult.getContactId() Limit 1]);
              if(ocrList.size() == 0)
              {
                //create new OCR but since it's existing Opp don't make OCR primary
                OpportunityContactRole ocr = new OpportunityContactRole();
                ocr.ContactId = lcResult.getContactId();
                  ocr.OpportunityId = l.Opportunity__c;
                  ocr.Role = 'Decision Maker';
                  System.debug('inserting OCR');
                insert ocr;
              }
              else
              {
                //Contact Role exists, make sure they have a role, don't make primary
                if(String.isBlank(ocrList[0].Role))
                {
                  ocrList[0].Role = 'Decision Maker';
                  update ocrList[0];
                }
              }
            }
            
            //List<Contact> con = new List<Contact>([Select Id From Contact Where Id = :lcResult.getContactId() Limit 1]);
            //PageReference destination = null;
            System.debug('opp.size: ' + opp.size());
            String destID = '';
            if(opp.size() != 0)
            {
                destID = opp[0].Id;
            }
            
            //else if(l.Opportunity__c != null)
            //{
            //  destID = l.Opportunity__c;
            //}
            
            else
            {
                destID = lcResult.getContactId();
            }
            PageReference destination = new PageReference('/' + destID + '/e?retURL=%2F' + destID + '&saveURL=%2F' + destID);
            destination.setRedirect(true);
            return destination;
        }
        catch(Exception e)
        {
            System.debug('caught problem');
            //problem = true;
            errorMessage = 'Sorry, there was an error. The lead was not converted. Please notify support. <br/>' + e.getMessage() + ' Line #: ' + e.getLineNumber();
            Database.rollback(sp);
            return null;
        }
    }
    */
    public void fetchProducts()
  {
    products = new List<Product>();
    if(!String.isBlank(OPG))
    {
        List<PricebookEntry> prodList = new List<PricebookEntry>([Select Id, Name, UnitPrice From PricebookEntry Where IsDeleted = false And isActive = true And Pricebook2.Name = :OPG Order By Name]);
        for(PricebookEntry prod : prodList)
        {
          Product p = new Product(prod.Id, prod.Name, 0, prod.UnitPrice, '');
          products.add(p);
        }
    }
    //return products;
    }
    
    public class Product
    {
      public String Id {get;set;}
      public String Name {get;set;}
      public Integer Quantity {get;set;}
      public Decimal UnitPrice {get;set;}
      public String Description {get;set;}
      
      public Product(String i, String n, Integer q, Decimal u, String d)
      {
        this.Id = i;
        this.Name = n;
        this.Quantity = q;
        this.UnitPrice = u;
        this.Description = d;
      }
    }
    
    public class Response
    {
      public String message {get;set;}
      public String forwardTo {get;set;}
      public Boolean success {get;set;}
    }
}