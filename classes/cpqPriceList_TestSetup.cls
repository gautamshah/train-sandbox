public class cpqPriceList_TestSetup extends cpq_TestSetup
{
    public static List<CPQ_Variable__c> create(List<Apttus_Config2__PriceList__c> priceLists)
    {
        List<CPQ_Variable__c> vars = new List<CPQ_Variable__c>();
        if (priceLists != NULL)
        {
            for (Apttus_Config2__PriceList__c pl : priceLists)
                vars.add(
                	CPQVariable_c.create(
                		cpqPriceList_c.PriceListName2VariableName.get(pl.Name),
                        pl.Id));
        }
        
        return vars;
    }
}