public with sharing class CreateContactCaseController 
{
	private Case currCase {get;set;}
	private String currCaseId {get; set;}
	private String deptValue = 'Other';
	private String connectedAsValue = 'Other';
	private ApexPages.StandardController controller {get;set;}
	public CreateContactCaseController(ApexPages.StandardController controller)
	{
		currCaseId = controller.getId();
		this.controller = controller;
		if(!Test.isRunningTest())
		{
			List<String> addlFields = new List<String>{'AccountId', 'ContactId', 'SuppliedName', 'SuppliedEmail', 'SuppliedPhone'};
			controller.addFields(addlFields);
			currCase = (Case) controller.getRecord();
		}
	}
	public PageReference createContact()
	{
		PageReference pr;
		if(String.isNotBlank(currCase.AccountId) && String.isNotBlank(currCase.SuppliedName))
		{
			Contact con = new Contact();
			con.AccountId = currCase.AccountId;
			if(currCase.SuppliedName.containsWhitespace())
			{
				con.FirstName = currCase.SuppliedName.substringBefore(' ');
				con.LastName = currCase.SuppliedName.substringAfter(' ');
			}
			else
			{
				con.LastName = currCase.SuppliedName;
			}
			con.Email = currCase.SuppliedEmail;
			con.Phone = currCase.SuppliedPhone;
			con.Department_picklist__c = deptValue;
			con.Other_Department__c = 'TBD';
			con.Affiliated_Role__c = connectedAsValue;
			con.RecordTypeId = Utilities.recordTypeMap_DeveloperName_Id.get('Affiliated_Non_Clinician_Contact');
			try
			{
				insert con;
				currCase.ContactId = con.Id;
				update currCase;
				pr = new PageReference('/' + con.Id + '/e?retURL=' + currCaseId);
				pr.setRedirect(true);
			}
			catch(Exception e)
			{
				System.debug('error: ' + e.getMessage());
			}
		}
		else
		{
			ApexPages.Message oPageMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'The case must have Account and Web Name defined');
		    ApexPages.addMessage(oPageMessage);
		}
		
		return pr;
	}
}