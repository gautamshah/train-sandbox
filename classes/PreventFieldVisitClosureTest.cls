@isTest(seealldata=true)
public class PreventFieldVisitClosureTest {
        
        static testMethod void FieldvisitTest() {
        RecordType fvrt = [select id from recordtype where DeveloperName = 'EMEA_EM_Field_Visit'];
        Recordtype taskrt = [select id from recordtype where DeveloperName = 'Support_Task'];
        Profile pId = [select id from Profile where Name = 'EMEA EM'];
        
        User manager = new User();
        manager.firstName = 'Manager';
        manager.LastName = 'Test';
        manager.Alias = 'MTest';
        manager.Email = 'manager.test@Test.com';
        manager.Username = 'manager.test@Test.com';
        manager.CommunityNickname = 'manager.test';
        manager.ProfileID= pId.Id; 
        manager.Region__c = 'EM';
        manager.Business_Unit__c  = 'EMID';
        manager.User_Role__c  = 'Field manager';
        manager.Function__c= 'Sales';
        manager.TimeZoneSidKey ='Asia/Riyadh';
        manager.LocaleSidKey = 'en_US';
        manager.EmailEncodingKey ='UTF-8';
        manager.LanguageLocaleKey ='en_US';
        Insert (manager);
        
        User rep = new User();
        rep.firstName = 'Rep';
        rep.LastName = 'Test';
        rep.Alias = 'RTest';
        rep.Email = 'Rep.test@Test.com';
        rep.Username = 'Rep.test@Test.com';
        rep.CommunityNickname = 'Rep.test';
        rep.ProfileID= pId.Id; 
        rep.Region__c = 'EM';
        rep.Business_Unit__c  = 'EMID';
        rep.User_Role__c  = 'Rep';
        rep.Function__c= 'Sales';
        rep.TimeZoneSidKey ='Asia/Riyadh';
        rep.LocaleSidKey = 'en_US';
        rep.EmailEncodingKey = 'UTF-8';
        rep.LanguageLocaleKey = 'en_US';
        //rep.User_Manager__c='';
        Insert (rep);
        
        Field_visit__c f = new Field_visit__c();
        f.recordtypeId = fvrt.Id  ;
        f.Manager__c = manager.Id;
        f.Sales_Rep__c = rep.Id;
        f.field_visit_status__c = 'Planned';
        Insert f;
        
        Task t = new Task();
        t.recordtypeId = taskrt.Id;
        t.ownerId = rep.Id;
        t.subject = 'Call';
        t.status = 'In Progress';
        t.priority = 'Normal';
        //t.who = 'Test';
        t.WhatId = f.Id;
        Insert t;
        try {
            System.runAs(manager)
            {
            f.field_visit_status__c = 'Closed';
            update f;
            }
        }
        catch (Exception e)
        {
              System.Assert(e.getMessage().contains ('Cannot close Field visit record since there are open tasks'));
        }
        }
}