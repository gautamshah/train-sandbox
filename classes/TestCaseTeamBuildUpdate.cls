@isTest(SeeAllData=true)
private class TestCaseTeamBuildUpdate {

    static testMethod void myUnitTest() {
        List<User> userList = new List<User>([SELECT Id from User where UserType = 'Standard' and IsActive = True LIMIT 2]);
        
        List <Case> caseListUpdate = new List<Case>();
        Id compCaseRTId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Compensation_Case' LIMIT 1].Id;
        Id randTRTId = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'R_T_Case' LIMIT 1].Id;
        
        for (Case c : [Select Id, Requested_By__c, Manager_of_Requested_By__c from Case where RecordTypeId = :compCaseRTId LIMIT 2]){
            c.Subject = 'Testing Subject: Compensation Case';
            c.Requested_By__c = userList[0].Id;
            c.Manager_of_Requested_By__c = userList[1].Id;
            caseListUpdate.add(c);
        }
        
        for (Case c : [Select Id, Requested_By__c, Manager_of_Requested_By__c from Case where RecordTypeId = :randTRTId LIMIT 2]){
            c.Subject = 'Testing Subject: R & T Case';
            c.Requested_By__c = userList[0].Id;
            c.Manager_of_Requested_By__c = userList[1].Id;
            caseListUpdate.add(c);
        }
        
        Test.startTest();
        update caseListUpdate;
        Test.stopTest();
    }
}