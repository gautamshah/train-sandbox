public with sharing class PRSOpportunityUpdateController {
    /*****************************************************************************************
    * Name    : PRS UpdateOpportunity Controller for JAPAN
    * Author  : Naoya Kotani
    * Date    : 10/06/2012
    * Purpose : Process Review Sheet
    * Outline :
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 01/24/13    Naoya Kotani         Speed up search query.
    * 12/02/13    Kan Hayashi           Optimize SOQL query
    * 04/14/15    Naoya Kotani         Change fisical year date for MDT
    *****************************************************************************************/

    /*** STATIC VARIABLE DECLARATION ***/
    //** Opportunity Owner type
    private static final String OWNER_OWN = 'own';
    private static final String OWNER_TEM = 'tem';
    private static final String OWNER_ALL = 'all';

    //** Opportunity Owner type
    private static final String VIEW_LIST = 'list';
    private static final String VIEW_SUMMARY = 'summary';

    //** SortKey
    private static final String SORT_1 = 'stagename';
    private static final String SORT_2 = 'closedate';
    private static final String SORT_3 = 'accountid';
    private static final String SORT_4 = 'jp_budget_amount__c';
    private static final String SORT_5 = 'jp_annual_amount__c';
    private static final String SORT_6 = 'totalopportunityquantity';
    private static final String SORT_7 = 'jp_target_product_category__c';
    private static final String SORT_8 = 'jp_key_dr__c';
    private static final String SORT_9 = 'jp_target_opportunity__c';
    private static final String SORT_10 = 'LastModifiedDate';
    private static final String SORT_11 = 'JP_Account_Rank__c';
    private static final String SORT_12 = 'JP_Target_Account__c';
    private static final String SORT_13 = 'Probability';

    //** Stage type
    private static final String STAGE_ALL = 'all';

    //** Product type
    private static final String PRODUCT_ALL = 'all';
    private static final String PRODUCT_DSP = 'dsp';
    private static final String PRODUCT_CAP = 'cap';

    //** Number of Search Results per Page
    private static final String NUMBEROFRECORDSPERPAGE = '20';
    private static final Integer MAXNUMBEROFRECORDSPERPAGE = 50;
    
    /*** LOCAL VARIABLE DECLARATION ***/
    //** Picklist values of Opportunity Phase
    public List<SelectOption> stageNames{get;set;}
    public List<String> selectedStage{get;set;}
    //** Picklist values of Opportunity Owner
    public List<SelectOption> ownerTypes{get;set;}
    //** Include Closed Opportunity Flag
    public Boolean includeClosed{get;set;}
    //** Include Base Opportunity Flag
    public Boolean includeBase{get;set;}
    //** Picklist values of Product Type
    public List<SelectOption> stage{get;set;}
    //** Picklist values of Product Type
    public List<SelectOption> productType{get;set;}
    //** Picklist values of Sort field
    public List<SelectOption> sortFields{get;set;}
    public List<SelectOption> sortOrders{get;set;}
    public String userID {get; set;}
    //** Picklist values of viewType
    public List<SelectOption> viewType{get;set;}
    
    public Integer rowCount {get; set;}
    public Integer totalRowCount {get; set;}
    public Integer searchResultPageNumber {get; set;}
    public Integer maxSearchResultPageNumber {get; set;}
    public String numOfRecords {get; set;}
    public List<SelectOption> numOfRecordsOptions{get;set;}
    private String query;
    private String query_short;

    //** Set of search criteria object
    public PRSOpportunityUpdateCriteria__c criteria{get;set;}
    //** Set of Update Oppotunities List
    public List<OppDTO> opplist{get;set;}

    //** Searched Flag
    public Boolean searched{get;set;}
    public Boolean viewtypeflg{get;set;}
    
    public Integer lineNo{get;set;}
    
    /*****************************************************************************************
     * DTO Class : Visualforce - Apex
     *   This class has Item
    /****************************************************************************************/
    public class ItemDTO{
        public String itemID{get;set;}
        public String itemName{get;set;}
        public ItemDTO(){
            itemID = '';
            itemName = '';
        }
    }

    /*****************************************************************************************
     * DTO Class : Visualforce - Apex
     *   This class has Opportunity Object & Update Flag
    /****************************************************************************************/
    public class OppDTO{
        public Boolean checked{get;set;}
        public Integer lineNo{get;set;}
        public Opportunity opp{get;set;}
        public List<ItemDTO> itDTO{get;set;}
        // public ItemDTO itDTO{get;set;}
        public OppDTO(){
            checked = false;
            lineNo = 0;
            opp = new Opportunity();
            itDTO = new List<ItemDTO>();
            //itDTO = new ItemDTO();
        }
    }
    
    public void next() {
        oppList.clear();
        List<Opportunity> searchResult = new List<Opportunity>();
        query = query_short + ' LIMIT ' + numOfRecords;
        Integer offset = Integer.valueOf(numOfRecords) * searchResultPageNumber;
        query += ' OFFSET ' + offset;
        searchResult = database.query(query);

      if(searchResult.size() >= MAXNUMBEROFRECORDSPERPAGE){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '検索条件で得られる結果が'+MAXNUMBEROFRECORDSPERPAGE+'件を超えています。'+MAXNUMBEROFRECORDSPERPAGE+'件目以降は表示されません。条件を指定して絞り込んでください。'));
      }

      rowCount = 1;
      LIST<OpportunityLineItem> lineItem = [SELECT Id, OpportunityID, PricebookEntry.Product2.Name FROM OpportunityLineItem WHERE OpportunityId IN :searchResult];
      for(Opportunity opp : searchResult){
          oppList.add(getOpportunityLineItem(opp, rowCount, lineItem));
          rowCount++;
      }
      rowCount = oppList.size();
      
      searchResultPageNumber++;
    }
    
    public void previous() {
        searchResultPageNumber = searchResultPageNumber - 2;
        oppList.clear();
        List<Opportunity> searchResult = new List<Opportunity>();
        query = query_short + ' LIMIT ' + numOfRecords;
        Integer offset = Integer.valueOf(numOfRecords) * searchResultPageNumber;
        query += ' OFFSET ' + offset;
        searchResult = database.query(query);

      if(searchResult.size() >= MAXNUMBEROFRECORDSPERPAGE){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '検索条件で得られる結果が'+MAXNUMBEROFRECORDSPERPAGE+'件を超えています。'+MAXNUMBEROFRECORDSPERPAGE+'件目以降は表示されません。条件を指定して絞り込んでください。'));
      }

      rowCount = 1;
      LIST<OpportunityLineItem> lineItem = [SELECT Id, OpportunityID, PricebookEntry.Product2.Name FROM OpportunityLineItem WHERE OpportunityId IN :searchResult];
      for(Opportunity opp : searchResult){
          oppList.add(getOpportunityLineItem(opp, rowCount, lineItem));
          rowCount++;
      }
      rowCount = oppList.size();
      
      searchResultPageNumber++;
    }

    /*****************************************************************************************
     * Initialize
    /****************************************************************************************/
    public void init(){
        // get Territory ID of login user from UserTerritory
        //MAP<ID, User> userNames = new MAP<ID, User>([SELECT ID, Name FROM User]);
        MAP<ID, User> userNames = new MAP<ID, User>([SELECT ID, Alias FROM User]); // Japan Original (Name -> Alias)
    
        userID = (String)UserInfo.getUserID();
    
        // 部下のテリトリーを全て一気に取得
        LIST<Territory> childTerritories = [SELECT Id FROM Territory WHERE ParentTerritoryId IN (
                                             SELECT TerritoryId FROM UserTerritory 
                                             WHERE IsActive = TRUE AND UserId = :userID
                                         )];                                     
        // 部下のUserIDを全て一気に取得
        LIST<UserTerritory> userID = [SELECT UserId FROM UserTerritory 
                                      WHERE IsActive = TRUE AND TerritoryId IN :childTerritories]; 
        
        viewType = new List<SelectOption>(); 
        viewType.add(new SelectOption(VIEW_LIST,'一覧表示'));
        viewType.add(new SelectOption(VIEW_SUMMARY,'サマリ表示'));
    
        /*** make search criteria of opportunity ower ***/ 
        ownerTypes = new List<SelectOption>(); 
        ownerTypes.add(new SelectOption(OWNER_OWN,'私の案件')); // My opportunities
        ownerTypes.add(new SelectOption(OWNER_TEM,'私のチーム案件')); // My opportunities
        
        for(UserTerritory usrID : userID){ 
            //ownerTypes.add(new SelectOption((String)usrID.get('UserId'),userNames.get((ID)usrID.get('UserId')).Name)); // Subordinate's Opportunities 
        ownerTypes.add(new SelectOption((String)usrID.get('UserId'),userNames.get((ID)usrID.get('UserId')).Alias)); // Japan Original (Name -> Alias)
        }
        // ownerTypes.add(new SelectOption(OWNER_ALL,'すべての商談')); // All Opportunities
        
        /*** make search criteria of opportuniry stage ***/
        stage = new List<SelectOption>();
        stage.add(new SelectOption(STAGE_ALL,'全て'));
        List <Schema.PicklistEntry> stageNameEntries= Schema.SObjectType.Opportunity.fields.StageName.getPicklistValues();
        for(Schema.PicklistEntry stageNameEntry : stageNameEntries){
            if(stageNameEntry.isActive()){
                stage.add(new SelectOption(stageNameEntry.getvalue(), stageNameEntry.getLabel()));
            }
        }

        //Product Type
        productType = new List<SelectOption>();
        productType.add(new SelectOption(PRODUCT_ALL,'全て'));
        productType.add(new SelectOption(PRODUCT_DSP,'ディスポ／消耗品'));
        productType.add(new SelectOption(PRODUCT_CAP,'ハード／器械物'));

        /*** make search criteria of Sort ***/
        sortFields = new List<SelectOption>();
        Map <String, Schema.SObjectField> sortFieldMap = Schema.SObjectType.Opportunity.fields.getMap();
        Set<String> sortFieldKeySet = sortFieldMap.keySet();
        sortFields.add(new SelectOption('' , '-- 指定無し --'));
        for(String sortFieldKey : sortFieldKeySet){
            // Set Sort Key
            if (sortFieldKey == SORT_1) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_2) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_3) { sortFields.add(new SelectOption(sortFieldKey, '施設名')); }
            else if (sortFieldKey == SORT_4) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_5) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_6) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_7) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_8) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_9) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_10) { sortFields.add(new SelectOption(sortFieldKey, sortFieldMap.get(sortFieldKey).getDescribe().getLabel())); }
            else if (sortFieldKey == SORT_11) { sortFields.add(new SelectOption(sortFieldKey, '病院ランク')); }
            else if (sortFieldKey == SORT_12) { sortFields.add(new SelectOption(sortFieldKey, '管理／非管理')); }
            else if (sortFieldKey == SORT_13) { sortFields.add(new SelectOption(sortFieldKey, '確度(%)')); }
        }
        
        //SortOrder
        sortOrders = new List<SelectOption>();
        sortOrders.add(new SelectOption('DESC','降順'));
        sortOrders.add(new SelectOption('ASC','昇順'));
        
        // Number of Records to Show in Results
        numOfRecords = NUMBEROFRECORDSPERPAGE;
        numOfRecordsOptions = new List<SelectOption>();
        numOfRecordsOptions.add(new SelectOption('10','10件'));
        numOfRecordsOptions.add(new SelectOption('20','20件'));
        numOfRecordsOptions.add(new SelectOption('50','50件'));
        
        // Initialize
        searchResultPageNumber = 0;
        maxSearchResultPageNumber =0;
        
        /*** make layout ***/
        if(searched != true){
            criteria = new PRSOpportunityUpdateCriteria__c();
        }
        searched = false;
    }

    /*****************************************************************************************
     * Get This Fisical Year
    /****************************************************************************************/
    public Integer getFiscalYear(){
        Integer fiscalYear;
        Date d = Date.today();
        if(d.month() >= 5 && d.month() <= 12) {
            fiscalYear = d.year() + 1;
        } else {
            fiscalYear = d.year();
        }
        return fiscalYear ;
    }

    /*****************************************************************************************
     * Opportunity update controller
    /****************************************************************************************/
    public PageReference bulkUpdate() {
        List<Opportunity> updateOppList = new List<Opportunity>();
        for(oppDto dto : opplist){
            if(dto.checked){
                updateOppList.add(dto.opp);
            }
        }
        if(updateOppList.size() > 0){
            update updateOppList;
        }
        search();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, updateOppList.size() + '件のデータの処理が完了しました。'));
        return null;
    }

    /*****************************************************************************************
     * Clear PRS Page
    /****************************************************************************************/
    public PageReference clear(){
        searched = false;
        criteria = new PRSOpportunityUpdateCriteria__c();
        selectedStage = new List<String>();
        oppList = new List<OppDto>();
        return null;
    }

    /*****************************************************************************************
     * Cancel
    /****************************************************************************************/
    public PageReference cancel(){
        search();
        return null;
    }

    /*****************************************************************************************
     * Serch Opportunity
    /****************************************************************************************/
    public PageReference search() {
      searched = true;
     
      List<Opportunity> searchResult = new List<Opportunity>();
      oppList = new List<OppDto>();
      //現在Dynamic SOQL内でのへ変数バインドは有効化されていません。有効化後はSOQLインジェクション対策に適用予定
      //Map<String,Schema.SObjectField> fieldMap = Opportunity.sObjectType.getDescribe().fields.getMap();
      //List<Schema.SObjectField> fieldList = fieldMap.values();
      //String selectFields ='';
      //for(Integer i =0; i< fieldList.size();i++){
      //    Schema.SObjectField field = fieldList.get(i);
      //    if(i==0){
      //        selectFields += field.getDescribe().getName() + ' ';
      //    }else{
      //        selectFields += ', ' + field.getDescribe().getName();
      //    }
      //}
      String selectFields ='';
      selectFields ='Name, ';
      selectFields += 'JP_Target_Opportunity__c, ';
      selectFields += 'StageName, ';
      selectFields += 'JP_Target_Account__c, ';
      selectFields += 'JP_Account_Rank__c, ';
      selectFields += 'accountId, ';
      selectFields += 'JP_Diagnosis1__c, ';
      selectFields += 'JP_Diagnosis2__c, ';
      selectFields += 'JP_Key_Dr__c, ';
      selectFields += 'JP_Key_Dr_Title__c, ';
      //selectFields += 'JP_Intimacy__c, ';
      selectFields += 'Intimacy_degree__c, ';
      selectFields += 'JP_OpportunityClassification__c, ';
      selectFields += 'JP_RentalFlg__c, ';
      selectFields += 'JP_MonthFlg__c, ';
      selectFields += 'JP_Target_Product_Category__c, ';
      selectFields += 'TotalOpportunityQuantity, ';
      selectFields += 'JP_AnnualAmount__c, ';
      selectFields += 'JP_Budget_Amount__c, ';
      selectFields += 'JP_Expected_Budget_Amount__c, ';
      selectFields += 'closeDate, ';
      selectFields += 'Probability, ';
      selectFields += 'JP_Is_Estimate__c, ';
      selectFields += 'WonLossReasons__c, ';
      selectFields += 'Capital_Budget_Status__c, ';
      selectFields += 'JP_Tactics__c, ';
      selectFields += 'JP_Review__c, ';
      selectFields += 'Description, ';
      selectFields += 'LastModifiedDate, ';
      selectFields += 'OwnerId, ';
      selectFields += 'Id ';
      
      query='SELECT ' + selectFields + ' FROM Opportunity';
      String joinString = ' WHERE JP_OpportunityClassification__c != \'Base\' AND ';

      if(criteria.name__c != null && criteria.name__c != ''){
          String likeName = String.escapeSingleQuotes(criteria.name__c);
          likeName = likeName.replace(' ', '%');
          likeName = likeName.replace('*', '%');
          likeName = likeName.replace('?', '%');
          likeName = likeName.replace('　', '%');
          likeName = likeName.replace('＊', '%');
          likeName = likeName.replace('？', '%');
          likeName = '%' + likeName + '%';
          query  += joinString + ' name LIKE \'' + likeName + '\'';
          joinString = ' AND ';
      }
/*
      if(selectedStage.size() > 0){
          String tempStageQuery = '';
          query  += joinString + ' (';

          String tempStageJoinString = '';
          for(String stageName : selectedStage){
              tempStageQuery += tempStageJoinString + ' stageName = \'' + String.escapeSingleQuotes(stageName) +'\'';
              tempStageJoinString = ' OR ';
          }
          tempStageQuery += ') ';
         
          query  += tempStageQuery;
          joinString = ' AND ';
      }
*/

      if(criteria.stage__c != null && criteria.stage__c != STAGE_ALL){
          query  += joinString + ' stageName = \'' + String.escapeSingleQuotes(criteria.stage__c) +'\'';
          joinString = ' AND ';
      }

      if(criteria.startDate__c != null){
          query  += joinString + ' closeDate >= ' + String.valueOf(criteria.startDate__c).replace('/','-');
          joinString = ' AND ';
      }
      if(criteria.endDate__c != null){
          query  += joinString + ' closeDate <= ' + String.valueOf(criteria.endDate__c).replace('/','-');
          joinString = ' AND ';
      }

      if(criteria.opportunitiesOwner__c == OWNER_OWN){
          query += joinString + ' ownerId = \'' + UserInfo.getUserId() +'\'';
          joinString = ' AND ';
      }
      else if(criteria.opportunitiesOwner__c == OWNER_TEM){
        query += joinString + ' (';

    // get Territory ID of login user from UserTerritory
        userID = (String)UserInfo.getUserID();
    
        // 部下のテリトリーを全て一気に取得
        LIST<Territory> childTerritories = [SELECT Id FROM Territory WHERE ParentTerritoryId IN (
                                             SELECT TerritoryId FROM UserTerritory 
                                             WHERE IsActive = TRUE AND UserId = :userID
                                         )];                                     
        // 部下のUserIDを全て一気に取得
        LIST<UserTerritory> userID = [SELECT UserId FROM UserTerritory 
                                      WHERE IsActive = TRUE AND TerritoryId IN :childTerritories]; 
        
        for(UserTerritory usrID : userID){ 
            query += ' ownerId = \'' + (String)usrID.get('UserId') +'\' OR';
    }
        query += ' ownerId = \'' + UserInfo.getUserId() +'\')';
        joinString = ' AND ';
      }
      else if(criteria.opportunitiesOwner__c != OWNER_ALL && criteria.opportunitiesOwner__c != null){
          query += joinString + ' ownerId = \'' + criteria.opportunitiesOwner__c +'\'';
          joinString = ' AND ';
      }

      if(criteria.productType__c == PRODUCT_DSP){
          query += joinString + ' Capital_Disposable__c = \'Disposable\'';
          joinString = ' AND ';
      }
      else if(criteria.productType__c == PRODUCT_CAP){
          query += joinString + ' Capital_Disposable__c = \'Capital\'';
          joinString = ' AND ';
      }

      if(criteria.includeClosed__c == FALSE && criteria.onlyClosed__c == FALSE){
          query += joinString + ' IsClosed = FALSE ';
          joinString = ' AND ';
      }
      else if(criteria.includeClosed__c == FALSE && criteria.onlyClosed__c ==TRUE){
          query += joinString + ' IsClosed = TRUE ';
          joinString = ' AND ';
      }
      else if(criteria.includeClosed__c == TRUE && criteria.onlyClosed__c ==TRUE){
          query += joinString + ' IsClosed = TRUE ';
          joinString = ' AND ';
      }

      if(criteria.includeBase__c != FALSE){
          query += joinString + ' JP_Target_Opportunity__c = TRUE ';
          joinString = ' AND ';
      }

      //Order
      if(criteria.sort__c != null && criteria.sort__c != ''){
          query  += ' ORDER BY ' + String.escapeSingleQuotes(criteria.sort__c) + ' ' + String.escapeSingleQuotes(criteria.sortOrder__c) + ' NULLS LAST ';
      } else {
          query  += ' ORDER BY closedate ASC NULLS LAST ';
      }

      //Query Limit
      query_short = query;
      searchResult = database.query(query);
      totalRowCount = searchResult.size();
      Decimal decTemp = totalRowCount / Decimal.valueOf(numOfRecords);
      maxSearchResultPageNumber = decTemp.round(System.RoundingMode.UP).intValue();
      
      query += ' LIMIT ' + numOfRecords;
      searchResult = database.query(query);

      if(searchResult.size() >= MAXNUMBEROFRECORDSPERPAGE){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '検索条件で得られる結果が'+MAXNUMBEROFRECORDSPERPAGE+'件を超えています。'+MAXNUMBEROFRECORDSPERPAGE+'件目以降は表示されません。条件を指定して絞り込んでください。'));
      }

      if(criteria.View_Type__c == VIEW_SUMMARY){
          viewtypeflg = true;
      } else {
          viewtypeflg = false;
      }
      
      // Modify 12/2/13 K. Hayashi
      rowCount = 1;
      LIST<OpportunityLineItem> lineItem = [SELECT Id, OpportunityID, PricebookEntry.Product2.Name FROM OpportunityLineItem WHERE OpportunityId IN :searchResult];
      for(Opportunity opp : searchResult){
          oppList.add(getOpportunityLineItem(opp, rowCount, lineItem));
          rowCount++;
      }
      rowCount = oppList.size();
      
      searchResultPageNumber = 1;
      
      return null;
    }
    /*****************************************************************************************
     * Get OpportunityLineItem
    /****************************************************************************************/
    public OppDto getOpportunityLineItem(Opportunity opp, Integer it, LIST<OpportunityLineItem> lineItem) {
        OppDto temp = new OppDto();
        temp.opp = opp;
        temp.lineNo = it;
        ItemDTO tempItem;
        for(OpportunityLineItem iiD : lineItem){
            // Modify 12/2/13 K. Hayashi
            if(iiD.OpportunityID == temp.opp.id) {
                 tempItem = new ItemDTO();
                 String tmpStr = (String)iiD.get('Id');
                 tempItem.itemID = tmpStr.substring(0,tmpStr.length()- 3);
                 tempItem.itemName = (String)iiD.PricebookEntry.Product2.Name;
                 temp.itDTO.add(tempItem);
             }
        }
        return temp;
    }
}