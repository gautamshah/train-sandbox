@isTest (seeAllData=true)
public class TestBatchEmptyRecycleBin {   
        static testMethod void testEmptyRecycleBin() {
        
        testUtility tu = new testUtility();
        Account a = tu.testAccount('US-Healthcare Facility','New Account Test Name 1');
        Delete a;
        
        BatchEmptyRecycleBin accTerritory = new BatchEmptyRecycleBin();
        accTerritory.query = 'Select Id from Account where isDeleted=true LIMIT 1';
        accTerritory.obj = 'Account Territory';
        Database.executeBatch(accTerritory);
        }
 }