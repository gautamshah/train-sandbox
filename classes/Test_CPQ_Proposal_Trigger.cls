/*
Test Class

CPQ_Proposal_Before
CPQ_Proposal_After

CPQ_ProposalProcesses.InsertPriceList
CPQ_ProposalProcesses.UpdateRecordType
CPQ_ProposalProcesses.UpdateStatusWhenCancelled
CPQ_ProposalProcesses.ResetApprovalStatusWhenFieldChanged
CPQ_ProposalProcesses.UpdateERPBySales
CPQ_ProposalProcesses.UpdateConfig
CPQ_PriceListProcesses.SelectOpportunityPriceBook
CPQ_ApprovalRetriggerFieldEngine

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-27      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
2016-11-01      Isaac Lewis         Migrated to cpqProposal_uTest.cls
===============================================================================
*/
@isTest
private class Test_CPQ_Proposal_Trigger
{
    // static Apttus_Config2__PriceList__c testPriceList;
    // static List<Apttus_Proposal__Proposal__c> testProposals;
    // static List<Account> testAccounts;
    // static void createTestData() {
    //
    //  Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
    //  Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
    //
    //  List<Account> testGPOs = new List<Account>{
    //      new Account(Name = 'Test GPO 1', Status__c = 'Active', Account_External_ID__c = 'USGG-SG0150', RecordTypeID = gpoRT.getRecordTypeId()),
    //      new Account(Name = 'Test GPO 2', Status__c = 'Active', Account_External_ID__c = 'USGG-SG0150', RecordTypeID = gpoRT.getRecordTypeId())
    //  };
    //  insert testGPOs;
    //
    //  testAccounts = new List<Account> {
    //      new Account(Status__c = 'Active', Name = 'Test Account w GPO1', Primary_GPO_Buying_Group_Affiliation__c = testGPOs[0].ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-111111'),
    //      new Account(Status__c = 'Active', Name = 'Test Account w GPO2', Primary_GPO_Buying_Group_Affiliation__c = testGPOs[1].ID,   RecordTypeID = acctRT.getRecordTypeId(), Account_External_ID__c = 'US-222222')      };
    //  insert testAccounts;
    //
    //  List<ERP_Account__c> testERPs = new List<ERP_Account__c> {
    //      new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[0].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
    //      new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[1].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', RMS_Total_Prior_12_Mos_Sales1__c = 100, ERP_Account_Status__c = 'Active'),
    //      new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[1].ID, ERP_Account_Type__c = 'X', ERP_Source__c = 'E1', RMS_Total_Prior_12_Mos_Sales1__c = 1000, ERP_Account_Status__c = 'Active')
    //  };
    //  insert testERPs;
    //
    //  List<CPQ_Approval_Retrigger_Field__c> testRetriggers = new List<CPQ_Approval_Retrigger_Field__c> {
    //      new CPQ_Approval_Retrigger_Field__c(SObject__c = 'Proposal', Field_API_Name__c = 'Duration__c'),
    //      new CPQ_Approval_Retrigger_Field__c(SObject__c = 'Proposal', Field_API_Name__c = 'RecordTypeId')
    //  };
    //  insert testRetriggers;
    //
 //         Map<String,Id> mapProposalRType = New Map<String,Id>();
    //     for(RecordType R: [Select Id, Name, DeveloperName From RecordType Where SobjectType = 'Apttus_Proposal__Proposal__c']) {
    //         mapProposalRType.Put(R.DeveloperName, R.Id);
    //     }
    //
    //  testProposals = new List<Apttus_Proposal__Proposal__c> {
    //      new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test No Account', Apttus_Proposal__Account__c = testAccounts[0].ID, RecordTypeID = mapProposalRType.get('Current_Price_Quote')),
    //      new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test No Account', Apttus_Proposal__Account__c = testAccounts[0].ID, RecordTypeID = mapProposalRType.get('Current_Price_Quote'), Apttus_QPApprov__Approval_Status__c = 'Pending Approval'),
    //      new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test No Account', Apttus_Proposal__Account__c = testAccounts[0].ID, RecordTypeID = mapProposalRType.get('Current_Price_Quote'), Apttus_QPApprov__Approval_Status__c = 'Approved')
    //  };
    // }
    //
    // static testMethod void myUnitTest() {
    //     createTestData();
    //
    //     Test.startTest();
    //      insert testProposals;
    //
    //      Apttus_Config2__ProductConfiguration__c testConfig = new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[2].ID, Apttus_Config2__PriceListId__c = testPriceList.ID, Apttus_Config2__Status__c = 'Saved', Apttus_Config2__VersionNumber__c = 1);
    //      insert testConfig;
    //
    //      //Test UpdateRecordType
    //      testProposals[0].Apttus_Proposal__Approval_Stage__c = 'Accepted';
    //      //UpdateERPBySales
    //      testProposals[0].Apttus_Proposal__Account__c = testAccounts[1].ID;
    //
    //      //UpdateStatusWhenCancelled
    //      testProposals[1].Apttus_QPApprov__Approval_Status__c = 'Cancelled';
    //      testProposals[1].Apttus_Proposal__Presented_Date__c = System.Today();
    //      //ResetApprovalStatusWhenFieldChanged
    //      //UpdateConfig
    //      testProposals[2].Duration__c = '1 1/2 Year';
    //      update testProposals;
    //
    //      //UpdateERPBySales
    //      testProposals[0].Apttus_Proposal__Account__c = null;
    //      update testProposals;
    //
    //     Test.stopTest();
    // }
    //
    // @IsTest(SeeAllData=true)
    // static void TestSelectOpportunityPriceBook() {
    //  List<CPQ_Variable__c> cs = [Select ID, Name From CPQ_Variable__c Where Name = 'Default Opportunity PriceBook'];
    //  if (cs.size() > 0) {
    //      delete cs;
    //  }
    //
    //  Account testAccount = new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111');
    //  insert testAccount;
    //
    //  Opportunity testOpp = new Opportunity(Name = 'Test Opp', AccountID = testAccount.ID, StageName = 'Identify', CloseDate = System.today());
    //  insert testOpp;
    //
    //  Test.startTest();
    //      Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test No Account', Apttus_Proposal__Account__c = testAccount.ID, Apttus_Proposal__Opportunity__c = testOpp.ID);
    //      insert testProposal;
    //  Test.stopTest();
    // }
    //
    // static testMethod void TestDisabledTrigger() {
    //  try{
    //  CPQ_Trigger_Profile__c testTrigger = new CPQ_Trigger_Profile__c(SetupOwnerId = UserInfo.getUserID(), CPQ_Agreement_After__c = true, CPQ_Agreement_Before__c = true, CPQ_Participating_Facility_After__c = true, CPQ_Product2_After__c = true, CPQ_Product2_Before__c = true, CPQ_ProductConfiguration_After__c = true, CPQ_ProductConfiguration_Before__c = true, CPQ_Proposal_After__c = true, CPQ_Proposal_Before__c = true, CPQ_Proposal_Distributor_After__c = true, CPQ_Rebate_After__c = true, CPQ_Rebate_Agreement_After__c = true);
 //         insert testTrigger;
    //  } catch (Exception e) {}
 //
    //  createTestData();
    //
    //     Test.startTest();
    //      insert testProposals;
    //      update testProposals;
    //     Test.stopTest();
    // }
}