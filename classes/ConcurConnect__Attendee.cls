/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Attendee {
    global Attendee() {

    }
    global Attendee(Contact contact) {

    }
    global Attendee(String id) {

    }
    global Attendee(String id, String lastName) {

    }
    global Attendee(String id, String lastName, String firstName) {

    }
    global Attendee(String id, String lastName, String firstName, String company) {

    }
    global String getCompany() {
        return null;
    }
    global String getCustom1() {
        return null;
    }
    global String getCustom10() {
        return null;
    }
    global String getCustom11() {
        return null;
    }
    global String getCustom12() {
        return null;
    }
    global String getCustom13() {
        return null;
    }
    global String getCustom14() {
        return null;
    }
    global String getCustom15() {
        return null;
    }
    global String getCustom16() {
        return null;
    }
    global String getCustom17() {
        return null;
    }
    global String getCustom18() {
        return null;
    }
    global String getCustom19() {
        return null;
    }
    global String getCustom2() {
        return null;
    }
    global String getCustom20() {
        return null;
    }
    global String getCustom3() {
        return null;
    }
    global String getCustom4() {
        return null;
    }
    global String getCustom5() {
        return null;
    }
    global String getCustom6() {
        return null;
    }
    global String getCustom7() {
        return null;
    }
    global String getCustom8() {
        return null;
    }
    global String getCustom9() {
        return null;
    }
    global String getFirstName() {
        return null;
    }
    global String getID() {
        return null;
    }
    global String getLastName() {
        return null;
    }
    global String getTitle() {
        return null;
    }
    global void setCompany(String value) {

    }
    global void setCustom1(String value) {

    }
    global void setCustom10(String value) {

    }
    global void setCustom11(String value) {

    }
    global void setCustom12(String value) {

    }
    global void setCustom13(String value) {

    }
    global void setCustom14(String value) {

    }
    global void setCustom15(String value) {

    }
    global void setCustom16(String value) {

    }
    global void setCustom17(String value) {

    }
    global void setCustom18(String value) {

    }
    global void setCustom19(String value) {

    }
    global void setCustom2(String value) {

    }
    global void setCustom20(String value) {

    }
    global void setCustom3(String value) {

    }
    global void setCustom4(String value) {

    }
    global void setCustom5(String value) {

    }
    global void setCustom6(String value) {

    }
    global void setCustom7(String value) {

    }
    global void setCustom8(String value) {

    }
    global void setCustom9(String value) {

    }
    global void setFirstName(String value) {

    }
    global void setID(String value) {

    }
    global void setLastName(String value) {

    }
    global void setTitle(String value) {

    }
}
