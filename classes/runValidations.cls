public class runValidations {
    
    private static set<String> sellFromAccRTIDs=new set<String>{Label.ASIA_Sub_Dealer_RT_ID,Label.ASIA_Sub_Entity_RT_ID,Label.ASIA_Distributor_RT_ID};
    
    private static set<String> setStaticCountry;
    private static set<String> setStaticProductCode;
    private static set<String> setStaticSellfrom;
    private static set<String> setStaticSellTo;
    private static Map<String,Id> mapPartCode;
    private static Map<String,Id> mapSellFromAccounts;
    private static Map<String,Id> mapSellToAccounts;
    
    public static void lookupValidations(List<Sales_Out__c> lstSalesOuttoValidate)
    {
        if(setStaticCountry == null)
        {
            setStaticCountry=new set<String>();
            setStaticProductCode=new set<String>();
            setStaticSellfrom=new set<String>();
            setStaticSellTo=new set<String>();
            mapPartCode=new Map<String,Id>();
            mapSellFromAccounts= new Map<String,Id>();
            mapSellToAccounts= new Map<String,Id>();
        }
                
        set<String> skuset=new set<String>();
        set<String> accsetSellFrom=new set<String>();
        set<String> accsetSellTo=new set<String>();
        
        set<String> setCountry=new set<String>();
        set<String> setProductCode=new set<String>();
        set<String> setSellfrom=new set<String>();
        set<String> setSellTo=new set<String>();
        if(lstSalesOuttoValidate!=null&&lstSalesOuttoValidate.size()>0)
        {
            set<String> submissionIDs = new Set<String>();
            
            for(Sales_Out__c sot:lstSalesOuttoValidate)
            {
                submissionIDs.add(sot.Cycle_Period__c);
            }
            
            Map<Id,Cycle_Period__c> submission_partnerIDType = new Map<Id, Cycle_Period__c>();
            for(Cycle_Period__c cp : [select Id, Partner_ID_Used__c,Distributor_Name__c from Cycle_Period__c where Id = :submissionIDs])
            {
                submission_partnerIDType.put(cp.Id,cp);
            }
                        
            set<String> PartnerAccountID = new Set<String>();
            set<String> PartnerProductID = new Set<String>();
            
            for(Sales_Out__c sot:lstSalesOuttoValidate)
            {
                if(submission_partnerIDType.get(sot.Cycle_Period__c)!=null &&
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null && 
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Account'))
                {
                    if(sot.Sell_From__c!=null && sot.Sell_From__c!='')
                        PartnerAccountID.add(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c + sot.Sell_From__c);
                    if(sot.Sell_To__c!=null && sot.Sell_To__c!='')
                        PartnerAccountID.add(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c + sot.Sell_To__c);
                }
                
                if(sot.Product_Code__c!=null && sot.Product_Code__c!='' && 
                    submission_partnerIDType.get(sot.Cycle_Period__c)!=null &&
                    submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null && 
                    submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Product SKU'))
                {
                    PartnerProductID.add(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c + sot.Product_Code__c);
                }
            }
            
            Map<String, String> Account_Partner_COV = new Map<String, String>();
            Map<String, String> Product_Partner_COV = new Map<String, String>();
            
            for(Partner_Account__c pa : [select Partner_Account_ID__c,COV_Account_ID__c, key__c 
                                         from Partner_Account__c where isActive__c = true and key__c = :PartnerAccountID])
            {
                Account_Partner_COV.put(pa.key__c, pa.COV_Account_ID__c);
            }
            
            for(Partner_Product_SKU__c pp : [select Partner_Product_Code__c,COV_SKU__c, key__c 
                                         from Partner_Product_SKU__c where key__c = :PartnerProductID])
            {
                Product_Partner_COV.put(pp.key__c, pp.COV_SKU__c);
            }  
            
            for(Sales_Out__c sot:lstSalesOuttoValidate)
            {
                if(sot.Currency_Text__c!=null)
                    sot.Currency_Text__c = sot.Currency_Text__c.toUpperCase();
                if(sot.Product_Code__c!=null)
                    sot.Product_Code__c = sot.Product_Code__c.toUpperCase();
                if(sot.UOM__c!=null)
                    sot.UOM__c = sot.UOM__c.toUpperCase();
                if(sot.Sell_To__c != null)
                    sot.Sell_To__c = sot.Sell_To__c.toUpperCase();
                
                if(sot.Selling_Price_Text__c==null||sot.Selling_Price_Text__c=='')
                {
                  sot.Selling_Price_Text__c = '0.0000';
                }
                if(sot.Sell_From__c==null||sot.Sell_From__c=='')
                    sot.Sell_From_Account__c = null;
                else
                    sot.Sell_From__c = sot.Sell_From__c.toUpperCase();
                
                if(setStaticCountry.contains(sot.Country__c)==false)
                {
                    setStaticCountry.add(sot.Country__c);
                    setCountry.add(sot.Country__c);
                }
                if(submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null &&
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Product SKU'))
                {
                    if(Product_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c) && 
                       setStaticProductCode.contains(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c))==false)
                    {
                        string temp = Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c);
                        setStaticProductCode.add(temp);
                        setProductCode.add(temp);
                    }
                }
                else if(setStaticProductCode.contains(sot.Product_Code__c)==false)
                {
                    setStaticProductCode.add(sot.Product_Code__c);
                    setProductCode.add(sot.Product_Code__c);
                }
                
                if(submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null &&
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Account'))
                {
                    if(Account_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_From__c) && 
                       setStaticSellfrom.contains(Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_From__c))==false)
                    {
                        string temp = Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_From__c);
                        setStaticSellfrom.add(temp);
                        setSellfrom.add(temp);
                    }
                    if(Account_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_To__c) && 
                       setStaticSellfrom.contains(Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_To__c))==false)
                    {
                        string temp = Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_To__c);
                        setStaticSellTo.add(temp);
                        setSellTo.add(temp);
                    }
                }
                else 
                {
                    if(setStaticSellfrom.contains(sot.Sell_From__c)==false)
                    {
                        setStaticSellfrom.add(sot.Sell_From__c);
                        setSellfrom.add(sot.Sell_From__c);
                    }
                    if(setStaticSellTo.contains(sot.Sell_To__c)==false)
                    {
                        setStaticSellTo.add(sot.Sell_To__c);
                        setSellTo.add(sot.Sell_To__c);
                    }
                }
                
            }
            System.debug('setSellTo:'+setSellTo);
            System.debug('setProductCode:'+setProductCode);
            System.debug('setStaticCountry:'+setStaticCountry);
            List<RecordType> CFNRecordType = [SELECT Id FROM RecordType Where DeveloperName = 'Master_AP_CFN_Record'];
            if(setProductCode.size()>0)
            {               
                for(Product_SKU__c sku:[select Id,SKUSAPID__c,Country__c,RecordTypeId from Product_SKU__c where SKUSAPID__c in :setProductCode and SKUSAPID__c!=null and SKUSAPID__c!=''])
                {
                    if(sku.RecordTypeId == CFNRecordType[0].Id) {
                        if(mapPartCode.keyset().contains(sku.SKUSAPID__c)==false)
                            mapPartCode.put(sku.SKUSAPID__c,sku.ID);
                    }
                } 
            }
            System.debug('mapPartCode:'+mapPartCode);
            if(setSellfrom.size()>0 || setSellTo.size()>0)
            {
                for(Account acc:[select Id,Account_SAP_ID__c,RecordTypeId from Account where (Account_SAP_ID__c in :setSellFrom or Account_SAP_ID__c in :setSellTo)
                //for(Account acc:[select Id,Account_SAP_ID__c,RecordTypeId from Account where (Account_SAP_ID__c in :setSellFrom or Account_SAP_ID__c in :setSellTo)
                            and Account_SAP_ID__c!=null and Account_SAP_ID__c!='' and Status__c = 'Active'])
                {
                    if(setSellfrom.contains(acc.Account_SAP_ID__c) 
                       //&& mapSellFromAccounts.keyset().contains(acc.Account_External_ID__c)==false
                       && sellFromAccRTIDs.contains(acc.RecordTypeId))
                        mapSellFromAccounts.put(acc.Account_SAP_ID__c,acc.ID);
                    //if(setSellTo.contains(acc.Account_External_ID__c) && mapSellToAccounts.keyset().contains(acc.Account_External_ID__c)==false)
                    
                    if(!mapSellToAccounts.keyset().contains(acc.Account_SAP_ID__c))
                        mapSellToAccounts.put(acc.Account_SAP_ID__c,acc.ID);
                }
            }
            skuset=mapPartCode.keyset();
            accSetSellFrom=mapSellFromAccounts.keyset();
            accSetSellTo=mapSellToAccounts.keyset();
            
            for(Sales_Out__c sot:lstSalesOuttoValidate)
            {
                FileUploadErrors.CheckErrors(sot);
                
                if(submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null && 
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Product SKU'))
                {
                    /*if(Product_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c) == false || 
                       skuset.contains(sot.Country__c + Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c))==false)*/
                    if(Product_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c) == false || 
                       skuset.contains(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c))==false)
                    {
                        sot.Product_Code1__c=null;
                        sot.Error_messages__c=sot.Error_messages__c + label.product_code_not_found + ',';
                        System.debug('Error Found1');
                    }
                    else
                    {
                        //sot.Product_Code1__c=mapPartCode.get(sot.Country__c + Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c));
                        sot.Product_Code1__c=mapPartCode.get(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code__c));
                    }
                }
                else
                {
                    System.debug('SKU Set:'+skuset);
                    //if(skuset.contains(sot.Country__c + sot.Product_Code__c)==false)
                    if(skuset.contains(sot.Product_Code__c)==false)
                    {
                        sot.Product_Code1__c=null;
                        sot.Error_messages__c=sot.Error_messages__c + label.product_code_not_found + ',';
                        System.debug('Error Found2');
                    }
                    else
                    {
                        //sot.Product_Code1__c=mapPartCode.get(sot.Country__c + sot.Product_Code__c);
                        sot.Product_Code1__c=mapPartCode.get(sot.Product_Code__c);
                    }
                }
                
                if(submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null &&
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Account'))
                {
                    if(sot.Sell_From__c!=null && sot.Sell_From__c!= '' && 
                       (Account_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_From__c) == false ||
                       accSetSellFrom.contains(Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_From__c))==false))
                    {
                        sot.Sell_From_Account__c=null;
                        sot.Error_messages__c=sot.Error_messages__c+ label.Sell_From_Account_Not_Found +',';
                    }
                    else
                    {
                        sot.Sell_From_Account__c=mapSellFromAccounts.get(Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_From__c));
                    }
                    if(Account_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_To__c) == false ||
                       accSetSellTo.contains(Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_To__c))==false)
                    {
                        sot.Sell_To_Account__c=null;
                        sot.Error_messages__c=sot.Error_messages__c + label.Sell_To_Account_Not_Found + ',';
                    }
                    else
                    {
                        sot.Sell_To_Account__c=mapSellToAccounts.get(Account_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Sell_To__c));
                    }
                }
                else
                {
                    if(sot.Sell_From__c!=null && sot.Sell_From__c!= '' && accSetSellFrom.contains(sot.Sell_From__c)==false)
                    {
                        sot.Sell_From_Account__c=null;
                        sot.Error_messages__c=sot.Error_messages__c+ label.Sell_From_Account_Not_Found +',';
                    }
                    else
                    {
                        sot.Sell_From_Account__c=mapSellFromAccounts.get(sot.Sell_From__c);
                    }
                    if(accSetSellTo.contains(sot.Sell_To__c)==false)
                    {
                        sot.Sell_To_Account__c=null;
                        sot.Error_messages__c=sot.Error_messages__c + label.Sell_To_Account_Not_Found + ',';
                    }
                    else
                    {
                        sot.Sell_To_Account__c=mapSellToAccounts.get(sot.Sell_To__c);
                    }
                }
                
            }
        }
    }
    
    public static void CILookupValidations(List<Channel_Inventory__c> lstCItoValidate)
    {
        System.debug('CILookupValidations');
        if(mapPartCode==null)
            mapPartCode=new Map<String,Id>();
        
        set<String> skuset=new set<String>();
        
        set<String> setCountry=new set<String>();
        set<String> setProductCode=new set<String>();
        
        if(lstCItoValidate!=null&&lstCItoValidate.size()>0)
        {
            System.debug('lstCItoValidate:'+lstCItoValidate);
            set<String> submissionIDs = new Set<String>();
            
            for(Channel_Inventory__c sot:lstCItoValidate)
            {
                submissionIDs.add(sot.Cycle_Period__c);
            }
            
            System.debug('submissionIDs:'+submissionIDs);
            
            Map<Id,Cycle_Period__c> submission_partnerIDType = new Map<Id, Cycle_Period__c>();
            for(Cycle_Period__c cp : [select Id, Partner_ID_Used__c,Distributor_Name__c from Cycle_Period__c where Id = :submissionIDs])
            {
                submission_partnerIDType.put(cp.Id,cp);
            }
            System.debug('Submission PartnerTypeIdType:'+submission_partnerIDType);
                        
            set<String> PartnerProductID = new Set<String>();
            
            for(Channel_Inventory__c sot:lstCItoValidate)
            {   
                if(sot.Product_Code_Text__c!=null && sot.Product_Code_Text__c!='' && 
                    submission_partnerIDType.get(sot.Cycle_Period__c)!=null &&
                    submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null && 
                    submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Product SKU'))
                {
                    PartnerProductID.add(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c + sot.Product_Code_Text__c);
                }
            }
            System.debug('PartnerProductID:'+PartnerProductID);
            Map<String, String> Product_Partner_COV = new Map<String, String>();
            
            for(Partner_Product_SKU__c pp : [select Partner_Product_Code__c,COV_SKU__c, key__c 
                                         from Partner_Product_SKU__c where key__c = :PartnerProductID])
            {
                Product_Partner_COV.put(pp.key__c, pp.COV_SKU__c);
            }
            
            System.debug('Product_Partner_COV:'+Product_Partner_COV);
            for(Channel_Inventory__c sot:lstCItoValidate)
            {
                if(sot.UOM__c!=null)
                    sot.UOM__c = sot.UOM__c.toUpperCase();
                if(sot.Product_Code_Text__c!=null)
                    sot.Product_Code_Text__c = sot.Product_Code_Text__c.toUpperCase();
                if(setCountry.contains(sot.Country__c)==false)
                    setCountry.add(sot.Country__c);
                    
                if(submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null && 
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Product SKU') &&
                   Product_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c) && 
                   setProductCode.contains(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c))==false)
                {
                    setProductCode.add(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c));
                }
                else if(setProductCode.contains(sot.Product_Code_Text__c)==false)
                    setProductCode.add(sot.Product_Code_Text__c);
            }
            System.debug('setProductCode:'+setProductCode);
            List<RecordType> CFNRecordType = [SELECT Id FROM RecordType Where DeveloperName = 'Master_AP_CFN_Record'];
            //if(setCountry.size()>0 || setProductCode.size()>0)
            if(setProductCode.size()>0)
            {    
                for(Product_SKU__c sku:[select Id,SKUSAPID__c,RecordTypeId from Product_SKU__c where SKUSAPID__c in :setProductCode  and SKUSAPID__c!=null and SKUSAPID__c!=''])
                {
                    System.debug('SKU:'+sku);
                    if(sku.RecordTypeId == CFNRecordType[0].Id) {
                        /*if(mapPartCode.keyset().contains(sku.Country__c + sku.SKUSAPID__c)==false)
                            mapPartCode.put(sku.Country__c + sku.SKUSAPID__c,sku.ID);*/
                        
                        if(mapPartCode.keyset().contains(sku.SKUSAPID__c)==false)
                            mapPartCode.put(sku.SKUSAPID__c,sku.ID);
                    }
                }
            } 
            skuset=mapPartCode.keyset();
            
            System.debug('skuset:'+skuset);
            for(Channel_Inventory__c sot:lstCItoValidate)
            {
                FileUploadErrors.CheckInventoryErrors(sot);
                if(submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c != null && 
                   submission_partnerIDType.get(sot.Cycle_Period__c).Partner_ID_Used__c.contains('Product SKU'))
                {   
                    /*if(Product_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c) == false || 
                       skuset.contains(sot.Country__c + Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c))==false)*/
                    if(Product_Partner_COV.containsKey(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c) == false || 
                       skuset.contains(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c))==false)   
                    {
                        sot.Product_Code__c=null;
                        sot.Error_Message__c=sot.Error_Message__c+ label.Product_code_not_found + ',';
                        System.debug('Error Message');
                    }
                    else
                    {
                        //sot.Product_Code__c=mapPartCode.get(sot.Country__c + Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c));
                        sot.Product_Code__c=mapPartCode.get(Product_Partner_COV.get(submission_partnerIDType.get(sot.Cycle_Period__c).Distributor_Name__c+sot.Product_Code_Text__c));
                    }
                    
                }
                else
                {
                    //if(skuset.contains(sot.Country__c + sot.Product_Code_Text__c)==false)
                    if(skuset.contains(sot.Product_Code_Text__c)==false)
                    {
                        sot.Product_Code__c=null;
                        sot.Error_Message__c=sot.Error_Message__c+ label.Product_code_not_found + ',';
                        System.debug('Error Message');
                    }
                    else
                    {
                        //sot.Product_Code__c=mapPartCode.get(sot.Country__c + sot.Product_Code_Text__c);
                        sot.Product_Code__c=mapPartCode.get(sot.Product_Code_Text__c);
                    }
                }
            }
        }
    }
}