public with sharing class EMS_Controller_Exportcsv {

    private Id EventId;
    public CurrencyType c{get;set;}

    public EMS_Event__c CurrentEMSRecord{get;set;}
    
    public List<Event_Beneficiaries__c> LEventBfTransactions;
    public String filetype{get;set;}
    public String ConcatenatedDivManager{get;set;}

    //Constructor
    public EMS_Controller_Exportcsv (ApexPages.StandardController controller) {
    
         LEventBfTransactions= new List<Event_Beneficiaries__c>(); 
         EventId= ApexPages.currentPage().getParameters().get('EventId');
         filetype=ApexPages.currentPage().getParameters().get('filetype');
         CurrentEMSRecord=[Select Id,Name,Start_Date__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Design_Fee_Remarks__c,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Fee__c,Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,ISR_CSR__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Local_GTA__c,Total_Meal_Amount__c,Total_Consultancy_Fee__c,Total_Gifts_amount__c,Total_Air_Travel__c,Total_HCP_Expenses__c,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Venue__c,Publication_grant__c,Total_Others_Amount__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Total_LodA__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,
                                  Total_No_of_Recipients__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,CurrencyISOCode,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,Owner_Country__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Division_Multiple__c,Division_Primary__c,GBU_Multiple__c  
                                  from EMS_Event__c where Id=:EventId];
                                  
        /*                          
        If(currentEMSRecord.Owner_Country__c=='CN'){
        
        ChinaDivisionManager__c myCS1 = ChinaDivisionManager__c.getValues(currentEMSRecord.Division_Primary__c);
        ConcatenatedDivManager=currentEMSRecord.Division_Primary__c+'/'+myCS1.Division_Director__c;
        
        }
        */
        c = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and Isocode=:currentEMSRecord.CurrencyISOCode limit 1];
        system.debug('---------'+c);
                             
         
    }
    
    /*public Integer TotHCPExpense{
    get{
    Integer a = Integer.ValueOf((currentEMSRecord.Total_HCP_Expenses__c)/(c.conversionRate));  
    return a;
    }
    set;
    }
    */
    
    
    public List<Event_Beneficiaries__c> getLEventBfTransactions(){
    
    LEventBfTransactions= new List<Event_Beneficiaries__c>();
    Set<String> Type=new Set<String>();
    String InviteeType;
    if(filetype=='External'){
    InviteeType='Budgeted Beneficiary';
    Type.add('Individual Beneficiary');
    Type.add('Organization');
    
    }
    else if(filetype=='Internal'){
    InviteeType='Covidien Staff';
    Type.add('Covidien Staff');
    }
    LEventBfTransactions=  [Select Id,Beneficiary_Text__c,Contact__r.Account.BillingCountry,Contact__r.Account.Phonetic_Name__c,Organization__r.Phonetic_Name__c,Organization__r.BillingCountry,Contact__r.Phonetic_Name__c,Contact__r.Phonetic_Last_Name__c,Employee__r.Title__c,Contact__r.Account.Name,Organization__r.Name,Employee__r.Department__c,Employee__r.Country__c,Beneficiary_Type__c,Period_of_Benefit__c,Total_Beneficiary_Expenses__c,Lodging_Amount__c,Coach__c,Air_Travel__c,Booth_Space__c,Miscellaneous__c,Advertisement__c,Meeting_Room_Rental__c,Meeting_Package__c,Internal_Order_Fee__c,Sponsor__c,Gifts__c,Others_Others__c,Consultancy_fees__c,Beneficiary__c,Meal__c,Pickup__c,Taxi__c,Total_Ground_Transportation__c,Train__c,Type__c,Others__c,Invitee_Type__c from Event_Beneficiaries__c where EMS_Event__c=:EventId and Type__c in :Type and Invitee_Type__c=:InviteeType];                                                                                          
    return LEventBfTransactions;    
    }
    
    public string bom {
        get {
            return EncodingUtil.base64decode('77u/').tostring();
        }
    }
    
    
    
}