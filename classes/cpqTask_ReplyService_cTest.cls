@isTest (SeeAllData=true)
public class cpqTask_ReplyService_cTest
{

    @isTest
    public static void RecordEmailAndCloseTask() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
    
        email.subject = 'Some preceding text {Ref:' + sObject_c.dummyId(Task.class, 1) + 'AAA:Ref} and follow up text';
        email.plainTextBody = 'Is Implemented by CS: Yes';
        
        TaskReplyService service = new TaskReplyService();
        service.RecordEmailAndCloseTask(email);
    }
}