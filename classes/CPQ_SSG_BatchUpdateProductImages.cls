/**
Batch job to update all surgical product icon images for Apttus from the URL on
their associated Product_SKU__c records.

This job makes an HTTP callout for each product so the batch size should be
limited to 100 or less to avoid running into the limit of 100 callouts per
transaction.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
04/11/2016      Bryan Fry           Created
===============================================================================
*/
global class CPQ_SSG_BatchUpdateProductImages implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='Select Id, ProductCode, Product_SKU__r.Product_Image_URL__c, Apttus_Config2__IconId__c From Product2 Where Apttus_Surgical_Product__c = true And Product_SKU__r.Product_Image_URL__c != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Product2> scope) {
    	Map<Id,Id> productAttachmentMap = new Map<Id,Id>();
    	List<Attachment> attachments = new List<Attachment>();
    	List<Id> oldAttachmentIds = new List<Id>();

    	// Loop through the products in scope and call the URL in their Product_Image_URL__c field to get their image
    	// content for creating an attachment.
    	for (Product2 product: scope) {
    		if (product.Product_SKU__r.Product_Image_URL__c != null) {
	    		Blob imgBody = getImageContentFromURL(product.Product_SKU__r.Product_Image_URL__c);
	    		Attachment att = new Attachment(Name = product.ProductCode + ' Image' + '.jpg', Body = imgBody, ParentId = product.Id);
	    		attachments.add(att);
	    	}
	    	if (product.Apttus_Config2__IconId__c != null) {
	    		oldAttachmentIds.add(product.Apttus_Config2__IconId__c);
	    	}
    	}

    	// Get the old attachments and delete them.
    	if (!oldAttachmentIds.isEmpty()) {
    		List<Attachment> oldAttachments = [Select Id From Attachment Where Id in :oldAttachmentIds];
    		if (oldAttachments != null && !oldAttachments.isEmpty()) {
    			delete oldAttachments;
    		}
    	}

    	// Insert the new attachments
    	if (!attachments.isEmpty()) {
    		insert attachments;

	    	// Get a map from product Id to attachment Id to set on the correct product field
	    	for (Attachment att: attachments) {
	    		productAttachmentMap.put(att.ParentId, att.Id);
	    	}

	    	// Go through products and set the Apttus_Config2__IconId__c field on the product to the new Attachment Id
	    	for (Product2 product: scope) {
				product.Apttus_Config2__IconId__c = productAttachmentMap.get(product.Id);
	    	}

	    	// Update the products to contain the new attachment Id
	    	update scope;
	    }
    }

    // Connect to the URL passed in and retrieve a JPEG image. Get the Blob
    // version of the content and return so it can be used as an attachment.
    private Blob getImageContentFromURL(String url) {
	    Http h = new Http();
	    HttpRequest req = new HttpRequest();
	    req.setEndpoint(url);
	    req.setMethod('GET');
		req.setHeader('Content-Type', 'image/jpeg');
		req.setCompressed(true);

	    HttpResponse res = h.send(req);
	    return res.getBodyAsBlob();
	}

    global void finish(Database.BatchableContext BC) {}
}