@isTest(SeeAllData=true)
private class CaseTeamAddTest{
/****************************************************************************************
    * Name    : CaseTeamAddTest
    * Author  : Brajmohan Sharma
    * Date    : 28-Feb-2017
    * Purpose : To add Requested By and Manager of Requested By in case team when case is created.
    * case    : 00963024
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/ 
    
   
   
   
   static testMethod void testCaseTeam1()
    { 
   Profile P = [select id from profile where name = 'system administrator'];
String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
    User tuser = new User(  firstname = 'testcase',
                            lastName = 'data',
                            email = uniqueName + '@test' + orgId + '.org',
                            Username = uniqueName + '@test' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = P.Id);
                            //UserRoleId = roleId);
    insert tuser;    
    
     List<User> userlist = [select id from user where user.FirstName ='Brajmohan' AND user.LastName = 'Sharma'];
       Case c = new Case();
       c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Inquiry SI').getRecordTypeId();
       c.Current_State__c = 'new';
       c.Manager_of_Requested_By__c = tuser.id;
       c.Reason = 'Sales Inquiry';
        insert c;
         }
    
    }