global with sharing class Activities_Mobile {
 
 /****************************************************************************************
   * Name    : Activities_Mobile
   * Author  : Shawn Clark
   * Date    : 03/23/2016 
   * Purpose : Custom Activities Interface for mobile
 *****************************************************************************************/
    public Event e {get; private set;}
    public String ReccType {get;set;}
    public transient List<SelectOption> sol {get;set;}          //Holds RecordType Picklist Values
    public List<ApexPickListValLookup__c> apexpick {get; set;}  //Holds ActivityType Picklist Values based on Country
    public List <Contact> acc {get;set;}              //Holds Contacts in Lookup Window
    public List <Opportunity> opp {get;set;}          //Holds Opportunities in Lookup Window
    public Boolean IsRecTypeDisabled {get;set;}
    public String ActivityType {get;set;}
    public String UserCountry {get; set;}  //Used by Autocomplete
    
    public transient Integer Size {get; set;}
    public transient Integer OppSize {get; set;}
    public Boolean LockAccountSelection {get; set;}
    public String NewEvent{get; set;}
    public Boolean DisableContactLookup {get; set;}
    public Boolean DisableOpportunityLookup {get; set;}
    public Boolean ShowKoreaOnlyFields {get; set;}
    public transient String searchTerm {get; set;}  //Used by Autocomplete
    public transient String searchquery {get; set;}
    public transient String oppsearchquery {get; set;}
    public Integer IndexNum {get;set;}
    public Integer OppIndexNum {get;set;}
    public Integer PageLoadCount {get;set;}
    
    //Account Variables
    public String selectedAccountGuidId {get; set;} 
    public String selectedAccountName {get; set;} 

    public String selectedNotes {get; set;}
    //Korea Only
    public String selectedProductBU {get; set;}
    public String selectedProduct {get; set;}
    
    //Contact Variables
    public String selectedContactId {get; set;}
    public String selectedContactName {get; set;}
    
    //Opportunity Variables
    public String selectedOppId {get; set;}
    public String selectedOppName {get; set;}
    Public String OppStageFilter {get;set;}

    //Date Time Math & Parsing
    public transient Datetime OriginalDateTime {get;set;}
    public transient Datetime UpdatedDateTime {get;set;}
    public transient Integer currTimeMinute {get;set;}
    public transient Integer currTimeSecond {get;set;}
    public transient String currTimeMinuteStr {get;set;}
    public transient String currTimeMinuteRightOne {get;set;}
    
    public Date currStartDate {get;set;}
    public Time currStartTime {get;set;}  
    public Date currEndDate {get;set;}
    public Time currEndTime {get;set;}
    
    public Datetime StartDate {get;set;}
    public Datetime EndDate {get;set;}


    // SOQL query loads the case, with related Sample & Sample Product Data
    public Activities_Mobile () {
    
    //Get Users Country
    UserCountry = [select Country from user where id=:userinfo.getuserid()].Country;
    apexpick = [SELECT PickListVal__c FROM ApexPickListValLookup__c WHERE Country__c = :UserCountry];
    //Default - Allow user to select account
    LockAccountSelection     = false;
    DisableContactLookup     = true;
    DisableOpportunityLookup = true;
    selectedProductBU = 'ZZZ';
    selectedProduct = 'ZZZ';
    if (UserCountry == 'KR')
    {
    ShowKoreaOnlyFields = true;
    }
    else 
    {
    ShowKoreaOnlyFields = false;
    }
    //Only Set the Datetime on Initial Load, dont want to override user modifications
    IF(PageLoadCount==null) 
    {  
       SelectedAccountName = 'Please Enter Account Name';
       OriginalDateTime = System.now(); // returns date time value in GMT time zone.

       //Zero out seconds
       currTimeSecond = OriginalDateTime.Second();
       currTimeSecond = currTimeSecond*-1; 
       OriginalDateTime = OriginalDateTime.addSeconds(currTimeSecond);

       //Identify What the current Minute is
       currTimeMinute = OriginalDateTime.Minute();
       currTimeMinuteStr = String.ValueOf(currTimeMinute);
       currTimeMinuteRightOne = currTimeMinuteStr.Right(1);

       //Round Time Down to Nearest 5 minutes
       IF (currTimeMinuteRightOne == '1' || currTimeMinuteRightOne == '6') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-1);
       }
       ELSE IF (currTimeMinuteRightOne == '2' || currTimeMinuteRightOne == '7') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-2);
       }
       ELSE IF (currTimeMinuteRightOne == '3' || currTimeMinuteRightOne == '8') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-3);
       }
       ELSE IF (currTimeMinuteRightOne == '4' || currTimeMinuteRightOne == '9') 
       {
           UpdatedDateTime = OriginalDateTime.addMinutes(-4);
       }
       ELSE 
       {
          UpdatedDateTime = OriginalDateTime;
       }
   
       //Now Set the Current End Date & Time
       currEndDate = UpdatedDateTime.date();
       currEndTime = UpdatedDateTime.time();
       EndDate = datetime.newinstance(currEndDate,currEndTime);
            
       //Start Date is 0.5 Hours before End
       UpdatedDateTime = UpdatedDateTime.addMinutes(-30);
       currStartDate = UpdatedDateTime.date();
       currStartTime = UpdatedDateTime.time();
       StartDate = datetime.newinstance(currStartDate,currStartTime);
    
       //If there is only One Record Type, Set it, and Disable the field    
       getOpportunityRecTypes();
       IF (sol.size() < 2) 
       {
          IsRecTypeDisabled = true;
          ReccType = sol[0].getValue();
       }
       Else 
       {
          IsRecTypeDisabled = false;
       }
    }
    //Otherwise just add a PageLoad Count and don't recalc the date/time
    ELSE 
    {
       PageLoadCount = PageLoadCount+1;
    }  
   }
           

    //Autocomplete JSRemoting Account Name
    @RemoteAction
    public static List<Account> searchAccount(String searchTerm) {
        System.debug('Account Name is: '+searchTerm );
        List<Account> accounts = Database.query('SELECT Name, Id, Account_External_ID__c, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'  ORDER BY Name LIMIT 50');
        return accounts;     
    }          


    //Get AccountsType by Country
    public list<SelectOption> getActivityTypeList() {
       list<SelectOption> options = new list<SelectOption>();
       options.add(new SelectOption('','-- Select Activity Type --')); 
              
       //If the User belongs to a Country that has Picklist Values Defined, then use them
       if(String.IsEmpty(UserCountry) || apexpick.size() == 0)
       {
       Schema.DescribeFieldResult fieldResult = Event.Activity_Type__c.getDescribe(); 
       List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
          for(Schema.picklistEntry f:ple)    
          {    
          options.add(new selectOption(f.getLabel(),f.getValue()));                  
          }    
          return Options; 
       } 
       Else  
       {  
       for (list<ApexPickListValLookup__c> rts : [SELECT PickListVal__c FROM ApexPickListValLookup__c WHERE Country__c = :UserCountry ORDER BY PickListVal__c]) 
          {
          for (ApexPickListValLookup__c rt : rts) 
             {
             options.add(new SelectOption(rt.PickListVal__c, rt.PickListVal__c));
             } 
          } 
          return options;
       }  
    } 


    //Get Opportunity Stages
        public List<SelectOption> getOppStage() {    
        List<SelectOption> options =  new List<SelectOption>();  
        options.add(new selectOption('','--- Show All ---'));   
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
           for(Schema.picklistEntry f:ple)    
           {    
               options.add(new selectOption(f.getLabel(),f.getValue()));                  
           }    
           return Options;    
    }  

    


    //Get Record Types for the Current User
    public List<SelectOption> getOpportunityRecTypes() {
    List <RecordType> rl;
    sol = new List<SelectOption>();
    rl = [select Id, Name from RecordType where sObjectType='Event' and isActive=true];
       Schema.DescribeSObjectResult d = Schema.SObjectType.Event;
       Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
       
        for (RecordType r : rl) {
                if(rtMapById.get(r.id).isAvailable()){
                    sol.add(new SelectOption(r.id,r.Name));
                }
         }
           return sol;
        }  
  
    //Search Contact
    public PageReference searchMain(){  
    selectedAccountGuidId = selectedAccountGuidId;
    IF (String.isEmpty(selectedAccountGuidId)) 
    {
    system.debug('Account Guid For Yale is: ' + selectedAccountGuidId);
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You cannot lookup a contact, until you select an account'));
    return null;
    }
    Else
    {
    system.debug('Account Guid For Yale is: ' + selectedAccountGuidId);
    searchquery = 'select Salutation, firstname, lastname, name, id, AccountID, Contact.Account.Name, Personal_Email__c, ' +
                   'MobilePhone, Phone_Number_at_Account__c, Department_picklist__c, Other_Department__c, ' +
                   'Affiliated_Role__c, Title, Specialty1__c ' +
                   'from contact where AccountId = :selectedAccountGuidId and name like \'%' + String.escapeSingleQuotes(selectedContactName ) + '%\' order by Name ASC LIMIT 100';
       
    acc= Database.query(searchquery); 
    Size = acc.size();    
    IF (Size == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }                       
    PageReference pageRef= new PageReference('/apex/ContactLookUp');
    pageRef.setredirect(false);  
    return pageRef;   
    } 
    }
   
    //Search Opportunities  
    public PageReference searchOpp(){  
    selectedAccountGuidId = selectedAccountGuidId;
        IF (String.isEmpty(selectedAccountGuidId)) 
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You cannot lookup a opportunity, until you select an account'));
    return null;
    }
    Else
    {
    OppStageFilter = OppStageFilter;
    selectedOppName = selectedOppName;
    system.debug('Account Guid For Yale is: ' + selectedAccountGuidId);
    oppsearchquery = 'select Name, StageName, Type, id, AccountID, Opportunity.Account.Name, Amount ' +
                   'from opportunity where AccountId = :selectedAccountGuidId and name like \'%' + String.escapeSingleQuotes(selectedOppName ) + '%\' order by Name ASC LIMIT 100';
       
    //Dynamically Append additional conditions to the base searchquery if fields are populated
    if (!String.isEmpty(OppStageFilter))
        oppsearchquery += ' and StageName = :OppStageFilter';
       
    opp = Database.query(oppsearchquery); 
    OppSize = opp.size();  
    IF (OppSize == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }                          
    PageReference pageRef= new PageReference('/apex/Activities_OpportunityLookup');
    pageRef.setredirect(false);  
    return pageRef;   
    }
    }    
  
   //Search Opportunities No Redir - Allows user to search again from Opp Popup without being redirected
   public void searchOppNoRedir(){  
   selectedAccountGuidId = selectedAccountGuidId;
   OppStageFilter = OppStageFilter;
   selectedOppName = selectedOppName;
   oppsearchquery = 'select Name, StageName, Type, id, AccountID, Opportunity.Account.Name, Amount ' +
                   'from opportunity where AccountId = :selectedAccountGuidId and name like \'%' + String.escapeSingleQuotes(selectedOppName ) + '%\'';
       
   //Dynamically Append additional conditions to the base searchquery if fields are populated
   if (!String.isEmpty(OppStageFilter))
   {
        oppsearchquery += ' and StageName = :OppStageFilter order by Name ASC LIMIT 100';
   }
   else 
   {
       oppsearchquery += ' order by Name ASC LIMIT 100';
   }
   
   opp = Database.query(oppsearchquery); 
   OppSize = opp.size();
   IF (OppSize == 100) {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Your Search Exceeded 100 Records. (Only The first 100 records are shown). Please Limit your Search '));
    }    
   }
    

   //Back to Main Form 
   public PageReference ReviseSearch(){  
     PageReference pageRef= new PageReference('/apex/Activities_Mobile');
     pageRef.setredirect(false);       
     return pageRef; 
     }   
     
   //Used by Javascript to refresh PageMessages
   public PageReference Refresh(){   
     return null; 
     }   
     
   
   //Capture Contact Index Number Selection  
   public PageReference processButtonClick() {
   LockAccountSelection = true;
   selectedContactId = acc[IndexNum].Id; //This is actually Contact Guid 
   selectedContactName = acc[IndexNum].Name; //This is actually Contact Guid
   PageReference pageRef= new PageReference('/apex/Activities_Mobile');
   pageRef.setredirect(false);  
   return pageRef; 
   } 
    
   //Capture Opportunity Index Number Selection  
   public PageReference processOppSelection() {
   selectedOppId = opp[OppIndexNum].Id; 
   selectedOppName = opp[OppIndexNum].Name; 
   PageReference pageRef= new PageReference('/apex/Activities_Mobile');
   pageRef.setredirect(false);  
   return pageRef; 
   } 

   //Return Validation Error
   public PageReference ValidationMsgReturn() 
   {
   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please fill out all required (*) fields'));
   return null;
   }


   //Create Event
   public PageReference CreateEvent() {
   
   System.debug('Activity: ' + ActivityType + ' Contact: ' + selectedContactId + ' Account: ' + selectedAccountGuidId + ' Country: ' + UserCountry);
   
   StartDate = datetime.newinstance(currStartDate,currStartTime);
   EndDate = datetime.newinstance(currEndDate,currEndTime);
   Event e = new Event();
   e.RecordTypeId = ReccType;
   e.Activity_Type__c = ActivityType;
   e.Created_via_Salesforce1__c = true;
   e.ActivityDateTime = StartDate;
   e.StartDateTime = StartDate;
   e.EndDateTime = EndDate;
   e.WhoId = selectedContactId;
      
   //WhatID cannot be passed if blank/null, but is not required
      IF (String.isNotEmpty(selectedOppId)) 
      e.WhatId = selectedOppId;  
      
   IF(UserCountry== 'KR') {  
   e.Asia_Product_BU__c = selectedProductBU;
   e.Asia_Product__c = selectedProduct;
   }
   e.Description = selectedNotes;
      try{ 
         insert e; 
         NewEvent = e.Id;
         System.debug('Event was sucessfully Created: ' + NewEvent);
         } 
         catch (Exception ee) 
         {}
         return null;
         
     }
     
   }