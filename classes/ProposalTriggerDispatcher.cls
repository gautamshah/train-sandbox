/****************************************************************************************
* Name    : Class: ProposalTriggerDispatcher
* Author  : Paul Berglund
* Date    : 12/08/2014
* Purpose : Dispatches to necessary classes when invoked by Proposal trigger
* 
* Dependancies: 
*   Class: Proposal_Main
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 10/15/2015    Bryan Fry               Added method to capture Cycle Times
* 01/20/2016    Paul Berglund           Commented out SSG calls so we can promote to production
*                                       PopulateAnalyst
*                                       captureProposalCycleTimes
* 01/22/2016    Paul Berglund           Uncommented after promoting code to RlsStg
* 03/18/2016    Paul Berglund           Uncommented Cycle Time call
06/14/2016   Paul Berglund      Moved when to execute Cycle Time logic into class
10/31/2016  Paul Berglund   Remediation with CPQ - logice moved to cpqProposal_t
*****************************************************************************************/
public with sharing class ProposalTriggerDispatcher
{
/*  
    //to prevent a method from executing a second time add the name of the method to the 'executedMethods' set
    public static Set<String> executedMethods = new Set<String>();
  
    public static void Main(List<Apttus_Proposal__Proposal__c> newList, Map<Id, Apttus_Proposal__Proposal__c> newMap, List<Apttus_Proposal__Proposal__c> oldList, Map<Id, Apttus_Proposal__Proposal__c> oldMap, Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isExecuting)
    {
        if (isInsert || isUpdate) {
            if (isBefore && !executedMethods.contains('VerifyOwnerAndAssignApprovers'))
                Proposal_Main.VerifyOwnerAndAssignApprovers(newList, newMap, oldMap, isInsert);

            if(isBefore && !executedMethods.contains('PopulateTerritoryManager'))
                Proposal_Main.PopulateTerritoryManager(newList);
          
            if(isBefore && !executedMethods.contains('PopulateAnalyst')) {
                Proposal_Main.PopulateAnalyst(oldMap,newList);
            }
        }
        
        if(!executedMethods.contains('capture'))
        {
			CPQ_CycleTime_Processes.capture(
				newList,
				newMap,
				oldList,
				oldMap,
				isBefore,
				isAfter,
				isInsert,
				isUpdate,
				isDelete,
				isExecuting);        	
        }
    }
*/
}