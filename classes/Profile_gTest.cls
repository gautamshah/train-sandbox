@isTest
public class Profile_gTest {
    
    static Profile_g unit = new Profile_g();
  
  	static Map<Id,Profile> smallSet()
  	{
  		Map<Id,Profile> pES = new Map<Id, Profile>([SELECT Id, Name FROM Profile LIMIT 5]);
  		unit.put(pES.values());
  		return pES;
  	}
    
    static
    {
        system.debug('Profile_gTest');
    }
  
  	 @isTest
    static void testConstructor()
    {
        system.assertNotEquals(null, unit);
        
    }
  
    @isTest
    static void testCastMap()
    {
        Map<Id, sObject> input;
      Map<object, Map<Id, sObject>> source = new Map<object, Map<Id, sObject>>();
        
        Map<Id, Profile> result = Profile_g.cast(input);
        system.assert(result != null && result.isEmpty());
        
        input = new Map<Id, sObject>();
        result = Profile_g.cast(input);
        system.assert(result != null && result.isEmpty());
        
        Profile ut = [SELECT Id, Name FROM Profile LIMIT 1];
        
        input.put(ut.Id, null);
        result = Profile_g.cast(input);
        system.assert(result != null && result.isEmpty());
        
        input.put(ut.Id, ut);
        result = Profile_g.cast(input);
        system.assert(result != null &&
                      !result.isEmpty() &&
                      result.containsKey(ut.Id) &&
                      result.get(ut.Id) == ut);
    }
  
    @isTest
  	static void testCastDoubleMap()
  	{
  		Map<object, Map<Id, sObject>> source = new Map<object, Map<Id, sObject>>();
  		Map<object, Map<Id, Profile>> result = Profile_g.cast(source);
  		System.assert(result != null );
  	}

    
    @isTest 
    static void fetchByIdSet()
    {
        Map<Id,Profile> pEL; // Users expected (large)
        Map<Id,Profile> pA; // Users actual

        Map<Id,Profile> pES = smallSet();
      
        pEL = new Map<Id, Profile>([SELECT Id, Name FROM Profile LIMIT 10]);

        Test.startTest();

            // Start with no queries
            System.assertEquals(0, Limits.getQueries());

            System.debug('Fetch a set of Profiles');
            pA = unit.fetch();
            system.assertEquals(5, pA.size());
            System.assertEquals(5, unit.fetch().size());
            System.assertEquals(pES.size(), pA.size(),'Profiles not returned');
            System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

            System.debug('Fetch the same Profiles');
            pA = unit.fetch();
            System.assertEquals(unit.fetch().size(), 5);
            System.assertEquals(pES.size(), pA.size(),'Profiles not returned');
            System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

            System.debug('Fetch additional Profiles');
            unit.put(pEL.values());
            pA = unit.fetch();
            System.assertEquals(unit.fetch().size(), 10);
            System.assertEquals(pEL.size(), pA.keySet().size(),'Profiles not returned');
            System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

        Test.stopTest();

    }

    @isTest 
    static void fetchByIdList() {

        system.debug(CONSTANTS.NBSP(10) + 'fetchByIdList'); 

        Map<Id,Profile> pA;
        Map<Id,Profile> pE = smallSet();
        Test.startTest();

            pA = unit.fetch();

            System.assertEquals(5, pA.size());
            system.assertEquals(5, unit.fetch().size());
            System.assertEquals(pE.size(), pA.size(),'Users not returned');
            System.assertEquals(0, Limits.getQueries(),'Should not use any queries inside test');

        Test.stopTest();

    }
    
}