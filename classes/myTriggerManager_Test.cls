@isTest
public class myTriggerManager_Test {

    @testSetup
    public static void SetupMethod() {
        myTriggerManager__c tmgr = new myTriggerManager__c();
        tmgr.Name = 'Test';
        insert tmgr;
    }
    
    @isTest
    public static void ObjectExists() {
        myTriggerManager tmgr = new myTriggerManager('DNE');
        system.assertNotEquals(tmgr, null, 'The myTriggerManager object will always exist, even if there is no record for the named trigger');
        
        tmgr = new myTriggerManager('Test');
        system.assertNotEquals(tmgr, null, 'The myTriggerManager will be created if the named trigger exists');
    }
    
    @isTest
    public static void TriggerIsActive() {
        myTriggerManager tmgr = new myTriggerManager('Test');
        
        system.assertEquals(tmgr.isActive, true, 'Existing trigger return true');
        
        myTriggerManager__c obj = [SELECT Id FROM myTriggerManager__c WHERE Name = 'Test'];
        obj.isActive__c = false;
        upsert obj;
        myTriggerManager.myTriggerManagerMap = null;
        
        tmgr = new myTriggerManager('Test');
        system.assertEquals(tmgr.isActive, false, 'Existing trigger returns false');
        
        tmgr = new myTriggerManager('DNE');
        
        system.assertEquals(tmgr.isActive, true, 'Non-existing trigger returns true');
    }
    
    @isTest
    public static void MethodIsActive() {
        myTriggerManager tmgr = new myTriggerManager('DNE');
        system.assertEquals(tmgr.MethodIsEnabled('Test'), true, 'Non-existing record will return true');

        tmgr = new myTriggerManager('Test');
        
        system.assertEquals(tmgr.MethodIsEnabled(null), true, 'Null method name returns true');
        
        system.assertEquals(tmgr.MethodIsEnabled('Test'), true, 'Non-existent method will return true');
        
        myTriggerManager__c obj = [SELECT Id FROM myTriggerManager__c WHERE Name = 'Test'];
        
        obj.Method_Is_Disabled__c = 'Test';
        upsert obj;
        myTriggerManager.myTriggerManagerMap = null;

        tmgr = new myTriggerManager('Test');
        
        system.assertEquals(tmgr.MethodIsEnabled('Test'), false, 'Existing method will return false');
    }
    
    @isTest
    public static void MethodDebugIsActive() {
        myTriggerManager tmgr = new myTriggerManager('DNE');
        system.assertEquals(tmgr.MethodDebugIsEnabled('Test'), true, 'Non-existing record will return true');

        tmgr = new myTriggerManager('Test');
        
        system.assertEquals(tmgr.MethodDebugIsEnabled(null), true, 'Null method name returns true');
        
        system.assertEquals(tmgr.MethodDebugIsEnabled('Test'), true, 'Non-existent method will return true');
        
        myTriggerManager__c obj = [SELECT Id FROM myTriggerManager__c WHERE Name = 'Test'];
        
        obj.Method_Debug_Is_Disabled__c = 'Test';
        upsert obj;
        myTriggerManager.myTriggerManagerMap = null;
        
        tmgr = new myTriggerManager('Test');
        
        system.assertEquals(tmgr.MethodDebugIsEnabled('Test'), false, 'Existing method will return false');
    }
    
    @isTest
    public static void Debug1() {
        myTriggerManager tmgr = new myTriggerManager('DNE');
        tmgr.Debug('Test', system.LoggingLevel.DEBUG, 'Test method');
    }
    
    @isTest
    public static void Debug2() {
        myTriggerManager tmgr = new myTriggerManager('DNE');
        tmgr.Debug('Test', 'Test method');
    }
    
    @isTest
    public static void TriggerDebug() {
        myTriggerManager.TriggerDebug('Test', true, true, false, false, new List<sObject>{ new Task() }, new List<sObject>{ new Task() });
        myTriggerManager.TriggerDebug('Test', false, true, false, false, new List<sObject>{ new Task() }, new List<sObject>{ new Task() });
        myTriggerManager.TriggerDebug('Test', true, false, true, false, new List<sObject>{ new Task() }, new List<sObject>{ new Task() });
        myTriggerManager.TriggerDebug('Test', true, false, false, true, new List<sObject>{ new Task() }, new List<sObject>{ new Task() });
    }
}