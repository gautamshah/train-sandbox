/****************************************************************************************
* Name    : Class: TaskConfigSetting_Test
* Author  : Paul Berglund
* Date    : 03/08/2016
* Purpose : Test class for TaskConfigSetting
*
* Dependancies:
*   Class:  TaskConfigSetting
*
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 03/08/2016    Paul Berglund           Created to accomodate CPQ logic moved into
*                                       TaskConfigSetting
* 04/28/2016    Paul Berglund           Removed the code that set the language and any
*                                       related setup/testing associatd with language.
*                                       Class was changed to address the Profile name
*                                       issues associated with System Administrator changing
*                                       depending on the language of the user.
*****************************************************************************************/
@isTest (SeeAllData=false)
private class TaskConfigSetting_Test
{
    private static final string CRLF = '\r\n';

	private static final List<string> validProfiles
		= new List<string>
			{
				'APTTUS - RMS US Sales (RS+PM)',
			    'APTTUS - SSG US Account Executive'
			};

	private static User ValidUser()
	{
		return [SELECT Id,
		               LanguageLocaleKey
		        FROM User
		        WHERE IsActive = true AND
		              LanguageLocaleKey = 'en_US' AND
		              Profile.Name IN :validProfiles LIMIT 1];
	}

	private static User InvalidUser()
	{
		return [SELECT Id,
		               LanguageLocaleKey
		        FROM User
		        WHERE IsActive = true AND
		              LanguageLocaleKey != 'en_US' AND
		              Profile.Name NOT IN :validProfiles LIMIT 1];
	}

	@isTest
	static void CPQ()
	{
	    boolean exceptionThrown = false;
		TaskConfigSetting.throwException = true;

		system.runAs(InvalidUser())
		{
			TaskConfigSetting.CPQ obj = TaskConfigSetting.CPQ;

			system.assertEquals(null, obj, 'Should return null if user is not in valid profile');
		}

		TaskConfigSetting.Reset();

		system.runAs(ValidUser())
		{
			TaskConfigSetting.CPQ obj = TaskConfigSetting.CPQ;

			system.assertNotEquals(null, obj, 'Should return a valid object if user is in valid profile');
		}
	}

	@isTest
	static void Settings_ProfileIds()
	{
		TaskConfigSetting.throwException = true;

        boolean exceptionThrown = false;

		system.runAs(ValidUser())
		{
			TaskConfigSetting.CPQ obj = TaskConfigSetting.CPQ;
			System.debug('TaskConfigSetting.CPQ obj: ' + obj);

			integer size = obj.Settings.ProfileIds.size();

			system.assertEquals(4, size, 'A valid, English speaking user, will return 4');
		}

		// I'm assuming that the user running the tests would be in a
		// Profile with System Administrator privileges

		TaskConfigSetting.Reset();

		TaskConfigSetting.CPQ obj = TaskConfigSetting.CPQ;

		integer size = obj.Settings.ProfileIds.size();

		system.assertEquals(4, size, 'A user with System Administrator privileges, ' +
		                             'with English language, will return 4');
/*
		TaskConfigSetting.Reset();
		
		User u = [SELECT Id,
		                 LanguageLocaleKey
		          FROM User
		          WHERE Id = :UserInfo.getUserId()];

		u.LanguageLocaleKey = 'fr';
		u.LocaleSidKey = '	fr_FR';
		update u;
		
		system.runAs(u)
		{
			obj = TaskConfigSetting.CPQ;
		
			size = obj.Settings.ProfileIds.size();
		
			system.assertEquals(3, size, 'A user with System Administrator privileges, ' +
				                             'but non-English language, will return 3');
		}
*/		
		TaskConfigSetting.Reset();
		TaskConfigSetting.Clear();

		TaskConfigSetting__c t = new TaskConfigSetting__c();
		t.Name = TaskConfigSetting.DEFAULT_CONFIG_RECORD_NAME;
		t.CPQ_Profile_s__c = 'DNE';
		insert t;

        exceptionThrown = false;

		obj = TaskConfigSetting.CPQ;

		system.assertEquals(null, obj, 'Should be ignored');
	}

	@isTest
	static void Settings_OrgWideEmailAddressId()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null) {

		  		TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.Settings.OrgWideEmailAddressId;
				}
				catch (Exception ex) {
					string expected = 'No \'Org Wide Email Address - Display Name\' set ' +
					                      'in the \'CPQ - Fields Used Globally\' section of TaskConfigSetting';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Org_Wide_Address_Display_Name__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.Settings.OrgWideEmailAddressId;
				}
				catch (Exception ex) {
					string expected = 'The \'Org Wide Email Address - Display Name\' set ' +
					                  'in the \'CPQ - Fields Used Globally\' section of ' +
					                  'TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Org_Wide_Address_Display_Name__c = 'Medtronic - CPQ Task';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.Settings.OrgWideEmailAddressId,
									   'Should not be null');
			}
		}
	}

	@isTest
	static void NotifyOwnerWhenCDDone_TemplateId()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null)
			{
		 		TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.TemplateId;
				}
				catch (Exception ex) {
					string expected = 'No \'Template\' set in the \'NotifyOwnerWhenCDDone\' ' +
					                  'section of TaskConfigSetting';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_Owner_when_CD_done_Template__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.TemplateId;
				}
				catch (Exception ex) {
					string expected = 'The \'Template\' set in the \'NotifyOwnerWhenCDDone\' ' +
					                  'section of TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_Owner_when_CD_done_Template__c = 'Notify Owner when CD done';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.TemplateId,
									   'Should not be null');
			}
		}
	}

	@isTest
	static void NotifyOwnerWhenCDDone_RecordTypeIds()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null) {

				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.RecordTypeIds;
				}
				catch (Exception ex) {
					string expected = 'At least one of the \'Record Type Name(s)\' set in the ' +
									  '\'NotifyOwnerWhenCDDone\' section of ' +
									  'TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_Owner_when_CD_done_Rcrd_Type_s__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.RecordTypeIds;
				}
				catch (Exception ex) {
					string expected = 'At least one of the \'Record Type Name(s)\' set in the ' +
									  '\'NotifyOwnerWhenCDDone\' section of ' +
									  'TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_Owner_when_CD_done_Rcrd_Type_s__c
					= '-All' + CRLF +
					  '+Outright Quote (Hardware)-AG' + CRLF +
	                  '+Outright Quote (Hardware)-AG Locked';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.RecordTypeIds,
									   'Should not be null');

			}
		}
	}

	@isTest
	static void NotifyOwnerWhenCDDone_Subjects()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null) {

				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

				system.assertEquals(0,
									TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.Subjects.size(),
									'Should return an empty set');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_Owner_when_CD_done_Subject_s__c = 'DNE';
				update target;

				system.assertEquals(new Set<string> { 'DNE' },
									    TaskConfigSetting.CPQ.NotifyOwnerWhenCDDone.Subjects,
									    'Should be equal');
			}
		}
	}

	@isTest
	static void NotifyCSWhenAgreementActivated_AssignedToId()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null) {
				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.AssignedToId;
				}
				catch (Exception ex) {
					string expected = 'No \'Assigned To\' set in the \'NotifyCSWhenAgreementActivated\' ' +
					                  'section of TaskConfigSetting';
					string actual = ex.getMessage();
					system.assertEquals(expected,
		        			            actual,
		                			    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_CS_when_Agrmnt_Actvtd_Assigned__c = 'DNE';
				update target;

        		exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.AssignedToId;
				}
				catch (Exception ex) {
					string expected = 'The \'Assigned To\' set in the \'NotifyCSWhenAgreementActivated\' section ' +
		    		                  'of TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
			        		            actual,
			                		    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_CS_when_Agrmnt_Actvtd_Assigned__c = 'NASSC - Customer Service Queue';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.AssignedToId,
									   'Should not be null');
			}
		}
	}

	@isTest
	static void NotifyCSWhenAgreementActivated_ToAddresses()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null)
			{
				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

				system.assertEquals(0,
									TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.ToAddresses.size(),
									'Empty field should return 0');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_CS_when_Agrmnt_Actvtd_To_s__c = 'one@test.com' + CRLF + 'two@test.com';
				update target;

				system.assertEquals(2,
									TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.ToAddresses.size(),
									'Should be two');
			}
		}
	}

	@isTest
	static void NotifyCSWhenAgreementActivated_TemplateId()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null)
			{
				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.TemplateId;
				}
				catch (Exception ex) {
					string expected = 'No \'Template\' set in the \'NotifyCSWhenAgreementActivated\' ' +
					                  'section of TaskConfigSetting';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_CS_when_Agrmnt_Actvtd_Template__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.TemplateId;
				}
				catch (Exception ex) {
					string expected = 'The \'Template\' set in the \'NotifyCSWhenAgreementActivated\' ' +
									  'section of TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_CS_when_Agrmnt_Actvtd_Template__c
					= 'Customer Service Notification Email Outright Quote VF';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.TemplateId,
									   'Should not be null');
			}
		}
	}

	@isTest
	static void NotifyCSWhenAgreementActivated_RecordTypeIds()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null)
			{
				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        integer count = [SELECT COUNT()
		        				 FROM RecordType
		        				 WHERE sObjectType = 'Apttus__APTS_Agreement__c'];

				system.assertEquals(count,
									TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.RecordTypeIds.size(),
									'Should return the same number');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_CS_when_Agrmnt_Actvtd_Rcrd_Type__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.RecordTypeIds;
				}
				catch (Exception ex) {
					string expected = 'At least one of the \'Record Type Name(s)\' set in the ' +
									  '\'NotifyCSWhenAgreementActivated\' section ' +
									  'of TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_CS_when_Agrmnt_Actvtd_Rcrd_Type__c
					= '-All' + CRLF +
					  '+Outright Quote (Hardware)-AG' + CRLF +
				      '+Outright Quote (Hardware)-AG Locked';
				update target;

				system.assertEquals(2,
									TaskConfigSetting.CPQ.NotifyCSWhenAgreementActivated.RecordTypeIds.size(),
									'Should return only 2 records');
			}
		}
	}

	@isTest
	static void NotifyOwnerWhenAgreementImplemented_TemplateId()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null)
			{
				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.TemplateId;
				}
				catch (Exception ex) {
					string expected = 'No \'Template\' set in the \'NotifyOwnerWhenAgreementImplemented\' ' +
									  'section of TaskConfigSetting';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_Owner_Ag_Implemented_Template__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.TemplateId;
				}
				catch (Exception ex) {
					string expected = 'The \'Template\' set in the \'NotifyOwnerWhenAgreementImplemented\' section ' +
					                  'of TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_Owner_Ag_Implemented_Template__c = 'Notify Owner Agreement Implemented';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.TemplateId,
									   'Should not be null');
			}
		}
	}

	@isTest
	static void NotifyOwnerWhenAgreementImplemented_RecordTypeIds()
	{
		system.runas(ValidUser())
		{
			if (TaskConfigSetting.CPQ != null)
			{
				TaskConfigSetting.throwException = true;

		        boolean exceptionThrown = null;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.RecordTypeIds;
				}
				catch (Exception ex) {
					string expected = 'At least one of the \'Record Type Name(s)\' set in the ' +
									  '\'NotifyOwnerWhenAgreementImplemented\' section of ' +
									  'TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				TaskConfigSetting.Reset();
				TaskConfigSetting__c target = [SELECT Id
											   FROM TaskConfigSetting__c
											   WHERE Name = 'System Properties' LIMIT 1];
				target.Notify_Owner_Ag_Implemented_Rcrd_Type__c = 'DNE';
				update target;

		        exceptionThrown = false;
				try {
					object obj = TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.RecordTypeIds;
				}
				catch (Exception ex) {
					string expected = 'At least one of the \'Record Type Name(s)\' set in the ' +
									  '\'NotifyOwnerWhenAgreementImplemented\' section of ' +
									  'TaskConfigSetting does not exist: DNE';
					string actual = ex.getMessage();
					system.assertEquals(expected,
					                    actual,
					                    'Should throw exception with this message');
					exceptionThrown = true;
				}

				system.assert(exceptionThrown, 'Exception should have been thrown');

				TaskConfigSetting.Reset();
				target.Notify_Owner_Ag_Implemented_Rcrd_Type__c
					= '-All' + CRLF +
					  '+Outright Quote (Hardware)-AG' + CRLF +
					  '+Outright Quote (Hardware)-AG Locked';
				update target;

				system.assertNotEquals(null,
									   TaskConfigSetting.CPQ.NotifyOwnerWhenAgreementImplemented.RecordTypeIds,
									   'Should not be null');
			}
		}
	}
}