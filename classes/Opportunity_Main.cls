/****************************************************************************************
* Name    : Class: Opportunity_Main
* Author  : Gautam Shah
* Date    : 4/5/2014
* Purpose : A wrapper for static functionality for Opportunity records
* 
* Dependancies: 
*   Called by: OpportunityTriggerDispatcher, BatchOppRevenueScheduleUpdate
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE              AUTHOR                  CHANGE
* ----              ------                  ------
* 6/27/14           Gautam Shah             Created new methods to replace separate triggers("ProductReqForUSMedSupplies", "OpportunityOwnerRegionTrigger")
* 4/23/15           Paul Berglund           Added AssignTerritory
* 6/29/15           Amogh Ghodke            Updated AssignTerritory method from Line no 262 - line no.314
* 15/10/15          Bill Shan               Rewrite AssignTerritory method
* 21-Oct-2015       Amogh Ghodke            Updated Line no.33 for updated names of Canada Profile 
* Apr 7, 2016       Gautam Shah             Updated Line 95 and added Line 110 to include DefaultCurrencyIsoCode as values to copy from User Record
* June 14, 2016     Gautam Shah             Modified "AssignTerritory" method to exclude processing when current user is a Lead Qualification Specialist (line 271)
* August 24,2016    Amogh Ghodke            Added PopulateOCREmail method
20161111	A	PAB		N/A		Removed ProductReqForUSMedSupplies for per David M
*****************************************************************************************/
public with sharing class Opportunity_Main
{
    public static List<Opportunity> UpdateOpportunityGBU_Franchise(List<Opportunity> oppList)
    {
        set<Id> userIds = new set<Id>();
        //get the list of user Ids for the owner column
        for(Opportunity o : oppList)
        {
            userIds.add(o.OwnerId);
        }
        //create a map of owner to user for retrieving the GBU and Franchise Ids
        Map<id,User> userMap  = new Map<id,User>([select Id, Franchise__c, Business_Unit__c from User where Id in :userIds]);       
        Set<String> excludedProfiles = new Set<String>{'System Administrator', 'API Data Loader', 'CA DQ Analyst', 'Canada - All', 'Canada - Early Technologies', 'Canada - Surgical Innovations', 'Canada - Patient Monitoring And Recovery','Canada - Inside Sales'};
        for(Opportunity o : oppList)
        {
            //update GBU and Franchise to the Owner's values
            o.Franchise__c = userMap.get(o.OwnerId).Franchise__c;
            if(!excludedProfiles.contains(Utilities.CurrentUserProfileName))
            {
                o.Business_Unit__c = userMap.get(o.OwnerId).Business_Unit__c;
            }
        }
        OpportunityTriggerDispatcher.executedMethods.add('UpdateOpportunityGBU_Franchise');//to prevent this method from executing more than once.
        return oppList;
    }
    
    public static List<Opportunity> PopulateOCREmail(List<Opportunity> oppList)
    {
         for(Opportunity opp : oppList)
             {
              List<Contact> contactList = [ SELECT Id,Email FROM Contact WHERE Id IN ( SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId = :opp.Id) ];
              if(contactList !=NULL && !contactList.isEmpty()){
                 for( Contact conVar : contactList ){
                      if(conVar.Email !=null){
                         if(opp.OCR_Email__c != null && !(opp.OCR_Email__c.contains(conVar.Email)))
                         {
                             opp.OCR_Email__c = opp.OCR_Email__c +',' +conVar.Email;
                             opp.OCR_Email__c.remove('null');  
                         }
                         else{
                             if(contactList.size()==1 || contactList.size()>1)
                                {opp.OCR_Email__c = conVar.Email;    
                                 opp.OCR_Email__c.remove('null');
                                }
                                
                         }
                      }
                      
                   }
                         
              }
             else if(contactList == NULL || contactList.isEmpty()){
                     opp.OCR_Email__c ='';
              }
            }
    
        OpportunityTriggerDispatcher.executedMethods.add('PopulateOCREmail');//to prevent this method from executing more than once.
        return oppList;
    
    }
     
    // 20161111-A   
    ////public static List<Opportunity> ProductReqForUSMedSupplies(List<Opportunity> oppList)
    ////{
    ////    for(Opportunity opp : oppList)
    ////    {
    ////        system.debug('-- Bypass value --> '+ opp.By_Pass_ProductRequiredForUSMedSuppVali__c);
    ////        
    ////        if(opp.By_Pass_ProductRequiredForUSMedSuppVali__c == false)
    ////        {
    ////            //system.debug('-- current user --> '+ currUser);
    ////            if(
    ////                (
    ////                opp.StageName == 'Develop'  ||
    ////                opp.StageName == 'Evaluate' ||
    ////                opp.StageName == 'Propose'  ||
    ////                opp.StageName == 'Negotiate'    ||
    ////                opp.StageName == 'Closed Won'   ||
    ////                opp.StageName == 'Closed Lost'  ||
    ////                opp.StageName == 'Closed (Cancelled)' ||
    ////                opp.StageName == 'Closed (Resolved)'
    ////                ) 
    ////                && (opp.of_Products__c < 1)
    ////            )
    ////            {
    ////                system.debug('-- opp Stage --> '+ opp.StageName);
    ////                opp.addError('Please add a Product to this Opportunity before moving past the Identify Stage.');
    ////            }
    ////        }
    ////        else
    ////        {
    ////            opp.By_Pass_ProductRequiredForUSMedSuppVali__c = false;
    ////            system.debug('-- uncheking the by pass value --> '+ opp.StageName);
    ////        }
    ////    }
    ////    OpportunityTriggerDispatcher.executedMethods.add('ProductReqForUSMedSupplies');//to prevent this method from executing more than once.
    ////    return oppList;
    ////}
    
    public static List<Opportunity> OwnerRegionAssignment(List<Opportunity> oppList)
    {
        Set<id> oppOwnerIds = new Set<id>();
        Map<Id, User> userMap = new Map<Id, User>();
        
        for (Opportunity opportunity : oppList)
        {
            oppOwnerIds.add(opportunity.Ownerid);
        }
        
        for(User user : [SELECT Id, Name, Function__c, Region__c, User_Role__c, DefaultCurrencyIsoCode FROM User WHERE UserType = 'Standard' AND Id IN :oppOwnerIds])
        {      
            userMap.put(user.Id, user);
        }
        
        if(!userMap.IsEmpty())
        {
            for (Opportunity opp : oppList)
            {
                if(userMap.keySet().contains(opp.OwnerId))
                {
                    opp.Owner_Region__c = userMap.get(opp.OwnerId).Region__c;
                    opp.Region__c = userMap.get(opp.OwnerId).Region__c;
                    opp.User_Role__c = userMap.get(opp.OwnerId).User_Role__c; 
                    opp.Function__c = userMap.get(opp.OwnerId).Function__c;
                    if(String.isBlank(opp.CurrencyIsoCode))
                    {
                    	opp.CurrencyIsoCode = userMap.get(opp.OwnerId).DefaultCurrencyIsoCode;
                    }
                }
            }
        }
        OpportunityTriggerDispatcher.executedMethods.add('OwnerRegionAssignment');//to prevent this method from executing more than once.
        return oppList;
    }
    
    public static List<OpportunityLineItemSchedule> tempOLIS = new List<OpportunityLineItemSchedule>();
    public static List<OpportunityLineItem> origOliList = new List<OpportunityLineItem>();
    public static void reEstablishSchedule(List<Opportunity> oppList)
    {
        //List<Id> oppIDs = new List<Id>();
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        for(Opportunity o : oppList)
        {
            //oppIDs.add(o.Id);
            oppMap.put(o.Id, o);
        }
        origOliList = [Select Id, OpportunityId, Quantity, UnitPrice, TotalPrice, CurrencyIsoCode From OpportunityLineItem Where OpportunityId In :oppMap.keySet() Limit 50000];
        Savepoint sp1 = Database.setSavepoint();
        if(deleteOldSchedule(oppMap.keySet()))
        {
            try
            {
                //calculate the new schedules
                List<OpportunityLineItemSchedule> newSched = createNewSchedule(oppMap);
                System.debug('# of new OppLineItemSchedule records: ' + newSched.size());
                //insert
                if(newSched.size() < 10001)
                {
                    insert newSched;
                }
                else
                {
                    //insert in chunks to be within limits
                    tempOLIS.clear();
                    for(Integer i = 0; newSched.size() > 0; i++)
                    {
                        tempOLIS.set(i, newSched.get(i));
                        newSched.remove(i);
                        if(tempOLIS.size() == 10000)
                        {
                            System.debug('sending chunk of 10000 to futureInsert');
                            futureInsert();
                        }
                    }
                    //in case there are any remaining
                    if(tempOLIS.size() > 0)
                    {
                        System.debug('sending remainder to futureInsert');
                        futureInsert();
                    }
                }
                OpportunityTriggerDispatcher.executedMethods.add('reEstablishSchedule');//to prevent this method from executing more than once.
            }
            catch(Exception e)
            {
                System.debug('ERROR: ' + e.getMessage());
            }
        }
        else
        {
            Database.rollback(sp1); 
        }
    }
    
    private static Boolean deleteOldSchedule(Set<Id> oppIDs)
    {
        try
        {
            /*
            OppLineItemSched_Delete_Batch olisDEL = new OppLineItemSched_Delete_Batch(oppIDs);
            ID batchProcessId = Database.executeBatch(olisDEL);
            System.debug('Deleting old schedule via batch, ID: ' + batchProcessId);
            */
            List<OpportunityLineItemSchedule> olisList = new List<OpportunityLineItemSchedule>([Select Id From OpportunityLineItemSchedule Where OpportunityLineItem.OpportunityId In :oppIDs]);
            delete olisList;
            return true;
        }
        catch(Exception e)
        {
            System.debug('Deleting old schedule via batch, ERROR: ' + e.getMessage());
            return false;
        }
    }
    
    private static List<OpportunityLineItemSchedule> createNewSchedule(Map<Id, Opportunity> oppMap)
    {
        System.debug('createNewSchedule, oppMap size: ' + oppMap.size());
        List<OpportunityLineItemSchedule> totalSchedList = new List<OpportunityLineItemSchedule>();
        //List<OpportunityLineItemSchedule> oSchedList = new List<OpportunityLineItemSchedule>();
        //Map<Id, List<OpportunityLineItemSchedule>> itemSchedMap = new Map<Id, List<OpportunityLineItemSchedule>>();
        //List<OpportunityLineItem> oliList = new List<OpportunityLineItem>([Select Id, OpportunityId, Quantity, UnitPrice, TotalPrice, CurrencyIsoCode From OpportunityLineItem Where OpportunityId In :oppMap.keySet() Limit 50000]);
        //System.debug('createNewSchedule, oliList size: ' + oliList.size());
        
        for(OpportunityLineItem oli : origOliList)
        {
        //Quantity
            System.debug('oli quantity: ' + oli.Quantity);
            Decimal quant = 0;
            Decimal rev = 0;
            Date schedMonth = oppMap.get(oli.OpportunityId).CloseDate;
            System.debug('createNewSchedule, schedMonth: ' + schedMonth);
            //oSchedList.clear();
            for(Integer i = 0; i < 11; i++)//11 months because the 12th month will hold remainders
            {
                OpportunityLineItemSchedule os = new OpportunityLineItemSchedule();
                os.OpportunityLineItemId = oli.Id;
                os.ScheduleDate = schedMonth.addMonths(i);
                os.Type = 'Both';
                quant += Math.floor(oli.Quantity/12);
                System.debug('quant: ' + quant);
                os.Quantity = Math.floor(oli.Quantity/12);
                os.Revenue = 0;
                //oSchedList.add(os);
                totalSchedList.add(os);
            }
            System.debug('11 months quantity total: ' + quant);
            //12th month
            OpportunityLineItemSchedule os_quant12 = new OpportunityLineItemSchedule();
            os_quant12.OpportunityLineItemId = oli.Id;
            os_quant12.ScheduleDate = schedMonth.addMonths(11);
            os_quant12.Type = 'Both';
            os_quant12.Quantity = oli.Quantity - quant;
            os_quant12.Revenue = 0;
            //oSchedList.add(os);
            totalSchedList.add(os_quant12);
        //Revenue
            for(Integer i = 0; i < 11; i++)//11 months because the 12th month will hold remainders
            {
                OpportunityLineItemSchedule os = new OpportunityLineItemSchedule();
                os.OpportunityLineItemId = oli.Id;
                os.ScheduleDate = schedMonth.addMonths(i);
                os.Type = 'Both';
                rev += Math.floor(oli.Quantity/12) * oli.UnitPrice;
                System.debug('rev: ' + rev);
                os.Revenue = Math.floor(oli.Quantity/12) * oli.UnitPrice;
                os.Quantity = 0;
                //oSchedList.add(os);
                totalSchedList.add(os);
            }
            System.debug('11 months revenue total: ' + rev);
            //12th month
            OpportunityLineItemSchedule os_rev12 = new OpportunityLineItemSchedule();
            os_rev12.OpportunityLineItemId = oli.Id;
            os_rev12.ScheduleDate = schedMonth.addMonths(11);
            os_rev12.Type = 'Both';
            os_rev12.Revenue = oli.TotalPrice - rev;
            os_rev12.Quantity = 0;
            //oSchedList.add(os);
            totalSchedList.add(os_rev12);
            //itemSchedMap.put(oli.Id, oSchedList);
        }
        return totalSchedList;
    }
    
    //Assign territory to opportuntiy when opportutniy owner changed
    public static void AssignTerritory(List<Opportunity> opps) 
    {
        if(!Utilities.currentUserPermissionSetAssignmentMap_DeveloperName_Id.containsKey('Lead_Qualification_Specialist'))
        {
            //OpportunityTriggerDispatcher.executedMethods.add('AssignTerritory');//to prevent this method from executing more than once.
            
            Set<Id> setAccIds_OppOwnerChanged = new Set<Id>();
            Set<Id> setUserIds_OppOwners = new Set<Id>();
            
            for(Opportunity o: opps)
            {
                setAccIds_OppOwnerChanged.add(o.AccountId);
                setUserIds_OppOwners.add(o.OwnerId);
                
                o.DoNotByPassTerritorySelection__c = false;
            }
            
            Map<Id,Set<Id>> map_GroupId_setAccId = new Map<Id,Set<Id>>();
            Map<Id,Set<Id>> map_TerrId_setAccId = new Map<Id,Set<Id>>();
            Map<Id,Set<Id>> map_UserId_setTerrId = new Map<Id,Set<Id>>();
            Map<String,Set<Id>> map_AccNUser_setTerrId = new Map<String,Set<Id>>();
                
            for(AccountShare a : [Select UserOrGroupId,accountId From AccountShare WHERE RowCause = 'TerritoryManual' 
                                  and accountid = :setAccIds_OppOwnerChanged])
            {
                if(map_GroupId_setAccId.containsKey(a.UserOrGroupId))
                {
                    map_GroupId_setAccId.get(a.UserOrGroupId).add(a.AccountId);
                }
                else
                {
                    map_GroupId_setAccId.put(a.UserOrGroupId, new Set<Id>{a.AccountId});
                }
            }
            
            Set<String> Open_TerrExternalIds = new Set<String>();
            for(Open_Territory__c ot : Open_Territory__c.getAll().values())
            {
                Open_TerrExternalIds.add(ot.Territory_External_ID__c);
            }
            
            Set<String> setRegions_IgnoreAccountTerritory = new Set<String>();
            for(Regional_Variables__c rv : Regional_Variables__c.getAll().values())
            {
                if(rv.Ignore_Account_Territory__c)
                    setRegions_IgnoreAccountTerritory.add(rv.Name);
            }
            
            for(Group g: [Select Id,RelatedId From Group Where Id = :map_GroupId_setAccId.keyset() And
                          RelatedId not in (select Id from Territory where Custom_External_TerritoryID__c = :Open_TerrExternalIds)])
            {
                map_TerrId_setAccId.put(g.RelatedId,map_GroupId_setAccId.get(g.Id));
            }
            
            for(UserTerritory ut : [Select UserId, TerritoryId from UserTerritory where IsActive = true and UserId = :setUserIds_OppOwners])
            {
                if(map_UserId_setTerrId.containsKey(ut.UserId))
                {
                    map_UserId_setTerrId.get(ut.UserId).add(ut.TerritoryId);
                }
                else
                {
                    map_UserId_setTerrId.put(ut.UserId, new Set<Id>{ut.TerritoryId});
                }
                                  
                if(map_TerrId_setAccId.containsKey(ut.TerritoryId))
                {
                    for(Id accId : map_TerrId_setAccId.get(ut.TerritoryId))
                    {
                        String temp = ''+accId+ut.UserId;
                        if(map_AccNUser_setTerrId.containsKey(temp))
                        {
                            map_AccNUser_setTerrId.get(temp).add(ut.TerritoryId);
                        }
                        else
                        {
                            map_AccNUser_setTerrId.put(temp, new Set<Id>{ut.TerritoryId});
                        }
                    }
                }
            }
                
            for(Opportunity o: opps)
            {
                String temp = ''+ o.AccountId+o.OwnerId;
                if(map_AccNUser_setTerrId.containsKey(temp))
                {
                    System.debug('Account is in user terriotry, number of territories matched: '+ map_AccNUser_setTerrId.get(temp).size());
                    o.TerritoryId = new List<Id>(map_AccNUser_setTerrId.get(temp))[0];
                    if(map_AccNUser_setTerrId.get(temp).size()>1)
                        o.DoNotByPassTerritorySelection__c = true;
                }
                else if(setRegions_IgnoreAccountTerritory.contains(o.Owner_Region__c))
                {
                    System.debug('Account is not in user terriotry, to assign opp to one of user territory');
                    if(map_UserId_setTerrId.containsKey(o.OwnerId))
                    {
                        o.TerritoryId = new List<Id>(map_UserId_setTerrId.get(o.OwnerId))[0];
                        System.debug('Opportunity is assinged to terriotry: '+o.TerritoryId);
                        if(map_UserId_setTerrId.get(o.OwnerId).size()>1)
                            o.DoNotByPassTerritorySelection__c = true;
                    }
                    else
                        o.DoNotByPassTerritorySelection__c = true;
                }
                else 
                {
                    o.TerritoryId = null;
                    o.DoNotByPassTerritorySelection__c = true;
                }
            }
        }
    }
    
    @future
    private static void futureInsert()
    {
        insert tempOLIS;
        tempOLIS.clear();
    }
}