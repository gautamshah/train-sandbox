@isTest
private class TestCopyAndPaste {
    static testMethod void myUnitTest() {
       RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'mdttest2@mdttest.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 )
        {
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
        insert acc;
        Account acc1=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id,Account_External_ID__c='133');
        insert acc1;
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;
        Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        //Product_SKU__c;
        insert ps;
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
        insert cp;
        Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
        insert cp1;
        Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
        //si.
        Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
        insert si;
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        Channel_Inventory__c ci=new Channel_Inventory__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Quantity_Text__c='-100',Product_Code_Text__c='111');
        insert ci;
        Channel_Inventory__c ci1=new Channel_Inventory__c(Cycle_Period__c=cp1.Id,Distributor_Name__c=acc.Id,Quantity_Text__c='100',Product_Code_Text__c='1111');
        insert ci1;
        In_Transit__c it=new In_Transit__c(Sales_In__c=si.Id,Cycle_Period_dist__c=cp.Id,Quantity_Received__c=null);
        insert it;
        //In_Transit__c..Sales_In__c.Cycle_Period_dist__c
        List<Sales_Out__c> lstSout=new List<Sales_Out__c>();
        
        lstSout.add(so);
        lstSout.add(so1);
        List<Channel_Inventory__c> lstCI=new List<Channel_Inventory__c>();
        
        lstCI.add(ci);
         RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
        //Create portalusercontact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = acc.Id,
        RecordTypeId=rtc.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        Database.insert(contact1);
        Profile portalProfile = [SELECT Id FROM Profile where Name='Asia Distributor - KR' Limit 1];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test123456@mdttest.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        Asia_Team_Asia_use_only__c='PRM Team',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
        ContactShare cs=new ContactShare(ContactId=contact1.Id,UserOrGroupId=user1.Id,ContactAccessLevel='Read');
        Database.insert(cs);
        AccountShare ash=new AccountShare(AccountId=acc.Id,UserOrGroupId=user1.Id,AccountAccessLevel='Read',OpportunityAccessLevel='Read',ContactAccessLevel='Read',CaseAccessLevel='Read');
        Database.insert(ash);
        System.runAs ( user1 ) {
            Test.startTest();
            PageReference pageRef= Page.CopyAndPasteSalesOut;
            Test.setCurrentPage(pageRef);
            CopyLastMonthSalesOut controller = new CopyLastMonthSalesOut(new ApexPages.StandardController(cp));
           // controller.distributorId=acc.Id;
            System.runAs ( portalAccountOwner1 ) 
            {
                PageReference pg=controller.copyandpaste();
                Cycle_Period__c cpcreate=controller.createCyclePeriod(acc.Id,'11','2013',true);
                PageReference pageRef1= Page.CopyAndPasteSalesOut;
                Test.setCurrentPage(pageRef1);
                CopyLastMonthSalesOut controller1 = new CopyLastMonthSalesOut(new ApexPages.StandardController(cp1));
                pg=controller1.copyandpaste();
                ////Channel Inventory
                PageReference pageRef2= Page.CopyAndPasteCI;
                Test.setCurrentPage(pageRef2);
                CopyLastMonthCI controller2 = new CopyLastMonthCI(new ApexPages.StandardController(cp));
                 pg=controller2.copyandpaste();
                Cycle_Period__c cpcreate1=controller2.createCyclePeriod(acc.Id,'12','2013',true);
                 PageReference pageRef3= Page.CopyAndPasteCI;
                Test.setCurrentPage(pageRef3);
                CopyLastMonthCI controller3 = new CopyLastMonthCI(new ApexPages.StandardController(cp1));
                 pg=controller3.copyandpaste();
                 
                PageReference pageRef4= Page.CopyQuantityRemaining;
                Test.setCurrentPage(pageRef4);
                CopyReceivedQuantity controller4 = new CopyReceivedQuantity(new ApexPages.StandardController(cp));
                 pg=controller4.copyandpaste();
            }
            Test.stopTest();
        }
        }
    
        // TO DO: implement unit test
        
    }  

}