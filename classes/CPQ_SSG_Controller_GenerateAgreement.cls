/****************************************************************************************
 * Name    : CPQ_SSG_Controller_GenerateAgreement
 * Author  : Isaac Lewis
 * Date    : 2016.06.01
 * Purpose : Provides validation before passing user to template generation options
 * See     : CustomOpptyApprovalsLaunchController.cls
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 2016.06.01  Isaac Lewis          Created.
 *****************************************************************************************/

public with sharing class CPQ_SSG_Controller_GenerateAgreement {

    private Apttus__APTS_Agreement__c agmt;

    public CPQ_SSG_Controller_GenerateAgreement(ApexPages.StandardController stdController) {
        this.agmt = (Apttus__APTS_Agreement__c)stdController.getRecord();
        this.agmt = [SELECT Id, RecordTypeId, Apttus__Status__c, SSG_Stocking_Order_PO__c, SSG_Stocking_Order_Confirmation__c, SSG_Stocking_Order_Type__c, SSG_Tri_Staple_Core_Reloads__c, SSG_Tri_Staple_Specialty_Reloads__c, SSG_Legacy_Reloads__c 
                     FROM Apttus__APTS_Agreement__c WHERE Id = :agmt.Id];
    }

    public PageReference doLaunchGenerate(){

      // Require Smart Cart to have required information before generating template
      if (agmt.RecordTypeId == CPQ_AgreementProcesses.getAgreementRecordTypesByNameMap().get(CPQ_AgreementProcesses.SMART_CART_RT).Id) {
        if( agmt.SSG_Stocking_Order_PO__c == NULL ||
            agmt.SSG_Stocking_Order_Confirmation__c == NULL ||
            agmt.SSG_Stocking_Order_Type__c == NULL
        ) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.FATAL, 'All stocking order information MUST be provided. Account PO #, Medtronic Order Confirmation #, and Order # can be obtained through Covidien Connect or Customer Service.'));
        }
        if( agmt.SSG_Tri_Staple_Core_Reloads__c == NULL ||
            agmt.SSG_Tri_Staple_Specialty_Reloads__c == NULL ||
            agmt.SSG_Legacy_Reloads__c == NULL
        ) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.FATAL, 'Stocking Order Reload Mix section must be completed before generating a Smart Cart Agreement. Please adjust reload fields as needed.'));
        }
        if(ApexPages.hasMessages()){
          return null;
        }
      }

      // Redirect to generate page with passed parameters
      PageReference pageRef = Page.Apttus__SelectTemplate;

      // Pass all parameters to generate page
      Map<String,String> params = ApexPages.currentPage().getParameters();
      for(String key : params.keySet()) {
        pageRef.getParameters().put(key,params.get(key));
      }

      pageRef.setRedirect(true);
      return pageRef;

    }

    public PageReference doReturn() {
        PageReference pageRef = new ApexPages.StandardController(this.agmt).view();
        pageRef.setRedirect(true);
        return pageRef;
    }

}