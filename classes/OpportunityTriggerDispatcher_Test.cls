@isTest(SeeAllData=true)
public class OpportunityTriggerDispatcher_Test
{
    @isTest
    public static void testA()
    {
        Test.startTest();
        User u = [SELECT Id from user where Profile.name='US - RMS' AND isActive = TRUE LIMIT 1];
        System.runas(u){
        RecordType rtOpp=[Select Id from RecordType where DeveloperName='US_Opportunity_RMS' Limit 1];
        RecordType rtAcc=[Select Id from RecordType where DeveloperName='User_Submitted_Healthcare_Facility' Limit 1];
        Account newaccount = new Account();
        newaccount.Name = 'TestAccount';
        newaccount.BillingCity = 'Los Angeles';
        newaccount.BillingCountry = 'US';
        newaccount.BillingStreet ='Wallstreet';
        newaccount.BillingState ='California';
        newaccount.BillingPostalCode='123456';
        newaccount.RecordTypeId  = rtAcc.Id;
        insert newaccount;  
        Opportunity newOpp=new Opportunity();
        newOpp.RecordTypeId=rtOpp.Id;
        newOpp.Name='TestOpp';
        newOpp.Account=newaccount;
        newOpp.Capital_Disposable__c='Capital';
        newOpp.StageName='Identify';
        newOpp.CloseDate=Date.today().addDays(+60);
        insert newOpp;
        newopp.Name = 'UpdatedTestOpp';
        update newopp;
        }
        Test.stopTest();
    }
}