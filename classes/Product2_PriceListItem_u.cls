/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.	If there are multiple related Jiras, add them on the next line
5.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date		Id   Initials   Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161103	A	IL			AV-001		Title of the Jira – Some changes needed to be made to support Medical Supplies
20161115	A	PAB			AV-253		Moved code from cpqPriceListItem_Product2 to here - the code is driven by
										the Product2 trigger and should be located here
20161115	B	PAB			AV-64		Changed the reference from
										Apttus_Config2__PriceListItem__c.Apttus_Config2__ChargeType__c to
										Product2.Charge_Type__c because the first would not be present on an Insert
										Removed references to the Apttus_Surgical_Product__c to support the move
										to OrganizationName__c
20170216-A	PAB		AV-280	This was using cpqPriceListItem_g for some reason - they should only be for
    						the products passed in
20170303-A	PAB		AV-280		The original code was very inefficient - switched it to leverage the cache
                                classes, which already did most of the work that was in these methods
20170303-B	PAB		AV-280		Not sure if this was being handled originally - a Product2 record can currently
                                only be assigned to 1 Organization - so if you need to represent the same product
                                for 2+ Organizations, you would have to clone it and set the Organization
*/     
public class Product2_PriceListItem_u extends sObject_u
{
	//// This class provides support for PriceListItems, which defines a relationship
	//// between a Lookup(Product2) and Master-Detail(Price List) - it's a "join"
	//// and uniquely defines 1 combination of Price List/Product2.
	//// The OrganizationName__c field on the Price List & Product2 records can
	//// be used to automatically create a PLI between the two based on the
	//// OrganizationName being the same
	////
	//// Can only be instantiated for a Product2_u class
	////
	public Product2_PriceListItem_u(Product2_u parent)
	{
	}
	
    private static List<Schema.SObjectField> fieldsToCompare = new List<Schema.SObjectField>
    {
        Product2.Field.Apttus_Config2__Uom__c,
        Product2.Field.Catalog_Price__c,
        Product2.Field.Cost__c,
        Product2.Field.Apttus_Config2__ExpirationDate__c,
        Product2.Field.Apttus_Config2__EffectiveDate__c,
        Product2.Field.Charge_Type__c,
        Product2.Field.Apttus_Product__c
    };
                    
    private static  Map<Schema.SObjectField, Schema.SObjectField> fieldsToCopy =
    	new Map<Schema.SObjectField, Schema.SObjectField>
    	{
	        Product2.Field.Apttus_Config2__Uom__c =>
	        		Apttus_Config2__PriceListItem__c.Field.Apttus_Config2__PriceUom__c,
	        Product2.Field.Apttus_Config2__ExpirationDate__c =>
	        		Apttus_Config2__PriceListItem__c.Field.Apttus_Config2__ExpirationDate__c,
	        Product2.Field.Apttus_Config2__EffectiveDate__c =>
	        		Apttus_Config2__PriceListItem__c.Field.Apttus_Config2__EffectiveDate__c,
	        Product2.Field.Cost__c =>
	        		Apttus_Config2__PriceListItem__c.Field.Apttus_Config2__Cost__c,
	        Product2.Field.Charge_Type__c =>
	        		Apttus_Config2__PriceListItem__c.Field.Apttus_Config2__ChargeType__c
    	};
    
 	private static Map<Schema.SObjectField, Schema.SObjectField> fieldsToCopyIfChargeTypeNotIgnored =
 	   	new Map<Schema.SObjectField, Schema.SObjectField>
	   	{
	   		Product2.Field.Catalog_Price__c =>
	           		Apttus_Config2__PriceListItem__c.Field.Apttus_Config2__ListPrice__c
	   	};
 	
    public void Sync(List<Product2> newList, Map<Id, Product2> oldMap)
    {
        Map<ID, Product2> relatedApttusProducts = new Map<ID, Product2>();

        for(Product2 p : newList)
            if (p.Apttus_Product__c)
                // oldMap == null only occurs for Inserts
                // if oldMap != null (update) and the fields do not match (values changed)
                if (oldMap == null || !sObject_c.equal(p, oldMap.get(p.Id), fieldsToCompare))
                    relatedApttusProducts.put(p.Id, p);
        
        createOrUpdatePriceListItems(relatedApttusProducts);
    }
    
    private static void createOrUpdatePriceListItems(Map<Id, Product2> products)
    {
        if (products != null && !products.isEmpty())
        {
		    //// 20170216-A
		    //// 20170303-A The original logic was extracting data from products &
		    ////            price list items to determine which products already had
		    ////            and existing PLI.  The PLI cache has an index to do a
		    ////            lookup by the Product Id to accomplish the same thing
		    //// 
	    	Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> ProductId2PriceListItem =
	    		cpqPriceListItem_u.fetch(
	    			Apttus_Config2__PriceListItem__c.field.Apttus_Config2__ProductId__c,
	    			DATATYPES.castToObject(products.keySet()));
    
		    //// 20170216-A
			//// Determine which products have an existing Price List Item and
			//// create a list of those that don't
			set<Id> productsWithOutPLI = new set<Id>();
			
			for(Id pid : products.keySet())
				if (!ProductId2PriceListItem.containsKey(pid))
				    productsWithOutPLI.add(pid);

			Map<string, Id> OrgName2PLId = new Map<string, Id>();
			for(OrganizationNames_g.Abbreviations abbr : cpqPriceList_c.OrganizationName2PriceList.keySet())
				OrgName2PLId.put(abbr.name(), cpqPriceList_c.OrganizationName2PriceList.get(abbr).Id);
				
            //// For the products that didn't have a Price List Item, create one for them
            //// 20170303-A Streamlined now
            integer ndx = 1;
            for(object opid : productsWithOutPLI)
           	{
           		Id pid = DATATYPES.castToId(opid);
           		if (ProductId2PriceListItem.get(pid) == null)
           			ProductId2PriceListItem.put(pid, new Map<Id, Apttus_Config2__PriceListItem__c>());

				Id plId = OrgName2PLId.get(products.get(pid).OrganizationName__c);
       			ProductId2PriceListItem.get(pid).put(
       				cpqPriceListItem_c.dummyId(ndx++), //// Create a dummy Id so we can add to the map
                    cpqPriceListItem_c.create(plId, pid));
           	}
           	
            copyProduct2ValuesToPriceListItem(ProductId2PriceListItem, products);
               
		    // 20170216-A
			// Since we did Inserts & Updates, we can handle them with an Upsert
			// Using the Price List Item map ensures there are only 1 entry for
			// each product and they are referenced by the values()
			Map<Id, Apttus_Config2__PriceListItem__c> PLIs = new Map<Id, Apttus_Config2__PriceListItem__c>();
			for(object oid : ProductId2PriceListItem.keySet())
				PLIs.putAll(ProductId2PriceListItem.get(oid));
				
            upsert PLIs.values();
        }
    }
    
    private static void copyProduct2ValuesToPriceListItem(
    		Map<object, Map<Id, Apttus_Config2__PriceListItem__c>>  productId2PriceListItem,
    		Map<Id, Product2> products)
    {   
    	// 20170216-A
		// For all the new and existing Price List Items, copy data from the
		// Product2 to the Apttus_Config2__PriceListItem__c record
		// - ie. Add it for new PLIs, update the existing PLIs
        for (Id pid : products.keySet())
        {
        	Map<Id, Apttus_Config2__PriceListItem__c> plis = productId2PriceListItem.get(pid);
        	if (plis != null && !plis.isEmpty())
        	{
        		Product2 p = products.get(pid);
        	
	        	for(Id plid : plis.keySet())
	        	{
	        		sObject_c.copy(p, plis.get(plid), fieldsToCopy);
	
					// 20161115-B
					// If it's not in the list of Charge Types to ignore (Trade-ins, Promotions, )
					// then also copy the List (Catalog) Price over
	            	if (!cpqChargeType_c.ignore(p))
	                	sObject_c.copy(p, plis.get(plid), fieldsToCopyIfChargeTypeNotIgnored);
	        	}
        	}
        }
    }
}
    /*
    // This was the original logic
	//=========================================================
	// Auto Sync price list item with Product
	//=========================================================
	public static void SyncPriceListItem(List<Product2> inputProducts, Map<ID, Product2> oldMap) {
		Map<ID, Product2> candidates = new Map<ID, Product2>();
		for (Product2 p : inputProducts) {
			//only work with Apttus products
			if (!p.Apttus_Product__c && !p.Apttus_Surgical_Product__c) {
				continue;
			}

			//collect all newly created products
			if (oldMap == null) {
				candidates.put(p.ID, p);
			}

			//collect the products which certain key fields have been changed
			else {
				Product2 pOld = oldMap.get(p.ID);

				if (p.Apttus_Config2__Uom__c != pOld.Apttus_Config2__Uom__c ||
					p.Catalog_Price__c != pOld.Catalog_Price__c ||
					p.Cost__c != pOld.Cost__c ||
					p.Apttus_Config2__ExpirationDate__c != pOld.Apttus_Config2__ExpirationDate__c ||
					p.Apttus_Config2__EffectiveDate__c != pOld.Apttus_Config2__EffectiveDate__c ||
					p.Charge_Type__c != pOld.Charge_Type__c ||
					p.Apttus_Product__c != pOld.Apttus_Product__c ||
					p.Apttus_Surgical_Product__c != pOld.Apttus_Surgical_Product__c)
				{
					candidates.put(p.ID, p);
				}
			}
		}

		if (candidates.size() == 0)
			return;

		Map<String,String> priceListIDs = new Map<String,String>();
		priceListIDs.put('RMS',CPQ_Utilities.getRMSPriceListID());
		priceListIDs.put('SSG',CPQ_Utilities.getSSGPriceListID());
		if ( String.isEmpty(priceListIDs.get('RMS')) || String.isEmpty(priceListIDs.get('SSG')) )
			return;

		// Replaced with map to handle multiple price lists
		// String thePriceListID = CPQ_Utilities.getRMSPriceListID();
		// if (String.isEmpty(thePriceListID))

		//update existing price list items
		List<Apttus_Config2__PriceListItem__c> plisToUpdate = [Select Id, Apttus_Config2__ProductId__c, Apttus_Config2__ChargeType__c From Apttus_Config2__PriceListItem__c Where Apttus_Config2__PriceListId__c in: priceListIDs.values() and Apttus_Config2__ProductId__c in: candidates.keySet()];
		Set<ID> prodsHavePLI = new Set<ID>();
		Set<String> chargeTypeIgnoreList = CPQ_Utilities.getCallbackChargeTypeIgnoreList();

		for (Apttus_Config2__PriceListItem__c pli : plisToUpdate) {
			Product2 p = candidates.get(pli.Apttus_Config2__ProductId__c);

			pli.Apttus_Config2__PriceUom__c = p.Apttus_Config2__Uom__c;

			if (!chargeTypeIgnoreList.contains(pli.Apttus_Config2__ChargeType__c))
				pli.Apttus_Config2__ListPrice__c = p.Catalog_Price__c;

			pli.Apttus_Config2__ExpirationDate__c = p.Apttus_Config2__ExpirationDate__c;
			pli.Apttus_Config2__EffectiveDate__c = p.Apttus_Config2__EffectiveDate__c;
			pli.Apttus_Config2__Cost__c = p.Cost__c;
			pli.Apttus_Config2__ChargeType__c = p.Charge_Type__c;

			prodsHavePLI.add(p.ID);
		}

		if (plisToUpdate.size() > 0)
			update plisToUpdate;

		//create new price list items
		List<Apttus_Config2__PriceListItem__c> plisToInsert = new List<Apttus_Config2__PriceListItem__c>();
		for (ID pid : candidates.keySet()) {
			Product2 p = candidates.get(pid);

			//price list item need to be created
			if (!prodsHavePLI.contains(p.ID)) {
				Apttus_Config2__PriceListItem__c pli = new Apttus_Config2__PriceListItem__c();
				pli.Apttus_Config2__RollupPriceToBundle__c = true;
				pli.Apttus_Config2__ProductId__c = p.ID;
				pli.Apttus_Config2__PriceUom__c = p.Apttus_Config2__Uom__c;
				pli.Apttus_Config2__PriceType__c = 'One Time';
				pli.Apttus_Config2__PriceMethod__c = 'Per Unit';
				if(p.Apttus_Surgical_Product__c){
					pli.Apttus_Config2__PriceListId__c = priceListIDs.get('SSG');
				} else {
					pli.Apttus_Config2__PriceListId__c = priceListIDs.get('RMS');
				}
				pli.Apttus_Config2__ListPrice__c = p.Catalog_Price__c;
				pli.Apttus_Config2__ExpirationDate__c = p.Apttus_Config2__ExpirationDate__c;
				pli.Apttus_Config2__EffectiveDate__c = p.Apttus_Config2__EffectiveDate__c;
				pli.Apttus_Config2__Cost__c = p.Cost__c;
				pli.Apttus_Config2__ChargeType__c = p.Charge_Type__c;
				pli.Apttus_Config2__AllowManualAdjustment__c = true;
				pli.Apttus_Config2__AllocateGroupAdjustment__c = true;
				pli.Apttus_Config2__Active__c = true;

				plisToInsert.add(pli);
			}
		}

		if (plisToInsert.size() > 0)
			insert plisToInsert;
	}
    */