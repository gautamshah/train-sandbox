@isTest
public with sharing class Community_Add_Users_Test {
    public List<Community_User__c> usrlist;
    public static testMethod void addusers(){
    RecordType rtCom=[select Id, Name from Recordtype where name='US-Hospital Community' limit 1];
      Account a= new Account();
      a.name='Test_Community';
      a.RecordTypeId =rtCom.id;
      insert a;
      
      User user1 = new User();
      user1 = [select Id, Name from User limit 1];

        createData(a.id,user1.id);
        List<Community_User__c> usrlist = [select Id,Name from Community_User__c where Hospital_Community__c=:a.Id];
        system.debug('usrlist '+usrlist);
        test.starttest();
        PageReference pageRef = Page.Community_Add_User;
        pageRef.getParameters().put('accntid', a.id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        CommunityAddUsers com = new  CommunityAddUsers(sc);
        Community_User__c comuser = new Community_User__c ();
        comuser.Hospital_Community__c = a.id;
        comuser.SFDC_User__c = user1.id;
        comuser.Hospital_Community_User__c = true;
        insert comuser;
        com.Save();
        comuser.Rolodex__c = true;
        upsert comuser;
        com.Save();
        com.Next();
        delete comuser;
        com.Save();
        com.Cancel();
        test.stopTest();
    }
    private static void createData(Id accid,Id usrid){
      
      
      RecordType rtFacility=[select Id, Name from Recordtype where name='US-Healthcare Facility' limit 1];
      Account a1= new Account();
      a1.name='sampleTest1';
      a1.RecordTypeId =rtFacility.id;
      insert a1;

      Account_Affiliation__c aaf= new Account_Affiliation__c();
      aaf.Affiliated_with__c=a1.id;
      aaf.AffiliationMember__c=accid;
      insert aaf;
      
      User usr = new User();
      usr.LastName = 'testLastName';
      usr.FirstName = 'testFirstName';
      usr.Email = 'test@test.com';
      usr.LanguageLocaleKey = 'en_US';
      usr.ProfileId = '00eU0000000pN2dIAE';
      usr.LocaleSidKey = 'en_US';
      usr.TimeZoneSidKey = 'America/New_York';
      usr.Username = 'testl@testf.com';
      usr.Alias = 'test.t';
      usr.CommunityNickname = 'test.t';
      usr.EmailEncodingKey = 'ISO-8859-1';
      insert usr;
               
      AccountTeamMember atm = new AccountTeamMember();
      atm.AccountId = a1.id;
      atm.UserId = usrid;
      insert atm;
      
      AccountTeamMember atm1 = new AccountTeamMember();
      atm1.AccountId = a1.id;
      atm1.UserId = usr.id;
      insert atm1;
      
      Community_User__c comuser1 = new Community_User__c ();
      comuser1.Hospital_Community__c = accid;
      comuser1.SFDC_User__c = usrid;
      comuser1.Hospital_Community_User__c = true;
      insert comuser1;
        
      Community_User__c comuser2 = new Community_User__c ();
      comuser2.Hospital_Community__c = accid;
      comuser2.SFDC_User__c = usr.id;
      comuser2.Hospital_Community_User__c = true;
      insert comuser2;
    }
}