/****************************************************************************************
 * Name    : SETCase_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 14/12/2015 
 * Purpose : Contains all the logic coming from Account trigger
 * Dependencies: Create task automatically when a SET Case is create, so that the task reporting
 * 				will include the SET Case activities.
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
public class SETCase_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public SETCase_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void OnBeforeUpdate(List<Cost_Analysis_Case__c> newObjects, Map<Id,Cost_Analysis_Case__c> oldMap){
        
        Id SETTaskRTId = [Select Id from RecordType Where sObjectType = 'Task' and DeveloperName = 'SET_Activity' limit 1].Id;
        List<Task> setTasks = new List<Task>();
        Map<String,Cost_Analysis_Case__c> taskId_SETCase = new Map<String,Cost_Analysis_Case__c>(); 
        
        for(Cost_Analysis_Case__c setCase : newObjects)
        {
            if(!setCase.Task_Created__c && setCase.Opportunity__c != null && setCase.Start_Date__c != null)
            {
                Task SetTask = new Task();
                SetTask.OwnerId = setCase.OwnerId;
                SetTask.Status = 'Completed';
                SetTask.Priority = 'Low';
                SetTask.IsReminderSet = false;
                SetTask.ActivityDate = setCase.Start_Date__c;   
                SetTask.RecordTypeId = SETTaskRTId; 
                SetTask.WhatId = setCase.Opportunity__c;
                SetTask.Activity_Type__c = 'Scrub/Clin. Eval.';
                
                if(setCase.Contact__c != null)
                    SetTask.WhoId = setCase.Contact__c;
                
                SetTask.Subject = 'SET Activity for SET Case ' + setCase.Name;
                SetTask.Description = 'This is the SET Activity for SET Case ' + setCase.Name + '\n' + 
                                      'The details of the SET Case can be found at ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + setCase.Id;
                
                setTasks.add(SetTask);
                setCase.Task_Created__c = true;
            }
        }
        
        if(setTasks.size()>0)
            insert setTasks;
    }
}