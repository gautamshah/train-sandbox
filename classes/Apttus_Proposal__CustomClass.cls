/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global abstract class CustomClass {
    global CustomClass() {

    }
global enum ActionType {ACTION_CLONE, ACTION_CREATE_AGREEMENT, ACTION_CREATE_FROM_ACCOUNT, ACTION_CREATE_FROM_OPPORTUNITY}
global interface IQuoteLifecycleCallback {
}
global interface IQuoteLifecycleCallback2 extends Apttus_Proposal.CustomClass.IQuoteLifecycleCallback {
}
}
