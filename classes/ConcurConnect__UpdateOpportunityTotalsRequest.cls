/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class UpdateOpportunityTotalsRequest {
    global UpdateOpportunityTotalsRequest(String requestXML) {

    }
    global String getCurrencyCode() {
        return null;
    }
    global String getOpportunityId() {
        return null;
    }
    global String getPaidExpenses() {
        return null;
    }
    global String getUnpaidExpenses() {
        return null;
    }
    global String toXml() {
        return null;
    }
}
