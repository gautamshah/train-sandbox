/*
MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYY.MM.DD A    PAB        CPR-000    Updated comments section
                PAB                   Created.
2017.02.02      IL         AV-280     Optimized query for CPQ_SSG_OD_Assignments__c records to query only once.
*/ 
public class cpqOfferDevelopmentAssignment_u extends sObject_u
{
    private static cpqOfferDevelopmentAssignment_g cache =
    	new cpqOfferDevelopmentAssignment_g();
    	
    static
    {
    	cpqOfferDevelopmentAssignment_c.load();
    }
    	 
    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<CPQ_SSG_OD_Assignments__c> newList,
            Map<Id, CPQ_SSG_OD_Assignments__c> newMap,
            List<CPQ_SSG_OD_Assignments__c> oldList,
            Map<Id, CPQ_SSG_OD_Assignments__c> oldMap,
            integer size
        )
    {
		if ((isInsert || isUpdate) && isBefore)
		{
			VerifyTerritoryExists(newList);
		}
    }

    private static void VerifyTerritoryExists(List<CPQ_SSG_OD_Assignments__c> newList)
    {
		set<object> extIds = new set<object>();
    	for(CPQ_SSG_OD_Assignments__c od: newList)
        	extIds.add(od.External_Territory_Id__c); 
        	
        Map<object, Map<Id, Territory>> externalId2TerritoryMap =
        	Territory_u.fetch(Territory.field.Custom_External_TerritoryID__c, extIds);

    	for(CPQ_SSG_OD_Assignments__c od : newList)
        	if (!externalId2TerritoryMap.containsKey(od.External_Territory_Id__c))
	            od.addError(string.valueOf(cpqOfferDevelopmentAssignment_u.class) +
	            	': The territory designator \'' + od.External_Territory_Id__c +
	            	 ' does not exist in SalesForce.  ' +
	            	 'Please verify your entry and try again or contact your administrator');
    	    else
    	    {
    	    	Map<Id, Territory> tMap = externalId2TerritoryMap.get(od.External_Territory_Id__c);
    	    	for(Id id : tMap.keySet())
        	    	od.Territory_Description__c = tMap.get(id).Description;
    	    }
    }

	public static Map<string, Id> ExternalTerritoryName2OfferDevelopmentAnalystId
	{
		get
		{
			if (ExternalTerritoryName2OfferDevelopmentAnalystId == null)
			{
				ExternalTerritoryName2OfferDevelopmentAnalystId = new Map<string, Id> ();
				
				Map<object, Map<Id, CPQ_SSG_OD_Assignments__c>> externalName2Record =
					cache.fetch(CPQ_SSG_OD_Assignments__c.field.External_Territory_Id__c);
				
				for(object obj : externalName2Record.keySet())
					for(CPQ_SSG_OD_Assignments__c rec : externalName2Record.get(obj).values())
						ExternalTerritoryName2OfferDevelopmentAnalystId.put((string)obj, DATATYPES.castToId(rec.get(CPQ_SSG_OD_Assignments__c.field.CPQ_SSG_OD_Analyst__c)));
			}

			return ExternalTerritoryName2OfferDevelopmentAnalystId;
		}
		
		private set;
	}
}