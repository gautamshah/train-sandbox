/*
Name    : SalesOrganizationLevel_g
Type    : Picklist
sObject : Apttus__APTS_Agreement__c,
          Apttus_Proposal__Proposal__c
          
Author  : Paul Berglund
Date    : 10/11/2016
Purpose : Define supporting properties for the SalesOrganizationLevel field
     
========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE
----        ------            ------
*/
public class SalesOrganizationLevel_g
{
	@testVisible
	private static integer defaultCode = 0;

	@testVisible
	private static string defaultText = '0 - Territory';

	public static final string TERRITORY = '0 - Territory';
	public static final string REGIONAL = '1 - Regional';
	public static final string ZONE = '2 - Zone';
	public static final string ORGANIZATION = '3 - Organization';
	public enum Levels
	{
		Territory,
		Regional,
		Zone,
		Organization
	}
	
	public static Map<Levels, string> Level2Value =
		new Map<Levels, string>
		{
			Levels.Territory => TERRITORY,
			Levels.Regional => REGIONAL,
			Levels.Zone => ZONE,
			Levels.Organization => ORGANIZATION
		};
		
	public static Map<string, Levels> Value2Level =
		new Map<string, Levels>();
		
	static
	{
		for(Levels level : Level2Value.keySet())
			Value2Level.put(Level2Value.get(level), level);
	}
	
	@testVisible
	private static Map<integer, string> Code2Text =
			new Map<integer, string>
			{
				defaultCode => defaultText,
	    		1 => '1 - Regional',
	    		2 => '2 - Zone',
	    		3 => '3 - Organization'
			};
		
	@testVisible
	private static Map<string, integer> Text2Code = new Map<string, integer>();
	
	static
	{
		for(integer code : Code2Text.keySet())
			Text2Code.put(Code2Text.get(code), code);
	}
	
	public static string convert(integer code)
	{
		return (Code2Text.containsKey(code)) ?
				Code2Text.get(code) :
				defaultText;
	}
	
	public static integer convert(string text)
	{
		return (Text2Code.containsKey(text)) ?
				Text2Code.get(text) :
				defaultCode;
	}
}