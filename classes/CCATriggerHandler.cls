/****************************************************************************************
 * Name    : CCATriggerHandler 
 * Author  : John Romanowski
 * Date    : 24/04/2017 
 * Purpose : Class to handle Contract Compliance Automation (CCA) triggers
 * Dependencies: 
 *
 * MODIFICATION HISTORY 
 * DATE        AUTHOR               CHANGE
 *
 *****************************************************************************************/
public with sharing class CCATriggerHandler {

    //Update the Compliance_Requirement__c parent object of any cases that have closed
    public void NotifyParentCaseClosed(List<Case> newObjects){
        //Loop through the changes in the cases looking for closed cases to update parent Compliance_Requirement__c
        List<Compliance_Requirement__c> ccaList = new List<Compliance_Requirement__c>();
        for (Case c : newObjects) {
            //Only update if case closed
            if(c.Status == 'Closed-Resolved') {
                Compliance_Requirement__c req = new Compliance_Requirement__c(
                    Id = c.Compliance_Requirement__c
                    ,CaseClosedDate__c = Date.Today()
                    ,CaseCreated__c = FALSE
                    ,Out_of_Compliance__c = FALSE);
                ccaList.add(req);       
            }
        }
        
        // Update ant parent records
        try {
            update ccaList;
            } catch (Exception ex) {
            System.debug('Could not update Parent Compliance Requirement object with cause: ' + ex.getCause());
            }
    }
}