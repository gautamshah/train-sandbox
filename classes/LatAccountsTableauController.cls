/****************************************************************************************
* Name    : LatAccountsTableauController
* Author  : Karem Perez
* Date    : 6/14/2016
* Purpose : Extension for the Visualforce Page that is embedded into LATAM - Account - Healthcare Facility page layout.
* 
* Dependancies: 
* Referenced By: LAT_Accounts_Tableau.vfp
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
*   DATE            AUTHOR                  CHANGE
*   ----            ------                  ------
*   
*****************************************************************************************/ 

public class LatAccountsTableauController {
    
    private final Account acct;
    
	public Boolean AccountPotentialPlaybook {get; set;}
    public Boolean AccountPerformancePlaybook {get; set;}
    public Boolean DaySalesHistory {get; set;}
    
    public LatAccountsTableauController(ApexPages.StandardController controller){
        AccountPotentialPlaybook = true;
        AccountPerformancePlaybook = false;
        DaySalesHistory = false;
        
        this.acct = (Account)controller.getRecord();
    }
    
    public void viewAccountPotentialPlaybook(){
        AccountPotentialPlaybook = true;
        AccountPerformancePlaybook = false;
        DaySalesHistory = false;       
    }
    
    public void viewAccountPerformancePlaybook(){
		AccountPotentialPlaybook = false;
        AccountPerformancePlaybook = true;
        DaySalesHistory = false;                
    }
    
    public void viewDaySalesHistory(){
        AccountPotentialPlaybook = false;
        AccountPerformancePlaybook = false;
        DaySalesHistory = true;
    }    
}