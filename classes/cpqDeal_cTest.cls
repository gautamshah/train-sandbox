/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
2017.01.19   A    IL         AV-280     Created. To increase coverage on dependencies related to AV-280.

Notes:
1.  If there are multiple related Jiras, add them on the next line
2.  Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

@isTest
public class cpqDeal_cTest {


    @isTest
    static void proposalToAgreementFieldMap()
    {

        Map<sObjectField,sObjectField> p2a = cpqDeal_c.proposalToAgreementFieldMap;
        System.assertEquals(cpqAgreement_c.FIELD_ElectrosurgeryEaches, p2a.get(cpqProposal_c.FIELD_ElectrosurgeryEaches));

    }

    @isTest
    static void royaltyFreeAgreement()
    {
        Map<SObjectField,SObjectField> p2a = cpqDeal_c.RoyaltyFreeAgreement;
        System.assertEquals(11, p2a.keySet().size());
        System.assertEquals(cpqAgreement_c.FIELD_ElectrosurgeryEaches, p2a.get(cpqProposal_c.FIELD_ElectrosurgeryEaches));
    }

    @isTest
    static void copy()
    {
        Integer value = 1;

        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c();
        p.Electrosurgery_Eaches__c = value;

        Apttus__APTS_Agreement__c a = new Apttus__APTS_Agreement__c();
        Map<sObjectField,sObjectField> fields = cpqDeal_c.RoyaltyFreeAgreement;

        cpqDeal_c.copy(p,a,fields);
        System.assertEquals(value, a.Electrosurgery_Eaches__c);
        System.assertEquals(null, a.Electrosurgery_Sales__c);

    }


}