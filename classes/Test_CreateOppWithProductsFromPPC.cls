/****************************************************************************************
 * Name    : Test_CreateOppWithProductsFromPPC 
 * Author  : Bill Shan
 * Date    : 21/05/2014 
 * Purpose : Test class for CreateOppWithProductsFromPPCExt
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
@isTest (seeAllData=true)
private class Test_CreateOppWithProductsFromPPC {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile profile1 = [Select Id from Profile where name = 'EM - All'];
        User emUser = new User(
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'mdt3test2@mdttest.com',
            Alias = 'bshan',
            Email='bill.shan@covidien.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bill',
            Lastname='Shan',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            DefaultCurrencyIsoCode = 'CHF');
        insert emUser;
        
        Account acc=new Account(Name='Test EM Accuont',Status__c='Active',OwnerId = emUser.Id);
        insert acc;
        
        Contact con = new Contact();
        con.LastName = 'Tester';
        con.AccountId = acc.Id;
        con.Department_picklist__c = 'Administration';
        con.Connected_As__c = 'Administrator';
        insert con;
        
        Product_Preference_Card__c ppc = new Product_Preference_Card__c();
        ppc.Physician__c = con.Id;
        ppc.Procedure__c = 'test procedure';
        insert ppc;
        
        List<Pricebook2> pbs = new List<Pricebook2>(); 
        
		pbs.add(new Pricebook2(Name = 'pb1', IsActive = true));
		pbs.add(new Pricebook2(Name = 'pb2', IsActive = true));
		pbs.add(new Pricebook2(Name = 'pb3', IsActive = true));
		insert pbs;
		
		List<Product2> prds = new List<Product2>(); 		
        prds.add(new Product2(Name = 'prd1', CurrencyIsoCode = 'CHF',ProductCode = 'prd1',isActive=true));
		prds.add(new Product2(Name = 'prd2', CurrencyIsoCode = 'CHF',ProductCode = 'prd2',isActive=true));
		prds.add(new Product2(Name = 'prd3', CurrencyIsoCode = 'CHF',ProductCode = 'prd3',isActive=true));
		prds.add(new Product2(Name = 'prd4', CurrencyIsoCode = 'CHF',ProductCode = 'prd4',isActive=true));
		insert prds;
		
		Pricebook2 teststd = [SELECT Id, IsActive FROM Pricebook2 WHERE IsStandard = true AND IsActive = true limit 1];
		List<PricebookEntry> pbes1 = new List<PricebookEntry>();
		List<PricebookEntry> pbes2 = new List<PricebookEntry>();
		
		pbes1.add(new PricebookEntry(Pricebook2Id = teststd.Id, Product2Id = prds[0].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		pbes1.add(new PricebookEntry(Pricebook2Id = teststd.Id, Product2Id = prds[1].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		pbes1.add(new PricebookEntry(Pricebook2Id = teststd.Id, Product2Id = prds[2].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		
		insert pbes1;
		
		pbes2.add(new PricebookEntry(Pricebook2Id = pbs[0].Id, Product2Id = prds[0].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		pbes2.add(new PricebookEntry(Pricebook2Id = pbs[0].Id, Product2Id = prds[1].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		pbes2.add(new PricebookEntry(Pricebook2Id = pbs[1].Id, Product2Id = prds[1].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		pbes2.add(new PricebookEntry(Pricebook2Id = pbs[2].Id, Product2Id = prds[2].Id,  
														UnitPrice = 0, IsActive = true, CurrencyIsoCode = 'CHF'));
		insert pbes2;
        
        List<Prod_Preference_Card_Product__c> pps = new List<Prod_Preference_Card_Product__c>();
        pps.add(new Prod_Preference_Card_Product__c(Product_Preference_Card__c = ppc.Id,Target_Covidien_Product__c=prds[0].Id));
        pps.add(new Prod_Preference_Card_Product__c(Product_Preference_Card__c = ppc.Id,Target_Covidien_Product__c=prds[1].Id));
        pps.add(new Prod_Preference_Card_Product__c(Product_Preference_Card__c = ppc.Id,Target_Covidien_Product__c=prds[2].Id));
        pps.add(new Prod_Preference_Card_Product__c(Product_Preference_Card__c = ppc.Id,Target_Covidien_Product__c=prds[3].Id));
        pps.add(new Prod_Preference_Card_Product__c(Product_Preference_Card__c = ppc.Id));
        insert pps;
        
        
        System.runAs(emUser) 
        {
        	Test.startTest();
	    	PageReference pageRef= Page.CreateOppWithProductsFromPPC;
	        Test.setCurrentPage(pageRef);
	        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(pps);
	        sc.setSelected(pps);
	        CreateOppWithProductsFromPPCExt controller = new CreateOppWithProductsFromPPCExt(sc);
	        PageReference p = controller.CreateOppFromPPC();
	        Boolean b = controller.gethasMultiplePB();	
	        
	        pps.remove(4);
	        sc.setSelected(pps);
	        controller = new CreateOppWithProductsFromPPCExt(sc);
	        p = controller.CreateOppFromPPC();
	        b = controller.gethasMultiplePB();	
	        
	        pps.remove(3);
	        sc.setSelected(pps);
	        controller = new CreateOppWithProductsFromPPCExt(sc);
	        p = controller.CreateOppFromPPC();
	        b = controller.gethasMultiplePB();
	        
	        pps.remove(2);
	        sc.setSelected(pps);
	        controller = new CreateOppWithProductsFromPPCExt(sc);
	        p = controller.CreateOppFromPPC();
	        b = controller.gethasMultiplePB();
	        
	        pps.remove(1);
	        sc.setSelected(pps);
	        controller = new CreateOppWithProductsFromPPCExt(sc);
	        p = controller.CreateOppFromPPC();
	        b = controller.gethasMultiplePB();
	        
	        pps.remove(0);
	        sc.setSelected(pps);
	        controller = new CreateOppWithProductsFromPPCExt(sc);
	        p = controller.CreateOppFromPPC();
	        b = controller.gethasMultiplePB();	        
	        Test.stopTest();
        }
    }
}