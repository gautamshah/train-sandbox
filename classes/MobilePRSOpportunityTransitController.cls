global with sharing class MobilePRSOpportunityTransitController {
    /*****************************************************************************************
    * Name    : Mobile PRS UpdateOpportunity Transit Controller for JAPAN
    * Author  : Hidetoshi kawada (ECS)
    * Date    : 12/04/2015
    * Purpose : Process Review Sheet (Mobile)
    * Outline :
    *
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * 03/29/2016  Hidetoshi Kawada     modified in CVJ_SELLOUT_DEV-98
    *****************************************************************************************/

    /*****************************************************************************************
     * Get Event RecordType (Replicate from PubActionController)
    /****************************************************************************************/

    @RemoteAction
    global static List<RecordType> getRecordTypes(String sObj){
        String[] objs = new String[]{sObj};
        List<RecordType> recordTypes = new List<RecordType>();
        List<RecordType> results = new List<RecordType>();
        Map<Id,RecordType> rtMap = new Map<Id,RecordType>();
        String q = 'Select Id, Name From RecordType Where SobjectType = \'' + sObj + '\' and isActive = true';
        results = Database.query(q);
        for(RecordType rt : results) rtMap.put(rt.Id,rt);
        List<Schema.DescribeSObjectResult> R = Schema.describeSObjects(objs);
        List<Schema.RecordTypeInfo> RT = R[0].getRecordTypeInfos();
        for(Schema.RecordTypeInfo rtInfo : RT){
            if(rtInfo.isAvailable())
            recordTypes.add(rtMap.get(rtInfo.getRecordTypeId()));
        }
        return recordTypes;
    }
}