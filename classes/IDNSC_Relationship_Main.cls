/****************************************************************************************
* Name    : Class: IDNSC_Relationship_Main
* Author  : Gautam Shah
* Date    : 8/12/2014
* Purpose : A wrapper for static functionality for IDNSC_Relationship__c records
* 
* Dependancies: 
* 	Called By: IDNSC_Relationship_TriggerDispatcher
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          		AUTHOR                  CHANGE
* ----          		------                  ------
* Dec. 4, 2014			Gautam Shah				Added Lines 32-33 to make the task start date the same as the assigned date
*****************************************************************************************/
public with sharing class IDNSC_Relationship_Main {

	public static void setTaskAssignedDate(List<IDNSC_Relationship__c> rels)
	{//NOTE: The very first batch of assigned tasks is assigned by workflow.  This method assigns subsequent tasks.
		List <IDNSC_Task__c> updatedTasks = new List <IDNSC_Task__c>();
		Map <Id, String> parentIdMap = new Map <Id, String> ();
		for(IDNSC_Relationship__c rel : rels)
		{
			parentIdMap.put(rel.Id, rel.Current_Event_State__c);
		}
		for(IDNSC_Task__c idnscTask :[Select Id, Name, Runs_in_State__c, IDNSC_Parent__c, IDNSC_Parent__r.Name, Date_Assigned__c, Role__c, Start_Date_Actual__c From IDNSC_Task__c Where IDNSC_Parent__c in :parentIdMap.keySet()]) 
		{
		    if(parentIdMap.get(idnscTask.IDNSC_Parent__c) == idnscTask.Runs_in_State__c) 
		    {
		        idnscTask.Date_Assigned__c = date.today();
		        if(idnscTask.Start_Date_Actual__c == null)
		        	idnscTask.Start_Date_Actual__c = date.today();
		        updatedTasks.add(idnscTask);
		    }
		}
		update updatedTasks;
		IDNSC_Relationship_TriggerDispatcher.executedMethods.add('setTaskAssignedDate');
	}
	
	public static void createChatterGroup(Map<String, IDNSC_Relationship__c> relNameMap)
	{
		Map<String, CollaborationGroup> cGroupMap = new Map<String, CollaborationGroup>();
		List<CollaborationGroup> existingCgroups = new List<CollaborationGroup>([Select Id, Name, OwnerId From CollaborationGroup Where Name In :relNameMap.keySet()]);
		for(CollaborationGroup cg : existingCgroups)
		{
			cGroupMap.put(cg.Name, cg);
		}
		List<CollaborationGroup> newGroups = new List<CollaborationGroup>();
		for(IDNSC_Relationship__c ir : relNameMap.values())
		{
			if(!cGroupMap.keySet().contains(ir.Name))//if the group doesn't already exist
			{
				//create the group
				CollaborationGroup cg = new CollaborationGroup();
				cg.Name = ir.Name;
				cg.CanHaveGuests = false;
				cg.CollaborationType = 'Private';
				cg.OwnerId = ir.Role_Customer_Care__c;
				newGroups.add(cg);
			}
			else
			{
				//remove old members
				removeChatterGroupMembers(cGroupMap.get(ir.Name));
			}
		}
		insert newGroups;
		for(CollaborationGroup cg : newGroups)
		{
			cGroupMap.put(cg.Name, cg);
		}
		//add members to group
		for(IDNSC_Relationship__c rel : relNameMap.values())
		{
			Map<Id, String> groupMemberMap = new Map<Id, String>();
			//groupMemberMap.put(rel.Role_Customer_Care__c, 'Standard'); don't include since this role is the group owner
			if(!groupMemberMap.keySet().contains(rel.Role_Customer_Service__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_Customer_Service__c)
			{
				groupMemberMap.put(rel.Role_Customer_Service__c, 'Standard');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_Data_Steward__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_Data_Steward__c)
			{
				groupMemberMap.put(rel.Role_Data_Steward__c, 'Admin');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_EDI_Support__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_EDI_Support__c)
			{
				groupMemberMap.put(rel.Role_EDI_Support__c, 'Standard');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_Pricing_Specialist__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_Pricing_Specialist__c)
			{
				groupMemberMap.put(rel.Role_Pricing_Specialist__c, 'Standard');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_RVP__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_RVP__c)
			{
				groupMemberMap.put(rel.Role_RVP__c, 'Standard');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_Rebates_Tracings_Analyst__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_Rebates_Tracings_Analyst__c)
			{
				groupMemberMap.put(rel.Role_Rebates_Tracings_Analyst__c, 'Standard');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_Sales_Analyst__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_Sales_Analyst__c)
			{
				groupMemberMap.put(rel.Role_Sales_Analyst__c, 'Standard');
			}
			if(!groupMemberMap.keySet().contains(rel.Role_Transportation_Analyst__c) && cGroupMap.get(rel.Name).OwnerId != rel.Role_Transportation_Analyst__c)
			{
				groupMemberMap.put(rel.Role_Transportation_Analyst__c, 'Standard');
			}
			addChatterGroupMembers(cGroupMap.get(rel.Name).Id, groupMemberMap);
		}
		IDNSC_Relationship_TriggerDispatcher.executedMethods.add('createChatterGroup');
	}
	
	public static void removeChatterGroupMembers(CollaborationGroup cg)
	{
		List<CollaborationGroupMember> members = new List<CollaborationGroupMember>([Select Id From CollaborationGroupMember Where CollaborationGroupId = :cg.Id And MemberId != :cg.OwnerId]);
		delete members;
	}
	
	public static void addChatterGroupMembers(String GroupId, Map<Id, String> groupMemberMap)
	{
		List<CollaborationGroupMember> memberList = new List<CollaborationGroupMember>();
		for(Id memberId : groupMemberMap.keySet())
		{
			CollaborationGroupMember cgm = new CollaborationGroupMember();
			cgm.CollaborationGroupId = GroupId;
			cgm.MemberId = memberId;
			cgm.CollaborationRole = groupMemberMap.get(memberId);
			//cgm.NotificationFrequency = 'D';
			memberList.add(cgm);
		}
		insert memberList;
	}
}