public class DATATYPES
{
	public static set<object> cast(set<Id> ids)
	{
		set<object> oids = new set<object>();
		for(Id id : ids)
			oids.add(id);
			
		return oids;	
	}
	
    public static Boolean isValidId (String input)
    {
        // http://developer.force.com/cookbook/recipe/validating-an-id
        return (input instanceOf Id) ? true : false ;
    }

    public static Integer boolToInt (Boolean v)
    {
        // false is 0, and true is 1
        return v == true ? CONSTANTS.intTRUE : CONSTANTS.intFALSE;
    }

	public static List<string> toLowercase(List<string> input)
	{
        System.debug('DATATYPES.toLowercase');
       	System.debug('			======> input: ' + input);
        //for(string value : new List<string>(input))
        //	System.debug('			======> value: ' + value);

    	List<string> lowercase;
    	if (input != null)
    	{
        	//System.debug('			======> input != null');
    		lowercase = new List<string>();
    		for(string value : input)
    		{
		        //System.debug('			======> value: ' + value);
    			lowercase.add(value.toLowercase());
    		}
    		
    	}
        System.debug('			======> lowercase: ' + lowercase);
        return lowercase;
	}
	
	public static Set<String> toLowercase (Set<String> input)
    {
    	return new Set<string>(toLowercase(new List<string>(input)));
    }

    //
    // Multi-Picklist Methods
    //
	public static Set<string> getMultiPicklistValueSet(string value)
	{
        DEBUG.write(new List<string>{ value });
		return getMultiPicklistValueSet(value, ';');
	}

    public static Set<String> getMultiPicklistValueSet(string value, string separator)
    {
        // Converts Multi-Select Picklist String into Set<String>
        Set<String> valuesSet = new Set<String>();
        if(!String.isEmpty(value))
        {
            String[] a = value.split(separator);
            for (String s : a)
            {
                String st = s.Trim();
                if (!String.isEmpty(st))
                    valuesSet.add(st);
            }
        }
        return valuesSet;
    }
    
    public static Set<String> getSetInnerJoin (Set<String> setOne, Set<String> setTwo)
    {
        // Compares two sets and returns a set containing the inner join results
        Set<String> innerJoin = new Set<String>();

        for (String item : setOne)
			if(setTwo.contains(item))
                innerJoin.add(item);

        return innerJoin;
    }

    // Parses values containing +All, -All, and +/- into a set of strings
    // NOTE: the sobject must include the Name field for this to work
    public static Set<string> GetValues(string property, Map<id, sObject> objs)
    {
        Set<string> target = new Set<string>();
        Set<string> properties = new Set<string>();

        if (property != null)
            properties.addAll(getMultiPicklistValueSet(property, CONSTANTS.CRLF));
            
        // Trim white spaces
        for (string s : properties)
            s = s.trim();
            
        if (objs != null)
        {
            if (property == null) properties.add('+All'); // A blank field means use them all
            
            for (string p : properties)
            {
                if (p == '-All')
                    target.clear();
                else if (p == '+All')
                {
                    for (sObject sobj : objs.values())
                    {
                    	// Make sure you query the Name field in your Map!
                        target.add((string)sobj.get('Name'));
                    }
                }
                else if (p.startsWith('-'))
                    target.remove(p.substring(1));
                else if (p.startsWith('+'))
                    target.add(p.substring(1));
                else
                    target.add(p);
            }
        }
        else
            target.addAll(properties);

        return target;
    }
    
    // Don't check to see if it's an Id or null, it will throw an
    // Exception if it can't cast
    // Handle the exception if you need to handle it not being an Id
    public static Id castToId(object obj)
    {
   		return Id.valueOf((string)obj);
    }
    
	public static set<object> castToObject(set<string> strs)
	{
		set<object> obs = new set<object>();
		for(string str : strs)
			obs.add(str);
		return obs;
	}

    public static set<string> castToString(set<object> objs)
    {
    	set<string> strs = new set<string>();
    	for(object obj : objs)
    		strs.add(string.valueOf(obj));
    		
    	return strs;
    }
    
    public static set<Id> castToId(set<object> objs)
    {
    	set<Id> ids = new set<Id>();
    	for(object obj : objs)
    		ids.add(castToId(obj));
    	return ids;
    }
    
    public static Map<Id, Set<Id>> castToId(Map<object, set<object>> source)
    {
    	Map<Id, set<Id>> target = new Map<Id, set<Id>>();
    	for(object key : source.keySet())
    		target.put(castToId(key), castToId(source.get(key)));
    		
    	return target;
    }
    
    public static set<object> castToObject(set<Id> ids)
    {
    	set<object> oids = new set<object>();
    	for(Id id : ids)
    		oids.add(id);
    		
    	return oids;
    }
}