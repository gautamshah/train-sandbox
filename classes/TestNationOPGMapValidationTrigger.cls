@isTest
private class TestNationOPGMapValidationTrigger {
	
	private static final integer recordCount = 200;
	
	static private List<Nation_OPG_Map__c> createTestData(){
		
		List <Nation_OPG_Map__c> listNationOPGMap = new List <Nation_OPG_Map__c>(); 
		
		for(Integer i=0;i<recordCount;i++){
        	Nation_OPG_Map__c testNationOPGMap = new Nation_OPG_Map__c();
        	testNationOPGMap.nation__c = 'TestNation'+i;
        	testNationOPGMap.Opportunity_Product_Group__c = 'TestOPG'+i;
        	listNationOPGMap.add(testNationOPGMap);
        }
        
        return listNationOPGMap;
	}
	
    static testMethod void testInsert() {
		
		List <Nation_OPG_Map__c> listNationOPGMapInsert = createTestData();
		
		Test.startTest();
		insert listNationOPGMapInsert;
		Test.stopTest();
    }
    
    static testMethod void testInsertError(){
    	
    	List <Nation_OPG_Map__c> listNationOPGMapInsert = createTestData();
		List <Nation_OPG_Map__c> listNationOPGMapInsert2 = createTestData();
			
		insert listNationOPGMapInsert;

		Test.startTest();
		try {
			insert listNationOPGMapInsert2;
		} catch (DMLException e){
     		system.debug('***[DMLException e]: ' + e);
    	}
	}

	static testMethod void testUpdate() {
		Set <Id> setIdInsert = new Set <Id>();
		List <Nation_OPG_Map__c> listNationOPGMapInsert = createTestData();
		List <Nation_OPG_Map__c> listNationOPGMapUpdate = new List <Nation_OPG_Map__c>();
		Integer testCount = recordCount + 1;
	
		Database.SaveResult[] srA = Database.insert(listNationOPGMapInsert);

        for(Integer i=0; i<srA.size(); i++){
            Database.SaveResult saveResult = srA[i];
            setIdInsert.add(saveResult.getId());
        }
        
    	for(Nation_OPG_Map__c nomUpdate : [Select Id, Nation__c, Opportunity_Product_Group__c from Nation_OPG_Map__c where Id in :setIdInsert]){
        	nomUpdate.Nation__c = 'TestNation' + testCount;
        	nomUpdate.Opportunity_Product_Group__c = 'TestOPG' + testCount;
        	listNationOPGMapUpdate.add(nomUpdate);
        }
    	
    	update(listNationOPGMapUpdate);
    	
	}

	static testMethod void testUpdateError() {

		Set <Id> setIdInsert = new Set <Id>();
		List <Nation_OPG_Map__c> listNationOPGMapInsert = createTestData();
		List <Nation_OPG_Map__c> listNationOPGMapUpdate = new List <Nation_OPG_Map__c>();
		
		Database.SaveResult[] srA = Database.insert(listNationOPGMapInsert);

        for(Integer i=0; i<srA.size(); i++){
            Database.SaveResult saveResult = srA[i];
            setIdInsert.add(saveResult.getId());
        }
        	
        for(Nation_OPG_Map__c nomUpdate : [Select Id, Nation__c, Opportunity_Product_Group__c from Nation_OPG_Map__c where Id in :setIdInsert]){
        	nomUpdate.Nation__c = 'TestNation1';
        	nomUpdate.Opportunity_Product_Group__c = 'TestOPG1';
        	listNationOPGMapUpdate.add(nomUpdate);
        }
        try {
        	update listNationOPGMapUpdate;
        } catch (Exception e){
        		system.debug('***[DMLException e]: ' + e);
        }
    }	
}