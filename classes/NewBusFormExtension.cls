public with sharing class NewBusFormExtension {
   
    private New_Business_Form__c newBusRecord;
    private Opportunity sfopp ;    
    public list<New_Business_Form__c> newBusRecs {get;set;}
    public boolean Reprocess;
   
    public NewBusFormExtension(ApexPages.StandardController controller) {
        this.newBusRecord = (New_Business_Form__c)controller.getRecord();
        sfopp = [Select id,Type,StageName, Reprocessing_Opportunity__c  from  Opportunity where id =: newBusRecord.Opportunity__c];
            System.debug('----sfopp ----' + sfopp );
            
        if (sfopp.Reprocessing_Opportunity__c == true)
            {
           Reprocess = true;
           }
           else {
           Reprocess = false;
           }
        newBusRecs = new List<New_Business_Form__c>(); //[SELECT Item_Number__c, Quantity__c WHERE Opportunity__c = newBusRecord.Opportunity__c]
        addRow();    
            System.debug('----newBusRecs----' + newBusRecs);
    }
    
    public void addRow(){
        newBusRecs.add(new New_Business_Form__c(opportunity__c = newBusRecord.Opportunity__c,
           Reprocessing__c = Reprocess));
    }   
    
    public PageReference saveRecords(){
        try{
            if((sfopp.Type == 'New Business' && sfopp.StageName == 'Closed Won') 
            || (sfopp.Type == 'At Risk' && sfopp.StageName== 'Closed Lost')
            || sfopp.Reprocessing_Opportunity__c == true)
            {
                upsert newBusRecs; 
            }
            else
            {
            //alert(); label.FileUploadError
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR, 'New Business Forms can only be created for opportunities that were won (New Business - Closed Won) and opportunities that were lost (At Risk - Closed Lost), or for opportunities marketed as "Reprocessing Opportunities". Please click on the Cancel button, update the opportunity stage, and then enter a New Business Form.');
                ApexPages.addMessage(errormsg);  
                return null;
            }

            
            
    }    
    catch(DmlException  e){
         ApexPages.addMessages(e);
         return null;
         }
    String retURL = Apexpages.currentPage().getParameters().get('retURL');
    if(Apexpages.currentPage().getParameters().get('retURL') == null || Apexpages.currentPage().getParameters().get('retURL') == '') {
        retURL = '/' + newBusRecord.Opportunity__c;
    }    
    PageReference pageRef = new PageReference(retUrl);
    return pageRef;
    }
        
    public void removeLastEmptyRecord(){
        Integer indexOfRecToRemove = newBusRecs.size() - 1;
            System.debug(newBusRecs.size() + '----indexOfRecToRemove----' + indexOfRecToRemove);
        New_Business_Form__c newBusRec = newBusRecs.get(indexOfRecToRemove);
        newBusRecs.remove(indexofRecToRemove);
     /* if((newBusRecs.Item_Number__c == null || newBusRecs.Item_Number__c == '' || newBusRecs.Item_Number__c.length() == 0 ) &&
        (newBusRecs.Quantity__c == null || newBusRecs.Quantity__c =='' || newBusRecs.Quantity__c.length() == 0)){
            newBusRecs.remove(indexOfRecToRemove);
         }      
    */    
    }  
    
    public pageReference cancel(){
        String retURL = Apexpages.currentPage().getParameters().get('retURL');
        if(Apexpages.currentPage().getParameters().get('retURL') == null || Apexpages.currentPage().getParameters().get('retURL') != ''){
            retURL = '/' + newBusRecord.Opportunity__c;
        }
        PageReference pageRef = new PageReference(retURL);
        return pageRef;
    }      
}