/****************************************************************************************
* Name    : ActivityMigratorBatch
* Author  : Gautam Shah
* Date    : 4/20/2016
* Purpose : Used to migrate custom Activity fields into the Activity_Detail__c object to free up those fields.
* 
* Dependancies: ActivityFieldMigrator
* Referenced By: 
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* 	DATE         	AUTHOR                  CHANGE
* 	----          	------                  ------
*	
* ========================
* = TO-DOS =
* ========================
*	
*****************************************************************************************/
public class ActivityMigratorBatch implements Database.Batchable<sObject> 
{
	private String query;
    private String objName;
    private Map<String,String> fieldMapping;
    private String indicatorField;
    private String indicatorValue;
	
	public ActivityMigratorBatch(String objName, String query, Map<String,String> fieldMapping, String indicatorField, String indicatorValue)
	{
        this.objName = objName;
        this.query = query;
        this.fieldMapping = fieldMapping;
        this.indicatorField = indicatorField;
        this.indicatorValue = indicatorValue;
        System.debug('query: ' + query);
	}
	public Database.Querylocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		System.Savepoint sp = Database.setSavepoint();
        try
        {
            if(objName == 'Event')
            {
                List<Event> events = new List<Event>();
               	events.addAll((List<Event>) scope);
                if(ActivityFieldMigrator.migrateEventValues(events, fieldMapping))
                {
                    for(Event e : events)
                    {
                        e.put(indicatorField, indicatorValue);
                    }
                    update events;
                }
                else
                {
                    Database.rollback(sp);
                }
            }
            else if(objName == 'Task')
            {
                List<Task> tasks = new List<Task>();
               	tasks.addAll((List<Task>) scope);
                if(ActivityFieldMigrator.migrateTaskValues(tasks, fieldMapping))
                {
                    for(Task t : tasks)
                    {
                        t.put(indicatorField, indicatorValue);
                    }
                    update tasks;
                }
                else
                {
                    Database.rollback(sp);
                }
            }
        }
        catch(Exception e)
        {
            Database.rollback(sp);
            System.debug(e.getMessage());
        }
	}
	public void finish(Database.BatchableContext BC){}
}