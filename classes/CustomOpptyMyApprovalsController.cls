/**
 *  Apttus Approvals Management
 *  CustomOpptyMyApprovalsController
 *   
 *  @2014-2015 Apttus Inc. All rights reserved.
 */
public with sharing class CustomOpptyMyApprovalsController extends CustomApprovalsConstants {

    // context id
    private ID ctxObjId = null;
    // context object type
    private String ctxObjType = null;
    // return to object id
    private ID returnId = null;
    // return to custom page
    private String returnPage = null;
    // salesforce1 mode indicator
    private Boolean inSf1Mode = false;
    // page initially loaded
    private Boolean pageLoaded = false;
    // page URL
    private String pageURL = null;
    // base controller
    private Apttus_Approval.MyApprovalsController baseController = null;
    
    /**
     * Class Constructor 
     * @param stdController the base myapprovals controller
     */
    public CustomOpptyMyApprovalsController(Apttus_Approval.MyApprovalsController baseController) {
        this();
        this.baseController = baseController;
    }

    /**
     * Class Constructor 
     */
    public CustomOpptyMyApprovalsController() {

        // context object id
        String ctxObjIdStr = ApexPages.currentPage().getParameters().get(PARAM_SOBJECTID);
        if (! CustomApprovalsUtil.nullOrEmpty(ctxObjIdStr)) {
            ctxObjId = String.escapeSingleQuotes(ctxObjIdStr);
        }
        
        // context object type
        String ctxObjTypeStr = ApexPages.currentPage().getParameters().get(PARAM_SOBJECTTYPE);
        if (! CustomApprovalsUtil.nullOrEmpty(ctxObjTypeStr)) {
            ctxObjType = String.escapeSingleQuotes(ctxObjTypeStr);
        }
        
        // return-to object id
        String returnIdStr = ApexPages.currentPage().getParameters().get(PARAM_RETURNID);
        if (! CustomApprovalsUtil.nullOrEmpty(returnIdStr)) {
            returnId = String.escapeSingleQuotes(returnIdStr);
        }
        
        // return-to custom page
        String returnPageStr = ApexPages.currentPage().getParameters().get(PARAM_RETURNPAGE);
        if (! CustomApprovalsUtil.nullOrEmpty(returnPageStr)) {
            returnPage = String.escapeSingleQuotes(returnPageStr);
        }

    }

    /**
     * Set the controller mode to Salesforce1 when assignTo is passed in actionFunction param
     * @param mode true if we are in Salesforce1 mode, false otherwise
     */
    public void setInSf1Mode(Boolean mode) {
        inSf1Mode = mode;
    }

    /**
     * Check if the controller is in Salesforce1 mode
     * @return true if we are in Salesforce1 mode, false otherwise
     */
    public Boolean getInSf1Mode() {
        return inSf1Mode;
    }

    /**
     * Test if the page has been initially loaded
     * @return true if the page has been initially loaded, false otherwise
     */
    public Boolean getPageLoaded() {  
        return pageLoaded;  
    }
    
    /**
     * Get the page URL
     * @return the page URL
     */
    public String getPageURL() {  
        return pageURL;  
    }
    
    /**
     * Load MyOpptyApprovals - launch base MyApprovals page or custom page
     * @return pageRef page reference to appropriate page
     */
    public PageReference doLoadMyOpptyApprovals() {
        
        // navigate to the correct page based on whether we are in Salesforce1 or not
        PageReference pageRef = (getInSf1Mode())
            // call base pages
            ? new PageReference(CustomApprovalsUtil.getPageUrl('Apttus_Approval__MyApprovalsSf1'))
            : new PageReference(CustomApprovalsUtil.getPageUrl('Apttus_Approval__MyApprovals'));
            
            // call custom pages (NOTE: may not be in sync with base)
            //? new PageReference(CustomApprovalsUtil.getPageUrl('CustomOpptyMyApprovalsSF1UI'))
            //: new PageReference(CustomApprovalsUtil.getPageUrl('CustomOpptyMyApprovalsWebUI'));

        pageRef.getParameters().put(PARAM_SOBJECTID, ctxObjId);
        pageRef.getParameters().put(PARAM_SOBJECTTYPE, ctxObjType);

        // return to id or custom page but not both, id takes preference        
        if (! CustomApprovalsUtil.nullOrEmpty(returnPage)) {
            pageRef.getParameters().put(PARAM_RETURNPAGE, returnPage);
        } else {
            pageRef.getParameters().put(PARAM_RETURNID, returnId);
        }

        // redirect to the target page
        if (getInSf1Mode()) {
            // mark the page as loaded
            pageLoaded = true;
            
            // stay on same page so we can use sforce1 redirection in the visualforce page
            if (pageRef != null) {
                pageURL = pageRef.getURL();
            }
            return null;
            
        } else {
            // navigate to the new page
            if (pageRef != null) {
                pageRef.setRedirect(true);
            }
            return pageRef;
            
        }

    }
    
    /**
     * Load my approvals
     * @return pageRef page reference to appropriate page
     */
    public PageReference doLoadMyApprovals() {
        // load approvals
        PageReference pageRef = baseController.doLoadApprovals();

        // redirect to the target page
        if (getInSf1Mode()) {
            // mark the page as loaded
            pageLoaded = true;
            
            // stay on same page so we can use sforce1 redirection in the visualforce page
            if (pageRef != null) {
                pageURL = pageRef.getURL();
            }
            return null;
            
        } else {
            // navigate to the new page
            if (pageRef != null) {
                pageRef.setRedirect(true);
            }
            return pageRef;
            
        }

    }

    /**
     * Return to previous page identified by return id or page name (id takes preference)
     * @return page reference to appropriate page
     */
    public PageReference doMyDone() {
        
        PageReference pageRef = null;
        if (returnId != null) {
            pageRef = new PageReference(CustomApprovalsUtil.getPageUrlForObjectId(returnId));
            
        } else if (! CustomApprovalsUtil.nullOrEmpty(returnPage)) {
            pageRef = new PageReference(CustomApprovalsUtil.getPageUrl(returnPage));
            
        } else {
            pageRef = new PageReference(CustomApprovalsUtil.getPageUrlForObjectId(ctxObjId));
        
        }

        // redirect to the target page
        if (getInSf1Mode()) {
            // mark the page as loaded
            pageLoaded = true;
            
            // stay on same page so we can use sforce1 redirection in the visualforce page
            pageURL = pageRef.getURL();
            return null;
            
        } else {
            // navigate to the new page
            pageRef.setRedirect(true);
            return pageRef;
            
        }

    }

}