/****************************************************************************************
* Name    : LeadConversionExtensionV2
* Author  : Gautam Shah
* Date    : Feb 2, 2016
* Purpose : Version 2: Alternative to standard lead conversion
* 
* Dependancies: 
*  CustomLeadConversionV2.vfp            
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          	AUTHOR                  CHANGE
* ----          	------                  ------
* Apr 20, 2017		Gautam Shah				Added line to copy Lead.Lead_Source_Most_Recent__c to Opportunity.LeadSourceMostRecent__c 
*****************************************************************************************/ 
public class LeadConversionExtensionV2 
{
	public Lead l {get;set;}
    public Account a {get;set;}
    public Contact c {get;set;}
    public Opportunity o {get;set;}
    public Boolean addOpp {get;set;}
    public List<Product> products {get;set;}
    public String OPG {get;set;}
    private transient Savepoint sp;
    public String errorMessage {get;set;}
    public List<Schema.FieldSetMember> accountFieldSet {get;set;}
    public List<Schema.FieldSetMember> contactFieldSet {get;set;}
    public List<Schema.FieldSetMember> opportunityFieldSet {get;set;}
    public String userLocale {get;set;}
    
    public LeadConversionExtensionV2(ApexPages.StandardController stdController) 
    {
        this.userLocale = UserInfo.getLocale();
        this.addOpp = false;
        String contactRT = 'Connected Contact US';
        if(!test.isRunningTest())
        {
            
            stdController.addFields(new List<String>{'Name', 'Account__c', 'Contact__c', 'Opportunity__c', 'OwnerId', 'RecordType.Name', 'FirstName', 'LastName', 'Phone', 'Email', 'Company', 'Street', 'City', 'State', 'PostalCode', 'Country', 'Department__c', 'Job_Role__c', 'Specialty__c', 'Opportunity_Product_Group__c', 'Campaign_Most_Recent__c','Title','Connected_As__c','CurrencyIsoCode','Description','Lead_Comments__c','Lead_Source_Most_Recent__c'});
            this.l = (Lead)stdController.getRecord();
            
            String Region = Utilities.getRegion(l.Country);
            String fieldSetAccount = 'LeadConversion_' + Region + '_NewAccount';
            String fieldSetContact = 'LeadConversion_' + Region + '_NewContact';
            String fieldSetOpportunity = 'LeadConversion_' + Region + '_NewOpportunity';
            this.accountFieldSet = SObjectType.Account.FieldSets.getMap().get(fieldSetAccount).getFields();
            this.contactFieldSet = SObjectType.Contact.FieldSets.getMap().get(fieldSetContact).getFields();
            this.opportunityFieldSet = SObjectType.Opportunity.FieldSets.getMap().get(fieldSetOpportunity).getFields();
            
            contactRT = 'Connected Contact ' + Region;
        }
        else
        {
            this.l = (Lead)stdController.getRecord();
        }          

        this.a = new Account();
        this.c = (Contact)Contact.sObjectType.newSObject(Utilities.recordTypeMap_Name_Id.get(contactRT), true);
        this.o = new Opportunity();
        
        //populate default values
        
        this.a.Name = l.Company;
        this.a.Phone = l.Phone;
        this.a.BillingStreet = l.Street;
        this.a.BillingCity = l.City;
        this.a.BillingState = l.State;
        this.a.BillingPostalCode = l.PostalCode;
        this.a.BillingCountry = l.Country;
        
        this.c.Salutation = l.Salutation;
        this.c.FirstName = l.FirstName;
        this.c.LastName = l.LastName;
        this.c.Phone_Number_at_Account__c = l.Phone;
        this.c.Email = l.Email;
        this.c.Department_picklist__c = l.Department__c;
		this.c.Affiliated_Role__c = l.Job_Role__c;
        this.c.Title = l.Title;
        
        if(String.isBlank(l.Opportunity_Product_Group__c))
        {
          l.Opportunity_Product_Group__c = '--None--';
        }
        this.OPG = l.Opportunity_Product_Group__c;
        fetchProducts();
    }
   
    public List<SelectOption> getOPGList()
    {
        String strCountryFilter = Utilities.getRegion(l.Country) + '%';
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--', '--None--'));
        for(Pricebook2 opg : [Select Id, Name From Pricebook2 Where IsDeleted = false And IsActive = true and Name like :strCountryFilter Order By Name])
        {
            options.add(new SelectOption(opg.Name, opg.Name));
        }
        return options;
    }
    
    @RemoteAction
    public static Response tryConvert(String leadJSON, String accountJSON, String contactJSON, String oppJSON, String prodJSON, Boolean addOpp)
    {
        System.debug('tryConvert called');
        Lead l = (Lead)JSON.deserialize(leadJSON, Lead.class);
        Account a = (Account)JSON.deserialize(accountJSON, Account.class);
        Contact c = (Contact)JSON.deserialize(contactJSON, Contact.class);
        Opportunity o = (Opportunity)JSON.deserialize(oppJSON, Opportunity.class);
        List<Product> prodArray = (List<Product>)JSON.deserialize(prodJSON, List<Product>.class);
        
        System.debug('Opp Revenue Start Date: ' + o.CloseDate);
        
        String Region = Utilities.getRegion(l.Country);
        
        DataQuality dq = new DataQuality();
        Map<String,Id> acctRTMap = dq.getRecordTypes('Account');
        
        //a.RecordTypeId = acctRTMap.get('User Submitted Healthcare Facility');
        a.Created_by_Lead_Conversion__c = true;
        a.CurrencyIsoCode = Utilities.getCurrency(l.Country);
        System.debug('Account Currency: ' + a.CurrencyIsoCode);
        
        String contactRT = 'Connected Contact ' + Region;
        System.debug('contactRT: ' + contactRT);
        c.RecordTypeId = Utilities.recordTypeMap_Name_Id.get(contactRT);
        c.Created_by_Lead_Conversion__c = true;
        System.debug('c.RecordTypeId: ' + c.RecordTypeId);
        
        String OPG = o.Pricebook2Id;
        o.Pricebook2Id = null;//because it was holding the OPG name, not ID
        System.debug('OPG: ' + OPG);
        o.RecordTypeId = Utilities.getOppRecordTypeIdByOPG(OPG);
        
        Savepoint sp;
        try
        {
            sp = Database.setSavepoint();
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(l.Id);
            System.debug('l.Account__c: ' + l.Account__c);
            if(!String.isBlank(l.Account__c))
            {
                System.debug('choosing existing account');
                lc.setAccountId(l.Account__c);
            }   
            else 
            {
                System.debug('creating new account');
                insert a;
                lc.setAccountId(a.Id); 
            }
            System.debug('l.Contact__c: ' + l.Contact__c);
            if(!String.isBlank(l.Contact__c))
            {
                System.debug('choosing existing contact');
                lc.setContactId(l.Contact__c);
            }   
            else 
            {
                System.debug('creating new contact');
                
                if(!String.isBlank(l.Account__c))
                {
                    c.AccountId = l.Account__c;
                }
                else
                {
                    c.AccountId = a.Id;
                }
                insert c;
                lc.setContactId(c.Id); 
            }
            lc.setOverwriteLeadSource(false);
            lc.setSendNotificationEmail(false);
            lc.setConvertedStatus('Qualified');
            
            System.debug('addOpp: ' + addOpp);
            System.debug('l.Opportunity__c: ' + l.Opportunity__c);
            Boolean createNewOpp = false;
            if(addOpp && l.Opportunity__c == null)
            {
                createNewOpp = true;
            }
            lc.setDoNotCreateOpportunity(!createNewOpp);
            if(createNewOpp)
            {
                lc.setOpportunityName(o.Name);
            }
            
            List<Note> lNotes = new List<Note>([Select Id, ParentId, Title, IsPrivate, Body, OwnerId From Note Where ParentId = :l.Id]);
            List<Attachment> lAttachments = new List<Attachment>([Select Id, ParentId, Name, IsPrivate, ContentType, BodyLength, Body, OwnerId, Description From Attachment Where ParentId = :l.Id]);
            System.debug('# of Notes: ' + lNotes.size());
            System.debug('# of Attachments: ' + lAttachments.size());
            
            Database.LeadConvertResult lcResult = database.convertLead(lc);
            System.debug('LeadConvertResult: ' + lcResult);
            
            //Opportunity
            List<Opportunity> opp = new List<Opportunity>();
            if(createNewOpp)
            {
                List<Schema.FieldSetMember> opportunityFieldSet = SObjectType.Opportunity.FieldSets.getMap().get('LeadConversion_' + Region + '_NewOpportunity').getFields();
                
                String queryOppFields = 'Select Id,CurrencyIsoCode,Description,Lead_Comments__c,RecordTypeId,Created_by_Lead_Conversion__c,LeadSourceMostRecent__c';
                for(Schema.FieldSetMember fsm : opportunityFieldSet)
                {
                    queryOppFields += ',' + fsm.FieldPath;
                }
                queryOppFields += ' From Opportunity Where Id = \'' + lcResult.getOpportunityId() + '\' Limit 1';
                System.debug('queryOppFields: ' + queryOppFields);
                
                opp = Database.query(queryOppFields);
                if(!opp.isEmpty())
                {
                    for(Schema.FieldSetMember fsm : opportunityFieldSet)
                    {
                        opp[0].put(fsm.FieldPath, o.get(fsm.FieldPath));
                    }
                    opp[0].Description = l.Description;
                    opp[0].Lead_Comments__c = l.Lead_Comments__c;
                    opp[0].RecordTypeId = o.RecordTypeID;
                    opp[0].Created_by_Lead_Conversion__c = true;
                    opp[0].LeadSourceMostRecent__c = l.Lead_Source_Most_Recent__c;
                    opp[0].CurrencyIsoCode = a.CurrencyIsoCode;
                    update opp[0];
                }
                // Update Opportunity Contact Role
                List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>([Select Id, Role, isPrimary From OpportunityContactRole Where OpportunityId = :lcResult.getOpportunityId() And ContactId = :lcResult.getContactId() Limit 1]);
                if(ocrList.size() > 0)
                {
                    //new opp so make contact role primary
                    ocrList[0].Role = 'Decision Maker';
                    ocrList[0].isPrimary = true;
                    update ocrList[0];
                }
                // Add OpportunityLineItem Records     
                List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();           
                for(Product p : prodArray)
                {
                    if(p.Quantity > 0 && p.UnitPrice >= 0)
                    {
                        OpportunityLineItem oli = new OpportunityLineItem();
                        //oli.CurrencyIsoCode = CurrencyCode;
                        oli.Description = p.Description;
                        oli.OpportunityId = lcResult.getOpportunityId();
                        oli.PricebookEntryId = p.Id;
                        oli.Quantity = p.Quantity;
                        oli.UnitPrice = p.UnitPrice;
                        oliList.add(oli);
                    }
                }
                insert oliList;
            }
            else if(addOpp && !String.isBlank(l.Opportunity__c))
            {
                opp = [Select Id, CampaignId, Description, Lead_Comments__c From Opportunity Where Id = :l.Opportunity__c Limit 1];
                if(!opp.isEmpty())
                {
                    opp[0].CampaignId = l.Campaign_Most_Recent__c;
                    opp[0].Description += '\n' + l.Description;
                    opp[0].Lead_Comments__c += '\n' + l.Lead_Comments__c;
                    update opp[0];
                }
                
                List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>([Select Id, Role, isPrimary From OpportunityContactRole Where OpportunityId = :l.Opportunity__c And ContactId = :lcResult.getContactId() Limit 1]);
                if(ocrList.isEmpty())
                {
                    //create new OCR but since it's existing Opp don't make OCR primary
                    OpportunityContactRole ocr = new OpportunityContactRole();
                    ocr.ContactId = lcResult.getContactId();
                    ocr.OpportunityId = l.Opportunity__c;
                    ocr.Role = 'Decision Maker';
                    System.debug('inserting OCR');
                    insert ocr;
                }
                else
                {
                    //Contact Role exists, make sure they have a role, don't make primary
                    if(String.isBlank(ocrList[0].Role))
                    {
                        ocrList[0].Role = 'Decision Maker';
                        update ocrList[0];
                    }
                }
            }
            
            //Copy Notes from the Lead to the new Opportunity - Contact is done automatically
            List<Note> clonedNotes = new List<Note>();
            for(Note n : lNotes)
            {
                Note oNote = n.clone(false,true,false,false);
                if(createNewOpp)
                {
                    oNote.ParentId = lcResult.getOpportunityId();
                }
                else if(addOpp && !String.isBlank(l.Opportunity__c))
                {
                    oNote.ParentId = l.Opportunity__c;
                }
                clonedNotes.add(oNote);
            }
            System.debug('# of clonedNotes: ' + clonedNotes.size());
            insert clonedNotes;
            
            //Copy Attachments from the Lead to the new Opportunity - Contact is done automatically
            List<Attachment> clonedAttachments = new List<Attachment>();
            for(Attachment at : lAttachments)
            {
                //Attachment cAttach = at.clone(false,true,false,false);
                Attachment oAttach = at.clone(false,true,false,false);
                //cAttach.ParentId = lcResult.getContactId();
                if(createNewOpp)
                {
                    oAttach.ParentId = lcResult.getOpportunityId();
                }
                else if(addOpp && !String.isBlank(l.Opportunity__c))
                {
                    oAttach.ParentId = l.Opportunity__c;
                }
                clonedAttachments.add(oAttach);
            }
            System.debug('# of clonedAttachments: ' + clonedAttachments.size());
            insert clonedAttachments;
            
            String destID = '';
            if(!opp.isEmpty())
            {
                destID = opp[0].Id;
            }
            else
            {
                destID = lcResult.getContactId();
            }
            Response r = new Response();
            r.forwardTo = '/' + destID; 
            r.success = true;
            return r;
        }
        catch(Exception e)
        {
            System.debug('caught problem');
            String errorMessage = 'Sorry, there was an error. The lead was not converted. Please notify support. \n' + e.getMessage() + ' Line #: ' + e.getLineNumber();
            Database.rollback(sp);
            Response r = new Response();
            r.success = false;
            r.message = errorMessage;
            r.forwardTo = null;
            return r;
        }
    }
   
    public void fetchProducts()
  	{
        products = new List<Product>();
        System.debug('fetchProducts: Account Country: ' + a.BillingCountry);
        System.debug('fetchProducts: Lead Country: ' + l.Country);
        if(!String.isBlank(OPG))
        {
            //fetch products based on Account currency
            String AcctCurrency = fetchAcctCurrency();
            List<PricebookEntry> prodList = new List<PricebookEntry>([Select Id, Name, UnitPrice From PricebookEntry Where IsDeleted = false And isActive = true And Pricebook2.Name = :OPG And CurrencyIsoCode = :AcctCurrency Order By Name]);
            for(PricebookEntry prod : prodList)
            {
              Product p = new Product(prod.Id, prod.Name, 0, prod.UnitPrice, '');
              products.add(p);
            }
        }
    }
    
    private String fetchAcctCurrency()
    {
        String AcctCurrency = 'USD';
        if(!String.isBlank(l.Account__c))
        {
            Account selectedAcct = [Select CurrencyIsoCode From Account Where Id = :l.Account__c Limit 1];
            System.debug('selectedAcct.CurrencyIsoCode: ' + selectedAcct.CurrencyIsoCode);
            if(!String.isBlank(selectedAcct.CurrencyIsoCode))
            {
                AcctCurrency = selectedAcct.CurrencyIsoCode;
            }
        }
        else
        {
            //get currency from a lookup based on the country of the account to be created
            AcctCurrency = Utilities.getCurrency(l.Country);
        }
        System.debug('AcctCurrency: ' + AcctCurrency);
        return AcctCurrency;
    }
    
    public class Product
    {
        public String Id {get;set;}
        public String Name {get;set;}
        public Integer Quantity {get;set;}
        public Decimal UnitPrice {get;set;}
        public String Description {get;set;}
        
        public Product(String i, String n, Integer q, Decimal u, String d)
        {
            this.Id = i;
            this.Name = n;
            this.Quantity = q;
            this.UnitPrice = u;
            this.Description = d;
        }
    }
    
    public class Response
    {
        public String message {get;set;}
        public String forwardTo {get;set;}
        public Boolean success {get;set;}
    }
}