/****************************************************************************************
 * Name    : CoachingCallPlanTriggerTestClass
 * Author  : Chen Li
 * Date    : 04/July/2016 
 * Apex Involved: CoachingCallPlanTrigger
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 * 
 *****************************************************************************************/
 
@isTest
public class CoachingCallPlanTriggerTestClass{
  static testMethod void CoachingCallPlanTriggerTestClass(){
  //create user and its manager, create new Coaching_Call_Plan__c , edit the User_s_Manager_Id__c  to another one
   System.debug('---------CoachingCallPlanTriggerTestClass start--------');
    List<User> userList1 = new List<User>();//manager list
    List<User> userList2 = new List<User>();//sales rep list
    Id pId = [select Id from Profile where name = 'Asia - TH' limit 10].Id;
  
   try{
   
     User u1mgr = new User();
         u1mgr.LastName = 'Manager Test';
         u1mgr.Business_Unit__c = 'The Unit';
         u1mgr.Franchise__c = 'Burger King';
         u1mgr.email = 'testccp1@covidien.com';
         u1mgr.alias = 'testccp1';
         u1mgr.username = 'testccp1@covidien.com';
         u1mgr.communityNickName = 'testccp1@covidien.com';
         u1mgr.ProfileId = pId;
         u1mgr.CurrencyIsoCode='THB'; 
         u1mgr.EmailEncodingKey='ISO-8859-1';
         u1mgr.TimeZoneSidKey='America/New_York';
         u1mgr.LanguageLocaleKey='en_US';
         u1mgr.LocaleSidKey ='en_US';
         u1mgr.Country = 'TH';
         userList1.add(u1mgr);
         
         User u3mgr = new User();
         u3mgr.LastName = 'Manager Test';
         u3mgr.Business_Unit__c = 'The Unit';
         u3mgr.Franchise__c = 'Burger King';
         u3mgr.email = 'testccp2@covidien.com';
         u3mgr.alias = 'testccp2';
         u3mgr.username = 'testccp2@covidien.com';
         u3mgr.communityNickName = 'testccp2@covidien.com';
         u3mgr.ProfileId = pId;
         u3mgr.CurrencyIsoCode='THB'; 
         u3mgr.EmailEncodingKey='ISO-8859-1';
         u3mgr.TimeZoneSidKey='America/New_York';
         u3mgr.LanguageLocaleKey='en_US';
         u3mgr.LocaleSidKey ='en_US';
         u3mgr.Country = 'TH';
         userList1.add(u3mgr);
         
         System.debug('------ userList1'+userList1);
         insert userList1;
         
         User u1 = new User();
         u1.LastName = 'staff Test';
         u1.Business_Unit__c = 'The Unit';
         u1.Franchise__c = 'Burger King';
         u1.email = 'testccp3@covidien.com';
         u1.alias = 'testccp3';
         u1.username = 'testccp3@covidien.com';
         u1.communityNickName = 'testccp3@covidien.com';
         u1.ProfileId = pId;
         u1.CurrencyIsoCode='THB'; 
         u1.EmailEncodingKey='ISO-8859-1';
         u1.TimeZoneSidKey='America/New_York';
         u1.LanguageLocaleKey='en_US';
         u1.LocaleSidKey ='en_US';
         u1.Country = 'TH';
         u1.ManagerId = u1mgr.id;
         userList2.add(u1);
        
         User u3 = new User();
         u3.LastName = 'staff Test 3';
         u3.Business_Unit__c = 'The Unit';
         u3.Franchise__c = 'Burger King';
         u3.email = 'testccp4@covidien.com';
         u3.alias = 'testccp4';
         u3.username = 'testccp4@covidien.com';
         u3.communityNickName = 'testccp4@covidien.com';
         u3.ProfileId = pId;
         u3.CurrencyIsoCode='THB'; 
         u3.EmailEncodingKey='ISO-8859-1';
         u3.TimeZoneSidKey='America/New_York';
         u3.LanguageLocaleKey='en_US';
         u3.LocaleSidKey ='en_US';
         u3.Country = 'TH';
         u3.ManagerId = u3mgr.id;
         userList2.add(u3);
         System.debug('------ userList2'+userList2);
         
         
         
         insert userList2;
        
        // insert CCP with user info
        
        Coaching_Call_Plan__c ccp1 =new Coaching_Call_Plan__c();
        ccp1.Sales_Rep__c=u1.id;
        ccp1.Coaching_Manager__c=u1mgr.id;
        string values = 'Clarified visit purpose/updated call card; Prepared selling tools; Product knowledge';
        ccp1.Objective_picklist__c=values;
        System.debug('------before insert ccp1'+ccp1);
        insert ccp1;
        System.debug('------after insert ccp1'+ccp1);
            
        System.debug('------before update ccp1'+ccp1);  
        ccp1.Sales_Rep__c=u3.id;
        ccp1.Coaching_Manager__c=u3mgr.id;
        update ccp1;
        System.debug('------after update ccp1'+ccp1);
            
        }
         catch (Exception e){
         System.debug('----------CoachingCallPlanTriggerTestClass: Getting Exception: ' + e + e.getmessage());
              }
        }
  
  }