global class PriceType{
        webservice String QuantityRange{get;set;}
        webservice Date EffectiveDate{get;set;}
        webservice Date ExpirationDate{get;set;}
        webservice Decimal UnitPrice{get;set;}
        webservice Decimal Markup{get;set;}
        webservice Decimal EndCustomerPrice{get;set;}
        webservice String ContractNumber{get;set;}
        webservice String ContractDesignator{get;set;}
        webservice String ContractDescription{get;set;}
        webservice Integer SequenceNumber{get;set;}
        webservice String PriceCurrency{get;set;}
        webservice String PriceError{get;set;}
        webservice String PriceErrorDescription{get;set;}
    }