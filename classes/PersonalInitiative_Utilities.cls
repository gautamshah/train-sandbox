public with sharing class PersonalInitiative_Utilities
{
	public void OnBeforeInsert(List<Personal_Initiative__c> piList)
	{
		RollupOpportunityValuesInsert(piList);	
	}
	
	public void OnBeforeUpdate(List<Personal_Initiative__c> piList)
	{
		RollupOpportunityValues(piList);
	}
	
	public void RollupOpportunityValuesInsert(List<Personal_Initiative__c> piList)
	{
		System.debug('Opportunities_Utilities.deletedOpps: ' + Opportunities_Utilities.deletedOpps);
		for(Personal_Initiative__c p : piList)
		{
			p.Amount_Roll_up__c = 0;
			for(Opportunity o: p.Opportunities__r)
			{
				if(!Opportunities_Utilities.deletedOpps.contains(O.Id))
					p.Amount_Roll_up__c += o.Amount;
			}
		}
	}
	
	public void RollupOpportunityValues(List<Personal_Initiative__c> piList)
	{
		Map<Id, Personal_Initiative__c> piToOppMap = new Map<Id, Personal_Initiative__c>();
		System.debug('In RollupOpportunityValues');
		for(Personal_Initiative__c p : piList)
		{
			piToOppMap.put(p.Id, p);
			p.Amount_Roll_up__c = 0;
		}
		System.debug('piToOppMap: ' + piToOppMap);
		for(Opportunity o : [select Id, Amount, Personal_Initiative__c from Opportunity where Personal_Initiative__c IN: piToOppMap.keySet()])
		{
			if(!Opportunities_Utilities.deletedOpps.contains(O.Id))
			{
				System.debug('Opp Record: ' + o);
				piToOppMap.get(o.Personal_Initiative__c).Amount_Roll_up__c += o.Amount == null ? 0 : o.Amount;
			}
		}
	}
	
	public static void UpdatePersonalInitiatives(Set<Id> personalInitiativeIds)
	{

		List<Personal_Initiative__c> piMap = [SELECT Id from Personal_Initiative__c WHERE Id IN: personalInitiativeIds];
		update piMap;
	}
}