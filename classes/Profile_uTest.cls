@isTest
public class Profile_uTest
{
    @isTest
    static void testFetchAll()
    {
    	Map<Id, Profile> results = Profile_u.fetch();
    	       system.assert(results != null);
    }
    
    @isTest
    static void fetchField()
    {
    	set<string> names = new Set<String>();
    	names.add('APTTUS - SSG US Account Executive');
    	Map<Object, Map<Id, Profile>> results = Profile_u.fetch(Profile.field.Name, names);
    	//System.debug('how many results? ' + results.size());
    	system.assert(results != null);
    }
}