public with sharing class cpqOfferDevelopmentAssignment_t
{
    // To prevent a method from executing a second time add the name of the method
    // to the 'executedMethods' set
    public static Set<String> executedMethods = new Set<String>();

    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<CPQ_SSG_OD_Assignments__c> newList,
            Map<Id, CPQ_SSG_OD_Assignments__c> newMap,
            List<CPQ_SSG_OD_Assignments__c> oldList,
            Map<Id, CPQ_SSG_OD_Assignments__c> oldMap,
            integer size
        )
    {
        cpqOfferDevelopmentAssignment_u.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            newList,
            newMap,
            oldList,
            oldMap,
            size);
    }
}