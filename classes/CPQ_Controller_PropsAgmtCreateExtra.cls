/**
Controller for CPQ_ProposalAgreementCreateExtra page

Apttus ProposalAgreementCreate only copys Proposal and line items to agreement.
Extends Apttus page to copy other info from proposal to agreement.

CHANGE HISTORY
===============================================================================
DATE            NAME                            DESC
2014-11/21      Yuli Fintescu           Created
===============================================================================
*/
public with sharing class CPQ_Controller_PropsAgmtCreateExtra
{
	private Type type = CPQ_Controller_PropsAgmtCreateExtra.class;

	public Apttus_Proposal__Proposal__c theRecord{get;set;}

	public String AgreementRecordTypeParm {get; private set;}
	private String LockedRecordTypeId;

	private Set<String> facilityFields;
	private Set<String> agfacilityFields;
	private List<Participating_Facility__c> proposalFacilities;

	private Set<String> addressFields;
	private Set<String> agaddressFields;
	private List<Participating_Facility_Address__c> proposalAddresses;

	private Set<String> distributorFields;
	private Set<String> agdistributorFields;
	private List<Proposal_Distributor__c> proposalDistributors;

	private Set<String> rebateFields;
	private Set<String> agrebateFields;
	private List<Rebate__c> proposalRebates;

	private Map<Schema.SObjectType,Map<String,RecordType>> mapSObjectRType;
	private Map<String,RecordType> mapProposalRType;
	private Map<String,RecordType> mapAgreementRType;
    private Map<String,Id> mapAgreementRebateRType;

    public Boolean showCloseButton {get; private set;}

    public CPQ_Controller_PropsAgmtCreateExtra(ApexPages.StandardController controller) {
        //gather record types from proposal and agreement objects
        mapSObjectRType = cpqDeal_RecordType_c.getRecordTypesBySObjectAndName();
        mapProposalRType = mapSObjectRType.get(Schema.Apttus_Proposal__Proposal__c.SObjectType);
		mapAgreementRType = mapSObjectRType.get(Schema.Apttus__APTS_Agreement__c.SObjectType);
        mapAgreementRebateRType = New Map<String,Id>();

		Map<Id, RecordType> rts = RecordType_u.fetch(Rebate_Agreement__c.class);
        for(RecordType R: rts.values())
        {
            mapAgreementRebateRType.Put(R.DeveloperName, R.Id);
        }

        //get source proposal record
        theRecord = (Apttus_Proposal__Proposal__c)controller.getRecord();
        if (theRecord.ID != null)
        	init();

        showCloseButton = ApexPages.hasMessages();
    }

    private void init()
    {
        system.debug(string.valueOf(this.type) + '.init(): START');

    	CPQ_PartnerWSStruct struct = CPQ_PartnerContractWSInvoke.getProposalByID(theRecord.ID);
    	System.Debug(string.valueOf(this.type) + ' *** struct ' + struct);

    	if (struct != null) {
	    	for (CPQ_PartnerWSStruct.PartnerGuideLine g : struct.complaints) {
	    		if (g.auditId == null) {
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, g.message));
	    		}
	    	}
    	}

    	system.debug(string.valueOf(this.type) + '.init() Messages: ' + ApexPages.getMessages());

        if (ApexPages.hasMessages())
        	return;

        Set<String> proposalFields = CPQ_Utilities.getCustomFieldNames(Apttus_Proposal__Proposal__c.SObjectType, false);
        String specialFields = 'OwnerID, RecordTypeId, RecordType.Name, RecordType.DeveloperName, ERP_Ship_to_Address__r.Name';
        String queryStr = CPQ_Utilities.buildObjectQuery(proposalFields, specialFields, 'ID = \'' + theRecord.ID + '\'', 'Apttus_Proposal__Proposal__c');
        List<Apttus_Proposal__Proposal__c> records = (List<Apttus_Proposal__Proposal__c>)Database.query(queryStr);
        theRecord = records[0];

        system.debug(string.valueOf(this.type) + '.theRecord: ' + theRecord);

        if (struct != null) {
        	theRecord.Apttus_Proposal__ExpectedStartDate__c = struct.effectiveDate;
        	theRecord.Apttus_Proposal__ExpectedEndDate__c = struct.expirationDate;
        }

    	String DevName = CPQ_Utilities.getRecordTypeDeveloperName(theRecord);
        //find the corresponding agreement record type. agreement record types match with proposal record types by name.
		AgreementRecordTypeParm = mapAgreementRType.get(DevName + '_AG').Id;
		if (AgreementRecordTypeParm == null) {
			AgreementRecordTypeParm = mapAgreementRType.get('Agreement').Id;
		}

		System.Debug(string.valueOf(this.type)  + ' *** AgreementRecordTypeParm ' + AgreementRecordTypeParm);

		//find the corresponding proposal locked record type. after the agreement is created the proposal is locked
		LockedRecordTypeId = mapProposalRType.get(DevName + '_Locked').Id;

        System.Debug(string.valueOf(this.type)  + ' *** LockedRecordTypeId ' + LockedRecordTypeId);

    	//query proposal facilities for copying
		facilityFields = CPQ_Utilities.getCustomFieldNames(Participating_Facility__c.SObjectType, false);
		agfacilityFields = CPQ_Utilities.getCustomFieldNames(Agreement_Participating_Facility__c.SObjectType, true);
        queryStr = CPQ_Utilities.buildObjectQuery(facilityFields, 'Account__r.Account_External_ID__c', 'Proposal__c = \'' + theRecord.ID + '\'', 'Participating_Facility__c');
        proposalFacilities = (List<Participating_Facility__c>)Database.query(queryStr);

        //query proposal facility addresses for copying
		addressFields = CPQ_Utilities.getCustomFieldNames(Participating_Facility_Address__c.SObjectType, false);
		agaddressFields = CPQ_Utilities.getCustomFieldNames(Agreement_Participating_Facility_Address__c.SObjectType, true);
        queryStr = CPQ_Utilities.buildObjectQuery(addressFields, null, 'Proposal__c = \'' + theRecord.ID + '\'', 'Participating_Facility_Address__c');
        proposalAddresses = (List<Participating_Facility_Address__c>)Database.query(queryStr);

    	//query proposal distributor for copying
		distributorFields = CPQ_Utilities.getCustomFieldNames(Proposal_Distributor__c.SObjectType, false);
		agdistributorFields = CPQ_Utilities.getCustomFieldNames(Agreement_Distributor__c.SObjectType, true);
        queryStr = CPQ_Utilities.buildObjectQuery(distributorFields, 'Account__r.Account_External_ID__c', 'Proposal__c = \'' + theRecord.ID + '\'', 'Proposal_Distributor__c');
        proposalDistributors = (List<Proposal_Distributor__c>)Database.query(queryStr);

    	//query proposal rebates for copying
		rebateFields = CPQ_Utilities.getCustomFieldNames(Rebate__c.SObjectType, false);
		agrebateFields = CPQ_Utilities.getCustomFieldNames(Rebate_Agreement__c.SObjectType, true);
        queryStr = CPQ_Utilities.buildObjectQuery(rebateFields, 'RecordType.DeveloperName', 'Related_Proposal__c = \'' + theRecord.ID + '\'', 'Rebate__c');
        proposalRebates = (List<Rebate__c>)Database.query(queryStr);

        system.debug(string.valueOf(this.type) + '.init(): FINISH');
    }

	//action poller periodically checks agreement table to see if Apttus ProposalAgreementCreate has created a new agreement.
	//Stop action poller if there is an error in the page.
	public Apttus__APTS_Agreement__c theAgreement { get; set; }
	public Boolean getStopPoller()
	{
		return ApexPages.hasMessages();
	}

	//action poller periodically checks agreement table
	//if Apttus ProposalAgreementCreate has created a new agreement, copy stuff from proposal to it.
	public PageReference doCreateAgreementExtra()
	{
		system.debug(string.valueOf(this.type) + ' *** doCreateAgreementExtra ');
		system.debug(string.valueOf(this.type) + '.theRecord: ' + theRecord);
    	system.debug(string.valueOf(this.type) + '.init() Messages: ' + ApexPages.getMessages());

        if (theRecord.ID == null || ApexPages.hasMessages())
			return null;

		List<Apttus__APTS_Agreement__c> records =
			[SELECT Id
			        ,CreatedDate
			        ,CreatedById
			        ,Apttus__Workflow_Trigger_Created_From_Clone__c
			        ,Apttus__Perpetual__c
			 FROM Apttus__APTS_Agreement__c
			 WHERE Apttus_QPComply__RelatedProposalId__c = :theRecord.Id AND
				   Apttus__Workflow_Trigger_Created_From_Clone__c = true AND
				   CreatedById = :cpqUser_c.CurrentUser.Id AND
				   CreatedDate >: System.now().addMinutes(-1)
			 ORDER BY CreatedDate DESC
			 LIMIT 1];

		System.Debug(string.valueOf(this.type) + ' *** records ' + records);

		if (!records.isEmpty())
		{
        	theAgreement = records[0];

        	//Apttus ProposalAgreementCreate set Apttus__Workflow_Trigger_Created_From_Clone__c to true to
        	//indicate that the agreement is in processing of being cloned from proposal.
        	//set it to false here to indicate that being cloned from proposal has been completed.
        	theAgreement.Apttus__Workflow_Trigger_Created_From_Clone__c = false;

        	//copy stuff from proposal
        	theAgreement.ERP_Ship_to_Address__c = theRecord.ERP_Ship_to_Address__r.Name;
			theAgreement.Apttus__Contract_Start_Date__c = theRecord.Apttus_Proposal__ExpectedStartDate__c;
			theAgreement.Apttus__Contract_End_Date__c = theRecord.Apttus_Proposal__ExpectedEndDate__c;
        	theAgreement.Proposal_Expiration_Date__c = theRecord.Apttus_Proposal__Proposal_Expiration_Date__c;
        	theAgreement.Presented_Date__c = theRecord.Apttus_Proposal__Presented_Date__c;
        	theAgreement.Sales_Tax_Percent__c = theRecord.Apttus_Proposal__Sales_Tax_Percent__c;

        	//cpqDeal_c.copy(theRecord, theAgreement, cpqDeal_c.RoyaltyFreeAgreement);

			if (theAgreement.Apttus__Contract_End_Date__c == null && theAgreement.Apttus__Term_Months__c == null)
				theAgreement.Apttus__Perpetual__c = true;

		    List<Agreement_Participating_Facility__c> facilities =
		    	new List<Agreement_Participating_Facility__c>();
		    List<Agreement_Participating_Facility_Address__c> addresses =
		    	new List<Agreement_Participating_Facility_Address__c>();
		    List<Agreement_Distributor__c> distributors =
		    	new List<Agreement_Distributor__c>();
			List<Rebate_Agreement__c> rebates = new List<Rebate_Agreement__c>();

		    //copy facilities
    		for (Participating_Facility__c f : proposalFacilities)
    		{
    			Agreement_Participating_Facility__c af =
    				new Agreement_Participating_Facility__c(Agreement__c = theAgreement.ID);

    			for (String fieldName : facilityFields)
    			{
					if (agfacilityFields.contains(fieldName))
					{
						af.put(fieldName, f.get(fieldName));
					}
				}

    			facilities.add(af);
    		}

		    //copy addresses
    		for (Participating_Facility_Address__c f : proposalAddresses)
			{
    			Agreement_Participating_Facility_Address__c af =
    				new Agreement_Participating_Facility_Address__c(Agreement__c = theAgreement.ID);

    			for (String fieldName : addressFields)
    			{
					if (agaddressFields.contains(fieldName))
					{
						af.put(fieldName, f.get(fieldName));
					}
				}

    			addresses.add(af);
    		}

    		//copy distributors
    		for (Proposal_Distributor__c d : proposalDistributors)
    		{
    			Agreement_Distributor__c ad = new Agreement_Distributor__c(Agreement__c = theAgreement.ID);

    			for (String fieldName : distributorFields)
    			{
					if (agdistributorFields.contains(fieldName))
					{
						ad.put(fieldName, d.get(fieldName));
					}
				}

    			distributors.add(ad);
    		}

			//copy rebates
            for (Rebate__c r : proposalRebates)
            {
            	Rebate_Agreement__c ar = new Rebate_Agreement__c(Related_Agreement__c = theAgreement.ID);

            	String RRTID = mapAgreementRebateRType.get(r.RecordType.DeveloperName);
            	if (!String.isEmpty(RRTID))
            	{
            		ar.RecordTypeID = RRTID;
            	}

            	for (String fieldName : rebateFields)
            	{
					if (agrebateFields.contains(fieldName))
					{
						ar.put(fieldName, r.get(fieldName));
					}
				}

				rebates.add(ar);
            }

			//update records
		    try {
			    update theAgreement;
		    	insert facilities;
		    	insert addresses;
		    	insert distributors;
		    	insert rebates;

			    //if (facilities.size() > 0)
			    //{
			    //	insert facilities;
			    //}

			    //if (addresses.size() > 0)
			    //{
			    //	insert addresses;
			    //}

			    //if (distributors.size() > 0)
			    //{
			    //	insert distributors;
			    //}

			    //if (rebates.size() > 0)
			    //{
			    //	insert rebates;
			    //}

			    theRecord.Apttus_Proposal__Approval_Stage__c = 'Submitted to Contracting';
			    if (!String.isEmpty(LockedRecordTypeId))
			    {
			    	theRecord.RecordTypeId = LockedRecordTypeId;
			    }
		    	update theRecord;

		    }
		    catch (DMLException e)
		    {
		    	for (Integer i = 0; i < e.getNumDml(); i++)
		    	{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDMLMessage(i)));
	    		}
		    }
        }

		return null;
	}

}