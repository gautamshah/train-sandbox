/*
Test class

CPQ_Controller_PropsAgmtCreateExtra

CHANGE HISTORY
===============================================================================
DATE         NAME            DESC
2015-01-26   Yuli Fintescu   Created
10/31/2016   Paul Berglund   Remediated with CPQ, updated to use cpqAgreement_t
                             and cpqProposal_t
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
private class Test_CPQ_Controller_PropsAgmtCreateExtra {

    static List<Apttus_Proposal__Proposal__c> testProposals;

    static void createTestData()
    {
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        Schema.RecordTypeInfo gpoRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-GPO-IDN');
        Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

        Account testGPO = new Account(
            Name = 'Test GPO',
            Status__c = 'Active',
            Account_External_ID__c = 'USGG-SG0150',
            RecordTypeID = gpoRT.getRecordTypeId());
        insert testGPO;

        List<Account> testAccounts = new List<Account> {
            new Account(
                Status__c = 'Active',
                Name = 'Test Account',
                Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,
                RecordTypeID = acctRT.getRecordTypeId(),
                Account_External_ID__c = 'US-111111'),
            new Account(
                Status__c = 'Active',
                Name = 'Facilitiy',
                Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,
                RecordTypeID = acctRT.getRecordTypeId(),
                Account_External_ID__c = 'US-222222'),
            new Account(
                Status__c = 'Active',
                Name = 'Distributor',
                Primary_GPO_Buying_Group_Affiliation__c = testGPO.ID,
                RecordTypeID = acctRT.getRecordTypeId(),
                Account_External_ID__c = 'US-333333')
        };
        insert testAccounts;

        Opportunity testOpp = new Opportunity(
            Name = 'Test Opp',
            AccountID = testAccounts[0].ID,
            StageName = 'Identify',
            CloseDate = System.today());
        insert testOpp;

        List<ERP_Account__c> testERPs = new List<ERP_Account__c> {
            new ERP_Account__c(
                Name = 'S',
                Parent_Sell_To_Account__c = testAccounts[0].ID,
                ERP_Account_Type__c = 'S',
                ERP_Source__c = 'E1',
                ERP_Account_Status__c = 'Active'),
            new ERP_Account__c(
                Name = 'S',
                Parent_Sell_To_Account__c = testAccounts[1].ID,
                ERP_Account_Type__c = 'S',
                ERP_Source__c = 'E1',
                ERP_Account_Status__c = 'Active')
        };
        insert testERPs;


        Map<Schema.SObjectType,Map<String,RecordType>> mapSObjectRType = cpqDeal_RecordType_c.getRecordTypesBySObjectAndName();
        Map<String,RecordType> mapProposalRType = mapSObjectRType.get(Schema.Apttus_Proposal__Proposal__c.SObjectType);
        Map<String,RecordType> mapAgreementRType = mapSObjectRType.get(Schema.Apttus__APTS_Agreement__c.SObjectType);
        Map<String,Id> mapRebateRType = New Map<String,Id>();

        for(RecordType R: RecordType_u.fetch(Rebate__c.class).values()) {
            mapRebateRType.Put(R.DeveloperName, R.Id);
        }

        /////cpqProposal_t.executedMethods.add('capture');
        testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(
                Apttus_Proposal__Proposal_Name__c = 'Test No Account',
                RecordTypeID = mapProposalRType.get('Locally_Negotiated_Agreement_LNA').Id),
            new Apttus_Proposal__Proposal__c(
                Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                RecordTypeID = mapProposalRType.get('Locally_Negotiated_Agreement_LNA').Id,
                Apttus_Proposal__Account__c = testAccounts[0].ID,
                Apttus_Proposal__Opportunity__c = testOpp.ID,
                Deal_Type__c = 'New Deal')
        };
        insert testProposals;

        Participating_Facility__c testFacility = new Participating_Facility__c(
            Proposal__c = testProposals[1].ID, Account__c = testAccounts[1].ID);
        insert testFacility;

        Proposal_Distributor__c testDistributor = new Proposal_Distributor__c(
            Proposal__c = testProposals[1].ID, Account__c = testAccounts[2].ID);
        insert testDistributor;

        Participating_Facility_Address__c testAddress = new Participating_Facility_Address__c(
            Proposal__c = testProposals[1].ID, ERP_Record__c = testERPs[0].ID);
        insert testAddress;

        Rebate__c testRebate = new Rebate__c(
            Related_Proposal__c = testProposals[1].ID, RecordTypeID = mapRebateRType.get('Early_Signing_Incentive'));
        insert testRebate;

        /////cpqAgreement_t.executedMethods.add('capture');
        Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(
            Apttus__Workflow_Trigger_Created_From_Clone__c = true,
            Name = 'Test Proposal',
            Apttus_QPComply__RelatedProposalId__c = testProposals[1].ID,
            RecordTypeID = mapAgreementRType.get('Locally_Negotiated_Agreement_LNA_AG').Id);
        insert testAgreement;

        Apttus_Config2__ProductConfiguration__c testConfig = new Apttus_Config2__ProductConfiguration__c(
            Apttus_QPConfig__Proposald__c = testProposals[1].ID,
            Apttus_Config2__PriceListId__c = testPriceList.ID,
            Apttus_Config2__Status__c = 'Finalized',
            Apttus_Config2__VersionNumber__c = 1);
        insert testConfig;

        Product2 testProduct = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.RMS, 'ITEM', 1);
        insert testProduct;

        Apttus_Config2__PriceListItem__c testPriceListItem = cpqPriceListItem_u.fetch()[0];

        Apttus_Config2__LineItem__c testLine = new Apttus_Config2__LineItem__c(
            Apttus_Config2__IsPrimaryLine__c = true,
            Apttus_Config2__ConfigurationId__c = testConfig.ID,
            Apttus_Config2__ProductId__c = testProduct.ID,
            Apttus_Config2__PriceListItemId__c = testPriceListItem.ID,
            Apttus_Config2__ItemSequence__c = 1,
            Apttus_Config2__LineNumber__c = 1,
            Apttus_Config2__PrimaryLineNumber__c = 1,
            Apttus_Config2__ChargeType__c = 'Hardware',
            Apttus_Config2__PriceType__c = 'One Time',
            Apttus_Config2__Quantity__c = 1,
            Apttus_Config2__NetPrice__c = 10,
            Apttus_Config2__Uom__c = 'Case');
        insert testLine;
    }

    static testMethod void NegativeTest() {
        createTestData();

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalAgreementCreateExtra;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_PropsAgmtCreateExtra p = new CPQ_Controller_PropsAgmtCreateExtra(new ApexPages.StandardController(testProposals[0]));

            p.doCreateAgreementExtra();

            System.assertNotEquals(0, ApexPages.getMessages().size());
            // ApexPages.Message["Proposal must have an account."],
            // ApexPages.Message["Account Customer Reporting Number is invalid."],
            // ApexPages.Message["Proposal must have an opportunity."],
            // ApexPages.Message["Add Products by clicking on "Configure Products" within the Deal Configuration section below."]

            System.Debug(p.showCloseButton);
        Test.stopTest();
    }

    static testMethod void PositiveTest() {
        createTestData();

        Test.startTest();
            PageReference pageRef = Page.CPQ_ProposalAgreementCreateExtra;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_PropsAgmtCreateExtra p = new CPQ_Controller_PropsAgmtCreateExtra(new ApexPages.StandardController(testProposals[1]));

            p.getStopPoller();

            p.doCreateAgreementExtra();

            System.assertEquals(0, ApexPages.getMessages().size());

        Test.stopTest();
    }
}