/****************************************************************************************
 * Name    : CPQ_SSG_Controller_CreateDeal_VF
 * Author  : Isaac Lewis
 * Date    : 04-25-2016
 * Purpose : Centralized logic for all SSG "New <Deal_Type>" buttons
 * See		 : CPQ_SSG_Controller_CreateDeal, Test_CPQ_SSG_CreateDeal
 * Dependencies: ApexPages: [CPQ_SSG_CreateDeal], Buttons: [Account: [Create_Scrub_PO, Create_Smart_Cart_ACCT], Opportunity: [Create_Scrub_PO_OPPY, Create_Smart_Cart_OPPY, Custom_Kit]]
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/

public with sharing class CPQ_SSG_Controller_CreateDeal_VF {

	private final Id parentId;
	private final String recordSObjectDevName;
	private final RecordType recordType;

	/**
	 * Constructor for CPQ_SSG_Controller_CreateDeal_VF. Requires URL parameters to initialize correctly.
	 *
	 * @param			parentId 						record Id for parent sObject (Account, Opportunity)
	 * @param			sObjectDevName 			sObject name for new record (Apttus_Proposal__Proposal__c, Apttus__APTS_Agreement__c)
	 * @param			recordTypeDevName 	recordType name for new record
	 */
	public CPQ_SSG_Controller_CreateDeal_VF() {

		// Get parent sObject details
		parentId = Id.valueOf(ApexPages.currentPage().getParameters().get('parentId'));
		System.debug('parentId: ' + parentId);

		// Get deal sObject details (Apttus_Proposal__Proposal__c or Apttus__APTS_Agreement__c)
		recordSObjectDevName = ApexPages.currentPage().getParameters().get('sObjectDevName').replace('\'','');
		System.debug('recordSObjectDevName: ' + recordSObjectDevName);

		// Get deal RecordType details
		String recordTypeDevName = ApexPages.currentPage().getParameters().get('recordTypeDevName').replace('\'','');
		System.debug('recordTypeDevName: ' + recordTypeDevName);
		System.debug('recordTypeDevName Length: ' + recordTypeDevName.length());
		recordType = RecordType_u.fetch(sObject_u.getSObjectType(recordSObjectDevName), recordTypeDevName);
		System.debug('recordType: ' + recordType);

	}

	/**
	 * Action to create record, present edit page, and delete record on cancel
	 *
	 * @return		PageReference		Edit page of newly created record with cancel URL to delete record
	 * @see				CPQ_SSG_Controller_CreateDeal.createRecord
	 */
	public PageReference generateDeal(){

		sObject deal;
		PageReference dealPage;
		String cancelURL;

		if (parentId != NULL && recordType != NULL && recordSObjectDevName != NULL) {

				// Create new deal and set initial data
				deal = CPQ_SSG_Controller_CreateDeal.createRecord(recordSObjectDevName,recordType.Id,parentId);

				// Open deal record on save
				dealPage = new ApexPages.StandardController(deal).edit();
				dealPage.getParameters().put('retURL','/' + deal.Id);

				// On cancel, delete deal record and return to parent record
				cancelURL = '/apex/apttus_proposal__cancelactioninterceptor?actionName=create_oppty_proposal&objectId=' + parentId;
				if( Account.sObjectType == parentId.getSObjectType() ) {
					cancelURL += '&accountId=' + parentId;
				} else if( Opportunity.sObjectType == parentId.getSObjectType() ) {
					cancelURL += '&opportunityId=' + parentId;
				}
				cancelURL += '&agreementId=' + deal.id;
				cancelURL += '&rollbackId=' + deal.id;
				dealPage.getParameters().put('cancelURL','/' + cancelURL );

				return dealPage;
		}
		return null;
	}  // generateDeal

}