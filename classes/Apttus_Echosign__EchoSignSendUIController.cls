/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class EchoSignSendUIController extends Apttus_Echosign.ControllerConstants {
    global List<Attachment> attachments {
        get;
        set;
    }
    global ApexPages.StandardController controller;
    global echosign_dev1__SIGN_Agreement__c esAgmt {
        get;
        set;
    }
    global Id firstAttachmentId {
        get;
    }
    global Id lastAttachmentId {
        get;
    }
    global Id moveDownId {
        get;
        set;
    }
    global Id moveUpId {
        get;
        set;
    }
    global EchoSignSendUIController() {

    }
    global EchoSignSendUIController(ApexPages.StandardController stdController) {

    }
    global System.PageReference doCancel() {
        return null;
    }
    global System.PageReference doLaunchESAgreementAdvanced() {
        return null;
    }
    global System.PageReference doLoad() {
        return null;
    }
    global System.PageReference doMoveDown() {
        return null;
    }
    global System.PageReference doMoveUp() {
        return null;
    }
    global System.PageReference doSend() {
        return null;
    }
}
