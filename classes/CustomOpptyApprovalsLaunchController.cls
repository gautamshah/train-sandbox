/**
 *  Apttus Approvals Management
 *  CustomOpptyApprovalsLaunchController
 *   
 *  @2014 Apttus Inc. All rights reserved.
 */
public with sharing class CustomOpptyApprovalsLaunchController extends CustomApprovalsConstants {

    // context id
    private ID ctxObjId = null;
    // opportunity object
    Apttus__APTS_Agreement__c agmt = null;
    // error indicator
    public Boolean hasErrors { public get; private set; }

    /**
     * Class Constructor
     * @param stdController the standard controller
     */
    public CustomOpptyApprovalsLaunchController(ApexPages.StandardController stdController) {
        // get context id
        ctxObjId = stdController.getId();
        agmt = (Apttus__APTS_Agreement__c)stdController.getRecord();
        agmt = [Select Id, RecordTypeId, Apttus__Status__c From Apttus__APTS_Agreement__c Where Id = :agmt.Id];
    }
    
    /**
     * Gets context object Id
     */
    public ID getCtxObjectId() {
        return ctxObjId;
    }
    
    /**
     * Launch approvals
     * @return pageRef page reference to appropriate page
     */
    public PageReference doLaunchApprovals() {
        // Require Smart Cart and Scrub PO deal types for surgical to have at least 1 attachment before submitting for approval
        if (agmt.RecordTypeId == CPQ_AgreementProcesses.getAgreementRecordTypesByNameMap().get(CPQ_AgreementProcesses.SMART_CART_RT).Id) {
            Apttus__APTS_Agreement__c agreementAttachment = [Select Id, (Select Id From Attachments) From Apttus__APTS_Agreement__c Where Id = :ctxObjId];
            List<Attachment> attachments = agreementAttachment.Attachments;
            if (attachments == null || attachments.size() < 3) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Smart Cart Orders cannot be submitted for approval without supporting documentation.  Please attach either a usage report or a copy of the stocking confirmation order in the notes and attachment section of the agreement.'));
            }
            if (agmt.Apttus__Status__c != 'Fully Signed') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Need to obtain customer signature before submitting for approvals.'));
            }
            if (ApexPages.hasMessages()) {
                return null;
            }
        }

        // Redirect to opportunity approvals page
        PageReference pageRef = Page.CustomOpptyApprovals;
        pageRef.getParameters().put(PARAM_AGRMT_ID, ctxObjId);
        pageRef.getParameters().put(PARAM_RETURN_BUTTON_LABEL, Label.Apttus_Approval.Return);
        pageRef.getParameters().put(PARAM_RETURNID, ctxObjId);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    /**
     * Return to opportunity
     */
    public PageReference doReturn() {
        PageReference pageRef = new PageReference(CustomApprovalsUtil.getPageUrlForObjectId(ctxObjId));
        pageRef.setRedirect(true);
        return pageRef;
    }

}