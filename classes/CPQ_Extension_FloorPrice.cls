public with sharing class CPQ_Extension_FloorPrice {

    public Apttus_Config2__ProductConfiguration__c theRecord {get; set;}
    public List<Apttus_Config2__LineItem__c> lineItems {get; set;}
    public List<Product2> lineItemOptions {get; set;}
    public List<Apttus_Config2__LineItem__c> items {get; set;}
    public Map<Id, myProduct> productMap {get; set;}
    public Map<Id, myBundle> bundleMap {get; set;}
    public Map<Id, myProduct> optionMap {get; set;}
    public Set<Id> productIds {get; set;}
    
    public class FloorPriceException extends Exception { }
    
    public virtual class myProduct {
        public decimal unitNetPrice {get; set;}
        public decimal basePrice {get; set;}
        public decimal repFloorPrice {get; set;}
        private boolean pricesAreEqual { get { return (unitNetPrice == basePrice); } }
        public boolean belowRepFloorPrice { get { return (!pricesAreEqual && (unitNetPrice < repFloorPrice)); } }
        public decimal rsmFloorPrice {get; set;}
        public boolean belowRSMFloorPrice { get { return (!pricesAreEqual && (unitNetPrice < rsmFloorPrice)); } }
        public decimal zvpFloorPrice {get; set;}
        public boolean belowZVPFloorPrice { get { return (!pricesAreEqual && (unitNetPrice < zvpFloorPrice)); } }
        public Product2 details {get; set;}
        public boolean isBundle { get { return (this instanceof myBundle); } }
    }
    
    public class myBundle extends myProduct {
        public List<myProduct> Options {get; set;}
    }

    public CPQ_Extension_FloorPrice(ApexPages.StandardController controller) {
        theRecord = (Apttus_Config2__ProductConfiguration__c)controller.getRecord();
    }
    
    public PageReference onLoad() {

        if (theRecord == null || theRecord.Id == null) return null;
        
        items = [SELECT Apttus_Config2__ProductId__c,
                        Apttus_Config2__ProductId__r.Rep_Floor_Price__c,
                        Apttus_Config2__ProductId__r.RM_Floor_Price__c,
                        Apttus_Config2__ProductId__r.Zone_VP_Floor_Price__c,
                        Apttus_Config2__OptionId__c,
                        Apttus_Config2__OptionId__r.Rep_Floor_Price__c,
                        Apttus_Config2__OptionId__r.RM_Floor_Price__c,
                        Apttus_Config2__OptionId__r.Zone_VP_Floor_Price__c,
                        Unit_Net_Price__c,
                        Apttus_Config2__BasePrice__c,
                        Is_Bundle_Header__c
                       FROM Apttus_Config2__LineItem__c
                      WHERE Apttus_Config2__ConfigurationId__c = :theRecord.Id];
                      
        productIds = new Set<Id>();
        productMap = new Map<Id, myProduct>();
        optionMap = new Map<Id, myProduct>();
        bundleMap = new Map<Id, myBundle>();
        
        for (Apttus_Config2__LineItem__c lineItem : items ) {
            if (lineItem.Apttus_Config2__OptionId__c == NULL) {
                if (!productMap.containsKey(lineItem.Apttus_Config2__ProductId__c)) {
                    if (lineItem.Is_Bundle_Header__c) {
                        myBundle bundle = new myBundle();  
                        bundle.unitNetPrice = lineItem.Unit_Net_Price__c;
                        bundle.basePrice = lineItem.Apttus_Config2__BasePrice__c;
                        bundle.repFloorPrice = lineItem.Apttus_Config2__ProductId__r.Rep_Floor_Price__c;
                        bundle.rsmFloorPrice = lineItem.Apttus_Config2__ProductId__r.RM_Floor_Price__c;
                        bundle.zvpFloorPrice = lineItem.Apttus_Config2__ProductId__r.Zone_VP_Floor_Price__c;
                        bundle.Options = new List<myProduct>();                 
                        productMap.put(lineItem.Apttus_Config2__ProductId__c, bundle);
                        bundleMap.put(lineItem.Apttus_Config2__ProductId__c, bundle);
                        productIds.add(lineItem.Apttus_Config2__ProductId__c);
                    }
                    else {
                        myProduct product = new myProduct();
                        product.unitNetPrice = lineItem.Unit_Net_Price__c;
                        product.basePrice = lineItem.Apttus_Config2__BasePrice__c;
                        product.repFloorPrice = lineItem.Apttus_Config2__ProductId__r.Rep_Floor_Price__c;
                        product.rsmFloorPrice = lineItem.Apttus_Config2__ProductId__r.RM_Floor_Price__c;
                        product.zvpFloorPrice = lineItem.Apttus_Config2__ProductId__r.Zone_VP_Floor_Price__c;
                        productMap.put(lineItem.Apttus_Config2__ProductId__c, product);
                        productIds.add(lineItem.Apttus_Config2__ProductId__c);
                    }
                }
            }
            else {
                myBundle bundle = bundleMap.get(lineItem.Apttus_Config2__ProductId__c);
                if (bundle == null) throw new FloorPriceException('This product appears to be a Bundle but is configured as Standalone: ' + lineItem.Apttus_Config2__ProductId__c);
                myProduct product = new myProduct();
                product.unitNetPrice = lineItem.Unit_Net_Price__c;
                product.basePrice = lineItem.Apttus_Config2__BasePrice__c;
                product.repFloorPrice = lineItem.Apttus_Config2__OptionId__r.Rep_Floor_Price__c;
                product.rsmFloorPrice = lineItem.Apttus_Config2__OptionId__r.RM_Floor_Price__c;
                product.zvpFloorPrice = lineItem.Apttus_Config2__OptionId__r.Zone_VP_Floor_Price__c;
                bundle.Options.add(product);
                optionMap.put(lineItem.Apttus_Config2__OptionId__c, product);
                productIds.add(lineItem.Apttus_Config2__OptionId__c);
            }
        }

        lineItemOptions = new List<Product2>();
        for (Product2 product : [SELECT Id, 
                                        ProductCode,
                                        Description,
                                        Rep_Floor_Price__c,
                                        Apttus_Config2__ConfigurationType__c
                                 FROM Product2
                                 WHERE Id IN :productIds]) {

            myProduct target = null;

            if (productMap.containsKey(product.Id))
                target= (myProduct)productMap.get(product.Id);
            else if (optionMap.containsKey(product.Id))
                target = (myProduct)optionMap.get(product.Id);

            target.details = product;
        }
        return null;
    }
}