/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date		Id   Initials   Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD	A	PAB			CPR-000		Updated comments section
							AV-000
20161103	A	IL			AV-001		Title of the Jira – Some changes needed to be made to support Medical Supplies

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
public class cpqPriceList_c extends sObject_c
{
	private static cpqPriceList_g cache = new cpqPriceList_g();
	
    // Apttus_Config2__PriceList__c

    // Name
    // Apttus_Config2__AccountId__c -> Lookup(Account)
    // Apttus_Config2__Active__c
    // Apttus_Config2__BasedOnAdjustmentAmount__c
    // Apttus_Config2__BasedOnAdjustmentType__c (% Discount | Discount Amount | % Markup | Markup Amount | Price Factor)
    // Apttus_Config2__BasedOnPriceListId__c -> Lookup(Price List)
    // Apttus_Config2__ContractNumber__c
    // Apttus_Config2__Description__c
    // Apttus_Config2__EffectiveDate__c
    // Apttus_Config2__ExpirationDate__c
    // Apttus_Config2__GuidePage__c
    //
    // Price_List_External_ID__c
    // Apttus_Config2__Type__c (New)
	public static Apttus_Config2__PriceList__c RMS { get { return OrganizationName2PriceList.get(OrgName_RMS); } }
	public static Apttus_Config2__PriceList__c SSG { get { return OrganizationName2PriceList.get(OrgName_SSG); } }
	public static Apttus_Config2__PriceList__c MS { get { return OrganizationName2PriceList.get(OrgName_MS); } }

	public static final string RMS_Name = 'Covidien RMS US Master';
	public static final string SSG_Name = 'Covidien SSG US Master';
	public static final string MS_Name = 'Covidien MS US Master';

    public static final string CPQVariable_RMS = 'RMS PriceList ID';
    public static final string CPQVariable_SSG = 'SSG PriceList ID';
    public static final string CPQVariable_MS = 'MS PriceList ID';
    
	private static final OrganizationNames_g.Abbreviations OrgName_RMS = OrganizationNames_g.Abbreviations.RMS;
	private static final OrganizationNames_g.Abbreviations OrgName_SSG = OrganizationNames_g.Abbreviations.SSG;
	private static final OrganizationNames_g.Abbreviations OrgName_MS = OrganizationNames_g.Abbreviations.MS;

	public static final Map<OrganizationNames_g.Abbreviations, string> masterPriceListNames =
		new Map<OrganizationNames_g.Abbreviations, string>
			{
				OrgName_RMS => RMS_Name,
				OrgName_SSG => SSG_Name,
				OrgName_MS => MS_Name
			};
	
	public static final Map<OrganizationNames_g.Abbreviations, string> OrganizationName2PriceListName = masterPriceListNames;

   	public static Map<string, OrganizationNames_g.Abbreviations> PriceListName2OrganizationName =
	    new Map<string, OrganizationNames_g.Abbreviations>();

  	public static final Map<string, string> PriceListName2VariableName =
    	new Map<string, string>
    	{
      		RMS_Name => CPQVariable_RMS,
      		SSG_Name => CPQVariable_SSG,
      		MS_Name => CPQVariable_MS
  		};
 
	public static Map<string, string> VariableName2PriceListName =
		new Map<string, string>();

	public static Map<Id, Apttus_Config2__PriceList__c> PriceLists
    {
    	get
    	{
    		cache.dump('PriceLists - start');
    		if (PriceLists == null)
    			PriceLists = cache.fetch();
    			
    		cache.dump('PriceLists - finish');
    		return PriceLists;
    	}
    	
    	private set;
    }
 
	public static Map<OrganizationNames_g.Abbreviations, Apttus_Config2__PriceList__c> OrganizationName2PriceList
    {
    	get
    	{
     		if (OrganizationName2PriceList ==  null)
    		{
    			Map<object, Map<Id, Apttus_Config2__PriceList__c>> cached = cache.fetch(Apttus_Config2__PriceList__c.field.OrganizationName__c);
    			OrganizationName2PriceList = new Map<OrganizationNames_g.Abbreviations, Apttus_Config2__PriceList__c>();
    			for(object abbr : cached.keySet())
    				for(Apttus_Config2__PriceList__c pl : cached.get(abbr).values())
    					OrganizationName2PriceList.put(OrganizationNames_g.valueOf((string)abbr), pl);
    		}
    		
    		return OrganizationName2PriceList;
    	}
    	
    	private set;
    }
    
    public static Apttus_Config2__PriceList__c fetch(OrganizationNames_g.Abbreviations orgName)
    {
    	return OrganizationName2PriceList.containsKey(orgName) ? OrganizationName2PriceList.get(orgName) : null;
    }
    
    public static Map<string, Apttus_Config2__PriceList__c> Name2PriceList
    {
    	get
    	{
    		if (Name2PriceList == null)
    		{
    			Map<object, Map<Id, Apttus_Config2__PriceList__c>> cached = cache.fetch(Apttus_Config2__PriceList__c.field.Name);
    			Name2PriceList = new Map<string, Apttus_Config2__PriceList__c>();
    			for(object name : cached.keySet())
    				for(Apttus_Config2__PriceList__c pl : cached.get(name).values())
    					Name2PriceList.put((string)name, pl);
    		}
    			
    		return Name2PriceList;
    	}
    	
    	private set;
    }

	private static set<string> fieldsToInclude =
		new set<string>
		{
			'Id',
			'Name',
			'OrganizationName__c',
			'Apttus_Config2__Type__c',
			'Apttus_Config2__Active__c'
		};
		
	////
	//// fetch from db
	////
	private static List<Apttus_Config2__PriceList__c> fetchFromDB()
	{
		List<string> plNames = masterPriceListNames.values();
		
		string query =
			SOQL_select.buildQuery(
				Apttus_Config2__PriceList__c.sObjectType,
				fieldsToInclude,
				' WHERE Name IN :plNames ',
				null,
				null);
				
		return (List<Apttus_Config2__PriceList__c>)Database.query(query);
	} 
	
	////
	//// create
	////
    public static Apttus_Config2__PriceList__c create(
        OrganizationNames_g.Abbreviations orgName)
    {
        Apttus_Config2__PriceList__c pl = new Apttus_Config2__PriceList__c();

       	pl.Name = masterPriceListNames.get(orgName);
       	pl.OrganizationName__c = orgName.name();
       	pl.Apttus_Config2__Type__c = 'Standard';
       	pl.Apttus_Config2__Active__c = true;

        return pl;
    }
    
    @testVisible
	private static List<Apttus_Config2__PriceList__c> createMasters()
	{
		List<Apttus_Config2__PriceList__c> masters = new List<Apttus_Config2__PriceList__c>();

		for(OrganizationNames_g.Abbreviations abbr : OrganizationNames_g.Abbreviations.values())
			masters.add(create(abbr));

		return masters;
	}
    
	////
	//// load into cache
	////
	@testVisible
    private static void load()
    {
		if (cache.isEmpty())
		{
			List<Apttus_Config2__PriceList__c> fromDB = fetchFromDB();
			if (fromDB.isEmpty() && Test.isRunningTest())
			{
				insert createMasters();
				fromDB = fetchFromDB();
			}
			cache.put(fromDB);
		}
    }
    
	static
	{
		for(OrganizationNames_g.Abbreviations e : masterPriceListNames.keySet())
			PriceListName2OrganizationName.put(masterPriceListNames.get(e), e);
			
		for(string pln : PriceListName2VariableName.keySet())
			VariableName2PriceListName.put(PriceListName2VariableName.get(pln), pln);
			
		load();
	}		  
}