/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestValidateAndSubmit {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'mdt9test2@mdt9test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 )
        {
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',billingcountry='SG',OwnerId = portalAccountOwner1.Id);
        insert acc;
        Account acc1=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',billingcountry='SG',OwnerId = portalAccountOwner1.Id,Account_External_ID__c='133');
        insert acc1;
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
         Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;
        Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        //Product_SKU__c;
        insert ps;
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
        insert cp;
        Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
        insert cp1;
        Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
        //si.
        Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
        insert si;
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        Channel_Inventory__c ci=new Channel_Inventory__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Quantity_Text__c='-100',Product_Code_Text__c='111');
        insert ci;
        Channel_Inventory__c ci1=new Channel_Inventory__c(Cycle_Period__c=cp1.Id,Distributor_Name__c=acc.Id,Quantity_Text__c='100',Product_Code_Text__c='1111');
        insert ci1;
        In_Transit__c it=new In_Transit__c(Sales_In__c=si.Id,Cycle_Period_dist__c=cp.Id);
        insert it;
        //In_Transit__c..Sales_In__c.Cycle_Period_dist__c
        List<Sales_Out__c> lstSout=new List<Sales_Out__c>();
        
        lstSout.add(so);
        lstSout.add(so1);
        List<Channel_Inventory__c> lstCI=new List<Channel_Inventory__c>();
        
        lstCI.add(ci);
        RecordType rt1=[Select Id from RecordType where DeveloperName='Distributor_contact' Limit 1];
        //Create portalusercontact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        RecordTypeId=rt1.Id,
        AccountId = acc.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        Database.insert(contact1);
        Profile portalProfile = [SELECT Id FROM Profile where Name='Asia Distributor - KR' Limit 1];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@mdttest.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        Asia_Team_Asia_use_only__c = 'PRM Team',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
        ContactShare cs=new ContactShare(ContactId=contact1.Id,UserOrGroupId=user1.Id,ContactAccessLevel='Read');
        Database.insert(cs);
        AccountShare ash=new AccountShare(AccountId=acc.Id,UserOrGroupId=user1.Id,AccountAccessLevel='Read',OpportunityAccessLevel='Read',ContactAccessLevel='Read',CaseAccessLevel='Read');
        Database.insert(ash);
        System.runAs ( user1 ) {
            Test.startTest();
            PageReference pageRef= Page.ValidateSalesOut;
            pageRef.getParameters().put('id', cp.Id);
            Test.setCurrentPage(pageRef);
            ValidateandCompleteSalesOut controller = new ValidateandCompleteSalesOut(new ApexPages.StandardController(cp));
           // controller.distributorId=acc.Id;
            System.runAs ( portalAccountOwner1 ) {
            PageReference pg=controller.validateAndComplete();
           // PageReference pageRef2= Page.ValidateSalesOut;
           // pageRef2.getParameters().put('id', cp.Id);
           // Test.setCurrentPage(pageRef2);
           ValidateandCompleteSalesOut.runValidations(lstSout);
            ValidateandCompleteSalesOut.runCycleValidations(lstSout);
            so.Product_Code__c='1111';
            so.Sell_To__c='133';
            so.Sell_From__c='133';
            so.UOM__c='EA';
            so.Reason_Code__c = 'Y-Wrong Qty';
            update so;
            pg=controller.validateAndComplete();
            List<SelectOption> soptions=controller.getItems();
            
           // pg=controller.validateAndComplete();
            PageReference pageRef1= Page.ValidateChannelInventory;
            pageRef1.getParameters().put('id', cp.Id);
            Test.setCurrentPage(pageRef1);
            ValidateAndCompleteCI controllerCI = new ValidateAndCompleteCI(new ApexPages.StandardController(cp));
            pg=controllerCI.validateAndComplete();
            ValidateAndCompleteCI.runCycleValidations(lstCI);
            soptions=controllerCI.getItems();
            controllerCI.processSelectedPeriod();
            controllerCI.CyclePeriodChoice=cp.Id;
            controllerCI.processSelectedPeriod();
            PageReference pageRef3= Page.ValidateChannelInventory;
            pageRef1.getParameters().put('id', cp1.Id);
            Test.setCurrentPage(pageRef3);
            ValidateAndCompleteCI controllerCI1 = new ValidateAndCompleteCI(new ApexPages.StandardController(cp1));
            pg=controllerCI1.validateAndComplete();
            
            ////ValidateandCompleteIntransit and Copy Quatity
            List<In_Transit__c> iset=new List<In_Transit__c>();
            Pagereference pgIT=Page.CopyQuantityRemaining;
            Test.setCurrentPage(pgIT);
     //       ApexPages.StandardSetController cont=new ApexPages.StandardSetController(iset);
            CopyReceivedQuantity contIT = new CopyReceivedQuantity(new ApexPages.StandardController(cp1));
    //          CopyReceivedQuantity contIT = new CopyReceivedQuantity(cont);    
            pg=contIT.copyandpaste();
            PageReference pageRef4= Page.Validateintransit;
            pageRef4.getParameters().put('id', cp1.Id);
            Test.setCurrentPage(pageRef4);
            ValidateandCompleteIntransit controllerIT = new ValidateandCompleteIntransit(new ApexPages.StandardController(cp1));
            pg=controllerIT.validateAndComplete();
            PageReference pageRef5= Page.Validateintransit;
            pageRef5.getParameters().put('id', cp.Id);
            Test.setCurrentPage(pageRef5);
            ValidateandCompleteIntransit controllerIT1 = new ValidateandCompleteIntransit(new ApexPages.StandardController(cp));
            pg=controllerIT1.validateAndComplete();
            update ci;
            cp.Cycle_Locked__c = true;
            update cp;
            try
            {
                delete ci;
            }
            catch(Exception e)
            {
            }
            
            }
            Test.stopTest();
        }
        }
    
        // TO DO: implement unit test
    }
}