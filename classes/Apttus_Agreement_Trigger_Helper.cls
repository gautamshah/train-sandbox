/****************************************************************************************
* Name    : Class: Apttus_Agreement_Trigger_Helper
* Author  : Clerval Fokle - Statera
* Date    : 6/1/2015
* Purpose : A wrapper for static functionality for approval approvers records
*           This trigger will determine and update the OD Approver column on the agreement record.  It uses a custom object where the 
*           OD approver and Territory are managed in a junction table.
* 
* Dependencies: 
*   Called by: CPQ_Apttus_Agreement_Trigger
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 6/1/2015      Clerval Fokle - Statera Initial Code
* 9/19/2016     Bryan Fry               Opened up for all deal types
*
*****************************************************************************************/
public class Apttus_Agreement_Trigger_Helper
{
}