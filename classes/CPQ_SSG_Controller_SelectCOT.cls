public class CPQ_SSG_Controller_SelectCOT {
    public static final String LISTERINE_TYPE = 'Listerine';
    public static final String MANUAL_TYPE = 'Manual';
    public static final String NONE_TYPE = 'None';
    public static final String BOTH_TYPE = 'Both';
    public static final String PROPOSAL_TYPE = 'Proposal';
    public static final String CATEGORY_TYPE = 'Category';
    public Apttus_Proposal__Proposal__c qp {get;set;}
    public List<CPQ_SSG_Class_of_Trade__c> cots {get;set;}
    public List<COT_Display> cotDisplays {get;set;}
    public List<COT_Display> cotDisplaysSelected {get;set;}
    public List<CPQ_SSG_QP_Class_of_Trade__c> qpCots {get;set;}
    private Map<String,CPQ_SSG_QP_Class_of_Trade__c> qpCotsMap;
    private Map<String,COT_Display> cotDisplayMap;
    public String compareCotName {get;set;}
    public String familyValueTier {get;set;}

    public CPQ_SSG_Controller_SelectCOT(ApexPages.StandardController stdController) {
        this.qp = (Apttus_Proposal__Proposal__c)stdController.getRecord();
        qp = [Select Id, Committed_Stapling_Vessel_Sealing__c, Qualified_Family_Value_Percent__c, Apttus_Proposal__Account__c, Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__r.Name, Apttus_Proposal__Payment_Term__c from Apttus_Proposal__Proposal__c Where Id = :qp.Id];
        familyValueTier = getFamilyValueTier(qp.Qualified_Family_Value_Percent__c);

        // Get all Classes of Trade in the system
        cots = [Select Id, Name, (Select Id, Name, Description__c, Max_Dollar_Commitment__c, Max_Percent_Commitment__c, Min_Dollar_Commitment__c, Min_Percent_Commitment__c, Pricing_Tier_Type__c, Sort_Order__c From CPQ_SSG_Pricing_Tiers__r Where Is_Active__c = true Order By CPQ_SSG_Class_of_Trade__c, Sort_Order__c, Name) From CPQ_SSG_Class_of_Trade__c Order By Name];
        List<String> cotNames = new List<String>();
        cotDisplays = new List<COT_Display>();

        // Create container object for Visualforce
        for (CPQ_SSG_Class_of_Trade__c cot: cots) {
            cotDisplays.add(new COT_Display(cot));
            cotNames.add(cot.Name);
        }

        // Get all Classes of Trade already associated with the Quote/Proposal and create a Map by Class of Trade name

        qpCots = [Select CPQ_SSG_Class_of_Trade__c, CPQ_SSG_Class_of_Trade__r.Name, Expected_Annual_Compliance__c, Expected_Annual_Customer_Spend__c, Proposed_Pricing_Tier__c, Qualified_Pricing_Tier__c, Quote_Proposal__c, Quote_Proposal__r.Name, List_Type__c, Include_Rebates__c, All_Products_Rebate__c, New_Business_Rebate__c, Growth_Rebate__c, All_Products_Rebate_Years__c, New_Business_Rebate_Years__c, Growth_Rebate_Years__c, Compare_Tier_1__c, Compare_Tier_2__c, GPO_Contract__c From CPQ_SSG_QP_Class_of_Trade__c Where Quote_Proposal__c = :qp.Id];
        qpCotsMap = new Map<String, CPQ_SSG_QP_Class_of_Trade__c>();
        for (CPQ_SSG_QP_Class_of_Trade__c qpCot: qpCots) {
            qpCotsMap.put(qpCot.CPQ_SSG_Class_of_Trade__r.Name, qpCot);
        }

        // Fill previously selected values from CPQ_SSG_QP_Class_of_Trade__c into COTDisplay object
        cotDisplayMap = new Map<String,COT_Display>();
        for (COT_Display cot: cotDisplays) {
            cotDisplayMap.put(cot.Name, cot);
            CPQ_SSG_QP_Class_of_Trade__c qpCot = qpCotsMap.get(cot.Name);
            if (qpCot != null) {
                cot.setQpValues(qpCot);
            }
        }

        List<Partner_Root_Contract__c> partnerRootContracts = [SELECT Id, Commitment_Type__r.Name, Root_Contract_Number__c, (SELECT Id, Name FROM Related_Partner_Contracts__r) FROM Partner_Root_Contract__c WHERE Account__c = :qp.Apttus_Proposal__Account__c And Expiration_Date__c >= TODAY And Commitment_Type__r.Name not in ('IP','IR')];
        Set<String> rootContractNumbers = new Set<String>();
        for (Partner_Root_Contract__c contract: partnerRootContracts) {
            rootContractNumbers.add(contract.Root_Contract_Number__c);
            
            // If there is no related contract in sf, try R and D contract.
            if (contract.Related_Partner_Contracts__r.size() == 0) {
                rootContractNumbers.add('R' + contract.Root_Contract_Number__c);
                rootContractNumbers.add('D' + contract.Root_Contract_Number__c);
            } else {
                for (Related_Partner_Contract__c relatedContract : contract.Related_Partner_Contracts__r) {
                    rootContractNumbers.add(relatedContract.Name);
                }
            }
        }

        if (!rootContractNumbers.isEmpty()) {
            CPQ_ContractInfoInvoke.ContractInfo contractInfo;
            if (!Test.isrunningTest()) {
                contractInfo = CPQ_ContractInfoInvoke.InvokeContractInfo(new List<String>(rootContractNumbers));
            } else {
                contractInfo = new CPQ_ContractInfoInvoke.ContractInfo();
                contractInfo.CommitmentType = 'TST';
                contractInfo.itemList = new List<CPQ_ContractInfoInvoke.Items>();
                CPQ_ContractInfoInvoke.Items item = new CPQ_ContractInfoInvoke.Items();
                item.itemNumberField = 'TestProduct1';
                contractInfo.itemList.add(item);
            }
            List<Product2> cotProducts = [Select Id, ProductCode, Category__c From Product2 Where Apttus_Surgical_Product__c = true And Category__c in :cotNames Order By Category__c, ProductCode];

            // Get Map of product code to class of trade name
            Map<String,String> productToCotMap = new Map<String,String>();
            for (Product2 prod: cotProducts) {
                productToCotMap.put(prod.ProductCode, prod.Category__c);
            }
            if (contractInfo.CommitmentType !=  'IP' && contractInfo.CommitmentType != 'IR') {
                for (CPQ_ContractInfoInvoke.Items item: contractInfo.itemList) {
                    String cot = productToCotMap.get(item.itemNumberField);
                    if (cot != null) {
                        cotDisplayMap.get(cot).GPOContract = 'Yes';
                    }
                }
            }
        }
    }
    
    public PageReference doSelectClassesOfTrade() {
        CPQ_SSG_QP_Class_of_Trade__c newQpCot;
        List<CPQ_SSG_QP_Class_of_Trade__c> cotsToInsert = new List<CPQ_SSG_QP_Class_of_Trade__c>();
        List<CPQ_SSG_QP_Class_of_Trade__c> cotsToUpdate = new List<CPQ_SSG_QP_Class_of_Trade__c>();
        List<CPQ_SSG_QP_Class_of_Trade__c> cotsToDelete = new List<CPQ_SSG_QP_Class_of_Trade__c>();
        
        cotDisplaysSelected = new List<COT_Display>();

        for (COT_Display cot: cotDisplays) {
            if (cot.listType != NONE_TYPE) {
                cot.setTiersMap();
                CPQ_SSG_QP_Class_of_Trade__c qpCot;
                if (!qpCotsMap.containsKey(cot.Name)) {
                    qpCot = new CPQ_SSG_QP_Class_of_Trade__c(CPQ_SSG_Class_Of_Trade__c = cot.cotId, Quote_Proposal__c = qp.Id);
                    cotsToInsert.add(qpCot);
                    qpCotsMap.put(cot.Name, qpCot);
                } else {
                    qpCot = qpCotsMap.get(cot.Name);
                    cotsToUpdate.add(qpCot);
                }

                qpCot.List_Type__c = cot.listType;
                qpCot.Expected_Annual_Customer_Spend__c = cot.complianceAmount;
                qpCot.Expected_Annual_Compliance__c = cot.compliancePercent;
                qpCot.Include_Rebates__c = cot.includeRebates;

                if (qpCot.Include_Rebates__c == false) {
                    cot.allRebate = 0.0;
                    cot.newRebate = 0.0;
                    cot.growthRebate = 0.0;
                    cot.allRebateYears = 0;
                    cot.newRebateYears = 0;
                    cot.growthRebateYears = 0;
                }
                qpCot.All_Products_Rebate__c = cot.allRebate;
                qpCot.New_Business_Rebate__c = cot.newRebate;
                qpCot.Growth_Rebate__c = cot.growthRebate;
                qpCot.All_Products_Rebate_Years__c = cot.allRebateYears;
                qpCot.New_Business_Rebate_Years__c = cot.newRebateYears;
                qpCot.Growth_Rebate_Years__c = cot.growthRebateYears;
                qpCot.GPO_Contract__c = cot.GPOContract == 'Yes';
                cotDisplaysSelected.add(cot);
            } else {
                if (qpCotsMap.containsKey(cot.Name)) {
                    // Remove from the map and the database before continuing.
                    cotsToDelete.add(qpCotsMap.get(cot.Name));
                    qpCotsMap.remove(cot.Name);
                }
            }
        }

        // Take the selected Display COT objects, calculate the qualifying tier levels,
        // and set them on the Display COTs and QP COT objects. Only do this for Listerine
        // types.
        calculateQualifyingTiers();


        if (!cotsToInsert.isEmpty()) {
            insert cotsToInsert;
        }
        if (!cotsToUpdate.isEmpty()) {
            update cotsToUpdate;
        }

        update qp;

        if (!cotsToDelete.isEmpty()) {
            delete cotsToDelete;
        }

        qpCots = [Select CPQ_SSG_Class_of_Trade__c, CPQ_SSG_Class_of_Trade__r.Name, Expected_Annual_Compliance__c, Expected_Annual_Customer_Spend__c, Proposed_Pricing_Tier__c, Qualified_Pricing_Tier__c, Quote_Proposal__c, Quote_Proposal__r.Name, List_Type__c, Include_Rebates__c, All_Products_Rebate__c, New_Business_Rebate__c, Growth_Rebate__c, All_Products_Rebate_Years__c, New_Business_Rebate_Years__c, Growth_Rebate_Years__c, Compare_Tier_1__c, Compare_Tier_2__c, GPO_Contract__c From CPQ_SSG_QP_Class_of_Trade__c Where Quote_Proposal__c = :qp.Id];
        qpCotsMap = new Map<String, CPQ_SSG_QP_Class_of_Trade__c>();
        for (CPQ_SSG_QP_Class_of_Trade__c qpCot: qpCots) {
            qpCotsMap.put(qpCot.CPQ_SSG_Class_of_Trade__r.Name, qpCot);
        }

        PageReference ref = Page.CPQ_SSG_COTDetails;
        ref.setRedirect(false);
        return ref;
    }

    // Return to the Quote/Proposal record
    public PageReference doCancel() {
        return new PageReference('/' + qp.Id);
    }

    private void calculateQualifyingTiers() {
        // Populate first-level qualifying tier
        CPQ_SSG_Pricing_Tier__c tier;
        Integer familyValueQualifyingTiers = 0;
        for (COT_Display cot: cotDisplaysSelected) {
            if (cot.listType == LISTERINE_TYPE) {
                tier = getSinglePricingTier(cot);
                cot.qualifiedTier = tier.Id;
                cot.qualifiedTierName = tier.Name;
                qpCotsMap.get(cot.Name).Qualified_Pricing_Tier__c = tier.Id;
                if (cot.proposedTier == null || cot.proposedTier == '') {
                    cot.proposedTier = tier.Id;
                }
                CPQ_SSG_QP_Class_Of_Trade__c qpCot = qpCotsMap.get(cot.Name);
                if (qpCot.Proposed_Pricing_Tier__c == null) {
                    qpCot.Proposed_Pricing_Tier__c = tier.Id;
                }

                // For each Listerine class of trade at the highest percentage level of commitment,
                // increase the family value count by 1.
                if (cot.listType == LISTERINE_TYPE) {
                    if (tier.Sort_Order__c > 20 && tier.Sort_Order__c < 26) {
                        familyValueQualifyingTiers++;
                    }
                }
            } else {
                CPQ_SSG_QP_Class_Of_Trade__c qpCot = qpCotsMap.get(cot.Name);
                if (qpCot.Proposed_Pricing_Tier__c == null) {
                    List<CPQ_SSG_Pricing_Tier__c> tiers = cot.tiersMap.values();
                    if (tiers != null && !tiers.isEmpty()) {
                        qpCot.Proposed_Pricing_Tier__c = tiers[0].Id;
                    }
                }
            }
        }

        // In addition to Listerine classes of trade, add 1 to the family value count
        // if the account is committed in Stapling or Vessel Sealing.
        if (qp.Committed_Stapling_Vessel_Sealing__c == 'Yes') {
            familyValueQualifyingTiers++;
        }

        qp.Qualified_Family_Value_Percent__c = getListerineFamilyValueAdjustment(familyValueQualifyingTiers);
        familyValueTier = getFamilyValueTier(qp.Qualified_Family_Value_Percent__c);
    }

    private CPQ_SSG_Pricing_Tier__c getSinglePricingTier(COT_Display cot) {
        if (cot.complianceAmount != null && cot.compliancePercent != null) {
            for (CPQ_SSG_Pricing_Tier__c tier: cot.tiersMap.values()) {
                if (tier.Min_Dollar_Commitment__c != null && tier.Max_Dollar_Commitment__c != null &&
                        tier.Min_Percent_Commitment__c != null && tier.Max_Percent_Commitment__c != null) {
                    if (tier.Min_Dollar_Commitment__c <= cot.complianceAmount &&
                            tier.Max_Dollar_Commitment__c > cot.complianceAmount &&
                            tier.Min_Percent_Commitment__c <= cot.compliancePercent &&
                            tier.Max_Percent_Commitment__c > cot.compliancePercent) {
                        return tier;
                    }
                }
            }
        }

        // Add error
        return null;
    }

    private Integer getListerineFamilyValueAdjustment(Integer familyValueQualifyingTiers) {
        // The overall family value count represents the number of Listerine classes of trade
        // committed at the highest percentage levels plus 1 for an 85% commitment in
        // Stapling or Vessel Sealing.  1 or 2 does not result in a family value discount, but
        // values from 3-5 result in 3%-5% available discounts by switching to family value
        // tiers. This is not applied automatically, but listed on the screen to assist
        // the seller in selected a proposed tier.
        Integer familyValuePercent = 0;
        if (familyValueQualifyingTiers < 3) {
            familyValuePercent = 0;
        } else if (familyValueQualifyingTiers >= 3 && familyValueQualifyingTiers <= 5) {
            familyValuePercent = familyValueQualifyingTiers;
        } else if (familyValueQualifyingTiers > 5) {
            familyValuePercent = 5;
        }

        return familyValuePercent;
    }

    private String getFamilyValueTier(Decimal fvPercent) {
        Integer fvPercentInt;
        if (fvPercent != null) {
            fvPercentInt = Integer.valueOf(fvPercent);
        }

        if (fvPercentInt != null && fvPercentInt >= 3.0 && fvPercentInt <= 5) {
            return 'Family Value ' + String.valueOf(fvPercentInt - 2);
        } else {
            return '';
        }
    }

    // Proposed tiers are selected. Copy over product price information for proposed tiers and
    // create Apttus line items in cart.
    public PageReference doSelectPricingTiers() {
        // Get a list of proposed tier Ids for determining prices and a list of dummy COT products to get
        Map<String,CPQ_SSG_QP_Class_of_Trade__c> proposedTierIdMap = new Map<String,CPQ_SSG_QP_Class_of_Trade__c>();
        Map<String,CPQ_SSG_QP_Class_of_Trade__c> productNameMap = new Map<String,CPQ_SSG_QP_Class_of_Trade__c>();
        Map<Id,CPQ_SSG_QP_Class_of_Trade__c> productIdMap = new Map<Id,CPQ_SSG_QP_Class_of_Trade__c>();
        List<Rebate__c> rebatesToInsert = new List<Rebate__c>();
        Id allProductsRtId;
        Id growthRtId;
        Id newBusinessRtId;

        // Copy screen changes to proposed tier into qpCots list
        List<CPQ_SSG_QP_Class_of_Trade__c> cotsToUpdate = new List<CPQ_SSG_QP_Class_of_Trade__c>();
        for (COT_Display cot: cotDisplaysSelected) {
            CPQ_SSG_QP_Class_of_Trade__c qpCot = qpCotsMap.get(cot.Name);
            cotsToUpdate.add(qpCot);

            qpCot.Proposed_Pricing_Tier__c = cot.proposedTier;
        }
        if (!cotsToUpdate.isEmpty()) {
            update cotsToUpdate;
        }

        List<RecordType> recordTypes = [Select Id, DeveloperName from RecordType where SobjectType = 'Rebate__c' and DeveloperName in ('All_Product_Participation','Growth','New_Business_Conversion')];
        for (RecordType rt: recordTypes) {
            if (rt.DeveloperName == 'All_Product_Participation') {
                allProductsRtId = rt.Id;
            } else if (rt.DeveloperName == 'Growth') {
                growthRtId = rt.Id;
            } else if (rt.DeveloperName == 'New_Business_Conversion') {
                newBusinessRtId = rt.Id;
            }
        }

        for (CPQ_SSG_QP_Class_of_Trade__c qpCot: qpCots) {
            proposedTierIdMap.put(qpCot.Proposed_Pricing_Tier__c, qpCot);
            productNameMap.put(qpCot.CPQ_SSG_Class_of_Trade__r.Name + ' COT', qpCot);
            productIdMap.put(qpCot.CPQ_SSG_Class_of_Trade__r.Id, qpCot);
            if (qpCot.Include_Rebates__c == true && qpCot.All_Products_Rebate__c != null && qpCot.All_Products_Rebate__c > 0.0) {
                rebatesToInsert.add(new Rebate__c(RecordTypeId = allProductsRtId, Related_Proposal__c = qp.Id, Rebate_Percent__c = qpCot.All_Products_Rebate__c, Rebate_Years__c = String.valueOf(qpCot.All_Products_Rebate_Years__c)));
            }
            if (qpCot.Include_Rebates__c == true && qpCot.Growth_Rebate__c != null && qpCot.Growth_Rebate__c > 0.0) {
                rebatesToInsert.add(new Rebate__c(RecordTypeId = growthRtId, Related_Proposal__c = qp.Id, Rebate_Percent__c = qpCot.Growth_Rebate__c, Rebate_Years__c = String.valueOf(qpCot.Growth_Rebate_Years__c)));
            }
            if (qpCot.Include_Rebates__c == true && qpCot.New_Business_Rebate__c != null && qpCot.New_Business_Rebate__c > 0.0) {
                rebatesToInsert.add(new Rebate__c(RecordTypeId = newBusinessRtId, Related_Proposal__c = qp.Id, Rebate_Percent__c = qpCot.New_Business_Rebate__c, Rebate_Years__c = String.valueOf(qpCot.New_Business_Rebate_Years__c)));
            }
        }

        insert rebatesToInsert;

        // Get product pricing for all proposed tiers
        List<CPQ_SSG_QP_Product_Pricing__c> qpProductPrices = new List<CPQ_SSG_QP_Product_Pricing__c>();
        CPQ_SSG_QP_Product_Pricing__c qpPrice;

        // Get Price List information and construct Map of pricing tier to price lists
        List<CPQ_SSG_Pricing_Tier_Price_list__c> pricingTierPriceLists =
            [Select Id
                    ,CPQ_SSG_Pricing_Tier__c
                    ,CPQ_SSG_Price_List__c
                    ,CPQ_SSG_Price_List__r.Price_List_Code__c
             From CPQ_SSG_Pricing_Tier_Price_List__c
             Where CPQ_SSG_Pricing_Tier__c in :proposedTierIdMap.keySet()];
        Map<Id,List<String>> pricingTierToPriceListCodeMap = new Map<Id,List<String>>();
        Map<String,Id> priceListCodeToIdMap = new Map<String,Id>();
        for (CPQ_SSG_Pricing_Tier_Price_list__c pricingTierPriceList: pricingTierPriceLists) {
            if (!pricingTierToPriceListCodeMap.containsKey(pricingTierPriceList.CPQ_SSG_Pricing_Tier__c)) {
                pricingTierToPriceListCodeMap.put(pricingTierPriceList.CPQ_SSG_Pricing_Tier__c, new List<String>());
            }
            List<String> tierPriceListCodes = pricingTierToPriceListCodeMap.get(pricingTierPriceList.CPQ_SSG_Pricing_Tier__c);
            tierPriceListCodes.add(pricingTierPriceList.CPQ_SSG_Price_List__r.Price_List_Code__c);
            priceListCodeToIdMap.put(pricingTierPriceList.CPQ_SSG_Price_List__r.Price_List_Code__c, pricingTierPriceList.CPQ_SSG_Price_List__c);
        }
        // Select Id, Price_List_Code__c, Product__c, Product_Code__c, Price__c From CPQ_SSG_Product_Prices__r Where UOM__c = 'EA')

        // Get item prices by price list
        List<Contract_Item_Price__c> itemPrices = [SELECT Conversion_Factor__c, Direct_Price__c, Effective_Date__c, Expiration_Date__c, Item__c, Pricing_Identifier__c, Selling_UOM__c FROM Contract_Item_Price__c Where Pricing_Identifier__c in :priceListCodeToIdMap.keySet()];
        Set<String> productCodes = new Set<String>();
        Map<String,List<Contract_Item_Price__c>> contractItemPriceMap = new Map<String,List<Contract_Item_Price__c>>(); 
        for (Contract_Item_Price__c itemPrice: itemPrices) {
            if (!contractItemPriceMap.containsKey(itemPrice.Pricing_Identifier__c)) {
                contractItemPriceMap.put(itemPrice.Pricing_Identifier__c, new List<Contract_Item_Price__c>());
            }
            List<Contract_Item_Price__c> listitems = contractItemPriceMap.get(itemPrice.Pricing_Identifier__c);
            listItems.add(itemPrice);

            productCodes.add(itemPrice.Item__c);
        }

        // Get products used in the price list
        List<Product2> products = [Select Id, ProductCode From Product2 Where Apttus_Surgical_Product__c = true And ProductCode in :productCodes];
        Map<String,Id> productCodeToIdMap = new Map<String,Id>();
        for (Product2 product: products) {
            productCodeToIdMap.put(product.ProductCode, product.Id);
        }

        for (Id pricingTierId: pricingTierToPriceListCodeMap.keySet()) {
            List<String> priceListCodes = pricingTierToPriceListCodeMap.get(pricingTierId);

            // Find QP Class of Trade for this price list
            CPQ_SSG_QP_Class_of_Trade__c qpCot;
            qpCot = proposedTierIdMap.get(pricingTierId);

            // Construct QP product price objects for all prices in this price list and add them to a list to insert.
            for (String priceListCode: priceListCodes) {
                List<Contract_Item_Price__c> contractItemPrices = contractItemPriceMap.get(priceListCode);
                if (contractItemPrices != null) {
                    for (Contract_Item_Price__c itemPrice: contractItemPrices) {
                        Id productId = productCodeToIdMap.get(itemPrice.Item__c);
                        if (productId != null) {
                            qpPrice = new CPQ_SSG_QP_Product_Pricing__c();
                            qpPrice.CPQ_SSG_QP_Class_of_Trade__c = qpCot.Id;
                            qpPrice.Price__c = itemPrice.Direct_Price__c;
                            qpPrice.Product__c = productId;
                            qpPrice.Proposal_Product_Combined_Key_Text__c = qp.Id + '_' + productId;
                            qpProductPrices.add(qpPrice);
                        }
                    }
                }
            }
        }

        // Delete exising QP product price rows so only the new set is associated with the Quote/Proposal
        List<CPQ_SSG_QP_Product_Pricing__c> oldQpProductPrices = [Select Id From CPQ_SSG_QP_Product_Pricing__c Where CPQ_SSG_QP_Class_of_Trade__r.Quote_Proposal__c = :qp.Id];
        if (oldQpProductPrices != null && !oldQpProductPrices.isEmpty()) {
            delete oldQpProductPrices;
        }

        // Get dummy products by name
        List<Product2> cotProducts = [Select Id, Name, Description From Product2 Where Name in :productNameMap.keySet()];
        List<Id> productIds = new List<Id>();
        if (cotProducts != null) {
            for (Product2 product: cotProducts) {
                productIds.add(product.Id);
            }
        }

        // Get Product Classifications
        List<Apttus_Config2__ProductClassification__c> productClassifications = [SELECT Apttus_Config2__ClassificationId__c, Apttus_Config2__ProductId__c FROM Apttus_Config2__ProductClassification__c Where Apttus_Config2__ProductId__c in :productIds];
        Map<Id,Apttus_Config2__ProductClassification__c> productClassificationMap = new Map<Id,Apttus_Config2__ProductClassification__c>();
        if (productClassifications != null) {
            for (Apttus_Config2__ProductClassification__c classification: productClassifications) {
                productClassificationMap.put(classification.Apttus_Config2__ProductId__c, classification);
            }
        }

        // Get Price List
        Apttus_Config2__PriceList__c priceList = cpqPriceList_c.SSG;

        // Get Price List items
        List<Apttus_Config2__PriceListItem__c> priceListItems = [SELECT Id, Apttus_Config2__ProductId__c FROM Apttus_Config2__PriceListItem__c Where Apttus_Config2__ProductId__c in :productIds And Apttus_Config2__PriceListId__c = :priceList.Id];
        Map<Id,Apttus_Config2__PriceListItem__c> priceListItemMap = new Map<Id,Apttus_Config2__PriceListItem__c>();
        if (priceListItems != null) {
            for (Apttus_Config2__PriceListItem__c priceListItem: priceListItems) {
                priceListItemMap.put(priceListItem.Apttus_Config2__ProductId__c, priceListItem);
            }
        }

        // Get existing configuration and set to superseded if it exists
        List<Apttus_Config2__ProductConfiguration__c> existingConfigurations = [Select Id, Apttus_Config2__Status__c, Apttus_Config2__VersionNumber__c 
                                                                                From Apttus_Config2__ProductConfiguration__c 
                                                                                Where Apttus_Config2__BusinessObjectId__c = :qp.Id
                                                                                And Apttus_Config2__Status__c != 'Superseded'
                                                                                Order By Apttus_Config2__VersionNumber__c Desc
                                                                                Limit 1];
        Integer newConfigurationVersion = 1;
        if (existingConfigurations != null && !existingConfigurations.isEmpty()) {
            newConfigurationVersion = Integer.valueOf(existingConfigurations[0].Apttus_Config2__VersionNumber__c + 1);
            existingConfigurations[0].Apttus_Config2__Status__c = 'Superseded';
            update existingConfigurations[0];
        }

        // Create new product configuration for the quote
        Apttus_Config2__ProductConfiguration__c configuration = new Apttus_Config2__ProductConfiguration__c();
        configuration.Name = qp.Name;
        configuration.Apttus_Config2__AccountId__c = qp.Apttus_Proposal__Account__c;
        configuration.Apttus_Config2__SummaryGroupType__c = CATEGORY_TYPE;
        configuration.Apttus_Config2__PriceListId__c = priceList.Id;
        configuration.Apttus_Config2__EffectivePriceListId__c = priceList.Id;
        configuration.Apttus_Config2__ExpectedStartDate__c = System.today().addDays(60);
        configuration.Apttus_Config2__ExpectedEndDate__c = getNewExpectedEndDate(configuration.Apttus_Config2__ExpectedStartDate__c, qp.Apttus_Proposal__Payment_Term__c);
        configuration.Apttus_QPConfig__Proposald__c = qp.Id;
        configuration.Apttus_Config2__Status__c = 'Saved';
        configuration.Apttus_Config2__BusinessObjectId__c = qp.Id;
        configuration.Apttus_Config2__BusinessObjectType__c = PROPOSAL_TYPE;
        configuration.Apttus_Config2__VersionNumber__c = newConfigurationVersion;
        insert configuration;

        // Create new line items for each Class of Trade selected
        Integer lineNumber = 1;
        List<Apttus_Config2__LineItem__c> lineItems = new List<Apttus_Config2__LineItem__c>();
        if (cotProducts != null) {
            for (Product2 prod: cotProducts) {
                CPQ_SSG_QP_Class_of_Trade__c qpCot = productNameMap.get(prod.Name);
                Apttus_Config2__LineItem__c lineItem = new Apttus_Config2__LineItem__c();
                lineItem.Apttus_Config2__AllowableAction__c = 'Unrestricted';
                lineItem.Apttus_Config2__LineStatus__c = 'New';
                lineItem.Apttus_Config2__ChargeType__c = 'Standard Price';
                lineItem.Apttus_Config2__PriceMethod__c = 'Flat Price';
                lineItem.Apttus_Config2__BasePriceMethod__c = 'Flat Price';
                lineItem.Apttus_Config2__SyncStatus__c = 'Synchronized';
                lineItem.Apttus_Config2__IsPrimaryLine__c = true;
                lineItem.Apttus_Config2__LineType__c = 'Product/Service';
                lineItem.Apttus_Config2__TotalQuantity__c = 1;
                lineItem.Apttus_Config2__Quantity__c = 1;
                lineItem.Apttus_Config2__Uom__c = 'Each';
                lineItem.Apttus_Config2__PriceUom__c = 'Each';
                lineItem.Apttus_Config2__Frequency__c = 'One Time';
                lineItem.Apttus_Config2__PriceType__c = 'One Time';
                lineItem.Apttus_Config2__SellingFrequency__c = 'One Time';
                lineItem.Apttus_Config2__PricingStatus__c = 'Complete';
                lineItem.Apttus_Config2__ConstraintCheckStatus__c = 'NA';
                lineItem.Apttus_Config2__SellingTerm__c = 1;
                lineItem.Apttus_Config2__ItemSequence__c = 1;
                lineItem.Apttus_Config2__Term__c = 1;
                lineItem.Apttus_Config2__ConfigStatus__c = 'NA';
                lineItem.Class_of_Trade_Tier_Item__c = true;

                lineItem.Apttus_Config2__ProductId__c = prod.Id;
                lineItem.Apttus_Config2__Description__c = prod.Description;

                lineItem.Apttus_Config2__ClassificationId__c = productClassificationMap.get(prod.Id).Apttus_Config2__ClassificationId__c;
                lineItem.Apttus_Config2__ConfigurationId__c = configuration.id;
                lineItem.Apttus_Config2__PriceListId__c = priceList.Id;
                //lineItem.Apttus_Config2__SummaryGroupId__c
                //lineItem.Apttus_Config2__AdHocGroupId__c
                lineItem.Apttus_Config2__PriceListItemId__c = priceListItemMap.get(prod.Id).Id;

                lineItem.Apttus_Config2__ListPrice__c = qpCot.Expected_Annual_Customer_Spend__c;
                lineItem.Apttus_Config2__AdjustedPrice__c = qpCot.Expected_Annual_Customer_Spend__c;
                lineItem.Apttus_Config2__BasePrice__c = qpCot.Expected_Annual_Customer_Spend__c;
                lineItem.Apttus_Config2__BaseExtendedPrice__c = qpCot.Expected_Annual_Customer_Spend__c;
                lineItem.Apttus_Config2__ExtendedPrice__c = qpCot.Expected_Annual_Customer_Spend__c;
                lineItem.Apttus_Config2__NetPrice__c = qpCot.Expected_Annual_Customer_Spend__c;

                lineItem.Apttus_Config2__PrimaryLineNumber__c = lineNumber;
                lineItem.Apttus_Config2__LineNumber__c = lineNumber;
                lineItems.add(lineItem);

                lineNumber++;
            }
        }

        // Insert line items
        if (!lineItems.isEmpty()) {
            insert lineItems;
        }

        // Insert QP product prices
        if (!qpProductPrices.isEmpty()) {
            insert qpProductPrices;
        }

        PageReference ref = Page.Apttus_QPConfig__ProposalConfiguration;
        ref.getParameters().put('id', qp.Id);
        ref.getParameters().put('flow', 'SSG Cart');
        ref.getParameters().put('useAdvancedApproval', 'true');
        ref.getParameters().put('useDealOptimizer', 'false');
        ref.setRedirect(true);
        return ref;
    }

    public PageReference doReturnToClassOfTrade() {
        PageReference ref = Page.CPQ_SSG_SelectCOT;
        ref.setRedirect(false);
        return ref;
    }

    public static Date getNewExpectedEndDate(Date startDate, String paymentTerm) {
        Integer termInMonths = 0;
        String termInYearsString;
        try {
            if (paymentTerm != null) {
                termInYearsString = paymentTerm.left(1);
                termInMonths = Integer.valueOf(termInYearsString) * 12;
            }
        } catch (Exception e) {}
        return startDate.addMonths(termInMonths);
    }

    public void compare() {
        COT_Display cot = cotDisplayMap.get(compareCotName);

        CPQ_SSG_QP_Class_of_Trade__c qpCot = qpCotsMap.get(compareCotName);

        if (qpCot != null) {
            qp.Compare_Class_of_Trade__c = qpCot.Id;
            update qp;
        }
    }

    public void doCloseComparisonDialog() {
        COT_Display cot = cotDisplayMap.get(compareCotName);

        CPQ_SSG_QP_Class_of_Trade__c qpCot = qpCotsMap.get(compareCotName);
        CPQ_SSG_QP_Class_of_Trade__c updatedQpCot = [Select Proposed_Pricing_Tier__c, Proposed_Pricing_Tier__r.Name From CPQ_SSG_QP_Class_of_Trade__c Where Id = :qpCot.Id];
        qpCot.Proposed_Pricing_Tier__c = updatedQpCot.Proposed_Pricing_Tier__c;
        cot.proposedTier = updatedQpCot.Proposed_Pricing_Tier__r.Id;
    }

    // Select options for the case where there are no listerine or manual price lists
    public static List<SelectOption> noneOptions {
        get {
            if (noneOptions == null) {
                noneOptions = new List<SelectOption>();
                noneOptions.add(new SelectOption(NONE_TYPE,NONE_TYPE));
            }
            return noneOptions;
        }
        set;
    }

    // Select options for the case where there are only listerine price lists
    public static List<SelectOption> listerineOptions {
        get {
            if (listerineOptions == null) {
                listerineOptions = new List<SelectOption>();
                listerineOptions.add(new SelectOption(NONE_TYPE,NONE_TYPE));
                listerineOptions.add(new SelectOption(LISTERINE_TYPE,LISTERINE_TYPE));
            }
            return listerineOptions;
        }
        set;
    }

    // Select options for the case where there are only manaual price lists
    public static List<SelectOption> manualOptions {
        get {
            if (manualOptions == null) {
                manualOptions = new List<SelectOption>();
                manualOptions.add(new SelectOption(NONE_TYPE,NONE_TYPE));
                manualOptions.add(new SelectOption(MANUAL_TYPE,MANUAL_TYPE));
            }
            return manualOptions;
        }
        set;
    }

    // Select options for the case where there both listerine and manual price lists
    public static List<SelectOption> bothOptions {
        get {
            if (bothOptions == null) {
                bothOptions = new List<SelectOption>();
                bothOptions.add(new SelectOption(NONE_TYPE,NONE_TYPE));
                bothOptions.add(new SelectOption(LISTERINE_TYPE,LISTERINE_TYPE));
                bothOptions.add(new SelectOption(MANUAL_TYPE,MANUAL_TYPE));
            }
            return bothOptions;
        }
        set;
    }

    // Container object for Class of Trade with SelectOption list for Visualforce
    public class COT_Display {
        public String cotId {get;set;}
        public String name {get;set;}
        public String GPOContract {get;set;}
        public String listType {get;set;}
        public String selectOptionsType {get;set;}
        public List<SelectOption> selectOptions {get;set;}
        public List<SelectOption> manualTierOptions {get;set;}
        public List<SelectOption> listerineTierOptions {get;set;}
        public List<SelectOption> tierOptions {get;set;}
        public Map<Id,CPQ_SSG_Pricing_Tier__c> tierIdMap {get;set;}
        public Map<String,CPQ_SSG_Pricing_Tier__c> manualTiersMap {get;set;}
        public Map<String,CPQ_SSG_Pricing_Tier__c> listerineTiersMap {get;set;}
        public Map<String,CPQ_SSG_Pricing_Tier__c> tiersMap {get;set;}
        public Decimal complianceAmount {get;set;}
        public Decimal compliancePercent {get;set;}
        public Boolean includeRebates {get;set;}
        public Decimal allRebate {get;set;}
        public Decimal newRebate {get;set;}
        public Decimal growthRebate {get;set;}
        public Integer allRebateYears {get;set;}
        public Integer newRebateYears {get;set;}
        public Integer growthRebateYears {get;set;}
        public String qualifiedTier {get;set;}
        public String qualifiedTierName {get;set;}
        public String proposedTier {get;set;}
        
        public COT_Display(CPQ_SSG_Class_of_Trade__c cot) {
            this.cotId = cot.Id;
            this.name = cot.name;
            this.listType = 'None';
            this.complianceAmount = 0.00;
            this.compliancePercent = 0.00;
            this.includeRebates= false;
            this.allRebate = 0.00;
            this.newRebate = 0.00;
            this.growthRebate = 0.00;
            this.allRebateYears = 0;
            this.newRebateYears = 0;
            this.growthRebateYears = 0;
            this.GPOContract = 'No';

            Boolean listerine = false;
            Boolean manual = false;

            tierIdMap = new Map<Id,CPQ_SSG_Pricing_Tier__c>();
            listerineTiersMap = new Map<String,CPQ_SSG_Pricing_Tier__c>();
            manualTiersMap = new Map<String,CPQ_SSG_Pricing_Tier__c>();
            listerineTierOptions = new List<SelectOption>();
            manualTierOptions = new List<SelectOption>();
            for (CPQ_SSG_Pricing_Tier__c tier: cot.CPQ_SSG_Pricing_Tiers__r) {
                if (tier.Pricing_Tier_Type__c == LISTERINE_TYPE) {
                    listerine = true;
                    listerineTiersMap.put(tier.name, tier);
                    listerineTierOptions.add(new SelectOption(tier.Id, tier.Description__c));
                } else if (tier.Pricing_Tier_Type__c == MANUAL_TYPE) {
                    manual = true;
                    manualTiersMap.put(tier.name, tier);
                    manualTierOptions.add(new SelectOption(tier.Id, tier.Description__c));
                }
                tierIdMap.put(tier.Id, tier);
            }

            if (manual == true && listerine == true) {
                selectOptionsType = BOTH_TYPE;
                selectOptions = bothOptions;
            } else if (manual == true) {
                selectOptionsType = MANUAL_TYPE;
                selectOptions = manualOptions;
            } else if (listerine == true) {
                selectOptionsType = LISTERINE_TYPE;
                selectOptions = listerineOptions;
            } else {
                selectOptionsType = NONE_TYPE;
                selectOptions = noneOptions;
            }
        }

        public void setQpValues(CPQ_SSG_QP_Class_of_Trade__c qpCot) {
            this.listType = qpCot.List_Type__c;
            this.complianceAmount = qpCot.Expected_Annual_Customer_Spend__c;
            this.compliancePercent = qpCot.Expected_Annual_Compliance__c;
            this.includeRebates = qpCot.Include_Rebates__c;
            this.allRebate = qpCot.All_Products_Rebate__c;
            this.newRebate = qpCot.New_Business_Rebate__c;
            this.growthRebate = qpCot.Growth_Rebate__c;
            this.allRebateYears = Integer.valueOf(qpCot.All_Products_Rebate_Years__c);
            this.newRebateYears = Integer.valueOf(qpCot.New_Business_Rebate_Years__c);
            this.growthRebateYears = Integer.valueOf(qpCot.Growth_Rebate_Years__c);
            this.proposedTier = qpCot.Proposed_Pricing_Tier__c;
            this.qualifiedTier = qpCot.Qualified_Pricing_Tier__c;
            CPQ_SSG_Pricing_Tier__c tier = tierIdMap.get(qpCot.Qualified_Pricing_Tier__c);
            if (tier != null) {
                this.qualifiedTierName = tier.Id;
            }
            this.gpoContract = qpCot.GPO_Contract__c ? 'Yes' : 'No';
            setTiersMap();
        }

        public void setTiersMap() {
            if (this.listType == LISTERINE_TYPE) {
                this.tierOptions = this.listerineTierOptions;
                this.tiersMap = this.listerineTiersMap;
            } else if (this.listType == MANUAL_TYPE) {
                this.tierOptions = this.manualTierOptions;
                this.tiersMap = this.manualTiersMap;
            }
        }
    }
}