@isTest
public class communityPrePopulateControllerTest
{


static testMethod void test2()
 {
 RecordType rt=[select Id, Name from Recordtype where name='US-Hospital Community' limit 1];
 Account a= new Account();
 a.name='samleTest'; 
 a.RecordTypeId =rt.id;
 a.BillingStreet = 'Test Billing Street';
 a.BillingCity = 'Test Billing City';
 a.BillingState = 'Test';
 a.BillingPostalCode = '12345';
 insert a;
 
 RecordType rt1=[select Id, Name from Recordtype where name='US-Healthcare Facility' limit 1];
 Account a1= new Account();
 a1.name='samleTest1'; 
 a1.RecordTypeId =rt1.id;
 a1.BillingStreet = 'Test Billing Street';
 a1.BillingCity = 'Test Billing City';
 a1.BillingState = 'Test';
 a1.BillingPostalCode = '12345';
 insert a1;
 
 /*RecordType rt1=[select Id, Name from Recordtype where name='US - S2 - Community Opportunity' limit 1];
Opportunity opp= new Opportunity();
opp.name='sampleOpp';
opp.accountid=a.id;
opp.StageName='Identify';
 opp.RecordTypeId =rt1.id;
 opp.CloseDate=System.today();
insert opp;
*/

 Profile p = [SELECT Id  FROM Profile WHERE Name ='System Administrator'limit 1]; 

  User usr = new User();
  usr.LastName = 'testLastName';  
  usr.FirstName = 'testFirstName'; 
  usr.Email = 'test@test.com';   
  usr.LanguageLocaleKey = 'en_US';  
  usr.ProfileId = p.id; 
  usr.LocaleSidKey = 'en_US'; 
  usr.TimeZoneSidKey = 'America/New_York'; 
  usr.Username = 'testl@testf.com';  
  usr.Alias = 'test.t';  
  usr.CommunityNickname = 'test.t';
  usr.EmailEncodingKey = 'ISO-8859-1';
  insert usr;
  System.runAs(usr)
  {
  Community_Evaluation__c  ce= new Community_Evaluation__c();
  ce.Community_Name__c=a.id;
ce.Account_Name__c=a1.id;
ce.Approaches__c='open';
ce.Other_Procedure__c='otherprod';

insert ce;

  PageReference pageRef = Page.communityNamePrePopulate; 
  pageRef.getParameters().put('id', a.id);  
  Test.setCurrentPage(pageRef); 
  ApexPages.StandardController sc = new ApexPages.StandardController( a);
  communityPrePopulateController1 testAcc = new  communityPrePopulateController1(sc);
  Test.startTest();
  testAcc.getReq();
  testAcc.getOpp();
  testAcc.Cinical();
  testAcc.evalButtonClick();
  Test.stopTest();
  }
 }


static testMethod void test1()
 {
 RecordType rt=[select Id, Name from Recordtype where name='US-Hospital Community' limit 1];
 Account a= new Account();
 a.name='samleTest'; 
 a.RecordTypeId =rt.id;
 a.BillingStreet = 'Test Billing Street';
 a.BillingCity = 'Test Billing City';
 a.BillingState = 'Test';
 a.BillingPostalCode = '12345';
 insert a;
 
 Account_Affiliation__c aaf = new Account_Affiliation__c();

aaf.Affiliated_with__c =a.id;
aaf.AffiliationMember__c=a.id;

insert aaf;

RecordType rt1=[select Id, Name from Recordtype where name='US - S2 - Community Opportunity' limit 1];
Opportunity opp= new Opportunity();
opp.name='sampleOpp';
opp.accountid=a.id;
opp.StageName='Identify';
 opp.RecordTypeId =rt1.id;
 opp.CloseDate=System.today();
insert opp;

 Profile p = [SELECT Id  FROM Profile WHERE Name ='System Administrator'limit 1]; 

  User usr = new User();
  usr.LastName = 'testLastName';  
  usr.FirstName = 'testFirstName'; 
  usr.Email = 'test@test.com';   
  usr.LanguageLocaleKey = 'en_US';  
  usr.ProfileId = p.id; 
  usr.LocaleSidKey = 'en_US'; 
  usr.TimeZoneSidKey = 'America/New_York'; 
  usr.Username = 'testl@testf.com';  
  usr.Alias = 'test.t';  
  usr.CommunityNickname = 'test.t';
  usr.EmailEncodingKey = 'ISO-8859-1';
  insert usr;
  
  Community_User__c comuser1 = new Community_User__c ();
  comuser1.Hospital_Community__c = a.id; 
  comuser1.SFDC_User__c = usr.id;   
  comuser1.Hospital_Community_User__c = true;   
  insert comuser1;
  
 
  System.runAs(usr)
  {
   /*Clinical_Evaluation__c  ce= new Clinical_Evaluation__c();
  ce.Opportunity_Name__c=opp.id;
ce.Account__c=a.id;

insert ce;
*/
  PageReference pageRef = Page.communityNamePrePopulate; 
  pageRef.getParameters().put('id', a.id);  
  Test.setCurrentPage(pageRef); 
  ApexPages.StandardController sc = new ApexPages.StandardController( a);
  communityPrePopulateController1 testAcc = new  communityPrePopulateController1(sc);
  Test.startTest();
  testAcc.getReq();
  testAcc.getOpp();
  testAcc.Cinical();
  
  Test.stopTest();
  
 }
 }
 }