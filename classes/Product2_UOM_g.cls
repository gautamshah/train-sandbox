/**

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016/10/21      Paul Berglund       Created.
===============================================================================
*/

public class Product2_UOM_g
{
    // We only need to maintain 1 map and use the code
    // to build reverse Map - doesn't require updating two
    // places, making sure spelling is correct in both
    // places, etc.  We utilize 1 default value and the
    // Maps to stay consistent.
    
    @testVisible
    private static string defaultCode = 'EA';
    @testVisible
    private static string defaultText = 'Each';
    
    //=========================================================
    // translate code in UOM_Desc__c to Apttus_Config2__Uom__c
    // value
    //=========================================================
    public static Map<string, string> Code2Text = new Map<string, string>
    {
        'CT' => 'Carton',
            defaultCode => defaultText,
            'BX' => 'Box',
            'CA' => 'Case',
            'PK' => 'Package',
            'GA' => 'Gallon',
            'BG' => 'Bag'
            };
                
                //=========================================================
                // translate Apttus_Config2__Uom__c value to code in
                // UOM_Desc__c
                //=========================================================
                public static Map<string, string> Text2Code = new Map<string, string>();
    
    static
    {
        for(string s : Code2Text.keySet())
            Text2Code.put(Code2Text.get(s), s);
    }
    
    public static String translateCode(string code)
    {
        return string.isBlank(code) ?
            null :
        Code2Text.containsKey(code) ?
            Code2Text.get(code) :
        defaultText;
    }
    
    public static String translateText(string text)
    {
        return string.isBlank(text) ?
            null :
        Text2Code.containsKey(text) ?
            Text2Code.get(text) :
        defaultCode;
    }
}