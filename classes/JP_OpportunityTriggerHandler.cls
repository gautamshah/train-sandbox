public with sharing class JP_OpportunityTriggerHandler{
     /****************************************************************************************
     * Name    : JP_OpportunityTriggerHandler 
     * Author  : Hiroko Kambayashi
     * Date    : 10/11/2012 
     * Purpose : Trigger Handler to update Intimacy Information in Opportunity record.
     *           
     * Dependencies: Opportunity Object
     *             
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 11/08/2012  Hiroko Kambayashi    Modifying the method "onBeforeInsert"
     *             Hiroko Kambayashi    Adding the method "onAfterUpdate"
     * 02/25/2013  Hiroko Kambayashi    Revise the conditionals.(Because of the bug at Producttion)
     * 06/16/2013  Bill Shan            Stop the opp close date update by SFDC when opp is closed
     *
     *****************************************************************************************/
     
    private Boolean isExecuting = false;
    private Integer batchSize = 0;
    /**
     * Opportunity TriggerHandler Constructor
     * @param Boolean isExecute
     * @param Integer size
     */
    public JP_OpportunityTriggerHandler(Boolean isExecute, Integer size){
        this.isExecuting = isExecute;
        this.batchSize = size;
    }
    /**
     * Handling of Opportunity BeforeInsert Trigger
     * @param triggerNewList     NewOpportunity handed in Trigger.new
     * @return none
     */
    public void onBeforeInsert(List<Opportunity> triggerNewList){
        
        JP_OpportunityBusiness business = new JP_OpportunityBusiness();
        //Opportunity List for set JP_Intimacy__c info
        List<Opportunity> inputIntimacyList = new List<Opportunity>();
        Set<String> intimacyUniqueKey = new Set<String>();
        
        //Opportunity List for set Account_Extended_Profile__c info
        List<Opportunity> inputBUInfoList = new List<Opportunity>();
        Set<String> accBunits = new Set<String>();
        
        for(Opportunity opp : triggerNewList){
            if(opp.JP_RecordTypeName_Formula__c != null && opp.JP_RecordTypeName_Formula__c.startsWith(System.Label.JP_Opportunity_RecordTypeName_Prefix)){
                //For set JP_Intimacy__c info
                if(opp.JP_Key_Dr__c != null){
                    inputIntimacyList.add(opp);
                    intimacyUniqueKey.add(opp.JP_Key_Dr_Owner__c);
                }
                //For set Account_Extended_Profile__c Id
                if(opp.JP_Account_Extended_Profile__c == null && (opp.JP_Business_Unit__c != null || opp.JP_Business_Unit__c != '')){
                    inputBUInfoList.add(opp);
                    accBunits.add(opp.AccountId + '|' + opp.JP_Business_Unit__c);
                }
            }
        }
        //Execution of setting JP_Intimacy__c info to Opportunity record
        if(!inputIntimacyList.isEmpty()){
            //business.inputIntimacyByKeyDoctor(inputIntimacyList, intimacyUniqueKey);
        }
        //Execution of setting Account_Extended_Profile__c Id to Opportunity record
        if(!inputBUInfoList.isEmpty()){
            //business.inputBUInfoByBusinessUnit(inputBUInfoList, accBunits);
        }
    }
    /**
     * Handling of Opportunity BeforeUpdate Trigger
     * @param triggerOldMap     Opportunity info before update handed in Trigger.oldMap
     * @param triggerNewList    NewOpportunity handed in Trigger.new
     * @return none
     */
    public void onBeforeUpdate(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNewList){
        JP_OpportunityBusiness business = new JP_OpportunityBusiness();
        //Opportunity List for set JP_Intimacy__c info
        List<Opportunity> inputIntimacyList = new List<Opportunity>();
        Set<String> intimacyUniqueKey = new Set<String>();
        
        Boolean firstTimeEnter = !SingleExecution.hasAlreadyDone();
        
        if(firstTimeEnter)
        {           
            SingleExecution.setAlreadyDone();
        }
        
        for(Opportunity newOpp : triggerNewList){
            //Get Opportunity info before update
            Opportunity oldOpp = triggerOldMap.get(newopp.Id);
        
            //Update the Opportunity close date
            if(firstTimeEnter)
            {
                newOpp.NeedRollBackCloseDate__c = false;
                if(newOpp.IsWon && newOpp.closeDate == oldOpp.closeDate)
                {
                    newOpp.NeedRollBackCloseDate__c = true;
                }
            }
                
            if(newOpp.JP_RecordTypeName_Formula__c != null && newOpp.JP_RecordTypeName_Formula__c.startsWith(System.Label.JP_Opportunity_RecordTypeName_Prefix)){
                //For set JP_Intimacy__c info
                if(oldOpp.JP_Key_Dr__c != newOpp.JP_Key_Dr__c || oldOpp.OwnerId != newOpp.OwnerId){
                    inputIntimacyList.add(newOpp);
                    intimacyUniqueKey.add(newOpp.JP_Key_Dr_Owner__c);
                }
            }
        }
        //Execution of setting JP_Intimacy__c info to Opportunity record
        if(!inputIntimacyList.isEmpty()){
            //business.inputIntimacyByKeyDoctor(inputIntimacyList, intimacyUniqueKey);
        }
    }
    /**
     * Handling of Opportunity AfterUpdate Trigger
     * @param triggerOldMap     Opportunity info after update handed in Trigger.oldMap
     * @param triggerNewList    NewOpportunity handed in Trigger.new
     * @return none
     */
    public void onAfterUpdate(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNewList){
        JP_OpportunityBusiness business = new JP_OpportunityBusiness();
        //Opportunity Id Set for delete and insert OpportunityLineItem records when Field JP_Sales_start_Date__c is changed on Opportunity
        Set<Id> reDivideIds = new Set<Id>();
        for(Opportunity newOpp : triggerNewList){
            //Get Opportunity info before update
            Opportunity oldOpp = triggerOldMap.get(newopp.Id);
            if(newOpp.JP_RecordTypeName_Formula__c != null && newOpp.JP_RecordTypeName_Formula__c.startsWith(System.Label.JP_Opportunity_RecordTypeName_Prefix)){
                if(newOpp.of_Products__c > 0 && oldOpp.JP_Sales_start_Date__c != newOpp.JP_Sales_start_Date__c){
                    reDivideIds.add(newOpp.Id);
                }
            }
        }
        //Execution of delete and insert OpportunityLineItem records(Then, JP_OpportunityLineItemTrigger is run)
        if(reDivideIds.size() > 0){
            business.reDevideRevenueSchaduleBySalesStartDate(reDivideIds);
        }
    }
}