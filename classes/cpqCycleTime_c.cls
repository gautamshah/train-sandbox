public class cpqCycleTime_c
{
    public static Id dummyId(integer value)
    {
        string prefix = CPQ_Cycle_Time__c.sObjectType.getDescribe().getKeyPrefix();
		string suffix = string.valueOf(value).leftPad(15, '0');
		Id dummy = Id.valueOf(prefix + suffix);
        return dummy;
    }
}