global class TerritoryBatchSchedule implements Schedulable{
    
    /****************************************************************************************
     * Name    : TerritoryBatchSchedule 
     * Author  : Suchin Rengan
     * Date    : 06/05/2011  
     * Purpose : Schedular for the EstablishTerritoryRelationship class 
     *           
     * Dependencies: EstablishTerritoryRelationship Apex Class
     *
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 06/05/2011  Suchin Rengan        Created
     *
     *****************************************************************************************/
    global void execute(SchedulableContext ctx) 
    {
     
        EstablishTerritoryRelationship batchApex = new EstablishTerritoryRelationship();
        String schedule = '0 0 * * * ?';
        List<AsyncApexJob> openJobs = [select Id from AsyncApexJob where Status = 'Processing' OR Status = 'Queued']; 
        
        //if(openJobs.size() < 5)
        if(openJobs.size() < 50)
        {
            ID batchprocessid = Database.executeBatch(batchApex);
        }
    }

}