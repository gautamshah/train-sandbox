public class cpqUser_c extends User_c
{
	private static User_g cache = new User_g();
    
	public static set<string> fieldsToInclude =
		new set<string>
		{
			'Id',
			'Name',
			'Profile.Id',
			'Profile.Name',
			'OrganizationName__c',
			'SalesOrganizationLevel__c',
			'Business_Unit__c',
			'Region__c',
			'Manager.Id',
			'Manager.OrganizationName__c',
			'Manager.SalesOrganizationLevel__c',
			'Manager.Manager.Id',
			'Manager.Manager.OrganizationName__c',
			'Manager.Manager.SalesOrganizationLevel__c',
			'Manager.Manager.Manager.Id',
			'Manager.Manager.Manager.OrganizationName__c',
			'Manager.Manager.Manager.SalesOrganizationLevel__c'
		};

	public static final string whereValidCPQUser =
    	// A valid CPQ User has the following:
    	// - OrganizationName__c
    	// - SalesOrganizationLevel__c
		' WHERE OrganizationName__c != null AND SalesOrganizationLevel__c != null AND SalesOrganizationLevel__c != \'N/A\' ';

	static
	{
		load();
	}
	
	////
	//// load
	////
	public static void load()
	{
		if (cache.isEmpty())
		{
			List<User> fromDB = fetchFromDB();
			cache.put(fromDB);
		}
	}
	
	////
	//// fetch
	////
    public static User CurrentUser
    {
    	get
    	{
    		if (CurrentUser == null || CurrentUser.Id != UserInfo.getUserId())
    			CurrentUser = fetchFromDB(UserInfo.getUserId());
    			
    		return CurrentUser;
    	}
    	
    	private set;
    }

	public static OrganizationNames_g.Abbreviations getOrganizationName(User u)
	{
		return (u != null & u.OrganizationName__c != null) ?
					OrganizationNames_g.valueOf(u.OrganizationName__c) :
					null;
	}

	public static boolean isValid(User u)
	{
		return u.OrganizationName__c != null &&
		       u.SalesOrganizationLevel__c != null;
	}
	
	public static User fetch(Id id)
	{
		
		if (cache.fetch(UserInfo.getUserId()) == null)
           	cache.put(fetchFromDB(UserInfo.getUserId()));
        
        return cache.fetch(UserInfo.getUserId());
	}

	////
	//// fetchFromDB
	////
	@testVisible
    private static List<User> fetchFromDB()
    {
    	string query =
			SOQL_select.buildQuery(
				User.sObjectType,
				fieldsToInclude,
				whereValidCPQUser + ' AND isActive = true ',
				null,
				null);
    	
		return (List<User>)Database.query(query);
    }

	@testVisible
    private static User fetchFromDB(Id id)
    {
    	string query =
			SOQL_select.buildQuery(
				User.sObjectType,
				fieldsToInclude,
				' WHERE Id = :id AND isActive = true ',
				null,
				1);
    	
		return (User)Database.query(query);
    }
}