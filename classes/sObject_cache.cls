/*
	This class can be used to create a "cache" for a particular sObject.  It can be used within
	another class for a local "cache" to avoid multiple SOQL queries, or it can be used to setup
	a global cache for a particular sObject (classes that end in _g).

	You can add sObjectFields related to the sObject to create "indexes" by that field.  Keep in mind
	that Salesforce doesn't let you create a Map with a null key - so if it's null, you it is not
	indexed.

	If you setup a global cache - put the casting from sObject to the specific sObjectType in
	the _g class.

	TODO
	====
	Create a "null" index by converting null into a placeholder value and then add it as an index
	- This works for strings, but other primitive types will need to be thought out
	  - Id - (dummyId?)
	  - Numbers?
*/
/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.	If there are multiple related Jiras, add them on the next line
5.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date-Id		Initials	Jira(s)		Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20170220-A	PAB			AV-280		Extracted all the "caching" code into it's own class
									and updated the following classes to utlize it:
									cpqConfigSetting_c
20170302-A  PAB         AV-280		Removing the ability to add "external" fields to indexes
                                    to simplify the caching.  This was being done to support
                                    Territory/UserTerritory relationships, but it doesn't have
                                    to be done in the cache
20170302-B	PAB			AV-280		Changed sObjectFields to a Map<string, sObjectField> so there
                                    is less overhead.  It is now just a pointer to the cache of
                                    .getMap() in the sObject_c class.  It is easier to do a 
                                    .containsKey(.name()) rather than convert the entire .values()
                                    to a Set<sObjectField> to be able to do a "contains"
20170302-C	PAB			AV-280		Before we can use a sObjectField as an index, it must not
									be an Id and must belong to the current sObject - this logic
									is repeated in multiple locations and was inconsitent.  Moved
									The logic into the fieldIsIndexed() and added a call to this
									anywhere an index is used
20170302-D
20170302-E	PAB			AV-280		A fetch should only return entries where the sObject is not null
                                    That indicates that the cache does not contain that sObject
20170302-F	PAB			AV-280		All fetch methods should return a Map - except for fetch(id),
                                    it retuns a null if there is no sObject in the cache.
*/
public abstract class sObject_cache
{
	protected DateTime timestamp;

	public sObjectType sObjType { get; private set; }
	public sObjectField IdField { get; private set; }

	public DateTime created { get; private set; }

	@testVisible
	private Map<Id, sObject> cache = new Map<Id, sObject>();

	// indexed field (sObjectField)
	// key values in the indexed field (object)
	// records associated with the key Map<Id, sObject>
	//
	// indexes.get(sObjectField) => sObjectField.get(object) => Map<Id, sObject>
	//
	// 1 or more indexed fields containing (sObjectField)
	//    1 or more key values which index (object)
	//       1 or more records (Map<Id, sObject>)
	//
	@testVisible
	private Map<sObjectField, Map<object, Map<Id, sObject>>> indexes =
				new Map<sObjectField, Map<object, Map<Id, sObject>>>();
	
	@testVisible
	private Set<sObjectField> indexedFields() { return indexes.keySet(); }

	@testVisible
	private Map<string, sObjectField> sObjectFields { get; private set; } //// 20170302-B

	@testVisible
	private class sObjectFieldNotIndexedException extends Exception { }
	@testVisible
	private class sObjectNotValidException extends Exception { }
	@testVisible
	private class sObjectFieldNotValidForSObjectException extends Exception { }
	@testVisible
	private class sObjectNeedsIdException extends Exception { }
	@testVisible
	private class sObjectQueryByIdFieldException extends Exception { }

	public sObject_cache(sObjectType sObjType, sObjectField IdField)
	{
		this.sObjType = sObjType;
		this.IdField = IdField;
		this.sObjectFields = sObject_c.getSObjectFields(sObjType);
		this.created = system.now();

		DEBUG.constructorTitleBlock(string.valueOf(this.sObjType));
	}

	////
	//// clear
	////
	//// This simply clears the cache & indexes maps
	public void clear()
	{
		this.cache.clear();
		for(sObjectField index : indexedFields())
			indexes.get(index).clear();
	}

	////
	//// Why List instead of Set?
	////    We work with Maps - you can't convert a Set to a Map
	////    Most of the code uses .values() which is a List
	////    So we decided to use List instead of Set
	////
	//// put
	////
	//// This adds a single sObject to the cache
	public void put(sObject sobj)
	{
		//// The cache is a Map
		Map<Id, sObject> sobjsMap = new Map<Id, sObject>{ sobj.Id => sobj };
		put(sobjsMap);
	}

	//// This adds a List of sObjects to the cache
	public void put(List<sObject> sobjs)
	{
		Map<Id, sObject> sobjsMap = new Map<Id, sObject>( sobjs );
		put(sobjsMap);
	}

	//// This adds a native Map to the cache
	public void put(Map<Id, sObject> sobjs)
	{
		cache.putAll(sobjs);

		putIndex(sobjs);
		
		dump('sObject_cache.put(Map<Id, sObject> sobjs)');
	}

	////
	//// fetch
	////
	public Map<Id, sObject> fetch()
	{
		return cache;
	}

	public sObject fetch(Id id)
	{
		return (cache.containsKey(id)) ? cache.get(id) : null;
	}

	//// 20170302-E
	public Map<Id, sObject> fetch(set<Id> ids)
	{
		Map<Id, sObject> found = new Map<Id, sObject>();

		for(Id id : ids)
		{
			sObject sobj = fetch(id);

			if (sobj != null)
				found.put(id, fetch(id));
		}

		return found;
	}

	////
	//// fetch using sObjectField
	////
	public Map<object, Map<Id, sObject>> fetch(sObjectField field)
	{
		return fieldIsIndexed(field) ? indexes.get(field) : new Map<object, Map<Id, sObject>>();
	}

	public Map<object, Map<Id, sObject>> fetch(sObjectField index, set<object> keys)
	{
		Map<object, Map<Id, sObject>> found = new Map<object, Map<Id, sObject>>();

		if (fieldIsIndexed(index))
		{
			Map<object, Map<Id, sObject>> indexed = indexes.get(index);
			
			for(object key : keys)
				if (indexed.containsKey(key))
					found.put(key, indexed.get(key));
		}

		return found;
	}

	public Map<Id, sObject> fetch(sObjectField field, object value)
	{
		if (fieldIsIndexed(field))
		{		
			Map<object, Map<Id, sObject>> results = fetch(field);
			
			return results.containsKey(value) ? results.get(value) : new Map<Id, sObject>();
		}
		else
			return new Map<Id, sObject>();
	}

	////
	//// Index methods
	////
	//// This is protected to force the addition to occur
	//// in the constructors
	////
	//// 20170302-A
	//// 20170302-B
	//// 20170302-C
	protected void addIndex(sObjectField field)
	{
		if (!fieldIsIndexed(field))
		{
			this.indexes.put(field, new Map<object, Map<Id, sObject>>());

			//// If we add an index and there are existing
			//// entries, we need to index them
			if (!this.cache.isEmpty())
				addIndex(field, this);
		}
	}

	//// This should be quicker
	////
	//// index (index a set of objects)
	////
	//// 20170302-C This is a special index method, it bypasses
	////            the check to see if a field is indexed
	////            under the assumption it has already been
	////            checked.  It should only be used by the
	////			addIndex.
	private void addIndex(sObjectField indexedField, sObject_cache self)
	{
		Map<object, Map<Id, sObject>> index = indexes.get(indexedField);
		
		for(Id sobjId : this.cache.keySet())
		{
			sObject sobj = this.cache.get(sobjId);
			object key = sobj.get(indexedField);

			if (key != null)
			{
				if (!index.containsKey(key))
					index.put(key, new Map<Id, sObject>());

				index.get(key).put(sobjId, sobj);
			}
		}
	}

	//// 20170302-D This should only be used by put()
	////            methods, it loops through existing
	////            indexes and updates them with the
	////            information in the new sObjects
	private void putIndex(Map<Id, sObject> sobjs)
	{
		for(Id sobjId : sobjs.keySet())
		{
			for(sObjectField indexedField : indexes.keySet())
			{
				Map<object, Map<Id, sObject>> index = indexes.get(indexedField);
				
				//// If it's already indexed, move on to the next one
				//// When working with cached sObjects - they have
				//// Id's so any changes you make will be reflected
				//// anywhere the cache sObject is used
				if (!index.containsKey((sobjId)))
				{
					sObject sobj = sobjs.get(sobjId);
					object key = sobj.get(indexedField);
				
					//// Only index non-null values - you can't
					//// have a 'null' key
					//// TODO: If we find we need to get by null
					////       values, we would have to use a different
					////       mechanism to index those fields with
					////       additional logic to track them (maybe a
					////       a set<Id> for the null entries - but we
					////       would need to be able to handle when the
					////	   field is updated.
					//// TODO: Handle when a field is updated - this would
					////       impact which key points to the sObject.  May
					////       require adding method that is called by the
					////       sObject trigger to update the index
					if (key != null)
					{
						if (!index.containsKey(key))
							index.put(key, new Map<Id, sObject>());
						
						index.get(key).put(sobjId, sobj);
					}
				} 
			}
		}
	}

	////
	//// Cache related methods
	////
	public boolean isEmpty()
	{
		return this.cache.isEmpty();
	}

	public boolean containsKey(Id id)
	{
		return this.cache.containsKey(id);
	}

	public boolean containsAllKeys(set<id> ids)
	{
		return containsAll(ids);
	}

	public boolean containsAll(set<id> ids)
	{
		return this.cache.keySet().containsAll(ids);
	}

	private boolean fieldIsIndexed(sObjectField field)
	{
		if (field == IdField)
			throw new sObjectQueryByIdFieldException();

		else if (!this.sObjectFields.containsKey(sObject_c.getSObjectFieldDescribe(field).getName()))
			throw new sObjectFieldNotValidForSObjectException();
					
		else
			return indexes.containsKey(field);
	}

	public override string toString()
	{
		string str = '';
		str += 'sObjectType: ' + this.sObjType + CONSTANTS.CRLF;
		str += 'Created: ' + this.created + CONSTANTS.CRLF;
		str += 'Cache size: ' + this.cache.size() + CONSTANTS.CRLF;
		str += 'Indexes: ' + indexes.size() + CONSTANTS.CRLF;
		integer indexNdx = 0;
		for(sObjectField f : indexes.keySet())
		{
			str += CONSTANTS.NBSP(3) + sObject_c.getSObjectFieldDescribe(f).getName() + ' size: ' + indexes.get(f).size() + CONSTANTS.CRLF;
			Map<object, Map<Id, sObject>> anIndex = indexes.get(f);

			integer keysNdx = 0;
			for(object key : anIndex.keySet())
			{
				str += CONSTANTS.NBSP(6) + key + ' # of records: ' + anIndex.get(key).size() + CONSTANTS.CRLF;
				if (keysNdx++ > 5) break;
			}
			if (indexNdx++ > 5) break;
		}

		return str;
	}

	public void dump(string location)
	{
		DEBUG.dump(
			new map<object, object>
			{
				'location' => location,
				'this' => this
			});

		DEBUG.logLimits();
	}
}