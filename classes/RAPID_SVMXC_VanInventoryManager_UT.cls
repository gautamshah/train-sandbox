@isTest(SeeAllData = true)
public class RAPID_SVMXC_VanInventoryManager_UT{
    static testMethod void setupData(){
    
        //Insert account
        Account acc = new Account(
                                Name = 'Test Account',
                                BillingStreet = '714 test street',
                                BillingCity = 'Chesterfield',
                                BillingState = 'MO',
                                BillingPostalCode = '12345',
                                BillingCountry = 'United States',
                                ShippingStreet = '714 test street',
                                ShippingCity = 'Chesterfield',
                                ShippingState = 'MO',
                                ShippingPostalCode = '12345',
                                ShippingCountry = 'US');
        insert acc; 
        
        Contact  ct = new Contact(LastName='Last',AccountId =acc.Id); 
        insert ct; 
        
        //Insert Inventpry location
        SVMXC__Site__c site = new SVMXC__Site__c(
                                Name = 'Test Location',
                                SVMXC__Account__c = acc.id,
                                SVMXC__Stocking_Location__c = true,
                                SVMXC__State__c = 'NY',
                                SVMXC__Service_Engineer__c = userInfo.getUserId(),
                                SVMXC__Country__c = 'United States',
                                SVMXC__Zip__c = '12345');
        insert site;
        
        //Insert sertialsed product
        Product2 serializedProd = new Product2(
                        Name = 'Test Product',
                        IsActive = true,
                        SVMXC__Tracking__c = 'Serialized',
                        SVMXC__Stockable__c = true,
                        SVMXC__Enable_Serialized_Tracking__c = true);
        insert serializedProd;
        
        //Insert sertialsed product
        Product2 nonSerializedProd = new Product2(
                        Name = 'Test Product',
                        IsActive = true,
                        SVMXC__Tracking__c = 'Non-Tracked',
                        SVMXC__Stockable__c = true);
        insert nonSerializedProd;
        
        Product2 lotbatchProd = new Product2(
                        Name = 'Test Product',
                        IsActive = true,
                        SVMXC__Tracking__c = 'Lot/Batch Tracked',
                        SVMXC__Stockable__c = true,
                        SVMXC__Enable_Serialized_Tracking__c = false);
        insert lotbatchProd;
        
        SVMXC__Installed_Product__c instprod1 =  new SVMXC__Installed_Product__c (
                                            Name='IB1',
                                            SVMXC__Serial_Lot_Number__c='IBs1',
                                            SVMXC__Product__c=serializedProd.id ,
                                            SVMXC__Company__c=acc.id,
                                            SVMXC__Contact__c=ct.id,
                                            SVMXC__Site__c =site.id,
                                            SVMXC__Status__c='Installed');
        Insert instprod1;  
        SVMXC__Installed_Product__c instprod2 =  new SVMXC__Installed_Product__c (
                                            Name='IB1',
                                            SVMXC__Serial_Lot_Number__c='IBs1',
                                            SVMXC__Product__c=nonSerializedProd.id ,
                                            SVMXC__Company__c=acc.id,
                                            SVMXC__Contact__c=ct.id,
                                            SVMXC__Site__c =site.id,
                                            SVMXC__Status__c='Installed');
        Insert instprod2;  
        SVMXC__Installed_Product__c instprod3 =  new SVMXC__Installed_Product__c (
                                            Name='IB1',
                                            SVMXC__Serial_Lot_Number__c='IBs1',
                                            SVMXC__Product__c=lotbatchProd.id ,
                                            SVMXC__Company__c=acc.id,
                                            SVMXC__Contact__c=ct.id,
                                            SVMXC__Site__c =site.id,
                                            SVMXC__Status__c='Installed');
        Insert instprod3;  
        
        //Insert service team anfd a technician for it
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c(
                                                    Name = 'Test Service Team',
                                                    SVMXC__Active__c = true,
                                                    SVMXC__State__c = 'NY',
                                                    SVMXC__Country__c = 'United States',
                                                    SVMXC__Zip__c = '12345');
        insert serviceTeam;
        
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(
                                                        SVMXC__Active__c = true,
                                                        Name = 'Test Technician',
                                                        SVMXC__Service_Group__c = serviceTeam.Id,
                                                        SVMXC__Salesforce_User__c = userInfo.getUserId(),
                                                        SVMXC__Inventory_Location__c = site.Id);
        insert technician;
        
        List<SVMXC__Product_Stock__c> productStockList = new List<SVMXC__Product_Stock__c>();
        //Insert Product Stock for serialized product
        SVMXC__Product_Stock__c serializedProdStock = new SVMXC__Product_Stock__c(
                                                    SVMXC__Location__c = site.Id,
                                                    SVMXC__Product__c = serializedProd.Id,
                                                    SVMXC__Status__c = 'Available',
                                                    SVMXC__Quantity2__c = 2);
        productStockList.add(serializedProdStock);
 
        //Insert Product Stock for serialized product
        SVMXC__Product_Stock__c nonSerializedProdStock = new SVMXC__Product_Stock__c(
                                                    SVMXC__Location__c = site.Id,
                                                    SVMXC__Product__c = nonSerializedProd.Id,
                                                    SVMXC__Status__c = 'Available',
                                                    SVMXC__Quantity2__c = 10);
        productStockList.add(nonSerializedProdStock);
        
        //Insert Product Stock for serialized product
        SVMXC__Product_Stock__c nonSerializedDefProdStock = new SVMXC__Product_Stock__c(
                                                    SVMXC__Location__c = site.Id,
                                                    SVMXC__Product__c = nonSerializedProd.Id,
                                                    SVMXC__Status__c = 'Defective',
                                                    SVMXC__Quantity2__c = 3);
        productStockList.add(nonSerializedDefProdStock);
        
        //Insert Product Stock for lot/batch product
        SVMXC__Product_Stock__c lotBatchProdStock = new SVMXC__Product_Stock__c(
                                                    SVMXC__Location__c = site.Id,
                                                    SVMXC__Product__c = lotbatchProd.id,
                                                    SVMXC__Status__c = 'Available',
                                                    SVMXC__Quantity2__c = 3);
        productStockList.add(lotBatchProdStock);
        
        insert productStockList;
         
        //Insert stock serials for serialized product stock
        List<SVMXC__Product_Serial__c> stockedSerialList = new List<SVMXC__Product_Serial__c>();
        SVMXC__Product_Serial__c stockSerial1 = new SVMXC__Product_Serial__c(
                                                SVMXC__Active__c = true,
                                                SVMXC__Product__c = serializedProd.id,
                                                SVMXC__Product_Stock__c = serializedProdStock.id);
        stockedSerialList.add(stockSerial1);
        SVMXC__Product_Serial__c stockSerial2 = new SVMXC__Product_Serial__c(
                                                SVMXC__Active__c = true,
                                                SVMXC__Product__c = serializedProd.id,
                                                SVMXC__Product_Stock__c = serializedProdStock.id);
        stockedSerialList.add(stockSerial2);
        
        SVMXC__Product_Serial__c stockSerial3 = new SVMXC__Product_Serial__c(
                                                SVMXC__Active__c = true,
                                                SVMXC__Product__c = lotbatchProd.id,
                                                SVMXC__Product_Stock__c = lotBatchProdStock.id,
                                                Name = 'B1');
        stockedSerialList.add(stockSerial3);
        SVMXC__Product_Serial__c stockSerial4 = new SVMXC__Product_Serial__c(
                                                SVMXC__Active__c = true,
                                                SVMXC__Product__c = lotbatchProd.id,
                                                SVMXC__Product_Stock__c = lotBatchProdStock.id,
                                                Name = 'B1');
        stockedSerialList.add(stockSerial4);
        
        SVMXC__Product_Serial__c stockSerial5 = new SVMXC__Product_Serial__c(
                                                SVMXC__Active__c = true,
                                                SVMXC__Product__c = lotbatchProd.id,
                                                SVMXC__Product_Stock__c = lotBatchProdStock.id,
                                                Name = 'B2');
        stockedSerialList.add(stockSerial5);
        
        insert stockedSerialList;
        
        //Insert Work Orders
        List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c workOrder1 = new SVMXC__Service_Order__c();
        workOrder1.SVMXC__Order_Status__c = 'Open';
        workOrder1.SVMXC__Component__c = instprod1.id;
        workOrder1.SVMXC__Group_Member__c = technician.id;
        workOrderList.add(workOrder1);
        
        SVMXC__Service_Order__c workOrder2 = new SVMXC__Service_Order__c();
        workOrder2.SVMXC__Order_Status__c = 'Open';
        workOrder2.SVMXC__Component__c = instprod2.id;
        workOrder2.SVMXC__Group_Member__c = technician.Id;
        workOrderList.add(workOrder2);
        
        SVMXC__Service_Order__c workOrder3 = new SVMXC__Service_Order__c();
        workOrder3.SVMXC__Order_Status__c = 'Open';
        workOrder3.SVMXC__Component__c = instprod3.id;
        workOrder3.SVMXC__Group_Member__c = technician.Id;
        workOrderList.add(workOrder3);
        
        insert workOrderList;
        
        
        //Insert work details
        RecordType usageRecordType = [Select Id from RecordType where Name = 'Usage/Consumption' And SObjectType = 'SVMXC__Service_Order_Line__c'];
        List<SVMXC__Service_Order_Line__c> workDetailList = new List<SVMXC__Service_Order_Line__c>();
        
        SVMXC__Service_Order_Line__c workDetail1 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Line_Type__c = RAPID_SVMXC_Define.PART_LINE_TYPE,
                                                    SVMXC__Line_Status__c = RAPID_SVMXC_Define.OPEN_LINE_STATUS,
                                                    SVMX_Product_Stock__c = serializedProdStock.id,
                                                    SVMXC__Product__c = serializedProd.Id,
                                                    SVMX_Stocked_Serial__c = stockSerial1.id);
        workDetailList.add(workDetail1);
        
        SVMXC__Service_Order_Line__c workDetail2 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder2.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Line_Type__c  = RAPID_SVMXC_Define.PART_LINE_TYPE,
                                                    SVMXC__Line_Status__c = RAPID_SVMXC_Define.OPEN_LINE_STATUS,
                                                    SVMXC__Product__c = nonSerializedProd.Id,
                                                    SVMX_Product_Stock__c = nonSerializedProdStock.id);
        workDetailList.add(workDetail2);
        
        SVMXC__Service_Order_Line__c workDetail3 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder3.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Line_Type__c  = RAPID_SVMXC_Define.PART_LINE_TYPE,
                                                    SVMXC__Line_Status__c = RAPID_SVMXC_Define.OPEN_LINE_STATUS,
                                                    SVMXC__Product__c = serializedProd.Id,
                                                    SVMX_Product_Stock__c = serializedProdStock.id,
                                                    SVMX_Stocked_Serial__c = stockSerial1.id,
                                                    SVMX_Return_Part__c = serializedProd.id,
                                                    SVMX_Serial_Number__c = '123');
        workDetailList.add(workDetail3);
        
        SVMXC__Service_Order_Line__c workDetail4 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder3.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Line_Type__c  = RAPID_SVMXC_Define.PART_LINE_TYPE,
                                                    SVMXC__Line_Status__c = RAPID_SVMXC_Define.OPEN_LINE_STATUS,
                                                    SVMXC__Product__c = serializedProd.Id,
                                                    SVMX_Product_Stock__c = serializedProdStock.id,
                                                    SVMX_Stocked_Serial__c = stockSerial1.id,
                                                    SVMX_Return_Part__c = nonSerializedProd.id);
        workDetailList.add(workDetail4);
        
        SVMXC__Service_Order_Line__c workDetail5 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder3.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Line_Type__c  = RAPID_SVMXC_Define.PART_LINE_TYPE,
                                                    SVMXC__Line_Status__c = RAPID_SVMXC_Define.OPEN_LINE_STATUS,
                                                    SVMXC__Product__c = lotbatchProd.Id,
                                                    SVMX_Product_Stock__c = lotBatchProdStock.id,
                                                    SVMX_Stocked_Serial__c = stockSerial4.id,
                                                    SVMXC__Actual_Quantity2__c = 2,
                                                    SVMX_Return_Part__c = lotbatchProd.id,
                                                    SVMX_Serial_Number__c = 'B5');
        workDetailList.add(workDetail5);
        
        insert workDetailList;
        
        workOrderList = [select id, SVMXC__Order_Status__c, SVMX_Consumption_Lines__c, SVMXC__Group_Member__c, SVMX_Revert_Status_on_Error__c from SVMXC__Service_Order__c where id in (:workOrder1.id,:workOrder2.id, :workOrder3.id)];
        
        workOrderList[0].SVMXC__Order_Status__c = 'Closed'; 
        workOrderList[0].NetReport_Status__c = 'Complete';        
        workOrderList[1].SVMXC__Order_Status__c = 'Closed';
        workOrderList[1].NetReport_Status__c = 'Complete';
        workOrderList[2].SVMXC__Order_Status__c = 'Closed';
        workOrderList[2].NetReport_Status__c = 'Complete';
        update workOrderList;
    }
}