public class cpqProposal_FIELD
{
 	public static final sObjectField
 	
 		MasterAgreementId =
 						Apttus_Proposal__Proposal__c.fields.Apttus_QPComply__MasterAgreementId__c,
 		Duration 							= Apttus_Proposal__Proposal__c.fields.Duration__c,
 		BuyoutAsOfDate 						= Apttus_Proposal__Proposal__c.fields.Buyout_as_of_Date__c,
 		BuyoutAmount 						= Apttus_Proposal__Proposal__c.fields.Buyout_Amount__c,
 		TaxGrossNetOfTradein 				= Apttus_Proposal__Proposal__c.fields.Tax_Gross_Net_of_TradeIn__c,
 		SalesTaxPercent =
 						Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Sales_Tax_Percent__c,
 		TBDAmount 							= Apttus_Proposal__Proposal__c.fields.TBD_Amount__c,
 		TotalAnnualSensorCommitmentAmount =
 						Apttus_Proposal__Proposal__c.fields.Total_Annual_Sensor_Commitment_Amount__c,
 		Opportunity 						= Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Opportunity__c,
 		Account 							= Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Account__c,
		OwnerId								= Apttus_Proposal__Proposal__c.fields.OwnerId,
		Approver_Seller 					= Apttus_Proposal__Proposal__c.fields.SellerApprover__c,
		Approver_RSM 						= Apttus_Proposal__Proposal__c.fields.RSM_Approver__c,
		Approver_ZVP 						= Apttus_Proposal__Proposal__c.fields.ZVP__c,
		Approver_SVP 						= Apttus_Proposal__Proposal__c.fields.SVP_User__c,
		ApprovalIndicator					= Apttus_Proposal__Proposal__c.fields.approval_indicator__c,
		Name =
						Apttus_Proposal__Proposal__c.fields.Apttus_Proposal__Proposal_Name__c,
    	ElectrosurgeryEaches				= Apttus_Proposal__Proposal__c.fields.Electrosurgery_Eaches__c,
    	ElectrosurgerySales					= Apttus_Proposal__Proposal__c.fields.Electrosurgery_Sales__c,
    	ElectrosurgeryIncrementalSpend =
    					Apttus_Proposal__Proposal__c.fields.Electrosurgery_Incremental_Spend__c,
    	ElectrosurgeryIncrementalQuanitity =
    					Apttus_Proposal__Proposal__c.fields.Electrosurgery_Incremental_Quantity__c,
    	ElectrosurgeryIncrementalTotalSpend =
    					Apttus_Proposal__Proposal__c.fields.Electrosurgery_Total_Incremental_Spend__c,
    	LigasureEaches						= Apttus_Proposal__Proposal__c.fields.Ligasure_Eaches__c,
    	LigasureSales						= Apttus_Proposal__Proposal__c.fields.Ligasure_Sales__c,
    	LigasureIncrementalSpend			= Apttus_Proposal__Proposal__c.fields.Ligasure_Incremental_Spend__c,
    	LigasureIncrementalQuanitity 		= Apttus_Proposal__Proposal__c.fields.Ligasure_Incremental_Quantity__c,
    	LigasureIncrementalTotalSpend =
    					Apttus_Proposal__Proposal__c.fields.Ligasure_Total_Incremental_Spend__c,
    	IncrementalTotal					= Apttus_Proposal__Proposal__c.fields.Total_Incremental__c
    ;
}