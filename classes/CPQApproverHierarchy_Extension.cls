/*
History
02/16/2016 Paul Berglund        Added the toggleIsApprover() so the user can
                                update a User without having to go the user
                                details page.
                                Modified the layout of the User information
                                to make it easier to understand.
                                Changed the testUsers to use list of aliases
*/
global with sharing class CPQApproverHierarchy_Extension {

    public CPQApproverHierarchy_Extension() {

    }
    
    public string testUser { get; set; }
    
    public List<SelectOption> testUsers {
        get {
            Organization org = [SELECT Id, IsSandbox FROM Organization LIMIT 1];
            Map<string, Id> us = new Map<string, Id>();
            Set<string> aliases = new Set<string>{'khard', 'dbart', 'BDebo', 'Aly'};
            
            List<SelectOption> options = new List<SelectOption>();
            if(org.IsSandbox) {
                for(string a : aliases){
                    try {
                        User u = [SELECT Id, Name FROM User WHERE Alias = :a];
                        options.add(new SelectOption(u.Id, u.Name));
                    }
                    catch(Exception ex) {}
                }
            }
            return options;
        }
    }

    UserTerritory theRecord;
    
    public boolean display { get; set; }
    
    public Id selectedUser { get; set; }
    public Id parentId { get; set; }
    
    private static Map<Id, User> Users = null;
    private Map<Id, Map<Id, boolean>> IsActive = new Map<Id, Map<Id, boolean>>(); // User => (Territory, flag)
    private Set<Id> TerritoryIds = new Set<Id>();
    private Map<Id, Set<Id>> T2UIds = new Map<Id, Set<Id>>();

    public CPQApproverHierarchy_Extension(ApexPages.StandardController controller) {
        theRecord = (UserTerritory)controller.getRecord();
    }
    
    public List<myTerritory> AssignedTerritories { get; set; }

    public void onTestUserChange() {
        selectedUser = Id.valueOf(testUser);
        AssignedTerritories = getHierarchy(Id.valueOf(testuser));
        display = true;
    }
    
    // Just toggle the is_Approver__c field on the User
    @RemoteAction
    public static string toggleIsApprover(Id user) {
        try {
            User u = [SELECT Id, Is_Approver__c FROM User WHERE Id = :user];
            if(u != null) {
                u.Is_Approver__c = !u.Is_Approver__c;
                update u;
                return Id.valueOf(u.Id);
            }
        }
        catch (Exception ex) {
            return ex.getMessage();
        }
        return null;
    }

    public integer MaxDepth {
        get {
            return countLevel(AssignedTerritories);
        }
    }
    
    private integer countLevel(List<myTerritory> ts) {
        integer maxDepth = 0;
        if(ts != null || ts.size() != 0) {
            for(myTerritory t : ts)
                maxDepth = Math.max(countLevel(t.Children) + 1, maxDepth);
        }
        
        return maxDepth;
    }
    
    public void save () {
        selectedUser = theRecord.UserId;
        AssignedTerritories = getHierarchy(theRecord.UserId);
        display = true;
    }

    public virtual class myTerritory {
        public Id Id { get; set; }
        public string Name { get; set; }
        public Set<Id> AssignedUserIds { get; set; }
        public List<myTerritory> Children { get; set; }
    }

    public class Level {
        List<Territory> Territories = new List<Territory>();
        Level Child = null;
    }
    
    private void SetupUsers() {
        //Set<Id> uids = new Set<Id>();
        for(UserTerritory ut : [SELECT TerritoryId, UserId, isActive FROM UserTerritory WHERE TerritoryId IN :TerritoryIds]) {
            if(ut.isActive == true) {
                Map<Id, boolean> flag = IsActive.get(ut.UserId);
                if(flag == null) {
                    flag = new Map<Id, boolean>();
                    IsActive.put(ut.UserId, flag);
                }
                flag.put(ut.TerritoryId, true);
            }
                
            Set<Id> uids = T2UIds.get(ut.TerritoryId);
            if(uids == null) {
                uids = new Set<Id>();
                T2UIds.put(ut.TerritoryId, uids);
            }
            uids.add(ut.UserId);
        }
        
        // Create a list of all the User Ids referenced by the UserTerritory relationships
        Set<Id> AllUIds = new Set<Id>();
        for(Set<Id> uidset : T2UIds.values())
            AllUIds.addAll(uidset);
            
        // Get a list of all potential Users that are active
        Users = new Map<Id, User>([SELECT Id, Name, is_Approver__c, Sales_Org_PL__c, FirstName, LastName FROM User WHERE Id IN :AllUIds AND isActive = true]);
        
        // Remove any User Ids where the User is inactive
        for(Set<Id> uidset : T2Uids.values()){
            for(Id id : uidset) {
                if (Users.get(id) == null)
                    uidset.remove(id);
            }
        }
    }
    
    private List<myTerritory> getHierarchy(Id userId) {
        T2UIds.clear();
        TerritoryIds.clear();
        IsActive.clear();
        Level root = getAssignedTerritories(userId);
        SetupUsers();
    
        return getHierarchy(null, root);
    }
    
    private List<myTerritory> getHierarchy(Territory t, Level l) {
        if(l == null) return null;
        
        List<myTerritory> myTerritories = new List<myTerritory>();
        for(Territory c : l.Territories) {
            if(t == null || // First pass through, so we're building the assigned territories
               c.ParentTerritoryId == t.Id) { // Subsequent passes, we are passing in the parent territory with the list of Child territories to select from
                myTerritory myt = new myTerritory();
                myt.Id = c.Id;
                myt.Name = c.Name;
                myt.AssignedUserIds = T2Uids.get(c.Id);
                myt.Children = getHierarchy(c, l.Child);
                myTerritories.add(myt);
                TerritoryIds.add(c.Id);
            }
        }
        return myTerritories;
    }
    
    private Level getAssignedTerritories(Id userId) {
        Set<Id> tids = new Set<Id>();
        for(UserTerritory ut : [SELECT TerritoryId FROM UserTerritory WHERE UserId = :userId AND isActive = true])
            tids.add(ut.TerritoryId);

        Level target = new Level();
        target.Territories = new List<Territory>([SELECT Id, ParentTerritoryId, Name FROM Territory WHERE Id IN :tids]);
        target.Child = getChildTerritories(tids);
        TerritoryIds.addAll(tids);
        
        return target;
    }
    
    private Level getChildTerritories(Set<Id> pids) {
        if (pids == null || pids.size() == 0) return null;
        
        Level target = new Level();
        target.Territories = new List<Territory>([SELECT Id, ParentTerritoryId, Name FROM Territory WHERE ParentTerritoryId IN :pids]);

        Set<Id> tids = new Set<Id>();
        for(Territory t : target.Territories)
            tids.add(t.Id);
            
        target.Child = getChildTerritories(tids);
        TerritoryIds.addAll(tids);

        return target;
    }
    
    public string generateTable {
        get {
            if(AssignedTerritories == null || AssignedTerritories.size() == 0)
                return 'None Found';
            else {
                string str = '<table>';
                str += generateHeader(MaxDepth);
                str += '<tr>';
                str += generateTD(AssignedTerritories, 1, MaxDepth);
                return str += '</table>';
            }
        }
    }
    
    private string generateTD(List<myTerritory> ts, integer l, integer d) {
        string str = '';
        string term = '';
        if(ts == null || ts.size() == 0) {
            str += generateTD(d - l);
            str += '</tr>';
        }
        else {
            for(myTerritory t : ts) {
                str += generateTD(t, l, d);
                term = '<tr>';
                term += generateTD(l - 1);
                str += term;
            }
        }
        
        return str.removeEnd(term);
    }

    private string generateTD(myTerritory t, integer l, integer d) {
        string str = '';
        str += generateTD(generateLink(t)+generateAssignedUsers(t.Id, t.AssignedUserIds),false);
        str += generateTD(t.Children, l + 1, d);
        
        return str;
    }
    
    private string generateTD(string text, boolean indent) {
        return (indent) ? '<td/><td>' + text + '</td>' :
                          (text == null) ?  '<td colspan="2"/>' :
                                            '<td colspan="2" align="left">' + text + '</td>';
    }
    
    private string generateAssignedUsers(Id tid, Set<Id> uids) {
        if(uids == null || uids.size() == 0)
            return generateList(ListType.Unordered, new List<string>{'None found'});
        else {
            List<User> us = new List<User>();
            for(Id uid : uids)
                us.add(Users.get(uid));
            return generateUserList(tid, us);
        }
    }
    
    enum ListType {
        Ordered,
        Unordered
    }
    
    private boolean isUserActiveinTerritory(Id u, Id t) {
        Map<Id, boolean> flags = IsActive.get(u);
        if(flags == null || !flags.containsKey(t))
            return false;
        else
            return flags.get(t);
    }
    
    private string generateLabel(string labelFor, string labelValue) {
        return '<label for="' + labelFor + '">' + labelValue + '</label>';
    }
    
    private string generateIsApproverCheckBox(string u, boolean checked) {
        return generateLabel('isApprover', 'Is Approver') + '<input type="checkbox" ' + (checked ? 'checked' : '') + ' onchange="toggleIsApprover(\'' + u + '\')">';
    }
    
    private string generateCheckbox(string id, string label, boolean checked) {
        return generateLabel(id, label) + '<input type="checkbox" ' + (checked ? 'checked' : '') + ' disabled>';
    }
    
    private string generateUserList(Id tid, List<User> us) {
        if(us == null || us.size() == 0)
            return generateList(ListType.Unordered, new List<string>{'None found'});
        else {
            List<string> links = new List<string>();
            // Create sorted list of users
            Map<string, User> lastfirst = new Map<string, User>();
            List<string> sorted = new List<string>();
            for(User u : us) {
                sorted.add(u.LastName + ', ' + u.FirstName);
                lastfirst.put(u.LastName + ', ' + u.FirstName, u);
            }
            sorted.sort();
            for(string key : sorted) {
                // So the format is now:
                // User Link (Sales Org)
                // * Is Active []
                // * Is Approver []
                User u = lastfirst.get(key);
                links.add(generateLink(u) + ' (' + u.Sales_Org_PL__c + ')' + generateBR() + generateList(ListType.Unordered, new List<string>{ generateCheckbox('isActive', 'Is Active', isUserActiveinTerritory(u.Id, tid)), generateIsApproverCheckbox(u.Id, u.is_Approver__c)}));
            }
            return generateList(ListType.Ordered, links);
        }
    }
    
    private string generateList(ListType lt, List<string> strs) {
        if(strs == null || strs.size() == 0)
            return '';
        else {
            string str = (lt == ListType.Ordered) ? '<ol>' : '<ul>';
            for(string s : strs)
                str += generateLI(s);
            str += (lt == ListType.Ordered) ? '</ol>' : '</ul>'; 
            return str;
        }
    }
        
    private string generateLI(string text) {
        return '<li>' + text + '</li>';
    }
    
    private string generateBR() {
        return '<br/>';
    }
    private string generateBR(string text) {
        return (text == null) ? generateBR() : '<br>' + text + '</br>';
    }
    
    private string generateLink(User u) {
        return generateLink('/' + u.Id + '?noredirect=1&isUserEntityOverride=1', u.LastName + ', ' + u.FirstName);  
    }
    
    private string generateLink(myTerritory t) {
        return generateLink('/' + t.Id, t.Name);
    }
    
    private string generateLink(string href, string name) {
        return '<a target="_blank" href="' + href + '">' + name + '</a>';
    }
    
    private string generateTD(integer cnt) {
        string str = '';
        for(integer ndx = 1; ndx <= cnt; ndx++)
            str += generateTD('',false);
        return str;
    }
    
    private string generateHeader(integer maxDepth) {
        string str = '<tr>';
        
        for(integer ndx = 1; ndx <= maxDepth; ndx++)
            str += '<th colspan="2">Level ' + ndx + '</th>';
        
        return str + '</tr>';
    }
    
/*    
    public class myParent extends myTerritory {
        public integer MaxDepth { get; set; }
    }
*/

    public User assignedUser {
        get {
            return [SELECT Id, Name FROM User WHERE Id = :selectedUser LIMIT 1];
        }
/*        get {
            try {
                return [SELECT Id, Name FROM User WHERE Id = :selectedUser LIMIT 1];
            }
            catch (Exception ex) {
                return new User();
            }
        }
*/    }
    
/*
    private integer addChildren(myTerritory parent) {
        integer maxDepth = 1;
        
        for(Territory t : [SELECT Id, Name FROM Territory WHERE ParentTerritoryId = :parent.self.Id LIMIT 1]) {
            if(parent.Children == null) parent.Children = new List<myTerritory>();
            
            myTerritory myt = new myTerritory();
            myt.self = t;
            maxDepth = Math.max(addChildren(myt) + 1, maxDepth); // This should help us obtain the max
                                                                 // depth of all the children
        
        }
    
        return maxDepth;
    }
    
    private List<myParent> buildHierarchy(Id userId) {
        MaxDepth = 0;
        Set<Id> tids = new Set<Id>();
        for(UserTerritory ut : [SELECT TerritoryId FROM UserTerritory WHERE UserId = :userId)
            tids.add(ut.TerritoryId);
            
        List<myParent> assigned = new List<myParent>();
        for(Territory t : [SELECT Id, Name FROM Territory WHERE Id IN :tids]) {
            myParent myp = new myParent();
            myp.self = t;
            myp.MaxDepth = addChildren(myp);
            MaxDepth = Math.max(myp.MaxDepth, MaxDepth);
            assigned.add(myp);
        }
        return assigned;
    }
*/
}