@isTest (SeeAllData = True)
private class Test_ConcatenatedProductsOppLineItemT {
   public static testMethod void Test_ConcatenatedProductsOppLineItemTrigger() {
      // Setup test data and user context
      Profile p = [SELECT Id FROM Profile WHERE Name='US - RMS']; 
      User u = new User(Alias = 'testuser', Email='testuser@covidien.com', 
      EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='testuser@covidien.com');
      
      String longName = '';
      for (Integer i=0; i < 21; i++) {
          longName = longName + '0123456789';
      }
      System.assertEquals(longName.length(), 210);
      
      List<Product2> p2L = new List<Product2>();
      String str;
      for (Integer a=0; a<10; a++) {
          str = a + longName;
	      p2L.add(new Product2 (Name = str, CurrencyIsoCode = 'USD'));
      }
      insert p2L;
      System.assertNotEquals(p2L[0].id, null);
       
      Pricebook2 spb = [SELECT id FROM Pricebook2 WHERE IsStandard = true LIMIT 1];
      List<PricebookEntry> pbeL = new List<PricebookEntry>();
      for (Product2 p2 : p2L) {
	      pbeL.add(new PricebookEntry (Pricebook2Id = spb.id, Product2Id = p2.id, UseStandardPrice = false, UnitPrice = 1, IsActive = true, CurrencyIsoCode = 'USD'));
      }
      insert pbeL;
      System.assertNotEquals(pbeL[0].id, null);
       
      System.runAs(u) {
          //List<PriceBookEntry> pbeList = [SELECT Id FROM PriceBookEntry WHERE IsActive = true AND CurrencyIsoCode = 'USD' LIMIT 10];
          List<Opportunity> oL = new List<Opportunity>();
          oL.add(new Opportunity(Name = 'Test 1', StageName = 'Identify', CloseDate = Date.Today(), CurrencyIsoCode = 'USD'));
          oL.add(new Opportunity(Name = 'Test 2', StageName = 'Identify', CloseDate = Date.Today(), CurrencyIsoCode = 'USD'));
          insert oL;
          List<OpportunityLineItem> oliL = new List<OpportunityLineItem>();
          for (PricebookEntry pe : pbeL) {
	          oliL.add(new OpportunityLineItem(OpportunityId=oL[0].id, PricebookEntryId = pe.id, Quantity = 1.0, TotalPrice = 10));
          }
          insert oliL;
          delete oliL;
      }
   }
}