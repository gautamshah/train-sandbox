/****************************************************************************************
 * Name    : Test_Contact_UpdateRegionalRecordType
 * Author  : Gautam Shah
 * Date    : 11/13/2013 
 * Purpose : Unit test of Contact_UpdateRegionalRecordType.trigger
 * Dependencies:
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 *****************************************************************************************/
@isTest(SeeAllData=true)
private class Test_Contact_UpdateRegionalRecordType 
{
    static testMethod void myUnitTest() 
    {
        Test.startTest();
        User u = [Select Id From User Where IsActive = true And Profile.Name = 'US - RMS' And Region__c = 'US' Limit 1];
        RecordType rt = [Select Id From RecordType Where sObjectType = 'Contact' And DeveloperName = 'Affiliated_Contact_US' Limit 1];
        
        System.runAs(u)
        {
            Account accnew=new Account();
            accnew.Name='test1';         
            accnew.BillingStreet='US Street';
            accnew.BillingState='US';
            accnew.BillingCountry='US';
            accnew.BillingCity='US';
            accnew.BillingPostalCode='044555';
            insert accnew;
            Contact con = new Contact();
            con.RecordTypeId = rt.Id;
            con.LastName = 'Tester';
            con.AccountId = accnew.Id;
            con.Department_picklist__c = 'Administration';
            con.Connected_As__c = 'Administrator';
            insert con;
            con.Connected_As__c = 'Buyer';
            update con;
        }
        Test.stopTest();
    }
}