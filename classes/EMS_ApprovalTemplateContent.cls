public with sharing class EMS_ApprovalTemplateContent {



    public Id EMSEvent{get;set;}
    public CurrencyType c;

    public EMS_Event__c currentEMSRecord;

    public CurrencyType getc(){
    c = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and Isocode=:currentEMSRecord.CurrencyISOCode limit 1];
    return c;
    }
    
    
    public EMS_Event__c getCurrentEMSRecord() {
        CurrentEMSRecord= [Select Id,Name,Start_Date__c,Budget_External_Recipient_URL__c ,Budget_Internal_Recipient_URL__c ,
        
         Lodging_Description_Remarks__c ,Lodging_per_Person__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,Plan_Budget_Amt__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Design_Fee_Remarks__c,Gifts_Description_Remarks__c,Air_Travel_Description_Remarks__c ,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Fee__c,Event_Production_Description__c,Event_Native_Name__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Total_Consultancy_Fee__c,Total_LodA__c ,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,ISR_CSR__c,eCPA_url__c,Benefit_Start_Date__c,Location_City__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Local_GTA__c,Total_Gifts_amount__c,Financial_Year__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,
                                  Event_Id__c,Manager1__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Auto_Number__c,Venue__c,Publication_grant__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Owner_Country__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,Division_Multiple__c,GBU_Multiple__c,
                                  Total_No_of_Recipients__c,No_of_Approved_eCPAs__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,Division_Primary__c,Expense_Type__c,Total_Others_Count__c,Others_per_person__c,
                                  Total_lodging_count__c,Total_Air_Travel_Count__c,Total_Air_travel_per_person__c,Total_Gifts_Count__c,Gifts_value_per_person__c,Total_Consultancy_Count__c,Consultancy_Per_person__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Event_URL__c,Any_HCP_Invitees_from_Outside_Asia__c,CPA_Status__c,Total_Others_Amount__c,
                                  Consultation_Fees_Description_Remarks__c,Total_Air_Travel__c,Others_Description_Remarks__c,Others_Sample_Decription_Remarks__c,Others_SKU_Sample_NCE_Amount__c,
                                  CPA_Request_Date__c,Covidien_Solo_Sponsorship__c,Sponsorship_Pay_To_Agency__c,Total_Meal_Count__c,Total_LGT_Count__c,Track_Meal_Flag__c,Track_LT_Flag__c  
                                  from EMS_Event__c where Id= :EMSEvent];
                                  
        return currentEMSRecord;
    }
    
    public String getApprovalStepId(){
    String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/'+'p/process/ProcessInstanceWorkitemWizardStageManager?id=';
    ProcessInstanceWorkitem Lpes = [SELECT Id,ActorId,ProcessInstance.TargetObjectId,ProcessInstance.Id,ProcessInstance.LastActorId,ProcessInstance.Status FROM ProcessInstanceWorkitem where ProcessInstance.TargetObjectId=:EMSEvent order by SystemModstamp desc Limit 1];
    return (fullRecordURL + Lpes.Id);
    }
}