Public class EMS_csvFileReaderController {
public Blob csvFileBody{get;set;}
public Blob csvInviteesFileBody{get;set;}
Public string csvAsString{get;set;}
Public string InviteescsvAsString{get;set;}
Public String[] csvfilelines{get;set;}
Public String[] inputvalues{get;set;}
Public List<string> fieldList{get;set;}
Public List<EMS_Event__c> existingsObjectList;
Public List<EMS_Event__c> newsObjectList;
Public List<EMSEventWrapper> LEWrapper;
public String secondbutton{get;set;}
public String thirdbutton{get;set;}

public EMS_csvFileReaderController(){
    csvfilelines = new String[]{};
    fieldList = New List<string>();
    existingsObjectList = New List<EMS_Event__c>(); 
    newsObjectList = New List<EMS_Event__c>(); 
    LEWrapper=new List<EMSEventWrapper>();
    secondbutton='1';
    thirdbutton='1';
    }



 
 
 
  Public void readcsvFile(){
       csvAsString = csvFileBody.toString();
       csvfilelines = csvAsString.split('\n');
       inputvalues = new String[]{};
       System.debug('======>'+csvfilelines.size());
       for(string st:csvfilelines[0].split(',')) 
           fieldList.add(st);   
       //System.debug('======>'+csvfilelines[2]+'================>****'+csvfilelines[3]);
       System.debug('(((((((('+fieldlist);
       Set<String> AllUniqueExternalIds = new Set<String>();
       for(Integer i=2;i<csvfilelines.size();i++){
           string[] csvRecordData = csvfilelines[i].split(',');
           AllUniqueExternalIds.add(csvRecordData[0]);
           }
       Map<Id,EMS_Event__c> MAllEMSEvents= new Map<Id,EMS_Event__c>([Select Id,Upload_External_Unique_ID__c from EMS_Event__c where Upload_External_Unique_ID__c!=null AND Upload_External_Unique_ID__c in :(AllUniqueExternalIds)]);
       Map<String,Id> MUploadIdstoIds = new Map<String,Id>();
       Set<String> SExistingUEUIds = new Set<String>();
       
       for(EMS_Event__c eachEMSrecord : MAllEMSEvents.values()){
       
       MUploadIdstoIds.put(eachEMSrecord.Upload_External_Unique_ID__c,eachEMSrecord.Id);
       SExistingUEUIds.add(eachEMSrecord.Upload_External_Unique_ID__c);
             
       }
       
       for(Integer i=2;i<csvfilelines.size();i++){
           EMS_EVent__c EMSRec = new EMS_Event__c();
           datetime myDate;
           string[] csvRecordData = csvfilelines[i].split(',');
           system.debug('--------->'+csvRecordData );
           //AllUniqueExternalIds.add(EMSrec.Upload_External_Unique_Id__c);
           EMSrec.Upload_External_Unique_Id__c= csvRecordData[0];         
           EMSRec.GBU_Primary__c=csvRecordData[1];
           EMSRec.Division_Primary__c=csvRecordData[2];
           //EMSRec.Start_Date__c= date.newinstance(Integer.valueof(csvRecordData[3].split('-')[0]),Integer.valueof(csvRecordData[3].split('-')[1]),Integer.valueof(csvRecordData[3].split('-')[2]));
           //EMSRec.End_Date__c =date.newinstance(Integer.valueof(csvRecordData[4].split('-')[0]),Integer.valueof(csvRecordData[4].split('-')[1]),Integer.valueof(csvRecordData[4].split('-')[2])); 
           EMSRec.Start_Date__c = Date.parse(csvRecordData[3]);
           EMSRec.End_Date__c = Date.parse(csvRecordData[4]);
           system.debug('=====>'+(csvRecordData[5]==null)+(csvRecordData[5]==''));
           EMSRec.Plan_Budget_Amt__c=(csvRecordData[5]==''?0:(Integer.valueOf(csvRecordData[5].trim())));
           EMSRec.Name= csvRecordData[6];
           EMSrec.Event_Native_Name__c=csvRecordData[7];
           EMSRec.Location_Country__c=csvRecordData[8];  
           EMSRec.Location_State__c=csvRecordData[9];           
           EMSRec.Expense_Type__c=csvRecordData[10];
           EMSRec.Event_Type__c=csvRecordData[11];
           EMSRec.Training_conducted_in_Hosp_Training_Cent__c=csvRecordData[12];
           EMSRec.HTC_Name__c=csvRecordData[13];
           EMSRec.Primary_COT__c=csvRecordData[14];
           EMSRec.Reoccurring_or_New__c=csvRecordData[15];
           EMSRec.Event_Host__c=csvRecordData[16];
           EMSRec.CurrencyIsoCode=csvRecordData[17].trim();
          
           EMSrec.Event_Status__c='Planned';
           EMSrec.Max_Attendees__c=1;
           
           if(SExistingUEUIds.contains(csvRecordData[0])){
               EMSrec.Id=MUploadIdstoIds.get(csvRecordData[0]);
               existingsObjectList.add(EMSRec);     
           }
           else{
           
           NewsObjectList.add(EMSrec);
           }
           //existingsObjectList.add(EMSRec);   
       }
       
       
       
       
       System.debug('------->'+existingsObjectList);
       System.debug('------->'+NewsObjectList);
System.debug('------->'+secondbutton);
System.debug('------->'+thirdbutton);
       try{
       if(secondbutton=='2'){      
       
       Database.SaveResult[] srListupdate = database.update(existingsObjectList,false);
       System.debug('=========>'+srListupdate );
       }
       if(thirdbutton=='1'){
       Database.SaveResult[] srList=database.insert(NewsObjectList,false);
       System.debug('=========>'+srList);
       }
       }
       catch(Exception e){
       
       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getmessage());
       ApexPages.addMessage(myMsg);
       

       
       }
  }
  
  public Pagereference InsertEvents(){
  System.debug('\\\\\\inside InsertEvents');
  
   try{
       if(secondbutton=='2'){      
       
       Database.SaveResult[] srListupdate = database.update(existingsObjectList,false);
       System.debug('=========>'+srListupdate );
       }
       if(thirdbutton=='1'){
       Database.SaveResult[] srList=database.insert(NewsObjectList,false);
       System.debug('=========>'+srList);
      }
       }
       catch(Exception e){
       
       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getmessage());
       ApexPages.addMessage(myMsg);
       

       
       }
  
  return null;
  
  }
  public Pagereference Back(){
  
  return new PageReference('/apex/EMS_MassEventUploader');
  
  }
  
  Public Pagereference writecsvFile(){
  LEWrapper.clear();
       csvAsString = csvFileBody.toString();
       csvfilelines = csvAsString.split('\n');
       inputvalues = new String[]{};
       System.debug('======>'+csvfilelines.size());
       for(string st:csvfilelines[0].split(',')) 
           fieldList.add(st);   
       //System.debug('======>'+csvfilelines[2]+'================>****'+csvfilelines[3]);
       System.debug('(((((((('+fieldlist);
       Set<String> AllEventOwners = new Set<String>();
       Set<String> AllUniqueExternalIds = new Set<String>();
       for(Integer i=2;i<csvfilelines.size();i++){
           string[] csvRecordData = csvfilelines[i].split(',');
           AllUniqueExternalIds.add(csvRecordData[0]);
           AllEventOwners.add(csvRecordData[24]); 
           }
       Map<Id,EMS_Event__c> MAllEMSEvents= new Map<Id,EMS_Event__c>([Select Id,Upload_External_Unique_ID__c from EMS_Event__c where Upload_External_Unique_ID__c in :(AllUniqueExternalIds)]);
       Map<Id,User> MAllEMSEventOwners = new Map<Id,User>([Select Id,Email from user where Email in :(AllEventOwners)]);
       Map<String,Id> MEmailtoID = new Map<String,Id>();
       Map<String,Id> MUploadIdstoIds = new Map<String,Id>();
       Set<String> SExistingEmails = new Set<String>();
       Set<String> SExistingUEUIds = new Set<String>();
       
       for(EMS_Event__c eachEMSrecord : MAllEMSEvents.values()){
       
       MUploadIdstoIds.put(eachEMSrecord.Upload_External_Unique_ID__c,eachEMSrecord.Id);
       SExistingUEUIds.add(eachEMSrecord.Upload_External_Unique_ID__c);
            
       }
       for(User eachUser : MAllEMSEventOwners.values()){
       
       MEmailtoID.put(eachUser.Email,eachUser.Id);
       SExistingEmails.add(eachUser.Email);     
        
       }
       
       for(Integer i=2;i<csvfilelines.size();i++){
           EMSEventWrapper EMSRec = new EMSEVentWrapper();
           datetime myDate;
           string[] csvRecordData = csvfilelines[i].split(',');
           system.debug('--------->'+csvRecordData );
           EMSrec.EMSRecord.Upload_External_Unique_Id__c= csvRecordData[0];         
           EMSRec.EMSRecord.GBU_Primary__c=csvRecordData[1];
           EMSRec.EMSRecord.Division_Primary__c=csvRecordData[2];
          
           EMSRec.EMSRecord.Start_Date__c = Date.parse(csvRecordData[3]);
           EMSRec.EMSRecord.End_Date__c = Date.parse(csvRecordData[4]);
           system.debug('=====>'+(csvRecordData[5]==null)+(csvRecordData[5]==''));
           EMSRec.EMSRecord.Plan_Budget_Amt__c=(csvRecordData[5]==''?0:(Integer.valueOf(csvRecordData[5].trim())));
           //EMSRec.EMSRecord.CPA_Required__c=csvRecordData[6];
           EMSRec.EMSRecord.Name= csvRecordData[6];
           EMSrec.EMSRecord.Event_Native_Name__c=csvRecordData[7];
           EMSRec.EMSRecord.Location_Country__c=csvRecordData[8];  
           EMSRec.EMSRecord.Location_State__c=csvRecordData[9];
           
           EMSRec.EMSRecord.Expense_Type__c=csvRecordData[10];
           EMSRec.EMSRecord.Event_Type__c=csvRecordData[11];
           EMSRec.EMSRecord.Training_conducted_in_Hosp_Training_Cent__c=csvRecordData[12];
           EMSRec.EMSRecord.HTC_Name__c=csvRecordData[13];
           EMSRec.EMSRecord.Primary_COT__c=csvRecordData[14];
           EMSRec.EMSRecord.Reoccurring_or_New__c=csvRecordData[15];
           EMSRec.EMSRecord.Event_Host__c=csvRecordData[16];
           EMSRec.EMSRecord.CurrencyIsoCode=csvRecordData[17];
           EMSRec.EMSRecord.Location_City__c=csvRecordData[18];
           EMSRec.EMSRecord.Venue__c=csvRecordData[19];
           EMSRec.EMSRecord.First_Payment_Date__c=date.parse(csvRecordData[20]);
           EMSRec.EMSRecord.Max_Attendees__c=((csvRecordData[21]=='')? (0):(Integer.valueOf(csvRecordData[21])));
           EMSRec.EMSRecord.Any_recipient_s_who_are_KOL_s_HCP_s__c=csvRecordData[22];
           EMSRec.EMSRecord.Any_HCP_Invitees_from_Outside_Asia__c=csvRecordData[23];
           //EMSRec.EMSRecord.Event_Owner_User__c=csvRecordData[24];
           EMSRec.EMSRecord.Benefit_Summary__c=csvRecordData[25].trim();
           EMSRec.EMSRecord.CCI__c=csvRecordData[26].trim();
         EMSRec.EMSRecord.KMDIA__c=csvRecordData[27].trim();
           EMSrec.EMSRecord.Event_Status__c='Budget';
           if(!SExistingEmails.contains(csvRecordData[24])){  
           system.debug('%%%%%'+csvRecordData[24]);         
           EMSRec.errormessage='Event Owner is not a User in Salesforce.';
           
           }
           else
           EMSRec.errormessage='Success';
           LEWrapper.add(EMSRec);
           if(SExistingUEUIds.contains(csvRecordData[0]) && SExistingEmails.contains(csvRecordData[24])){
               EMSrec.EMSRecord.Id=MUploadIdstoIds.get(csvRecordData[0]);
               existingsObjectList.add(EMSrec.EMSRecord);     
           }
           else if(!SExistingUEUIds.contains(csvRecordData[0]) && (SExistingEmails.contains(csvRecordData[25]))){
           
           NewsObjectList.add(EMSrec.EMSRecord);
           }
           //existingsObjectList.add(EMSRec);   
       }
       
       
       System.debug('------->'+LEWrapper.size());
       
       System.debug('------->'+existingsObjectList);
       System.debug('------->'+NewsObjectList);
       System.debug('=======>'+secondbutton);
       System.debug('=======>'+thirdbutton);
       return Page.EMS_MassEventUploadDisplay;
  }
  
  public List<Event_Beneficiaries__c> LEventBenf ;
  public List<EMSEventWrapper> getUploadedEvents(){

    return LEWrapper;

    }
    
    public Class EMSEventWrapper{
    public String OwnerEmailId{get;set;}   
    public String ErrorMessage{get;set;}
    public EMS_Event__c EMSrecord{get;set;}

    public EMSEventWrapper(){
    
    EMSRecord=new EMS_Event__c();
    }
}
}