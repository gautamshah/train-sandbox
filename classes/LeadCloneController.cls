public class LeadCloneController 
{
    private Lead l {get;set;}
    
	public LeadCloneController(ApexPages.StandardController controller)
    {
        Schema.DescribeSObjectResult dsr = Lead.SObjectType.getDescribe();
        List<String> lFields = new List<String>();
        for(String apiName : dsr.fields.getMap().keySet())
        {
            lFields.add(apiName);
        }
        if(!Test.isRunningTest())
        {
        	controller.addFields(lFields);
        }
        this.l = (Lead)controller.getRecord();
    }
    
    public PageReference doClone()
    {
        PageReference pr;
        System.debug('country: ' + l.Country);
        try
        {
        	Lead newL = l.clone(false,true,false,false);
            newL.Cloned_From__c = l.Id;
            insert newL;
            Cloner.cloneNotes(l.Id, newL.Id);
            Cloner.cloneAttachments(l.Id, newL.Id);
            Cloner.cloneTasks(l.Id, newL.Id, null, null);
            Cloner.cloneEvents(l.Id, newL.Id, null, null);
			pr = new PageReference('/' + newL.Id + '/e?retURL=/' + newL.Id);
            pr.setRedirect(true);
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error cloning this lead'));
        }
        return pr;
    }
}