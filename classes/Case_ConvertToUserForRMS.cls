global with sharing class Case_ConvertToUserForRMS {
    private final Profile p;
    
    public Case_ConvertToUserForRMS() {
    }
    
    public Case_ConvertToUserForRMS(ApexPages.StandardController controller) {

    }

    public string Id { get; set; }
    public string UserRole { get; set; }
    public string UserProfile { get; set; }
    public string Function { get; set; }
    public string TimeZone { get; set; }
    //public string UserVisibility {get; set; }
    
    public PageReference Convert() {

        
        System.Debug('**Id' + Id);
        
        Case aCase = [SELECT Comments__c, Requested_By_Email__c, GBU__c, Region__c,
                             First_Name_NUPP__c, Last_Name_NUPP__c, Email_Address_NUPP__c,
                             Employee_Number_NUPP__c, Covidien_Department_NUPP__c,
                             Business_Card_Title_NUPP__c, Mobile_Phone_NUPP__c,
                             Reason_for_User_Request_NUPP__c, Language_NUPP__c, Currency_NUPP__c,
                             Time_Zone_NUPP__c, Cost_Center_NUPP__c, Workday_ID_Medtronic_Username__c, Country_Code_NUPP__c,
                             Sales_Org__c, User_Manager__c, Salesforce_for_Outlook__c,Asia_Team_Asia_use_only__c
                      FROM Case
                      WHERE Id = :Id LIMIT 1];
                      
        User bUser=new User();              
        bUser = [SELECT LocaleSidKey, LanguageLocaleKey,CurrencyIsoCode FROM USER WHERE TimeZoneSidKey = : TimeZone LIMIT 1];
        System.Debug('**bUser =' + bUser );   
                     
        User aUser = new User(LastName = aCase.Last_Name_NUPP__c,
                              FirstName = aCase.First_Name_NUPP__c,
                              Department = aCase.Covidien_Department_NUPP__c,
                              Title = aCase.Business_Card_Title_NUPP__c,
                              Email = aCase.Email_Address_NUPP__c,
                              MobilePhone = aCase.Mobile_Phone_NUPP__c,
                              isActive = true,
                              EmployeeNumber = aCase.Employee_Number_NUPP__c,
                              Business_Unit__c = aCase.GBU__c,
                              Region__c = aCase.Region__c,
                              Country = aCase.Country_Code_NUPP__c.substring(0,2),
                              User_External_ID__c = aCase.Employee_Number_NUPP__c,
                              Cost_Center__c = aCase.Cost_Center_NUPP__c,
                              FederationIdentifier = aCase.Workday_ID_Medtronic_Username__c,
                              Workday_Employee_Number__c = aCase.Workday_ID_Medtronic_Username__c,
                              Username = aCase.Email_Address_NUPP__c,
                              Alias = aCase.First_Name_NUPP__c.Substring(0,1) + aCase.Last_Name_NUPP__c.substring(0,Math.Min(aCase.Last_Name_NUPP__c.length(),4)),
                              CommunityNickname = aCase.First_Name_NUPP__c + '.' + aCase.Last_Name_NUPP__c,
                              Asia_Team_Asia_use_only__c=aCase.Asia_Team_Asia_use_only__c,
                              TimeZoneSidKey = TimeZone,
                              UserRoleId = '00EU0000000gf3Q',
                              LocaleSidKey = bUser.LocaleSidKey,
                              EmailEncodingKey = 'UTF-8',
                              ProfileId = UserProfile,
                              LanguageLocaleKey = bUser.LanguageLocaleKey,
                              Function__c = Function,
                              User_Role__c = UserRole,
                              //User_Visibility__c=UserVisibility,
                              CurrencyIsoCode=bUser.CurrencyIsoCode
                              );
        PageReference userPage;
        
        if(!test.isRunningTest())
        {                      
            insert(aUser);
            userPage = new PageReference('/' + aUser.Id);
        }
        else
        {
            userPage = new PageReference('/' + UserInfo.getUserId());
        }
        userPage.getParameters().put('nooveride','1');
        userPage.setRedirect(true);
        return userPage;
    }
    
    public List<SelectOption> getUserRoles() {
         List<SelectOption> options = new List<SelectOption>();
         
         Schema.DescribeFieldResult fieldResult = User.User_Role__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         
         for(Schema.PicklistEntry f: ple)
         {
             options.add(new SelectOption(f.getLabel(), f.getValue()));
         }
         
         return options;
     }

    public List<SelectOption> getFunctions() {
         List<SelectOption> options = new List<SelectOption>();
         
         Schema.DescribeFieldResult fieldResult = User.Function__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         
         for(Schema.PicklistEntry f: ple)
         {
             options.add(new SelectOption(f.getLabel(), f.getValue()));
         }
         
         return options;
     }
     
     public List<SelectOption> getTimeZones() {
         List<SelectOption> options = new List<SelectOption>();
         
         Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         
         for(Schema.PicklistEntry f: ple)
         {
             options.add(new SelectOption(f.getValue(), f.getLabel()));
         }
         
         return options;
     }
     
     //public List<SelectOption> getUserVisibilities() {
     //    List<SelectOption> options = new List<SelectOption>();
     //    
     //    Schema.DescribeFieldResult fieldResult = User.User_Visibility__c.getDescribe();
     //    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
     //    
     //    for(Schema.PicklistEntry f: ple)
     //    {
     //        options.add(new SelectOption(f.getValue(), f.getLabel()));
     //    }
     //    
     //    return options;
     //}
     
     public List<SelectOption> getUserProfiles() {
         
         List<SelectOption> options = new List<SelectOption>();
                 
         for(Profile f: [SELECT ID, NAME FROM Profile])
         {
             options.add(new SelectOption(f.ID, f.NAME));
         }
         
         return options;
     }

}