/****************************************************************************************
* Name    : EventTriggerHandlerTest
* Author  : Amogh Ghodke
* Date    : Oct 29, 2015
* Purpose : Test class
* 
* Dependancies: 
* - Must run this test in ENGLISH context
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          	AUTHOR                  CHANGE
* ----          	------                  ------
* Jan 27, 2016     	Kan Hayashi             
* Jan 27, 2016		Gautam Shah				Changes to use the Utilities class to minimize SOQL queries(Lines 24-25, 28, 38); commented out lines 49-57
*****************************************************************************************/ 
@isTest(SeeAllData=true)
public class EventTriggerHandlerTest {
  static testMethod void test1() {
    // Prepare Test Data
    //insert new Trigger_Profile__c(Name='GBU_Franchise_Update',Profile_Name__c='System Administrator');
    //insert new EventTrigger_PostToChatterProfiles__c(Name='System Administrator',active__c=true);
    //Profile p = [SELECT Id FROM Profile WHERE Name='JP - Admin']; 
    Id profileID = Utilities.profileMap_Name_Id.get('System Administrator');
    User u1 = new User(Alias = 'sysad1', Email='sysadmin1@email.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = profileID, 
        TimeZoneSidKey='America/Los_Angeles', UserName='sysadmin1@email.com', Country='JP'
      );
    insert u1;          
    Collaborationgroup g = new CollaborationGroup(Name='TestGroup', CollaborationType='Public',OwnerId = u1.id);
    insert g;
    u1.DailyReportChatterGroupID__c = g.id;
    update u1;
    User u2 = new User(Alias = 'sysad2', Email='sysadmin2@email.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = profileID, 
        TimeZoneSidKey='America/Los_Angeles', UserName='sysadmin2@email.com',
        Sales_Org_PL__c = 'RMS', Region__c = 'EU', Country='JP'
      );
    insert u2;
    // Start Test
    System.runAs(u1) {
      Account a = new Account(Name='Test Account');
      insert a;
      Contact c = new Contact(LastName='Test Contact',accountId=a.id);
      insert c;
/*        
      insert new Event(
        Subject='Test', 
        Activity_Type__C='JP_PR',
        StartDateTime=datetime.newInstance(2016, 10, 31, 10, 00, 0),
        EndDateTime=datetime.newInstance(2016, 10, 31, 11, 00, 0),
        Description='Test Event'
      );

      insert new Event(
        Subject='Test', 
        WhatId=a.id,
        Activity_Type__C='JP_PR',
        StartDateTime=datetime.newInstance(2016, 10, 31, 10, 00, 0),
        EndDateTime=datetime.newInstance(2016, 10, 31, 11, 00, 0),
        Description='Test Event',
        WhoId=c.id,
        JP_Target_Product_Category__c='Sat_Message'
      );
      insert new Event(
        Subject='Test', 
        WhatId=a.id,
        Activity_Type__C='JP_PR',
        StartDateTime=datetime.newInstance(2016, 10, 31, 10, 00, 0),
        EndDateTime=datetime.newInstance(2016, 10, 31, 11, 00, 0),
        Description='Test Event',
        WhoId=c.id,
        JP_Visiting_Destination__c='ICU',
        JP_Visiting_Destination2__c='SCU',
        JP_Target_Product_Category__c='Sat_Message'
      );*/
    }
    System.runAs(u2) {
      Account a = new Account(Name='Test Account');
      insert a;
      Contact c = new Contact(LastName='Test Contact',accountId=a.id);
      insert c;
      Event e = new Event(
        Subject='Test', 
        Activity_Type__C='JP_PR',
        StartDateTime=datetime.newInstance(2016, 10, 31, 10, 00, 0),
        EndDateTime=datetime.newInstance(2016, 10, 31, 11, 00, 0),
        Description='Test Event',
        WhoId=c.id,
        JP_Visiting_Destination__c='ICU',
        JP_Visiting_Destination2__c='SCU',
        JP_Target_Product_Category__c='Sat_Message'
      );
      insert e;
      e.WhatId = a.id;
      update e;
    }
    //System.assertEquals('', '');
  } 
}



/* Old code    
    e.OwnerId = '005U0000003rtLV';
    e.Subject = 'Meeting';
    e.Activity_Type__c = 'Develop Opportunity';
    e.StartDateTime = datetime.newInstance(2015, 10, 10, 11, 46, 0);
    e.EndDateTime = datetime.newInstance(2015, 10, 10, 11, 59, 0);
    e.Description = 'Test Event';
    Insert e;
*/