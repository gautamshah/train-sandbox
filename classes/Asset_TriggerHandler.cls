/****************************************************************************************
 * Name    : Asset_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 17/12/2012 
 * Purpose : Contains all the logic coming from Asset trigger
 * Dependencies: AssetTrigger Trigger
 *             , Asset Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
public with sharing class Asset_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public Asset_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
 
    public void OnBeforeDelete(List<Asset> oldObjects){
        
        //ID apiProfileID = [SELECT Id FROM Profile WHERE Name = 'API Data Loader'].ID;
    Id dataloaderProfile;
    Id sysadminProfile ;
    
    Id currentUser = UserInfo.getUserId();
    for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader' or Name='System Administrator']) 
    {
       if (p.Name == 'API Data Loader' )
       {
             dataloaderProfile = p.Id;
       }
       if (p.Name == 'System Administrator')
       {
             sysadminProfile = p.Id;
       }
       
    }        
       if(UserInfo.getProfileId() != dataloaderProfile && UserInfo.getProfileId() != sysadminProfile)
       {       
            for(Asset item : oldObjects)
            {
                //User cannot delete assets which 
                //API Data Loader profile can delete any asset
                
                if(item.Asset_External_ID__c <> null) //&& userinfo.getProfileId() <> apiProfileID)
                {
                    item.addError(Label.DeleteAssetError);
                }
            }
        }
    }
}