/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB                  Created.
20170123        IL                   Added check on UserMap to initialize in `load` method
20170207        IL                   Added currentUser and getCurrentUser to centralize queries for current user info

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
public class cpqUser_u extends User_u
{
	private static User_g cache = new User_g();
	
	static
	{
		cpqUser_c.load();
	}

	////
	//// put
	////
	
	////
	//// fetch
	////
	public static Map<Id, User> fetch()
	{
		return cache.fetch();
	}
	
	public static Map<Id, User> fetch(set<Id> ids)
	{
		Map<Id, User> existing = cache.fetch(ids);
		
		if (existing.size() != ids.size())
		{
			set<id> toFetch = new set<Id>();
			for(Id id : ids)
				if (!existing.containsKey(id))
					toFetch.add(id);
				
			List<User> fromDB = fetchFromDB(toFetch);
			cache.put(fromDB);

			return cache.fetch(ids);
		}
		else
			return existing;
	}

	public static Map<Id, User> fetchReps()
	{
		Map<Id, User> validReps = new Map<Id, User>();
		
		Map<Id, User> existing = cache.fetch();
		
		for(Id key : existing.keySet())
		{
			User u = existing.get(key);
			
			if (isValidCPQUser(u.Manager) &&
			    isValidCPQUser(u.Manager.Manager) &&
			    isValidCPQUser(u.Manager.Manager.Manager))
			    validReps.put(key, u);
		}
		
		cache.dump('end of fetchReps');
		
		return validReps;
	}
	
	private static boolean isValidCPQUser(User u)
	{
		//DEBUG.dump(
		//	new Map<object, object>
		//	{
		//		'method' => 'cpqUser_u.isValidCPQUser(User u)',
		//		'u' => u
		//	});
		
		return (u != null &&
		        u.Id != null &&
		        u.OrganizationName__c != null &&
		        u.SalesOrganizationLevel__c != null);
	}
	
	public static Map<Id, User> fetch(OrganizationNames_g.Abbreviations abbr)
	{
		return cache.fetch(User.field.OrganizationName__c, abbr.name());
    }
    
	@testVisible
    private static List<User> fetchFromDB(set<Id> ids)
    {
    	string query =
			SOQL_select.buildQuery(
				User.sObjectType,
				cpqUser_c.fieldsToInclude,
				' WHERE Id IN :ids AND isActive = true ',
				null,
				null);
    	
		return (List<User>)Database.query(query);
    }
}