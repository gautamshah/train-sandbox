public interface SOQL_Result_i
{
    Database.Error[] getErrors();
    Id getId();
    boolean isSuccess();
}