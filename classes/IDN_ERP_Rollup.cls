global with sharing class IDN_ERP_Rollup {
    
/****************************************************************************************
* Name    : IDN_ERP_Rollup
* Author  : Shawn Clark
* Date    : 10/06/2016 
* Purpose : Extend the IDN Account--> Retrieve "Grandchild" ERP Records
*****************************************************************************************/

    public final Account AccountDetails {get;set;}
    public list <ERP_Account__c> erp {get;set;}

    // SOQL query loads the case, with related Sample & Sample Product Data
    public IDN_ERP_Rollup(ApexPages.StandardController controller) {
        String actid = ApexPages.currentPage().getParameters().get('id');
        String theQuery = 'SELECT ID, Name, ERP_ID__c, Reporting_Number__c, ERP_Source__c, ' +
            'RMS_Total_Prior_12_Mos_Sales1__c, COV_ERP_Total_Sales__c, Address_1__c, ' +
            'City__c, State_Region__c, ' +
            'Parent_Sell_To_Account__c, Parent_Sell_To_Account__r.Name, ' + 
            'S2_Total_Prior_12_Mos_Sales1__c, VT_Total_Prior_12_Mos_Sales__c, ' +
            'Supplies_Total_Prior_12_Mos_Sales__c, RecordTypeId ' +
            'FROM ERP_Account__c ' +
            'where Parent_Sell_To_Account__c IN ' + 
            '(SELECT Id FROM Account ' +
            'where Covidien_IDN_Relationship__c = :actid)';
        this.erp = Database.query(theQuery);  
    }
   
    // Call ContentType Page to Export to Excel
    public PageReference Export() 
    {
        return page.IDN_ERP_Rollup_ExportExcel;
    }
    
    
}