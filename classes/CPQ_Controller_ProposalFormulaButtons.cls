/*
Name    : CPQ_Controller_ProposalFormulaButtons
Author  : ???
Date    : ???
Purpose : ???
Dependencies: ApexPages: [buttonQuickQuoteRouter], Buttons: [CPQ_QuickQuote]

========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR          CHANGE
----        ------          ------
2016-04-25  Isaac Lewis		Refactored callQuickQuoteURL method to centralize create logic
                            in CPQ_SSG_Controller_CreateDeal
2016-08-05  Isaac Lewis     Added option to process Opportunity Id for RMS flow
10/26/2016  Paul Berglund   Refactored to use OrganizationNames_g
*/
public with sharing class CPQ_Controller_ProposalFormulaButtons
{
	
    private String purpose;
    public String Title{get; set;}
    public Apttus_Proposal__Proposal__c theRecord {get; set;}
    public Boolean isRSCOOP {get; set;}

    public CPQ_Controller_ProposalFormulaButtons(ApexPages.StandardController controller)
    {
        theRecord = (Apttus_Proposal__Proposal__c)controller.getRecord();
        isRSCOOP = CPQ_Utilities.getCoreDealTypeName(theRecord).startsWith('Respiratory_Solutions_COOP');

        purpose = ApexPages.CurrentPage().getParameters().get('purpose');
        if (purpose == 'Deny')
            Title = System.Label.DenyQuoteorProposal;
        else if (purpose == 'AmendedAreas')
            Title = System.Label.CPQ_AmendedAreas;
    }

    public PageReference doAction()
    {
        if (purpose == 'Deny')
            theRecord.Apttus_Proposal__Approval_Stage__c = 'Denied';

        try
        {
        	update theRecord;
        }
        catch (DmlException e)
        {
        	for (Integer i = 0; i < e.getNumDml(); i++)
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDMLMessage(i)));
        }
        return null;
    }

    public Boolean getHasErrors()
    {
        return ApexPages.hasMessages();
    }

    public static PageReference callQuickQuoteURL()
    {
        String varAccountId = ApexPages.currentPage().getParameters().get('varAccountId');
        String varOpportunityId = ApexPages.currentPage().getParameters().get('varOpportunityId');
        // String varOpportunityId =   (ApexPages.currentPage().getParameters().get('varOpportunityId') == NULL)
        //                             ? ''
        //                             : ApexPages.currentPage().getParameters().get('varOpportunityId');
        ////User user = [SELECT Id, Sales_Org_PL__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        ////CPQ_Utilities.SalesOrg salesOrg = CPQ_Utilities.getSalesOrg(user.Sales_Org_PL__c);
		OrganizationNames_g.Abbreviations org = cpqUser_c.getOrganizationName(cpqUser_c.CurrentUser);

        if (org == OrganizationNames_g.Abbreviations.RMS)
        {
            return new PageReference(
            	'/flow/CPQ_QuickQuote?varAccountID=' +
            	varAccountId +
            	((varOpportunityId == NULL) ? '' : ('&varOpportunityId=' + varOpportunityId)));
        }
        else if (org == OrganizationNames_g.Abbreviations.SSG)
        {
            String sObjectTypeName = 'Apttus_Proposal__Proposal__c';
            RecordType rt = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class, 'Hardware_or_Product_Quote');
            Id varSurgicalQuickQuoteRecordId = rt.Id;

            Apttus_Proposal__Proposal__c surgicalProposal =
            	(Apttus_Proposal__Proposal__c)CPQ_SSG_Controller_CreateDeal.createRecord(
            										sObjectTypeName,
            										varSurgicalQuickQuoteRecordId,
            										varAccountId);

            PageReference proposalPage = new ApexPages.StandardController(surgicalProposal).edit();
            proposalPage.getParameters().put('Id','/' + surgicalProposal.Id);
            proposalPage.getParameters().put('retURL','/' + surgicalProposal.Id);

            // Build the cancel URL.  If the user clicks cancel on the new proposal record,
            // call the Apttus method to delete the new proposal
            // and place the user back on the account record.
        	string cancelURL =
            	'/apex/apttus_proposal__cancelactioninterceptor?actionName=create_oppty_proposal&objectId=' +
                varAccountId +
                '&accountId = ' +
                varAccountId +
                '&proposalId=' +
                surgicalProposal.id +
                '&rollbackId=' +
                surgicalProposal.id;

            proposalPage.getParameters().put('cancelURL','/' + cancelURL );

            return proposalPage;
        }
        else
        { // Must be Unknown, so stay on the same page and do a pop-up?
            return null;
        }
    }
}