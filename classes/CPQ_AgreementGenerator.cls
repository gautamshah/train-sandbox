/**
Handle cloning proposal record.

The page is used for proposal cloning, renewing and amending.
- copies a proposal from another proposal for purposes of "cloning with details", "renewing" or "amending"
- creates a shell proposal based on an agreement

Page parameters:

proposal - source proposal Id
agreement - source agreement Id
purpose - clone, renew, amend, amendpricing, amendfacility
scope - full: creates a complete proposal based on an agreement - not used

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-01-01      Yuli Fintescu		Created
05/09/2016  Paul Berglund    Fix for Clone with Details and new Apttus version:
                             - The product configuration is using a new field called
                               Apttus_Config2__BusinessObjectRefId__c - needs to be
                               set to the new Agreement Id as well.
                             - Needed to create a Task for the LastAcivityDate
                               to work
2016-09-28		Bryan Fry	Set Sent_Activation_Email__c to false when cloning
===============================================================================
*/
public class CPQ_AgreementGenerator {
	public class AgreementGeneratorException extends Exception{}
	public Enum SOURCETYPE{AGREEMENT}
	
/*====================
	callback listeners
======================*/
	private List<CPQ_AgreementGeneratorCallBack> callbackListeners;
	public void addCallBackListener(CPQ_AgreementGeneratorCallBack l) {
		callbackListeners.add(l);
	}
	
/*===================
	constructor
=====================*/
	public CPQ_AgreementGenerator() {
		callbackListeners = new List<CPQ_AgreementGeneratorCallBack>();
	}
	
/*===================
	members
=====================*/
	private Apttus__APTS_Agreement__c sourceAgreement;
    private Apttus_Config2__ProductConfiguration__c sourceConfig;

	private Apttus__APTS_Agreement__c outputAgreement;
	public Apttus__APTS_Agreement__c getOutputAgreement() {
		return outputAgreement;
	}
	
	private Apttus_Config2__ProductConfiguration__c outputConfig;
	private Map<ID, Apttus_Config2__LineItem__c> outputLineItems;
	
/*=====================
	sobjects field sets
=======================*/
	public Set<String> ConfigFields {
		get {
			if (configFields == null) {
	        	configFields = CPQ_Utilities.getCustomFieldNames(Apttus_Config2__ProductConfiguration__c.SObjectType, true); 
			}
			return configFields;
		}
		private set;
	}
	
	public Set<String> LineItemFields {
		get {
			if (LineItemFields == null) {
	        	LineItemFields = CPQ_Utilities.getCustomFieldNames(Apttus_Config2__LineItem__c.SObjectType, true); 
			}
			return LineItemFields;
		}
		private set;
	}
	
	public Set<String> AgreementFields {
		get {
			if (AgreementFields == null) {
	        	AgreementFields = CPQ_Utilities.getCustomFieldNames(Apttus__APTS_Agreement__c.SObjectType, true); 
			}
			return AgreementFields;
		}
		private set;
	}
	
	public Set<String> AgreementFacilityFields {
		get {
			if (AgreementFacilityFields == null) {
	        	AgreementFacilityFields = CPQ_Utilities.getCustomFieldNames(Agreement_Participating_Facility__c.SObjectType, true); 
			}
			return AgreementFacilityFields;
		}
		private set;
	}
	
	public Set<String> AgreementDistributorFields {
		get {
			if (AgreementDistributorFields == null) {
	        	AgreementDistributorFields = CPQ_Utilities.getCustomFieldNames(Agreement_Distributor__c.SObjectType, true); 
			}
			return AgreementDistributorFields;
		}
		set;
	}
	
	public Set<String> AgreementRebateFields {
		get {
			if (AgreementRebateFields == null) {
	        	AgreementRebateFields = CPQ_Utilities.getCustomFieldNames(Rebate_Agreement__c.SObjectType, true); 
			}
			return AgreementRebateFields;
		}
		private set;
	}
	
/*=====================
	private vars
=======================*/
	private List<Agreement_Participating_Facility__c> outputFacilities;
	private List<Agreement_Distributor__c> outputDistributors;
	private List<Rebate_Agreement__c> outputRebates;
	private SOURCETYPE source;
	
/*================================================================
	determine the source and generate agreement from source record
==================================================================*/
	public void doGenerate(String sourceID, String oppID, SOURCETYPE source) {
		if (String.isEmpty(sourceID))
			throw new AgreementGeneratorException('No Source ID');
		
		this.source = source;
		
		if (source == SOURCETYPE.AGREEMENT) {//clone agreement from agrement
			doGenerateFromAgreement(sourceID, oppID);
			return;
		}
		
	}
	
	//============================
	//Simple clone agreement from agreement
	//============================
	public void doGenerateFromAgreement(String sourceID, String oppID) {
		QuerySource_Agreement(sourceID);
		CreateRecords_Agreement(oppID);
	}
	
	//query the source agreement for simple clone, query only config and line items
	//============================================================================
	private void QuerySource_Agreement(String sourceID) {
        String special = 'OwnerID, RecordTypeID, RecordType.Name, RecordType.DeveloperName, (Select ID, Apttus_Config2__Status__c From Apttus_CMConfig__Configurations__r Where Apttus_Config2__Status__c not in (\'New\', \'Superseded\') Order by Apttus_Config2__VersionNumber__c desc)';
        String queryStr = CPQ_Utilities.buildObjectQuery(AgreementFields, special, 'ID = \'' + String.escapeSingleQuotes(sourceID) + '\'', 'Apttus__APTS_Agreement__c');
 	    List<Apttus__APTS_Agreement__c> records = ((List<Apttus__APTS_Agreement__c>)Database.query(queryStr));
        if (records.size() == 0)
        	throw new AgreementGeneratorException('The source agreement is not found.');
        
        sourceAgreement = records[0];
        if (sourceAgreement.Apttus_CMConfig__Configurations__r.size() == 0)
        	throw new AgreementGeneratorException('Error:  you cannot make a clone of an agreement that does not have previously configured products.');
        
        String configId = sourceAgreement.Apttus_CMConfig__Configurations__r[0].ID;
        
        String configQueryStr = CPQ_Utilities.buildObjectQuery(ConfigFields, null, 'ID = \'' + configId + '\'', 'Apttus_Config2__ProductConfiguration__c'); 
        String lineQueryStr = CPQ_Utilities.buildObjectQuery(LineItemFields, null, 'Apttus_Config2__ConfigurationId__c = \'' + configId + '\'', 'Apttus_Config2__LineItem__c'); 
        String facilityQueryStr = CPQ_Utilities.buildObjectQuery(AgreementFacilityFields, null, 'Agreement__c = \'' + sourceAgreement.ID + '\'', 'Agreement_Participating_Facility__c'); 
        String distributorQueryStr = CPQ_Utilities.buildObjectQuery(AgreementDistributorFields, null, 'Agreement__c = \'' + sourceAgreement.ID + '\'', 'Agreement_Distributor__c'); 
        String rebateQueryStr = CPQ_Utilities.buildObjectQuery(AgreementRebateFields, 'RecordTypeID, RecordType.Name, RecordType.DeveloperName', 'Related_Agreement__c = \'' + sourceAgreement.ID + '\'', 'Rebate_Agreement__c'); 

        sourceConfig = ((List<Apttus_Config2__ProductConfiguration__c>)Database.query(configQueryStr))[0];
        List<Apttus_Config2__LineItem__c> sourceLines = (List<Apttus_Config2__LineItem__c>)Database.query(lineQueryStr);
        List<Agreement_Participating_Facility__c> sourceFacilities = (List<Agreement_Participating_Facility__c>)Database.query(facilityQueryStr);
        List<Agreement_Distributor__c> sourceDistributors = (List<Agreement_Distributor__c>)Database.query(distributorQueryStr);
        List<Rebate_Agreement__c> sourceRebates = (List<Rebate_Agreement__c>)Database.query(rebateQueryStr);
        
        //copy agreement
        outputAgreement = new Apttus__APTS_Agreement__c();
        for (String fieldName : AgreementFields) {
			outputAgreement.put(fieldName, sourceAgreement.get(fieldName));
        }
		
		//copy config
        outputConfig = new Apttus_Config2__ProductConfiguration__c();
        for (String fieldName : ConfigFields) {
			outputConfig.put(fieldName, sourceConfig.get(fieldName));
        }
       	
        //copy line items
        outputLineItems = new Map<ID, Apttus_Config2__LineItem__c>();
		for (Apttus_Config2__LineItem__c s : sourceLines) {
			Apttus_Config2__LineItem__c t = new Apttus_Config2__LineItem__c();
			for (String fieldName : LineItemFields) {
				t.put(fieldName, s.get(fieldName));
			}
			outputLineItems.put(s.ID, t);
		}
		
        //copy facilities
        outputFacilities = new List<Agreement_Participating_Facility__c>();
		for (Agreement_Participating_Facility__c s : sourceFacilities) {
			Agreement_Participating_Facility__c t = new Agreement_Participating_Facility__c();
			for (String fieldName : AgreementFacilityFields) {
				t.put(fieldName, s.get(fieldName));
			}
			outputFacilities.add(t);
		}
		
        //copy distributors
        outputDistributors = new List<Agreement_Distributor__c>();
		for (Agreement_Distributor__c s : sourceDistributors) {
			Agreement_Distributor__c t = new Agreement_Distributor__c();
			for (String fieldName : AgreementDistributorFields) {
				t.put(fieldName, s.get(fieldName));
			}
			outputDistributors.add(t);
		}
		
		//copy rebates
		outputRebates = new List<Rebate_Agreement__c>();
        for (Rebate_Agreement__c s : sourceRebates) {
        	Rebate_Agreement__c t = new Rebate_Agreement__c();
        	for (String fieldName : AgreementRebateFields) {
				t.put(fieldName, s.get(fieldName));
			}
			outputRebates.add(t);
        }
	}
	
	//create the agreement. simple clone, clone only config and line items
	//============================================================================
	private void CreateRecords_Agreement(String oppID) {
		CPQ_RollupFieldEngine rollupEngine = new CPQ_RollupFieldEngine();
		
		//non-copied fields in agreement
        if (!String.isEmpty(oppID))
        	outputAgreement.Apttus__Related_Opportunity__c = oppID;
        
        outputAgreement.OwnerID = cpqUser_c.CurrentUser.Id;
		
		rollupEngine.CleanTargetFields(new List<Apttus__APTS_Agreement__c>{outputAgreement});
        outputAgreement.Name = sourceAgreement.Name.left(75) + ' Copy';
        outputAgreement.Apttus_CMConfig__ConfigurationFinalizedDate__c = null;
        outputAgreement.Apttus_CMConfig__ConfigurationSyncDate__c = null;
        outputAgreement.Apttus__Contract_End_Date__c = null;
        outputAgreement.Apttus__Contract_Start_Date__c = null;
        
        outputAgreement.Legacy_External_ID__c = null;
        outputAgreement.Net_Discount__c = null;
        outputAgreement.Presented_Date__c = null;
        outputAgreement.Apttus_CMConfig__PricingDate__c = null;
        outputAgreement.Apttus__Agreement_Number__c = null;
        outputAgreement.APIP_Rebate_Option_1_Customer_Loyalty__c = false;
		outputAgreement.APIP_Rebate_Option_2_High_Growth_Optio__c = false;
		outputAgreement.CPP_PM_HPG_Accrual_Rebate__c = false;
		outputAgreement.CPP_PM_Standard_Accrual_Rebate__c = false;
		outputAgreement.CPP_RS_Standard_Accrual_Rebate__c = false;
		outputAgreement.INVOS_VIP_Rebate_Program__c = false;
		outputAgreement.PM_Growth_Rebate_Option_1_Growth_Rebat__c = false;
		outputAgreement.PM_Growth_Rebate_Option_2_Incremental__c = false;
		outputAgreement.PM_Standard_Rebate_Option_1_One_Rebate__c = false;
		outputAgreement.PM_Standard_Rebate_Option_2_Tiered_Reb__c = false;
		outputAgreement.Rebates_Selected__c = false;
		outputAgreement.Shamrock_Rebate__c = false;
		outputAgreement.Early_Signing_Incentive__c = false;
		outputAgreement.Distributors_Selected_Template__c = false;
        outputAgreement.Task_Contract_Dev_Comments__c = null;
        outputAgreement.Task_Contract_Dev_CompletedBy__c = null;
        outputAgreement.Task_Contract_Dev_CompletedOn__c = null;
        outputAgreement.Task_Customer_Service_Selected_Docs__c = null;
        outputAgreement.Sent_Activation_Email__c = false;
		
		//non-copied fields in config
        outputConfig.OwnerID = cpqUser_c.CurrentUser.Id;
        outputConfig.Apttus_CQApprov__Approval_Preview_Status__c = 'Pending';
        outputConfig.Apttus_Config2__VersionNumber__c = 1;
        outputConfig.Apttus_Config2__Status__c = 'Saved';
        outputConfig.Apttus_Config2__IsTransient__c = true;
        
		rollupEngine.CleanTargetFields(new List<Apttus_Config2__ProductConfiguration__c>{outputConfig});
        outputConfig.Apttus_CMConfig__AgreementId__c = null;
        outputConfig.Apttus_CQApprov__Approval_Status__c = null;
        outputConfig.Apttus_Config2__AncestorId__c = null;
        outputConfig.Apttus_Config2__CartDataCache__c = null;
        outputConfig.Apttus_Config2__CartDataCache2__c = null;
        outputConfig.Apttus_Config2__EffectiveDate__c = null;
        outputConfig.Apttus_Config2__ExpectedEndDate__c = null;
        outputConfig.Apttus_Config2__ExpectedStartDate__c = null;
        outputConfig.Apttus_Config2__FinalizedDate__c = null;
        outputConfig.Apttus_Config2__PrimordialId__c = null;
		
		//non-copied fields in line items
		for (Apttus_Config2__LineItem__c t : outputLineItems.values()) {
            t.Apttus_Config2__SummaryGroupId__c = null;
            t.Apttus_Config2__AdHocGroupId__c = null;
            t.Apttus_Config2__DerivedFromId__c = null;
            
    		if (t.Apttus_Config2__LineType__c != 'Misc') {
            	t.Apttus_Config2__PricingStatus__c = 'Pending';
    		}
    		
            t.Apttus_CQApprov__Approval_Status__c = null;
            t.Price_Mode__c = null;
            t.Commitment_Type__c = null;
            t.GPO__c = null;
            t.Non_Committed_Price__c = null;
            t.Current_Root_Contract__c = null;
		}
		
		//user can add additional works here before generate the records
        for (CPQ_AgreementGeneratorCallBack l : callbackListeners) {
			if (source == SOURCETYPE.AGREEMENT)
			l.BeforeGenerate(sourceAgreement, outputAgreement, outputConfig, outputLineItems.values());
		}
		
		Savepoint sp = Database.setSavepoint();
		try {
			
			insert outputAgreement;
			
			outputConfig.Name = outputAgreement.Name.left(80);
	        outputConfig.Apttus_Config2__AccountId__c = outputAgreement.Apttus__Account__c;
            outputConfig.Apttus_CMConfig__AgreementId__c = outputAgreement.ID;
            outputConfig.Apttus_Config2__BusinessObjectId__c = outputAgreement.ID;
            // PAB05022016 Added this field
            outputConfig.Apttus_Config2__BusinessObjectRefId__c = outputAgreement.ID;
            outputConfig.Apttus_Config2__BusinessObjectType__c = 'Agreement';
            insert outputConfig;
            
            // Added this for the LastActivityDate to work
            Task configCreatedTask = new Task();
            configCreatedTask.WhatId = outputConfig.Id;
            configCreatedTask.OwnerId  = cpqUser_c.CurrentUser.Id; 
            configCreatedTask.Subject = 'Created Version';
            configCreatedTask.Description = 'Cloned to Version 1';
            configCreatedTask.Business_Unit__c = 'RMS';
            configCreatedTask.Type = 'Telephone Call';
            configCreatedTask.Status = 'Completed';
            configCreatedTask.Priority = 'Normal';
            configCreatedTask.ActivityDate = DateTime.now().date();
            insert configCreatedTask;

            for (Apttus_Config2__LineItem__c t : outputLineItems.values()) {
            	t.Apttus_Config2__ConfigurationId__c = outputConfig.ID;
            }
            if (outputLineItems.size() > 0)
            	insert outputLineItems.values();
            
            for (Agreement_Participating_Facility__c t : outputFacilities) {
                t.Agreement__c = outputAgreement.ID;
            }
			if (outputFacilities.size() > 0)
			    insert outputFacilities;
			    
            for (Agreement_Distributor__c t : outputDistributors) {
                t.Agreement__c = outputAgreement.ID;
            }
			if (outputDistributors.size() > 0)
			    insert outputDistributors;
			
            for (Rebate_Agreement__c t : outputRebates) {
                t.Related_Agreement__c = outputAgreement.ID;
            }
			if (outputRebates.size() > 0)
			    insert outputRebates;

		} catch (Exception e) {
			Database.rollback(sp);
			throw new AgreementGeneratorException(e.getMessage());
		}
	}
}