global class BatchCopyInTransit implements Database.Batchable<sObject> {
    
    String Query;
    global BatchCopyInTransit(String q){
             Query=q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);  
    }
    
     global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<In_Transit__c> intransitout= new list<In_Transit__c>();
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
        	CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=	String.valueOf(dt.year());
        //List<Sales_In__c> letTest=[select  name, quantity_remaining__c,Distributor_Name__c,(select Id from Sales_In_Ack__r where Cycle_Period_dist__r.Cycle_Period_Reference__r.Month__c=:CurrentCyclePeriodMonth and Cycle_Period_dist__r.Cycle_Period_Reference__r.Year__c=:CurrentCyclePeriodYear) from sales_in__c where quantity_remaining__c > 0];
        Map<Id,Id> cpMap=new Map<Id,Id>();
        List<Cycle_Period__c> lstCurrentCyclePeriod=[Select Id,Distributor_Name__c from Cycle_Period__c where Cycle_Period_Reference__r.Month__c=:CurrentCyclePeriodMonth and Cycle_Period_Reference__r.Year__c=:CurrentCyclePeriodYear];
        for(Cycle_Period__c cp:lstCurrentCyclePeriod)
        {
        	if(cpMap.keyset().contains(cp.Distributor_Name__c)==false)
        		cpMap.put(cp.Distributor_Name__c, cp.Id);
        }
        for (Sales_In__c sinraw: (List<Sales_In__c>)scope)
        {
        	if(cpMap.keyset().contains(sinraw.Distributor_Name__c))
        	{
        	
	        	if(sinraw.Sales_In_Ack__r==null||sinraw.Sales_In_Ack__r.size()==0)
	        	{
		            In_Transit__c finalintransitout= new In_Transit__c();
		             
		          	finalintransitout.Cycle_Period_dist__c=cpMap.get(sinraw.Distributor_Name__c);
		          	//finalintransitout.Quantity_Received__c=	sinraw.Quantity_Remaining__c;
		          	system.debug(sinraw.id);
		            finalintransitout.Sales_In__c = sinraw.id;
		      // added by Srinivas on 8-14 - Need to fill the Quantity acknowledged in Prev cycles field 
		            //finalintransitout.quantity_acknowledged_in_prev_cycles__c = sinraw.Quantity_Remaining__c;     
		            intransitout.add(finalintransitout);
	        	}
        		
        	}
                  
        }
        if(intransitout.size()>0)
        	insert intransitout;
        
     }
     
      global void finish(Database.BatchableContext BC){  
        }

}