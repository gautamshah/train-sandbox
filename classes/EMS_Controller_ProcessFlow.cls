public with sharing class EMS_Controller_ProcessFlow {
    public Id EventId; 
    Public String redirectURL{get;set;}  
    public string chosenstep{get;set;}
    public boolean shouldRedirect{get;set;}
    public EMS_Event__c currentEMSRecord{get;set;}
    public String SamplesString{get;set;}
    
    
    public EMS_Controller_ProcessFlow(ApexPages.StandardController controller) {
    if(ApexPages.currentPage().getParameters().get('id')!=null){
        EventId= [select id from EMS_Event__c where id = :ApexPages.currentPage().getParameters().get('id')].Id;
    }
     
    else{
    EventId= ApexPages.currentPage().getParameters().get('EventId');
    }
    
    chosenstep= ApexPages.currentPage().getParameters().get('step');
    System.debug('------->=='+chosenstep);
    if(chosenstep==null)
        chosenstep='0';
        
        
    //Id profileId=userinfo.getProfileId();
    String Countryname=[Select Id,Name,Country from User where Id=:UserInfo.getUserId() limit 1].Country;
    if(Countryname=='AU' || Countryname=='NZ')
    SamplesString='Samples';
    else
    SamplesString='Samples/CCI';
    system.debug('--->'+chosenstep);
    if(EventId!=null){
     CurrentEMSRecord=[Select Id,Name,Start_Date__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Design_Fee_Remarks__c,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Fee__c,Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,ISR_CSR__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,Total_Local_GTA__c,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Venue__c,Publication_grant__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,
                                  Total_No_of_Recipients__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Division_Multiple__c,GBU_Multiple__c 
                                  from EMS_Event__c where Id=:EventId];
    }
    }
 
 
  public Pagereference redirectmainpage(){
   shouldRedirect=true;
   redirectURL='/'+EventId;
   //P.setredirect(true);
   return null; 
  } 
 
 
    
 public Pagereference redirectGBU(){
  shouldRedirect=true;
  redirectURL='/apex/EMS_Vf_EnterDetails?EventId='+EventId+'&step=1';
 //P.setredirect(true);
 return null;
 
 }  
 
public Pagereference createnewEMSRecord(){

System.debug('--->called');

//shouldRedirect=true;
Pagereference P= new Pagereference('/a1u/e?retURL=%2Fa1u%2Fo&nooverride=1');
P.setRedirect(true);
//redirectUrl='/a1u/e?retURL=%2Fa1u%2Fo';
return P;

}

public Pagereference redirectNonHCP(){
  shouldRedirect=true;
  redirectURL='/apex/EMS_VFNonHCPExpenses?EventId='+EventId+'&step=3';
 //P.setredirect(true);
 return null;
 
 }  
 
 public Pagereference redirectFRDescription(){
  shouldRedirect=true;
  redirectURL='/apex/EMS_VFFundRSummary?EventId='+EventId+'&step=4';

 //P.setredirect(true);
 return null;
 
 }
 
 public PageReference redirectBeneficiaries(){
  shouldRedirect=true;
  redirectURL='/apex/EMS_VFBudgetReciepients?EventId='+EventId+'&step=2';
 //P.setredirect(true);
 return null;
 
 }
 
 public PageReference redirectCovidien(){
   shouldRedirect=true;

 redirectURL='/apex/EMS_EnterCovStaffDetails?EventId='+EventId+'&step=5';
 //P.setredirect(true);
 return null;
 
 }
 
 public Pagereference redirectAttachDocuments(){
   shouldRedirect=true;

 redirectURL='/apex/EMS_OverrideAddAttachmentButton?EventId='+EventId+'&step=6';
 return null;
 }
 
 public Pagereference redirectActualRecipients(){
   shouldRedirect=true;

 redirectURL='/apex/EMS_VFActualRecipients?EventId='+EventId+'&step=9';
 return null;
 }
 public Pagereference redirectSKUSamplesCCI(){
   shouldRedirect=true;

 redirectURL='/apex/EMS_SamplesCCIPage?EventId='+EventId+'&step=7';
 return null;
 }
 public Pagereference redirectCCIExpenses(){
   shouldRedirect=true;

 redirectURL='/apex/EMS_VFAddCCIExpenses?EventId='+EventId+'&step=8';
 return null;
 }
 

 
}