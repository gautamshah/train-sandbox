/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com

MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD  A    PAB        CPR-000    Updated comments section
                          AV-000
               PAB                   Created.
20170124       IL                    Added comment header.
20170406       BF                    Participating Facility Quantities prevent reconfiguring a finalized cart

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

public class cpqProposalLineitem_u {
    public static final String HARDWARE = 'Hardware';
    public static final String TRADE_IN = 'Trade-In';

    public static List<Apttus_Proposal__Proposal_Line_Item__c> filterByCategory(
    	List<Apttus_Proposal__Proposal_Line_Item__c> lineItems,
    	String category)
	{
        List<Apttus_Proposal__Proposal_Line_Item__c> filteredLineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        for (Apttus_Proposal__Proposal_Line_Item__c pli: lineItems) {
            if (pli.Apttus_QPConfig__ChargeType__c == category) {
                filteredLineItems.add(pli);
            }
        }
        return filteredLineItems;
    }

    public static void deleteRelatedParticipatingFacilityLineItems(List<Apttus_Proposal__Proposal_Line_Item__c> lineItems) {
        List<Id> proposalLineItemIds = new List<Id>();
        for (Apttus_Proposal__Proposal_Line_Item__c pli: lineItems) {
            proposalLineItemIds.add(pli.Id);
        }

        List<cpqParticipatingFacility_LineItem__c> participatingFacilityLineItems = 
            [Select Id
             From cpqParticipatingFacility_LineItem__c
             Where ProposalLineItem__c in :proposalLineItemIds];

        delete participatingFacilityLineItems;
    }
}