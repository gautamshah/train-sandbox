@isTest

public class AsiaPriceQueryTest{
    static testMethod void AsiaPriceQueryTest(){
        
        test.startTest();
        User u = [select id, Business_Unit__c, Country from User where id = :UserInfo.getUserId()];
        u.Business_Unit__c = 'S2';
        u.Country = 'SG';
        update u;

        //Test converage for the myPage visualforce page
        PageReference pageRef = Page.Asia_LTP_Price_Query;
        Test.setCurrentPageReference(pageRef);
        
        //Create Test Account
        //RecordType rt = [SELECT id FROM RecordType WHERE Name='ASIA-Healthcare Facility' AND SobjectType='Account']; 
        ERP_Account__c newERPAcct = new ERP_Account__c (name='XYZ Organization');
        insert newERPAcct;
        Account newAccount = new Account (name='XYZ Organization');
        insert newAccount;
        
        //Create list of Product SKU
        List<Product_SKU__c> skuList = new List<Product_SKU__c>();
        
        Product_SKU__c sku1 = new Product_SKU__c(name='Test SKU1', SKU__c='T111', Vendor_Item_Code__c = '111', 
                                                    Selling_UOM__c = 'ea', Sales_Class__c = 'AA', 
                                                    Country__c=u.Country, GBU__c = 'SD');
        Product_SKU__c sku2 = new Product_SKU__c(name='Test SKU2', SKU__c='T222', Vendor_Item_Code__c = '222', 
                                                    Selling_UOM__c = 'ea', Sales_Class__c = 'AA', 
                                                    Country__c=u.Country, GBU__c = 'SD');
        Product_SKU__c sku3 = new Product_SKU__c(name='Test SKU3', SKU__c='T333', Vendor_Item_Code__c = '333', 
                                                    Selling_UOM__c = 'ea', Sales_Class__c = 'AA', 
                                                    Country__c=u.Country, GBU__c = 'SD');
        
        //Create List of Test Sales Transactions
        List<Sales_Transaction__c > stList = new List<Sales_Transaction__c>();
        Sales_Transaction__c st1 = new Sales_Transaction__c(name='Test Prod1', Shipped_Quantity_Eaches__c=1, 
                                                            Net_Sales_Amount__c=10, Invoice_Date__c=System.Today(), 
                                                            SKU__C='T111', SellToAccount__c=newAccount.id,
                                                            ERP_Account__c=newERPAcct.id, Business_Unit__c='SD');
        Sales_Transaction__c st2 = new Sales_Transaction__c(name='Test Prod2', Shipped_Quantity_Eaches__c=2, 
                                                            Net_Sales_Amount__c=30, Invoice_Date__c=System.Today(), 
                                                            SKU__C='T222', SellToAccount__c=newAccount.id,
                                                            ERP_Account__c=newERPAcct.id, Business_Unit__c='SD');
        Sales_Transaction__c st3 = new Sales_Transaction__c(name='Test Prod3', Shipped_Quantity_Eaches__c=5, 
                                                            Net_Sales_Amount__c=50, Invoice_Date__c=System.Today(), 
                                                            SKU__C='T333', SellToAccount__c=newAccount.id,
                                                            ERP_Account__c=newERPAcct.id, Business_Unit__c='SD');                                            
        stList.add(st1); stList.add(st2); stList.add(st3);
        insert stList;
        
        //Create List of Test Products with Pricebook and Pricebook Entries
        Pricebook2 pb = new Pricebook2(name=u.Country+' - Pricebook', isActive=true);
        insert pb;
        ID standardPBID = Test.getStandardPricebookId();
        
        List<Product2> pList = new List<Product2>();
        Product2 prod1 = new Product2(name='Test Prod1', Country__c=u.Country , IsActive=true, ProductCode='T111', GBU__c='SD', Selling_Quantity__c =1);
        Product2 prod2 = new Product2(name='Test Prod2', Country__c=u.Country , IsActive=true, ProductCode='T222', GBU__c='SD', Selling_Quantity__c =2);
        Product2 prod3 = new Product2(name='Test Prod3', Country__c=u.Country , IsActive=true, ProductCode='T333', GBU__c='SD', Selling_Quantity__c =0);
        pList.Add(prod1); pList.Add(prod2); pList.Add(prod3);
        insert pList;
        
        List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
        PriceBookEntry pbe1s = new PriceBookEntry(Product2Id=prod1.id, Pricebook2Id=Test.getStandardPricebookId(), UnitPrice=0, IsActive = true);
        PriceBookEntry pbe1 = new PriceBookEntry(Product2Id=prod1.id, Pricebook2Id=pb.id, UnitPrice=8, IsActive = true);
        PriceBookEntry pbe2s = new PriceBookEntry(Product2Id=prod2.id, Pricebook2Id=Test.getStandardPricebookId(), UnitPrice=0, IsActive = true);
        PriceBookEntry pbe2 = new PriceBookEntry(Product2Id=prod2.id, Pricebook2Id=pb.id, UnitPrice=0, IsActive = true);
        PriceBookEntry pbe3s = new PriceBookEntry(Product2Id=prod3.id, Pricebook2Id=Test.getStandardPricebookId(), UnitPrice=0, IsActive = true);
        PriceBookEntry pbe3 = new PriceBookEntry(Product2Id=prod3.id, Pricebook2Id=pb.id, UnitPrice=12, IsActive = true);
        pbeList.add(pbe1s); pbeList.add(pbe2s); pbeList.add(pbe3s);
        insert pbeList;
        pbeList.clear();
        pbeList.add(pbe1); pbeList.add(pbe2); pbeList.add(pbe3);
        insert pbeList;

        //Set Standard controller
        ApexPages.StandardController sc = new ApexPages.standardController(newAccount);
        // create an instance of the controller
        AsiaLTPQueryExtension myPageCon = new AsiaLTPQueryExtension(sc);
        
        //try calling methods/properties of the controller in all possible scenarios
        // to get the best coverage.
        //Search no-product
        myPageCon.searchPrd = 'FAIL';
        myPageCon.doPrdSearch();
        //Search product
        myPageCon.searchPrd = 'Test';
        myPageCon.doPrdSearch();
        
        //search price
        myPageCon.searchText = '111';
        myPageCon.doSearch();
        myPageCon.searchText = '222';
        myPageCon.doSearch();
        myPageCon.searchText = '333';
        myPageCon.doSearch();
        //search no price
        myPageCon.searchText = '444';
        myPageCon.doSearch();
        
        test.stopTest();
    }
}