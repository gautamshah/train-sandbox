@isTest
public class CaseUserDetails_HandlerTest {
/****************************************************************************************
    * Name    : CaseUserDetails_HandlerTest
    * Author  : Brajmohan Sharma
    * Date    : 24-Apr-2017
    * Purpose : it will aware about case work load of user.
    * case    : 00981782
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/ 
  static @testSetup void testCase1()
    { 
    Profile P = [select id from profile where name = 'system administrator'];
    String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*100000));
    String uniqueName = orgId + dateString + randomInt;
   User  tuser = new User(  firstname = 'testcase1',
                            lastName = 'data11',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = P.Id);
                            //UserRoleId = roleId);
     insert tuser; 
        
       List<User> userlist = [select id from user where user.FirstName ='testcase1' AND user.LastName = 'data11'];
       Case c = new Case();
       c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IDNSC Case Feed').getRecordTypeId();
       c.Current_State__c = 'In Process';
       c.Status = 'In Process';
       c.ownerid = userlist.get(0).id;
       c.Origin = 'Collaboration Request';
       insert c;
    }
     
     static testMethod void testCase2(){
        List<User> tuser1 = [select id from user where user.FirstName ='testcase1' AND user.LastName = 'data11'];
        List<User> userlist = [select id from user where user.FirstName ='Amogh' AND user.LastName = 'Ghodke'];
        List<case> cs = new List<case>();
        List<case> csit = new List<case>();
        cs = [select id from case where ownerid =: tuser1.get(0).id and Status = 'In Process'];
            for(case c : cs){
                c.OwnerId = userlist.get(0).id;
                csit.add(c);
            }
             Database.SaveResult[] srList = Database.update(csit,false);
             for (Database.SaveResult sr : srList) {
             if (sr.isSuccess()) {
             System.assertEquals(true, sr.isSuccess());
              }
             else{
              System.assertNotEquals(true, sr.isSuccess());
             }
           
          }
      } 
    static testMethod void testCase3(){
        List<User> tuser1 = [select id from user where user.FirstName ='Amogh' AND user.LastName = 'Ghodke'];
        List<User> userlist = [select id from user where user.FirstName ='Brajmohan' AND user.LastName = 'Sharma'];
        List<case> cs = new List<case>();
        List<case> csit = new List<case>();
        cs = [select id from case where ownerid =: tuser1.get(0).id];
            for(case c : cs){
                c.OwnerId = userlist.get(0).id;
                csit.add(c);
            }
        update csit;
    }
    }