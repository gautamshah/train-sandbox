/****************************************************************************************
* Name    : Class: UserDeactivation
* Author  : Stefan Mationg
* Date    : 9/9/2015
* Purpose : Automation to deactivate users and free up paid SFDC licenses on a monthly basis when 
*   1. a user has not logged into SFDC recently (25 users)
*   2. a user has not logged into SFDC ever (25 users)
*   Note: Deactivation only affects license type "Salesforce" 
* Dependancies: 
*   Called By: Apex Scheduler
*   1. **NEW FIELD** - Auto_Deactivate_Date__c - PURPORSE: logs datetime of deactivation
*   2. **NEW FIELD** - Do_Not_Deactivate__c  - PURPOSE: Allows admins to bypass certain users
*
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE              AUTHOR                  CHANGE
* ----              ------                  ------
* Oct. 9, 2015      Gautam Shah             Tweaks and comments
* Nov 6, 2015       Stefan Mationg          Expanded ManagerIDs to include managers of inactive users
* Nov 17,2015       Stefan Mationg          Added Method to DML operation
* Apr 25, 2016      Shawn Clark             Test Class taking too long to run. Added Test.isRunningTest to limit query
*****************************************************************************************/
global class UserDeactivation
{
        Datetime timenow = Datetime.now();
        Datetime datelast3mos = timenow.addDays(-90);   
        Datetime datelast6mos = timenow.addDays(-180);
        Datetime datelast12mos = timenow.addDays(-360);
        public String LicenseType = 'Salesforce';
        public List<User> TotalDeactivate = new List<User>();
        public List<User> AllUsers = new List<User>();
        public Set<id> ManagerIds = new Set<id>();
        global String searchquery {get;set;}
        public String CompanyName = 'UserDeactivationTestFactory';
    
    public void runDeactivation()
        {
        
        ////////////////////////////////////////////////////////
        //FIND MANAGER IDS//////////////////////////////////////
        //// ////////////////////////////////////////////////////
       
        searchquery = 'SELECT ID, ManagerID, User_Manager__c, name, isactive FROM USER WHERE User.Profile.UserLicense.Name = :LicenseType'; 
        
        //If Test Class is running code, add additional filter to limit results
        if (Test.isRunningTest())
        searchquery += ' and CompanyName = \''+String.escapeSingleQuotes(CompanyName)+'\'';
            
        AllUsers = Database.query(searchquery); 
        for(User a : AllUsers)
        {
            
            // -- The conditions below will add both manager values to the set if there is a value in either field.
            if(String.isNotBlank(a.ManagerID))
            {
                ManagerIds.add(a.ManagerID);
            }
            if(String.isNotBlank(a.User_Manager__c))
            {
                ManagerIds.add(a.User_Manager__c);
            }
        }
 
      //************ [START: TEST METHOD CODE ] ************
        
        if (Test.isRunningTest()) 
        {
           //Cannot Test Recently Logged In, as I cannot update LastLoginDate via Apex
           //Get Test Users Who Have Never Logged In      
           List<User> DeActivateList_ZeroLogin = Database.query('SELECT id,User_Manager__c, email, Auto_Deactivate_Date__c, Do_Not_Deactivate__c,'
            + 'name, IsActive, Manager.ID,'
            + 'User.Profile.UserLicense.Name FROM USER '
            + 'WHERE IsActive = TRUE AND LastLoginDate = NULL AND Do_Not_Deactivate__c = FALSE '
            + 'AND User.Profile.UserLicense.Name = :LicenseType '
            + 'AND Auto_Deactivate_Date__c = NULL ' 
            + 'AND ID NOT IN :ManagerIDs '
            + 'AND COMPANYNAME = :CompanyName '
            + 'ORDER BY CreatedDate ASC LIMIT 25'); 
            
           TotalDeactivate.addall(DeactivateList_ZeroLogin);
           Deactivate(TotalDeactivate);             
        }
        
        //************ [END: TEST METHOD CODE ] ************      

         if (!Test.isRunningTest()) 
        {   
        /////////////////////////////////////////////////////////
        //Find bottom 25 USERS Who have not LOGGED In recently//
        /////////////////////////////////////////////////////////
        List<User> DeActivateList_RecentLogin = Database.query('SELECT id,User_Manager__c, email, Auto_Deactivate_Date__c,Do_Not_Deactivate__c, ' 
            + 'name, IsActive, LastLoginDate, Manager.ID, '
            + 'User.Profile.UserLicense.Name FROM User ' 
            + 'WHERE IsActive = TRUE AND LastLoginDate < :datelast6mos AND Do_Not_Deactivate__c = FALSE '
            + 'AND User.Profile.UserLicense.Name = :LicenseType  AND LASTLOGINDATE != NULL '
            + 'AND (Auto_Deactivate_Date__c = NULL OR (Auto_Deactivate_Date__c !=NULL AND LastModifiedDate <:datelast3mos))' 
            + 'AND ID NOT IN :ManagerIDs '
            + 'ORDER BY LastLoginDate ASC LIMIT 25');           

        /////////////////////////////////////////////////////////
        //Find bottom 25 USERS who have NEVER Logged in //
        /////////////////////////////////////////////////////////
        List<User> DeActivateList_ZeroLogin = Database.query('SELECT id,User_Manager__c, email, Auto_Deactivate_Date__c,Do_Not_Deactivate__c,'
            + 'name, IsActive, LastLoginDate, Manager.ID, CreatedDate,'
            + 'User.Profile.UserLicense.Name FROM USER '
            + 'WHERE IsActive = TRUE AND LastLoginDate = NULL AND Do_Not_Deactivate__c = FALSE '
            + 'AND User.Profile.UserLicense.Name = :LicenseType  AND CreatedDate < :datelast12mos '
            + 'AND (Auto_Deactivate_Date__c = NULL OR (Auto_Deactivate_Date__c !=NULL AND LastModifiedDate <:datelast3mos))' 
            + 'AND ID NOT IN :ManagerIDs '
            + 'ORDER BY CreatedDate ASC LIMIT 25'); 
            
        TotalDeactivate.addall(DeactivateList_ZeroLogin);
        TotalDeactivate.addall(DeactivateList_RecentLogin);
         System.Debug('Size of List is should not be seen in test log: ' + TotalDeactivate.size());
    
    Deactivate(TotalDeactivate);
        }
    }

    /////////////////////////////////////////////////////////
    ////METHOD TO COMPLETE CHANGE/////
    /////////////////////////////////////////////////////////
    
    public void Deactivate(List<User> TotalDeactivate2)
        {        
            for(User b :TotalDeactivate2)      
                {
                    b.isActive = FALSE;
                    b.Auto_Deactivate_Date__c = Datetime.now();
                    System.debug('Deactivate User: ' + b.id);
                }     
            

            if(!TotalDeActivate2.isEmpty())
                {
                    // DML statement
                    Database.SaveResult[] srList = Database.update(TotalDeactivate2, false);
                    System.debug('Save Result List size: ' + srList.size());
                    // Iterate through each returned result
                    for (Database.SaveResult sr : srList) 
                        {
                            System.debug('::USER:: ' + sr.getID());
                            if (sr.isSuccess()) 
                                {
                                    // Operation was successful, so get the ID of the record that was processed
                                    System.debug('Successfully updated User ID: ' + sr.getId());
                                   
                                }
                            else 
                                {
                                    // Operation failed, so get all errors                
                                    for(Database.Error err : sr.getErrors()) 
                                        {
                                            System.debug('The following error has occurred.');                    
                                            System.debug('USER: ' + sr.getId() + ' ERROR: ' + err.getStatusCode() + ': ' + err.getMessage());
                                        
                                        }
                                }
                        }
                        
                        
                        
                }
            else
                {
                    System.debug('UPDATE NOT COMPLETED: NO USERS FOUND TO DEACTIVATE');
                }
            
        }
}