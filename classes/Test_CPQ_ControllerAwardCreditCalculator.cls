/*
Test class

CPQ_Controller_AwardCreditCalculator

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-23      Yuli Fintescu       Created
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
===============================================================================
*/
@isTest
private class Test_CPQ_ControllerAwardCreditCalculator
{
    static void createTestData()
    {
        OrganizationNames_g.Abbreviations RMS = OrganizationNames_g.Abbreviations.RMS;

        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

        upsert cpqVariable_TestSetup.create(cpqVariable_TestSetup.Transmit.DISABLED);

        cpq_TestSetup.testRollups = new List<CPQ_Rollup_Field__c>
        {
            new CPQ_Rollup_Field__c(SObject__c = 'Configuration', Code_Group__c = 'Monitor', Field_API_Name__c = 'Equipment_Lines_Subtotal__c', Purpose__c = 'Subtotal', Source__c = 'Category Attribute'),
            new CPQ_Rollup_Field__c(SObject__c = 'Configuration', Code_Group__c = 'Third Party', Field_API_Name__c = 'X3rd_Party_Products_Subtotal__c', Purpose__c = 'Subtotal', Source__c = 'Category Attribute')
        };
        insert cpq_TestSetup.testRollups;

        Map<string, RecordType> proposalRecordTypes = cpqDeal_RecordType_c.getProposalRecordTypesByName();

        cpq_TestSetup.testProducts = cpqProduct2_TestSetup.create();
        insert cpq_TestSetup.testProducts;

        cpq_TestSetup.testPriceListItems = cpqPriceListItem_u.fetch(
            Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
            testPriceList.Id);

    //  cpq_TestSetup.testProducts =
    //      new List<Product2>
    //      {
    //          cpqProduct2_c.create(RMS, 'MONITOR', 1),
    //          cpqProduct2_c.create(RMS, 'SENSOR', 1),
    //          cpqProduct2_c.create(RMS, 'SENSOR', 2),
    //          cpqProduct2_c.create(RMS, '3rdPARTY', 1),
    //          cpqProduct2_c.create(RMS, 'NADA', 1)
    //      };
    //  insert cpq_TestSetup.testProducts;
        //
        // Map<Id, Product2> product2Map = new Map<Id, Product2>(cpq_TestSetup.testProducts);
        //
        // cpq_TestSetup.testPriceListItems = new Map<Id, Apttus_Config2__PriceListItem__c>(
        //  [SELECT Id
        //   FROM Apttus_Config2__PriceListItem__c
        //   WHERE Apttus_Config2__ProductId__c = :product2Map.keySet()]);
        //
        // List<Apttus_Config2__PriceListItem__c> testPriceListItemsList = new List<Apttus_Config2__PriceListItem__c>();
        // for (Id pId : product2Map.keySet()) {
        //  testPriceListItemsList.add(cpqPriceListItem_c.create(testPriceList.Id,pId));
        // }
        // insert testPriceListItemsList;
        // cpqPriceListItem_u.addAll(testPriceListItemsList);
        // cpq_TestSetup.testPriceListItems = cpqPriceListItem_u.fetch();

        cpq_TestSetup.testAccounts =
            new List<Account>
            {
                new Account(
                    Status__c = 'Active',
                    Name = 'Test Account',
                    Account_External_ID__c = 'US-111111'),
                new Account(
                    Status__c = 'Active',
                    Name = 'Test Account NO CRN'),
                new Account(
                    Status__c = 'Active',
                    Name = 'Facilitiy',
                    Account_External_ID__c = 'US-222222')
            };
        insert cpq_TestSetup.testAccounts;

        cpq_TestSetup.testProposals =
            new List<Apttus_Proposal__Proposal__c>
            {
                new Apttus_Proposal__Proposal__c(
                    Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                    RecordTypeID = proposalRecordTypes.get('Respiratory_Solutions_COOP').Id,
                    Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID),
                new Apttus_Proposal__Proposal__c(
                    Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                    RecordTypeID = proposalRecordTypes.get('Respiratory_Solutions_COOP').Id,
                    Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[0].ID),
                new Apttus_Proposal__Proposal__c(
                    Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
                    RecordTypeID = proposalRecordTypes.get('Respiratory_Solutions_COOP').Id,
                    Apttus_Proposal__Account__c = cpq_TestSetup.testAccounts[1].ID)
            };
        insert cpq_TestSetup.testProposals;

        cpq_TestSetup.testFacilities =
            new List<Participating_Facility__c>
            {
                new Participating_Facility__c(
                    Proposal__c = cpq_TestSetup.testProposals[0].ID,
                    Account__c = cpq_TestSetup.testAccounts[2].ID)
            };
        insert cpq_TestSetup.testFacilities;

        cpq_TestSetup.testProductConfigurations.addAll(
            new List<Apttus_Config2__ProductConfiguration__c>
            {
                new Apttus_Config2__ProductConfiguration__c(
                    Apttus_QPConfig__Proposald__c = cpq_TestSetup.testProposals[0].ID,
                    Apttus_Config2__PriceListId__c = testPriceList.ID,
                    Apttus_Config2__Status__c = 'Saved',
                    Apttus_Config2__VersionNumber__c = 1),
                //No Sensor
                new Apttus_Config2__ProductConfiguration__c(
                    Apttus_QPConfig__Proposald__c = cpq_TestSetup.testProposals[1].ID,
                    Apttus_Config2__PriceListId__c = testPriceList.ID,
                    Apttus_Config2__Status__c = 'Saved',
                    Apttus_Config2__VersionNumber__c = 1),
                //No Facility
                new Apttus_Config2__ProductConfiguration__c(
                    Apttus_QPConfig__Proposald__c = cpq_TestSetup.testProposals[2].ID,
                    Apttus_Config2__PriceListId__c = testPriceList.ID,
                    Apttus_Config2__Status__c = 'Saved',
                    Apttus_Config2__VersionNumber__c = 1)
            });
        upsert cpq_TestSetup.testProductConfigurations;

        cpq_TestSetup.testCategory = new Apttus_Config2__ClassificationName__c(Name = 'Root', Apttus_Config2__Type__c = 'Offering', Apttus_Config2__Active__c = true, Apttus_Config2__HierarchyLabel__c = 'Root');
        insert cpq_TestSetup.testCategory;

        cpq_TestSetup.testCategoryHierarchies = new List<Apttus_Config2__ClassificationHierarchy__c> {
            new Apttus_Config2__ClassificationHierarchy__c(
                    Name = 'Monitor',
                    Apttus_Config2__Label__c = 'Monitor',
                    Apttus_Config2__HierarchyId__c = cpq_TestSetup.testCategory.ID,
                    Apttus_Config2__Level__c = 0,
                    Apttus_Config2__Left__c = 2,
                    Apttus_Config2__Right__c = 3,
                    Attribute__c = 'Monitor'),
            new Apttus_Config2__ClassificationHierarchy__c(
                    Name = 'Sensor',
                    Apttus_Config2__Label__c = 'Sensor',
                    Apttus_Config2__HierarchyId__c = cpq_TestSetup.testCategory.ID,
                    Apttus_Config2__Level__c = 0,
                    Apttus_Config2__Left__c = 4,
                    Apttus_Config2__Right__c = 5,
                    Attribute__c = 'Sensor'),
            new Apttus_Config2__ClassificationHierarchy__c(
                    Name = 'Third Party',
                    Apttus_Config2__Label__c = 'Third Party',
                    Apttus_Config2__HierarchyId__c = cpq_TestSetup.testCategory.ID,
                    Apttus_Config2__Level__c = 0,
                    Apttus_Config2__Left__c = 6,
                    Apttus_Config2__Right__c = 7,
                    Attribute__c = 'Third Party'),
            new Apttus_Config2__ClassificationHierarchy__c(
                    Name = 'Nothing',
                    Apttus_Config2__Label__c = 'Nothing',
                    Apttus_Config2__HierarchyId__c = cpq_TestSetup.testCategory.ID,
                    Apttus_Config2__Level__c = 0,
                    Apttus_Config2__Left__c = 8,
                    Apttus_Config2__Right__c = 9)
        };
        insert cpq_TestSetup.testCategoryHierarchies;

        cpq_TestSetup.testClasses = new List<Apttus_Config2__ProductClassification__c> {
            new Apttus_Config2__ProductClassification__c (Apttus_Config2__ClassificationId__c = cpq_TestSetup.testCategoryHierarchies[0].ID, Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[0].ID),
            new Apttus_Config2__ProductClassification__c (Apttus_Config2__ClassificationId__c = cpq_TestSetup.testCategoryHierarchies[1].ID, Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[1].ID),
            new Apttus_Config2__ProductClassification__c (Apttus_Config2__ClassificationId__c = cpq_TestSetup.testCategoryHierarchies[2].ID, Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[2].ID),

            new Apttus_Config2__ProductClassification__c (Apttus_Config2__ClassificationId__c = cpq_TestSetup.testCategoryHierarchies[3].ID, Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[3].ID)
        };
        insert cpq_TestSetup.testClasses;

        cpq_TestSetup.testLines =
            new List<Apttus_Config2__LineItem__c>
            {
                new Apttus_Config2__LineItem__c(
                    Apttus_Config2__IsPrimaryLine__c = true,
                    Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                    Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[0].ID,
                    Apttus_Config2__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[0].ID,
                    Apttus_Config2__ItemSequence__c = 1,
                    Apttus_Config2__LineNumber__c = 1,
                    Apttus_Config2__PrimaryLineNumber__c = 1,
                    Apttus_Config2__ChargeType__c = 'Hardware',
                    Apttus_Config2__PriceType__c = 'One Time',
                    Apttus_Config2__Quantity__c = 5,
                    Apttus_Config2__NetPrice__c = 150,
                    Apttus_Config2__Uom__c = 'Each'),
                new Apttus_Config2__LineItem__c(
                    Apttus_Config2__IsPrimaryLine__c = true,
                    Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                    Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[1].ID,
                    Apttus_Config2__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[1].ID,
                    Apttus_Config2__ItemSequence__c = 1,
                    Apttus_Config2__LineNumber__c = 2,
                    Apttus_Config2__PrimaryLineNumber__c = 2,
                    Apttus_Config2__ChargeType__c = 'Consumable',
                    Apttus_Config2__PriceType__c = 'One Time',
                    Apttus_Config2__Quantity__c = 1,
                    Apttus_Config2__NetPrice__c = 50,
                    Apttus_Config2__Uom__c = 'Each'),
                new Apttus_Config2__LineItem__c(
                    Apttus_Config2__IsPrimaryLine__c = true,
                    Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[0].ID,
                    Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[2].ID,
                    Apttus_Config2__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[2].ID,
                    Apttus_Config2__ItemSequence__c = 1,
                    Apttus_Config2__LineNumber__c = 3,
                    Apttus_Config2__PrimaryLineNumber__c = 3,
                    Apttus_Config2__ChargeType__c = 'Hardware',
                    Apttus_Config2__PriceType__c = 'One Time',
                    Apttus_Config2__Quantity__c = 5,
                    Apttus_Config2__NetPrice__c = 150,
                    Apttus_Config2__Uom__c = 'Each'),

                //No Sensor
                new Apttus_Config2__LineItem__c(
                    Apttus_Config2__IsPrimaryLine__c = true,
                    Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[1].ID,
                    Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[3].ID,
                    Apttus_Config2__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[3].ID,
                    Apttus_Config2__ItemSequence__c = 1,
                    Apttus_Config2__LineNumber__c = 3,
                    Apttus_Config2__PrimaryLineNumber__c = 3,
                    Apttus_Config2__ChargeType__c = 'Consumable',
                    Apttus_Config2__PriceType__c = 'One Time',
                    Apttus_Config2__Quantity__c = 5,
                    Apttus_Config2__NetPrice__c = 150,
                    Apttus_Config2__Uom__c = 'Each'),

                //Sensor but No Facility
                new Apttus_Config2__LineItem__c(
                    Apttus_Config2__IsPrimaryLine__c = true,
                    Apttus_Config2__ConfigurationId__c = cpq_TestSetup.testProductConfigurations[2].ID,
                    Apttus_Config2__ProductId__c = cpq_TestSetup.testProducts[1].ID,
                    Apttus_Config2__PriceListItemId__c = cpq_TestSetup.testPriceListItems.values()[1].ID,
                    Apttus_Config2__ItemSequence__c = 1,
                    Apttus_Config2__LineNumber__c = 2,
                    Apttus_Config2__PrimaryLineNumber__c = 2,
                    Apttus_Config2__ChargeType__c = 'Consumable',
                    Apttus_Config2__PriceType__c = 'One Time',
                    Apttus_Config2__Quantity__c = 1,
                    Apttus_Config2__NetPrice__c = 50,
                    Apttus_Config2__Uom__c = 'Each')
            };
        insert cpq_TestSetup.testLines;

        cpq_TestSetup.testSummaries = new List<Apttus_Config2__SummaryGroup__c>();
        insert cpq_TestSetup.testSummaries;

        Test.setMock(WebServiceMock.class, new Test_CPQ_WebSvcCalloutMock());
    }

    static testMethod void myUnitTest() {
        createTestData();

        Test.startTest();
            PageReference pageRef = Page.CPQ_AwardCreditCalculator;
            Test.setCurrentPage(pageRef);

            CPQ_Controller_AwardCreditCalculator p = new CPQ_Controller_AwardCreditCalculator(new ApexPages.StandardController(cpq_TestSetup.testProductConfigurations[0]));
            p.onLoad();

            p.theRecord.Duration__c = null;
            p.doCalculate();

            p.theRecord.Duration__c = '1 Years';
            p.theRecord.Total_Annual_Sensor_Commitment_Amount__c = null;
            p.doCalculate();

            p.theRecord.Total_Annual_Sensor_Commitment_Amount__c = 100;
            p.theRecord.Duration__c = '4 Years';
            p.doCalculate();

            p.theRecord.Total_Annual_Sensor_Commitment_Amount__c = 100;
            p.theRecord.Duration__c = '3 Years';
            p.doCalculate();

            p.doUsage();

            p = new CPQ_Controller_AwardCreditCalculator(new ApexPages.StandardController(cpq_TestSetup.testProductConfigurations[1]));
            p.onLoad();
            p.doUsage();

            p = new CPQ_Controller_AwardCreditCalculator(new ApexPages.StandardController(cpq_TestSetup.testProductConfigurations[2]));
            p.onLoad();
            p.doUsage();

         Test.stopTest();
    }
}