public with sharing class dmGroupTriggerDispatcher extends dmTriggerDispatcher
{
    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<dmGroup__c> newList,
            Map<Id, dmGroup__c> newMap,
            List<dmGroup__c> oldList,
            Map<Id, dmGroup__c> oldMap,
            integer size
        )
    { 
        dmGroup.main(isExecuting, isInsert, isUpdate, isDelete, isBefore, isAfter, isUndelete, newList, newMap, oldList, oldMap, size);
    }
}