/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AutoLeadConverter {
    global AutoLeadConverter() {

    }
    @InvocableMethod(label='Auto Convert Leads' description='Auto Convert Leads based on the AutoLeadConverter custom settings: status~find account 0/1~find contact 0/1~ownerid/space~create oppty 0/1')
    global static void AutoConvert(List<Id> leads) {

    }
}
