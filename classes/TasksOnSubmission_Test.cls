@isTest
public with sharing class TasksOnSubmission_Test {

    static testmethod void m1(){
        
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test3@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        Asia_Team_Asia_use_only__c = 'CRM Team',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            //Create portalusercontact
            Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = acc.Id,
            RecordtypeID=rtc.id,
            Email = System.now().millisecond() + 'test@test.com',
            type__c='DIS Contact'
            );
            Database.insert(contact1);
            
            Profile profile2 = [Select Id from Profile where name = 'Asia Distributor - CN'];
            User portalAccountOwner2 = new User(
                    ProfileId = profile2.Id,
                    Username = System.now().millisecond() + 'test4@test.com',
                    Alias = 'batman',
                    Email='bruce.wayne@wayneenterprises.com',
                    EmailEncodingKey='UTF-8',
                    Firstname='Bruce',
                    Lastname='Wayne',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    Asia_Team_Asia_use_only__c = 'CRM Team',
                    TimeZoneSidKey='America/Chicago',
                    contactId = contact1.id
                    );
            Database.insert(portalAccountOwner2);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
             Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            //Product_SKU__c;
            insert ps;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            
            cp1.No_Sales_this_month__c = true;
            cp1.Channel_Inventory_Submission_Date__c=System.today();
            update cp1;
        }
    }
    static testmethod void m2(){
        
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test5@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            //Create portalusercontact
            Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = acc.Id,
            RecordtypeID=rtc.id,
            Email = System.now().millisecond() + 'test@test.com',
            type__c='Sales Manager'
            );
            Database.insert(contact1);
            
            Profile profile2 = [Select Id from Profile where name = 'Asia Distributor - CN'];
            User portalAccountOwner2 = new User(
                    ProfileId = profile2.Id,
                    Username = System.now().millisecond() + 'test8@test.com',
                    Alias = 'batman',
                    Email='bruce.wayne@wayneenterprises.com',
                    EmailEncodingKey='UTF-8',
                    Firstname='Bruce',
                    Lastname='Wayne',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    Asia_Team_Asia_use_only__c = 'CRM Team',
                    TimeZoneSidKey='America/Chicago',
                    contactId = contact1.id
                    );
            Database.insert(portalAccountOwner2);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
             Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            //Product_SKU__c;
            insert ps;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp1.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='-23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='-23');
            insert so1;
        
        }
    }
    static testmethod void m3(){
        
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test6@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) {
            Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
            Database.insert(acc);
            
            RecordType rtc=[Select Id from RecordType where DeveloperName='Distributor_Contact' Limit 1];
            //Create portalusercontact
            Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = acc.Id,
            RecordtypeID=rtc.id,
            Email = System.now().millisecond() + 'test@test.com',
            type__c='Sales Rep'
            );
            Database.insert(contact1);
            
            Profile profile2 = [Select Id from Profile where name = 'Asia Distributor - CN'];
            User portalAccountOwner2 = new User(
                    ProfileId = profile2.Id,
                    Username = System.now().millisecond() + 'test7@test.com',
                    Alias = 'batman',
                    Email='bruce.wayne@wayneenterprises.com',
                    EmailEncodingKey='UTF-8',
                    Firstname='Bruce',
                    Lastname='Wayne',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago',
                    Asia_Team_Asia_use_only__c = 'CRM Team',
                    contactId = contact1.id
                    );
            Database.insert(portalAccountOwner2);
            
            Date dt=Date.today().addMonths(-1);
            String CurrentCyclePeriodMonth=String.valueOf(dt.month());
            
            if(dt.month()<10)
                CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
            String CurrentCyclePeriodYear=  String.valueOf(dt.year());
            
            Date PrevDt=Date.today().addMonths(-2);
            String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
            
            if(PrevDt.month()<10)
                PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
            String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
            
             Date AdvDt=Date.today();
            String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
            
            if(AdvDt.month()<10)
                AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
            String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
            
            Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                                Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
            insert PrevCpRef;
            Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                                Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
            insert CurrentCpRef;
            
            Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                                Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
            insert AdvCpRef;
            Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111');
            //Product_SKU__c;
            insert ps;
            Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
            insert cp;
            Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
            insert cp1;
            
            Sales_In__c si=new Sales_In__c(Cycle_Period__c=cp.Id,Distributor_Name__c=acc.Id,Invoice_No__c='1234',Product_SKU__c=ps.Id,Sales_Amount__c=1000,Quantity_Invoiced__c=1000);
            //si.
            Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
            insert si;
            insert so;
            Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp1.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EA',Quantity_Text__c='-23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='-23');
            insert so1;
        
        }
    }


}