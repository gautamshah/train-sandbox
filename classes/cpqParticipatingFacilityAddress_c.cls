public with sharing class cpqParticipatingFacilityAddress_c {
    public static List<Participating_Facility_Address__c> fetch(Id proposalId) {
        return [SELECT Id,
        				Shipto_Number_Template__c,
        				ERP_Record__c,
        				Name_Template__c,
        				Proposal__c
                FROM Participating_Facility_Address__c
                Where Proposal__c = :proposalId];
    }

    public static Map<Id,Participating_Facility_Address__c> fetchMap(Id proposalId) {
        return new Map<Id,Participating_Facility_Address__c>(cpqParticipatingFacilityAddress_c.fetch(proposalId));
    }

	public static String getDisplayName(Participating_Facility_Address__c facility) {
		String facilityName = 'Unknown';
		if (facility != null) {
			facilityName = facility.Name_Template__c + ' (' + facility.Shipto_Number_Template__c + ')';
		}

		return facilityName;
	}
}