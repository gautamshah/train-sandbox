@isTest
private class RecordType_uTest
{
    private static List<Type> types;
    private static List<Schema.SObjectType> soTypes;
    private static List<String> soTypeNames;

    private static void setup ()
    {
        // Add any objects to test in this list. Everything else should pull dynamically from this list
        types = new List<Type>{
            Apttus_Proposal__Proposal__c.class,
            Apttus__APTS_Agreement__c.class
        };

        soTypes = new List<Schema.SObjectType>();
        for (Type t : types) {
            soTypes.add(sObject_u.getSObjectType(t));
        }

        soTypeNames = new List<String>();
        for (Schema.SObjectType sot : soTypes) {
            soTypeNames.add(sot.getDescribe().getName());
        }
    }

    private static List<RecordType> fetchFromDB(string whereStr, object value, integer limitSize)
    {
        string query =
            SOQL_select.buildQuery(
                RecordType.getSObjectType(),
                whereStr,
                null,
                limitSize);

        return (List<RecordType>)Database.query(query);
    }

    @isTest
    static void loadRecordTypes_NoShare ()
    {
        // Start with no queries
        System.assertEquals(0, Limits.getQueries());

        // Initial load should use one query and load methods
        List<RecordType> rts = RecordType_u.RecordTypes;
        System.assertNotEquals(null, rts);
        System.assertEquals(rts.size(), RecordType_u.fetch().size());
        System.assertEquals(1, Limits.getQueries());
        System.debug('RecordTypes.size(): ' + rts.size());

        // Second reference should access the same data set, using no extra queries
        List<RecordType> rts_2 = RecordType_u.RecordTypes;
        System.assertNotEquals(null, rts_2);
        System.assertEquals(rts_2.size(), rts.size());
        System.assertEquals(1, Limits.getQueries());
        System.debug('RecordTypes.size(): ' + rts_2.size());
    }

    @isTest
    static void recordTypes()
    {
        ////
        List<RecordType> rts = RecordType_u.RecordTypes;
        System.assertNotEquals(null, rts);

        ////
        Map<Id, RecordType> rtMap = RecordType_u.fetch();
        System.assertEquals(rts.size(), rtMap.size());

        ////
        Map<sObjectType, Map<Id, RecordType>> rtMapBySO = RecordType_u.sObjectType2Map;

        Integer rtMapBySOSize=0;
        Set<RecordType> rtset = new Set<RecordType>( rts );
        Set<RecordType> rtl = new Set<RecordType>();
        
        for(sObjectType key : rtMapBySO.keySet())
            rtl.addAll(new Set<RecordType>( rtMapBySO.get(key).values() ));

        for(RecordType rt : rtset)
            if (!rtl.contains(rt))
                system.debug('##########' + rt);
                
        System.assertEquals(rts.size(), rtl.size());

        ////
        Map<Map<Schema.sObjectType, String>, RecordType> rtMapBySOandDevName = RecordType_u.sObjectTypeAndDeveloperName2RecordType;
        System.assertEquals(rts.size(), rtMapBySOandDevName.keyset().size());
    }

    @isTest
    static void fetchById()
    {
        setup();

        RecordType rte = RecordType_u.fetch().values()[0];

        RecordType rta = RecordType_u.fetch(rte.Id);

        System.assertEquals(rte.Id, rta.Id);
        System.assertEquals(null, RecordType_u.fetch(sObject_c.dummyId(types.get(0), 1)));
    }

    @isTest
    static void fetchBySObjectFieldAndValues()
    {

        setup();

        Set<Object> values = DATATYPES.castToObject(new Set<String>(soTypeNames));

        List<RecordType> rte = fetchFromDB(' WHERE IsActive = true AND sObjectType IN :value', soTypeNames, null);

        Map<object, Map<Id,RecordType>> rta = RecordType_u.fetch(RecordType.field.SObjectType, values);

        System.assertEquals(soTypes.size(), rta.size());

    }

    @isTest
    static void fetchBySObjectTypeAndDeveloperName()
    {
        RecordType rte = fetchFromDB(' WHERE IsActive = true ', null, null)[0];
        RecordType rta =
            RecordType_u.sObjectTypeAndDeveloperName2RecordType.get(
                new Map<sObjectType, string>
                    {
                        sObject_u.getSObjectType(rte.sObjectType) => rte.DeveloperName
                    });

        System.assertEquals(rte.Id, rta.Id);
    }

    @isTest
    static void fetchBySObjectType ()
    {
        setup();
        Schema.SObjectType sot = soTypes.get(0);

        List<RecordType> rte =
            fetchFromDB(' WHERE sObjectType = :value AND IsActive = true ', sot.getDescribe().getName(), null);

        List<RecordType> rta = RecordType_u.fetch(sot).values();

        System.assertEquals(rte.size(), rta.size());
    }

    // Method is not used anywhere, so removing test
    // @isTest static void fetchBySObjectTypes ()
    // {
    //  setup();
    //      List<RecordType> rte =
    //          fetchFromDB(' WHERE sObjectType IN :value AND IsActive = true ', soTypeNames, null);
    //
    //  set<sObjectType> ts = new set<sObjectType>(soTypes);
    //  system.debug('fetchBysObjectTypes ' + ts.size());
    //  system.debug('fetchBysObjectTypes ' + ts);
    //  system.debug('fetchBysObjectTypes ' + soTypeNames);
    //
    //     Map<Id, RecordType> rta =
    //      RecordType_u.fetch(
    //          RecordType.field.sObjectType,
    //          ts);
    //
    //     System.assertEquals(rte.size(), rta.size());
    // }

    @isTest
    static void fetchBySObjectClassAndDeveloperName ()
    {
        setup();
        Type t = types.get(0);
        Schema.SObjectType sot = sObject_u.getSObjectType(t);

        RecordType rte = fetchFromDB(' WHERE sObjectType = :value AND IsActive = true ', sot.getDescribe().getName(), 1)[0];

        RecordType rta = RecordType_u.fetch(t, rte.DeveloperName);

        System.assertEquals(rte.Id, rta.Id);
    }

    @isTest
    static void fetchBySObjectClass ()
    {
        setup();
        Type t = types.get(0);
        Schema.SObjectType sot = sObject_u.getSObjectType(t);
        List<RecordType> rte = fetchFromDB(' WHERE sObjectType = :value AND IsActive = true ', sot.getDescribe().getName(), null);
        List<RecordType> rta = RecordType_u.fetch(t).values();
        System.assertEquals(rte.size(), rta.size());
    }

    @isTest
    static void fetchBySobjectClasses ()
    {
        setup();
        List<RecordType> rte = fetchFromDB(' WHERE sObjectType IN :value AND IsActive = true ', soTypeNames, null);
        List<RecordType> rta = RecordType_u.fetch(new Set<Type>(types)).values();
        System.assertEquals(rte.size(), rta.size());
    }
}


    // @isTest static void loadRecordTypes ()
    // {
    //
    //  User u = [SELECT Id, Name FROM User WHERE Profile.Name LIKE 'APTTUS%' LIMIT 1]; // Profile.PermissionsViewAllData = false
    //
    //  Test.startTest();
    //
    //      // Start with no queries
    //      System.assertEquals(0, Limits.getQueries());
    //      System.debug('Limits.getLimitQueries(): ' + Limits.getLimitQueries());
    //      System.debug('Limits.getLimitHeapSize(): ' + Limits.getLimitHeapSize());
    //      System.debug('Limits.getLimitCpuTime(): ' + Limits.getLimitCpuTime());
    //
    //      // Initial load should use one query and load subsequent methods
    //      List<RecordType> rts = RecordType_g.RecordTypes;
    //      System.assertNotEquals(null, rts);
    //      System.assertEquals(rts.size(), RecordType_g.RecordTypeMap.keySet().size());
    //      System.assertEquals(1, Limits.getQueries());
    //      System.debug('RecordTypes.size(): ' + rts.size());
    //      logLimits();
    //
    //      // Second reference should access the same data set, using no extra queries
    //      List<RecordType> rts_2 = RecordType_g.RecordTypes;
    //      System.assertNotEquals(null, rts_2);
    //      System.assertEquals(rts_2.size(), rts.size());
    //      System.assertEquals(1, Limits.getQueries());
    //      System.debug('RecordTypes.size(): ' + rts_2.size());
    //      logLimits();
    //
    //      // Loading records with sharing should decrease the data set, reload subsequent methods, and use one query
    //      System.runAs(u) {
    //          System.debug('Running as user: ' + u.Name);
    //          RecordType_g.loadRecordTypesWithSharing();
    //          List<RecordType> rts_sharing = RecordType_g.RecordTypes;
    //          System.assertNotEquals(null, rts_sharing);
    //          System.assertNotEquals(rts_sharing.size(), rts.size());
    //          System.assertEquals(rts_sharing.size(), RecordType_g.RecordTypeMap.keyset().size());
    //          System.assertEquals(2, Limits.getQueries());
    //          System.debug('RecordTypes.size(): ' + rts_sharing.size());
    //          logLimits();
    //      }
    //
    //      // Reloading all records should restore the data set, reload subsequent methods, and use one query
    //      // NOTE: This could be optimized to separate filtered record type lists from the absolute record type list,
    //      // so we don't need to query again on reload of all record types.
    //      RecordType_g.loadRecordTypes();
    //      List<RecordType> rts_reload = RecordType_g.RecordTypes;
    //      System.assertNotEquals(null, rts_reload);
    //      System.assertNotEquals(rts_reload.size(), rts.size());
    //      System.assertEquals(rts_reload.size(), RecordType_g.RecordTypeMap.keyset().size());
    //      System.assertEquals(3, Limits.getQueries());
    //      System.debug('RecordTypes.size(): ' + rts_reload.size());
    //      logLimits();
    //
    //      // Loading a record type subset should decrease the data set, reload subsequent methods, and use no queries
    //      RecordType_g.loadRecordTypes(new List<RecordType>{rts.get(0),rts.get(1)});
    //      List<RecordType> rts_filtered = RecordType_g.RecordTypes;
    //      System.assertNotEquals(null, rts_filtered);
    //      System.assertEquals(2, rts_filtered.size());
    //      System.assertEquals(rts_filtered.size(), RecordType_g.RecordTypeMap.keyset().size());
    //      System.assertEquals(3, Limits.getQueries());
    //      System.debug('RecordTypes.size(): ' + rts_filtered.size());
    //      logLimits();
    //
    //  Test.stopTest();
    //
    // }

//// We don't need this test at the moment, we are always pulling all of them
/*
        // Reload with a subset should decrease data set, reload methods, and use no queries
        RecordType_g.loadRecordTypes(new List<RecordType>{rts.get(0),rts.get(1)});
        List<RecordType> rts_filtered = RecordType_u.RecordTypes;
        System.assertNotEquals(null, rts_filtered);
        System.assertEquals(2, rts_filtered.size());
        System.assertEquals(rts_filtered.size(), RecordType_u.fetchMap().size());
        System.assertEquals(1, Limits.getQueries());
        System.debug('RecordTypes.size(): ' + rts_filtered.size());
        logLimits();

        // Reload all records should restore data set, reload methods, and use no queries
        // TODO: Optimize by separating filtered query data set from the absolute data set,
        // so we don't requery on restore of absolute data set.
        RecordType_g.loadRecordTypes();
        List<RecordType> rts_reload = RecordType_u.RecordTypes;
        System.assertNotEquals(null, rts_reload);
        System.assertEquals(rts_reload.size(), rts.size());
        System.assertEquals(rts_reload.size(), RecordType_u.fetchMap().size());
        System.assertEquals(1, Limits.getQueries());
        System.debug('RecordTypes.size(): ' + rts_reload.size());
        logLimits();
*/