/****************************************************************************************
* Name    : IncoPPDReports
* Author  : Gautam Shah
* Date    : June 1, 2016
* Purpose : Makes the call to compile a report and emails it to customer and distributor contacts for the Inco PPD program
* 
* Dependancies: 
*  IncoReportWrapper.vfp            
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
*
*****************************************************************************************/ 
public class IncoPPDReports 
{
    private List<Incontinence_Submissions__c> subs {get;set;}
    private Set<Id> custAcctIDs {get;set;}
    private Set<Id> distAcctIDs {get;set;}
    private OrgWideEmailAddress orgEmail {get;set;}
    private EmailTemplate template {get;set;}
    
    public IncoPPDReports()
    {
        System.debug('userSession: ' + UserInfo.getSessionId());
        //query for the accounts that have submissions in the past week
        this.subs = new List<Incontinence_Submissions__c>([Select Account__c, Account__r.Inco_Distributor__c From Incontinence_Submissions__c Where Submitted__c = true And Submitted_Date__c = LAST_N_DAYS:7 Order By For_the_week_of__c ASC]);
        this.orgEmail = [SELECT Id FROM OrgWideEmailAddress Where Address = 'ppdportal@medtronic.com' Limit 1];
        this.template = [SELECT Id FROM EmailTemplate Where DeveloperName = 'Inco_PPD_Weekly_Report' Limit 1];
        this.custAcctIDs = new Set<Id>();
        this.distAcctIDs = new Set<Id>();
        for(Incontinence_Submissions__c sub : subs)
        {
            if(!String.isBlank(sub.Account__c))
            {
                custAcctIDs.add(sub.Account__c);
            }
            if(!String.isBlank(sub.Account__r.Inco_Distributor__c))
            {
                distAcctIDs.add(sub.Account__r.Inco_Distributor__c);
            }
        }
    }
    
    public void sendCustomerReports()
    {   
        //query for the Inco contacts for those accounts
        System.debug('# of customer Accts: ' + custAcctIDs.size());
        Map<Id, Contact> mapAcctEmail = new Map<Id, Contact>();
        for(Contact c : [Select Id, AccountId, IncoPortalEmail__c From Contact Where Inco_Customer_Contact__c = true And IncoPortalEmail__c != null And AccountId In :custAcctIDs])
        {
            mapAcctEmail.put(c.AccountId, c);
        }
        // loop through each account to generate and send the report
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        String daysAgo = '7';
        for(Id i : custAcctIDs)
        {
            if(mapAcctEmail.containsKey(i))
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(mapAcctEmail.get(i).Id);
                email.setOrgWideEmailAddressId(orgEmail.Id);
                email.setTemplateId(template.Id);
                List<String> toAddresses = new List<String>{mapAcctEmail.get(i).IncoPortalEmail__c};
                email.setToAddresses(toAddresses);
                
                //create attachment
                PageReference report;
                report = Page.IncoReportWrapper;
                report.getParameters().put('acctID', i);
                report.getParameters().put('daysAgo', daysAgo);
                report.getParameters().put('recipient', 'customer');
                report.setRedirect(true);
                if(!Test.isRunningTest())
                {
                    Blob content = report.getContent();
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName('IncoReport-' + Date.Today().month() + '-' + Date.Today().day() + '-' + Date.Today().year() + '.csv');
                    efa.setBody(content);
                    efa.setContentType('text/csv');
                    email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                }
                outboundEmails.add(email);
            }
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(outboundEmails, false);
        for(Messaging.SendEmailResult r : results)
        {
            Messaging.SendEmailError[] errors = r.getErrors();
            for(Messaging.SendEmailError err : errors)
            {
                System.debug('Error sending to ContactID: ' + err.getTargetObjectId() + ' ; message: ' + err.getMessage());
            }
        }
    }
    
    public void sendDistributorReports()
    {   
        //query for the Inco contacts for those accounts
        System.debug('# of distributor Accts: ' + distAcctIDs.size());
        Map<Id, Contact> mapAcctEmail = new Map<Id, Contact>();
        for(Contact c : [Select Id, AccountId, IncoPortalEmail__c From Contact Where Inco_Distributor_Contact__c = true And IncoPortalEmail__c != null And AccountId In :distAcctIDs])
        {
            mapAcctEmail.put(c.AccountId, c);
        }
        // loop through each account to generate and send the report
        System.debug('# of distributor contacts: ' + mapAcctEmail.size());
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        String daysAgo = '7';
        for(Id i : distAcctIDs)
        {
            if(mapAcctEmail.containsKey(i))
            {
            	Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                System.debug('Distributor ContactID: ' + mapAcctEmail.get(i).Id);
                email.setTargetObjectId(mapAcctEmail.get(i).Id);
                email.setOrgWideEmailAddressId(orgEmail.Id);
                email.setTemplateId(template.Id);
                List<String> toAddresses = new List<String>{mapAcctEmail.get(i).IncoPortalEmail__c};
                System.debug('distributor toAddresses: ' + toAddresses);
                email.setToAddresses(toAddresses);
                
                //create attachment
                PageReference report;
                report = Page.IncoReportWrapper;
                report.getParameters().put('acctID', i);
                report.getParameters().put('daysAgo', daysAgo);
                report.getParameters().put('recipient', 'distributor');
                report.setRedirect(true);
                if(!Test.isRunningTest())
                {
                    Blob content = report.getContent();
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName('IncoReport-' + Date.Today().month() + '-' + Date.Today().day() + '-' + Date.Today().year() + '.csv');
                    efa.setBody(content);
                    efa.setContentType('text/csv');
                    email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                }
                outboundEmails.add(email);
            }
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(outboundEmails, false);
        for(Messaging.SendEmailResult r : results)
        {
            Messaging.SendEmailError[] errors = r.getErrors();
            for(Messaging.SendEmailError err : errors)
            {
                System.debug('Error sending to ContactID: ' + err.getTargetObjectId() + ' ; message: ' + err.getMessage());
            }
        }
        
    }
}