/****************************************************************************************
Name    : Class: cpqCycleTime_u
Author  : Bryan Fry
Date    : 10/15/2015
Purpose : Handle trigger processing for finding and saving Cycle Time-related timestamps
     
========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE
----------  ----------------  ------------------------------
10/15/2015  Bryan Fry         Created
11/12/2015  Paul Berglund     Replaced system.now() with NOW variable to keep
                              times consistent.  Ran into a ListException where
                              "identical" CPQ_Cycle_Time__c objects where in a
                              Set being used in an upsert.
04/13/2016  Bryan Fry         Only query by opportunity Ids in
                              captureProposalCycleTimes when opportunity Ids are
                              present.
06/14/2016  Paul Berglund     Added abstraction so we can deploy the triggers that use this
                              code ahead of time
07/15/2016  Paul Berglund     Problems during the deployment to production - commented out
                              calls while troubleshooting
08/11/2016  Bryan Fry         Changed proposal capture method to limit calls to inserts
                              and updates to fix issue with deleting proposals.
10/03/2016  Paul Berglund     Fixed issue where 2 Tasks caused duplicate object in upsert
                              Moved Task related code to cpqCycleTime_Task
11/01/2016  Paul Berglund   Issue with the "duplicate id" in Agreements
*****************************************************************************************/
public abstract class cpqCycleTime_u
{
    public virtual void capture(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<sObject> newList,
            Map<Id, sObject> newMap,
            List<sObject> oldList,
            Map<Id, sObject> oldMap,
            integer size
        )
    {
        system.debug(string.valueOf(this) + '.capture.queries: ' + Limits.getQueries());
        
        if (isInsert || isUpdate) {
            if (isAfter) {
                capture(newList, oldMap, isInsert, isUpdate);
            }
        }
    }
    
    protected abstract void capture(
        List<sObject> newList,
        Map<Id,sObject> oldMap,
        Boolean isInsert,
        Boolean isUpdate);
	
    // Keep a static list of User Queues to match and prevent re-querying in the same transaction
    public static Map<String,Id> UserQueueName2Id;

    static
    {
        UserQueueName2Id = new Map<String,Id>();
        List<User> users = [Select Id, Name From User Where Name like '%Queue%'];
        for (User u: users)
            UserQueueName2Id.put(u.Name, u.Id);
    }
    
    // These are object specific, so we can move them into their respective
    // classes - after we move the test methods to their respective classes
    //
    // Proposal Stage values
    @testVisible
    protected final String IN_REVIEW = 'In Review';
    @testVisible
    protected final String PENDING_APPROVAL = 'Pending Approval';
    @testVisible
    protected final String APPROVED = 'Approved';
    @testVisible
    protected final String PRESENTED = 'Presented';
    @testVisible
    protected final String ACCEPTED = 'Accepted';

    // Agreement Stage values
    @testVisible
    protected final String READY_FOR_SIGNATURES = 'Ready for Signatures';
    @testVisible
    protected final String INTERNAL_SIGNATURES = 'Internal Signatures';
    @testVisible
    protected final String OTHER_PARTY_SIGNATURES = 'Other Party Signatures';
    @testVisible
    protected final String FULLY_SIGNED = 'Fully Signed';
    @testVisible
    protected final String ACTIVATED = 'Activated';

    // Task Stage values
    @testVisible
    protected final String IN_PROGRESS = 'In Progress';
    @testVisible
    protected final String COMPLETED = 'Completed';

    // Task Subjects
    @testVisible
    protected final String NON_STANDARD_AGREEMENT_SUBJECT = 'Request for a Non Standard Agreement';
    @testVisible
    protected final String SENT_FOR_REVIEW_SUBJECT = 'Sent for Review';

    // Task User Queue Owners
    @testVisible
    protected final String CONTRACT_DEV_QUEUE = 'CommOps - Contract Dev Queue';
    @testVisible
    protected final String EQUIP_ADMIN_QUEUE = 'CommOps - Equip Admin Queue';
    @testVisible
    protected final String CUSTOMER_SERVICE_QUEUE = 'NASSC - Customer Service Queue';
    @testVisible
    protected final String PRICING_QUEUE = 'NASSC - Individual Pricing Queue';
    @testVisible
    protected final String END_CUSTOMER_REBATES_QUEUE = 'NASSC - End Customer Rebates Queue';
}