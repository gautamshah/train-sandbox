/**
 * PRSOpportunityUpdateControllerのテストクラス
 */
@isTest
private class PRSOpportunityUpdateControllerTest{
    
    /**
     * init()とclear()のテスト
     */
    static testMethod void initAndClear() {
        PRSOpportunityUpdateController con = new PRSOpportunityUpdateController();
       
        con.init();
                
        System.assertNotEquals(null,con.criteria);
        System.assertEquals(false,con.searched);

        con.clear(); 
        
        PRSOpportunityUpdateController.ItemDTO itDto = new PRSOpportunityUpdateController.ItemDTO();
        PRSOpportunityUpdateController.OppDto dot = new PRSOpportunityUpdateController.OppDto();
        con.oppList.add(dot);

        con.clear();

        System.assertNotEquals(null,con.criteria);
        System.assertEquals(false,con.searched);
        System.assertEquals(0,con.oppList.size());
        
        
    }

    static testMethod void search1() {
        //Todo Test
        PRSOpportunityUpdateController con = new PRSOpportunityUpdateController();
        con.init();
        con.search();

        //TODO Assert未実装
        
        con.criteria.name__c = 'test';
        con.criteria.startdate__c = Date.today();
        con.criteria.enddate__c = Date.today();
        con.criteria.opportunitiesOwner__c = 'tem';
        con.criteria.stage__c ='Evaluate';
        con.criteria.onlyClosed__c = true;
        con.criteria.productType__c ='dsp';
        con.criteria.includeBase__c = true;
        con.search();
        con.getFiscalYear();
        con.cancel();
        clearCriteria(con);
        con.cancel();
        
        // ・・・・・・
    }

    static testMethod void search2() {
        //Todo Test
        PRSOpportunityUpdateController con = new PRSOpportunityUpdateController();
        con.init();
        con.search();

        //TODO Assert未実装
        con.searched = true;
        con.criteria.opportunitiesOwner__c = 'own';
        con.criteria.productType__c ='cap';
        con.criteria.onlyClosed__c = true;
        con.criteria.includeClosed__c = true;
        con.criteria.includeBase__c = false;
        con.criteria.View_Type__c = 'summary';
        con.criteria.sort__c = 'accountid';
        con.criteria.sortOrder__c = 'ASC';
        con.search();
        con.getFiscalYear();
        con.cancel();
        clearCriteria(con);
        con.cancel();
        
        // ・・・・・・
    }

    static testMethod void search3() {
        //Todo Test
        PRSOpportunityUpdateController con = new PRSOpportunityUpdateController();
        con.init();
        con.search();

        //TODO Assert未実装
        con.criteria.productType__c ='cap';
        con.criteria.includeBase__c = false;
        con.criteria.opportunitiesOwner__c = 'all';
        con.criteria.startdate__c = Date.newInstance(2012, 10, 1);
        con.criteria.enddate__c = Date.newInstance(2013, 10, 1);
        con.search();
        // ・・・・・・
    }

    static testMethod void getOpportunityLineItem() {
        //Todo Test
        PRSOpportunityUpdateController conl = new PRSOpportunityUpdateController();

        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky0924');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky0924:Opportunity', 
                                                            System.Label.JP_Opportunity_StageName_Develop, 
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable, 
                                                            2013, 7, 20, null);
        // ・・・・・・
    }

    static testMethod void bulkupdate() {
        //Todo Test
        PRSOpportunityUpdateController conl = new PRSOpportunityUpdateController();
        conl.init();
        conl.search();
        //TODO Assert未実装
        Test.startTest();
        //Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account or Opportunity
        RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
        RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
        //Create Test Account
        Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky0924');
        //Create Test Opportunity
        Opportunity opp = JP_TestUtil.createTestOpportunity(oppRec.Id, acc.Id, 'test_TerraSky0924:Opportunity', 
                                                            System.Label.JP_Opportunity_StageName_Develop, 
                                                            System.Label.JP_Opportunity_CapitalDisposable_Disposable, 
                                                            2013, 7, 20, null);
        // ・・・・・・
        PRSOpportunityUpdateController.ItemDTO itDto = new PRSOpportunityUpdateController.ItemDTO();           
        PRSOpportunityUpdateController.OppDto dot = new PRSOpportunityUpdateController.OppDto();        
        dot.checked = true;
        dot.opp = opp;
        conl.oppList.add(dot);
        conl.bulkupdate();        
        conl.next();
        conl.previous();
    }
    
    
    private static void clearCriteria(PRSOpportunityUpdateController con){
        con.criteria.name__c = null;
        con.criteria.startdate__c = null;
        con.criteria.enddate__c = null;
    }
    
}