@isTest
private class cpqParticipatingFacility_LineItem_RT_cT {

    static Map<Id, RecordType> rts;

    static void setup ()
    {
        if (rts == NULL)
        {
            rts = RecordType_u.fetch(cpqParticipatingFacility_Lineitem__c.class);
        }
    }

    @isTest
    static void convert()
    {
        System.assertEquals('Locked',
            cpqParticipatingFacility_LineItem_RT_c.convert(cpqParticipatingFacility_LineItem_RT_c.RecordTypes.Locked));
        System.assertEquals('Unlocked',
            cpqParticipatingFacility_LineItem_RT_c.convert(cpqParticipatingFacility_LineItem_RT_c.RecordTypes.Unlocked));
    }

    @isTest
    static void RecordTypeId()
    {
        setup();
        RecordType rtUnlocked = rts.values().get(0).DeveloperName == 'Unlocked' ? rts.values().get(0) : rts.values().get(1);
        RecordType rtLocked = rts.values().get(0).DeveloperName == 'Locked' ? rts.values().get(0) : rts.values().get(1);
        System.assertEquals(rtUnlocked.Id,
            cpqParticipatingFacility_LineItem_RT_c.RecordTypeId(
                cpqParticipatingFacility_LineItem_RT_c.RecordTypes.Unlocked));
        System.assertEquals(rtLocked.Id,
            cpqParticipatingFacility_LineItem_RT_c.RecordTypeId(
                cpqParticipatingFacility_LineItem_RT_c.RecordTypes.Locked));
    }

}