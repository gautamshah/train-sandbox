/**
SF WS: Partner passes Contract ID and other deal related info. 
Partner_Root_Contract__c and Related_Partner_Contract__c records are created.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-11/21      Yuli Fintescu		Created
===============================================================================
*/
global class CPQ_PartnerContractService {
	global class PartnerContractItem {
		webService String AgreementID;
		webService String RootContractID;
		webService String PricingID;
		webService String Direction;
		webService Date ActualEffectiveDate;
		webService Date ActualExpirationDate;
	}
	
	/*
	The input items need to be put into 2 SObjects Partner_Root_Contract__c and Related_Partner_Contract__c.
	and Related_Partner_Contract__c records need to link to Partner_Root_Contract__c record
	*/
	webService static String LoadPartnerContractItems(List<PartnerContractItem> items) {
		List<CPQ_Error_Log__c> errors = new List<CPQ_Error_Log__c>();
		
/*=========================================================
	Extract data from input items to build linkage structures
===========================================================*/
		//unique root contract ids. the values of the map is direction. a root can have both O and I directions, this makes 2 entries in the input items. 
		Map<String, Set<String>> RootNos = new Map<String, Set<String>>();
		//map of agreement to root. for update agreement.Partner_Root_Contract__c field
		Map<String, Set<String>> AgmtToRootNos = new Map<String, Set<String>>();
		for (PartnerContractItem item : Items) {
			if (String.isEmpty(item.RootContractID))
				continue;
			
			if (!RootNos.containsKey(item.RootContractID))
				RootNos.put(item.RootContractID, new Set<String>{item.Direction});
			else {
				Set<String> dirs = RootNos.get(item.RootContractID);
				dirs.add(item.Direction);
				RootNos.put(item.RootContractID, dirs);
			}
			
			if (String.isEmpty(item.AgreementID))
				continue;
			
			if (!AgmtToRootNos.containsKey(item.AgreementID))
				AgmtToRootNos.put(item.AgreementID, new Set<String>{item.RootContractID});
			else {
				Set<String> dirs = AgmtToRootNos.get(item.AgreementID);
				dirs.add(item.RootContractID);
				AgmtToRootNos.put(item.AgreementID, dirs);
			}
		}
		
		Map<String, Apttus__APTS_Agreement__c> existingAgreements = new Map<String, Apttus__APTS_Agreement__c>();
		for (Apttus__APTS_Agreement__c a : [Select ID, Apttus__Account__c, AgreementID__c, Partner_Root_Contract__c From Apttus__APTS_Agreement__c Where AgreementID__c in: AgmtToRootNos.keySet()]) {
			existingAgreements.put(a.AgreementID__c, a);
		}
		
		Map<String, Partner_Root_Contract__c> existingRoots = new Map<String, Partner_Root_Contract__c>();
		for (Partner_Root_Contract__c r : [Select ID, Root_Contract_Number__c, Direction__c, Expiration_Date__c, Effective_Date__c, Root_Contract_Name__c, Account__c From Partner_Root_Contract__c Where Root_Contract_Number__c in: RootNos.keySet()]) {
			existingRoots.put(r.Root_Contract_Number__c, r);
		}
		
		Map<String, Partner_Root_Contract__c> toRootUpsert = new Map<String, Partner_Root_Contract__c>();
		Map<String, Related_Partner_Contract__c> toContractUpsert = new Map<String, Related_Partner_Contract__c>();
		List<Apttus__APTS_Agreement__c> toAgmtUpdate = new List<Apttus__APTS_Agreement__c>();
		
/*=========================================================
	Upsert root contracts
===========================================================*/
		for (PartnerContractItem item : Items) {
			if (String.isEmpty(item.RootContractID)) 
				continue;
			
			if (toRootUpsert.containsKey(item.RootContractID))
				continue;
			
			//direction string
			String dir = '';
			for (String d : RootNos.get(item.RootContractID))
				dir = dir + d + '; ';
			dir = dir.endsWith('; ') ? dir.substring(0, dir.length() - 2) : dir;
			
			Partner_Root_Contract__c root;
			if (!existingRoots.containsKey(item.RootContractID)) {
				root = new Partner_Root_Contract__c();
				root.Root_Contract_Number__c = item.RootContractID;
				root.Root_Contract_Name__c = item.RootContractID;
				root.Name = item.RootContractID;
			} else {
				root = existingRoots.get(item.RootContractID);
			}
			root.Expiration_Date__c = item.ActualExpirationDate;
			root.Effective_Date__c = item.ActualEffectiveDate;
			
			//root.Direction__c = dir;
			
			//link root contract to agreement.account
			Apttus__APTS_Agreement__c agreement = existingAgreements.get(item.AgreementID);
			if (agreement != null) {
				root.Account__c = agreement.Apttus__Account__c;
			}
			
			toRootUpsert.put(item.RootContractID, root);
		}
		
		if (toRootUpsert.size() > 0) {
			Database.UpsertResult[] results = Database.upsert(toRootUpsert.values(), Schema.Partner_Root_Contract__c.Root_Contract_Number__c, false);
			for (Integer i = 0; i < results.size(); i++) {
				Partner_Root_Contract__c r = toRootUpsert.values()[i];
				Database.UpsertResult res = results[i];
				
	            if (!res.isSuccess())
	            	errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PartnerContractService', 'Error', 'Upsert Root Contract: ' + 'Root Contract # ' + r.Root_Contract_Number__c + ' - ' + res.getErrors()[0].getMessage(), null, null));
	        }
		}
		
/*=========================================================
	Update agreement Partner_Root_Contract__c field
===========================================================*/
		for (String aNo : AgmtToRootNos.keySet()) {
			Apttus__APTS_Agreement__c agreement = existingAgreements.get(aNo);
			if (agreement == null)
				continue;
			
			Set<String> rNos = AgmtToRootNos.get(aNo);
			for (String rNo : rNos) {
				Partner_Root_Contract__c root = toRootUpsert.get(rNo);
				
				if (root != null && root.ID != null) {
					agreement.Partner_Root_Contract__c = root.ID;
					
					if (root.Effective_Date__c != null)
						agreement.Apttus__Contract_Start_Date__c = root.Effective_Date__c;
					
					if (root.Expiration_Date__c != null) {
						agreement.Apttus__Contract_End_Date__c = root.Expiration_Date__c;
						agreement.Apttus__Perpetual__c = false;
					}
					
					toAgmtUpdate.add(agreement);
				}
			}
		}
		
		if (toAgmtUpdate.size() > 0) {
			Database.SaveResult[] results = Database.update(toAgmtUpdate, false);
			for (Integer i = 0; i < results.size(); i++) {
				Apttus__APTS_Agreement__c r = toAgmtUpdate[i];
				Database.SaveResult res = results[i];
				
	            if (!res.isSuccess())
	            	errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PartnerContractService', 'Error', 'Update Agreement: ' + 'AgreementID ' + r.AgreementID__c + ' - ' + res.getErrors()[0].getMessage(), null, null));
	        }
		}
		
/*=========================================================
	Upsert related contracts
===========================================================*/
		for (PartnerContractItem item : Items) {
			if (String.isEmpty(item.PricingID))
				continue;
			
			if (toContractUpsert.containsKey(item.PricingID))
				continue;
			
			if (!toContractUpsert.containsKey(item.PricingID))
				toContractUpsert.put(item.PricingID, new Related_Partner_Contract__c(Name = item.PricingID, Root_Contract__r = new Partner_Root_Contract__c(Root_Contract_Number__c = item.RootContractID)));
		}
		
		//upsert related contracts
		if (toContractUpsert.size() > 0) {
			Database.UpsertResult[] results = Database.upsert(toContractUpsert.values(), Schema.Related_Partner_Contract__c.Name, false);
			for (Integer i = 0; i < results.size(); i++) {
				Related_Partner_Contract__c r = toContractUpsert.values()[i];
				Database.UpsertResult res = results[i];
				
	            if (!res.isSuccess())
	            	errors.add(CPQ_Utilities.CreateErrorLogEntry('CPQ_PartnerContractService', 'Error', 'Upsert Related Contract: ' + 'Contract # ' + r.Name + ' - ' + res.getErrors()[0].getMessage(), null, null));
	        }
		}
		
/*=========================================================
	error handling
===========================================================*/
		if (errors.size() > 0 || Test.isRunningTest()) {
			if (Test.isRunningTest())
				errors.add(new CPQ_Error_Log__c(Error_Message__c = 'cover more lines'));
			insert errors;
			
			String serror = 'Error in Loading Partner Contract Items: ';
			for (CPQ_Error_Log__c s : errors) {
				serror = serror + '\n' + s.Error_Message__c;
			}
			
			return serror;
		}
		
		return 'Successful';
	}
}