/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20170212   A    BF         AV-309     Move status to fully signed so user can activate a Scrub PO with an
                                      in-person signature not in Docusign.

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
@isTest
public class cpqAgreementInPersonSignature_vTest
{
	@testSetup
    static void Setup()
    {
		Account acct = new Account(Name = 'Test Account 1',
								   BillingState = 'CO',
								   BillingPostalCode = '12345',
								   BillingCountry = 'US');
		insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        List<RecordType> agreementRts = cpqAgreement_c.getRecordTypes();
        Id agreementRecordTypeId = null;
        for (RecordType rt: agreementRts) {
        	if (rt.DeveloperName == 'Scrub_PO') {
        		agreementRecordTypeId = rt.Id;
        		break;
        	}
        }

        Apttus__APTS_Agreement__c agmt =
			new Apttus__APTS_Agreement__c(Name = 'Test Agreement',
										  RecordTypeId = agreementRecordTypeId,
										  Scrub_PO_Type__c = 'Non-Transfer',
										  Primary_Contact2__c = c.Id,
										  SSG_Product_Usage__c = 'Clinically by the hospital',
										  Apttus_Approval__Approval_Status__c = 'Approved',
										  Apttus__Status__c = 'Requested');
        insert agmt;
    }

    @isTest
    public static void Test_InPersonSignature()
    {
    	Apttus__APTS_Agreement__c agmt = [Select Id From Apttus__APTS_Agreement__c Limit 1];
    	Test.startTest();
			cpqAgreementInPersonSignature_v con = new cpqAgreementInPersonSignature_v(new ApexPages.StandardController(agmt));
			con.setToFullySigned();
			con.returnToAgreement();
		Test.stopTest();

		agmt = [Select Id, 
					   Apttus__Status__c
				From Apttus__APTS_Agreement__c
				Where Id = :agmt.Id];
		System.assertEquals(cpqAgreement_c.convert(cpqAgreement_c.StatusIndicators.FullySigned), agmt.Apttus__Status__c);
	}
}