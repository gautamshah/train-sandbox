@isTest
private class JP_IntimacyBusinessTest{
     /****************************************************************************************
     * Name    : JP_IntimacyBusinessTest 
     * Author  : Hiroko Kambayashi
     * Date    : 10/17/2012 
     * Purpose : Test class for JP_IntimacyBusiness
     *           
     * Dependencies: RecordType Object
     *             , Account Object
     *             , Opportunity Object
     *             , Contact Object
     *             , JP_Intimacy__c Object
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 
     *
     *****************************************************************************************/
     
	/*
     * Field JP_Intimacy__c on Opportunity record Update test after JP_Intimacy__c record is inserted and updated.
     */
	static testMethod void TestInputIntimacy01(){
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1017');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1017');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1017', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		Test.startTest();
		//Opportunity info before run Trigger 
		Opportunity beforeOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertNotEquals(beforeOpp.JP_Intimacy__c, insAfterOpp.JP_Intimacy__c);
		System.assertEquals(insAfterOpp.JP_Intimacy__c, intm.JP_Intimacy__c);
		Test.stopTest();
	}
	
	static testMethod void TestUpdateIntimacy01(){
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1017');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1017');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1017', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		Test.startTest();
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		//Run Trigger after update Intimacy
		JP_Intimacy__c insIntm = [SELECT Id, JP_Intimacy__c FROM JP_Intimacy__c WHERE Id = : intm.Id];
		insIntm.JP_Intimacy__c = System.Label.JP_Intimacy_Intimacy_Afavor;
		update insIntm;
		//Opportunity info after run Trigger.isUpdate
		Opportunity updAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertNotEquals(insAfterOpp.JP_Intimacy__c,updAfterOpp.JP_Intimacy__c);
		System.assertEquals(updAfterOpp.JP_Intimacy__c, insIntm.JP_Intimacy__c);
		Test.stopTest();
	}
	
	static testMethod void TestUpdateIntimacy02(){
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1017');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1017');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1017', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		Test.startTest();
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		//Run Trigger after update Intimacy(Field JP_Intimacy__c value on JP_Intimacy__c record: 'A favor' → '')
		JP_Intimacy__c insIntm = [SELECT Id, JP_Intimacy__c FROM JP_Intimacy__c WHERE Id = : intm.Id];
		insIntm.JP_Intimacy__c = '';
		update insIntm;
		//Opportunity info after run Trigger.isUpdate
		Opportunity updAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertNotEquals(insAfterOpp.JP_Intimacy__c,updAfterOpp.JP_Intimacy__c);
		System.assertEquals(updAfterOpp.JP_Intimacy__c, System.Label.JP_Opportunity_Intimacy_Nothing);
		Test.stopTest();
	}
/*
	static testMethod void Test_InputIntimacyToOpportunity_01(){
		Test.startTest();
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1017');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1017');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1017', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
		//Opportunity info before run Trigger 
		Opportunity beforeOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertNotEquals(beforeOpp.JP_Intimacy__c, insAfterOpp.JP_Intimacy__c);
		System.assertEquals(insAfterOpp.JP_Intimacy__c, intm.JP_Intimacy__c);
		
		//Run Trigger after update Intimacy
		JP_Intimacy__c insIntm = [SELECT Id, JP_Intimacy__c FROM JP_Intimacy__c WHERE Id = : intm.Id];
		insIntm.JP_Intimacy__c = System.Label.JP_Intimacy_Intimacy_Afavor;
		update insIntm;
		//Opportunity info after run Trigger.isUpdate
		Opportunity updAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertEquals(updAfterOpp.JP_Intimacy__c, insIntm.JP_Intimacy__c);
		
		//Run Trigger after update Intimacy(Field JP_Intimacy__c value on JP_Intimacy__c record: 'A favor' → '')
		JP_Intimacy__c updIntm = [SELECT Id, JP_Intimacy__c FROM JP_Intimacy__c WHERE Id = : intm.Id];
		updIntm.JP_Intimacy__c = '';
		update updIntm;
		//Opportunity info after run Trigger.isUpdate (Field JP_Intimacy__c value on Opportunity record: 'A favor' → 'None')
		Opportunity updAfterOpp02 = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertEquals(updAfterOpp02.JP_Intimacy__c, System.Label.JP_Opportunity_Intimacy_Nothing);
		Test.stopTest();
	}
*/
	/*
     * Field JP_Intimacy__c on Opportunity record Update test after JP_Intimacy__c record is inserted and deleted.
     */
	static testMethod void Test_InputIntimacyToOpportunity_02(){
		Test.startTest();
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1017');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1017');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1017', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		//Run Trigger after delete Intimacy
		JP_Intimacy__c del = [SELECT Id FROM JP_Intimacy__c WHERE Id = : intm.Id];
		delete del;
		//Opportunity info after run Trigger.isDelete
		Opportunity delAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertNotEquals(insAfterOpp.JP_Intimacy__c, delAfterOpp.JP_Intimacy__c);
		System.assertEquals(delAfterOpp.JP_Intimacy__c, System.Label.JP_Opportunity_Intimacy_Nothing);
		Test.stopTest();
	}
	/*
     * Field JP_Intimacy__c on Opportunity record Update test after Field JP_SR__c is updated on JP_Intimacy__c record.
     */
	static testMethod void Test_InputIntimacyToOpportunity_03(){
		Test.startTest();
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_JapanSurgicalDevice);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky1101');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor1101');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert1101', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2012, 10, 12, con.Id);
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertEquals(insAfterOpp.JP_Intimacy__c, intm.JP_Intimacy__c);
		//Run Trigger after update Intimacy(Field JP_SR__c value on JP_Intimacy__c to null)
		JP_Intimacy__c updIntm = [SELECT Id, JP_SR__c FROM JP_Intimacy__c WHERE Id = : intm.Id];
		updIntm.JP_SR__c = null;
		update updIntm;
		//Opportunity info after run Trigger.isUpdate 
		Opportunity updAfterOpp = [SELECT Id, JP_Intimacy__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertEquals(updAfterOpp.JP_Intimacy__c, System.Label.JP_Opportunity_Intimacy_Nothing);
		Test.stopTest();
	}
	/*
     * Field JP_Intimacy__c on Opportunity record NOT Update test when the target Opportunity's RecordType is NOT Japan's.
     */
	static testmethod void Test_InputIntimacyToOpportunity04(){
		//Get RecordTypeId FROM RecordType by RecordType.DeveloperName of Account, Contact, or Opportunity
		RecordType accRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Account_HealthcareFacility);
		RecordType oppRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Opportunity_USRMS);
		RecordType conRec = JP_TestUtil.selectRecordType(System.Label.JP_RecordType_Contact_AffiliatedClinician);
		//Create Test Account
		Account acc = JP_TestUtil.createTestAccount(accRec.Id, 'test_TerraSky0618');
		//Create Test Contact
		Contact con = JP_testUtil.createTestContact(conRec.Id, acc.Id, 'test_ContactDoctor0618');
		//Create Test Opportunity
		Opportunity opp = JP_TestUtil.createIntimacyTestOpportunity(oppRec.Id, acc.Id, 'test_IntimacyInsert0618', 
																	System.Label.JP_Opportunity_StageName_Develop, 
																	System.Label.JP_Opportunity_CapitalDisposable_Disposable, 2013, 09, 21, con.Id);
		//Get Test User Id
		User testUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
		Test.startTest();
		//Create Test Intimacy
		JP_Intimacy__c intm = JP_TestUtil.createTestIntimacy(con.Id, System.Label.JP_Intimacy_Intimacy_Bgood, testUser.Id);
		Test.stopTest();
		//Opportunity info after run Trigger.isInsert
		Opportunity insAfterOpp = [SELECT Id, JP_Intimacy__c, JP_Key_Dr__c FROM Opportunity WHERE Id = : opp.Id];
		System.assertNotEquals(insAfterOpp.JP_Intimacy__c, System.Label.JP_Intimacy_Intimacy_Bgood);
		System.assertEquals(insAfterOpp.JP_Intimacy__c, null);
	}
}