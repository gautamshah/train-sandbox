global class batchPPDCreation implements Database.Batchable<sObject>,Database.Stateful  
{
    global List<Incontinence_Submissions__c> lstIncoPPD = new List<Incontinence_Submissions__c>();     
    global Map<ID,Contact> mapIncoCustomer = new Map<ID,Contact>();   
    //global Map<ID,String> mapIncoCustomerId = new Map<ID,String>();
    global Map<ID,String> mapIncoDistributor = new Map<ID,String>();
    global List<Contact> lstCustomerContacts = [Select c.ID,c.Name,c.Inco_Customer_Contact__c,c.AccountId,c.Account.id,Email From Contact c where c.Inco_Customer_Contact__c = TRUE and Email != null and LastModifiedDate >= 2016-09-01T00:00:00Z];
    global Map<String,ID> mapIncoItemSeriesId = new Map<String,ID>();
    global List<Inco_Item__c> lstIncoItems = [SELECT ID,Series__c,Product_SKU__c From Inco_Item__c where Series__c != NULL and Product_SKU__c !=NULL];
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String query = 'Select i.Product_Series__c, i.Product_Family__c, i.Nighttime_PAR__c, i.Name, i.Id, i.Daytime_PAR__c, i.Account__r.ADC__c, i.Account__r.Id,Account__c From Inco_Product_PAR__c i where i.Account__r.Id != NULL';       
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Inco_Product_PAR__c> scope) 
    {
        for(Inco_Product_PAR__c IncoPAR : scope)
        {
            for(Inco_Item__c IncoItemVar : lstIncoItems)
            {
                if(IncoPAR.Product_Series__c == IncoItemVar.Series__c)
                { 
                    Incontinence_Submissions__c IncoPPDItem = new Incontinence_Submissions__c();
                    IncoPPDItem.Account__c = IncoPAR.Account__r.Id;
                    IncoPPDItem.Inco_Product_PAR__c = IncoPAR.Id;
                    IncoPPDItem.For_the_week_of__c = System.Today();
                    IncoPPDItem.Inco_Item__c = IncoItemVar.ID;
                    DateTime dt = DateTime.newInstance(IncoPPDItem.For_the_week_of__c.year(), IncoPPDItem.For_the_week_of__c.month(),IncoPPDItem.For_the_week_of__c.day());
                    String dateStr = dt.format('dd MMMM YYYY');
                    IncoPPDItem.Name=dateStr + ' ' + '-' + ' ' + IncoItemVar.Product_SKU__c + ' ' + '-' + ' ' + IncoPAR.Product_Series__c;
                    lstIncoPPD.add(IncoPPDItem);
                }
            }          
        }
         insert lstIncoPPD;
         system.debug('list of contacts' + lstCustomerContacts);
         system.debug('list of PPD' + lstIncoPPD);
        
      
        for(Incontinence_Submissions__c createdPPDVar : lstIncoPPD)
        {
            for(contact conVar : lstCustomerContacts)
            {
                if(createdPPDVar.Account__c == conVar.Account.Id)
                {
                    //system.debug('PPD ID to put inside IF' + createdPPDVar.ID);
                    //system.debug('Email to put inside IF' + conVar.email);
                    mapIncoCustomer.put(createdPPDVar.Account__c,conVar);
                    //mapIncoCustomerId.put(createdPPDVar.Account__c,conVar.Id);
                }
            } 
        }
    }    
    
    global void finish(Database.BatchableContext BC) 
    {
        OrgWideEmailAddress orgEmail = [SELECT Id FROM OrgWideEmailAddress Where Address = 'ppdportal@medtronic.com' Limit 1];
        EmailTemplate template = [SELECT Id FROM EmailTemplate Where DeveloperName = 'Inco_PPD_Customer_Submission_Request' Limit 1];
        List<Messaging.SingleEmailMessage> lMails = new List<Messaging.SingleEmailMessage>();
        for(ID customerID : mapIncoCustomer.keySet())
        {
            System.debug('Contact Email: ' + mapIncoCustomer.get(customerID).Email);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(mapIncoCustomer.get(customerID).Id);
            mail.setOrgWideEmailAddressId(orgEmail.Id);
            mail.setTemplateId(template.Id);
            lMails.add(mail);
        }
        Messaging.SendEmail(lMails);
    }
}