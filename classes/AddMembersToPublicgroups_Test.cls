@isTest
private class AddMembersToPublicgroups_Test {

    static testMethod void runTest() {
    Group g1=new Group(name='DE_MCS');
    insert g1;
    Group g2=new Group(name='DE_GSP');
    insert g2;
   
    User U1 = new user(alias='demo1',LastName='Demo1',Username='mdtdemo1@demopods.com',Email='demo11@demopods.com',LocaleSidKey='en_US',TimeZoneSidKey= 'America/Los_Angeles',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',ProfileId='00eU0000000pN2d',Country='DE',Sales_Org_PL__c='MCS',User_Role__c='Field Manager');                                 
    insert U1;  
      
    User U2 = new user(alias='demo2',LastName='Demo1',Username='mdtdemo2@demopods.com',Email='demo11@demopods.com',LocaleSidKey='en_US',TimeZoneSidKey= 'America/Los_Angeles',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',ProfileId='00eU0000000pN2d',Country='DE',Sales_Org_PL__c='GSP',User_Role__c='Field Manager');
    insert U2;     
        
    Test.startTest();
       
    AddMembersToPublicgroups batchApex = new AddMembersToPublicgroups();
    ID batchprocessid = Database.executeBatch(batchApex);
              
    Test.stopTest();
    /*   
    System.assertEquals(1,[select count() from groupmember where groupid =:g1.id and userorgroupid=:U1.id]);
    System.assertEquals(1,[select count() from groupmember where groupid =:g2.id and userorgroupid=:U2.id]);
    */
     
    }
}