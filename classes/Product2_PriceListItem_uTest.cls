@isTest
public class Product2_PriceListItem_uTest
{
    @isTest
    static void SyncPriceListItems()
    {
    	//// This method creates or updates the PLI if a Product2 is Inserted
    	//// or Updated
        Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.RMS;

    	Product2 p = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.RMS, 'TEST', 1);
    	insert p;
    	
    	Apttus_Config2__PriceListItem__c pli = cpqPriceListItem_u.fetch()[0];
    	
    	system.assertNotEquals(null, pli);
    	system.assertEquals(p.Id, pli.Apttus_Config2__ProductId__c);
    }
}