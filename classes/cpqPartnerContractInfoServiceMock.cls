/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed  80 => *                            120 => *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry

YYYYMMDD-A  PAB         CPR-000 Updated comments section

7.  Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com
Gautam Shah             GS              Medtronic.com

MODIFICATION HISTORY
====================
Date-Id     Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20171212-A  HN                 Created at 3:48:04 PM by Henry Noerdlinger

*/

@isTest
global class cpqPartnerContractInfoServiceMock implements WebServiceMock
{
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType)
    {

        object responseElement;
        //if (requestName == 'getTheContract')
        if (requestName == 'ContractInfo')
        {
            cpqPartnerContractInfoService.ContractInfoResponse_element aResponse =
                new cpqPartnerContractInfoService.ContractInfoResponse_element();

            aResponse.ContractInfoResult = new cpqPartnerContractInfoClasses.ContractInfoResponseType();
            aResponse.ContractInfoResult.contractInfoField = new cpqPartnerContractInfoClasses.ContractInfoType();
            aResponse.ContractInfoResult.contractInfoField.PropertyChanged 
                        = new webServiceConstants.PropertyChangedEventHandler();
            aResponse.ContractInfoResult.contractInfoField.effectiveDateField = DateTime.now();
            aResponse.ContractInfoResult.contractInfoField.effectiveDateField.addDays(-20);
            aResponse.ContractInfoResult.contractInfoField.nameField = 'Test Contract Name';
            aResponse.ContractInfoResult.contractInfoField.rootContractField = 'Test Root Contract';
            aResponse.ContractInfoResult.contractInfoField.commitmentTypeField = 'Test Commitment Type';
            aResponse.ContractInfoResult.contractInfoField.expirationDateField  = DateTime.now();
            aResponse.ContractInfoResult.contractInfoField.expirationDateField.addDays(20);

            aResponse.ContractInfoResult.dealersField = new cpqPartnerContractInfoClasses.ArrayOfContractDealerType();
            aResponse.ContractInfoResult.dealersField.ContractDealerType
                        = new cpqPartnerContractInfoClasses.ContractDealerType[1];

            aResponse.ContractInfoResult.dealersField.ContractDealerType[0] 
                        = new cpqPartnerContractInfoClasses.ContractDealerType();

            aResponse.ContractInfoResult.dealersField.ContractDealerType[0].shipToField = 333333;
           
            aResponse.ContractInfoResult.errorCodeField = '0';
            //aResponse.ContractInfoResult.errorDescriptionField = 'One Two and Tree';

            aResponse.ContractInfoResult.itemsField = new cpqPartnerContractInfoClasses.ArrayOfContractItemType();
            aResponse.ContractInfoResult.itemsField.ContractItemType = new cpqPartnerContractInfoClasses.ContractItemType[1];
            aResponse.ContractInfoResult.itemsField.ContractItemType[0] 
                        = new cpqPartnerContractInfoClasses.ContractItemType();
            aResponse.ContractInfoResult.itemsField.ContractItemType[0].alternateUOMCaseQuantityField = 2;
            aResponse.ContractInfoResult.itemsField.ContractItemType[0].alternateUOMField  = 'Box';
            aResponse.ContractInfoResult.itemsField.ContractItemType[0].itemDescriptionField = 'Item Desc';
            aResponse.ContractInfoResult.itemsField.ContractItemType[0].dealerNetField = 3;
            aResponse.ContractInfoResult.itemsField.ContractItemType[0].unitOfMeasureField = 'Box';
      
            responseElement = aResponse;
        }
        else
        if (requestName == 'GetContractEligibilityByRoot')
        {
            cpqPartnerContractInfoService.GetContractEligibilityByRootResponse_element aResponse =
                new cpqPartnerContractInfoService.GetContractEligibilityByRootResponse_element();
                
            aResponse.GetContractEligibilityByRootResult = new cpqPartnerContractInfoClasses.Customer_Wrapper(); 
            aResponse.GetContractEligibilityByRootResult.TotalCount = 1;
            aResponse.GetContractEligibilityByRootResult.Customers 
                    = new cpqPartnerContractInfoClasses.ArrayOfInformation();
            aResponse.GetContractEligibilityByRootResult.Customers.Information 
                    = new cpqPartnerContractInfoClasses.Information[1];
                    
           aResponse.GetContractEligibilityByRootResult.Customers.Information[0] 
              = new cpqPartnerContractInfoClasses.Information();

            aResponse.GetContractEligibilityByRootResult.Customers.Information[0].ShipTo = 333333;
            responseElement = aResponse;
        }
        
        response.put('response_x', responseElement);
    }
    
}