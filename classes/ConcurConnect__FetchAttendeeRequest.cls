/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FetchAttendeeRequest {
    global FetchAttendeeRequest(String requestXML) {

    }
    global String getCompany() {
        return null;
    }
    global String getCustom1() {
        return null;
    }
    global String getCustom10() {
        return null;
    }
    global String getCustom11() {
        return null;
    }
    global String getCustom12() {
        return null;
    }
    global String getCustom13() {
        return null;
    }
    global String getCustom14() {
        return null;
    }
    global String getCustom15() {
        return null;
    }
    global String getCustom16() {
        return null;
    }
    global String getCustom17() {
        return null;
    }
    global String getCustom18() {
        return null;
    }
    global String getCustom19() {
        return null;
    }
    global String getCustom2() {
        return null;
    }
    global String getCustom20() {
        return null;
    }
    global String getCustom3() {
        return null;
    }
    global String getCustom4() {
        return null;
    }
    global String getCustom5() {
        return null;
    }
    global String getCustom6() {
        return null;
    }
    global String getCustom7() {
        return null;
    }
    global String getCustom8() {
        return null;
    }
    global String getCustom9() {
        return null;
    }
    global String getFirstName() {
        return null;
    }
    global String getLastName() {
        return null;
    }
    global String getTitle() {
        return null;
    }
    global String toXml() {
        return null;
    }
}
