/****************************************************************************************
* Name    : ActivityReferencer
* Author  : Gautam Shah
* Date    : 4/20/2016
* Purpose : Used to establish the references from Activity to Activity_Detail__c object
*			Used after running ActivityMigratorBatch
* 
* Dependancies: 
* Referenced By: ActivityReferencerBatch
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* 	DATE         	AUTHOR                  CHANGE
* 	----          	------                  ------
*	
*****************************************************************************************/
public class ActivityReferencer 
{
	public static Boolean setEventReference(List<Event> events, String indicatorField, String indicatorValue)
    {
        List<String> eventIds = new List<String>();
        for(Event e : events)
        {
            eventIds.add(e.Id);
        }
        List<Activity_Detail__c> objActivities = new List<Activity_Detail__c>([Select Id, ActivityId__c From Activity_Detail__c Where ActivityId__c In :eventIds]);
        Map<String, String> actDetailMap = new Map<String, String>();
        for(Activity_Detail__c ad : objActivities)
        {
            actDetailMap.put(ad.ActivityId__c, ad.Id);
        }
        for(Event e : events)
        {
            e.Activity_Detail__c = actDetailMap.get(e.Id);
            e.put(indicatorField,indicatorValue);
        }
        try
        {
        	update events;
        }
        catch(Exception e)
        {
            System.debug(e.getMessage());
            return false;
        }
        return true;
    }
    
    public static Boolean setTaskReference(List<Task> tasks, String indicatorField, String indicatorValue)
    {
        List<String> taskIds = new List<String>();
        for(Task t : tasks)
        {
            taskIds.add(t.Id);
        }
        List<Activity_Detail__c> objActivities = new List<Activity_Detail__c>([Select Id, ActivityId__c From Activity_Detail__c Where ActivityId__c In :taskIds]);
        Map<String, String> actDetailMap = new Map<String, String>();
        for(Activity_Detail__c ad : objActivities)
        {
            actDetailMap.put(ad.ActivityId__c, ad.Id);
        }
        for(Task t : tasks)
        {
            t.Activity_Detail__c = actDetailMap.get(t.Id);
            t.put(indicatorField,indicatorValue);
        }
        try
        {
        	update tasks;
        }
        catch(Exception e)
        {
            System.debug(e.getMessage());
            return false;
        }
        return true;
    }
}