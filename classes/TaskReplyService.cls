/****************************************************************************************
* Name    : TaskReplyService
* Author  : Paul Berglund
* Date    : 11/12/2015
* Purpose : Email handler for Tasks
* 
* Dependancies: 
*   Organization-Wide Address:  Medtronic - CPQ Task
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 11/12/2015    Paul Berglund           Initial version created
* 02/12/2016    Paul Berglund           Removed an @future attribute
*****************************************************************************************/
global class TaskReplyService implements Messaging.InboundEmailHandler
{

    //
    // This method detects if the email being received is the initial verification
    // email for the Organization-Wide Email Address that requires human acknowledgement
    // and forwards it to me.
    //
    static string OrgWideEmailAddressTo = 'paul.berglund@medtronic.com';
    static string OrgWideEmailAddressDisplayName = 'Medtronic - CPQ Task';
    
    private static boolean OrgWideEmailAddressVerified(Messaging.InboundEmail email) {
        try {
            OrgWideEmailAddress owea = [SELECT Id, Address FROM OrgWideEmailAddress WHERE DisplayName = :OrgWideEmailAddressDisplayName LIMIT 1];

            // Test if Organization-Wide Email Is Verified
            // We have to look for s or z in the work Organization to accomodate
            // other localities doing the setup
            string part1 = 'New Organi';
            string part2 = 'ation-Wide Email Address: ' + owea.Address;
            Pattern p = Pattern.compile(Pattern.quote(part1)+'[s|z]'+Pattern.quote(part2));
            system.debug(p);
            Matcher m = p.matcher(email.plainTextBody);
            boolean found = m.find();
    
            if(found) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.Subject = email.Subject;
                mail.HtmlBody = 'From: ' + email.FromAddress + '<br/>ReplyTo: ' + email.inReplyTo + '<br/>' + email.HtmlBody;
                mail.ToAddresses = new List<string> { OrgWideEmailAddressTo };
                try {
                    Messaging.SendEmail(new List<Messaging.SingleEmailMessage> { mail } );
                }
                catch (Exception ex) {
                    system.debug(ex);
                    return false;
                }
            }
            return true;
        }
        catch (QueryException ex) {
            system.debug(ex);
            return false;
        }
    }        

    //
    // This is the inbound email handler method
    //        
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
                                                           Messaging.InboundEnvelope envelope) {

        system.debug('Email: ' + email);
        if(OrgWideEmailAddressVerified(email)) {
            RecordEmailAndCloseTask(email);
        
        }
                                                           
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        result.success = true;
        
        return result;
    }
    
    @testVisible
    private void RecordEmailAndCloseTask(Messaging.InboundEmail email) {
        Id relatedTo = ExtractRelatedToFromText(email.subject);
        
        if (relatedTo != null) {
            Id activity = SaveAsActivity(relatedTo, email);

            if (HasAttachments(email))
                AddAttachmentsToActivity(activity, email);
                
            if (TaskIsCompleted(email.plainTextBody))
                CloseAgreementImplementationTask(relatedTo);
        }
    }
    
    @testVisible
    private Id ExtractRelatedToFromText(string text) {
        Pattern p = Pattern.compile(Pattern.quote('{Ref:') + '(\\w{18})' + Pattern.quote(':Ref}'));
        Matcher m = p.matcher(text);
        
        if (m.find()) {
            string relatedTo = m.group(1);
            if (relatedTo != null)
                return Id.valueOf(relatedTo);
        }
        return null;
    }
    
    @testVisible
    private Id SaveAsActivity(Id relatedTo, Messaging.InboundEmail email) {
        Task target = new Task();
        target.subject = 'Email: ' + email.subject;
        target.description = email.plainTextBody;
        target.whatId = relatedTo;
        target.status = 'Completed';
        target.type = 'Email';
    
        system.debug('pre-Insert target: ' + target);
        insert target;
        Task test = [SELECT Id, Subject, Description FROM Task WHERE Id = :target.Id];
        system.debug('post-Insert target: ' + test);
        return target.Id;
    }
    
    @testVisible
    private boolean HasAttachments(Messaging.InboundEmail email) {
        return (HasBinaryAttachments(email.binaryAttachments) || HasTextAttachments(email.textAttachments));
    }
    
    @testVisible
    private boolean HasBinaryAttachments(Messaging.InboundEmail.BinaryAttachment[] binaryAttachments) {
        return (binaryAttachments != null && binaryAttachments.size() > 0);
    }
    
    @testVisible
    private boolean HasTextAttachments(Messaging.InboundEmail.TextAttachment[] textAttachments) {
        return (textAttachments != null && textAttachments.size() > 0);
    }
    
    @testVisible
    private void AddAttachmentsToActivity(Id activity, Messaging.InboundEmail email) {
        if (email == null) return;
        if (!HasAttachments(email)) return;
        
        List<Attachment> targets = new List<Attachment>();
        
        if (HasBinaryAttachments(email.binaryAttachments)) {
            for(Messaging.InboundEmail.BinaryAttachment a : email.binaryAttachments) {
                Attachment target = new Attachment();
                target.ParentId = activity;    
                target.OwnerId  = cpqUser_c.CurrentUser.Id;
                for(Messaging.InboundEmail.Header h : a.headers) {
                    if (h.name == 'ContentType') {
                        target.ContentType = h.value;
                        break;
                    }
                }
                target.Name = a.fileName;
                target.Description = a.fileName;
                target.Body = a.body;
            
                targets.add(target);
            }
        
        }
        
        if (HasTextAttachments(email.textAttachments)) {
            for(Messaging.InboundEmail.TextAttachment a : email.textAttachments) {
                Attachment target = new Attachment();
                target.ParentId = activity;    
                target.OwnerId  = cpqUser_c.CurrentUser.Id;
                for(Messaging.InboundEmail.Header h : a.headers) {
                    if (h.name == 'ContentType') {
                        target.ContentType = h.value;
                        break;
                    }
                }
                target.Name = a.fileName;
                target.Description = a.fileName;
                target.Body = Blob.valueOf(a.body);
            
                targets.add(target);
            }
        }
        
        if (targets.size() > 0)
            insert targets;
    }
    
    @testVisible
    private boolean TaskIsCompleted(string text) {
        system.debug('IsCompleted: ' + text);
        
        Pattern p = Pattern.compile(Pattern.quote('Is Implemented by CS:') + '(.*)');
        Matcher m = p.matcher(text);
        
        if (m.find()) {
            string response = m.group(1);
            system.debug('TaskIsCompleted: ' + response);
            return !response.contains('No');
        }
        return false;
    }
    
    @testVisible
    private static void CloseAgreementImplementationTask(Id relatedTo) {
        system.debug('Close: ' + relatedTo);
        
        Map<Id, Task> openAgreementImplementationTasks =
            new Map<Id, Task>([SELECT Id
                               FROM Task
                               WHERE WhatId = :relatedTo AND
                                     Subject IN ('Agreement Implementation', 'Sent Activation Email to Customer Service', 'Sent to Customer Service') AND
                                     Status != 'Completed']);

        system.debug('open tasks: ' + openAgreementImplementationTasks);

        if (openAgreementImplementationTasks.size() > 0) {                                     
            for(Id key : openAgreementImplementationTasks.keySet())
                openAgreementImplementationTasks.get(key).Status = 'Completed';
            
            update openAgreementImplementationTasks.values();
        }
        
        Map<Id, Task> updated = new Map<Id, Task>([SELECT Id,
                                                          Subject,
                                                          Description,
                                                          Status
                                                   FROM Task
                                                   WHERE WhatId = :relatedTo AND
                                                         Subject IN ('Agreement Implementation', 'Sent Activation Email to Customer Service', 'Sent to Customer Service')]);
        system.debug('updated tasks: ' + updated);
    }
}