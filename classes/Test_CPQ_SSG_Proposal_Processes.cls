@isTest
private class Test_CPQ_SSG_Proposal_Processes {
    @testSetup static void setupRFAProposalData() {
        //fetches or creates a Custom Setting at the organization level
        /*
        CPQ_Trigger_Profile__c setting = CPQ_Trigger_Profile__c.getOrgDefaults();
        if (setting == null) {
            setting = new HierarchySetting__c();
        }
        setting.CustomField__c = 'http://salesforce.stackexchange.com/';
        upsert setting;
        */

        List<Product2> products = new List<Product2>();
        products.add(new Product2(Name = 'Test Product 1', ProductCode = 'TEST1', Category__c = 'Electrosurgery'));
        products.add(new Product2(Name = 'Test Product 2', ProductCode = 'TEST2', Category__c = 'Electrosurgery'));
        products.add(new Product2(Name = 'Test Product 3', ProductCode = 'TEST3', Category__c = 'Vessel Sealing'));
        insert products;

        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US'));
        accounts.add(new Account(Name = 'Test Account 2', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US'));
        accounts.add(new Account(Name = 'Test Account 3', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US'));
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(FirstName = 'Test', LastName = 'Contact 1', AccountId = accounts[0].Id));
        contacts.add(new Contact(FirstName = 'Test', LastName = 'Contact 2', AccountId = accounts[1].Id));
        contacts.add(new Contact(FirstName = 'Test', LastName = 'Contact 3', AccountId = accounts[2].Id));
        insert contacts;

        Opportunity opp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = accounts[0].Id);
        insert opp;

		RecordType rfaRecordType = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,'Royalty_Free_Agreement');
		RecordType otherRecordType = [Select Id From RecordType Where SobjectType = 'Apttus_Proposal__Proposal__c' And DeveloperName != 'Royalty_Free_Agreement' limit 1];
		List<Apttus_Proposal__Proposal__c> proposals = new List<Apttus_Proposal__Proposal__c>();
        proposals.add(new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id, Apttus_Proposal__Approval_Stage__c = 'Draft', Apttus_Proposal__Account__c = accounts[0].Id, Primary_Contact2__c = contacts[0].Id, RecordTypeId = rfaRecordType.Id, Apttus_QPConfig__PONumber__c = '12345', Electrosurgery_Incremental_Spend__c = 10000, Ligasure_Incremental_Spend__c = 20000));
        proposals.add(new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id, Apttus_Proposal__Approval_Stage__c = 'Draft', Apttus_Proposal__Account__c = accounts[0].Id, Primary_Contact2__c = contacts[0].Id, RecordTypeId = rfaRecordType.Id, Apttus_QPConfig__PONumber__c = '54321', Electrosurgery_Incremental_Spend__c = 10000, Ligasure_Incremental_Spend__c = 20000));
        proposals.add(new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id, Apttus_Proposal__Approval_Stage__c = 'Draft', Apttus_Proposal__Account__c = accounts[0].Id, Primary_Contact2__c = contacts[0].Id, RecordTypeId = otherRecordType.Id, Apttus_QPConfig__PONumber__c = '23451', Electrosurgery_Incremental_Spend__c = 10000, Ligasure_Incremental_Spend__c = 20000));
        insert proposals;

        List<Apttus_Config2__ProductConfiguration__c> configs = new List<Apttus_Config2__ProductConfiguration__c>();
        configs.add(new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = proposals[0].Id, Apttus_Config2__Status__c = 'Ready for Finalization', Apttus_Config2__BusinessObjectId__c = proposals[0].Id, Apttus_Config2__BusinessObjectType__c = 'Proposal', Apttus_Config2__VersionNumber__c = 1));
        configs.add(new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = proposals[1].Id, Apttus_Config2__Status__c = 'Ready for Finalization', Apttus_Config2__BusinessObjectId__c = proposals[1].Id, Apttus_Config2__BusinessObjectType__c = 'Proposal', Apttus_Config2__VersionNumber__c = 1));
        configs.add(new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = proposals[1].Id, Apttus_Config2__Status__c = 'Ready for Finalization', Apttus_Config2__BusinessObjectId__c = proposals[2].Id, Apttus_Config2__BusinessObjectType__c = 'Proposal', Apttus_Config2__VersionNumber__c = 1));
        insert configs;

        List<Apttus_Config2__LineItem__c> lineItems = new List<Apttus_Config2__LineItem__c>();
        lineItems.add(createLineItem(configs[0].Id, products[0], 1, 'Hardware'));
	    lineItems.add(createLineItem(configs[0].Id, products[0], 2, 'Consumable'));
	    lineItems.add(createLineItem(configs[0].Id, products[1], 3, 'Consumable'));
	    lineItems.add(createLineItem(configs[1].Id, products[1], 1, 'Hardware'));
        lineItems.add(createLineItem(configs[1].Id, products[1], 2, 'Consumable'));
	    lineItems.add(createLineItem(configs[1].Id, products[2], 3, 'Consumable'));
        insert lineItems;

        List<Participating_Facility__c> participatingFacilities = new List<Participating_Facility__c>();
        participatingFacilities.add(new Participating_Facility__c(Account__c = accounts[1].Id, Proposal__c = proposals[0].Id));
        participatingFacilities.add(new Participating_Facility__c(Account__c = accounts[2].Id, Proposal__c = proposals[1].Id));
        insert participatingFacilities;

        List<Sales_History__c> salesHistories = new List<Sales_History__c>();
        salesHistories.add(new Sales_History__c(Customer__c = accounts[0].Id, SKU__c = products[0].ProductCode, Total_Current_Year_Eaches__c = 1, Total_Current_Year_Sales__c = 2));
        salesHistories.add(new Sales_History__c(Customer__c = accounts[0].Id, SKU__c = products[1].ProductCode, Total_Current_Year_Eaches__c = 3, Total_Current_Year_Sales__c = 4));
        salesHistories.add(new Sales_History__c(Customer__c = accounts[1].Id, SKU__c = products[1].ProductCode, Total_Current_Year_Eaches__c = 5, Total_Current_Year_Sales__c = 6));
        salesHistories.add(new Sales_History__c(Customer__c = accounts[1].Id, SKU__c = products[2].ProductCode, Total_Current_Year_Eaches__c = 7, Total_Current_Year_Sales__c = 8));
        salesHistories.add(new Sales_History__c(Customer__c = accounts[2].Id, SKU__c = products[0].ProductCode, Total_Current_Year_Eaches__c = 9, Total_Current_Year_Sales__c = 10));
        salesHistories.add(new Sales_History__c(Customer__c = accounts[2].Id, SKU__c = products[2].ProductCode, Total_Current_Year_Eaches__c = 11, Total_Current_Year_Sales__c = 12));
        insert salesHistories;
    }

    private static Apttus_Config2__LineItem__c createLineItem(Id configId, Product2 product, Integer lineNumber, String chargeType) {
        Apttus_Config2__LineItem__c li = new Apttus_Config2__LineItem__c();
        li.Apttus_Config2__ConfigurationId__c = configId;
        li.Apttus_Config2__ItemSequence__c = 1;
        li.Apttus_Config2__LineNumber__c = lineNumber;
        li.CurrencyIsoCode = 'USD';
        li.Apttus_Config2__LineType__c = 'Option';
        li.Apttus_Config2__ProductId__c = product.Id;
        li.Apttus_Config2__RollupPriceToBundle__c = true;
        li.Apttus_Config2__AllocateGroupAdjustment__c = true;
        li.Apttus_Config2__Term__c = 1;
        li.Apttus_Config2__AllowManualAdjustment__c = true;
        li.Apttus_Config2__AllowProration__c = true;
        li.Apttus_Config2__AllowRemoval__c = true;
        li.Apttus_Config2__SellingFrequency__c = 'Yearly';
        li.Apttus_Config2__AllowableAction__c = 'Unrestricted';
        li.Apttus_Config2__SellingTerm__c = 1;
        li.Apttus_Config2__PricingStatus__c = 'Complete';
        li.Apttus_Config2__HasOptions__c = false;
        li.Apttus_Config2__Quantity__c = 1;
        li.Apttus_Config2__PrimaryLineNumber__c = 2;
        li.Apttus_Config2__ConfigStatus__c = 'Complete';
        li.Apttus_Config2__TotalQuantity__c = 1;
        li.Apttus_Config2__Frequency__c = 'Yearly';
        li.Apttus_Config2__Description__c = product.Name;
        li.Apttus_Config2__AddedBy__c = 'User';
        li.Apttus_Config2__SyncStatus__c = 'Synchronized';
        li.Apttus_Config2__LineStatus__c = 'New';
        li.Apttus_Config2__ChargeType__c = chargeType;
        return li;
    }

    static testMethod void Test_RFA_Proposal() {
    	List<Apttus_Config2__ProductConfiguration__c> configs = [Select Id From Apttus_Config2__ProductConfiguration__c];
    	Test.startTest();
    		for (Apttus_Config2__ProductConfiguration__c config: configs) {
    			config.Apttus_Config2__Status__c = 'Finalized';
    		}
    		update configs;
    		List<Apttus_Proposal__Proposal__c> proposals = [Select Id, Electrosurgery_Eaches__c, Ligasure_Eaches__c, Electrosurgery_Sales__c, Ligasure_Sales__c, Apttus_QPConfig__PONumber__c From Apttus_Proposal__Proposal__c];
	    		for (Apttus_Proposal__Proposal__c proposal: proposals) {
				if (proposal.Apttus_QPConfig__PONumber__c == '12345') {
                    /*
					System.assertEquals(9, proposal.Electrosurgery_Eaches__c);
					System.assertEquals(12, proposal.Electrosurgery_Sales__c);
					System.assertEquals(0, proposal.Ligasure_Eaches__c);
					System.assertEquals(0, proposal.Ligasure_Sales__c);
                    */
                    System.debug('12345 ES Eaches = ' + proposal.Electrosurgery_Eaches__c);
                    System.debug('12345 ES Sales = ' + proposal.Electrosurgery_Sales__c);
                    System.debug('12345 LS Eaches = ' + proposal.Ligasure_Eaches__c);
                    System.debug('12345 LS Sales = ' + proposal.Ligasure_Sales__c);
				} else if (proposal.Apttus_QPConfig__PONumber__c == '54321') {
                    /*
					System.assertEquals(3, proposal.Electrosurgery_Eaches__c);
					System.assertEquals(4, proposal.Electrosurgery_Sales__c);
					System.assertEquals(11, proposal.Ligasure_Eaches__c);
					System.assertEquals(12, proposal.Ligasure_Sales__c);
                    */
                    System.debug('54321 ES Eaches = ' + proposal.Electrosurgery_Eaches__c);
                    System.debug('54321 ES Sales = ' + proposal.Electrosurgery_Sales__c);
                    System.debug('54321 LS Eaches = ' + proposal.Ligasure_Eaches__c);
                    System.debug('54321 LS Sales = ' + proposal.Ligasure_Sales__c);
				}
			}
    	Test.stopTest();
    }
}