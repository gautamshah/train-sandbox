public with sharing class CopyReceivedQuantity {
    public Id cyclePeriodId{get;set;}
    public CopyReceivedQuantity(ApexPages.StandardController stdController) {
        cyclePeriodId=stdController.getId();
    }
    
    public Pagereference copyandpaste(){
        List<In_Transit__c> lstIn=[select Id,Sales_In__r.Quantity_Remaining__c,Quantity_Received__c, cycle_period_dist__c from In_Transit__c where Quantity_Received__c=null and Cycle_Period_dist__c=:cyclePeriodId];
        if(lstIn!=null)
        {
            for(In_Transit__c intr:lstIn)
                intr.Quantity_Received__c=intr.Sales_In__r.Quantity_Remaining__c;
        }
        update lstIn;
        return null;
    }

}