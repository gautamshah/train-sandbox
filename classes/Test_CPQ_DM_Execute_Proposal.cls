/**
Test class

CPQ_DM_Execute_Proposal

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-04      Yuli Fintescu       Created
11/02/2016   Paul Berglund   Commented out to save space, but keep in case we
                             do something similar again
===============================================================================
*/
@isTest
private class Test_CPQ_DM_Execute_Proposal
{
/*
    static void createTestData() {
        Map<String,Id> mapProposalRType = New Map<String,Id>();
        for(RecordType R: [Select Id, Name, DeveloperName From RecordType Where SobjectType = 'Apttus_Proposal__Proposal__c']) {
            mapProposalRType.Put(R.DeveloperName, R.Id);
        }
        
        List<CPQ_Variable__c> testVars = new List<CPQ_Variable__c> {
            new CPQ_Variable__c(Name = 'Proxy EndPoint', Value__c = 'Test Proxy EndPoint'),
            new CPQ_Variable__c(Name = 'RMS PriceList ID', Value__c = testPriceList.ID),
            new CPQ_Variable__c(Name = 'HPG Group Code', Value__c = 'SG0150')
        };
        insert testVars;
        
        List<CPQ_DM_Variable__c> testDMVars = new List<CPQ_DM_Variable__c> {
            new CPQ_DM_Variable__c(Name = 'Proposal COOP', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('COOP_Plus_Program')),
            new CPQ_DM_Variable__c(Name = 'Proposal EPP', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Equipment_Placement_Program_EPP_s')),
            new CPQ_DM_Variable__c(Name = 'Proposal LNA', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Locally_Negotiated_Agreement_LNA')),
            new CPQ_DM_Variable__c(Name = 'Proposal Quote', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Outright_Quote_Hardware')),
            new CPQ_DM_Variable__c(Name = 'Proposal Pricing', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Sales_and_Pricing_Agreement')),
            new CPQ_DM_Variable__c(Name = 'Proposal Offer Letter', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Price_Offer_Letter')),
            new CPQ_DM_Variable__c(Name = 'Proposal Current Price Quote', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Current_Price_Quote')),
            new CPQ_DM_Variable__c(Name = 'Proposal Rebate Agreement', Type__c = 'RecordType ID', Value__c = mapProposalRType.get('Rebate_Agreement')),
            
            new CPQ_DM_Variable__c(Name = 'Opportunity', Type__c = 'RecordType ID', Value__c = '012U0000000LgOsIAK')
        };
        insert testDMVars;
        
        Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Old Agreement for REW/ADJ', Legacy_External_ID__c = 'D-111111');
        insert testAgreement;
        
        CPQ_STG_Partner_Contract__c testStagePartnerContract = new CPQ_STG_Partner_Contract__c(STGPC_ContractNumber__c = '11111', STGPC_Agreement__c = testAgreement.ID, STGPC_IsRoot__c = true);
        insert testStagePartnerContract;
        
        List<CPQ_STG_DealContract__c> testDealContracts = new List<CPQ_STG_DealContract__c> {
            new CPQ_STG_DealContract__c(STGDC_IsRoot__c = true, STGDC_External_ID__c = 'D-222222', STGDC_ContractNumber__c = '11111'),
            new CPQ_STG_DealContract__c(STGDC_IsRoot__c = true, STGDC_External_ID__c = 'D-333333', STGDC_ContractNumber__c = '11111'),
            new CPQ_STG_DealContract__c(STGDC_IsRoot__c = true, STGDC_External_ID__c = 'D-444444', STGDC_ContractNumber__c = '99999')
        };
        insert testDealContracts;
        
        List<Apttus_Proposal__Proposal__c> testProposals = new List<Apttus_Proposal__Proposal__c> {
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal'),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal'),
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test Proposal', RecordTypeID = mapProposalRType.get('Outright_Quote_Hardware'))
        };
        insert testProposals;
        
        List<Apttus_Config2__ProductConfiguration__c> testConfigs = new List<Apttus_Config2__ProductConfiguration__c> {
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[2].ID),
            new Apttus_Config2__ProductConfiguration__c(Apttus_QPConfig__Proposald__c = testProposals[2].ID)
        };
        insert testConfigs;
        
        List<CPQ_STG_Proposal__c> testStages = new List<CPQ_STG_Proposal__c> {
            new CPQ_STG_Proposal__c(STGPS_Proposal__c = testProposals[0].ID, STGPS_External_ID__c = 'D-222222', Name = 'Renewal with contract', STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(STGPS_Proposal__c = testProposals[0].ID, STGPS_External_ID__c = 'D-333333', Name = 'Renewal with contract', STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(STGPS_Proposal__c = testProposals[1].ID, STGPS_External_ID__c = 'D-444444', Name = 'Renewal contract not found', STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(STGPS_Configuration__c = testConfigs[0].ID, STGPS_Proposal__c = testProposals[2].ID, STGPS_External_ID__c = 'D-555555', Name = 'OCQ to current price quote', STGPS_To_Test__c = true),
            new CPQ_STG_Proposal__c(STGPS_Configuration__c = testConfigs[1].ID, STGPS_Proposal__c = testProposals[2].ID, STGPS_External_ID__c = 'D-666666', Name = 'OCQ to current price quote', STGPS_To_Test__c = true)
        };
        insert testStages;
        
        List<Apttus_Config2__LineItem__c> testLines = new List<Apttus_Config2__LineItem__c> {
            new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = testConfigs[0].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1, Apttus_Config2__ChargeType__c = 'Consumable'),
            new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = testConfigs[1].ID, Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1, Apttus_Config2__ChargeType__c = 'Consumable')
        };
        insert testLines;
    }
    
    static testMethod void myUnitTest() {
        createTestData();
        
        Test.startTest();
            CPQ_DM_Execute_Proposal p = new CPQ_DM_Execute_Proposal();
            Database.executeBatch(p);
        Test.stopTest();
    }
*/
}