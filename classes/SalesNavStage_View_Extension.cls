global with sharing class SalesNavStage_View_Extension
{
	private static Sales_Navigator_Stage__c staticStage = null;
	public Sales_Navigator_Stage__c stage {get; Set;}
	public SalesNavStage_View_Extension(ApexPages.StandardController controller)
	{
		stage = (Sales_Navigator_Stage__c)controller.getRecord();
		SalesNavStage_View_Extension.staticStage = stage;
	}
	
	@RemoteAction
	global static Boolean SaveAndClose()
	{
		System.debug('In SaveAndClose() RemoteAction');
		update SalesNavStage_View_Extension.staticStage;
		return true;	
	}
	
	public PageReference SaveAndExit()
	{
		try
		{
			update stage;
		}
		catch(DmlException exc)
		{
			ApexPages.addMessages(exc);
		}
		return null;
	}
	
	public PageReference GoToStandardView()
	{
		System.debug('In GoToStandardView(): ');
		PageReference pr = new PageReference('/' + stage.Id);
		pr.setRedirect(true);
		return pr;
	}
	
	public PageReference GoToRedirectPage()
	{
		PageReference pr = new PageReference('/Apex/SalesNav_StandardPageRedirect');
		pr.getParameters().put('id', stage.Id);
        pr.setRedirect(true);
		return pr;
	}
	
	public PageReference GoToCustomPage()
	{
		RecordType rts = RecordType_Utilities.GetRecordTypeById(stage.RecordTypeId);
		System.debug('rts: ' + rts);
		if(rts.DeveloperName != 'Identify')
		{
			return GoToStandardView();
		}
		
		PageReference pr = new PageReference('/apex/SalesNavStage_View2');
		pr.getParameters().put('id', stage.Id);
        pr.setRedirect(true);
		return pr;
	}

	public PageReference OnPageLoad()
	{
		System.debug('In OnPageLoad()');
		RecordType rts = RecordType_Utilities.GetRecordTypeById(stage.RecordTypeId);
		System.debug('rts: ' + rts);
		if(rts.DeveloperName != 'Identify')
		{
			System.debug('Redirecting to standard view: ');
			return GoToStandardView();
		}
		return null;	
	}
}