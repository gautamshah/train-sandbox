public class VerifyDocumentController {
    public Sales_Out__c so{get; set;}
    public boolean isSuccess{get; set;}
    public integer docCount{get; set;}
    public List<Sales_out__c> lstSales;
    public VerifyDocumentController(ApexPages.StandardController sc){
        so=(Sales_Out__c)sc.getRecord();
        Sales_Out__c sor = [Select id,Document_No__c,Cycle_Period__c from Sales_Out__c where id=:so.Id];
        System.debug('>>>>>>>>>>>>>>>>>>>'+sor.Cycle_Period__c);
        System.debug('>>>>>>>>>>>>>>>>>>>'+sor.Document_No__c);
        isSuccess = false;
        lstSales = [Select id,Document_Type__c,Verification_Status__c,Verification_Comments__c from Sales_out__c where Document_No__c=:sor.Document_No__c and Cycle_Period__c=:sor.Cycle_Period__c];
        docCount = lstSales.size();
    }
    public string url{get; set;}
    public pagereference okay(){
        if(lstSales.size()>0){
            for(Sales_out__c soRec :lstSales){
                soRec.Verification_Status__c = so.Verification_Status__c;
                soRec.Verification_Comments__c = so.Verification_Comments__c;
                soRec.Document_Type__c = so.Document_Type__c;
                soRec.Verification_By__c = UserInfo.getName();
                soRec.Verification_Date__c = System.today();
            }
            
            if(lstSales.size()>0)
            update lstSales;                            
            isSuccess = true;    
            
            String urlVal = ApexPages.currentPage().getHeaders().get('Host');
            System.debug('>>>>>>>>>>>>>>>>>>>>>>'+urlVal);
            //Schema.DescribeSobjectResult res = Sales_Out__c.SobjectType.getDescribe();
            if(urlVal.contains('distributor'))        
                url='/distributor/'+so.Id;
            else
                url= '/'+so.Id;        
        }
        return null;
    }
    
    static testMethod void m1(){
        
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'mdttest2@mdttest.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 )
        {
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
        insert acc;
        
        RecordType rts = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = acc.Id;
        c.Connected_As__c = 'Administrator';
        c.RecordTypeId = rts.Id;
        c.Department_picklist__c = 'Other';
        c.Other_Department__c = 'Accounting';
        c.Type__c='DIS Contact';
        insert c;
        
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
        Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;
        Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        //Product_SKU__c;
        insert ps;
        
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
        insert cp;
        Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
        insert cp1;
        
        Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        
        ApexPages.CurrentPage().getParameters().put('id',so1.id);
         ApexPages.CurrentPage().getHeaders().put('host','distributor');
        ApexPages.StandardController sc = new ApexPages.StandardController(new Sales_Out__c(Id=so1.Id));
        VerifyDocumentController vdc = new VerifyDocumentController(sc);
        vdc.okay();
        }
    }
    
    static testMethod void m2(){
        
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'mdttest3@mdttest.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 )
        {
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
        insert acc;
        
        RecordType rts = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = acc.Id;
        c.Connected_As__c = 'Administrator';
        c.RecordTypeId = rts.Id;
        c.Department_picklist__c = 'Other';
        c.Other_Department__c = 'Accounting';
        c.Type__c='Sales Manager';
        insert c;
        
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
        Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;
        Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        //Product_SKU__c;
        insert ps;
        
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
        insert cp;
        Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
        insert cp1;
        
        Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        
        ApexPages.CurrentPage().getParameters().put('id',so1.id);
         ApexPages.CurrentPage().getHeaders().put('host','distributor');
        ApexPages.StandardController sc = new ApexPages.StandardController(new Sales_Out__c(Id=so1.Id));
        VerifyDocumentController vdc = new VerifyDocumentController(sc);
        vdc.okay();
        }
    }
    static testMethod void m3(){
        
        RecordType rt=[Select Id from RecordType where DeveloperName='ASIA_Distributor' Limit 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'CRM Admin Support'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'mdttest4@mdttest.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 )
        {
        Account acc=new Account(Name='Test Asia Distributor',RecordTypeId=rt.Id,Status__c='Active',OwnerId = portalAccountOwner1.Id);
        insert acc;
        
        RecordType rts = [Select Id From RecordType Where SobjectType = 'Contact' And Name = 'Connected Clinician' Limit 1];
        Contact c = new Contact();
        c.LastName = 'Tester';
        c.AccountId = acc.Id;
        c.Connected_As__c = 'Administrator';
        c.RecordTypeId = rts.Id;
        c.Department_picklist__c = 'Other';
        c.Other_Department__c = 'Accounting';
        c.Type__c='Sales Rep';
        insert c;
        
        
        Date dt=Date.today().addMonths(-1);
        String CurrentCyclePeriodMonth=String.valueOf(dt.month());
        
        if(dt.month()<10)
            CurrentCyclePeriodMonth='0'+CurrentCyclePeriodMonth;
        String CurrentCyclePeriodYear=  String.valueOf(dt.year());
        
        Date PrevDt=Date.today().addMonths(-2);
        String PrevCyclePeriodMonth=String.valueOf(PrevDt.month());
        
        if(PrevDt.month()<10)
            PrevCyclePeriodMonth='0'+PrevCyclePeriodMonth;
        String PrevCyclePeriodYear= String.valueOf(PrevDt.year());
        
        Date AdvDt=Date.today();
        String AdvCyclePeriodMonth=String.valueOf(AdvDt.month());
        
        if(AdvDt.month()<10)
            AdvCyclePeriodMonth='0'+AdvCyclePeriodMonth;
        String AdvCyclePeriodYear=  String.valueOf(AdvDt.year());
        
        Cycle_Period_Reference__c PrevCpRef=new Cycle_Period_Reference__c(Name=PrevCyclePeriodMonth+'-'+PrevCyclePeriodYear,
                                            Month__c=PrevCyclePeriodMonth,Year__c=PrevCyclePeriodYear);
        insert PrevCpRef;
        Cycle_Period_Reference__c CurrentCpRef=new Cycle_Period_Reference__c(Name=CurrentCyclePeriodMonth+'-'+CurrentCyclePeriodYear,
                                            Month__c=CurrentCyclePeriodMonth,Year__c=CurrentCyclePeriodYear);
        insert CurrentCpRef;
        
        Cycle_Period_Reference__c AdvCpRef=new Cycle_Period_Reference__c(Name=AdvCyclePeriodMonth+'-'+AdvCyclePeriodYear,
                                            Month__c=AdvCyclePeriodMonth,Year__c=AdvCyclePeriodYear);
        insert AdvCpRef;
        Product_SKU__c ps=new Product_SKU__c(Name='test',SKU__c='1111',Product_Sku_External_ID__c='1111',UOM__c='EA');
        //Product_SKU__c;
        insert ps;
        
        Cycle_Period__c cp=new Cycle_Period__c(Cycle_Period_Reference__c=PrevCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false, status__c='Open');
        insert cp;
        Cycle_Period__c cp1=new Cycle_Period__c(Cycle_Period_Reference__c=CurrentCpRef.Id,Distributor_Name__c=acc.Id,Sales_Out_Validated__c=false,status__c='Open');
        insert cp1;
        
        Sales_Out__c so=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='111',Sell_To__c='123',Sell_From__c='123',UOM__c='EP',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23',Run_Validations__c=true);
        insert so;
        Sales_Out__c so1=new Sales_Out__c(Cycle_Period__c=cp.Id,Product_Code__c='1111',Sell_To__c='133',Sell_From__c='133',UOM__c='EA',Quantity_Text__c='23',Document_Date_Text__c='2013-09-11',Selling_Price_Text__c='23');
        insert so1;
        
        ApexPages.CurrentPage().getParameters().put('id',so1.id);
         ApexPages.CurrentPage().getHeaders().put('host','distributor');
        ApexPages.StandardController sc = new ApexPages.StandardController(new Sales_Out__c(Id=so1.Id));
        VerifyDocumentController vdc = new VerifyDocumentController(sc);
        vdc.okay();
        }
    }
}