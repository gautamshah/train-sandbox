@istest
private class CustomOpptyApprovalsLaunControllerTest extends CustomApprovalsConstants
{

    static testmethod void method_One()
    {

        User usr = cpqUser_TestSetup.getSysAdminUser();
        if (usr == null) {
            usr = cpqUser_c.CurrentUser;
        }

        cpqOpportunity_TestSetup.buildScenario_CPQ_Opportunity();
        Account acc = cpq_TestSetup.testAccounts.get(0);
        Contact ct = cpq_TestSetup.testContacts.get(0);
        Opportunity opp = cpq_TestSetup.testOpportunities.get(0);

        Apttus__APTS_Agreement__c agmt = cpqAgreement_TestSetup.generateAgreement(acc,ct,opp,
            cpqRecordType_TestSetup.getRecordType('Apttus__APTS_Agreement__c').DeveloperName);
        agmt.Approval_Preview_Status__c='Pending';
        insert agmt;

        sObject record = agmt;
        Test.startTest();
        CustomOpptyApprovalsLaunchController ctrl = new CustomOpptyApprovalsLaunchController(new ApexPages.StandardController(record));
        PageReference pgrf = ctrl.doLaunchApprovals();
        PageReference pgref = ctrl.doReturn();
        Id objId = ctrl.getCtxObjectId();
        Boolean errorMsg = ctrl.hasErrors;
        Test.stopTest();

    }

    static testmethod void method_One_Scrub_PO()
    {

        User usr = cpqUser_TestSetup.getSysAdminUser();
        if (usr == null) {
            usr = cpqUser_c.CurrentUser;
        }

        cpqOpportunity_TestSetup.buildScenario_CPQ_Opportunity();
        Account acc = cpq_TestSetup.testAccounts.get(0);
        Contact ct = cpq_TestSetup.testContacts.get(0);
        Opportunity opp = cpq_TestSetup.testOpportunities.get(0);

        // Creating two agreements to bypass CustomOpptyApprovalsTrigger
        cpq_TestSetup.testAgreements.add(cpqAgreement_TestSetup.generateAgreement(acc,ct,opp,
            CPQ_AgreementProcesses.SMART_CART_RT));
        cpq_TestSetup.testAgreements.add(cpqAgreement_TestSetup.generateAgreement(acc,ct,opp,
            CPQ_AgreementProcesses.SMART_CART_RT));
        cpq_TestSetup.testAgreements.get(0).Approval_Preview_Status__c = 'Pending';
        cpq_TestSetup.testAgreements.get(1).Approval_Preview_Status__c = 'Pending';
        insert cpq_TestSetup.testAgreements;
        Apttus__APTS_Agreement__c agmt = cpq_TestSetup.testAgreements.get(0);

        sObject record = agmt;
        Test.startTest();
        CustomOpptyApprovalsLaunchController ctrl = new CustomOpptyApprovalsLaunchController(new ApexPages.StandardController(record));
        PageReference pgrf = ctrl.doLaunchApprovals();
        PageReference pgref = ctrl.doReturn();
        Id objId = ctrl.getCtxObjectId();
        Boolean errorMsg = ctrl.hasErrors;
        Test.stopTest();

    }

}