@isTest

private class TestIndividualDevelopmentPlanApexShare {
    
    static testMethod void insertNewIdp() {
    	Individual_Development_Plan__c idp = new Individual_Development_Plan__c();
            idp.Manager_Mentor__c = '005U0000000epU5';
            idp.Period__c = 'Q1 2015';
            idp.Customer_Territory_Acumen__c = 'L1=FOUNDATIONAL';
            idp.Building_a_Business_Case__c = 'L1=FOUNDATIONAL';
            idp.Healthcare_Economics__c = 'L1=FOUNDATIONAL';
            idp.Setting_Sales_Strategy__c = 'L1=FOUNDATIONAL';
            idp.Treatment_Procedural_Knowledge__c = 'L1=FOUNDATIONAL';
            idp.Building_Relationships__c = 'L1=FOUNDATIONAL';
            idp.Product_Knowledge__c = 'L1=FOUNDATIONAL';
            idp.Creating_and_Closing_Opportunities__c = 'L1=FOUNDATIONAL';
            idp.Corporate_GBU_Market_Strategy__c = 'L1=FOUNDATIONAL';
            idp.Protecting_Accounts__c = 'L1=FOUNDATIONAL';
            idp.Clinical_Information__c = 'L1=FOUNDATIONAL';
            idp.Passion_Industry_Enthusiasm__c = 'L1=FOUNDATIONAL';
            idp.Partnering_Building_Relationships__c = 'L1=FOUNDATIONAL';
            idp.Deliberate_Patience_Diligence__c = 'L1=FOUNDATIONAL';
            idp.Understanding_Business_Context__c = 'L1=FOUNDATIONAL';
            idp.Entrepreneurial_Mentality__c = 'L1=FOUNDATIONAL';
            idp.Articulating_Value__c ='L1=FOUNDATIONAL';
            idp.Exploring_Expanding_Opportunities__c = 'L1=FOUNDATIONAL';
            idp.Shows_Discipline__c = 'L1=FOUNDATIONAL';
            insert idp;
        
        }
   
    static testMethod void updateIDP() {
        Individual_Development_Plan__c idp = new Individual_Development_Plan__c();
            idp.Manager_Mentor__c = '005U0000000epjy';
            idp.Period__c = 'Q1 2015';
            idp.Customer_Territory_Acumen__c = 'L1=FOUNDATIONAL';
            idp.Building_a_Business_Case__c = 'L1=FOUNDATIONAL';
            idp.Healthcare_Economics__c = 'L1=FOUNDATIONAL';
            idp.Setting_Sales_Strategy__c = 'L1=FOUNDATIONAL';
            idp.Treatment_Procedural_Knowledge__c = 'L1=FOUNDATIONAL';
            idp.Building_Relationships__c = 'L1=FOUNDATIONAL';
            idp.Product_Knowledge__c = 'L1=FOUNDATIONAL';
            idp.Creating_and_Closing_Opportunities__c = 'L1=FOUNDATIONAL';
            idp.Corporate_GBU_Market_Strategy__c = 'L1=FOUNDATIONAL';
            idp.Protecting_Accounts__c = 'L1=FOUNDATIONAL';
            idp.Clinical_Information__c = 'L1=FOUNDATIONAL';
            idp.Passion_Industry_Enthusiasm__c = 'L1=FOUNDATIONAL';
            idp.Partnering_Building_Relationships__c = 'L1=FOUNDATIONAL';
            idp.Deliberate_Patience_Diligence__c = 'L1=FOUNDATIONAL';
            idp.Understanding_Business_Context__c = 'L1=FOUNDATIONAL';
            idp.Entrepreneurial_Mentality__c = 'L1=FOUNDATIONAL';
            idp.Articulating_Value__c ='L1=FOUNDATIONAL';
            idp.Exploring_Expanding_Opportunities__c = 'L1=FOUNDATIONAL';
            idp.Shows_Discipline__c = 'L1=FOUNDATIONAL';   
            insert idp;

        	idp.Manager_Mentor__c = '005U0000000fUaq';
        	update idp;
    }


}