/****************************************************************************************
 * Name    : MovementValidationTestClass
 * Author  : Fenny Saputra
 * Date    : 26/Jun/2014  
 * Apex Involved: MovementValidation.trigger
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 * 
 *****************************************************************************************/
 
@isTest
public class MovementValidationTestClass{
    static testMethod void MovValidationTestClass(){
        Id pId = [select Id from Profile where name = 'CRM Admin Support'].Id;
         
        List<User> userList = new List<User>();
        
        User u1 = new User(
            LastName = 'Rep1',
            Business_Unit__c = 'The Unit',
            Franchise__c = 'Burger King',
            email = 'testFSrep1@covidien.com',
            alias = 'trep1',
            username = 'testFSrep1@covidien.com',
            communityNickName = 'testFSrep1@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='SGD',
            EmailEncodingKey='ISO-8859-1',
            TimeZoneSidKey='America/New_York',
            LanguageLocaleKey='en_US',
            LocaleSidKey ='en_US',
            Country = 'SG');
         userList.add(u1);
         
         User u2 = new User(
            LastName = 'Rep2',
            Business_Unit__c = 'The Unit',
            Franchise__c = 'Burger King',
            email = 'testFSrep2@covidien.com',
            alias = 'trep2',
            username = 'testFSrep2@covidien.com',
            communityNickName = 'testFSrep2@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='SGD',
            EmailEncodingKey='ISO-8859-1',
            TimeZoneSidKey='America/New_York',
            LanguageLocaleKey='en_US',
            LocaleSidKey ='en_US',
            Country = 'SG');
         userList.add(u2);
         
         insert userList;
         
         Product_SKU__c sku;
         try{
             sku = [SELECT id FROM Product_SKU__c WHERE Country__c ='CN' LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( sku == null){
                 sku = new Product_SKU__c(Name='Test SKU', Country__c='CN');
                 insert sku;
             }
         }
         
         RecordType rtAccount = [SELECT id FROM RecordType WHERE Name = 'ASIA-Healthcare Facility' AND SobjectType='Account'];
         
         Account a;
         try{
             a = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a == null){
                 a = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a;
             }
         } 
         
         Demo_Product__c dp;
         try{
             dp = [SELECT id FROM Demo_Product__c LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( dp == null){
                 dp = new Demo_Product__c(
                     Name='New Request', 
                     Asset_Name__c=sku.Id,
                     Approval_Status__c='Approved',
                     OwnerId=u1.Id);
                 insert dp;
             }
         }
         
         Movement__c mov1 = new Movement__c(
             Demo_Product_Name__c=dp.Id, 
             Hospital__c=a.Id,
             Start_Date__c=system.today(),
             End_Date__c=system.today()+90,
             Approval_Status__c='Approved');
         insert mov1;
         
         Movement__c mov2 = new Movement__c(
             Demo_Product_Name__c=dp.Id, 
             Hospital__c=a.Id,
             Start_Date__c=system.today(),
             End_Date__c=system.today()+90,
             Approval_Status__c='Approved');
         try{
             insert mov2;
         }catch(Exception e){
             Boolean expectedExceptionThrown =  e.getMessage().contains('Demo Product is currently being loaned at another place') ? true : false;
             System.AssertEquals(expectedExceptionThrown, true);
         }
         
         Movement__c mov3 = new Movement__c(
             Demo_Product_Name__c=dp.Id, 
             Hospital__c=a.Id,
             Start_Date__c=system.today()+100,
             End_Date__c=system.today()+190,
             Reloan__c = true,
             Approval_Status__c='Approved');
         try{
             insert mov3;
         }catch(Exception e){
             Boolean expectedExceptionThrown =  e.getMessage().contains('Demo Product cannot be loaned more than one time') ? true : false;
             System.AssertEquals(expectedExceptionThrown, true);
         }
         
         Movement__c mov4 = new Movement__c(
             Demo_Product_Name__c=dp.Id, 
             Hospital__c=a.Id,
             Start_Date__c=system.today()+200,
             End_Date__c=system.today()+290,
             Reloan__c = true,
             Approval_Status__c='Approved');
         try{
             insert mov4;
         }catch(Exception e){
             Boolean expectedExceptionThrown =  e.getMessage().contains('Demo Product cannot be loaned more than one time') ? true : false;
             System.AssertEquals(expectedExceptionThrown, true);
         }
         
         
         /*
         Below is added by Chen Li for Korean MovementValidation update
         */
         
                 
         List<User> userList2 = new List<User>();
         User u3 = new User(
            LastName = 'Rep3',
            Business_Unit__c = 'The Unit',
            Franchise__c = 'Boston',
            email = 'testFSrep3@covidien.com',
            alias = 'trep3',
            username = 'testFSrep3@covidien.com',
            communityNickName = 'testFSrep3@covidien.com',
            ProfileId = pId,
            CurrencyIsoCode='USD',
            EmailEncodingKey='ISO-8859-1',
            TimeZoneSidKey='America/New_York',
            LanguageLocaleKey='en_US',
            LocaleSidKey ='en_US',
            Country = 'KR');
     
        userList2.add(u3);
         
         
           insert userList2;
         
         Product_SKU__c sku2;
         try{
             sku2 = [SELECT id FROM Product_SKU__c WHERE Country__c ='KR' LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( sku2 == null){
                 sku2 = new Product_SKU__c(Name='Test sku2', Country__c='KR');
                 insert sku2;
             }
         }
         
         
         Account a2;
         try{
             a2 = [SELECT id FROM Account WHERE RecordTypeId=:rtAccount.id LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( a2 == null){
                 a2 = new Account(Name='Test Account', RecordTypeId=rtAccount.id);
                 insert a2;
             }
         } 
         
         Demo_Product__c dpkr;
         try{
             dpkr = [SELECT id FROM Demo_Product__c where Country__c ='KR' LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( dpkr == null){
                 dpkr = new Demo_Product__c(
                     Name='New Request', 
                     Asset_Name__c=sku2.Id,
                     Approval_Status__c='Approved',
                     Country__c ='KR',
                     OwnerId=u3.Id);
                 insert dpkr;
             }
         }
         
         Movement__c mov99 = new Movement__c(
             Demo_Product_Name__c=dpkr.Id, 
             Hospital__c=a2.Id,
             Start_Date__c=system.today()+200,
             End_Date__c=system.today()+220,
             Approval_Status__c='Approved');
        
         try{
             insert mov99;
         }catch(Exception e){
             Boolean expectedExceptionThrown =  true;
             System.AssertEquals(expectedExceptionThrown, true);
         }
          
         
/*         
        Movement__c mov1;
        try{
             mov1 = [SELECT id FROM Movement__c LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( mov1 == null){
                 mov1 = new Movement__c(
                     Demo_Product_Name__c=dp.Id, 
                     Hospital__c=a.Id,
                     Start_Date__c=system.today(),
                     End_Date__c=system.today()+90,
                     Approval_Status__c='Approved');
                 insert mov1;
             }
         }
         
        Movement__c mov2;
        try{
             mov2 = [SELECT id FROM Movement__c LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( mov2 == null){
                 mov2 = new Movement__c(
                     Demo_Product_Name__c=dp.Id, 
                     Hospital__c=a.Id,
                     Start_Date__c=system.today(),
                     End_Date__c=system.today()+90,
                     Approval_Status__c='Approved');
                 insert mov2;
                 Boolean expectedExceptionThrown =  e.getMessage().contains('Demo Product is currently being loaned at another place') ? true : false;
                 System.AssertEquals(expectedExceptionThrown, true);
             }
         }
         
        Movement__c mov3;
        try{
             mov3 = [SELECT id FROM Movement__c LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( mov3 == null){
                 mov3 = new Movement__c(
                     Demo_Product_Name__c=dp.Id, 
                     Hospital__c=a.Id,
                     Start_Date__c=system.today()+100,
                     End_Date__c=system.today()+190,
                     Reloan__c = true,
                     Approval_Status__c='Approved');
                 insert mov3;
             }
         }
         
        Movement__c mov4;
        try{
             mov4 = [SELECT id FROM Movement__c LIMIT 1];
         }catch (Exception e){
             System.debug('MovValidationTestClass Test Class: Getting Asia Account Exception: ' + e);
             if( mov4 == null){
                 mov4 = new Movement__c(
                     Demo_Product_Name__c=dp.Id, 
                     Hospital__c=a.Id,
                     Start_Date__c=system.today()+200,
                     End_Date__c=system.today()+290,
                     Reloan__c = true,
                     Approval_Status__c='Approved');
                 insert mov4;
                 Boolean expectedExceptionThrown =  e.getMessage().contains('Demo Product cannot be loaned more than one time') ? true : false;
                 System.AssertEquals(expectedExceptionThrown, true);
             }
         }
*/        
    }
}