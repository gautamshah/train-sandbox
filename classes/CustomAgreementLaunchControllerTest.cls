@isTest
private class CustomAgreementLaunchControllerTest {

	@testSetup static void createData()
	{
		Product2 testProduct = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.RMS, 'DUMMY', 1);
		insert testProduct;
		
		Apttus_Config2__PriceListItem__c testPriceListItem = [SELECT Id FROM Apttus_Config2__PriceListItem__c WHERE Apttus_Config2__ProductId__c = :testProduct.Id];

		// Configure Account Data
		Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US', Account_External_ID__c = 'TEST_EXTERNAL');
		insert acct;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
		insert c;

		// Configure Agreement Data

		Opportunity testOpp = new Opportunity(Name = 'Test Opportunity', StageName = 'Early', CloseDate = System.today(), AccountId = acct.Id);
		insert testOpp;

		RecordType scrubRT = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,'Scrub_PO');

		Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(Name = 'Test Agreement', Apttus__Related_Opportunity__c = testOpp.Id, RecordTypeId = scrubRT.Id, Scrub_PO_Type__c = 'Non-Transfer', Primary_Contact2__c = c.Id, SSG_Product_Usage__c = 'Clinically by the hospital', Apttus_Approval__Approval_Status__c = 'Approved', Apttus__Status__c = 'Requested');
		insert testAgreement;

		// Configure Cart Data
		Apttus_Config2__ProductConfiguration__c testConfig =
			new Apttus_Config2__ProductConfiguration__c(
				Apttus_CMConfig__AgreementId__c = testAgreement.ID,
				Apttus_Config2__PriceListId__c = cpqPriceList_c.RMS.Id,
				Apttus_Config2__Status__c = 'Finalized',
				Apttus_Config2__VersionNumber__c = 1);
		insert testConfig;

		Apttus_Config2__LineItem__c li =
			new Apttus_Config2__LineItem__c(
				Apttus_Config2__IsPrimaryLine__c = true,
				Apttus_Config2__ConfigurationId__c = testConfig.ID,
				Apttus_Config2__ProductId__c = testProduct.ID,
				Apttus_Config2__PriceListItemId__c = testPriceListItem.Id,
				Apttus_Config2__ItemSequence__c = 1, Apttus_Config2__LineNumber__c = 1, Apttus_Config2__PrimaryLineNumber__c = 1, Apttus_Config2__Quantity__c = 5);
		insert li;

		Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Apttus__AgreementId__c = testAgreement.Id, Apttus_CMConfig__DerivedFromId__c = li.Id);
		insert ali;

	}

	@isTest static void test_doLaunchPreview() {
		Apttus__APTS_Agreement__c app = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
		CustomAgreementLaunchController ctrl = new CustomAgreementLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchPreview();
	}

	@isTest static void test_doLaunchGenerate() {
		Apttus__APTS_Agreement__c app = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
		CustomAgreementLaunchController ctrl = new CustomAgreementLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchGenerate();
	}

	@isTest static void test_doLaunchRegenerate(){
		Apttus__APTS_Agreement__c app = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
		CustomAgreementLaunchController ctrl = new CustomAgreementLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchRegenerate();
	}

	@isTest static void test_doLaunchActivate(){
		Apttus__APTS_Agreement__c app = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
		CustomAgreementLaunchController ctrl = new CustomAgreementLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchActivate();
	}

	@isTest static void test_doLaunchSendForReview(){
		Apttus__APTS_Agreement__c app = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
		CustomAgreementLaunchController ctrl = new CustomAgreementLaunchController(new ApexPages.StandardController(app));
		PageReference pgref = ctrl.doLaunchSendForReview();
	}

}