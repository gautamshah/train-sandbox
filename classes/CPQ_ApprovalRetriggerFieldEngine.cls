/**
CPQ_ApprovalRetriggerFieldEngine
Handle approval retrigger fields on proposal, configuration, agreeement.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-01-08      Yuli Fintescu       Created
2017-02-07		Isaac Lewis			Replaced recordType maps with cpqDeal_RecordType_c reference
									Made CPQ_Approval_Retrigger_Field__c query static to reduce query usage
===============================================================================
*/
public with sharing class CPQ_ApprovalRetriggerFieldEngine {

	private static List<CPQ_Approval_Retrigger_Field__c> fields {
		get {
			if (fields == null) {
				fields = [Select ID, SObject__c, Name, Related_SObject__c, Field_API_Name__c From CPQ_Approval_Retrigger_Field__c];
			}
			return fields;
		}
		private set;
	}

	private Map<String, Map<String, List<CPQ_Approval_Retrigger_Field__c>>> organized;
	// private Map<String,String> mapProposalRType {
	// 	get {
	// 		if (mapProposalRType == null) {
	// 			mapProposalRType = New Map<String,String>();
	//
	// 			for(RecordType R: [Select Id, Name, DeveloperName From RecordType Where SobjectType = 'Apttus_Proposal__Proposal__c']) {
	// 				mapProposalRType.Put(R.Id, R.DeveloperName);
	// 			}
	// 		}
	//
	// 	return mapProposalRType;
	// 	}
	// 	set;
	// }
	//
	// private Map<String,String> mapAgreementRType {
	// 	get {
	// 		if (mapAgreementRType == null) {
	// 			mapAgreementRType = New Map<String,String>();
	//
	// 			for(RecordType R: [Select Id, Name, DeveloperName From RecordType Where SobjectType = 'Apttus__APTS_Agreement__c']) {
	// 				mapAgreementRType.Put(R.Id, R.DeveloperName);
	// 			}
	// 		}
	//
	// 		return mapAgreementRType;
	// 	}
	// 	set;
	// }

	public CPQ_ApprovalRetriggerFieldEngine() {
		organized = new Map<String, Map<String, List<CPQ_Approval_Retrigger_Field__c>>>();
		//collect and organize CPQ Retrigger Field records
		OrganizeApprovalRetriggerFields(organized);
	}

	//Organize CPQ_Approval_Retrigger_Field__c records into groups: SObject->RelatedSObject->records.
	private void OrganizeApprovalRetriggerFields(Map<String, Map<String, List<CPQ_Approval_Retrigger_Field__c>>> organized) {
		for (CPQ_Approval_Retrigger_Field__c f : fields) {

			Map<String, List<CPQ_Approval_Retrigger_Field__c>> relToRetrigger;
			if (organized.containsKey(f.SObject__c)) {
				relToRetrigger = organized.get(f.SObject__c);
			} else {
				relToRetrigger = new Map<String, List<CPQ_Approval_Retrigger_Field__c>>();
				organized.put(f.SObject__c, relToRetrigger);
			}

			String related = f.Related_SObject__c == null ? '*' : f.Related_SObject__c;

			List<CPQ_Approval_Retrigger_Field__c> flds;
			if (relToRetrigger.containsKey(related)) {
				flds = relToRetrigger.get(related);
			} else {
				flds = new List<CPQ_Approval_Retrigger_Field__c>();
				relToRetrigger.put(related, flds);
			}
			flds.add(f);
		}

		System.Debug('*** organized ' + organized);
	}

	public List<CPQ_Approval_Retrigger_Field__c> getFields(String SO, String RelSO) {
		String related = String.isEmpty(RelSO) ? '*' : RelSO;
		if (organized.containsKey(SO) && organized.get(SO).containsKey(related))
			return organized.get(SO).get(related);
		return null;
	}

	public Boolean anyFieldChanged(List<CPQ_Approval_Retrigger_Field__c> fieldSet, SObject newRecord, SObject oldRecord) {
		Map<Id,RecordType> mapRType = cpqDeal_RecordType_c.getRecordTypesBySObjectAndId().get(newRecord.getSObjectType());
		// Map<String,String> mapRType;
		// if (newRecord instanceof Apttus_Proposal__Proposal__c)
		// 	mapRType = mapProposalRType;
		// else if (newRecord instanceof Apttus__APTS_Agreement__c)
		// 	mapRType = mapAgreementRType;

		if (fieldSet != null) {
			for (CPQ_Approval_Retrigger_Field__c f : fieldSet) {
				Object oldValue = oldRecord.get(f.Field_API_Name__c);
				Object newValue = newRecord.get(f.Field_API_Name__c);

				if (newValue != oldValue &&
						(f.Field_API_Name__c != 'RecordTypeId' ||
						mapRType == null ||
						// (mapRType.get(String.valueOf(newValue)) != mapRType.get(String.valueOf(oldValue)) + '_Locked'))) {
						(mapRType.get(String.valueOf(newValue)).DeveloperName != mapRType.get(String.valueOf(oldValue)).DeveloperName + '_Locked'))) {
					return true;
				}
			}
		}

		return false;
	}
}