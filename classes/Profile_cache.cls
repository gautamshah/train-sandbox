public class Profile_cache extends sObject_cache
{
	public static Profile_cache get() { return cache; }
	
	private static Profile_cache cache
	{
		get
		{
			if (cache == null)
				cache = new Profile_cache();
				
			return cache;
		}
		
		private set;
	}
	
	private Profile_cache()
	{
		super(Profile.sObjectType, Profile.field.Id);
        this.addIndex(Profile.field.Name);
	}
}