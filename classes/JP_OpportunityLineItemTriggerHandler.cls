public with sharing class JP_OpportunityLineItemTriggerHandler{
     /****************************************************************************************
     * Name    : JP_OpportunityLineItemTriggerHandler 
     * Author  : Hiroko Kambayashi
     * Date    : 09/18/2012 
     * Purpose : Trigger Handler to insert OpportunityLineItemSchedule record by Opportunity
     *           
     * Dependencies: OpportunityLineItem Object
     *             
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 12/11/2015  Syu    Iken          Correcting the mtehod "onAfterInsert"
     * 1/30/2016   Hayashi Kan          Comment out execution of "business.inputTargetProductCategoryToOpportunity" (line 100)
     *****************************************************************************************/
    private Boolean isExecuting = false;
    private Integer batchSize = 0;
    
    /**
     * OpportunityLineItemTrigger Constructor
     * @param Boolean isExecute
     * @param Integer size
     */
    public JP_OpportunityLineItemTriggerHandler(Boolean isExecute, Integer size){
        this.isExecuting = isExecute;
        this.batchSize = size;
    }
    
    /**
     * Handling of OpportunityLineItem BeforeInsert Trigger
     * @param List<OpportunityLineItem> triggerNewList
     * @return none
     */
    public void onBeforeInsert(List<OpportunityLineItem> triggerNewList){
        //Create Business Logic  instance
        JP_OpportunityLineItemBusiness business = new JP_OpportunityLineItemBusiness();
        
        List<OpportunityLineItem> inputOriginalList = new List<OpportunityLineItem>();
        for(OpportunityLineItem opl : triggerNewList){
            if(opl.JP_Opportunity_RecordType_Name__c != null && opl.JP_Opportunity_RecordType_Name__c.startsWith(System.Label.JP_Opportunity_RecordTypeName_Prefix)){
                if(opl.JP_Product_CanUseRevenueSchedule__c == 'TRUE' && opl.JP_Product_ScheduleSetting__c == 'FALSE'){
                    inputOriginalList.add(opl);
                }
            }
        }
        //Copy of TotalPrice
        if(!inputOriginalList.isEmpty()){
            business.inputOriginalTotalPrice(inputOriginalList);
        }
    }
    /**
     * Handling of OpportunityLineItem AfterInsert Trigger
     * @param Map<Id, OpportunityLineItem> triggerNewMap
     * @return none
     */
    public void onAfterInsert(Map<Id, OpportunityLineItem> triggerNewMap){
        //Create Business Logic  instance
        JP_OpportunityLineItemBusiness business = new JP_OpportunityLineItemBusiness();
        
        //The Map for Revenue auto Schedule by Opportunity info
        Map<Id, OpportunityLineItem> divideNewMap = new Map<Id, OpportunityLineItem>();
        //The Map for input Target Product Category Id to Opportunity
        Map<Id, OpportunityLineItem> inputNewMap = new Map<Id, OpportunityLineItem>();
        
        //2015.11.06 ecs i-shu add start
        /**CVJ_SELLOUT_DEV-44 of fixing**/
        for(OpportunityLineItem opl : triggerNewMap.values()){
            if(opl.JP_Opportunity_RecordType_Name__c != null && opl.JP_Opportunity_RecordType_Name__c.startsWith(System.Label.JP_Opportunity_RecordTypeName_Prefix)){
                if(opl.JP_Product_CanUseRevenueSchedule__c == 'TRUE' || opl.Product_CanUseQuantitySchedule__c == TRUE){
                    divideNewMap.put(opl.Id, opl);
                }
                if(opl.JP_Product_TargetProductCategory__c != null && opl.JP_Opportunity_TargetProductCategory__c == null){
                    inputNewMap.put(opl.OpportunityId, opl);
                }
            }
        }
        //2015.11.06 ecs i-shu mod start
        /*
        for(OpportunityLineItem opl : triggerNewMap.values()){
            if(opl.JP_Opportunity_RecordType_Name__c != null && opl.JP_Opportunity_RecordType_Name__c.startsWith(System.Label.JP_Opportunity_RecordTypeName_Prefix)){
                if(opl.JP_Product_CanUseRevenueSchedule__c == 'TRUE'){
                    divideNewMap.put(opl.Id, opl);
                }
                if(opl.JP_Product_TargetProductCategory__c != null && opl.JP_Opportunity_TargetProductCategory__c == null){
                    inputNewMap.put(opl.OpportunityId, opl);
                }
            }
        }*/
        //2015.11.06 ecs i-shu mod end
        //2015.11.06 ecs i-shu add end
        //Execution of insert Revenue OpportunityLineItemSchedule
        if(!divideNewMap.isEmpty()){
            business.divideProfitScheduleByOpportunity(divideNewMap);
        }
        //Execution of update Target Product Category info to Opportunity record
        if(!inputNewMap.isEmpty()){
            //business.inputTargetProductCategoryToOpportunity(inputNewMap);
        }
    }
}