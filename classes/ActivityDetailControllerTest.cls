@isTest(SeeAllData=true)
public class ActivityDetailControllerTest {

    static testmethod void testActivityDetailController(){
        Test.startTest();
        String rtId = Utilities.recordTypeMap_DeveloperName_Id.get('RMS_Events_EU_ANZ');
        Event e = [Select Id, Activity_Detail__c, RecordTypeId  From Event Where RecordTypeId = :rtId And IsPrivate = false And IsChild = false And IsGroupEvent = false And IsArchived = false And IsRecurrence = false And GroupEventType != '2' Limit 1];
        ActivityDetailController adc = new ActivityDetailController(new ApexPages.StandardController(e));
        adc.doSave();
        Test.stopTest();
    }
    
    static testmethod void testActivityDetailController1()
    {
        User userTest = new User();
    	userTest.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id;
    	userTest.LastName = 'lasst';
   		userTest.Email = 'pudser000@amamama.com';
     	userTest.Username = 'pudser000@amamama.com' + System.currentTimeMillis();
     	userTest.CompanyName = 'TEST';
     	userTest.Title = 'title';
     	userTest.Alias = 'aliass';
     	userTest.TimeZoneSidKey = 'America/Los_Angeles';
     	userTest.EmailEncodingKey = 'UTF-8';
     	userTest.LanguageLocaleKey = 'en_US';
     	userTest.LocaleSidKey = 'en_US';
       	insert userTest;
		
        String rtId = Utilities.recordTypeMap_DeveloperName_Id.get('RMS_Events_EU_ANZ');
        
        Activity_Detail__c actDetail = new Activity_Detail__c();
        actDetail.OwnerId = userTest.Id;
        insert actDetail;
            
		Event event = new Event();
		event.RecordTypeId = rtId;
		event.Subject = '-';
		event.OwnerId = userTest.Id;
        event.Activity_Detail__c = actDetail.Id;
        event.DurationInMinutes = 60;
        event.ActivityDateTime = system.Datetime.now();
        insert event;
        
        Test.startTest();
            Event e = [SELECT e.Id, e.Activity_Detail__c, e.RecordTypeId  
                       FROM Event e
                       WHERE e.Activity_Detail__c = :event.Activity_Detail__c AND RecordTypeId = :rtId And IsPrivate = false And IsChild = false And IsGroupEvent = false And IsArchived = false And IsRecurrence = false And GroupEventType != '2'
                       LIMIT 1];
            
            ActivityDetailController adc = new ActivityDetailController(new ApexPages.StandardController(e));
            adc.doSave();
        Test.stopTest();
    }
}