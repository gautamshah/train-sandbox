public class cpqOfferDevelopmentAssignment_g extends sObject_g
{
	public cpqOfferDevelopmentAssignment_g()
	{
		super(cpqOfferDevelopmentAssignment_cache.get());
	}
	
	////
	//// cast
	////
	public static Map<Id, CPQ_SSG_OD_Assignments__c> cast(Map<Id, sObject> sobjs)
	{
		try
		{
			return new Map<Id, CPQ_SSG_OD_Assignments__c>((List<CPQ_SSG_OD_Assignments__c>)sobjs.values());
		}
		catch(Exception e)
		{
			return new Map<Id, CPQ_SSG_OD_Assignments__c>();
		}
	}

	public static Map<object, Map<Id, CPQ_SSG_OD_Assignments__c>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, CPQ_SSG_OD_Assignments__c>> target = new Map<object, Map<Id, CPQ_SSG_OD_Assignments__c>>();
		for(object key : source.keySet())
			target.put(key, cast(source.get(key)));
			
		return target;
	}

	
	////
	//// fetch
	////
	public Map<Id, CPQ_SSG_OD_Assignments__c> fetch()
	{
		return cast(this.cache.fetch());
	}
	
	public CPQ_SSG_OD_Assignments__c fetch(Id id)
	{
		return (CPQ_SSG_OD_Assignments__c)this.cache.fetch(id);
	}
	
	public Map<Id, CPQ_SSG_OD_Assignments__c> fetch(sObjectField field, object value)
	{
		return cast(this.cache.fetch(field, value));
	}
	
	////
	//// fetch sets
	////
	public Map<Id, CPQ_SSG_OD_Assignments__c> fetch(Set<Id> ids)
	{
		return cast(this.cache.fetch(ids));
	}
	
	public Map<object, Map<Id, CPQ_SSG_OD_Assignments__c>> fetch(sObjectField field)
	{
		return cast(this.cache.fetch(field));
	}
}