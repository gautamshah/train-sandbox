@isTest(SeeAllData=true)
public class ActivityMigratorTest
{
    static testmethod void testEventMigrator()
    {
        RecordType eventMigration = [Select Id From RecordType Where SobjectType = 'Event' And DeveloperName = 'Event_Migration' Limit 1];
        RecordType rmsEvent = [Select Id From RecordType Where SobjectType = 'Event' And Name = 'RMS & MS Events - EU/ANZ' Limit 1];
        Test.startTest();
        String objName = 'Event';
        //String query = 'select Id, Owner.Name, RecordTypeId, PCA_Problems__c, PCA_Implications__c, PCA_Needs__c, PCA_Value_or_Pay_Off__c, Sales_Call_Objective__c, Situation__c, Pre_Call_Problems__c, Pre_Call_Implications__c, Pre_call_Payoff__c from Event Where CreatedDate > 2006-05-06T00:00:00Z And RecordTypeId = \'' + rmsEvent.Id + '\' Limit 10';
        String query = 'select Id, Owner.Name, RecordTypeId from Event Where CreatedDate > 2006-05-06T00:00:00Z And IsPrivate = False And IsChild = False And IsArchived = False And IsRecurrence = False And IsGroupEvent = False And RecordTypeId = \'' + rmsEvent.Id + '\' Limit 10';
        String indicatorField = 'RecordTypeId';
        String indicatorValue = eventMigration.Id;
        Map<String,String> fieldMapping = new Map<String,String>();
        //<Activity Detail, Event>
        fieldMapping.put('ActivityId__c','Id');
        /*
        fieldMapping.put('PCA_Problems__c','PCA_Problems__c');
        fieldMapping.put('PCA_Implications__c','PCA_Implications__c');
        fieldMapping.put('PCA_Needs__c','PCA_Needs__c');
        fieldMapping.put('PCA_Value_or_Pay_Off__c','PCA_Value_or_Pay_Off__c');
        fieldMapping.put('Sales_Call_Objective__c','Sales_Call_Objective__c');
        fieldMapping.put('Situation__c','Situation__c');
        fieldMapping.put('Pre_Call_Problems__c','Pre_Call_Problems__c');
        fieldMapping.put('Pre_Call_Implications__c','Pre_Call_Implications__c');
        fieldMapping.put('Pre_call_Payoff__c','Pre_call_Payoff__c');
        fieldMapping.put('Target_Completion_Date__c','Target_Completion_Date__c');
        fieldMapping.put('Target_Groups__c','Target_Groups__c');
		*/
        ActivityMigratorBatch amb = new ActivityMigratorBatch(objName,query,fieldMapping,indicatorField,indicatorValue);
        Database.executeBatch(amb);
        Integer migrationCount = [Select count() From Event Where RecordTypeId = :eventMigration.Id];
        Test.stopTest();
        //System.assertNotEquals(migrationCount, 0);
    }
    
    static testmethod void testTaskMigrator()
    {
        
        RecordType taskMigration = [Select Id From RecordType Where SobjectType = 'Task' And DeveloperName = 'Task_Migration' Limit 1];
        RecordType emeaTask = [Select Id From RecordType Where SobjectType = 'Task' And Name = 'EMEA EM Tasks' Limit 1];
        Test.startTest();
        String objName = 'Task';
        //String query = 'select Id, Owner.Name, RecordTypeId, PCA_Problems__c, PCA_Implications__c, PCA_Needs__c, PCA_Value_or_Pay_Off__c, Sales_Call_Objective__c, Situation__c, Pre_Call_Problems__c, Pre_Call_Implications__c, Pre_call_Payoff__c from Task Where CreatedDate > 2006-05-06T00:00:00Z And RecordTypeId = \'' + emeaTask.Id + '\' Limit 10';
        String query = 'select Id, Owner.Name, RecordTypeId from Task Where CreatedDate > 2006-05-06T00:00:00Z And IsArchived = False And IsRecurrence = False And RecordTypeId = \'' + emeaTask.Id + '\' Limit 10';
        String indicatorField = 'RecordTypeId';
        String indicatorValue = taskMigration.Id;
        Map<String,String> fieldMapping = new Map<String,String>();
        //<Activity Detail, Task>
        fieldMapping.put('ActivityId__c','Id');
        /*
        fieldMapping.put('PCA_Problems__c','PCA_Problems__c');
        fieldMapping.put('PCA_Implications__c','PCA_Implications__c');
        fieldMapping.put('PCA_Needs__c','PCA_Needs__c');
        fieldMapping.put('PCA_Value_or_Pay_Off__c','PCA_Value_or_Pay_Off__c');
        fieldMapping.put('Sales_Call_Objective__c','Sales_Call_Objective__c');
        fieldMapping.put('Situation__c','Situation__c');
        fieldMapping.put('Pre_Call_Problems__c','Pre_Call_Problems__c');
        fieldMapping.put('Pre_Call_Implications__c','Pre_Call_Implications__c');
        fieldMapping.put('Pre_call_Payoff__c','Pre_call_Payoff__c');
        fieldMapping.put('Target_Completion_Date__c','Target_Completion_Date__c');
        fieldMapping.put('Target_Groups__c','Target_Groups__c');
		*/
        ActivityMigratorBatch amb = new ActivityMigratorBatch(objName,query,fieldMapping,indicatorField,indicatorValue);
        Database.executeBatch(amb);
        Integer migrationCount = [Select count() From Task Where RecordTypeId = :taskMigration.Id];
        Test.stopTest();
        //System.assertNotEquals(migrationCount, 0);
    }
}