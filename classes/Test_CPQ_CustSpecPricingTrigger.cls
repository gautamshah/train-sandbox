/**
Test class

CPQ_Customer_Specific_Pricing Trigger
CPQ_PriceListProcesses.Lookup

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-08      Yuli Fintescu       Created
===============================================================================
*/
@isTest
private class Test_CPQ_CustSpecPricingTrigger {
	static testMethod void myUnitTest() {
		
		Test_CPQ_Product_Trigger.createTestData();

    	Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');
		Account testAccount = new Account(Status__c = 'Active', Name = 'Test Account', Account_External_ID__c = 'US-111111', RecordTypeID = acctRT.getRecordTypeId(), AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main');
		insert testAccount;

 		List<Product2> testProducts = new List<Product2> {
 			new Product2(Name = 'ITEM1', ProductCode = 'ITEM1', Product_External_ID__c = 'US:RMS:ITEM1', OrganizationName__c = 'RMS', Apttus_Product__c = true, IsActive = true),
 			new Product2(Name = 'ITEM2', ProductCode = 'ITEM2', Product_External_ID__c = 'US:RMS:ITEM2', OrganizationName__c = 'RMS', Apttus_Product__c = true, IsActive = true)
 		};
 		insert testProducts;
 		
		List<Commitment_Type__c> testCommitTypes = new List<Commitment_Type__c> {
			new Commitment_Type__c (Name = 'IP', Description__c = 'IP'),
			new Commitment_Type__c (Name = 'G3', Description__c = 'G3')
		};
		insert testCommitTypes;
		
		List<Partner_Root_Contract__c> testContracts = new List<Partner_Root_Contract__c> {
			new Partner_Root_Contract__c(Name = '011111', Root_Contract_Name__c = '011111', Root_Contract_Number__c = '011111'),
			new Partner_Root_Contract__c(Name = '022222', Root_Contract_Name__c = '022222', Root_Contract_Number__c = '022222')
		};
		insert testContracts;
		
		Test.startTest();
			List<CPQ_Customer_Specific_Pricing__c> testPricings = new List<CPQ_Customer_Specific_Pricing__c> {
				new CPQ_Customer_Specific_Pricing__c(CommTypeDesc_INT__c = 'IP', Account__c = testAccount.ID, External_ID__c = 'ITEM:111111', Product__c = testProducts[0].ID, Root_Contract__c = '011111'),
				new CPQ_Customer_Specific_Pricing__c(CommTypeDesc_INT__c = 'IP', Account__c = testAccount.ID, External_ID__c = 'ITEM:111111', Product__c = testProducts[1].ID, Root_Contract__c = '011111')
			};
			insert testPricings;
			
			for (CPQ_Customer_Specific_Pricing__c c : testPricings) {
				c.CommTypeDesc_INT__c = 'G3';
				c.Root_Contract__c = '022222';
			}
			update testPricings;
		Test.stopTest();
	}
}