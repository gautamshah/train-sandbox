/*
Name    : cpqProposal_t
sObject : Apttus_Proposal__Proposal__c

Author  : Paul Berglund
Date    : 10/11/2016
Purpose : Dispatches to necessary classes when invoked by the trigger
          of the sObject

          DO NOT PUT ANY LOGIC IN THIS CLASS!!!!
          - Creat a main() method in your class and call it from this
            class and pass ALL of the trigger context values.

========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR            CHANGE
----        ------            ------
2016-11-01  Isaac Lewis       Migrated CPQ_Proposal_After.trigger to cpqProposal_u.main()

*/
public class cpqProposal_t
{
	private static cpqProposal_u proposal = new cpqProposal_u();
	private static cpqCycleTime_Proposal_u cycletime = new cpqCycleTime_Proposal_u();
		
    // To prevent a method from executing a second time add the name of the method
    // to the 'executedMethods' set 
    public static Set<String> executedMethods = new Set<String>();

    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<Apttus_Proposal__Proposal__c> newList,
            Map<Id, Apttus_Proposal__Proposal__c> newMap,
            List<Apttus_Proposal__Proposal__c> oldList,
            Map<Id, Apttus_Proposal__Proposal__c> oldMap,
            integer size
        )
    {

        proposal.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            (List<sObject>)newList,
            (Map<Id, sObject>)newMap,
            (List<sObject>)oldList,
            (Map<Id, sObject>)oldMap,
            size
        );

        if(!executedMethods.contains('capture'))
        {
	        cycletime.capture(
	            isExecuting,
	            isInsert,
	            isUpdate,
	            isDelete,
	            isBefore,
	            isAfter,
	            isUndelete,
	            (List<sObject>)newList,
	            (Map<Id, sObject>)newMap,
	            (List<sObject>)oldList,
	            (Map<Id, sObject>)oldMap,
	            size
			);

			executedMethods.add('capture');
        }
    }
}