/****************************************************************************************
 * Name    : Account_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 25/07/2013 
 * Purpose : Contains all the logic coming from Account trigger
 * Dependencies: AccountTrigger Trigger
 *             , Account Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 10/21/13    Bill Shan            Don't create the chatter feeds for the updates from 
 *                                  API Data loader user.
 *  6/27/14     Gautam Shah         Lines 26-27, 64-65, 71-72: removed queries in favor of call to Utilities static method; Lines 107, 136: added exclusion condition for sys admin and api users
 * 9/14/2015   Amogh Ghodke         Line 220: Added APAC-ASIA & GC as regions for comparison   
 *****************************************************************************************/
public with sharing class Account_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    private String strUserProfileName;

    public Account_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
        //strUserProfileName = [select Name from Profile where Id = :UserInfo.getProfileId() limit 1].Name;
        strUserProfileName = Utilities.CurrentUserProfileName;
    }
 
    public void OnBeforeInsert(List<Account> newObjects){
        if(!Utilities.isSysAdminORAPIUser)
        {
            Set<String> strProfiles_CannotCreateAccount = new Set<String>{'EU - One Covidien', 'EMEA EM'};
                    
            if(strProfiles_CannotCreateAccount.contains(strUserProfileName))
            {
                for(Account a : newObjects)
                    a.addError(Label.AccountCreation_NoPermission); 
            }
            else
            {
                for(Account a : newObjects)
                {
                    //Update account record type and transfer address values for account created by distributor users.
                    if(Utilities.isPortalUser && a.RecordTypeId == Utilities.recordTypeMap_Name_Id.get('In Process Account')) 
                        updateDistributorCreatedAccount(a);
                    else
                    {
                        if(a.RecordTypeId == Utilities.recordTypeMap_Name_Id.get('Ems In Process Account')){
                            UpdateEMSAccountAddress(a);
                        }else{
                            a.Country__c = a.billingCountry;
                        
                            if(a.Bariatric_Procedures__c != a.Bariatric_Procedures_VS__c)
                                a.Bariatric_Procedures_VS__c = a.Bariatric_Procedures__c;
                            
                            if(a.Colorectal_Procedures__c != a.Colorectal_Procedures_VS__c)
                                a.Colorectal_Procedures_VS__c = a.Colorectal_Procedures__c;
                         }
                    }
                    

                    
                }
            }
            
            
        }
    }
 
    public void OnAfterInsert(List<Account> newObjects){
        User currentUser1 = [Select Name, UserType, Country,IS_EMS_User__c From User Where Id = :UserInfo.getUserId() Limit 1];
        User currentUser = Utilities.CurrentUser;
        System.debug(Utilities.isSysAdminORAPIUser+'>>>>>>>>>>>hello>>>>>>'+currentUser);
        if((!Utilities.isSysAdminORAPIUser && currentUser.UserType != 'Standard') || currentUser1.Is_EMS_User__C == true)
        {
            List<Case> caseList = new List<Case>();
                
            //RecordType accountCreationRT = [Select Id From RecordType Where SobjectType = 'Case' and DeveloperName = 'Account_Creation_Case' Limit 1];
            Id accountCreationRT_Id = Utilities.recordTypeMap_DeveloperName_Id.get('Account_Creation_Case');
            Id accountEMSRT_Id = Utilities.recordTypeMap_DeveloperName_Id.get('Ems_In_Process_Account');
            
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            dmo.EmailHeader.TriggerUserEmail = true;
            /*
            Map<Id,String> rtMap = new Map<Id, String>();
            for(RecordType accRT : [Select Id, Name From RecordType Where SobjectType = 'Account'])
            {
                rtMap.put(accRT.Id, accRT.Name);
            }
            */
            for(Account a : newObjects)
            {
                System.debug(accountEMSRT_Id+'>>>>>>>>>>>>>>>>>>>>>>>'+a.RecordTypeId);
                Case c = new Case();
                c.RecordTypeId = accountCreationRT_Id;
                c.TEMP_Account_Name__c = a.Name;
                //c.TEMP_Account_Record_Type__c = rtMap.get(a.RecordTypeId);
                c.TEMP_Account_Record_Type__c = Utilities.recordTypeMap_Id_Name.get(a.RecordTypeId);
                c.TEMP_Account_Address__c = Utilities.CurrentUser.Country + '\n' + a.BillingState + '\n' + a.BillingCity + '\n' + a.BillingStreet + '\n' + a.BillingPostalCode;
                c.Temporary_Account_Number__c = 'TEMP';
                if(accountEMSRT_Id == a.RecordTypeId){
                    c.Temporary_Account_Number__c = 'TEMP-EMS-';
                }
                
                //if(a.Country__c != null)
                //    c.Temporary_Account_Number__c += a.Country__c + '-' + a.Account_Auto_Number__c;//a.Account_External_ID__c; not created yet when this trigger runs since it's set by workflow.  Using the same formula as in workflow
                
                if(Utilities.CurrentUser.Country != null)
                    c.Temporary_Account_Number__c += Utilities.CurrentUser.Country + '-' + a.Account_Auto_Number__c;//a.Account_External_ID__c; not created yet when this trigger runs since it's set by workflow.  Using the same formula as in workflow
                else
                    c.Temporary_Account_Number__c += '-' + a.Account_Auto_Number__c;//a.Account_External_ID__c; not created yet when this trigger runs since it's set by workflow.  Using the same formula as in workflow
                c.AccountId = a.Id;
                c.Description = a.Comments__c;
                c.Account_Number_text__c = c.Temporary_Account_Number__c;
                c.TEMP_Account_Address__c= a.Street__c+','+a.city__c+','+a.State_Province__c+','+a.country__c+','+a.Zip_Postal_Code__c;
                c.setOptions(dmo);
                caseList.add(c);
                
            }
            System.debug('>>>>>>>>>>>>>>>>>>>>'+caseList);
            insert caseList;
        }
    }
    public void OnBeforeUpdate(List<Account> newObjects, Map<Id, Account> oldMap){
        if(!Utilities.isSysAdminORAPIUser)
        {
            for(Account a: newObjects)
            {
                a.Country__c = a.billingCountry;
                
                if(a.Bariatric_Procedures__c != oldMap.get(a.Id).Bariatric_Procedures__c && 
                    a.Bariatric_Procedures__c != a.Bariatric_Procedures_VS__c)
                    a.Bariatric_Procedures_VS__c = a.Bariatric_Procedures__c;
                else if(a.Bariatric_Procedures_VS__c != oldMap.get(a.Id).Bariatric_Procedures_VS__c && 
                    a.Bariatric_Procedures__c != a.Bariatric_Procedures_VS__c)
                    a.Bariatric_Procedures__c = a.Bariatric_Procedures_VS__c;
                    
                if(a.Colorectal_Procedures__c != oldMap.get(a.Id).Colorectal_Procedures__c && 
                    a.Colorectal_Procedures__c != a.Colorectal_Procedures_VS__c)
                    a.Colorectal_Procedures_VS__c = a.Colorectal_Procedures__c;
                else if(a.Colorectal_Procedures_VS__c != oldMap.get(a.Id).Colorectal_Procedures_VS__c && 
                    a.Colorectal_Procedures__c != a.Colorectal_Procedures_VS__c)
                    a.Colorectal_Procedures__c = a.Colorectal_Procedures_VS__c;
            }
        }
    }
 
    public void OnAfterUpdate(List<Account> newObjects, Map<Id, Account> oldMap){
        /*
        Id dataloaderProfile = [select Id, Name from Profile where Name = 'API Data Loader' limit 1].Id;
        if (UserInfo.getProfileId() == dataloaderProfile)
            return;//If this is the integration user then exit
        */
        if(!Utilities.isSysAdminORAPIUser)
        {
            List<FeedItem> feedItems = new List<FeedItem>();
            Account AccountObject = new Account();
                    
            Schema.SObjectType accType = AccountObject.getSObjectType();
            Map<String, Schema.SObjectField> M = Schema.SObjectType.Account.fields.getMap();
            
            Map<String, String> N = new Map<String, String>();
            
            for (String str : M.keyset()) 
            {
                if(str != 'systemmodstamp' && str != 'lastmodifieddate' && str !='LastModifiedById')
                {
                    N.put(str, M.get(str).getDescribe().getLabel());
                }
            }
             
            for(Account a : newObjects)
            {
                String strBody = '';
                
                for (String str : M.keyset()) 
                {
                    if(str != 'systemmodstamp' && str != 'lastmodifieddate' 
                            && str !='LastModifiedById' && str != 'billingaddress' && str != 'shippingaddress' && a.get(str) != oldMap.get(a.Id).get(str))
                    {
                        strBody += N.get(str) + ' ' + Label.is_changed_from + ' ' + oldMap.get(a.Id).get(str) + ' ' + Label.to + ' ' + a.get(str) + '\r\n';
                        if(strBody.length()>900)
                        {
                            strBody += '......\r\n';
                            break;
                        }
                    }
                }
                if(strBody.length()>0)
                feedItems.add(new FeedItem(ParentId = a.Id, Body = strBody));
            }
            
            if(feedItems.size()>0)
                insert feedItems;
        }
    }
    
    private void UpdateEMSAccountAddress(Account acc){
        acc.BillingCountry = acc.Country__c;
        acc.BillingState = acc.State_Province__c;
        acc.BillingCity = acc.City__c;
        acc.BillingStreet = acc.Street__c;
        acc.BillingPostalCode = acc.Zip_Postal_Code__c;
    }
    
    //Update account record type and transfer address values for account created by distributor users.
    private void updateDistributorCreatedAccount(Account acc)
    {
        acc.BillingCountry = acc.Country__c;
        acc.BillingState = acc.State_Province__c;
        acc.BillingCity = acc.City__c;
        acc.BillingStreet = acc.Street__c;
        acc.BillingPostalCode = acc.Zip_Postal_Code__c;
        
        if(acc.AccountType__c == 'Hospital' || acc.AccountType__c == 'Clinic' || acc.AccountType__c == 'Pharmacy' )
        {
            User currentUser = Utilities.CurrentUser;
            if(currentUser.Region__c == 'APAC-ASIA' || currentUser.Region__c == 'GC' )
                acc.RecordTypeId = Utilities.recordTypeMap_Name_Id.get('ASIA-Healthcare Facility');
            if(currentUser.Region__c == 'EM' || currentUser.Region__c == 'EU')
                acc.RecordTypeId = Utilities.recordTypeMap_Name_Id.get('EU-Healthcare Facility');
        }
        else if(acc.AccountType__c == 'Sub Dealer')
            acc.RecordTypeId = Utilities.recordTypeMap_Name_Id.get('Sub-Dealer');
        else if(acc.AccountType__c == 'Sub Entity')
            acc.RecordTypeId = Utilities.recordTypeMap_Name_Id.get('Sub-Entity');
    }
    
    //after delete on Connected Accounts for the Story -PBP-176
    
     public void OnAfterDelete(List<Account_Affiliation__c>  oldAccAffList ){
     
     List<Account_Affiliation__c>  oldAccAffList1= oldAccAffList;  
     Set<Id> accIds = new Set<Id>();
     Set<Id> ctIds = new Set<Id>();
     Set<ID> ids= new set<ID>();
    for(Account_Affiliation__c  ac :oldAccAffList1)
       {
        System.debug('recorddeleted =======>'+ac.Affiliated_with__c);
        System.debug('recorddeleted =======>'+ac.AffiliationMember__c);
      accids.add(ac.Affiliated_with__c);
      ids.add(ac.AffiliationMember__c);
      }

    List<Contact> cts= [select id,name,AccountId from contact where AccountId in :accids];

    for(Contact ct1 :cts)
      {
     ctIds.add(ct1.id);
       }
     System.debug('cts are =======>'+ids);
    List<Contact_Affiliation__c > caf= [select id, name,Contact__c,AffiliatedTo__c from Contact_Affiliation__c where Contact__c in:ctIds and AffiliatedTo__c in:ids  ];

    if(caf.size()>0)
    delete caf;

      
     }
     //End AfterDelete

}