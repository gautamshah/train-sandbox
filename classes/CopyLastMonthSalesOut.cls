public class CopyLastMonthSalesOut {
	public string isNewcp='F';
	public String ErrorAlert{get;set;}
	public String cyclePeriodId{get;set;}
	private final Cycle_Period__c sfcp;
	public CopyLastMonthSalesOut(ApexPages.StandardController stdController) {
        this.sfcp=[Select Id,Cycle_Period_Reference__r.Year__c,Cycle_Period_Reference__r.Month__c,Distributor_Name__c,Channel_Inventory_Validated__c,Sales_Out_Validated__c,Status__c From Cycle_Period__c where Id=:stdController.getId()];
    	cyclePeriodId=sfcp.Id;
    }
    
	public pageReference copyandpaste()
	{
		ErrorAlert='Z';
		Pagereference pgref=null;
		Date dt=Date.valueOf(sfcp.Cycle_Period_Reference__r.Year__c + '-' + sfcp.Cycle_Period_Reference__r.Month__c+ '-01');
		Date currentPeriod=Date.today().addMonths(-1);
		if(dt.month()!=currentPeriod.month()||dt.year()!=currentPeriod.year())
		{
			ErrorAlert='M';
		}
		else
		{
			Integer currentMonthvalue=Date.today().addMonths(-1).month();
			Integer prevMonthvalue=Date.today().addMonths(-2).month();
			String currentMonth='';
			String previousMonth='';
			String currentyear=String.valueOf(Date.today().addMonths(-1).year());
			String previousYear=String.valueOf(Date.today().addMonths(-2).year());
			
			if(currentMonthvalue<10)
				currentMonth='0'+String.valueOf(currentMonthvalue);
			else
				currentMonth=String.valueOf(currentMonthvalue);	
			
			if(prevMonthvalue<10)
				previousMonth='0'+String.valueOf(prevMonthvalue);
			else
				previousMonth=String.valueOf(prevMonthvalue);
						
			// get The Distributor Id
	       Id distributorId=sfcp.Distributor_Name__c;//FileUploader.getUserAccount();
	       //Get or create Cycle Period for current month
	       Cycle_Period__c cp=sfcp;//createCyclePeriod(distributorId, currentMonth, currentyear,true);
	       
	       //get Cycle Period for previous month
	       Cycle_Period__c previouscp=createCyclePeriod(distributorId, previousMonth, previousYear, false);
	       if(cp.Sales_Out_Validated__c==false&&cp.status__c=='Open')
	       {
	       		
	       		List<Sales_Out__c> lstsoutPrevious=[select comments__c,cycle_period__c,Currency_Text__c, Quantity_Text__c ,
	       							Document_No__c,Distributor_Name_F__c,Document_Date_Text__c,Lots_Serial_Number__c,
	       							Product_Code__c,Selling_Price_Text__c,UOM__c,Sell_From__c,Sell_To__c,
	       							salesout_uploads__c from Sales_Out__c where Cycle_Period__c=:previouscp.Id];
	       		//Distributor_Name_F__c=:distributorId and 					
	       		if(lstsoutPrevious!=null&&lstsoutPrevious.size()>0)	
	       		{
	       			try
	       			{
		       			if(isNewcp=='F')	
		            		FileUploader.deleteSalesOut(distributorId, cp.Id,'Replace');	
		   			 	SalesOut_uploads__c upl = new SalesOut_uploads__c();
		   			 	upl.date_uploaded__c =Date.today(); //myDate;
						upl.Upload_file_name__c = 'Copy From Last Month';
						upl.upload_status__C = 'Uploaded';
						upl.account__c = distributorId;//sfupl.account__C;
						upl.cycle_period__c = cp.Id;
						upl.Upload_Object__c='Sales Out';
						upl.Total_Number_of_Records__c = lstsoutPrevious.size();
		   
						insert upl;
						id result = upl.id;
					       
		       			List<Sales_Out__c> lstsoutNew=new List<Sales_Out__c>();
		       			for(Sales_Out__c sout:lstsoutPrevious)
		       			{
		       				Sales_Out__c so=new Sales_Out__c();
		       				so.comments__c = sout.comments__c;
				            so.cycle_period__c = cp.Id;
				            so.Quantity_Text__c = sout.Quantity_Text__c;
				            so.Document_No__c= sout.Document_No__c;
				           // so.Distributor_Name__c= sout.Distributor_Name__c;
				            so.Document_Date_Text__c= sout.Document_Date_Text__c;
				            so.Lots_Serial_Number__c= sout.Lots_Serial_Number__c;
				            so.Product_Code__c= sout.Product_Code__c;
				            so.Selling_Price_Text__c= sout.Selling_Price_Text__c;
				            so.UOM__c= sout.UOM__c;
				            so.Sell_From__c= sout.Sell_From__c;
				            so.Sell_To__c= sout.Sell_To__c;
				            so.Currency_Text__c=sout.Currency_Text__c;
				            so.salesout_uploads__c = result;
		       				lstsoutNew.add(so);
		       			}
		       			insert lstsoutNew;
		       			ErrorAlert='F';
	       			}
	       			catch(Exception e)
	       			{
	       				ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
	            		ApexPages.addMessage(errormsg);
	       			}
	       		}
	       		else
	       		{	
	       			ErrorAlert='Y';
	       		}			
	       }
	       /*
	       else
	       {
	       		ErrorAlert='V';
	       }*/
		}//end of else for if(dt.month()!=currentPeriod.month()||dt.year()!=currentPeriod.year())
		return pgref;
	}
	public Cycle_Period__c createCyclePeriod(Id DistributorName,String CyclePeriodMonth, String CyclePeriodYear,boolean CreatePeriod)
    {
    	List<Cycle_Period_Reference__c> lstRef=[select Id from Cycle_Period_Reference__c where Month__c=:CyclePeriodMonth and Year__c=:CyclePeriodYear];
    	Cycle_Period__c cprToUse=new Cycle_Period__c();
    	if(lstRef!=null&&lstRef.size()>0)
    	{
	    	List<Cycle_Period__c> lstcprToUse=[Select Id,Channel_Inventory_Validated__c,Sales_Out_Validated__c,Status__c from Cycle_Period__c where Cycle_Period_Reference__c=:lstRef[0].Id and Distributor_Name__c=:DistributorName limit 1];
	    	if(lstcprToUse!=null&&lstcprToUse.size()>0)
	    	{
	    		cprToUse=lstcprToUse[0];
	    	}
	    	/*
	    	else
	    	{
	    		if(CreatePeriod==true)
	    		{
		    		Cycle_Period__c cprNew= new Cycle_Period__c(Cycle_Period_Reference__c=lstRef[0].Id, Distributor_Name__c=DistributorName, Status__c='Open',Channel_Inventory_Validated__c=false,Sales_Out_Validated__c=false);
		    		insert cprNew;
		    		cprToUse=cprNew;
		    		isNewcp='T';
	    		}
	    	}*/
    	}
    	return cprToUse;
    }
}