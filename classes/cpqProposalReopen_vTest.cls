/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161210  A    BF         AV-240     Create new Proposal for Cancelled Agreement - Created test class

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/
@isTest
public class cpqProposalReopen_vTest
{
	@testSetup
    static void Setup()
    {
		Account acct = new Account(Name = 'Test Account 1',
								   BillingState = 'CO',
								   BillingPostalCode = '12345',
								   BillingCountry = 'US');
		insert acct;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
        insert c;

        List<RecordType> proposalRts = cpqProposal_c.getRecordTypes();
        Id proposalRecordTypeId = null;
        for (RecordType rt: proposalRts) {
        	if (rt.DeveloperName == 'Outright_Quote_Hardware_Locked') {
        		proposalRecordTypeId = rt.Id;
        		break;
        	}
        }

        Apttus_Proposal__Proposal__c prop  = cpqProposal_c.create(proposalRecordTypeId, 1);
        prop.Apttus_Proposal__Approval_Stage__c = 'Submitted to Contracting';
        prop.Primary_Contact2__c = c.Id;
        insert prop;

        List<RecordType> agreementRts = cpqAgreement_c.getRecordTypes();
        Id agreementRecordTypeId = null;
        for (RecordType rt: agreementRts) {
        	if (rt.DeveloperName == 'Outright_Quote_Hardware_AG') {
        		agreementRecordTypeId = rt.Id;
        		break;
        	}
        }

        Apttus__APTS_Agreement__c agreement = cpqAgreement_c.create(agreementRecordTypeId, 1);
        agreement.Apttus_QPComply__RelatedProposalId__c = prop.Id;
        agreement.Primary_Contact2__c = c.Id;
        insert agreement;
    }

    @isTest
    public static void Test_Reopen_Proposal()
    {
    	Apttus_Proposal__Proposal__c prop = [Select Id From Apttus_Proposal__Proposal__c Limit 1];
    	Test.startTest();
			cpqProposalReopen_v con = new cpqProposalReopen_v(new ApexPages.StandardController(prop));
			con.reopenProposal();
		Test.stopTest();

		prop = [Select Id, 
					   RecordType.DeveloperName,
					   Apttus_Proposal__Approval_Stage__c
				From Apttus_Proposal__Proposal__c
				Where Id = :prop.Id];
		Apttus__APTS_Agreement__c agreement = [Select Id, 
													  Apttus_QPComply__RelatedProposalId__c,
													  Apttus__Status__c
											   From Apttus__APTS_Agreement__c
											   Limit 1];
		System.assertEquals('Outright_Quote_Hardware', prop.RecordType.DeveloperName);
		System.assertNotEquals('Submitted to Contracting', prop.Apttus_Proposal__Approval_Stage__c);
		System.assertEquals(null, agreement.Apttus_QPComply__RelatedProposalId__c);
		System.assertEquals(cpqAgreement_c.convert(cpqAgreement_c.StatusIndicators.CancelledRequest),
							agreement.Apttus__Status__c);
	}
}