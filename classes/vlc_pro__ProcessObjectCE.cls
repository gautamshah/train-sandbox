/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessObjectCE {
    global String displayMode {
        get;
        set;
    }
    global ProcessObjectCE(ApexPages.StandardController stdController) {

    }
    global vlc_pro.ProcessObjectCE getCtrl() {
        return null;
    }
}
