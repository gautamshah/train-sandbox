/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed  80 => *                            120 => *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry

YYYYMMDD-A	PAB			CPR-000	Updated comments section

7.  Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com
Gautam Shah				GS				Medtronic.com

MODIFICATION HISTORY
====================
Date-Id		Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
Nov 21, 2017-A	PAB					Created at 10:50:49 AM by paul.berglund

*/
public class cpqPartnerContractInfoService
{
    public static final integer MAXTIMEOUT = 120000; // 2 Minutes, or 120 secons, or 120,000 milliseconds 
    private static final String endpoint_x = cpqVariable_c.getValue('Proxy EndPoint');
    private Integer timeout_x;
    private String[] ns_map_type_info = new String[]
    {
        'http://tempuri.org/', 'cpqPartnerContractInfoService', 
        'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract', 'cpqPartnerContractInfoClasses', 
        'http://schemas.microsoft.com/2003/10/Serialization/', 'cpqPartnerContractInfoService'
     };
       
    ////
    //// constructor
    ////
    public cpqPartnerContractInfoService()
    {
        this(cpqPartnerContractInfoService.MAXTIMEOUT);
    }
    
    public cpqPartnerContractInfoService(integer timeout)
    {
        this.timeout_x = timeout;
    }
   
    ////
    //// service methods
    //// 
    public cpqPartnerContractInfoClasses.ContractInfo getTheContract(List<String> contractNums)
    {
        cpqPartnerContractInfoClasses.ContractInfo myContractInfo = null;
        for (String contractNumber : contractNums)
        {
            if (myContractInfo == null)
            {
               myContractInfo = this.GetTheContract(contractNumber);
            }
            else
            {
                myContractInfo.mergeResults(this.GetTheContract(contractNumber));
            }
 
        }
        return myContractInfo;
    }
    
    public cpqPartnerContractInfoClasses.ContractInfo getTheContract(String contractNumber)
    {
        cpqPartnerContractInfoClasses.ContractInfoRequestType contract 
                    = new cpqPartnerContractInfoClasses.ContractInfoRequestType();

        contract.contractNumberField = contractNumber;
        contract.getContractItemsField = true;
        contract.getDealerServiceField = true;
        contract.identityTokenField = '1000';
        contract.maximumResponseRecordsField = '1000';
    
        cpqPartnerContractInfoService.ContractInfo_element request_x 
                    = new cpqPartnerContractInfoService.ContractInfo_element();
        request_x.request = contract;
        
        cpqPartnerContractInfoService.ContractInfoResponse_element response_x;
        
        Map<String, cpqPartnerContractInfoService.ContractInfoResponse_element> 
                response_map_x = new Map<String, cpqPartnerContractInfoService.ContractInfoResponse_element>();
        response_map_x.put('response_x', response_x);
        
            WebServiceCallout.invoke(
            this,
            request_x,
            response_map_x,
            new String[]
            {
              endpoint_x,
              'http://tempuri.org/IProxy/ContractInfo',
              'http://tempuri.org/',
              'ContractInfo',
              'http://tempuri.org/',
              'ContractInfoResponse',
              'cpqPartnerContractInfoService.ContractInfoResponse_element'
            }
        );
        
        response_x = response_map_x.get('response_x');
        return new cpqPartnerContractInfoClasses.ContractInfo(response_x.ContractInfoResult);
    }
    
    public cpqPartnerContractInfoClasses.Customer_Wrapper GetContractEligibilityByRoot(String rootContract, String startsWith) 
    {
          String[] ns_map_type_info 
            = new String[]
            {
                'http://tempuri.org/', 'cpqPartnerContractInfoService', 
                'http://schemas.datacontract.org/2004/07/SFDC.Services.DataContract.Contract', 'cpqPartnerContractInfoClasses', 
                'http://schemas.microsoft.com/2003/10/Serialization/', 'cpqPartnerContractInfoService'
            };
       
            cpqPartnerContractInfoService.GetContractEligibilityByRoot_element request_x = 
                        new cpqPartnerContractInfoService.GetContractEligibilityByRoot_element();
            request_x.rootContract = rootContract;
            request_x.startsWith = startsWith;
            cpqPartnerContractInfoService.GetContractEligibilityByRootResponse_element response_x;
            Map<String, cpqPartnerContractInfoService.GetContractEligibilityByRootResponse_element> response_map_x 
                        = new Map<String, cpqPartnerContractInfoService.GetContractEligibilityByRootResponse_element>();

            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{
               endpoint_x,
              'http://tempuri.org/IProxy/GetContractEligibilityByRoot',
              'http://tempuri.org/',
              'GetContractEligibilityByRoot',
              'http://tempuri.org/',
              'GetContractEligibilityByRootResponse',
              'cpqPartnerContractInfoService.GetContractEligibilityByRootResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.GetContractEligibilityByRootResult;
        }

    ////
    //// wsdl generated classes for service
    ////
     public class ContractInfo_element {
        public cpqPartnerContractInfoClasses.ContractInfoRequestType request;
        private String[] request_type_info = new String[]{'request','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'request'};
    }
    public class ContractInfoResponse_element {
        public cpqPartnerContractInfoClasses.ContractInfoResponseType ContractInfoResult;
        private String[] ContractInfoResult_type_info = new String[]{'ContractInfoResult','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'ContractInfoResult'};
    }

    public class GetContractEligibilityByRootResponse_element {
        
        public cpqPartnerContractInfoClasses.Customer_Wrapper GetContractEligibilityByRootResult;
        
        private String[] GetContractEligibilityByRootResult_type_info 
            = new String[]{'GetContractEligibilityByRootResult','http://tempuri.org/',null,'0','1','true'};
            
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'GetContractEligibilityByRootResult'};
    }
    public class GetContractEligibilityByRoot_element {
        public String rootContract;
        public String startsWith;
        private String[] rootContract_type_info = new String[]{'rootContract','http://tempuri.org/',null,'0','1','true'};
        private String[] startsWith_type_info = new String[]{'startsWith','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'rootContract','startsWith'};
    }
    
}