@isTest
private class UserSecurityPublicGroupUpdateTestUpdate {
    static testMethod void myUnitTest() {
        
        List<User> insertUserList = new List<User>();
        List<User> updateUserList = new List<User>();
        Group g=new Group(name='FR_MCS');
        insert g;
        Profile profileEURMS = [SELECT Id from Profile where Name = 'EU - One Covidien' LIMIT 1];
            
        for(Integer r=0;r<10;r++){          
            User userToInsertRegion = new User();
            userToInsertRegion.FirstName = 'Region' + r;
            userToInsertRegion.LastName = 'User' + r;
            userToInsertRegion.Alias = 'regu' + r;          
            userToInsertRegion.Email =  r + 'region.user@covidien.security';
            userToInsertRegion.Username = r + 'region.user@covidien.security.com';
            userToInsertRegion.ProfileId = profileEURMS.Id;
            userToInsertRegion.TimeZoneSidKey = 'America/New_York';
            userToInsertRegion.LanguageLocaleKey= 'en_US';
            userToInsertRegion.LocaleSidKey = 'en_US';
            userToInsertRegion.EmailEncodingKey = 'ISO-8859-1';
            userToInsertRegion.FederationIdentifier = 'Region'; //for test
            userToInsertRegion.Region__c = 'EU';
            userToInsertRegion.Sales_Org_PL__c= 'MCS';
            userToInsertRegion.User_Role__c='Rep';
            userToInsertRegion.Country = 'FR';
            insertUserList.add(userToInsertRegion);
        }
            // for LATAM_Country group
            User userToInsertLatam = new User();
            userToInsertLatam.FirstName = 'Country1';
            userToInsertLatam.LastName = 'User11';
            userToInsertLatam.Alias = 'ctyu11';         
            userToInsertLatam.Email =  '11country.user@covidien.security';
            userToInsertLatam.Username = '11country.user@covidien.security.com';
            userToInsertLatam.ProfileId = profileEURMS.Id;
            userToInsertLatam.TimeZoneSidKey = 'America/New_York';
            userToInsertLatam.LanguageLocaleKey= 'en_US';
            userToInsertLatam.LocaleSidKey = 'en_US';
            userToInsertLatam.EmailEncodingKey = 'ISO-8859-1';
            userToInsertLatam.FederationIdentifier = 'Region';
            userToInsertLatam.Region__c = 'LATAM';
            userToInsertLatam.Country = 'AR';
            userToInsertLatam.Sales_Org_PL__c= 'MCS';
            userToInsertLatam.User_Role__c='Field Manager';
            userToInsertLatam.Region__c = 'LATAM';
            userToInsertLatam.Country = 'AR'; 
            insertUserList.add(userToInsertLatam);
            
        for(Integer c=0;c<10;c++){          
            User userToInsertCountry = new User();
            userToInsertCountry.FirstName = 'Country' + c;
            userToInsertCountry.LastName = 'User' + c;
            userToInsertCountry.Alias = 'ctyu' + c;         
            userToInsertCountry.Email =  c + 'country.user@covidien.security';
            userToInsertCountry.Username = c + 'country.user@covidien.security.com';
            userToInsertCountry.ProfileId = profileEURMS.Id;
            userToInsertCountry.TimeZoneSidKey = 'America/New_York';
            userToInsertCountry.LanguageLocaleKey= 'en_US';
            userToInsertCountry.LocaleSidKey = 'en_US';
            userToInsertCountry.EmailEncodingKey = 'ISO-8859-1';
            userToInsertCountry.FederationIdentifier = 'Country';
            userToInsertCountry.Country = 'FR';
            userToInsertCountry.Sales_Org_PL__c= 'MCS';
            userToInsertCountry.User_Role__c='Field Manager';
    
            
            insertUserList.add(userToInsertCountry);
        }
        
        for(Integer p=0;p<10;p++){          
            User userToInsertConstrained = new User();
            userToInsertConstrained.FirstName = 'Private' + p;
            userToInsertConstrained.LastName = 'User' + p;
            userToInsertConstrained.Alias = 'privu' + p;            
            userToInsertConstrained.Email =  p + 'private.user@covidien.security';
            userToInsertConstrained.Username = p + 'private.user@covidien.security.com';
            userToInsertConstrained.ProfileId = profileEURMS.Id;
            userToInsertConstrained.TimeZoneSidKey = 'America/New_York';
            userToInsertConstrained.LanguageLocaleKey= 'en_US';
            userToInsertConstrained.LocaleSidKey = 'en_US';
            userToInsertConstrained.EmailEncodingKey = 'ISO-8859-1';
            userToInsertConstrained.FederationIdentifier = 'Private';
            userToInsertConstrained.Sales_Org_PL__c= 'MCS';
            userToInsertConstrained.User_Role__c='Field Manager';
            userToInsertConstrained.Country = 'FR';
            insertUserList.add(userToInsertConstrained);
        }
        
        for(Integer x=0;x<10;x++){          
            User userToInsertConstrained = new User();
            userToInsertConstrained.FirstName = 'Constrained' + x;
            userToInsertConstrained.LastName = 'User' + x;
            userToInsertConstrained.Alias = 'conu' + x;         
            userToInsertConstrained.Email =  x + 'constrained.user@covidien.security';
            userToInsertConstrained.Username = x + 'constrained.user@covidien.security.com';
            userToInsertConstrained.ProfileId = profileEURMS.Id;
            userToInsertConstrained.TimeZoneSidKey = 'America/New_York';
            userToInsertConstrained.LanguageLocaleKey= 'en_US';
            userToInsertConstrained.LocaleSidKey = 'en_US';
            userToInsertConstrained.EmailEncodingKey = 'ISO-8859-1';
            userToInsertConstrained.FederationIdentifier = 'Constrained';
            userToInsertConstrained.Sales_Org_PL__c= 'MCS';
            userToInsertConstrained.User_Role__c='Field Manager';
            userToInsertConstrained.Country='FR';
            
            insertUserList.add(userToInsertConstrained);
        }
        
        insert insertUserList;
        
        String PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME = 'Private_Covidien_Users';
    
        List <Group> groupList = new List <Group>([SELECT Id, DeveloperName from Group where DeveloperName like 'Region_Group_%' OR DeveloperName like 'Country_Group_%' OR DeveloperName like 'LATAM_%' OR DeveloperName = :PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME]);
        Map<String, Id> groupMap = new Map <String, Id>();
        for (Group grp : groupList)
            groupMap.put(grp.DeveloperName, grp.Id);
        
        String groupName;
        List<GroupMember> insertGroupMemberList = new List<GroupMember>();
        for (user u: insertUserList){
            if (u.User_Visibility__c == 'Region' || u.User_Visibility__c == 'Country' || u.User_Visibility__c == 'Private'){
                 if (u.User_Visibility__c == 'Region'){
                             if(u.Region__c == 'LATAM')  
                                groupName =  'LATAM_' + u.Country;
                             else
                                groupName =  'Region_Group_' + u.Region__c;
                        }
                else if (u.User_Visibility__c == 'Country')
                    groupName = 'Country_Group_' + u.Country;
                else if (u.User_Visibility__c == 'Private')
                    groupName = PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME;
                
                GroupMember grpMemAdd = new GroupMember();
                grpMemAdd.UserOrGroupId = u.Id;
                grpMemAdd.GroupId = groupMap.get(groupName);
                insertGroupMemberList.add(grpMemAdd);       
            } 
        }
        
        //for (List<User> userList : [SELECT id, FederationIdentifier from User where user.UserType = 'Standard' and IsActive = TRUE and Region__c = 'US' and User_Visibility__c != 'Region' LIMIT 200]){
        for (List<User> userList : [SELECT id, FederationIdentifier, User_Visibility__c,Country_SalesOrg__c,User_Role__c from User 
            where user.UserType = 'Standard' AND FederationIdentifier IN ('Region', 'Country', 'Private', 'Constrained')]){ 
            for(User u : userList){
                u.User_Visibility__c = u.FederationIdentifier;                             
               if( updateUserList.size()>1 )
              {  
              if(u.User_Role__c=='Rep')
              u.User_Role__c='Field Manager';
              else u.User_Role__c='Rep';              
                }
                
                updateUserList.add(u);
            }
        }
        system.debug('***[updateUserList Size]:' + updateUserList.size());
        Test.startTest();
        update updateUserList;
        Test.stopTest();
        System.assertEquals([select Franchise__c from user where name='Constrained0User0' ],[select Sales_Org_PL__c from user where name='Constrained0User0' ]);
        
            } // end: myUnitTest()
}