public with sharing class AccountExt {
public Id accId;
public Account acc{get;set;}
public List<Account_Affiliation__c> affi  = new List<Account_Affiliation__c>();
public List<contact> cont {get;set;}
public set<Id> affiIds =new Set<Id>();
public String accName {get;set;}
public List<wrapperAccount> lstw {get;set;}
public List<Contact> cnts {get;set;}
List<Contact_Affiliation__c> cc1{get;set;}
Set<Id> afiliationContIds{get;set;}
Public Integer size{get;set;}
Public Integer noOfRecords{get; set;} 
 





    public AccountExt(ApexPages.StandardController controller) {
       size = 50;
       accId = apexpages.currentpage().getparameters().get('id');
       acc= [select id, name from Account where id=:accId];
       accName= acc.name;
        System.debug('accName is-------'+accName);
           //System.debug('contacts are----'+contacts);*/
          affi= [select id,AffiliationMember__c,Affiliated_with__c,name from Account_Affiliation__c where AffiliationMember__c =:accid];
          
          cc1 = new List<Contact_Affiliation__c>([select id,name,AffiliatedTo__c,Contact__c,User_Login__c, Evaluating_Clinician__c from Contact_Affiliation__c where AffiliatedTo__c =:acc.id ]);
          afiliationContIds = new Set<Id>();
          
          if(cc1!=NULL || cc1.size()>0)
          {
              for(Contact_Affiliation__c l : cc1)
              {
                 afiliationContIds.add(l.Contact__c); 
              }
          }
         // cc1= [select id,name,AffiliatedTo__c,Contact__c,User_Login__c, Evaluating_Clinician__c from Contact_Affiliation__c where AffiliatedTo__c =:acc.id ];
          
          
          System.debug('Aff1 are -----'+affi);
          for(Account_Affiliation__c ac:affi)
          {
              affiIds.add(ac.Affiliated_with__c);
          }
                
                System.debug('Account_Affiliation__c with-----'+affiIds);
        
                

    }
    
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
                if(con == null) {
                   //List<contact>cont1=[select id, name from contact where accountId In :affiIds ];
                     
            
            con = new ApexPages.StandardSetController([select id, name,Account.name from contact where accountId In :affiIds ]);
            con.setPageSize(50);
  noOfRecords=con.getResultSize();                       
                }
            return con;
        }
        set;
    }

     public List<wrapperAccount> getRecords() {
        
        lstw= new List<wrapperAccount>();
        cnts = new List<Contact>();
        cnts = con.getRecords();
        
        for(Contact c : cnts)
        {
        
           if(afiliationContIds.contains(c.id))
           {
                for(Contact_Affiliation__c affCon :cc1)
                {
                    if(affCon.Contact__c == c.id)
                    {
                      lstw.add(new wrapperAccount(acc,c,affCon.User_Login__c,affCon.Evaluating_Clinician__c));
                    }
                }               
           }else
           {
               lstw.add(new wrapperAccount(acc,c,false,false));
           }
        }
                       
        return lstw;
        
    }
    
    public Boolean hasNext {
        get {
             return con.getHasNext();
            }
        set;
    }
   
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
   
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
   
    public void first() {
        con.first();
    }
   
    public void last() {
        con.last();
    }

    public void previous() {
                con.previous();
    }
    
    public void next() {
        
                con.next();
    }
  
  /*
 //Returns current page number 
 public Decimal getCurrentPageNumber() {
  return this.currentPage;
 }

 //action for next click
 public PageReference next() {
  if(maxSize > this.currentPage * PAGE_NUMBER) {
   this.currentPage = this.currentPage + 1;
  }
  return null;
 }    
 
 //action for previous click
 public PageReference previous() {        
  if(this.currentPage > 1)
   this.currentPage = this.currentPage - 1;
  return null;
 }

*/

    
          
    
       
      
   public pagereference customSave()
   {
    List<Contact_Affiliation__c> addCommunity = new List<Contact_Affiliation__c>();
    
    List<Contact_Affiliation__c> cc= [select id,name,AffiliatedTo__c,Contact__c,User_Login__c, Evaluating_Clinician__c from Contact_Affiliation__c where AffiliatedTo__c =:acc.id ];

        System.debug('cc is---------'+cc);
        set<ID> ids1= new set<ID>();
        
        for(Contact_Affiliation__c cc1 :cc)
        {
            ids1.add(cc1.Contact__c);
        }
            System.debug('lstw is--------'+lstw);
        
        for(wrapperAccount wa:lstw)
        {
           System.debug('contact id from query is------'+ids1);
           System.debug('contact id from wrapper list is is------'+Ids1.contains(wa.myContact.id));
            if(Ids1.contains(wa.myContact.id))
            {
                List<Contact_Affiliation__c> c22 =[select id,name,AffiliatedTo__c,Contact__c,User_Login__c, Evaluating_Clinician__c  from Contact_Affiliation__c where Contact__c=:wa.myContact.id and AffiliatedTo__c=:wa.acc1.id ];
                system.debug('the c22 values are '+ c22);
                if(wa.isselected==false && wa.isselected1==false )
                {
                   delete c22;
                    
                }
                else
                {
                   // if((c22[0].AffiliatedTo__c==acc.id))

                        System.debug('wa.isSelectd is-------'+wa.isselected);
                        
                        System.debug('wa.isSelectd is1-------'+wa.isselected1);
                        //system.debug('the c22[0] value WAS '+ c22[0] +'affiliated to '+c22[0].AffiliatedTo__c +'the acc id we are taking is '+acc.id);
                        c22[0].User_Login__c=wa.isselected;
                        c22[0].Evaluating_Clinician__c=wa.isselected1;
                        c22[0].AffiliatedTo__c=acc.id;
                        c22[0].Affiliation_Role__c = 'External Community Member';
                        system.debug('the c22[0] value IS '+ c22[0] +'affiliated to '+c22[0].AffiliatedTo__c +'the acc id we are taking is '+acc.id);
                        
                        update c22;
                    
                }
            }else
            {
                if(wa.isselected ||wa.isselected1)
                {
                    Contact_Affiliation__c c= new Contact_Affiliation__c();
                   // c.name=wa.myContact.name;
                    c.AffiliatedTo__c=acc.id;
                    c.User_Login__c=wa.isselected; 
                    c.Evaluating_Clinician__c=wa.isselected1;
                   c.Contact__c= wa.myContact.id;  
                   c.Affiliation_Role__c = 'External Community Member';
                   c.HospitalFacility__c=wa.myContact.Account.id;
                    addCommunity.add(c);
                    upsert addCommunity;
                }
            }
        }
        /*List<Community_Contact__c> cc= [select id,HospitalCommunity__c from Community_Contact__c where HospitalCommunity__c =:acc.id ];
  Set<ID> ids= new Set<ID>();
  
  if(cc.size()>0)
  {
  for(Community_Contact__c cc1:cc)
  {ids.add(cc1.id);
  }
           update cc;
   }
   else
   {
   insert addCommunity;
   }*/
   //upsert addCommunity;

    PageReference pr = new PageReference('/' + acc.id);
      
    return pr;
   }
   
   public pagereference customCancel()
   {
   PageReference pr = new PageReference('/' + acc.id);
      
   return pr;
   }
   

    
    public class wrapperAccount{
    
        public Account acc1 {get;set;}
        public Contact myContact {get;set;}
        public Boolean  isselected {get;set;}
        public Boolean  isselected1 {get;set;}
        
   
    public wrapperAccount(Account acc1, Contact myContact, Boolean userLogin, Boolean clinician)
    {
    this.acc1=acc1;
    this.myContact=myContact;
    this.isselected =userLogin;
    this.isselected1 =clinician;

    }
    
    
    }
}