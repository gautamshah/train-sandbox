/**
Invoke ContractInfo WSs

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-02-21      Yuli Fintescu       Created
===============================================================================
*/
public with sharing class CPQ_ContractInfoInvoke {
    public class Dealers
    {
        public Integer billToField {get; set;}
        public String cityField {get; set;}
        public DateTime effectiveDateField {get; set;}
        public DateTime expirationDateField {get; set;}
        public String nameField {get; set;}
        public Integer shipToField {get; set;}
        public String stateField {get; set;}
        public String zipCodeField {get; set;}
    }
	
    public class Items 
    {
        public Integer alternateUOMCaseQuantityField {get; set;}
        public String alternateUOMField {get; set;}
        public Decimal priceField {get; set;}
        public DateTime effectiveDateField {get; set;}
        public DateTime expirationDateField {get; set;}
        public String itemDescriptionField {get; set;}
        public String itemNumberField {get; set;}
        public Decimal marginField {get; set;}
        public Integer quantityBreakField {get; set;}
        public String salesClassField {get; set;}
        public Decimal suggestedResaleField {get; set;}
        public String unitOfMeasureField {get; set;}
    }
	
	public class ContractInfo { 
    	public String ContractName {get; set;}
    	public String RootContractNum {get; set;}
    	public String effectiveDateField {get; set;}
    	public String CommitmentType {get; set;}
    
    	public List<Dealers> dealerList {get; set;}
    	public List<Items> itemList {get; set;}
    	public List<String> relatedContracts {get; set;}
    	
    	public ContractInfo() {
    		dealerList = new List<Dealers>();
    		itemList = new List<Items>();
    		relatedContracts = new List<String>();
    	}
	}
	
	public static ContractInfo InvokeContractInfo(String[] ContractNums) {
	    Map<Integer, Dealers> dealerMap = new Map<Integer, Dealers>();
	    Map<String, Items> itemMap = new Map<String, Items>();
        ContractInfo contract = new ContractInfo();
        
        CPQ_ProxyServices.BasicHttpBinding_IProxy service = new CPQ_ProxyServices.BasicHttpBinding_IProxy();
        CPQ_ProxyContractInfoDataType.ContractInfoResponseType response;
        
        //run WS for both R and D - show the related contracts which returned from WS successfully
        for (String s : ContractNums) {
	        CPQ_ProxyContractInfoDataType.ContractInfoRequestType request = new CPQ_ProxyContractInfoDataType.ContractInfoRequestType();
	        
	        //request.PropertyChanged = new CPQ_ProxyPropertyChangedEvent.PropertyChangedEventHandler();
	        request.contractNumberField = s;
	        request.getContractItemsField = true;
	        request.getDealerServiceField = true;
	        request.identityTokenField = '1000';
	        request.maximumResponseRecordsField = '1000';
	        System.debug('*** request ***' + request);
	        
	        response = service.ContractInfo(request);
	        System.debug('*** response ***' + response);
	        
	        if (response == null || response.errorCodeField != '0') 
	        	continue;
	        
        	//related contracts
        	contract.relatedContracts.add(response.contractInfoField.pricingIdField);
        	
        	//header
	        if (!String.isEmpty(response.contractInfoField.nameField))
	        	contract.ContractName = response.contractInfoField.nameField;
	        
	        if (!String.isEmpty(response.contractInfoField.rootContractField))
	        	contract.RootContractNum = response.contractInfoField.rootContractField;
	        
	        if (!String.isEmpty(response.contractInfoField.commitmentTypeField))
        		contract.CommitmentType = response.contractInfoField.commitmentTypeField;
        	
        	//dates
	        Date startd = null, endd = null;
	        if (response.contractInfoField.effectiveDateField != null && response.contractInfoField.effectiveDateField.year() != 1)
	            startd = response.contractInfoField.effectiveDateField.dateGMT();
	        if (response.contractInfoField.expirationDateField != null && response.contractInfoField.expirationDateField.year() != 1)
	            endd = response.contractInfoField.expirationDateField.dateGMT();
	        
	        if (startd != null || endd != null)
	            contract.effectiveDateField = '<B>' + (startd == null ? '' : startd.format()) + ' - ' + (endd == null ? '' : endd.format()) + '</B>';
	        
	        //dealers
	        if (response.dealersField.ContractDealerType != null) {
	            for (CPQ_ProxyContractInfoDataType.ContractDealerType t : response.dealersField.ContractDealerType) {
	            	Dealers cus;
	            	if (dealerMap.containsKey(t.shipToField))
	            		cus = dealerMap.get(t.shipToField);
	            	else {
	            		cus = new Dealers();
	            		dealerMap.put(t.shipToField, cus);
	            		contract.dealerList.add(cus);
	            	}
	            	
	                cus.billToField = t.billToField;
	                cus.cityField = t.cityField;
	                cus.effectiveDateField = t.effectiveDateField;
	                cus.expirationDateField = t.expirationDateField;
	                cus.nameField = t.nameField;
	                cus.shipToField = t.shipToField;
	                cus.stateField = t.stateField;
	                cus.zipCodeField = t.zipCodeField;
	            }
	        }
        	
        	//items
	        if (response.itemsField.ContractItemType != null) {
	            for (CPQ_ProxyContractInfoDataType.ContractItemType t : response.itemsField.ContractItemType) {
					Items it;
	            	if (itemMap.containsKey(t.itemNumberField))
	            		it = itemMap.get(t.itemNumberField);
	            	else {
	            		it = new Items();
		                it.alternateUOMCaseQuantityField = t.alternateUOMCaseQuantityField;
		                it.alternateUOMField = t.alternateUOMField;
		                it.itemDescriptionField = t.itemDescriptionField;
		                it.itemNumberField = t.itemNumberField;
		                it.marginField = t.marginField;
		                it.quantityBreakField = t.quantityBreakField;
		                it.salesClassField = t.salesClassField;
		                it.suggestedResaleField = t.suggestedResaleField;
		                it.effectiveDateField = t.effectiveDateField;
		                it.expirationDateField = t.expirationDateField;
		                it.unitOfMeasureField = t.unitOfMeasureField;
		                it.priceField = 0;
		                
	            		itemMap.put(t.itemNumberField, it);
	            		contract.itemList.add(it);
	            	}
	            	
	            	if (it.priceField != t.dealerNetField && t.dealerNetField != 0)
	                	it.priceField = t.dealerNetField;
	                
	                if (it.priceField != t.directPriceField && t.directPriceField != 0)
	                	it.priceField = t.directPriceField;
	           	}
	        }
        }
        
        return contract;
	}
}