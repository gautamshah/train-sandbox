global class Batch_Perge_Pricing_API_Data implements Database.Batchable<Sobject>{
    /****************************************************************************************
     * Name    : Batch_Perge_Pricing_API_Data 
     * Author  : Nathan Shinn
     * Date    : 12/09/2011 
     * Purpose : Batch Job used to clean up records from the bi-directional, pricing API.
     * Dependencies: Territory Object
     *
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 12/09/2011  Nathan Shinn         Created
     *
     *****************************************************************************************/
      
    public String query='select Id from PRICING_INTEGRATION_SKU_TYPE__c where createdDate < TODAY';
    public String testSKU = 'SKUCode__c';
    
    global database.querylocator start(Database.BatchableContext BC)
    {
        //if(Test.isRunningTest())
        //  query = 'select Id from PRICING_INTEGRATION_SKU_TYPE__c where SKUCode__c = \''+testSKU+'\'';
          
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, Sobject[] scope)
    {
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
    
    static testMethod void testThisClass() {
        
        PRICING_INTEGRATION_SKU_TYPE__c pi = new PRICING_INTEGRATION_SKU_TYPE__c(SalesClass__c = 'Test'
                                                                                ,SKUCode__c  = 'TEST-'+system.now()
                                                                                ,UOM__c = 'li'
                                                                                ,PricingRequestId__c = '111');
                                                                                
        insert pi;
        
        PRICING_INTEGRATION__c pic = new    PRICING_INTEGRATION__c(PRICING_INTEGRATION_SKU_TYPE__c = pi.Id);
        
        insert pic;
        
       // system.assertEquals(pi.Id, [select PRICING_INTEGRATION_SKU_TYPE__c from PRICING_INTEGRATION__c where Id = :pic.Id].PRICING_INTEGRATION_SKU_TYPE__c);
        
        Test.startTest();
        
           Batch_Perge_Pricing_API_Data b = new Batch_Perge_Pricing_API_Data();
           b.query='select Id from PRICING_INTEGRATION_SKU_TYPE__c where createdDate >= TODAY';
           b.testSKU = pi.SKUCode__c;
           Database.executeBatch(b);
           
        Test.stopTest();
        
  //    system.assertEquals(0, [select count() from PRICING_INTEGRATION__c where Id = :pic.Id]);
        
    }

}