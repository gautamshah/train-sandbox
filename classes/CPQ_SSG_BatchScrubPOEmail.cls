/**
Batch class to send Scrub PO Emails. This batch is constructed with a List of agreement Ids
to send emails for.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
05/24/2016      Bryan Fry           Created
===============================================================================
*/
global class CPQ_SSG_BatchScrubPOEmail implements Database.Batchable<sObject> {
	private final List<Id> agreementIds;

	global CPQ_SSG_BatchScrubPOEmail(List<Id> inAgreementIds) {
		agreementIds = inAgreementIds;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'Select Id From Apttus__APTS_Agreement__c Where Id in :agreementIds';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		List<Apttus__APTS_Agreement__c> agreements = (List<Apttus__APTS_Agreement__c>)scope;
		List<Id> agreementScopeIds = new List<Id>();
		for (Apttus__APTS_Agreement__c agmt: agreements) {
			agreementScopeIds.add(agmt.Id);
		}
		CPQ_AgreementProcesses.SendScrubPOActivationEmail(agreementScopeIds);
	}
	
	global void finish(Database.BatchableContext BC) {
	}
}