public class Product2_Price_c
{

	public void before()
	{
		Id productId = null;
		User usr = cpqUser_c.CurrentUser;
		Product2 prod = [SELECT Id, Name, ProductCode, Family, Apttus_Surgical_Product__c, UOM_Desc__c, GBU__c, Catalog_Price__c, Unit_Quantity__c FROM Product2 WHERE Id = :productId];
		Product2 replace;
		//ProductPrice prodP;

		Boolean isMatchProduct = false;

		if( prod.Apttus_Surgical_Product__c &&
				// 2016.06.09 - Changed reference from prod.Family to prod.GBU__c
				( prod.GBU__c == usr.Business_Unit__c ||
				  ( prod.GBU__c == 'S2' &&
					  usr.Business_Unit__c == 'Surgical Innovations (SI)'
					)
				)
			){
			isMatchProduct = true;
		}

		if(!isMatchProduct){

		}

	}

	public void test()
	{
		Id productId = null;
		// This will always return a record if you are logged in as a User
		User user = cpqUser_c.CurrentUser;

		List<Product2> product = [SELECT Id
					                     ,Name
					                     ,ProductCode
					                     ,Family
					                     ,UOM_Desc__c
					                     ,Catalog_Price__c
					                     ,Unit_Quantity__c
                                  FROM Product2
                                  WHERE Id = :productId AND
                                        OrganizationName__c = :user.OrganizationName__c];

		if (!product.isEmpty())
		{

		}
	}
}