/**
Test class for cpq_u and cpq_utilities (until deprecated).

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
10/18/2016      Isaac Lewis         Created.
10/24/2016      Isaac Lewis         Migrated test methods from Test_CPQ_Utilities.
                                    Methods should be refactored to use cpq_u
                                    after cpq_utilities is deprecated.
11/07/2016      Isaac Lewis         Uncommented tests and updated to pass after refactor
11/07/2016      Isaac Lewis         Added test for getting sObjectType from sObject name
01/23/2017      Isaac Lewis         Updated itShouldGetStandardPriceBookId to test against Test.getStandardPriceBookId()
??/??/????      Paul Berglund       Added miscellaneous methods
01/24/2017      Isaac Lewis         Added test coverage to increase for Av-280
01/31/2017      Isaac Lewis         Added non standard term request fields to myUnitTest3 to bypass validation errors
===============================================================================
*/
@isTest
public with sharing class cpq_uTest
{

    @isTest
    static void itShouldGetMultiPicklistSystemProperty ()
    {

        //cpq_test.buildScenario_CPQ_ConfigSettings();

        Test.startTest();

        // Set<String> result = cpq_u.getMultiPicklistSystemProperty('SSG_Deal_Types__c');
        Set<String> result = cpqConfigSetting_c.getMultiPicklistSystemProperty(
            Schema.SObjectType.CPQ_Config_Setting__c.fields.SSG_Deal_Types__c.getSObjectField());
        System.assert(result.size() > 0);
        System.assert(result.contains('Scrub PO'));

        Test.stopTest();

    }

    @isTest
    static void itShouldGetMultiPicklistValueSet ()
    {

        String value = 'A;B;C';

        Set<String> result = DATATYPES.getMultiPicklistValueSet(value);

        System.assertEquals(3,result.size());
        System.assert(result.contains('A'));

    }

    @isTest static void itShouldGetSetInnerJoin () {

        Set<String> X = new Set<String>{'A','B','C'};
        Set<String> Y = new Set<String>{'A','B'};

        Set<String> result = DATATYPES.getSetInnerJoin(X,Y);

        System.assertEquals(2,result.size());
        System.assert(result.contains('A'));
        System.assert(result.contains('B'));
        System.assert(!result.contains('C'));

    }

    @isTest static void itShouldGetSystemProperties () {

        //cpq_test.buildScenario_CPQ_ConfigSettings();

        Test.startTest();

        CPQ_Config_Setting__c result;

        // result = cpq_u.getSystemProperties();
        result = cpqConfigSetting_c.SystemProperties;
        System.assertNotEquals(NULL,result);
        // System.assertEquals(1,Limits.getQueries());

        // // result = cpq_u.getSystemProperties();
        // result = cpqConfigSetting_c.SystemProperties;
        // System.assertEquals(1,Limits.getQueries());

        delete result;

        // cpq_u.getSystemProperties();
        result = cpqConfigSetting_c.SystemProperties;

        Test.stopTest();

    }

    @isTest static void itShouldGetSalesOrg () {

        //cpq_test.buildScenario_CPQ_ConfigSettings();

        Test.startTest();

        // These should be depricated since we are switching to OrganizationNames__c
        ///System.assertEquals(OrganizationNames_g.Abbreviations.Unknown, cpq_u.getSalesOrg(''), 'If there are no Sales Orgs assigned, return unknown');
        ///System.assertEquals(cpq_u.SalesOrg.Surgical, cpq_u.getSalesOrg('Surgical'), 'If the name is Surgical, return Surgical');
        ///System.assertEquals(cpq_u.SalesOrg.RMS, cpq_u.getSalesOrg('RMS'), 'If the name is RMS, return RMS');

        Test.stopTest();

    }

    @isTest static void itShouldGetSObjectTypeFromSObjectName () {
        System.assertEquals(Apttus__APTS_Agreement__c.SObjectType,
            sObject_u.getSObjectType(cpq_u.AGREEMENT_DEV_NAME));
        System.assertEquals(Apttus_Proposal__Proposal__c.SObjectType,
            sObject_u.getSObjectType(cpq_u.PROPOSAL_DEV_NAME));
        System.assertEquals(Account.SObjectType,
            sObject_u.getSObjectType('Account'));
    }

    @isTest static void itShouldCheckIsValidBusinessDay () {
        DateTime sunday = DateTime.newInstanceGMT(2017,1,1,1,1,1);
        DateTime monday = DateTime.newInstanceGMT(2017,1,2,1,1,1);
        System.assertEquals(false, cpq_u.isLocalBusinessDay(sunday));
        System.assertEquals(true, cpq_u.isLocalBusinessDay(monday));
    }

    @isTest static void itShouldCheckIsValidId () {
        Account acc = cpqAccount_TestSetup.generateAccounts(1).get(0);
        insert acc;
        System.assertEquals(true, DATATYPES.isValidId(acc.Id));
        System.assertEquals(false, DATATYPES.isValidId('badId'));
    }

    // Erroring out due to udpate restrictions from CustomQuoteApprovalsTrigger
    // Not needed because cpq_u is already over 90%, but this trigger needs to be addressed!
    // @isTest static void itShouldCheckIsRecordDestinedToPartner () {
    //
    //  Set<String> rtNames = new Set<String>{'COOP_Plus_Program','Respiratory_Solutions_COOP'};
    //  RecordType rt = [SELECT DeveloperName FROM RecordType WHERE SObjectType = :cpq_u.PROPOSAL_DEV_NAME
    //                  AND DeveloperName NOT IN :rtNames LIMIT 1];
    //
    //  cpq_test.buildScenario_CPQ_Deal(cpq_u.PROPOSAL_SOBJECT_TYPE,rt.DeveloperName);
    //
    //  Apttus_Proposal__Proposal__c proposal = cpq_test.testProposals.get(0);
    //
    //  List<CPQ_Variable__c> vars = [SELECT Name, Value__c FROM CPQ_Variable__c];
    //  CPQ_Variable__c sendCOOPDealsToPartner;
    //  CPQ_Variable__c sendCOOPADJDealToPartner;
    //  for (CPQ_Variable__c var : vars) {
    //      if (var.Name == 'Send COOP Deals to Partner') {
    //          sendCOOPDealsToPartner = var;
    //      } else if (var.Name == 'Send COOP ADJ Deals to Partner') {
    //          sendCOOPADJDealToPartner = var;
    //      }
    //  }
    //
    //  Test.startTest();
    //
    //      // Not COOP deal type
    //      proposal = [SELECT Id, Destined_to_Partner__c, RecordTypeId, RecordType.DeveloperName
    //                  FROM Apttus_Proposal__Proposal__c
    //                  WHERE Id = :proposal.Id];
    //      System.assertEquals(true, cpq_u.isRecordDestinedToPartner(proposal));
    //
    //      // COOP deal type, No Send COOP Deals to Partner
    //      proposal.RecordTypeId = cpqDeal_RecordType_c.getProposalRecordTypesByName().get('COOP_Plus_Program').Id;
    //      update proposal;
    //      proposal = [SELECT Id, Destined_to_Partner__c, RecordTypeId, RecordType.DeveloperName
    //                  FROM Apttus_Proposal__Proposal__c
    //                  WHERE Id = :proposal.Id];
    //
    //      sendCOOPDealsToPartner.Value__c = 'Off';
    //      update sendCOOPDealsToPartner;
    //      System.assertEquals(false, cpq_u.isRecordDestinedToPartner(proposal));
    //
    //      sendCOOPDealsToPartner.Value__c = 'On';
    //      update sendCOOPDealsToPartner;
    //      System.assertEquals(true, cpq_u.isRecordDestinedToPartner(proposal));
    //
    //      // COOP Amendment deal type, No Send COOP ADJ Deals to Partner
    //      proposal.Deal_Type__c = 'Amendment';
    //      update proposal;
    //      proposal = [SELECT Id, Destined_to_Partner__c, RecordTypeId, RecordType.DeveloperName
    //                  FROM Apttus_Proposal__Proposal__c
    //                  WHERE Id = :proposal.Id];
    //
    //      sendCOOPADJDealToPartner.Value__c = 'Off';
    //      System.assertEquals(false, cpq_u.isRecordDestinedToPartner(proposal));
    //      System.assertEquals(true, cpq_u.isRecordDestinedToPartner(proposal));
    //
    //  Test.stopTest();
    //
    // }

    @isTest static void itShouldGetRenewalUpliftPercentage () {
        CPQ_Config_Setting__c settings = cpqConfigSetting_c.SystemProperties;
        settings.Renewal_Uplift_perc__c = NULL;
        System.assertEquals(3.00, cpqConfigSetting_c.getRenewalUpliftPercentage());
        settings.Renewal_Uplift_perc__c = 4.00;
        System.assertEquals(4.00, cpqConfigSetting_c.getRenewalUpliftPercentage());
    }

    @isTest static void itShouldGetRequireOpportunityAmountThreshold () {
        CPQ_Config_Setting__c settings = cpqConfigSetting_c.SystemProperties;
        settings.Require_Opportunity_Amount_Threshold__c = NULL;
        System.assertEquals(1500.00, cpqConfigSetting_c.getRequireOpportunityAmountThreshold());
        settings.Require_Opportunity_Amount_Threshold__c = 1600.00;
        System.assertEquals(1600.00, cpqConfigSetting_c.getRequireOpportunityAmountThreshold());
    }

    @isTest static void itShouldGetCustomFieldNames () {

        String calculatedField = 'X18_character_ID__c';

        Set<String> allFields = sObject_c.getCustomFieldNames(Schema.Account.SObjectType, false);

        Set<String> modifyFields = sObject_c.getCustomFieldNames(Schema.Account.SObjectType, true);

        System.assert(allFields.size() > modifyFields.size(),'Returned too many fields');
        System.assert(allFields.contains(calculatedField), calculatedField + ' should be present');
        System.assert(!modifyFields.contains(calculatedField), calculatedField + ' should not be present');

    }

    @isTest static void itShouldGetValidCRN () {
        String customerRelationshipNumber = '1234567890';
        System.assertEquals('4567890', cpq_u.getValidCRN(customerRelationshipNumber));
        System.assertEquals(NULL, cpq_u.getValidCRN('BADINPUT'));
    }

    @isTest static void itShouldGetCOTFilters ()
    {
        // Setup
        CPQ_Config_Setting__c settings;
        List<string> profileNames;
        RecordType pRT;
        RecordType rRT;
        Apttus_Proposal__Proposal__c testProposal;
        User usr;
        Rebate__c rebate;

        settings = cpqConfigSetting_c.SystemProperties;
        
        System.debug('RecordType: ' + settings.North_Star_Proposal_Record_Types__c.split('\r\n').get(0));
        
        pRT = [ SELECT DeveloperName FROM RecordType
                WHERE Name = :settings.North_Star_Proposal_Record_Types__c.split('\r\n').get(0)];
                
        cpqDeal_TestSetup.buildScenario_CPQ_Deal(cpq_u.PROPOSAL_SOBJECT_TYPE,pRT.DeveloperName);
        
        profileNames = settings.North_Star_Profiles__c.split('\r\n');
        
        usr = [SELECT Id FROM User WHERE Profile.Name = :profileNames.get(0) LIMIT 1];
        testProposal = cpq_TestSetup.testProposals.get(0);
        rRT = [ SELECT Id, DeveloperName FROM RecordType
            WHERE sObjectType = 'Rebate__c'
            AND Name = :settings.North_Star_Rebate_Record_Types__c.split('\r\n').get(0)
            LIMIT 1];

        Test.startTest();

            // Empty Input
            System.assertEquals(0, cpqConfigSetting_c.getCOTFilters(null).size());

            // No Users With North Star Profile
            settings.North_Star_Profiles__c = '';
            testProposal.ownerId = usr.Id;
            update testProposal;
            System.assertEquals(0, cpqConfigSetting_c.getCOTFilters(testProposal).size());
            settings.North_Star_Profiles__c = String.join(profileNames,'\r\n');

            // No Rebates
            System.assertEquals(0, cpqConfigSetting_c.getCOTFilters(testProposal).size());
            rebate = cpqRebate_TestSetup.generateRebate(rRT.DeveloperName, testProposal);
            insert rebate;

            // North Star User, North Star Proposal, North Star Rebate
            System.assertEquals(
                settings.Participating_Facility_COTs__c.split('\r\n').size(),
                cpqConfigSetting_c.getCOTFilters(testProposal).size());

        Test.stopTest();

    }

    @isTest static void itShouldFilterAccountsByERP () {

        List<String> ERPATs = new List<String>(ERPAccount_c.VALID_ERP_ACCOUNT_TYPES);

        // for given input account Ids, gets accountIds that have only Tracing ERPs or no valid ERPs
        List<Account> inputs = new List<Account>();

        List<Account> inputsNoERP = cpqAccount_TestSetup.generateAccounts(5);
        inputs.addAll(inputsNoERP);

        List<Account> inputsWithERP_NoSX = cpqAccount_TestSetup.generateAccounts(5);
        inputs.addAll(inputsWithERP_NoSX);

        List<Account> inputsWithERP = cpqAccount_TestSetup.generateAccounts(5);
        inputs.addAll(inputsWithERP);

        insert inputs;

        System.debug('inputsNoERP: ' + inputsNoERP);
        System.debug('inputsWithERP_NoSX: ' + inputsWithERP_NoSX);
        System.debug('inputsWithERP: ' + inputsWithERP);
        System.debug('inputs: ' + inputs);

        List<ERP_Account__c> erpRecords = new List<ERP_Account__c>();

        List<ERP_Account__c> erpRecords_SX = cpqERPAccount_TestSetup.generateERPAccounts(inputsWithERP,1);
        erpRecords.addAll(erpRecords_SX);

        List<ERP_Account__c> erpRecords_NoSX = cpqERPAccount_TestSetup.generateERPAccounts(inputsWithERP_NoSX,1);
        for (ERP_Account__c erpa : erpRecords_NoSX) {
            erpa.ERP_Account_Type__c = ERPATs.get(ERPATs.size()-1);
        }
        erpRecords.addAll(erpRecords_NoSX);

        insert erpRecords;

        Set<Id> outputHasNoERPs = new Set<Id>();
        Set<Id> outputHasOnlyTracings = new Set<Id>();

        Test.startTest();
            ERPAccount_c.filterAccountsByERP(inputs,outputHasNoERPs,outputHasOnlyTracings);
            System.assertEquals(5, outputHasNoERPs.size());
            System.assertEquals(5, outputHasOnlyTracings.size());
        Test.stopTest();

    }

    @isTest static void itShouldCreateErrorLogEntry () {

        // WHEN a CPQ Error Log is inserted
        String logType = 'Error';
        String longText = '';
        for (Integer i=0;i<32800;i++) { longText += 'A'; }
        CPQ_Error_Log__c result = cpqErrorLog_c.createErrorLogEntry('operation',logType,'errorMessage','additionalInfo',longText);
        insert result;

        // THEN long strings are truncated
        result = [SELECT Request_Body__c FROM CPQ_Error_Log__c WHERE Id = :result.Id];
        // 32768 = Max length for Request_Body__c field
        System.assertEquals(32768, result.Request_Body__c.length());

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// MIGRATED (Should be refactored after cpq_utilities is removed)
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @isTest static void getNorthStarCOTsToIgnore () {

        // Why does this return here?
        return;

        //cpq_test.buildScenario_CPQ_ConfigSettings();

        Apttus_Proposal__Proposal__c proposal = null;

        List<string> results = cpqConfigSetting_c.getCOTFilters(proposal);
        system.assertEquals(0, results.size(), 'Should return an empty list if the proposal is a null');

        // Test Profiles

        string profileName = 'APTTUS - RMS US Sales (RS+PM)';

        User u = [SELECT Id FROM User
            WHERE Profile.Name = 'APTTUS - RMS US Sales (RS+PM)'
            AND Sales_Org_PL__c IN ('Patient Monitoring', 'Respiratory Solutions')
            AND isActive = true LIMIT 1];
        cpqConfigSetting_c.SystemProperties.North_Star_Profiles__c = profileName;
        cpqConfigSetting_c.SystemProperties.Participating_Facility_COTs__c = '003\r\nDME';
        cpqConfigSetting_c.SystemProperties.North_Star_Proposal_Record_Types__c =
            'Rebate Agreement\r\nRebate Agreement Locked';
        cpqConfigSetting_c.SystemProperties.North_Star_Rebate_Record_Types__c = 'CPP North Star';

        proposal = new Apttus_Proposal__Proposal__c();
        proposal.OwnerId = u.Id;
        proposal.RecordTypeId = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class,'Rebate_Agreement').Id;
        insert proposal;

        results = cpqConfigSetting_c.getCOTFilters(proposal);
        system.assertEquals(0, results.size(), 'Should return an empty list at this point');

		RecordType cpp = RecordType_u.fetch(Rebate__c.class, 'CPP_North_Star');
        Rebate__c r = new Rebate__c();
        r.RecordTypeId = cpp.Id;
        r.Related_Proposal__c = proposal.Id;
        insert r;

        results = CPQ_Utilities.getCOTFilters(proposal);
        system.assertEquals(2, results.size(), 'Should return the number entered');
    }

    @isTest static void getSetTaskOwnerToAgreementOwner () {
        //cpq_test.buildScenario_CPQ_ConfigSettings();
        boolean result;
        result = CPQ_Utilities.getSetTaskOwnerToAgreementOwner('Task_Subjects_to_trigger_setting_Owner__c');
        system.assertEquals(true, result);
        result = CPQ_Utilities.getSetTaskOwnerToAgreementOwner('Some Random Text');
        system.assertEquals(false, result);
    }

    @isTest static void getSendEmailToAgreementOwner () {
        //cpq_test.buildScenario_CPQ_ConfigSettings();
        boolean result;
        result = CPQ_Utilities.getSendEmailToAgreementOwner('Task_Subjects_to_trigger_email_to_Owner__c');
        system.assertEquals(true, result);
        result = CPQ_Utilities.getSendEmailToAgreementOwner('Some Random Text');
        system.assertEquals(false, result);
    }

    @isTest static void getAllowedToSeeCOOPPricingFile () {
        //cpq_test.buildScenario_CPQ_ConfigSettings();
        boolean result;
        result = CPQ_Utilities.getAllowedToSeeCOOPPricingFile('CRM Admin Support');
        system.assertEquals(true, result);
        result = CPQ_Utilities.getAllowedToSeeCOOPPricingFile('Some Random Text');
        system.assertEquals(false, result);
    }

    @isTest static void boolToInt () {
        system.assertEquals(1, CPQ_Utilities.boolToInt(true), 'True should return 1');
        system.assertEquals(0, CPQ_Utilities.boolToInt(false), 'False should return 0');
    }

    @isTest static void ValidCRN () {
        system.assertEquals('999', CPQ_Utilities.ValidCRN('0030000000999'), 'Should match the first 3 characters');
        system.assertEquals(null, CPQ_Utilities.ValidCRN(null), 'Should throw an error and be caught by catch()');
    }

    @isTest static void MultiPicklistValues () {
        system.assertEquals(3, CPQ_Utilities.MultiPicklistValues('one;two;three').size(),
            'Should return a set of 3 values');
    }

    @isTest static void listToIDInList () {
        system.assertEquals(
            '\'^^THERE IS NOTHING IN THE INLIST^^\'',
            CPQ_Utilities.listToIDInList(new List<sObject>()),
            'Should match if the input is null');

        List<sObject> inputs = new List<sObject>{
            new Account(Id = '001000000000001'),
            new Contact(Id = '003000000000002'),
            new User(Id = '005000000000003') };
        system.assertEquals( '\'001000000000001AAA\', \'003000000000002AAA\', \'005000000000003AAA\'', CPQ_Utilities.listToIDInList(inputs), 'Should match the Id values from the sObjects input');
    }

    @isTest static void itShouldCalculateDurationOption () {

        System.assertEquals(null,CPQ_Utilities.calculateDurationOption(0));
        System.assertEquals('1/4 years',CPQ_Utilities.calculateDurationOption(3));
        System.assertEquals('1/2 years',CPQ_Utilities.calculateDurationOption(6));
        System.assertEquals('3/4 years',CPQ_Utilities.calculateDurationOption(9));
        System.assertEquals('1 year',CPQ_Utilities.calculateDurationOption(12));
        System.assertEquals('1 1/4 years',CPQ_Utilities.calculateDurationOption(15));
        System.assertEquals('1 1/2 years',CPQ_Utilities.calculateDurationOption(18));
        System.assertEquals('1 3/4 years',CPQ_Utilities.calculateDurationOption(21));
        System.assertEquals('2 years',CPQ_Utilities.calculateDurationOption(24));

    }

    @isTest static void itShouldCalculateDurationInMonth () {

        System.assertEquals(null,CPQ_Utilities.calculateDurationInMonth(null));
        System.assertEquals(null,CPQ_Utilities.calculateDurationInMonth('4/5'));
        System.assertEquals(15,CPQ_Utilities.calculateDurationInMonth('1 1/4 Year'));
        System.assertEquals(18,CPQ_Utilities.calculateDurationInMonth('1 1/2 Year'));
        System.assertEquals(21,CPQ_Utilities.calculateDurationInMonth('1 3/4 Year'));
        System.assertEquals(null,CPQ_Utilities.calculateDurationInMonth('4/5 Year'));
        System.assertEquals(3,CPQ_Utilities.calculateDurationInMonth('1/4 Year'));
        System.assertEquals(6,CPQ_Utilities.calculateDurationInMonth('1/2 Year'));
        System.assertEquals(9,CPQ_Utilities.calculateDurationInMonth('3/4 Year'));

    }

    @isTest static void itShouldTranslateUOMText () {

        System.assertEquals('CT',CPQ_Utilities.translateUOMText('Carton'));
        System.assertEquals('BX',CPQ_Utilities.translateUOMText('Box'));
        System.assertEquals('PK',CPQ_Utilities.translateUOMText('Package'));
        System.assertEquals('GA',CPQ_Utilities.translateUOMText('Gallon'));
        System.assertEquals('BG',CPQ_Utilities.translateUOMText('Bag'));
        System.assertEquals('EA',CPQ_Utilities.translateUOMText('NONE'));

    }

    @isTest static void itShouldGetEndDate () {
        // This should be asserted because it deals with business day logic
        CPQ_Utilities.GetEndDate(System.now(), 1);
    }

    // see cpqPriceList_cTest
    // @isTest static void itShouldGetPriceLists () {}

	////
	//// Not sure how this worked - you can't use a List<sObject> to add records
	//// that depend on parent Id's unless you create a proxy record for it as well.
	//// I removed the insert objs and  added the individual inserts.
	////
    @isTest static void myUnitTest2()
    {
        //cpq_test.buildScenario_CPQ_ConfigSettings();

        List<sObject> objs = new List<sObject>();

        Apttus_Proposal__Proposal__c testRelatedProposal;
        for(integer i = 0; i < 50; i++) {
            testRelatedProposal = new Apttus_Proposal__Proposal__c(
                Apttus_Proposal__Proposal_Name__c = 'Test Related Proposal'+i,
                RecordTypeID = cpqDeal_RecordType_c.getProposalRecordTypesByName().get('Locally_Negotiated_Agreement_LNA_Locked').Id);
            objs.add(testRelatedProposal);
        }
        
		insert testRelatedProposal;

        testRelatedProposal = new Apttus_Proposal__Proposal__c(
            Apttus_Proposal__Proposal_Name__c = 'Test Related Proposal',
            RecordTypeID = cpqDeal_RecordType_c.getProposalRecordTypesByName().get('Locally_Negotiated_Agreement_LNA_Locked').Id);
        objs.add(testRelatedProposal);
        
		insert testRelatedProposal;

        Apttus__APTS_Agreement__c testMasterAgreement = new Apttus__APTS_Agreement__c(
            Name = 'Test Master Agreement',
            Apttus_QPComply__RelatedProposalId__c = testRelatedProposal.ID,
            RecordTypeID = cpqDeal_RecordType_c.getAgreementRecordTypesByName().get('Locally_Negotiated_Agreement_LNA_AG_Locked').Id);
        objs.add(testMasterAgreement);
        
		insert testMasterAgreement;

        Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(
            Apttus_Proposal__Proposal_Name__c = 'Test Proposal',
            RecordTypeID = cpqDeal_RecordType_c.getProposalRecordTypesByName().get('Amendment_to_Add_Facilities_Locked').Id,
            Apttus_QPComply__MasterAgreementId__c = testMasterAgreement.ID);
        objs.add(testProposal);

		insert testProposal;

        Apttus__APTS_Agreement__c testAgreement = new Apttus__APTS_Agreement__c(
            Name = 'Test Agreement',
            Apttus_QPComply__RelatedProposalId__c = testProposal.ID,
            RecordTypeID = cpqDeal_RecordType_c.getAgreementRecordTypesByName().get('Amendment_to_Add_Facilities_AG').Id);
        objs.add(testAgreement);

		insert testAgreement;

        Apttus_Proposal__Proposal__c testProp = [
            SELECT  RecordType.DeveloperName,
                    Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                    RecordType.Name,
                    Apttus_QPComply__MasterAgreementId__r.RecordType.Name
            FROM Apttus_Proposal__Proposal__c WHERE ID = :testProposal.ID];

        Apttus__APTS_Agreement__c testAg = [
            SELECT  RecordType.DeveloperName,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                    RecordType.Name,
                    Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.Name
            FROM Apttus__APTS_Agreement__c WHERE ID = :testAgreement.ID];

        test.startTest();

            CPQ_Utilities.getCoreDealTypeLabel('A-AG Locked');
            CPQ_Utilities.getCoreDealTypeLabel(testProp);
            CPQ_Utilities.getCoreDealTypeName(testProp);
            CPQ_Utilities.getRecordTypeDeveloperName(testProp);
            CPQ_Utilities.getCoreDealTypeLabel(testAg);
            CPQ_Utilities.getCoreDealTypeName(testAg);
            CPQ_Utilities.getRecordTypeDeveloperName(testAg);

        test.stopTest();
    }

    @isTest static void myUnitTest3 () {

        //cpq_test.buildScenario_CPQ_ConfigSettings();

        test.startTest();

            Apttus_Proposal__Proposal__c testProposal2 = new Apttus_Proposal__Proposal__c(
                Apttus_Proposal__Proposal_Name__c = 'Test Proposal 2',
                RecordTypeID = cpqDeal_RecordType_c.getProposalRecordTypesByName().get('COOP_Plus_Program').Id);
            insert testProposal2;
            CPQ_Utilities.isProposalDestinedToPartner([
                SELECT RecordType.DeveloperName, RecordType.Name, Destined_to_Partner__c, Deal_Type__c
                FROM Apttus_Proposal__Proposal__c WHERE ID = :testProposal2.ID]);

            Apttus__APTS_Agreement__c testAgreement2 = new Apttus__APTS_Agreement__c(
                Name = 'Test Agreement',
                RecordTypeID = cpqDeal_RecordType_c.getAgreementRecordTypesByName().get('COOP_Plus_Program_AG_Locked').Id);
            insert testAgreement2;
            CPQ_Utilities.isAgreementDestinedToPartner([
                SELECT RecordType.DeveloperName, RecordType.Name, Destined_to_Partner__c, Deal_Type__c
                FROM Apttus__APTS_Agreement__c WHERE ID = :testAgreement2.ID]);

        test.stopTest();

    }

    // see cpqDeal_uTest.isRecordDestinedToPartner
    // @isTest static void myUnitTest4 () {
    // }

}