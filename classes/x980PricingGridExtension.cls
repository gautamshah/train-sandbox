public class x980PricingGridExtension {

    public x980PricingGridExtension(ApexPages.StandardController controller) {

    }


    public x980PricingGridExtension(ApexPages.StandardSetController controller) {

    }

    public List<X980_Pricing_Grid__c> getPricingGrid() {
        List<X980_Pricing_Grid__c> grid = [SELECT Id, Name, ASP__c, Description__c, From_10_upto_and_including_19_vents__c, From_2_upto_and_including_9_vents__c,
                                           Less_than_2_vents__c, Twenty_or_more_vents__c FROM X980_Pricing_Grid__c ORDER BY Sort_Order__c];
                                      
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(grid);
        
        return (ssc.getRecords());
    }

    public boolean displayPopup {get; set;}
    
    public void closePopup() {
        displayPopup = false;
    }
    
    public void showPopup() {
        displayPopup = true;
    }
}