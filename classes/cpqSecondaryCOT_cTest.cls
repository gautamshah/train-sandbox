/*
cpqSecondaryCOT_cTest

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-12/06      Henry Noerdlinger   Created
===============================================================================
*/
@isTest
public class cpqSecondaryCOT_cTest {
	
	@isTest static void itCreatesSecondaryCotExpressionForParticipatingFacility() 
	{
		cpqPrimaryCOT_c primaryForUseWithSecondary = new cpqPrimaryCOT_c('-003');
		cpqClassOfTrade_c secondary = new cpqSecondaryCOT_c(primaryForUseWithSecondary, '07H');
		String expectedValue = ' (  ( Class_of_Trade__c IN (\'003\')) AND Secondary_Class_of_Trade__c = \'07H\' ) ';
		System.assertEquals(expectedValue.trim(), secondary.buildParticipatingFacilityExpression(null).trim());
		
	}
	
	@isTest static void itCreatesSecondaryCotExpressionForParticipatingFacilityWithObjectPrefix() 
    {
        cpqPrimaryCOT_c primaryForUseWithSecondary = new cpqPrimaryCOT_c('-003');
        cpqClassOfTrade_c secondary = new cpqSecondaryCOT_c(primaryForUseWithSecondary, '07H');
        String expectedValue = ' (  ( Account__r.Class_of_Trade__c IN (\'003\')) AND Account__r.Secondary_Class_of_Trade__c = \'07H\' ) ';
        System.assertEquals(expectedValue.trim(), secondary.buildParticipatingFacilityExpression('Account__r').trim());
        
    }

	@isTest static void itCreatesSecondaryCotExpressionForDistributor() 
	{
		cpqPrimaryCOT_c primaryForUseWithSecondary = new cpqPrimaryCOT_c('-004');
		cpqClassOfTrade_c secondary = new cpqSecondaryCOT_c(primaryForUseWithSecondary, '07J');
		
		String expectedValue = ' ( NOT ( Account__r.Class_of_Trade__c IN (\'004\') AND Account__r.Secondary_Class_of_Trade__c = \'07J\' ) )';
		System.assertEquals(expectedValue.trim(), secondary.buildDistributorExpression('Account__r').trim());
		
	}
	
}