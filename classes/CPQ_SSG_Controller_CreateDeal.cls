/****************************************************************************************
 * Name    : CPQ_SSG_Controller_CreateDeal
 * Author  : Isaac Lewis
 * Date    : 04-25-2016
 * Purpose : Central initialization logic for all SSG deals created via mobile (Lightning) or desktop (VisualForce). Also provides REST API for CPQ_SSG_CreateDeal.cmp
 * See		 : Test_CPQ_SSG_CreateDeal
 * Dependencies: AuraBundles: [CPQ_SSG_CreateDeal], ApexClasses: [CPQ_SSG_Controller_CreateDeal_VF], ApexPages: [CPQ_SSG_CreateDeal], Buttons: [Account: [Create_Scrub_PO, Create_Smart_Cart_ACCT], Opportunity: [Create_Scrub_PO_OPPY, Create_Smart_Cart_OPPY, Custom_Kit]]
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
public with sharing class CPQ_SSG_Controller_CreateDeal {


	/**
	 * Returns Ids of all RecordTypes available to running user for a given sObject type
	 *
	 * @param		objType 	Schema.SObjectType for target sObject
	 * @return	List<Id>	List of RecordType Ids
	 * @see			http://salesforce.stackexchange.com/questions/5063/finding-if-which-users-have-a-record-type-available-to-them-using-soql
	 */
	private static List<Id> GetAvailableRecordTypeIdsForSObject( Schema.SObjectType objType ) {

	    List<Id> ids = new List<Id>();
	    List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();

	    // If there are 2 or more RecordTypes...
	    if (infos.size() > 1) {
	        for (RecordTypeInfo i : infos) {
						// Ignore the Master Record Type, whose Id always ends with 'AAA'.
						// Check Id because Name can change depending on the user's language.
						if ( i.isAvailable() && !String.valueOf(i.getRecordTypeId()).endsWith('AAA')) {
							ids.add(i.getRecordTypeId());
						}
	        }
	    }
	    // Otherwise there's just the Master record type, so add it in, since it MUST always be available
	    else {
				ids.add(infos[0].getRecordTypeId());
			}
			System.debug('Ids Found: ' + ids);
	    return ids;
	}

	/**
	 * Returns Ids of all RecordTypes available to running user for a given sObject type
	 *
	 * @param		sObjectName 			Developer name of target sObject
	 * @return	List<RecordType>	List of RecordTypes
	 * @see			CPQ_SSG_Controller_CreateDeal.GetAvailableRecordTypeIdsForSObject
	 */
	@AuraEnabled
	public static List<RecordType> getRecordTypes(String sObjectName){

		// Check for Valid Object
		System.debug('Selected sObject: ' + sObjectName);
		Map<String, Schema.SObjectType> gd = sObject_u.Name2sObjectType;
		Schema.SObjectType soType;

		if(!gd.containsKey(sObjectName)){
			throw new AuraHandledException(sObjectName + ' is not a valid object');
			System.debug(LoggingLevel.ERROR, sObjectName + ' is not a valid object');
		}
		soType = gd.get(sObjectName);

		List<Id> rtNames = GetAvailableRecordTypeIdsForSObject(soType);

		// Return Available Record Types
		List<RecordType> rts = [
			SELECT Id, Description, DeveloperName, IsActive, Name, SobjectType
			FROM RecordType
			WHERE Id IN :rtNames
			AND (NOT Name LIKE '%Locked')
		];
		System.debug('Record Types Found: ' + rts);
		return rts;

	}

	/**
	 * Creates a Proposal or Agreement with initialized data
	 *
	 * @param		sObjectName 			Developer name of target sObject (Apttus_Proposal__Proposal__c, Apttus__APTS_Agreement__c)
	 * @param		recordTypeId			RecordType Id for target sObject
	 * @param		parentId					Record Id for parent sObject (Account, Opportunity)
	 * @return	sObject						Generic sObject for record returned
	 */
	@AuraEnabled
	public static sObject createRecord(String sObjectName, Id recordTypeId, Id parentId){

		System.debug('*** ParentId: ' + parentId);

		// GET RECORD TYPE & DEFAULT PRICE LIST
		String ssgPriceListId = CPQ_Utilities.getSSGPriceListID();
		RecordType rt = RecordType_u.fetch(recordTypeId);

		// GET PARENT LOOKUPS
		SObject parentSObject;
		Id accountId;
		Id opportunityId;

		if ( Schema.Account.SObjectType == parentId.getSObjectType() ) {
			parentSObject = [SELECT Id, Name FROM Account WHERE Id = :parentId];
			accountId = (Id) parentSObject.get('Id');
		} else if (Schema.Opportunity.SObjectType == parentId.getSObjectType()) {
			parentSObject = [SELECT Id, Name, AccountId FROM Opportunity WHERE Id = :parentId];
			opportunityId = (Id) parentSObject.get('Id');
			accountId = (Id) parentSObject.get('AccountId');
		}

		System.debug('*** accountId: ' + accountId);
		System.debug('*** opportunityId: ' + opportunityId);

		// SET RECORD NAME (Record Type + Parent Name, 80 Characters Max)
		String namePrefix = rt.Name + ' - ';
		String parentName = (String) parentSObject.get('Name');
		Integer maxLength = 80 - namePrefix.length();
		String name = namePrefix + parentName.left(maxLength);
		System.debug('Total Name Length: ' + name.length());


		// CREATE DEAL (Check sObjectType)
		Schema.SObjectType dealSObjectType = sObject_u.Name2sObjectType.get(sObjectName);

		if ( dealSObjectType == Apttus_Proposal__Proposal__c.sObjectType ) {

			// CREATE PROPOSAL
			Apttus_Proposal__Proposal__c prop = new Apttus_Proposal__Proposal__c();
			prop.Apttus_Proposal__Account__c = accountId;
			prop.Apttus_Proposal__Opportunity__c = opportunityId;
			prop.RecordTypeId = rt.Id;
			prop.Apttus_QPApprov__Approval_Status__c = 'Not Submitted';
			if(Test.isRunningTest()){
				// To accomodate CustomQuoteApprovalsTrigger requirements
				List<Apttus_Proposal__Proposal__c> props = new List<Apttus_Proposal__Proposal__c>();
				props.add(prop);
				Apttus_Proposal__Proposal__c clonedProp = prop.clone(false, true, false, false);
				props.add(clonedProp);
				insert props;
			} else {
				insert prop;
			}
			return prop;

		}
		else if ( dealSObjectType == Apttus__APTS_Agreement__c.sObjectType ) {

			// CREATE AGREEMENT
			Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c ();
			agmt.Apttus__Related_Opportunity__c = opportunityId;
			agmt.Apttus__Account__c = accountId;
			agmt.RecordTypeId = rt.Id;
			agmt.Apttus_CMConfig__PriceListId__c = ssgPriceListId;
			agmt.Name = name;
			agmt.Apttus_Approval__Approval_Status__c = 'Not Submitted';
			agmt.Apttus__Status__c = 'Requested';
			insert agmt;
			return agmt;

		}

		return null;

	}

	/**
	 * Fetches Opportunity selected in component Opportunity lookup field
	 *
	 * @param		oppId 					record Id for Opportunity sObject
	 * @return	Opporutnity			Account sObject
	 */
	@AuraEnabled
	public static Opportunity getOpportunity(String oppId){
		return [SELECT Id, Name, AccountId FROM Opportunity WHERE Id = :oppId];
	}

	/**
	 * Fetches Account selected in component Account lookup field
	 *
	 * @param			accId			 	record Id for Account sObject
	 * @return		account			Account sObject
	 */
	@AuraEnabled
	public static Account getAccount(String accId){
		return [SELECT Id, Name FROM Account WHERE Id = :accId];
	}

	/**
	 * Deletes record by Id reference
	 *
	 * @param		recordId 	Id of record to delete
	 * @return	boolean		true for success, false for errors
	 */
	@AuraEnabled
	public static boolean deleteRecord(Id recordId){
		String objName = recordId.getSObjectType().getDescribe().getName();
		sObject record = sObject_u.Name2sObjectType.get(objName).newSObject(recordId);
		System.debug('Deleting ' + objName + 'with Id ' + recordId);
		try {
			delete record;
		} catch (Exception e) {
			System.debug(e);
			return false;
		}
		return true;
	}

}