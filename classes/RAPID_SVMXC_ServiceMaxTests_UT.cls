@isTest(SeeAllData=true)
private class RAPID_SVMXC_ServiceMaxTests_UT {
    static Account a = new Account(); 
    static Contact c = new Contact(); 
    static SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();    
    static SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
    static SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
    static SVMXC__Site__c loc = new SVMXC__Site__c();
    
    static void createData1(){
        a = new Account(Name='Account1'); insert a;
        c = new Contact(FirstName='C', LastName='L', AccountId=a.id); insert c;
        wo = new SVMXC__Service_Order__c(); wo.SVMXC__Order_Status__c='Closed';
        insert wo;
    } 
   
    static testmethod void test_UpdateLocationTrigger(){
        // Test IP without Site insert and location update
        ip.Name='IP1'; insert ip;
        loc.SVMXC__City__c='city'; insert loc;
        update ip; ip.SVMXC__site__c =loc.id; update ip;
        //Test IP with Site insert
        ip2.Name='IP2'; ip2.SVMXC__site__c =loc.id; insert ip2;
        //Test IP delete Site from previous having one
        ip.SVMXC__Site__c=null; update ip;
    }
    
}