public with sharing class CommunityAddUsers{
public List<Community_User__c> AddUserLst {get; set;}
public Set<Id> CollectExistingUserSet = new Set<ID>();
public List<Community_User__c> theUserLst {get; set;}
public Community_User__c theuser {get;set;}
public List<Account_Affiliation__c> ConnectedWithList = new List<Account_Affiliation__c>();
public List<AccountTeamMember> Sellers = new List<AccountTeamMember>();
public Set<Id> ConnectedWithSet = new Set<Id>();
public Set<Id> SellersSet = new Set<Id>();
public List<Id> SellersList = new List<Id>();
public Integer count{get;set;}
public Id accId;
  public Boolean ShowBilling {get;set;}
  public CommunityAddUsers(ApexPages.StandardController controller) {
       system.debug('count===>>>'+count);
       accId = apexpages.currentpage().getparameters().get('accntid');
       system.debug('accId===>>>'+accId);
       AddUserLst = new List<Community_User__c>();
           if(count>50){
            count = count;
           }else{
           count=50;
          }
      system.debug('count===>>>'+count);
       theUserLst = [SELECT Hospital_Community_User__c,Hospital_Community__c,Id,Name,website__c,Rolodex__c,SFDC_User__c FROM Community_User__c where Hospital_Community__c=:accId limit 100];
           count = count - theUserLst.size();
           for(Community_User__c tempuser:theUserLst){
           system.debug('tempuser===>>>'+tempuser);
               AddUserLst.add(tempuser);
           }
           for(Community_User__c addUserToSet:AddUserLst){
            CollectExistingUserSet.add(addUserToSet.SFDC_User__c);
           }
       ConnectedWithList = [select Affiliated_with__c,AffiliationMember__c from Account_Affiliation__c where AffiliationMember__c=:accId];
       system.debug('ConnectedWithList===>>>'+ConnectedWithList);
       for(Account_Affiliation__c cwl:ConnectedWithList){
          ConnectedWithSet.add(cwl.Affiliated_with__c);
       }
       system.debug('ConnectedWithSet===>>>'+ConnectedWithSet);
       Sellers = [SELECT AccountId,Id,UserId FROM AccountTeamMember where (AccountId IN:ConnectedWithSet) AND (UserId NOT IN:CollectExistingUserSet)];
       system.debug('Sellers===>>>'+Sellers);
       for(AccountTeamMember sell:Sellers){
           system.debug('sell===>>>'+sell);
           if(AddUserLst!=null && AddUserLst.size()>0){
               for(Community_User__c AUL:AddUserLst){
               system.debug('AUL.SFDC_User__c'+AUL.SFDC_User__c);
               system.debug('sell.UserId'+sell.UserId);
                 if(sell.UserId==AUL.SFDC_User__c){
                  
                  }else{
                  SellersSet.add(sell.UserId);
                  system.debug('SellersSet===>>>'+SellersSet);
                  }
               }  
             }
             else{
             SellersSet.add(sell.UserId);
             }       
       }
       for(Id i:SellersSet){
           SellersList.add(i);
       }
       system.debug('SellersSet===>>>'+SellersSet);
       for(Integer i=0;i<count;i++){
        Community_User__c user1 = new Community_User__c();
        user1.Hospital_Community__c = accId;
        system.debug('Account Id ====>'+user1.Hospital_Community__c);
        if(SellersSet!=null && SellersSet.size()>i){
            user1.SFDC_User__c = SellersList[i];
        }
        AddUserLst.add(user1);
       }
    }
    public pageReference Save(){
     List<Community_User__c> CreateUserLst = new List<Community_User__c>();
     List<Community_User__c> DelUserLst = new List<Community_User__c>();
     List<Id> DelUserIdLst = new List<Id>();
      
      for (Community_User__c user2:AddUserLst){
            Community_User__c user3 = new Community_User__c();
            user3.Hospital_Community__c = user2.Hospital_Community__c;
            user3.SFDC_User__c = user2.SFDC_User__c;
            user3.Hospital_Community_User__c= user2.Hospital_Community_User__c;
            user3.Rolodex__c = user2.Rolodex__c;
            if(user3.SFDC_User__c!=null && (user3.Hospital_Community_User__c==true || user3.Rolodex__c==true)){
                  for(Community_User__c exuser:theUserLst){
                      if(user3.SFDC_User__c==exuser.SFDC_User__c){
                          user3.Id = exuser.Id;
                      }
                  }
              CreateUserLst.add(user3);
              system.debug('CreateUserLst===>>>'+CreateUserLst);
              }
              
              else if(user3.SFDC_User__c!=null && (user3.Hospital_Community_User__c!=true && user3.Rolodex__c!=true)){
                for(Community_User__c exuser:theUserLst){
                      if(user3.SFDC_User__c==exuser.SFDC_User__c){
                          user3.Id = exuser.Id;
                      }
                  }
                  DelUserIdLst.add(user3.Id);
              }
            system.debug('DelUserIdLst===>>>'+DelUserIdLst);
        }
         system.debug('CreateUserLst===>>>'+CreateUserLst);
         system.debug('DelUserIdLst===>>>'+DelUserIdLst);
         DelUserLst = [select Id from Community_User__c where Id IN:DelUserIdLst];
         try{
         upsert CreateUserLst; 
         }catch(Exception e)
         {ShowBilling =true;
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'User Already Inserted'));
          
         }
         Delete DelUserLst;
         PageReference reRend = new PageReference('/'+accId);
        reRend.setRedirect(true);
        return reRend;
      
    }
    public pageReference Next(){
    Integer countPrevious = this.count;
    this.count = this.count+50;
    system.debug('count===>>>'+count);
    
     for(Integer j=countPrevious;j<count;j++){
     Community_User__c NextUser = new Community_User__c();
        NextUser.Hospital_Community__c = accId;
        AddUserLst.add(NextUser);
     }
     return null;
    
    }
     public pageReference Cancel(){
     PageReference cancl = new PageReference('/'+accId);
     cancl.setRedirect(true);
     return cancl;
    }
}