public without sharing virtual class dmDocument extends dm
{
	// 
	// TRIGGER METHODS (BULKIFIED)
	//
    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<dmDocument__c> newList,
            Map<Id, dmDocument__c> newMap,
            List<dmDocument__c> oldList,
            Map<Id, dmDocument__c> oldMap,
            integer size)
    {
        if (isBefore)
        {
            if (isInsert)
            {
                dm.onInsert(newList);
            }
        }
        else
        {
        	if (isInsert)
        	{
        		afterInsert(newList);
        	}
        }
    }
            
  	private static void afterInsert(List<dmDocument__c> newList)
  	{
  		system.debug('dmDocument.afterInsert.newList: ' + newList);
  		
  		if (newList == null || newList.size() == 0) return;
  		
	    List<dmDocument__Share> shares = createAdminSharesForRecords(newList);
	    insert shares;
  	}

	//
	// CONSTRUCTORS
	//
	public dmDocument() { }
	
    public dmDocument(ApexPages.StandardSetController setcontroller)
    {
        super(setcontroller);
    }
    
    public dmDocument(ApexPages.StandardController controller)
    {
        super(controller);
    }
    
    //
    // PROPERTIES
    //
    public dmDocument__c record
    {
        get { return (dmDocument__c)base; }     
        set { base = value; }
    }
    
    public string NameEncoded
    {
    	get
    	{
    		return EncodingUtil.urlEncode(record.Name, 'UTF-8');
    	}	
    }
    
	//
	// COLLECTION METHODS (BULKIFIED)
	//

	public static List<dmDocument__c> fetchRecords()
	{
		return dmDocuments.All.values();
	}
	
    public static List<dmDocument__c> fetchRecords(Set<Id> recordIds)
    {
		List<dmDocument__c> targets = new List<dmDocument__c>();
		
		if (recordIds != null && recordIds.size() > 0)
		
		for(dmDocument__c doc : dmDocuments.All.values())
			if (recordIds.contains(doc.Id))
				targets.add(doc);
		
		return targets;
    }
    
    public static dmDocument__c fetch(Id id)
    {
    	return dmDocuments.All.get(id);
    }
	
    //
    // RECORD METHODS
    //
    public static dmFolder__c ApplicationFolder(Id id) { return (id != null) ? ApplicationFolder(fetch(id)) : null; }
    public dmFolder__c ApplicationFolder { get { return ApplicationFolder(record); } }
    private static dmFolder__c ApplicationFolder(dmDocument__c document)
    {
    	return (document == null || document.Folder__c == null) ? null : dmFolder.ApplicationFolder(document.Folder__c);
    }

    //
    // SHARING
    //
	private static List<dmDocument__Share> createAdminSharesForRecords(List<dmDocument__c> records)
	{
		List<dmDocument__Share> shares = new List<dmDocument__Share>();
		
		if (records == null || records.size() == 0) return shares;
		
		for(dmDocument__c d : records)
		{
			for(dmGroup__c admin : dmGroups.ApplicationId2Groups.get(ApplicationFolder(d).Id))
			{
				Group g = dmGroups.GUID2sObject.get(admin.GUID__c);
				dmDocument__Share share = new dmDocument__Share();
			
				if (g.Name.endsWith(' - View'))
					share.AccessLevel = 'Read';
				else
					share.AccessLevel = 'Edit';
					
				share.put('ParentId', d.Id);
				share.put('UserOrGroupId', g.Id);
				
				shares.add(share);
			}
		}
		return shares;
	}

	@future
	public static void deleteSharing()
	{
		List<dmDocument__Share> shares =
			[SELECT Id
			 FROM dmDocument__Share
			 WHERE AccessLevel IN ('Read', 'Edit')];
			 
		delete shares;
	}		

	@future
	public static void fixSharing()
	{
		List<dmDocument__c> records = fetchRecords();

	    List<dmDocument__Share> shares = createAdminSharesForRecords(records);
	    insert shares;
	}
	
	//
	// VISUAL FORCE PROPERTIES & METHODS
	//
    // Properties & Methods that support the VF component or page
    //
    public void AssignGroup()
    {
    	
    }
    
    public PageReference onLoad()
    {
    	string guid = querystring.get('sObject');
    	
    	if (guid != null) return viewAttachment(guid);

        if (record != null && record.Id != null)
            record = (dmDocument__c)fetchRecord(record.Id);
        return null;
    }

    public static string keyPrefix
    {
        get
        {
            if (keyPrefix == null)
                keyPrefix = dm.keyPrefix(dmDocument__c.class);
            return keyPrefix;
        }
    }    
    
    //
    // VIEW
    //
    public PageReference viewAttachment()
    {
    	return viewAttachment(querystring.get('sObject'));
    }
    
    public PageReference viewAttachment(string guid)
    {
    	return dmAttachment.view(guid);
    }
}