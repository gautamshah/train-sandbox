/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com

MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A               AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies
20170123        IL         AV-280     Created to increase code coverage on dependencies
20170406   A    BF         AV-286     Participating Facility Quantities prevent reconfiguring a finalized cart

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/

@isTest
private class cpqProposalLineItem_uTest {

	@isTest static void filterByCategory () {

		// Create PLIs with different categories
		String HARDWARE = cpqProposalLineitem_u.HARDWARE;
		String TRADE_IN = cpqProposalLineitem_u.TRADE_IN;

		List<Apttus_Proposal__Proposal_Line_Item__c> lineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
		for (Integer i=0; i<10; i++) {
			String chargeType = math.mod(i, 2) == 0 ? HARDWARE : TRADE_IN;
			lineItems.add(new Apttus_Proposal__Proposal_Line_Item__c(
				Apttus_QPConfig__ChargeType__c = chargeType
			));
		}

		// Filtered by Hardware category
		List<Apttus_Proposal__Proposal_Line_Item__c> hardwarePLIs;
		hardwarePLIs = cpqProposalLineitem_u.filterByCategory(lineItems,HARDWARE);
		System.assertEquals(5, hardwarePLIs.size() );
		System.assertEquals(HARDWARE, hardwarePLIs.get(0).Apttus_QPConfig__ChargeType__c);

		// Filtered by Trade-In category
		List<Apttus_Proposal__Proposal_Line_Item__c> tradeInPLIs;
		tradeInPLIs = cpqProposalLineitem_u.filterByCategory(lineItems,TRADE_IN);
		System.assertEquals(5, tradeInPLIs.size() );
		System.assertEquals(TRADE_IN, tradeInPLIs.get(0).Apttus_QPConfig__ChargeType__c);

		// Filtered with bad category
		List<Apttus_Proposal__Proposal_Line_Item__c> noPLIs;
		noPLIs = cpqProposalLineitem_u.filterByCategory(lineItems,'no lines with category');
		System.assertNotEquals(null, noPLIs);
		System.assertEquals(0, noPLIs.size());

	}

	@isTest static void deleteProposalLineItems() {
		Schema.RecordTypeInfo acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('US-Healthcare Facility');

		Account acct = new Account(Status__c = 'Active',
								   Name = 'Test Account', 
								   RecordTypeID = acctRT.getRecordTypeId(), 
								   Account_External_ID__c = 'US-111111');
		insert acct;

		ERP_Account__c erp = new ERP_Account__c(Name = 'Test ERP', 
												Parent_Sell_To_Account__c = acct.Id,
												ERP_Account_Type__c = 'S',
												ERP_Source__c = 'E1',
												ERP_Account_Status__c = 'Active');
		insert erp;

		Map<String,Id> mapProposalRType = New Map<String,Id>();
		Map<Id, RecordType> rts = RecordType_u.fetch(Apttus_Proposal__Proposal__c.class);
		for(RecordType R: rts.values())
			mapProposalRType.Put(R.DeveloperName, R.Id);

		Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = 'Test', 
																				 Apttus_Proposal__Account__c = acct.Id, 
																				 RecordTypeID = mapProposalRType.get('Current_Price_Quote'));
		insert proposal;

		Apttus_Proposal__Proposal_Line_Item__c pli = new Apttus_Proposal__Proposal_Line_Item__c(Apttus_Proposal__Proposal__c = proposal.Id,
																								Apttus_QPConfig__ChargeType__c = 'Hardware');
		insert pli;

		cpqParticipatingFacility_LineItem__c pfli = new cpqParticipatingFacility_LineItem__c(Proposal__c = proposal.Id,
																							 ProposalLineItem__c = pli.Id,
																							 ERPRecord__c = erp.Id,
																							 QuantityToBePlacedAtShipTo__c = 1);
		insert pfli;

		Test.startTest();
			delete pli;
		Test.stopTest();

		List<cpqParticipatingFacility_LineItem__c> pflis = [Select Id From cpqParticipatingFacility_LineItem__c];
		System.assertEquals(true, pflis.isEmpty());
	}
}