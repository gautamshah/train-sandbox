/**
Test Class

CPQ_Utilities

========================================================================================================================
========================================================================================================================
============== DO NOT COMMENT OUT TESTS UNLESS CORRESPONDING METHOD IS COMMENTED OUT IN CPQ_UTILITIES.CLS ==============
========================================================================================================================
========================================================================================================================

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2015-03-05      Yuli Fintescu       Created
2015-10-12      Paul Berglund       Added test methods for:
                                    - boolean getSetTaskOwnerToAgreementOwner
                                    - boolean getSendEmailToAgreementOwner
                                    - boolean getAllowedToSeeCOOPPricingFile
11-10-2015      Paul Berglund       Added test methods for:
                                    - List<string> getNorthStarCOTsToIgnore
                                    - boolean validAgreementRecordType
                                    - boolean validProposalRecordType
11/18/2015      Paul Berglund       Added getSalesOrg
02/18/2016      Paul Berglund       Removed @setupTest to allow testing without
                                    a CPQ_Config_Setting__c record
02/24/2016      Paul Berglund       Fixed error in getNorthStarCOTsToIgnore
                                    by selecting an existing User instead of
                                    creating one.
                                    Disabled getNorthStarCOTsToIgnore test,
                                    requires too much effor to get the test
                                    working right now with the deploy date
                                    approaching.
10/19/2016    Isaac Lewis           Removed territory-based approver logic
10/24/2016    Isaac Lewis           Migrated test methods to cpq_uTest
01/24/2016    Isaac Lewis           Added mock method calls to provide coverage to CPQ_Utilities
===============================================================================
*/
@isTest
private class Test_CPQ_Utilities {


    @isTest static void mockTest () {

        // These methods should have deeper testing of the core methods they surface
        // They simply need to be called here for the sake of test coverage

        //cpq_test.buildScenario_CPQ_ConfigSettings();

        try {
            CPQ_Utilities.getCustomFieldNames(Schema.Account.sObjectType,true);
        } catch (Exception e) {}
        try {
            CPQ_Utilities.buildObjectQuery(null,null,null,'Account');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.calculateDurationOption(12);
        } catch (Exception e) {}
        try {
            CPQ_Utilities.calculateDurationInMonth('12');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.translateUOM('BX');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.translateUOMText('Box');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getRMSPriceListID();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getSSGPriceListID();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getMSPriceListID();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getBusinessDayCutOffHour();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getRenewalUpliftPercentage();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getProxyEndpoint();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCallbackChargeTypeIgnoreList();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getDealTypesAllowingTracingCustomer();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getDealTypesRequiringNoCart();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getDealTypesRequiringNoOpportunity();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getDealTypesNotAllowingPriceAdjustment();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getRMSAgreementDirectDealTypes();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getRequireOpportunityAmountThreshold();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getDealTypesRequireOpportunityByAmount();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getProposalValidForAfterPresented();
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getSetTaskOwnerToAgreementOwner('Subject');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getAllowedToSeeCOOPPricingFile('Profile');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getSendEmailToAgreementOwner('Subject');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.listToIDInList(new List<SObject>());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.MultiPicklistValues('a;b;c');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCoreDealTypeLabel('recordTypeName');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCoreDealTypeLabel(new Apttus_Proposal__Proposal__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCoreDealTypeLabel(new Apttus__APTS_Agreement__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCoreDealTypeName(new Apttus_Proposal__Proposal__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCoreDealTypeName(new Apttus__APTS_Agreement__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getRecordTypeDeveloperName(new Apttus_Proposal__Proposal__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getRecordTypeDeveloperName(new Apttus__APTS_Agreement__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.isProposalDestinedToPartner(new Apttus_Proposal__Proposal__c());
        } catch (Exception e) {}
        try {
          CPQ_Utilities.isAgreementDestinedToPartner(new Apttus__APTS_Agreement__c());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.ERPFilter(new List<Account>(), new Set<Id>(), new Set<Id>());
        } catch (Exception e) {}
        try {
            CPQ_Utilities.CreateErrorLogEntry('operation','logType','errorMessage','additional','request');
        } catch (Exception e) {}
        try {
          CPQ_Utilities.GetEndDate(System.now(),1);
        } catch (Exception e) {}
        try {
          CPQ_Utilities.boolToInt(false);
        } catch (Exception e) {}
        try {
          CPQ_Utilities.ValidCRN('ABCDEFG');
        } catch (Exception e) {}
        try {
            CPQ_Utilities.getCOTFilters(new Apttus_Proposal__Proposal__c());
        } catch (Exception e) {}

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// MIGRATED
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // @isTest static void getSalesOrg() {}
    //
    // @isTest static void getNorthStarCOTsToIgnore() {}
    //
    // @isTest static void getSetTaskOwnerToAgreementOwner_true() {}
    //
    // @isTest static void getSetTaskOwnerToAgreementOwner_false() {}
    //
    // @isTest static void getSendEmailToAgreementOwner_true() {}
    //
    // @isTest static void getSendEmailToAgreementOwner_false() {}
    //
    // @isTest static void getAllowedToSeeCOOPPricingFile_true() {}
    //
    // @isTest static void getAllowedToSeeCOOPPricingFile_false() {}
    //
    // @isTest static void boolToInt_true() {}
    //
    // @isTest static void boolToInt_false() {}
    //
    // @isTest static void ValidCRN() {}
    //
    // @isTest static void MultiPicklistValues() {}
    //
    // @isTest static void listToIDInList() {}
    //
    // @isTest static void myUnitTest() {}
    //
    // @isTest static void myUnitTest2() {}
    //
    // @isTest static void myUnitTest3() {}
    //
    // @isTest static void myUnitTest4() {}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// DEPRECATED
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // @isTest
    // static void validAgreementRecordType_true() {
    //     setupCPQ_Utilities();
    //     system.assertEquals(true, CPQ_Utilities.validAgreementRecordType(null), 'If the SystemProperties field is empty, then the result should true');
    //
    //     CPQ_Utilities.SystemProperties.Approver_Agreement_Record_Types__c = 'Same';
    //     system.assertEquals(true, CPQ_Utilities.validAgreementRecordType('Same'), 'If the SystemProperties field is empty, then the result should true');
    // }
    //
    // @isTest
    // static void validProposalRecordType_true() {
    //     setupCPQ_Utilities();
    //     system.assertEquals(true, CPQ_Utilities.validProposalRecordType(null), 'If the SystemProperties field is empty, then the result should true');
    //
    //     CPQ_Utilities.SystemProperties.Approver_Proposal_Record_Types__c = 'Same';
    //     system.assertEquals(true, CPQ_Utilities.validProposalRecordType('Same'), 'If the SystemProperties field is empty, then the result should true');
    // }
    //
    // @isTest
    // static void validRecordType_true() {
    //     setupCPQ_Utilities();
    //
    //     system.assertEquals(true, CPQ_Utilities.validRecordType('ANYTHING', new List<string>{ '+All' }), 'If the List contains +All then the result is true');
    //     system.assertEquals(true, CPQ_Utilities.validRecordType('ANYTHING', new List<string>{ '+ANYTHING' }), 'If the name is included in the List of strings prefixed with +, then the result is true');
    // }
    //
    // @isTest
    // static void validRecordType_false() {
    //     setupCPQ_Utilities();
    //
    //     system.assertEquals(false, CPQ_Utilities.validRecordType(null, new List<string>()), 'If the name of the record is set to null, the result is false');
    //     system.assertEquals(false, CPQ_Utilities.validRecordType('ANYTHING', null), 'Passing null for the List of strings to check will result in false');
    //     system.assertEquals(false, CPQ_Utilities.validRecordType('ANYTHING', new List<string>{ '-All' }), 'If the List of strings contains -All then the result is false');
    //     system.assertEquals(false, CPQ_Utilities.validRecordType('ANYTHING', new List<string>{ '-ANYTHING' }), 'If the name is included in the List of strings prefixed with -, then the result is false');
    // }

}