/*
11/01/2016   Paul Berglund   Replaced CPQ_Config_Setting__c & 'System Properties' setup/insert
                             with call to cpqConfigSetting_cTest.create()
*/
@isTest
public class cpqDeal_Approver_uTest
{
	private static string RSMAssigned = 'Should have RSM assigned';
	private static string RSMNull = 'RSM should be null';
	private static string ZVPAssigned = 'Should have ZVP assigned';
	private static string ZVPNull = 'ZVP should be null';
	private static string SVPAssigned = 'Should have SVP assigned';
	private static string SVPNull = 'SVP should be null';

	@testSetup
    static void Setup()
    {
		User u = cpqUser_c.CurrentUser;
		u.OrganizationName__c = OrganizationNames_g.Abbreviations.RMS.name();
		update u;

        List<RecordType> rts = cpqProposal_c.getRecordTypes();
        insert cpqProposal_c.create(rts[0].Id, 1);
    }

    @isTest
    public static void Agreement()
    {
    	Apttus__APTS_Agreement__c a = cpqAgreement_cTest.create(1);
		// Use a record type that won't trigger CustomOpptyApprovalsTrigger
		a.RecordTypeId = cpqDeal_RecordType_c.getAgreementRecordTypesByName().get('Custom_Kit_Locked').Id;
		// a.RecordTypeId = cpqDeal_RecordType_c.getAgreementRecordTypesById().values()[4].Id;

    	List<User> users = cpqUser_u.fetchReps().values();
    	////List<User> users = [SELECT Id FROM User WHERE Manager.SalesOrganizationLevel__c = '1 - Regional' AND
    	////                                              Manager.Manager.SalesOrganizationLevel__c = '2 - Zone' AND
    	////                                              Manager.Manager.Manager.SalesOrganizationLevel__c = '3 - Organization'];

    	system.runas(users[0])
    	{
    		insert a;

    		a = cpqAgreement_c.fetchApprovers(a.Id);

    		system.assertNotEquals(a.RSM_Approver__c, null, 'Should have RSM assigned');
    		system.assertNotEquals(a.ZVP__c, null, 'Should have ZVP assigned');
    		system.assertNotEquals(a.SVP_User__c, null, 'Should have SVP assigned');

    		a.OwnerId = a.RSM_Approver__c;
    		upsert a;

    		a = cpqAgreement_c.fetchApprovers(a.Id);

    		system.assertEquals(a.RSM_Approver__c, null, 'RSM should be null');
    		system.assertNotEquals(a.ZVP__c, null, 'Should have ZVP assigned');
    		system.assertNotEquals(a.SVP_User__c, null, 'Should have SVP assigned');

    		a.OwnerId = a.ZVP__c;
    		upsert a;

    		a = cpqAgreement_c.fetchApprovers(a.Id);

    		system.assertEquals(a.RSM_Approver__c, null, 'RSM should be null');
    		system.assertEquals(a.ZVP__c, null, 'ZVP should be null');
    		system.assertNotEquals(a.SVP_User__c, null, 'Should have SVP assigned');

    		a.OwnerId = a.SVP_User__c;
    		upsert a;

    		a = cpqAgreement_c.fetchApprovers(a.Id);

    		system.assertEquals(a.RSM_Approver__c, null, 'RSM should be null');
    		system.assertEquals(a.ZVP__c, null, 'ZVP should be null');
    		system.assertEquals(a.SVP_User__c, null, 'SVP should be null');

			for(User u : users)
			{
				if (u.Id != cpqUser_c.CurrentUser.Id &&
				    u.Manager.SalesOrganizationLevel__c == SalesOrganizationLevel_g.REGIONAL)
				{
					a.SellerApprover__c = u.Id;
					break;
				}
			}
    		upsert a;

    		a = cpqAgreement_c.fetchApprovers(a.Id);

    		system.assertNotEquals(a.RSM_Approver__c, null, 'Should have RSM assigned');
    		system.assertNotEquals(a.ZVP__c, null, 'Should have ZVP assigned');
    		system.assertNotEquals(a.SVP_User__c, null, 'Should have SVP assigned');
    	}
    }

    @isTest
    public static void Proposal()
    {
    	Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c();
		// Use a record type that won't trigger CustomOpptyApprovalsTrigger
		p.RecordTypeId = cpqDeal_RecordType_c.getProposalRecordTypesByName().get('Hardware_or_Product_Quote_Locked').Id;
    	// p.RecordTypeId = cpqDeal_RecordType_c.getProposalRecordTypesById().values()[4].Id;

    	List<User> users = cpqUser_u.fetchReps().values();
    	////List<User> users = [SELECT Id FROM User WHERE Manager.SalesOrganizationLevel__c = '1 - Regional' AND
    	////                                              Manager.Manager.SalesOrganizationLevel__c = '2 - Zone' AND
    	////                                              Manager.Manager.Manager.SalesOrganizationLevel__c = '3 - Organization'];

    	system.runas(users[0])
    	{
			System.debug('proposal to insert: ' + p);
    		insert p;
Test.startTest();
    		p = cpqProposal_c.fetchApprovers(p.Id);

    		system.assertNotEquals(p.RSM_Approver__c, null, 'Should have RSM assigned');
    		system.assertNotEquals(p.ZVP__c, null, 'Should have ZVP assigned');
    		system.assertNotEquals(p.SVP_User__c, null, 'Should have SVP assigned');

    		p.OwnerId = p.RSM_Approver__c;
    		upsert p;

    		p = cpqProposal_c.fetchApprovers(p.Id);

    		system.assertEquals(p.RSM_Approver__c, null, 'RSM should be null');
    		system.assertNotEquals(p.ZVP__c, null, 'Should have ZVP assigned');
    		system.assertNotEquals(p.SVP_User__c, null, 'Should have SVP assigned');

    		p.OwnerId = p.ZVP__c;
    		upsert p;

    		p = cpqProposal_c.fetchApprovers(p.Id);

    		system.assertEquals(p.RSM_Approver__c, null, 'RSM should be null');
    		system.assertEquals(p.ZVP__c, null, 'ZVP should be null');
    		system.assertNotEquals(p.SVP_User__c, null, 'Should have SVP assigned');

    		p.OwnerId = p.SVP_User__c;
    		upsert p;

    		p = cpqProposal_c.fetchApprovers(p.Id);

    		system.assertEquals(p.RSM_Approver__c, null, 'RSM should be null');
    		system.assertEquals(p.ZVP__c, null, 'ZVP should be null');
    		system.assertEquals(p.SVP_User__c, null, 'SVP should be null');

			for(User u : users)
			{
				if (u.Id != cpqUser_c.CurrentUser.Id &&
				    u.Manager.SalesOrganizationLevel__c == SalesOrganizationLevel_g.REGIONAL)
				{
					p.SellerApprover__c = u.Id;
					break;
				}
			}
			upsert p;

    		p = cpqProposal_c.fetchApprovers(p.Id);
Test.stopTest();
    		system.assertNotEquals(p.RSM_Approver__c, null, 'Should have RSM assigned');
    		system.assertNotEquals(p.ZVP__c, null, 'Should have ZVP assigned');
    		system.assertNotEquals(p.SVP_User__c, null, 'Should have SVP assigned');
    	}
    }

    @isTest static void ssgODApprover()
    {
        RecordType scrubRT =
        	[SELECT Id
        	 FROM RecordType
        	 WHERE DeveloperName = 'Scrub_PO' AND
        	 	   SObjectType = 'Apttus__APTS_Agreement__c'];

        Map<Id, User> testUsers = cpqUser_u.fetchReps();


		Set<Id> userids = new Set<Id>(); // Orignially had this = to testUsers.keySet(), but got System.FinalException: Collection is read-only
		userids.addAll(testUsers.keySet());

		for(User u : testUsers.values())
		{
			userids.add(u.Manager.Id);
    		userids.add(u.Manager.Manager.Id);
    		userids.add(u.Manager.Manager.Manager.Id);
		}

//        	[Select Id,
//        	        ManagerId
//        	 From User
//        	 Where ManagerId != null And
//        	       Id in (Select UserId
//        	              From UserTerritory) And
//        	       IsActive = true
//        	       limit 100];
//
//        User testOD =
//        	[Select Id
//        	 From User
//        	 Where ManagerId != null And
//        	       Id in (Select UserId
//        	              From UserTerritory) And
//        	       IsActive = true And
//        	       Id = :testUser.ManagerId
//        	       Limit 1];

        Map<Id, Territory> ts = new Map<Id, Territory>(
        	[SELECT Id,
        	        Custom_External_TerritoryID__c
        	 FROM Territory
        	 WHERE ParentTerritoryId != null AND
        	 	   Custom_External_TerritoryID__c != NULL]);

		Map<Id, UserTerritory> uts = new Map<Id, UserTerritory>(
			[SELECT Id,
					UserId,
			        TerritoryId
			 FROM UserTerritory
			 WHERE UserId IN :userIds AND
			       TerritoryId IN :ts.keySet() AND
			       isActive = true]);

		Map<Id, Id> u2t = new Map<Id, Id>();

		User testUser = null;
		integer ndx = 0;
		for (UserTerritory ut : uts.values())
		{
			u2t.put(ut.UserId, ut.TerritoryId);
			if (testUsers.containsKey(ut.UserId) &&
				testUsers.get(ut.UserId).Manager != null)
					testUser = testUsers.get(ut.UserId);
		}

		string tid = ts.get(u2t.get(testUser.Manager.Id)).Custom_External_TerritoryID__c;
        CPQ_SSG_OD_Assignments__c odAssignment =
        	new CPQ_SSG_OD_Assignments__c(
        			CPQ_SSG_OD_Analyst__c = testUser.Manager.Id,
        			External_Territory_Id__c = tid);
        insert odAssignment;

        Product2 product = cpqProduct2_c.create(OrganizationNames_g.Abbreviations.SSG, 'TestProduct', 1);

        Account acct = new Account(
        					Name = 'Test Account 1',
        					BillingState = 'CO',
        					BillingPostalCode = '12345',
        					BillingCountry = 'US');
        insert acct;

        Contact c = new Contact(
        					FirstName = 'Test',
        					LastName = 'Contact',
        					AccountId = acct.Id);
        insert c;

        Opportunity opp = new Opportunity(
        						Name = 'Test Opportunity',
        						StageName = 'Early',
        						CloseDate = System.today(),
        						AccountId = acct.Id);
        insert opp;

        Test.startTest();
        	// TODO:  Convert this to use the cpqAgreement_c method using a Map of parameters
            Apttus__APTS_Agreement__c agreement =
            	cpqAgreement_cTest.create(
            		'Test Agreement',
            		testUser.Id,
            		opp.Id,
            		acct.Id,
            		scrubRT.Id,
            		'Non-Transfer',
            		c.Id,
            		'Clinically by the hospital',
            		'Approved',
            		'Requested');

            insert agreement;
            Apttus__APTS_Agreement__c before = [SELECT Id,OD_Approver__c FROM Apttus__APTS_Agreement__c WHERE Id = :agreement.Id];

            agreement.OD_Approver__c = null;
            //Apttus_Agreement_Trigger_Manager.PopulateAnalystAndManager_Exec_Num++;
            update agreement;
            Apttus__APTS_Agreement__c after = [SELECT Id,OD_Approver__c FROM Apttus__APTS_Agreement__c WHERE Id = :agreement.Id];

            system.assertEquals(before, after, 'OD Approver should be the same when doing an Insert or Update if Owner is the same');

        Test.stopTest();
    }

    @isTest
    static void testInsert() {
        Setup();

        // Insert
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c LIMIT 1];
        system.assertNotEquals(target.Id, null, 'A record should have been created');
    }

    @isTest
    static void testUpsert() {
        Setup();

        // Upsert
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c LIMIT 1];
        target.Apttus_Proposal__Proposal_Name__c = 'Upsert Test';
        upsert target;
        target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
        system.assertEquals(target.Apttus_Proposal__Proposal_Name__c, 'Upsert Test', 'Existing record should be updated');
    }

    @isTest
    static void testUpdate() {
        Setup();

        // Update
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c LIMIT 1];
        target.Apttus_Proposal__Proposal_Name__c = 'Update Test';
        update target;
        target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
        system.assertEquals(target.Apttus_Proposal__Proposal_Name__c, 'Update Test', 'Existing record should be updated');
    }

    @isTest
    static void testDelete() {
        Setup();

        // Delete
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c LIMIT 1];
        delete target;
        try {
            target = [SELECT Id FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
            system.assert(false, 'Record should have been deleted');
        }
        catch (Exception ex) {
        }
    }

    @isTest
    static void CPQ_Trigger_Profile() {
        Setup();

        // CPQ Trigger Profile
        CPQ_Trigger_Profile__c p = new CPQ_Trigger_Profile__c(SetupOwnerId = cpqUser_c.CurrentUser.Id, Proposal__c = true);
        upsert p;

        // Upsert
        Apttus_Proposal__Proposal__c target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c LIMIT 1];
        target.Apttus_Proposal__Proposal_Name__c = 'Upsert Test';
        upsert target;
        target = [SELECT Id, Apttus_Proposal__Proposal_Name__c FROM Apttus_Proposal__Proposal__c WHERE Id = :target.Id LIMIT 1];
        system.assertEquals(target.Apttus_Proposal__Proposal_Name__c, 'Upsert Test', 'Existing record should be updated');
    }
}