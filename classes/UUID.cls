global class UUID
{
	private string value;
	
	private static string generatorURL = 'https://www.uuidgenerator.net/api/';
	
	private static Set<integer> supportedVersions =
		new Set<integer>
		{
			1,
			4
		};
		
	private class InvalidVersionException extends Exception {}
	private static string InvalidVersionExceptionMessage = 'Supported versions are ' + supportedVersions;
	
	global UUID(integer version)
	{
		this(null, version);
	}
	
	global UUID(string prefix, integer version)
	{
		value = newUUID(prefix, version);
	}
	
	public override string toString()
	{
		return value;	
	}
	
	public static string newUUID(integer version)
	{
		return newUUID(null, version);
	}
	
    public static string newUUID(string prefix, integer version)
    {
        return (prefix == null ? '' : prefix) + create(version);
    }
    
    private static string create(integer version)
    {
    	if (!supportedVersions.contains(version))
    		throw new InvalidVersionException(InvalidVersionExceptionMessage);

		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(generatorURL + 'version' + version);
		
		Http http = new Http();
		
		try
		{
			HttpResponse res = http.send(req);
			return res.getBody();
		}
		catch (System.CalloutException e)
		{
			return null;
		}   	
    }
    
    private static string create()
    {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String uuid = h.SubString(0,8) + h.SubString(8,12) + h.SubString(12,16) + h.SubString(16,20) + h.substring(20);
        return uuid;
    }
}