/*
Name    : cpqAgreement_t
sObject : Apttus__APTS_Agreement__c

Author  : Paul Berglund
Created : 10/11/2016
Purpose : Dispatches to necessary classes when invoked by the trigger
          of the sObject

          DO NOT PUT ANY LOGIC IN THIS CLASS!!!!
          - Creat a main() method in your class and call it from this
            class and pass ALL of the trigger context values.

========================
= MODIFICATION HISTORY =
========================
DATE        AUTHOR           CHANGE
----        ------           ------
*/
public class cpqAgreement_t
{
	private static cpqAgreement_u a = new cpqAgreement_u();
	private static cpqCycleTime_Agreement_u ct = new cpqCycleTime_Agreement_u();

    //to prevent a method from executing a second time add the name of the method to the 'executedMethods' set
    public static Set<String> executedMethods = new Set<String>();

    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<Apttus__APTS_Agreement__c> newList,
            Map<Id, Apttus__APTS_Agreement__c> newMap,
            List<Apttus__APTS_Agreement__c> oldList,
            Map<Id, Apttus__APTS_Agreement__c> oldMap,
            integer size
        )
    {

        a.main(
            isExecuting,
            isInsert,
            isUpdate,
            isDelete,
            isBefore,
            isAfter,
            isUndelete,
            (List<sObject>)newList,
            (Map<Id, sObject>)newMap,
            (List<sObject>)oldList,
            (Map<Id, sObject>)oldMap,
            size
        );

        if(!executedMethods.contains('capture'))
        {
           ct.capture(
                isExecuting,
                isInsert,
                isUpdate,
                isDelete,
                isBefore,
                isAfter,
                isUndelete,
                (List<sObject>)newList,
                (Map<Id, sObject>)newMap,
                (List<sObject>)oldList,
                (Map<Id, sObject>)oldMap,
                size
            );

			executedMethods.add('capture');
        }

    	Apttus_Agreement_Trigger_Manager util =
            new Apttus_Agreement_Trigger_Manager(
            	isInsert,
            	isUpdate,
            	isBefore,
            	isAfter,
            	newList,
            	oldList,
            	oldMap,
            	newMap);

	    util.execute();
    }
 /*
    public static void Main(List<Apttus__APTS_Agreement__c> newList, Map<Id, Apttus__APTS_Agreement__c> newMap, List<Apttus__APTS_Agreement__c> oldList, Map<Id, Apttus__APTS_Agreement__c> oldMap, Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isExecuting)
    {
        if (isInsert || isUpdate) {

            if (isBefore && !executedMethods.contains('VerifyOwnerAndAssignApprovers'))
                Agreement_Main.VerifyOwnerAndAssignApprovers(newList, newMap, oldMap, isInsert);

        }

        if (!executedMethods.contains('capture')) {
          	CPQ_CycleTime_Processes.capture(
           		newList,
           		newMap,
           		oldList,
           		oldMap,
				isBefore,
				isAfter,
				isInsert,
				isUpdate,
				isDelete,
				isExecuting);
        }
    }
*/
}