@isTest
public class VFPageviewTracker_Test {
    
    static testMethod void insertNewVFTRacking() {
        
        PageReference aPage = Page.HR_Diversity;
        test.setCurrentPage(aPage);
 
        // Instantiate the standard controller
        VF_Tracking__c vft = new VF_Tracking__c();
        Apexpages.StandardController sc = new Apexpages.standardController(vft);
 
        // Instantiate the extension
        VFPageviewTracker vftCtrl = new VFPageviewTracker(sc);        
        vftCtrl.trackVisits();        
    }
    
    static testMethod void insertNewVFTRacking2() 
    {
        
        PageReference aPage = Page.HR_Diversity;     
        test.setCurrentPage(aPage);
 
        // Instantiate the standard controller
        VF_Tracking__c vft = new VF_Tracking__c();
        Apexpages.StandardController sc = new Apexpages.standardController(vft);
 
        // Instantiate the extension
        VFPageviewTracker vftCtrl = new VFPageviewTracker(sc);        
        vftCtrl.trackVisits2();     
        
    }
}