public class cpqPriceList_cache extends sObject_cache
{
	public static cpqPriceList_cache get() { return cache; }
	
	private static cpqPriceList_cache cache
	{
		get
		{
			if (cache == null)
				cache = new cpqPriceList_cache();
				
			return cache;
		}
		
		private set;
	}
	
	private cpqPriceList_cache()
	{
		super(Apttus_Config2__PriceList__c.sObjectType, Apttus_Config2__PriceList__c.field.Id);
		this.addIndex(Apttus_Config2__PriceList__c.field.OrganizationName__c);
		this.addIndex(Apttus_Config2__PriceList__c.field.Name);
	}
}