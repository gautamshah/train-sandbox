/****************************************************************************************
 * Name    : Test_LicenseRedirect 
 * Author  : Gautam Shah
 * Date    : November 4, 2014 
 * Purpose : Test class for LicenseRedirect.cls
 * Dependencies: LicenseRedirect.cls
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 
 *****************************************************************************************/
@isTest
public with sharing class Test_LicenseRedirect {
	
	private static final String NAMESPACE = 'vlc_pro';

    static testMethod void TestStandardController(){
        boolean userIsLicensed = UserInfo.isCurrentUserLicensed(NAMESPACE);
        IDNSC_Relationship__c rel = new IDNSC_Relationship__c(Name = 'Tester', On_Board_Start_Date__c = Date.Today(), Go_Live_Date_Desired__c = Date.Today() + 1);
        insert rel;
        System.assertNotEquals(null,rel.Id);
        ApexPages.StandardController ctl = new ApexPages.StandardController(rel);
        LicenseRedirect r = new LicenseRedirect(ctl);
        System.assertEquals(userIsLicensed,r.getLicensed());
    }

    static testMethod void TestStandardSetController(){
        boolean userIsLicensed = UserInfo.isCurrentUserLicensed(NAMESPACE);
        ApexPages.StandardSetController ctl = new ApexPages.StandardSetController(
            [select Id,Name from IDNSC_Relationship__c limit 5]
        );
        LicenseRedirect r = new LicenseRedirect(ctl);        
        System.assertEquals(userIsLicensed,r.getLicensed());        
    }  
}