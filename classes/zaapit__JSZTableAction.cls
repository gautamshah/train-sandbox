/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class JSZTableAction {
    @ReadOnly
    @RemoteAction
    global static Map<String,Double> calculateTotalSumsRMT(String queryCOunt1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static String getCustomLabel(String labelName) {
        return null;
    }
    global static Map<String,Double> getDatedExchangeRates(String queryFrom) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static String hierarchyFetcherFather(String table, String sonField, String faterField, String extraFields, String startSonIdInHierarchy, String sort1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static String hierarchyFetcherFatherX1(String table, String sonField, String faterField, String startSonIdInHierarchy, String sort1, Integer limit1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<SObject> hierarchyFetcherx(String table, String sonField, String faterField, String extraFields, String startSonIdInHierarchy, String sort1) {
        return null;
    }
    @RemoteAction
    global static List<String> massConvertX1(String IdsX, String overwriteValsX) {
        return null;
    }
    @RemoteAction
    global static List<String> massMergeX1(String objectName, String master, String IdsX, String overwriteValsX) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<Map<String,Object>> queryAjax1(String queryCOunt1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<Map<String,String>> queryAjax2CountFL(String queryCOunt1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<Map<String,String>> queryAjax2CountMPLX(String queryCOunt1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<String> queryAjax2GetIDSDups(String query1, String currentID) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<String> queryAjax2GetIDS(String query1, String currentID, Integer batchSize) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<Map<String,String>> queryAjax2GetTbFields(String table1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<Map<String,String>> queryAjax2SC(String queryCOunt1) {
        return null;
    }
    @ReadOnly
    @RemoteAction
    global static List<Map<String,String>> queryAjax2(String queryCOunt1) {
        return null;
    }
    @RemoteAction
    global static String sendMassEmails(String contactIds, String templateId, String saveAsActivity, String bccSender, String useSignature, String replyTo) {
        return null;
    }
    @RemoteAction
    global static Boolean setColumnWidth(String field1, String width, String layout) {
        return null;
    }
    @RemoteAction
    global static String setColumnWidth1(String field1, String width, String layout, String viewid) {
        return null;
    }
    @RemoteAction
    global static Integer setFollowChatterSts(String id, Boolean follow) {
        return null;
    }
}
