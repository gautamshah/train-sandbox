/****************************************************************************************
 * Name    : Test_AttachmentonSampleTrigger 
 * Author  : Fenny Saputra
 * Date    : 27/01/2016
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers. 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/

@isTest
private class Test_AttachmentonSampleTrigger {

    static testMethod void myUnitTest() {
    	List<User> u = [select Id from User where Country = 'MY'];
    	
    	Account a = new Account(
	    	Name = 'MY test',
	    	BillingCountry = 'MY'); 
    	insert a;
    	
    	Contact c = new Contact(
	    	LastName = 'test con',
	    	AccountId = a.Id,
	    	Country__c = 'MY');
    	insert c;
    	
    	Opportunity o = new Opportunity(
    		Name = 'test opp',
    		Capital_Disposable__c = 'Capital',
    		StageName = 'Develop',
    		CloseDate = system.today(),
    		AccountId = a.Id);
    	insert o;
    
        Samples__c s = new Samples__c(
	        Name = 'STI sample',
	        GBU__c = 'STI',
	        Sample_Date__c = system.today(),
	        Requestor__c = u[0].Id,
	        Purpose_of_Request__c = 'Demo',
	        Opportunity__c = o.Id,
	        Surgeon__c = c.Id);        
		insert s;
		
		Attachment att = new Attachment(Name='An attachment',body=blob.valueof('b'),parentId=s.Id);
		try{
		insert att;
		}
		catch(exception e)
		{}
		
		try{
		delete att;
		}
		catch(exception e)
		{}
    }
}