public class EMS_Controller_AddCCIExpenses {

    public Id EventId{get;set;}
    Public boolean IssamplePage;
    public Integer Chosenstep{get;set;}
    public String SKUSampleCode{get;set;}
    public String SKUSampleName{get;set;}
    public String tabletype{get;set;}
    public String tabledescription{get;set;}
    public String SampleCode{get;set;}
    public String SampleDescription{get;set;}
    public String ProcedureCode{get;set;}
    public String ProcedureName{get;set;}
    public String Country{get;set;}
    public integer selectedsearchcriteria{get;set;}
    public EMS_Event__c CurrentEMSRecord{get;set;}
    public String sourceFlowURL{get;set;}
    public List<Sample_CCIExpense__c> LSCCI ;
    public List<Event_Procedures__c> LEP;
    private String selectedaction;
    public String ReciepientId{
        get;
        set {
            ReciepientId= value;
        }
    }

    
    public boolean refreshpage{get;set;}
    public string sourceURL{get;set;}
    //Constructor
    public EMS_Controller_AddCCIExpenses(ApexPages.StandardController controller) {
         refreshpage=false;
         selectedContactIds = new Set<Id>();
         //IssamplePage=false;
         selectedsearchcriteria=1;
         LSCCI = new List<Sample_CCIExpense__c>();
         LEP = new List<Event_Procedures__c>();
         EventId= ApexPages.currentPage().getParameters().get('EventId');
         String chosenstep= ApexPages.currentPage().getParameters().get('step');
         sourceURL='/apex/zaapit_tab_EMS_CCI?Id='+EventId;
         sourceflowUrl='/apex/EMS_VFProcessflow?EventId='+EventId+'&step='+chosenstep;
         CurrentEMSRecord = new EMS_Event__c();
         for(EMS_Event__c obj:[Select Id,Name,Start_Date__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Design_Fee_Remarks__c,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,Event_Production_Fee__c,ISR_CSR__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,Total_Local_GTA__c,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Venue__c,Publication_grant__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,Division_Multiple__c,
                                  Total_No_of_Recipients__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,GBU_Multiple__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Total_Meal_Count__c,Total_LGT_Count__c 
                                  from EMS_Event__c where Id=:EventId]){
                                      CurrentEMSRecord = obj;
                                  } 
    }
   
   
    
    
    //Method to redirect back to Event Detail Page Record
    Public Pagereference Back(){
    
    Pagereference P = new Pagereference('/'+EventId);
    P.setredirect(true);
    return P;    
    }
    
    
    //Method to redirect as per Category
    
    
    public boolean iscriteriachanged{get;set;}
    
    public Pagereference rendersearchfields(){
    system.debug('}}}}}}}called');
    iscriteriachanged=true;
    selectedContactIds=new Set<Id>();

    getContacts();
    return null;
    
    }
  
    
    //Method to insert CCI Samples
    public PageReference AddCCISamples(){  
    refreshpage=true;
    LSCCI.clear();
    LEP.clear();
    for(Id eachConId : selectedContactIds){
        if(selectedsearchcriteria==1 ){
        Sample_CCIExpense__c SampleExpense= new Sample_CCIExpense__c(Cost_Element__c=eachConId,EMS_Event__c=EventId,Type__c='Non Product Category-CCI');
        LSCCI.add(SampleExpense);
        }
        else if(selectedsearchcriteria==2 ){
        System.debug('////////inside Org insert');
        Sample_CCIExpense__c SampleExpense= new Sample_CCIExpense__c (Cost_Element__c=eachConId,EMS_Event__c=EventId,Type__c='Product Package-CCI');
        LSCCI.add(SampleExpense);
        
        }
        else if(selectedsearchcriteria==3 ){
        System.debug('////////inside Org insert');
        Event_Procedures__c EP= new Event_Procedures__c(Condition_Procedure__c=eachConId,EMS_Event__c=EventId);
        LEP.add(EP);
        
        }
    }
    
     try{    
         Database.saveresult[] sr=database.insert(LSCCI,false);
         Database.saveresult[] sr1=database.insert(LEP,false);
         System.debug('<<<<<<<'+sr);
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Samples added to Event successfully');
         ApexPages.addMessage(myMsg);
         return null; 
        }
     catch(Exception E){
     
      System.debug('---->An Error occured while inserting Transactions records'+E.getmessage());
      return null;
     }   
        
    
    
    }
    
    
    
   
        public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null && selectedsearchcriteria==1 ) {
            system.debug('yes m here');
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id FROM Cost__c WHere Id=null]));
            }
            else if(setCon == null && selectedsearchcriteria==2){
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Name FROM Cost__c WHere Id=null]));
            }
            else if(setCon == null && selectedsearchcriteria==3 ){
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Name FROM Condition_Procedure__c WHere Id=null]));
            
            
            
            }
            
            return setCon;
        }
        set;
    }
 
        private Set<Id> selectedContactIds=  new Set<Id>();
      
       //Function to display Contact Search Results
       
       public String query;
       public PageReference SearchContacts(){
       iscriteriachanged=false;
       
       System.debug(')))))))'+IssamplePage);
        this.selectedContactIds= new Set<Id>();
        
        setcon=null;
        Set<Id> SExistingCEIds = new Set<Id>();
        Set<Id> SExistingProcedIds = new Set<Id>();
        EMS_Event__c updatedEMSChildRecords =  new EMS_Event__c();
        for(EMS_Event__c objEvent:[Select Id,(Select Id,Condition_Procedure__c from Event_Procedures__r),(select Id,Cost_Element__c from Sample_CCIExpenses__r) from EMS_Event__c where Id=:currentEMSRecord.Id LIMIT 1]){
            updatedEMSChildRecords = objEvent;
        }
        for(Event_Procedures__c eachEventProc :updatedEMSChildRecords.Event_Procedures__r){
        
            SExistingProcedIds.add(eachEventProc.Condition_Procedure__c);
        }
        
        for(Sample_CCIExpense__c eachEventProc :updatedEMSChildRecords.Sample_CCIExpenses__r){
        
            SExistingCEIds.add(eachEventProc.Cost_Element__c);
        }
        
        
        string type;
        if(selectedsearchCriteria==1 ){
        type='Non Product Category-CCI';
        query='Select Id,Code__c,Name,Cost_Element_Type__c,Country__c,Description__c,Name__c,Status__c,Table_Type__c,Unit_Price__c from Cost__c where Table_Type__c LIKE \'%' + tabletype + '%\'' + ' and Description__c LIKE \'%' + tabledescription + '%\'' + 'and Country__c = ' + '\''+ country + '\''+' and Cost_Element_Type__c = ' +'\''+ type +'\''+' and Id not in :SExistingCEIds'+' ORDER BY Name DESC ';
        this.setCon= new ApexPages.StandardSetController( database.query(query) );
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(5);
       
       if(setcon==null ){
           setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id,Code__c from Cost__c where Id=null]));
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search results found for this search criteria');
           ApexPages.addMessage(myMsg);
       }
        }
        else if(selectedsearchCriteria==2 ){
        type='Product Package';
         query='Select Id,Code__c,Cost_Element_Type__c,Country__c,Description__c,Name__c,Status__c,Table_Type__c,Unit_Price__c from Cost__c where Code__c LIKE \'%' + SampleCode + '%\'' + ' and name__c LIKE \'%' + SampleDescription + '%\'' + ' and Country__c= '+ '\'' +Country+  '\'' +' and Cost_Element_Type__c= '+ '\'' + type +  '\'' +' and Id not in :SExistingCEIds'+' ORDER BY Code__c DESC ';

        this.setCon= new ApexPages.StandardSetController( database.query(query) );
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(5);
       
       if(setcon==null ){
           setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id from Cost__c where Id=null]));
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search results found for this search criteria');
           ApexPages.addMessage(myMsg);
       }
       
                
        }
        
        else if(selectedsearchCriteria==3 ){
        String Status='Active';
        query='Select Id,Department__c,Country__c,Name,Procedure_Code__c,Status__c from Condition_Procedure__c where Name LIKE \'%' + ProcedureName + '%\'' + 'and Procedure_Code__c LIKE \'%' + ProcedureCode + '%\'' + 'and Country__c = ' + '\'' +country +'\'' +' and Status__c = '+ '\'' +Status +'\''+' and Id not in :SExistingProcedIds' +' ORDER BY Name DESC ';
                 System.debug('---------->'+query);

        this.setCon= new ApexPages.StandardSetController( database.query(query) );
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(5);
       
       if(setcon==null ){
           setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id from Condition_Procedure__c  where Id=null]));
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search results found for this search criteria');
           ApexPages.addMessage(myMsg);
       }
        }
        
        
       return Null;
       }
       
      
       public List<CCIExpenseWrapper> getContacts(){
         System.debug('>>>>>>>>>>>'+IssamplePage);
        List<CCIExpenseWrapper> rows = new List<CCIExpenseWrapper>();
        System.debug('>>>>>'+iscriteriachanged);
        if(iscriteriachanged==true){
        
        return rows;
        }
                
        if(selectedsearchCriteria==1 ){
        
        for(sObject r : this.setCon.getRecords()){
            CCIExpenseWrapper row;
            Cost__c c = (Cost__c)r;
            row = new CCIExpenseWrapper(c,null,null,false); 
            
            if(this.selectedContactIds.contains(r.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
        }
        }
        else if(selectedsearchCriteria==2 ){
         
       System.debug('+++++++'+setcon.getrecords());
        for(sObject r : this.setCon.getRecords()){
            CCIExpenseWrapper row;
            Cost__c c = (Cost__c)r;
            row = new CCIExpenseWrapper(c,null,null,false); 
            
            if(this.selectedContactIds.contains(r.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
        }
        
        }
         else if(selectedsearchCriteria==3){
         
       System.debug('+++++++'+setcon);
        for(sObject r : this.setCon.getRecords()){
            CCIExpenseWrapper row;
            Condition_Procedure__c c= (Condition_Procedure__c )r;
            row = new CCIExpenseWrapper(null,c,null,false); 
            
            if(this.selectedContactIds.contains(r.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
        }
        
        }
       
        
      
        return rows;
 
    }
       
       
      
      //Delete Contact
      public Pagereference deleteContact(){
      try{
      database.delete(ReciepientId);
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'HCP Beneficiary successfully deleted');
      ApexPages.addMessage(myMsg);
      return null; 
      }
      catch(Exception e){
      
      System.debug('---->An Error occured while deleting HCP Beneficiary'+E.getmessage());
      return null;
      
      
      
      }
      } 
     
   
  
    public String contextItem{get;set;}
    public void doSelectItem(){
        System.debug('--->'+contextitem);
        selectedContactIds.add(contextItem);
 
    }
 
    /*
    *   handle item deselected
    */
    public void doDeselectItem(){
 
        selectedContactIds.remove(contextItem);
 
    }
 
    /*
    *   return count of selected items
    */
    public Integer getSelectedCount(){
 
        return this.selectedContactIds.size();
 
    }
 
    /*
    *   advance to next page
    */
    public void doNext(){
 
        if(this.setCon.getHasNext())
            this.setCon.next();
 
    }
 
    /*
    *   advance to previous page
    */
    public void doPrevious(){
 
        if(this.setCon.getHasPrevious())
            this.setCon.previous();
 
    }
    
    
    public Boolean getHasPrevious(){
 
        return this.setCon.getHasPrevious();
 
    }
 
    /*
    *   return whether next page exists
    */
    public Boolean getHasNext(){
 
        return this.setCon.getHasNext();
 
    }
 
    /*
    *   return page number
    */
    public Integer getPageNumber(){
 
        return this.setCon.getPageNumber();
 
    }
 
    /*
    *    return total pages
    */
    Public Integer getTotalPages(){
 
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
 
        Decimal pages = totalSize/pageSize;
 
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    
    
    //Wrapper for CCI Expenses
    public class CCIExpenseWrapper{
    
    
    public Cost__c CstElement{get;set;}
    public Condition_Procedure__c CP{get;set;}
    Public Product_SKU__c PSKU{get;set;}
    public boolean isselected{get;set;}
    
    
    
    public CCIExpenseWrapper(Cost__c cost,Condition_Procedure__c condproc,Product_SKU__c eachProdSKU,boolean checked){
    
    CstElement = cost;
    CP = condproc;
    PSKU=eachProdSKU;
    isselected = checked;
    
    
    }
    
    
    }
    
    
    
    
    
    
}