/****************************************************************************************
 * Name    : Test_LeadTrigger 
 * Author  : Gautam Shah
 * Date    : 07/08/2013 
 * Purpose : test class for LeadTrigger.trigger
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *  27/08/2014  Lakhan Dubey       Coverage for changes made in original trigger.
 *****************************************************************************************/
@isTest (SeeAllData=true)
private class Test_LeadTrigger {

static testMethod void myUnitTest() {
    ID RtID = [Select id from RecordType where Name =: 'EU S2 Lead PILOT' limit 1].id;
    Campaign c = [Select Id From Campaign Limit 1];
    Account Acc = [SELECT Account_External_ID__c FROM Account where Account_External_ID__c !=null limit 1];
    
    Lead l = new Lead(LastName='Tester', Company='Test Co.', Campaign_id__c=c.Id, Country='US', RecordtypeId  = RtID, CovidienAccountNumber__c=Acc.Account_External_ID__c);
    insert l;
    system.assertequals(l.CovidienAccountNumber__c,Acc.Account_External_ID__c);
                                    }
                               }