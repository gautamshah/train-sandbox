/**

CHANGE HISTORY
===============================================================================
DATE        NAME            DESC
            Paul Berglund   Created.
2016-11-03  Isaac Lewis     Made create() method public, so it can be used in cpqConfigSetting_c during test executions.
===============================================================================
*/

@isTest
public class cpqConfigSetting_cTest
{
	@isTest
	static void SystemProperties()
	{
		system.debug(CONSTANTS.NBSP(10) + '****** cpqConfigSetting_cTest');

		CPQ_Config_Setting__c SystemProperties = cpqConfigSetting_c.SystemProperties;
		system.debug(CONSTANTS.NBSP(10) + '======> SystemProperties' + CONSTANTS.CRLF + SystemProperties);
		
		system.assertNotEquals(null, SystemProperties);
	}
	
	@isTest
	static void reload()
	{
		cpqConfigSetting_c.reload();
		
		system.assertNotEquals(null, cpqConfigSetting_c.SystemProperties);
	}
	
	@isTest
	static void getAllowedToSeeCOOPPricingFile()
	{
		string profile = 'APTTUS - RMS US Contract Dev';
		
		system.assert(cpqConfigSetting_c.getAllowedToSeeCOOPPricingFile(profile));
	}
	
	@isTest
	static void getMultiPicklistSystemProperty()
	{
		Set<string> values = cpqConfigSetting_c.getMultiPicklistSystemProperty(CPQ_Config_Setting__c.fields.Equipment_Attributes__c);
		system.assert(values.contains('Hardwired Oxinet'));
	}
	
	@isTest
	static void getBusinessEndDateBeforeCutOff()
	{
		integer offset = 17;

		DateTime start1 = DateTime.newInstance(2017, 4, 1, 08, 00, 00);
		Date finish1 = Date.newInstance(2017, 4, 26);

		Date endDate1 = cpqConfigSetting_c.getBusinessEndDate(start1, offset);
		//system.assertEquals(finish1, endDate1);
	}

	@isTest 
	static void getBusinessEndDateAfterCutoff()
	{
		integer offset = 17;

		DateTime start1 = DateTime.newInstance(2017, 4, 1, 17, 00, 00);
		Date finish1 = Date.newInstance(2017, 4, 27);
		
		Date endDate1 = cpqConfigSetting_c.getBusinessEndDate(start1, offset);
		//system.assertEquals(finish1, endDate1);
	}

	@isTest
	static void getBusinessDayCutOffHour()
	{
		DateTime targetDate = DateTime.newInstanceGMT(system.today(), time.newInstance(15, 0, 0, 0)); 
		DateTime endDateTime = cpqConfigSetting_c.getBusinessDayCutOffHour();
		
		system.assertEquals(targetDate, endDateTime);
	}
	
	@isTest
	static void getProposalValidForAfterPresented()
	{
		system.assertEquals(90, cpqConfigSetting_c.getProposalValidForAfterPresented());
	}
	
	@isTest
	static void getRenewalUpliftPercentage()
	{
		system.assertEquals(3.00, cpqConfigSetting_c.getRenewalUpliftPercentage());
	}
	
	@isTest
	static void getRequireOpportunityAmountThreshold()
	{
		system.assertEquals(15000.00, cpqConfigSetting_c.getRequireOpportunityAmountThreshold());
	}
	
	@isTest
	static void getSendEmailToAgreementOwner()
	{
		
	}
	
	@isTest
	static void getSetTaskOwnerToAgreementOwner()
	{
		
	}
	
	@isTest
	static void getCOTFilters()
	{
		
	}
	
	
}