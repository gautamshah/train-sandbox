@isTest
private class AddOpportunityPlayerExtension_Test 
{
	private static Opportunity GetOpportunityTestRecord(string recordTypeName)
	{
		Account a = new Account(Name = 'Test Account');
		insert a;
		Opportunity testOp = new Opportunity(
			Name = 'Test Opp',
			RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Opportunity').Get(recordTypeName).Id,
			Type = 'New Customer - Conversion',
			Capital_Disposable__c = 'Disposable',
			StageName = 'Identify',
			CloseDate = Date.today(),
			AccountId = a.Id
		);
		insert testOp;
		return testOp;
	}
	
	static testMethod void TestExtenion() 
	{
		PageReference pageRef = Page.AddOpportunityPlayer;
        Test.setCurrentPage(pageRef);
		Opportunity o = GetOpportunityTestRecord('US_AST_Opportunity');
		List<Contact> contacts = new List<Contact>();
		contacts.add(new Contact(LastName = 'Contact 1', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id));
		contacts.add(new Contact(LastName = 'Contact 2', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id));
		contacts.add(new Contact(LastName = 'Contact 3', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id));
		contacts.add(new Contact(LastName = 'Contact 4', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id));
		insert contacts;
		Opportunity_Player__c op1 = new Opportunity_Player__c(Opportunity__c = o.Id, Contact__c = contacts[0].Id);
		Opportunity_Player__c op2 = new Opportunity_Player__c(Opportunity__c = o.Id, Contact__c = contacts[1].Id);
		insert op1;
		insert op2;
		Apexpages.currentPage().getParameters().put('oppId', o.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(o);
		AddOpportunityPlayerExtension extension = new AddOpportunityPlayerExtension(sc);
		Test.startTest();
		extension.accountContacts[0].Opportunity_Role__c = 'Test';
		extension.existingContacts[0].Opportunity_Role__c = 'Test';
		extension.blankOppPlayers[0].Contact__c = contacts[2].Id;
		extension.blankOppPlayers[0].Opportunity_Role__c = 'Test';
		extension.SaveRecords();
		Test.stopTest();
	}
	
	static testMethod void TestExtenion_Failures() 
	{
		Test.startTest();
		PageReference pageRef = Page.AddOpportunityPlayer;
        Test.setCurrentPage(pageRef);
		Opportunity o = GetOpportunityTestRecord('US_AST_Opportunity');
		List<Contact> contacts1 = new List<Contact>{
			new Contact(LastName = 'Contact 1', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id),
			new Contact(LastName = 'Contact 2', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id),
			new Contact(LastName = 'Contact 3', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id),
			new Contact(LastName = 'Contact 4', FirstName = 'Test', AccountId = o.AccountId, RecordTypeId = RecordType_Utilities.GetRecordTypesMap('Contact').Get('Affiliated_Contact_US').Id)
		};
		insert contacts1;
		List<Opportunity_Player__c> ops = new List<Opportunity_Player__c>{	new Opportunity_Player__c(Opportunity__c = o.Id, Contact__c = contacts1[0].Id)};
		insert ops;
		Apexpages.currentPage().getParameters().put('oppId', o.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(o);
		AddOpportunityPlayerExtension extension = new AddOpportunityPlayerExtension(sc);
		
		extension.accountContacts[0].Contact__c = contacts1[1].Id;
		extension.accountContacts[0].Opportunity_Role__c = 'test';
		extension.accountContacts[0].Opportunity__c = null;
		extension.SaveRecords();
		extension.accountContacts[0].Opportunity__c = o.Id;
		extension.existingContacts[0].Contact__c = contacts1[2].Id;
		extension.existingContacts[0].Opportunity_Role__c = 'Test';
		//extension.existingContacts[0].Opportunity__c = null;
		extension.SaveRecords();
		//extension.existingContacts[0].Opportunity__c = o.Id;
		extension.blankOppPlayers[0].Contact__c = contacts1[3].Id;
		extension.blankOppPlayers[0].Opportunity_Role__c = 'Test';
		extension.blankOppPlayers[0].Opportunity__c = null;
		extension.SaveRecords();
		extension.blankOppPlayers[0].Opportunity__c = o.Id;
		extension.SaveRecords();
		Test.stopTest();
	}
}