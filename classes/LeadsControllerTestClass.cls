/***********************
Aurthor: Yap Zhen-Xiong
Email: Zhenxiong.Yap@covidien.com
Description: Test Class for Leads Entry Module 
***********************/

@isTest

public class LeadsControllerTestClass {

    static testMethod void LeadsControllerTestClass ()
    {
        Campaign c = new Campaign();
        c.IsActive = true;
        c.Name = 'Test Campaign';
        c.StartDate = System.Today() - 1;
        c.EndDate = System.Today() + 1; 
        insert c;
    
        //Test converage for the LeadsSetup visualforce page
        PageReference leadsSetupPage = Page.LeadsSetup;
        Test.setCurrentPageReference(leadsSetupPage);
        
        // create an instance of the LeadsSetupController 
        LeadsSetupController leadsSetupCon = new LeadsSetupController();
        leadsSetupCon.launchLeadsCollection();
        
        //Test converage for the LeadsEntryFormvisualforce page
        PageReference leadsEntryFormPage = Page.LeadsEntryForm;
        Test.setCurrentPageReference(leadsEntryFormPage);
        
        // create an instance of the LeadsEntryController without oid and cid
        LeadsEntryController leadsEntryCon = new LeadsEntryController ();
        leadsEntryCon.submitLead();
        
        // create an instance of the LeadsEntryController with oid and cid
        c = [SELECT Id from Campaign WHERE id =: c.id];
        System.currentPageReference().getParameters().put('oid', UserInfo.getUserId());
        System.currentPageReference().getParameters().put('cid', c.id);
        leadsEntryCon = new LeadsEntryController ();
        leadsEntryCon.submitLead();
        
        leadsEntryCon.newLead.FirstName = 'FName';
        leadsEntryCon.newLead.Title = 'Director';
        leadsEntryCon.newLead.Description= 'RMS STI EbD EMIT VTMS';
        leadsEntryCon.submitLead();
        
        leadsEntryCon.newLead.Salutation = 'Dr';
        leadsEntryCon.newLead.LastName = 'LName';
        leadsEntryCon.newLead.Company = 'Test Hospital';
        leadsEntryCon.newLead.Country = 'SG';
        leadsEntryCon.submitLead();
        
        leadsEntryCon.newLead.MobilePhone = '987654321';
        leadsEntryCon.newLead.Email = 'test@email.com';
        leadsEntryCon.submitLead();
        
        //Test converage for the LeadsThankYou visualforce page
        PageReference leadsTYPage = Page.LeadsThankYou;
        Test.setCurrentPageReference(leadsTYPage);

    }
    
}