@isTest
public class TestBatchInsertLoginHistory {   
        static testMethod void testLoginHistory() {
        string batchQuery ='';
    // Making the assumption that at least one user will match this. Better to assign to list and check that has at least one record.   
    batchQuery = 'select id,userid,status,logintime,application,logintype from loginhistory where logintime > LAST_WEEK limit 1';        
        System.debug('===batchQuery==='+batchQuery);
        BatchInsertLoginHistory b = new BatchInsertLoginHistory(batchQuery); 
        database.executebatch(b);
}
}