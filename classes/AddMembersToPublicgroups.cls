global class AddMembersToPublicgroups implements Database.Batchable<sObject>{
/****************************************************************************************
   * Name    : AddMembersToPublicgroups
   * Author  : Lakhan Dubey
   * Date    : 19/09/2014 
   * Purpose : Batch Job to add users into public group depending upon their country and sales org.
   * Dependencies: User, Group and Groupmember Objects
 *****************************************************************************************/
    String s= '\'__\\_%\'';
    global final Datetime d=Datetime.now().adddays(-1);   
    global final String Query= 'SELECT Id,Name FROM group  WHERE  (CreatedDate > '+d.format('yyyy-MM-dd')+'T'+
    d.format('HH:mm')+':00.000Z '+ 'AND  Name   LIKE' +s+ ')' ;
    List<Id> gId=new List<Id>();
    Map<Id,Set<Id>> group_User=new Map<Id,Set<Id>>();
    List<Groupmember> gm=new List<Groupmember>();
    Map <String,Id> publicgroup = new Map <String,Id>();   
   
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    
   }

global void execute(Database.BatchableContext BC, List<sObject> scope){
  
    for(sObject s :scope)
    {
        Group g = (Group) s;
        gId.add(g.Id);
        publicgroup.put(g.Name,g.Id); 
    }
    
    for(Groupmember g:[SELECT UserOrGroupId, GroupId FROM GroupMember WHERE GroupId IN:gId])  
    {
        if(group_User.containskey(g.GroupId))
            group_User.get(g.GroupId).add(g.UserOrGroupId);
        else
        {
            Set<Id> temp = new Set<Id>();
            temp.add(g.UserOrGroupId);
            group_User.put(g.GroupId,temp);
        }
    }
     
  
    for(User ur:[SELECT Id,Country_SalesOrg__c FROM User WHERE User_Role__c='Field Manager' and Country_SalesOrg__c = :publicgroup.keyset()])
    {  
        if(!group_User.containskey(publicgroup.get(ur.Country_SalesOrg__c)) || 
           !group_User.get(publicgroup.get(ur.Country_SalesOrg__c)).contains(ur.Id))
        {
            GroupMember gm1=new GroupMember();
            gm1.UserOrGroupId = ur.Id;
            gm1.GroupId = publicgroup.get(ur.Country_SalesOrg__c);
            gm.add(gm1);
        }
    }
    
    if(gm.size()>0)
    {
        Database.SaveResult[] srList=Database.insert(gm,false);
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted  ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug(' fields that affected this error: ' + err.getFields());}
            }
        }
    }
}

global void finish(Database.BatchableContext BC){}
}