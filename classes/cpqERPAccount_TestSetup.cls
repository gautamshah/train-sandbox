@isTest
public class cpqERPAccount_TestSetup 
{
    public static ERP_Account__c generateERPAccount (Account account, String erpAccountType)
    {
        return new ERP_Account__c(
            Name = 'ERP Record',
            Parent_Sell_To_Account__c = account.Id,
            ERP_Account_Type__c = erpAccountType,
            ERP_Source__c = 'E1',
            ERP_Account_Status__c = 'A'
        );
    }

    public static List<ERP_Account__c> generateERPAccounts (List<Account> accounts, Integer quantity)
    {
        List<ERP_Account__c> erpas = new List<ERP_Account__c>();
        List<String> ERP_ATs = new List<String>(ERPAccount_c.VALID_ERP_ACCOUNT_TYPES);
        for (Account acc : accounts)
            for (Integer i=0; i<quantity; i++)
                erpas.add(generateERPAccount(acc,ERP_ATs.get(0)));

        return erpas;
    }

    
    
}