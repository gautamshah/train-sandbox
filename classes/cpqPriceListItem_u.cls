/*
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed    *                                       *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com


MODIFICATION HISTORY
====================
Date        Id   Initials   Jira(s)     Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD    A   PAB         CPR-000     Updated comments section
                            AV-000
20161103    A   IL          AV-001      Title of the Jira – Some changes needed to be made to support Medical Supplies
20161115    A   PAB         AV-253      Created fetch method

Notes:
1.  If there are multiple related Jiras, add them on the next line
2.  Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/     
public class cpqPriceListItem_u extends sObject_u
{
    private static cpqPriceListItem_g cache = new cpqPriceListItem_g();
    
    ////
    //// fetch(sets)
    ////
    public static Map<Id, Apttus_Config2__PriceListItem__c> fetch(sObjectField indexField, object key)
    {
        Map<Id, Apttus_Config2__PriceListItem__c> existingIndex = cache.fetch(indexField, key);
        
        if (existingIndex.isEmpty())
        {
            List<Apttus_Config2__PriceListItem__c> fromDB = fetchFromDB(indexField, key);
            
            cache.put(fromDB); //// UserId is already included to be indexed                    

            return cache.fetch(indexField, key);
        }
        else
            return existingIndex;
    }

    
    public static Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> fetch(sObjectField indexField, set<object> keys)
    {
        Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> existingIndex = cache.fetch(indexField, keys);
        
        if (existingIndex.size() != keys.size())
        {
            set<object> toFetch = new set<object>();
            for(object key : keys)
                if (!existingIndex.containsKey(key))
                    toFetch.add(key);

            List<Apttus_Config2__PriceListItem__c> fromDB = fetchFromDB(indexField, toFetch);
            
            cache.put(fromDB); //// UserId is already included to be indexed                    

            return cache.fetch(indexField, keys);
        }
        else
            return existingIndex;
    }

    ////
    //// Leave this private - it's only for testing purposes
    ////
    @testVisible
    private static List<Apttus_Config2__PriceListItem__c> fetch()
    {
        if (Test.isRunningTest() &&
            cache.isEmpty())
        {
            List<Apttus_Config2__PriceListItem__c> fromDB = fetchFromDB();
                
            cache.put(fromDB); //// UserId is already included to be indexed
        }
        
        return cache.fetch().values();
    }
    
    ////
    //// fetchFromDB
    ////
    @testVisible
    private static List<Apttus_Config2__PriceListItem__c> fetchFromDB(sObjectField field, object value)
    {
        // All the fields are Ids, so convert the values to Ids
        string whereStr = ' WHERE ' + field.getDescribe().getName() + ' = :value ';

        string query =
            SOQL_select.buildQuery(
                Apttus_Config2__PriceListItem__c.sObjectType,
                cpqPriceListItem_c.fieldsToInclude,
                whereStr,
                null,
                null);
                    
        return (List<Apttus_Config2__PriceListItem__c>)Database.query(query);
    }

    @testVisible
    private static List<Apttus_Config2__PriceListItem__c> fetchFromDB(sObjectField field, set<object> ovalues)
    {
        // We only support string for the moment
        set<string> values = DATATYPES.castToString(ovalues);
        
        // All the fields are Ids, so convert the values to Ids
        string whereStr = ' WHERE ' + field.getDescribe().getName() + ' IN :values ';

        string query =
            SOQL_select.buildQuery(
                Apttus_Config2__PriceListItem__c.sObjectType,
                cpqPriceListItem_c.fieldsToInclude,
                whereStr,
                null,
                null);
                    
        return (List<Apttus_Config2__PriceListItem__c>)Database.query(query);
    }

    ////
    //// This is for testing only - you will exceed governor limits in production
    ////
    @testVisible
    private static List<Apttus_Config2__PriceListItem__c> fetchFromDB()
    {
        string query =
            SOQL_select.buildQuery(
                Apttus_Config2__PriceListItem__c.sObjectType,
                cpqPriceListItem_c.fieldsToInclude,
                null,
                null,
                null);
                    
        return (List<Apttus_Config2__PriceListItem__c>)Database.query(query);
    }
    
    
    
    // 20161115-A
    public static Map<Id, Apttus_Config2__PriceListItem__c> fetch(
        Set<Id> priceListIds,
        Set<Id> productIds)
    {
        Map<object, Map<Id, Apttus_Config2__PriceListItem__c>> PL2PLI =
            fetch(
                Apttus_Config2__PriceListItem__c.field.Apttus_Config2__PriceListId__c,
                DATATYPES.castToObject(priceListIds));
                
        Map<Id, Apttus_Config2__PriceListItem__c> PLIwithBoth =
            new Map<Id, Apttus_Config2__PriceListItem__c>();
            
        for(object key : PL2PLI.keySet())
        {
            Map<Id, Apttus_Config2__PriceListItem__c> PLI = PL2PLI.get(key);
            for(Id pliId : PLI.keySet())
                if (PLI.get(pliId) != null &&
                    PLI.get(pliId).Apttus_Config2__ProductId__c != null &&
                    productIds.contains(PLI.get(pliId).Apttus_Config2__ProductId__c))
                    PLIwithBoth.put(pliId, PLI.get(pliId));
        }
        
        return PLIwithBoth;     
    }
    
/* Legacy code


    public static Map<Id, Apttus_Config2__PriceListItem__c> fetch()
    {
        return cpqPriceListItem_g.PriceListItems;
    }
    
    public static void add(Apttus_Config2__PriceListItem__c pli)
    {
        put(pli.Id, pli);
    }
    
    public static void addAll(List<Apttus_Config2__PriceListItem__c> plis)
    {
        putAll(new Map<Id, Apttus_Config2__PriceListItem__c>(plis));    
    }
    
    public static void put(Id id, Apttus_Config2__PriceListItem__c pli)
    {
        cpqPriceListItem_g.PriceListItems.put(id, pli);
    }
    
    public static void putAll(Map<Id, Apttus_Config2__PriceListItem__c> plis)
    {
        cpqPriceListItem_g.PriceListItems.putAll(plis);
    }
    
/*
    private Id priceListId;
    
    public Map<string, Apttus_Config2__PriceListItem__c> sobjs { get; set; }
   
    public cpqPriceListItem_u(Id priceListId)
    {
        this.priceListId = priceListId;
    }
  
    public void load(List<contractProduct_c> products)
    {
      sobjs = new Map<string, Apttus_Config2__PriceListItem__c>();
      
    integer sequence = 1;
    for(contractProduct_c p : products)
    {
      if (p.sobj != null)
      {
        Apttus_Config2__PriceListItem__c pli = new Apttus_Config2__PriceListItem__c();
      
          pli.Apttus_Config2__Sequence__c = sequence++;
        pli.Apttus_Config2__ChargeType__c = p.sobj.Charge_Type__c;
        pli.Apttus_Config2__Cost__c = p.sobj.Cost__c;
        pli.Apttus_Config2__ListPrice__c = p.sobj.Catalog_Price__c;
        pli.Apttus_Config2__PriceMethod__c = 'Per Unit'; // Per Unit
          pli.Apttus_Config2__PriceType__c = 'One Time'; // One Time
          //pli.Apttus_Config2__PriceUom__c = p.ws.UnitOfMeasure; // Each | Case | Box
          pli.Apttus_Config2__ProductId__c = p.sobj.Id;
  
        //sobjs.put(p.ws.ProductNumber, pli);
      }
    }
  }
  
     public void add()
     {
       for(Apttus_Config2__PriceListItem__c p : sobjs.values())
         p.Apttus_Config2__PriceListId__c = priceListId;
         
       upsert sobjs.values();  
     }
*/    
}