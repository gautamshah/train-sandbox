/**
Controller for CPQ_ProposalCustomer_Config page

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
11/21/2014      Yuli Fintescu       Created
08/22/2015      Paul Berglund       Replaced Status__c = 'Active' with isActive = true
08/24/2015      Bryan Fry           Added SSG logic to allow all accounts to be associated
10/15/2015      Paul Berglund       Added Northstar logic - filter out certain
                                    COTs on certain record types
08/24/2016      Isaac Lewis         Added Implementation/Licensed Location specification for
                                    Vital Sync products
09/19/2016      Bryan Fry           Added search by Allied Group
===============================================================================
*/


public with sharing class CPQ_Controller_ProposalCustomer_Config extends cpqControllerProposalBase {
    public Apttus_Proposal__Proposal__c theRecord {get; set;}
    private Set<String> RecordTypesAllowingTracingCustomer;
    private String DealTypeName;
    private Set<String> incontracts;
    private Boolean surgicalRecordType = false;
    public string COTsToFilter {get; set;}
    public Boolean hasImplementationLocation {get;set;}

    public CPQ_Controller_ProposalCustomer_Config(ApexPages.StandardController controller) {
        hasImplementationLocation = false;
        SelectedResultMap = new Map<String, CPQ_FacilityItem>();

        //get source proposal record
        theRecord = (Apttus_Proposal__Proposal__c)controller.getRecord();

        if (theRecord.ID != null) {
            RecordType rt = RecordType_u.fetch(theRecord.RecordTypeId);
            if (rt.DeveloperName == 'Agreement_Proposal' ||
                rt.DeveloperName == 'Energy_Hardware' ||
                rt.DeveloperName == 'Hardware_or_Product_Quote' ||
                rt.DeveloperName == 'New_Product_LOC' ||
                rt.DeveloperName == 'Promotional_LOC' ||
                rt.DeveloperName == 'Royalty_Free_Agreement') {

                surgicalRecordType = true;
            } else {
                surgicalRecordType = false;
            }
            List<Apttus_Proposal__Proposal__c> records = [Select Name, Id, OwnerId, Apttus_Proposal__Account__c,
                                                            Apttus_Proposal__Account__r.Name,
                                                            Apttus_Proposal__Account__r.Status__c,
                                                            Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c,
                                                            Apttus_Proposal__Account__r.Account_External_ID__c,
                                                            Apttus_Proposal__Account__r.BillingStreet,
                                                            Apttus_Proposal__Account__r.BillingState,
                                                            Apttus_Proposal__Account__r.BillingPostalCode,
                                                            Apttus_Proposal__Account__r.BillingCity,
                                                            Apttus_Proposal__Account__r.BillingCountry,
                                                            Apttus_Proposal__Opportunity__c,
                                                            Destined_to_Partner__c,
                                                            RecordTypeId,
                                                            RecordType.Name,
                                                            RecordType.DeveloperName,
                                                            Deal_Type__c,
                                                            T_HLVL_VA__c,
                                                            Apttus_QPComply__MasterAgreementId__c,
                                                            Apttus_QPComply__MasterAgreementId__r.RecordTypeId,
                                                            Apttus_QPComply__MasterAgreementId__r.RecordType.Name,
                                                            Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName,
                                                            (Select Id, Name, Account__c,
                                                                Account__r.Name,
                                                                Account__r.Status__c,
                                                                Account__r.Primary_GPO_Buying_Group_Affiliation__c,
                                                                Account__r.Account_External_ID__c,
                                                                Account__r.BillingStreet,
                                                                Account__r.BillingState,
                                                                Account__r.BillingPostalCode,
                                                                Account__r.BillingCity,
                                                                Account__r.BillingCountry,
                                                                Proposal__c,
                                                                Implementation_Location__c,
                                                                Licensed_Location__c
                                                            From Participating_Facilities__r
                                                            Order by Account__r.Name, Account__r.Account_External_ID__c)
                                                        From Apttus_Proposal__Proposal__c s Where ID =: theRecord.ID];

            if (records.size() > 0) {
                theRecord = records[0];

                DealTypeName = CPQ_Utilities.getCoreDealTypeLabel(theRecord);
                System.Debug('*** DealTypeName ' + DealTypeName);

                incontracts = new Set<String>();
                if (theRecord.Deal_Type__c == 'Amendment' && CPQ_Utilities.isProposalDestinedToPartner(theRecord) && theRecord.Apttus_QPComply__MasterAgreementId__c != null) {
                    for (Agreement_Participating_Facility__c f : [Select Id, Name, Agreement__c, Account__c, Account__r.Account_External_ID__c, Implementation_Location__c, Licensed_Location__c From Agreement_Participating_Facility__c Where Account__r.Account_External_ID__c like 'US-%' and Agreement__c =: theRecord.Apttus_QPComply__MasterAgreementId__c]) {
                        incontracts.add(f.Account__r.Account_External_ID__c);
                    }
                }

                //update the "selected results" table from existing records
                for (Participating_Facility__c r : theRecord.Participating_Facilities__r) {
                    CPQ_FacilityItem f = new CPQ_FacilityItem();
                    f.account = r.Account__r;
                    f.disabled = incontracts.contains(r.Account__r.Account_External_ID__c);
                    f.implementationLocation = r.Implementation_Location__c;
                    f.licensedLocation = r.Licensed_Location__c;

                    if (!SelectedResultMap.containsKey(r.Account__r.Account_External_ID__c)) {
                        SelectedResultMap.put(r.Account__r.Account_External_ID__c, f);

                        SelectedResults.add(f);
                    }
                }

                for(CPQ_FacilityItem f : SelectedResultMap.values()){
                    if(f.implementationLocation == true){
                        hasImplementationLocation = true;
                        break;
                    }
                }

            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The record does not exist.'));
            }
        }

        //get the proposal record types that allow tracing customer as participating facilities
        RecordTypesAllowingTracingCustomer = CPQ_Utilities.getDealTypesAllowingTracingCustomer();
        System.Debug('*** RecordTypesAllowingTracingCustomer ' + RecordTypesAllowingTracingCustomer);
    }

    public PageReference setHasImplementationLocation() {
        hasImplementationLocation = !hasImplementationLocation;
        return null;
    }

/*=======================================================
    copy related records from other proposal or agreement
=========================================================*/
    public String SearchProposalSourceID {get;set;}
    public String SearchAgreementSourceID {get;set;}
    public String SearchSource {get;set;}
    public PageReference doSearchByCopy() {
        if (SearchSource == 'Proposal') {
            if (String.isEmpty(SearchProposalSourceID)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the source Proposal ID.'));
                return null;
            }

            //retrieve from source proposal
            List<Participating_Facility__c> records;
            if (surgicalRecordType) {
                records = [Select Id, Name, Account__c,
                                Account__r.Name,
                                Account__r.Status__c,
                                Account__r.Primary_GPO_Buying_Group_Affiliation__c,
                                Account__r.Account_External_ID__c,
                                Account__r.BillingStreet,
                                Account__r.BillingState,
                                Account__r.BillingPostalCode,
                                Account__r.BillingCity,
                                Account__r.BillingCountry,
                                Implementation_Location__c,
                                Licensed_Location__c,
                                Proposal__c,
                                Proposal__r.Name
                            From Participating_Facility__c
                            Where Proposal__r.Name =: SearchProposalSourceID and
                                Account__r.isActive__c = true and
                                Account__r.Account_External_ID__c like 'US-%'
                            Order by Account__r.Name, Account__r.Account_External_ID__c];
            } else {
                List<string> cots = CPQ_Utilities.getCOTFilters(theRecord);
                COTsToFilter = JSON.Serialize(cots);
                String gpo = theRecord.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c;
                String query = 'Select Id, Name, Account__c, ' +
                                'Account__r.Name, ' +
                                'Account__r.Status__c, ' +
                                'Account__r.Primary_GPO_Buying_Group_Affiliation__c, ' +
                                'Account__r.Account_External_ID__c, ' +
                                'Account__r.BillingStreet, ' +
                                'Account__r.BillingState, ' +
                                'Account__r.BillingPostalCode, ' +
                                'Account__r.BillingCity, ' +
                                'Account__r.BillingCountry, ' +
                                'Implementation_Location__c, ' +
                                'Licensed_Location__c, ' +
                                'Proposal__c, ' +
                                'Proposal__r.Name ' +
                            'From Participating_Facility__c ' +
                            'Where Proposal__r.Name = :SearchProposalSourceID and  ' +
                                'Account__r.Primary_GPO_Buying_Group_Affiliation__c = :gpo and  ' +
                                'Account__r.isActive__c = true and ' +
                                'Account__r.Account_External_ID__c like \'US-%\' AND ' +
            				    injectParticipatingFacilityQueryPredicate('Account__r') +
                            'Order by Account__r.Name, Account__r.Account_External_ID__c';
            records = Database.query(query);
            }

            if (records.size() > 0) {
                List<Account> inputAccts = new List<Account>();
                Map<String,Participating_Facility__c> inputAcctsMap = new Map<String,Participating_Facility__c>();
                for (Participating_Facility__c r : records) {
                    inputAccts.add(r.Account__r);
                    inputAcctsMap.put(r.Account__c,r);
                }

                List<Account> accounts = SetERPFilter(inputAccts);
                if (accounts.size() > 0) {
                    for (Account a : accounts) {
                        CPQ_FacilityItem f = new CPQ_FacilityItem();
                        f.account = a;
                        f.disabled = incontracts.contains(a.Account_External_ID__c);
                        f.implementationLocation = inputAcctsMap.get(a.Id).Implementation_Location__c;
                        f.licensedLocation = inputAcctsMap.get(a.Id).Licensed_Location__c;

                        if (!SelectedResultMap.containsKey(a.Account_External_ID__c)) {
                            SelectedResultMap.put(a.Account_External_ID__c, f);

                            SelectedResults.add(f);
                        }
                    }

                    SelectedResults.sort();
                    return null;
                }
            }

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Participating Facility is found for the given Proposal ' + SearchProposalSourceID + '.'));
        } else if (SearchSource == 'Agreement') {
            if (String.isEmpty(SearchAgreementSourceID)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the source Agreement ID.'));
                return null;
            }

            //retrieve from source agreement
            List<Agreement_Participating_Facility__c> records;
            if (surgicalRecordType) {
                records = [Select Id, Name, Account__c,
                                Account__r.Name,
                                Account__r.Status__c,
                                Account__r.Primary_GPO_Buying_Group_Affiliation__c,
                                Account__r.Account_External_ID__c,
                                Account__r.BillingStreet,
                                Account__r.BillingState,
                                Account__r.BillingPostalCode,
                                Account__r.BillingCity,
                                Account__r.BillingCountry,
                                Implementation_Location__c,
                                Licensed_Location__c,
                                Agreement__c,
                                Agreement__r.AgreementId__c
                            From Agreement_Participating_Facility__c
                            Where Agreement__r.AgreementId__c =: SearchAgreementSourceID and
                                Account__r.isActive__c = true and
                                Account__r.Account_External_ID__c like 'US-%'
                            Order by Account__r.Name, Account__r.Account_External_ID__c];
            } else {
                List<string> cots = CPQ_Utilities.getCOTFilters(theRecord);
                COTsToFilter = JSON.Serialize(cots);
               String gpo = theRecord.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c;
               String query =
                'Select Id, Name, Account__c, ' +
                                'Account__r.Name, ' +
                                'Account__r.Status__c, ' +
                                'Account__r.Primary_GPO_Buying_Group_Affiliation__c, ' +
                                'Account__r.Account_External_ID__c, ' +
                                'Account__r.BillingStreet, ' +
                                'Account__r.BillingState, ' +
                                'Account__r.BillingPostalCode, ' +
                                'Account__r.BillingCity, ' +
                                'Account__r.BillingCountry, ' +
                                'Implementation_Location__c, ' +
                                'Licensed_Location__c, ' +
                                'Agreement__c, ' +
                                'Agreement__r.AgreementId__c ' +
                            'From Agreement_Participating_Facility__c ' +
                            'Where Agreement__r.AgreementId__c = :SearchAgreementSourceID and ' +
                                'Account__r.Primary_GPO_Buying_Group_Affiliation__c = :gpo  and ' +
                                'Account__r.isActive__c = true and ' +
                                'Account__r.Account_External_ID__c like \'US-%\' AND ' +
                                injectParticipatingFacilityQueryPredicate('Account__r') +
                            'Order by Account__r.Name, Account__r.Account_External_ID__c';
            records = Database.query(query);
            }

            if (records.size() > 0) {
                List<Account> inputAccts = new List<Account>();
                Map<String,Agreement_Participating_Facility__c> inputAcctsMap = new Map<String,Agreement_Participating_Facility__c>();
                for (Agreement_Participating_Facility__c r : records) {
                    inputAccts.add(r.Account__r);
                    inputAcctsMap.put(r.Account__c,r);
                }

                List<Account> accounts = SetERPFilter(inputAccts);
                if (accounts.size() > 0) {
                    for (Account a : accounts) {
                        CPQ_FacilityItem f = new CPQ_FacilityItem();
                        f.account = a;
                        f.disabled = incontracts.contains(a.Account_External_ID__c);
                        f.implementationLocation = inputAcctsMap.get(a.Id).Implementation_Location__c;
                        f.licensedLocation = inputAcctsMap.get(a.Id).Licensed_Location__c;

                        if (!SelectedResultMap.containsKey(a.Account_External_ID__c)) {
                            SelectedResultMap.put(a.Account_External_ID__c, f);

                            SelectedResults.add(f);
                        }
                    }

                    SelectedResults.sort();
                    return null;
                }
            }

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Participating Facility is found for the given Agreement ' + SearchAgreementSourceID + '.'));
        }

        return null;
    }

/*=======================================================
    search by CRN
=========================================================*/
    public String SearchCRNs{get;set;}
    public PageReference doSearchByCRNs() {
        SearchResults.clear();
        SearchAllChecked = false;

        if (String.isEmpty(SearchCRNs)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Customer Report Number to search.'));
            return null;
        }

        String[] a = SearchCRNs.split(',');
        Set<String> inList = new Set<String>();
        for (String s : a) {
            if (!String.isEmpty(s)) {
                String trimmed = s.trim();
                if (!String.isEmpty(trimmed)) {
                    if (trimmed.startsWith('US-'))
                        inList.add(trimmed);
                    else
                        inList.add('US-' + trimmed);
                }
            }
        }

        List<Account> records;
        if (surgicalRecordType) {
            records = [Select Id, Name,
                            Status__c,
                            Primary_GPO_Buying_Group_Affiliation__c,
                            Account_External_ID__c,
                            BillingStreet,
                            BillingState,
                            BillingPostalCode,
                            BillingCity,
                            BillingCountry
                        From Account
                        Where Account_External_ID__c in: inList and
                            isActive__c = true
                        Order by Name, Account_External_ID__c];
        } else {
            List<string> cots = CPQ_Utilities.getCOTFilters(theRecord);
            COTsToFilter = JSON.Serialize(cots);
            String gpo = theRecord.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c;
            String soql = 'Select Id, Name, ' +
                            'Status__c, ' +
                            'Primary_GPO_Buying_Group_Affiliation__c, ' +
                            'Account_External_ID__c, ' +
                            'BillingStreet, ' +
                            'BillingState, ' +
                            'BillingPostalCode, ' +
                            'BillingCity, ' +
                            'BillingCountry ' +
                        'From Account ' +
                        'Where Account_External_ID__c in: inList and ' +
                            'Primary_GPO_Buying_Group_Affiliation__c = :gpo and ' +
                            'isActive__c = true AND ' +
                            injectParticipatingFacilityQueryPredicate() +
                        'Order by Name, Account_External_ID__c';
        
            records = Database.query(soql);
        }
        if (records.size() > 0) {
            List<Account> accounts = SetERPFilter(records);
            if (accounts.size() > 0) {
                for (Account r : accounts) {
                    if (!String.isEmpty(CPQ_Utilities.ValidCRN(r.Account_External_ID__c))) {
                        CPQ_FacilityItem f = new CPQ_FacilityItem();
                        f.account = r;
                        SearchResults.add(f);
                    }
                }

                SearchResults.sort();
                return null;
            }
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Participating Facility is found for the given CRNs.'));
        return null;
    }

/*=======================================================
    search by Allied Groups
=========================================================*/
    public String SearchAllied{get;set;}
    public PageReference doSearchByAllied() {
        SearchResults.clear();
        SearchAllChecked = false;

        if (String.isEmpty(SearchAllied)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Allied Group to search.'));
            return null;
        }

        String[] splitList = SearchAllied.split(',');
        String searchClause = '';
        if (splitList != null && !splitList.isEmpty()) {
            searchClause += ' And (';
            Boolean first = true;
            for (String s : splitList) {
                if (!String.isEmpty(s)) {
                    String trimmed = s.trim();
                    if (!String.isEmpty(trimmed)) {
                        if (!first) {
                            searchClause += ' Or ';
                        } else {
                            first = false;
                        }
                        searchClause += 'Allied_Group__c like \'%' + trimmed + '%\'';
                    }
                }
            }
            searchClause += ')';
        }

        List<Account> records;

        if (surgicalRecordType) {
            String sqlQuery = 'Select Id, Name, Status__c, Primary_GPO_Buying_Group_Affiliation__c, Account_External_ID__c, BillingStreet, BillingState, BillingPostalCode, BillingCity, BillingCountry, Allied_Group__c From Account Where isActive__c = true ';
            sqlQuery += searchClause;
            sqlQuery += ' Order by Name, Allied_Group__c';
            records = Database.query(sqlQuery);
        } else {
            List<string> cots = CPQ_Utilities.getCOTFilters(theRecord);
            COTsToFilter = JSON.Serialize(cots);
            String primaryGroupAffiliation = theRecord.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c;
            String sqlQuery = 'Select Id, Name, Status__c, Primary_GPO_Buying_Group_Affiliation__c, Account_External_ID__c, ' +
                'BillingStreet, BillingState, BillingPostalCode, BillingCity, BillingCountry, Allied_Group__c From Account ' +
                'Where isActive__c = true AND ' + injectParticipatingFacilityQueryPredicate();
            sqlQuery += searchClause;
            sqlQuery += ' and Primary_GPO_Buying_Group_Affiliation__c = :primaryGroupAffiliation and isActive__c = true AND ' +
             injectParticipatingFacilityQueryPredicate() +
            ' Order by Name, Allied_Group__c';
            records = Database.query(sqlQuery);
        }

        if (records.size() > 0) {
            List<Account> accounts = SetERPFilter(records);
            if (accounts.size() > 0) {
                for (Account r : accounts) {
                    if (!String.isEmpty(CPQ_Utilities.ValidCRN(r.Account_External_ID__c))) {
                        CPQ_FacilityItem f = new CPQ_FacilityItem();
                        f.account = r;
                        SearchResults.add(f);
                    }
                }

                SearchResults.sort();
                return null;
            }
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Participating Facility is found for the given Allied Groups.'));
        return null;
    }

/*=======================================================
    general search
=========================================================*/
    public String SearchString{get;set;}
    public PageReference doSearch() {
        SearchResults.clear();
        SearchAllChecked = false;

        if (String.isEmpty(SearchString)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter search criteria.'));
            return null;
        }
        String GPOFilter;
        if (surgicalRecordType) {
            GPOFilter = '';
        } else {
            GPOFilter = theRecord.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c == null ?
                            'Primary_GPO_Buying_Group_Affiliation__c = NULL and' :
                            'Primary_GPO_Buying_Group_Affiliation__c = \'' + theRecord.Apttus_Proposal__Account__r.Primary_GPO_Buying_Group_Affiliation__c + '\' and';
        }

        List<string> cots = CPQ_Utilities.getCOTFilters(theRecord);
        COTsToFilter = JSON.Serialize(cots);
        String searchQuery='FIND \'' + SearchString + '\' ' +
                            'RETURNING Account(Id, Name, ' +
                                'Status__c, ' +
                                'Primary_GPO_Buying_Group_Affiliation__c, ' +
                                'Account_External_ID__c, ' +
                                'BillingStreet, ' +
                                'BillingState, ' +
                                'BillingPostalCode, ' +
                                'BillingCity, ' +
                                'BillingCountry ' +
                            'Where ' + GPOFilter + ' isActive__c = true and ' +
                                'Account_External_ID__c like \'US-%\' AND ' +
                             injectParticipatingFacilityQueryPredicate() +
                            ' Order by Name, Account_External_ID__c)';

        List<List<SObject>> searchList;

        if (Test.isRunningTest()) {
            List<Account> testAccounts = new List<Account>{
                new Account(Status__c = 'Active', Name = 'Test Facilitiy', Account_External_ID__c = 'US-111111', AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Class_of_Trade__c = '001', Secondary_Class_of_Trade__c = '00A' ),
                new Account(Status__c = 'Active', Name = 'Test Facilitiy 2', Account_External_ID__c = 'US-222222', AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Class_of_Trade__c = '002', Secondary_Class_of_Trade__c = '01A' ),
                new Account(Status__c = 'Active', Name = 'Test Facilitiy 3', Account_External_ID__c = 'US-333333', AccountType__c = 'Hospital', BillingCountry = 'USA', BillingPostalCode = '10000', BillingState = 'CO', BillingCity = 'Denver', BillingStreet = '123 Main', Class_of_Trade__c = '003', Secondary_Class_of_Trade__c = '00A' )
            };
            insert testAccounts;

            List<ERP_Account__c> testERPs = new List<ERP_Account__c> {
                new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[0].ID, ERP_Account_Type__c = 'S', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
                new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[1].ID, ERP_Account_Type__c = 'X', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active'),
                new ERP_Account__c(Name = 'Test ERP', Parent_Sell_To_Account__c = testAccounts[2].ID, ERP_Account_Type__c = 'TS', ERP_Source__c = 'E1', ERP_Account_Status__c = 'Active')
            };
            insert testERPs;
            searchList = new List<List<Account>>();
            searchList.add(testAccounts);
        } else
            searchList = Search.query(searchQuery);

        List<Account> records = searchList[0];
        if (records.size() > 0) {
            List<Account> accounts = SetERPFilter(records);
            if (accounts.size() > 0) {
                for (Account r : accounts) {
                    if (!String.isEmpty(CPQ_Utilities.ValidCRN(r.Account_External_ID__c))) {
                        CPQ_FacilityItem f = new CPQ_FacilityItem();
                        f.account = r;
                        SearchResults.add(f);
                    }
                }

                SearchResults.sort();
                return null;
            }
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Participating Facility is found for the given criteria.'));
        return null;
    }

    //check all checkboxes
    public Boolean SearchAllChecked {get; set;}
    public PageReference doSearchCheckAll() {
        for (CPQ_FacilityItem f : SearchResults) {
            f.checked = SearchAllChecked;
        }

        return null;
    }

    public Boolean getShowSearchResult() {
        return SearchResults.size() > 0;
    }

    public List<CPQ_FacilityItem> SearchResults {
        get {
            if (SearchResults == null) {
                SearchResults = new List<CPQ_FacilityItem>();
            }
            return SearchResults;
        }
        set;
    }

    public Boolean getAddCheckedEnabled() {
        for (CPQ_FacilityItem f : SearchResults) {
            if (f.checked == true)
                return true;
        }

        return false;
    }

    //add all checked
    public PageReference doAddChecked() {
        SelectAllChecked = false;
        for (CPQ_FacilityItem f : SearchResults) {
            if (f.checked) {
                if (!SelectedResultMap.containsKey(f.account.Account_External_ID__c)) {
                    CPQ_FacilityItem copy = f.copy();
                    copy.bgColor = CPQ_FacilityItem.INPUT_COLOR;
                    copy.disabled = incontracts.contains(f.account.Account_External_ID__c);

                    SelectedResultMap.put(f.account.Account_External_ID__c, copy);
                    SelectedResults.add(copy);
                }
            }
        }

        SearchResults.clear();

        SelectedResults.sort();

        return null;
    }

    //check all checkboxes
    public Boolean SelectAllChecked {get; set;}
    public PageReference doSelectCheckAll() {
        for (CPQ_FacilityItem f : SelectedResults) {
            if (f.disabled == false)
                f.checked = SelectAllChecked;
        }

        return null;
    }

    //remove all checked
    public Boolean getRemoveCheckedEnabled() {
        for (CPQ_FacilityItem f : SelectedResults) {
            if (f.checked == true)
                return true;
        }

        return false;
    }

    public PageReference doRemoveChecked() {
        List<CPQ_FacilityItem> l = new List<CPQ_FacilityItem>();
        Map<String, CPQ_FacilityItem> m = new Map<String, CPQ_FacilityItem>();

        for (CPQ_FacilityItem f : SelectedResults) {
            if (!f.checked) {
                l.add(f);
                m.put(f.account.Account_External_ID__c, f);
            }
        }

        SelectedResults = l;
        SelectedResultMap = m;
        return null;
    }

    public Boolean getShowSelectedResult() {
        return SelectedResults.size() > 0;
    }

    public Map<String, CPQ_FacilityItem> SelectedResultMap;
    public List<CPQ_FacilityItem> SelectedResults {
        get {
            if (SelectedResults == null) {
                SelectedResults = new List<CPQ_FacilityItem>();
            }
            return SelectedResults;
        }
        set;
    }

    //Save, add if CRNs do not existing for exisitng proposal. deleted if CRNs are no longer selected
    public PageReference doSave() {

        Set<String> updateKeys = new Set<String>();

        Map<String, Participating_Facility__c> existings = new Map<String, Participating_Facility__c>();
        for (Participating_Facility__c r : theRecord.Participating_Facilities__r) {
            existings.put(r.Account__r.Account_External_ID__c, r);
            updateKeys.add(r.Account__r.Account_External_ID__c);
        }

        List<Participating_Facility__c> toInsert = new List<Participating_Facility__c>();
        for (String key : SelectedResultMap.keySet()) {
            if (!existings.containsKey(key)) {
                CPQ_FacilityItem f = SelectedResultMap.get(key);

                Participating_Facility__c r = new Participating_Facility__c();
                r.Proposal__c = theRecord.ID;
                r.Account__c = f.account.ID;
                r.Implementation_Location__c = f.implementationLocation;
                r.Licensed_Location__c = f.licensedLocation;

                toInsert.add(r);
            }
        }

        List<Participating_Facility__c> toDelete = new List<Participating_Facility__c>();
        for (String key : existings.keySet()) {
            if (!SelectedResultMap.containsKey(key)) {
                toDelete.add(existings.get(key));
                updateKeys.remove(key);
            }
        }

        List<Participating_Facility__c> toUpdate = new List<Participating_Facility__c>();
        for(String key : updateKeys){

            CPQ_FacilityItem f = SelectedResultMap.get(key);
            Participating_Facility__c pf = existings.get(key);
            Boolean forUpdate = false;

            if(pf.Implementation_Location__c != f.implementationLocation){
                pf.Implementation_Location__c = f.implementationLocation;
                forUpdate = true;
            }
            if(pf.Licensed_Location__c != f.licensedLocation){
                pf.Licensed_Location__c = f.licensedLocation;
                forUpdate = true;
            }
            if(forUpdate){
                toUpdate.add(pf);
            }
        }

        // Update Existing Records with Implementation/Licensed Location Checked

        if (toInsert.size() > 0)
            insert toInsert;

        if (toDelete.size() > 0)
            delete toDelete;

        if (toUpdate.size() > 0)
            update toUpdate;

        return new ApexPages.StandardController(theRecord).view();
    }

    //additional filter on selectable accounts:
    //must have an ERP record that have Bill Type Code "S", "X",
    //for certain deal type, bill type code = "TS", "TX" are also allowed to be selected
    private List<Account> SetERPFilter(List<Account> inputs) {

        Set<ID> hasNoERPs = new Set<ID>();
        Set<ID> hasOnlyTracings = new Set<ID>();
        CPQ_Utilities.ERPFilter(inputs, hasNoERPs, hasOnlyTracings);
        Boolean allowTracing = RecordTypesAllowingTracingCustomer.contains(DealTypeName);

        List<Account> outputs = new List<Account>();
        for (Account a : inputs) {
            if (!hasNoERPs.contains(a.Id) && (allowTracing || !hasOnlyTracings.contains(a.Id))) {
                outputs.add(a);
            }
        }

        return outputs;
    }
}