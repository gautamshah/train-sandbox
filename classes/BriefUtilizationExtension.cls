public with sharing class BriefUtilizationExtension {

    private Brief_Utilization_Tool__c briefUsageRecord;
    public list<Brief_Utilization_Tool__c> briefUsageRecs {get; set;}

    public BriefUtilizationExtension(ApexPages.StandardController controller) {
        this.briefUsageRecord = (Brief_Utilization_Tool__c)controller.getRecord();
            System.debug('----briefUsageRecord ----' + briefUsageRecord );
        briefUsageRecs = new List<Brief_Utilization_Tool__c>();    
        addRow();
            System.debug('----briefUsageRecs----' + briefUsageRecs);
    }
    
    public void addRow(){
        briefUsageRecs.add(new Brief_Utilization_Tool__c(Account__c = briefUsageRecord.Account__c));
    }

     public PageReference saveRecords(){
        try{
            upsert briefUsageRecs;
        }
        catch(exception e){
        
        }
        String retUrl = Apexpages.currentPage().getParameters().get('retUrl');
        if(Apexpages.currentPage().getParameters().get('retUrl') == null || Apexpages.currentPage().getParameters().get('retUrl') == ''){
            retUrl = '/' + briefUsageRecord.Account__c;
        }
        PageReference pageRef = new PageReference(retUrl);
        return pageRef;
   }
   
       public void removeLastEmptyRecord(){
        Integer indexOfRecToRemove = briefUsageRecs.size() - 1;
            System.debug(briefUsageRecs.size() + '----indexOfRecToRemove----' + indexOfRecToRemove);
        Brief_Utilization_Tool__c BriefUsageRec = briefUsageRecs.get(indexOfRecToRemove);
        briefUsageRecs.remove(indexOfRecToRemove);  
   }
   
   public PageReference cancel(){
        String retUrl = Apexpages.currentPage().getParameters().get('retUrl');
        if(Apexpages.currentPage().getParameters().get('retUrl') == null || Apexpages.currentPage().getParameters().get('retUrl') == ''){
            retUrl = '/' + briefUsageRecord.Account__c;
        }
        PageReference pageRef = new PageReference(retUrl);
        return pageRef;
    }
}