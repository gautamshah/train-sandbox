/********************************************************************************************************************
  * Name    : User_TriggerHandler
  * Author  : Angad Pathak 
  * Date    : 12-Dec-2013
  * Purpose : To create a case and to notify asia admin queue when a non-admin asia user creates a user
  * 
  * ========================
  * = MODIFICATION HISTORY =
  * ========================
  * DATE        AUTHOR        CHANGE
  * ---------------  ---------------     -------
  * 12-Dec-2013    Angad Pathak    Method added - createCaseNotificationMethod
  *
  *********************************************************************************************************************/
public class User_TriggerHandler 
{
    @future
    public static void createCaseNotificationMethod(list <id> triggerObjectIds)
    {
        list <Case> caseList = new list <Case>();
        list <RecordType> CrmSupportRtype = [SELECT id,DeveloperName FROM RecordType where DeveloperName = 'CRM_Support'];
        list <Messaging.SingleEmailmessage> listEmailObject;
        list <User> asiaAdminQueueUsers;
        list <Group> asiaAdminQueue = [SELECT id,name,developername FROM Group WHERE developername = 'ASIA_Admin_Support'];
        
        
        list <String> userNames = new list <String>();
        
        if(CrmSupportRtype != null && CrmSupportRtype.size() > 0 && asiaAdminQueue != null && asiaAdminQueue.size() > 0)
        {
            for(User u : [select id,name from User where id IN :triggerObjectIds AND createdby.profile.name IN ('Asia - CN','Asia - HK / SG','Asia - KR','Asia Distributor - KR','Asia-CN-Marketing')])
            {
                Case c = new Case();
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = false;
                c.subject = 'New User Created';
                c.Description = 'A new has been created. New User : ' + u.name + ' Created By : ' + userinfo.getname();
                c.recordtypeid = CrmSupportRtype[0].id;
                c.Object__c = 'Users/Groups';
                c.Problem_Type__c = 'User Set Up';
                c.Admin_Priority__c = 'Medium';
                c.ownerid = asiaAdminQueue[0].id;
                c.setOptions(dmo);
                caseList.add(c);
                
                userNames.add(u.name);
            }
            
            if(caseList.size() > 0)
            {
                insert caseList;
                system.debug('No of cases inserted--->'+caseList.size());
                asiaAdminQueueUsers = [select id,name from user where id in (select UserOrGroupId from GroupMember where GroupId In : asiaAdminQueue)];
                
                if(asiaAdminQueueUsers != null && asiaAdminQueueUsers.size() > 0)
                {
                    system.debug('No of users in asia admin queue --->'+asiaAdminQueueUsers.size());
                    
                    caseList = [SELECT id,CaseNumber FROM Case WHERE id in :caseList]; // To fetch case numbers
                    listEmailObject = new list <Messaging.SingleEmailmessage>();
                    for(integer i=0;i <caseList.size();i++)
                    {
                        for(User u : asiaAdminQueueUsers)
                        {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setTargetObjectId(u.id);
                            mail.setSubject('Case # '+ caseList[i].CaseNumber + ': New User Created');
                            mail.sethtmlBody('New User has been created.<br/><br/>New User : '+ userNames[i] +'<br/>Created By : '+ userinfo.getname() +'<br/><br/>Case # <a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/' + caseList[i].id +'">'+caseList[i].caseNumber+'</a> has been assigned to '+ asiaAdminQueue[0].name +' .<br/>');
                            mail.setsaveAsActivity(false);
                            mail.setCharset('UTF-8');
                            listEmailObject.add(mail);
                        }
                    }
                    try
                    {
                        Messaging.sendEmail(listEmailObject);
                        system.debug('No of emails sent--->' + listEmailObject.size());
                    }
                    catch(exception e){}
                }               
            }
        }           
    }
}