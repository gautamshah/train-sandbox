/*
Util class for CPQ project

CHANGE HISTORY
===============================================================================
DATE        NAME           DESC
2014-11/21  Yuli Fintescu  Created.
2015-08-17  Paul Berglund  Added method for checking the Task Subject
                           to determine if we need to set the Owner to
                           the the Agreement Owner
2015-10-07  Paul Berglund  Added method for checking the Task Subject
                           to determine if we need to send an email
                           to the Agreement Owner when Customer
                           Service finishes their task
2015-10-12  Paul Berglund  Added method for checking profiles allowed
                           to see the COOP Price File
10/15/2015  Paul Berglund  Added North Star Config class and constructor
10/19/2015  Paul Berglund  Added static CPQ_Config_Settings__c variable
                           called SystemProperties so there is only 1
                           DML statement to populate it for the entire
                           class (in case CPQ_Utilities was called
                           more than once).
10/26/2015  Paul Berglund  Created InitializeSystemProperties and added
                           it to the methods that are using SystemProperties.
                           This will initialize the SystemProperites
                           variable 1 time, regardless of how many
                           times the properties are used.
11/10/2015  Paul Berglund  Added methods for the CPQApprovers class
11/11/2015  Paul Berglund  Added enum and method for SalesOrg
02/18/2016  Paul Berglund  Removed the check for 'Locked' record types
                                    in validRecordTypes()
10/8/2016   Paul Berglund  Moved UOM translation code to cpqProduct_UOM_u
10/8/2016   Paul Berglund  Moved Price List Id lookup logic to cpqPriceList_repo
10/19/2016  Isaac Lewis    Migrated class to cpq_u and cpqDeal_u:
                            - Moved SystemProperties to cpq_u
                            - Moved record type reference logic to cpqDeal_u
                            - Removed territory-based approver logic:
                              validRecordType, validAgreementRecordType, validProposalRecordType
                            - Migrate remaining methods to cpq_u
10/26/2016  Paul Berglund  Refactored getStandardPriceBookID
01/24/2017  Isaac Lewis    Moved reference from cpq_u.buildObjectQuery to SOQL_select.buildObjectQuery
*/
public class CPQ_Utilities
{
    /**
    * Returns a list of custom field names
    */
    public static Set<String> getCustomFieldNames(Schema.SObjectType token, Boolean modifiableOnly)
    {
        return sObject_c.getCustomFieldNames(token,modifiableOnly);

        //Set<String> fieldNames = new Set<String>();

        //Map<String, Schema.SObjectField> M = token.getDescribe().fields.getMap();
        //for (String s : M.keySet()) {
        //    Schema.DescribeFieldResult r = M.get(s).getDescribe();
        //    if (r.isCustom() && (!modifiableOnly || (modifiableOnly && !r.isAutoNumber() && !r.isCalculated()))) {
        //        fieldNames.add(r.getName());
        //    }
        //}

        //return fieldNames;
    }

    /**
    * Builds query string
    */
    public static String buildObjectQuery(
        Set<String> fieldNames,
        String specialFields,
        String whereClause,
        String sObjectName)
    {
        return
        SOQL_select.buildObjectQuery(fieldNames,specialFields,whereClause,sObjectName);

        //String queryStr = 'SELECT ';

        //// standard fields
        //queryStr += 'Id';
        //queryStr += ', Name';

        //if(specialFields != null) {
        //    queryStr += ',' + specialFields;
        //}

        //// custom fields
        //if (fieldNames != null) {
        //    for (String fieldName : fieldNames) {
        //        if (fieldName == 'Id' || fieldName == 'Name') {
        //            continue;
        //        }

        //        queryStr += ',' + fieldName;
        //    }
        //}

        //queryStr += ' FROM ' + sObjectName;

        //if(whereClause != null) {
        //    queryStr += ' WHERE ';
        //    queryStr += whereClause;
        //}

        //return queryStr;
    }

/*=========================================================
    translate number of months to duration picklist value
===========================================================*/
    public static String calculateDurationOption(Integer duration)
    {
        return cpq_u.getDurationOption(duration);

        //Integer quotient = Integer.valueOf(duration/12);
        //Integer remainder = Integer.valueOf(Math.mod(duration, 12));

        //String fraction = '';
        //if (remainder == 3) {
        //    fraction = '1/4';
        //} else if (remainder == 6) {
        //    fraction = '1/2';
        //} else if (remainder == 9) {
        //    fraction = '3/4';
        //}

        //String option;
        //if (quotient == 1 && fraction == '')
        //    option = '1 year';
        //else if (quotient == 0 && fraction == '')
        //    option = null;
        //else if (quotient == 0 && fraction != '')
        //    option = fraction + ' years';
        //else if (fraction == '')
        //    option = String.valueOf(quotient) + ' years';
        //else
        //    option = String.valueOf(quotient) + ' ' + fraction + ' years';

        //return option;
    }

/*=========================================================
    translate duration picklist value to number of months
===========================================================*/
    public static Integer calculateDurationInMonth(String dur)
    {
        return cpq_u.getDurationMonths(dur);

        //if (dur == null) {
        //    return NULL;
        //} else {
        //    String duration = dur.toUpperCase();

        //    integer y = duration.indexOf(' YEAR');
        //    if (y > 0) {
        //        String n = duration.substring(0, y);
        //        String[] a = n.split(' ');

        //        try {
        //            Integer t = Integer.valueOf(a[0]);
        //            Integer m = 0;
        //            if (a.size() > 1) {
        //                String f = a[1].trim();
        //                if (f == '1/4') {
        //                    m = 3;
        //                } else if (f == '1/2') {
        //                    m = 6;
        //                } else if (f == '3/4') {
        //                    m = 9;
        //                }
        //            }
        //            return t * 12 + m;
        //        } catch (Exception e){
        //            String f = a[0].trim();
        //            if (f == '1/4') {
        //                return 3;
        //            } else if (f == '1/2') {
        //                return 6;
        //            } else if (f == '3/4') {
        //                return 9;
        //            }
        //
        //            return NULL;
        //        }
        //    } else {
        //        return NULL;
        //    }
        //}
    }

/*=========================================================
    translate code in UOM_Desc__c to Apttus_Config2__Uom__c value
===========================================================*/
    public static String translateUOM(String UOMCode)
    {
        return Product2_Uom_g.translateCode(UOMCode);

        //if (UOMCode == 'CT')
        //    return 'Carton';
        //else if (UOMCode == 'EA')
        //    return 'Each';
        //else if (UOMCode == 'BX')
        //    return 'Box';
        //else if (UOMCode == 'CA')
        //    return 'Case';
        //else if (UOMCode == 'PK')
        //    return 'Package';
        //else if (UOMCode == 'GA')
        //    return 'Gallon';
        //else if (UOMCode == 'BG')
        //    return 'Bag';

        //return 'Each';
    }

/*=========================================================
    translate Apttus_Config2__Uom__c value to code in UOM_Desc__c
===========================================================*/
    public static String translateUOMText(String UOMText)
    {
        return Product2_Uom_g.translateText(UOMText);

        //if (UOMText == 'Carton')
        //    return 'CT';
        //else if (UOMText == 'Each')
        //    return 'EA';
        //else if (UOMText == 'Box')
        //    return 'BX';
        //else if (UOMText == 'Case')
        //    return 'CA';
        //else if (UOMText == 'Package')
        //    return 'PK';
        //else if (UOMText == 'Gallon')
        //    return 'GA';
        //else if (UOMText == 'Bag')
        //    return 'BG';

        //return 'EA';
    }

/*=========================================================
    get default RMS PriceList ID
===========================================================*/

    //private final static String PRICELIST = 'Covidien RMS US Master';
    public static Id getRMSPriceListId()
    {
        return cpqPriceList_c.RMS.Id;

        //String thePriceListID = null;
        //CPQ_Variable__c cs = CPQ_Variable__c.getValues('RMS PriceList ID');
        //if (cs == null) {
        //    List<Apttus_Config2__PriceList__c> records = [Select ID From Apttus_Config2__PriceList__c Where Name =: PRICELIST];
        //    if (records.size() > 0) {
        //        thePriceListID = records[0].ID;
        //    }
        //} else
        //    thePriceListID = cs.Value__c;

        //return thePriceListID;
    }

/*=========================================================
    get default SSG PriceList ID
===========================================================*/
    //private final static String SSG_PRICELIST = 'Covidien SSG US Master';
    public static Id getSSGPriceListID()
    {
        return cpqPriceList_c.SSG.Id;

        //String thePriceListID = null;
        //CPQ_Variable__c cs = CPQ_Variable__c.getValues('SSG PriceList ID');
        //if (cs == null) {
        //    List<Apttus_Config2__PriceList__c> records = [Select ID From Apttus_Config2__PriceList__c Where Name =: SSG_PRICELIST];
        //    if (records.size() > 0) {
        //        thePriceListID = records[0].ID;
        //    }
        //} else
        //    thePriceListID = cs.Value__c;

        //return thePriceListID;
    }

    public static String getMSPriceListID()
    {
        return cpqPriceList_c.MS.Id;
    }

/*=========================================================
    get businessday cut off hour
===========================================================*/
    public static DateTime getBusinessDayCutOffHour()
    {
        return cpqConfigSetting_c.getBusinessDayCutOffHour();

        //InitializeSystemProperties();
        //DateTime cutOff = DateTime.newInstanceGMT(System.Today(), Time.newInstance(15, 0, 0, 0));

        //if (SystemProperties != null && !String.isEmpty(SystemProperties.BusinessDay_Cutoff_Hour__c)) {
        //    String[] a = SystemProperties.BusinessDay_Cutoff_Hour__c.split(':');
        //    if (a.size() == 2) {
        //        try {
        //            cutOff = DateTime.newInstanceGMT(System.Today(), Time.newInstance(Integer.valueOf(a[0]), Integer.valueOf(a[1]), 0, 0));
        //        } catch (Exception e) {}
        //    }
        //}

        //return cutOff;
    }

/*=========================================================
    get renewal uplift percent global setup
===========================================================*/
    public static Decimal getRenewalUpliftPercentage()
    {
        return cpqConfigSetting_c.getRenewalUpliftPercentage();

        //InitializeSystemProperties();

        //Decimal uplift = 3.00;
        //if (SystemProperties != null && SystemProperties.Renewal_Uplift_perc__c != null) {
        //    uplift = SystemProperties.Renewal_Uplift_perc__c;
        //}

        //return uplift;
    }

/*=========================================================
    get ws endpoint
===========================================================*/
    public static String getProxyEndpoint()
    {
        return cpqVariable_c.getValue('Proxy EndPoint');

        //return cpq_u.getProxyEndpoint();

        //String endpoint = null;
        //CPQ_Variable__c cs = CPQ_Variable__c.getValues('Proxy EndPoint');
        //if (cs != null)
        //    endpoint = cs.Value__c;

        //return endpoint;
    }

/*=========================================================
    get PricingCallBack ChargeType Ignore List in string set
===========================================================*/
    public static Set<String> getCallbackChargeTypeIgnoreList()
    {
        return cpqChargeType_c.IgnoreList;

        //Set<String> ignores = new Set<String>();
        //CPQ_Variable__c cs = CPQ_Variable__c.getValues('PricingCallBack ChargeType Ignore List');
        //if (cs != null && cs.Value__c != null) {
        //    String[] a = cs.Value__c.split('\\|');
        //    for (String s : a) {
        //        if (!String.isEmpty(s))
        //            ignores.add(s);
        //    }
        //}

        //return ignores;
    }

/*=========================================================
    get DealTypes Allow Tracing company
===========================================================*/
    public static Set<String> getDealTypesAllowingTracingCustomer()
    {
        return cpqDeal_Type_c.getDealTypesAllowingTracingCustomer();

        //InitializeSystemProperties();

        //Set<String> dealTypes = new Set<String>();

        //if (SystemProperties != null && SystemProperties.Deal_Types_Allowing_Tracing_Customers__c != null) {
        //    dealTypes = CPQ_Utilities.MultiPicklistValues(SystemProperties.Deal_Types_Allowing_Tracing_Customers__c);
        //}

        //return dealTypes;
    }

/*=========================================================
    get DealTypes requiring no cart
===========================================================*/
    public static Set<String> getDealTypesRequiringNoCart()
    {
        return cpqDeal_Type_c.getDealTypesRequiringNoCart();

        //InitializeSystemProperties();

        //Set<String> dealTypes = new Set<String>();

        //if (SystemProperties != null && SystemProperties.Deal_Types_Requiring_No_Cart__c != null) {
        //    dealTypes = CPQ_Utilities.MultiPicklistValues(SystemProperties.Deal_Types_Requiring_No_Cart__c);
        //}

        //return dealTypes;
    }

/*=========================================================
    get DealTypes requiring no opportunity
===========================================================*/
    public static Set<String> getDealTypesRequiringNoOpportunity()
    {
        return cpqDeal_Type_c.getDealTypesRequiringNoOpportunity();

        //InitializeSystemProperties();

        //Set<String> dealTypes = new Set<String>();

        //if (SystemProperties != null && SystemProperties.Deal_Types_Requiring_No_Opportunity__c != null) {
        //    dealTypes = CPQ_Utilities.MultiPicklistValues(SystemProperties.Deal_Types_Requiring_No_Opportunity__c);
        //}

        //return dealTypes;
    }

/*=========================================================
    get DealTypes not allow adjust price
===========================================================*/
    public static Set<String> getDealTypesNotAllowingPriceAdjustment()
    {
        return cpqDeal_Type_c.getDealTypesNotAllowingPriceAdjustment();

        //InitializeSystemProperties();

        //Set<String> dealTypes = new Set<String>();

        //if (SystemProperties != null && SystemProperties.Deal_Types_Not_Allowing_Price_Adjustment__c != null) {
        //    dealTypes = CPQ_Utilities.MultiPicklistValues(SystemProperties.Deal_Types_Not_Allowing_Price_Adjustment__c);
        //}

        //return dealTypes;
    }

/*=========================================================
    get RMS DealTypes
===========================================================*/
    public static Set<String> getRMSAgreementDirectDealTypes()
    {
        return cpqDeal_Type_c.getDirectDealTypes();

        //InitializeSystemProperties();

        //Set<String> dealTypes = new Set<String>();

        //if (SystemProperties != null && SystemProperties.RMS_Agreement_Direct_Deal_Types__c != null) {
        //    dealTypes = CPQ_Utilities.MultiPicklistValues(SystemProperties.RMS_Agreement_Direct_Deal_Types__c);
        //}

        //return dealTypes;
    }

/*=========================================================
    get businessday cut off hour
===========================================================*/
    public static Decimal getRequireOpportunityAmountThreshold()
    {
        return cpqConfigSetting_c.getRequireOpportunityAmountThreshold();

        //InitializeSystemProperties();

        //Decimal value = 1500.00;

        //if (SystemProperties != null && SystemProperties.Require_Opportunity_Amount_Threshold__c != null) {
        //    value = SystemProperties.Require_Opportunity_Amount_Threshold__c;
        //}

        //return value;
    }

/*=========================================================
    get DealTypes requiring no opportunity by amount
===========================================================*/
    public static Set<String> getDealTypesRequireOpportunityByAmount()
    {
        return cpqDeal_Type_c.getDealTypesRequireOpportunityByAmount();

        //InitializeSystemProperties();

        //Set<String> dealTypes = new Set<String>();

        //if (SystemProperties != null && SystemProperties.Deal_Types_Require_Opportunity_by_Amount__c != null) {
        //    dealTypes = CPQ_Utilities.MultiPicklistValues(SystemProperties.Deal_Types_Require_Opportunity_by_Amount__c);
        //}
        //return dealTypes;
    }

/*=========================================================
    get Proposal Valid For after Presented
===========================================================*/
    public static integer getProposalValidForafterPresented()
    {
        return cpqConfigSetting_c.getProposalValidForafterPresented();

        //InitializeSystemProperties();

        //Integer days = 45;

        //if (SystemProperties != null && SystemProperties.Proposal_Valid_For_after_Presented__c != null) {
        //    days = Integer.valueOf(SystemProperties.Proposal_Valid_For_after_Presented__c);
        //}

        //return days;
    }

/*=========================================================
    get is Subject Task in the list
===========================================================*/
    public static boolean getSetTaskOwnerToAgreementOwner(string subject)
    {
        return cpqConfigSetting_c.getSetTaskOwnerToAgreementOwner(subject);

        //InitializeSystemProperties();

        //if (SystemProperties != null && SystemProperties.Task_Subjects_to_trigger_setting_Owner__c != null && SystemProperties.Task_Subjects_to_trigger_setting_Owner__c.indexOf(subject) != -1)
        //    return true;
        //else
        //    return false;
    }

/*=========================================================
    get is Subject Task in the list
===========================================================*/
    public static boolean getAllowedToSeeCOOPPricingFile(string profile)
    {
        return cpqConfigSetting_c.getAllowedToSeeCOOPPricingFile(profile);

        //InitializeSystemProperties();

        //if (SystemProperties != null && SystemProperties.Profiles_allowed_to_see_COOP_Price_File__c != null && SystemProperties.Profiles_allowed_to_see_COOP_Price_File__c.indexOf(profile) != -1)
        //    return true;
        //else
        //    return false;
    }

/*=========================================================
    get is Subject Task in the list
===========================================================*/
    public static boolean getSendEmailToAgreementOwner(string subject)
    {
        return cpqConfigSetting_c.getSendEmailToAgreementOwner(subject);

        //InitializeSystemProperties();

        //if (SystemProperties != null && SystemProperties.Task_Subjects_to_trigger_email_to_Owner__c != null && SystemProperties.Task_Subjects_to_trigger_email_to_Owner__c.indexOf(subject) != -1)
        //    return true;
        //else
        //    return false;
    }

    /*=========================================================
    get soql inlist from string set
===========================================================
    public static String SetToInList(Set<String> input) {
        String s = '';
        for (String a : input) {
            if (String.isEmpty(s))
                s = '\'' + a + '\'';
            else
                s = s + ', \'' + a + '\'';
        }

        if (String.isEmpty(s))
            s = '\'^^THERE IS NOTHING IN THE INLIST^^\'';
        return s;
    }

/*=========================================================
    get soql inlist from sobject
===========================================================*/
    public static String listToIDInList(List<SObject> input)
    {
        return SOQL_select.getSoqlInListFromSObjectList(input);

        //String s = '';
        //for (SObject r : input) {
        //    if (String.isEmpty(s)) {
        //        s = '\'' + r.ID + '\'';
        //    } else {
        //        s = s + ', \'' + r.ID + '\'';
        //    }
        //}

        //if (String.isEmpty(s))
        //    s = '\'^^THERE IS NOTHING IN THE INLIST^^\'';
        //return s;
    }

/*=========================================================
    get set<> from Multi Picklist value
===========================================================*/
    public static Set<String> MultiPicklistValues(String value)
    {
        return DATATYPES.getMultiPicklistValueSet(value);

        //Set<String> valuesSet = new Set<String>();

        //String[] a = value.split(';');
        //for (String s : a) {
        //    String st = s.Trim();
        //    if (!String.isEmpty(st))
        //        valuesSet.add(st);
        //}

        //return valuesSet;
    }

/*=========================================================
    translate actual recordtypes (Locked, -AG) to the core deal type
===========================================================*/
    public static String getCoreDealTypeLabel(String RecordTypeName)
    {
        return cpqDeal_Type_c.getDealType(RecordTypeName);

        //if (DealTypeName.endsWith(' Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf(' Locked')).Trim();

        //if (DealTypeName.endsWith('-AG'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('-AG')).Trim();

        //return DealTypeName;
    }

    public static String getCoreDealTypeLabel(Apttus_Proposal__Proposal__c proposal)
    {
        return cpqDeal_Type_c.getDealType(proposal);

        //String DealTypeName = proposal.RecordType.Name == null ? '' : proposal.RecordType.Name;

        ////every deal type can have Amendment to Add Facilities. the original record type counts
        //if (DealTypeName.startsWith('Amendment to Add Facilities'))
        //    DealTypeName = proposal.Apttus_QPComply__MasterAgreementId__r.RecordType.Name == null ? 'Proposal' : proposal.Apttus_QPComply__MasterAgreementId__r.RecordType.Name;

        //if (DealTypeName.endsWith(' Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf(' Locked')).Trim();

        //if (DealTypeName.endsWith('-AG'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('-AG')).Trim();

        //return DealTypeName;
    }

    public static String getCoreDealTypeLabel(Apttus__APTS_Agreement__c agreement)
    {
        return cpqDeal_Type_c.getDealType(agreement);

        //String DealTypeName = agreement.RecordType.Name == null ? '' : agreement.RecordType.Name;

        ////every deal type can have Amendment to Add Facilities. the original record type counts
        //if (DealTypeName.startsWith('Amendment_to_Add_Facilities'))
        //    DealTypeName = agreement.Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.Name == null ? 'Proposal' : agreement.Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.Name;

        //if (DealTypeName.endsWith(' Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf(' Locked')).Trim();

        //if (DealTypeName.endsWith('-AG'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('-AG')).Trim();

        //return DealTypeName;
    }

    public static String getCoreDealTypeName(Apttus_Proposal__Proposal__c proposal)
    {
        return cpqDeal_Type_c.getDealType(proposal);

        //String DealTypeName = proposal.RecordType.DeveloperName == null ? 'Proposal' : proposal.RecordType.DeveloperName;

        ////every deal type can have Amendment to Add Facilities. the original record type counts
        //if (DealTypeName.startsWith('Amendment_to_Add_Facilities'))
        //    DealTypeName = proposal.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName == null ? 'Proposal' : proposal.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName;

        //if (DealTypeName.endsWith('_Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('_Locked'));

        //if (DealTypeName.endsWith('_AG'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('_AG'));

        //return DealTypeName;
    }

    public static String getCoreDealTypeName(Apttus__APTS_Agreement__c agreement)
    {
        return cpqDeal_Type_c.getDealType(agreement);

        //String DealTypeName = agreement.RecordType.DeveloperName == null ? 'Proposal' : agreement.RecordType.DeveloperName;

        ////every deal type can have Amendment to Add Facilities. the original record type counts
        //if (DealTypeName.startsWith('Amendment_to_Add_Facilities'))
        //    DealTypeName = agreement.Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName == null ? 'Proposal' : agreement.Apttus_QPComply__RelatedProposalId__r.Apttus_QPComply__MasterAgreementId__r.RecordType.DeveloperName;

        //if (DealTypeName.endsWith('_Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('_Locked'));

        //if (DealTypeName.endsWith('_AG'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('_AG'));

        //return DealTypeName;
    }

/*=========================================================
    get unlocked recordtype name
===========================================================*/
    public static String getRecordTypeDeveloperName(Apttus_Proposal__Proposal__c proposal)
    {
        return cpqDeal_RecordType_c.getUnlockedRecordTypeName(proposal);

        //String DealTypeName = proposal.RecordType.DeveloperName == null ? '' : proposal.RecordType.DeveloperName;
        //if (DealTypeName.endsWith('_Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('_Locked'));
        //return DealTypeName;
    }

    public static String getRecordTypeDeveloperName(Apttus__APTS_Agreement__c agreement)
    {
        return cpqDeal_RecordType_c.getUnlockedRecordTypeName(agreement);

        //String DealTypeName = agreement.RecordType.DeveloperName == null ? '' : agreement.RecordType.DeveloperName;
        //if (DealTypeName.endsWith('_Locked'))
        //    DealTypeName = DealTypeName.substring(0, DealTypeName.lastIndexOf('_Locked'));
        //return DealTypeName;
    }

/*=========================================================
    check if the proposal will be sent to partner
===========================================================*/
    public static Boolean isProposalDestinedToPartner(Apttus_Proposal__Proposal__c proposal)
    {
        return cpqDeal_u.isRecordDestinedToPartner(proposal);
    }

/*=========================================================
    check if the agreement will be sent to partner
===========================================================*/
    public static Boolean isAgreementDestinedToPartner(Apttus__APTS_Agreement__c agreement)
    {
        return cpqDeal_u.isRecordDestinedToPartner(agreement);
    }

/*=========================================================
    deprecated
===========================================================*/

    //public static Boolean isCOOPAgreementDestinedToPartner(Apttus__APTS_Agreement__c agreement) {
    //    String DealTypeName = CPQ_Utilities.getCoreDealTypeName(agreement);
    //    System.Debug('*** DealTypeName ' + DealTypeName);
    //    if (DealTypeName != 'COOP_Plus_Program' && DealTypeName != 'Respiratory_Solutions_COOP')
    //        return true;

    //    CPQ_Variable__c cs = CPQ_Variable__c.getValues('Send COOP Deals to Partner');
    //    if (cs != null && cs.Value__c != 'On')
    //        return false;

    //    if (agreement.Deal_Type__c == 'Amendment') {
    //        cs = CPQ_Variable__c.getValues('Send COOP ADJ Deals to Partner');
    //        if (cs != null && cs.Value__c != 'On')
    //            return false;
    //    }

    //    return true;
    //}

/*=========================================================
    for given input account Ids, get accountIDs that have no valid ERPs or have only Tracing ERPs
===========================================================*/
    public static void ERPFilter(List<Account> inputs, Set<ID> hasNoERPs, Set<ID> hasOnlyTracings)
    {
        ERPAccount_c.filterAccountsByERP(inputs,hasNoERPs,hasOnlyTracings);

        //Set<ID> SXs = new Set<ID>();
        //Set<ID> hasERPs = new Set<ID>();
        //for (ERP_Account__c erp : [Select ID, Name, ERP_Account_Type__c, Parent_Sell_To_Account__c From ERP_Account__c Where Parent_Sell_To_Account__c in: inputs and ERP_Account_Type__c in ('S', 'X', 'TS', 'TX') and ERP_Source__c = 'E1' and isActive__c = true Order by Parent_Sell_To_Account__c]) {
        //    hasERPs.add(erp.Parent_Sell_To_Account__c);

        //    if (erp.ERP_Account_Type__c == 'S' || erp.ERP_Account_Type__c == 'X')
        //        SXs.add(erp.Parent_Sell_To_Account__c);
        //}

        //for (Account a : inputs) {
        //    if (!hasERPs.contains(a.Id))
        //        hasNoERPs.add(a.Id);
        //    else if (!SXs.contains(a.Id))
        //        hasOnlyTracings.add(a.Id);
        //}
    }

    public static CPQ_Error_Log__c CreateErrorLogEntry(
        String operation,
        String logType,
        String errorMessage,
        String additional,
        String request)
    {
        return cpqErrorLog_c.createErrorLogEntry(operation,logType,errorMessage,additional,request);

        //CPQ_Error_Log__c error = new CPQ_Error_Log__c(Operation__c = operation, Log_Type__c = logType, Error_Message__c = errorMessage, Additional_Info__c = additional, Request_Body__c = request);
        //Database.DMLOptions dml = new Database.DMLOptions();
        //dml.allowFieldTruncation = true;
        //error.setOptions(dml);

        //return error;
    }

/// <summary>
/// <para>Determines the end point of a time interval specified
/// by its start and an interval in business days.</para>
/// <para>Weekends are not considered business days. This
/// function also implements the after-3-o'clock rule,
/// which says that requests received after 3:00pm are
/// considered to have arrived on the next business day.</para>
/// </summary>
//the input StartDate in gmt time, usually System.now()
    public static Date GetEndDate(DateTime gmtStartDate, Integer businessDays)
    {
        return cpqConfigSetting_c.getBusinessEndDate(gmtStartDate,businessDays);

        //DateTime localStartDate = DateTime.newInstanceGMT(gmtStartDate.date(), gmtStartDate.time());
        //Date localEndDate = Date.valueOf(localStartDate);//exact copy of start date

        //// Work requested after 3:00pm can't start until the next business day
        //DateTime cutoff = getBusinessDayCutOffHour();
        //System.Debug('*** cutoff ' + cutoff + ', localStartDate ' + localStartDate);

        //if (localStartDate >= cutoff) {
        //    do {
        //        localEndDate = localEndDate.addDays(1);
        //    } while (!businessDayP(localEndDate));
        //}

        //// Nothing gets done on the weekends
        //while (!businessDayP(localEndDate)) {
        //    localEndDate = localEndDate.addDays(1);
        //}

        //for (Integer dayCount = 0; dayCount < businessDays; dayCount++) {
        //    do {
        //        localEndDate = localEndDate.addDays(1);
        //    } while (!businessDayP(localEndDate));
        //}

        //return localEndDate;
    }

    /// <summary>
    /// Returns true if the given dt is a workday, false otherwise.
    /// </summary>
    // 2017.01.24 - Only used within cpq_utilities, and methods are already commented out
    // private static Boolean businessDayP(DateTime localDt)
    // {
    //  return cpq_u.isLocalBusinessDay(localDt);
    //
    //     //DateTime gmtDt = DateTime.newInstance(localDt.dateGMT(), localDt.timeGMT());
    //     ////TODO: holidays
    //     //String dayOfWeek = gmtDt.format('EEE');//format converts GMT to local
    //     //if (dayOfWeek == 'Sat' || dayOfWeek == 'Sun') {
    //     //    // weekend
    //     //    return false;
    //     //}
    //     //return true;
    // }

    //false is 0, and true is 1
    public static Integer boolToInt(Boolean v)
    {
        return DATATYPES.boolToInt(v);

        //return v == true ? 1 : 0;
    }

    public static String ValidCRN(String crn) {
        try {
            String s = crn.substring(3);
            return String.valueOF(Integer.valueOf(s.trim()));
        } catch (Exception e) {
            return null;
        }
    }

    //
    // North Star code
    //
    public static List<string> getCOTFilters(Apttus_Proposal__Proposal__c proposal)
    {
        return cpqConfigSetting_c.getCOTFilters(proposal);

        //InitializeSystemProperties();

        //if(SystemProperties.North_Star_Profiles__c != null) {

        //    List<string> profileNames = SystemProperties.North_Star_Profiles__c.split('\r\n');
        //    List<User> user = [SELECT Id FROM User WHERE Profile.Name IN :profileNames AND Id = :p.OwnerId];

        //    if(user.size() == 0) return new List<string>();

        //}

        //List<string> proposalRecordTypesStr = new List<string>();
        //if(SystemProperties.North_Star_Proposal_Record_Types__c != null)
        //    proposalRecordTypesStr.addAll(SystemProperties.North_Star_Proposal_Record_Types__c.split('\r\n'));

        //List<string> rebateRecordTypesStr = new List<string>();
        //if(SystemProperties.North_Star_Rebate_Record_Types__c != null)
        //    rebateRecordTypesStr.addAll(SystemProperties.North_Star_Rebate_Record_Types__c.split('\r\n'));

        //Map<Id, RecordType> recordTypes = new Map<Id, RecordType>([SELECT Id
        //                                                           FROM RecordType
        //                                                           WHERE Name IN :proposalRecordTypesStr OR
        //                                                                 Name IN :rebateRecordTypesStr]);

        //List<Rebate__c> rebates = [SELECT Id
        //                           FROM Rebate__c
        //                           WHERE RecordTypeId IN :recordTypes.keySet() AND
        //                                 Related_Proposal__r.RecordTypeId IN :recordTypes.keySet()];

        //if(rebates.size() == 0) return new List<string>();

        //List<string> cotStr = new List<string>();
        //if(SystemProperties.North_Star_COTs__c != null)
        //    cotStr.addAll(SystemProperties.North_Star_COTs__c.split('\r\n'));

        //return cotStr;
    }

    //
    // CPQApprover class methods (deprecated)
    //

    //// Returns true if the Agreement Record Type name is enabled for the code
    //public static boolean validAgreementRecordType(string name) {
    //    InitializeSystemProperties();

    //    if (SystemProperties.Approver_Agreement_Record_Types__c == null) return true; // If there are no values, then we assume all are valid
    //    return validRecordType(name, SystemProperties.Approver_Agreement_Record_Types__c.split('\r\n'));
    //}

    //// Returns true if the Proposal Record Type name is enabled for the code
    //public static boolean validProposalRecordType(string name) {
    //    InitializeSystemProperties();

    //    if (SystemProperties.Approver_Proposal_Record_Types__c == null) return true; // If there are no values, then we assume all are valid
    //    return validRecordType(name, SystemProperties.Approver_Proposal_Record_Types__c.split('\r\n'));
    //}

    // Common method to determine if Record Type name is enabled
    // 02/18/2016 PB - Removed check for 'Locked' record types - was causing fields to be cleared when record
    // type changed to 'Locked'
    //@testVisible
    //private static boolean validRecordType(string name, List<string> recordTypeNames) {
    //    if (name == null) return false;
    //    if (recordTypeNames == null) return false;

    //    if (recordTypeNames.size() == 0) return true; // Means the field is empty, so all are included

    //    Set<string> validNames = new Set<string>( recordTypeNames );

    //    boolean valid = false;
    //    if (validNames.contains('+All')) {
    //        valid = true;
    //        if (validNames.contains('-' + name)) valid = false;

    //        return valid;
    //    }
    //    else if (validNames.contains('-All')) {
    //        valid = false;
    //        if (validNames.contains('+' + name)) valid = true;

    //        return valid;
    //    }
    //    else {
    //        if (validNames.contains('-' + name))
    //            valid = false;
    //        else
    //            valid = true;

    //        return valid;
    //    }
    //}


    //
    // Sales Org moved to OrganizationNames__c
    //
    //public enum SalesOrg {
    //    RMS,
    //    Surgical,
    //    Unknown
    //}

    //public static SalesOrg getSalesOrg(string name) {
    //    InitializeSystemProperties();

    //    if (SystemProperties.Sales_Org_RMS__c != null) {
    //        Set<string> orgs = new Set<string>( SystemProperties.Sales_Org_RMS__c.split('\r\n') );
    //        if (orgs.size() != 0 && orgs.contains(name)) return SalesOrg.RMS;
    //    }
    //    if (SystemProperties.Sales_Org_Surgical__c != null) {
    //        Set<string> orgs = new Set<string>( SystemProperties.Sales_Org_Surgical__c.split('\r\n') );
    //        if (orgs.size() != 0 && orgs.contains(name)) return SalesOrg.Surgical;
    //    }
    //    return SalesOrg.Unknown;
    //}
}