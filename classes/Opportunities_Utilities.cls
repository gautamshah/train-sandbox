public with sharing class Opportunities_Utilities
{
    public static Set<Id> deletedOpps = new Set<Id>();
    List<Opportunity> opportunitiesToAddNavTo = null;
    public static List<String> availableRecordTypes = new List<String>{'US_Sales_Nav_Opportunity','US_SUS_Opportunity','US_AST_Opportunity'};
    
    private static  Map<Id, RecordType> recordTypeMap = null;
    public static Map<Id, RecordType> GetRecordTypeMap()
    {
        if(recordTypeMap == null)
        {
            recordTypeMap = new Map<Id, RecordType>();
            for(String s: availableRecordTypes)
            {
                RecordType rt = RecordType_Utilities.GetRecordTypesMap('Opportunity').Get(s);   
                recordTypeMap.put(rt.Id, rt);
            }
        }
        return recordTypeMap;
    }
    
    public void CheckOppAmount(List<Opportunity> opps, Map<ID, Opportunity> oldOppsMap, Boolean bForce)
    {
        Set<Id> personalInitiativeIds = new Set<Id>();
        
        for(Opportunity o: opps)
        {
            CheckOppStatus(o);
            if(bForce)
            {
                if(o.Personal_Initiative__c != null)
                    personalInitiativeIds.Add(o.Personal_Initiative__c);
            }
            else
            {
                Opportunity oldOpp = oldOppsMap.get(o.Id);
                if(o.Amount != oldOpp.Amount)
                    personalInitiativeIds.Add(o.Personal_Initiative__c);
            }
        }
        System.debug('personalInitiativeIds: ' + personalInitiativeIds);
        PersonalInitiative_Utilities.UpdatePersonalInitiatives(personalInitiativeIds);
    }
    
    public void CheckOppStatus(Opportunity o)
    {
        if(GetRecordTypeMap().containsKey(o.RecordTypeId) && o.Sales_Navigator_Flag__c)
        {
            o.StageName = o.Earliest_Stage_Complete__c;
        }
    }
    
    public void CheckStatuses(List<Opportunity> opps)
    {
        for(Opportunity o: opps)
        {
            CheckOppStatus(o);
        }
    }
    
    public List<Opportunity> RollupCompletedStageSteps(List<Opportunity> opps)
    {
        Map<Id, Opportunity> oppsMap = new Map<Id, Opportunity>(opps);
        Map<Id, Decimal> oppToTotalMap = new Map<Id, Decimal>();
        for(Sales_Navigator_Stage__c s: [SELECT Id, No_Critical_Steps_Complete__c, Opportunity__r.Id FROM Sales_Navigator_Stage__c WHERE Opportunity__r.Id IN : oppsMap.keySet() AND Id NOT IN : SalesNavigatorStage_Utilities.deletedStages])
        {
            if(!oppToTotalMap.containsKey(s.Opportunity__r.Id))
                oppToTotalMap.put(s.Opportunity__r.Id, 0);
            Decimal amt = oppToTotalMap.get(s.Opportunity__r.Id);
            amt += s.No_Critical_Steps_Complete__c;
            oppToTotalMap.put(s.Opportunity__r.Id, amt);
        }
        List<Opportunity> retOpps = new List<Opportunity>();
        for(Id eyeDee : oppToTotalMap.keySet())
        {
            oppsMap.get(eyeDee).Total_Completed_Steps__c = oppToTotalMap.get(eyeDee);
            retOpps.add(oppsMap.get(eyeDee));
        }
        return retOpps;
    }
    
    public void RollupStages(Set<Id> oppIds)
    {
        System.debug('oppIds: ' + oppIds);
        List<Opportunity> oppsToUpdate = RollupCompletedStageSteps([SELECT Id FROM Opportunity WHERE Id IN :oppIds]);
        System.debug('oppsToUpdate: ' + oppsToUpdate.size());
        try
        {
            if(oppsToUpdate.size() > 0)
                update oppsToUpdate;
        }
        catch(DmlException ex)
        {
            System.debug('Exception caught: ' + ex.getMessage());
        }
    }
    
    public void OnBeforeInsert(List<Opportunity> opps)
    {
        CheckStatuses(opps);
    }
    
    public void OnBeforeUpdate(List<Opportunity> opps, Map<ID, Opportunity> oldOppsMap)
    {
        CheckOppAmount(opps, oldOppsMap, false);
    }
    
    public void OnAfterInsert(List<Opportunity> opps)
    {
        opportunitiesToAddNavTo= [select Id, RecordTypeId, Sales_Navigator_Flag__c, RecordType.DeveloperName, Type, Capital_Disposable__c, Personal_Initiative__c,Amount,of_products__c,Opportunity_Type__c FROM Opportunity where Id IN: opps];
        SalesNavigatorStage_Utilities.InsertSalesNavStages(opportunitiesToAddNavTo);
        CheckOppAmount(opportunitiesToAddNavTo, null, true);
    }
    
    public void OnAfterUpdate(List<Opportunity> opps)
    {
        opportunitiesToAddNavTo = new List<Opportunity>();
        for(Opportunity o: [select Id, RecordTypeId, RecordType.DeveloperName, Type, Capital_Disposable__c, ( Select Id from Sales_Navigator_Steps__r),Amount,of_products__c,Opportunity_Type__c FROM Opportunity where Id IN: opps])
            if(o.Sales_Navigator_Steps__r == null || o.Sales_Navigator_Steps__r.size() == 0)
                opportunitiesToAddNavTo.add(o);
        SalesNavigatorStage_Utilities.InsertSalesNavStages(opportunitiesToAddNavTo);
    }
    
    public void OnBeforeDelete(List<Opportunity> opps, Map<ID, Opportunity> oppsMap)
    {
        deletedOpps = oppsMap.keySet();
        CheckOppAmount(opps, oppsMap, true);
    }
}