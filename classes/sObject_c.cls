/* 
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry
    // YYYYMMDD A   PAB         CPR-000 Updated comments section
    Only put “inline” comments related to the specific logic found at this location

DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com

MODIFICATION HISTORY
====================
Date        Id  Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161118    A   PAB         AV-253  Created for Refactoring
20170127        IL          AV-280  Added Id check to correctly separate from where clauses.
                                    Commented out buildSOQL method (not referenced + logic is already in SOQL.cls).
                                    Added try/catch block around fetch query method.


*/
public abstract class sObject_c
{
    ////
    //// Instance Properties & Methods
    ////
    public UUID UUID
    {
        get
        {
            if (UUID == null)
                UUID = new UUID(1);

            return UUID;
        }
        
        private set;
    }

    protected sObject sobj;
    public sObject get() { return sobj; }
    
    public Id Id
    {
        get { return sobj.Id; }
        set { sobj.Id = value; }
    }
    
    public string Name
    {
        get { return (string)sobj.get('Name'); }
        set { sobj.put('Name', value); }
    }
    
    //
    // sObject Properties
    //
    //
    // Constructors
    //

    // Adding an sObject is required, so hide
    // the default constructor
    //
    // It's needed for Test classes, which require
    // the default constructor
    @testVisible
    protected sObject_c() { throw new sObjectRequiredException(sObjectRequiredMessage); }

    // These constructors create new instances of the sObject
    protected sObject_c(sObjectType sObjType)
    {
        this(sObjType, null, null); 
    }
    
    protected sObject_c(sObjectType sObjType, Id recordTypeId)
    {
        this(sObjType, recordTypeId, false);
    }
    
    protected sObject_c(sObjectType sObjType, boolean loadDefaults)
    {
        this(sObjType, null, loadDefaults);
    }
    
    protected sObject_c(sObjectType sObjType, Map<sObjectField, object> fields) 
    {
        this(sObjType, new List<Map<sObjectField, object>>{ fields });      
    }
    
    protected sObject_c(sObjectType sObjType, List<Map<sObjectField, object>> fieldList)
    {
        this(sObjType, null, null);
        
        for(Map<sObjectField, object> fields : fieldList)
            for(sObjectField field : fields.keySet())
                put(field, fields.get(field));
    }

    protected sObject_c(type classType, Id recordTypeId, boolean loadDefaults)
    {
        this(((sObject)classType.newInstance()).getSObjectType(), recordTypeId, loadDefaults);
    }
    
    // This constructor does the actual creation of the sObject
    protected sObject_c(sObjectType sObjType, Id recordTypeId, boolean loadDefaults)
    {
        this(sObjType.newSObject(recordTypeId, (loadDefaults == null ? false : loadDefaults)), null, null);
    }
    
    // These constructors wrap existing sObjects
    // This one would normally be called if you are handling just 1 sObject
    protected sObject_c(sObject sobj)
    {
        this(sobj, null, null);
    }

    protected sObject_c(sObjectType sObjType, string name)
    {
        this(fetch(sObjType, name));
    }

    protected sObject_c(Id id)
    {
        this(fetch(id.getSObjectType(), id));
    }

    // This is the main constructor - it set's up the required
    // values for the methods & properties to function correctly
    private sObject_c(
        sObject sobj,
        sObjectType sobjType,
        Schema.DescribeSObjectResult describe)
    {
        if (sobj == null)
            throw new sObjectRequiredException(sObjectRequiredMessage);
        else
        {
            this.sobj = sobj;

            this.sobjType =
                (sobjType == null) ?
                    this.sobj.getSObjectType() :
                    sobjType;

            this.describe =
                (describe == null) ?
                    new sObjectDescribe(this.sobjType) :
                    new sObjectDescribe(describe);
        }
    }

    // Not sure if this one would ever be used, so private for now
    // private sObject_c(
    //  sObject sobj,
    //  sObjectType sobjType)
    // {
    //  this(sobj, sobjType, null);
    // }
    
    public virtual List<string> save()
    {
        List<string> errors = new List<string>();

        Database.UpsertResult result = Database.upsert(this.sobj, false);
        if (!result.isSuccess())
                errors.add(string.valueOf(this) + CONSTANTS.CRLF +
                'save()' + CONSTANTS.CRLF +
                ' err: ' + result.getErrors());

        return errors;
    }

    private Set<sObjectField> fieldsSet = new Set<sObjectField>();
    
    public object get(sObjectField field) { return sobj.get(field); }
    public void put(sObjectField field, object value) { sobj.put(field, value); fieldsSet.add(field); }
    public void putAll(Map<sObjectField, object> values)
    {
        for(sObjectField field : values.keySet())
            put(field, values.get(field));
    }
    
    public void putIfNull(sObjectField field, object value)
    {
        putIfNull(get(), field, get(field));
    }
    
    public void putAllIfNull(Map<sObjectField, object> values)
    {
        putAllIfNull(get(), values);
    }

    protected sObjectType sobjType;
    protected transient sObjectDescribe describe;

    private class sObjectRequiredException extends Exception { }
    @testVisible
    private static string sObjectRequiredMessage = 'A sObject is required to use this class';
    private class sObjectsMustBeSameTypeException extends Exception { }
    @testVisible
    private static string sObjectsMustBeSameTypeMessage = 'The sObjects must be the same type to used this method';

    //
    // Describe Methods
    //
    public class sObjectDescribe
    {
        private Schema.DescribeSObjectResult describe;

        public sObjectDescribe(sObjectType sObjType)
        {
            this.describe = sObject_c.getSObjectDescribe(sObjType);
        }

        public sObjectDescribe(Schema.DescribeSObjectResult describe)
        {
            this.describe = describe;
        }

        public string getName { get { return describe.getName(); } }
        public string getKeyPrefix { get { return describe.getKeyPrefix(); } }
    }

    ////////
    //////// Class Propeties & Methods
    ////////
    
    ////
    //// Describe using sObjectType
    ////
    private static Map<sObjectType, DescribeSObjectResult> sObjectTypeDescribeResults =
        new Map<sObjectType, DescribeSObjectResult>();
        
    private static Map<sObjectType, Map<string, sObjectField>> sObjectType2SObjectField =
        new Map<sObjectType, Map<string, sObjectField>>();
        
    private static Map<sObjectField, DescribeFieldResult> sObjectFieldDescribeResults =
        new Map<sObjectField, DescribeFieldResult>();

    ////
    //// getSObjectDescribe
    ////
    public static DescribeSObjectResult getSObjectDescribe(sObjectType sObjType)
    {
        if (!sObjectTypeDescribeResults.containsKey(sObjType))
            sObjectTypeDescribeResults.put(sObjType, sObjType.getDescribe());
            
        return sObjectTypeDescribeResults.get(sObjType);
    }
    
    ////
    //// getSObjectFields
    ////
    public static Map<string, sObjectField> getSObjectFields(sObjectType sObjType)
    {
        if (!sObjectType2SObjectField.containsKey(sObjType))
            sObjectType2SObjectField.put(sObjType, getSObjectDescribe(sObjType).fields.getMap());
        
        return sObjectType2SObjectField.get(sObjType);
    }
    
    ////
    //// getSObjectFieldDescribe
    ////
    public static DescribeFieldResult getSObjectFieldDescribe(sObjectField sObjField)
    {
        if (!sObjectFieldDescribeResults.containsKey(sObjField))
            sObjectFieldDescribeResults.put(sObjField, sObjField.getDescribe());
            
        return sObjectFieldDescribeResults.get(sObjField);
    }

    ////
    //// cast
    ////
    private static sObjectType cast(Type classType)
    {
        return ((sObject)classType.newInstance()).getSObjectType();
    }
    
    ////
    //// Describe using Type
    ////
    ////
    //// getSObjectDescribe
    ////
    public static DescribeSObjectResult getSObjectDescribe(Type classType)
    {
        return getSObjectDescribe(cast(classType));
    }
    
    ////
    //// getSObjectFields
    ////
    public static Map<string, sObjectField> getSObjectFields(Type classType)
    {
        return getSObjectFields(cast(classType));
    }
    
    ////
    //// IfNull
    ////
    //// There are a lot of instances where a field is only set if is is null
    public static void putIfNull(sObject sobj, sObjectField field, object value)
    {
        if (sobj.get(field) == null)
            sobj.put(field, value);
    }
    
    public static void putAllIfNull(sObject sobj, Map<sObjectField, object> values)
    {
        for(sObjectField field : values.keySet())
            putIfNull(sobj, field, values.get(field));
    }

    private static Map<sObjectField, object> combine(List<Map<sObjectField, object>> fieldSets)
    {
        Map<sObjectField, object> combined = new Map<sObjectField, object>();
        for(Map<sObjectField, object> fields : fieldSets)
            combined.putAll(fields);
        return combined;
    }

    public static Set<String> getCustomFieldNames (Schema.SObjectType token, Boolean modifiableOnly) {

        Set<String> fieldNames = new Set<String>();

        Map<String, Schema.SObjectField> M = token.getDescribe().fields.getMap();
        for (String s : M.keySet()) {
            Schema.DescribeFieldResult r = M.get(s).getDescribe();
            if (r.isCustom() && (!modifiableOnly || (modifiableOnly && !r.isAutoNumber() && !r.isCalculated()))) {
                fieldNames.add(r.getName());
            }
        }

        return fieldNames;
    }

    @testVisible
    private static sObject fetch(sObjectType sObjType, Id id)
    {
        string whereClause = ' WHERE Id = :Id ';

        string query =
            SOQL_select.buildQuery(
                sObjType,
                whereClause,
                null,
                1);

        return Database.query(query);
    }

    @testVisible
    private static sObject fetch(sObjectType sObjType, string name)
    {
        string whereClause = ' WHERE Name = :name ';

        string query =
            SOQL_select.buildQuery(
                sObjType,
                whereClause,
                null,
                1);

        try
        {
            return Database.query(query);
        }
        catch(Exception e)
        {
            return null;
        }
    }

    //
    // Class Methods
    //
    public static boolean fieldChanged(
        sObject newVersion,
        Map<Id, sObject> oldVersions,
        Schema.sObjectField field)
    {
        return fieldChangedTo(newVersion, oldVersion(newVersion, oldVersions), field, newVersion.get(field));
    }


    public static boolean fieldChanged(
        sObject newVersion,
        sObject oldVersion,
        Schema.sObjectField field)
    {
        return fieldChangedTo(newVersion, oldVersion, field, newVersion.get(field));
    }

    public static boolean fieldChangedTo(
        sObject newVersion,
        Map<Id, sObject> oldVersions,
        Schema.sObjectField field,
        object value)
    {
      return fieldChangedTo(newVersion, oldVersion(newVersion, oldVersions), field, value);
    }

    public static boolean fieldChangedTo(
        sObject newVersion,
        sObject oldVersion,
        Schema.sObjectField field,
        object value)
    {
        object newValue = newVersion.get(field);
        object oldValue = ((oldVersion == null) ? null : oldVersion.get(field));

        return sObjectField_c.valueChangedTo(newValue, oldValue, value);
    }

    // This elminates all the code that checks to see if oldMap is null
    // before it tries to access the the prior record
    //
    public static sObject oldVersion(sObject newVersion, Map<Id, sObject> oldVersions)
    {
        return (oldVersions == null) ? null : oldVersions.get(newVersion.Id);
    }

    public static Id dummyId(type type, integer value)
    {
        return dummyId(sObject_u.getSObjectType(type), value);
    }

    public static Id dummyId(sObjectType sObjType, integer value)
    {
        string prefix = sObjType.getDescribe().getKeyPrefix();
        string suffix = string.valueOf(value).leftPad(15, '0');
        Id dummy = Id.valueOf(prefix + suffix);
        return dummy;
    }

    //
    // COPY
    //

    // Copy using list of strong typed field names
    // This can only be used if the sObjects are identical types
    public static void copy(sObject source,
                            sObject target,
                            List<Schema.SObjectField> fields)
    {
        copy(source, target, fields, false, false);
    }

    // Copy using list of strong typed field names
    // This can only be used if the sObjects are identical types
    public static void copy(sObject source,
                            sObject target,
                            List<Schema.SObjectField> fields,
                            boolean onlyIfSourceNotNull,
                            boolean onlyIfTargetIsNull)
    {
        if (source.getSObjectType() != target.getSObjectType())
            throw new sObjectsMustBeSameTypeException(sObjectsMustBeSameTypeMessage);
        else
            copy(source, target, makeMap(fields), onlyIfSourceNotNull, onlyIfTargetIsNull);
    }

    // Copy using list of strong typed field names
    // Source and Target can be different sObjects
    // Provide the field to field mapping
    public static void copy(sObject source,
                            sObject target,
                            Map<Schema.SObjectField, Schema.SObjectField> fields)
    {
        copy(source, target, fields, false, false);
    }

    // Copy using list of strong typed field names
    // Source and Target can be different sObjects
    // Provide the field to field mapping
    public static void copy(sObject source,
                            sObject target,
                            Map<Schema.SObjectField, Schema.SObjectField> fields,
                            boolean onlyIfSourceNotNull,
                            boolean onlyIfTargetIsNull)
    {
        //system.debug('path: ' + debug_u.path());
        //if (source == null || target == null) throw new NullPointerException();
        for(Schema.SObjectField f : fields.keySet())
            if (!onlyIfSourceNotNull || (onlyIfSourceNotNull && source.get(f) != null))
                if (!onlyIfTargetIsNull || (onlyIfTargetIsNull || fields.get(f) == null))
                    target.put(fields.get(f), source.get(f));
    }

    //
    // EQUAL
    //

    // Are the list of strong typed field names the same for each sObject?
    // This can only be used if the sObjects are identical types
    public static boolean equal(sObject left,
                                sObject right,
                                List<Schema.SObjectField> fields)
    {
        if (left.getSObjectType() != right.getSObjectType())
            throw new sObjectsMustBeSameTypeException(sObjectsMustBeSameTypeMessage);
        else
            return equal(left, right, makeMap(fields));
    }

    // Are the list of strong typed field name the same for each sObject
    // The sObjects can be of different sObject types
    // Provide the field to field mapping you want compared
    public static boolean equal(sObject left,
                                sObject right,
                                Map<Schema.SObjectField, Schema.SObjectField> fields)
    {
        for(Schema.SObjectField f : fields.keySet())
            if (left.get(f) != right.get(fields.get(f))) return false;

        return true;
    }

    @testVisible
    // Utility to convert a List of stongly type field names into a Map
    // This allows us to put the logic in the methods that take
    // Maps and call them from the methods that allow a List
    private static Map<Schema.SObjectField, Schema.SObjectField> makeMap(List<Schema.SObjectField> fields)
    {
        Map<Schema.SObjectField, Schema.SObjectField> newMap =
            new Map<Schema.SObjectField, Schema.SObjectField>();

        for(Schema.SObjectField f : fields)
            newMap.put(f, f);

        return newMap;
    }
}