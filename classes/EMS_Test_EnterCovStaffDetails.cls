@isTest
public class EMS_Test_EnterCovStaffDetails {
    
    static testmethod void m1(){
        
        Account acc =new Account();
        acc.name='test acc';
        acc.BillingCountry='Country';
        insert acc;
        EMSEndPointURL__c ed = new EMSEndPointURL__c(name='EMS_CreateCPAPDF',URL__c='https://cs11.salesforce.com');
        insert ed;
        
        Approver_matrix__c app = new Approver_matrix__c();
        app.Name='SG';
        app.Marketing_Country_Controller_1__c = UserInfo.getUserId(); 
        app.Marketing_Country_Controller_2__c = UserInfo.getUserId();
        app.CCP_Coordinator_1__c = UserInfo.getUserId();
        app.CCP_Coordinator_2__c = UserInfo.getUserId();             
        app.GCC_Finance_1__c = UserInfo.getUserId();
        app.GCC_Finance_2__c = UserInfo.getUserId(); 
        app.GCC_Legal_1__c = UserInfo.getUserId();
        app.GCC_Legal_2__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_1__c = UserInfo.getUserId();
        app.GCC_Medical_Affairs_2__c = UserInfo.getUserId();
        app.GCC_PACE_1__c = UserInfo.getUserId(); 
        app.GCC_PACE_2__c = UserInfo.getUserId();
        app.GCC_R_D_1__c = UserInfo.getUserId();
        app.GCC_R_D_2__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_1__c = UserInfo.getUserId();
        app.In_Country_Pace_Manager_2__c = UserInfo.getUserId();
        app.Regional_Business_Head_1__c = UserInfo.getUserId();
        app.Regional_Business_Head_2__c = UserInfo.getUserId();
        app.Regional_PACE_Manager_1__c =UserInfo.getUserId(); 
        app.Regional_PACE_Manager_2__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_1__c = UserInfo.getUserId();
        app.Sales_Marketing_Country_Controller_2__c = UserInfo.getUserId(); 
        app.ARO_CA_Director_1__c=UserInfo.getUserId();
        app.ARO_CA_Director_2__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_1__c= UserInfo.getUserId();
        app.ARO_Comms_and_PR_2__c= UserInfo.getUserId();
        insert app;
        
        Contact con = new Contact();
        con.FirstName='First Name';
        con.LastName='Test Contact';
        con.accountid= acc.id;
        con.Country__c='Country';
        insert con;
        
        Contact con1 = new Contact();
        con1.FirstName='FirstName';
        con1.LastName='LastName';
        con1.Country__c='Country';
        con1.accountid= acc.id;
        insert con1;
        
        Employee__c emp = new Employee__c();
        emp.Name = 'Test';
        emp.User_Details__c= UserInfo.getUserId();
        insert emp;
        
        Profile p = [select id from profile where name='System Administrator'];   
        
        User u = new User(profileId = p.id, username = 'c@mixmaster.com', email = 'c@k.com', 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', country='SG',IsEMSEventOwner__c=true,
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname');
        
        System.runAs(u){
        EMSMarketingGBU__c mycs = new EMSMarketingGBU__c(Name= 'SG;EBD');
            mycs.ARO_Marketing_Head_1__c= u.id;
            mycs.ARO_Marketing_Head_2__c=u.id;
            insert mycs;
        EMS_EVent__c EMSRec = new EMS_Event__c();
        EMSrec.Upload_External_Unique_Id__c= '1248';         
        EMSRec.GBU_Primary__c='test';
        EMSRec.Division_Primary__c='test';
        EMSRec.Start_Date__c = System.Today().addDays(22);
        EMSRec.End_Date__c = System.Today().addDays(45);
        EMSRec.Plan_Budget_Amt__c=50000;
        EMSRec.CPA_Required__c='Yes';
        EMSRec.Name= 'test event';
        EMSrec.Event_Native_Name__c='test event name';
        EMSRec.Location_Country__c='China';  
        EMSRec.Location_State__c='Shanghai';
        EMSRec.Expense_Type__c='Sales';
        EMSRec.Event_Type__c='CME';
        EMSRec.Training_conducted_in_Hosp_Training_Cent__c='Yes';
        EMSRec.HTC_Name__c='Test';
        EMSRec.Primary_COT__c='AB';
        EMSRec.Reoccurring_or_New__c='New';
        EMSRec.Event_Host__c='Covidien';
        EMSRec.Location_City__c='Test City';
        EMSRec.Venue__c= 'Test Venue'; 
        EMSRec.First_Payment_Date__c=System.Today();
        EMSRec.Max_Attendees__c=400;
        EMSRec.Any_recipient_s_who_are_KOL_s_HCP_s__c='YES';
        EMSRec.Any_HCP_Invitees_from_Outside_Asia__c='YES';
        EMSRec.Benefit_Summary__c='Test Summary';
        EMSRec.CCI__c='YES';
        EMSRec.Event_Owner_User__c = u.Id;
        EMSrec.Event_Status__c='Planned';
        EMSrec.GBU_Primary__c='EBD';
        insert EMSrec;
        
        /*
        EMS_Event__c emsRec = [Select Id,Name,Budget_External_Recipient_URL__c,Start_Date__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,GBU_Primary__c,Budget_Internal_Recipient_URL__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Division_Primary__c,Design_Fee_Remarks__c,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,Lodging_Description_Remarks__c ,Total_LodA__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,Event_Production_Fee__c,Total_Air_Travel__c,ISR_CSR__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,Total_Local_GTA__c,Air_Travel_Description_Remarks__c ,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Owner_Country__c,Location_State__c,Venue__c,Publication_grant__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Event_Owner_User__c,Event_Type__c,Total_Others_Amount__c,Requestor_Name_1__c,Division_Multiple__c,
                                  Total_No_of_Recipients__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Meeting_Video_Translation_Description__c,Other_Meeting_Facility_Fee_Description__c,Other_Meeting_Facility_Fee__c,Meeting_Video_Translation__c,
                                  Agency_Fee_Description__c,Agency_Fee__c,Other_Non_Benefit_Type_Fee__c,Total_Consultancy_Fee__c,Total_Gifts_amount__c,Other_Non_Benefit_Type_Fee_Description__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,GBU_Multiple__c,Total_Air_Travel_Count__c,Total_Gifts_Count__c ,Total_Consultancy_Count__c ,
                                  Gifts_Description_Remarks__c ,Consultancy_Description_Remarks__c,Others_Description_Remarks__c,Others_Sample_Decription_Remarks__c,Total_Others_Count__c ,Others_SKU_Sample_NCE_Amount__c ,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Total_Meal_Count__c,Total_LGT_Count__c
                                  from EMS_Event__c where id='a3FZ00000005b94' LIMIT 1];
                                  */
                                  
        Event_Beneficiaries__c eb = new Event_Beneficiaries__c();
        eb.EMS_Event__c = emsrec.Id;
        eb.Invitee_Type__c='Covidien Staff';
        eb.type__c='Covidien Staff';
        eb.no_of_pax__c = 25;
        insert eb;
        Event_Beneficiaries__c eb1 = new Event_Beneficiaries__c();
        eb1.EMS_Event__c = emsrec.Id;
        eb1.Invitee_Type__c='Covidien Staff';
        eb1.type__c='Covidien Staff';
        eb1.no_of_pax__c = 25;
        insert eb1;
        Event_Beneficiaries__c eb2 = new Event_Beneficiaries__c();
        eb2.EMS_Event__c = emsrec.Id;
        eb2.Invitee_Type__c='Covidien Staff';
        eb2.type__c='Covidien Staff';
        eb2.no_of_pax__c = 25;
        insert eb2;
        Event_Beneficiaries__c eb3 = new Event_Beneficiaries__c();
        eb3.EMS_Event__c = emsrec.Id;
        eb3.Invitee_Type__c='Covidien Staff';
        eb3.type__c='Covidien Staff';
        eb3.no_of_pax__c = 25;
        insert eb3;
        
        Attachment att = new Attachment();
        att.parentId = EMSrec.Id;
        att.name='Event_Invitees_Budgeted.xls';
        att.body = blob.valueOf('1234');
        insert att;  
        
        EMSrec.No_of_Approved_eCPAs__c=1;
        EMSrec.CPA_Status__c='Completed';
        update EMSrec;
        
        try{
        EMSrec.No_of_Approved_eCPAs__c=1;
        EMSrec.CPA_Status__c='New Request';
        update EMSrec;
        }
        catch(Exception e){
        }
        
        
        EMSrec.CPA_Status__c='Pending CCP Coordinator Approval';
        update EMSrec;
        
        EMSrec.CPA_Status__c='Declined Approval';
        update EMSrec;
        
        Cost_Center__c cc = new Cost_center__c(name='Singapore');
        insert cc;
        
        Cost_Center__c cc1 = new Cost_center__c(name='India');
        insert cc1;
        
        GBU_Division_Ems__c gde = new GBU_Division_Ems__c();
        gde.EMS_Event__c = EMSrec.Id;
        gde.Cost_Center__c = cc.Id;
        insert gde; 
        
        GBU_Division_Ems__c gde1 = new GBU_Division_Ems__c();
        gde1.EMS_Event__c = EMSrec.Id;
        gde1.Cost_Center__c = cc.Id;
        insert gde1; 
        
        ApexPages.currentPage().getParameters().put('EventId',emsrec.id);
        ApexPages.currentPage().getParameters().put('step','2');
        
        EMS_EnterCovStaffDetails obj = new EMS_EnterCovStaffDetails(new ApexPages.StandardController(new EMS_Event__c()));
        obj.getCountries();
        ApexPages.standardSetController ssc1 = obj.setConbenef;
        ApexPages.currentPage().getParameters().put('step','5');
        ApexPages.standardSetController ssc2 = obj.setConbenef;
        
        List<EMS_EnterCovStaffDetails.EventBeneficiariesWrapper> lst = obj.lstUIRecipients;
        obj.getlstAddedBudgetReciepients();
        obj.expandLTcolumns();
        obj.collapseLTColumns();
        obj.expandLodgingColumns();
        obj.collapseLodgingColumns();
        obj.expandNamingColumns();
        obj.collapseNamingColumns();
        obj.expandOthersColumns();
        obj.collapseOthersColumns();
        obj.expandATcolumns();
        obj.collapseATColumns();
        obj.refresh();
        obj.back();
        obj.getlistsize();
        obj.getlstApportionReciepients();
        obj.SaveEmployeeTransactions();
        obj.AddEmployees();
        ApexPages.StandardSetController setCons = obj.setcon;
        
        List<EMS_EnterCovStaffDetails.EventBeneficiariesWrapper> lstwrap1 = new  List<EMS_EnterCovStaffDetails.EventBeneficiariesWrapper>();
        lstwrap1.add(new EMS_EnterCovStaffDetails.EventBeneficiariesWrapper(eb2,true));
        lstwrap1.add(new EMS_EnterCovStaffDetails.EventBeneficiariesWrapper(eb3,true));
        obj.SearchCovStaff();
        obj.CheckAllbenef();
        obj.CheckAll();
        List<EMS_EnterCovStaffDetails.EmpCOnWrapper> lstwrap = obj.Employees;
        obj.deleteEmployee();
        obj.showPopup();
        obj.showPopupairtravel();
        obj.showPopuplodg();
        obj.showOthers();
        obj.showPopuplodging();
        obj.closePopup();
        obj.showGifts();
        
        
        obj.doSelectbenef();
        obj.doDeselectBenef();
        obj.doDeselectItem();
        obj.doselectItem();
        
        obj.getSelectedCount();
        obj.doNext();
        obj.doPrevious();
        obj.getHasPrevious();
        obj.getHasNext();
        
        obj.next();
        obj.first();
        obj.last();
        obj.previous();
        obj.getpageNumber();
        obj.getTotalPages();
        
        Test.startTest();
        obj.option='1';
        obj.BenefcontextItem=eb1.Id;
        obj.doSelectBenef();
        obj.redirectpopupairtravel();
        obj.redirectpopuplodging(); 
        obj.redirectPopup();
        obj.redirectpopupOthers();
        obj.redirectpopuptransport();
        
        obj.option='2';
        obj.BenefcontextItem=eb1.Id;
        obj.doSelectBenef();
        obj.redirectpopupairtravel();
        obj.redirectpopuplodging();        
        obj.redirectPopup();        
        obj.redirectpopupOthers();
        obj.redirectpopuptransport();
        //obj.redirectpopupgifts();
        
        Test.stopTest();
        
        
        
        
        
        
        
        }
    }
    
    
}