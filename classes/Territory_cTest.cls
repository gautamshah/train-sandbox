@isTest 
public class Territory_cTest
{
	@isTest
	static void create()
	{
		Territory t = Territory_c.create(1);
		
		system.assertNotEquals(null, t);
		system.assertEquals(t.Name, 'NAME 1');
		system.assertEquals(t.DeveloperName, 'NAME1');
	}

	@isTest
	static void createWithField()
	{
		Territory t = Territory_c.create(Territory.field.Custom_External_TerritoryID__c, null, 1);
		
		system.assertNotEquals(null, t);
		system.assertEquals(t.Custom_External_TerritoryID__c, 'EXTID1');
	}
	
}