public with sharing class EMS_HomePageController {

    public EMS_HomePageController() {

    }


    
    public Map<Id,EMS_Event__c> MEmsEvents{get;set;}
    Set<Id> AllEventIds = new Set<Id>();
    
    Public List<EMS_Event__c > getEventsIOwn(){
    DateTime last6months = DateTime.NOW() - 180;
    List <EMS_Event__c > Lpes = new List<EMS_Event__c >([SELECT Id,Name,Event_Status__c,CPA_Status__c,Event_Owner_User__c FROM EMS_Event__c where Event_Owner_User__c =:Userinfo.getUserId() and CreatedDate > :last6months]);
     
    return Lpes;
    } 
    Public List<ProcessInstanceWorkitem> getProcessInssteps(){
    List<EMS_GCC_Approvers_Delegated__c> AllDelegatedApprovals = EMS_GCC_Approvers_Delegated__c.getall().values();
    Set<String> FinalGCCApprovers = new Set<string>();
    for(EMS_GCC_Approvers_Delegated__c eachGCCApp : AllDelegatedApprovals){
        FinalGCCApprovers.add(eachGCCApp.Name);
    
    }
    Set<String> Status = new Set<String>{'Rejected','Approved','Recalled'};
    List<ProcessInstanceWorkitem> Lpes = new List<ProcessInstanceWorkitem>();
    if(!FinalGCCApprovers.contains(UserInfo.getUserId())){
        Lpes = [SELECT Id,ActorId,ProcessInstance.TargetObjectId,ProcessInstance.Id,ProcessInstance.LastActorId,ProcessInstance.Status FROM ProcessInstanceWorkitem where ActorId=:Userinfo.getUserId() and ProcessInstance.TargetObjectId!=null and ProcessInstance.Status ='Pending'];
    }
    else{
        EMS_GCC_Approvers_Delegated__c myCS1 = EMS_GCC_Approvers_Delegated__c.getValues(UserInfo.getUserId());
        Lpes = [SELECT Id,ActorId,ProcessInstance.TargetObjectId,ProcessInstance.Id,ProcessInstance.LastActorId,ProcessInstance.Status FROM ProcessInstanceWorkitem where ActorId=:myCs1.Delegated_Approval__c and ProcessInstance.TargetObjectId!=null and ProcessInstance.Status ='Pending'];
    } for(ProcessInstanceWorkitem p : Lpes){
     
       AllEventIds.add(p.ProcessInstance.TargetObjectId);  
     }
    MEmsEvents = new Map<Id,EMS_Event__c>([Select Id,Plan_Budget_Amt__c,Location_Country__c from EMS_Event__c where Id in:(AllEventIds)]);

    return Lpes;
    }    
   
    public Pagereference redirectcnv(){
    
    
    return new Pagereference('/a1u/o');
    }
}