@isTest
private class boxBuilderRemoteActionControllerTest{

    static testmethod void testCovProductsEmpty(){
        Opportunity testOpp = new Opportunity(Name = 'TestOpp', StageName='Test', CloseDate = Date.today());
        insert testOpp;

        Account testAcc = new Account(Name = 'Test Account');
        insert testAcc;
        
        Test.startTest();
        Integer emailbefore = Limits.getEmailInvocations();
        String emailSubject= 'testEmail';
        String emailBody= '<span>test</span>';
        String plainBody= 'test';
        String report= 'test,true';
        List<String> recipients = new List<String> { 'test@test.com', 'test2@test.com' };

        boxBuilderRemoteActionController.sendMail(emailSubject, emailBody, plainBody, report, recipients, testOpp.id, 'test');

        System.assertNotEquals(emailbefore,Limits.getEmailInvocations(),'email not sent');
        Test.stopTest();
    }       
}