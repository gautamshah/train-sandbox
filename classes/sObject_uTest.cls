@isTest
// We extend it to create a concrete class
public class sObject_uTest extends sObject_u
{
	private sObject_uTest()
	{
		super();
	}
	
	private sObject_uTest(sObjectType sObjType)
	{
		super(sObjType);
	}
	
	private sObject_uTest(List<sObject> sobjs)
	{
		super(sobjs);
	}
	
	private sObject_uTest(sObjectType sObjType, string whereClause)
	{
		super(sObjType, whereClause);
	}
	
	private static List<Account> testData()
	{
    	List<Account> accounts = new List<Account>();
    	accounts.add(new Account(Name = 'A'));
    	accounts.add(new Account(Name = 'B', Id = '001000000000001'));
		return accounts;		
	}
	
    @isTest
    static void constructors()
    {
    	sObject_uTest concrete = new sObject_uTest();
    	system.assertNotEquals(concrete.sobjs, null, 'Should not be null');
    	system.assertEquals(concrete.sobjs, concrete.get(), 'Should be the same');
    	
    	concrete = new sObject_uTest(Account.getSObjectType());
    	system.assertEquals(Account.getSObjectType(), concrete.sObjType, 'Should be same type');
    	
    	List<Account> accounts = new List<Account>();
    	accounts.add(new Account(Name = 'A'));
    	
    	concrete.addAll(accounts);
    	system.assertEquals(concrete.get().size(), accounts.size(), 'Should be the same size');
    	
    	concrete = new sObject_uTest((List<sObject>)accounts);
    	system.assertEquals(accounts, (List<Account>)concrete.get(), 'Should be the same list');
    	
    	concrete.save();
    	
    	concrete = new sObject_uTest(Account.sObjectType, ' WHERE Name = \'A\'');
    	system.assertEquals(accounts[0].Id, concrete.get()[0].Id, 'Should be the same record');
    	
    }
    
    @isTest
    static void extract()
    {
    	sObject_uTest concrete = new sObject_uTest();
    	concrete.addAll(testData());
    	
    	Set<object> names = concrete.extract(Account.fields.Name);
    	system.assert(names.contains('A'), 'A should be in the Set');
    }
    
    @isTest
    static void remove()
    {
    	sObject_uTest concrete = new sObject_uTest();
    	concrete.addAll(testData());

    	List<string> errors = concrete.save();
    	
    	sObject_uTest results = new sObject_uTest(Account.sObjectType, null);
    	List<string> delErrors = results.remove();
    	
    	system.assertEquals(delErrors, new List<string>{'total 1', 'number to be processed 1', 'not processed 0'}, delErrors);
    	
    	string status = results.checkStatus();
    	system.assertEquals(null, status, 'Should be done');
    }
    
    @isTest
    static void save()
    {
    	sObject_uTest concrete = new sObject_uTest();
    	concrete.addAll(testData());
    	
    	concrete.add(new Account(Name = 'C'));

    	List<string> errors = concrete.save();
    	system.assertEquals(errors.size(), 4, errors);
    }
    
    @isTest
    static void main()
    {
    	sObject_uTest concrete = new sObject_uTest();
		concrete.main(
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			(List<sObject>)new List<Account>(),
			(Map<Id, sObject>)new Map<Id, Account>(),
			(List<sObject>)new List<Account>(),
			(Map<Id, sObject>)new Map<Id, Account>(),
			0);
			   
		// Doesn't do anythng yet - nothing to assert 	
    }
}