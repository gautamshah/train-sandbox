/*

MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
2017.02.08      PAB        AV-280     Created
2017.02.09      IL         AV-280     Updated references to RecordType_g to ensure we use a centralized cache


USAGE
=====
RecordType_u rts = RecordType_u;
rts.fetch(Id); // => RecordType

*/
public with sharing class RecordType_u extends sObject_u
{
    private static RecordType_g cache = new RecordType_g();

    // This is typically called in cpqXXXX classes, but makes sense to leave here for RecordType
    static
    {
        if (cache.isEmpty())
        {
            cache.put(fetchFromDB());
        }
    }

    ////
    //// fetch
    ////
    public static RecordType fetch(Id id)
    {
        return cache.fetch(id);
    }

    ////
    //// fetch sets
    ////
    public static Map<Id, RecordType> fetch()
    {
        return cache.fetch();
    }

    public static Map<Id, RecordType> fetch(sObjectField field, object value)
    {
        return cache.fetch(field, value);
    }

    ////
    //// These fetch routines are based on the sObjectType field values
    ////
    //// fetch all the RecordType records for this sObject class
    public static Map<Id, RecordType> fetch(type classType)
    {
        return fetch(RecordType.field.sObjectType, sObject_c.getSObjectDescribe(classType).getName());
    }

    //// fetch all the RecordType records for this sObjectType
    public static Map<Id, RecordType> fetch(sObjectType sObjType)
    {
        return fetch(RecordType.field.sObjectType, sObject_c.getSObjectDescribe(sObjType).getName());
    }

    public static Map<Id, RecordType> fetch(set<Type> types)
    {
        Map<Id, RecordType> rMap = new Map<Id, RecordType>();
        for(type t : types)
            rMap.putAll(fetch(t));

        return rMap;
    }

    public static RecordType fetch(type classType, string developerName)
    {
        sObjectType sObjType = sObject_u.getSObjectType(classType);
        return fetch(sObjType, developerName);
    }

    public static RecordType fetch(sObjectType sObjType, string developerName)
    {
        if (sObjType != null && developerName != null)
        {
            Map<sObjectType, string> key = new Map<sObjectType, string>{ sObjType => developerName };
            return sObjectTypeAndDeveloperName2RecordType.containsKey(key) ?
                sObjectTypeAndDeveloperName2RecordType.get(key) :
                null;
        }
        else
            return null;
    }

    public static Map<object, Map<Id, RecordType>> fetch(sObjectField field, set<object> values)
    {
        return cache.fetch(field, values);
    }

    ////
    //// fetchFromDB
    ////
    private static List<RecordType> fetchFromDB()
    {
        string whereStr = ' WHERE isActive = true ';
        return fetchFromDB(whereStr);
    }

    @testVisible
    private static List<RecordType> fetchFromDB(string whereStr)
    {
        string query =
            SOQL_select.buildQuery(
                RecordType.getSObjectType(),
                whereStr,
                null,
                null);

        return (List<RecordType>)Database.query(query);
    }

    ////
    //// Additional methods
    ////
    public static List<RecordType> RecordTypes
    {
        get
        {
            return fetch().values();
        }
    }

    ////
    //// Additional Maps
    ////
    public static Map<sObjectType, Map<Id, RecordType>> sObjectType2Map
    {
        get
        {
            if (sObjectType2Map == null)
            {
                sObjectType2Map = new Map<sObjectType, Map<Id, RecordType>>();
                
                Map<object, Map<Id, RecordType>> found = cache.fetch(RecordType.field.sObjectType);
                
                for(object key : found.keySet())
                    sObjectType2Map.put(sObject_u.getSObjectType((string)key), found.get(key));
            }

            return sObjectType2Map;
        }

        private set;
    }

    public static Map<Map<sObjectType, string>, RecordType> sObjectTypeAndDeveloperName2RecordType
    {
        get
        {
            if (sObjectTypeAndDeveloperName2RecordType == null)
            {
                sObjectTypeAndDeveloperName2RecordType = new Map<Map<Schema.sObjectType, string>, RecordType>();

                for(sObjectType sot : sObjectType2Map.keySet())
                {
                    Map<Id, RecordType> rtMap = sObjectType2Map.get(sot);
                    for(RecordType rt : rtMap.values())
                        sObjectTypeAndDeveloperName2RecordType.put(
                            new Map<sObjectType, string>{ sot => rt.DeveloperName }, rt);
                }
            }

            return sObjectTypeAndDeveloperName2RecordType;
        }

        private set;
    }
}