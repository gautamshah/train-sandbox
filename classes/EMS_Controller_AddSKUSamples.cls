public class EMS_Controller_AddSKUSamples{

    public Id EventId{get;set;}
    public String SKUSampleCode{get;set;}
    public String SKUSampleName{get;set;}
    public String SourceUrl{get;set;}
    public String sourceflowUrl{get;set;}
    public boolean refreshPage{get;set;}
    private boolean IsSearched;
    public String Country{get;set;}
    public EMS_Event__c CurrentEMSRecord{get;set;}
    public List<Sample_CCIExpense__c> LSCCI ;
    public String ReciepientId{
        get;
        set {
            ReciepientId= value;
        }
    }

    
   public List<SelectOption> getCountries()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Product_SKU__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
   
    //Constructor
    public EMS_Controller_AddSKUSamples(ApexPages.StandardController controller) {
         refreshPage=false;
         IsSearched=false;
         selectedContactIds=new Set<Id>();
         LSCCI = new List<Sample_CCIExpense__c>();
         String chosenstep= ApexPages.currentPage().getParameters().get('step');
         EventId= ApexPages.currentPage().getParameters().get('EventId');
         sourceUrl='/apex/zaapit_tab_EMS_SKUSamples?Id='+EventId;
         sourceflowUrl='/apex/EMS_VFProcessflow?EventId='+EventId+'&step='+chosenstep;
         system.debug('======>'+sourceUrl);
         CurrentEMSRecord = new EMS_Event__c();
         for(EMS_Event__c objEve:[Select Id,Name,Start_Date__c,End_Date__c,Booth_Construction_Fee__c,Booth_Construction_Description__c,
                                  Booth_Construction_Remarks__c,Printing_Fee_Remarks__c,Printing_Fee__c,Printing_Fee_Description__c,
                                  Design_Fee__c,Design_Fee_Description__c,Design_Fee_Remarks__c,Meal_Value_Per_Person__c,Local_Transportation_per_Person__c,
                                  Event_Production_Description__c,Event_Production_Remarks__c,Advertisement__c,
                                  Total_Non_HCP_Expenses__c,Meal_Summary_Description_Remark__c,Local_Ground_Transportation_Description__c,
                                  Education_Grant__c,CEP__c,Event_Production_Fee__c,ISR_CSR__c,Event_level_sponsorship_of_congresses_co__c,Speaker_or_other_consulting_services__c,
                                  Total_No_of_Days__c,Total_Meal_Amount__c,Total_HCP_Expenses__c,Total_Local_GTA__c,
                                  Event_Id__c,Benefit_Summary__c,Location_Country__c,Location_State__c,Venue__c,Publication_grant__c,
                                  Is_Event_21_days_and_GCC_Approval_done__c,Event_Owner_User__c,Event_Type__c,Requestor_Name_1__c,Division_Multiple__c,
                                  Total_No_of_Recipients__c,Booth_Space__c,Workshop_conducted_by_Covidien_sales__c,Patient_awareness_public_forum__c,
                                  Stand_alone_gift__c,VOC_interviews_discussions__c,Event_Status__c,GBU_Multiple__c,
                                  GBU_Expense_Percent__c,Division_Expense_Percent__c,Total_Meal_Count__c,Total_LGT_Count__c 
                                  from EMS_Event__c where Id=:EventId]){
             CurrentEMSRecord = objEve;        
         }
         
                                   
    }
   
   
    
    
    //Method to redirect back to Event Detail Page Record
    Public Pagereference Back(){
    
    Pagereference P = new Pagereference('/'+EventId);
    P.setredirect(true);
    return P;    
    }
    
    
    //Method to redirect as per Category
    
    
    
        
    //Method to insert CCI Samples
    public PageReference AddCCISamples(){  
    refreshPage=true;
    LSCCI.clear();
    for(Id eachConId : selectedContactIds){
       
        System.debug('////////inside Org insert');
        Sample_CCIExpense__c SampleExpense= new Sample_CCIExpense__c (Product_SKU__c=eachConId,EMS_Event__c=EventId,Type__c='Product Package');
        LSCCI.add(SampleExpense);
        
        
    }
    
     try{    
         Database.saveresult[] sr=database.insert(LSCCI,false);
         System.debug('<<<<<<<'+sr);
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Samples added to Event successfully');
         ApexPages.addMessage(myMsg);
         return null; 
        }
     catch(Exception E){
     
      System.debug('---->An Error occured while inserting Transactions records'+E.getmessage());
      return null;
     }   
        
    
    
    }
    
    
    
   
        public ApexPages.StandardSetController setCon {
        get {
            
            if(setcon==null){
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id,Name FROM Product_SKU__c WHere Id=null]));
           }
            return setCon;
        }
        set;
    }
 
        private Set<Id> selectedContactIds=  new Set<Id>();
      
       //Function to display Contact Search Results
       
       public String query;
       public PageReference SearchContacts(){
        refreshPage=false;
        IsSearched=true;
        this.selectedContactIds= new Set<Id>();
        List<Sample_CCIExpense__c> LexistingSampleCCIs =new List<Sample_CCIExpense__c>([Select Id,Name,Product_SKU__r.Name,Product_SKU__c,Type__c from Sample_CCIExpense__c where Type__c='Product Package' and EMS_Event__c=:currentEMSRecord.Id and Product_SKU__c!=null]);
        Set<String> AlreadyExistingSKus = new Set<String>();
        for(Sample_CCIExpense__c eachSampleCCI : LexistingSampleCCIs ){
            AlreadyExistingSKus.add(eachSampleCCI.Product_SKU__c);
        
        }
        setcon=null;
        
        string type;
        
         Integer limitRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        System.debug('???????????????');
         query='Select Id,SKU__c,Name,Country__c,Sales_Class__c,Current_FY_Standard_Cost__c,EMS_Sku_Price__c from Product_SKU__c where SKU__c LIKE \'%' + SKUSampleCode + '%\'' + ' and name LIKE \'%' + SKUSampleName + '%\''+' and Country__c=:Country and Id not in :AlreadyExistingSKus ' +' ORDER BY SKU__c DESC LIMIT 5000 ';
        System.debug('===========?????'+query);
        this.setCon= new ApexPages.StandardSetController( database.query(query) );
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(5);
       
       if(setcon==null ){
           setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id from Product_SKU__c where Id=null]));
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search results found for this search criteria');
           ApexPages.addMessage(myMsg);
       }
       
        
       return Null;
       }
       
      
       public List<CCIExpenseWrapper> getContacts(){
       
        List<CCIExpenseWrapper> rows = new List<CCIExpenseWrapper>();
                      
        
        
         
       System.debug('+++++++'+this.setcon.getrecords());
        for(sObject r : this.setCon.getRecords()){
            CCIExpenseWrapper row;
            Product_SKU__c c= (Product_SKU__c)r;
            row = new CCIExpenseWrapper(null,null,c,false); 
            
            if(this.selectedContactIds.contains(r.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
            System.debug('??????'+rows);
        }
        if(rows.isEmpty() && IsSearched){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Search Results found for this criteria!!');
            ApexPages.addMessage(myMsg);
        }
        IsSearched=false;
        return rows;
 
    }
       
       
      
      //Delete Contact
      public Pagereference deleteContact(){
      try{
      database.delete(ReciepientId);
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'HCP Beneficiary successfully deleted');
      ApexPages.addMessage(myMsg);
      return null; 
      }
      catch(Exception e){
      
      System.debug('---->An Error occured while deleting HCP Beneficiary'+E.getmessage());
      return null;
      
      
      
      }
      } 
     
   
  
    public String contextItem{get;set;}
    public void doSelectItem(){
        System.debug('--->'+contextitem);
        selectedContactIds.add(contextItem);
 
    }
 
    /*
    *   handle item deselected
    */
    public void doDeselectItem(){
 
        selectedContactIds.remove(contextItem);
 
    }
 
    /*
    *   return count of selected items
    */
    public Integer getSelectedCount(){
 
        return this.selectedContactIds.size();
 
    }
 
    /*
    *   advance to next page
    */
    public void doNext(){
 
        if(this.setCon.getHasNext())
            this.setCon.next();
 
    }
 
    /*
    *   advance to previous page
    */
    public void doPrevious(){
 
        if(this.setCon.getHasPrevious())
            this.setCon.previous();
 
    }
    
    
    public Boolean getHasPrevious(){
 
        return this.setCon.getHasPrevious();
 
    }
 
    /*
    *   return whether next page exists
    */
    public Boolean getHasNext(){
 
        return this.setCon.getHasNext();
 
    }
 
    /*
    *   return page number
    */
    public Integer getPageNumber(){
 
        return this.setCon.getPageNumber();
 
    }
 
    /*
    *    return total pages
    */
    Public Integer getTotalPages(){
 
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
 
        Decimal pages = totalSize/pageSize;
 
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    
    
    //Wrapper for CCI Expenses
    public class CCIExpenseWrapper{
    
    
    public Cost__c CstElement{get;set;}
    public Condition_Procedure__c CP{get;set;}
    Public Product_SKU__c PSKU{get;set;}
    public boolean isselected{get;set;}
    
    
    
    public CCIExpenseWrapper(Cost__c cost,Condition_Procedure__c condproc,Product_SKU__c eachProdSKU,boolean checked){
    
    CstElement = cost;
    CP = condproc;
    PSKU=eachProdSKU;
    isselected = checked;
    
    
    }
    
    
    }
    
    
    
    
    
    
}