/*
COMMENT INSTRUCTIONS
====================
1.	Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.	Tab stops every 4 spaces
3.	Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead

DEVELOPER INFORMATION
=====================
Name                 Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund        PAB             Medtronic.com
Isaac Lewis          IL              Statera.com
Bryan Fry            BF              Statera.com


MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
YYYYMMDD   A    PAB        CPR-000    Updated comments section
                           AV-000
20161103   A    IL         AV-001     Title of the Jira – Some changes needed to be made to support Medical Supplies
20161207   A    BF         AV-240     Create new Proposal for Cancelled Agreement - Add cancel()) and StatusIndicators
20170212   A    BF         AV-309     Move status to fully signed so user can activate a Scrub PO with an
                                      in-person signature not in Docusign.

Notes:
1.	If there are multiple related Jiras, add them on the next line
2.	Combine the Date & Id and use it to mark changes related to the entry
     // YYYYMMDD-A (Only put “inline” comments related to the specific logic found at this location here)
*/     
public class cpqAgreement_c extends cpqDeal_c
{
	// Namespace:  Apttus__
	// Common Name:  Agreement
	// API: Apttus__APTS_Agreement__c
	
 	public static final sObjectField
 		//FIELD_MasterAgreementId 				= Apttus__APTS_Agreement__c.fields.Apttus_QPComply__MasterAgreementId__c,
 		FIELD_Duration 							= Apttus__APTS_Agreement__c.fields.Duration__c,
 		FIELD_BuyoutAsOfDate 					= Apttus__APTS_Agreement__c.fields.Buyout_as_of_Date__c,
 		FIELD_BuyoutAmount 						= Apttus__APTS_Agreement__c.fields.Buyout_Amount__c,
 		FIELD_TaxGrossNetOfTradein 				= Apttus__APTS_Agreement__c.fields.Tax_Gross_Net_of_TradeIn__c,
 		FIELD_SalesTaxPercent 					= Apttus__APTS_Agreement__c.fields.Sales_Tax_Percent__c,
 		FIELD_TBDAmount 						= Apttus__APTS_Agreement__c.fields.TBD_Amount__c,
 		FIELD_TotalAnnualSensorCommitmentAmount = Apttus__APTS_Agreement__c.fields.Total_Annual_Sensor_Commitment_Amount__c,
 		FIELD_Opportunity 						= Apttus__APTS_Agreement__c.fields.Apttus__Related_Opportunity__c,
 		FIELD_Account 							= Apttus__APTS_Agreement__c.fields.Apttus__Account__c,
		FIELD_OwnerId							= Apttus__APTS_Agreement__c.fields.OwnerId,
		FIELD_Approver_Seller 					= Apttus__APTS_Agreement__c.fields.SellerApprover__c,
		FIELD_Approver_RSM 						= Apttus__APTS_Agreement__c.fields.RSM_Approver__c,
		FIELD_Approver_ZVP 						= Apttus__APTS_Agreement__c.fields.ZVP__c,
		FIELD_Approver_SVP 						= Apttus__APTS_Agreement__c.fields.SVP_User__c,
		FIELD_ApprovalIndicator					= Apttus__APTS_Agreement__c.fields.Approval_Indicator__c,
		FIELD_Name								= Apttus__APTS_Agreement__c.fields.Name,
    	FIELD_ElectrosurgeryEaches				= Apttus__APTS_Agreement__c.fields.Electrosurgery_Eaches__c,
    	FIELD_ElectrosurgerySales				= Apttus__APTS_Agreement__c.fields.Electrosurgery_Sales__c,
    	FIELD_ElectrosurgeryIncrementalSpend	= Apttus__APTS_Agreement__c.fields.Electrosurgery_Incremental_Spend__c,
    	FIELD_ElectrosurgeryIncrementalQuanitity= Apttus__APTS_Agreement__c.fields.Electrosurgery_Incremental_Quantity__c,
    	FIELD_ElectrosurgeryIncrementalTotalSpend=Apttus__APTS_Agreement__c.fields.Electrosurgery_Total_Incremental_Spend__c,
    	FIELD_LigasureEaches					= Apttus__APTS_Agreement__c.fields.Ligasure_Eaches__c,
    	FIELD_LigasureSales						= Apttus__APTS_Agreement__c.fields.Ligasure_Sales__c,
    	FIELD_LigasureIncrementalSpend			= Apttus__APTS_Agreement__c.fields.Ligasure_Incremental_Spend__c,
    	FIELD_LigasureIncrementalQuanitity 		= Apttus__APTS_Agreement__c.fields.Ligasure_Incremental_Quantity__c,
    	FIELD_LigasureIncrementalTotalSpend 	= Apttus__APTS_Agreement__c.fields.Ligasure_Total_Incremental_Spend__c,
    	FIELD_IncrementalTotal					= Apttus__APTS_Agreement__c.fields.Total_Incremental__c;

    public enum StatusIndicators
    {
        Request,
        RequestApproval,
        SubmittedRequest,
        ApprovedRequest,
        CancelledRequest,
        InAmendment,
        Activated,
        FullySigned
    }

    public static Map<StatusIndicators, string> enum2string = new Map<StatusIndicators, string>
    {
        StatusIndicators.Request            => 'Request',
        StatusIndicators.RequestApproval    => 'Request Approval',
        StatusIndicators.SubmittedRequest   => 'Submitted Request',
        StatusIndicators.ApprovedRequest    => 'Approved Request',
        StatusIndicators.CancelledRequest   => 'Cancelled Request',
        StatusIndicators.InAmendment        => 'In Amendment',
        StatusIndicators.Activated          => 'Activated',
        StatusIndicators.FullySigned        => 'Fully Signed'
    };

    public static string convert(StatusIndicators si)
    {
        return enum2string.get(si);
    }

    public static List<RecordType> getRecordTypes() 
    {
        return cpqDeal_RecordType_c.getRecordTypesBySObjectAndId().get(cpq_u.AGREEMENT_SOBJECT_TYPE).values();
    }

    public static Apttus__APTS_Agreement__c create(Id rtId, integer ndx)
    {
        Apttus__APTS_Agreement__c sobj = new Apttus__APTS_Agreement__c();
        sobj.put(FIELD_Name, 'Test Agreement ' + string.valueOf(ndx).leftPad(10));
        sobj.RecordTypeId = rtId;

        return sobj;
    }

	public static Apttus__APTS_Agreement__c fetchApprovers(Id id)
    {
    	return [SELECT OwnerId,
    			       SellerApprover__c,
    		           RSM_Approver__c,
    		           ZVP__c,
    		           SVP_User__c
    		    FROM Apttus__APTS_Agreement__c
    		    WHERE Id = :id];
    }

    public static void cancel(Apttus__APTS_Agreement__c agmt)
    {
        agmt.Apttus__Status__c = convert(StatusIndicators.CancelledRequest);
        agmt.Parent_Proposal_Cancelled__c = agmt.Apttus_QPComply__RelatedProposalId__c;
        agmt.Apttus_QPComply__RelatedProposalId__c = null;
        update agmt;

        // Create task on related proposal to make note of the cancelled agreement.
        Task t = new Task(Subject = 'Related Agreement ' + agmt.AgreementID__c + ' cancelled and proposal reopened', ActivityDate = System.today(),
                          Description = 'Related Agreement ' + agmt.AgreementID__c + ' cancelled and proposal reopened',
                          Type = 'Email', Status = 'Completed', WhatId = agmt.Parent_Proposal_Cancelled__c);
        insert t;
    }

    public static Boolean setToFullySigned(Apttus__APTS_Agreement__c agmt)
    {
        try {
            agmt.Apttus__Status__c = convert(StatusIndicators.FullySigned);
            update agmt;
        } catch (Exception e) {
            return false;
        }
        
        return true;
    }
}