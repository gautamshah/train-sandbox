@isTest
public class cpqUser_uTest
{
	@isTest
	static void fetchValidReps()
	{
		Map<Id, User> users = cpqUser_u.fetchReps();
		System.assert(users.size() > 0,'No users with three manager levels returned');
	}

	@isTest static void fetchByOrganizationName ()
	{
		Map<Id, User> RMS = cpqUser_u.fetch(OrganizationNames_g.Abbreviations.RMS);
		System.assert(RMS.size() > 0, 'No users returned');
	}
}