/*
Controller for DocuSign links, buttons, and actions
===================================================

Based on JavaScript Button Code from DocuSign
---------------------------------------------
- [Button Code Samples](https://support.docusign.com/en/guides/dfs-admin-guide-sample-js-code-custom-buttons)
- [Recipient Types & Parameters](https://www.docusign.com/p/RESTAPIGuide/Content/REST%20API%20References/Recipient%20Parameter.htm)
- [Offline Signing](https://www.docusign.com/p/RESTAPIGuide/Content/REST%20API%20References/Offline%20Signing.htm)

Recipient Types
---------------
Uses different names in button code (no reference found)

- Agent
- Carbon Copy
- Certified Delivery
- Editor
- In Person Signer
- Intermediary
- Signer

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2016-01-26      Isaac Lewis         Created.
2016-02-23      Isaac Lewis         Change Routing Order for In-Person Signing
2016-07-19      Isaac Lewis         Added validation to prevent signature if agreement is not generated
2016-08-29      Isaac Lewis         Switched signer order so customer signs first.
                                    This means user will not default to signing
                                    screen anymore since user must wait for
                                    customer signature.
2016-10-31      Isaac Lewis         Added RFA record type
===============================================================================
*/

public class CPQ_SSG_AGM_DocuSign_Controller {

    private Apttus__APTS_Agreement__c agmt;
    private string recordType;
    private User ccUser;

    private string pageName;
    private boolean isHosted;
    private boolean inTesting = false;
    private boolean hasErrors = false;

    private map<Integer, map<String,String>> crlMap;
    private map<Integer, String> ccrMap;
    private map<Integer, String> cctMap;


    // Recipient Types
    public static final String CCTM_SIGNER = 'Signer';
    public static final String CCTM_SIGN_IN_PERSON = 'Sign In Person';
    public static final String CCTM_CARBON_COPY = 'CC';

    //********* DocuSign Option Declarations (Do not modify )*********//
    private string RC, RSL, RSRO, RROS, CCRM, CCTM, CCNM, CRCL, CRL, OCO, DST, LA, CEM, CES, STB, SSB, SES, SEM, SRS, SCS, RES;
    //****************************************************************//

    public CPQ_SSG_AGM_DocuSign_Controller(ApexPages.StandardController stdController) {

        this.agmt = (Apttus__APTS_Agreement__c)stdController.getRecord();

        // SAMPLES
        // this.agmt.Id = 'a2hK0000000hF5E'; // Scrub PO
        // this.inTesting = true;

        this.agmt = [SELECT Id, Name, Primary_Contact2__r.FirstName, Primary_Contact2__r.LastName, Primary_Contact2__r.Email, Primary_Contact_Email__c, Contact_Full_Name__c, RecordType.DeveloperName FROM Apttus__APTS_Agreement__c WHERE Id = :this.agmt.Id];
        this.recordType = this.agmt.RecordType.DeveloperName;

        this.pageName = ApexPages.currentPage().geturl().substringAfter('/apex/').substringBefore('?');
        this.isHosted = isHosted();

        this.crlMap = new Map<Integer, Map<String,String>>();
        this.ccrMap = new Map<Integer, String>();
        this.cctMap = new Map<Integer, String>();
        this.ccUser = [SELECT Id, Email, FirstName, LastName FROM User WHERE Username LIKE 'ssgcustserv%' LIMIT 1];

        initDocuSignVars();

        //*********** Modify Options Here ************//

        // Load Attachments (default on)
        this.LA = '1'; //Ex: '0'

        // Custom Email Subject (default in config)
        this.CES = 'Document for Signature: ' + EncodingUtil.urlEncode(this.agmt.Name,'UTF-8'); //Ex: 'Re: Opportunity Name: {!Opportunity.Name}'

        // Show Tab (Next) Button
        if(this.isHosted){
            this.STB = '0';
        }

        // Recipient Routing Order Sequential (default not sequential)
        // Default to receive in order created, unless overridden in recipient list
        // Hosted signing requires all signers be in same routing order position of 1
        if(!this.isHosted){
            this.RROS = '1';
        }

        // Recipient Lists
        // TODO: Make a custom setting to configure deal-specific settings, like signer order. This would help on the admin side.

        // // Customer (Signer) > Sales Rep (Signer)
        // // RFA, SMART CART, SCRUB PO
        if ( this.recordType.startsWith(CPQ_AgreementProcesses.ROYALTY_FREE_AGREEMENT_RT) ||
             this.recordType.startsWith(CPQ_AgreementProcesses.SMART_CART_RT) ||
             this.recordType.startsWith(CPQ_AgreementProcesses.SCRUB_PO_RT)
        ) {
            addPrimaryContact('Signer 1',CCTM_Signer); // Type will switch for in-person hosting
            addCurrentUser('Signer 2',CCTM_Signer);
            // Old SCRUB PO: Sales Rep (Signer) > Customer (Signer) > Customer Service (CC)
            // addCarbonCopy(ccUser.email,ccUser.FirstName+' '+ccUser.LastName);
        }

        // // CUSTOM KIT
        // // Customer (Signer)
        else if (this.recordType.startsWith(CPQ_AgreementProcesses.CUSTOM_KIT_RT)) {
            addPrimaryContact('Signer 1',CCTM_Signer);
        }

        //*******************************************************//

    }

    private boolean isHosted(){
        if (ApexPages.currentPage().getParameters().get('isHosted') <> NULL) {
            return Boolean.valueOf(ApexPages.currentPage().getParameters().get('isHosted'));
        } else if (this.pageName.toLowerCase().contains('host')) {
            return true;
        } else {
            return false;
        }
    }

    private boolean addCurrentUser(String Role_Name, String Type_Name){
        Map<String,String> recipient = new Map<String,String>();
            recipient.put('Email',UserInfo.getUserEmail());
            recipient.put('FirstName',UserInfo.getFirstName());
            recipient.put('LastName',UserInfo.getLastName());
            recipient.put('Role',Role_Name);
            recipient.put('Type',Type_Name);
        if(this.isHosted){
            recipient.put('RoutingOrder','1'); // All hosted users must be in routing order 1
            recipient.put('SignNow','1');
        }

        Boolean isSuccess = addRecipient(recipient);
        return isSuccess;
    }

    private boolean addPrimaryContact(String Role_Name, String Type_Name){

        Map<String,String> recipient = new Map<String,String>();

        if(this.isHosted){ // In-Person Signature
            recipient.put('Email',UserInfo.getUserEmail());
            recipient.put('FirstName',UserInfo.getFirstName()); // Must use User.FirstName and User.LastName for hosted session
            recipient.put('LastName',UserInfo.getLastName());
            recipient.put('SignInPersonName',this.agmt.Contact_Full_Name__c);
            recipient.put('Role',Role_Name);
            recipient.put('Type',CCTM_SIGN_IN_PERSON);
            recipient.put('RoutingOrder','1'); // All hosted users must be in routing order 1
            recipient.put('SignNow','1');
        } else { // Remote Signature
            recipient.put('Email',this.agmt.Primary_Contact_Email__c);
            recipient.put('LastName',this.agmt.Contact_Full_Name__c);
            recipient.put('Role',Role_Name);
            recipient.put('Type',Type_Name);
        }

        Boolean isSuccess = addRecipient(recipient);
        return isSuccess;

    }

    // TODO: Resolve Carbon Copy conflict with Offline Signing
    /*
    private boolean addCarbonCopy(String Email, String Name){

        Map<String,String> recipient = new Map<String,String>();
        recipient.put('Email',Email);
        recipient.put('LastName',Name);
        recipient.put('Role','Carbon Copy');
        recipient.put('Type','Carbon Copy');

        boolean isSuccess = addRecipient(recipient);
        return isSuccess;

    }
    */

    private boolean addRecipient(Map <String,String> Vars){

        boolean isSuccess = true;
        Integer idx;

        // Set Index for Role, Type & Recipient
        if(this.crlMap.size() <> NULL){
            idx = this.crlMap.size()+1;
            this.crlMap.put(idx,Vars);
        } else {
            isSuccess = false;
            return isSuccess;
        }

        // Assign Role to Index
        string role_name = this.crlMap.get(idx).get('Role'); // Get Role Name
        this.ccrMap.put(idx,role_name); // Put Role Name in Role Map
        this.crlMap.get(idx).put('Role',String.valueOf(idx)); // Replace Role Name with Role Map Index
        this.CCRM += idx + '~' + role_name + ';';

        // Assign Type to Index
        string type_name = this.crlMap.get(idx).get('Type'); // Get Type Name
        this.cctMap.put(idx,type_name); // Put Type Name in Type Map
        this.crlMap.get(idx).remove('Type'); // Remove Type Name
        this.CCTM += idx + '~' + type_name + ';';

        // Assign Recipient to Index
        if(this.CRL != ''){
            this.CRL += ',';
        }
        for(string key : this.crlMap.get(idx).keySet()){
            this.CRL += key + '~' + this.crlMap.get(idx).get(key) + ';' ;
        }
        return isSuccess;

    }

    //***** DocuSign Option Init (Do not modify) *****//
    private boolean initDocuSignVars(){
        // Related Content (default no related content)
        this.RC = ''; //Ex: GetRelContentIDs("{!Opportunity.Id}");
        // Recipient Signer Limit (default no limit)
        this.RSL = ''; //Ex: '3'
        // Recipient Starting Routing Order (default 1)
        this.RSRO = ''; // Ex: '1'
        // Recipient Routing Order Sequential (default not sequential)
        this.RROS = ''; //Ex: '1'
        // Custom Contact Role Map (default config role)
        this.CCRM = ''; //Ex: 'Decision Maker~Signer1;Economic Buyer~Carbon Copy'
        // Custom Contact Type Map (default Signer)
        this.CCTM = ''; //Ex: 'Decision Maker~Signer;Economic Buyer~CC'
        // Custom Contact Note Map (default no note)
        this.CCNM = ''; //Ex: 'Decision Maker~Note for DM;Economic Buyer~Note For EB;DEFAULT_NOTE~Default Note'
        // Custom Related Contact List (default object contact)
        this.CRCL = ''; //Ex: 'MyContacts__r,Email~Email__c;FirstName~First_Name__c;LastName~Last_Name__c;Role ~Role__c,LoadDefaultContacts~0'
        // Custom Recipient List
        this.CRL = ''; //Ex: 'Email~;FirstName~;LastName~;Role~SignInPersonName~;RoutingOrder~;AccessCode~;RecipientNote~;SignNow~, LoadDefaultContacts~1'
        // One Click Option (default edit envelope screen)
        this.OCO = ''; //Ex: Tag
        // DocuSign Template ID (default no template)
        this.DST = ''; //Ex: '67870A79-A0B5-4596-8AC1-CC7CC1EA01EB'
        // Load Attachments (default on)
        this.LA = ''; //Ex: '0'
        // Custom Email Message (default in config)
        this.CEM = ''; //Ex: 'Envelope sent by [FirstName] [LastName] ([Email])!'
        // Custom Email Subject (default in config)
        this.CES = ''; //Ex: 'Re: Opportunity Name: {!Opportunity.Name}'
        // Show Tag Button (default in config)
        this.STB = ''; //Ex: '1'
        // Show Send Button (default in config)
        this.SSB = ''; //Ex: '1'
        // Show Email Subject (default in config)
        this.SES = ''; //Ex: '1'
        // Show Email Message (default in config)
        this.SEM = ''; //Ex: '1'
        // Show Reminder/Expire (default in config)
        this.SRS = ''; //Ex: '1'
        // Show Chatter (default in config)
        this.SCS = ''; //Ex: '1'
        // Reminder and Expiration Settings
        this.RES = ''; //Ex: '0,1,2,0,120,3'
        return true;
    }
    //************************************************//

    private boolean hasGeneratedDocument(){
        boolean generated = false;
        Task tsk;
        try {
            tsk = [SELECT Id FROM Task WHERE WhatId = :this.agmt.Id AND (Subject = 'Generated Agreement' OR Subject = 'Regenerated Agreement') LIMIT 1];
        } catch (QueryException qe) {
            System.debug('*** QueryException: ' + qe.getMessage());
        }
        if(tsk != NULL){
            generated = true;
        }
        return generated;
    }

    //********* Page Callout (Do not modify) *********//
    public PageReference sendToDocusign() {

        PageReference ref = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID=' + this.agmt.Id + '&RC=' + this.RC + '&RSL=' + this.RSL + '&RSRO=' + this.RSRO + '&RROS=' + this.RROS + '&CCRM=' + this.CCRM + '&CCTM=' + this.CCTM + '&CRCL=' + this.CRCL + '&CRL=' + this.CRL + '&OCO=' + this.OCO + '&DST=' + this.DST + '&CCNM=' + this.CCNM + '&LA=' + this.LA + '&CEM=' + this.CEM + '&CES=' + this.CES + '&SRS=' + this.SRS + '&STB=' + this.STB + '&SSB=' + this.SSB + '&SES=' + this.SES + '&SEM=' + this.SEM + '&SRS=' + this.SRS + '&SCS=' + this.SCS + '&RES=' + this.RES);

        // Prevent from sending without primary contact
        if(this.agmt.Primary_Contact2__c == NULL){
            this.hasErrors = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Please provide a primary contact.'));
        }

        // Prevent from sending without generating document
        if(!hasGeneratedDocument()){
            this.hasErrors = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Please generate an agreement from the record before sending for signature.'));
        }

        // For Parameter Debugging
        /*
        if(this.inTesting){

            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Record Type: ' + this.recordType));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Page Name: ' + this.pageName));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'Is Hosted: ' + this.isHosted));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'crlMap: ' + this.crlMap.size()));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'ccrMap: ' + this.ccrMap.size()));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,'cctMap: ' + this.cctMap.size()));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'URL Params: ' ));
            String[] strings = new List<String>(ref.getUrl().split('&'));
            for(String s : strings){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, EncodingUtil.urlDecode(s, 'UTF-8') ));
            }
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'URL: ' ));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, EncodingUtil.urlDecode(ref.getUrl(), 'UTF-8')));

        }
        */
        if(this.hasErrors || this.inTesting){
            ref.setRedirect(false);
            return null;
        }

        return ref;

    }
    //*******************************************************//

}