// https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_controller_error_handling.htm

@isTest
private class Test_CPQ_SSG_AGM_DocuSign_Controller {

	static List<Apttus__APTS_Agreement__c> testAgreements;
    static final string PageName = '/apex/CPQ_SSG_AGM_DocuSign';

    static map<String,String> getParams(String url){
        String[] params = new List<String>(url.substringAfter('?').split('&'));
        Map<String,String> paramList = new Map<String,String>();
        for(String s : params){
            String param = EncodingUtil.urlDecode(s, 'UTF-8');
            List<String> paramSplit = param.split('=');
            String key = paramSplit.get(0);
            String value;
            if(paramSplit.size() > 1){
                value = param.split('=').get(1);
            } else {
                value = '';
            }
            paramList.put(key, value);
        }
        return paramList;
    }

    static void createTestData()
    {

		//cpq_test.buildScenario_CPQ_ConfigSettings();

		Account acct = new Account(Name = 'Test Account 1', BillingState = 'CO', BillingPostalCode = '12345', BillingCountry = 'US');
		insert acct;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = acct.Id);
		insert c;

		Apttus_Config2__PriceList__c testPriceList = cpqPriceList_c.SSG;

		Apttus_Approval__ApprovalsCustomConfig__c approvalsConfig = new Apttus_Approval__ApprovalsCustomConfig__c(Name = 'Apttus__APTS_Agreement__c', Apttus_Approval__ApprovalStatusField__c = 'Apttus_Approval__Approval_Status__c', Apttus_Approval__ApprovalContextType__c = 'Single');
		insert approvalsConfig;

		RecordType Scrub_PO = RecordType_u.fetch(Apttus__APTS_Agreement__c.class,'Scrub_PO');

		Map<String,Id> mapAgreementRType = New Map<String,Id>();
		for(RecordType R: RecordType_u.fetch(Apttus__APTS_Agreement__c.class).values()) {
				mapAgreementRType.Put(R.DeveloperName, R.Id);
		}

		testAgreements = new List<Apttus__APTS_Agreement__c>{
            // No Primary Contact
			new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeId = mapAgreementRType.get('Scrub_PO'), Apttus__Account__c = acct.Id),
			// Scrub PO
			new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeId = mapAgreementRType.get('Scrub_PO'), Apttus__Account__c = acct.Id, Primary_Contact2__c = c.Id),
			// Smart Cart
			new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeId = mapAgreementRType.get('Smart_Cart'), Apttus__Account__c = acct.Id, Primary_Contact2__c = c.Id),
			// Custom Kit
			new Apttus__APTS_Agreement__c(Name = 'Test Agreement', RecordTypeId = mapAgreementRType.get('Custom_Kit'), Apttus__Account__c = acct.Id, Primary_Contact2__c = c.Id)
		};
		insert testAgreements;

		List<Task> tasks = new List<Task>{
			new Task(Subject = 'Generated Agreement', Priority = 'Low', Status = 'Completed', WhatId = testAgreements[0].Id),
			new Task(Subject = 'Regenerated Agreement', Priority = 'Low', Status = 'Completed', WhatId = testAgreements[1].Id),
			new Task(Subject = 'Generated Agreement', Priority = 'Low', Status = 'Completed', WhatId = testAgreements[2].Id),
			new Task(Subject = 'Generated Agreement', Priority = 'Low', Status = 'Completed', WhatId = testAgreements[3].Id)
		};
		insert tasks;

    }

	@isTest static void Test_CPQ_SSG_AGM_DocuSign_Controller_No_Primary_Contact() {

		createTestData();

		Test.startTest();

			Apttus__APTS_Agreement__c agmt = testAgreements[0];
        	PageReference pageRef = new PageReference(pageName);
        	pageRef.getParameters().put('id',agmt.id);
        	Test.setCurrentPage(pageRef);

			CPQ_SSG_AGM_DocuSign_Controller ctrl = new CPQ_SSG_AGM_DocuSign_Controller(new ApexPages.StandardController(agmt));
						
			ApexPages.PageReference page = ctrl.sendToDocuSign();

        	System.assertEquals(null,page,'No redirect');

        	ApexPages.Message[] msgs = ApexPages.getMessages();
        	System.assertNotEquals(0,msgs.size());

        	Boolean errorThrown = false;
            for(ApexPages.Message msg : msgs){
                if(msg.getSeverity() == ApexPages.Severity.WARNING){
                    errorThrown = true;
					System.debug('Message: ' + msg.getSummary());
                }
            }
	        System.assert(errorThrown);

		Test.stopTest();

	}

	@isTest static void Test_CPQ_SSG_AGM_DocuSign_Controller_No_Generated_Document() {

		createTestData();

		Test.startTest();

			// Smart Cart
			Apttus__APTS_Agreement__c agmt = testAgreements[2];
			Task tsk = [SELECT Id FROM Task WHERE WhatId = :agmt.Id];
			delete tsk;

        	PageReference pageRef = new PageReference(pageName);
        	pageRef.getParameters().put('id',agmt.id);
        	Test.setCurrentPage(pageRef);

			CPQ_SSG_AGM_DocuSign_Controller ctrl = new CPQ_SSG_AGM_DocuSign_Controller(new ApexPages.StandardController(agmt));
			ApexPages.PageReference page = ctrl.sendToDocuSign();

        	System.assertEquals(null,page,'No redirect');

        	ApexPages.Message[] msgs = ApexPages.getMessages();
        	System.assertNotEquals(0,msgs.size());

        	Boolean errorThrown = false;
            for(ApexPages.Message msg : msgs){
                if(msg.getSeverity() == ApexPages.Severity.WARNING){
                    errorThrown = true;
					System.debug('Message: ' + msg.getSummary());
                }
            }
	        System.assert(errorThrown);

		Test.stopTest();

	}

    @isTest static void Test_CPQ_SSG_AGM_DocuSign_Controller_Scrub_PO_Hosted() {

		createTestData();

		Test.startTest();

			Apttus__APTS_Agreement__c agmt = testAgreements[1];
        	PageReference pageRef = new PageReference(pageName);
        	pageRef.getParameters().put('id',agmt.id);
        	pageRef.getParameters().put('isHosted','true');
        	Test.setCurrentPage(pageRef);

			CPQ_SSG_AGM_DocuSign_Controller ctrl = new CPQ_SSG_AGM_DocuSign_Controller(new ApexPages.StandardController(agmt));
			ApexPages.PageReference page = ctrl.sendToDocuSign();

        	System.assertNotEquals(null,page,'No redirect');
        	System.debug('Scrub PO Hosted - Page Parameters: ' + page.getParameters());
        	Map<String,String> params = getParams(page.getURL());
	        System.assertEquals(agmt.Id,params.get('SourceID'),'No match for SourceID');
        	System.assert(params.get('CRL').contains('SignInPersonName'),'SignInPersonName not found');
        	System.assert(params.get('CRL').contains(UserInfo.getUserEmail()),'User email not found');
        	System.assertEquals('0',params.get('STB'),'Should not show \'Next\' button');

		Test.stopTest();

	}

    @isTest static void Test_CPQ_SSG_AGM_DocuSign_Controller_Scrub_PO_Remote() {

		createTestData();

		Test.startTest();

			Apttus__APTS_Agreement__c agmt = testAgreements[1];
        	PageReference pageRef = new PageReference(pageName);
        	pageRef.getParameters().put('id',agmt.id);
        	Test.setCurrentPage(pageRef);

			CPQ_SSG_AGM_DocuSign_Controller ctrl = new CPQ_SSG_AGM_DocuSign_Controller(new ApexPages.StandardController(agmt));
			ApexPages.PageReference page = ctrl.sendToDocuSign();

        	System.assertNotEquals(null,page,'No redirect');
        	System.debug('Scrub PO Remote - Page Parameters: ' + page.getParameters());
        	Map<String,String> params = getParams(page.getURL());
	        System.assertEquals(agmt.Id,params.get('SourceID'),'No match for SourceID');
        	System.assert(!params.get('CRL').contains('SignInPersonName'),'SignInPersonName found');
        	System.assert(params.get('CRL').contains(UserInfo.getUserEmail()),'User email not found');

		Test.stopTest();

	}

    @isTest static void Test_CPQ_SSG_AGM_DocuSign_Controller_Smart_Cart() {

		createTestData();

		Test.startTest();

			Apttus__APTS_Agreement__c agmt = testAgreements[2];
        	PageReference pageRef = new PageReference(pageName);
        	pageRef.getParameters().put('id',agmt.id);
        	Test.setCurrentPage(pageRef);

			CPQ_SSG_AGM_DocuSign_Controller ctrl = new CPQ_SSG_AGM_DocuSign_Controller(new ApexPages.StandardController(agmt));
			ApexPages.PageReference page = ctrl.sendToDocuSign();

        	System.assertNotEquals(null,page,'No redirect');
        	System.debug('Smart Cart Remote - Page Parameters: ' + page.getParameters());
        	Map<String,String> params = getParams(page.getURL());
	        System.assertEquals(agmt.Id,params.get('SourceID'),'No match for SourceID');
        	System.assert(!params.get('CCTM').contains('3'),'Too many recipients');
        	System.assert(params.get('CCTM').contains('2'),'Too few recipients');
        	System.assert(params.get('CRL').contains(UserInfo.getUserEmail()),'User email not found');

		Test.stopTest();

	}

    @isTest static void Test_CPQ_SSG_AGM_DocuSign_Controller_Custom_Kit() {

		createTestData();

		Test.startTest();

			Apttus__APTS_Agreement__c agmt = testAgreements[3];
        	PageReference pageRef = new PageReference(pageName);
        	pageRef.getParameters().put('id',agmt.id);
        	Test.setCurrentPage(pageRef);

			CPQ_SSG_AGM_DocuSign_Controller ctrl = new CPQ_SSG_AGM_DocuSign_Controller(new ApexPages.StandardController(agmt));
			ApexPages.PageReference page = ctrl.sendToDocuSign();

        	System.assertNotEquals(null,page,'No redirect');
        	System.debug('Custom Kit - Page Parameters: ' + page.getParameters());
        	Map<String,String> params = getParams(page.getURL());
	        System.assertEquals(agmt.Id,params.get('SourceID'),'No match for SourceID');
        	System.assert(params.get('CCTM').contains('1'),'No recipients');
        	System.assert(!params.get('CCTM').contains('2'),'Too many recipients');
        	System.assert(!params.get('CRL').contains(UserInfo.getUserEmail()),'User email found');

		Test.stopTest();

	}



}