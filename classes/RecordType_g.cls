/*

MODIFICATION HISTORY
====================
Date      Id   Initials   Jira(s)    Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
2017.02.09      IL         AV-280     Created
*/

public class RecordType_g extends sObject_g
{
	public RecordType_g()
	{
		super(RecordType_cache.get());
	}

	////
	//// cast
	////
	public static Map<Id, RecordType> cast(Map<Id, sObject> sobjs)
	{
		return new Map<Id, RecordType>((List<RecordType>)sobjs.values());
	}

	public static Map<object, Map<Id, RecordType>> cast(Map<object, Map<Id, sObject>> source)
	{
		Map<object, Map<Id, RecordType>> target = new Map<object, Map<Id, RecordType>>();
		for(object key : source.keySet())
			target.put(key, cast(source.get(key)));

		return target;
	}

	////
	//// fetch
	////
	public Map<Id, RecordType> fetch()
	{
		return cast(this.cache.fetch());
	}

	public RecordType fetch(Id id)
	{
		return (RecordType)this.cache.fetch(id);
	}

	public Map<Id, RecordType> fetch(Set<Id> ids)
	{
		return cast(this.cache.fetch(ids));
	}

	public Map<object, Map<Id, RecordType>> fetch(sObjectField index)
	{
		return cast(this.cache.fetch(index));
	}

	public Map<Id, RecordType> fetch(sObjectField index, object value)
	{
		return cast(this.cache.fetch(index, value));
	}

	////
	//// fetch sets
	////
	public Map<object, Map<Id, RecordType>> fetch(sObjectField index, set<object> values)
	{
		return cast(this.cache.fetch(index, values));
	}

	////
	//// Additional methods
	////
}