@isTest
public class cpqProductClassification_uTest
{
	@testVisible
	static Map<Id, Apttus_Config2__ProductClassification__c> loadData(Map<Id, Set<Id>> product2classifications)
	{
		List<Apttus_Config2__ProductClassification__c> toUpsert = 
			new List<Apttus_Config2__ProductClassification__c>();
		
		Map<Id, integer> productSequence = new Map<Id, integer>();
		
		for(Id productId : product2classifications.keySet())
		{
			for(Id classificationId : product2classifications.get(productId))
			{
				if (!productSequence.containsKey(classificationId))
					productSequence.put(classificationId, 1);
				
				integer sequence = productSequence.get(classificationId);
				toUpsert.add(
					new Apttus_Config2__ProductClassification__c
					(
						Apttus_Config2__ProductId__c = productId,
						Apttus_Config2__ClassificationId__c = classificationId,
						Apttus_Config2__Sequence__c = sequence++
					)
				);
				
				productSequence.put(classificationId, sequence);
			}
		}
		
		upsert toUpsert;
		
		return new Map<Id, Apttus_Config2__ProductClassification__c>(toUpsert);
	}    
}