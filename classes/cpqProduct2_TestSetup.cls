public class cpqProduct2_TestSetup
{
	static cpq_TestSetup setup = new cpq_TestSetup();

    public static List<Product2> generateProducts ()
    {
        List<Product2> products = new List<Product2>();

        if (cpq_TestSetup.org == OrganizationNames_g.Abbreviations.SSG)
        {
            products.add(new Product2(
                ProductCode = 'TestProduct',
                Name = 'Test Product',
                Apttus_Surgical_Product__c = true,
                Category__c = 'Test'
            ));
        }
        else
            products = generateProducts(5);
        
        return products;
    }

    public static List<Product2> generateProducts (Integer quantity)
    {
        List<Product2> products = new List<Product2>();

        for (Integer i=0;i<quantity;i++)
        {
            Product2 p = new Product2();
            p.ProductCode = 'TestProduct'+i;
            p.Name = 'Test Product ' + i;
            p.Category__c = 'Test';
            p.Apttus_Surgical_Product__c = (cpq_TestSetup.org == OrganizationNames_g.Abbreviations.SSG) ? true : false;
            products.add(p);
        }
        
        return products;
    }

    public static List<Product2> create()
    {
    	OrganizationNames_g.Abbreviations RMS = OrganizationNames_g.Abbreviations.RMS;
    	
    	List<Product2> productList = new List<Product2>();

		productList.add(cpqProduct2_c.create(RMS, 'HARDWARE', 1));
		productList.add(cpqProduct2_c.create(RMS, 'SENSOR', 1));
		productList.add(cpqProduct2_c.create(RMS, 'SENSOR', 2));
		productList.add(cpqProduct2_c.create(RMS, 'SENSOR', 3));

		return productList;
    }
}