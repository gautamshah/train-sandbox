@isTest
public class Profile_cTest
{
	
    @isTest
    static void fetch()
    {
    	List<Profile> profileList = Database.query('select id,name,description,userlicenseid,usertype from profile');
    	String myString = profileList.get(0).name;
    	
        Profile p = Profile_c.fetch(myString);
        System.assertEquals(p.name, myString);
    	
    }
    
}