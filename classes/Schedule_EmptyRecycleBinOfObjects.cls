global class Schedule_EmptyRecycleBinOfObjects implements Schedulable{
/****************************************************************************************
    * Name    : Schedule_EmptyRecycleBinOfObjects
    * Author  : Brajmohan Sharma
    * Date    : 17-Feb-2017
    * Purpose : Performs a deletion job to empty the recycle bin of the given object.
    * 
    * Dependancies: Need to send object name while calling the class
    *       
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    *
    ****************************************************************************************/ 
public String ObjectArray = 'account,Contact,Territory_Quota__c,Seller_Quota__c,Sales_History_CA__c,ERP_Account__c,Sales_Transaction__c,Sales_History__c,Demo_Product__c';
String[] arrTest = ObjectArray .split(',');

  global void execute(SchedulableContext sc)
  {
  for(integer i=0;i<arrTest.size();i++){
    BatchEmptyRecycleBinOfObjects batchEmptyRB = new BatchEmptyRecycleBinOfObjects(arrTest[i]);
    Database.executeBatch(batchEmptyRB);
  }
  }
  
  
}