public with sharing virtual class dmGroup extends dm implements Comparable
{
    //
    // Class Description
    //
    // The dmGroup class is a wrapper class that contains the information
    // about the dmGroup__c and Group object that it represents.
    // dmGroup 1 - 1 dmGroup__c 1 - 1 Group
    // 
    // dmGroup <- dm
    //    record (dmGroup__c)
    //    sobj (Group)
    //
    // It can be called from Apex, Trigger, or VF component of page
    //
	
	//
	// TRIGGER METHODS (BULKIFIED)
	//
    public static void main(
            boolean isExecuting,
            boolean isInsert,
            boolean isUpdate,
            boolean isDelete,
            boolean isBefore,
            boolean isAfter,
            boolean isUndelete,
            List<dmGroup__c> newList,
            Map<Id, dmGroup__c> newMap,
            List<dmGroup__c> oldList,
            Map<Id, dmGroup__c> oldMap,
            integer size
        )
    {
        //if (size == 0) return; // There's nothing to process
        
        if (isBefore)
        {
            if (isInsert)
            {
                dm.onInsert(newList);
            }
        }

        if (isAfter)
        {
            if (isInsert)
            {	
            	AfterInsert(newList);
            }
            else if (isUpdate)
            {
	            update dmGroup.updateSObjects(newList);
            }
            else if (isDelete)
            {
            	// Because we can't pass sObjects to methods
            	// annotated with @future - anyway, we only need
            	// the list of GUIDs
                dmGroup.deleteSObjects(dm.extractGUIDs(oldList));
            }
        }
    }

	public static void AfterInsert(List<dmGroup__c> newList)
	{
		Map<dmGroup__c, Group> Record2SObject = createRecord2SObject(newList);
        insert Record2SObject.values();
	    List<dmGroup__Share> Shares = createAdminSharesForRecords(newList);
	    insert Shares;
	}
	
	//
	// CONSTRUCTORS
	//
    public dmGroup() { }
    
    public dmGroup(ApexPages.StandardSetController controllerSet)
    {
        super(controllerSet);
    }
    
    public dmGroup(ApexPages.StandardController controller)
    {
        super(controller);
    }
    
    //
    // PROPERTIES
    //
    public dmGroup__c record
    {
        get { return (dmGroup__c)base; }     
        set { base = (dmGroup__c)value; }
    }

	//
	// underlying sObject
	//
	public Group sobj
	{
		get { return (sobj == null) ? fetchSObject(record.Id) : sobj; }
		private set;
	}		
	
	//
	// COLLECTION METHODS (BULKIFIED)
	//

	// CREATE
	//
	public static List<dmGroup__c> createAdminRecords(List<dmFolder__c> applications)
	{
		List<dmGroup__c> groups = new List<dmGroup__c>();
		
		if (applications == null) return groups;
		
		for(dmFolder__c f : applications)
			groups.addAll(createAdminRecords(f));
			
		return groups;
	}
	
	//
	// FETCH
	//
	public Map<Id, List<dmGroup>> fetchObjectId2Objects(Map<Id, Id> ObjectId2ApplicationId)
	{
		Map<Id, List<dmGroup>> targets = new Map<Id, List<dmGroup>>();
		
		if (ObjectId2ApplicationId == null) return targets;

		// Start by fetching the Groups created for an Application
		Set<Id> appIds = new Set<Id>(ObjectId2ApplicationId.values());
		Map<Id, List<dmGroup>> ApplicationId2Records = fetchApplicationId2Objects(appIds);

		Set<string> objShares = new Set<string>();
		for(Id objId : ObjectId2ApplicationId.keySet())
			objShares.add(getObjectShareType(objId));
		
		// Then fetch the Group sObjects from each of the Objects' __Share object
		List<sObject> shares = new List<sObject>();
		Set<Id> objIds = ObjectId2ApplicationId.keySet();
		for(string objShare : objShares)
		{
			string query = 'SELECT ParentId, UserOrGroupId FROM ' + objShare + ' WHERE ParentId IN :objIds';
			shares.addAll(Database.query(query));
		}
		
		Map<Id, Set<Id>> ParentId2UserOrGroupIds = new Map<Id, Set<Id>>();
		for (sObject share : shares)
		{
			Id groupId = (Id)share.get('UserOrGroupId');
			Id parentId = (Id)share.get('ParentId');
			
			if (groupId != null && parentId != null)
			{
				if (!ParentId2UserOrGroupIds.containsKey(parentId))
					 ParentId2UserOrGroupIds.put(parentId, new Set<Id>());
					 
				ParentId2UserOrGroupIds.get(parentId).add(groupId);
			}
		}
		
		for(Id objectId : ObjectId2ApplicationId.keySet())
		{
			boolean isGroup = getObjectShareType(objectId) == 'dmGroup__Share';
			List<dmGroup> groups = ApplicationId2Records.get(ObjectId2ApplicationId.get(objectId));
			List<dmGroup> filtered = new List<dmGroup>();
			for(dmGroup g : groups)
			{
				if (!(isGroup && g.record.isAdministrator__c != true))
				{
					if (ParentId2UserOrGroupIds.get(objectId).contains(g.sobj.Id))
						g.assigned = true;
					filtered.add(g);
				}
			}
			
			targets.put(objectId, filtered);
		}
		return targets;
	}

	//
	// FETCH
	// Return a Map of FolderId 2 List of dmGroup from a Set of Folder Ids
	//
	// BULKIFIED
	public static Map<Id, List<dmGroup>> fetchApplicationId2Objects(List<sObject> applications)
	{
		Set<Id> applicationIds = new Set<Id>();
		for(sObject obj : applications)
			applicationIds.add(obj.Id);
			
		return fetchApplicationId2Objects(applicationIds);
	}
	
	//
	// FETCH
	// Return a Map of FolderId 2 List of dmGroup from a Set of Folder Ids
	//
	// BULKIFIED
	private static Map<Id, List<dmGroup>> fetchApplicationId2Objects(Set<Id> applicationIds)
	{
		Map<Id, List<dmGroup>> targets = new Map<Id, List<dmGroup>>();
		
		if (applicationIds == null) return targets;
		
		Map<Id, List<dmGroup__c>> ApplicationId2Records = fetchApplicationId2Records(applicationIds);
		Set<string> RecordGUIDs = new Set<string>();
		
		for(Id recordId : ApplicationId2Records.keySet())
			for(dmGroup__c record : ApplicationId2Records.get(recordId))
				RecordGUIDs.add(record.GUID__c);

		Map<string, Group> GUID2SObject = fetchSObjects(RecordGUIDs);
		
		// For each Application, create a List of dmGroup and associate it with the
		// dmGroup__c object and Group object
		// Application 1 - M dmGroup 1 - 1 dmGroup__c & 1 - 1 Group
		for(Id appId : applicationIds)
		{
			if (!targets.containsKey(appId))
				targets.put(appId, new List<dmGroup>());
			
			for(dmGroup__c record : ApplicationId2Records.get(appId))
			{
				dmGroup target = new dmGroup();
				target.record = record;
				target.sobj = GUID2SObject.get(record.GUID__c);
			
				targets.get(appId).add(target);
			}
		}
		return targets;
	}

	//
	// FETCH
	//
	// BULKIFIED

    public static List<dmGroup__c> fetch(Set<Id> recordIds)
    {
    	List<dmGroup__c> targets = new List<dmGroup__c>();
    	
		if (recordIds != null && recordIds.size() > 0)
			for(dmGroup__c g : dmGroups.All.values())
				if (recordIds.contains(g.Id))
					targets.add(g);

		return targets;
    }
	
	public static dmGroup__c fetch(Id id)
	{
		return dmGroups.All.get(id);
	}
	
	//
	// FETCH
	// Return Application Id 2 Records
	//
	// BULKIFIED
	private static Map<Id, List<dmGroup__c>> fetchApplicationId2Records(Set<Id> applicationIds)
	{
		Map<Id, List<dmGroup__c>> targets = new Map<Id, List<dmGroup__c>>();
		
		List<dmGroup__c> groups = new List<dmGroup__c>();
		
		if (applicationIds != null && applicationIds.size() > 0)
			for(dmGroup__c g : dmGroups.All.values())
				if (applicationIds.contains(g.Application__c))
					groups.add(g);
			 
		for(dmGroup__c g : groups)
		{
			if (!targets.containsKey(g.Application__c))
				targets.put(g.Application__c, new List<dmGroup__c>());
				
			targets.get(g.Application__c).add(g);
		}

		return targets;
	}

	//
	// SHARING
	//
	//
	// CREATE
	//
	private static List<dmGroup__Share> createAdminSharesForRecords(List<dmGroup__c> records)
	{
		List<dmGroup__Share> shares = new List<dmGroup__Share>();
		
		if (records == null || records.size() == 0) return shares;
		
		for(dmGroup__c g : records)
		{
			for(dmGroup__c admin : dmGroups.ApplicationId2Groups.get(g.Application__c))
			{
				Group s = dmGroups.GUID2sObject.get(admin.GUID__c);
				dmGroup__Share share = new dmGroup__Share();
			
				if (s.Name.endsWith(' - View'))
					share.AccessLevel = 'Read';
				else
					share.AccessLevel = 'Edit';
					
				share.put('ParentId', g.Id);
				share.put('UserOrGroupId', s.Id);
				
				shares.add(share);
			}
		}
		return shares;
	}
	
	public static void deleteSharing()
	{
		List<dmGroup__Share> shares = new List<dmGroup__Share>();
		
		for (dmGroup__Share share : dmGroups.Shares.values())
			if (share.AccessLevel == 'Read' || share.AccessLevel == 'Edit')
				shares.add(share);

		delete shares;
	}		
 
	@future
	public static void fixSharing()
	{
    	string query = dm.buildSOQL(dmGroup__c.getSObjectType(), null, null);
    	system.debug(query);
		
		List<dmGroup__c> g = dmGroups.All.values();
		system.debug(g);

	    List<dmGroup__Share> shares = createAdminSharesForRecords(g);
	    insert shares;
	}
	
	//
	// RECORD METHODS
	//
	// Need the Id and Name of the Application folder
	public static List<dmGroup__c> createAdminRecords(dmFolder__c application)
	{
		List<dmGroup__c> groups = new List<dmGroup__c>();
		
		if (application == null) return groups;
		
  		dmGroup__c target = new dmGroup__c();
  		target.isAdministrator__c = true;
  		target.Application__c = application.Id;
  		target.Name = application.Name.left(Math.min(40, application.Name.length()));
		groups.add(target);  			
  			
  		target = new dmGroup__c();
  		target.isAdministrator__c = true;
  		target.Application__c = application.Id;
  		target.Name = application.Name.left(Math.min(33, application.Name.length())) + ' - View';
		groups.add(target);

		return groups;
	}
	
    @testVisible
    private string getObjectShareType(Id objectId)
    {
 		return objectId.getSObjectType().getDescribe().Name.remove('[').remove(']').replace('__c', '__Share');
    }
    
    @testVisible
    private sObject createObjectShareRecord(Id objectId, Id sobjectId)
    {
    	string objectName = getObjectShareType(objectId);
    	sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
    	sObj.put('ParentId', objectId);
    	sObj.put('UserOrGroupId', sobjectId);
    	sObj.put('AccessLevel', 'Read');
    	
    	return sObj;
    }
    
    @testVisible
    private List<sObject> selectObjectShareRecord(Id objectId, Id sobjectId)
    {
    	string query = 'SELECT Id FROM ' + getObjectShareType(objectId) + ' WHERE ParentId = \'' + objectId + '\' AND UserOrGroupId = \'' + sobjectId + '\'';
    	return Database.query(query);
    }
	
	// If they are not both Administrator or both non-Administrator, make the Administrator first
	// If they are the same, then sort within that group - will end up with List of Sorted
	// Administrators first, then Sorted non-Administrators
	public integer compareTo(Object obj)
	{
		dmGroup target = (dmGroup)obj;
		
		if (this.record.isAdministrator__c == target.record.isAdministrator__c)
		{
	    	if (this.record.Name > target.record.Name)
		        return 1;
	    	else if (this.record.Name == target.record.Name)
	    		return 0;
	    	else
	    		return -1;
		}
		
		return (this.record.isAdministrator__c) ? -1 : 1;
    }

	//
	// VISUAL FORCE PROPERTIES & METHODS
	//
    // Properties & Methods that support the VF component or page
    //
   	public boolean assigned { get; set; }
   	public boolean locked
   	{
   		get { return this.record.isAdministrator__c; }
   		set;
   	}

	public Id ApplicationId
	{
		get
		{
			return record.Application__c;
		}
	}
	
    public List<dmGroup> RelatedGroups
    {
    	get
    	{
	   		runAsSystem ras = new runAsSystem(parameters);
	   		return ras.RelatedGroups;
    	}
    }
    
    public void AssignGroup()
    {
    	runAsSystem ras = new runAsSystem(selected);
    	ras.AssignGroups();
    }

	public without sharing class runAsSystem extends dmGroup
	{
		private Parameters parameters;
		private Selected selected;
		
		public runAsSystem(Parameters p)
		{
			this.parameters = p;
		}
		
		public runAsSystem(Selected s)
		{
			this.selected = s;
		}
		
	    public List<dmGroup> RelatedGroups
	    {
	    	get
	    	{
	    		Map<Id, Id> ObjectId2ApplicationId = new Map<Id, Id>();
		    	ObjectId2ApplicationId.put(parameters.objectId, parameters.applicationId);
	    		Map<Id, List<dmGroup>> groups = fetchObjectId2Objects(ObjectId2ApplicationId);
	    		List<dmGroup> objectGroups = new List<dmGroup>();
	    		objectGroups.addAll(groups.get(parameters.objectId));
	    		objectGroups.sort();
		   		return objectGroups;
	    	}
	    }
	    
	    public void AssignGroups()
	    {
	    	if (!selected.assigned)
	    	{
	    		List<sObject> sobjs = new List<sObject>{ createObjectShareRecord(selected.objectid, selected.sobjectid) };
	    		if (sobjs.size() > 0) AssignParent(selected.objectid, selected.sobjectId, sobjs);
	    		insert sobjs;
	    	}
	    	else
	    	{
		   		List<sObject> sobjs = new List<sObject>();
		   		sobjs.addAll(selectObjectShareRecord(selected.objectid, selected.sobjectid));
		   		if (sobjs.size() > 0) AssignChildren(new Set<Id>{ selected.objectId }, selected.sobjectId, sobjs);
	   			delete sobjs;
	    	}
	    }
	}
	
    public PageReference onLoad()
    {
    	string guid = querystring.get('sObject');
    	
    	if (guid != null) return viewSObject(guid);

    	if (record != null && record.Id != null)
    		record = fetch(record.Id);
    		
    	return null;
    }

    public PageReference view()
    {
        return (new ApexPages.StandardController(record)).view();
    }

    @testVisible
    private void AssignParent(Id objectId, Id sobjectId, List<sObject> parents)
    {
    	if (objectId == null || sobjectId == null) return;
    	
    	string objTypeName = objectId.getSObjectType().getDescribe().Name.remove('[').remove(']');
    	sObject record;

    	if (objTypeName == 'dmDocument__c')
    		record = dmDocuments.All.get(objectId);
    	else if (objTypeName == 'dmFolder__c')
    		record = dmFolders.All.get(objectId);
    	
    	if (record == null) return;
    	
    	Id parentId = (objTypeName == 'dmDocument__c') ? (Id)record.get('Folder__c') : (objTypeName == 'dmFolder__c') ? (Id)record.get('Parent__c') : null;
    	if (parentId != null && selectObjectShareRecord(parentId, sobjectid).size() == 0)
    	{
    		sObject obj = createObjectShareRecord(parentId, sobjectid);
    		parents.add(obj);
    	}
    	
    	AssignParent(parentId, sobjectId, parents);
    }
    
    @testVisible
    private void AssignChildren(Set<Id> objectId, Id sobjectId, List<sObject> children)
    {
    	if (objectId == null || objectId.size() == 0 || sobjectId == null || children == null) return;
    	
    	Map<Id, dmDocument__c> documents = new Map<Id, dmDocument__c>();
    	
    	for (dmDocument__c d : dmDocuments.All.values())
    		if (objectId.contains(d.Folder__c))
    			documents.put(d.Id, d);
    			
    	if (documents.size() > 0)
    	{
    		List<dmDocument__Share> docShares = new List<dmDocument__Share>();
    		
    		for (dmDocument__Share share : dmDocuments.Shares.values())
    			if (documents.keySet().contains(share.ParentId) && sobjectId == share.UserOrGroupId)
    				docShares.add(share);
    			       
			children.addAll((List<sObject>)docShares);
    	}
    	
    	Map<Id, dmFolder__c> folders = new Map<Id, dmFolder__c>();
    	
    	for(dmFolder__c f : dmFolders.All.values())
    		if (objectId.contains(f.Parent__c))
    			folders.put(f.Id, f);
    		 
    	if (folders.size() > 0)
    	{
    		List<dmFolder__Share> folderShares = new List<dmFolder__Share>();
    		
    		for (dmFolder__Share share : dmFolders.Shares.values())
    			if (folders.keySet().contains(share.ParentId) && sobjectId == share.UserOrGroupId)
    				folderShares.add(share);
    			       
			children.addAll((List<sObject>)folderShares);
			
			AssignChildren(folders.keySet(), sobjectId, children);
    	}
    }
    
    //
    // RELATED SOBJECT
    //
    // Static methods that maintain the underlying SF Group associated by
    // DeveloperName (dmGroup's GUID) or Id
    // These are bulkified
    
    //
    // CREATE
    //
    // BULKIFIED
    public static Map<dmGroup__c, Group> createRecord2SObject(List<dmGroup__c> records)
    {
    	Map<dmGroup__c, Group> targets = new Map<dmGroup__c, Group>();
    	
    	if (records == null) return targets;
	    	
		for (dmGroup__c source : records)
		{
        	Group g = new Group();
        	g.DeveloperName = source.GUID__c;
        	g.Name = source.Name;
        	g.DoesIncludeBosses = true;
        	g.DoesSendEmailToMembers = false;
	        
        	targets.put(source, g);
		}
		
        return targets;
    }
    
    //
    // UPDATE
    //
    // You can only update the Name of the Group to match
    // the name of the dmGroup
    //
    // BULKIFIED
    public static List<Group> updateSObjects(List<dmGroup__c> records)
    {
    	Map<string, Group> targets = new Map<string, Group>();

		if (records == null) return targets.values();
		
    	targets.putAll(fetchSObjects(records));

		for (dmGroup__c g : records)
			if (targets.containsKey(g.GUID__c))
				targets.get(g.GUID__c).Name = g.Name;

        return targets.values();
    }

	//
	// FETCH
	//
	public static Group fetchSObject(string GUID)
	{
		Map<string, Group> groups = fetchSObjects(new Set<string>{ GUID });
		return (groups != null && groups.size() == 0) ? null : groups.values()[0];
	}
	
	public static Map<string, Group> fetchSObjects(Set<string> GUIDs)
	{
        Map<string, Group> groups = new Map<string, Group>();

    	if (GUIDs != null)
    	{
	        for (Group g : dmGroups.sObjects.values())
    	    	if (GUIDs.contains(g.DeveloperName))
        			groups.put(g.DeveloperName, g);
    	}

		return groups;
	}    

	// BULKIFIED
    public static Map<string, Group> fetchSObjects(List<dmGroup__c> groups)
    {
    	return fetchSObjects(dm.extractGUIDs(groups));
    }

    //
    // DELETE
    //
    // BULKIFIED
    @future
    public static void deleteSObjects(Set<string> GUIDs)
    {
        Map<string, Group> sObjects = fetchSObjects(GUIDs);
        if (sObjects != null)
			delete sObjects.values();
    }
    
    //
    // VIEW
    //
    public PageReference viewSObject()
    {
    	return viewSObject(querystring.get('sObject'));
    }
    
    public PageReference viewSObject(string guid)
    {
        Group g = fetchSObject(guid);
        
        if (g != null)
	        return new ApexPages.StandardController(g).view();
	    else
	    	return null;
    }
}