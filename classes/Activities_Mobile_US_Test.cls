@isTest
   private class Activities_Mobile_US_Test {
   static testMethod void TestPageFunctionality() {

    RecordType ce = [SELECT Id FROM RecordType where SobjectType = 'Contact' and RecordType.DeveloperName = 'Covidien_Employee' LIMIT 1];
    RecordType hf = [SELECT Id FROM RecordType where SobjectType = 'Account' AND RecordType.DeveloperName = 'US_Account_Hospital' LIMIT 1];
    RecordType rt = [SELECT Id FROM RecordType where SobjectType = 'Contact' and DeveloperName = 'Affiliated_Contact_US' LIMIT 1];  
    RecordType ct = [SELECT Id FROM RecordType where SobjectType = 'Account' AND RecordType.DeveloperName = 'Country_Accounts' LIMIT 1];
    
    String ReccType = [SELECT Id FROM RecordType where SobjectType = 'Event' LIMIT 1].Id;
    Integer PageLoadCount = 1;
    string Searchterm = 'Test';
       
       
    
       
    //Create Account
    Account act = new Account();
    act.RecordTypeId = hf.id;
    act.name='Test Account';
    act.billingPostalCode = '99999';
    act.billingCity = 'TestCity';
    act.billingState = 'MA'; 
    act.billingCountry = 'US'; 
    
    insert act;


       Contact con = new Contact();
       con.RecordTypeId = ce.id;
       con.FirstName = 'Bob';
       con.LastName = 'Smith';
       con.AccountId = act.id;
       
       insert con;   
       
       
       //Create New Opportunity
       Opportunity opp = new Opportunity();
          opp.name='Test Opp';
          opp.StageName = 'Identify';
          opp.Type = 'New Business';
          opp.AccountId = act.id; 
          opp.Closedate = System.Today(); 
       
       insert opp;       
       
       

        PageReference pageRef2 = Page.Activities_Mobile_US; 
        Test.setCurrentPageReference(pageRef2);
        
        Activities_Mobile_US am = new Activities_Mobile_US(); 
        
        
               // Set Variable Value


        am.ActivityType = 'Business Trip';
        am.Subject = 'My Test Event';
        am.selectedAccountGuidId = act.id; 
        am.selectedContactId = con.id;
        am.selectedContactName = con.FirstName;
        am.SelectedAccountName = act.Name;
        am.selectedOppId = '';
        am.selectedOppName = '';
        am.ReccType = ReccType;
        am.selectedNotes = 'Test Note';
        am.IndexNum = 0;
        am.OppIndexNum = 0;
        am.AccIndexNum = 0;
        am.searchTerm = 'Test';
        am.selectedAccountState = 'MA';
        am.selectedAccountCity = '';
        am.selectedExternalAccountId='';
        am.WhichAccounts = 'AllAccounts';
       
        am.getActivityTypeList();
        am.getOppStage();
        am.searchMain();
        am.searchOpp();
        am.searchOppNoRedir();
        am.ReviseSearch();
        am.Refresh();
        am.getItems();
        am.getStates();
        am.searchAccount();
        am.processActSelection();
        am.ValidationMsgReturn();
        am.processButtonClick();
        am.processOppSelection();
        am.EndDateCheck();
        am.StartDateCheck();
        am.WhichAccounts = 'AllAccounts';
        am.CreateEvent();
       am.ActivityType = '';
       am.CreateEvent();
          }
       }