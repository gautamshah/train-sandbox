@isTest
private class Test_CPQ_SSG_DM_Batch_Deal 
{
    @testSetup static void setupData() {
        // Create account
        Account acc=new Account();
        acc.Name='Deal_Account';
        acc.Account_External_ID__c='US-342881';
        insert acc;

        // Create Product Records
        Product2 p2=new product2();
        p2.Name='Electrosurgery COT';
        p2.Apttus_Product_External_ID__c='1649.000000000000000';
        insert p2;
        
        // Create Price List Records
        Apttus_Config2__PriceList__c acpList = cpqPriceList_c.SSG;
        acpList.Price_List_External_ID__c='1649.000000000000000';
        update acpList;
        
        // Create User information
        Profile p= new Profile();
        p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduser@testorg.com';
        u.EmailEncodingKey='UTF-8';
        u.LastName='kimberly';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US';
        u.ProfileId =p.Id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='deal.statera@statera.com';
        insert u;
        
        // Create CPQ DM Variable
        CPQ_DM_Variable__c cpqDMVar=new CPQ_DM_Variable__c();
        cpqDMVar.Value__c=u.Id;
        cpqDMVar.Type__c='User';
        cpqDMVar.Name='StateraDeal';
        insert cpqDMVar;
        System.debug('cpqDMVar*****'+cpqDMVar);

        // Create CPQ SSG STG Deal
        CPQ_SSG_STG_Deal__c cpqStgDeal=new CPQ_SSG_STG_Deal__c();
        cpqStgDeal.STG_AccountNumber__c='342881';
        cpqStgDeal.STG_CreatedByName_c__c='StateraDeal';
        cpqStgDeal.STG_DealId__c='1649';
        cpqStgDeal.STG_EffectiveDate__c=Date.Today()+50;
        cpqStgDeal.STG_Status__c='Closed - Lost';
        cpqStgDeal.STG_ASTCommitmentFlag__c=True;
        cpqStgDeal.STG_DealDuration__c=2;
        cpqStgDeal.STG_Process__c = true;
        insert cpqStgDeal;

        // Create Opportunity
        Opportunity opp=new Opportunity();
        opp.Name='COT Opportunity';
        opp.AccountId=acc.Id;
        opp.Capital_Disposable__c='Capital';
        opp.Type='New Custome';
        opp.Financial_Program__c='Advanced Tech Bridge';
        opp.Promotion_Program__c='Capnography Customer Care- PM';
        opp.CloseDate=Date.Today()+50;
        opp.StageName='Draft';
        insert opp;
    }

    static testmethod void Deal_Method1() {
        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal = new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method2() {
        // Update CPQ SSG STG Deal
        Opportunity opp = [Select Id From Opportunity];
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Closed - Won';
        cpqStgDeal.STG_ASTCommitmentFlag__c=False;
        cpqStgDeal.STG_OpportunityId__c=opp.Id;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method3() {
        // Update CPQ SSG STG Deal
        Opportunity opp = [Select Id From Opportunity];
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Assigned to';
        cpqStgDeal.STG_OpportunityId__c=opp.Id;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method4() {
        // Update CPQ SSG STG Deal
        Opportunity opp = [Select Id From Opportunity];
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Rejected';
        cpqStgDeal.STG_OpportunityId__c=opp.Id;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method5() {
        // Update CPQ SSG STG Deal
        Opportunity opp = [Select Id From Opportunity];
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='DELETED';
        cpqStgDeal.STG_OpportunityId__c=opp.Id;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method6() {
        // Update CPQ SSG STG Deal
        Opportunity opp = [Select Id From Opportunity];
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Draft';
        cpqStgDeal.STG_OpportunityId__c=opp.Id;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method7() {
        // Update CPQ SSG STG Deal
        Opportunity opp = [Select Id From Opportunity];
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Approved';
        cpqStgDeal.STG_OpportunityId__c=opp.Id;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method8() {
        // Update CPQ SSG STG Deal
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Closed - Won';
        cpqStgDeal.STG_ASTCommitmentFlag__c=False;
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }

    static testmethod void Deal_Method9() {
        // Update CPQ SSG STG Deal
        CPQ_SSG_STG_Deal__c cpqStgDeal = [Select Id From CPQ_SSG_STG_Deal__c Where STG_DealId__c = '1649'];
        cpqStgDeal.STG_Status__c='Approved';
        update cpqStgDeal;

        Test.startTest();
            CPQ_SSG_DM_Batch_Deal cpqDeal=new CPQ_SSG_DM_Batch_Deal();
            Database.executebatch(cpqDeal);
        Test.stopTest();
    }
}