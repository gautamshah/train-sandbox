/**
Item Wrapper class used in CPQ_Controller_ProposalDistributor_Config and CPQ_Controller_ProposalCustomer_Config

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11-26      Yuli Fintescu       Created
2016-08-24		Isaac Lewis			Added implementation/licensed location designation flags for Vital Sync products
===============================================================================
*/
public with sharing class CPQ_FacilityItem implements Comparable {
	public final static String INPUT_COLOR = 'rgb(250, 255, 189)';

	public Account account {get; set;}
    public Boolean checked {get; set;}
    public Boolean disabled {get; set;}
	public Boolean implementationLocation {get; set;}
	public Boolean licensedLocation {get; set;}
    public String bgColor {get; set;}

    public CPQ_FacilityItem() {
    	checked = false;
    	disabled = false;
		implementationLocation = false;
		licensedLocation = false;
    }

    public PageReference doGotoAccount() {
    	return new PageReference ('/' + account.ID);
    }

	public Integer compareTo(Object o) {
		CPQ_FacilityItem comp = (CPQ_FacilityItem)o;

		if (account.Name > comp.account.Name)
			return 1;
		else if (account.Name != comp.account.Name)
			return -1;

		if (account.Account_External_ID__c > comp.account.Account_External_ID__c)
			return 1;
		else if (account.Account_External_ID__c != comp.account.Account_External_ID__c)
			return -1;

		return 0;
	}

	public CPQ_FacilityItem copy() {
		CPQ_FacilityItem f = new CPQ_FacilityItem();
		f.account = account; //.clone(true, false, true, true);

		return f;
	}
}