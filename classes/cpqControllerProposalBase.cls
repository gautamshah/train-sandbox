public abstract class cpqControllerProposalBase 
{

	protected  CPQ_Config_Setting__c SystemProperties = cpqConfigSetting_c.SystemProperties;
	
    public string Name;
    @testVisible private static FINAL String SEPARATOR = '::';
    @testVisible private static FINAL String CRLF = '\r\n';

  	private List<String> cotHierarchy
  	{
        get { return SystemProperties.Participating_Facility_COTs__c != NULL ?
        		SystemProperties.Participating_Facility_COTs__c.toUpperCase().split(CRLF) : new List<String>(); }
    }
  
  	 /*
  	  turn these options:
  	  ---------------
  	  -003
	  ::07H
	
      ----------------
 		+All  WHERE (id != null)
	    -003  AND (Account__r.Class_of_Trade__c != '003'
		::07H OR (Account__r.secondary_Class_of_Trade__c != '07H') )

      into this:
      ----------------
		(
		  (ID != null)
		  OR
		  (Account__r.Class_of_Trade__c != '003')
		  OR
		  (Account__r.Class_of_Trade__c = '003' AND Account__r.secondary_Class_of_Trade__c = '07H')
		)

	*/
	private List<cpqClassOfTrade_c> buildExpressions() 
	{
		
		List<cpqClassOfTrade_c> expressions = new List<cpqClassOfTrade_c>();
		cpqPrimaryCOT_c primary = null;
		
		for ( String row : cotHierarchy ) 
		{
 	
 	      if (row.toUpperCase().contains('ALL'))  
	 	  {
            cpqClassOfTrade_c all = new cpqAll_c(row);
            expressions.add(all);
	 	     
	 	  }
		  //if does not include :: then it is primary
		  else if (!row.contains(SEPARATOR))	
		  {
		  	primary = new cpqPrimaryCOT_c(row);
		  	expressions.add(primary);
	
		  }
		  else
	  	  {
	  	  	//all secondaries are assumed to include the *inverse* of primary as part of the expression
	  	  	String val = row.replaceAll(SEPARATOR, '');
	  	  	cpqClassOfTrade_c secondary = new cpqSecondaryCOT_c(primary, val);
            expressions.add(secondary);
	  	  }

		}

		return expressions;
	}
	
	public String injectParticipatingFacilityQueryPredicate()
	{
		return injectParticipatingFacilityQueryPredicate(null);
	}
	
	public String injectParticipatingFacilityQueryPredicate(String obj) 
	{
		
		List<cpqClassOfTrade_c> expressions = buildExpressions();
		Integer counter = 0;
		
		String queryPredicate = ' ( ';
		for (cpqClassOfTrade_c cot : expressions)
		{
			counter++;
			queryPredicate += cot.buildParticipatingFacilityExpression(obj);
			if (counter < expressions.size())
			{
			   //if this is 'all' and it is include, then we use AND, otherwise it is OR
			   queryPredicate += (cot instanceof cpqAll_c && !cot.exclude ) ? ' AND ' : ' OR ';
			}
		}
		
		return queryPredicate + ' ) ';
		
	}
	
	//distributor is the inverse of participating facility 
	//https://en.wikipedia.org/wiki/De_Morgan's_laws
	public String injectDistributorQueryPredicate(String obj) 
	{

		List<cpqClassOfTrade_c> expressions = buildExpressions();
        Integer counter = 0;
      
		String queryPredicate = ' ( ';
		for (cpqClassOfTrade_c cot : expressions)
		{
			counter++;
			queryPredicate += cot.buildDistributorExpression(obj);
			if (counter < expressions.size())
            {
               queryPredicate += ' AND ';
            }
			
		}
		
		return queryPredicate + ' ) ';
		
	}
	
	public String injectDistributorQueryPredicate() 
    {
    	return injectDistributorQueryPredicate(null);
    }
    
}