@isTest(SeeAllData=true)
public class IncoPPDReports_Test 
{    
    @isTest
    static void doTest()
    {
        Test.startTest();
        IncoPPDReports ipr = new IncoPPDReports();
        ipr.sendCustomerReports();
        ipr.sendDistributorReports();
        PageReference pageRef = Page.IncoReportWrapper;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('acctID', [Select Id From Account Limit 1].Id);
        ApexPages.currentPage().getParameters().put('daysAgo', '7');
        ApexPages.currentPage().getParameters().put('recipient', 'customer');
        IncoReportWrapperController irwc = new IncoReportWrapperController();
        irwc.getRptData();
        ApexPages.currentPage().getParameters().put('recipient', 'distributor');
        irwc.getRptData();
        Test.stopTest();
    }
}