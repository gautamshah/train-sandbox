@isTest
public class ManageReportOfInActiveUserTest {
 /****************************************************************************************
    * Name    : ManageReportOfInActiveUserTest
    * Author  : Brajmohan Sharma
    * Date    : 23-Mar-2017
    * Purpose :
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/    
  @testSetup
  static void Case1InsertUser()
    {    
    Profile P = [select id from profile where name = 'system administrator'];
    String orgId = UserInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
    User tuser = new User(  firstname = 'testcase',
                            lastName = 'data',
                            email = uniqueName + '@test' + orgId + '.org',
                            Username = uniqueName + '@test' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = P.Id);
                            //UserRoleId = roleId);
    insert tuser;    
    }
    
    static testMethod void Case3InactiveUser(){
    List<User> id = [select id,isActive,Name from user where firstname = 'Sinem' and lastName = 'Mavi'];
    User u = id.get(0);
    u.IsActive = false;
    update u;
   List<User> idd = [select id,isActive,Name from user where firstname = 'Sinem' and lastName = 'Mavi'];
    User u1 = idd.get(0);
    u1.IsActive = true;
    update u1;
    }
}