<!--  
Copyright (c) 2011, salesforce.com, inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution.
    * Neither the name of the salesforce.com, Inc. nor the names of its contributors 
    may be used to endorse or promote products derived from this software 
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
-->
<apex:page controller="MPM4_BASE.Milestone1_GettingStartedController" sidebar="false" applyBodyTag="false" docType="html-5.0">  
<html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <apex:stylesheet value="{!URLFOR($Resource.MPM4_BASE__SLDS070, '/assets/styles/salesforce-lightning-design-system-vf.min.css')}" />    
    
</head>

    <apex:form >
    <div class="slds slds-p-around--xx-large slds-container--large slds-container--center slds-grid slds-grid--frame" role="main">    
    <apex:pageBlock title="{!$Label.mpm4_base__welcometomilestonespm}" rendered="{!settingsReady == false}">
    
    <apex:outputPanel id="mainpanel"> 
        <apex:outputPanel id="stepmain">    
            <h2>{!$Label.NextSteps}</h2>

            <p>{!$Label.YourNextFewSteps}</p>
        
            <p>1. {!$Label.InitializeYourConfiguration} &nbsp;
            <apex:outputPanel id="step1panel">
                <apex:outputPanel id="step1" rendered="{!settingsReady == false}">      
                    <apex:actionStatus id="executeStatus" >
                         <apex:facet name="start">
                             <apex:outputPanel id="executeStatusWorking" layout="none">
                                 <img src = "{!URLFOR($Resource.Milestone1_Resource_Bundle,'images/Milestone1_Load_Wheel_Img')}"/>
                             </apex:outputPanel>
                         </apex:facet>
                         <apex:facet name="stop"  >
                             <apex:outputPanel id="executeStatusNotworking" layout="none">
                                 <apex:commandButton action="{!setupStandardSettings}" value="{!$Label.mpm4_base__clickheretoinitializethisapp}" status="executeStatus" styleClass="slds-button slds-button--brand" rerender="step1panel,step2panel,mainpanel" />
                             </apex:outputPanel>
                         </apex:facet>
                    </apex:actionStatus>
                </apex:outputPanel>
            </apex:outputPanel> 

            </p>
        
            <p>2. {!$Label.ViewTheGettingStartedProject}</p>
            
            <p>3. {!$Label.StartCreatingYourFirstProject}</p>
        </apex:outputPanel> 
    </apex:outputPanel>

    <p>&nbsp;</p>
    </apex:pageBlock>

    <apex:pageBlock title="Milestones PM+ Video Playlist">
        <center>    
            <div id="videos" style="max-width:680px;"> 
            You may view more videos in the playlist by hovering your mouse over the video and clicking the icon in the top left corner. Or by <a href="https://www.youtube.com/watch?v=_5yF3ZDR7F4&list=PLfryc3zXHX0o10fbqZpGTurSNW4Q0zuDz" target="_blank">viewing the playlist on Youtube</a>.
                <div>
                    <apex:iframe width="672px" height="378px" src="https://www.youtube.com/embed/videoseries?list=PLfryc3zXHX0o10fbqZpGTurSNW4Q0zuDz&rel=0" frameborder="0" />
                </div>
            </div>
        </center> 
    </apex:pageBlock>

    <apex:pageBlock title="{!$Label.mpm4_base__importantupper}">
        <ul style="list-style-type: initial;">
                
                <li>Resources:
                    <ul style="list-style-type: initial;">      
                        <!--MPM+ Overview-->
                        <li><apex:outputText value="{!$Label.mpm4_base__basicfaq}" escape="false" /></li>  
                        <!--MPM+ Admin Guide-->
                        <li><apex:outputText value="{!$Label.mpm4_base__adminguide}" escape="false" /></li>    
                        <!--MPM+ Template Guide-->
                        <li><apex:outputText value="{!$Label.mpm4_base__templateguide}" escape="false" /></li> 
                        <!--MPM+ FAQ-->
                        <li><apex:outputText value="{!$Label.mpm4_base__enhancedfaq}" escape="false" /></li>
                        <!--community portal-->
                        <li><apex:outputText value="{!$Label.mpm4_base__forcommunityassistance}" escape="false" /></li>
                    </ul>
                </li>
                
                <li>Contacts:
                    <ul style="list-style-type: initial;">
                        <!--Contact Us-->
                        <li><apex:outputText value="{!$Label.mpm4_base__contactuspage}" escape="false" /></li> 
                        
                        <!--for assistance: Email support to log any defects-->
                        <li><apex:outputText value="{!$Label.mpm4_base__tosubmitissues}" escape="false" /></li> 
                        <!-- for enhancement ideas: Email support to log any enhancement requests -->
                        <li><apex:outputText value="{!$Label.mpm4_base__tosubmitideas}" escape="false" /></li>
                        <!--add-ons and accelerators-->
                        <li><apex:outputText value="{!$Label.mpm4_base__learnmore}" escape="false"></apex:outputText></li>
                    </ul>
                </li>
            </ul>
    </apex:pageBlock>
        
    <apex:pageBlock title="Update Resources" id="resourcesMain" rendered="{!OR(hasNullResourcesOnTasks, hasNullResourcesOnTimes)}">
        <apex:pageMessages />
        <apex:outputPanel >
            <h2>When upgrading from a version lower than 7.24, please consider updating the task assignments and the time incurred assignemnts. Version 7.24 has deprecated 
                the Assign To field on Tasks, and Version 7.36 has changed the Incurred By field on Time, both in favor of a more dynamic field called Resource, which allows assignments to 
                either a user (Premium or Free users) or a contact (Premium users only). To copy the existing "Assigned To" and/or "Incurred By" data to the new Resource fields, please use the button below.
                The process will first create resource records for each user if needed, then initiate a batch job to copy the values from the existing deprecated fields to the new 
                fields on Task and/or Time; this process will not delete any data.  
                
                <br/> <br/>
                Note:
                <li>  To prevent excessive resource update notifications being sent to email inboxes, it is highly recommended to 
                    check the "Disable All Resource Email Notifications" setting prior to executing the scripts.</li>
                
                <li> The process will create resource records for all users by default.
                    To only create resource records for users with Salesforce licenses (excluding others such as Community licenses),
                    please enable the "Only Create Resource For SF License" setting. </li>
                
                <li> <apex:commandLink action="{!goToCustomSetting}" target="_blank" style="color:blue">Go to the settings page</apex:commandLink> </li>
            </h2>
        </apex:outputPanel>
        <br/> 

        <apex:pageBlockSection title="Update Resources For Tasks" id="resourcesTask" rendered="{!hasNullResourcesOnTasks}" columns="1" collapsible="false">
            <apex:pageMessages />
            <apex:outputPanel >
                <h2>When upgrading from 7.24 or lower, please press execute below to update resource on tasks. Please feel free to leave this page, the job will run in the background. </h2>
                <center><apex:commandButton value="Execute on Tasks Only" action="{!assignResourcesForTasks}" rerender="resourcesTask" /></center>
            </apex:outputPanel>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Update Resources For Times" id="resourcesTime" rendered="{!hasNullResourcesOnTimes}" columns="1"  collapsible="false">
            <apex:pageMessages />
            <apex:outputPanel >
                <h2>When upgrading from 7.36 or lower, please press execute below to update resource on times. Please feel free to leave this page, the job will run in the background. </h2>
                <center><apex:commandButton value="Execute on Times Only" action="{!assignResourcesForTimes}" rerender="resourcesTime" /></center>
            </apex:outputPanel>
        </apex:pageBlockSection>

    </apex:pageBlock>
    </div>
    </apex:form>
</html>
</apex:page>