<apex:page standardController="vlc_pro__Action_Object_Create__c"
    extensions="vlc_pro.ActionObjectCreateCE">
    
    <apex:includeScript value="{!$Resource.vlc_pro__jQuery}" />
    <apex:includeScript value="{!$Resource.vlc_pro__formula_editor}" />
    <apex:stylesheet value="{!$Resource.vlc_pro__formula_editor_css}"/>

    <apex:sectionHeader title="{!$ObjectType.vlc_pro__Action_Object_Create__c.Label} {!$Label.vlc_pro__Label_Edit}"
        subtitle="{!vlc_pro__Action_Object_Create__c.Name}" />

    <apex:pageMessages />

    <apex:form >
        <apex:actionFunction name="changeObj" action="{!processObjChange}"
            immediate="false" />
            
        <apex:inputHidden value="{!vlc_pro__Action_Object_Create__c.vlc_pro__Object_Name__c}" id="selectedObject"/>

        <apex:pageBlock title="{!$ObjectType.vlc_pro__Action_Object_Create__c.Label} Details" mode="edit">
            <apex:pageBlockButtons >
                <apex:commandButton action="{!processEditSave}" value="{!$Label.vlc_pro__Button_Save}" />
                <apex:commandButton action="{!cancel}" value="{!$Label.vlc_pro__Button_Cancel}"
                    immediate="true" />
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="{!$ObjectType.vlc_pro__Process__c.Label} Details" showHeader="true"
                collapsible="false">
                <apex:outputField value="{!vlc_pro__Action_Object_Create__c.Action__r.vlc_pro__Process__c}" />
                <apex:outputField value="{!vlc_pro__Action_Object_Create__c.Action__r.Process__r.vlc_pro__Master_Object__c}" />
                <apex:outputField value="{!vlc_pro__Action_Object_Create__c.vlc_pro__Action__c}" />
                <apex:pageBlockSectionItem />
                <apex:outputField value="{!vlc_pro__Action_Object_Create__c.Action__r.vlc_pro__Before_State__c}" />
                <apex:outputField value="{!vlc_pro__Action_Object_Create__c.Action__r.vlc_pro__After_State__c}" />
            </apex:pageBlockSection>
            <apex:pageBlockSection title="{!$ObjectType.vlc_pro__Action_Object_Create__c.Label} Details">
                <apex:inputField value="{!vlc_pro__Action_Object_Create__c.Name}"
                    required="true" />
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.vlc_pro__Action_Object_Create__c.fields.vlc_pro__Object_Name__c.Label}" for="objselect" />
                    <apex:outputPanel >
                        <select id="objselect" disabled="disabled">
                            
                        </select>
                        <script>
                            var allObjects = {!availableObjects};
                            $j('#objselect').empty();
                            for(var i = 0; i < allObjects.length; i++)
                            {
                                var newOption = document.createElement("option");
                                newOption.setAttribute('value', allObjects[i].value);
                                if(allObjects[i].value === $j('[id$="selectedObject"]').val())
                                    newOption.setAttribute('selected', 'selected');
                                newOption.appendChild(document.createTextNode(allObjects[i].label));
                                $j('#objselect').append(newOption);
                            }
                            $j('#objselect').prop('disabled', '');
                            $j('#objselect').on('change', function(e)
                            {
                                $j('#objselect :checked').prop('selected', 'selected');
                                $j('[id$="selectedObject"]').val($j('#objselect').val());
                                changeObj();
                            });
                            
                        </script>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>

        <apex:pageBlock title="{!$ObjectType.vlc_pro__Action_Object_Create_Field__c.LabelPlural}" mode="detail">
            <apex:pageBlockButtons location="top">
                <apex:commandButton action="{!processAddField}" value="{!$Label.vlc_pro__Button_New}" />
            </apex:pageBlockButtons>
            <apex:variable value="{!0}" var="count"/> 
            <apex:variable value="{!0}" var="count2"/> 
            <apex:pageBlockTable value="{!fieldList}" var="field">
                <apex:column headerValue="{!$Label.vlc_pro__Label_Action}">
                    <apex:commandLink value="{!$Label.vlc_pro__Link_Del}" action="{!processRemoveField}" immediate="true" >
                        <apex:param name="row" value="{!count2}" assignTo="{!removeField}" />
                    </apex:commandLink>
                    <apex:variable var="count2" value="{!count2 + 1}"/>
                </apex:column>          
                <apex:column headerValue="{!$ObjectType.vlc_pro__Action_Object_Create_Field__c.fields.Name.Label} ({!vlc_pro__Action_Object_Create__c.vlc_pro__Object_Name__c})">
                    <apex:selectList value="{!field.Name}" multiselect="false" size="1"
                        required="true">
                        <apex:selectOptions value="{!availableFields}" />
                    </apex:selectList>
                </apex:column>
                <apex:column headerValue="{!$ObjectType.vlc_pro__Action_Object_Create_Field__c.fields.vlc_pro__Formula__c.Label}">
                    
                    <apex:outputPanel layout="block">
                        <apex:selectList styleClass="vlc_formula__fieldSelect"
                            multiselect="false" size="1" onChange="addField('{!count}');">
                            <apex:selectOptions value="{!availableObjectFields}" />
                        </apex:selectList>
                        <apex:selectList styleClass="vlc_formula__operatorSelect"
                            multiselect="false" size="1" onChange="addOperator('{!count}');">
                            <apex:selectOptions value="{!availableOperators}" />
                        </apex:selectList>
                    </apex:outputPanel>
                    <apex:outputPanel layout="block" styleClass="vlc_formula_wrapper">  
                        <apex:outputPanel layout="block" styleClass="vlc_formula_formula_box">  
                            <apex:inputField styleClass="vlc_formula__formulaField" required="true"
                                value="{!field.vlc_pro__Formula__c}"
                                onBlur="storeFormulaCursor('{!count}');" /> 
                        </apex:outputPanel>
                        <apex:outputPanel layout="block" styleClass="vlc_formula_function_box"> 
                            <apex:selectList styleClass="vlc_formula__functionFilter"
                                multiselect="false" size="1" onChange="filterFunctions('{!count}');">
                                <apex:selectOptions value="{!availableFilters}" />
                            </apex:selectList>
                            <br />
                            <apex:selectList styleClass="vlc_formula__functionSelect"
                                multiselect="false" size="7" onChange="addFunction('{!count}');">
                                <apex:selectOptions value="{!availableFunctions}" />
                            </apex:selectList>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:variable var="count" value="{!count + 1}"/>
                    
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlock>
    </apex:form>
</apex:page>