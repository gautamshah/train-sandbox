trigger Contact_UpdateRegionalRecordType on Contact (before insert) {

/****************************************************************************************
* Name    : Contact_UpdateRegionalRecordType
* Author  : Gautam Shah
* Date    : 8/30/2013
* Purpose : When contacts are updated, update the record type to the regional version, based on the region of the user performing the update (with the exception of System Admins and API Data Loader).
* 
* Dependancies: 
*              
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 9/3/13        Gautam Shah             (line 22) Added code to only allow processing for US users
* 12/5/13       Gautam Shah             (line 22) Added code to allow processing for US and ANZ users.
* 12/15/13		Gautam Shah				Removed unnecessary code and changed event invocation by removing "before update"
*****************************************************************************************/

    User u = [Select Name, Region__c From User Where Id = :UserInfo.getUserId() Limit 1];
    List<RecordType> rtList = new List<RecordType>([Select Id, Name, DeveloperName From RecordType Where Sobjecttype = 'Contact']);
    Map<String,String> rtMapByName = new Map<String,String>();
    for(RecordType r : rtList)
    {
        rtMapByName.put(r.Name, r.DeveloperName);
    }
    if(u.Region__c == 'US' || u.Region__c == 'ANZ' || u.Region__c == 'LATAM')//temporarily added to only allow processing for US users and ANZ users and LATAM user
    {
        for(Contact c : Trigger.New)
        {
            System.debug('Record_Type_Original__c 1: ' + c.Record_Type_Original__c);
        	String newRecordTypeName = 'Connected Contact ' + u.Region__c;
            c.Record_Type_Original__c = rtMapByName.get(newRecordTypeName);
            System.debug('Record_Type_Original__c 2: ' + c.Record_Type_Original__c);
        }
    }//end temporary switch
}