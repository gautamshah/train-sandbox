trigger MergeWorkplaceAccount on Account (before update) {

    /****************************************************************************************
    * Name    : MergeWorkplaceAccount
    * Author  : Mike Melcher
    * Date    : 11-20-2011
    * Purpose : Create Data Quality Case for Workplace Account when Merge Into Healthcare Facility 
    *           is populated.
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    *
    *****************************************************************************************/
/* 
List<recordtype> recordtypes = [select id from recordtype where name = 'Unverified Sell-To Cross Reference'];
Id pendingRT;
if (recordtypes.size() > 0) {
   pendingRT = recordtypes[0].Id;
}

Id CurrentUser = UserInfo.getUserId();
DataQuality dq = new DataQuality();
id queue = [select QueueId from QueueSObject where Queue.Name = 'Data Quality'].QueueId;
   
Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
   {
    for (integer i=0;i<Trigger.size;i++) {
       if (Trigger.old[i].Merge_Into_Healthcare_Facility__c == null && Trigger.new[i].Merge_Into_Healthcare_Facility__c != null) {
          Trigger.new[i].RecordTypeId = pendingRT;
          dq.createCase(queue,'DQ-Validate New Workplace Sell-To Cross Reference',Trigger.new[i].Id,'Data Quality','Medium', currentUser,'Validation','Assigned');
       }
            
          
    }
}
*/
}