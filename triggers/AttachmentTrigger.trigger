/****************************************************************************************
 * Name    : AttachmentTrigger 
 * Author  : Bill Shan
 * Date    : 02/04/2015 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
trigger AttachmentTrigger on Attachment (after insert) {
    
    Attachment_TriggerHandler handler = new Attachment_TriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }

}