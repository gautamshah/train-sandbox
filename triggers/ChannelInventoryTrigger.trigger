trigger ChannelInventoryTrigger on Channel_Inventory__c (before delete, before insert, before update) {
	
	ChannelInventory_TriggerHandler handler = new ChannelInventory_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
	
	if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.new);
    }
	
	if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old);
    }

}