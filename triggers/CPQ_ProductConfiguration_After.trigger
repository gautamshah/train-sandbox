/**
trigger on Apttus_Config2__ProductConfiguration__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-11/21      Yuli Fintescu		Created. Populate Proposal fields with configuation upon finalizing proposal
2015-06-12		Bryan Fry			Added call to calculate consumable usage for RFA proposals
===============================================================================
*/
trigger CPQ_ProductConfiguration_After on Apttus_Config2__ProductConfiguration__c (after update) {
/* Moved to cpqProductConfiguration_u
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_ProductConfiguration_After__c == true) {
		return;//If this is the integration user then exit
    }
    
    CPQ_ProposalProcesses.FinalizingConfig(Trigger.new, Trigger.oldMap);
    CPQ_SSG_Proposal_Processes.RFACalculateUsage(Trigger.new, Trigger.old);
    CPQ_AgreementProcesses.FinalizingConfig(Trigger.new, Trigger.oldMap);
    */
}