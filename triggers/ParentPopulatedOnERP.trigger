trigger ParentPopulatedOnERP on ERP_Account__c (before update) {

    /****************************************************************************************
    * Name    : ParentPopulatedOnERPAccount
    * Author  : Mike Melcher
    * Date    : 11-15-2011
    * Purpose : When a user populates the Parent Sell-to on an ERP Account update the record type
    *           of the ERP Account to Pending Approval and create a Data Quality Case
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    *3/15/2012    MJB                  Changed all record types to "ERP Change In Process" and added the new field "In Process Status" (line 53 and 59)
    * 5/16/2012   MJM                  Removed logic for determining Case owner -- now covered in createCase method
    *    
    *****************************************************************************************/

List<ERP_Account__c> ERPAccountsForCases = new List<ERP_Account__c>();

Id dataloaderProfile;
Id SystemAdminProfile;
Id currentUser = UserInfo.getUserId();

for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader' or Name='System Administrator']) {
   if (p.Name == 'API Data Loader') {
      dataloaderProfile = p.Id;
   }
//   if (p.Name == 'System Administrator') {
//      SystemAdminProfile = p.Id;
//   }
}

if (UserInfo.getProfileId() != dataloaderProfile && UserInfo.getProfileId() != SystemAdminProfile) {
   dataQuality dq = new dataQuality();
   Map<String,Id> rtMap = dq.getRecordtypes();
   
   for (integer i=0;i<Trigger.size;i++) {
      System.debug('ParentPopulated trigger, old parent: ' + trigger.old[i].Parent_Sell_To_Account__c + ' new parent: ' + trigger.new[i].Parent_Sell_To_Account__c);
      if (rtMap.get('ERP Change In Process') == null) {
         Trigger.new[i].addError('Unable to complete request.  Record Type "ERP Change In Process" Not Found');
         return;
      }
   
      if (Trigger.new[i].Parent_Sell_To_Account__c != null) {
         if (Trigger.old[i].Parent_Sell_To_Account__c == null) {
             ERPAccountsForCases.add(Trigger.new[i]);
             Trigger.new[i].recordtypeid = rtMap.get('ERP Change In Process');
             Trigger.new[i].In_Process_status__c = 'Sell-To Change Pending';             
             System.debug('ParentPopulated Trigger, Updated recordtype on ERP Account to ' + rtMap.get('ERP Change In Process'));
          
         } else if (Trigger.old[i].Parent_Sell_To_Account__c != Trigger.new[i].Parent_Sell_To_Account__c) {
               ERPAccountsForCases.add(Trigger.new[i]);
               Trigger.new[i].recordtypeid = rtMap.get('ERP Change In Process');
               Trigger.new[i].In_Process_status__c = 'Sell-To Change Pending';               
         }
      }
   }


   if (ERPAccountsForCases.size() > 0) {
   
     
      for (ERP_Account__c a : ERPAccountsForCases) {
     
         dq.createCase('Validate Parent Sell-To Account link for ' + a.Name,a.Id,'Data Quality','Medium', a.LastModifiedById,'Validation','Assigned');
      }
   }
} 
}