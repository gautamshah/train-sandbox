/**
trigger on Rebate__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-01-05      Yuli Fintescu		Created. Check/Uncheck Rebates_Selected__c box based on if any Rebate__c records existing on proposal
2015-02-22      Yuli Fintescu		Updated. Update approval status to Not Submitted after approved if any rebated fields are changed
===============================================================================
*/
trigger CPQ_Rebate_After on Rebate__c (after insert, after delete, after undelete, after update) {
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Rebate_After__c == true) {
		return;
    }
    
    //Update Rebates_Seleted__c if the proposal has/has not rebates
	Map<ID, List<Rebate__c>> candidates = new Map<ID, List<Rebate__c>>();
	if (Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate) {
		for(Rebate__c r : Trigger.new) {
			List<Rebate__c> rebates;
			if (candidates.containsKey(r.Related_Proposal__c))
				rebates = candidates.get(r.Related_Proposal__c);
			else {
				rebates = new List<Rebate__c>();
				candidates.put(r.Related_Proposal__c, rebates);
			}
			rebates.add(r);
		}
	} else if (Trigger.isDelete) {
		for(Rebate__c r : Trigger.old) {
			List<Rebate__c> rebates;
			if (candidates.containsKey(r.Related_Proposal__c))
				rebates = candidates.get(r.Related_Proposal__c);
			else {
				rebates = new List<Rebate__c>();
				candidates.put(r.Related_Proposal__c, rebates);
			}
			rebates.add(r);
		}
	}
	
	CPQ_ProposalProcesses.UpdateRebateSelected(candidates, Trigger.isUpdate, Trigger.oldMap);
}