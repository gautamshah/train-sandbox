/**
trigger on Product2.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-01-07      Yuli Fintescu		Created. translate code in UOM_Desc__c to Apttus_Config2__Uom__c value
10/10/2016   Paul Berglund   Moved logic to cpqProduct_UOM_u
===============================================================================
*/
trigger CPQ_Product2_Before on Product2 (before insert, before update)
{
	/*
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Product2_Before__c == true) {
		return;
    }
    
    //translate code in UOM_Desc__c to Apttus_Config2__Uom__c value
	for (Product2 p : Trigger.new) {
		if (p.UOM_Desc__c != null && p.Apttus_Product__c == true &&
				(Trigger.isInsert ||
				(Trigger.isUpdate && (p.UOM_Desc__c != Trigger.oldMap.get(p.ID).UOM_Desc__c || 
									Trigger.oldMap.get(p.ID).Apttus_Product__c == false)))) {
			p.Apttus_Config2__Uom__c = CPQ_Utilities.translateUOM(p.UOM_Desc__c);
		}
	}
	*/
}