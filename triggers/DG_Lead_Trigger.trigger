trigger DG_Lead_Trigger on Lead (before insert, after insert, before update) {
	if (Trigger.isBefore && Trigger.isInsert){
		DG_Lead_Class.ResetHasInterest_OnInsert(trigger.New); 
	}
	if (Trigger.isAfter && Trigger.isInsert){
		DG_Lead_Class.LeadHasInterest_OnInsert(trigger.New);
		DG_Lead_Class.LeadAddCM_OnInsert(trigger.New);
	}
	if (Trigger.isBefore && Trigger.isUpdate){
		DG_Lead_Class.LeadHasInterest_OnUpdate(trigger.New,trigger.Old);
		DG_Lead_Class.LeadAddCM_OnUpdate(trigger.New,trigger.Old);
	}
}