trigger ChildCourseUpdation on Course__c (before  update) {
/****************************************************************************************
    * Name    : ChildCourseUpdation    
    * Author  : Lakhan Dubey    
    * Date    : 13-3-2015    
    * Purpose :  To populate child Course  fields Logistic Survey,Follow Up Survey,Status from corresponding parent Course fields when updated.         
    * ========================    
    * = MODIFICATION HISTORY =    
    * ========================    
    * 
    *   
    * *****************************************************************************************/
List<Course__c> ChildCoursetobeUpdated =new List<Course__c>([Select Id,Course__c, Logistics_Survey__c,Follow_Up_Survey__c,Status__c from Course__c where Course__c IN :Trigger.new]);
for(Course__c c: ChildCoursetobeUpdated)
{ 
if(Trigger.newMap.keyset().contains(c.Course__c))
{
c.Logistics_Survey__c=Trigger.newMap.get(c.Course__c).Logistics_Survey__c;
c.Follow_Up_Survey__c=Trigger.newMap.get(c.Course__c).Follow_Up_Survey__c;
if(Trigger.newMap.get(c.Course__c).Status__c=='Cancelled')
c.Status__c=Trigger.newMap.get(c.Course__c).Status__c;
}
Update  ChildCoursetobeUpdated;
}
}