/**
trigger on Apttus_Proposal__Proposal__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-11/21      Yuli Fintescu		Created. Populate "Covidien RMS US Master" PriceList when created
2014-01-05      Yuli Fintescu		Updated. Update recordtype to locked when accepted for Demo and Current Pricing Quote proposal
2014-01-12      Yuli Fintescu		Updated. calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
2014-01-30      Yuli Fintescu		Updated. Update approval status to Not Submitted from Cancelled
											Cancel Quote/Proposal Approvals button on Approve Request History related list sets approval status to Cancelled.
2015-02-22      Yuli Fintescu		Updated. Update approval status to Not Submitted after approved if any fields are changed
2015-03-31      Yuli Fintescu		Updated. Set Proposal Exp Date 45 days out after presented date
2015-03-31      Yuli Fintescu		Updated. Update ERP_Ship_to_Address__c with ERP that has the highest sales
2015-08-25      Bryan Fry           Updated. Added call to set proposal approval template fields
2015-12-28      Bryan Fry           Updated. Added call to create primary contact if needed
===============================================================================
*/
trigger CPQ_Proposal_Before on Apttus_Proposal__Proposal__c (before insert, before update) {
	/* Moved to cpqProposal_u
	//calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
	for (Apttus_Proposal__Proposal__c p : trigger.New) {
		p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);
	}
	
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Proposal_Before__c == true) {
		return;//If this is the integration user then exit
    }
    
    if (Trigger.isInsert) {
	    //Populate "Covidien RMS US Master" PriceList when created
        CPQ_ProposalProcesses.InsertPriceList(Trigger.new);
    }
    
    if (Trigger.isUpdate) {
	    //Update recordtype to locked when accepted for Demo and Current Pricing Quote proposal
        CPQ_ProposalProcesses.UpdateRecordType(Trigger.new, Trigger.oldMap);
        
        //Update approval status to Not Submitted from Cancelled
        //Set Proposal Exp Date 45 days out after presented date
        CPQ_ProposalProcesses.UpdateStatusWhenCancelled(Trigger.new, Trigger.oldMap);
        
        //Update approval status to Not Submitted after approved if any fields are changed
        CPQ_ProposalProcesses.ResetApprovalStatusWhenFieldChanged(Trigger.new, Trigger.oldMap);

        // Create new contact and associate it as the primary contact on the proposal if needed
        if (CPQ_ProposalProcesses.CreatePrimaryContact_Exec_Num > 0) {
            CPQ_ProposalProcesses.CreatePrimaryContact(Trigger.new, Trigger.oldMap);
            CPQ_ProposalProcesses.CreatePrimaryContact_Exec_Num--;
        }
        
    }
    
    //Update ERP_Ship_to_Address__c with ERP that has the highest sales
    CPQ_ProposalProcesses.UpdateERPBySales(Trigger.new, Trigger.oldMap);

    // Set fields needed for Apttus standard quote approval email
    CPQ_SSG_Proposal_Processes.SetProposalApprovalTemplateFields(Trigger.new, Trigger.oldMap);
    */
}