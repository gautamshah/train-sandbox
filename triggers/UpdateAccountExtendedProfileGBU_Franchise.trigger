trigger UpdateAccountExtendedProfileGBU_Franchise on Account_Extended_Profile__c (before insert) {
    /****************************************************************************************
    * Name    : UpdateAccountExtendedProfileGBU_Franchise
    * Author  : Nathan Shinn
    * Date    : 08/25/2011
    * Purpose : Sets the Account_Extended_Profile__c's GBU and Franchise to the values on the 
    *           Account_Extended_Profile__c Owner's User record.
    * 
    * Dependancies: Account_Extended_Profile__c
    *              ,User
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 4th May     Jason Rayner         added business unit population back in (Case 43623) - a.Business_Unit__c = userMap.get(a.OwnerId).Business_Unit__c;
    * 2/6/2013    Mihir Shah           Commented the ower field logic and replaced it with the check for current logged in user for the Master-Detail Realationship changes
    *****************************************************************************************/ 
Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];

if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
{
     if(UserInfo.getProfileId()== [select Id 
                                    from Profile 
                                   where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id
       )
    {
      return;//If this is the integration user then exit
    }
    
    map<Id, User> userMap = new map<Id, User>();
    //set<Id> userIds = new set<Id>();
    user usr=[select id from user where id=:userinfo.getuserid() limit 1];
    //get the list of user Ids for the owner column
    /*for(Account_Extended_Profile__c a : trigger.new)
    {
        userIds.add(a.OwnerId);
    }*/
    
    system.debug('usr--->'+usr);
    
    //create a map of owner to user for retrieving the GBU and Franchise Ids
    for(User u : [select Id
                       , Franchise__c
                       , Business_Unit__c                     
                     from User
                   where Id =:usr.id]) 
    {
      userMap.put(u.Id, u);
    }
   
    system.debug('userMap--->'+userMap);
    if(!userMap.IsEmpty() && userMap.size()>0){
        for(Account_Extended_Profile__c a : trigger.new)
        {
            //update GBU and Franchise to the Owner's values
            //a.Franchise__c = userMap.get(a.OwnerId).Franchise__c;
            //a.Business_Unit__c = userMap.get(a.OwnerId).Business_Unit__c;
            a.Business_Unit__c = userMap.get(usr.id).Business_Unit__c;
            a.Sales_Org__c = userMap.get(usr.id).Franchise__c;
        }
    }
}
}