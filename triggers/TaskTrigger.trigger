/****************************************************************************************
 * Name    : TaskTrigger 
 * Author  : Bill Shan
 * Date    : 5/2/2015 
 * Purpose : Case Trigger Entry
 * Dependencies: Case Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 4/20/2015   Bill Shan			Add after update logic
 *****************************************************************************************/
trigger TaskTrigger on Task (before insert, after insert, after update) {
	
	Task_TriggerHandler handler = new Task_TriggerHandler(Trigger.isExecuting, Trigger.size);

	if(Trigger.isInsert){
		if(Trigger.isBefore)
        	handler.onBeforeInsert(Trigger.new);
        else
        	handler.onAfterInsert(Trigger.new);	
	}
    else if(Trigger.isUpdate){
		if(Trigger.isAfter)
        	handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);	
	}
        	
}