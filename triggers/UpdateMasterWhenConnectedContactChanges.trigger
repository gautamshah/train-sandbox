trigger UpdateMasterWhenConnectedContactChanges on Contact (before insert, before update) {
  /****************************************************************************************    
  * Name    : UpdateMasterWhenConnectedContactChanges
  * Author  : Mike Melcher
  * Date    : 6-22-2012    
  * Purpose : When a Connected Contact is associated with a Master populate the Master Contact
  *           fields    
  *     
  * Dependancies:     
  *                  
  *          
  * ========================    
  * = MODIFICATION HISTORY =    
  * ========================    
  * DATE            AUTHOR              CHANGE    
  * ----            ------              ------    
  * 6/26/2012       MJM                 Updated to run when any target field is updated  
  * 6/27/2012       MJM                 Updated to use common class code, check for replication in progress to avoid recursion
  * 6/27/2012       MJM                 Also update other connected contacts for the same master.  This is to avoid recursion problems.
  * 7/10/2012       MJM                 Added code to allow for more than one Connected Contact to update a single Master in one batch
  * 9/27/2012       MJB                 Removed "Phone" field and commented out "Email" field
  * 11/20/2012      BSH                 Add field "KOL_Status__c" and "KOL_Type__c"
  * 01/28/2013      BSH                 Add field "S2_KOL__c", "RMS_KOL__c", "VT_KOL__c" and "MS_KOL__c"
  * 04/04/2013      BSH                 Add field "Agreed_on_PIPA__c"
  * 12/17/2013      Gautam Shah         Modified so that trigger processes regional contact recordtypes.  Code changed: lines 54-67, 76, 88
  * 3/30/2014       Gautam Shah         Commented out SOQL query to Profile object and replaced with call to Utilities.profileMap (Line 40)
  *****************************************************************************************/
  
   // Verify contact created by user and not the Integration processes
/*  
  Id dataloaderProfile;
  Id currentUser = UserInfo.getUserId();
  
  for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader']){ //or Name='System Administrator']) { 
     if (p.Name == 'API Data Loader') {      
        dataloaderProfile = p.Id;   
     } 
  }
*/
  if (Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader') {
  
  
  // Check to see if Connected Contact was updated as part of the trigger that replicates changes to Masters 
  // down to Connected Contacts.  If so, skip processing.
  
  if (!DataQuality.ContactReplicationInProgress()) {
     
     System.debug('In Update Master, no replication in progress...');
     List<Contact> ContactsToProcess = new List<Contact>();
     Set<Id> MasterIds = new Set<Id>();
  
  
      DataQuality dq = new DataQuality();
      Map<String,Id> rtmap = dq.getRecordTypes(); 
   
      Set<Id> RTsToProcess = new Set<Id>();
      RTsToProcess.add(rtMap.get('Master Clinician'));
      RTsToProcess.add(rtMap.get('Master Non Clinician'));
      RTsToProcess.add(rtMap.get('Connected Clinician'));
      RTsToProcess.add(rtMap.get('Connected Non Clinician Contact'));
      RTsToProcess.add(rtMap.get('In Process Connected Contact'));
      //add regional variants
      RTsToProcess.add(rtMap.get('Connected Contact ANZ'));
      RTsToProcess.add(rtMap.get('Connected Contact ASIA'));
      RTsToProcess.add(rtMap.get('Connected Contact CA'));
      RTsToProcess.add(rtMap.get('Connected Contact EM'));
      RTsToProcess.add(rtMap.get('Connected Contact EU'));
      RTsToProcess.add(rtMap.get('Connected Contact Japan'));
      RTsToProcess.add(rtMap.get('Connected Contact US'));
      RTsToProcess.add(rtMap.get('Connected Contact LATAM'));      
      // Newly inserted Contacts
      // If the contact is connected and the Master is populated, save Contact in list of contacts to be processed.
      System.debug('In updatemaster, checking trigger type...');
      if (Trigger.isInsert) {
        System.debug('In Updatemaster, trigger is insert...');
        for (Contact c : Trigger.new) {
           if (c.Master_Contact_Record__c != null) {
              //if (c.RecordtypeId == connected || c.RecordtypeId == connectedNon || c.RecordtypeId == inprocess) {
              if(RTsToProcess.contains(c.RecordtypeId))
              {
                 ContactsToProcess.add(c);
                 MasterIds.add(c.Master_Contact_Record__c);
              }
           }
        }
      } else { // Trigger is update
         for (integer i=0;i<Trigger.size;i++) {
            System.debug('Update section, old master: ' + Trigger.old[i].Master_Contact_Record__c + ' master: ' + Trigger.new[i].Master_Contact_Record__c + ' recordtype: ' + Trigger.new[i].recordtypeId);
            if (Trigger.new[i].Master_Contact_Record__c != null) {
                //if (Trigger.new[i].RecordtypeId == connected || Trigger.new[i].RecordtypeId == connectedNon || trigger.new[i].RecordtypeId == inprocess) {
                if(RTsToProcess.contains(Trigger.new[i].RecordtypeId))
                {
                   System.debug('Adding contact to process...');
                   ContactsToProcess.add(Trigger.new[i]);
                   MasterIds.add(Trigger.new[i].Master_Contact_Record__c);
                }
            }
         }
      }
   
      if (MasterIds.size() > 0) {
         DataQuality.setContactReplication();
         System.debug('In update master, ' + masterIds.size() + ' masters to process...');
         Map<Id, Contact> MasterMap = new Map<Id,Contact>();
         List<Contact> MastersForUpdate = new List<Contact>();
       
         for (Contact MC : [select Id, FirstName, LastName, Title, Salutation,
                                   Suffix__c, Contact_Photograph__c, Phonetic_Pronunciation__c,
                                   TranslatedName__c, NPI_Code__c, State_Region_License__c, Specialty1__c,
                                   Specialty_2__c, Specialty_3__c, Type__c, Gender__c, MobilePhone,
                                   LinkedIn_Profile__c, Preferred_Contact_Method__c, Contact_Character_Profile__c,
                                   Contact_Segment__c, Contact_Motivations__c, MaritalStatus__c, PreferredLanguage__c,
                                   LanguagesKnown__c, Hobbies__c, Birthdate, Personal_Email__c, /*Email,*/ OtherPhone, 
                                   Website__c,KOL_Status__c,KOL_Type__c,KOL_Procedures__c, Agreed_on_PIPA__c, Request_EbD_KOL__c,
                                   Request_EMID_KOL__c, Request_RMS_KOL__c, Request_STI_KOL__c, Request_VT_KOL__c
                            from Contact where Id in :MasterIds]) {
            MasterMap.put(MC.Id, MC);
         }
         boolean MasterUpdated = false;
         Map<Id,Contact> UpdatedMasters = new Map<Id,Contact>();
      
         for (Contact c : ContactsToProcess) {
            id mcId = c.Master_Contact_Record__c;
            Contact mc;
            // If the Master Contact hasn't already been updated as part of the batch retrieve it from
            // the map of Master Contacts.  If the Master Contact has already been updated, retrieve it from
            // the map of updated Master Contacts
            if (UpdatedMasters.get(mcId) == null) {
               mc = MasterMap.get(mcId);
            } else {
               mc = UpdatedMasters.get(mcId);
            }
               
            System.debug('Replicating Contact...');
            MasterUpdated = dq.ReplicateContact(c,mc);
        
            if (MasterUpdated) {
               UpdatedMasters.put(mc.Id,mc);
            }
        
            System.debug('Updated master: ' + mc);
         }
         if (UpdatedMasters.size() > 0) {
            update UpdatedMasters.values();
         }
         
         // Get all associated Connected Contacts and update them as well
         List<Contact> UpdatedConnected = new List<Contact>(); 
         for (Contact CC : [select Id, FirstName, LastName, Title, Salutation,
                                   Suffix__c, Contact_Photograph__c, Phonetic_Pronunciation__c,
                                   TranslatedName__c, NPI_Code__c, State_Region_License__c, Specialty1__c,
                                   Specialty_2__c, Specialty_3__c, Type__c, Gender__c, MobilePhone, OtherPhone,
                                   LinkedIn_Profile__c, Preferred_Contact_Method__c, Contact_Character_Profile__c,
                                   Contact_Segment__c, Contact_Motivations__c, MaritalStatus__c, PreferredLanguage__c,
                                   LanguagesKnown__c, Hobbies__c, Birthdate, Master_Contact_Record__c, /*Email,*/ Website__c,
                                   Personal_Email__c,KOL_Status__c,KOL_Type__c,KOL_Procedures__c,Agreed_on_PIPA__c, Request_EbD_KOL__c,
                                   Request_EMID_KOL__c, Request_RMS_KOL__c, Request_STI_KOL__c, Request_VT_KOL__c
                            from Contact where Master_Contact_Record__c in :MasterIds
                                      and Id not in :ContactsToProcess]) {
            id mcId = cc.Master_Contact_Record__c;
            Contact mc = MasterMap.get(mcId);
            System.debug('Replicating Contact...');
            boolean ConnectedUpdated = dq.ReplicateContact(mc,cc);
        
            if (ConnectedUpdated) {
               UpdatedConnected.add(cc);
            }
        
            System.debug('Updated connected: ' + cc);
         }
         if (UpdatedConnected.size() > 0) {
            update UpdatedConnected;
         }
         
      }
    }  else {
       System.debug('In UpdateMaster, data replication in progress...');
    }
 }
 }