/****************************************************************************************
 * Name    : AccountTrigger 
 * Author  : Bill Shan
 * Date    : 05/02/2013 
 * Purpose : Account Trigger Entry
 * Dependencies: Account Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 6/25/13		Gautam Shah			Changed references to profiles 'EU - SD' & 'EU - EbD' to 'EU - S2'
 * 10/21/13		Bill Shan			Added after insert logic
 *****************************************************************************************/
trigger AccountTrigger on Account (before insert, after insert, before update, after update) {
	
	Account_TriggerHandler handler = new Account_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }
    
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}