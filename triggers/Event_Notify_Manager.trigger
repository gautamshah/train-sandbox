/***********************
Aurthor:     Yap Zhen-Xiong
Email:       Zhenxiong.Yap@covidien.com
Description: Trigger on Event to send Email to Manager about the creation of the event 
             or any major changes to the event. 
Notes:       Currently works for Asia only.

 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * 4Nov15      Fenny Saputra        Add Philippines into AllowedCountries list (line 30)
 * 11Feb16     Zhen-Xiong Yap       Add all Asia conutries to AllowedCountries
 * 21Jun16     Fenny Saputra        Add email template for KR users
***********************/

trigger  Event_Notify_Manager on Event (after insert, after update) {

    Set<Id> ownerIds = new Set<Id>();
    Set<Id> accIds = new Set<Id>();
    Set<Id> conIds = new Set<id>();
    Set<String> AllowedCountries = new Set<String>();
    List<Event> passedCreatedEvent = new List<Event>();
    //Map<id, User> eventMgrMap = new Map<id, User>();
    
    Map<Id, List<Event>> PassedCriteriaEvents = new Map<id, List<Event>>(); //Owner mapped to multiple Event
    
    //Start-List of allowed Countries for Email notification
    AllowedCountries.add('SG'); AllowedCountries.add('IN');
    AllowedCountries.add('HK'); AllowedCountries.add('MY');
    AllowedCountries.add('KR'); AllowedCountries.add('TH');
    AllowedCountries.add('CN'); AllowedCountries.add('VN');
    AllowedCountries.add('TW'); AllowedCountries.add('ID');
    AllowedCountries.add('PH');
    //End-list
    
    for(Event e: Trigger.new){
        ownerIds.add(e.OwnerId);
        accIds.add(e.WhatId);
        conIds.add(e.WhoId);
    }
    
    List<User> ownerList = [SELECT Country, Email, Name, ManagerId, LanguageLocaleKey FROM User WHERE Id in :ownerIds];
    Map<id, User> userMgrMap = new Map<id, User>();
    
    Set<id> mgrId = new Set<id>();
    for(User u : ownerList){
        System.debug('TriggerDebug:-Owner\'s Manager: ' + u.ManagerId);
        mgrId.add(u.ManagerId);
    }
        
    for(User mgr : [SELECT id, Email, Name, LanguageLocaleKey, LocaleSidKey, Country FROM User WHERE Id in :mgrId]){
        System.debug('TriggerDebug:-Assigning Manager: ' + mgr .Name);
        for(User sub : ownerList){
            System.debug('TriggerDebug:-Assigning Subordinate: ' + sub.Name);
            if(sub.ManagerId == mgr.Id){
                userMgrMap.put(sub.id, mgr);
            }
        }
    }
    
    Map<Id,Account> accMap = new Map<Id,Account>();
    if(accIds <> null){
        List<Account> accList = [select Id, Name from Account where Id in :accIds];   
        for(Account acc : accList){
            for(Event e:Trigger.new){
                if(e.WhatId == acc.Id){
                    accMap.put(e.Id, acc);
                }
            }
        }
    }
    
    Map<Id,Contact> conMap = new Map<Id,Contact>();
    if(conIds <> null){
        List<Contact> conList = [select Id, Name from Contact where Id in :conIds];
        for(Contact con : conList){
            for(Event e:Trigger.new){
                if(e.WhoId == con.Id){
                    conMap.put(e.Id, con);
                }
            }
        }
    }
    
    User owner;
    for(Event e: Trigger.new){
        owner = userMgrMap.get(e.ownerId);
        //check if Owner's Country is in the Allowed Country List And rep choose to notify manager
        if(owner != null)
            if(AllowedCountries.contains(owner.Country) && e.Notify_Manager__c == true){ 
                 passedCreatedEvent.add(e);
                 //eventMgrMap.put(e.id, userMgrMap.get(e.ownerId));
            }
     }
    
    //Select email template for sending
    EmailTemplate et_TW;
    EmailTemplate et_CN;
    EmailTemplate et_Default;
    EmailTemplate et_KR;
    
    for(EmailTemplate et : [SELECT id, developerName, Body, HtmlValue, Subject FROM EmailTemplate WHERE developerName = 'Manager_Notification' 
                        OR developerName = 'Manager_Notification_TW' OR developerName = 'Manager_Notification_CN'
                        OR developerName = 'Manager_Notification_KR']){
        String devName = et.developerName;
        if(devName.contains('_TW'))
            et_TW = et;    
        else if(devName.contains('_CN'))
            et_CN = et;  
        else if(devName.contains('_KR'))
            et_KR = et;
        else{  
            et_Default = et;      
        }
    }
    System.debug('TriggerDebug:-Assigning KR email template: ' + et_KR);
    
    for(Event e : passedCreatedEvent){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        User u = userMgrMap.get(e.ownerId);
        String[] toAddresses = new String[] {u.email};
        mail.setToAddresses(toAddresses);    // Set the TO addresses
            
        EmailTemplate et; 
        if(u.LanguageLocaleKey == 'zh_TW')
            et = et_TW;
        else if(u.LanguageLocaleKey == 'zh_CN')
            et = et_CN;
        else if(u.Country == 'KR')
            et = et_KR;
        else
            et = et_Default;
        System.debug('user locale:' + u.LocaleSidKey);
        System.debug('user country:' + u.Country);
        System.debug('TriggerDebug:-Email template before send email: ' + et);
         
        mail.setSenderDisplayName('Covidien - Salesforce.com');
        mail.setReplyTo('no-reply@covidien.com');
        mail.setUseSignature(false);
            
        String subject = et.Subject;
        subject = subject.replace('{!Event.Subject}', e.Subject);
        mail.setSubject(subject);
           
        System.debug('Email Template Body: '+ et.body);
        String htmlBody = et.Body;
        htmlBody = htmlBody.replace('{!Event.Subject}', e.Subject);
        htmlBody = htmlBody.replace('{!Event.RecurrenceStartDateTime}', e.StartDateTime.format());
        if(u.Country == 'KR'){
            if(e.WhatId <> null){
                Account acc = accMap.get(e.Id);
                htmlBody = htmlBody.replace('{!Event.What}', acc.Name);
            }else{
                htmlBody = htmlBody.replace('{!Event.What}', '');
            }
            if(e.WhoId <> null){
                Contact con = conMap.get(e.Id);
                htmlBody = htmlBody.replace('{!Event.Who}', con.Name);
            }else{
                htmlBody = htmlBody.replace('{!Event.Who}', '');
            }
            htmlBody = htmlBody.replace('{!Event.Asia_Product__c}', e.Asia_Product__c); 
        }       
        if(e.Description != null)
            htmlBody = htmlBody.replace('{!Event.Description}', e.Description);
        else
            htmlBody = htmlBody.replace('{!Event.Description}', '');
        String strURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.id;
        htmlBody = htmlBody.replace('{!Event.Link}', strURL);
        mail.setPlainTextBody(htmlBody); 
                
        Messaging.SendEmail(new Messaging.SingleEmailMessage[] {mail});
        System.debug('TriggerDebug:-Email Sent');
        }
}