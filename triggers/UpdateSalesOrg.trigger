trigger UpdateSalesOrg on User (before insert, before update) 
{
    for(User u : trigger.new)
    {
        If (u.Sales_Org_PL__c==null || u.Sales_Org_PL__c=='')
        {
            u.Sales_Org_PL__c=u.Franchise__c;
        }
        Else if (u.Franchise__c!=u.Sales_Org_PL__c)
        {
            u.Franchise__c=u.Sales_Org_PL__c;
        }
    }
}