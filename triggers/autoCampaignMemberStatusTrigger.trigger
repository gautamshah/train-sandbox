trigger autoCampaignMemberStatusTrigger on Campaign (after insert) 
{
    ID rectypeid = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('US MITG Campaign').getRecordTypeId();
    //Recordtype rt = [SELECT DeveloperName,Id,Name FROM RecordType WHERE SobjectType = 'Campaign' AND DeveloperName = 'US_MITG_Campaign'];
    List<Campaign> newCamps = [select Id from Campaign where Id IN :trigger.new AND ParentID = Null AND RecordTypeId =:rectypeid];
    List<CampaignMemberStatus> cms2Insert = new List<CampaignMemberStatus>();
         
    for(Campaign cm: newCamps) 
    {            
            CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId = cm.Id, HasResponded=false,
             Label = 'Active', SortOrder = 3, isDefault = true);
             cms2Insert.add(cms1);         
            
            CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId = cm.Id, HasResponded=false,
             Label = 'Remove', SortOrder = 4);
             cms2Insert.add(cms2);
             
            CampaignMemberStatus cms3 = new CampaignMemberStatus(CampaignId = cm.Id, HasResponded=true,
             Label = 'Complete', SortOrder = 5);
             cms2Insert.add(cms3); 
             
    }
    insert cms2Insert;
}