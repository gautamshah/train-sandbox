/****************************************************************************************
 Name    : Attachment - one trigger to rule them all
 Author  : Paul Berglund
 Date    : 08/17/2016
 
 ========================
 = MODIFICATION HISTORY =
 ========================
 DATE        AUTHOR           CHANGE
 ----        ------           ------
 08/17/2016  Paul Berglund    Initial deployment to production in support of Document
                              Management application
 *****************************************************************************************/
trigger Attachment on Attachment
    (
        after delete,
        after insert,
        after undelete,
        after update,
        before delete,
        before insert,
        before update
    )
{
	system.debug('Attachment - Entry: ' + limits.getQueries());
	
    Attachment_td.main(
            trigger.isExecuting,
            trigger.isInsert,
            trigger.isUpdate,
            trigger.isDelete,
            trigger.isBefore,
            trigger.isAfter,
            trigger.isUndelete,
            trigger.new,
            trigger.newMap,
            trigger.old,
            trigger.oldMap,
            trigger.size);

	system.debug('Attachment - Exit: ' + limits.getQueries());
}