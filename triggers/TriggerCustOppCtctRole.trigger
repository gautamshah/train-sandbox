/**
*   CreatedDate: 2014.9.5
*   Target:     1) Keep Specialty_PL__c & Department_PL__c field on Opportunity_Contact_Role__c
*                   synced with contact
*
**/

trigger TriggerCustOppCtctRole on Opportunity_Contact_Role__c(after insert) {
    if (trigger.isAfter && trigger.isInsert) {
        List<Opportunity_Contact_Role__c> roles =
            [SELECT Department_PL__c, Specialty_PL__c, 
            Opportunity_Contact__r.Department_pl__c, Opportunity_Contact__r.Specialty__c
            FROM Opportunity_Contact_Role__c WHERE Id IN : trigger.new];

        for (Opportunity_Contact_Role__c role : roles) {
            role.Department_PL__c = role.Opportunity_Contact__r.Department_pl__c;
            role.Specialty_PL__c = role.Opportunity_Contact__r.Specialty__c;
        }

        try {
            update roles;
        }
        catch (DMLException e) {
            trigger.new[0].addError(e.getDMLMessage(0));
            System.debug(' DML Exception: ' + e);
        }
    }
}