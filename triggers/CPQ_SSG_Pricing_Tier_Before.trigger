trigger CPQ_SSG_Pricing_Tier_Before on CPQ_SSG_Pricing_Tier__c (before insert, before update) {
    Map<String,List<CPQ_SSG_Pricing_Tier__c>> tiersToProcess = new Map<String,List<CPQ_SSG_Pricing_Tier__c>>();
    for (CPQ_SSG_Pricing_Tier__c tier: Trigger.new) {
        if (tier.Class_of_Trade_Name__c != null) {
            if (!tiersToProcess.containsKey(tier.Class_of_Trade_Name__c)) {
                tiersToProcess.put(tier.Class_of_Trade_Name__c, new List<CPQ_SSG_Pricing_Tier__c>());
            }
            tiersToProcess.get(tier.Class_of_Trade_Name__c).add(tier);
        }
    }

	List<CPQ_SSG_Class_of_Trade__c> cots = [Select Id, Name From CPQ_SSG_Class_of_Trade__c Where Name in :tiersToProcess.keySet()];

    for (CPQ_SSG_Class_of_Trade__c cot: cots) {
        List<CPQ_SSG_Pricing_Tier__c> tiers = tiersToProcess.get(cot.Name);
        if (tiers != null) {
            for (CPQ_SSG_Pricing_Tier__c tier: tiers) {
                tier.CPQ_SSG_Class_of_Trade__c = cot.Id;
            }
        }
    }
}