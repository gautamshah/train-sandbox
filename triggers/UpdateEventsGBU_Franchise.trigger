trigger UpdateEventsGBU_Franchise on Event (before insert, before update) {
    /****************************************************************************************
    * Name    : UpdateEventsGBU_Franchise
    * Author  : Nathan Shinn
    * Date    : 08/25/2011
    * Purpose : Sets the task's GBU and Franchise to the falues on the Event Owner's
    *           User record.
    * 
    * Dependancies: Event
    *              ,User
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * 02-11-14    S. Clark             Ln 54-57: Added if statement to bypass Franchise Update if owner is Public Calendar
    * 
    *26-03-2015   Lakhan Dubey         Ln 52 : added logic to prevent gbu updation for record type 'EU_Inside_Sales_Activity_Matrix'
    *****************************************************************************************/

Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
{
    if(UserInfo.getProfileId()== [select Id from Profile where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id
       )
    {
      return;//If this is the integration user then exit
    }
    
    map<Id, User> userMap = new map<Id, User>();
    set<Id> userIds = new set<Id>();
    
    //get the list of user Ids for the owner column
    for(Event t : trigger.new)
    {
        userIds.add(t.OwnerId);
    }
    
    //create a map of owner to user for retrieving the GBU and Franchise Ids
    for(User u : [select Id
                       , Franchise__c
                       , Business_Unit__c
                    from User
                   where Id in :userIds ]) 
    {
      userMap.put(u.Id, u);
    }
   
    for(Event tsk : trigger.new)
    {
    
    if (((String)tsk.OwnerID).startsWith('023') ||Utilities.recordTypeMap_Id_Name.get(tsk.RecordTypeId)=='EU - Inside Sales Activity Matrix') //023 represents Public Calendar as Event Owner
      
          return; //If this is a Public Calendar and Not a User then Exit
         else

        //update GBU and Franchise to the Owner's values
        tsk.Franchise__c = userMap.get(tsk.OwnerId).Franchise__c;
        tsk.Business_Unit__c = userMap.get(tsk.OwnerId).Business_Unit__c;
    }
}
}