trigger UpdateAccountAffiliation_GBU_Franchise on Account_Affiliation__c (before insert, before update) {
    /****************************************************************************************
    * Name    : UpdateAccountAffiliation_GBU_Franchise
    * Author  : Nathan Shinn
    * Date    : 08/25/2011
    * Purpose : Sets the Account_Affiliation__c's GBU and Franchise to the values on the 
    *           Account_Affiliation__c Owner's User record.
    * 
    * Dependancies: Account_Affiliation__c
    *              ,User
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    * 2/7/2013    Mihir Shah           Commented the ower field logic and replaced it with the check for current logged in user for the Master-Detail relationship changes
    *****************************************************************************************/
Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
{
    if(UserInfo.getProfileId()== [select Id 
                                    from Profile 
                                   where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id
       )
    {
      return;//If this is the integration user then exit
    }
    
    map<Id, User> userMap = new map<Id, User>();
   // set<Id> userIds = new set<Id>();
   user usr=[select id from user where id=:userinfo.getuserid() limit 1]; 
    //get the list of user Ids for the owner column
   /* for(Account_Affiliation__c a : trigger.new)
    {
        userIds.add(a.OwnerId);
    }*/
    
    //create a map of owner to user for retrieving the GBU and Franchise Ids
    for(User u : [select Id
                       , Franchise__c
                       , Business_Unit__c
                    from User
                   where Id = :usr.id])  
    {
      userMap.put(u.Id, u);
    }
   if(!userMap.IsEmpty() && userMap.size()>0){
    for(Account_Affiliation__c a : trigger.new)
    {
        //update GBU and Franchise to the Owner's values
        //a.Franchise__c = userMap.get(a.OwnerId).Franchise__c;
       // a.Business_Unit__c = userMap.get(a.OwnerId).Business_Unit__c;
           a.Franchise__c = userMap.get(usr.id).Franchise__c;
           a.Business_Unit__c = userMap.get(usr.id).Business_Unit__c;
    }
  }
}
}