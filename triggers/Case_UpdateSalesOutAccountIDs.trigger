trigger Case_UpdateSalesOutAccountIDs on Case (after update) {
    Set<String> caseIDs = new Set<String>();
    //RecordType accountCreationRT = [Select Id From RecordType Where SobjectType = 'Case' and DeveloperName = 'Account_Creation_Case' Limit 1];
    String accountCreationRT = Utilities.recordTypeMap_DeveloperName_Id.get('Account_Creation_Case');
    for(Case c : Trigger.New)
    {
        if(c.RecordTypeId == accountCreationRT && c.isClosed)
        {
            caseIDs.add(c.Id);
        }
    }
    if(caseIDs.size() > 0)
    {
        List<Case> caseList = new List<Case>([Select Id, Temporary_Account_Number__c, CreatedById, Master_Account_Number__c From Case Where Id in :caseIDs]);
        Map<String,Case> caseMap = new Map<String,Case>();
        for(Case c : caseList)
        {
            System.debug('Temporary_Account_Number: ' + c.Temporary_Account_Number__c);
            caseMap.put(c.Temporary_Account_Number__c,c);
        }
        System.debug('case size: ' + caseMap.keySet().size());
        List<Sales_Out__c> salesOutList_SellTo = new List<Sales_Out__c>([Select CreatedById, Sell_To__c, Reason_Code__c From Sales_Out__c Where Sell_To__c in :caseMap.keySet()]);
        for(Sales_Out__c so : salesOutList_SellTo)
        {
            so.Sell_To__c = caseMap.get(so.Sell_To__c).Master_Account_Number__c;
            if(so.Reason_Code__c == null || so.Reason_Code__c == '')
            	so.Reason_Code__c = 'N-Replace TEMP Code';
        }
        update salesOutList_SellTo;
        
        
        List<Sales_Out__c> salesOutList_SellFrom = new List<Sales_Out__c>([Select CreatedById, Sell_From__c, Reason_Code__c From Sales_Out__c Where Sell_From__c in :caseMap.keySet()]);
        for(Sales_Out__c so : salesOutList_SellFrom)
        {
            so.Sell_From__c = caseMap.get(so.Sell_From__c).Master_Account_Number__c;
            if(so.Reason_Code__c == null || so.Reason_Code__c == '')
            	so.Reason_Code__c = 'N-Replace TEMP Code';
        }
        update salesOutList_SellFrom;
        //SendDistributorEmail.send(caseList);
    }
}