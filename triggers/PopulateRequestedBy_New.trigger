trigger PopulateRequestedBy_New on Case (before insert) {
    
    /*Populate created by profile name - case 00357737*/
    String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name; 
    
    for(Case r: Trigger.New) {
        if(r.Requested_By__c == null) 
        {r.Requested_By__c = UserInfo.getUserId();}
        
        /*Insert Profile Information*/
        if(r.Created_By_Profile__c == null || r.Created_By_Profile__c == '') 
        {r.Created_By_Profile__c = usrProfileName;}
    }
}