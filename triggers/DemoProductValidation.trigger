/****************************************************************************************
 * Name    : DemoProductValidation 
 * Author  : Fenny Saputra
 * Date    : 10/Jun/2014 
 * Purpose : To prevent users to edit current location when product is loaned at hospital. 
 * Dependencies: Demo Product Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 3/Oct/14    Fenny Saputra        Differentiate the rule from CN & SG
 *                                  Add validation rule for SG
 * 6/Feb/15    Fenny Saputra        Add country filter (CN, SG, HK)
 *                                  Add new logic for HK
 * 7/Mar/16    Chen Li              mapping the exactLocation of Movement Request back to Demo Product as case 00724418
*  Map<String, String> dpexactlocation = new Map<String, String>(); // <id, exact location string>
*
*   dpexactlocation.put(mov.Demo_Product_Name__c, mov.Delivery_Address__c);
*
*  dp.Exact_Location__c = dpexactlocation.get(dp.Id);
*
******************************************************************************************/

trigger DemoProductValidation on Demo_Product__c (before insert, before update, after insert, after update) {

    if(Trigger.isBefore){
        
        //Copy Hospital Name in Movement to Demo Product Current Location
        Set<String> dpID = new Set<String>();
        Map<String, String> dpacc = new Map<String, String>();
        Map<String, String> dpexactlocation = new Map<String, String>(); // <id, exact location string>

        for(Demo_Product__c dp : Trigger.new){
            if(dp.Current_Location__c == 'Hospital'){
                dpID.add(dp.Id);
            }
        }
        
        if(dpID.size() > 0){
        
            List<Movement__c> currentMovList = [select Id, Hospital__c, Delivery_Address__c, Hospital_Name__c, Demo_Product_Name__c, Location__c from Movement__c
                                                where Demo_Product_Name__c in :dpID
                                                and ((Start_Date__c <= :system.today()
                                                and End_Date__c >= :system.today()
                                                and Approval_Status__c = 'Approved') OR
                                                (Start_Date_and_Time__c <= :system.now()
                                                and End_Date_and_Time__c >= :system.now()))
                                                order by Start_Date__c desc];
            
            for(Movement__c mov : currentMovList){
                dpacc.put(mov.Demo_Product_Name__c, mov.Hospital_Name__c);
                dpexactlocation.put(mov.Demo_Product_Name__c, mov.Delivery_Address__c);
            }
            
            for(Demo_Product__c dp : Trigger.new){
                if(dp.Current_Location__c == 'Hospital'){
                    dp.Current_Location__c = dpacc.get(dp.Id);
                    if(dp.Country__c == 'SG'){
                    System.debug('dp.Exact_Location__c' + dpexactlocation.get(dp.Id));
                    dp.Exact_Location__c = dpexactlocation.get(dp.Id);
                    }
                }
            }
        }
    }
    
    if(Trigger.isAfter){
    
        if(Trigger.isUpdate){
            //Share Demo Product record to Owner's territory
            Set<String> dpOwnerOld = new Set<String>();
            List<Demo_Product__Share> dpOldShare = new List<Demo_Product__Share>();
            
            for(Demo_Product__c dp : trigger.new){
                if(dp.Country__c == 'CN'){
                    Demo_Product__c beforeUpdate = System.Trigger.oldMap.get(dp.Id);
                    if(beforeUpdate.OwnerId != dp.OwnerId){
                        dpOwnerOld.add(beforeUpdate.Id);
                    }
                }
                system.debug('dpOwnerOld' + dpOwnerOld);
            }
            
            for(Demo_Product__Share dpshare : [select Id, ParentID, RowCause from Demo_Product__Share where ParentID in :dpOwnerOld and RowCause = 'Owner_Territory_Sharing__c']){
                dpOldShare.add(dpshare);
            }
            
            System.debug('dpOldShare' + dpOldShare);
            delete dpOldShare;          
        }
        
        List<Demo_Product__Share> dpShares  = new List<Demo_Product__Share>();
        Map<String, String> dpOwner = new Map<String, String>();
        Map<String, String> ownerTerritory = new Map<String, String>();
        Map<String, String> territoryGroup = new Map<String, String>();
        
        for(Demo_Product__c dp: trigger.new){
            dpOwner.put(dp.OwnerId, dp.Id);
        }
        
        system.debug('dpOwner' + dpOwner);
        
        for(UserTerritory ut : [select Id, TerritoryID, UserID from UserTerritory where userID =: dpOwner.keySet() and IsActive = true]){
            String dp = dpOwner.get(ut.UserID);
            ownerTerritory.put(ut.TerritoryID, dp);
        }
        
        system.debug('ownerTerritory' + ownerTerritory);
        
        for(Group g : [SELECT Id, RelatedId FROM Group WHERE Type = 'Territory' and RelatedId in :ownerTerritory.keySet()]){
            String dp = ownerTerritory.get(g.RelatedId);
            territoryGroup.put(g.Id, dp);
            system.debug('territoryGroup' + territoryGroup);
        }
    
        for(Demo_Product__c dp: trigger.new){
            system.debug('territoryGroup.keySet()' + territoryGroup.keySet());
            for(String id : territoryGroup.keySet()){
                if(dp.Id == territoryGroup.get(id)){
                    Demo_Product__Share ownerTerritoryShare = new Demo_Product__Share();
                    ownerTerritoryShare.ParentId = dp.Id;
                    ownerTerritoryShare.UserOrGroupId = id;
                    ownerTerritoryShare.AccessLevel = 'edit';
                    ownerTerritoryShare.RowCause = Schema.Demo_Product__Share.RowCause.Owner_Territory_Sharing__c;
                    dpShares.add(ownerTerritoryShare);
//                    return;
                    system.debug('dpShares' + dpShares);
                }
            }
        }
            
        Database.SaveResult[] dpShareInsertResult = Database.insert(dpShares,false);
        
        system.debug('dpShareInsertResult' + dpShareInsertResult);
    } 
}