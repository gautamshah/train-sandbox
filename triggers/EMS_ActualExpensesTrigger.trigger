trigger EMS_ActualExpensesTrigger on Actual_Expenses__c (after insert,after update,after delete) {




// List of parent record ids to update
   Set<Id> parentIds = new Set<Id>();
   // In-memory copy of parent records
   Map<Id,EMS_Event__c> parentRecords = new Map<Id,EMS_Event__c>();
   // Gather the list of ID values to query on
   for(Actual_Expenses__c c: ((Trigger.isDelete)?(Trigger.old):(Trigger.new))){
       parentIds.add(c.EMS_Event__c);
    
    }
   // Avoid null ID values
   parentIds.remove(null);
   // Create in-memory copy of parents
    for(Id parentId : parentIds){

    parentRecords.put(parentId,new EMS_Event__c(Id=parentId,Actual_Meal_Amount__c=0,Actual_Lodging_Amount__c=0,Actual_Local_Ground_Transportation__c=0,
                                                Actual_Air_Travel_Amount__c=0,Actual_Consultancy_Fees_Amount__c=0,Actual_Others_Amount__c=0,
                                                Actual_Gifts_Amount__c=0));
                                                
    }
    
    
    Aggregateresult[] ar=[Select SUM(Amount__c) sum,EMS_Event__c ems,EMS_Expense_Group__c eg from Actual_Expenses__c  where EMS_Event__c in :parentIds group by rollup(EMS_Expense_Group__c,EMS_Event__c)];
    Map<String, Integer> MEmsExpGrpToTotalMount = new Map<String, Integer>();
    for(Aggregateresult a : ar){  
        MEmsExpGrpToTotalMount .put((String) (a.get('eg')+';'+a.get('ems')), Integer.valueOf(a.get('sum')));
    }
    System.debug('====>'+MEmsExpGrpToTotalMount );
                                                
  // Query all children for all parents, update Rollup Field value
  list<CurrencyType> curr = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
  for(EMS_Event__c c: [select Id,CurrencyIsoCode,Actual_Air_Travel_Amount__c,Actual_Local_Ground_Transportation__c,Actual_Lodging_Amount__c,Actual_Meal_Amount__c,
                                        Actual_Gifts_Amount__c,Actual_Consultancy_Fees_Amount__c,Actual_Others_Amount__c from EMS_Event__c
                                                where Id in :parentIds]){
     for(integer i = 0; i< curr.size(); i++){ 
     if(c.CurrencyISOCode == curr[i].ISOCode){
     parentRecords.get(c.Id).Actual_Meal_Amount__c= ((MEmsExpGrpToTotalMount.get('Meal'+';'+c.Id)==NULL)?(0):((MEmsExpGrpToTotalMount.get('Meal'+';'+c.Id))* curr[i].ConversionRate));
     parentRecords.get(c.Id).Actual_Lodging_Amount__c= ((MEmsExpGrpToTotalMount.get('Lodging'+';'+c.Id)==NULL)?0:((MEmsExpGrpToTotalMount.get('Lodging'+';'+c.Id))*curr[i].ConversionRate));
     parentRecords.get(c.Id).Actual_Local_Ground_Transportation__c= ((MEmsExpGrpToTotalMount.get('Local Ground Transportation'+';'+c.Id)==NULL)?0:((MEmsExpGrpToTotalMount.get('Local Ground Transportation'+';'+c.Id))* curr[i].ConversionRate));
     parentRecords.get(c.Id).Actual_Air_Travel_Amount__c= ((MEmsExpGrpToTotalMount.get('Air Travel'+';'+c.Id)==NULL)?0:((MEmsExpGrpToTotalMount.get('Air Travel'+';'+c.Id))* curr[i].ConversionRate));
     parentRecords.get(c.Id).Actual_Consultancy_Fees_Amount__c= ((MEmsExpGrpToTotalMount.get('Consultancy Fees'+';'+c.Id)==NULL)?0:((MEmsExpGrpToTotalMount.get('Consultancy Fees'+';'+c.Id))* curr[i].ConversionRate));
     parentRecords.get(c.Id).Actual_Others_Amount__c= ((MEmsExpGrpToTotalMount.get('Others'+';'+c.Id)==NULL)?0:((MEmsExpGrpToTotalMount.get('Others'+';'+c.Id))* curr[i].ConversionRate));
     parentRecords.get(c.Id).Actual_Gifts_Amount__c= ((MEmsExpGrpToTotalMount.get('Gifts'+';'+c.Id)==NULL)?0:((MEmsExpGrpToTotalMount.get('Gifts'+';'+c.Id))* curr[i].ConversionRate));
     }
     }
   }
     // Commit changes to the database
     Database.saveresult[] sr = Database.update(parentRecords.values(),false);
     System.debug('----->'+sr);

   
   
   
   
   
   

}