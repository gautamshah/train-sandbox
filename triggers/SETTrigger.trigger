trigger SETTrigger on Cost_Analysis_Case__c (before update) {

	SETCase_TriggerHandler handler = new SETCase_TriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    
}