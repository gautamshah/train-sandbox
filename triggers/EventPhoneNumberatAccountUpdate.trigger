trigger EventPhoneNumberatAccountUpdate on Event (before insert, before update) {
    /****************************************************************************************
    * Name    : EventPhoneNumberatAccountUpdate    
    * Author  : Lakhan Dubey    
    * Date    : 13-2-2015    
    * Purpose :  To populate custom event field 'Phone number at account' from contact field 'Phone number at account'
            
    * ========================    
    * = MODIFICATION HISTORY =    
    * ========================    
    * 
    *   
    * *****************************************************************************************/
    if(Utilities.profileMap.get(UserInfo.getProfileId()) == 'ANZ - All' ){
    Set<Id> WhoIds =new Set<Id>();
    for(Event e:trigger.new)
    WhoIds.add(e.whoId);
    Map<Id,Contact> MapCon=new Map<Id,Contact>([Select Phone_Number_at_Account__c from Contact where Id IN: WhoIds and Phone_Number_at_Account__c!=null ]);
    for(Event e:trigger.new)
    { 
    if(MapCon.keyset().contains(e.whoId))
    e.Phone_number_at_account__c=MapCon.get(e.whoId).Phone_Number_at_Account__c ; 
    }
    }
    }