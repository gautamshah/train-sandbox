trigger UpdateConnectedContactsWhenMasterChanges on Contact (after update) {
  /****************************************************************************************    
  * Name    : UpdateConnectedContactsWhenMasterChanges
  * Author  : Mike Melcher
  * Date    : 2-3-2012    
  * Purpose : When a Master Contact name or title changes update any Connected Contacts
  *           with the new information    
  *     
  * Dependancies:     
  *                  
  *          
  * ========================    
  * = MODIFICATION HISTORY =    
  * ========================    
  * DATE        	AUTHOR               CHANGE    
  * ----        	------               ------    
  * 6-22-2012   	MJM          		Added additional fields for update  
  * 6-26-2012   	MJM          		Re-wrote to use shared code
  * 9/27/2012   	MJB          		Removed "Phone" field and commented out "Email" field    
  * 11/20/2012 		BSH          		Add field "KOL_Status__c" and "KOL_Type__c"
  * 01/28/2013  	BSH          		Add field "S2_KOL__c", "RMS_KOL__c", "VT_KOL__c" and "MS_KOL__c"
  * 04/04/2013  	BSH          		Add field "Agreed_on_PIPA__c"
  * 3/30/2014		Gautam Shah			Commented out SOQL query to Profile object and replaced with call to Utilities.profileMap (Line 37)
  *****************************************************************************************/
  
  // Verify contact created by user and not the Integration processes
/*  
  Id dataloaderProfile;
  Id currentUser = UserInfo.getUserId();
  
  for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader' or Name='System Administrator']) {   
     if (p.Name == 'API Data Loader') {      
        dataloaderProfile = p.Id;   
     } 
  }
*/  
  if (Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && !DataQuality.ContactReplicationInProgress()) {
       DataQuality dq = new DataQuality(); 
       DataQuality.setContactReplication();      
       Map<String,Id> rtmap = dq.getRecordTypes(); 
       
       // Collect a list of all Master Contacts with name or title changes in trigger
       Map<Id,Contact> MasterContacts = new Map<Id,Contact>();
              
       for (integer i=0;i<Trigger.size;i++) {
          Contact newC = Trigger.new[i];
       //   Contact oldC = Trigger.old[i];
          if (newC.RecordTypeId == rtmap.get('Master Clinician') || newC.RecordTypeId == rtmap.get('Master Non Clinician')) {              
             MasterContacts.put(newC.Id,newC);
          }
       }
       
       if (MasterContacts.values().size() > 0) {
          List<Contact> ContactsForUpdate = [select Id, FirstName, LastName, Title, Salutation,
                                                  Suffix__c, Contact_Photograph__c, Phonetic_Pronunciation__c,
                                                  TranslatedName__c, NPI_Code__c, State_Region_License__c, Specialty1__c,
                                                  Specialty_2__c, Specialty_3__c, Type__c, Gender__c, MobilePhone,
                                                  LinkedIn_Profile__c, Preferred_Contact_Method__c, Contact_Character_Profile__c,
                                                  Contact_Segment__c, Contact_Motivations__c, MaritalStatus__c, PreferredLanguage__c,
                                                  LanguagesKnown__c, Hobbies__c, Birthdate, Master_Contact_Record__c, Website__c,
                                                  Personal_Email__c, /*Email,*/ OtherPhone,KOL_Status__c,KOL_Type__c, KOL_Procedures__c, 
                                                  Agreed_on_PIPA__c, Request_EbD_KOL__c, Request_EMID_KOL__c, Request_RMS_KOL__c, 
                                                  Request_STI_KOL__c, Request_VT_KOL__c
                                             from Contact
                                             where Master_Contact_Record__c in :MasterContacts.keyset()];
                                      
          if (ContactsForUpdate.size() > 0) {
             List<Contact> ContactsUpdated = new List<Contact>();
             
             for (contact cc : ContactsForUpdate) {
                Contact mc = MasterContacts.get(cc.Master_Contact_Record__c); 
                System.debug('Calling ReplicateContact...');
                if (dq.ReplicateContact(mc,cc)) {
                   System.debug('Contact Replicated...');
                   ContactsUpdated.add(cc);
                }
             }   
          
             
             if (ContactsUpdated.size() > 0) {
                update ContactsUpdated;
             }
          }
       }
    }

       
  
 }