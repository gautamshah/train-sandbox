/****************************************************************************************
 * Name    : AccountTrigger 
 * Author  : Bill Shan
 * Date    : 22/09/2014 
 * Purpose : OpportunityLineItem Trigger Entry
 * Dependencies: OpportunityLineItem Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
trigger OpportunityProductTrigger on OpportunityLineItem (before insert, before update, after insert, after update) {

	OpportunityProduct_TriggerHandler handler = new OpportunityProduct_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isBefore && Trigger.isInsert)
	{
		handler.OnBeforeInsert(trigger.new);
	}
	
	if(Trigger.isBefore && Trigger.isUpdate)
	{
		handler.OnBeforeUpdate(trigger.new, trigger.oldMap);
	}
	
	if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate) && !OpportunityProduct_TriggerHandler.hasLineItemUpdated())
	{
		handler.setAllLost(Trigger.new);
		OpportunityProduct_TriggerHandler.setLineItemUpdated();
	}
    
}