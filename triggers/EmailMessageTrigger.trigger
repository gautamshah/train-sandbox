/****************************************************************************************
 * Name    : EmailMessageTrigger 
 * Author  : Bill Shan
 * Date    : 14/1/2015 
 * Purpose : EmailMessage Trigger Entry
 * Dependencies: Case & EmailMessage Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
trigger EmailMessageTrigger on EmailMessage (before insert, after insert) {
    
    EmailMessage_TriggerHandler handler = new EmailMessage_TriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }
}