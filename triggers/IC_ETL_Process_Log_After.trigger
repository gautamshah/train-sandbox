/**
trigger on IC_ETL_Process_Log__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-05-22      Yuli Fintescu		Created. 
									Creating a "Controller" log type entry triggers something to run.
2015-08-12		Bill Shan			Not execute batch jobs while testing
===============================================================================
*/
trigger IC_ETL_Process_Log_After on IC_ETL_Process_Log__c (after insert) {
	//expected to be one record. if more than one, suspect data loading
	if (Trigger.New.size() > 1)
		return;
	
	IC_ETL_Process_Log__c log = Trigger.New[0];
	if (log.Log_Type__c == 'Controller') {
		if (log.Operation__c == 'IC_Batch_Territory' && !Test.isRunningTest()) {
			IC_Execute_Territory_Deletion.execute();
		} 
		
		else if (log.Operation__c == 'IC_Batch_Territory_Relation') {
	        IC_Batch_Territory_Relation p = new IC_Batch_Territory_Relation();
            
            if(!Test.isRunningTest())
				String cronid = Database.executeBatch(p, 1);
		} 
		
		else if (log.Operation__c == 'Abort Job') {
			System.abortJob(log.Message__c);
		} 
		
		else if (log.Operation__c == 'Schecule IC_Batch_Territory_Relation') {
			IC_Batch_Territory_Relation_Schedule m = new IC_Batch_Territory_Relation_Schedule();
			String schedule = log.Message__c == null ? '0 30 * * * ?' : log.Message__c;
			String schedulename = log.Additional_Info__c == null ? 'Establish Territory Relationship US' : log.Additional_Info__c;
            if(!Test.isRunningTest())
				String cronid = system.schedule(schedulename, schedule, m);
		} 
		
		else if (log.Operation__c == 'Unschecule Task') {
			System.abortJob(log.Message__c);//String SCHEDULE_NAME = 'test'; id cronid = System.schedule(SCHEDULE_NAME, '0 15 0-23 * * ?', new scheduledMaintenance()); System.abortJob(cronid);
		}
	}
}