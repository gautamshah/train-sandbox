trigger ERPAccountSalesRollup on ERP_Account__c (after update) {
/****************************************************************************************
    * Name    : ERP Account Sales Rollup
    * Author  : Mike Melcher
    * Date    : 12-21-2011
    * Purpose : Update the Gran Total 12 Month Sales field on related Accounts any time an
    *           ERP Account is created or modified
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 03-30-2012   M. Melcher    Update trigger to only re-calculate Account level sales totals
    *                            when the Sell To Account for an ERP Record changes
    * 05-10-2012   M. Melcher    Updated code to reference new names for RMS totals on ERP Record and Account
    * 05-14-2012   MJM           Bug fix for VT and Supplies totals
    * 05-16-2012   MJM           Prevent execution of logic if user has API Data Loader Profile
    *****************************************************************************************/

    //Code commented as trigger is inactive
    
    /*Id dataloaderProfile;
    Id SystemAdminProfile;

    Id currentUser = UserInfo.getUserId();

    for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader' or Name='System Administrator']) {
       if (p.Name == 'API Data Loader') {
          dataloaderProfile = p.Id;
       }
 
    }

    if (UserInfo.getProfileId() != dataloaderProfile) {
        Set<ID> AccountIds = new Set<ID>();
        Map<Id,Account> AccountMap = new Map<Id,Account>();
        Map<Id,Decimal> S2Map = new Map<Id,Decimal>();
        Map<Id,Decimal> RMSMap = new Map<Id,Decimal>();
        Map<Id,Decimal> SuppliesMap = new Map<Id,Decimal>();
        Map<Id,Decimal> VTMap = new Map<Id,Decimal>();
     
        for (integer i=0; i<Trigger.size; i++) {
              if (Trigger.new[i].Parent_Sell_To_Account__c != null) {
                 if (Trigger.old[i].Parent_Sell_To_Account__c == null) {
                    AccountIds.add(Trigger.new[i].Parent_Sell_To_Account__c);
                
                 } else if (Trigger.new[i].Parent_Sell_To_Account__c != Trigger.old[i].Parent_Sell_To_Account__c) {
                    AccountIds.add(Trigger.new[i].Parent_Sell_To_Account__c);
                    AccountIds.add(Trigger.old[i].Parent_Sell_To_Account__c);
                 }
              }
        }
          
        if (AccountIds.size() > 0) {
           for (Account a : [select Id, S2_Total_Prior_12_Mos_Sales__c, RMS_Total_Prior_12_Mos_Sales__c,
                                    VT_Total_Prior_12_Mos_Sales__c, Supplies_Total_Prior_12_Mos_Sales__c
                             from Account
                             where Id in :AccountIds]) {
              AccountMap.put(a.Id, a);
              S2Map.put(a.Id, 0);
              RMSMap.put(a.Id, 0);
              VTMap.put(a.Id, 0);
              SuppliesMap.put(a.Id, 0);
           }
           System.debug('Maps initialized, updating...');
           for (ERP_Account__c erp : [select Id, Parent_Sell_To_Account__c, S2_Total_Prior_12_Mos_Sales1__c,
                                      RMS_Total_Prior_12_Mos_Sales1__c, VT_Total_Prior_12_Mos_Sales__c, 
                                      Supplies_Total_Prior_12_Mos_Sales__c
                                      from ERP_Account__c
                                      where Parent_Sell_To_Account__c in :AccountIds]) {
              if (erp.S2_Total_Prior_12_Mos_Sales1__c != null) {
                 S2Map.put(erp.Parent_Sell_To_Account__c,S2Map.get(erp.Parent_Sell_To_Account__c) + erp.S2_Total_Prior_12_Mos_Sales1__c);
              }
              if (erp.RMS_Total_Prior_12_Mos_Sales1__c != null) {
                 RMSMap.put(erp.Parent_Sell_To_Account__c,RMSMap.get(erp.Parent_Sell_To_Account__c) + erp.RMS_Total_Prior_12_Mos_Sales1__c);
              }
              if (erp.VT_Total_Prior_12_Mos_Sales__c != null) {
                 VTMap.put(erp.Parent_Sell_To_Account__c,VTMap.get(erp.Parent_Sell_To_Account__c) + erp.VT_Total_Prior_12_Mos_Sales__c);
              }
              if (erp.Supplies_Total_Prior_12_Mos_Sales__c != null) {
                 SuppliesMap.put(erp.Parent_Sell_To_Account__c,SuppliesMap.get(erp.Parent_Sell_To_Account__c) + erp.Supplies_Total_Prior_12_Mos_Sales__c);
              }
       
           }
           System.debug('Maps updated, updating Accounts...');
           for (Account a : AccountMap.values()) {
              System.debug('Updating for account ' + a.Id);
              a.S2_Total_Prior_12_Mos_Sales__c = S2Map.get(a.Id);
              a.RMS_Total_Prior_12_Mos_Sales__c = RMSMap.get(a.Id);
              a.VT_Total_Prior_12_Mos_Sales__c = VTMap.get(a.Id);
              a.Supplies_Total_Prior_12_Mos_Sales__c = SuppliesMap.get(a.Id);
          
           } 
           System.debug('Updating Account sales totals...');
           if (AccountMap.values().size() > 0) {
              update AccountMap.values();
           }
        }
     }*/
}