trigger ManageAccountTerritoriesTrigger on Account_Territory__c (before insert, after update) {
    /****************************************************************************************
    * Name    : ManageAccountTerritoriesTrigger
    * Author  : Suchin Rengan
    * Date    : 06-05-2011
    * Purpose : This trigger is used for associating an Account to an appropriate territory.
    * 
    * Dependancies: AccountShare
    *              ,Account_Territory__c
    *              ,Opportunity    
    *              ,Group
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 06-05-2011  Suchin Rengan        Created
    *
    *****************************************************************************************/
    Map<Id, Id> TerritoryGroupIdsMap = new Map<Id, Id>();
    List<AccountShare> newAccountShares = new List<AccountShare>();
    List<AccountShare> removeAccountShares = new List<AccountShare>();
    List<Account_Territory__c> AcctTerritoryTransferred = new List<Account_Territory__c>();
    Map<String,Account_Territory__c> AcctGroupStringMap = new Map<String,Account_Territory__c>();
    Map<String,Account_Territory__c> AcctIdAcctTerritoryMap = new Map<String,Account_Territory__c>();
    Set<Id> AccountShareIds = new Set<Id>();
    List<Opportunity> OpptyUpdates = new List<Opportunity>();
    Set<String> TerritoryIds = new Set<String>();
    Set<String> AccountTerritoryIds = new Set<String>();
     
    // process all the new relationships
    for(Account_Territory__c acctTerr: Trigger.new){
        if(acctTerr.Territory_ID__c!= null)
            TerritoryIds.add(acctTerr.Territory_ID__c); 
            
        if(acctTerr.PreviousTerritory__c != null)
        {
            AcctIdAcctTerritoryMap.put(acctTerr.AccountID__c,acctTerr);
            TerritoryIds.add(acctTerr.PreviousTerritory__c);
            AcctTerritoryTransferred.add(acctTerr);
            String key = acctTerr.AccountID__c;
            key = key + acctTerr.PreviousTerritory__c;
            AccountTerritoryIds.add(key);
        }
    }
    
    //collect all the territory groups for the Territories in the trigger's Account_Territory__c records
    for(Group grp:[select Id, Type, RelatedId 
                     from Group 
                    where Type='Territory' 
                      and RelatedId in :TerritoryIds])
    {
        TerritoryGroupIdsMap.put(grp.RelatedId, grp.Id);
    }
    
    if(Trigger.isInsert)
    {
        for(Account_Territory__c acctTerr: Trigger.new)
        {
            //create shares for the account terrirory records using the group objects mapping by territory
            AccountShare acctShare = new AccountShare();
            if(acctTerr.Territory_ID__c != null)
                acctShare.UserOrGroupId = TerritoryGroupIdsMap.get(acctTerr.Territory_ID__c);
            
            // Also handle for cases if there is a previous territory ids
            // Change all oppty related to Accounts from one territory to another
            String key = acctTerr.AccountID__c;
            key = key + TerritoryGroupIdsMap.get(acctTerr.Territory_ID__c);
            AcctGroupStringMap.put(key,acctTerr);
            acctShare.AccountId = acctTerr.AccountID__c;
            newAccountShares.add(acctShare);
            key='';
        }
        
        // Get the newly inserted AccountShares to update the Account Territory record with the corresponding 
        // Account Share ids
        Database.SaveResult[] srs = Database.insert(newAccountShares);
        for(Integer i=0; i<srs.size(); i++){
            Database.SaveResult sr = srs[i];
            AccountShareIds.add(sr.getId());
        }
        for(AccountShare acctShare:[select Id, AccountId, UserOrGroupId 
                                      from AccountShare 
                                     where Id IN:AccountShareIds 
                                       AND isDeleted=false]){
            String key = acctShare.AccountId;
            key = key + acctShare.UserOrGroupId;
            Account_Territory__c acctTerritory = AcctGroupStringMap.get(key);
            acctTerritory.AccountShareId__c = acctShare.Id;
            key = '';
        }
        for(Opportunity oppty: [select Id, AccountId, StageName, AccountTerritoryId__c 
                                  from Opportunity 
                                 where AccountTerritoryId__c IN: AccountTerritoryIds 
                                   AND StageName != 'Closed Won']){
            // find all opportunities that are part of this Account + Territory
            // Would be great if the Opportunity had a custom field that is indexed
            // And that has a Account+TerritoryId combination
            // Easy to find the impacted opportunities
            // Now this can be done as part of an After insert
            Account_Territory__c acctTerr = AcctIdAcctTerritoryMap.get(oppty.AccountId);
            oppty.TerritoryId = acctTerr.Territory_ID__c;
            OpptyUpdates.add(oppty);
        } 
        update OpptyUpdates;
    }
    if(Trigger.isUpdate){
        for(Account_Territory__c acctTerr: Trigger.new){
            AccountShare acctShareRemove = new AccountShare();
            if(acctTerr.Delete__c)
                if(acctTerr.AccountShareId__c != null)
                    AccountShareIds.add(acctTerr.AccountShareId__c);
                else {
                    // get the Account Share Ids from doing a SOQL query
                }
                    
        }
        for(AccountShare acctShare:[select Id 
                                      from AccountShare 
                                     where Id IN:AccountShareIds 
                                       AND isDeleted=false]){
            removeAccountShares.add(acctShare);
        }
        delete removeAccountShares;
    }
}