/**
trigger on Proposal_Distributor__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-01-05      Yuli Fintescu		Created. Check/Uncheck Distributors_Selected_Template__c box based on if any Proposal_Distributor__c records existing on proposal
===============================================================================
*/
trigger CPQ_Proposal_Distributor_After on Proposal_Distributor__c (after insert, after delete, after undelete) {
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Proposal_Distributor_After__c == true) {
		return;
    }
    
    //Update Distributors_Selected_Template__c if the proposal has/has not distributors
	Set<ID> candidates = new Set<ID>();
	if (Trigger.isInsert || Trigger.isUndelete) {
		for(Proposal_Distributor__c r : Trigger.new) {
			candidates.add(r.Proposal__c);
		}
	} else if (Trigger.isDelete) {
		for(Proposal_Distributor__c r : Trigger.old) {
			candidates.add(r.Proposal__c);
		}
	}
	
	CPQ_ProposalProcesses.UpdateDistributorSelected(candidates);
}