trigger MilestoneCompletion_trigger on Case (before update) {
List<ID> milestones=new List<ID>();
MilestoneCompletion sf=new MilestoneCompletion();
public ID casemilestoneid;
List<ID> caseid_BA=new List<ID>();
List<ID> caseid_AS=new List<ID>();
List<ID> caseid_DV=new List<ID>();
List<ID> caseid_QA=new List<ID>();
List<ID> caseid_UT=new List<ID>();
List<ID> caseid_DP=new List<ID>();
List<ID> caseid=new List<ID>();
List<ID> allid=new List<ID>();

for(Case checkrtype : Trigger.New)
{
if(checkrtype.RecordTypeID == '012U0000000LrOB') // Check Record Type if CRM SUPPORT then process else not
{

for(case c:trigger.new)
{ 

if((c.status=='In Progress'&& trigger.old[0].status!=trigger.new[0].status)||c.status=='Awaiting Response/Dependancy'|| c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{
caseid.add(c.id);
}

group o=new group(); //creating group object
      o= [SELECT Id, Name FROM Group WHERE Name = 'CoE - Support'];  
    
 if((c.Ownerid==o.id && trigger.old[0].Ownerid!=trigger.new[0].Ownerid) || (c.Functional_Role__c=='CoE Technical Review' && trigger.old[0].Functional_Role__c!=trigger.new[0].Functional_Role__c) || c.status=='Awaiting Response/Dependancy'|| c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{

caseid_BA.add(c.id);
}

if((c.Stage__c=='Development' && trigger.old[0].Stage__c!=trigger.new[0].Stage__c)|| c.status=='Awaiting Response/Dependancy'|| c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{
 caseid_AS.add(c.id);
}

 if((c.Functional_Role__c=='Quality Review' && trigger.old[0].Functional_Role__c!=trigger.new[0].Functional_Role__c) || c.status=='Awaiting Response/Dependancy' || c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{
 caseid_DV.add(c.id);
}

if((c.Functional_Role__c=='Test / Validation Ready'  && trigger.old[0].Functional_Role__c!=trigger.new[0].Functional_Role__c)|| c.status=='Awaiting Response/Dependancy'|| c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{
 caseid_QA.add(c.id);
}

if((c.Functional_Role__c=='Ready for Production' && trigger.old[0].Functional_Role__c!=trigger.new[0].Functional_Role__c) || c.status=='Awaiting Response/Dependancy'|| c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{
 caseid_UT.add(c.id);
}

if(c.status=='Awaiting Response/Dependancy' || c.status=='Closed-Duplicate' || c.status=='Closed-Resolved' || c.status=='Closed-Transferred')
{
 caseid_DP.add(c.id);
}
allid.addall(caseid);
allid.addall(caseid_BA);
allid.addall(caseid_AS);
allid.addall(caseid_DV);
//allid.add(caseid_DV);
allid.addall(caseid_QA);
allid.addall(caseid_UT);
allid.addall(caseid_DP);

for(CaseMilestone cm :[select id from casemilestone where caseid in : allid and IsCompleted=false])
{
milestones.add(cm.id);
}
 System.debug('++++++' + milestones);
}


if(caseid.size()>0)
{

if(!milestones.isEmpty())
{

    DateTime completionDate = System.now();
    sf.updatemilestone(milestones[0],completionDate);
          
 } 
System.debug('*****' + caseid );
}


if(caseid_BA.size()>0)
{

if(!milestones.isEmpty())
{
  DateTime completionDate = System.now();
  sf.updatemilestone(milestones[0],completionDate);
          
}  

System.debug('*****' + caseid_BA );
}

if(caseid_AS.size()>0)
{

if(!milestones.isEmpty())
{
 DateTime completionDate = System.now();
sf.updatemilestone(milestones[0],completionDate);
  }

System.debug('*****' + caseid_AS );
}


if(caseid_DV.size()>0)
{

if(!milestones.isEmpty())
{
DateTime completionDate = System.now();
sf.updatemilestone(milestones[0],completionDate);
   }
System.debug('*****' + caseid_DV );
}

if(caseid_QA.size()>0)
{

if(!milestones.isEmpty())
{
DateTime completionDate = System.now();
sf.updatemilestone(milestones[0],completionDate);
  
}

System.debug('*****' + caseid_QA );
}


if(caseid_UT.size()>0)
{
if(!milestones.isEmpty())
{
DateTime completionDate = System.now();
sf.updatemilestone(milestones[0],completionDate);
   }
//}
System.debug('*****' + caseid_UT );

if(caseid_DP.size()>0)
{
if(!milestones.isEmpty())
{
DateTime completionDate = System.now();
sf.updatemilestone(milestones[0],completionDate);
    }
//}
System.debug('*****' + caseid_DP );
}
}
}
}
}