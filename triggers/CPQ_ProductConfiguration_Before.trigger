/**
trigger on Apttus_Config2__ProductConfiguration__c.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-01-05      Yuli Fintescu       Created. calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
2014-01-21      Yuli Fintescu       Updated. copy coop fields from proposal to configuration when create
===============================================================================
*/
trigger CPQ_ProductConfiguration_Before on Apttus_Config2__ProductConfiguration__c (before insert, before update) {
	/* Moved to cpqProductConfiguration
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_ProductConfiguration_Before__c == false) {
        //copy coop fields from proposal to configuration
        if (Trigger.isInsert) {
            CPQ_ProposalProcesses.UpdateConfig(Trigger.new);
        }
    }
    
    //calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
    for (Apttus_Config2__ProductConfiguration__c p : trigger.New) {
        p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);
    }
    */
}