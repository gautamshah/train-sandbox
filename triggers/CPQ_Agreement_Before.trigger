/**
trigger on CPQ_Agreement_Before.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11-21      Yuli Fintescu       Created. Update record when activated
2014-01-12      Yuli Fintescu       Updated. calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
2014-01-30      Yuli Fintescu       Updated. Update approval status to Not Submitted from Cancelled
                                            Cancel Agreement Approvals button on Approve related list sets approval status to Cancelled.
2015-02-22      Yuli Fintescu       Updated. Update approval status to Not Submitted after approved if any fields are changed
02/23/2016      Paul Berglund       Logic moved to Apttus_Agreement_Trigger_Manager
===============================================================================
*/
trigger CPQ_Agreement_Before on Apttus__APTS_Agreement__c (before insert, before update) {
/*
    //calculate duration_month__c based on duration__c field. unable to use formula field for duration_month__c, hitting over 5000 chars limit
    for (Apttus__APTS_Agreement__c p : trigger.New) {
        p.Duration_month__c = CPQ_Utilities.calculateDurationInMonth(p.Duration__c);
    }
    
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Agreement_Before__c == true) {
        return;//If this is the integration user then exit
    }
    
    if (Trigger.isUpdate) {
        //Update record when activated
        CPQ_AgreementProcesses.UpdateRecordType(Trigger.new, Trigger.oldMap);
        
        //Update approval status to Not Submitted from Cancelled
        CPQ_AgreementProcesses.UpdateStatusWhenCancelled(Trigger.new, Trigger.oldMap);
        
        //Update approval status to Not Submitted after approved if any fields are changed
        CPQ_AgreementProcesses.ResetApprovalStatusWhenFieldChanged(Trigger.new, Trigger.oldMap);
        
        //Update pricelist and expiration date on rms agreement direct
        CPQ_AgreementProcesses.RMSAgreementDirect(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
    } else if (Trigger.isInsert) {
        //Update pricelist and expiration date on rms agreement direct
        CPQ_AgreementProcesses.RMSAgreementDirect(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
    }
*/
}