trigger FieldVisitCreateShares on Field_Visit__c (after insert, after update) {
 /****************************************************************************************
    * Name    : FieldVisitCreateShares
    * Author  : Mike Melcher
    * Date    : 2-7-2012
    * Purpose : Automatically create and update sharing on a Field Visit record for
    *           the Manager and the Sales Rep.
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    *
    *****************************************************************************************/
   
   Map<Id,Set<Id>> SharesToCreate = new Map<Id,Set<Id>>();
   List<Field_Visit__Share> SharesForInsert = new List<Field_Visit__Share>();
   
   if (Trigger.isInsert) {
   
   // Add all Managers and SalesReps to map of Shares To create
   
      for (Field_Visit__c fv : Trigger.new) {
         Set<Id> newIds = new Set<Id>();
         if (fv.Manager__c != null && Userinfo.getUserId() != fv.Manager__c) {
            newIds.add(fv.Manager__c);
         }
         if (fv.Sales_Rep__c != null && Userinfo.getUserId() != fv.Sales_Rep__c) {
            newIds.add(fv.Sales_Rep__c);
         }
         if (newIds.size() > 0) {
            SharesToCreate.put(fv.Id,newIds);
         }
      }
   
   } else {  // Trigger is Update
      
      for (integer i=0;i<Trigger.size;i++) {
         // If either the manager or the sales rep has changed add the old info to a list
         // of shares to delete, and the new infor to a list of shares to create
         Set<Id> addIds = new Set<Id>();
         if (Trigger.old[i].Manager__c != Trigger.new[i].Manager__c && Userinfo.getUserId() != Trigger.new[i].Manager__c) {
            addIds.add(Trigger.new[i].Manager__c);
         }
         if (Trigger.old[i].Sales_Rep__c != Trigger.new[i].Sales_Rep__c && Userinfo.getUserId() != Trigger.new[i].Sales_Rep__c) {
            addIds.add(Trigger.new[i].Sales_Rep__c);
         }
         if (addIds.size() > 0) {
            SharesToCreate.put(Trigger.New[i].Id, addIds);
         }
      }

   }
   
   if (SharesToCreate.keyset().size() > 0) {
      for (Id fvID : SharesToCreate.keyset()) {
         for (Id userId : SharesToCreate.get(fvId)) {
            Field_Visit__Share s = new Field_Visit__Share();
            s.parentId = fvId;
            s.UserOrGroupId = userId;
            s.RowCause = 'Manual';
            s.AccessLevel = 'Edit';
            sharesForInsert.add(s);
         }
      }
   }

      
   if (SharesForInsert.size() > 0) {
      insert SharesForInsert;
   }
   
   }