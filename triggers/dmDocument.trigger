trigger dmDocument on dmDocument__c
    (
        after delete,
        after insert,
        after undelete,
        after update,
        before delete,
        before insert,
        before update
    )
{
	system.debug('dmDocument - Entry: ' + limits.getQueries());

    dmDocumentTriggerDispatcher.main(
            trigger.isExecuting,
            trigger.isInsert,
            trigger.isUpdate,
            trigger.isDelete,
            trigger.isBefore,
            trigger.isAfter,
            trigger.isUndelete,
            trigger.new,
            trigger.newMap,
            trigger.old,
            trigger.oldMap,
            trigger.size);

	system.debug('dmDocument - Entry: ' + limits.getQueries());
}