/**
trigger on Participating_Facility__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-01-05      Yuli Fintescu		Created. reset ERP related list for proposal. 
										Load all ERPs of Primary account and facilities in proposal
===============================================================================
*/
trigger CPQ_Participating_Facility_After on Participating_Facility__c (after insert, after delete, after undelete) {
	if (CPQ_Trigger_Profile__c.getInstance().CPQ_Participating_Facility_After__c == true) {
		return;
    }
    
    //reset ERP related list for proposal. 
	Set<ID> candidates = new Set<ID>();
	if (Trigger.isInsert || Trigger.isUndelete) {
		for(Participating_Facility__c r : Trigger.new) {
			candidates.add(r.Proposal__c);
		}
	} else if (Trigger.isDelete) {
		for(Participating_Facility__c r : Trigger.old) {
			candidates.add(r.Proposal__c);
		}
	}
	
	CPQ_ProposalProcesses.LoadERPAddresses(candidates);
}