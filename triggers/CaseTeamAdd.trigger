trigger CaseTeamAdd on Case (after insert) {
/****************************************************************************************
    * Name    : CaseTeamAdd
    * Author  : Brajmohan Sharma
    * Date    : 28-Feb-2017
    * Purpose : To add Requested By and Manager of Requested By in case team when case is created.
    * case    : 00963024
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
    ****************************************************************************************/ 
    
    public List<CaseTeamMember> ctm = new List<CaseTeamMember>();     
    public List<CaseTeamRole> ctrole = [SELECT Name, Id FROM CaseTeamRole];  //fetch all given role as a CaseTeamRole
    public Map<String,String> caseteamr = new Map<String,String>{};
    
    for(CaseTeamRole ct:ctrole){
    caseteamr.put(ct.Name, ct.Id);}
    Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();

     for(Case c:Trigger.new){  
      if(rt_map.get(c.recordTypeID).getName().containsIgnoreCase('Sales Inquiry PR') ||  rt_map.get(c.recordTypeID).getName().containsIgnoreCase('Sales Inquiry SI')){
         //if(c.RecordTypeId == '0126C00000009I7' || c.RecordTypeId == '0126C00000008n4'){  //record type check Sales Inquiry SI/Sales Inquiry PR
         CaseTeamMember ctmadd = new CaseTeamMember();      //add details about Requested By
            ctmadd.ParentId = c.id;
            ctmadd.MemberId = c.Requested_By__c;
            ctmadd.TeamRoleId = caseteamr.get('Sales Rep');
            CaseTeamMember ctmadd1 = new CaseTeamMember();     //add details about Manager Of Requested By
            ctmadd1.ParentId = c.id;
            ctmadd1.MemberId = c.Manager_of_Requested_By__c;
            ctmadd1.TeamRoleId = caseteamr.get('Sales Manager');
            if(ctmadd.MemberId != ctmadd1.MemberId){  // CaseTeam can't contain duplicate member
                ctm.add(ctmadd1);ctm.add(ctmadd);
                system.debug('value inside ctm1'+ctm.size());
               }else{
                ctm.add(ctmadd1);
                system.debug('value inside ctm1'+ctm.size());
               }
                system.debug('value inside ctm1'+ctm.size());
          }//if
      }//for
             if(ctm.size()>0){  //list size check to perform insert dml operation
                insert ctm; 
                }//if
   }//trigger