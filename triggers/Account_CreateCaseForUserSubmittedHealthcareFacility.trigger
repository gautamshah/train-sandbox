/*
Author            Date            Changes
Amogh Ghodke     15 March 2016  Added If for ASIA_Healthcare_Facility
Bryan Fry        31 Aug 2016    Added static record type to help cut down on SOQL needed from test code that calls this
*/
trigger Account_CreateCaseForUserSubmittedHealthcareFacility on Account (after insert) 
{
    List<Case> caseList = new List<Case>();
    Map<String,String> acctRTMap = DataQuality.getAccountRTMap();
    Id currentUser = UserInfo.getUserId();
    String userString = UserInfo.getUserId();
    String userName = UserInfo.getName();
    string region = DataQuality.getRegion();
    string country = DataQuality.getCountry();
    String contactName = userString.substring(0, 15);
    DataQuality dq = new DataQuality();
    
    for(Account a : Trigger.New)
    {
        if(a.RecordTypeId == acctRTMap.get('User_Submitted_Healthcare_Facility'))
        {
            //dq.createCase('New User Submitted Healthcare Facility',a.Id,'Data Quality','Medium', currentUser,'Validation','Assigned', 'Case', 'User Submitted Healthcare Facility');
            dq.createCase('New User Submitted Healthcare Facility', a.Id, 'Validation', 'Medium', currentUser, 'Data Quality', 'Assigned', 'Case', 'User Submitted Healthcare Facility');
        }
        
        if(a.RecordTypeId == acctRTMap.get('ASIA_Healthcare_Facility') && region == 'APAC-ASIA' && country == 'KR')
        {
            dq.createCaseForKR('Account Creation Case of Korea', a.Id, 'Validation', 'Medium', currentUser, 'Data Quality', 'Assigned', 'Case', 'Account Creation Case',a.Id,a.Name);        
        }
        
    }
    insert caseList;
}