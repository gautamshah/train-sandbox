/****************************************************************************************
 * Name    : StatusbacktoNoShow
 * Author  : Lakhan Dubey
 * Date    : 23/04/2015 
 * Purpose : to change record status back to "No Show" if the previous value was "No Show" after status pick list changed & updated with "closed" with final approval
 * Dependencies: Training__c 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               -------
 *****************************************************************************************/

trigger StatusbacktoNoShow on Training__c (before update) {
for(Training__c tt:trigger.new)
{
if(tt.IsOuttafinalApproval__c && trigger.oldMap.get(tt.id).Status__c=='No show')
tt.Status__c='No show';
}
}