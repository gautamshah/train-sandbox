trigger ManageReportOfInActiveUser on User (before update,after update) {
/****************************************************************************************
    * Name    : ManageReportOfInActiveUser
    * Author  : Brajmohan Sharma
    * Date    : 22-Mar-2017
    * Purpose :
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                       CHANGE
    * ----        ------                      --------
****************************************************************************************/ 
 
     Public List<USER> activenew;
     Public List<USER> activeold;
     //List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
     if(Test.isRunningTest() || !(userinfo.getProfileId() =='00eU0000000dLrtIAE')){  //data loader using check
     activenew = [select isActive from User where id in : Trigger.newmap.Keyset()]; //fetching list of update user
     activeold = [select isActive from User where id in :Trigger.Oldmap.Keyset()];  //fetching list of updating user
     System.debug('Size of trigger new state'+activenew.size()+'size of trigger old state'+activeold.size());
  for(User u : [select id,name,FirstName,isActive from user where id in : Trigger.new]){ 
     system.debug('inside loop for all updating user');     //loop for all updating user
     //actions for user who converting from active to deactive
     system.debug(activeold.get(0).isActive+'user being deactive'+u.isActive);
  if(u.isActive ==false){
     system.debug(activeold.get(0).isActive+'user being deactive'+u.isActive);
     //check for every user that contain run report or not
  if(ManageReportOfInActiveUserC.checkReportAvail(u.id)){
     List<CronTrigger> jobs = ManageReportOfInActiveUserC.getJobs(u.id);
     System.debug('before sdlJobInsert'+jobs.size());
     //job details persist into ScheduleRunReport object
     ManageReportOfInActiveUserC.sdlJobInsert(u.id);
     System.debug('after sdlJobInsert'+jobs.size());    
     system.debug(u.FirstName+' have '+jobs.size()+' report jobs');
  }
  else{
    //user that havent run report
    system.debug(u.FirstName+'havent reportjob');
  }//if - else on checkReportAvail
  } //exit if active state from true to false
  }//exit for
    //send mail to user who converting in active state from inactive state due to update
  if(Trigger.isAfter){
     Map<ID,User> Name = new Map<ID,USER>([select id,name from user where id in : Trigger.newmap.Keyset()]);
      List<SheduleRunReport__c> srr = [select RUserId__c,Name,CreatedUserName__c,EndDate__c,StartDate__c,CronExpression__c,NextFireDate__c from SheduleRunReport__c];
      system.debug('inside after trigger');  
  for(User u : Trigger.new){
  for(User u1 : Trigger.old){
     system.debug('state of new user in after trigger'+u.IsActive+'state of old user in after trigger'+u1.IsActive);
  if(u.IsActive == true && u1.isactive == false){  //check of active state from inactive
     system.debug('inside after trigger'+u.FirstName);
     String uiddd = u.id;
     //List<User> Name = [select name from user where id =: uiddd];
     //List<SheduleRunReport__c> jobs = [select RUserId__c,Name,CreatedUserName__c,EndDate__c,StartDate__c,CronExpression__c,NextFireDate__c from SheduleRunReport__c where RUserId__c =: Name.get(u.id).name];
     List<SheduleRunReport__c> jobs = new List<SheduleRunReport__c>();
      for(SheduleRunReport__c job : srr) 
      {
          if(job.RUserId__c == Name.get(u.id).name)
              jobs.add(job);
      }
     system.debug('size of job list inside after trigger'+jobs.size());
  if(jobs.size()>0)
     ManageReportOfInActiveUserC.SendMailToSheduleReport(uiddd);
     system.debug('size of all jobs inside after trigger'+jobs.size()); 
  }//new trigger
  }//old trigger
  }//state changes in active
  }//isafter 
  }//custom setting check  
  } //Trigger