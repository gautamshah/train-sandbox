/**
Trigger for the Product_SKU__c object

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
04/11/2016      Bryan Fry           Created
===============================================================================
*/
trigger CPQ_Product_SKU_After on Product_SKU__c (after insert, after update) {
	// Set a lookup field from surgical products to related Product_SKU__c records
	CPQ_ProductProcesses.linkSurgicalProducts(Trigger.new);
}