trigger CCACaseTrigger on Case (after update) {
	CCATriggerHandler handler = new CCATriggerHandler();
	handler.NotifyParentCaseClosed(Trigger.new);
}