trigger PopulateAccountOnClinicalEvaluation on Clinical_Evaluation__c (before insert) {

Map<Id,Id> OppMap = new Map<Id,Id>();
Set<Id> OppsInTrigger = new Set<Id>();

for (Clinical_Evaluation__c cl : Trigger.new) {
   OppsInTrigger.add(cl.Opportunity_Name__c);
}

for (Opportunity o : [select Id, AccountId from Opportunity where Id in :OppsInTrigger]) {
   OppMap.put(o.Id, o.AccountId);
}

for (Clinical_Evaluation__c cl : Trigger.new) {
   cl.Account__c = OppMap.get(cl.Opportunity_Name__c);
}

}