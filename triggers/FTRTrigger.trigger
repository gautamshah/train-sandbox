/****************************************************************************************
 * Name    : FTRTrigger 
 * Author  : Bill Shan
 * Date    : 17/12/2012 
 * Purpose : FTR Trigger Entry
 * Dependencies: FTRTriggerHandler Apex Class
 *             , FTR Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
trigger FTRTrigger on Field_Technical_Report__c (before insert, before update) {
	
	FTRTriggerHandler handler = new FTRTriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.new);
    }
}