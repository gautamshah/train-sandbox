trigger EMS_Trigger on EMS_Event__c (after update,before insert,before update,after insert) {

     if(Trigger.isAfter){
            
         if(Trigger.isUpdate){
         
         
        
         Map<Id,EMS_Event__c> MEmsEvents = Trigger.newMap.deepClone();
         
         
         List<EMS_Event__c> LRejectedRecalledEVents = new List<EMS_Event__c>();
         List<Id> EMSFirstapprovedeCPAs = new List<Id>();
         Set<Id> EMSSecondapprovedeCPAs = new Set<Id>();
         List<id> EMSConcludedEVentIds= new List<id>();
         List<Id> EMSnewrewuestecpaIds= new List<Id>();
         List<Id> EMSPSUapprovedeCPAs = new List<Id>();
         List<Id> EMSPendingCCPcpaIds= new List<Id>();
         Set<Id> SEMSRejectedIds = new Set<Id>();
         List<EMS_Event__c> LEMSPendingCCPEvents = new List<EMS_Event__c>();
         List<EMS_Event__c> LEMSRejectedCCPEvents = new List<EMS_Event__c>();
         //List<Id> EMSActiveEventIds = new List<Id>();
         
         List<EMS_Event__Share> LEMSUpdateShareRecords = new List<EMS_Event__Share>();
         Set<Id> EMSSharingDeleteIds = new Set<Id>();
         for(Id eachEMSEventId : MEmsEvents.keySet()){
         
         //Give Sharing access to Event Owner and Manager for ANZ
         if(((Trigger.oldMap.get(eachEMSEventId).Event_Owner_User__c!= MEmsEvents.get(eachEMSEventId).Event_Owner_User__c) || (Trigger.oldMap.get(eachEMSEventId).Manager1__c!= MEmsEvents.get(eachEMSEventId).Manager1__c)) && (MEmsEvents.get(eachEMSEventId).Owner_Country__c=='AU' || MEmsEvents.get(eachEMSEventId).Owner_Country__c=='NZ')){
                    System.debug('^^^^^^^^^^');
                    EMSSharingDeleteIds.add(eachEMSEventId);
                    EMS_Event__Share emsShrEventOwner  = new EMS_Event__Share();
                    emsShrEventOwner.ParentId = eachEMSEventId;
                    emsShrEventOwner.UserOrGroupId = MEmsEvents.get(eachEMSEventId).Event_Owner_User__c;
                    emsShrEventOwner.AccessLevel = 'Edit';
                    emsShrEventOwner.RowCause = Schema.EMS_Event__Share.RowCause.EMS_Event_Owner_Manager_Sharing_for_ANZ__c;
                    
                    LEMSUpdateShareRecords .add(emsShrEventOwner);
                    
                    EMS_Event__Share emsShrManager  = new EMS_Event__Share();
                    emsShrManager.ParentId = eachEMSEventId;
                    emsShrManager.UserOrGroupId = MEmsEvents.get(eachEMSEventId).Manager1__c;
                    emsShrManager.AccessLevel = 'Edit';
                    emsShrManager.RowCause = Schema.EMS_Event__Share.RowCause.EMS_Event_Owner_Manager_Sharing_for_ANZ__c;
                    LEMSUpdateShareRecords.add(emsShrManager);
                    System.debug('%%%%%%%%%%%'+LEMSUpdateShareRecords);
             
          }
                  
         
         
         /*
         Creating List to store Events whose eCPA has been cancelled or approved.
         */         
         if((Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='Completed' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='Completed' && Trigger.oldMap.get(eachEMSEventId).No_of_Approved_eCPAs__c !=Trigger.newMap.get(eachEMSEventId).No_of_Approved_eCPAs__c)||(Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='Cancelled' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='Cancelled')){
             EMSConcludedEVentIds.add(eachEMSEventId);
             }
             
         if(Trigger.oldMap.get(eachEMSEventId).Rejected_Recalled_flag__c ==false && Trigger.newMap.get(eachEMSEventId).Rejected_Recalled_flag__c==true )  {  
         
         LRejectedRecalledEVents.add(MEmsEvents.get(eachEMSEventId));
         
         }
         /*
           Creating List to store Events whose original eCPA has ben approved so that we can copy over the Recipients into Event Beneficiaries records with 
           Invitee Type = Initial Budgeted Beneficiary.
           
        */    
               
         if(Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='Completed' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='Completed' && Trigger.newMap.get(eachEMSEventId).No_of_Approved_eCPAs__c==1){
            EMSFirstapprovedeCPAs.add(eachEMSEventId);
            }
            
         /*
           Creating List to store latest approved eCPA's Beneficiaries      
        */ 
            
         if(Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='Completed' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='Completed' ){
            EMSSecondapprovedeCPAs.add(eachEMSEventId);
            }
        /*

        Creating List to store Events whose Pre-Event eCPA has been approved so that we can copy over the Recipients into Event Beneficiaries records with 
        Invitee Type = PSU Budgeted Beneficiary.
           
        */   
         
        if(Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='Completed' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='Completed' && Trigger.oldMap.get(eachEMSEventId).Pre_Event_SUPPL_CPA_Required__c==true && Trigger.oldMap.get(eachEMSEventId).Post_Event_SUPPL_CPA_Required__c!=true && Trigger.newMap.get(eachEMSEventId).No_of_Approved_eCPAs__c==2){
            EMSPSUapprovedeCPAs.add(eachEMSEventId);
            }
            
        /*
           Creating List to store Events who have been submitted for approval so that we can create Excel files for HCPs and Employees and 
           store them as attachments under EMS Event.
           
        */      
            
         if((Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='New Request' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='New Request')||(Trigger.oldMap.get(eachEMSEventId).CPA_Status__c !='Pending CCP Coordinator Approval' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c =='Pending CCP Coordinator Approval' && Trigger.newMap.get(eachEMSEventId).In_Country_CPA_Routing_USD_1000__c==true && Trigger.newMap.get(eachEMSEventId).Owner_Country__c=='MY')){
            EMSnewrewuestecpaIds.add(eachEMSEventId);
            }
            
          /*
           Creating List to store Events who have been declined at any stage  so that we can delete Excel files for HCPs and Employees  
           stored as attachments under EMS Event.
           
        */  
             
          if(Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!=null && !Trigger.oldMap.get(eachEMSEventId).CPA_Status__c.contains('Declined') && Trigger.newMap.get(eachEMSEventId).CPA_Status__c.contains('Declined')){
             SEMSRejectedIds.add(eachEMSEventId);
             LEMSRejectedCCPEvents.add(MEmsEvents.get(eachEMSEventId));
             
           }
           
         /*
            Logic to create map so that we can update links of Excel files created for HCPs/Employees on EMS Event
         
         */  
             
         if((Trigger.oldMap.get(eachEMSEventId).CPA_Status__c!='Pending CCP Coordinator Approval' && Trigger.newMap.get(eachEMSEventId).CPA_Status__c=='Pending CCP Coordinator Approval')){
             EMSPendingCCPcpaIds.add(eachEMSEventId);
             LEMSPendingCCPEvents.add(MEmsEvents.get(eachEMSEventId));
            }
         }
         
                 
         if(!EMSPendingCCPcpaIds.isEmpty()){
                 List<Attachment> Lattachments = new List<Attachment>([Select Id,Name,ParentId from Attachment where (name like 'Employees_Budgeted%' or name like 'Event_Invitees_Budgeted%') and ParentId in :(EMSPendingCCPcpaIds)]);
                 Map<String,Id> MAttachments = new Map<String,Id>();
         for(Attachment eachattachment : Lattachments){
             MAttachments.put(String.valueof(eachattachment.ParentId)+eachattachment.name,eachattachment.Id);         
         }
         System.debug('%%%%%'+Mattachments);
         for(EMS_Event__c eachEvent : LEMSPendingCCPEvents) {
         Integer num=Integer.valueOf(eachEvent.No_of_Approved_eCPAs__c+1);
         String ecpanum = 'eCPA-'+eachEVent.Owner_Country__c+'-'+eachEvent.Financial_Year__c+'-'+eachEvent.Auto_Number__c+'-'+'R'+String.valueof(num)+'.xls';    
         system.debug('---->'+eCPAnum);
         eachEvent.Budget_External_Recipient_URL__c=URL.getSalesforceBaseUrl().toExternalForm() +'/' +MAttachments.get(eachEvent.Id+'Event_Invitees_Budgeted-'+ecpanum);
         eachEvent.Budget_Internal_Recipient_URL__c=URL.getSalesforceBaseUrl().toExternalForm() +'/'+MAttachments.get(eachEvent.Id+'Employees_Budgeted-'+ecpanum);
         eachEvent.Notify_Special_EMails__c=true;
         }
         database.saveresult[] sr = Database.update(LEMSPendingCCPEvents,false);  
         system.debug('=====>'+sr);
         }
         
         //Incase,eCPA is rejected,delete the recently generated Excel file attachments and remove the references on EMS Event. 
         if(!SEMSRejectedIds.isEmpty()){
         Set<String> SDocNames = new Set<String>();
          for(EMS_Event__c eachEvent : LEMSRejectedCCPEvents){
          
              Integer num=Integer.valueOf(eachEvent.No_of_Approved_eCPAs__c+1);
              String ecpanum = 'eCPA-'+eachEVent.Owner_Country__c+'-'+eachEvent.Financial_Year__c+'-'+eachEvent.Auto_Number__c+'-'+'R'+String.valueof(num)+'.xls';    
              system.debug('---->'+eCPAnum);
              SDocNames.add('Event_Invitees_Budgeted-'+ecpanum);
              SDocNames.add('Employees_Budgeted-'+ecpanum);
              eachEvent.Budget_External_Recipient_URL__c=null;
              eachEvent.Budget_Internal_Recipient_URL__c=null;
              eachEvent.CPA_ref__c='eCPA-'+eachEVent.Owner_Country__c+'-'+eachEvent.Financial_Year__c+'-'+eachEvent.Auto_Number__c+'-'+'R'+String.valueof(num);
          
          }
         
          List<Attachment> Ldelattachments = new List<Attachment>([Select Id,Name,ParentId from Attachment where name in :(SDocNames)  and ParentId in :(SEMSRejectedIds)]);
          if(!Ldelattachments.isEmpty()){
          
          database.delete(Ldelattachments,true);
          database.update(LEMSRejectedCCPEvents);
          }
         
         }
         
         //Deleting the Sharing records for Changed Event Owner and Manager for ANZ and addingupdated ones.
         /*System.debug('!!!!!!!!!!!!!'+EMSSharingDeleteIds);
         List<EMS_Event__Share> LDeleteSharerecords = new List<EMS_EVent__Share>([Select Id,RowCause,ParentId from EMS_EVent__Share where ParentId in :(EMSSharingDeleteIds) AND Rowcause='EMS_Event_Owner_Manager_Sharing_for_ANZ__c' ]);
         
         System.debug('********'+LDeleteSharerecords);
         if(!LDeleteSharerecords.isEmpty()){
             
             database.delete(LDeleteShareRecords,false);
         }
         
         */
         if(!LEMSUpdateShareRecords.isEmpty()){
          System.debug('===>'+LEMSUpdateShareRecords);
          database.insert(LEMSUpdateShareRecords,false);
         
         }
         
         
         if(!LRejectedRecalledEVents.isEmpty()){
         
         for(EMS_Event__c eachEMSEvent :LRejectedRecalledEVents){
         
         if(eachEMSEvent.No_of_Completed_eCPAs__c==0){
             eachEMSEvent.Event_Status__c='Budget';   
             }
         
         if(eachEMSEvent.No_of_Completed_eCPAs__c>0){
             eachEMSEvent.Event_Status__c='Active';
             }   
         }
         
         database.saveresult[] sr = Database.update(LRejectedRecalledEVents,false);  
            System.debug('---->'+sr);
         }
         
         
         /*
           Copy over the Recipients into Event Beneficiaries records with Invitee Type = Initial Budgeted Beneficiary when
           Original eCPA has been approved.
           
        */  
         if(!EMSFirstapprovedeCPAs.isEmpty()){
         
            List<Event_Beneficiaries__c> LFirstApprovedeCPA = new List<Event_Beneficiaries__c>([Select Id,Type__c,EMS_Event__c,Employee__r.First_Name__c,Employee__r.Last_Name__c,Contact__r.Name,Speaker__c,Meal__c,Total_Ground_Transportation__c,Taxi__c,
                                  Train__c,Coach__c,EMS_Event__r.Event_Id__c,Pickup__c,Others__c,Beneficiary__c,Sponsor__c,Internal_Order_Fee__c,Meeting_Package__c,Meeting_Room_Rental__c,Booth_Space__c,Lodging_Amount__c,
                                  Consultancy_Fees__c,Gifts__c,Check_In_Date__c,Air_Travel__c,Organization__r.Name,Others_Others__c,Miscellaneous__c,Ticket_Type__c,Advertisement__c,Check_out_Date__c,Rate_per_night__c,Bed_Type__c 
                                  from Event_Beneficiaries__c where EMS_Event__c in :EMSFirstapprovedeCPAs and Invitee_Type__c='Budgeted Beneficiary' and Type__c!='Covidien Staff']);
            List<Event_Beneficiaries__c> LInsertBenef = new List<Event_Beneficiaries__c>();                      
            
            for(Event_Beneficiaries__c eachEventBenef : LFirstApprovedeCPA ) {
                eachEventBenef.Id=null;
                eachEventBenef.Invitee_Type__c='Initial Budgeted Beneficiary';
                LInsertBenef.add(eachEventBenef ); 
            }     
            
            database.saveresult[] sr = Database.insert(LInsertBenef,false);  
            System.debug('---->'+sr);              
         
         }
         
         /*
          Copy over the Recipients into Event Beneficiaries records with Invitee Type = PreEvent Budgeted Beneficiary when
           latest eCPA has been approved.
           
         */  
         if(!EMSSecondapprovedeCPAs.isEmpty()){
            List<Event_Beneficiaries__c> LSecondapprovedrecords = new List<Event_Beneficiaries__c>([Select Id,Name,Type__c from Event_Beneficiaries__c where EMS_Event__c in :EMSSecondapprovedeCPAs and Invitee_Type__c='PreEvent Budgeted Beneficiary' and Type__c!='Covidien Staff']);
            database.delete(LSecondapprovedrecords ,false);
            List<Event_Beneficiaries__c> LSecondApprovedeCPA = new List<Event_Beneficiaries__c>([Select Id,Type__c,EMS_Event__c,Employee__r.First_Name__c,Employee__r.Last_Name__c,Contact__r.Name,Speaker__c,Meal__c,Total_Ground_Transportation__c,Taxi__c,
                                  Train__c,Coach__c,EMS_Event__r.Event_Id__c,Pickup__c,Others__c,Beneficiary__c,Sponsor__c,Internal_Order_Fee__c,Meeting_Package__c,Meeting_Room_Rental__c,Booth_Space__c,Lodging_Amount__c,
                                  Consultancy_Fees__c,Gifts__c,Check_In_Date__c,Air_Travel__c,Organization__r.Name,Others_Others__c,Miscellaneous__c,Ticket_Type__c,Advertisement__c,Check_out_Date__c,Rate_per_night__c,Bed_Type__c 
                                  from Event_Beneficiaries__c where EMS_Event__c in :EMSSecondapprovedeCPAs and Invitee_Type__c='Budgeted Beneficiary' and Type__c!='Covidien Staff']);
            List<Event_Beneficiaries__c> LInsertBenef = new List<Event_Beneficiaries__c>();                      
            
            for(Event_Beneficiaries__c eachEventBenef : LSecondApprovedeCPA) {
                eachEventBenef.Id=null;
                eachEventBenef.Invitee_Type__c='PreEvent Budgeted Beneficiary';
                LInsertBenef.add(eachEventBenef ); 
            }     
            
            database.saveresult[] sr = Database.insert(LInsertBenef,false);  
            System.debug('---->'+sr);              
         
         }
         
         /*
         Copy over the Recipients into Event Beneficiaries records with Invitee Type = PSU Budgeted Beneficiary when
         Pre-Event eCPA has been approved.
         
         */
         
         if(!EMSPSUapprovedeCPAs.isEmpty()){
            List<Event_Beneficiaries__c> PSUapprovedrecords = new List<Event_Beneficiaries__c>([Select Id,Name,Type__c from Event_Beneficiaries__c where EMS_Event__c in :EMSPSUapprovedeCPAs and Invitee_Type__c='PSU Budgeted Beneficiary' and Type__c!='Covidien Staff']);
            database.delete(PSUapprovedrecords,false);
            List<Event_Beneficiaries__c> PSUApprovedeCPA = new List<Event_Beneficiaries__c>([Select Id,Type__c,EMS_Event__c,Employee__r.First_Name__c,Employee__r.Last_Name__c,Contact__r.Name,Speaker__c,Meal__c,Total_Ground_Transportation__c,Taxi__c,
                                  Train__c,Coach__c,EMS_Event__r.Event_Id__c,Pickup__c,Others__c,Beneficiary__c,Sponsor__c,Internal_Order_Fee__c,Meeting_Package__c,Meeting_Room_Rental__c,Booth_Space__c,Lodging_Amount__c,
                                  Consultancy_Fees__c,Gifts__c,Check_In_Date__c,Air_Travel__c,Organization__r.Name,Others_Others__c,Miscellaneous__c,Ticket_Type__c,Advertisement__c,Check_out_Date__c,Rate_per_night__c,Bed_Type__c 
                                  from Event_Beneficiaries__c where EMS_Event__c in :EMSSecondapprovedeCPAs and Invitee_Type__c='Budgeted Beneficiary' and Type__c!='Covidien Staff']);
            List<Event_Beneficiaries__c> LInsertBenef = new List<Event_Beneficiaries__c>();                      
            
            for(Event_Beneficiaries__c eachEventBenef : PSUApprovedeCPA) {
                eachEventBenef.Id=null;
                eachEventBenef.Invitee_Type__c='PSU Budgeted Beneficiary';
                LInsertBenef.add(eachEventBenef ); 
            }     
            
            database.saveresult[] sr = Database.insert(LInsertBenef,false);  
            System.debug('---->'+sr);              
         
         }
         
         /*
             Sending request to Webservice for creating eCPA as pdf after an eCPA has been Completed.
         */
         System.debug('=====>'+userInfo.getSessionId()+'+++++++++'+EMSConcludedEVentIds);
         //Need to send the session id to the future method as the you cannot use the userInfo.getSessionID() method in the future method because it is asynchronous and doesn't run in the user context.
         if(!EMSConcludedEVentIds.isEmpty()){
         EMS_Trigger_Controller.addPDFAttach(userInfo.getSessionId(),EMSConcludedEVentIds,'pdf');
         }
         
         /*
             Sending request to Webservice for creating Excel files for HCPs/Employees after an eCPA is submitted for approval.
         */
         
         if(!EMSnewrewuestecpaIds.isEmpty()){
         EMS_Trigger_Controller.addPDFAttach(userInfo.getSessionId(),EMSnewrewuestecpaIds,'excel');
         }
         
         
         
              
         }
         
         
         if(Trigger.isInsert){
         
             List<EMS_Event__Share> LEMSEOManagerHSareRecords = new List<EMS_Event__Share>();
             for(EMS_Event__c eachEMSEvent : Trigger.New){
             
                 if(eachEMSEvent.Owner_Country__c=='AU' || eachEMSEvent.Owner_Country__c=='NZ'){
                 
                    EMS_Event__Share emsShrEventOwner  = new EMS_Event__Share();
                    emsShrEventOwner.ParentId = eachEMSEvent.Id;
                    emsShrEventOwner.UserOrGroupId = eachEMSEvent.Event_Owner_User__c;
                    emsShrEventOwner.AccessLevel = 'Edit';
                    emsShrEventOwner.RowCause = Schema.EMS_Event__Share.RowCause.EMS_Event_Owner_Manager_Sharing_for_ANZ__c;
                    
                    LEMSEOManagerHSareRecords.add(emsShrEventOwner);
                    
                    EMS_Event__Share emsShrManager  = new EMS_Event__Share();
                    emsShrManager.ParentId = eachEMSEvent.Id;
                    emsShrManager.UserOrGroupId = eachEMSEvent.Manager1__c;
                    emsShrManager.AccessLevel = 'Edit';
                    emsShrManager.RowCause = Schema.EMS_Event__Share.RowCause.EMS_Event_Owner_Manager_Sharing_for_ANZ__c;
                    LEMSEOManagerHSareRecords.add(emsShrManager);
                 
                 }
             
             
             
             
             }
         
               database.saveresult[] sr = Database.insert(LEMSEOManagerHSareRecords,false); 
         
         }
        }
        if(Trigger.IsBefore){
         Map<String,Decimal> MIsoCurr = new Map<String,Decimal>();
         List<CurrencyType> LCurrTypes= new List<CurrencyType>([SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE]);
         for(CurrencyType eachvalue : LCurrTypes){
   
             MIsoCurr.put(eachvalue.IsoCode,eachValue.Conversionrate);
   
        }
        Set<String> OwnerCountry = new Set<String>(); 
        for(EMS_Event__c eachEMSEvent : Trigger.new){
             if((Trigger.isupdate &&((eachEMSEvent.Owner_Country__c!=Trigger.OldMap.get(eachEMSEvent.Id).Owner_Country__c)||(eachEMSEvent.GBU_Primary__c!=Trigger.OldMap.get(eachEMSEvent.Id).GBU_Primary__c)||(eachEMSEvent.CPA_Status__c=='New Request' && Trigger.OldMap.get(eachEMSEvent.Id).CPA_Status__c!='New Request')||(Trigger.oldMap.get(eachEMSEvent.Id).CPA_Status__c !='Pending CCP Coordinator Approval' && Trigger.newMap.get(eachEMSEvent.Id).CPA_Status__c =='Pending CCP Coordinator Approval' && Trigger.newMap.get(eachEMSEvent.Id).In_Country_CPA_Routing_USD_1000__c==true && Trigger.newMap.get(eachEMSEvent.Id).Owner_Country__c=='MY'))) || Trigger.IsInsert ){
             
                 OwnerCountry.add(eachEMSEvent.Owner_Country__c);  
                 }  
                 eachEmsEvent.Conversion_factor__c=MIsoCurr.get(eachEMSEvent.CurrencyISOCode);
                 eachEmsEvent.AUD_Conversion_factor__c=MIsoCurr.get('AUD');
             }
         Map<String,Approver_Matrix__c> MApproverMap = new Map<String,Approver_Matrix__c>();

         if(!OwnerCountry.isEmpty()){    
         List<Approver_Matrix__c> LApproverMatrix = new List<Approver_Matrix__c> ([Select Id,Name,Marketing_Country_Controller_1__c ,Marketing_Country_Controller_2__c,CCP_Coordinator_1__c,CCP_Coordinator_2__c,GCC_Finance_1__c ,GCC_Finance_2__c ,GCC_Legal_1__c ,GCC_Legal_2__c,GCC_Medical_Affairs_1__c,GCC_Medical_Affairs_2__c ,GCC_PACE_1__c,GCC_PACE_2__c ,GCC_R_D_1__c ,GCC_R_D_2__c ,
                                                                                            In_Country_Pace_Manager_1__c ,In_Country_Pace_Manager_2__c ,direct_supervisor_1__c,Regional_Business_Head_1__c,Regional_Business_Head_2__c ,Regional_PACE_Manager_1__c,Regional_PACE_Manager_2__c ,Sales_Marketing_Country_Controller_1__c ,Sales_Marketing_Country_Controller_2__c ,
                                                                                             ARO_CA_Director_1__c ,Country_Manager__c,ARO_CA_Director_2__c,ARO_Comms_and_PR_1__c,ARO_Comms_and_PR_2__c from Approver_Matrix__c where Name in :OwnerCountry ]);   
         
         for(Approver_Matrix__c eachrec : LApproverMatrix){
             MApproverMap.put(eachrec.Name,eachRec);         
             }
         }
         //Functionality to retreive approvers from custom Setting before an Event is updated/created.
         
         for(EMS_Event__c eachEMSEvent : Trigger.new){
         if(eachEMSEvent.Owner_Country__c!=null){
         if((Trigger.isupdate &&((eachEMSEvent.Owner_Country__c!=Trigger.OldMap.get(eachEMSEvent.Id).Owner_Country__c)||(eachEMSEvent.GBU_Primary__c!=Trigger.OldMap.get(eachEMSEvent.Id).GBU_Primary__c)||(eachEMSEvent.CPA_Status__c=='New Request' && Trigger.OldMap.get(eachEMSEvent.Id).CPA_Status__c!='New Request'))) || Trigger.IsInsert ){
             IF(eachEMSEvent.GBU_Primary__c!='' && eachEMSEvent.GBU_Primary__c!=null  && eachEMSEvent.Owner_Country__c!=null){
                 System.debug('>>>>>>>>>>>>>>>>'+eachEMSEvent.Owner_Country__c+'>>>>>>>>>>>>>>>>>>>>>>>>'+eachEMSEvent.GBU_Primary__c);
                 EMSMarketingGBU__c myMCC = EMSMarketingGBU__c.getvalues(eachEMSEvent.Owner_Country__c+';'+eachEMSEvent.GBU_Primary__c);
                 System.debug('>>>>>>>>>>>>>>>>'+myMCC);
                 eachEMSEvent.ARO_Marketing_Head_1__c= myMCC.ARO_Marketing_Head_1__c;   
                 eachEMSEvent.ARO_Marketing_Head_2__c= myMCC.ARO_Marketing_Head_2__c;        

                 
             }
             eachEMSEvent.Marketing_Country_Controller_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Marketing_Country_Controller_1__c; 
             eachEMSEvent.Marketing_Country_Controller_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Marketing_Country_Controller_2__c;
             eachEMSEvent.CCP_Coordinator_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).CCP_Coordinator_1__c;
             eachEMSEvent.CCP_Coordinator_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).CCP_Coordinator_2__c;             
             eachEMSEvent.GCC_Finance_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_Finance_1__c;
             eachEMSEvent.GCC_Finance_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_Finance_2__c; 
             eachEMSEvent.GCC_Legal_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_Legal_1__c ;
             eachEMSEvent.GCC_Legal_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_Legal_2__c ;
             eachEMSEvent.GCC_Medical_Affairs_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_Medical_Affairs_1__c ;
             eachEMSEvent.GCC_Medical_Affairs_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_Medical_Affairs_2__c ;
             eachEMSEvent.GCC_PACE_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_PACE_1__c; 
             eachEMSEvent.GCC_PACE_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_PACE_2__c;
             eachEMSEvent.GCC_R_D_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_R_D_1__c;
             eachEMSEvent.GCC_R_D_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).GCC_R_D_2__c;
             eachEMSEvent.In_Country_Pace_Manager_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).In_Country_Pace_Manager_1__c ;
             eachEMSEvent.In_Country_Pace_Manager_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).In_Country_Pace_Manager_2__c; 
             eachEMSEvent.Regional_Business_Head_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Regional_Business_Head_1__c ;
             eachEMSEvent.Regional_Business_Head_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Regional_Business_Head_2__c ;
             eachEMSEvent.Regional_PACE_Manager_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Regional_PACE_Manager_1__c ;
             eachEMSEvent.Regional_PACE_Manager_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Regional_PACE_Manager_2__c ;
             eachEMSEvent.Sales_Marketing_Country_Controller_1__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Sales_Marketing_Country_Controller_1__c; 
             eachEMSEvent.Sales_Marketing_Country_Controller_2__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Sales_Marketing_Country_Controller_2__c; 
             eachEMSEvent.ARO_CA_Director_1__c= MApproverMap.get(eachEMSEvent.Owner_Country__c).ARO_CA_Director_1__c;
             eachEMSEvent.ARO_CA_Director_2__c= MApproverMap.get(eachEMSEvent.Owner_Country__c).ARO_CA_Director_2__c;
             eachEMSEvent.ARO_Govt_Affairs__c = MapproverMap.get(eachEMSEvent.Owner_Country__c).Direct_Supervisor_1__c;
             eachEMSEvent.ARO_Comms_and_PR_1__c= MApproverMap.get(eachEMSEvent.Owner_Country__c).ARO_Comms_and_PR_1__c;
             eachEMSEvent.ARO_Comms_and_PR_2__c= MApproverMap.get(eachEMSEvent.Owner_Country__c).ARO_Comms_and_PR_2__c;
             eachEMSEvent.Country_Manager__c = MApproverMap.get(eachEMSEvent.Owner_Country__c).Country_Manager__c;
             
         }
      }   
      
      if(Trigger.isupdate &&(eachEMSEvent.CurrencyISOCode!=Trigger.OldMap.get(eachEMSEvent.Id).CurrencyISOCode)){
      eachEmsEvent.Conversion_factor__c=MIsoCurr.get(eachEMSEvent.CurrencyISOCode);
      
      }
     
     }  
     
     /*
      Logic to check if Event is atleast 21 days after today (when we are submitting the Event for approval.)
     */
     if(Trigger.IsUpdate){
     Set<Id> SManagersIds = new Set<Id>();
     
     for(EMS_Event__c eachEMSEvent : Trigger.new){
         SManagersIds.add(eachEMSEvent.Manager1__c);    
     }
     Map<Id,User> MUserManager;
     if(SManagersIds.isEmpty()){
         MUserManager=new Map<Id,User>([Select Id,UserName from user where Id in :SManagersIds]);
     }
     for(EMS_Event__c eachEMSEvent : Trigger.new){
     
         if(eachEMSEvent.CPA_Status__c =='New Request' && eachEMSEvent.Days_before_Event_Start__c<21 && eachEMSEvent.Is_Event_21_days_and_GCC_Approval_done__c==false && eachEMSEvent.No_Of_Completed_eCPAs__c==0 && eachEMSEvent.In_Country_CPA_Routing_USD_1000__c==false){
         
             eachEMSEvent.adderror('Please get GCC approval because Event submitted is due to happen in less than 21 days from today.After obtaining approval please select the following checkbox:  Is Event < 21 days and GCC Approval done ');
         
         }
         if(eachEMSEvent.CPA_Status__c=='New Request'&& eachEMSEvent.Owner_Country__c =='KR' && (eachEMSEvent.Manager_Certified_only_for_Korea_eCPAs__c==false ||(eachEMSEvent.Manager_Certified_By__c!= MUserManager.get(eachEMSEvent.Manager1__c).UserName))){
         
             eachEMSEvent.adderror('Certification from Manager missing/invalid. ');
         
         }
         if(((eachEMSEvent.CPA_Status__c =='New Request') && (eachEMSEvent.Division_Primary__c==null || eachEMSEvent.GBU_Primary__c==null ))||(eachEMSEvent.CPA_Status__c =='Pending CCP Coordinator Approval' && eachEMSEvent.In_Country_CPA_Routing_USD_1000__c==true && eachEMSEvent.Owner_Country__c=='MY' && (eachEMSEvent.Division_Primary__c==null || eachEMSEvent.GBU_Primary__c==null ))){
         
             eachEMSEvent.adderror('Please select Primary Division/Primary GBU before submitting for approval ');
         
         }
        /* if((eachEMSEvent.CPA_Status__c =='New Request') && (eachEMSEvent.Total_HCP_Expenses__c < (1000*eachEMSEvent.Conversion_Factor__c))){
         
             eachEMSEvent.adderror('Events with HCP Expenses less than USD 1000 cannot be submitted for approval');
         
         }
         */
         
         if(eachEMSEvent.CPA_Status__c =='New Request' &&(eachEMSEvent.Total_HCP_Expenses__c>=50000) && eachEMSEvent.Is_Approval_taken_from_MD_50k_AUD__c==false && (eachEMSEvent.Owner_Country__c=='AU' || eachEMSEvent.Owner_Country__c=='NZ')){
         
             eachEMSEvent.adderror('Please get offline approval from Managing Director and attach it to the Event.After obtaining approval please select the following checkbox: Is Approval taken from MD(> 50k AUD)');
         
         }
              }
     
     
     }  
         
       }  
    
}