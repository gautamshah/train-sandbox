/****************************************************************************************
* Name    : Trigger: Task
* Author  : Paul Berglund
* Date    : 07/23/2015
* Purpose : Single entry point for all Task triggers
* 
* Dependancies: 
*   Class: TaskTriggerDispatcher           
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
05/12/2016  Paul Berglund    No logic changes - just refactored to fit on one page
*****************************************************************************************/ 
trigger Task on Task
	(
		after delete,
		after insert,
		after undelete,
		after update,
		before delete,
		before insert,
		before update
	) 
{
    if (CPQ_Trigger_Profile__c.getInstance().Task__c == true)
    {
		return;//If this is the integration user then exit
    }
    
    // Don't put stuff here anymore, put it in Task_t
    TaskTriggerDispatcher.Main(
    	trigger.new,
    	trigger.newMap,
    	trigger.old,
    	trigger.oldMap,
    	trigger.isBefore,
    	trigger.isAfter,
    	trigger.isInsert,
    	trigger.isUpdate,
    	trigger.isDelete,
    	trigger.isExecuting);
    	
    cpqTask_t.main(
        trigger.isExecuting,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isBefore,
        trigger.isAfter,
        trigger.isUndelete,
        trigger.new,
        trigger.newMap,
        trigger.old,
        trigger.oldMap,
        trigger.size
	);
}