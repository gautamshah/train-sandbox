trigger ProductReqForUSMedSuppValidation on Opportunity (before insert,before update)
{
/********************************************************************************************************************
DEPRECATED!!!!!
This functionality has been moved to Opportunity_Main.ProductReqForUSMedSupplies()
By: Gautam Shah
When: 27 - June - 2014

    * Name    : ProductReqForUSMedSuppValidation
    * Author  : Tejas Kardile
    * Date    : 17 - January - 2014
    * Purpose : 
    * 
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE              AUTHOR              CHANGE
    * ---------------   ---------------     -------
    * 
    *********************************************************************************************************************   
    
    User currUser = Utilities.CurrentUser;
    
        try
        {
            //currUser = [SELECT id,profile.name FROM User WHERE id = :userinfo.getuserid()];
            currUser = Utilities.CurrentUser;
        }catch(exception e){}
    
    if(Utilities.CurrentUserProfileName == 'US - Med Supplies')
    {
        for(Opportunity opp : trigger.new)
        {
            system.debug('-- By pass value --> '+ opp.By_Pass_ProductRequiredForUSMedSuppVali__c);
            
            if(opp.By_Pass_ProductRequiredForUSMedSuppVali__c == false)
            {
                if(currUser != null)
                {
                    system.debug('-- current user --> '+ currUser);
                    if(
                    (
                    opp.StageName == 'Develop'  ||
                    opp.StageName == 'Evaluate' ||
                    opp.StageName == 'Propose'  ||
                    opp.StageName == 'Negotiate'    ||
                    opp.StageName == 'Closed Won'   ||
                    opp.StageName == 'Closed Lost'  ||
                    opp.StageName == 'Closed (Cancelled)' ||
                    opp.StageName == 'Closed (Resolved)'
                    ) 
                    //&& (curruser.profile.name == 'US - Med Supplies')
                    && (opp.of_Products__c < 1)
                    )
                    {
                        system.debug('-- opp Stage --> '+ opp.StageName);
                        opp.addError('Please add a Product to this Opportunity before moving past the Identify Stage.');
                    }
                }
            }
            else
            {
                opp.By_Pass_ProductRequiredForUSMedSuppVali__c = false;
                system.debug('-- uncheking the by pass value --> '+ opp.StageName);
            }
        }
    }
*/
}