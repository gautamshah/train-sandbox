/**
trigger on CPQ_Agreement_Before.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-04-08      Yuli Fintescu		Created. Prevent user from delete/edit agreement attachments
===============================================================================
*/
trigger CPQ_Attachment_Before on Attachment (before delete, before update) {
	if (CPQ_Trigger_Profile__c.getInstance().CPQ_Attachment_Before__c == true) {
		return;//specif user can delete/edit attachemnt
    }
    
	List<Attachment> candidates = new List<Attachment>();
	
    Schema.DescribeSObjectResult r = Apttus__APTS_Agreement__c.sObjectType.getDescribe();
	String keyPrefix = r.getKeyPrefix();
	
	if (Trigger.isUpdate) {
		for (Attachment a : Trigger.new) {
			if (a.Name != Trigger.oldMap.get(a.ID).Name && 
					a.ParentID != null && String.valueOf(a.ParentID).startsWith(keyPrefix)) {
				candidates.add(a);
			}
		}
	} else if (Trigger.isDelete) {
		for (Attachment a : Trigger.old) {
			if (a.ParentID != null && String.valueOf(a.ParentID).startsWith(keyPrefix)) {
				candidates.add(a);
			}
		}
	}
	
	//Prevent user from delete/edit agreement attachments
	CPQ_AgreementProcesses.BlockAttachmentChanges(candidates);
}