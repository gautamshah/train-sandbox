/****************************************************************************************
    * Name    : CoachingCallPlanTrigger
    * Author  : Chen Li
    * Date    : 15-4-2016
    * Purpose : Automatically create and update sharing on a CoachingCallPlan record for
    *           the Manager of the Sales Rep. Based on Sales_Rep__r, User_s_Manager_Id__c
    * 
    * Dependancies: 
    *              Triggers -> Workflows - > Process Builder.
    * Test Class: CoachingCallPlanTriggerTestClass     
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    *
    *****************************************************************************************/
trigger CoachingCallPlanTrigger on Coaching_Call_Plan__c (after insert, after update) {

 try{ 
  // get the id for the user for manager in the org
   
   

  // inserting new records
  if (Trigger.isInsert)
      {
  List<Coaching_Call_Plan__Share> sharesToCreate = new List<Coaching_Call_Plan__Share>();
  Map<Id, set<Id> > ccpManagerMap=new Map<Id, set<Id> >();  // manager list of one call plan
  set<Id> Managerlist =new set<Id>();
  List<User> usersWithSalesforceLicense = [Select Id, Name, ManagerId ,Profile.UserLicense.Name From User Where Profile.UserLicense.Name = 'Salesforce' and IsActive =true];
  
  for (Coaching_Call_Plan__c ccp : Trigger.new)
  {     
       Managerlist =new set<Id>();
       Boolean allDone;
      //If the account we are starting at has a Parent ID, add it
      //to the set of IDs
      if(ccp.Coaching_Manager__c!= Null){
        System.debug('ccp.Coaching_Manager__c------'+ccp.Coaching_Manager__c);
        Managerlist.add(ccp.Coaching_Manager__r.Id);
      }
      // based on Coaching_Manager__c field
      if(ccp.User_s_Manager_Id__c!= Null){
        System.debug('ccp.User_s_Manager_Id__c------'+ccp.User_s_Manager_Id__c);
        Managerlist.add(Id.valueOf(ccp.User_s_Manager_Id__c));
      }
      // based on manager in user's record
      if(ccp.Users_Manager_Id__c!= Null){
      System.debug('ccp.Users_Manager_Id__c------'+ccp.Users_Manager_Id__c);
      Managerlist.add(Id.valueOf(ccp.Users_Manager_Id__c));
      }
      
      if(ccp.Sales_Rep__c!=null)
      {
        System.debug('ccp.Sales_Rep__c------'+ccp.Sales_Rep__c);
        Managerlist.add(ccp.Sales_Rep__c);
      }
      
   
       System.debug('----Managerlist initial------'+Managerlist);
      do{
         //Set the flag to indicate loop should stop
         allDone = true;
     
        for(User u:usersWithSalesforceLicense){
            
          if(Managerlist.contains(u.id)&& (!Managerlist.contains(u.ManagerId) && u.ManagerId != null))
          {
            if(u.ManagerId != null)
              Managerlist.add(u.ManagerId);
       
            //Reset flag to find more parents / children
            allDone = false;

          }

        }

      } while(allDone == false); 
      
       //Managerlist.add(ccp.User_s_Manager_Id__c);
       //TODO query Manager List
       ccpManagerMap.put(ccp.id,Managerlist);
       System.debug('ccpManagerMap------'+ccpManagerMap);
  }
  
  
    for (Coaching_Call_Plan__c ccp : Trigger.new) {
        // create the new share for group
               
        set<Id> managerlistonCCP=ccpManagerMap.get(ccp.Id);
        for(Id u:managerlistonCCP)
        {
        Coaching_Call_Plan__Share ccps = new Coaching_Call_Plan__Share();
        ccps.AccessLevel  = 'Edit';
        ccps.ParentId  = ccp.Id;
        ccps.RowCause =Schema.Coaching_Call_Plan__Share.RowCause.Share_to_managers__c; 
        ccps.UserOrGroupId = u;
        System.debug('ccps------'+ccps);
        if( ccps.UserOrGroupId!=null)
        sharesToCreate.add(ccps);
         System.debug('User_s_Manager_Id__c------'+u);
        }
       }

    // do the DML to create shares
    if (!sharesToCreate.isEmpty())
    { System.debug('sharesToCreate------'+sharesToCreate);
      insert sharesToCreate;
    }
  
  } 

  // updating existing records
  else if (Trigger.isUpdate) {
List<Coaching_Call_Plan__Share> sharesToCreate = new List<Coaching_Call_Plan__Share>();
  Map<Id, set<Id> > ccpManagerMap=new Map<Id, set<Id> >();  // manager list of one call plan
  set<Id> Managerlist =new set<Id>();
  List<User> usersWithSalesforceLicense = [Select Id, Name, ManagerId ,Profile.UserLicense.Name From User Where Profile.UserLicense.Name = 'Salesforce' and IsActive =true];
  
    for (Coaching_Call_Plan__c ccp : Trigger.new)
    {
         if (Trigger.oldMap.get(ccp.id).User_s_Manager_Id__c != ccp.User_s_Manager_Id__c)
         {
        
         Managerlist =new set<Id>();
       Boolean allDone;
      //If the account we are starting at has a Parent ID, add it
      //to the set of IDs
      if(ccp.Coaching_Manager__c!= Null){
        System.debug('ccp.Coaching_Manager__c------'+ccp.Coaching_Manager__c);
        Managerlist.add(ccp.Coaching_Manager__r.Id);
      }
      if(ccp.User_s_Manager_Id__c!= Null){
        System.debug('ccp.User_s_Manager_Id__c------'+ccp.User_s_Manager_Id__c);
        Managerlist.add(Id.valueOf(ccp.User_s_Manager_Id__c));
      }
      if(ccp.Sales_Rep__c!=null)
      {
        System.debug('ccp.Sales_Rep__c------'+ccp.Sales_Rep__c);
        Managerlist.add(ccp.Sales_Rep__c);
      }
   
       System.debug('----Managerlist initial------'+Managerlist);
      do{
         //Set the flag to indicate loop should stop
         allDone = true;
     
        for(User u:usersWithSalesforceLicense){
            
          if(Managerlist.contains(u.id)&& (!Managerlist.contains(u.ManagerId) && u.ManagerId != null))
          {
            if(u.ManagerId != null)
              Managerlist.add(u.ManagerId);
       
            //Reset flag to find more parents / children
            allDone = false;

          }

        }

      } while(allDone == false); 
      
       //Managerlist.add(ccp.User_s_Manager_Id__c);
       //TODO query Manager List
       ccpManagerMap.put(ccp.id,Managerlist);
       System.debug('ccpManagerMap------'+ccpManagerMap);
        
         }
    }
         for (Coaching_Call_Plan__c ccp : Trigger.new) {
        // create the new share for group
               
        set<Id> managerlistonCCP=ccpManagerMap.get(ccp.Id);
        for(Id u:managerlistonCCP)
        {
        Coaching_Call_Plan__Share ccps = new Coaching_Call_Plan__Share();
        ccps.AccessLevel  = 'Edit';
        ccps.ParentId  = ccp.Id;
        ccps.RowCause =Schema.Coaching_Call_Plan__Share.RowCause.Share_to_managers__c; 
        ccps.UserOrGroupId = u;
        System.debug('ccps------'+ccps);
        if( ccps.UserOrGroupId!=null)
        sharesToCreate.add(ccps);
         System.debug('User_s_Manager_Id__c------'+u);
        }
       }

    // do the DML to create shares
    if (!sharesToCreate.isEmpty())
    { System.debug('sharesToCreate------'+sharesToCreate);
      insert sharesToCreate;
    }

  }

}
    
    Catch (Exception e) { System.debug('======Comparing Demo Exception: '+e.getMessage()); }  
}