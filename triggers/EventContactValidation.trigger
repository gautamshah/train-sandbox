trigger EventContactValidation on Event (before insert, before update) {
    /****************************************************************************************
    * Name    : Event Contact Validation    
    * Author  : Mike Melcher    
    * Date    : 1-6-2012    
    * Purpose : Ensure Events that are associated with Contacts only relate to 
    *           Affiliated Clinician and Affiliated Non Clinician Contacts    
    *     
    * Dependancies:     
    *                  
    *          
    * ========================    
    * = MODIFICATION HISTORY =    
    * ========================    
    * DATE        		AUTHOR              CHANGE    
    * ----        		------              ------    
    *  1/24/2012  		Pete Reed           Added Affiliated Non Clinician Contact to cMap.get(t.WhoId)
    *  Jan 27, 2016		Gautam Shah			Changes to use Utilities class to reduce SOQL queries (Lines 24-27)
    *    *****************************************************************************************/
    
Set<Id> ContactsInTrigger = new Set<Id>();
Map<Id,Id> cMap = new Map<Id,Id>();

//Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
//if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
String currentProfileName = Utilities.CurrentUserProfileName;
if(currentProfileName != 'API Data Loader' && currentProfileName != 'System Administrator')
{
    for (Event t : Trigger.new) {
       string w = t.WhoId;
       if (w != null && w.startsWith('003')) {
          ContactsInTrigger.add(t.WhoId);
       }
    }
    
    if (ContactsInTrigger.size() > 0) {
       DataQuality dq = new DataQuality();
       Map<String,Id> rtMap = dq.getRecordTypes();
       for (Contact c : [select Id, RecordtypeId from Contact where Id in :ContactsInTrigger]) {
          cMap.put(c.Id, c.RecordTypeId);
       }
       
       for (Event t : Trigger.new) {
          if (cMap.get(t.WhoId) == rtMap.get('Master Clinician') ||
              cMap.get(t.WhoId) == rtMap.get('Master Non Clinician')) {
              t.addError('Tasks Cannot Be Associated With Master Contacts');
           }
       }
    }
}
}