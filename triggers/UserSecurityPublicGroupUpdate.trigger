trigger UserSecurityPublicGroupUpdate on User (before insert, after insert, before update, after update) {
    /********************************************************************************************************************
    * Name    : UserSecurityPublicGroupUpdate
    * Author  : John E.J. Hogan (john.hogan@salesforce.com)
    * Date    : 29-January-2013
    * Purpose : Used to create and delete GroupMember records; Public Groups are used with
    *           Sharing Rules to give users "Regional" or "Country" data access 
    * 
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE              AUTHOR              CHANGE
    * ---------------   ---------------     ------
    * 29-January-2013   John E.J. Hogan     Created
    * 07-March-2013     John E.J. Hogan     Includes functionality to add users to 'Private Covidien Users' Public Group
    * 19-September      Lakhan Dubey        Adding users to public groups based on their country and salesorg fields.
    * 30-September      Bill Shan           UPdated logic of adding field managers to proper public group
    * 19-December-2016  Amogh Ghodke        Updated logic to add and remove LATAM users to LATAM_Country group as per user's country
    * 17-January-2017  Amogh Ghodke        Updated logic to add and remove LATAM users to LATAM_Country group as per user's country
    * 20-FEB-2017      Brajmohan Sharma     Updated for case 00908124
    * 01-sep-2017      Brajmohan Sharma     updated custom check due to data loader issue.
    *********************************************************************************************************************/
    
    
    static final String PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME = 'Private_Covidien_Users';
    static final String PRIVATE_USER_VISIBILITY_TERM = 'Private OEM R&D';
    static Boolean nonStandardConvert = FALSE;
   // List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
    if(Test.isRunningTest() || !(userinfo.getProfileId() =='00eU0000000dLrtIAE')){  //data loader using check
    String s='__\\_%'; 
    Map <String,Id> publicgroup = new Map <String,Id>();
   
    for(Group g:[SELECT Id,Name FROM Group  WHERE Name LIKE :s])
    {
        publicgroup.put(g.name,g.Id);    
    }
       
    List <Group> groupList = new List <Group>([SELECT Id, DeveloperName from Group where DeveloperName like 'Region_Group_%' OR DeveloperName like 'LATAM_%' OR DeveloperName like 'Country_Group_%' OR DeveloperName = :PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME]);
    Map<String, Id> groupMap = new Map <String, Id>();
    for (Group grp : groupList)
        groupMap.put(grp.DeveloperName, grp.Id);
    
    List<GroupMember> insertGroupMemberList = new List<GroupMember>();
    List<GroupMember> deleteGroupMemberList = new List<GroupMember>();
        
    if (trigger.isInsert){
        if (trigger.isBefore){
            for(User u : Trigger.new){
                if (u.UserType != 'Standard')
                    u.User_Visibility__c = 'Constrained';
                    
                if(u.Sales_Org_PL__c!=null && u.Sales_Org_PL__c!='')  // if the Sales_Org_PL__c field is not blank, copy the value to Franchise__c field.
                   u.Franchise__c=u.Sales_Org_PL__c; 
            }
        }
        if (trigger.isAfter){
            for(User u : Trigger.new){
                if (u.UserType == 'Standard'){
                String groupName;
                String groupNameLATAM;
                if (u.User_Visibility__c == 'Region' || u.User_Visibility__c == 'Country'){             
                    if (u.User_Visibility__c == 'Region'){
                        if(u.Region__c == 'LATAM'){  
                           groupName =  'Region_Group_' + u.Region__c;
                           groupNameLATAM = 'LATAM_' + u.Country;
                           }
                        else
                           groupName =  'Region_Group_' + u.Region__c;
                    }
                    else
                        groupName = 'Country_Group_' + u.Country;
                } else if (u.User_Visibility__c == PRIVATE_USER_VISIBILITY_TERM)
                        groupName =  PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME;
                    // if the user is an Open or Private User then insert new GroupMember record                
                    if (!String.isEmpty(groupMap.get(groupName))){
                        GroupMember grpMemAdd = new GroupMember();
                        grpMemAdd.UserOrGroupId = u.Id;
                        grpMemAdd.GroupId = groupMap.get(groupName);
                        insertGroupMemberList.add(grpMemAdd);
                        }// End: if (groupName != null)
                        
                    if (!String.isEmpty(groupMap.get(groupNameLATAM))){
                        GroupMember grpMemAddLATAM = new GroupMember();
                        grpMemAddLATAM.UserOrGroupId = u.Id;
                        grpMemAddLATAM.GroupId = groupMap.get(groupNameLATAM);
                        insertGroupMemberList.add(grpMemAddLATAM);
                        
                    } // End: if (groupNameLATAM != null)
                }  // End: if (u.UserType == 'Standard')
                
                if(u.User_Role__c=='Field Manager'&& publicgroup.keyset().contains(u.Country_SalesOrg__c))
                {
                    GroupMember gm= new GroupMember(); 
                    gm.GroupId=publicgroup.get(u.Country_SalesOrg__c);
                    gm.UserOrGroupId = u.id;
                    insertGroupMemberList.add(gm);
                } //if the user's User_Role__c value is "Field Manager" and the public group for the user's country and sales org exist, then add the user into the public group.
            } // for(User u : Trigger.new)
        }
    } //End: if (trigger.isInsert)
            
    if (trigger.isUpdate){
        if (trigger.isBefore){
            for(User u : Trigger.new){
                if (u.UserType != 'Standard')
                    u.User_Visibility__c = 'Constrained';
                if(u.Sales_Org_PL__c!=null && u.Sales_Org_PL__c!='')// if the Sales_Org_PL__c field is not blank, copy the value to Franchise__c field.
                    u.Franchise__c=u.Sales_Org_PL__c;
            }
        }
        if(trigger.isAfter){
            Boolean groupMemberFound = FALSE;
            Map<String,GroupMember> User_Group = new Map<String,GroupMember>();
            for(List<GroupMember> groupMemberList : [SELECT Id, Group.DeveloperName, UserOrGroupId from GroupMember where UserOrGroupId IN :trigger.newMap.keyset()]) {
                for (GroupMember grpMem : groupMemberList){
                    User_Group.put('' + grpMem.UserOrGroupId + grpMem.GroupId, grpMem);
                }
                             
                for(User u : Trigger.new){
                    if (u.User_Visibility__c == 'Region' || u.User_Visibility__c == 'Country' || u.User_Visibility__c == PRIVATE_USER_VISIBILITY_TERM){
                        groupMemberFound = FALSE;
                        String groupName; 
                        String groupName2;                  
                        if (u.User_Visibility__c == 'Region'){
                             if(u.Region__c == 'LATAM') { 
                                groupName =  'LATAM_' + u.Country;
                                groupName2 =  'Region_Group_' + u.Region__c;
                             }
                             else 
                               groupName =  'Region_Group_' + u.Region__c;
                        }
                        else if (u.User_Visibility__c == 'Country')
                            groupName = 'Country_Group_' + u.Country;
                        else if (u.User_Visibility__c == PRIVATE_USER_VISIBILITY_TERM){
                            groupName = PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME;                        
                        }
                        for (GroupMember grpMem : groupMemberList){
                            if (grpMem.UserOrGroupId == u.Id && grpMem.Group.DeveloperName == groupName){
                                groupMemberFound = TRUE;
                                break;
                            }
                        } 
                      
                        if (!groupMemberFound) {                    
                            GroupMember grpMemAdd = new GroupMember();
                            grpMemAdd.UserOrGroupId = u.Id;
                            if(!String.isBlank(groupMap.get(groupName))){
                             grpMemAdd.GroupId = groupMap.get(groupName);
                                insertGroupMemberList.add(grpMemAdd);
                       
                                // Remove User from all other Region and Country groups
                                for (GroupMember grpMem : groupMemberList){
                                    if (grpMem.UserOrGroupId == u.Id && (grpMem.Group.DeveloperName.startsWith('Region_Group_') || grpMem.Group.DeveloperName.startsWith('LATAM_') || grpMem.Group.DeveloperName.startsWith('Country_Group_') || grpMem.Group.DeveloperName == PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME))                                       
                                        if((grpMem.Group.DeveloperName != 'Region_Group_LATAM' && u.Region__c !='LATAM') || (grpMem.Group.DeveloperName != 'Region_Group_LATAM' && u.Region__c=='LATAM')|| (grpMem.Group.DeveloperName == 'Region_Group_LATAM' && u.Region__c!='LATAM'))
                                            deleteGroupMemberList.add(grpMem);
                                        System.debug('Delete List after line 149'+deleteGroupMemberList);
                                }
                            }
                            
                       
                        } // End: (!groupMemberFound)
                        
                       if(groupName =='LATAM_' + u.Country){
                      if (!String.isEmpty(groupMap.get(groupName2))){
                        GroupMember grpMemAddRegien = new GroupMember();
                        grpMemAddRegien.UserOrGroupId = u.Id;
                        grpMemAddRegien.GroupId = groupMap.get(groupName2);
                        insertGroupMemberList.add(grpMemAddRegien );
                        }
                        }
                    } else if (u.User_Visibility__c == 'Constrained'){ 
                        for (GroupMember grpMem : groupMemberList){
                            if (grpMem.UserOrGroupId == u.Id && (grpMem.Group.DeveloperName.startsWith('LATAM_') || grpMem.Group.DeveloperName.startsWith('Region_Group_') || grpMem.Group.DeveloperName.startsWith('Country_Group_') || grpMem.Group.DeveloperName == PRIVATE_PUBLIC_GROUP_DEVELOPER_NAME))
                                deleteGroupMemberList.add(grpMem);  
                                System.debug('Delete List after line 168'+deleteGroupMemberList);                    
                        }
                    }
                     
                } // End: for(User u : Trigger.new)
            } //End: for(List<GroupMember> groupMemberList
            
             for(user u:trigger.new)  {
                
                boolean added = false;
                boolean deleted = false;
                    
                if (u.User_Role__c != Trigger.oldMap.get(u.Id).User_Role__c){
                    if(u.User_Role__c=='Field Manager'&& publicgroup.keyset().contains(u.Country_SalesOrg__c))
                    {
                        GroupMember gm= new GroupMember(); 
                        gm.GroupId=publicgroup.get(u.Country_SalesOrg__c);
                        gm.UserOrGroupId = u.id;
                        insertGroupMemberList.add(gm);
                        added = true;
                    }// if the User_Role__c value is changed and the new value of User_Role__c is "Field Manager" and the public group for the user's country and sales org exist, then add the user into the public group.  
                    
                    if(Trigger.oldMap.get(u.Id).User_Role__c=='Field Manager'&& publicgroup.keyset().contains(Trigger.oldMap.get(u.Id).Country_SalesOrg__c))
                    {
                        if(User_Group.containskey(''+u.Id+publicgroup.get(Trigger.oldMap.get(u.Id).Country_SalesOrg__c)))
                        {
                            deleteGroupMemberList.add(User_Group.get(''+u.Id+publicgroup.get(Trigger.oldMap.get(u.Id).Country_SalesOrg__c)));
                            deleted = true;
                        }
                    } //if the User_Role__c value is changed and the old value of User_Role__c is "Field Manager" and the public group for the user's country and sales org exist, then remove the user from the public group.
                }
                
                if (u.Country_SalesOrg__c!= Trigger.oldMap.get(u.Id).Country_SalesOrg__c) { 
                    if(added == false && u.User_Role__c=='Field Manager'&& publicgroup.keyset().contains(u.Country_SalesOrg__c))
                    {
                        GroupMember gm= new GroupMember(); 
                        gm.GroupId=publicgroup.get(u.Country_SalesOrg__c);
                        gm.UserOrGroupId = u.id;
                        insertGroupMemberList.add(gm);
                    }// if the Sales_Org_PL__c value is changed and the  value of the User_Role__c value is "Field Manager" and the public group for the user's country and new sales org exist, then add the user into the public group.  
                    
                    if(deleted == false && publicgroup.keyset().contains(Trigger.oldMap.get(u.Id).Country_SalesOrg__c)
                     && User_Group.containskey(''+u.Id+publicgroup.get(Trigger.oldMap.get(u.Id).Country_SalesOrg__c)))
                    {
                        deleteGroupMemberList.add(User_Group.get(''+u.Id+publicgroup.get(Trigger.oldMap.get(u.Id).Country_SalesOrg__c)));
                    } //if the Sales_Org_PL__c value is changed and  the user is in the public group based on the old sales org and country value, then remove the user from the public group. 
                } 
            }
        } // End: (trigger.isAfter) 
    }// End: else if (trigger.isUpdate)
    
    if (insertGroupMemberList.size() > 0){
        insert insertGroupMemberList;
    }
    
    if (deleteGroupMemberList.size() > 0){  
        delete deleteGroupMemberList;   
        System.debug('Delete List after line 149'+deleteGroupMemberList);
    }
    
  }//custom setting check  
  } 
// End: trigger