trigger ContactValidation on Contact (before insert) {

    /****************************************************************************************
    * Name    : Contact Validation
    * Author  : Mike Melcher
    * Date    : 11-20-2011
    * Purpose : Update the first and last name and recordtype of new Connected Contacts
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                CHANGE
    * ----        ------                ------
    * 1-25-2012   MJM                   Updated recordtype names to Master Clinician and Master Non Clinician
    * 3-02-2012   MJM                   Trigger de-activated -- no longer needed
    * 3-16-2012   MJM                   Re-activated, repurposed to ensure first letters of the Name are capitalized
    * 4-23-2012   MJM                   Combined with trigger to check for duplicate Connected Contacts
    * 5-03-2012   MJM                   Removed logic for creating DQ Case -- Case is created in ContactCreateDQCase trigger
    * 5-16-1964   MJM                   Update recordtypes and flag for new Contacts
    * 3-08-2013   JEJH                  Added logic to determine if the Contact is being created by a Private Covidien user
    * 8-28-2013   Gautam Shah           Modified to make a call to the DataQuality method "convertRecordTypeForDQProcessing" to eval criteria
    * 3/30/2014     Gautam Shah         Commented out SOQL query to Profile object and replaced with call to Utilities.profileMap (Line 49)
     * 18 December 2015 Amogh Ghodke     Added line 62,63 for Distributor Contact for ASIA - PH 
    *****************************************************************************************/

// Verify contact created by user and not the Integration processes
/*
Id dataloaderProfile;
Id SystemAdminProfile;
Id privateUserProfile;

Id currentUser = UserInfo.getUserId();

for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader' or Name='System Administrator' or Name='Private Covidien User']) {
   if (p.Name == 'API Data Loader') {
      dataloaderProfile = p.Id;
   }
   if (p.Name == 'Private Covidien User'){
      privateUserProfile = p.Id;    
   }
 //  if (p.Name == 'System Administrator') {
 //     SystemAdminProfile = p.Id;
 //  }
}
*/
//if (UserInfo.getProfileId() != dataloaderProfile  && UserInfo.getProfileId() != privateUserProfile) 
if(Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && Utilities.profileMap.get(UserInfo.getProfileId()) != 'Private Covidien User')
{
    List<Contact> ContactsForCases = new List<Contact>();
    // Get the RecordtypeIds we will need
    DataQuality dq = new DataQuality();
    Map<String,Id> rtmap = dq.getRecordTypes();
    Set<Id> AccountsInTrigger = new Set<Id>();
    for(Contact c : Trigger.new)
    {
        System.debug('Contact Validation, RecordTypeId: ' + c.RecordTypeId);
        if (dq.convertRecordTypeForDQProcessing(c.RecordtypeId))
        {   
            if(c.RecordtypeId == rtMap.get('Distributor Contact'))
               c.recordtypeid = rtMap.get('In Process Distributor Contact');
            else{
            c.recordtypeid = rtMap.get('In Process Connected Contact');
            System.debug('New RecordTypeId: ' + c.RecordTypeId);
            if (c.Master_Contact_Record__c != null) 
            {
                AccountsInTrigger.add(c.AccountId);
                c.Contact_Creation_Status__c = 'Contact Creation Approved';
            } 
            else 
            {
                c.Contact_Creation_Status__c = 'Contact Creation Pending';
            }
            }
        }
    }
    if (AccountsInTrigger.size() > 0) 
    {
        Map<Id,Set<Id>> AccountMasterMap = new Map<Id,Set<Id>>();
        for (Id i : AccountsInTrigger)
        {
            Set<Id> nullList = new Set<Id>();
            AccountMasterMap.put(i,nullList);
        }
      
      // Build map of Master Contacts associated with Accounts in Trigger
      
    for (Contact c : [select Id, AccountId, RecordTypeId, Master_Contact_Record__c from Contact where AccountId in :AccountsInTrigger])
    {
        //System.debug('Checking Contacts for master: ' + c.Id + ' recordtype: ' + c.RecordtypeId + ' master RT:' + rtMap.get('Master Clinician'));
        if (c.Master_Contact_Record__c != null) 
        {
            //System.debug('Found connected contact ' + c.Id + ' for Account ' + c.AccountId);
            Set<Id> AccountMasters = AccountMasterMap.get(c.AccountId);
            AccountMasters.add(c.Master_Contact_Record__c);
            AccountMasterMap.put(c.AccountId,AccountMasters);
            //System.debug('Added master to list in map: ' + AccountMasterMap.get(c.AccountId));
        }
    }
      
      // Check Connected Contacts in Trigger to see if there is already a Connected Contact with the same Master Contact
      //    for the Account.  Add error if duplicate found.
      
      for (Contact c : Trigger.new) {
         Set<Id> AccountMasters = AccountMasterMap.get(c.AccountId);
         if (AccountMasters.size() > 0) {
            for (Id i : AccountMasters) {
               if (c.Master_Contact_Record__c == i) {
                  c.addError('Duplicate Connected Contact: There is already a Connected Contact for this Master Contact on this Account');
                  return;
               }
            }
         }
      }
   }
  
  
} 

}