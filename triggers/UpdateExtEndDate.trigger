trigger UpdateExtEndDate  on Tenders__c (before insert, before update){
Integer i;


    for(Tenders__c t : trigger.new)
        {
            if(t.Extension_No_of_Years__c != null)
            {
                i=integer.valueof(t.Extension_No_of_Years__c);
                t.Extension_End_Date__c=t.Tender_End_Date__c.addYears(i);
            }
        }           
}