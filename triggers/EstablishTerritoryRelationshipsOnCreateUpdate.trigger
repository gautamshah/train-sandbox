trigger EstablishTerritoryRelationshipsOnCreateUpdate on Territory (before insert, before update) {
    /****************************************************************************************
	* Name    : EstablishTerritoryRelationshipsOnCreateUpdate
	* Author  : Suchin Rengan
	* Date    : 06-05-2011
	* Purpose : Used to set up territory records for processing by the Batch 
	*           program (EstablishTerritoryRelationship) that builds the territory hierarchy.
	*                 
	*
	* ========================
	* = MODIFICATION HISTORY =
	* ========================
	* DATE        AUTHOR               CHANGE
	* ----        ------               ------
	* 06-05-2011  Suchin Rengan        Created
	*
	*****************************************************************************************/
	Map<String,Id> TerritoryStringIdMap	 = new Map<String,Id>();
	List<Territory> territoryUpdateList = new List<Territory>();
	Set<String> newTerritoryNames = new Set<String>();
	Set<String> checkExistingTerritoryNames = new Set<String>();
	Map<Id,Territory> oldTerrMap = Trigger.oldMap;
	
	// Validate for the parent territory names
	for (Territory territory : Trigger.new) {
				
		// Flag all those territories that have a parent territory Id
		if(Trigger.isInsert){
			if(territory.ExternalParentTerritoryID__c != null){
				territory.flagForProcessing__c = true;
			}
		}
		if(Trigger.isUpdate){
			// only flag the ones that have the parent territory ids changed
			Territory oldTerr = oldTerrMap.get(territory.Id);
			if(oldTerr.ExternalParentTerritoryID__c != territory.ExternalParentTerritoryID__c)
				territory.flagForProcessing__c = true;
		}
	}
}