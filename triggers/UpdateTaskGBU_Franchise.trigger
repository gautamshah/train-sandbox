trigger UpdateTaskGBU_Franchise on Task (before insert, before update) 
{
    /****************************************************************************************
    * Name    : UpdateTaskGBU_Franchise
    * Author  : Nathan Shinn
    * Date    : 08/04/2011
    * Purpose : Sets the task's GBU and Franchise to the falues on the Task Owner's
    *           User record.
    * 
    * Dependancies: Task
    *              ,User
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    *
    *****************************************************************************************/
    /*
    if(!test.isRunningTest()){
        if
        (
            
            
                UserInfo.getProfileId()== [select Id
                                            from Profile
                                            where name = :Trigger_Profile__c.getInstance('GBU_Franchise_Update').Profile_Name__c].Id
        )
        
        {
          return;//If this is the integration user then exit
        }
    }
    */
         
    
    //Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1]; 
    //if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
    if(!Utilities.isSysAdminORAPIUser)
    {
		map<Id, User> userMap = new map<Id, User>();
    	set<Id> userIds = new set<Id>();
        //get the list of user Ids for the owner column
        for(Task t : trigger.new)
        {
           userIds.add(t.OwnerId);
        }        
        
           //create a map of owner to user for retrieving the GBU and Franchise Ids
        for
        (
           User u : [select Id           
                     , Franchise__c            
                     , Business_Unit__c                    
                     from User                   
                     where Id in :userIds ]
        )     
        
        {      
           userMap.put(u.Id, u);    
        }       
        
           //for(Task tsk : trigger.new)    
        for
        (
            Task t: trigger.new
        )    
        
        {       
           //update GBU and Franchise to the Owner's values        
           t.Franchise__c = userMap.get(t.OwnerId).Franchise__c;        
           t.Business_Unit__c = userMap.get(t.OwnerId).Business_Unit__c;    
        }
	}    
}