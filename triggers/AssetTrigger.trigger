/****************************************************************************************
 * Name    : AssetTrigger 
 * Author  : Bill Shan
 * Date    : 17/12/2012 
 * Purpose : Asset Trigger Entry
 * Dependencies: Asset_TriggerHandler Apex Class
 *             , Asset Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *
 *****************************************************************************************/
trigger AssetTrigger on Asset (before delete) {
	
	Asset_TriggerHandler handler = new Asset_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old);
    }

}