/**
trigger on Product2.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-01-07      Yuli Fintescu		Created. Auto Sync price list item with Product
10/7/2016   Paul Berglund   Logic moved to cpqProduct2_t
===============================================================================
*/
trigger CPQ_Product2_After on Product2 (after insert, after update)
{
/*
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Product2_After__c == true) {
		return;
    }
    
	//Auto Sync price list item with Product
	CPQ_PriceListProcesses.SyncPriceListItem(Trigger.new, Trigger.oldMap);
*/
}