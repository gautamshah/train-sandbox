trigger CloseTasksOnSubmission on Cycle_Period__c (after update) {
    Set<Id> stPeriods = new Set<Id>();
    
    for(Cycle_Period__c cp : trigger.new){
        if((cp.No_Sales_This_Month__c || cp.Sales_out_submission_date__c != null) && cp.Channel_Inventory_Submission_Date__c != null )
          stPeriods.add(cp.id);  
    }
    
    if(stperiods.size()>0){
        list<Task> tasksToInsert = new list<Task>();
        for(Task t : [Select id, status from Task where whatId in: stPeriods]){
            t.status = 'Completed';
            tasksToInsert.add(t);
        }
        update tasksToInsert;
    }
    
}