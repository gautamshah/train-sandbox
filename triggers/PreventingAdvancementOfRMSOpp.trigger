trigger PreventingAdvancementOfRMSOpp on Opportunity (after update,after insert) { 
/****************************************************************************************
   * Name    : PreventingAdvancementOfRMSOpp
   * Author  : Lakhan Dubey
   * Date    : 16/04/2015
   * Purpose : No Opportunity in the system can be set to Evaluate, Propose, Negotiate, Closed Won, or Closed Lost without a Contact in a Contact Role on the Opportunity. 
               The Opportunity  can be created or saved in the Identify, Develop or Closed Cancelled stages.
   * Dependencies: Opportunity,OCR 
 *****************************************************************************************/
if(Utilities.CurrentUserProfileName=='US - RMS') {

for(Opportunity   opp:[select id,StageName,(select ContactId from OpportunityContactRoles) from Opportunity where id IN:Trigger.new ])
{
if(opp.OpportunityContactRoles.size()==0&&(opp.StageName=='Evaluate'||opp.StageName=='Propose'||opp.StageName=='Negotiate'||opp.StageName=='Closed Won'||opp.StageName=='Closed Lost')){

Trigger.newMap.get(opp.Id).adderror('No Opportunity in the system can be set to Evaluate, Propose, Negotiate, Closed Won, or Closed Lost without a Contact in a Contact Role on the Opportunity. The Opportunity  can be created or saved in the Identify, Develop or Closed Cancelled stages.');
}
}
}
}