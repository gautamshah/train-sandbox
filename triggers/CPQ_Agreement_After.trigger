/**
trigger on CPQ_Agreement_After.

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
2014-11-21      Yuli Fintescu       Created. Sent agreement to partner when activated
02/23/2016      Paul Berglund       Logic moved to Apttus_Agreement_Trigger_Manager
===============================================================================
*/
trigger CPQ_Agreement_After on Apttus__APTS_Agreement__c (after update) {
/*
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Agreement_After__c == true) {
        return;//If this is the integration user then exit
    }
    
    if (Trigger.isUpdate) {
        CPQ_AgreementProcesses.SendToPartner(Trigger.new, Trigger.oldMap);
    }
*/
}