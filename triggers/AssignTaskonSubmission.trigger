trigger AssignTaskonSubmission on Cycle_Period__c (after insert) {
    List<Task> lstTasks = new List<Task>();
    Map<id,id> mpids = new Map<id,id>();
    for(Cycle_Period__c  cp: trigger.new){
        mpids.put(cp.Id,cp.Distributor_Name__c);
    }
    
    Map<Id,Id> userIds = new Map<Id,Id>();
    for(User u : [Select id, contactId from User where  contactId != null and UserRoleId != null and isActive=true]){
        userIds.put(u.contactId,u.id);
    }
    
    
    List<Contact> lstCons = [Select id,AccountId from Contact where AccountId in:mpIds.values() and type__c='DIS Contact'];
    map<id,List<Id>> mpconsIds = new Map<Id,List<id>>();
    RecordTYpe rec = [Select id from RecordType where developerName='Asia_Distributor_Task'];
    if(lstcons.size()>0){
        for(Contact con : lstcons){
            if(mpConsIds.get(con.accountId) == null)
                mpConsIds.put(con.accountId,new List<Id>{con.id});
            else{
                mpConsIds.get(con.accountId).add(con.Id);
            }
        }
        
        Set<Id> stAccIds = new Set<id>();
        for(Cycle_Period__c  cp: trigger.new){
            System.debug('>>>>>>>>>>First For>>>>>>>>');
            if(mpConsIds.containsKey(mpids.get(cp.Id)) && mpConsIds.get(cp.Distributor_Name__c) != null){
                for(id cid: mpConsIds.get(cp.Distributor_Name__c)){
                System.debug('>>>>>>>>>>Second For>>>>>>>>');
                    if(userIds.get(cid) != null){
                    System.debug('>>>>>>>>>>Second For>>>>>>>>'+userIds.get(cid));
                        Task tsk = new Task();
                        tsk.Subject='Submission Deadline';
                        tsk.WhoId=cid;
                        tsk.recordtypeid = rec.Id;
                        tsk.ownerId = userIds.get(cid);
                        tsk.activitydate= System.today();
                        tsk.status='In Progress';
                        tsk.activity_type__c='Submit Sales-Out and Inventory Data';
                        tsk.whatId=cp.Id;
                        tsk.ActivityDate = cp.Submission_Due_Date__c;
                        lstTasks.add(tsk);
                    }
                }
            }
        
        }
    }
    
    else{
        System.debug('>>>>>>>>>>>>First Else>>>>>>>>>>>>');
        List<Contact> lstConSales = [Select id,AccountId from Contact where AccountId in:mpIds.values() and type__c='Sales Manager'];
        map<id,List<Id>> mpconsIdsNoSales = new Map<Id,List<id>>();
        if(lstConSales.size()>0){
            for(Contact con : lstConSales ){
                if(mpconsIdsNoSales.get(con.accountId) == null)
                    mpconsIdsNoSales.put(con.accountId,new List<Id>{con.id});
                else{
                    mpconsIdsNoSales.get(con.accountId).add(con.Id);
                }
            }
            System.debug('>>>>>>>>>>>>First Else>>>>>>>>>>>>'+mpconsIdsNoSales.size());
            System.debug('>>>>>>>>>>>>First Else Sales>>>>>>>>>>>>'+mpconsIdsNoSales);
            Set<Id> stAccIds1 = new Set<id>();
            for(Cycle_Period__c  cp: trigger.new){
                System.debug('>>>>>>>>>>First For>>>>>>>>');
                if(mpconsIdsNoSales.containsKey(cp.Distributor_Name__c) && mpconsIdsNoSales.get(cp.Distributor_Name__c) != null){
                    System.debug('>>>>>>>>>>Map Contains Key>>>>>>>>');
                    for(id cid: mpconsIdsNoSales.get(cp.Distributor_Name__c)){
                        System.debug('>>>>>>>>>>Second For>>>>>>>>');
                        if(userIds.get(cid) != null){
                            System.debug('>>>>>>>>>>User Record found>>>>>>>>');
                            Task tsk = new Task();
                            tsk.Subject='Submission Deadline';
                            tsk.WhoId=cid;
                            tsk.activitydate= System.today();
                            tsk.status='In Progress';
                            tsk.whatId=cp.Id;
                            tsk.activity_type__c='Submit Sales-Out and Inventory Data';
                            tsk.recordtypeid = rec.Id;
                            tsk.ownerId = userIds.get(cid);    
                            tsk.ActivityDate= cp.Submission_Due_Date__c;                        
                            lstTasks.add(tsk);
                        }    
                    }
                }
            }
        }
        else{
            List<Contact> lstConSalesREP = [Select id,AccountId from Contact where AccountId in:mpIds.values() and type__c='Sales Rep']; 
            map<id,List<Id>> mpconsIdsNoSalesREP = new Map<Id,List<id>>();
            if(lstConSalesREP .size()>0){
                for(Contact con : lstConSalesREP ){
                    if(mpconsIdsNoSalesREP.get(con.accountId) == null)
                        mpconsIdsNoSalesREP.put(con.accountId,new List<Id>{con.id});
                    else{
                        mpconsIdsNoSalesREP.get(con.accountId).add(con.Id);
                    }
                }
            }   
            for(Cycle_Period__c  cp: trigger.new){
                if(mpconsIdsNoSalesREP.get(cp.Distributor_Name__c) != null){
                    for(id cid: mpconsIdsNoSalesREP.get(cp.Distributor_Name__c)){
                        if(userIds.get(cid) != null){
                            Task tsk = new Task();
                            tsk.Subject='Submission Deadline';
                            tsk.WhoId=cid;
                            tsk.activitydate= System.today();
                            tsk.status='In Progress';
                            tsk.whatId=cp.Id;
                            tsk.activity_type__c='Submit Sales-Out and Inventory Data';
                            tsk.recordtypeid = rec.Id;
                            tsk.ownerId = userIds.get(cid); 
                            tsk.ActivityDate= cp.Submission_Due_Date__c;
                            lstTasks.add(tsk);
                        }    
                    }
                }
               
            }
        }
    }
    System.debug('>>>>>>>Size>>>>>>>'+lsttasks.size());
    System.debug('>>>>>>>Tasks>>>>>>>'+lsttasks);
    insert lstTasks;
}