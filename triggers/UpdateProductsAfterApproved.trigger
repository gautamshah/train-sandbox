/****************************************************************************************
 * Name    : UpdateProductsAfterApproved 
 * Author  : Fenny Saputra
 * Date    : 31/05/2016 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
//For Malaysia

trigger UpdateProductsAfterApproved on Samples__c (before update) {
    Set<ID> sampleIds = new Set<ID>();
    Set<ID> approvedproductcount = new Set<ID>();
    
    for(Samples__c sam : trigger.new){
        if(sam.Country__c == 'MY' && sam.Approval_Status__c == 'Approved'){
            List<Sample_Product__c> samprod = [select Id, Sample_Request__c, Approved__c from Sample_Product__c where 
                                                Sample_Request__c = : sam.Id];
            system.debug('sample prods' + samprod);
            
            for(Sample_Product__c sp : samprod){
                if(sp.Approved__c == false){
                    approvedproductcount.add(sp.Id);
                }
            }
            
            system.debug('not checked' + approvedproductcount.size());
            system.debug('record size' + samprod.size());
            
            if(approvedproductcount.size() == samprod.size()){
                sam.Send_MY_GWF_Form__c = true;
                for(Sample_Product__c sp : samprod){
                    sp.Approved__c = true;
                }
                update samprod;
            }
            
            if(approvedproductcount.size() <> samprod.size()){
                sam.Send_MY_GWF_Form__c = true;
            }
        }
    }   
    
}