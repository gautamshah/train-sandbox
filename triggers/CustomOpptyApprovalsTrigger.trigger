/**
 *  Apttus Approvals Management
 *  CustomOpptyApprovalsTrigger
 *   - performs approval required check
 *
 *  2014 Apttus Inc. All rights reserved.
 */
trigger CustomOpptyApprovalsTrigger on Apttus__APTS_Agreement__c (before update) {

  // preview status pending
  private String PREVIEW_STATUS_PENDING = 'Pending';

    // status none
    private String STATUS_NONE = 'Not Submitted';

    // status approved
    private String STATUS_APPROVED = 'Approved';

    // status pending approval
    private String STATUS_PENDING_APPROVAL = 'Pending Approval';

    // status approved
    private String STATUS_APPROVED_APPROVAL = 'Approved';


    // null string
    private String VALUE_NULL = 'null';

    // skip checking under following conditions
    //    - trigger size is greater than 1
    //    - current approval status is pending approval
    //        , meaning it is the initial submission action approval status update OR process in progress
    //    - old approval status is pending approval and new approval status is other thatn pending approval
    //        meaning, it is process completion approval status update
    //    - record type is not a Surgical type using cart approvals
    if ((Trigger.new.size() > 1)
            || (Trigger.new[0].Apttus_Approval__Approval_Status__c == STATUS_PENDING_APPROVAL)
            || (Trigger.new[0].Apttus_Approval__Approval_Status__c == STATUS_APPROVED_APPROVAL)

            || ((Trigger.old[0].Apttus_Approval__Approval_Status__c == STATUS_PENDING_APPROVAL)
                && (Trigger.new[0].Apttus_Approval__Approval_Status__c != Trigger.old[0].Apttus_Approval__Approval_Status__c))){
        return;
    }

    RecordType rt = [Select Id, DeveloperName From RecordType Where Id = :Trigger.new[0].RecordTypeId];
    if (rt.DeveloperName != 'Agreement_Proposal_AG' &&
        rt.DeveloperName != 'Custom_Kit' &&
        rt.DeveloperName != 'Energy_Hardware_AG' &&
        rt.DeveloperName != 'GPO_LOC' &&
        rt.DeveloperName != 'New_Product_LOC_AG' &&
        rt.DeveloperName != 'Promotional_LOC_AG' &&
        rt.DeveloperName != 'Royalty_Free_Agreement_AG' &&
        rt.DeveloperName != 'Scrub_PO' &&
        rt.DeveloperName != 'Smart_Cart'
       ) {

        return;
    }

    if (Trigger.isBefore && Trigger.isUpdate) {
        doCheckIfApprovalRequired(Trigger.new[0]);
    }

    /**
     * Performs approval required check for header and child objects
     */
    private void doCheckIfApprovalRequired(Apttus__APTS_Agreement__c agrmt) {
        String doubleUnderscore = '__';

        // header param
        String headerIdStatus = null;
        Map<ID, String> headerStatusById = new Map<ID, String>();
        headerStatusById.put(agrmt.Id, agrmt.Apttus_Approval__Approval_Status__c);
        headerIdStatus = agrmt.Id + doubleUnderscore + agrmt.Apttus_Approval__Approval_Status__c;

        // child objects param
        List<String> childIdStatusList = new List<String>();
        Map<ID, String> childObjectsStatusById = new Map<ID, String>();
        // modified child object ids
        List<ID> modifiedChildObjectIds = new List<ID>();

        // get all opportunity products
        // NOTE - INCLUDE ADDITIONAL 'where' CLAUSE, AS REQUIRED, TO IDENTIFY MODIFIED LINES
        for (Apttus__AgreementLineItem__c lineItem : [select Id, Apttus_Approval__Approval_Status__c from Apttus__AgreementLineItem__c
                                                    where Apttus__AgreementId__c = :agrmt.Id
                                                  and Apttus_Approval__Approval_Status__c != :STATUS_APPROVED]) {

            childObjectsStatusById.put(lineItem.Id, lineItem.Apttus_Approval__Approval_Status__c);
            childIdStatusList.add(lineItem.Id + doubleUnderscore + lineItem.Apttus_Approval__Approval_Status__c);

            modifiedChildObjectIds.add(lineItem.Id);
        }

        // cache uncommited version of the context object for the rule evaluation routine to use
        Apttus_Approval.ApprovalsWebService.addSObjectToCache(agrmt);

        // perform the check
        // System.debug('Apptus_Approval.ApprovalsWebService.CheckIfApprovalRequired2 Inputs: ');
        // System.debug('headerIdStatus: ' + headerIdStatus);
        // System.debug('childIdStatusList: ' + childIdStatusList);
        // System.debug('modifiedChildObjectIds: ' + modifiedChildObjectIds);

        List<String> resultList = Apttus_Approval.ApprovalsWebService.CheckIfApprovalRequired2(headerIdStatus, childIdStatusList, modifiedChildObjectIds);

        System.debug('resultList >>>>>' + resultList);

        // construct map by spliting the string by '__'
        Map<ID, String> resultMap = new Map<ID, String>();
        for (String resultStr : resultList) {
            List<String> resultSplitList = resultStr.split(doubleUnderscore);
            String statusValue = resultSplitList[1];
            if (statusValue == VALUE_NULL) {
                statusValue = STATUS_NONE;
            }
            resultMap.put(resultSplitList[0], statusValue);
        }

        // lines
        List<Apttus__AgreementLineItem__c> childUpdateList = new List<Apttus__AgreementLineItem__c>();

        for (ID objId : resultMap.keySet()) {
            if (objId == agrmt.Id) {
                if (resultMap.get(objId) != agrmt.Apttus_Approval__Approval_Status__c) {
                    agrmt.Apttus_Approval__Approval_Status__c = resultMap.get(objId);
                }
                // always reset approval preview status to Pending
                agrmt.Approval_Preview_Status__c = PREVIEW_STATUS_PENDING;
            } else {

                if (resultMap.get(objId) != childObjectsStatusById.get(objId)) {
                    Apttus__AgreementLineItem__c child = new Apttus__AgreementLineItem__c(Id = objId
                                                        , Apttus_Approval__Approval_Status__c = resultMap.get(objId));
                    childUpdateList.add(child);
                }
            }
        }

        // save changes
        if (!childUpdateList.isEmpty()) {
            update childUpdateList;
        }
    }
}