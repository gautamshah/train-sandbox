/**
trigger on Rebate_Agreement__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-02-22      Yuli Fintescu		Created. Update approval status to Not Submitted after approved if any rebated fields are changed
===============================================================================
*/
trigger CPQ_Rebate_Agreement_After on Rebate_Agreement__c (after delete, after insert, after undelete, after update) {
    if (CPQ_Trigger_Profile__c.getInstance().CPQ_Rebate_Agreement_After__c == true) {
		return;
    }
    
	Map<ID, List<Rebate_Agreement__c>> candidates = new Map<ID, List<Rebate_Agreement__c>>();
	if (Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate) {
		for(Rebate_Agreement__c r : Trigger.new) {
			List<Rebate_Agreement__c> rebates;
			if (candidates.containsKey(r.Related_Agreement__c))
				rebates = candidates.get(r.Related_Agreement__c);
			else {
				rebates = new List<Rebate_Agreement__c>();
				candidates.put(r.Related_Agreement__c, rebates);
			}
			rebates.add(r);
		}
	} else if (Trigger.isDelete) {
		for(Rebate_Agreement__c r : Trigger.old) {
			List<Rebate_Agreement__c> rebates;
			if (candidates.containsKey(r.Related_Agreement__c))
				rebates = candidates.get(r.Related_Agreement__c);
			else {
				rebates = new List<Rebate_Agreement__c>();
				candidates.put(r.Related_Agreement__c, rebates);
			}
			rebates.add(r);
		}
	}
	
	CPQ_AgreementProcesses.UpdateRebateSelected(candidates, Trigger.isUpdate, Trigger.oldMap);
}