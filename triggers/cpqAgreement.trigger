/*
    DO NOT PUT ANY LOGIC IN THIS CLASS!!!!
    - Creat a entry() method in your class and call it from this
      class and pass ALL of the trigger context values.
     
COMMENT INSTRUCTIONS
====================
1.  Do not exceed 120 characters on any line to avoid wrapping when printed        *                                   *
2.  Tab stops every 4 spaces
3.  Do not use / *  * / for comments in the code unless you are commenting out a large section of code – use // instead
4.  Id is just a unique value to distinguish changes made on the same date
5.  If there are multiple related Jiras, add them on the next line
6.  Combine the Date & Id and use it to mark changes related to the entry
    // YYYYMMDD A   PAB         CPR-000 Updated comments section
    Only put “inline” comments related to the specific logic found at this location
    
DEVELOPER INFORMATION
=====================
Name                    Initials        Company Website
-----------------------------------------------------------------------
Paul Berglund           PAB             Medtronic.com
Isaac Lewis             IL              Statera.com
Bryan Fry               BF              Statera.com
Henry Noerdlinger       HN              Medtronic.com

MODIFICATION HISTORY
====================
Date        Id  Initials    Jira(s) Jira Title - Brief Summary (Details should be in Jira)
------------------------------------------------------------------------------------------------------------------------
20161118    A   PAB         AV-253  Created for Refactoring

*/
trigger cpqAgreement on Apttus__APTS_Agreement__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update)
{

    cpqAgreement_t.main(
        trigger.isExecuting,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isBefore,
        trigger.isAfter,
        trigger.isUndelete,
        trigger.new,
        trigger.newMap,
        trigger.old,
        trigger.oldMap,
        trigger.size);    
}