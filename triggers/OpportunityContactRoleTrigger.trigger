/****************************************************************************************
 * Name    : OpportunityContactRoleTrigger
 * Author  : Leena Khatri
 * Date    : 22/01/2015 
 * Purpose : Synchronize Outlook
 * Dependencies: Custom Object - Opportunity Contact Role
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               -------
 *****************************************************************************************/
trigger OpportunityContactRoleTrigger on Opportunity_Contact_Role__c (after insert, after update, before delete) 
{
    
    OpportunityContactRole_TriggerHandler   handler = new OpportunityContactRole_TriggerHandler();   
    
    
    if(Trigger.isInsert && Trigger.isAfter)
    {
        handler.OnAfterInsert(Trigger.new);
    }
    else if(Trigger.isDelete && Trigger.isBefore)
    {
        handler.OnBeforeDelete(Trigger.old,Trigger.oldMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter)
    {
        handler.OnAfterUpdate(Trigger.new,Trigger.oldMap,Trigger.newMap);   
    }
    
}