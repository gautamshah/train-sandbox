/****************************************************************************************
 * Name    : LeadTrigger 
 * Author  : Bill Shan
 * Date    : 09/01/2012 
 * Purpose : Lead Trigger Entry
 * Dependencies: Lead_TriggerHandler Apex Class
 *             , Lead Object
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 6/25/13    Gautam Shah         Moved logic from separate class into the trigger
 
 * 8/25/14    Lakhan Dubey        Added logic for before insert and before update:
                                  In the before insert and before update lead trigger,
                                  if the value of "CovidienAccountNumber__c" field is a valid account external ID, 
                                  then set the account which has the external ID to the "AccountAffiliation__c"
                                  account lookup  field of the lead record. 
 
 *****************************************************************************************/
trigger LeadTrigger on Lead (before insert,before update,after insert)  {
if (Trigger.isafter) {
/*
Lead_TriggerHandler handler = new Lead_TriggerHandler(Trigger.isExecuting, Trigger.size);

if(Trigger.isInsert && Trigger.isAfter){
handler.OnAfterInsert(Trigger.new);
}
*/
List<CampaignMember> cms = new List<CampaignMember>();
for(Lead l : Trigger.new)
{
if(l.Campaign_id__c != null)
{
    cms.add(new CampaignMember(LeadId=l.Id, CampaignID=l.Campaign_id__c));
}
}
insert cms;          } 


else  {
Set<String> ACCId=new Set<String>();
Map<String, Account> m = new Map<String, Account>();
for(Lead L:trigger.new)
{
if(L.CovidienAccountNumber__c!=null)
ACCId.add(L.CovidienAccountNumber__c);      
}
for(Account objCS : [SELECT Account_External_ID__c, Id FROM Account where Account_External_ID__c IN :ACCId ])
{
m.put(objCS.Account_External_ID__c, objCS);

}  
                
for(Lead L:trigger.new) 
{
if(m.containskey(L.CovidienAccountNumber__c))
L.AccountAffiliation__c=m.get(L.CovidienAccountNumber__c).Id;
}


      }
                                                                        }