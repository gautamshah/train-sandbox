trigger RAPID_SVMXC_AfterInsertUpdate_AttachmentTrigger on Attachment (after insert, after update) {
    //Not handling undelete functionality as the customer would get the same email twice (was already sent out during original insert)
    RAPID_SVMXC_AttachmentHelper.emailServiceWorkOrder(Trigger.New);
}