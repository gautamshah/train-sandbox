trigger Product2 on Product2 (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update)
{
    Product2_t.main(
            trigger.isExecuting,
            trigger.isInsert,
            trigger.isUpdate,
            trigger.isDelete,
            trigger.isBefore,
            trigger.isAfter,
            trigger.isUndelete,
            trigger.new,
            trigger.newMap,
            trigger.old,
            trigger.oldMap,
            trigger.size);
    
}