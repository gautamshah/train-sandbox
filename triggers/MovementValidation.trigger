/****************************************************************************************
 * Name    : MovementValidation 
 * Author  : Fenny Saputra
 * Date    : 10/Jun/2014 
 * Purpose : To check if movement records are in conflict for same product
             To check demo product doesn't re-loan more than once
 * Dependencies: Movement Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 6/Feb/15    Fenny Saputra        Add logic for HK users
 * 16/03/2015  Chen Li              Add logic for KR to validate one demo (SKU) can only loan to per doctor per Hospital SUM-UP <=30 days during whole life time 
 * 27/Sep/16   Fenny Saputra        Fix the logic to cater for in between dates request
******************************************************************************************/

trigger MovementValidation on Movement__c (before insert, before update) {
    
    if((Trigger.isBefore)&&(Trigger.isInsert)){
        
        //Check if same demo product is being loaned to another hospital
        Map<String, Date> dpStartDates = new Map<String, Date>();
        Map<String, Date> dpEndDates = new Map<String, Date>();
        Map<String, DateTime> dpStartDateTimes = new Map<String, DateTime>();
        Map<String, DateTime> dpEndDateTimes = new Map<String, DateTime>();
        Map<String, String> dpHospitals = new Map<String, String>();
        
//        if(Trigger.isInsert){
            for(Movement__c mov : Trigger.new){
                dpStartDates.put(mov.Demo_Product_Name__c, mov.Start_Date__c);
                dpEndDates.put(mov.Demo_Product_Name__c, mov.End_Date__c);
                dpStartDateTimes.put(mov.Demo_Product_Name__c, mov.Start_Date_and_Time__c);
                dpEndDateTimes.put(mov.Demo_Product_Name__c, mov.End_Date_and_Time__c);
                system.debug('dpStartDateTimes' + dpStartDateTimes);
                system.debug('dpEndDateTimes' + dpEndDateTimes);
                dpHospitals.put(mov.Demo_Product_Name__c, mov.Hospital__c);
            }
            
            List<Movement__c> movList = [select Id, Name, Hospital__c, Demo_Product_Name__c, Start_Date__c, End_Date__c, 
                                     Reloan__c, Start_Date_and_Time__c, End_Date_and_Time__c from Movement__c 
                                     where (Demo_Product_Name__c in :dpStartDates.keySet()
                                     and Approval_Status__c = 'Approved') 
                                     or Demo_Product_Name__c in :dpStartDateTimes.keySet()];
        
            system.debug('movList' + movList);
            
            for(Movement__c movnew : Trigger.new){
                for(Movement__c mov : movList){
                    system.debug('start date' + dpStartDates.get(mov.Demo_Product_Name__c));
                    system.debug('enddate' + dpEndDates.get(mov.Demo_Product_Name__c));
                    system.debug('end date time' + mov.End_Date_and_Time__c);
                    system.debug('dpDateTimes-start' + dpStartDateTimes);
                    system.debug('start date time' + mov.Start_Date_and_Time__c);
                    system.debug('dpDateTimes-end' + dpEndDateTimes);

                    if(( mov.End_Date__c > dpStartDates.get(mov.Demo_Product_Name__c) &&
                    mov.Start_Date__c < dpEndDates.get(mov.Demo_Product_Name__c)) ||
                    ( mov.End_Date_and_Time__c > dpStartDateTimes.get(mov.Demo_Product_Name__c) && 
                    mov.Start_Date_and_Time__c < dpEndDateTimes.get(mov.Demo_Product_Name__c))){
                        movnew.addError('Demo Product is currently being loaned at another place');
                        return;
                    }
                    
                    if ((mov.Hospital__c == dpHospitals.get(mov.Demo_Product_Name__c))
                    && mov.Reloan__c == true 
                    && (mov.End_Date__c > (dpStartDates.get(mov.Demo_Product_Name__c) - 30))){
                        movnew.addError('Demo Product cannot be loaned more than one time');
                        return;
                    }
                }
            }
//        }
/*
        else if(Trigger.isUpdate){
            for(Movement__c mov : Trigger.old){
                dpDates.put(mov.Demo_Product_Name__c, mov.Start_Date__c);
                dpDateTimes.put(mov.Demo_Product_Name__c, mov.Start_Date_and_Time__c);
                system.debug('dpDateTimes' + dpDateTimes);
                dpHospitals.put(mov.Demo_Product_Name__c, mov.Hospital__c);
            }
            
            
            List<Movement__c> movList = [select Id, Name, Hospital__c, Demo_Product_Name__c, End_Date__c, 
                                     Reloan__c, End_Date_and_Time__c from Movement__c 
                                     where (Demo_Product_Name__c in :dpDates.keySet()
                                     and Approval_Status__c = 'Approved') 
                                     or Demo_Product_Name__c in :dpDateTimes.keySet()];
        
            system.debug('movList' + movList);
    
            
            for(Movement__c movold : Trigger.old){
                for(Movement__c mov : movList){
                    system.debug('start date' + dpDates.get(mov.Demo_Product_Name__c));
                    
                    if((mov.End_Date__c > dpDates.get(mov.Demo_Product_Name__c)) ||
                    (mov.End_Date_and_Time__c > dpDateTimes.get(mov.Demo_Product_Name__c))){
                        Movement__c actualmov = Trigger.oldMap.get(mov.Id);
                        actualmov.addError('Demo Product is currently being loaned at another place');
                        return;
                    }
                }
            } 
        } */                     
    }
    //Below logic is for KR only as March 2016
    else if((Trigger.isBefore)&&((Trigger.isUpdate)||(Trigger.isInsert))){
 
    
/*
    Contact_Name__c
    Logic to implement :
1.  Filter the new/edit records the country = KR and Approval Status = Draft
2.  Get current movement request SKU code.
3.   Date logic:
a.  Condition In one record : End Date-Start Date <=30  -->validation rule
b.  Get Contact Name (current is Free Text? ),
Connected record type.
c.  Enquire all movement requests having the same SKU code and country = KR and all status and same Contact and same hospital
d.  SUM-UP the date period from above historical result,  add current movement request period > 30. Show alert or no alert
*/
     try{ 
    Set<Id> CurrentMovementIDSet= new Set<Id>();//Before Country filter
    
    list<Movement__c> MovementRequestList= new list<Movement__c>();
    Set<Id> MovementIDSet=new set<Id>();
     Set<Id> DemoproductSet=new Set<Id>();
     List<Demo_Product__c> DemoproductList=new List<Demo_Product__c>();
    Set<Id> DemoIDSet=new Set<Id>();
    
    Set<String> SKUset= new Set<String>();
    Set<Id> contactSet= new Set<Id>();
    Set<Id> acctSet= new Set<Id>();
    
    list<Movement__c> compareMovementRequestList= new list<Movement__c>();
    Map<Id,integer> ErrorMovementRequest= new Map<Id,integer>(); // Movement request Id, historyLoanDays
    
    //c.    Enquire all movement requests having the same SKU code and country = KR and all //status and same Contact and same hospital
    

    
    Set<String> AllowedCountries = new Set<String>();
    //Start-List of allowed Countries for below validation logic
    AllowedCountries.add('KR'); 
    //AllowedCountries.add('SG');// TEMP for testing
    DemoproductList = [SELECT id, name, Country__c from Demo_Product__c where 
    Country__c in :AllowedCountries and Country__c !='US' and Asset_Name__c!=null limit 100000] ;
    for(Demo_Product__c d:DemoproductList)
        DemoproductSet.add(d.Id);
     
     
     for(Movement__c m: Trigger.new){
        System.debug('=====START Trigger');
        CurrentMovementIDSet.add(m.Id);
        System.debug('m.Demo_Product_Name__r: '+m.Demo_Product_Name__c);
        //System.debug('m.Demo_Product_Name__r.Country__c: '+m.Demo_Product_Name__r.Country__c);
        if(DemoproductSet.contains(m.Demo_Product_Name__c)){
            //Add below condition due to sometime KR will use the demo product for event and seminar which 30 days rules not applied
            if((m.Hospital__c!=null)&&(m.Contact_Name__c!=null))
            {MovementRequestList.add(m);
            System.debug('add MovementRequest: '+m);}
        }    
        
        System.debug('111 CurrentMovementIDSet: ' + CurrentMovementIDSet);
    }
     
     //TODO
     
     /*
      MovementRequestList = [SELECT id, name, Hospital__c,Contact_Name__c,End_Date__c, Demo_End_Date__c,Product_SKU__c, Start_Date__c FROM Movement__c WHERE Id in :CurrentMovementIDSet and Movement__c.in :AllowedCountries]; 
     */
      System.debug('111 MovementRequestList: ' + MovementRequestList);
     
     
     for(Movement__c m: MovementRequestList){
        MovementIDSet.add(m.id);
        contactSet.add(m.Contact_Name__c);
        acctSet.add(m.Hospital__c);
        SKUset.add(m.Product_SKU__c);
        System.debug('111 m.Product_SKU__c: ' + m.Product_SKU__c);
    }
      
     compareMovementRequestList = [SELECT id, name, Hospital__c,Contact_Name__c, End_Date__c,Product_SKU__c, Start_Date__c FROM Movement__c WHERE Movement__c.Demo_Product_Name__r.Country__c in :AllowedCountries 
      and Hospital__c in :acctSet
      and Contact_Name__c in :contactSet
      and Product_SKU__c in :SKUset
      and id NOT IN :MovementIDSet
      ]; 
      System.debug('111 compareMovementRequestList: ' + compareMovementRequestList);
      
       for(Movement__c m: MovementRequestList){
           integer intDays=0;   
            System.debug('222 m.Start_Date__c: ' + m.Start_Date__c+'  m.End_Date__c  '+ m.End_Date__c);        
           intDays =  m.Start_Date__c.daysBetween( m.End_Date__c);
           integer totalLoanDays=intDays;
           integer historyLoanDays=0;
           System.debug('222: m' + m);
           System.debug('222 intDays: ' + intDays);
           for(Movement__c m2: compareMovementRequestList)
           {
               if((m2.Hospital__c==m.Hospital__c)&&(m2.Contact_Name__c==m.Contact_Name__c)&&(m2.Product_SKU__c==m.Product_SKU__c))
               {
                   historyLoanDays+=m2.Start_Date__c.daysBetween(m2.End_Date__c);
                   totalLoanDays+=m2.Start_Date__c.daysBetween(m2.End_Date__c);
                   System.debug('222: m2' + m2);
                   System.debug('222: totalLoanDays' + totalLoanDays);
               }   
           } 
              System.debug('333 totalLoanDays: ' + totalLoanDays);
               System.debug('333 historyLoanDays: ' + historyLoanDays);
            if(totalLoanDays>30)
                if((m.Hospital__c!=null)&&(m.Contact_Name__c!=null))
                ErrorMovementRequest.put(m.Id,historyLoanDays);
            
           }
            System.debug('333 ErrorMovementRequest: ' + ErrorMovementRequest);
           
           
     for(Movement__c m: Trigger.new){
        System.debug('=====add error message');
        if(ErrorMovementRequest.keyset().contains(m.Id))
            {   
                System.debug('ErrorMovementRequest: ' + m);
                m.addError('This demo product has exceeded the loan period of 30 days for this doctor in this hospital. Currently it has been loaned for '+ErrorMovementRequest.get(m.Id)+' days.');   
                
            }
    }
       
           
         
           
           
     System.debug('=====END Trigger');
    
        
    }
    Catch (Exception e) { System.debug('======Comparing Demo Exception: '+e.getMessage()); }  
}
}