trigger EMS_EventBeneficiariesTrigger on Event_Beneficiaries__c (after insert,after update,after delete) {


   
   // List of parent record ids to update
   Set<Id> parentIds = new Set<Id>();
   
   // In-memory copy of parent records
   Map<Id,EMS_Event__c> parentRecords = new Map<Id,EMS_Event__c>();
   
   // Gather the list of ID values to query on
   for(Event_Beneficiaries__c c: ((Trigger.isDelete)?(Trigger.old):(Trigger.new))){
       parentIds.add(c.EMS_Event__c);
    
    }
    
   // Avoid null ID values
   parentIds.remove(null);
   
   // Create in-memory copy of parents
   
    for(Id parentId : parentIds){
    parentRecords.put(parentId,new EMS_Event__c(Id=parentId,Total_Meal_Amount__c=0,Total_LodA__c =0,Total_Local_GTA__c=0,
                                                Total_Air_Travel__c=0,Total_Consultancy_Fee__c=0,Total_Others_Amount__c =0,
                                                Total_Gifts_amount__c=0,Approved_Meal_Amount__c=0,Approved_Lodging_Amount__c=0,
                                                Approved_LGT_Amount__c=0,Approved_Air_Travel__c=0,Approved_Consultancy_fee_amount__c=0,
                                                Approved_Others_Amount__c=0,Approved_Gifts_amount__c =0));
                                                
    }
    
    if(parentIds.size()>0){   
                                             
          // Query all children for all parents, update Rollup Field value for Budgeted Values
          for(Event_Beneficiaries__c c : [select EMS_Event__c,Meal__c,Lodging_Amount__c,Total_Ground_Transportation__c,
                                                        Air_Travel__c,Consultancy_fees__c,Others_Others__c,Gifts__c from Event_Beneficiaries__c
                                                        where Ems_Event__c in :parentIds and Invitee_type__c='Budgeted Beneficiary' AND Type__c IN('Individual Beneficiary','Organization')]){
             
             parentRecords.get(c.EMS_Event__c).Total_Meal_Amount__c += ((c.Meal__c==NULL)?(0):(c.Meal__c));
             parentRecords.get(c.EMS_Event__c).Total_LodA__c += ((c.Lodging_Amount__c==NULL)?0:(c.Lodging_Amount__c));
             parentRecords.get(c.EMS_Event__c).Total_Local_GTA__c += ((c.Total_Ground_Transportation__c==NULL)?0:(c.Total_Ground_Transportation__c));
             parentRecords.get(c.EMS_Event__c).Total_Air_Travel__c+= ((c.Air_Travel__c==NULL)?0:(c.Air_Travel__c));
             parentRecords.get(c.EMS_Event__c).Total_Consultancy_Fee__c += ((c.Consultancy_fees__c==NULL)?0:(c.Consultancy_fees__c));
             parentRecords.get(c.EMS_Event__c).Total_Others_Amount__c += ((c.Others_Others__c==NULL)?0:(c.Others_Others__c));
             parentRecords.get(c.EMS_Event__c).Total_Gifts_amount__c += ((c.Gifts__c==NULL)?0:(c.Gifts__c));
           }
           
           // Query all children for all parents, update Rollup Field value for Approved values
           for(Event_Beneficiaries__c c : [select EMS_Event__c,Meal__c,Lodging_Amount__c,Total_Ground_Transportation__c,
                                                        Air_Travel__c,Consultancy_fees__c,Others_Others__c,Gifts__c  from Event_Beneficiaries__c
                                                   where Ems_Event__c in :parentIds and Invitee_type__c='PreEvent Budgeted Beneficiary' AND Type__c IN('Individual Beneficiary','Organization')]){
             
             parentRecords.get(c.EMS_Event__c).Approved_Meal_Amount__c+= ((c.Meal__c==NULL)?(0):(c.Meal__c));
             parentRecords.get(c.EMS_Event__c).Approved_Lodging_Amount__c+= ((c.Lodging_Amount__c==NULL)?0:(c.Lodging_Amount__c));
             parentRecords.get(c.EMS_Event__c).Approved_LGT_Amount__c+= ((c.Total_Ground_Transportation__c==NULL)?0:(c.Total_Ground_Transportation__c));
             parentRecords.get(c.EMS_Event__c).Approved_Air_Travel__c+= ((c.Air_Travel__c==NULL)?0:(c.Air_Travel__c));
             parentRecords.get(c.EMS_Event__c).Approved_Consultancy_fee_amount__c+= ((c.Consultancy_fees__c==NULL)?0:(c.Consultancy_fees__c));
             parentRecords.get(c.EMS_Event__c).Approved_Others_Amount__c+= ((c.Others_Others__c==NULL)?0:(c.Others_Others__c));
             parentRecords.get(c.EMS_Event__c).Approved_Gifts_amount__c  += ((c.Gifts__c==NULL)?0:(c.Gifts__c));
           }
           
             // Commit changes to the database
             Database.saveresult[] sr = Database.update(parentRecords.values(),false);
             System.debug('----->'+sr);

    }
   
   
   
   
   
   }