trigger ContactWorkplaceLink on Contact (before update) {
/****************************************************************************************
    * Name    : Contact Workplace Link
    * Author  : Mike Melcher
    * Date    : 12-21-2011
    * Purpose : If the Link To Workplace Account is filled in update the AccountId and
    *           recordtype for the contact
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 1/9/2012     MJM      Set recordtype to Global Affiliated Clinician or Global Affiliated Non Clinician
    *                       based on the recordtype of the Master Contact 
    * 1/25/2012    MJM      Update recordtype names to match Contact recortype name changes
    * 3-02-2012    MJM      Trigger de-activated per Case 10006
    *
    *****************************************************************************************/

// 
    List<Contact> ContactsForUpdate = new List<Contact>();
    Set<Id> MasterContacts = new Set<Id>();
    

    for (integer i=0;i<Trigger.new.size();i++) {
       if ((Trigger.new[i].Link_To_Healthcare_Facility__c != null &&
           Trigger.old[i].Link_To_Healthcare_Facility__c == null)) {
           ContactsForUpdate.add(Trigger.new[i]);
           MasterContacts.add(Trigger.new[i].Master_Contact_Record__c);
       }
    }
    
    if (ContactsForUpdate.size() > 0) {
       // Get Recordtypes
       DataQuality dq = new DataQuality();
       Map<String,Id> rtmap = dq.getRecordTypes(); 
       
       // Map Master Contacts
       Map<Id,Id> MasterMap = new Map<Id,Id>();
       for (Contact c : [select Id, RecordTypeId from Contact where Id in :MasterContacts]) {
          MasterMap.put(c.Id, c.RecordTypeId);
       }

       
       Id currentUser = UserInfo.getUserId();
       id queue = [select QueueId from QueueSObject where Queue.Name = 'Data Quality'].QueueId;  
          
       
       for (contact c : ContactsForUpdate) {
          if (c.AccountId == null) {
             c.AccountID = c.Link_To_Healthcare_Facility__c;
          }
  //        if (MasterMap.get(c.Master_Clinician_Record__c) == rtMap.get('Global Master Clinician')) {
  //           c.recordtypeid = rtMap.get('Connected Clinician');
  //        } else { 
  //           if (MasterMap.get(c.Master_Clinician_Record__c) == rtMap.get('Global Master Non Clinician')) {
  //              c.recordtypeid = rtMap.get('Connected Non Clinician Contact');
  //           }
  //        }

          
          ///////////////////////////////////////////////
          // Create DQ Case 
          ///////////////////////////////////////////////
          dq.createCase(queue,'DQ-Validate New Workplace Link',c.Id,'Data Quality','Medium', currentUser,'Validation','Assigned');           
       }
   }

}