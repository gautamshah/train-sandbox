/***********************
Aurthor: Yap Zhen-Xiong
Email: Zhenxiong.Yap@covidien.com
Description: Trigger on Task to send Email to Task Owner when a Feedback is given. Currently works for Asia only.
Edited 6/11/14 by Gautam Shah --::-- Modified so that queries and code do not run if basic conditions are not met.
***********************/

trigger Task_Send_Email_Upon_Mgr_Feedback on Task (before update) 
{    
    Set<Id> ownerIds = new Set<Id>();
    Set<String> AllowedCountries = new Set<String>();
    Map<Id, List<Task>> PassedCriteriaTasks = new Map<id, List<Task>>(); //Owner mapped to multiple Task
    Map<id, User> userMap = new Map<Id, User>();
    Map<id, User> allowedUserMap = new Map<id, User>();
    
    //Start-List of allowed Countries for Email notification
    AllowedCountries.add('SG');
    AllowedCountries.add('HK');
    AllowedCountries.add('KR');
    AllowedCountries.add('CN');
    AllowedCountries.add('TW');
    //End-list
    
    for(Task tsk: Trigger.new)
    {
	    Task oldTask = Trigger.oldMap.get(tsk.ID);
    	if(tsk.JP_Feedback__c != oldTask.JP_Feedback__c){ //check if feedback has been changed
        	ownerIds.add(tsk.OwnerId);
    	}
    }
    if(ownerIds.size() > 0)
    {
	    for(User u : [SELECT Id, Country, Email, Name, LanguageLocaleKey FROM User WHERE Id in :ownerIds])
	    {
	    	userMap.put(u.Id, u);
	    }
    }   
    //Criteria Check for Email Notification
    if(userMap.size() > 0)
    {
	    for(Task tsk: Trigger.new)
	    {
	        User owner = userMap.get(tsk.ownerId);
	        if(AllowedCountries.contains(owner.Country)){ //check if Owner's Country is in the Allowed Country List
	            ownerIds.add(tsk.ownerId);
	            List<Task> taskList;  
	            
	            if(PassedCriteriaTasks.get(tsk.ownerId) == null)
	                taskList = new List<Task>(); 
	            else
	                taskList = PassedCriteriaTasks.get(tsk.ownerId);
	  
	            taskList.add(tsk);
	            PassedCriteriaTasks.put(tsk.ownerId, taskList);
	            allowedUserMap.put(tsk.ownerId, userMap.get(tsk.OwnerId));
	
	        }
	    }
    }
    
    if(PassedCriteriaTasks.size() > 0 && allowedUserMap.size() > 0)
    {
	    EmailTemplate et_TW;
	    EmailTemplate et_CN;
	    EmailTemplate et_Default;
	    
	    for(EmailTemplate et : [SELECT id, developerName, Body, HtmlValue, Subject FROM EmailTemplate WHERE developerName = 'Manager_Feedback_Notification' OR developerName = 'Manager_Feedback_Notification_TW' OR developerName = 'Manager_Feedback_Notification_CN'])
		{
	        String devName = et.developerName;
	        if(devName.contains('_TW'))
	            et_TW = et;    
	        else if(devName.contains('_CN'))
	            et_CN = et;  
	        else  
	            et_Default = et;
	    }
    
	    //Code Section that sends the email.
	    for(User u : allowedUserMap.values()){
	        for(Task t : PassedCriteriaTasks.get(u.id)){
	            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
	            
	            String[] toAddresses = new String[] {u.Email};
	            mail.setToAddresses(toAddresses);    // Set the TO addresses  
	            
	            EmailTemplate et; 
	            if(u.LanguageLocaleKey == 'zh_TW')
	                et = et_TW;
	            else if(u.LanguageLocaleKey == 'zh_CN')
	                et = et_CN;
	            else
	                et = et_Default;
	         
	            mail.setSenderDisplayName('Covidien - Salesforce.com');
	            mail.setReplyTo('no-reply@covidien.com');
	            mail.setUseSignature(false);
	            
	            String subject = et.Subject;
	            subject = subject.replace('{!Task.Subject}', t.Subject);
	            mail.setSubject(subject);
	            
	            System.debug('Email Template Body: '+ et.body);
	            String htmlBody = et.Body;
	            htmlBody = htmlBody.replace('{!Task.Subject}', t.Subject);
	            htmlBody = htmlBody.replace('{!Task.JP_Feedback__c}', t.JP_Feedback__c);
	            String strURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.id;
	            htmlBody = htmlBody.replace('{!Task.Link}', strURL);
	            mail.setPlainTextBody(htmlBody); 
	            
	            /* Next, create a string template. Specify {0}, {1} etc. in place of actual values.
	            // You can replace these values with a call to String.Format.
	            String template;
	            if(u.LanguageLocaleKey == 'zh_CN'){
	                template = '你好{0}， 你收到一个反馈信息，内容如下： \n\n';
	                template+= '主题 - {1}\n';
	                template+= '到期日期 - {2}\n';
	                template+= '反馈内容 - {3}\n';
	                template+= '任务链接 - {4}\n';
	                //template+= '事件链接 - {4}\n';
	            }else if(u.LanguageLocaleKey == 'zh_TW'){
	                template = 'Hello {0}, \n\nYou have recieved a feedback. Here are the details - \n\n';
	                template+= 'Subject - {1}\n';
	                template+= 'Due Date - {2}\n';
	                template+= 'Feedback - {3}\n';
	                template+= 'Task Link - {4}\n';
	            }else{
	                template = 'Hello {0}, \n\nYou have recieved a feedback. Here are the details - \n\n';
	                template+= 'Subject - {1}\n';
	                template+= 'Due Date - {2}\n';
	                template+= 'Feedback - {3}\n';
	                template+= 'Task Link - {4}\n';
	                //template+= 'Test - {5}\n';
	            }
	            
	            String duedate = '';
	            
	            if (t.ActivityDate==null)
	                duedate = '';
	            else
	                duedate = t.ActivityDate.format();
	                
	            List<String> args = new List<String>();
	            args.add(u.Name); //{0}
	            args.add(t.Subject); //{1}
	            args.add(duedate); //{2}
	            args.add(t.JP_Feedback__c); //{3}
	            String strURL= URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.id;
	            args.add(strURL); //{4}
	            //args.add(u.LanguageLocaleKey ); //{}
	                   
	            // Here's the String.format() call.
	            String formattedHtml = String.format(template, args);
	            
	            mail.setPlainTextBody(formattedHtml);*/
	                
	            Messaging.SendEmail(new Messaging.SingleEmailMessage[] {mail});
	        }
	    }
	}
}