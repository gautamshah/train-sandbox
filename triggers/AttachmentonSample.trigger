/****************************************************************************************
 * Name    : AttachmentonSampleTrigger 
 * Author  : Fenny Saputra
 * Date    : 27/01/2016 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
trigger AttachmentonSample on Attachment (before insert, before delete) {
    Set<ID> sampleIds = new Set<ID>();
    Set<ID> sampleDelIds = new Set<ID>();
    
    if(Trigger.isInsert){
    for(Attachment att : trigger.New){
         //Check if added attachment is related to Sample or not
         if(att.ParentId.getSobjectType() == Samples__c.SobjectType){
              sampleIds.add(att.ParentId);
         }
    }
    
    List<Samples__c> sampleList = [select id, Product_Evaluation_Attached__c from Samples__c where id in : sampleIds];
    if(sampleList!=null && sampleList.size()>0){
        for(Samples__c sample : sampleList){
            sample.Product_Evaluation_Attached__c = true;
        }
        update sampleList;
    }
    }
    
    if(Trigger.isDelete){
    for(Attachment att : trigger.old){
         //Check if added attachment is related to Sample or not
         if(att.ParentId.getSobjectType() == Samples__c.SobjectType){
              sampleDelIds.add(att.ParentId);
         }
    } system.debug('sampleDelID' + sampleDelIds);
    
    List<Samples__c> sampleDelList = [select id, Product_Evaluation_Attached__c from Samples__c where id in : sampleDelIds];
    if(sampleDelList!=null && sampleDelList.size()>0){
        for(Samples__c sample : sampleDelList){
            sample.Product_Evaluation_Attached__c = false;
        }
        update sampleDelList;
    } system.debug('sampleDelList' + sampleDelList);
    } 
}