trigger JP_OpportunityTrigger on Opportunity (before insert, before update, after update) {
     /****************************************************************************************
     * Name    : JP_OpportunityTrigger 
     * Author  : Hiroko Kambayashi
     * Date    : 10/11/2012 
     * Purpose : To input Intimacy information when Key Dr (i.e., Contact) is inputed 
     *           or Owner is changed
     * Dependencies: Opportunity Object
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 
     *
     *****************************************************************************************/
    //Create TriggerHandler instance
    JP_OpportunityTriggerHandler handler = new JP_OpportunityTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        } else {
            handler.onBeforeUpdate(Trigger.oldMap, Trigger.new);
        }
    } else {
        if(Trigger.isUpdate){
            handler.onAfterUpdate(Trigger.oldMap, Trigger.new);
        }
    }
}