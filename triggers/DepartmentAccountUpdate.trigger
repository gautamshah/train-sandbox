trigger DepartmentAccountUpdate on Department__c (after update) {
    /****************************************************************************************
    * Name    : DepartmentAccountUpdate
    * Author  : Mike Melcher
    * Date    : 11-20-2011
    * Purpose : When the Account on a Department record is changed from pointing to a Workplace
    *           Account to a Healthcare Facility, update the Workplace Account and populate the 
    *           Merge Into Healthcare Facility field with the Account entered on the Department record.
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 2-7-2012     MJM          Trigger Disabled -- No longer needed
    *
    *****************************************************************************************/
/*

Map<Id,Id> changedAccountsMap = new Map<Id,Id>();
Set<Id> AccountIdsInTrigger = new Set<Id>();
Map<Id,Account> AccountRTMap = new Map<Id,Account>();
List<Account> AccountsForUpdate = new List<Account>();

for (integer i=0;i<Trigger.size;i++) {
   if (Trigger.old[i].Account__c != Trigger.new[i].Account__c) {
      changedAccountsMap.put(Trigger.old[i].Account__c,Trigger.new[i].Account__c);
      AccountIdsInTrigger.add(Trigger.old[i].Account__c);
      AccountIdsInTrigger.add(Trigger.new[i].Account__c);
   }
}

if (changedAccountsMap.size() > 0) {
   id workplaceRT;
   for (Recordtype rt : [select Id from RecordType where Name = 'Workplace Account']) {
      workplaceRT = rt.Id;
   }
   for (Account a : [select Id, RecordtypeId from Account where Id in :AccountIdsInTrigger]) {
      AccountRTMap.put(a.Id, a);
   }
   for (Id cID : changedAccountsMap.keyset()) {
      if (AccountRTMap.get(cID).RecordTypeId == workplaceRT &&
          AccountRTMap.get(changedAccountsMap.get(cID)).RecordTypeId != workplaceRT) {
      // The Account on the Department was changed from pointing to a Workplace Account to a Non-Workplace Account
      //   Populate the Merge Into Account field on the Workplace Account
         account a = AccountRTMap.get(cID);
         a.Merge_Into_Healthcare_Facility__c = changedAccountsMap.get(cID);
         AccountsForUpdate.add(a);
      }
   }
   
   if (AccountsForUpdate.size() > 0) {
      update AccountsForUpdate;
   }
   
   
}

*/
}