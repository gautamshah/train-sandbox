// Moved to cpqOfferDevelopmentAssignment
trigger cpqODAssignment on CPQ_SSG_OD_Assignments__c
	(
		after delete,
		after insert,
		after undelete,
		after update,
		before delete,
		before insert,
		before update
	) 
{
}