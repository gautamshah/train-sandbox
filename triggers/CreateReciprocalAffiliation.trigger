trigger CreateReciprocalAffiliation on Account_Affiliation__c (after insert) {

 /****************************************************************************************
    * Name    : CreateReciprocalAffiliation
    * Author  : Mike Melcher
    * Date    : 12-19-2011
    * Purpose : Create a reciprocal Account Affiliation when a new Account Affiliation is created
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 
    *
    *****************************************************************************************/
    List<Account_Affiliation__c> Existing = new List<Account_Affiliation__c>();
    List<Account_Affiliation__c> Reciprocals = new List<Account_Affiliation__c>();
    Set<Id> WithIds = new Set<Id>();
    Set<Id> MemberIds = new Set<Id>();

    for (Account_Affiliation__c af : Trigger.new) {
    
       WithIds.add(af.Affiliated_With__c);
       MemberIds.add(af.AffiliationMember__c);
       Account_Affiliation__c naf = new Account_Affiliation__c();
       
       //Switch the Affiliated With and the Affiliation Member
       id am = af.AffiliationMember__c;
       naf.AffiliationMember__c = af.Affiliated_With__c;
       naf.Affiliated_With__c = am;
       
       // Populate remaining fields
       naf.Affiliation_Type__c = af.Affiliation_Type__c;
       naf.Business_Unit__c = af.Business_Unit__c;
       naf.Comments__c = af.Comments__c;
       //naf.CovPrimaryRelationship__c = af.CovPrimaryRelationship__c;
       naf.LegalRelationship__c = af.LegalRelationship__c;
       naf.Member_ID__c = af.Member_Id__c;
       naf.Franchise__c = af.Franchise__c;
       naf.Source__c  = af.Source__c;
       
       Reciprocals.add(naf);
       
    }
    // Get list of current Account Affiliations with links used by the trigger
    
    Existing = [select id, Affiliated_With__c, AffiliationMember__c
                from Account_Affiliation__c
                where Affiliated_With__c in :MemberIds
                  and AffiliationMember__c in :WithIds];
    
    if (Existing.size() == 0) { // There were no existing reciprocal Affiliations, so insert entire list from trigger
       insert Reciprocals;
    } else {  // Go through reciprocals from trigger, create and insert a list of ones that don't already exist
       List<Account_Affiliation__c> NewReciprocals = new List<Account_Affiliation__c>();
       for (Account_Affiliation__c af : Reciprocals) {
          boolean match = false;
          for (Account_Affiliation__c e : Existing) {
             if (e.Affiliated_With__c == af.Affiliated_With__c &&
                 e.AffiliationMember__c == af.AffiliationMember__c) {
                    match = true;
             }
          }
          if (!match) {
             NewReciprocals.add(af);
          }
       }
       
       if (NewReciprocals.size() > 0) {
          insert NewReciprocals;
       }
    }
}