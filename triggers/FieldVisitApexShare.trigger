trigger FieldVisitApexShare on Field_Visit__c (after insert) {  
  
if(trigger.isInsert){  

//Map Record Type

map<id, recordtype> recordTypes = new map<id, recordtype>(
[select id,name from recordtype where sobjecttype='Field_Visit__c']);

List<Field_Visit__Share> fvShares = new List<Field_Visit__Share>();  

for(Field_Visit__c fv: trigger.new){  

if(recordTypes.get(fv.recordtypeid)!=null && (recordtypes.get(fv.recordtypeid).name=='FST Field Visit #1'|| recordtypes.get(fv.recordtypeid).name=='FST Field Visit #2' || recordtypes.get(fv.recordtypeid).name=='FST Field Visit #3' || recordtypes.get(fv.recordtypeid).name=='FST-AST Field Visit')){

//Add FST Sharing
Field_Visit__Share FST_Share = new Field_Visit__Share();  
FST_Share.ParentId = fv.Id;  
FST_Share.UserOrGroupId = fv.FST__c;  
FST_Share.AccessLevel = 'edit';  
FST_Share.RowCause = Schema.Field_Visit__Share.RowCause.FST_Defined_on_Record__c; 

fvShares.add(FST_Share);  

//Add Seller Sharing
Field_Visit__Share Seller_Share = new Field_Visit__Share();  
Seller_Share.ParentId = fv.Id;  
Seller_Share.UserOrGroupId = fv.Seller__c;  
Seller_Share.AccessLevel = 'edit';  
Seller_Share.RowCause = Schema.Field_Visit__Share.RowCause.Seller_Defined_on_Record__c;  

fvShares.add(Seller_Share); 

//Add RM Sharing
Field_Visit__Share RM_Share = new Field_Visit__Share();  
RM_Share.ParentId = fv.Id;  
RM_Share.UserOrGroupId = fv.RM__c;  
RM_Share.AccessLevel = 'edit';  
RM_Share.RowCause = Schema.Field_Visit__Share.RowCause.RM_Defined_on_Record__c;  

fvShares.add(RM_Share); 

}
}

// Insert all of the newly created Share records and capture save result   

Database.SaveResult[] fvSharesInsertResult = Database.insert(fvShares,false);  

}  

}