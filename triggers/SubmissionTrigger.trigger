/****************************************************************************************
 * Name    : SubmissionTrigger 
 * Author  : Bill Shan
 * Date    : 28/8/2014 
 * Purpose : Submission Trigger Entry
 * Dependencies: Submission Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
trigger SubmissionTrigger on Cycle_Period__c (before insert) {
	
	Submission_TriggerHandler handler = new Submission_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }

}