/****************************************************************************************
   * Name    : create_visibility_on_Mastercntctrecrd
   * Author  : Lakhan Dubey
   * Date    : 19/12/2014
   * Purpose :Create a visibility on the Master contact record for all the OCRs from the connected contacts.
   * Dependencies: Opportunity_Contact_Role__c,contact
 *****************************************************************************************/
trigger create_visibility_on_Mastercntctrecrd on Opportunity_Contact_Role__c (before insert,before update) {

Set<Id> set_OpportunityContactIDs = new Set<Id>();

For(Opportunity_Contact_Role__c ocr : trigger.new)

if(ocr.Opportunity_Contact__c!=null)
{
set_OpportunityContactIDs.add(ocr.Opportunity_Contact__c);
}

Map<Id,Contact> map_MasterContactRecord = new Map<Id,Contact>
    (   [   SELECT  Master_Contact_Record__c
            FROM    Contact
            WHERE   Id IN :set_OpportunityContactIDs
        ]
    );
    
    
for ( Opportunity_Contact_Role__c ocr1: trigger.new )
    {
        
        if(ocr1.Opportunity_Contact__c!=null)
        ocr1.MasterContactrecord__c = map_MasterContactRecord.get( ocr1.Opportunity_Contact__c ).Master_Contact_Record__c;
    }
}