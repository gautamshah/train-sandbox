trigger Contact_Affiliation_Territories on Contact_Affiliation__c (before insert, before update) {
   /****************************************************************************************
	* Name    : Contact_Affiliation_Territories
	* Author  : Nathan Shinn
	* Date    : 11-07-2011
	* Purpose : Pulls the territory information from the Account Territory Object and populates 
	*           it on the Contact Affiliation Object. This would need to include all Territories.
	*                 
	*
	* ========================
	* = MODIFICATION HISTORY =
	* ========================
	* DATE        AUTHOR               CHANGE
	* ----        ------               ------
	* 11-07-2011  Nathan Shinn         Created
	*
	*****************************************************************************************/
	set<Id> accountIds = new set<Id>();
	map<Id, String> acctTerritoriesMap = new map<Id, String>();
	map<String, List<Id>> territoryAccontMap = new map<String, List<Id>>();
	
	for(Contact_Affiliation__c af : trigger.new)
	   accountIds.add(af.AffiliatedTo__c);
	   
	for(Account_Territory__c at : [select Id, Name, accountId__c , Territory_ID__c
	                                 from Account_Territory__c 
	                                where AccountId__c in :accountIds
	                               order by AccountId__c])
	{
		if(territoryAccontMap.get(at.Territory_ID__c) == null)
		{
		  List<Id> accountId = new List<Id>();
		  accountId.add(at.AccountId__c);
		  territoryAccontMap.put(at.Territory_ID__c.substring(0,15), accountId );
		  
		}
		else
		{
			List<String> acctIds = territoryAccontMap.get(at.Territory_ID__c);
			acctIds.add(at.accountId__c);
		    territoryAccontMap.put(at.Territory_ID__c.substring(0,15), acctIds);
		}
	}
	
	for(Territory t : [select Id, Name from Territory where Id in :territoryAccontMap.keySet()])
	{
		if( territoryAccontMap.get(((String)t.Id).substring(0,15) ) == null)
		  continue;
		  
		for(Id a : territoryAccontMap.get(((String)t.Id).substring(0,15)))
		{
			if(acctTerritoriesMap.get(a) == null)
			{
				acctTerritoriesMap.put(a, t.Name);
			}
			else
			{
				String s = acctTerritoriesMap.get(a) + '\n' + t.Name;
				acctTerritoriesMap.put(a, s);
			}
		}
		
		
	}  
	
	for(Contact_Affiliation__c af : trigger.new)
	   af.Territories__c =  acctTerritoriesMap.get(af.AffiliatedTo__c);
}