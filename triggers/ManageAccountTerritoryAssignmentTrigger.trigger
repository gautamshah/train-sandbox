/****************************************************************************************
 * Name		:	ManageAccountTerritoryAssignmentTrigger 
 * Author	:	John Hogan
 * Date		:	9/26/2013 
 * Purpose	:	Create or delete Account Shares when Account Territory Assignment records are
 				inserted by DI processes.
 * Dependencies: Account_Territory__Assignment__c object, AccountShare object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        			AUTHOR               	CHANGE
 * ----        			------               	------
 * Dec 12, 2017		   	Gautam Shah				Line 18: wraps all logic so that it only executes if running a test or invoked by the Sys Admin or API Data Loader profile. 
 ******************************************************************************************/
trigger ManageAccountTerritoryAssignmentTrigger on Account_Territory_Assignment__c (before insert) 
{
    if(Test.isRunningTest() || Utilities.isSysAdminORAPIUser)
    {
        //exclude records from certain sources
        Set<String> ignores = new Set<String>();
        IC_Config_Setting__c cs = IC_Config_Setting__c.getValues('ATA Trigger Exclude Sources');
        if (cs != null && cs.Value__c != null) 
        {
            String[] a = cs.Value__c.split('\\|');
            for (String s : a) 
            {
                if (!String.isEmpty(s))
                {
                    ignores.add(s);
                }
            }
        }
        System.Debug('*** ignores sources: ' + ignores);
        
        //*******************************************************************************************************************
        // Build map of TerritoryId to GroupId be used when inserting or deleting AccountShare records
        Set <Id> setTerritoryId = new Set <Id>();
        Set <Id> setAccountId = new Set <Id>();
        
        List <Account_Territory_Assignment__c> listInsertACT = new List <Account_Territory_Assignment__c>();
        List <Account_Territory_Assignment__c> listDeleteACT = new List <Account_Territory_Assignment__c>();
        List <AccountShare> listInsertAccountShare = new List <AccountShare>();
        List <AccountShare> listDeleteAccountShare = new List <AccountShare>();
        
        Map <String, AccountShare> mapAcctIdTerrId_AccountShareId = new Map<String, AccountShare>();
        Map <String, Account_Territory_Assignment__c> mapAcctIdTerrId_ACT = new Map <String, Account_Territory_Assignment__c>();
        Map <Id, Id> mapGroupId_TerritoryId = new Map<Id, Id>();
        Map <Id, Id> mapTerritoryId_GroupId = new Map<Id, Id>();
        
        //*******************************************************************************************************************
        //*******************************************************************************************************************
        for(Account_Territory_Assignment__c acctTerrAssign: trigger.new)
        {
            if(acctTerrAssign.AccountId__c!=null && acctTerrAssign.TerritoryId__c!= null && !ignores.contains(acctTerrAssign.Source__c)) 
            {
                
                mapAcctIdTerrId_ACT.put(acctTerrAssign.AccountId__c + acctTerrAssign.TerritoryId__c, acctTerrAssign);
                setTerritoryId.add(acctTerrAssign.TerritoryId__c);  //Populate Set of Territory Ids used for SOQL
                setAccountId.add(acctTerrAssign.AccountId__c);  //Populate Set of AccountIds used for SOQL
                
                if (acctTerrAssign.Action__c.toUpperCase() =='I')
                {
                    listInsertACT.add(acctTerrAssign);  //Populate list if Account_Territory_Assignment__c records for which to Insert AccountShare
                }
                else if (acctTerrAssign.Action__c.toUpperCase() =='D')
                {
                    listDeleteACT.add(acctTerrAssign);  // Populate list if Account_Territory_Assignment__c records for which to Insert AccountShare
                }
            }// end if
        } //end for
        //*******************************************************************************************************************
        
        //*******************************************************************************************************************
        for(Group grp : [select Id, RelatedId from Group where Type='Territory' and RelatedId in :setTerritoryId])
        {
            mapTerritoryId_GroupId.put(grp.RelatedId, grp.Id);  //Populate TerritoryId --> GroupId Map
            mapGroupId_TerritoryId.put(grp.Id, grp.RelatedId);  //Populate GroupId --> TerritoryId Map
        }
        //*******************************************************************************************************************
        
        //*******************************************************************************************************************
        //AccountIdTerritoryId --> AccountShare Map
        for(AccountShare acctShare : [select Id, AccountId, UserOrGroupId from AccountShare 
                                        where RowCause = 'TerritoryManual' and AccountId in :setAccountId])
        {
            // Populate Map with (AccountShare.AccountId+AccountShare.TerritoryId) --> AccountShare object
            mapAcctIdTerrId_AccountShareId.put(acctShare.AccountId + (string) mapGroupId_TerritoryId.get(acctShare.UserOrGroupId),acctShare);
        }
        //*******************************************************************************************************************
        
        //Insert*************************************************************************************************************
        // For each Account Territory Assignment record with the Insert Action,
        // create a new corresponding AccountShare record, and add to a list
        for (Account_Territory_Assignment__c iACT : listInsertACT)
        {  
            AccountShare accountShare = new AccountShare(); 
            accountShare.AccountId = iACT.AccountId__c;
            System.debug('**** accountShare.AccountId='+accountShare.AccountId);
            System.debug('**** mapTerritoryId_GroupId.get(iACT.TerritoryId__c)='+mapTerritoryId_GroupId.get(iACT.TerritoryId__c));
            accountShare.UserOrGroupId = mapTerritoryId_GroupId.get(iACT.TerritoryId__c);
            System.debug('**** accountShare.UserOrGroupId='+accountShare.UserOrGroupId);
            listInsertAccountShare.add(accountShare);
            
        }
        
        if (listInsertAccountShare.size()>0)
        {   
            Set <Id> setAccountShareIdInsert = new Set <Id>();
            // Insert list of AccountShare records
            Database.SaveResult[] srs = Database.insert(listInsertAccountShare); 
            // Build a Set of the Ids of the AccountShare records inserted
            for(Integer i=0; i<srs.size(); i++)
            {
                Database.SaveResult saveResult = srs[i];
                setAccountShareIdInsert.add(saveResult.getId());
            }
            // Retreive AccountShares that were inserted and mark the Account_Territory_Assignment__c records as processed
            String iKey;
            for(AccountShare acctShareInserted:[select Id, AccountId, UserOrGroupId from AccountShare
                                         where Id IN:setAccountShareIdInsert AND isDeleted=false])
            {
                iKey = acctShareInserted.AccountId + (string) mapGroupId_TerritoryId.get(acctShareInserted.UserOrGroupId);
                Account_Territory_Assignment__c actProcessed = mapAcctIdTerrId_ACT.get(iKey);
                actProcessed.Processed__c = true;
                iKey = '';
            }
        } // end if (listInsertAccountShare.size()>0); end of inserts
        //*******************************************************************************************************************
        
        //Delete*************************************************************************************************************
        // For each Account Territory Assignment record with the Delete Action, create a new corresponding AccountShare record, 
        // and add to a list.
        Set <Id> setAccountShareIdToDelete = new Set <Id>(); 
        String dACTKey;
        for (Account_Territory_Assignment__c dACT : listDeleteACT)
        {
            dACTKey = dACT.AccountId__c + dACT.TerritoryId__c;
            AccountShare acctShareDel = mapAcctIdTerrId_AccountShareId.get(dACTKey);
            if (acctShareDel != null)
            {
                listDeleteAccountShare.add(acctShareDel);
            }
            dACTKey = '';
        }
        
        if (listDeleteAccountShare.size()>0)
        {
            Set <Id> setAccountShareIdDelete = new Set <Id>();
            // Delete list of AccountShare records
            Database.DeleteResult[] drs = Database.delete(listDeleteAccountShare);
            // Build a Set of the Ids of the AccountShare records inserted
            for(Integer d=0; d<drs.size(); d++)
            {
                Database.DeleteResult deleteResult = drs[d];
                setAccountShareIdDelete.add(deleteResult.getId());
            }
            
            // Retreive AccountShares that were deleted and mark the Account_Territory_Assignment__c records as processed
            String dKey;
            for (AccountShare asd: listDeleteAccountShare)
            {
                if (setAccountShareIdDelete.contains(asd.id))
                {
                    dKey = asd.AccountId + (string) mapGroupId_TerritoryId.get(asd.UserOrGroupId);
                    Account_Territory_Assignment__c actDeleted = mapAcctIdTerrId_ACT.get(dKey);
                    actDeleted.Processed__c = true;
                    dKey = '';
                }
            }
        }
    }
} // End Trigger