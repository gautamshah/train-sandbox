trigger ContactCreateDQCase on Contact (after insert) {

    /****************************************************************************************
    * Name    : Contact Create New DQ Case
    * Author  : Mike Melcher
    * Date    : 03-16-2012
    * Purpose : Create Data Quality Case for new Contacts created by users
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE          AUTHOR               CHANGE
    * ----          ------               ------
    * 4/23-2012     MJM                 Made trigger inactive -- logic rolled into ContactValidation trigger
    * 5-03-2012     MJM                 Re-activated trigger -- must be done as an after insert to properly build the reference link on the case that's created.
    * 5-15-2012     MJM                 Update to only create DQ Case when Master Contact field is null
    * 5-16-2012     MJM                 Update to work with In Process recordtype
    * 5-16-2012     MJM                 Removed logic to determine Case Owner -- that is determined in the createCase method
    * 3-08-2013     JEJH                Added logic to determine if the Contact is being created by a Private Covidien user
    * 3/30/2014     Gautam Shah         Commented out SOQL query to Profile object and replaced with call to Utilities.profileMap (Line 47)
    * 12/10/2014    Ankit Singhal       Added Contact Country as parameter while invoking CreateCase method in dataQuality Class at line 64
    * 18 December 2015 Amogh Ghodke     Added line 72 for Distributor Contact for ASIA - PH 
    *****************************************************************************************/

// Verify contact created by user and not the Integration processes
/*
Id dataloaderProfile;
Id SystemAdminProfile;
Id privateUserProfile;

Id currentUser = UserInfo.getUserId();

for (Profile p : [select Id, Name from Profile where Name = 'API Data Loader' or Name='System Administrator' or Name='Private Covidien User']) {
   if (p.Name == 'API Data Loader') {
      dataloaderProfile = p.Id;
   }
   
   if (p.Name == 'Private Covidien User') {
      privateUserProfile = p.Id;
   }
 //  if (p.Name == 'System Administrator') {
 //     SystemAdminProfile = p.Id;
 //  }
}
*/
if (Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && Utilities.profileMap.get(UserInfo.getProfileId()) != 'Private Covidien User'
 && Utilities.profileMap.get(UserInfo.getProfileId()) != 'CRM SSC Admin and Data Support'){

   List<Contact> ContactsForCases = new List<Contact>();

   // Get the RecordtypeIds we will need

   DataQuality dq = new DataQuality();
        
   Map<String,Id> rtmap = dq.getRecordTypes(); 
   
   //id queue = [select QueueId from QueueSObject where Queue.Name = 'Data Quality'].QueueId;
   Map<Id,Contact> MAllNewContacts = new Map<Id,Contact>([Select Id,Account.BIllingCountry from Contact where Id in :(Trigger.newMap.keyset())]);
   for (Contact c : Trigger.new) {
      System.debug('=====>'+c.RecordTypeId+'----->'+c.Master_Contact_Record__c+'*******'+c.Country__c+'&&&&&&'+MAllNewContacts.get(c.Id).Account.BillingCountry);
      if (c.RecordtypeId == rtMap.get('In Process Connected Contact')) {
         if (c.Master_Contact_Record__c == null) {
            //dq.createEMSCase('DQ-Validate New Master Contact Request',c.Id,'Data Quality','Medium', UserInfo.getUserId(),'Validation','Assigned',MAllNewContacts.get(c.Id).Account.BillingCountry);
            dq.createEMSCase('DQ-Validate New Master Contact Request', c.Id,'Validation', 'Medium', UserInfo.getUserId(), 'Data Quality', 'Assigned', MAllNewContacts.get(c.Id).Account.BillingCountry);
         
         }
         
      }
      if (c.RecordtypeId == rtMap.get('In Process Distributor Contact')) {
                     
            dq.createEMSCase('DQ-Validate New Distributor Contact Request', c.Id,'Validation', 'Medium', UserInfo.getUserId(), 'Data Quality', 'Assigned', MAllNewContacts.get(c.Id).Account.BillingCountry);
         
            
      }
   }
   
  
} 

}