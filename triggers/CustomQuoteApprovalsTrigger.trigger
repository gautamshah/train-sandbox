/**
 *  Apttus Approvals Management
 *  CustomQuoteApprovalsTrigger
 *   - performs approval required check
 *
 *  2014 Apttus Inc. All rights reserved.
 */
trigger CustomQuoteApprovalsTrigger on Apttus_Proposal__Proposal__c (before update) {
    // preview status pending
    private String PREVIEW_STATUS_PENDING = 'Pending';
    
    // status none
    private String STATUS_NONE = 'Not Submitted';
    
    // status approved
    private String STATUS_APPROVED = 'Approved';
    
    // status pending approval
    private String STATUS_PENDING_APPROVAL = 'Pending Approval';
    
    // status pending approval
    private String STATUS_APPROVED_APPROVAL = 'Approved';
     
    
    // null string
    private String VALUE_NULL = 'null';
    
    // skip checking under following conditions
    //    - trigger size is greater than 1
    //    - current approval status is pending approval
    //        , meaning it is the initial submission action approval status update OR process in progress
    //    - old approval status is pending approval and new approval status is other that pending approval
    //        meaning, it is process completion approval status update
    //    - record type is not a Surgical type using cart approvals
    if ((Trigger.new.size() > 1) 
            || (Trigger.new[0].Apttus_QPApprov__Approval_Status__c == STATUS_PENDING_APPROVAL)
            || (Trigger.new[0].Apttus_QPApprov__Approval_Status__c == STATUS_APPROVED_APPROVAL)            
            || ((Trigger.old[0].Apttus_QPApprov__Approval_Status__c == STATUS_PENDING_APPROVAL)
             
                && (Trigger.new[0].Apttus_QPApprov__Approval_Status__c != Trigger.old[0].Apttus_QPApprov__Approval_Status__c))){
        return;
    }

    RecordType rt = CPQ_ProposalProcesses.getProposalRecordTypesByIdMap().get(Trigger.new[0].RecordTypeId);
    if (rt.DeveloperName != 'Agreement_Proposal' &&
        rt.DeveloperName != 'Energy_Hardware' &&
        rt.DeveloperName != 'Hardware_or_Product_Quote' &&
        rt.DeveloperName != 'New_Product_LOC' &&
        rt.DeveloperName != 'Promotional_LOC' &&
        rt.DeveloperName != 'Rentals' &&
        rt.DeveloperName != 'Royalty_Free_Agreement') {

        return;
    }

    if (Trigger.isBefore && Trigger.isUpdate) {
        doCheckIfApprovalRequired(Trigger.new[0]);
        
    }
    
    /**
     * Performs approval required check for header and child objects
     */
    private void doCheckIfApprovalRequired(Apttus_Proposal__Proposal__c quote) {
        String doubleUnderscore = '__';
        
        // header param
        String headerIdStatus = null;
        Map<ID, String> headerStatusById = new Map<ID, String>();
        headerStatusById.put(quote.Id, quote.Apttus_QPApprov__Approval_Status__c);
        headerIdStatus = quote.Id + doubleUnderscore + quote.Apttus_QPApprov__Approval_Status__c;
        
        // child objects param
        List<String> childIdStatusList = new List<String>();
        Map<ID, String> childObjectsStatusById = new Map<ID, String>();
        // modified child object ids
        List<ID> modifiedChildObjectIds = new List<ID>();
        
        // get all opportunity products 
        // NOTE - INCLUDE ADDITIONAL 'where' CLAUSE, AS REQUIRED, TO IDENTIFY MODIFIED LINES
        for (Apttus_Proposal__Proposal_Line_Item__c lineItem : [select Id, Apttus_QPApprov__Approval_Status__c from Apttus_Proposal__Proposal_Line_Item__c
                                                    where Apttus_Proposal__Proposal__c = :quote.Id
                                                    and Apttus_QPApprov__Approval_Status__c != :STATUS_APPROVED]) {
                                                        
            childObjectsStatusById.put(lineItem.Id, lineItem.Apttus_QPApprov__Approval_Status__c);
            childIdStatusList.add(lineItem.Id + doubleUnderscore + lineItem.Apttus_QPApprov__Approval_Status__c);
            
            modifiedChildObjectIds.add(lineItem.Id);                                            
            
        }
        
        // cache uncommited version of the context object for the rule evaluation routine to use
        Apttus_Approval.ApprovalsWebService.addSObjectToCache(quote);
        
        // perform the check
System.debug('headerIdStatus = ' + headerIdStatus + ', childIdStatusList = ' + childIdStatusList + ', modifiedChildObjectIds = ' + modifiedChildObjectIds);
        List<String> resultList = Apttus_Approval.ApprovalsWebService.CheckIfApprovalRequired2(headerIdStatus
                                                                , childIdStatusList, modifiedChildObjectIds);
        
        System.debug('resultList >>>>>' + resultList);
        
        // construct map by spliting the string by '__'
        Map<ID, String> resultMap = new Map<ID, String>();   
        for (String resultStr : resultList) {
            List<String> resultSplitList = resultStr.split(doubleUnderscore);
            String statusValue = resultSplitList[1];
            if (statusValue == VALUE_NULL) {
                statusValue = STATUS_NONE;
            }
            resultMap.put(resultSplitList[0], statusValue);
        }
        
        // lines
        List<Apttus_Proposal__Proposal_Line_Item__c> childUpdateList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        
        for (ID objId : resultMap.keySet()) {
            if (objId == quote.Id) {
                if (resultMap.get(objId) != quote.Apttus_QPApprov__Approval_Status__c) {
                    quote.Apttus_QPApprov__Approval_Status__c = resultMap.get(objId);
                }
        // always reset approval preview status to Pending
        quote.Approval_Preview_Status__c = PREVIEW_STATUS_PENDING;
            } else {
                
                if (resultMap.get(objId) != childObjectsStatusById.get(objId)) {
                    Apttus_Proposal__Proposal_Line_Item__c child = new Apttus_Proposal__Proposal_Line_Item__c(Id = objId
                                                        , Apttus_QPApprov__Approval_Status__c = resultMap.get(objId));
                    childUpdateList.add(child);                                 
                }
            }
        }   
        
        // save changes 
        if (!childUpdateList.isEmpty()) {
            update childUpdateList;
        }                                                   
    }                                           
}