trigger OpportunityOwnerRegionTrigger on Opportunity (Before Insert, Before Update)
{
/********************************************************************************************************************
DEPRECATED!
The functionality in this trigger has been moved to Opportunity_Main.cls method: OwnerRegionAssignment
By: Gautam Shah
When: 27 - June - 2014
*********************************************************************************************************************    
    if(!Utilities.isSysAdminORAPIUser)
    {
        //Map<string, id> ownerMap=new Map<String, id>(); //This is not being used?
        //Set<id> oppIds=new Set<id>();
        //*******************************************
        Set<id> oppOwnerIds = new Set<id>();
        Map<Id, User> userMap = new Map<Id, User>();
        //*******************************************
        
        //*******************************************
        for (Opportunity opportunity: Trigger.new)
        {
            oppOwnerIds.add(opportunity.Ownerid);
        }
        //*******************************************
        
        //*******************************************
        for(User user : [SELECT Id, Function__c, Region__c, User_Role__c FROM User
            WHERE UserType = 'Standard' AND Id IN :oppOwnerIds]){      
            userMap.put(user.Id, user);
        }
        //*******************************************
        
        //*******************************************
        for (Opportunity opp: Trigger.new)
        {
            if(!userMap.IsEmpty() && userMap.size()>0){
                opp.Owner_Region__c=userMap.get(opp.OwnerId).Region__c;
                opp.Region__c=userMap.get(opp.OwnerId).Region__c;
                opp.User_Role__c=userMap.get(opp.OwnerId).User_Role__c; 
                opp.Function__c=userMap.get(opp.OwnerId).Function__c;
            }
        }
    //*******************************************
    }
*/
}