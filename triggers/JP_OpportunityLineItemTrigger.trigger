trigger JP_OpportunityLineItemTrigger on OpportunityLineItem (before insert, after insert) {
     /****************************************************************************************
     * Name    : JP_OpportunityLineItemTrigger 
     * Author  : Hiroko Kambayashi
     * Date    : 09/18/2012 
     * Purpose : To insert OpportunityLineItemSchedule record after inserting OpportunityLineItem 
     *           by Opportunity
     * Dependencies: OpportunityLineItem Object
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 
     *
     *****************************************************************************************/
	
	//Create TriggerHandler instance
	JP_OpportunityLineItemTriggerhandler handler = new JP_OpportunityLineItemTriggerhandler(Trigger.isExecuting, Trigger.size);
	
	if(Trigger.isInsert && Trigger.isAfter){
		handler.onAfterInsert(Trigger.newMap);
	} else {
		handler.onBeforeInsert(Trigger.new);
	}
}