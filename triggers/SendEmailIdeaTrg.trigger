trigger SendEmailIdeaTrg on Idea (after insert, after update) {
    List<String> lstEmails = new List<String>();
    if(trigger.isInsert || trigger.isUpdate){
        User u = [Select id,AccountId from User where Id=:UserInfo.getUserId()];
        List<Convidien_Contact__c> lstContacts = [Select id,Email__c from Convidien_Contact__c where type__c='Customer Service Agent' and Distributor_Name__c =:u.AccountId];
        Idea newIdea = trigger.new[0];
        if(lstContacts.size()>0){
            for(Convidien_Contact__c cc : lstContacts){
                if(cc.email__c != null)
                lstEmails.add(cc.email__c);
            }
        } 
        System.debug('>>>>>>lstEmails>>>>>'+lstEmails.size());
            
            /*
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String subject ='New Idea !!';
            email.setSubject(subject);
            if(lstEmails.size()>0){
            email.setToAddresses( lstEmails);
            }
            else
            email.setToAddresses( new List<String>{'asia.PartnerConnect@covidien.com'});
            if(trigger.isInsert){
                email.setEmailTemplate
                email.setHTMLBody(' A new Idea has been submitted by an Asia Partner Community User .<a href="/'+newIdea.Id+'">Click Here</a>.');
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});    
            }
            if(trigger.isUpdate && newIdea.Comments != trigger.old[0].comments){
                email.setPlainTextBody('An Idea has been modified.<a href="/'+newIdea.Id+'">Click Here</a>');
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            */
        System.debug('Host: ' + URL.getSalesforceBaseUrl().getHost());   
        System.debug('Protocol: ' + URL.getSalesforceBaseUrl().getProtocol());
        System.debug('>>>>>>>>>>>>>>'+System.URL.getSalesforceBaseUrl().getHost().remove('-api' ));
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(lstEmails.size()>0){
                mail.setToAddresses( lstEmails);
            }
            else
            mail.setToAddresses( new List<String>{System.Label.NoCustomerServiceAgent });
            mail.setSubject('A new Idea has been submitted by an Asia Partner Community User!!');
            /*string recLink = System.Label.Sandbox_url+'/ideas/viewIdea.apexp?id='+newIdea.Id;*/
            string recLink = URL.getSalesforceBaseUrl().toExternalForm() +'/distributor/ideas/viewIdea.apexp?id='+newIdea.Id;
            String body='A new Idea has been submitted by an Asia Partner Community User. Please <a href="'+recLink+'">Click Here</a> to review the Idea and make <font color="red">sure</font> the content is appropriate.<br/><br/>Idea Title:'+newIdea.Title+'<br/><br/><br/>Idea Body:<br/><br/>'+(newIdea.Body==null?'':newIdea.body)+'<br/><br/>NOTE:<br/><br/>If the Idea is valid, please forward this message to the in-country Commercial team or PartnerConnect contacts and ask them to consider the idea for execution.  If the Idea is not easily understood or unclear, you can ask the Idea creator to add a comment or explain their suggestion more clearly in the comments.';
            body = body+'<br/><br/>If the Idea is the same as one that has already been posted by another user, please reply to the creator via Chatter or email and invite them to vote up the original idea instead of creating another one. Ideas with a lot of votes are more likely to be considered for implementation. Please include a link to the original idea in your message to them.';
            body = body+'<br/><br/>If the Idea contains a complaint or other type of message that is not appropriate for the Ideas forum, please delete the Idea and send the creator a message informing them that their complaint or comment has been removed from Ideas, but will be followed up on offline.  Please make sure the complaint is forwarded to the appropriate person who can address the complaint.';
            
            
            mail.setHTMLBody(body);
           
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }
    
    
}