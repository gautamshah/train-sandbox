trigger TaskContactValidation on Task (before insert, before update) {
 /****************************************************************************************
    * Name    : Task Contact Validation    
    * Author  : Mike Melcher    
    * Date    : 1-6-2012    
    * Purpose : Ensure Events that are associated with Contacts only relate to 
    *           Affiliated Clinician and Affiliated Non Clinician Contacts    
    *     
    * Dependancies:     
    *                  
    *          
    * ========================    
    * = MODIFICATION HISTORY =    
    * ========================    
    * DATE        	AUTHOR              CHANGE    
    * ----        	------              ------    
    *  1/24/2012  	Pete Reed      		Added Affiliated Non Clinician Contact to cMap.get(t.WhoId)  
    *  6/11/2014	Gautam Shah 		Eliminated the Profile query in favor of using the Utilities class member 
    *    *****************************************************************************************/
    
    //Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
	//if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
	if(!Utilities.isSysAdminORAPIUser)
	{
		Set<Id> ContactsInTrigger = new Set<Id>();
		Map<Id,Id> cMap = new Map<Id,Id>();
		
	    for (Task t : Trigger.new) {
	       string w = t.WhoId;
	       if (w != null && w.startsWith('003')) {
	          ContactsInTrigger.add(t.WhoId);
	       }
	    }
	    
	    if (ContactsInTrigger.size() > 0) {
	       DataQuality dq = new DataQuality();
	       Map<String,Id> rtMap = dq.getRecordTypes();
	       for (Contact c : [select Id, RecordtypeId from Contact where Id in :ContactsInTrigger]) {
	          cMap.put(c.Id, c.RecordTypeId);
	       }
	       
	       for (Task t : Trigger.new) {
	          if (cMap.get(t.WhoId) == rtMap.get('Master Clinician') ||         
	              cMap.get(t.WhoId) == rtMap.get('Master Non Clinician')) {
	              t.addError('Tasks Cannot Be Associated With Master Contacts');
	           }
	       }
	    }
	}
}