/****************************************************************************************
    * Name    : contactAffDeletion 
    * Author  : 
    * Date    : 09/23/2014
    * Purpose : Delete the records from the AffilateContacted related list --PBP-176(JIIRA)
    *
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR                CHANGE
    * ----        ------                ------
    * 3/5/2015   Netta Grant          exclude the API Data Loader profile from the logic          
   
    *****************************************************************************************/


trigger contactAffDeletion on Account_Affiliation__c (after delete) {

Account_TriggerHandler handler = new Account_TriggerHandler(Trigger.isExecuting, Trigger.size);
if(Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader') {
 if(Trigger.isdelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.old);
        }
      }
}