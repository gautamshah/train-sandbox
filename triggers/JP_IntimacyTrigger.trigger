trigger JP_IntimacyTrigger on JP_Intimacy__c (after insert, after update, after delete) {
     /****************************************************************************************
     * Name    : JP_IntimacyTrigger 
     * Author  : Hiroko Kambayashi
     * Date    : 10/17/2012 
     * Purpose : To input Intimacy information in Opportunity record 
     *           when Intimacy record is inserted, updated, or deleted.
     * Dependencies: JP_Intimacy__c Object
     *             
     * ========================
     * = MODIFICATION HISTORY =
     * ========================
     * DATE        AUTHOR               CHANGE
     * ----        ------               ------
     * 
     *
     *****************************************************************************************/
    //Create TriggerHandler instance
    JP_IntimacyTriggerHandler handler = new JP_IntimacyTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            handler.onAfterInsert(Trigger.new);
        } else if (Trigger.isUpdate){
            handler.onAfterUpdate(Trigger.oldMap, Trigger.new);
        } else {
            handler.onAfterDelete(Trigger.old);
        }
    }
}