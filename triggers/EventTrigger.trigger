trigger EventTrigger on Event (before insert, after insert, after update) {
    EventTriggerHandler handler = new EventTriggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
          handler.onBeforeInsert(Trigger.new);
        } else {
          handler.onBeforeUpdate(Trigger.oldMap, Trigger.new);
        }
    } 
    else {
        if(Trigger.isInsert){
            handler.onAfterInsert(Trigger.new);
        } else {
            handler.onAfterUpdate(Trigger.oldMap, Trigger.new);
        }
    }

}