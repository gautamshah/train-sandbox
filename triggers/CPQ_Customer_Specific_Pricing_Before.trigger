/**
trigger on CPQ_Customer_Specific_Pricing__c.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2014-01-29      Yuli Fintescu		Created. Link Commitment_Type__c and Partner_Root_Contract__c object 
										when CommTypeDesc_INT__c and Root_Contract__c field is populated
===============================================================================
*/
trigger CPQ_Customer_Specific_Pricing_Before on CPQ_Customer_Specific_Pricing__c (before insert, before update) {
	//Link Commitment_Type__c and Partner_Root_Contract__c object 
	//when CommTypeDesc_INT__c and Root_Contract__c field is populated
	CPQ_PriceListProcesses.Lookup(Trigger.new, Trigger.oldMap);
}