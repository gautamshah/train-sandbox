/****************************************************************************************
* Name    : Trigger: IDNSC_Task
* Author  : Gautam Shah
* Date    : 8/12/2014
* Purpose : Single entry point for all IDNSC_Task__c triggers
* 
* Dependancies: 
*   Class: IDNSC_Task_TriggerDispatcher           
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/
trigger IDNSC_Task on IDNSC_Task__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
	IDNSC_Task_TriggerDispatcher.Main(trigger.new, trigger.newMap, trigger.old, trigger.oldMap, trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isExecuting);
}