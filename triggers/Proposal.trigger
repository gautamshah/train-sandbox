/****************************************************************************************
* Name    : Trigger: Proposal
* Author  : Paul Berglund
* Date    : 12/08/2014
* Purpose : Single entry point for all Proposal triggers
* 
* Dependancies: 
*   Class: ProposalTriggerDispatcher           
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/ 
trigger Proposal on Apttus_Proposal__Proposal__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
}