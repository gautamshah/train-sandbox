/**
Trigger on the Apttus__Agreement_Document__c object

CHANGE HISTORY
===============================================================================
DATE            NAME                DESC
05/24/2016      Bryan Fry           Created
===============================================================================
*/
trigger AgreementDocument on Apttus__Agreement_Document__c (before insert, before update, after insert, after update) {
    Apttus_AgreementDocument_Trigger_Manager util = 
                new Apttus_AgreementDocument_Trigger_Manager(Trigger.isInsert, 
                                            				 Trigger.isUpdate, 
                                            				 Trigger.isBefore, 
				                                             Trigger.isAfter, 
				                                             Trigger.New, 
				                                             Trigger.Old, 
				                                             Trigger.OldMap, 
				                                             Trigger.NewMap);
    
    util.execute();
}