trigger NationOPGMapValidationTrigger on Nation_OPG_Map__c (before insert, before update) {
	Map <String, Nation_OPG_Map__c> mapNationOPG_NOM = new Map <String, Nation_OPG_Map__c>();
	String keyNationOPG;
		
	for (Nation_OPG_Map__c  n : [Select Id, Nation__c, Opportunity_Product_Group__c from Nation_OPG_Map__c])
		mapNationOPG_NOM.put(n.Nation__c.toUpperCase().trim() + n.Opportunity_Product_Group__c.toUpperCase().trim(), n);
		
	for (Nation_OPG_Map__c nom : trigger.new){
		keyNationOPG = nom.Nation__c.toUpperCase().trim() + nom.Opportunity_Product_Group__c.toUpperCase().trim(); 
		if( mapNationOPG_NOM.containsKey(keyNationOPG) && mapNationOPG_NOM.get(keyNationOPG).id != nom.id){
				nom.addError('Duplicate Nation and Opportunity Product Group pair.');
		}
	} 
} // end trigger