//When an existing case record type equals CRM Support
//AND the picklist values for the field "Type" equals to "Problem" OR "Service Request" OR "Question" 
//AND There is already an entitlement populated in the field
//AND 
//  If the picklist value fo the custom field "Admin_Priority__c" equals "Low"
//  Then the value of Entitlement  is set to "Case SLA Low"
//  OR
//  If the picklist value fo the custom field "Admin_Priority__c" equals "Medium"
//  Then the value of Entitlement  is set to "Case SLA Medium"
//  OR
//  If the picklist value fo the custom field "Admin_Priority__c" equals "High"
//  Then the value of Entitlement  is set to "Case SLA High"
//  OR
//  If the picklist value fo the custom field "Admin_Priority__c" equals "Critical"
//  Then the value of Entitlement  is set to "Case SLA Critical"

/* DECOMMISSIONED!!!  THIS FUNCTIONALITY HAS MOVED TO CASE_MAIN.DefaultEntitlement3 */
 trigger DefaultEntitlement3 on Case (before insert, before update)
 { 
/* 
   list<Entitlement> e1 = [select id from Entitlement where name=:'Case SLA Medium' limit 1];
   list<Entitlement> e2 = [select id from Entitlement where name=:'Case SLA Low' limit 1];
   list<Entitlement> e3 = [select id from Entitlement where name=:'Case SLA High' limit 1];
   list<Entitlement> e4 = [select id from Entitlement where name=:'Case SLA Critical' limit 1];
   
     for(Case c: Trigger.new)
   {//for default Entitlement  value
    
    If(c.RecordTypeID =='012U0000000LrOB' && (c.Type=='Problem' || c.Type=='Service Request' || c.Type=='Question') && c.Entitlement==null)
    {
    if(e1 != null)
    {
    c.EntitlementId  = e1[0].id;
    }
     }
     
     
   If(c.RecordTypeID =='012U0000000LrOB' && (c.Type=='Problem' || c.Type=='Service Request' || c.Type=='Question'))
    //if(true) To change Admin_Priority__c as per Entitlement Name
    {
    if(c.Admin_Priority__c == 'Low')
   {
    
    if(e2 != null)
    {
    c.EntitlementId  = e2[0].id;
    }
    system.debug('c.EntitlementId' + '>>>>>>>>>>>>>>>>'+c.EntitlementId);
    }

    if(c.Admin_Priority__c == 'Medium')
    {
     
         if(e1 != null)
    {
     c.EntitlementId  = e1[0].id;
     }
    }

    if(c.Admin_Priority__c == 'High')
      {
     
     if(e3 != null)
     {
     c.EntitlementId  = e3[0].id;
     }
     }

    if(c.Admin_Priority__c == 'Critical')
      {  
      
      if(e4 != null)
      {
      c.EntitlementId  = e4[0].id;
      }
      }
      }
       //To assign case to COE support
      /*   
      //   {
      
      //group o=new group(); //creating group object
      //o= [SELECT Id, Name FROM Group WHERE Name = 'CoE - Support'];  
      //system.debug('>>>>>>>>>>>>>>'+c);    
      //system.debug('>>>>>>>>>>>>>>'+caserecordtype.name);
     // system.debug('>>>>>>>>>>>>>>'+c.ownerid);
     // system.debug('>>>>>>>>>>>>>>'+o.id);
      
     // if ( (c.RecordTypeID =='012U0000000LrOB') &&(c.Type=='Problem' || c.Type=='Service Request' || c.Type=='Question'))  
     //   {
     // c.Case_Owner_EQUALS_COE_Support__c=true;
     //   }
           
    //     }
         
}
*/
}