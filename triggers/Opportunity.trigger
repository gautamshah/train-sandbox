/****************************************************************************************
* Name    : Trigger: Opportunity
* Author  : Gautam Shah
* Date    : 4/5/2014
* Purpose : Single entry point for all Opportunity triggers
* 
* Dependancies: 
*   Class: OpportunityTriggerDispatcher           
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
20161109 PAB Added cpqOpportunity_t
* 
*****************************************************************************************/ 
trigger Opportunity on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
    OpportunityTriggerDispatcher.Main(
    	trigger.new,
    	trigger.newMap,
    	trigger.old,
    	trigger.oldMap,
    	trigger.isBefore,
    	trigger.isAfter,
    	trigger.isInsert,
    	trigger.isUpdate,
    	trigger.isDelete,
    	trigger.isExecuting
    );
    
    cpqOpportunity_t.main(
        trigger.isExecuting,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isBefore,
        trigger.isAfter,
        trigger.isUndelete,
        trigger.new,
        trigger.newMap,
        trigger.old,
        trigger.oldMap,
        trigger.size
	);
}