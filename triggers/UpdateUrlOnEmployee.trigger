trigger UpdateUrlOnEmployee on Attachment (after insert, after update, after delete) {
	Schema.Describesobjectresult obj = Employee__c.sObjectType.getDescribe();
	if(trigger.isInsert || trigger.isUpdate){
		
		Map<Id,Id> mpAttIds = new Map<Id,Id>();
		for(Attachment att : trigger.new){
			if(String.valueOf(att.ParentId).substring(0,3) == obj.getKeyPrefix()){
				mpAttIds.put(att.Id,att.ParentId);
			}
		}
		
		
		if(mpAttIds.size()>0){
			List<Employee__c> lstEmp = new List<Employee__c>();
			for(Id AttId : mpAttIds.keyset()){
				Employee__c emp = new Employee__c(Id=mpAttIds.get(attId));
				emp.Employee_Photo__c = '/servlet/servlet.FileDownload?file='+attId;
				lstEmp.add(emp);
			}
			if(lstEmp.size()>0)
				update lstEmp;
		}
	}
	if(trigger.isdelete){
		List<Employee__c> lstEmp = new List<Employee__c>();
		for(Attachment att : trigger.old){
			if(String.valueOf(att.ParentId).substring(0,3) == obj.getKeyPrefix()){
				Employee__c emp = new Employee__c(Id=att.parentId);
				emp.Employee_Photo__c = null;
				lstEmp.add(emp);
			}	
		}
		if(lstEmp.size()>0)
			update lstEmp;
	
	}
}