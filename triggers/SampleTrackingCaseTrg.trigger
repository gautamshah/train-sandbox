/****************************************************************************************
 * Name    : Sample Tracking Case Creation
 * Author  : Shawn Clark
 * Date    : 04/01/2015 
 * Purpose : Used by ANZ Sample Tracking Approval Process. On Final approval of Samples by
             Product Manager, a Case is created and related back to the Sample to track
             Sample Shipping details, and allow Reps to work with Customer Services via
             Chatter and Case comments. 
 * Dependencies: Sample Object 
 *****************************************************************************************/
trigger SampleTrackingCaseTrg on Samples__c (before update) {

    Set<Id> SampleIDs = new Set<Id>();
    RecordType SampleCase = [SELECT Id FROM RecordType where SobjectType = 'Case' and DeveloperName = 'Sample_Tracking' LIMIT 1];
    public string CaseOwnr = [Select Id from Group where Type = 'Queue' and DeveloperName = 'ANZ_Customer_Service' LIMIT 1].ID;  
    List<Case> Caselist = new List<Case>();
    
    
     for (Samples__c s: Trigger.new) {
     
        if (s.CreateCase__c == true &&
            s.Case__c == NULL &&
            s.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('ANZ_Sample'))    
            { 
            SampleIDs.add(s.id);
            Case c = new Case();
            c.RecordTypeID = SampleCase.id;
            c.Subject = s.Name;
            c.Priority = 'Medium';
            c.Order_Status__c = 'Order Submitted';
            c.Status = 'New';
            c.Description = s.Name;
            c.Notes__c = s.Notes__c;
            c.Deliver_To_Address__c = s.Deliver_To_Address__c;
            c.Deliver_To_Contact__c = s.Deliver_To_Contact__c;
            c.Date_Sample_Ordered__c = s.Date_Sample_Ordered__c;
            c.Sample_Date__c = s.Sample_Date__c;
            c.Sample_Name__c = s.id;
            c.Opportunity_Name__c = s.Opportunity__c;
            c.ContactID = s.Contact_Lookup__c;
            c.OwnerID = CaseOwnr;
            c.Requested_By_Rep__c = s.Requested_By_Sales_Rep__c;
            Caselist.add(c);
              }
            }  insert Caselist;  
    
   //map constructor only supports ID-->Sobject.
   //If you want a different key, then you have to build the map yourself.
 
   Map<String, Case> samps = new Map<String, Case>();
   for (Case c : [SELECT Sample_Name__c, ID FROM Case WHERE Sample_Name__c IN :SampleIDs]) {
   samps.put(c.Sample_Name__c, c);
   }
          

    for (Samples__c s: Trigger.new) {
        if (s.CreateCase__c == true &&
            s.Case__c == NULL &&
            s.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('ANZ_Sample'))    
    { 
    s.Case__c = samps.get(s.id).ID;
    }
  }
 }