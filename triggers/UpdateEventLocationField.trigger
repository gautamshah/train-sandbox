/****************************************************************************************
    * Name    : UpdateEventLocationField    
    * Author  : Tejas Kardile    
    * Date    : 13-1-2014
    * Purpose : Update Account & Contact name in Location field of Event 
    * Dependancies:     
    * Modified by: Yap Zhen-Xiong
    * Modified on: 27-05-2014
    * Modification Details: Added filter to only work for Korea owners
    *    *****************************************************************************************/

trigger UpdateEventLocationField on Event (before insert, before update){

    //Start of Addition by ZX 20140527
    List<Event> eventList = new List<Event>();
    Map<id, List<Event>> ownerEventMap = new Map<id, List<Event>>();
    //Filter KR owners only
    for (Event e : Trigger.new){
        List<Event> eList = ownerEventMap.get(e.OwnerId);
        
        if(eList == null)
            eList = new List<Event>();
        
        eList.add(e);
        ownerEventMap.put(e.OwnerId, eList);
    }
    
    //Finding Korea Owners
    for(User u : [SELECT id, Country, Function__c FROM User WHERE id in :ownerEventMap.keySet()]){
        if ( u.Country == 'KR' ){
            eventList.addAll(ownerEventMap.get(u.id));
        }
    }
    
    Event_TriggerHandler handler = new Event_TriggerHandler(Trigger.isExecuting, eventList.size());
    handler.OnbeforeInsertUpdate(eventList);
    //End of addition by ZX 20140527

    /*Commented off by ZX 20140527
    Event_TriggerHandler handler = new Event_TriggerHandler(Trigger.isExecuting, Trigger.size);
    handler.OnbeforeInsertUpdate(Trigger.new);
    */
}