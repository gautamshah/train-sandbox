trigger Account_Territory_Contact_Affiliation on Account_Territory__c (after insert, after update) 
{
    /****************************************************************************************
	* Name    : Account_Territory_Contact_Affiliation
	* Author  : Nathan Shinn
	* Date    : 11-07-2011
	* Purpose : Retrieces the related, Contact_Affiliation__c records usiing the 
	*           the AccountId__c field from the Account_Territory__c. Updates 
	*           the Contact_Affiliation__c records in order to execute the trigger
	*           on that object that updates the Territories field.                
	*
	* ========================
	* = MODIFICATION HISTORY =
	* ========================
	* DATE        AUTHOR               CHANGE
	* ----        ------               ------
	* 11-07-2011  Nathan Shinn         Created
	*
	*****************************************************************************************/
	
    set<Id> accountIds = new set<Id>();
	set<Id> contactIds = new set<Id>();
	list<Contact_Affiliation__c> contactAffiliation 
	   = new list<Contact_Affiliation__c>();
	
	for(Account_Territory__c at : trigger.new)
	   accountIds.add(at.AccountId__c);
	   
	for(Contact_Affiliation__c ca : [select Id 
	                                   from Contact_Affiliation__c 
	                                  where AffiliatedTo__c in :accountIds])
	{
		contactAffiliation.add(ca);
	}
	
	//Update the Contact_Affiliation__c records to trigger the territories update
	if(contactAffiliation.size() > 0)
	  update contactAffiliation;
}