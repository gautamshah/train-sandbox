trigger CPQ_SSG_Pricing_Tier_Price_List_Before on CPQ_SSG_Pricing_Tier_Price_List__c (before insert, before update) {
	// Get Pricing Tier and Price List Ids based on codes/names entered
	Map<String,List<CPQ_SSG_Pricing_Tier_Price_List__c>> rowsToProcessTier = new Map<String,List<CPQ_SSG_Pricing_Tier_Price_List__c>>();
	Map<String,List<CPQ_SSG_Pricing_Tier_Price_List__c>> rowsToProcessList = new Map<String,List<CPQ_SSG_Pricing_Tier_Price_List__c>>();
	for (CPQ_SSG_Pricing_Tier_Price_List__c junction: Trigger.new) {
		if (junction.Pricing_Tier_Name__c != null) {
			if (!rowsToProcessTier.containsKey(junction.Pricing_Tier_Name__c)) {
				rowsToProcessTier.put(junction.Pricing_Tier_Name__c, new List<CPQ_SSG_Pricing_Tier_Price_List__c>());
			}
			rowsToProcessTier.get(junction.Pricing_Tier_Name__c).add(junction);
		}
		if (junction.Price_List_Code__c != null) {
			if (!rowsToProcessList.containsKey(junction.Price_List_Code__c)) {
				rowsToProcessList.put(junction.Price_List_Code__c, new List<CPQ_SSG_Pricing_Tier_Price_List__c>());
			}
			rowsToProcessList.get(junction.Price_List_Code__c).add(junction);
		}
	}

	List<CPQ_SSG_Pricing_Tier__c> tiers = [Select Id, Name From CPQ_SSG_Pricing_Tier__c Where Name in :rowsToProcessTier.keySet()];
	for (CPQ_SSG_Pricing_Tier__c tier: tiers) {
		List<CPQ_SSG_Pricing_Tier_Price_List__c> junctions = rowsToProcessTier.get(tier.Name);
		if (junctions != null) {
			for (CPQ_SSG_Pricing_Tier_Price_List__c junction: junctions) {
				junction.CPQ_SSG_Pricing_Tier__c = tier.Id;
			}
		}
	}

	List<CPQ_SSG_Price_List__c> priceLists = [Select Id, Price_List_Code__c From CPQ_SSG_Price_List__c Where Price_List_Code__c in :rowsToProcessList.keySet()];
	for (CPQ_SSG_Price_List__c priceList: priceLists) {
		List<CPQ_SSG_Pricing_Tier_Price_List__c> junctions = rowsToProcessList.get(priceList.Price_List_Code__c);
		if (junctions != null) {
			for (CPQ_SSG_Pricing_Tier_Price_List__c junction: junctions) {
				junction.CPQ_SSG_Price_List__c = priceList.Id;
			}
		}
	}
}