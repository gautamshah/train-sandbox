trigger PreventFieldVisitClosure on Field_Visit__c (before update) 
{
    //create a map of open tasks related to the Field_Visit__c
   // Map<Id, Task> taskMap = new Map<Id, Task>();
    Set<Id> newlyClosedfvIds = new Set<Id>();
    Profile MyProfileName  = [SELECT Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1]; 
    //public boolean IsClosed__c = False;
    if (MyProfileName.Name == 'EMEA EM')
    {
    for (Id fvId : Trigger.newMap.keySet()) {
        if (Trigger.newMap.get(fvId).IsClosed__c) {
            newlyClosedfvIds.add(fvId);
        }
    }

    //query open tasks related to Field_Visit__c and populate map
    for (AggregateResult aggResult : [
            Select Count(Id), WhatId From Task
            Where WhatId In :newlyClosedfvIds
                  And IsClosed = false
            Group by WhatId
            Having Count(Id) > 0
    ]) {
        Id fvId = (Id) aggResult.get('WhatId');
        Field_Visit__c errorfv = Trigger.newMap.get(fvId);

        // change error message as appropriate...
        errorfv.adderror('Cannot close Field visit record since there are open tasks');
    }
    }
}