trigger ConsolidateProductNames on Opportunity (before insert, before update) {

    for (Opportunity o : trigger.new)
    
{
    o.Product_Name_List__c = '';
    o.Product_Family_List__c = '';
        
    List<PricebookEntry> pes = [SELECT Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM OpportunityLineItem WHERE OpportunityId = :o.Id)];
   
    Set<Id> pesIds = new Set<Id>();    
    
    for(PricebookEntry pe : pes)
    {
        pesIds.Add(pe.Product2Id);
        
    }
    List<Product2> products = [SELECT Name FROM Product2 WHERE Id IN :pesIds];
       
    for (Product2 p :products)
    {
        if (p.Name != null)
        {        
           o.Product_Name_List__c += p.Name + ', ';
        }           
    }  
    o.Product_Name_List__c = o.Product_Name_List__c.RemoveEnd(', ');
    
    List<AggregateResult> families = [SELECT Family FROM Product2 WHERE Id IN :pesIds GROUP BY Family];
    
    for (AggregateResult family :families)
    {
         if (family.Get('Family') != null)
          {
             o.Product_Family_List__c += family.Get('Family') + ', ';
              }
    }
     
      o.Product_Family_List__c = o.Product_Family_List__c.RemoveEnd(', ');            
}}