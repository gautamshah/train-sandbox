//
/*
Author             Date          Details
----------------------------------------
Amogh ghodke  23 Feb 2016  Modified line no.29 & removed ' && Userinfo.getUserId() != Trigger.new[i].Manager_Mentor__c)' */
//
trigger IndividualDevelopmentPlanApexShare on Individual_Development_Plan__c (after insert, after update) {

Map<Id, Set<Id>> SharesToCreate = new Map<Id, Set<Id>>();
    
    List <Individual_Development_Plan__Share> SharesForInsert = new List<Individual_Development_Plan__Share>();
    List <Individual_Development_Plan__Share> SharesToDelete = new List<Individual_Development_Plan__Share>();
    
    if (Trigger.isInsert) { //Insert
        
        //Add all Managers to map of shares to create
        
        for (Individual_Development_Plan__c idp : Trigger.new) {
            Set<Id> newIds = new Set<Id>();
            
            if (idp.Manager_Mentor__c != null && Userinfo.getUserId() != idp.Manager_Mentor__c) {
                newIds.add(idp.Manager_Mentor__c);
            }
            
            if (newIds.size() > 0) {
                SharesToCreate.put(idp.Id, newIds);
            }
        }
    }
    else { //Update
        for (integer i = 0; i < Trigger.size; i++) {
            // If manager has changed update info in new shares to create
            
            Set<Id> addIds = new Set<Id>();
            if (Trigger.old[i].Manager_Mentor__c != Trigger.new[i].Manager_Mentor__c) {
                addIds.add(Trigger.new[i].Manager_Mentor__c);
                Individual_Development_Plan__Share dShare = [Select Id, RowCause, UserOrGroupID From Individual_Development_Plan__Share where ParentId = :Trigger.old[i].id And RowCause = 'Manager_Mentor_Defined_on_Record__c' Limit 1];
                SharesToDelete.add(dShare);
                Individual_Development_Plan__Share dShare2 = [Select Id, RowCause, UserOrGroupID From Individual_Development_Plan__Share where ParentId = :Trigger.old[i].id And RowCause = 'IDP_RM_Manager_Granted_Access__c' Limit 1];
                SharesToDelete.add(dShare2);
            }
            if (addIds.size() > 0) {
                SharesToCreate.put(Trigger.new[i].Id, addIds);
            }
        } 
    }
    
    Database.DeleteResult[] SharesDeleteResult = Database.delete(SharesToDelete,false);
    
    if (SharesToCreate.keyset().size() > 0) {
        for (Id idpId : SharesToCreate.keyset()) {
            for (Id userId : SharesToCreate.get(idpId)) {
                Individual_Development_Plan__Share s = new Individual_Development_Plan__Share();
                s.parentId = idpId;
                s.UserOrGroupId = userId;
                s.RowCause = Schema.Individual_Development_Plan__Share.RowCause.Manager_Mentor_Defined_on_Record__c;
                s.AccessLevel = 'Edit';
                sharesForInsert.add(s);
                
                //Add AVP Sharing - RM's Manager
                User avp = [select id, name, Manager.id from User where id = :userId];
                System.debug('AVP is ' + avp.ManagerId);
                Individual_Development_Plan__Share AVP_Share = new Individual_Development_Plan__Share();
                AVP_Share.ParentId = idpId;
                AVP_Share.UserOrGroupId = avp.ManagerId;
                AVP_Share.AccessLevel = 'edit';
                AVP_Share.RowCause = Schema.Individual_Development_Plan__Share.RowCause.IDP_RM_Manager_Granted_Access__c;
                sharesForInsert.add(AVP_Share);
            }
        }
    }
    
    // Iterate through each returned result
    for(Database.DeleteResult dr : SharesDeleteResult) {
        if (dr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully deleted share with ID: ' + dr.getId());
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : dr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Account fields that affected this error: ' + err.getFields());
            }
        }
    }
    
    if (SharesForInsert.size() > 0) {
        insert SharesForInsert;
    }
   
   }