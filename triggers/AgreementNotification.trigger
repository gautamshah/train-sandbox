trigger AgreementNotification on Agreement__c (before update) {
        
            /****************************************************************************************
        * Name    : AgreementNotification
        * Author  : Shawn Clark
        * Date    : 01/31/2017
        * Purpose : Time-Based workflow monitors Contract Completion Date. When Date is within 
        *****************************************************************************************/
         
        
          // Step 0: Create a master list to hold the emails that will be sent
          List<Messaging.SingleEmailMessage> mails = 
          new List<Messaging.SingleEmailMessage>();
          
          for (Agreement__c myAgreement : Trigger.new) {
          
          String AgreementGuid = myAgreement.ID;
          String OrgURL = System.URL.getSalesforceBaseURL().toExternalForm(); 
          list<Demo_Product__c> MyAssets = new List<Demo_Product__c>();
          set<ID> AssetOwners = new Set<ID>();
          list<User> MyRegionManagers = new List<User>(); 
          set<String> UniqueEmailList = new Set<String>();
          set<String> UniqueCCEmailList = new Set<String>();
          DateTime dt = myAgreement.Contract_Completion_Date__c; 
          String NotificationStage = myAgreement.Notification_Stage__c;
          String CCD_DateString = '';
          IF(dt != null)
          {
              CCD_DateString = dt.format('MM/dd/yyyy');
          }
          

          
          System.Debug('The Value of Notification Stage is: ' + NotificationStage);
          
           System.Debug('Number of Agreements in Trigger List: ' + Trigger.new.size());
           
           for (Integer i = 0; i< Trigger.new.size(); i++)
                    
                    {
                        System.Debug('My ' + i + ' Agreement is: ' + Trigger.new[i].Name);
                    }
          
         //************************************************************************************ 
         //Two Week Reminder 
         //************************************************************************************ 
         
            IF (myAgreement.Trigger_Email_Alert__c == TRUE && myAgreement.Notification_Stage__c == 'Two_Week_Reminder') {
            System.Debug('This is a Two Week Alert');
        
            //Retrieve Owner Email Addresses & Asset Operations Group Email Addresses
            MyAssets = [SELECT Owner.Email, Owner.Name, Asset_Operations_Email__c FROM Demo_Product__c where Status__c = 'At Customer Site' AND ACT_Number2__c = :AgreementGuid];
            
               //Build Email List          
               for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Owner.Email != null & MyAssets[i].Owner.Name != 'Informatica Cloud')
                    {
                        UniqueEmailList.add(MyAssets[i].Owner.Email.toLowerCase()); // Create Unique Email Set
                    }
               //Add Asset Ops Groups to CC
               for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Asset_Operations_Email__c != null)
                    {
                        UniqueCCEmailList.add(MyAssets[i].Asset_Operations_Email__c.toLowerCase()); // Create Unique Email Set
                    }
          
              // Step 1: Create a new Email
              Messaging.SingleEmailMessage mail = 
              new Messaging.SingleEmailMessage();
            
              // Step 2: Set list of people who should get the email
              List<String> sendTo = new List<String>();
              sendTo.addAll(UniqueEmailList); 
              mail.setToAddresses(sendTo);
            
              // Step 3: Set From to Organization-Wide Email Addresses
                
              for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
              if(owa.Address.contains('notification@covidien.com')) mail.setOrgWideEmailAddressId(owa.id); }

              //(Optional) Set list of people who should be CC'ed
              List<String> ccTo = new List<String>();
              ccTo.addAll(UniqueCCEmailList);
              mail.setCcAddresses(ccTo);

              If(UniqueEmailList.size() == 0) {
              UniqueEmailList.addAll(UniqueCCEmailList);
              UniqueCCEmailList.clear();          
              }
              
              // Step 4. Set email contents - you can use variables!
              mail.setSubject('Evaluation Agreement ' + myAgreement.Name + ' Due in ' +  myAgreement.Days_Until_Due__c + ' Days');
              String body = '<b>Attention Reps:</b> ' + '</br></br>';
              body += 'Agreement <a href="' + OrgURL + '/' +  myAgreement.ID + '">' + myAgreement.Name + '</a>' + ' is due on: <u>' + CCD_DateString + '</u></br></br>';
              body += 'You are receiving this email, because you have outstanding Evaluation Assets that are due in ' +  myAgreement.Days_Until_Due__c + ' Days!' + '</br></br>';
              body += 'To view your outstanding evaluation assets, Please click the link below:' + '</br></br>';
              body += '<a href="' + OrgURL + '/00O0B000003i8ai?pv0=' + myAgreement.Act_Number__c + '"> Oustanding Evaluation Asset Report</a>' +  '</br></br>';
              body += 'Thanks!' + '</br></br>';
              mail.setHtmlBody(body);
            
              // Step 5. Add your email to the master list
              mails.add(mail);
              //Update Flag
             myAgreement.Two_Week_Notification__c = TRUE;
             myAgreement.Two_Week_Notification_Date__c = DateTime.now();
            }
          
          
        //************************************************************************************  
        //One Day Reminder 
        //************************************************************************************ 
          
            IF (myAgreement.Trigger_Email_Alert__c == TRUE && myAgreement.Notification_Stage__c == 'One_Day_Reminder') {
           System.Debug('This is a One Day Reminder');
        
            //Retrieve Owner Email Addresses & Asset Operations Group Email Addresses
            MyAssets = [SELECT Owner.Email, Owner.Name, Asset_Operations_Email__c FROM Demo_Product__c where Status__c = 'At Customer Site' AND ACT_Number2__c = :AgreementGuid];
            
               //Build Email List          
           for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Owner.Email != null & MyAssets[i].Owner.Name != 'Informatica Cloud')
                    {
                        UniqueEmailList.add(MyAssets[i].Owner.Email.toLowerCase()); // Create Unique Email Set
                    }
           //Add Asset Ops Groups to CC
           for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Asset_Operations_Email__c != null)
                    {
                        UniqueCCEmailList.add(MyAssets[i].Asset_Operations_Email__c.toLowerCase()); // Create Unique Email Set
                    }
          
            
              // Step 1: Create a new Email
              Messaging.SingleEmailMessage mail = 
              new Messaging.SingleEmailMessage();
            
              // Step 2: Set list of people who should get the email
              List<String> sendTo = new List<String>();
              sendTo.addAll(UniqueEmailList); 
              mail.setToAddresses(sendTo);
            
              // Step 3: Set From to Organization-Wide Email Addresses
                
              for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
              if(owa.Address.contains('notification@covidien.com')) mail.setOrgWideEmailAddressId(owa.id); } 
            
              //(Optional) Set list of people who should be CC'ed
              List<String> ccTo = new List<String>();
              ccTo.addAll(UniqueCCEmailList);
              mail.setCcAddresses(ccTo);
              
        
              // Step 4. Set email contents - you can use variables!
              mail.setSubject('Evaluation Agreement ' + myAgreement.Name + ' Due in ' +  myAgreement.Days_Until_Due__c + ' Day');
              String body = '<b>Attention Reps:</b> ' + '</br></br>';
              body += 'Agreement <a href="' + OrgURL + '/' +  myAgreement.ID + '">' + myAgreement.Name + '</a>' + ' is due on: <u>' + CCD_DateString + '</u></br></br>';
              body += 'You are receiving this email, because you have outstanding Evaluation Assets that are due in ' +  myAgreement.Days_Until_Due__c + ' Day!' + '</br></br>';
              body += 'To view your outstanding evaluation assets, Please click the link below:' + '</br></br>';
              body += '<a href="' + OrgURL + '/00O0B000003i8ai?pv0=' + myAgreement.Act_Number__c + '"> Oustanding Evaluation Asset Report</a>' +  '</br></br>';
              body += 'Thanks!' + '</br></br>';
              mail.setHtmlBody(body);
            
              // Step 5. Add your email to the master list
              mails.add(mail);
              //Update Flag
             myAgreement.One_Day_Reminder__c = TRUE;
             myAgreement.One_Day_Reminder_Date__c = DateTime.now();
             Date Contract_Completion = myAgreement.Contract_Completion_Date__c;
             myAgreement.Next_Overdue_Alert_Date__c = Contract_Completion.AddDays(3);
            }
            
         //************************************************************************************  
        //Overdue Alert Cycle
        //************************************************************************************ 
          
            IF (myAgreement.Trigger_Email_Alert__c == TRUE && myAgreement.Notification_Stage__c == 'Overdue_Alert_Cycle') {
           
            Decimal DaysOverDue = myAgreement.Days_Until_Due__c*-1;
            System.Debug('This is an Overdue Alert');
           
            System.Debug('Ok, Its getting to the right spot');
           MyAssets = [SELECT OwnerId, Owner.Name, Owner.Email, Asset_Operations_Email__c FROM Demo_Product__c where ACT_Number2__c = :AgreementGuid];
        
           //Add Asset Owner IDs to a Set
           for (Integer i = 0; i< MyAssets.size(); i++)
                    {
                        AssetOwners.add(MyAssets[i].OwnerId); // Create Unique Email Set
                    }
        
           //Retrieve Region Manager Email Addresses based on above set
           MyRegionManagers = [SELECT Manager.Email FROM User where ID IN :AssetOwners];
        
        
           //Build Email List          
           for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Owner.Email != null & MyAssets[i].Owner.Name != 'Informatica Cloud')
                    {
                        UniqueEmailList.add(MyAssets[i].Owner.Email.toLowerCase()); // Create Unique Email Set
                    }
        
              // Step 1: Create a new Email
              Messaging.SingleEmailMessage mail = 
              new Messaging.SingleEmailMessage();
            
              // Step 2: Set list of people who should get the email
              List<String> sendTo = new List<String>();
              sendTo.addAll(UniqueEmailList); 
              mail.setToAddresses(sendTo);
            
              // Step 3: Set From to Organization-Wide Email Addresses
                
              for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
              if(owa.Address.contains('notification@covidien.com')) mail.setOrgWideEmailAddressId(owa.id); }
            
              //Do Not CC Asset Operations on the Loop Notifications
              
              String OD_Count = String.ValueOf(myAgreement.Overdue_Alert_Count__c+1);
              Integer OD_Count_Int = Integer.ValueOf(OD_Count);
            
            
              // Step 4. Set email contents - you can use variables!
              mail.setSubject('Overdue: Evaluation Agreement ' + myAgreement.Name + ' is ' +  DaysOverDue + ' Day Overdue!');
              String body = '<b>Attention Reps:</b> ' + '</br></br>';
              body += 'Overdue Warning (' + OD_Count  +'): Agreement <a href="' + OrgURL + '/' +  myAgreement.ID + '">' + myAgreement.Name + '</a>' + ' was due on: <u>' + CCD_DateString + '</u></br></br>';
              body += 'You are receiving this email, because you have Evaluation Assets that are <b><font color="red">' +  DaysOverDue + ' Day(s) Overdue!</font></b>' + '</br></br>';
              body += 'To view your outstanding evaluation assets, Please click the link below:' + '</br></br>';
              body += '<a href="' + OrgURL + '/00O0B000003i8ai?pv0=' + myAgreement.Act_Number__c + '"> Oustanding Evaluation Asset Report</a>' +  '</br></br>';
              body += 'Thanks!' + '</br></br>';
              mail.setHtmlBody(body);
            
              // Step 5. Add your email to the master list
              mails.add(mail);
              //Update Flag
             myAgreement.Overdue_Alert_Cycle__c = TRUE;
             myAgreement.Overdue_Alert_Cycle_Date__c = DateTime.now();
             myAgreement.Overdue_Alert_Count__c = myAgreement.Overdue_Alert_Count__c+1;
             Datetime Next_Alert_Date = myAgreement.Next_Overdue_Alert_Date__c;
             IF(OD_Count_Int < 5) {
             myAgreement.Next_Overdue_Alert_Date__c = Next_Alert_Date.AddDays(3);
             }
             ELSE {
             myAgreement.Next_Overdue_Alert_Date__c = NULL;
             }
             
            } 
         //************************************************************************************  
        //Final Alert (Escalation to RM)
        //************************************************************************************ 
          
            IF (myAgreement.Trigger_Email_Alert__c == TRUE && myAgreement.Notification_Stage__c == 'Final_Alert') {
           System.Debug('This is a Final Alert');
            Decimal DaysOverDue = myAgreement.Days_Until_Due__c*-1;
            system.debug('Decimal Value of Days overdue is: ' + DaysOverDue);
           
            
           MyAssets = [SELECT OwnerId, Owner.Name, Owner.Email, Asset_Operations_Email__c FROM Demo_Product__c where ACT_Number2__c = :AgreementGuid];
        
           //Add Asset Owner IDs to a Set
           for (Integer i = 0; i< MyAssets.size(); i++)
                    {
                        AssetOwners.add(MyAssets[i].OwnerId); // Create Unique Email Set
                    }
        
           //Retrieve Region Manager Email Addresses based on above set
           MyRegionManagers = [SELECT Manager.Email FROM User where ID IN :AssetOwners];
        
        
           //Build Email List          
           for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Owner.Email != null & MyAssets[i].Owner.Name != 'Informatica Cloud')
                    {
                        UniqueEmailList.add(MyAssets[i].Owner.Email.toLowerCase()); // Create Unique Email Set
                    }
           //Add Asset Ops Groups to CC
           for (Integer i = 0; i< MyAssets.size(); i++)
                    IF(MyAssets[i].Asset_Operations_Email__c != null)
                    {
                        UniqueCCEmailList.add(MyAssets[i].Asset_Operations_Email__c.toLowerCase()); // Create Unique Email Set
                    }
        
           //Add Regional Managers to CC 
            for (Integer i = 0; i< MyRegionManagers.size(); i++)
                    IF(MyRegionManagers[i].Manager.Email != null)
                    {
                        UniqueCCEmailList.add(MyRegionManagers[i].Manager.Email.toLowerCase()); // Create Unique Email Set
                    }
          
            
              // Step 1: Create a new Email
              Messaging.SingleEmailMessage mail = 
              new Messaging.SingleEmailMessage();
            
              // Step 2: Set list of people who should get the email
              List<String> sendTo = new List<String>();
              sendTo.addAll(UniqueEmailList); 
              mail.setToAddresses(sendTo);
            
              // Step 3: Set From to Organization-Wide Email Addresses
                
              for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
              if(owa.Address.contains('notification@covidien.com')) mail.setOrgWideEmailAddressId(owa.id); }
                
                
              //(Optional) Set list of people who should be CC'ed
              List<String> ccTo = new List<String>();
              ccTo.addAll(UniqueCCEmailList);
              mail.setCcAddresses(ccTo);
              
        
              // Step 4. Set email contents - you can use variables!
              mail.setSubject('FINAL NOTIFICATION: Evaluation Agreement ' + myAgreement.Name + ' is ' +  DaysOverDue + ' Day Overdue!');
              String body = '<b>Attention Reps & Region Managers:</b> ' + '</br></br>';
              body += 'Agreement <a href="' + OrgURL + '/' +  myAgreement.ID + '">' + myAgreement.Name + '</a>' + ' was due on: <u>' + CCD_DateString + '</u></br></br>';
              body += 'You are receiving this email, because you have Evaluation Assets that are <b><font color="red">' +  DaysOverDue + ' Day(s) Overdue!</font></b>' + '</br></br>';
              body += 'To view your outstanding evaluation assets, Please click the link below:' + '</br></br>';
              body += '<a href="' + OrgURL + '/00O0B000003i8ai?pv0=' + myAgreement.Act_Number__c + '"> Oustanding Evaluation Asset Report</a>' +  '</br></br>';
              body += 'Thanks!' + '</br></br>';
              mail.setHtmlBody(body);
            
              // Step 5. Add your email to the master list
              mails.add(mail);
              //Update Flag
             myAgreement.Final_Alert__c = TRUE;
             myAgreement.Final_Alert_Date__c = DateTime.now();
            }

           myAgreement.Trigger_Email_Alert__c = FALSE;
          
         } 
          
          // Step 6: Send all emails in the master list
          Messaging.sendEmail(mails);
          
        }