trigger FieldVisitApexShare2 on Field_Visit__c (after insert, after update) {  
    
    List<Field_Visit__Share> fvShares = new List<Field_Visit__Share>();
    List<Field_Visit__Share> fvSharesToDelete = new List<Field_Visit__Share>();
    
    //Inserts
    if(trigger.isInsert){  //start insert
        for(Field_Visit__c fv: trigger.new){
            
            System.debug('FST = ' + fv.FST__c);
            System.debug('Seller = ' + fv.Seller__c);
            System.debug('Sales Rep = ' + fv.Sales_Rep__c);
            System.debug('RM = ' + fv.RM__c);
            System.debug('Manager = ' + fv.Manager__c);
            System.debug('Mentee = ' + fv.Mentee__c);	
            
            if (fv.FST__c != null) {
                //Add FST Sharing
                Field_Visit__Share FST_Share = new Field_Visit__Share(); 
                FST_Share.ParentId = fv.Id;  
                FST_Share.UserOrGroupId = fv.FST__c;  
                FST_Share.AccessLevel = 'edit';  
                FST_Share.RowCause = Schema.Field_Visit__Share.RowCause.FST_Defined_on_Record__c; 
                fvShares.add(FST_Share);
            }
            
            if (fv.Seller__c != null) {
                //Add Seller Sharing
                Field_Visit__Share Seller_Share = new Field_Visit__Share();
                Seller_Share.ParentId = fv.Id;  
                Seller_Share.UserOrGroupId = fv.Seller__c;  
                Seller_Share.AccessLevel = 'edit';  
                Seller_Share.RowCause = Schema.Field_Visit__Share.RowCause.Seller_Defined_on_Record__c;  
                fvShares.add(Seller_Share);
            }
            
            if (fv.RM__c != null) {
                //Add RM Sharing
                Field_Visit__Share RM_Share = new Field_Visit__Share();
                RM_Share.ParentId = fv.Id;  
                RM_Share.UserOrGroupId = fv.RM__c;
                System.debug('Regional Manager is ' + fv.RM__c);
                RM_Share.AccessLevel = 'edit';  
                RM_Share.RowCause = Schema.Field_Visit__Share.RowCause.RM_Defined_on_Record__c;
                fvShares.add(RM_Share);
                
                //Add AVP Sharing - RM's Manager
                User avp = [select id, name, Manager.id from User where id = :fv.RM__c];
                System.debug('AVP is ' + avp.ManagerId);
                Field_Visit__Share AVP_Share = new Field_Visit__Share();
                AVP_Share.ParentId = fv.Id;
                AVP_Share.UserOrGroupId = avp.ManagerId;
                AVP_Share.AccessLevel = 'edit';
                AVP_Share.RowCause = Schema.Field_Visit__Share.RowCause.RM_Manager_Granted_Access__c;
                fvShares.add(AVP_Share);
            }
            
            if (fv.Mentee__c != null) {
                //Add Mentee Sharing
                Field_Visit__Share Mentee_Share = new Field_Visit__Share();
                Mentee_Share.ParentId = fv.Id;
                Mentee_Share.UserOrGroupId = fv.Mentee__c;
                Mentee_Share.AccessLevel = 'edit';
                Mentee_Share.RowCause = Schema.Field_Visit__Share.RowCause.Mentee_Defined_on_Record__c;
                fvShares.add(Mentee_Share);
            }
            
            if (fv.Manager__c != null) {
                //Add Manager Sharing
                Field_Visit__Share Manager_Share = new Field_Visit__Share(); 
                Manager_Share.ParentId = fv.Id;  
                Manager_Share.UserOrGroupId = fv.Manager__c;  
                Manager_Share.AccessLevel = 'edit';  
                Manager_Share.RowCause = Schema.Field_Visit__Share.RowCause.Manager_Defined_on_Record__c; 
                fvShares.add(Manager_Share);  
                
                //Add AVP Sharing - Manager's Manager
                User avp = [select id, name, Manager.id from User where id = :fv.Manager__c];
                System.debug('AVP is ' + avp.ManagerId);
                Field_Visit__Share AVP_Share = new Field_Visit__Share();
                AVP_Share.ParentId = fv.Id;
                AVP_Share.UserOrGroupId = avp.ManagerId;
                AVP_Share.AccessLevel = 'edit';
                AVP_Share.RowCause = Schema.Field_Visit__Share.RowCause.Manager_Mentor_Manager_Granted_Access__c;
                fvShares.add(AVP_Share);
            }
            
            if (fv.Sales_Rep__c != null) {
                //Add Sales Rep Sharing
                Field_Visit__Share Sales_Rep_Share = new Field_Visit__Share(); 
                Sales_Rep_Share.ParentId = fv.Id;  
                Sales_Rep_Share.UserOrGroupId = fv.Sales_Rep__c;  
                Sales_Rep_Share.AccessLevel = 'edit';  
                Sales_Rep_Share.RowCause = Schema.Field_Visit__Share.RowCause.Sales_Rep_Defined_on_Record__c; 
                fvShares.add(Sales_Rep_Share);
            }
            
            else {
                System.debug('All fields were blank');
            }
            
        }
        
        // Insert all of the newly created Share records and capture save result   
        System.debug('The shares to be created are' + fvShares);
        Database.SaveResult[] fvSharesInsertResult = Database.insert(fvShares,false);  
        
    } //end insert
    
    //Updates
    else {  // Trigger is Update
        System.Debug('is Update');
        for (integer i=0;i<Trigger.size;i++) {

            //Updating FST
            if (Trigger.old[i].FST__c != Trigger.new[i].FST__c && Userinfo.getUserId() != Trigger.new[i].FST__c) {
                Field_Visit__Share FST_Share = new Field_Visit__Share(); 
                FST_Share.ParentId = Trigger.new[i].Id;  
                FST_Share.UserOrGroupId = Trigger.new[i].FST__c;  
                FST_Share.AccessLevel = 'edit';  
                FST_Share.RowCause = Schema.Field_Visit__Share.RowCause.FST_Defined_on_Record__c;
                fvShares.add(FST_Share);
                Field_Visit__Share dShare = [Select Id, RowCause, UserOrGroupID From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'FST_Defined_on_Record__c' Limit 1];
                fvSharesToDelete.add(dShare);
            }
            
            //Updating seller
            if (Trigger.old[i].Seller__c != Trigger.new[i].Seller__c && Userinfo.getUserId() != Trigger.new[i].Seller__c) {
                Field_Visit__Share Seller_Share = new Field_Visit__Share();
                Seller_Share.ParentId = Trigger.new[i].Id;  
                Seller_Share.UserOrGroupId = Trigger.new[i].Seller__c;  
                Seller_Share.AccessLevel = 'edit';  
                Seller_Share.RowCause = Schema.Field_Visit__Share.RowCause.Seller_Defined_on_Record__c;  
                fvShares.add(Seller_Share);
                Field_Visit__Share dShare = [Select Id, RowCause, UserOrGroupID From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'Seller_Defined_on_Record__c' Limit 1];
                fvSharesToDelete.add(dShare);
            }
            
            //Updating RM
            if (Trigger.old[i].RM__c != Trigger.new[i].RM__c && Userinfo.getUserId() != Trigger.new[i].RM__c) {
                Field_Visit__Share RM_Share = new Field_Visit__Share();
                RM_Share.ParentId = Trigger.new[i].Id;  
                RM_Share.UserOrGroupId = Trigger.new[i].RM__c;  
                RM_Share.AccessLevel = 'edit';  
                RM_Share.RowCause = Schema.Field_Visit__Share.RowCause.RM_Defined_on_Record__c;
                fvShares.add(RM_Share);
                User avp = [select id, name, Manager.id from user where id = :Trigger.new[i].RM__c];
                Field_Visit__Share AVP_Share = new Field_Visit__Share();
                AVP_Share.ParentId = Trigger.new[i].Id;
                AVP_Share.UserOrGroupId = avp.ManagerId;
                AVP_Share.AccessLevel = 'edit';
                AVP_Share.RowCause = Schema.Field_Visit__Share.RowCause.RM_Manager_Granted_Access__c;
                fvShares.add(AVP_Share);
                Field_Visit__Share dShare = [Select Id, RowCause, UserOrGroupID From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'RM_Defined_on_Record__c' Limit 1];
                fvSharesToDelete.add(dShare);
                Field_Visit__Share dShare2 = [Select Id, RowCause, UserOrGroupID From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'RM_Manager_Granted_Access__c' Limit 1];
                fvSharesToDelete.add(dShare2);
            }
            
            //Updating Manager/Mentor & their manager
            if (Trigger.old[i].Manager__c != Trigger.new[i].Manager__c && Userinfo.getUserId() != Trigger.new[i].Manager__c) {
                Field_Visit__Share Manager_Share = new Field_Visit__Share(); 
                Manager_Share.ParentId = Trigger.new[i].Id;  
                Manager_Share.UserOrGroupId = Trigger.new[i].Manager__c;   
                Manager_Share.AccessLevel = 'edit';  
                Manager_Share.RowCause = Schema.Field_Visit__Share.RowCause.Manager_Defined_on_Record__c; 
                fvShares.add(Manager_Share);
                User avp = [select id, name, Manager.id from User where id = :Trigger.new[i].Manager__c];
                System.debug('AVP is ' + avp.ManagerId);
                Field_Visit__Share AVP_Share = new Field_Visit__Share();
                AVP_Share.ParentId = Trigger.new[i].Id;
                AVP_Share.UserOrGroupId = avp.ManagerId;
                AVP_Share.AccessLevel = 'edit';
                AVP_Share.RowCause = Schema.Field_Visit__Share.RowCause.Manager_Mentor_Manager_Granted_Access__c;
                fvShares.add(AVP_Share);
                Field_Visit__Share dShare = [Select Id, RowCause, UserOrGroupID From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'Manager_Defined_on_Record__c' Limit 1];
                fvSharesToDelete.add(dShare);
                Field_Visit__Share dShare2 = [Select Id, RowCause, UserOrGroupID From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'Manager_Mentor_Manager_Granted_Access__c' Limit 1];
                fvSharesToDelete.add(dShare2);
            }
            
            //Updating Sales Rep
            if (Trigger.old[i].Sales_Rep__c != Trigger.new[i].Sales_Rep__c && Userinfo.getUserId() != Trigger.new[i].Sales_Rep__c) {
                Field_Visit__Share Sales_Rep_Share = new Field_Visit__Share(); 
                Sales_Rep_Share.ParentId = Trigger.new[i].Id;  
                Sales_Rep_Share.UserOrGroupId = Trigger.new[i].Sales_Rep__c;   
                Sales_Rep_Share.AccessLevel = 'edit';  
                Sales_Rep_Share.RowCause = Schema.Field_Visit__Share.RowCause.Sales_Rep_Defined_on_Record__c; 
                fvShares.add(Sales_Rep_Share);
                Field_Visit__Share dShare = [Select Id From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'Sales_Rep_Defined_on_Record__c' Limit 1];
                fvSharesToDelete.add(dShare);
            }
            
            //Updating Mentee
            if (Trigger.old[i].Mentee__c != Trigger.new[i].Mentee__c && Userinfo.getUserId() != Trigger.new[i].Mentee__c) {
                Field_Visit__Share Mentee_Share = new Field_Visit__Share();
                Mentee_Share.ParentId = Trigger.new[i].Id;
                Mentee_Share.UserOrGroupId = Trigger.new[i].Mentee__c;
                Mentee_Share.AccessLevel = 'edit';
                Mentee_Share.RowCause = Schema.Field_Visit__Share.RowCause.Mentee_Defined_on_Record__c;
                fvShares.add(Mentee_Share);
                Field_Visit__Share dShare = [Select Id From Field_Visit__Share where ParentId = :Trigger.old[i].id And RowCause = 'Mentee_Defined_on_Record__c' Limit 1];
                fvSharesToDelete.add(dShare);
            }
        }
    
        Database.SaveResult[] fvSharesInsertResult = Database.insert(fvShares,false);
        Database.DeleteResult[] fvSharesDeleteResult = Database.delete(fvSharesToDelete,false);
    
        // Iterate through each returned result
        for(Database.DeleteResult dr : fvSharesDeleteResult) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted share with ID: ' + dr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
    
}