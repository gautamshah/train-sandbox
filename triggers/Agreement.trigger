/****************************************************************************************
* Name    : Trigger: Agreement
* Author  : Paul Berglund
* Date    : 10/13/2015
* Purpose : Single entry point for all Agreement triggers
* 
* Dependancies: 
*   Class: AgreementTriggerDispatcher           
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/ 
trigger Agreement on Apttus__APTS_Agreement__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
}