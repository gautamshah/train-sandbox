/****************************************************************************************
* Name    : Trigger: IDNSC_Relationship
* Author  : Gautam Shah
* Date    : 6/12/2014
* Purpose : Single entry point for all IDNSC_Relationship__c triggers
* 
* Dependancies: 
*   Class: IDNSC_Relationship_TriggerDispatcher           
*      
* ========================
* = MODIFICATION HISTORY =
* ========================
* DATE          AUTHOR                  CHANGE
* ----          ------                  ------
* 
*****************************************************************************************/ 
trigger IDNSC_Relationship on IDNSC_Relationship__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
	IDNSC_Relationship_TriggerDispatcher.Main(trigger.new, trigger.newMap, trigger.old, trigger.oldMap, trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isExecuting);
}