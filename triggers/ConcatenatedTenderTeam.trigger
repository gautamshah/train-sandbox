/****************************************************************************************
    * Name    : ConcatenatedTenderTeam
    * Author  : Lakhan Dubey
    * Date    : 10/14/2014
    * Purpose : Trigger to populate a hidden concatenated Tender Team field on Tender.
    * Dependancies: Tenders__c,Tender_Team__c        
    *****************************************************************************************/

trigger ConcatenatedTenderTeam on Tender_Team__c(after insert, after update, after delete) {
    if(Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && Utilities.profileMap.get(UserInfo.getProfileId()) != 'System Administrator')
    {
        
        // Max size of Tenders__c.Tender_Team__c
        Integer strLimit = 2000;
                  
            List<Id> TenderIds = new List<Id>();
            List<Tenders__c> Tenders = new List<Tenders__c>();

            if(trigger.isInsert) {
            
                for(Tender_Team__c Tt : trigger.new) {
                   TenderIds.add(Tt.Tender__c);
                }
            } else if(trigger.isDelete|| trigger.isUpdate) {
               for(Tender_Team__c Tt : trigger.old) {
                   TenderIds.add(Tt.Tender__c);
               }
            }
            
            List<Tenders__c> TenderList = [SELECT Id, Tender_Team__c FROM Tenders__c WHERE Id IN :TenderIds];
            for (Tenders__c Tndr : TenderList) {
                Tndr.Tender_Team__c = null;
                
            }
            update TenderList;

            Map<Id, Tenders__c> TenderMap = new Map<Id, Tenders__c>([SELECT Id, Tender_Team__c FROM Tenders__c WHERE Id IN :TenderIds]);
         
            for(Tender_Team__c Tt : [SELECT Id, Tender__r.name,Tender__c,Team_Member__r.name  FROM Tender_Team__c WHERE Tender__c IN :TenderIds ]) {
            if(Tt.Team_Member__r.name!=null){
                Tenders__c Tdr = TenderMap.get(Tt.Tender__c);
                

                String str;
           
                    if(Tdr.Tender_Team__c == null) {
                        Tdr.Tender_Team__c = Tt.Team_Member__r.name;
                       
                    } else { 
                    
                    if(!Tdr.Tender_Team__c.contains(Tt.Team_Member__r.name)) {
                        str = Tdr.Tender_Team__c +', '+Tt.Team_Member__r.name;
                        if (str.length() > strLimit) {
                            str = str.left(strLimit);
                        }
                        Tdr.Tender_Team__c = str;
                    } }
                    TenderMap.put(Tdr.Id,Tdr);
                }
        }

            Tenders = TenderMap.values();

            update Tenders;
      
  }
}