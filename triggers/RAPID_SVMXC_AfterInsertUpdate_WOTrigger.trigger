/******************************************************************************************************* 
     CLIENT
        ORR
    
    DESCRIPTION
        On WO closure process the Usage Consumption Lines and adjust the technician inventory. 
    
    CHANGE LOG
        [[Version; Date; Author; Description]]
        v1.0; 2014/05/06; Sajana Thomas/ServiceMax; Initial release  
        V1.2; 2014/14/07; Chinmay Kant/ServiceMax; Changing Name from SVMX_AfterWOTrigger 
                          to RAPID_SVMXC_AfterInsertUpdate_WOTrigger
*******************************************************************************************************/

trigger RAPID_SVMXC_AfterInsertUpdate_WOTrigger on SVMXC__Service_Order__c (after update, after insert) {
    
    if(trigger.isAfter){
        if(trigger.isUpdate){
            RAPID_SVMXC_VanInventoryManager.startProcessing(trigger.new, trigger.oldMap);
        }
    }
   
}