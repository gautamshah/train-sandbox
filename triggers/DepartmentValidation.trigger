trigger DepartmentValidation on Department__c (after insert) {

    /****************************************************************************************
    * Name    : Department Validation
    * Author  : Mike Melcher
    * Date    : 11-20-2011
    * Purpose : Create Data Quality Case for new Departments created by users
    * 
    * Dependancies: 
    *              
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 3-8-2012     MJM          Changed trigger to After Insert so the new Department record Id 
    *                           properly appears in the Reference field of the Case that gets generated
    * 5-16-2012    MJM          Removed logic to determine Case owner -- now covered in createCase method
    *
    *****************************************************************************************/


List<recordtype> recordtypes = [select id from recordtype where name = 'Unverified Department Record'];
Id pendingRT;
if (recordtypes.size() > 0) {
   pendingRT = recordtypes[0].Id;
}
Id dataloaderProfile;
Id currentUser = UserInfo.getUserId();

for (Profile p : [select Id from Profile where Name = 'API Data Loader']) {
   dataloaderProfile = p.Id;
}
if (UserInfo.getProfileId() != dataloaderProfile) {
   DataQuality dq = new DataQuality();
   
   for (Department__c d : Trigger.new) {
         //dq.createCase('DQ-Validate New Department Record',d.Id,'Data Quality','Medium', currentUser,'Validation','Assigned');
         dq.createCase('DQ-Validate New Department Record',d.Id,'Validation','Medium',currentUser,'Data Quality', 'Assigned','Case','Data Quality Case');
         
   }
   
} 

}