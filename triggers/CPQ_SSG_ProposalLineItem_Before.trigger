trigger CPQ_SSG_ProposalLineItem_Before on Apttus_Proposal__Proposal_Line_Item__c (before insert, before update, before delete) {
	if (Trigger.isInsert || Trigger.isUpdate) {
		CPQ_SSG_Proposal_Processes.SetProposalLineItemApprovalTemplateFields(Trigger.new, Trigger.oldMap);
	}
	if (Trigger.isDelete) {
		cpqProposalLineItem_u.deleteRelatedParticipatingFacilityLineItems(Trigger.old);
	}
}