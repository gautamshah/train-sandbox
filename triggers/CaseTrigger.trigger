/****************************************************************************************
 * Name    : CaseTrigger 
 * Author  : Bill Shan
 * Date    : 19/12/2013 
 * Purpose : Case Trigger Entry
 * Dependencies: Case Object 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE                 AUTHOR              CHANGE
 * ----                 ------              ------
 * 6/1/2015             Bill Shan           Add before insert
 * May 12, 2015         Gautam Shah         Experimenting with dispatcher pattern
 * May 13, 2015         Gautam Shah         Reverting back to handler pattern pending further eval and testing
 *****************************************************************************************/
/*
trigger CaseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
    CaseTriggerDispatcher.Main(trigger.new, trigger.newMap, trigger.old, trigger.oldMap, trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isExecuting);
}
*/

trigger CaseTrigger on Case (before insert, after insert, before update, after update) {
    
    Case_TriggerHandler handler = new Case_TriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert){
        if(Trigger.isBefore)
        {
            handler.OnBeforeInsert(Trigger.new);
        }
        else
        {
            handler.OnAfterInsert(Trigger.new);
        }
    }
    else if(Trigger.isBefore){
        if(Trigger.isUpdate){
          handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap);
          if(!Utilities.executeOnce){
              new CaseUserDetails_Handler().CaseUserDetailsonBefore(Trigger.new,Trigger.newMap,Trigger.oldMap);  //IDNSC Case Feed case#00981782
              Utilities.executeOnce = true;
          }
          }
        else
            handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}