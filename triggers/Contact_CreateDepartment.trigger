/****************************************************************************************
 * Name    : Contact_CreateDepartment
 * Author  : Gautam Shah
 * Date    : 06/25/2013 
 * Purpose : Determine if a department object record exists for the chosen value for the department field on the contact object.  If not, create it.
 * Dependencies: Contact fields: Department_picklist__c, Other_Department__c (does not appear on all page layouts) 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 * 7/11/13      Gautam Shah         Added condition to prevent trigger from invoking if current user's profile is "API Data Loader" or "System Administrator".  This is to prevent errors during DI and bulk data loads.
 * 8/2/13       Gautam Shah         Added condition to prevent trigger from invoking if the Contact recordtype is NOT "In Process Connected Contact".  This is to prevent errors when Master Contact records are created or updated.
 * 3/31/14		Gautam Shah			Commented out SOQL query to Profile object and replaced with call to Utilities.profileMap (Line 20)
 *****************************************************************************************/
trigger Contact_CreateDepartment on Contact (before insert, before update) {
    //Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
    //if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator')
    if(Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && Utilities.profileMap.get(UserInfo.getProfileId()) != 'System Administrator')
    {
        System.debug('CreateDept trigger invoked');
        //get record types
        List<RecordType> contactRtList = new List<RecordType>([Select r.Name, r.Id From RecordType r Where r.SobjectType = 'Contact']);
        Map<String, String> contactRtMap = new Map<String, String>();
        for(RecordType r : contactRtList)
        {
            contactRtMap.put(r.Id, r.Name);
        }
        Boolean containsCorrectRT = false;
        for(Contact c : Trigger.New)
        {
            //Contact RecordType = 'In Process Connected Contact';
            //if(contactRtMap.get(c.RecordTypeId) == 'In Process Connected Contact' && c.Department_picklist__c != null)
            if(c.Department_picklist__c != null)
            {
                containsCorrectRT = true;
                break;
            }
        }
        //
        System.debug('containsCorrectRT: ' + containsCorrectRT);
        if(containsCorrectRT)
        {
            List<RecordType> deptRtList = new List<RecordType>([Select r.Name, r.Id From RecordType r Where r.SobjectType = 'Department__c']);
            Map<String, String> deptRtMap = new Map<String, String>();
            for(RecordType r : deptRtList)
            {
                deptRtMap.put(r.Name, r.Id);
            }
            //populate collections
            Map<String, String> contactDeptMap = new Map<String, String>();
            Set<String> acctIDs = new Set<String>();
            for(Contact c : Trigger.New)
            {
                System.debug('Department Picklist value: ' + c.Department_picklist__c);
                contactDeptMap.put(c.Id, c.Department_picklist__c);
                acctIDs.add(c.AccountId);
            }
            //check departments
            List<Department__c> deptList = new List<Department__c>([Select Id, Name, Account__c From Department__c Where Account__c in :acctIDs]);
            List<Department__c> newDeptList = new List<Department__c>();
            for(Contact c : Trigger.New)
            {
                //if(contactRtMap.get(c.RecordTypeId) == 'In Process Connected Contact' && c.Department_picklist__c != null)
                if(c.Department_picklist__c != null)
                {
                    Boolean deptExists = false;
                    for(Department__c dept : deptList)
                    {
                        if(c.AccountId == dept.Account__c)
                        {
                            System.debug('contactDeptMap value: ' + contactDeptMap.get(c.Id));
                            if((!String.isBlank(contactDeptMap.get(c.Id)) && dept.Name.contains(contactDeptMap.get(c.Id))) || (!String.isBlank(c.Other_Department__c) && dept.Name.contains(c.Other_Department__c)))
                            {
                                System.debug('Department already exists for Account');
                                deptExists = true;
                                c.Department__c = dept.Id;
                                break;
                            }
                        }
                    }
                    if(!deptExists)
                    {
                        Department__c newDept = new Department__c();
                        newDept.Account__c = c.AccountId;
                        System.debug('Other Department Name: ' + c.Other_Department__c);
                        if(contactDeptMap.get(c.Id) == 'Other' && !String.isBlank(c.Other_Department__c))
                        {
                            System.debug('Creating Dept with name from OTHER: ' + c.Other_Department__c);
                            newDept.Name = c.Other_Department__c;
                        }
                        else
                        {
                            System.debug('Creating Dept with name from MAP: ' + contactDeptMap.get(c.Id));
                            newDept.Name = contactDeptMap.get(c.Id);
                        }
                        newDept.RecordTypeId = deptRtMap.get('Unverified Department Record');
                        newDeptList.add(newDept);
                    }
                }
            }
            insert newDeptList;
            for(Contact c : Trigger.New)
            {
                //if(contactRtMap.get(c.RecordTypeId) == 'In Process Connected Contact' && c.Department_picklist__c != null)
                if(c.Department_picklist__c != null)
                {
                    if(contactDeptMap.get(c.Id) != 'Other' && !String.isBlank(c.Other_Department__c))
                    {
                        c.Other_Department__c = null;
                    }
                    System.debug('newDeptList size: ' + newDeptList.size());
                    for(Department__c dept : newDeptList)
                    {
                        if(c.AccountId == dept.Account__c)
                        {
                            System.debug('Department Name: ' + dept.Name);
                            if((!String.isBlank(contactDeptMap.get(c.Id)) && dept.Name.contains(contactDeptMap.get(c.Id))) || (!String.isBlank(c.Other_Department__c) && dept.Name.contains(c.Other_Department__c)))
                            {
                                c.Department__c = dept.Id;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}