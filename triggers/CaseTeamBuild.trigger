trigger CaseTeamBuild on Case (after insert, after update) {
    /********************************************************************************************************************
    * Name    : CaseTeamBuild
    * Author  : John E.J. Hogan (john.hogan@salesforce.com)
    * Date    : 25-March-2013
    * Purpose : Used to create Case Team Member records for Sales Inquiry and R&T Cases
    * 
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE              AUTHOR              CHANGE
    * ---------------   ---------------     -------
    * 25-March-2013     John E.J. Hogan     Created
    * 3/31/14           Gautam Shah         Commented out SOQL query to RecordType and replaced with call to Utilities (Line 42)
    * 6/23/14           Bill Shan           Modify line 26 & 31 logic to Utilities to resolve a Too many query rows issue
    * May 12, 2015		Gautam Shah			DECOMMISSIONED!!! REFACTORED INTO CLASS: CASE_MAIN.CaseTeamBuild
    *********************************************************************************************************************/
    
    /*Build Map to determine if a Case is a Compensation/Sales Inquiry or a  
    List<RecordType> caseRTList = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Case' and (DeveloperName = 'Compensation_Case' or DeveloperName = 'R_T_Case')];
    Map<String, Id> caseRTMap = new Map<String,Id>();
    for(RecordType rt : caseRTList ){
        caseRTMap.put(rt.DeveloperName, rt.Id);
    }
    */
    /*
    // Build a Set of IDs for Active, Standard users
    Map<Id, User> userMap = Utilities.activeStandardUsers_Id_User;
    Set<Id> userIdSet = userMap.keySet();
    
    //Build a Map to reference the CaseTeamRoles
    Map <String, Id> caseTeamRoleMap = new Map<String, ID>();
    for (CaseTeamRole role : Utilities.lstCaseTeamRole){
        caseTeamRoleMap.put(role.Name, role.Id);
    }
    
    List<CaseTeamMember> cTeamMemberListInsert = new List<CaseTeamMember>();  
    List<CaseTeamMember> caseTeamMemberList = [SELECT ParentId, MemberId FROM CaseTeamMember Where ParentId IN :Trigger.new];
    Boolean requestedByOnCaseTeam = FALSE;
    Boolean requestedByManagerOnCaseTeam = FALSE;
    
    for (Case c : Trigger.new){
        //Create Case Team Members only if the Case is a Compensation/Sales Inquiry Case or R&T Case
        //if (c.RecordTypeId == caseRTMap.get('Compensation_Case') || c.RecordTypeId == caseRTMap.get('R_T_Case')){
        if (c.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('Compensation_Case') || c.RecordTypeId == Utilities.recordTypeMap_DeveloperName_Id.get('R_T_Case')){
            //Determine if the user in the Requested By and Manager of Requested By field is already a team member
            for (CaseTeamMember ctm : caseTeamMemberList){
                if (c.Id == ctm.ParentId){
                    if(c.Requested_By__c == ctm.MemberId)
                        requestedByOnCaseTeam = TRUE;
                    if (c.Manager_of_Requested_By__c == ctm.MemberId)
                        requestedByManagerOnCaseTeam = TRUE;
                }
            }
            
            if (c.Requested_By__c != null && !requestedByOnCaseTeam && userIdSet.contains(c.Requested_By__c)){
                CaseTeamMember caseTeamMemberRequestedBy = new CaseTeamMember();
                caseTeamMemberRequestedBy.MemberId = c.Requested_By__c;
                caseTeamMemberRequestedBy.ParentId = c.Id;
                caseTeamMemberRequestedBy.TeamRoleId = caseTeamRoleMap.get('Sales Rep');
    
                cTeamMemberListInsert.add(caseTeamMemberRequestedBy);
            }
                
            if (c.Manager_of_Requested_By__c != null && !requestedByManagerOnCaseTeam && c.Requested_By__c != c.Manager_of_Requested_By__c && userIdSet.contains(c.Manager_of_Requested_By__c)){
                CaseTeamMember caseTeamMemberManagerOfRequestedBy = new CaseTeamMember();
                caseTeamMemberManagerOfRequestedBy.MemberId = c.Manager_of_Requested_By__c;
                caseTeamMemberManagerOfRequestedBy.ParentId = c.Id;
                caseTeamMemberManagerOfRequestedBy.TeamRoleId = caseTeamRoleMap.get('Sales Manager');
    
                cTeamMemberListInsert.add(caseTeamMemberManagerOfRequestedBy);      
            }
        }   
    } // end if: Record Type check
    
    if (cTeamMemberListInsert.size() > 0)
        insert cTeamMemberListInsert;
    */
}