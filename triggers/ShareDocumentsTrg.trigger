trigger ShareDocumentsTrg on Distributor_Document__c (before insert, before update, after insert, after update) {
    /*
    if(trigger.isBefore){
    
        for(Distributor_Document__c dd : trigger.new){
            if(trigger.isInsert){
                if(dd.Document_Type__c == 'Other - Please specify in comments')
                    dd.Hide_from_Partner_User__c = true;
            }  
             
            //if(trigger.isUpdate){
            //    if(trigger.oldMap.get(dd.id).Hide_from_Partner_User__c == dd.Hide_from_Partner_User__c){
            //        if(dd.Document_Type__c == 'Other - Please specify in comments')
            //            dd.Hide_from_Partner_User__c = true;
            //        else
            //            dd.Hide_from_Partner_User__c = false; 
            //    }
            }
        }
    }*/
    
    if(trigger.isAfter){
    
        Set<Id> accIds = new Set<Id>();        
        Set<Id> RemoveaccIds = new Set<Id>(); 
        for(Distributor_Document__c dd : trigger.new){
            
            if(dd.Hide_from_Partner_User__c == false){
                accIds.add(dd.Account_Name__c);
            }  
            
            if(dd.Hide_from_Partner_User__c == true){
                RemoveaccIds.add(dd.Id);
            }  
        
        }
        System.debug('>>>>>>>>accIds>>>>>>>>>>'+accIds.size());
        List<User> lstUsers = [Select id, accountId from User where isActive=true and UserRoleId != null];
        Map<Id,Set<Id>> mpUsers = new Map<Id,Set<Id>>();
        
        for(User u : lstUsers){
            
            if(mpUsers.get(u.accountId) == null)
               mpUsers.put(u.accountId,new Set<Id>{u.Id});
            else
               mpUsers.get(u.accountId).add(u.Id);     
            
        }
        if(accIds.size()>0){
            List<Distributor_Document__Share> lstShare = new List<Distributor_Document__Share>();
            for(Distributor_Document__c dd : trigger.new){
                if(mpUsers.get(dd.Account_Name__c) != null){
                    for(Id UserId : mpUsers.get(dd.Account_Name__c)){
                        Distributor_Document__Share dds = new Distributor_Document__Share();
                        dds.AccessLevel='Read';
                        dds.parentId = dd.Id;
                        dds.UserOrGroupId=UserId;
                        dds.RowCause = Schema.Distributor_Document__Share.RowCause.Manual;
                        lstShare.add(dds);
                    }
                }
                
            }
            System.debug('>>>>lstshare>>>>>>>>>>>>>>'+lstshare.size());
            if(lstShare.size()>0)
            insert lstShare;
        }
        
        if(RemoveaccIds.size()>0){
           
            List<Distributor_Document__Share> lstShare = new List<Distributor_Document__Share>();
            for(Distributor_Document__Share dds : [Select id,parentId,UserOrGroupId,parent.Account_Name__c from Distributor_Document__Share where parentId In: removeaccIds and RowCause=:Schema.Distributor_Document__Share.RowCause.Manual]){
                if(mpUsers.get(dds.parent.Account_Name__c).contains(dds.userOrGroupid)){
                   lstShare.add(dds);
                }
            }
            
            if(lstShare.size()>0)
            delete lstShare;
        
        }
    
    }
    
}