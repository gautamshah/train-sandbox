/**
trigger on Product2.

CHANGE HISTORY
===============================================================================
DATE            NAME				DESC
2015-01-07      Yuli Fintescu		Created. Auto select opp price book when apttus proposal is created
2015-01-16      Yuli Fintescu		Updated. Load all ERPs of Primary account and in proposal when account lookup updated
2015-01-16      Yuli Fintescu		Updated. Update Config with proposal COOP fields, When proposal COOP field changed
2016-11-01      Isaac Lewis         Migrated logic to cpqProposal_u.main()
===============================================================================
*/
trigger CPQ_Proposal_After on Apttus_Proposal__Proposal__c (after insert, after update) {
    // if (CPQ_Trigger_Profile__c.getInstance().CPQ_Proposal_After__c == true) {
	// 	return;
    // }
	//
	// Set<ID> candidates1 = new Set<ID>();
	// Set<ID> candidates2 = new Set<ID>();
	// Map<ID, Apttus_Proposal__Proposal__c> candidates3 = new Map<ID, Apttus_Proposal__Proposal__c>();
	// for (Apttus_Proposal__Proposal__c p : Trigger.new) {
	// 	//Auto select opp price book when apttus proposal is created
	// 	if (p.Apttus_Proposal__Opportunity__c != null &&
	// 			(Trigger.isInsert ||
	// 			Trigger.isUpdate && p.Apttus_Proposal__Opportunity__c != Trigger.oldMap.get(p.ID).Apttus_Proposal__Opportunity__c)) {
	// 		candidates1.add(p.ID);
	// 	}
	//
	// 	//Load all ERPs of Primary account and in proposal when account lookup updated
	// 	if (p.Apttus_Proposal__Account__c != null &&
	// 			(Trigger.isInsert ||
	// 			Trigger.isUpdate && p.Apttus_Proposal__Account__c != Trigger.oldMap.get(p.ID).Apttus_Proposal__Account__c)) {
	// 		candidates2.add(p.ID);
	// 	}
	//
	// 	//Update Config with proposal COOP fields, When proposal COOP field changed
	// 	if (Trigger.isUpdate) {
	// 		Apttus_Proposal__Proposal__c oldp = Trigger.oldMap.get(p.ID);
	//
	// 		if (p.Duration__c != oldp.Duration__c ||
	// 				p.Buyout_as_of_Date__c != oldp.Buyout_as_of_Date__c ||
	// 				p.Buyout_Amount__c != oldp.Buyout_Amount__c ||
	// 				p.Tax_Gross_Net_of_TradeIn__c != oldp.Tax_Gross_Net_of_TradeIn__c ||
	// 				p.Apttus_Proposal__Sales_Tax_Percent__c != oldp.Apttus_Proposal__Sales_Tax_Percent__c ||
	// 				p.TBD_Amount__c != oldp.TBD_Amount__c ||
	// 				p.Total_Annual_Sensor_Commitment_Amount__c != oldp.Total_Annual_Sensor_Commitment_Amount__c) {
	// 			candidates3.put(p.ID, p);
	// 		}
	// 	}
	// }
	//
	// CPQ_PriceListProcesses.SelectOpportunityPriceBook(candidates1);
	// CPQ_ProposalProcesses.LoadERPAddresses(candidates2);
	// CPQ_ProposalProcesses.UpdateConfig(candidates3);
}