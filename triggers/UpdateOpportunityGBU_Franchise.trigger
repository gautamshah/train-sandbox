trigger UpdateOpportunityGBU_Franchise on Opportunity (before insert, before update) {
/****************************************************************************************
DEPRECATED!!!
This functionality has been moved to class Opportunity_Main.UpdateOpportunityGBU_Franchise()

By: Gautam Shah
When: 27 - June - 2014


    * Name    : UpdateOpportunityGBU_Franchise
    * Author  : Nathan Shinn
    * Date    : 08/24/2011
    * Purpose : Sets the Opportunity's GBU and Franchise to the values on the Opportunity Owner's
    *           User record.
    * 
    * Dependancies: Opportunity
    *              ,User
    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE          AUTHOR              CHANGE
    * ----          ------              ------
    * 4-23-2012     MJM                 Updated to directly check the Profile of the current user
    * 3/30/2014     Gautam Shah         Commented out SOQL query to Profile object and replaced with call to Utilities.profileMap (Line 34)
    *****************************************************************************************
    set<Id> userIds = new set<Id>();
    //get the list of user Ids for the owner column
    for(Opportunity o : trigger.new)
    {
        userIds.add(o.OwnerId);
    }
    //create a map of owner to user for retrieving the GBU and Franchise Ids
    Map<id,User> userMap  = new Map<id,User>([select Id, Franchise__c, Business_Unit__c from User where Id in :userIds ]);
    if (Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && Utilities.profileMap.get(UserInfo.getProfileId())!='CA DQ Analyst'
     && Utilities.profileMap.get(UserInfo.getProfileId()) !='Canada - All'  && Utilities.profileMap.get(UserInfo.getProfileId())!='Canada - EbD' 
     && Utilities.profileMap.get(UserInfo.getProfileId())!='Canada - RMS' && Utilities.profileMap.get(UserInfo.getProfileId())!='Canada - SD'  && Utilities.profileMap.get(UserInfo.getProfileId())!='Canada - VT') 
    {
        
       for(Opportunity o : trigger.new)
        {
            //update GBU and Franchise to the Owner's values
            o.Franchise__c = userMap.get(o.OwnerId).Franchise__c;
            o.Business_Unit__c = userMap.get(o.OwnerId).Business_Unit__c;
        }
    }
    Else if(Utilities.profileMap.get(UserInfo.getProfileId())=='CA DQ Analyst' || Utilities.profileMap.get(UserInfo.getProfileId())=='Canada - All'  
    || Utilities.profileMap.get(UserInfo.getProfileId())=='Canada - EbD' || Utilities.profileMap.get(UserInfo.getProfileId())=='Canada - RMS' || Utilities.profileMap.get(UserInfo.getProfileId())=='Canada - SD'  
    || Utilities.profileMap.get(UserInfo.getProfileId())=='Canada - VT') 
    {
        
        for(Opportunity o : trigger.new)
        {
            //update Franchise to the Owner's values
            o.Franchise__c = userMap.get(o.OwnerId).Franchise__c;
            
        }
    }
*/
}