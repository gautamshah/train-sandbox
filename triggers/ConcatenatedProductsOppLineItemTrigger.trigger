/****************************************************************************************
    * Name    : PopulateConcatenateProduct Field
    * Author  : Netta Grant
    * Date    : 05-20-2013
    * Purpose : Trigger to populate a hidden concatenated product field on Opportunity.
    * 
    * Dependancies: Opportunity
    *              ,OpportunityLineItem    *      
    * ========================
    * = MODIFICATION HISTORY =
    * ========================
    * DATE        AUTHOR               CHANGE
    * ----        ------               ------
    * 05-20-2013  Netta Grant        Created
    * 10-02-2013  Kan Hayashi        Modified
    *                                                    - fix for erroring out at max size of Opportunity.Type_of_Products__c 
    *                                                    - fix potential bug for not initiallizing all Opportunities at start of the program
    *                                                    - added "ORDER BY" clause to SOQL on OpportunityLineItems for the main loop
    * 4 Aug 2016 Amogh Ghodke        Added                - added OpportunityLineItem Description in line 65 to append to str                 
    *
    ****************************************************************************************
*/

trigger ConcatenatedProductsOppLineItemTrigger on OpportunityLineitem(after insert, after update, after delete) {
    //Profile currentProfile = [Select Name From Profile Where Id = :UserInfo.getProfileId() Limit 1];
    //if(currentProfile.Name != 'API Data Loader' && currentProfile.Name != 'System Administrator') 
    if(Utilities.profileMap.get(UserInfo.getProfileId()) != 'API Data Loader' && Utilities.profileMap.get(UserInfo.getProfileId()) != 'System Administrator')
    {
        
        // Max size of Opportunity.Type_of_Products__c
        Integer strLimit = 2000;
            
        if(trigger.isInsert || trigger.isDelete) {
            List<Id> OppIds = new List<Id>();
            List<Opportunity> Oppts = new List<Opportunity>();

            if(trigger.isInsert) {
                for(OpportunityLineitem Opl : trigger.new) {
                    OppIds.add(opl.OpportunityId);
                }
            } else if(trigger.isDelete) {
               for(OpportunityLineitem Opl : trigger.old) {
                   OppIds.add(opl.OpportunityId);
               }
            }

            List<Opportunity> OpptyList = [SELECT Id, Type_of_Products__c FROM Opportunity WHERE Id IN :OppIds];
            for (Opportunity Oppty2 : OpptyList) {
                Oppty2.Type_of_Products__c = null;
                Oppty2.By_Pass_ProductRequiredForUSMedSuppVali__c = true;
            }
            update OpptyList;

            Map<Id, Opportunity> OppMap = new Map<Id, Opportunity>([SELECT Id, Type_of_Products__c FROM Opportunity WHERE Id IN :OppIds]);
         
            for(OpportunityLineItem Opl : [SELECT Id, OpportunityId, PriceBookEntry.Product2.Name,  Description  FROM OpportunityLineItem WHERE OpportunityId IN :OppIds ORDER BY PriceBookEntry.Product2.Name]) {
                Opportunity Oppty = OppMap.get(Opl.OpportunityId);
                Oppty.By_Pass_ProductRequiredForUSMedSuppVali__c = true;

                String str;
                if(opl.PriceBookEntry.Product2.Name <> null) {
                    if(Oppty.Type_of_Products__c == null) {
                        if(Opl.Description == '' || Opl.Description == NULL )
                        Oppty.Type_of_Products__c = Opl.PriceBookEntry.Product2.Name +', ';
                        else
                        Oppty.Type_of_Products__c = Opl.PriceBookEntry.Product2.Name +'-'+Opl.Description +', ';    
                    } else if(!Oppty.Type_of_Products__c.contains(Opl.PriceBookEntry.Product2.Name)) {
                        if(Opl.Description == '' || Opl.Description == NULL )
                        str = Oppty.Type_of_Products__c + Opl.PriceBookEntry.Product2.Name +', ';
                        else
                        str = Oppty.Type_of_Products__c + Opl.PriceBookEntry.Product2.Name +'-'+Opl.Description +', ';  
                        if (str.length() > strLimit) {
                            str = str.left(strLimit);
                        }
                        Oppty.Type_of_Products__c = str;
                    }
                    OppMap.put(Oppty.Id, Oppty);
                }
            }

            Oppts = OppMap.values();

            update Oppts;
        }
    }
}