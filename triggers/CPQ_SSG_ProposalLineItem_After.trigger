trigger CPQ_SSG_ProposalLineItem_After on Apttus_Proposal__Proposal_Line_Item__c (after insert, after update) {
	Set<Id> proposalIds = new Set<Id>();
	for (Apttus_Proposal__Proposal_Line_Item__c pli: Trigger.new) {
		proposalIds.add(pli.Apttus_Proposal__Proposal__c);
	}

	// Get Proposals that have the correct record type and use that to filter the list of Proposal Line Items
	Map<Id,Apttus_Proposal__Proposal__c> proposalMap = new Map<Id,Apttus_Proposal__Proposal__c>([Select Id From Apttus_Proposal__Proposal__c Where Id in :proposalIds And RecordType.Developername = 'Agreement_Proposal']);
	Map<String,Apttus_Proposal__Proposal_Line_Item__c> proposalPriceLineItemMap = new Map<String,Apttus_Proposal__Proposal_Line_Item__c>();
	for (Apttus_Proposal__Proposal_Line_Item__c pli: Trigger.new) {
		if (proposalMap.containsKey(pli.Apttus_Proposal__Proposal__c)) {
			if (pli.Apttus_Proposal__Proposal__c != null && pli.Apttus_Proposal__Product__c != null) {
				String proposalId = String.valueOf(pli.Apttus_Proposal__Proposal__c);
				String productId = String.valueOf(pli.Apttus_Proposal__Product__c);
				proposalPriceLineItemMap.put(proposalId + '_' + productId, pli);
			}
		}
	}
System.debug('proposalPriceLineItemMap keys = ' + proposalPriceLineItemMap.keySet());
	// Get current price records associated with the proposal and overwrit them with the Proposal Line Item price.
	if (!proposalPriceLineItemMap.isEmpty()) {
		List<CPQ_SSG_QP_Product_Pricing__c> productPrices = [SELECT Id, Price__c, Product__c, Proposal_Product_Combined_Key__c FROM CPQ_SSG_QP_Product_Pricing__c Where Proposal_Product_Combined_Key_Text__c in :proposalPriceLineItemMap.keySet()];
		for (CPQ_SSG_QP_Product_Pricing__c productPrice: productPrices) {
			Decimal newPrice = proposalPriceLineItemMap.get(productPrice.Proposal_Product_Combined_Key__c).Apttus_QPConfig__NetPrice__c;
			if (newPrice != null) {
				productPrice.Price__c = newPrice;
			}
		}
		update productPrices;
	}
}