/***********************
Aurthor: Yap Zhen-Xiong
Email: Zhenxiong.Yap@covidien.com
Description: Trigger on Event to send Email to Task Owner when a Feedback is given. 
             Currently works for Asia only.
             
* ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * 4Nov15      Fenny Saputra        Add Philippines into AllowedCountries list (line 26)
 * 11Feb16     Zhen-Xiong Yap       Added Thailand, Vietnam, India, Malaysia,Indonesia
***********************/

trigger Event_Send_Email_Upon_Mgr_Feedback on Event(before update) {

    Set<Id> ownerIds = new Set<Id>();
    Set<String> AllowedCountries = new Set<String>();
    Map<Id, List<Event>> PassedCriteriaEvents = new Map<id, List<Event>>(); //Owner mapped to multiple Event
    
    //Start-List of allowed Countries for Email notification
    AllowedCountries.add('SG'); AllowedCountries.add('TH');
    AllowedCountries.add('HK'); AllowedCountries.add('VN');
    AllowedCountries.add('KR'); AllowedCountries.add('IN');
    AllowedCountries.add('CN'); AllowedCountries.add('MY');
    AllowedCountries.add('TW'); AllowedCountries.add('ID');
    AllowedCountries.add('PH');
    //End-list
    
    for(Event e: Trigger.new){
        ownerIds.add(e.OwnerId);
    }
    
    Map<id, User> userMap = new Map<id, User>([SELECT Country, Email, Name, LanguageLocaleKey FROM User WHERE Id in :ownerIds]);
    Map<id, User> allowedUserMap = new Map<id, User>();
       
    //Criteria Check for Email Notification
    for(Event e: Trigger.new){
        User owner = userMap.get(e.ownerId);
        Event oldEvent = Trigger.oldMap.get(e.ID);
        
        if(e.JP_Feedback__c != oldEvent.JP_Feedback__c && e.JP_Feedback__c != null){ //check if feedback has been changed
            if(AllowedCountries.contains(owner.Country)){ //check if Owner's Country is in the Allowed Country List
                ownerIds.add(e.ownerId);
                List<Event> eventList;  
                
                if(PassedCriteriaEvents.get(e.ownerId) == null)
                    eventList = new List<Event>(); 
                else
                    eventList = PassedCriteriaEvents.get(e.ownerId);
      
                eventList.add(e);
                PassedCriteriaEvents.put(e.ownerId, eventList);
                allowedUserMap.put(e.ownerId, userMap.get(e.OwnerId));
    
            }
        }
    }
    
    EmailTemplate et_TW;
    EmailTemplate et_CN;
    EmailTemplate et_Default;
    
    for(EmailTemplate et : [SELECT id, developerName, Body, HtmlValue, Subject FROM EmailTemplate WHERE developerName = 'Manager_Feedback_Notification' 
                        OR developerName = 'Manager_Feedback_Notification_TW' OR developerName = 'Manager_Feedback_Notification_CN']){
        String devName = et.developerName;
        if(devName.contains('_TW'))
            et_TW = et;    
        else if(devName.contains('_CN'))
            et_CN = et;  
        else{  
            et_Default = et;      
        }
    }
    
    //Code Section that sends the email.
    for(User u : allowedUserMap.values())
        for(Event e : PassedCriteriaEvents.get(u.id)){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            
            String[] toAddresses = new String[] {u.Email};
            mail.setToAddresses(toAddresses);    // Set the TO addresses  
            
            EmailTemplate et; 
            if(u.LanguageLocaleKey == 'zh_TW')
                et = et_TW;
            else if(u.LanguageLocaleKey == 'zh_CN')
                et = et_CN;
            else
                et = et_Default;
         
            mail.setSenderDisplayName('Covidien - Salesforce.com');
            mail.setReplyTo('no-reply@covidien.com');
            mail.setUseSignature(false);
            
            String subject = et.Subject;
            subject = subject.replace('{!Task.Subject}', e.Subject);
            mail.setSubject(subject);
            
            System.debug('Email Template Body: '+ et.body);
            String htmlBody = et.Body;
            htmlBody = htmlBody.replace('{!Task.Subject}', e.Subject);
            htmlBody = htmlBody.replace('{!Task.JP_Feedback__c}', e.JP_Feedback__c);
            String strURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.id;
            htmlBody = htmlBody.replace('{!Task.Link}', strURL);
            mail.setPlainTextBody(htmlBody); 
                
            Messaging.SendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
    
}