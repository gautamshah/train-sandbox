trigger CPQ_SSG_Product_Price_Before on CPQ_SSG_Product_Price__c (before insert) {
	Set<String> productCodes = new Set<String>();
	Set<String> priceListCodes = new Set<String>();
	Set<CPQ_SSG_Product_Price__c> rowsToUpdate = new Set<CPQ_SSG_Product_Price__c>();

	for (CPQ_SSG_Product_Price__c price: Trigger.new) {
		if (price.Product_Code__c != null) {
			productCodes.add(price.Product_Code__c);
			rowsToUpdate.add(price);
		}
		if (price.Price_List_Code__c != null) {
			priceListCodes.add(price.Price_List_Code__c);
			rowsToUpdate.add(price);
		}
	}

	List<Product2> products = [Select Id, ProductCode From Product2 Where ProductCode in :productCodes];
	Map<String,Product2> productMap = new Map<String,Product2>();
	for (Product2 product: products) {
		productMap.put(product.ProductCode, product);
	}

	List<CPQ_SSG_Price_List__c> priceLists = [Select Id, Price_List_Code__c From CPQ_SSG_Price_List__c Where Price_List_Code__c in :priceListCodes];
	Map<String,CPQ_SSG_Price_List__c> priceListMap = new Map<String,CPQ_SSG_Price_List__c>();
	for (CPQ_SSG_Price_List__c priceList: priceLists) {
		priceListMap.put(pricelist.Price_List_Code__c, priceList);
	}

	for (CPQ_SSG_Product_Price__c price: rowsToUpdate) {
		if (price.Product_Code__c != null) {
			Product2 product = productMap.get(price.Product_Code__c);
			if (product != null) {
				price.Product__c = product.Id;
			}
		}
		if (price.Price_List_Code__c != null) {
			CPQ_SSG_Price_List__c priceList = priceListMap.get(price.Price_List_Code__c);
			if (priceList != null) {
				price.CPQ_SSG_Price_List__c = priceList.Id;
			}
		}
	}
}