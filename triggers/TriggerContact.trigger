/**
*   CreatedDate: 2014.9.5
*   Target:     1) Keep Specialty_PL__c & Department_PL__c field on Opportunity_Contact_Role__c
*                   synced with contact
*
**/

trigger TriggerContact on Contact(after update) {
    if (trigger.isAfter && trigger.isUpdate) {

        // The Opportunity_Contact_Role__c that need to be updated
        List<Opportunity_Contact_Role__c> roles2Update = new List<Opportunity_Contact_Role__c>();

        // Get the contact that have changed Specialty__c or Department_pl__c
        Set<Id> contactIds = new Set<Id>();
        for (Contact ctct : trigger.new) {

            if (ctct.Department_pl__c != trigger.oldMap.get(ctct.Id).Department_pl__c ||
                ctct.Specialty__c != trigger.oldMap.get(ctct.Id).Specialty__c) {
                
                contactIds.add(ctct.Id);
            }
        }

        // All the contacts with roles attached
        List<Contact> contacts = 
            [SELECT Department_pl__c, Specialty__c,
                (SELECT Specialty_PL__c, Department_PL__c FROM Opportunity_Contact_Roles__r)
            FROM Contact WHERE Id IN : contactIds];

        for (Contact ctct : contacts) {
            for (Opportunity_Contact_Role__c role : ctct.Opportunity_Contact_Roles__r) {
                if (role.Specialty_PL__c != ctct.Specialty__c ||
                    role.Department_PL__c != ctct.Department_pl__c) {
                    
                    role.Specialty_PL__c = ctct.Specialty__c;
                    role.Department_PL__c = ctct.Department_pl__c;
                    roles2Update.add(role);
                }
            }
        }

        try {
            update roles2Update;
        }
        catch (DMLException e) {
            trigger.new[0].addError(e.getDMLMessage(0));
            System.debug(' DML Exception: ' + e);
        }
    }
}